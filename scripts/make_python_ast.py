#!/usr/bin/env python

import os
import sys

import asdl


input_path = "."


def is_simple(sum):
    """Return True if a sum is a simple.

    A sum is simple if its types have no fields, e.g.
    unaryop = Invert | Not | UAdd | USub
    """
    for t in sum.types:
        if t.fields:
            return False
    return True


def is_int_only(fields):
    for field in fields:
        if isinstance(field, asdl.Field):
            if field.type.value != 'int':
                return False
        elif field[1] != 'int':
            return False
    return True


def rewrite_fields(fields):
    """Rewrite a sequence of asdl.Field objects into a 4-tuple that contains
    the raw name, the C type for the field, a Boolean indicating whether
    the field is optional (i.e., accepts NULL), and a Boolean indicating
    whether the field is a sequence. Sequences are sky_object_t, because they
    could be lists or tuples; identifers and strings are both sky_string_t;
    bytes is sky_bytes_t; and int is int.
    """
    new_fields = []
    anonymous = {}
    for field in fields:
        if field.name:
            name = field.name
        else:
            name = field.type
            count = anonymous[name] = anonymous.get(name, 0) + 1
            if count > 1:
                name = "%s_%d" % (name, (count - 1))
        if field.seq:
            type = 'sky_object_t'
        elif field.type.value == 'identifier':
            type = 'sky_string_t'
        elif field.type.value == 'int':
            type = 'int'
        elif field.type.value == 'object':
            type = 'sky_object_t'
        elif field.type.value == 'string':
            type = 'sky_string_t'
        elif field.type.value == 'bytes':
            type = 'sky_bytes_t'
        else:
            type = 'sky_python_ast_%s_t' % (field.type)
        new_fields.append((str(name), type, field.opt, field.seq))
    return tuple(new_fields)


class EmitVisitor(asdl.VisitorBase):
    def __init__(self, file):
        self.file = file
        super(EmitVisitor, self).__init__()

    def emit(self, s, depth=0):
        if not s or not depth:
            self.file.write("%s\n" % s)
        else:
            self.file.write("%s%s\n" % (" " * (4 * depth)), s)


##############################################################################
##
##  Visitors for generation of sky_python_ast.h
##

class TypeDeclarationVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_typedef(name, sum.attributes, ())
        if is_simple(sum):
            for type in sum.types:
                self.emit_typedef(type.name, (), (), singleton=name)
        else:
            for type in sum.types:
                self.visit(type, sum, sum.attributes)

    def visitConstructor(self, cons, type, attributes):
        self.emit_typedef(cons.name, attributes, cons.fields)

    def visitProduct(self, prod, name, depth):
        self.emit_typedef(name, (), prod.fields)

    def emit_typedef(self, name, attributes, fields, singleton=None):
        self.emit("typedef struct sky_python_ast_%s_s *sky_python_ast_%s_t;" % (name, name))
        self.emit("extern sky_type_t const sky_python_ast_%s_type;" % (name,))
        if singleton:
            self.emit("extern sky_python_ast_%s_t const sky_python_ast_%s;" % (singleton, name))
        self.emit("")


class PrototypeVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_function(name, (), rewrite_fields(sum.attributes))
        if is_simple(sum):
            for type in sum.types:
                self.emit_function(type.name, (), ())
        else:
            for type in sum.types:
                self.visit(type, name, sum.attributes)

    def visitProduct(self, prod, name, depth):
        self.emit_function(name, rewrite_fields(prod.fields), ())

    def visitConstructor(self, cons, type, attributes):
        fields = rewrite_fields(cons.fields)
        attributes = rewrite_fields(attributes)
        self.emit_function(cons.name, fields, attributes)

    def emit_function(self, name, fields, attributes):
        parameters = fields + attributes
        fname = "sky_python_ast_%s_create" % (name,)
        self.emit("struct sky_python_ast_%s_s {" % (name,))
        self.emit("    %-32s    object_data;" % ("sky_object_data_t",))
        for attribute in attributes:
            self.emit("    %-32s    %s;" % (attribute[1], attribute[0]))
        for field in fields:
            self.emit("    %-32s    %s;" % (field[1], field[0]))
        self.emit("    %-32s    __dict__;" % ('sky_dict_t',))
        self.emit("};")
        self.emit("")
        self.emit("extern sky_python_ast_%s_t" % (name,))
        if not parameters:
            self.emit("%s(void);" % (fname,))
        elif len(parameters) == 1:
            self.emit("%s(%s %s);" % (fname, parameters[0][1], parameters[0][0]))
        else:
            padding = max([len(p[1]) for p in parameters])
            self.emit("%s(%-*s %s," % (fname, padding, parameters[0][1], parameters[0][0]))
            for parameter in parameters[1:-1]:
                self.emit("%*s %-*s %s," % (len(fname), "", padding, parameter[1], parameter[0]))
            self.emit("%*s %-*s %s);" % (len(fname), "", padding, parameters[-1][1], parameters[-1][0]))
        self.emit("")


##############################################################################
##
##  Visitors for generation of sky_python_ast.c
##

class TypeDataStructVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        if not is_simple(sum):
            if sum.attributes:
                self.emit_data_struct(name, sum.attributes)
            for type in sum.types:
                self.visit(type, name)

    def visitConstructor(self, cons, type):
        self.emit_data_struct(cons.name, cons.fields)

    def visitProduct(self, prod, name, depth):
        if prod.fields:
            self.emit_data_struct(name, prod.fields)

    def emit_data_struct(self, name, fields):
        fields = rewrite_fields(fields)
        self.emit("typedef struct sky_python_ast_%s_data_s {" % (name,))
        for field in fields:
            self.emit("    %-36s%s;" % (field[1], field[0]))
        self.emit("} sky_python_ast_%s_data_t;" % (name,))
        self.emit("")
        if not is_int_only(fields):
            self.emit("static void")
            self.emit("sky_python_ast_%s_instance_visit(SKY_UNUSED sky_object_t self, void *data, sky_object_visit_data_t *visit_data)" % (name,))
            self.emit("{")
            self.emit("    sky_python_ast_%s_data_t *self_data = data;" % (name,))
            self.emit("")
            for field in fields:
                if field[1] != "int":
                    self.emit("    sky_object_visit(self_data->%s, visit_data);" % (field[0],))
            self.emit("}")
            self.emit("")


class TypeMethodVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_functions(name, (), rewrite_fields(sum.attributes))
        if is_simple(sum):
            for type in sum.types:
                self.emit_functions(type.name, (), ())
        else:
            for type in sum.types:
                self.visit(type, name, sum.attributes)

    def visitProduct(self, prod, name, depth):
        self.emit_functions(name, rewrite_fields(prod.fields), ())

    def visitConstructor(self, cons, type, attributes):
        fields = rewrite_fields(cons.fields)
        attributes = rewrite_fields(attributes)
        self.emit_functions(cons.name, fields, attributes)

    def emit_create_function(self, name, fields, attributes):
        parameters = fields + attributes
        fname = "sky_python_ast_%s_create" % (name,)
        self.emit("extern sky_python_ast_%s_t" % (name,))
        if not parameters:
            self.emit("%s(void)" % (fname,))
        elif len(parameters) == 1:
            self.emit("%s(%s %s)" % (fname, parameters[0][1], parameters[0][0]))
        else:
            padding = max([len(p[1]) for p in parameters])
            self.emit("%s(%-*s %s," % (fname, padding, parameters[0][1], parameters[0][0]))
            for parameter in parameters[1:-1]:
                self.emit("%*s %-*s %s," % (len(fname), "", padding, parameter[1], parameter[0]))
            self.emit("%*s %-*s %s)" % (len(fname), "", padding, parameters[-1][1], parameters[-1][0]))
        self.emit("{")
        if not parameters:
            self.emit("    return sky_object_allocate(sky_python_ast_%s_type);" % (name,))
        else:
            self.emit("    sky_python_ast_%s_t  self;" % (name,))
            self.emit("")
            self.emit("    self = sky_object_allocate(sky_python_ast_%s_type);" % (name,))
            self.emit_assignments(name, fields, attributes)
            self.emit("")
            self.emit("    return self;")
        self.emit("}")
        self.emit("")

    def emit_init_function(self, name, fields, attributes):
        def add_parameter(field, optional=False):
            self.emit("        sky_parameter_list_add(parameters,")
            self.emit("                               SKY_STRING_LITERAL(\"%s\")," % (field[0],))
            if field[1] == 'int':
                self.emit("                               SKY_DATA_TYPE_INT,")
                if optional:
                    self.emit("                               sky_integer_create(0));")
                else:
                    self.emit("                               NULL);")
            elif field[1] == 'sky_object_t':
                self.emit("                               SKY_DATA_TYPE_OBJECT,")
                if optional:
                    self.emit("                               sky_None);")
                else:
                    self.emit("                               NULL);")
            elif field[1] == 'sky_string_t':
                self.emit("                               SKY_DATA_TYPE_OBJECT_STRING,")
                if optional:
                    self.emit("                               sky_None);")
                else:
                    self.emit("                               NULL);")
            elif field[1] == 'sky_bytes_t':
                self.emit("                               SKY_DATA_TYPE_OBJECT_BYTES,")
                if optional:
                    self.emit("                               sky_None);")
                else:
                    self.emit("                               NULL);")
            else:
                self.emit("                               SKY_DATA_TYPE_OBJECT_INSTANCEOF,")
                if optional:
                    self.emit("                               sky_None,")
                else:
                    self.emit("                               NULL,")
                self.emit("                               %sype);" % (field[1],))

        self.emit("static sky_parameter_list_t sky_python_ast_%s_init_parameters = NULL;" % (name,))
        self.emit("")
        self.emit("static void")
        self.emit("sky_python_ast_%s_init(sky_python_ast_%s_t self, sky_tuple_t args, sky_dict_t kws)" % (name, name))
        self.emit("{")
        self.emit("    void **ffi_values;")
        self.emit("    struct sky_python_ast_%s_s values;" % (name,))
        self.emit("")
        self.emit("    if (!sky_python_ast_%s_init_parameters) {" % (name,))
        self.emit("        sky_parameter_list_t parameters = sky_parameter_list_create();")
        self.emit("")
        for field in fields:
            add_parameter(field)
        self.emit("        sky_parameter_list_add(parameters, SKY_STRING_LITERAL(\"*\"), SKY_DATA_TYPE_OBJECT_TUPLE, NULL);")
        for attribute in attributes:
            add_parameter(attribute, optional=True)
        self.emit("")
        self.emit("        sky_object_gc_setlibraryroot(")
        self.emit("                SKY_AS_OBJECTP(&sky_python_ast_%s_init_parameters)," % (name,))
        self.emit("                parameters,")
        self.emit("                SKY_TRUE);")
        self.emit("    }")
        self.emit("    ffi_values = sky_parameter_list_apply(sky_python_ast_%s_init_parameters," % (name,))
        self.emit("                                          args, kws,")
        self.emit("                                          SKY_STRING_LITERAL(\"ast.%s\"));" % (name,))
        idx = 0
        for field in fields:
            self.emit("    values.%s = *(%s *)ffi_values[%d];" % (field[0], field[1], idx))
            idx += 1
        for attribute in attributes:
            self.emit("    values.%s = *(%s *)ffi_values[%d];" % (attribute[0], attribute[1], idx))
            idx += 1
        self.emit("    sky_free(ffi_values);")
        self.emit("")
        self.emit_assignments(name, fields, attributes, prefix="values.")
        self.emit("}")
        self.emit("")

    def emit_functions(self, name, fields, attributes):
        self.emit_create_function(name, fields, attributes)
        if fields or attributes:
            self.emit_init_function(name, fields, attributes)

    def emit_assignment(self, name, field, prefix):
        if field[1] == 'int':
            self.emit("    self->%s = %s%s;" % (field[0], prefix, field[0]))
        else:
            if field[3]:
                self.emit("    if (!sky_object_isiterable(%s%s)) {" % (prefix, field[0]))
                self.emit("        sky_error_raise_format(sky_TypeError,")
                self.emit("                               \"expected sequence; got \\\'%@\\\'\",")
                self.emit("                               sky_type_name(sky_object_type(%s%s)));" % (prefix, field[0]))
                self.emit("    }")
            self.emit("    sky_object_gc_set(SKY_AS_OBJECTP(&(self->%s))," % (field[0],))
            self.emit("                      (%s%s ? (sky_object_t)%s%s" % (prefix, field[0], prefix, field[0]))
            self.emit("                       %*s : sky_None)," % (len(prefix) + len(field[0]), ""))
            self.emit("                      self);")

    def emit_assignments(self, name, fields, attributes, prefix=""):
        for attribute in attributes:
            self.emit_assignment(name, attribute, prefix)
        for field in fields:
            self.emit_assignment(name, field, prefix)


class TypeDefineVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_type_define(name, sum.attributes)
        if is_simple(sum):
            for type in sum.types:
                self.emit_type_define(type.name, (), singleton=name)
        else:
            for type in sum.types:
                self.visit(type, sum)

    def visitConstructor(self, cons, type):
        self.emit_type_define(cons.name, cons.fields)

    def visitProduct(self, prod, name, depth):
        self.emit_type_define(name, prod.fields)

    def emit_type_define(self, name, fields, singleton=None):
        self.emit("SKY_TYPE_DEFINE_SIMPLE(python_ast_%s," % (name,))
        self.emit("                       \"%s\"," % (name,))
        if not fields:
            self.emit("                       0,")
        else:
            self.emit("                       sizeof(sky_python_ast_%s_data_t)," % (name,))
        self.emit("                       NULL,")
        self.emit("                       NULL,")
        if is_int_only(fields):
            self.emit("                       NULL,")
        else:
            self.emit("                       sky_python_ast_%s_instance_visit," % (name,))
        self.emit("                       SKY_TYPE_FLAG_HAS_DICT,")
        self.emit("                       NULL);")
        self.emit("")
        if singleton:
            self.emit("static SKY_ALIGNED(16) struct sky_python_ast_%s_s sky_python_ast_%s_struct = {" % (name, name))
            self.emit("    SKY_OBJECT_DATA_INITIALIZER(&sky_python_ast_%s_type_struct)," % (name,))
            self.emit("    NULL,")
            self.emit("};")
            self.emit("sky_python_ast_%s_t const sky_python_ast_%s = (sky_python_ast_%s_t)&sky_python_ast_%s_struct;" % (singleton, name, singleton, name))


class TypeCompleteVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_type_complete(name, 'AST', attributes=sum.attributes)
        if is_simple(sum):
            init = False
            if sum.attributes:
                init = True
            for type in sum.types:
                self.emit_type_complete(type.name, name, init=init)
        else:
            for type in sum.types:
                self.visit(type, name, sum.attributes)

    def visitConstructor(self, cons, base, attributes):
        init = False
        if attributes:
            init = True
        self.emit_type_complete(cons.name, base, fields=cons.fields, init=init)

    def visitProduct(self, prod, name, depth):
        self.emit_type_complete(name, 'AST', fields=prod.fields)

    def emit_type_complete(self, name, base, fields=None, attributes=None, init=False):
        def emit_fields(fields, type):
            self.emit("    sky_type_setmembers(sky_python_ast_%s_type," % (name,))
            for field in fields:
                field_name = str(field.name)
                self.emit("        \"%s\", NULL, offsetof(sky_python_ast_%s_data_t, %s)," % (field_name, name, field_name))
                if field.type.value == 'int':
                    self.emit("        %*sSKY_DATA_TYPE_INT, 0," % (len(field_name) + 4, ""))
                else:
                    self.emit("        %*sSKY_DATA_TYPE_OBJECT," % (len(field_name) + 4, ""))
                    self.emit("        %*sSKY_MEMBER_FLAG_ALLOW_NONE | SKY_MEMBER_FLAG_NONE_IS_NULL," % (len(field_name) + 4, ""))
            self.emit("        NULL);")
            fields = ["SKY_STRING_LITERAL(\"%s\")" % (field.name,) for field in fields]
            self.emit("    sky_type_setattr_builtin(")
            self.emit("            sky_python_ast_%s_type," % (name,))
            self.emit("            \"%s\"," % (type,))
            self.emit("            sky_tuple_pack(%d, %s));" % (len(fields), ", ".join(fields)))

        self.emit("    sky_type_initialize_builtin(sky_python_ast_%s_type," % (name,))
        self.emit("                                1,")
        self.emit("                                sky_python_ast_%s_type);" % (base,))
        if attributes:
            emit_fields(attributes, '_attributes')
        if fields:
            emit_fields(fields, '_fields')

        if attributes or fields or init:
            self.emit("    sky_type_setmethodslots(sky_python_ast_%s_type," % (name,))
            self.emit("            \"__init__\", sky_python_ast_%s_init," % (name,))
            self.emit("            NULL);")
        self.emit("")


class TypeModuleVisitor(EmitVisitor):
    def visitModule(self, mod):
        for dfn in mod.dfns:
            self.visit(dfn)

    def visitType(self, type, depth=0):
        self.visit(type.value, type.name, depth)

    def visitSum(self, sum, name, depth):
        self.emit_type_define(name, sum.attributes)
        if is_simple(sum):
            for type in sum.types:
                self.emit_type_define(type.name, (), singleton=name)
        else:
            for type in sum.types:
                self.visit(type, sum)

    def visitConstructor(self, cons, type):
        self.emit_type_define(cons.name, cons.fields)

    def visitProduct(self, prod, name, depth):
        self.emit_type_define(name, prod.fields)

    def emit_type_define(self, name, fields, singleton=None):
        self.emit("    sky_module_setattr(module, \"%s\", sky_python_ast_%s_type);" % (name, name))


def generate_header(mod):
    h = open("sky_python_ast.h", "w")
    h.write("/* This file is automatically generated; do not edit. */\n")
    h.write("\n")
    h.write("#ifndef __SKYTHON_CORE_SKY_PYTHON_AST_H__\n")
    h.write("#define __SKYTHON_CORE_SKY_PYTHON_AST_H__ 1\n")
    h.write("\n")
    h.write("\n")
    h.write("#include \"sky_base.h\"\n")
    h.write("#include \"sky_object.h\"\n")
    h.write("\n")
    h.write("\n")
    h.write("SKY_CDECLS_BEGIN\n")
    h.write("\n")
    h.write("\n")
    h.write("typedef struct sky_python_ast_AST_s *sky_python_ast_AST_t;\n")
    h.write("extern sky_type_t const sky_python_ast_AST_type;\n")
    h.write("\n")
    h.write("\n")
    TypeDeclarationVisitor(h).visit(mod)
    PrototypeVisitor(h).visit(mod)
    h.write("\n")
    h.write("SKY_EXTERN sky_python_ast_expr_context_t\n")
    h.write("sky_python_ast_normalize_expr_context(sky_python_ast_expr_context_t ctx);\n")
    h.write("\n")
    h.write("SKY_EXTERN sky_string_t\n")
    h.write("sky_python_ast_dump(sky_object_t    node,\n")
    h.write("                    sky_bool_t      annotate_fields,\n")
    h.write("                    sky_bool_t      include_attributes,\n")
    h.write("                    sky_bool_t      pretty);")
    h.write("\n")
    h.write("SKY_EXTERN void\n")
    h.write("sky_python_ast_validate(sky_object_t node);\n")
    h.write("\n")
    h.write("extern void\n")
    h.write("sky_python_ast_initialize_module(sky_module_t module);\n")
    h.write("\n")
    h.write("\n")
    h.write("SKY_CDECLS_END\n")
    h.write("\n")
    h.write("\n")
    h.write("#endif  /* __SKYTHON_CORE_SKY_PYTHON_AST_H__ */\n")
    h.close()


def generate_source(mod):
    c = open("sky_python_ast.c", "w")
    c.write("/* This file is automatically generated; do not edit. */\n")
    c.write("\n")
    c.write("#include \"sky_private.h\"\n")
    c.write("#include \"sky_python_ast.h\"\n")
    c.write("\n")
    c.write("\n")

    c.write("sky_python_ast_expr_context_t\n")
    c.write("sky_python_ast_normalize_expr_context(sky_python_ast_expr_context_t ctx)\n")
    c.write("{\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_Load_type)) {\n")
    c.write("        return sky_python_ast_Load;\n")
    c.write("    }\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_Store_type)) {\n")
    c.write("        return sky_python_ast_Store;\n")
    c.write("    }\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_Del_type)) {\n")
    c.write("        return sky_python_ast_Del;\n")
    c.write("    }\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_AugLoad_type)) {\n")
    c.write("        return sky_python_ast_AugLoad;\n")
    c.write("    }\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_AugStore_type)) {\n")
    c.write("        return sky_python_ast_AugStore;\n")
    c.write("    }\n")
    c.write("    if (sky_object_isa(ctx, sky_python_ast_Param_type)) {\n")
    c.write("        return sky_python_ast_Param;\n")
    c.write("    }\n")
    c.write("    return ctx;\n")
    c.write("}\n")
    c.write("\n")
    c.write("\n")

    TypeDataStructVisitor(c).visit(mod)
    c.write("\n")

    TypeMethodVisitor(c).visit(mod)
    c.write("\n")

    c.write("SKY_TYPE_DEFINE_SIMPLE(python_ast_AST,\n")
    c.write("                       \"AST\",\n")
    c.write("                       0,\n")
    c.write("                       NULL,\n")
    c.write("                       NULL,\n")
    c.write("                       NULL,\n")
    c.write("                       SKY_TYPE_FLAG_HAS_DICT,")
    c.write("                       NULL);\n")
    c.write("\n")
    TypeDefineVisitor(c).visit(mod)
    c.write("\n")

    c.write("void\n")
    c.write("sky_python_ast_initialize_library(SKY_UNUSED unsigned int flags)\n")
    c.write("{\n")
    c.write("    sky_type_initialize_builtin(sky_python_ast_AST_type, 0);\n");
    c.write("    sky_type_setattr_builtin(sky_python_ast_AST_type,\n")
    c.write("                             \"_attributes\",\n")
    c.write("                             sky_tuple_empty);\n")
    c.write("    sky_type_setattr_builtin(sky_python_ast_AST_type,\n")
    c.write("                             \"_fields\",\n")
    c.write("                             sky_tuple_empty);\n")
    c.write("\n")
    TypeCompleteVisitor(c).visit(mod)
    c.write("}\n")
    c.write("\n")

    c.write("void\n")
    c.write("sky_python_ast_initialize_module(sky_module_t module)\n")
    c.write("{\n")
    c.write("    sky_module_setattr(module, \"AST\", sky_python_ast_AST_type);\n")
    TypeModuleVisitor(c).visit(mod)
    c.write("}\n")


def main(argc, argv):
    mod = asdl.parse(argv[1])
    if not asdl.check(mod):
        return 1

    generate_header(mod)
    generate_source(mod)
    return 0


if __name__ == "__main__":
    sys.exit(main(len(sys.argv), sys.argv))
