#!/usr/bin/env python

import os
import sys


input_path = 'unicode_data'


SKY_UNICODE_CTYPE_FLAG_ALPHA = 0x0001
SKY_UNICODE_CTYPE_FLAG_LOWER = 0x0002
SKY_UNICODE_CTYPE_FLAG_TITLE = 0x0004
SKY_UNICODE_CTYPE_FLAG_UPPER = 0x0008

SKY_UNICODE_CTYPE_FLAG_DECIMAL     = 0x0010
SKY_UNICODE_CTYPE_FLAG_DIGIT       = 0x0020
SKY_UNICODE_CTYPE_FLAG_INTEGRAL    = 0x0040
SKY_UNICODE_CTYPE_FLAG_FRACTIONAL  = 0x0080
SKY_UNICODE_CTYPE_FLAG_NUMERIC     = 0x00F0

SKY_UNICODE_CTYPE_FLAG_SPACE     = 0x0100
SKY_UNICODE_CTYPE_FLAG_PRINTABLE = 0x0200
SKY_UNICODE_CTYPE_FLAG_LINEBREAK = 0x0400

SKY_UNICODE_CTYPE_FLAG_XID_START    = 0x1000
SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE = 0x2000

SKY_UNICODE_CTYPE_FLAG_CASEFOLD_LOWER   = 0x10000
SKY_UNICODE_CTYPE_FLAG_CASEFOLD_TITLE   = 0x20000
SKY_UNICODE_CTYPE_FLAG_CASEFOLD_UPPER   = 0x40000
SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL = 0x80000

SKY_UNICODE_CTYPE_FLAG_MIRRORED = 0x100000

SKY_UNICODE_DECOMP_TYPE_CANONICAL   =  0
SKY_UNICODE_DECOMP_TYPE_FONT        =  1
SKY_UNICODE_DECOMP_TYPE_NOBREAK     =  2
SKY_UNICODE_DECOMP_TYPE_INITIAL     =  3
SKY_UNICODE_DECOMP_TYPE_MEDIAL      =  4
SKY_UNICODE_DECOMP_TYPE_FINAL       =  5
SKY_UNICODE_DECOMP_TYPE_ISOLATED    =  6
SKY_UNICODE_DECOMP_TYPE_CIRCLE      =  7
SKY_UNICODE_DECOMP_TYPE_SUPER       =  8
SKY_UNICODE_DECOMP_TYPE_SUB         =  9
SKY_UNICODE_DECOMP_TYPE_VERTICAL    = 10
SKY_UNICODE_DECOMP_TYPE_WIDE        = 11
SKY_UNICODE_DECOMP_TYPE_NARROW      = 12
SKY_UNICODE_DECOMP_TYPE_SMALL       = 13
SKY_UNICODE_DECOMP_TYPE_SQUARE      = 14
SKY_UNICODE_DECOMP_TYPE_FRACTION    = 15
SKY_UNICODE_DECOMP_TYPE_COMPAT      = 16
SKY_UNICODE_DECOMP_TYPE_COMPAT_MASK = 0x1F

SKY_UNICODE_DECOMP_TYPE_NFC_QC_MASK  = 0x0060
SKY_UNICODE_DECOMP_TYPE_NFD_QC_MASK  = 0x0180
SKY_UNICODE_DECOMP_TYPE_NFKC_QC_MASK = 0x0600
SKY_UNICODE_DECOMP_TYPE_NFKD_QC_MASK = 0x1800

SKY_UNICODE_DECOMP_TYPE_STARTER = 0x2000


decomp_type_map = {
    'font'      :   SKY_UNICODE_DECOMP_TYPE_FONT,
    'noBreak'   :   SKY_UNICODE_DECOMP_TYPE_NOBREAK,
    'initial'   :   SKY_UNICODE_DECOMP_TYPE_INITIAL,
    'medial'    :   SKY_UNICODE_DECOMP_TYPE_MEDIAL,
    'final'     :   SKY_UNICODE_DECOMP_TYPE_FINAL,
    'isolated'  :   SKY_UNICODE_DECOMP_TYPE_ISOLATED,
    'circle'    :   SKY_UNICODE_DECOMP_TYPE_CIRCLE,
    'super'     :   SKY_UNICODE_DECOMP_TYPE_SUPER,
    'sub'       :   SKY_UNICODE_DECOMP_TYPE_SUB,
    'vertical'  :   SKY_UNICODE_DECOMP_TYPE_VERTICAL,
    'wide'      :   SKY_UNICODE_DECOMP_TYPE_WIDE,
    'narrow'    :   SKY_UNICODE_DECOMP_TYPE_NARROW,
    'small'     :   SKY_UNICODE_DECOMP_TYPE_SMALL,
    'square'    :   SKY_UNICODE_DECOMP_TYPE_SQUARE,
    'fraction'  :   SKY_UNICODE_DECOMP_TYPE_FRACTION,
    'compat'    :   SKY_UNICODE_DECOMP_TYPE_COMPAT,
}

CATEGORY_NAMES = [
    'Cn', 'Lu', 'Ll', 'Lt', 'Mn', 'Mc', 'Me', 'Nd', 'Nl', 'No', 'Zs', 'Zl',
    'Zp', 'Cc', 'Cf', 'Cs', 'Co', 'Cn', 'Lm', 'Lo', 'Pc', 'Pd', 'Ps', 'Pe',
    'Pi', 'Pf', 'Po', 'Sm', 'Sc', 'Sk', 'So',
]

BIDIRECTIONAL_NAMES = [
    '', 'L', 'LRE', 'LRO', 'R', 'AL', 'RLE', 'RLO', 'PDF', 'EN', 'ES', 'ET',
    'AN', 'CS', 'NSM', 'BN', 'B', 'S', 'WS', 'ON',
]

EAST_ASIAN_WIDTH_NAMES = [
    'F', 'H', 'W', 'Na', 'A', 'N'
]


# Attributes are data read from various UCD sources, including UnicodeData.txt,
# CaseFolding.txt, SpecialCasing.txt, etc.
#
# There's one entry per codepoint, with each entry being a list with the
# following fields:
#        0  General_Category            (UnicodeData.txt field 2)
#        1  Canonical_Combining_Class   (UnicodeData.txt field 3)
#        2  Decomposition_Type          (UnicodeData.txt field 5)
#           Decomposition_Mapping
#        3  numeric value decimal       (UnicodeData.txt field 6)
#        4  numeric value digit         (UnicodeData.txt field 7)
#        5  numeric value numeric       (UnicodeData.txt field 8)
#        6  Simple_Uppercase_Mapping    (UnicodeData.txt field 12)
#        7  Simple_Lowercase_Mapping    (UnicodeData.txt field 13)
#        8  Simple_Titlecase_Mapping    (UnicodeData.txt field 14)
#        9  Normalization quick check   (DerivedNormalizationProps.txt)
#       10  case folding status         (CaseFolding.txt field 1)
#       11  case folding mapping        (CaseFolding.txt field 2)
#       12  special casing (lower)      (SpecialCasing.txt field 1)
#       13  special casing (title)      (SpecialCasing.txt field 2)
#       14  special casing (upper)      (SpecialCasing.txt field 3)
#       15  Bidi Class                  (UnicodeData.txt field 4)
#       16  East Asian Width            (EastAsianWidths.txt)
#       17  Mirrored                    (UnicodeData.txt field 9)

attribute_field_count = 18
attributes = [None] * 0x110000

composition_exclusions = set()
line_break_set = set()
other_id_start = set()
other_id_continue = set()


def split_index(index):
    best_size = sys.maxint
    index = tuple(index)
    for shift in xrange(0, 21):
        uniques = {}
        index_1 = []
        index_2 = []
        for i in xrange(0, len(index), 1 << shift):
            item = index[i:i + (1 << shift)]
            x = uniques.get(item)
            if x is None:
                uniques[item] = x = len(index_2)
                index_2.extend(item)
            index_1.append(x >> shift)
        size = (len(index_1) * 4) + (len(index_2) * 4)
        if size < best_size:
            best_size = size
            best_index_1 = index_1
            best_index_2 = index_2
            best_shift = shift
    return best_shift, best_index_1, best_index_2


def generate_ctype_header(version, table, index):
    if version is None:
        filename = "sky_unicode_ctype.h"
        suffix = ""
    else:
        filename = "sky_unicode_ctype-%s.h" % (version,)
        suffix = "_" + version.replace(".", "_")
    header = open(filename, "w")
    print >> header, "/* This file is automatically generated; do not edit. */"
    print >> header, ""
    print >> header, "#ifndef __SKYTHON_UNICODE_DATA_SKY_UNICODE_CTYPE%s_H__" % (suffix,)
    print >> header, "#define __SKYTHON_UNICODE_DATA_SKY_UNICODE_CTYPE%s_H__ 1" % (suffix,)
    print >> header, ""
    print >> header, "sky_unicode_ctype_t sky_unicode_ctypes%s[%d] = {" % (suffix, len(table))
    for item in table:
        print >> header, "    { 0x%06X, %6d, %6d, %6d," % (item[0], item[1], item[2], item[3]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[4][0], item[4][1], item[4][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[5][0], item[5][1], item[5][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[6][0], item[6][1], item[6][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[7][0], item[7][1], item[7][2]),
        print >> header, "{ { %d, %d } }, %2d, %2d, %d }," % (item[8][0], item[8][1], item[9], item[10], item[11])
    print >> header, "};"

    shift, index_1, index_2 = split_index(index)
    print >> header, ""
    print >> header, "#define sky_unicode_ctype_shift%s %d" % (suffix, shift)
    print >> header, ""
    print >> header, "int16_t sky_unicode_ctype_index%s_1[%d] = {" % (suffix, len(index_1))
    format = "%%%dd, " % (len(str(len(index_1))),)
    for cp in xrange(0, len(index_1), 16):
        line = "    " + format * 16 % (tuple(index_1[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""
    print >> header, "int16_t sky_unicode_ctype_index%s_2[%d] = {" % (suffix, len(index_2))
    for cp in xrange(0, len(index_2), 16):
        line = "    " + format * 16 % (tuple(index_2[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""

    print >> header, "sky_unicode_ctype_t sky_unicode_ctypes_latin1%s[256] = {" % (suffix,)
    for table_index in xrange(256):
        item = table[index[table_index]]
        print >> header, "    { 0x%06X, %6d, %6d, %6d," % (item[0], item[1], item[2], item[3]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[4][0], item[4][1], item[4][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[5][0], item[5][1], item[5][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[6][0], item[6][1], item[6][2]),
        print >> header, "{ 0x%06X, 0x%06X, 0x%06X }," % (item[7][0], item[7][1], item[7][2]),
        print >> header, "{ { %d, %d } }, %2d, %2d, %d }," % (item[8][0], item[8][1], item[9], item[10], item[11])
    print >> header, "};"

    print >> header, ""

    print >> header, "#endif  /* __SKYTHON_UNICODE_DATA_SKY_UNICODE_CTYPE%s_H__ */" % (version,)
    header.close()


def generate_decomp_header(version, data, table, index, maxlen, compose_table, compose_index):
    if version is None:
        filename = "sky_unicode_decomp.h"
        suffix = ""
    else:
        filename = "sky_unicode_decomp-%s.h" % (version,)
        suffix = "_" + version.replace(".", "_")
    header = open(filename, "w")
    print >> header, "/* This file is automatically generated; do not edit. */"
    print >> header, ""
    print >> header, "#ifndef __SKYTHON_UNICODE_DATA_SKY_UNICODE_DECOMP%s_H__" % (suffix,)
    print >> header, "#define __SKYTHON_UNICODE_DATA_SKY_UNICODE_DECOMP%s_H__ 1" % (suffix,)
    print >> header, ""
    print >> header, "#define SKY_UNICODE_DECOMP_MAXLEN%s %d" % (suffix, maxlen)
    print >> header, ""
    print >> header, "sky_unicode_char_t sky_unicode_decomp_data%s[%d] = {" % (suffix, len(data))
    for i in xrange(len(data)):
        if not (i % 7):
            header.write("    ")
        header.write("0x%06X," % (data[i],))
        if (i % 7) == 6:
            header.write("\n")
        else:
            header.write(" ")
    if len(data) % 7:
        header.write("\n")
    print >> header, "};"
    print >> header, ""
    print >> header, "sky_unicode_decomp_t sky_unicode_decomp_table%s[%d] = {" % (suffix, len(table))
    for i in xrange(len(table)):
        if not (i % 2):
            header.write("    ")
        item = table[i]
        header.write("{ 0x%04X, %2d, %3d, 0x%08X }," % (item[0], item[1], item[2], item[3]))
        if (i % 2) == 1:
            header.write("\n")
        else:
            header.write(" ")
    if len(table) % 2:
        header.write("\n")
    print >> header, "};"

    shift, index_1, index_2 = split_index(index)
    print >> header, ""
    print >> header, "#define sky_unicode_decomp_shift%s %d" % (suffix, shift)
    print >> header, ""
    print >> header, "int16_t sky_unicode_decomp_index%s_1[%d] = {" % (suffix, len(index_1))
    format = "%%%dd, " % (len(str(len(index_1))),)
    for cp in xrange(0, len(index_1), 16):
        line = "    " + format * 16 % (tuple(index_1[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""
    print >> header, "int16_t sky_unicode_decomp_index%s_2[%d] = {" % (suffix, len(index_2))
    for cp in xrange(0, len(index_2), 16):
        line = "    " + format * 16 % (tuple(index_2[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""

    print >> header, "int64_t sky_unicode_compose_table%s[%d] = {" % (suffix, len(compose_table))
    for i in xrange(len(compose_table)):
        if not (i % 2):
            header.write("    ")
        header.write("INT64_C(0x%016X)," % (compose_table[i],))
        if (i % 2) == 1:
            header.write("\n")
        else:
            header.write(" ")
    if len(data) % 2:
        header.write("\n")
    print >> header, "};"

    shift, index_1, index_2 = split_index(compose_index)
    print >> header, ""
    print >> header, "#define sky_unicode_compose_shift%s %d" % (suffix, shift)
    print >> header, ""
    print >> header, "int16_t sky_unicode_compose_index%s_1[%d] = {" % (suffix, len(index_1))
    format = "%%%dd, " % (len(str(len(index_1))),)
    for cp in xrange(0, len(index_1), 16):
        line = "    " + format * 16 % (tuple(index_1[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""
    print >> header, "int16_t sky_unicode_compose_index%s_2[%d] = {" % (suffix, len(index_2))
    for cp in xrange(0, len(index_2), 16):
        line = "    " + format * 16 % (tuple(index_2[cp:cp + 16]))
        print >> header, line[:-1]
    print >> header, "};"
    print >> header, ""
    print >> header, "#endif  /* __SKYTHON_UNICODE_DATA_SKY_UNICODE_DECOMP%s_H__ */" % (suffix,)
    header.close()


def process_attributes(version):
    ctype_dummy = (0, 0, 0, 0, (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0), 0, 0, 0)
    ctype_cache = { ctype_dummy : 0 }
    ctype_table = [ ctype_dummy ]
    ctype_index = [ 0 ] * len(attributes)

    decomp_data   = []
    decomp_dummy  = (0, 0, 0, 0)
    decomp_cache  = { decomp_dummy : 0 }
    decomp_table  = [ decomp_dummy ]
    decomp_index  = [ 0 ] * len(attributes)
    decomp_maxlen = 0

    compose_pairs = []
    compose_table = [ 0 ]
    compose_index = [ 0 ] * len(attributes)

    for cp in xrange(len(attributes)):
        fields = attributes[cp]
        if fields is None:
            continue
        flags = 0
        numeric_value = (0, 0)
        casefold = special_lower = special_title = special_upper = (0, 0, 0)

        # simple_upper, simple_lower, and simple_title are deltas.  if there
        # is no title, use upper.
        if fields[6]:   simple_upper = int(fields[6], 16) - cp
        else:           simple_upper = 0
        if fields[7]:   simple_lower = int(fields[7], 16) - cp
        else:           simple_lower = 0
        if fields[8]:   simple_title = int(fields[8], 16) - cp
        else:           simple_title = simple_upper

        if fields[0] in ["Lm", "Lt", "Lu", "Ll", "Lo"]:
            flags |= (SKY_UNICODE_CTYPE_FLAG_ALPHA |
                      SKY_UNICODE_CTYPE_FLAG_XID_START |
                      SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE)
            if fields[0] == "Ll":
                flags |= SKY_UNICODE_CTYPE_FLAG_LOWER
            elif fields[0] == "Lt":
                flags |= SKY_UNICODE_CTYPE_FLAG_TITLE
            elif fields[0] == "Lu":
                flags |= SKY_UNICODE_CTYPE_FLAG_UPPER

            if fields[10] == "C":
                delta = int(fields[11], 16) - cp
                if delta == simple_upper:
                    flags |= SKY_UNICODE_CTYPE_FLAG_CASEFOLD_UPPER
                elif delta == simple_lower:
                    flags |= SKY_UNICODE_CTYPE_FLAG_CASEFOLD_LOWER
                elif delta == simple_title:
                    flags |= SKY_UNICODE_CTYPE_FLAG_CASEFOLD_TITLE
                else:
                    flags = SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL
                    casefold = (int(fields[11], 16), 0, 0)
            elif fields[10] == "F":
                flags = SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL
                casefold = tuple((int(f, 16) for f in fields[11].split()))
                casefold = casefold + (0,) * (3 - len(casefold))

            if fields[12]:
                special_lower = tuple((int(f, 16) for f in fields[12].split()))
                special_lower = special_lower + (0,) * (3 - len(special_lower))
                if not special_lower[1] and special_lower[0] - cp == simple_lower:
                    special_lower = (0, 0, 0)
            if fields[13]:
                special_title = tuple((int(f, 16) for f in fields[13].split()))
                special_title = special_title + (0,) * (3 - len(special_title))
                if not special_title[1] and special_title[0] - cp == simple_title:
                    special_title = (0, 0, 0)
            if fields[14]:
                special_upper = tuple((int(f, 16) for f in fields[14].split()))
                special_upper = special_upper + (0,) * (3 - len(special_upper))
                if not special_upper[1] and special_upper[0] - cp == simple_upper:
                    special_upper = (0, 0, 0)

        elif fields[0] == "Zs":
            flags |= SKY_UNICODE_CTYPE_FLAG_SPACE
        elif fields[0] == "Nd":
            flags |= (SKY_UNICODE_CTYPE_FLAG_XID_START |
                      SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE)
        elif fields[0] == "Pc":
            flags |= SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE
        if fields[0][0] not in ["C", "Z"] or cp == ord(' '):
            flags |= SKY_UNICODE_CTYPE_FLAG_PRINTABLE

        if fields[15] in ('WS', 'B', 'S'):
            flags |= SKY_UNICODE_CTYPE_FLAG_SPACE
        if cp in line_break_set or fields[15] == 'B':
            flags |= SKY_UNICODE_CTYPE_FLAG_LINEBREAK

        if fields[3]:
            flags |= SKY_UNICODE_CTYPE_FLAG_DECIMAL
            numeric_value = (int(fields[3]), 0)
        if fields[4]:
            flags |= SKY_UNICODE_CTYPE_FLAG_DIGIT
            numeric_value = (int(fields[4]), 0)
        if fields[5]:
            slash = fields[5].find("/")
            if slash == -1:
                flags |= SKY_UNICODE_CTYPE_FLAG_INTEGRAL
                numeric_value = (int(fields[5]), 0)
            else:
                flags |= SKY_UNICODE_CTYPE_FLAG_FRACTIONAL
                numeric_value = (int(fields[5][:slash]),
                                 int(fields[5][slash + 1:]))

        if cp == ord("_"):
            flags |= (SKY_UNICODE_CTYPE_FLAG_XID_START |
                      SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE)
        if cp in other_id_start:
            flags |= SKY_UNICODE_CTYPE_FLAG_XID_START
        if cp in other_id_continue:
            flags |= SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE

        if fields[17] == 'Y':
            flags |= SKY_UNICODE_CTYPE_FLAG_MIRRORED

        category = CATEGORY_NAMES.index(fields[0])
        bidirectional = BIDIRECTIONAL_NAMES.index(fields[15])
        east_asian_width = EAST_ASIAN_WIDTH_NAMES.index(fields[16])

        ctype = (flags,
                 simple_lower, simple_title, simple_upper,
                 special_lower, special_title, special_upper,
                 casefold,
                 numeric_value,
                 category, bidirectional, east_asian_width)
        i = ctype_cache.get(ctype)
        if i is None:
            ctype_cache[ctype] = i = len(ctype_table)
            ctype_table.append(ctype)
        ctype_index[cp] = i

        ccc = int(fields[1])
        assert ccc <= 0xFF
        type = SKY_UNICODE_DECOMP_TYPE_CANONICAL
        length = 0
        offset = 0
        if fields[2]:
            cps = fields[2].split()
            if cps[0][0] == '<':
                type = decomp_type_map[cps[0][1:-1]]
                cps.pop(0)
            cps = [int(x, 16) for x in cps]
            offset = len(decomp_data)
            assert offset <= 0xFFFF
            length = len(cps)
            assert length <= 0xFF
            decomp_maxlen = max(decomp_maxlen, length)
            decomp_data.extend(cps)
        decomp = (offset, length, ccc, (int(fields[9]) << 5) | type)
        i = decomp_cache.get(decomp)
        if i is None:
            decomp_cache[decomp] = i = len(decomp_table)
            decomp_table.append(decomp)
        decomp_index[cp] = i
        if not (type & SKY_UNICODE_DECOMP_TYPE_COMPAT_MASK) and length == 2:
            if not int(attributes[cps[0]][1]):
                if cp not in composition_exclusions:
                    compose_pairs.append((cps[0], cps[1], cp))

    for pair in sorted(compose_pairs):
        item = (pair[2] << 42) | (pair[1] << 21) | pair[0]
        if not compose_index[pair[0]]:
            compose_index[pair[0]] = len(compose_table)
        compose_table.append(item)
    compose_table.append(0)

    generate_ctype_header(version, ctype_table, ctype_index)
    generate_decomp_header(version, decomp_data, decomp_table, decomp_index,
                           decomp_maxlen, compose_table, compose_index)


def source(filename, version):
    if version is not None:
        name, ext = os.path.splitext(filename)
        filename = "%s-%s%s" % (name, version, ext)
    return open(os.path.join(input_path, filename), "r")


def parse_unicode_data(version):
    count = 0
    range_first = None
    for line in source("UnicodeData.txt", version):
        fields = [f.strip() for f in line.strip().split(";")]
        cp = int(fields[0], 16)
        record = [None] * attribute_field_count
        record[0:10] = fields[2:4] + fields[5:9] + fields[12:15] + [0]
        record[15] = fields[4]
        record[17] = fields[9]
        if fields[1][0] == "<" and fields[1][-6:] == "First>":
            assert range_first is None
            range_first = cp
        elif fields[1][0] == "<" and fields[1][-5:] == "Last>":
            assert range_first is not None
            range_last = cp
            for cp in xrange(range_first, range_last + 1):
                attributes[cp] = record
                count += 1
            range_first = range_last = None
        else:
            attributes[cp] = record
            count += 1


def parse_old_normalization_props(version):
    prefixes = ["NFC", "NFD", "NFKC", "NFKD"]
    suffixes = ["MAYBE", "NO"]
    for line in source("DerivedNormalizationProps.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            tags = fields[1].split('_')
            if len(tags) != 2 or tags[0] not in prefixes:
                continue
            dotdot = fields[0].find("..")
            if dotdot != -1:
                first_cp = int(fields[0][:dotdot], 16)
                last_cp  = int(fields[0][dotdot + 2:], 16)
            else:
                first_cp = int(fields[0], 16)
                last_cp  = first_cp
            for cp in xrange(first_cp, last_cp + 1):
                value   = suffixes.index(tags[1]) + 1
                value <<= prefixes.index(tags[0]) * 2
                attributes[cp][9] |= value


def parse_normalization_props(version):
    # Starting with 4.0-Update1, the format of this file changed. There's no
    # reliable way to check what we've got, so parse the new format, but if
    # no data comes from it, parse the old format. Also, if an old format tag
    # is recognized, skip right to parsing the old format.

    count = 0
    qc_order = ["NFC_QC", "NFD_QC", "NFKC_QC", "NFKD_QC"]
    for line in source("DerivedNormalizationProps.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            if fields[1] not in qc_order:
                if fields[1] == 'FNC' or fields[1].endswith(('_MAYBE', '_NO')):
                    break
                continue
            dotdot = fields[0].find("..")
            if dotdot != -1:
                first_cp = int(fields[0][:dotdot], 16)
                last_cp  = int(fields[0][dotdot + 2:], 16)
            else:
                first_cp = int(fields[0], 16)
                last_cp  = first_cp
            count += (last_cp - first_cp)
            for cp in xrange(first_cp, last_cp + 1):
                value   = 'MN'.index(fields[2]) + 1
                value <<= qc_order.index(fields[1]) * 2
                attributes[cp][9] |= value
    if not count:
        parse_old_normalization_props(version)


def parse_case_folding(version):
    for line in source("CaseFolding.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            if fields[1].strip() in ["C", "F"]:
                dotdot = fields[0].find("..")
                if dotdot != -1:
                    first_cp = int(fields[0][:dotdot], 16)
                    last_cp  = int(fields[0][dotdot + 2:], 16)
                else:
                    first_cp = int(fields[0], 16)
                    last_cp  = first_cp
                for cp in xrange(first_cp, last_cp + 1):
                    attributes[cp][10:12] = fields[1:3]


def parse_special_casing(version):
    for line in source("SpecialCasing.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            if len(fields) <= 4 or (len(fields) == 5 and not fields[4]):
                dotdot = fields[0].find("..")
                if dotdot != -1:
                    first_cp = int(fields[0][:dotdot], 16)
                    last_cp  = int(fields[0][dotdot + 2:], 16)
                else:
                    first_cp = int(fields[0], 16)
                    last_cp  = first_cp
                    for cp in xrange(first_cp, last_cp + 1):
                        attributes[cp][12:15] = fields[1:4]


def parse_composition_exclusions(version):
    for line in source("CompositionExclusions.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            dotdot = line.find("..")
            if dotdot == -1:
                composition_exclusions.add(int(line, 16))
            else:
                first_cp = int(line[:dotdot], 16)
                last_cp  = int(line[dotdot + 2:], 16)
                composition_exclusions.update(xrange(first_cp, last_cp + 1))


def parse_prop_list(version):
    for line in source("PropList.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            if fields[1] == "Other_ID_Start":
                prop_set = other_id_start
            elif fields[1] == "Other_ID_Continue":
                prop_set = other_id_continue
            else:
                continue
            dotdot = fields[0].find("..")
            if dotdot == -1:
                prop_set.add(int(fields[0], 16))
            else:
                first_cp = int(fields[0][:dotdot], 16)
                last_cp  = int(fields[0][dotdot + 2:], 16)
                prop_set.update(xrange(first_cp, last_cp + 1))


def parse_line_breaks(version):
    mandatory_line_breaks = frozenset(["BK", "CR", "LF", "NL"])
    for line in source("LineBreak.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            if fields[1] not in mandatory_line_breaks:
                continue

            dotdot = fields[0].find("..")
            if dotdot == -1:
                line_break_set.add(int(fields[0], 16))
            else:
                first_cp = int(fields[0][:dotdot], 16)
                last_cp  = int(fields[0][dotdot + 2:], 16)
                line_break_set.update(xrange(first_cp, last_cp + 1))


def parse_east_asian_widths(version):
    for line in source("EastAsianWidth.txt", version):
        line = line[:line.find("#")].strip()
        if line:
            fields = [f.strip() for f in line.split(";")]
            dotdot = fields[0].find("..")
            if dotdot == -1:
                attributes[int(fields[0], 16)][16] = fields[1]
            else:
                first_cp = int(fields[0][:dotdot], 16)
                last_cp  = int(fields[0][dotdot + 2:], 16)
                for cp in xrange(first_cp, last_cp + 1):
                    if attributes[cp] is not None:
                        attributes[cp][16] = fields[1]


def main(argc, argv):
    try:
        import argparse
    except ImportError:
        import optparse

        parser = optparse.OptionParser()
        parser.add_option('-r', '--root', action='store')
        parser.add_option('-v', '--version', action='store')
        opts, args = parser.parse_args(argv[1:])
    else:
        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--root', action='store')
        parser.add_argument('-v', '--version', action='store')
        opts = parser.parse_args(argv[1:])

    if opts.root:
        global input_path
        input_path = os.path.join(opts.root, input_path)
    else:
        source_path = os.path.dirname(argv[0])
        if source_path:
            input_path = os.path.join(source_path, input_path)

    parse_unicode_data(opts.version)
    parse_normalization_props(opts.version)
    parse_case_folding(opts.version)
    parse_special_casing(opts.version)
    parse_composition_exclusions(opts.version)
    parse_prop_list(opts.version)
    parse_line_breaks(opts.version)
    parse_east_asian_widths(opts.version)

    process_attributes(opts.version)
    return 0


if __name__ == "__main__":
    sys.exit(main(len(sys.argv), sys.argv))
