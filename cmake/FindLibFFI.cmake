# The libffi.pc that gets installed by libffi is broken and has been for a
# long, long time. Do not even try to use it. Unfortunately, libffi also uses
# a completely bizarre non-standard installation layout, so we have to deal
# with that, which means that some of the built-in CMake functions aren't as
# helpful as usual.

IF(LIBFFI_ROOT)
    # Look in LIBFFI_ROOT for:
    #       include/ffi.h
    #       include/ffi/ffi.h
    #       lib/libffi-*/include/ffi.h
    #       lib/libffi-*/include/ffi/ffi.h

    FILE(GLOB LIBFFI_ROOT_LIBFFI RELATIVE "${LIBFFI_ROOT}" "${LIBFFI_ROOT}/lib/libffi-*")
    FIND_PATH(LIBFFI_INCLUDE_DIR
              PATHS                 "${LIBFFI_ROOT}" "${LIBFFI_ROOT_LIBFFI}"
              NAMES                 ffi.h
              PATH_SUFFIXES         include include/ffi
                                    "${LIBFFI_ROOT_LIBFFI}/include" "${LIBFFI_ROOT_LIBFFI}/include/ffi"
              NO_DEFAULT_PATH)

    # Look in LIBFFI_ROOT for:
    #       lib64
    #       lib
    FIND_LIBRARY(LIBFFI_LIBRARY
                 NAMES              ffi
                 PATHS              "${LIBFFI_ROOT}"
                 PATH_SUFFIXES      lib64 lib
                 NO_DEFAULT_PATH)
ELSE()
    FIND_PATH(LIBFFI_INCLUDE_DIR
              NAMES                 ffi.h
              PATH_SUFFIXES         include include/ffi)
    FIND_LIBRARY(LIBFFI_LIBRARY
                 NAMES              ffi
                 PATH_SUFFIXES      lib64 lib)
ENDIF()
              
MARK_AS_ADVANCED(LIBFFI_LIBRARY LIBFFI_INCLUDE_DIR)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBFFI REQUIRED_VARS LIBFFI_INCLUDE_DIR
                                                       LIBFFI_LIBRARY)

IF(LIBFFI_FOUND)
    SET(LIBFFI_INCLUDE_DIRS ${LIBFFI_INCLUDE_DIR})
    SET(LIBFFI_LIBRARIES ${LIBFFI_LIBRARY})
ENDIF()
