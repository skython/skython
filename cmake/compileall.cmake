SET(SKYTHON_LIB_PYTHON
    "${CMAKE_INSTALL_PREFIX}/lib/python${SKYTHON_PYTHON_VERSION_MAJOR}.${SKYTHON_PYTHON_VERSION_MINOR}")

SET(ENV{PYTHONPATH} "${SKYTHON_LIB_PYTHON}")
IF(APPLE)
    SET(ENV{DYLD_LIBRARY_PATH} "${CMAKE_INSTALL_PREFIX}/lib:$ENV{DYLD_LIBRARY_PATH}")
ELSE()
    SET(ENV{LD_LIBRARY_PATH} "${CMAKE_INSTALL_PREFIX}/lib:$ENV{LD_LIBRARY_PATH}")
ENDIF()

EXECUTE_PROCESS(
    COMMAND             bin/skython -Wi ${SKYTHON_LIB_PYTHON}/compileall.py
                        -d ${SKYTHON_LIB_PYTHON} -f
                        -x 'bad_coding|badsyntax|site-packages|lib2to3/tests/data'
                        ${SKYTHON_LIB_PYTHON}
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
EXECUTE_PROCESS(
    COMMAND             bin/skython -Wi -O ${SKYTHON_LIB_PYTHON}/compileall.py
                        -d ${SKYTHON_LIB_PYTHON} -f
                        -x 'bad_coding|badsyntax|site-packages|lib2to3/tests/data'
                        ${SKYTHON_LIB_PYTHON}
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
EXECUTE_PROCESS(
    COMMAND             bin/skython -Wi ${SKYTHON_LIB_PYTHON}/compileall.py
                        -d ${SKYTHON_LIB_PYTHON}/site-packages -f
                        -x badsyntax
                        ${SKYTHON_LIB_PYTHON}/site-packages
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
EXECUTE_PROCESS(
    COMMAND             bin/skython -Wi -O ${SKYTHON_LIB_PYTHON}/compileall.py
                        -d ${SKYTHON_LIB_PYTHON}/site-packages -f
                        -x badsyntax
                        ${SKYTHON_LIB_PYTHON}/site-packages
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
EXECUTE_PROCESS(
    COMMAND             bin/skython -m lib2to3.pgen2.driver
                        ${SKYTHON_LIB_PYTHON}/lib2to3/Grammar.txt
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
EXECUTE_PROCESS(
    COMMAND             bin/skython -m lib2to3.pgen2.driver
                        ${SKYTHON_LIB_PYTHON}/lib2to3/PatternGrammar.txt
    WORKING_DIRECTORY   ${CMAKE_INSTALL_PREFIX}
    )
