#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    sky_test(sky_False != NULL);
    sky_test(sky_object_isa(sky_False, sky_boolean_type));
    sky_test(0 == sky_object_hash(sky_False));

    sky_test(sky_True != NULL);
    sky_test(sky_object_isa(sky_True, sky_boolean_type));
    sky_test(0 != sky_object_hash(sky_True));

    sky_library_finalize();
    return 0;
}
