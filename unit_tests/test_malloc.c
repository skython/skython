#include "sky_unit_test.h"


#define MAX_ALLOCATION_COUNT    100000


/* It's fairly difficult to do much testing with malloc, because any failure
 * will cause an abort, and any catastrophic bugs will cause a crash. There
 * really isn't a good way to validate most returns, because the operating
 * system doesn't guarantee that we're going to get the same address every
 * time we ask it for memory, nor should it. The best that can really be
 * done, then, is to stress it as much as is reasonable, and let the testing
 * framework choke awkwardly if something crashes.
 *
 * I think that pretty much this is just going to have to be built out over
 * time. If bugs are found in some way, then add a test case to this when
 * the bug is discovered and fixed.
 */


int
main(int argc, char *argv[])
{
    static char zero_buffer[64] = {0,};

    void    **allocations, *huge_zero_buffer, *pointer;
    size_t  allocation_count, i;

    /* Start simple. Allocate something small and try to free it right away. */
    pointer = sky_malloc(64);
    sky_free(pointer);

    /* Try a calloc allocation to make sure that it's returning memory filled
     * with zero bytes.
     */
    pointer = sky_calloc(1, sizeof(zero_buffer));
    sky_test(0 == memcmp(pointer, zero_buffer, sizeof(zero_buffer)));
    sky_free(pointer);

    /* Try a huge allocation to see what happens. */
    pointer = sky_malloc(1024 * 1024);
    sky_free(pointer);

    /* Try a huge calloc. */
    huge_zero_buffer = calloc(1024, 1024);
    pointer = sky_calloc(1024, 1024);
    sky_test(0 == memcmp(pointer, huge_zero_buffer, 1024 * 1024));
    sky_free(pointer);
    free(huge_zero_buffer);

    /* Do a whole lot of randomly sized allocations, and then free them in
     * random order.
     */
    allocation_count = MAX_ALLOCATION_COUNT;
    allocations = sky_calloc(allocation_count, sizeof(void *));
    for (i = 0;  i < MAX_ALLOCATION_COUNT;  ++i) {
        allocations[i] = sky_malloc(16 * ((sky_util_random32() % 2047) + 1));
    }
    while (allocation_count > 0) {
        i = sky_util_random32() % allocation_count;
        pointer = allocations[i];
        if (allocation_count > 1 && i != allocation_count - 1) {
            allocations[i] = allocations[allocation_count - 1];
        }
        --allocation_count;
        sky_free(pointer);
    }
    sky_free(allocations);

    return 0;
}
