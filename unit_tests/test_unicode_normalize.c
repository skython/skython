#include "sky_unit_test.h"


typedef struct test_column_s {
    size_t                              length;
    union {
        sky_string_t                    string;
        sky_unicode_char_t *            content;
    } u;
} test_column_t;

typedef struct test_case_s {
    test_column_t                       column[5];
} test_case_t;


static test_case_t *
parse_test_case(const char *line, size_t line_length)
{
    int                 column = 0;
    size_t              count;
    const char          *c, *end;
    test_case_t         *test_case;
    sky_unicode_char_t  *out, value;

    count = 0;
    value = 0;
    end = line + line_length;
    for (c = line;  c < end;  ++c) {
        if (*c == ' ' || *c == ';') {
            ++count;
        }
    }

    test_case = sky_calloc(1, sizeof(test_case_t) +
                              (sizeof(sky_unicode_char_t) * count));
    test_case->column[0].u.content = (sky_unicode_char_t *)(test_case + 1);
    out = test_case->column[0].u.content;

    for (c = line;  c < end;  ++c) {
        if (*c == ' ') {
            *out++ = value;
            value = 0;
        }
        else if (*c == ';') {
            *out++ = value;
            value = 0;
            test_case->column[column].length =
                    out - test_case->column[column].u.content;
            if (++column ==
                    sizeof(test_case->column) / sizeof(test_case->column[0]))
            {
                break;
            }
            test_case->column[column].u.content = out;
        }
        else {
            sky_test(sky_ctype_isxdigit(*c));
            value = (value << 4) |
                    (sky_unicode_char_t)(strchr(sky_ctype_upper_hexdigits, *c) -
                                          sky_ctype_upper_hexdigits);
        }
    }
    if (c >= end) {
        ++column;
    }

    while (--column >= 0) {
        test_case->column[column].u.string =
                sky_string_createwithcodepoints(
                        test_case->column[column].u.content,
                        test_case->column[column].length * sizeof(sky_unicode_char_t),
                        sizeof(sky_unicode_char_t),
                        NULL);
    }

    return test_case;
}


#define test_compare_columns(a, b)                                  \
    do {                                                            \
        sky_test(test_case->column[a].length == c[b].length);       \
        sky_test(sky_object_compare(test_case->column[a].u.string,  \
                                    c[b].u.string,                  \
                                    SKY_COMPARE_OP_EQUAL));         \
    } while (0)


static void
test_nfc(test_case_t *test_case)
{
    int             i;
    test_column_t   c[5];

    for (i = 0;  i < 5;  ++i) {
        c[i].u.string = sky_string_normalize(test_case->column[i].u.string,
                                             SKY_STRING_NORMALIZE_FORM_NFC);
        c[i].length = sky_object_len(c[i].u.string);
    }

    test_compare_columns(1, 0);
    test_compare_columns(1, 1);
    test_compare_columns(1, 2);

    test_compare_columns(3, 3);
    test_compare_columns(3, 4);
}


static void
test_nfd(test_case_t *test_case)
{
    int             i;
    test_column_t   c[5];

    for (i = 0;  i < 5;  ++i) {
        c[i].u.string = sky_string_normalize(test_case->column[i].u.string,
                                             SKY_STRING_NORMALIZE_FORM_NFD);
        c[i].length = sky_object_len(c[i].u.string);
    }

    test_compare_columns(2, 0);
    test_compare_columns(2, 1);
    test_compare_columns(2, 2);

    test_compare_columns(4, 3);
    test_compare_columns(4, 4);
}


static void
test_nfkc(test_case_t *test_case)
{
    int             i;
    test_column_t   c[5];

    for (i = 0;  i < 5;  ++i) {
        c[i].u.string = sky_string_normalize(test_case->column[i].u.string,
                                             SKY_STRING_NORMALIZE_FORM_NFKC);
        c[i].length = sky_object_len(c[i].u.string);
    }

    test_compare_columns(3, 0);
    test_compare_columns(3, 1);
    test_compare_columns(3, 2);
    test_compare_columns(3, 3);
    test_compare_columns(3, 4);
}


static void
test_nfkd(test_case_t *test_case)
{
    int             i;
    test_column_t   c[5];

    for (i = 0;  i < 5;  ++i) {
        c[i].u.string = sky_string_normalize(test_case->column[i].u.string,
                                             SKY_STRING_NORMALIZE_FORM_NFKD);
        c[i].length = sky_object_len(c[i].u.string);
    }

    test_compare_columns(4, 0);
    test_compare_columns(4, 1);
    test_compare_columns(4, 2);
    test_compare_columns(4, 3);
    test_compare_columns(4, 4);
}


static void
test_part1_simple(sky_unicode_char_t *cps, size_t ncps)
{
    sky_string_t    nfc, nfd, nfkc, nfkd, string;

    string = sky_string_createwithcodepoints(cps,
                                             ncps * sizeof(cps[0]),
                                             sizeof(cps[0]),
                                             NULL);

    /* The data should already be in form NFD. */
    nfd = sky_string_normalize(string, SKY_STRING_NORMALIZE_FORM_NFD);
    sky_test(nfd == string);

    nfc = sky_string_normalize(string, SKY_STRING_NORMALIZE_FORM_NFC);
    if (nfc != string) {
        sky_test(sky_object_len(nfc) == sky_object_len(string));
        sky_test(sky_object_compare(nfc, string, SKY_COMPARE_OP_EQUAL));
    }

    /* The data should already be in form NFKD. */
    nfkd = sky_string_normalize(string, SKY_STRING_NORMALIZE_FORM_NFKD);
    sky_test(nfkd == string);

    nfkc = sky_string_normalize(string, SKY_STRING_NORMALIZE_FORM_NFKC);
    if (nfkc != string) {
        sky_test(sky_object_len(nfkc) == sky_object_len(string));
        sky_test(sky_object_compare(nfkc, string, SKY_COMPARE_OP_EQUAL));
    }
}


static sky_unicode_char_t
test_part1(test_case_t *test_case, sky_unicode_char_t last_cp)
{
    sky_unicode_char_t  cp;

    sky_test(1 == test_case->column[0].length);
    cp = sky_string_charat(test_case->column[0].u.string, 0);
    while (last_cp + 1 < cp) {
        if (++last_cp >= 0xD800 && last_cp <= 0xDFFF) {
            last_cp = 0xDFFF;
            continue;
        }
        test_part1_simple(&last_cp, 1);
    }

    test_nfd(test_case);
    test_nfc(test_case);
    test_nfkd(test_case);
    test_nfkc(test_case);

    return cp;
}


int
main(int argc, char *argv[])
{
    int                 part = -1;
    sky_unicode_char_t  last_part1_cp = -1;

    int                 num;
    char                *filename, *hash, *line, line_buffer[4096];
    FILE                *fp;
    size_t              line_length;
    test_case_t         *test_case;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    if (argc <= 1) {
        sky_format_asprintf(&filename,
                            "%s/unicode_data/NormalizationTest.txt",
                            CMAKE_SOURCE_DIR);
    }
    else {
        sky_format_asprintf(&filename,
                            "%s/unicode_data/NormalizationTest.txt",
                            argv[1]);
    }
    fprintf(stderr, "Using data from %s\n", filename);

#if defined(_WIN32)
    fopen_s(&fp, filename, "r");
#else
    fp = fopen(filename, "r");
#endif
    sky_test(NULL != fp);
    sky_free(filename);

    while ((line = fgets(line_buffer, sizeof(line_buffer), fp)) != NULL) {
        while (sky_ctype_isspace(*line)) {
            ++line;
        }
        if ((hash = strchr(line, '#')) != NULL) {
            *hash = '\0';
        }
        line_length = strlen(line);
        while (line_length > 0 && sky_ctype_isspace(line[line_length - 1])) {
            --line_length;
        }
        if (!line_length) {
            continue;
        }
        line[line_length] = '\0';

        if (line[0] == '@' &&
            !sky_strncasecmp(&(line[1]), "Part", 4) &&
            sky_ctype_isdigit(line[5]))
        {
            if (1 == part) {    /* Finishing part1 */
                while (last_part1_cp < SKY_UNICODE_CHAR_MAX) {
                    ++last_part1_cp;
                    test_part1_simple(&last_part1_cp, 1);
                }
            }
            part = line[5] - '0';
            printf("Testing NormalizationTest Part %d\n", part);
            sky_test(part >= 0 && part <= 3);
            num = 0;
            continue;
        }

        SKY_ASSET_BLOCK_BEGIN {
            test_case = parse_test_case(line, line_length);
            sky_test(NULL != test_case);
            sky_asset_save(test_case, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            ++num;
            switch (part) {
                case 0:
                case 2:
                case 3:
                    test_nfd(test_case);
                    test_nfc(test_case);
                    test_nfkd(test_case);
                    test_nfkc(test_case);
                    break;
                case 1:
                    last_part1_cp = test_part1(test_case, last_part1_cp);
                    break;
            }
        } SKY_ASSET_BLOCK_END;
    }

    fclose(fp);

    sky_library_finalize();
    return 0;
}
