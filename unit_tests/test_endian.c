#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    static char endian_test_b[4] = { 0x12, 0x34, 0x56, 0x78 },
                endian_test_l[4] = { 0x78, 0x56, 0x34, 0x12 };

    uint32_t    endian_test;

    endian_test = UINT32_C(0x12345678);
    if (!memcmp(&endian_test, endian_test_b, sizeof(endian_test))) {
        /* This system is big endian */
        sky_test(UINT16_C(0x1122) == sky_endian_btoh16(UINT16_C(0x1122)));
        sky_test(UINT32_C(0x11223344) == sky_endian_btoh32(UINT32_C(0x11223344)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_btoh64(UINT64_C(0x1122334455667788)));

        sky_test(UINT16_C(0x1122) == sky_endian_htob16(UINT16_C(0x2211)));
        sky_test(UINT32_C(0x11223344) == sky_endian_htob32(UINT32_C(0x44332211)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_htob64(UINT64_C(0x8877665544332211)));

        sky_test(UINT16_C(0x1122) == sky_endian_ltoh16(UINT16_C(0x2211)));
        sky_test(UINT32_C(0x11223344) == sky_endian_ltoh32(UINT32_C(0x44332211)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_ltoh64(UINT64_C(0x8877665544332211)));

        sky_test(UINT16_C(0x2211) == sky_endian_htol16(UINT16_C(0x1122)));
        sky_test(UINT32_C(0x44332211) == sky_endian_htol32(UINT32_C(0x11223344)));
        sky_test(UINT64_C(0x8877665544332211) == sky_endian_htol64(UINT64_C(0x1122334455667788)));
    }
    else if (!memcmp(&endian_test, endian_test_l, sizeof(endian_test))) {
        /* This system is little endian */
        sky_test(UINT16_C(0x2211) == sky_endian_btoh16(UINT16_C(0x1122)));
        sky_test(UINT32_C(0x44332211) == sky_endian_btoh32(UINT32_C(0x11223344)));
        sky_test(UINT64_C(0x8877665544332211) == sky_endian_btoh64(UINT64_C(0x1122334455667788)));

        sky_test(UINT16_C(0x1122) == sky_endian_htob16(UINT16_C(0x2211)));
        sky_test(UINT32_C(0x11223344) == sky_endian_htob32(UINT32_C(0x44332211)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_htob64(UINT64_C(0x8877665544332211)));

        sky_test(UINT16_C(0x1122) == sky_endian_ltoh16(UINT16_C(0x1122)));
        sky_test(UINT32_C(0x11223344) == sky_endian_ltoh32(UINT32_C(0x11223344)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_ltoh64(UINT64_C(0x1122334455667788)));

        sky_test(UINT16_C(0x1122) == sky_endian_htol16(UINT16_C(0x1122)));
        sky_test(UINT32_C(0x11223344) == sky_endian_htol32(UINT32_C(0x11223344)));
        sky_test(UINT64_C(0x1122334455667788) == sky_endian_htol64(UINT64_C(0x1122334455667788)));
    }
    else {
        fprintf(stderr, "This system is neither big nor little endian.\n");
        _exit(1);
    }

    return 0;
}
