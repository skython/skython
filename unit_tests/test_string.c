#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    sky_string_t    string;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    string = sky_string_createfrombytes("string", 6, NULL, NULL);
    sky_test(NULL != string);
    sky_test(sky_object_isa(string, sky_string_type));
    sky_test(6 == sky_string_len(string));
    sky_test('s' == sky_string_charat(string, 0));
    sky_test('t' == sky_string_charat(string, 1));
    sky_test('r' == sky_string_charat(string, 2));
    sky_test('i' == sky_string_charat(string, 3));
    sky_test('n' == sky_string_charat(string, 4));
    sky_test('g' == sky_string_charat(string, 5));
    sky_test(sky_object_compare(SKY_STRING_LITERAL("string"),
                                string,
                                SKY_COMPARE_OP_EQUAL));

    string = sky_string_replace(SKY_STRING_LITERAL("string"),
                                SKY_STRING_LITERAL(""),
                                SKY_STRING_LITERAL(" "),
                                -1);
    sky_format_fprintf(stdout, "'string'.replace('', ' ') -> %#@\n", string);
    sky_test(sky_object_compare(SKY_STRING_LITERAL(" s t r i n g "),
                                string,
                                SKY_COMPARE_OP_EQUAL));

    string = sky_string_replace(string,
                                SKY_STRING_LITERAL(" "),
                                SKY_STRING_LITERAL(""),
                                -1);
    sky_format_fprintf(stdout, "'s t r i n g '.replace(' ', '') -> %#@\n", string);
    sky_test(sky_object_compare(SKY_STRING_LITERAL("string"),
                                string,
                                SKY_COMPARE_OP_EQUAL));

    string = sky_string_upper(SKY_STRING_LITERAL("upper"));
    sky_format_fprintf(stdout, "'upper'.upper() -> %#@\n", string);
    sky_test(sky_object_compare(SKY_STRING_LITERAL("UPPER"),
                                string,
                                SKY_COMPARE_OP_EQUAL));

    string = sky_string_lower(SKY_STRING_LITERAL("LOWER"));
    sky_format_fprintf(stdout, "'LOWER'.lower() -> %#@\n", string);
    sky_test(sky_object_compare(SKY_STRING_LITERAL("lower"),
                                string,
                                SKY_COMPARE_OP_EQUAL));

    sky_library_finalize();
    return 0;
}
