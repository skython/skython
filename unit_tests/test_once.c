#include "sky_unit_test.h"


static sky_once_t   dynamic_once;
static sky_bool_t   dynamic_once_done  = SKY_FALSE;
static void *       dynamic_once_value;

static sky_once_t   static_once        = SKY_ONCE_INITIALIZER;
static sky_bool_t   static_once_done   = SKY_FALSE;
static void *       static_once_value;


static void
dynamic_once_function(void *arg)
{
    sky_test(0 == !!dynamic_once_done);
    sky_test(dynamic_once_value == arg);

    dynamic_once_done = SKY_TRUE;
}


static void
static_once_function(void *arg)
{
    sky_test(1 == !!dynamic_once_done);
    sky_test(static_once_value == arg);

    static_once_done = SKY_TRUE;
}


int
main(int argc, char *argv[])
{
#if defined(SKY_ARCH_32BIT)
    dynamic_once_value = (void *)sky_util_random32();
    static_once_value  = (void *)sky_util_random32();
#elif defined(SKY_ARCH_64BIT)
    dynamic_once_value = (void *)sky_util_random64();
    static_once_value  = (void *)sky_util_random64();
#else
#   error implementation missing
#endif

    sky_once_init(&dynamic_once);
    sky_once_run(&dynamic_once, dynamic_once_function, dynamic_once_value);
    sky_test(1 == !!dynamic_once_done);
    sky_once_run(&dynamic_once, dynamic_once_function, dynamic_once_value);
    sky_test(1 == !!dynamic_once_done);

    sky_once_run(&static_once, static_once_function, static_once_value);
    sky_test(1 == !!static_once_done);
    sky_once_run(&static_once, static_once_function, static_once_value);
    sky_test(1 == !!static_once_done);

    return 0;
}
