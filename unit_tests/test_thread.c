#include "sky_unit_test.h"


static void *
thread_main(void *arg)
{
    *(sky_bool_t *)arg = SKY_TRUE;

#if defined(SKY_ARCH_32BIT)
    return (void *)INT32_C(0x12345678);
#elif defined(SKY_ARCH_64BIT)
    return (void *)INT64_C(0x1122334455667788);
#else
#   error implementation missing
#endif
}


int
main(int argc, char *argv[])
{
    void            *exit_value;
    sky_bool_t      thread_ran;
    sky_thread_t    thread;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    thread_ran = SKY_FALSE;
    thread = sky_thread_create(thread_main, &thread_ran);
    sky_test(NULL != thread);
    sky_test(sky_object_isa(thread, sky_thread_type));

    exit_value = sky_thread_join(thread);
#if defined(SKY_ARCH_32BIT)
    sky_test(INT32_C(0x12345678) == (int32_t)exit_value);
#elif defined(SKY_ARCH_64BIT)
    sky_test(INT64_C(0x1122334455667788) == (int64_t)exit_value);
#else
#   error implementation missing
#endif

    sky_test(SKY_TRUE == thread_ran);

    sky_library_finalize();
    return 0;
}
