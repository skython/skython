#include "sky_unit_test.h"


typedef struct test_lifo_node_s {
    void * volatile                     next;
    char                                string[0];
} test_lifo_node_t;


int
main(int argc, char *argv[])
{
    test_lifo_node_t    *hello, *world;
    sky_atomic_lifo_t   lifo;

    sky_atomic_lifo_init(&lifo, offsetof(test_lifo_node_t, next));
    sky_test(NULL == sky_atomic_lifo_pop(&lifo));

    world = sky_malloc(sizeof(test_lifo_node_t) + sizeof("world"));
    memcpy(world->string, "world", sizeof("world"));
    sky_atomic_lifo_push(&lifo, world);
    sky_test(world == lifo.pointer);
    sky_test(NULL  == world->next);

    hello = sky_malloc(sizeof(test_lifo_node_t) + sizeof("hello"));
    memcpy(hello->string, "hello", sizeof("hello"));
    sky_atomic_lifo_push(&lifo, hello);
    sky_test(hello == lifo.pointer);
    sky_test(world == hello->next);
    sky_test(NULL  == world->next);

    sky_test(hello == sky_atomic_lifo_pop(&lifo));
    sky_test(world == sky_atomic_lifo_pop(&lifo));
    sky_test(NULL  == sky_atomic_lifo_pop(&lifo));

    sky_free(hello);
    sky_free(world);

    return 0;
}
