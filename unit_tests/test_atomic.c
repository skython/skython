#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    void        *initial_pointer, *new_pointer, *old_pointer, *value_pointer;
    int32_t     amount_int32, initial_int32, new_int32, old_int32, value_int32;
    int64_t     amount_int64, initial_int64, new_int64, old_int64, value_int64;
    uint32_t    bitn;
    uint32_t    initial_uint32, mask_uint32, new_uint32, old_uint32, value_uint32;
    uint64_t    initial_uint64, mask_uint64, new_uint64, old_uint64, value_uint64;
#if defined(__x86_64__) || defined(_M_X64)
    SKY_ALIGNED(16)
    int64_t     initial_128[2], new_128[2], old_128[2], value_128[2];
#endif


    /**************************************************************************
     ***
     ***    Arithmetic
     ***
     *************************************************************************/

    /* sky_atomic_add32() */
    amount_int32 = sky_util_random32();
    value_int32 = initial_int32 = sky_util_random32();
    new_int32 = sky_atomic_add32(amount_int32, &value_int32);
    sky_test(new_int32 == value_int32);
    sky_test(initial_int32 + amount_int32 == new_int32);

    /* sky_atomic_add32_old() */
    amount_int32 = sky_util_random32();
    value_int32 = initial_int32 = sky_util_random32();
    old_int32 = sky_atomic_add32_old(amount_int32, &value_int32);
    sky_test(old_int32 == initial_int32);
    sky_test(initial_int32 + amount_int32 == value_int32);

    /* sky_atomic_decrement32() */
    initial_int32 = value_int32;
    new_int32 = sky_atomic_decrement32(&value_int32);
    sky_test(value_int32 == new_int32);
    sky_test(initial_int32 - 1 == new_int32);

    /* sky_atomic_decrement32_old() */
    initial_int32 = value_int32;
    old_int32 = sky_atomic_decrement32_old(&value_int32);
    sky_test(old_int32 == initial_int32);
    sky_test(initial_int32 - 1 == value_int32);

    /* sky_atomic_increment32() */
    initial_int32 = value_int32;
    new_int32 = sky_atomic_increment32(&value_int32);
    sky_test(value_int32 == new_int32);
    sky_test(initial_int32 + 1 == new_int32);

    /* sky_atomic_increment32_old() */
    initial_int32 = value_int32;
    old_int32 = sky_atomic_increment32_old(&value_int32);
    sky_test(old_int32 == initial_int32);
    sky_test(initial_int32 + 1 == value_int32);

    /* sky_atomic_add64() */
    amount_int64 = sky_util_random64();
    value_int64 = initial_int64 = sky_util_random64();
    new_int64 = sky_atomic_add64(amount_int64, &value_int64);
    sky_test(new_int64 == value_int64);
    sky_test(initial_int64 + amount_int64 == new_int64);

    /* sky_atomic_add64_old() */
    amount_int64 = sky_util_random64();
    value_int64 = initial_int64 = sky_util_random64();
    old_int64 = sky_atomic_add64_old(amount_int64, &value_int64);
    sky_test(old_int64 == initial_int64);
    sky_test(initial_int64 + amount_int64 == value_int64);

    /* sky_atomic_decrement64() */
    initial_int64 = value_int64;
    new_int64 = sky_atomic_decrement64(&value_int64);
    sky_test(value_int64 == new_int64);
    sky_test(initial_int64 - 1 == new_int64);

    /* sky_atomic_decrement64_old() */
    initial_int64 = value_int64;
    old_int64 = sky_atomic_decrement64_old(&value_int64);
    sky_test(old_int64 == initial_int64);
    sky_test(initial_int64 - 1 == value_int64);

    /* sky_atomic_increment64() */
    initial_int64 = value_int64;
    new_int64 = sky_atomic_increment64(&value_int64);
    sky_test(value_int64 == new_int64);
    sky_test(initial_int64 + 1 == new_int64);

    /* sky_atomic_increment64_old() */
    initial_int64 = value_int64;
    old_int64 = sky_atomic_increment64_old(&value_int64);
    sky_test(old_int64 == initial_int64);
    sky_test(initial_int64 + 1 == value_int64);


    /**************************************************************************
     ***
     ***    Compare and Set
     ***
     *************************************************************************/

    /* sky_atomic_cas */
#if defined(SKY_ARCH_32BIT)
    new_pointer = (void *)sky_util_random32();
    value_pointer = old_pointer = initial_pointer = (void *)sky_util_random32();
#elif defined(SKY_ARCH_64BIT)
    new_pointer = (void *)sky_util_random64();
    value_pointer = old_pointer = initial_pointer = (void *)sky_util_random64();
#else
#   error implementation missing
#endif
    sky_test(0 != sky_atomic_cas(old_pointer, new_pointer, &value_pointer));
    sky_test(new_pointer == value_pointer);

    /* sky_atomic_cas32 */
    new_int32 = sky_util_random32();
    value_int32 = old_int32 = initial_int32 = sky_util_random32();
    sky_test(0 != sky_atomic_cas32(old_int32, new_int32, &value_int32));
    sky_test(new_int32 == value_int32);

#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
    /* sky_atomic_cas64 */
    new_int64 = sky_util_random64();
    value_int64 = old_int64 = initial_int64 = sky_util_random64();
    sky_test(0 != sky_atomic_cas64(old_int64, new_int64, &value_int64));
    sky_test(new_int64 == value_int64);
#endif

#if defined(__x86_64__) || defined(_M_X64)
    /* sky_atomic_cas128 */
    new_128[0] = sky_util_random64();
    new_128[1] = sky_util_random64();
    value_128[0] = old_128[0] = initial_128[0] = sky_util_random64();
    value_128[1] = old_128[1] = initial_128[1] = sky_util_random64();
    sky_test(0 != sky_atomic_cas128(old_128, new_128, (void **)&value_128));
    sky_test(new_128[0] == value_128[0]);
    sky_test(new_128[1] == value_128[1]);
#endif


    /**************************************************************************
     ***
     ***    Exchange
     ***
     *************************************************************************/

    /* sky_atomic_exchange() */
#if defined(SKY_ARCH_32BIT)
    new_pointer = (void *)sky_util_random32();
    value_pointer = initial_pointer = (void *)sky_util_random32();
#elif defined(SKY_ARCH_64BIT)
    new_pointer = (void *)sky_util_random64();
    value_pointer = initial_pointer = (void *)sky_util_random64();
#else
#   error implementation missing
#endif
    sky_test(old_int32 == initial_int32);
    sky_test(new_int32 == value_int32);

    /* sky_atomic_exchange32() */
    new_int32 = sky_util_random32();
    value_int32 = initial_int32 = sky_util_random32();
    old_int32 = sky_atomic_exchange32(new_int32, &value_int32);
    sky_test(old_int32 == initial_int32);
    sky_test(new_int32 == value_int32);

    /* sky_atomic_exchange64() */
    new_int64 = sky_util_random64();
    value_int64 = initial_int64 = sky_util_random64();
    old_int64 = sky_atomic_exchange64(new_int64, &value_int64);
    sky_test(old_int64 == initial_int64);
    sky_test(new_int64 == value_int64);


    /**************************************************************************
     ***
     ***    Bit-wise Logical Operators
     ***
     *************************************************************************/

    /* sky_atomic_and32() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    new_uint32 = sky_atomic_and32(mask_uint32, &value_uint32);
    sky_test(new_uint32 == value_uint32);
    sky_test((initial_uint32 & mask_uint32) == value_uint32);

    /* sky_atomic_and32_old() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    old_uint32 = sky_atomic_and32_old(mask_uint32, &value_uint32);
    sky_test(old_uint32 == initial_uint32);
    sky_test((initial_uint32 & mask_uint32) == value_uint32);

    /* sky_atomic_or32() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    new_uint32 = sky_atomic_or32(mask_uint32, &value_uint32);
    sky_test(new_uint32 == value_uint32);
    sky_test((initial_uint32 | mask_uint32) == value_uint32);

    /* sky_atomic_or32_old() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    old_uint32 = sky_atomic_or32_old(mask_uint32, &value_uint32);
    sky_test(old_uint32 == initial_uint32);
    sky_test((initial_uint32 | mask_uint32) == value_uint32);

    /* sky_atomic_xor32() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    new_uint32 = sky_atomic_xor32(mask_uint32, &value_uint32);
    sky_test(new_uint32 == value_uint32);
    sky_test((initial_uint32 ^ mask_uint32) == value_uint32);

    /* sky_atomic_xor32_old() */
    mask_uint32 = sky_util_random32();
    value_uint32 = initial_uint32 = sky_util_random32();
    old_uint32 = sky_atomic_xor32_old(mask_uint32, &value_uint32);
    sky_test(old_uint32 == initial_uint32);
    sky_test((initial_uint32 ^ mask_uint32) == value_uint32);

#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
    /* sky_atomic_and64() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    new_uint64 = sky_atomic_and64(mask_uint64, &value_uint64);
    sky_test(new_uint64 == value_uint64);
    sky_test((initial_uint64 & mask_uint64) == value_uint64);

    /* sky_atomic_and64_old() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    old_uint64 = sky_atomic_and64_old(mask_uint64, &value_uint64);
    sky_test(old_uint64 == initial_uint64);
    sky_test((initial_uint64 & mask_uint64) == value_uint64);

    /* sky_atomic_or64() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    new_uint64 = sky_atomic_or64(mask_uint64, &value_uint64);
    sky_test(new_uint64 == value_uint64);
    sky_test((initial_uint64 | mask_uint64) == value_uint64);

    /* sky_atomic_or64_old() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    old_uint64 = sky_atomic_or64_old(mask_uint64, &value_uint64);
    sky_test(old_uint64 == initial_uint64);
    sky_test((initial_uint64 | mask_uint64) == value_uint64);

    /* sky_atomic_xor64() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    new_uint64 = sky_atomic_xor64(mask_uint64, &value_uint64);
    sky_test(new_uint64 == value_uint64);
    sky_test((initial_uint64 ^ mask_uint64) == value_uint64);

    /* sky_atomic_xor64_old() */
    mask_uint64 = sky_util_random64();
    value_uint64 = initial_uint64 = sky_util_random64();
    old_uint64 = sky_atomic_xor64_old(mask_uint64, &value_uint64);
    sky_test(old_uint64 == initial_uint64);
    sky_test((initial_uint64 ^ mask_uint64) == value_uint64);
#endif


    /**************************************************************************
     ***
     ***    Bit Test and Set or Clear
     ***
     *************************************************************************/

    /* sky_atomic_testandclear() */
    bitn = sky_util_random32() % 32;
    initial_uint32 = value_uint32 = sky_util_random32();
    new_uint32 = sky_atomic_testandclear(bitn, &value_uint32);
    sky_test(!!(initial_uint32 & (1 << bitn)) == new_uint32);
    sky_test((initial_uint32 & ~(1 << bitn)) == value_uint32);

    /* sky_atomic_testandset() */
    bitn = sky_util_random32() % 32;
    initial_uint32 = value_uint32 = sky_util_random32();
    new_uint32 = sky_atomic_testandset(bitn, &value_uint32);
    sky_test(!!(initial_uint32 & (1 << bitn)) == new_uint32);
    sky_test((initial_uint32 | (1 << bitn)) == value_uint32);

    return 0;
}
