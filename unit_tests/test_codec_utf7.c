#include "sky_unit_test.h"


int
main(int argc, char **argv)
{
    sky_bytes_t     bytes;
    sky_string_t    encoding, string;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    encoding = SKY_STRING_LITERAL("utf7");

    string = sky_string_createfrombytes("A+ImIDkQ.", 9, encoding, NULL);
    sky_test(sky_object_isa(string, sky_string_type));
    sky_test(sky_object_len(string) == 4);
    sky_test(sky_string_charat(string, 0) == 0x0041);
    sky_test(sky_string_charat(string, 1) == 0x2262);
    sky_test(sky_string_charat(string, 2) == 0x0391);
    sky_test(sky_string_charat(string, 3) == 0x002E);

    bytes = sky_codec_encode(sky_codec_utf7, string, encoding, NULL, 0);
    sky_format_fprintf(stderr, "encoded: %@\n", bytes);
    sky_test(sky_object_isa(bytes, sky_bytes_type));
    sky_test(sky_object_len(bytes) == 9);
    sky_test(sky_bytes_byteat(bytes, 0) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 1) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 2) == 0x49);   /* I */
    sky_test(sky_bytes_byteat(bytes, 3) == 0x6D);   /* m */
    sky_test(sky_bytes_byteat(bytes, 4) == 0x49);   /* I */
    sky_test(sky_bytes_byteat(bytes, 5) == 0x44);   /* D */
    sky_test(sky_bytes_byteat(bytes, 6) == 0x6B);   /* k */
    sky_test(sky_bytes_byteat(bytes, 7) == 0x51);   /* Q */
    sky_test(sky_bytes_byteat(bytes, 8) == 0x2E);   /* . */

    string = sky_string_createfrombytes("Hi Mom -+Jjo--!", 15, encoding, NULL);
    sky_test(sky_object_isa(string, sky_string_type));
    sky_test(sky_object_len(string) == 11);
    sky_test(sky_string_charat(string,  0) == 0x0048);
    sky_test(sky_string_charat(string,  1) == 0x0069);
    sky_test(sky_string_charat(string,  2) == 0x0020);
    sky_test(sky_string_charat(string,  3) == 0x004D);
    sky_test(sky_string_charat(string,  4) == 0x006F);
    sky_test(sky_string_charat(string,  5) == 0x006D);
    sky_test(sky_string_charat(string,  6) == 0x0020);
    sky_test(sky_string_charat(string,  7) == 0x002D);
    sky_test(sky_string_charat(string,  8) == 0x263A);
    sky_test(sky_string_charat(string,  9) == 0x002D);
    sky_test(sky_string_charat(string, 10) == 0x0021);

    bytes = sky_codec_encode(sky_codec_utf7,
                             string,
                             encoding,
                             NULL,
                             SKY_CODEC_UTF7_ENCODER_FLAG_OPTIONAL |
                             SKY_CODEC_UTF7_ENCODER_FLAG_WHITESPACE);
    sky_format_fprintf(stderr, "encoded: %@\n", bytes);
    sky_test(sky_object_isa(bytes, sky_bytes_type));
    sky_test(sky_object_len(bytes) == 27);
    sky_test(sky_bytes_byteat(bytes, 0) == 0x48);   /* H */
    sky_test(sky_bytes_byteat(bytes, 1) == 0x69);   /* i */
    sky_test(sky_bytes_byteat(bytes, 2) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 3) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 4) == 0x43);   /* C */
    sky_test(sky_bytes_byteat(bytes, 5) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 6) == 0x2D);   /* - */
    sky_test(sky_bytes_byteat(bytes, 7) == 0x4D);   /* M */
    sky_test(sky_bytes_byteat(bytes, 8) == 0x6F);   /* o */
    sky_test(sky_bytes_byteat(bytes, 9) == 0x6D);   /* m */
    sky_test(sky_bytes_byteat(bytes, 10) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 11) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 12) == 0x43);   /* C */
    sky_test(sky_bytes_byteat(bytes, 13) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 14) == 0x2D);   /* - */
    sky_test(sky_bytes_byteat(bytes, 15) == 0x2D);   /* - */
    sky_test(sky_bytes_byteat(bytes, 16) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 17) == 0x4A);   /* J */
    sky_test(sky_bytes_byteat(bytes, 18) == 0x6A);   /* j */
    sky_test(sky_bytes_byteat(bytes, 19) == 0x6F);   /* o */
    sky_test(sky_bytes_byteat(bytes, 20) == 0x2D);   /* - */
    sky_test(sky_bytes_byteat(bytes, 21) == 0x2D);   /* - */
    sky_test(sky_bytes_byteat(bytes, 22) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 23) == 0x41);   /* A */
    sky_test(sky_bytes_byteat(bytes, 24) == 0x43);   /* C */
    sky_test(sky_bytes_byteat(bytes, 25) == 0x45);   /* E */
    sky_test(sky_bytes_byteat(bytes, 26) == 0x2D);   /* - */

    string = sky_string_createfrombytes("+ZeVnLIqe-", 10, encoding, NULL);
    sky_test(sky_object_isa(string, sky_string_type));
    sky_test(sky_object_len(string) == 3);
    sky_test(sky_string_charat(string, 0) == 0x65E5);
    sky_test(sky_string_charat(string, 1) == 0x672C);
    sky_test(sky_string_charat(string, 2) == 0x8A9E);

    bytes = sky_codec_encode(sky_codec_utf7, string, encoding, NULL, 0);
    sky_format_fprintf(stderr, "encoded: %@\n", bytes);
    sky_test(sky_object_isa(bytes, sky_bytes_type));
    sky_test(sky_object_len(bytes) == 10);
    sky_test(sky_bytes_byteat(bytes, 0) == 0x2B);   /* + */
    sky_test(sky_bytes_byteat(bytes, 1) == 0x5A);   /* Z */
    sky_test(sky_bytes_byteat(bytes, 2) == 0x65);   /* e */
    sky_test(sky_bytes_byteat(bytes, 3) == 0x56);   /* V */
    sky_test(sky_bytes_byteat(bytes, 4) == 0x6E);   /* n */
    sky_test(sky_bytes_byteat(bytes, 5) == 0x4C);   /* L */
    sky_test(sky_bytes_byteat(bytes, 6) == 0x49);   /* I */
    sky_test(sky_bytes_byteat(bytes, 7) == 0x71);   /* q */
    sky_test(sky_bytes_byteat(bytes, 8) == 0x65);   /* e */
    sky_test(sky_bytes_byteat(bytes, 9) == 0x2D);   /* - */

    sky_library_finalize();
    return 0;
}
