#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    ssize_t         i;
    sky_list_t      list;
    sky_tuple_t     tuple;
    sky_object_t    array[4], iterator, object_1, object_2;
    sky_string_t    first_item, second_item, third_item, fourth_item;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    first_item  = array[0] = SKY_STRING_LITERAL("first");
    second_item = array[1] = SKY_STRING_LITERAL("second");
    third_item  = array[2] = SKY_STRING_LITERAL("third");
    fourth_item = array[3] = SKY_STRING_LITERAL("fourth");

    tuple = sky_tuple_pack(3, first_item, second_item, third_item);
    sky_test(NULL != tuple);
    sky_test(sky_object_isa(tuple, sky_tuple_type));
    sky_test(sky_object_len(tuple) == 3);
    sky_test(sky_tuple_find(tuple, first_item) == 0);
    sky_test(sky_tuple_find(tuple, second_item) == 1);
    sky_test(sky_tuple_find(tuple, third_item) == 2);
    sky_test(sky_tuple_find(tuple, fourth_item) == -1);
    sky_test(sky_tuple_get(tuple, 0) == first_item);
    sky_test(sky_tuple_get(tuple, 1) == second_item);
    sky_test(sky_tuple_get(tuple, 2) == third_item);

    tuple = sky_tuple_createfromarray(4, array);
    sky_test(NULL != tuple);
    sky_test(sky_object_isa(tuple, sky_tuple_type));
    sky_test(sky_object_len(tuple) == 4);
    sky_test(sky_tuple_find(tuple, first_item) == 0);
    sky_test(sky_tuple_find(tuple, second_item) == 1);
    sky_test(sky_tuple_find(tuple, third_item) == 2);
    sky_test(sky_tuple_find(tuple, fourth_item) == 3);
    sky_test(sky_tuple_get(tuple, 0) == first_item);
    sky_test(sky_tuple_get(tuple, 1) == second_item);
    sky_test(sky_tuple_get(tuple, 2) == third_item);
    sky_test(sky_tuple_get(tuple, 3) == fourth_item);

    list = sky_list_create(NULL);
    sky_list_append(list, first_item);
    sky_list_append(list, second_item);
    sky_list_append(list, third_item);
    sky_list_append(list, fourth_item);

    tuple = sky_tuple_create(list);
    sky_test(NULL != tuple);
    sky_test(sky_object_isa(tuple, sky_tuple_type));
    sky_test(sky_object_len(tuple) == 4);
    sky_test(sky_tuple_find(tuple, first_item) == 0);
    sky_test(sky_tuple_find(tuple, second_item) == 1);
    sky_test(sky_tuple_find(tuple, third_item) == 2);
    sky_test(sky_tuple_find(tuple, fourth_item) == 3);
    sky_test(sky_tuple_get(tuple, 0) == first_item);
    sky_test(sky_tuple_get(tuple, 1) == second_item);
    sky_test(sky_tuple_get(tuple, 2) == third_item);
    sky_test(sky_tuple_get(tuple, 3) == fourth_item);

    iterator = sky_object_iter(tuple);
    for (i = 0; (object_1 = sky_object_next(iterator, NULL)) != NULL; ++i) {
        object_2 = sky_tuple_get(tuple, i);
        sky_test(sky_object_compare(object_1, object_2, SKY_COMPARE_OP_EQUAL));
    }

    sky_library_finalize();
    return 0;
}
