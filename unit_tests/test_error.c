#include "sky_unit_test.h"
#include "config.h"
#ifdef HAVE_LIMITS_H
#   include <limits.h>
#endif


int
main(int argc, char *argv[])
{
    sky_tuple_t     args;
    volatile int    executed;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    executed = 0;
    SKY_ERROR_TRY {
        executed = 1;
    } SKY_ERROR_TRY_END;
    sky_test(1 == executed);
    sky_test(NULL == sky_error_current());

    executed = 0;
    SKY_ERROR_TRY {
        sky_error_raise_object(sky_RuntimeError, sky_None);
    }
    SKY_ERROR_EXCEPT_ANY {
        executed = 1;
    } SKY_ERROR_TRY_END;
    sky_test(1 == executed);
    sky_test(NULL == sky_error_current());

    executed = 0;
    SKY_ERROR_TRY {
        SKY_ERROR_TRY {
            sky_error_raise_object(sky_RuntimeError, sky_None);
        }
        SKY_ERROR_EXCEPT_ANY {
            sky_test(NULL != sky_error_current());
            sky_test(sky_RuntimeError == sky_error_current()->type);
            sky_test(sky_object_isa(sky_error_current()->value,
                                    sky_RuntimeError));
            args = sky_object_getattr(sky_error_current()->value,
                                      SKY_STRING_LITERAL("args"),
                                      NULL);
            sky_test(NULL != args);
            sky_test(sky_object_isa(args, sky_tuple_type));
            sky_test(0 == sky_object_len(args));
            sky_error_raise();
        } SKY_ERROR_TRY_END;
    }
    SKY_ERROR_EXCEPT_ANY {
        sky_test(NULL != sky_error_current());
        sky_test(sky_RuntimeError == sky_error_current()->type);
        sky_test(sky_object_isa(sky_error_current()->value,
                                sky_RuntimeError));
        args = sky_object_getattr(sky_error_current()->value,
                                  SKY_STRING_LITERAL("args"),
                                  NULL);
        sky_test(NULL != args);
        sky_test(sky_object_isa(args, sky_tuple_type));
        sky_test(0 == sky_object_len(args));
        executed = 1;
    } SKY_ERROR_TRY_END;
    sky_test(1 == executed);
    sky_test(NULL == sky_error_current());

    executed = 0;
    SKY_ERROR_TRY {
        SKY_ERROR_TRY {
            sky_test(NULL == sky_error_current());
            sky_error_raise_object(sky_RuntimeError,
                                   sky_integer_create(0xdeadbeef));
        }
        SKY_ERROR_EXCEPT_ANY {
            sky_test(NULL != sky_error_current());
            sky_test(sky_RuntimeError == sky_error_current()->type);
            sky_test(sky_object_isa(sky_error_current()->value,
                                    sky_RuntimeError));
            args = sky_object_getattr(sky_error_current()->value,
                                      SKY_STRING_LITERAL("args"),
                                      NULL);
            sky_test(NULL != args);
            sky_test(sky_object_isa(args, sky_tuple_type));
            sky_test(1 == sky_object_len(args));
            sky_test(0xdeadbeef == sky_integer_value(sky_tuple_get(args, 0),
                                                     0,
                                                     INT64_MAX,
                                                     NULL));
            sky_error_raise_object(sky_RuntimeError,
                                   sky_integer_create(0xfeedface));
        } SKY_ERROR_TRY_END;
    }
    SKY_ERROR_EXCEPT_ANY {
        sky_test(NULL != sky_error_current());
        sky_test(sky_RuntimeError == sky_error_current()->type);
        sky_test(sky_object_isa(sky_error_current()->value,
                                sky_RuntimeError));
        args = sky_object_getattr(sky_error_current()->value,
                                  SKY_STRING_LITERAL("args"),
                                  NULL);
        sky_test(NULL != args);
        sky_test(sky_object_isa(args, sky_tuple_type));
        sky_test(1 == sky_object_len(args));
        sky_test(0xfeedface == sky_integer_value(sky_tuple_get(args, 0),
                                                 0,
                                                 INT64_MAX,
                                                 NULL));
        executed = 1;
    } SKY_ERROR_TRY_END;
    sky_test(1 == executed);

    sky_library_finalize();

    return 0;
}
