#include "sky_unit_test.h"


static void
asset_free_function(void *pointer)
{
    *(int *)pointer = 1;
}


int
main(int argc, char *argv[])
{
    int cleaned_up, some_other_integer;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    SKY_ASSET_BLOCK_BEGIN {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_NEVER);
    } SKY_ASSET_BLOCK_END;
    sky_test(0 == cleaned_up);

    SKY_ASSET_BLOCK_BEGIN {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_ALWAYS);
    } SKY_ASSET_BLOCK_END;
    sky_test(1 == cleaned_up);

    SKY_ASSET_BLOCK_BEGIN {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_ON_ERROR);
    } SKY_ASSET_BLOCK_END;
    sky_test(0 == cleaned_up);

    SKY_ASSET_BLOCK_BEGIN {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_ON_SUCCESS);
    } SKY_ASSET_BLOCK_END;
    sky_test(1 == cleaned_up);

    SKY_ERROR_TRY {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_ON_ERROR);
        sky_error_raise_object(sky_RuntimeError, sky_None);
    }
    SKY_ERROR_EXCEPT_ANY {
    } SKY_ERROR_TRY_END;
    sky_test(1 == cleaned_up);

    SKY_ERROR_TRY {
        cleaned_up = 0;
        sky_asset_save(&cleaned_up, asset_free_function, SKY_ASSET_CLEANUP_ON_SUCCESS);
        sky_error_raise_object(sky_RuntimeError, sky_None);
    }
    SKY_ERROR_EXCEPT_ANY {
    } SKY_ERROR_TRY_END;
    sky_test(0 == cleaned_up);

    SKY_ASSET_BLOCK_BEGIN {
        cleaned_up = some_other_integer = 0;
        sky_asset_save(&some_other_integer, asset_free_function, SKY_ASSET_CLEANUP_ALWAYS);
        sky_asset_update(&some_other_integer, &cleaned_up, SKY_ASSET_UPDATE_ALL_DEEP);
    } SKY_ASSET_BLOCK_END;
    sky_test(0 == some_other_integer);
    sky_test(1 == cleaned_up);

    sky_library_finalize();

    return 0;
}
