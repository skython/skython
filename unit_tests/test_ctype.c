#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    int i;

    /* sky_ctype_isalnum(), sky_ctype_isalpha(), sky_ctype_isdigit(),
     * sky_ctype_islower(), sky_ctype_isupper(),
     * sky_ctype_tolower(), sky_ctype_toupper()
     */
    for (i = 0;  i < 255;  i++) {
        switch (i) {
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
                sky_test(1 == !!sky_ctype_isalnum(i));
                sky_test(0 == !!sky_ctype_isalpha(i));
                sky_test(1 == !!sky_ctype_isdigit(i));
                sky_test(0 == !!sky_ctype_islower(i));
                sky_test(0 == !!sky_ctype_isupper(i));
                sky_test(sky_ctype_tolower(i) == i);
                sky_test(sky_ctype_toupper(i) == i);
                break;
            case 'A': case 'B': case 'C': case 'D': case 'E':
            case 'F': case 'G': case 'H': case 'I': case 'J':
            case 'K': case 'L': case 'M': case 'N': case 'O':
            case 'P': case 'Q': case 'R': case 'S': case 'T':
            case 'U': case 'V': case 'W': case 'X': case 'Y':
            case 'Z':
                sky_test(1 == !!sky_ctype_isalnum(i));
                sky_test(1 == !!sky_ctype_isalpha(i));
                sky_test(0 == !!sky_ctype_isdigit(i));
                sky_test(0 == !!sky_ctype_islower(i));
                sky_test(1 == !!sky_ctype_isupper(i));
                sky_test(sky_ctype_tolower(i) == (i + 0x20));
                sky_test(sky_ctype_toupper(i) == i);
                break;
            case 'a': case 'b': case 'c': case 'd': case 'e':
            case 'f': case 'g': case 'h': case 'i': case 'j':
            case 'k': case 'l': case 'm': case 'n': case 'o':
            case 'p': case 'q': case 'r': case 's': case 't':
            case 'u': case 'v': case 'w': case 'x': case 'y':
            case 'z':
                sky_test(1 == !!sky_ctype_isalnum(i));
                sky_test(1 == !!sky_ctype_isalpha(i));
                sky_test(0 == !!sky_ctype_isdigit(i));
                sky_test(1 == !!sky_ctype_islower(i));
                sky_test(0 == !!sky_ctype_isupper(i));
                sky_test(sky_ctype_tolower(i) == i);
                sky_test(sky_ctype_toupper(i) == (i - 0x20));
                break;
            default:
                sky_test(0 == !!sky_ctype_isalnum(i));
                sky_test(0 == !!sky_ctype_isalpha(i));
                sky_test(0 == !!sky_ctype_isdigit(i));
                sky_test(0 == !!sky_ctype_islower(i));
                sky_test(0 == !!sky_ctype_isupper(i));
                sky_test(sky_ctype_tolower(i) == i);
                sky_test(sky_ctype_toupper(i) == i);
                break;
        }
    }

    /* sky_ctype_isodigit() */
    for (i = 0;  i < 255;  i++) {
        switch (i) {
            case '0': case '1': case '2': case '3':
            case '4': case '5': case '6': case '7':
                sky_test(1 == !!sky_ctype_isodigit(i));
                break;
            default:
                sky_test(0 == !!sky_ctype_isodigit(i));
                break;
        }
    }

    /* sky_ctype_isprint() */
    for (i = 0;  i < 255;  i++) {
        if (i >= 0x20 && i <= 0x7e) {
            sky_test(1 == !!sky_ctype_isprint(i));
        }
        else {
            sky_test(0 == !!sky_ctype_isprint(i));
        }
    }

    /* sky_ctype_isspace() */
    for (i = 0;  i < 255;  i++) {
        switch (i) {
            case '\f': case '\n': case '\r':
            case '\t': case '\v': case ' ':
                sky_test(1 == !!sky_ctype_isspace(i));
                break;
            default:
                sky_test(0 == !!sky_ctype_isspace(i));
                break;
        }
    }

    /* sky_ctype_isxdigit() */
    for (i = 0;  i < 255;  i++) {
        switch (i) {
            case '0': case '1': case '2': case '3':
            case '4': case '5': case '6': case '7':
            case '8': case '9': case 'A': case 'B':
            case 'C': case 'D': case 'E': case 'F':
            case 'a': case 'b': case 'c': case 'd':
            case 'e': case 'f':
                sky_test(1 == !!sky_ctype_isxdigit(i));
                break;
            default:
                sky_test(0 == !!sky_ctype_isxdigit(i));
                break;
        }
    }

    return 0;
}
