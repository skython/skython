#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    int             got_one, got_three, got_two;
    sky_dict_t      dict;
    sky_object_t    iterator, key, value;
    sky_capsule_t   one_key, two_key, three_key;
    sky_capsule_t   one_value, two_value, three_value;

    sky_test(sky_library_initialize(argv[0], argc, argv,
                                    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    dict = sky_dict_create();
    sky_test(NULL != dict);
    sky_test(sky_object_isa(dict, sky_dict_type));

    one_key     = sky_capsule_create("one", NULL, NULL, NULL);
    one_value   = sky_capsule_create((void *)1, NULL, NULL, NULL);
    two_key     = sky_capsule_create("two", NULL, NULL, NULL);
    two_value   = sky_capsule_create((void *)2, NULL, NULL, NULL);
    three_key   = sky_capsule_create("three", NULL, NULL, NULL);
    three_value = sky_capsule_create((void *)3, NULL, NULL, NULL);

    sky_test(SKY_TRUE == sky_dict_add(dict, one_key, one_value));
    sky_test(SKY_FALSE == sky_dict_add(dict, one_key, one_value));
    sky_test(1 == sky_object_len(dict));
    sky_test(one_value == sky_dict_get(dict, one_key, NULL));

    sky_test(SKY_FALSE == sky_dict_delete(dict, two_key));

    sky_test(SKY_FALSE == sky_dict_replace(dict, two_key, two_value));
    sky_dict_setitem(dict, two_key, three_value);
    sky_test(SKY_TRUE == sky_dict_replace(dict, two_key, two_value));
    sky_test(2 == sky_object_len(dict));
    sky_test(one_value == sky_dict_get(dict, one_key, NULL));
    sky_test(two_value == sky_dict_get(dict, two_key, NULL));

    sky_test(SKY_FALSE == sky_dict_delete(dict, three_key));

    sky_dict_setitem(dict, three_key, three_value);
    sky_test(3 == sky_object_len(dict));
    sky_test(one_value == sky_dict_get(dict, one_key, NULL));
    sky_test(two_value == sky_dict_get(dict, two_key, NULL));
    sky_test(three_value == sky_dict_get(dict, three_key, NULL));

    sky_test(SKY_TRUE == sky_dict_delete(dict, two_key));
    sky_test(2 == sky_object_len(dict));
    sky_test(NULL == sky_dict_get(dict, two_key, NULL));
    sky_test(SKY_TRUE == sky_dict_add(dict, two_key, two_value));
    sky_test(3 == sky_object_len(dict));

    got_one = got_two = got_three = 0;
    iterator = sky_object_iter(dict);
    while ((key = sky_object_next(iterator, NULL)) != NULL) {
        sky_test(key == one_key || key == two_key || key == three_key);
        value = sky_dict_get(dict, key, NULL);
        if (key == one_key) {
            ++got_one;
            sky_test(one_value == value);
        }
        else if (key == two_key) {
            ++got_two;
            sky_test(two_value == value);
        }
        else if (key == three_key) {
            ++got_three;
            sky_test(three_value == value);
        }
    }
    sky_test(1 == got_one);
    sky_test(1 == got_two);
    sky_test(1 == got_three);

    got_one = got_two = got_three = 0;
    SKY_SEQUENCE_FOREACH(dict, key) {
        value = sky_dict_getitem(dict, key);
        sky_test(key == one_key || key == two_key || key == three_key);
        if (key == one_key) {
            ++got_one;
            sky_test(one_value == value);
        }
        else if (key == two_key) {
            ++got_two;
            sky_test(two_value == value);
        }
        else if (key == three_key) {
            ++got_three;
            sky_test(three_value == value);
        }
    } SKY_SEQUENCE_FOREACH_END;
    sky_test(1 == got_one);
    sky_test(1 == got_two);
    sky_test(1 == got_three);

    sky_dict_clear(dict);
    sky_test(0 == sky_object_len(dict));

    sky_library_finalize();
    return 0;
}
