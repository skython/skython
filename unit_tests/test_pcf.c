#include "sky_unit_test.h"


int
main(int argc, char *argv[])
{
    unsigned int flags = SKY_LIBRARY_INITIALIZE_FLAG_DONT_WRITE_BYTECODE |
                         SKY_LIBRARY_INITIALIZE_FLAG_VERBOSE;

    sky_test(1 == !!sky_library_initialize(argv[0], argc, argv, flags));
    sky_library_finalize();

    sky_test(1 == !!sky_library_initialize(argv[0], argc, argv, flags));
    sky_test(1 == !!sky_library_initialize(argv[0], argc, argv, flags));
    sky_test(1 == !!sky_library_initialize(argv[0], argc, argv, flags));
    sky_library_finalize();
    sky_library_finalize();
    sky_library_finalize();

    return 0;
}
