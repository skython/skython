#ifndef __SKYTHON_SKY_UNIT_TESTS_SKY_UNIT_TEST_H__
#define __SKYTHON_SKY_UNIT_TESTS_SKY_UNIT_TEST_H__ 1


#include "../core/skython.h"
#include "../config.h"

#include <time.h>


#define sky_test(expr)  do {                                                \
        if (unlikely(!(expr))) {                                            \
            fprintf(stderr, "%s line %u: " #expr "\n", __FILE__, __LINE__); \
            _exit(1);                                                       \
        }                                                                   \
    } while (0)


#endif  /* __SKYTHON_SKY_UNIT_TESTS_SKY_UNIT_TEST_H__ */
