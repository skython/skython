#include "../core/skython.h"

#include "sha2.h"


SKY_MODULE_EXTERN void
skython_module__sha512_initialize(sky_module_t module);


static sky_type_t sky__sha384_sha384_type;
static sky_type_t sky__sha512_sha512_type;

SKY_EXTERN_INLINE SHA384_CTX *
sky__sha384_sha384_data(sky_object_t object)
{
    return sky_object_data(object, sky__sha384_sha384_type);
}

SKY_EXTERN_INLINE SHA512_CTX *
sky__sha512_sha512_data(sky_object_t object)
{
    return sky_object_data(object, sky__sha512_sha512_type);
}


static void
sky__sha384_sha384_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    SHA384_CTX  *self_data = data;

    SHA384_Init(self_data);
}

static void
sky__sha512_sha512_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    SHA512_CTX  *self_data = data;

    SHA512_Init(self_data);
}


static sky_object_t
sky__sha384_sha384_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA384_BLOCK_LENGTH);
}

static sky_object_t
sky__sha512_sha512_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA512_BLOCK_LENGTH);
}

static sky_object_t
sky__sha384_sha384_copy(sky_object_t self)
{
    sky_object_t    new_sha384;

    new_sha384 = sky_object_allocate(sky__sha384_sha384_type);
    memcpy(sky__sha384_sha384_data(new_sha384),
           sky__sha384_sha384_data(self),
           sizeof(SHA384_CTX));

    return new_sha384;
}

static sky_object_t
sky__sha512_sha512_copy(sky_object_t self)
{
    sky_object_t    new_sha512;

    new_sha512 = sky_object_allocate(sky__sha512_sha512_type);
    memcpy(sky__sha512_sha512_data(new_sha512),
           sky__sha512_sha512_data(self),
           sizeof(SHA512_CTX));

    return new_sha512;
}

static sky_bytes_t
sky__sha384_sha384_digest(sky_object_t self)
{
    SHA384_CTX  *self_data = sky__sha384_sha384_data(self);

    uint8_t     *raw_bytes;
    SHA384_CTX  final_data;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(SHA384_CTX));

        raw_bytes = sky_asset_malloc(SHA384_DIGEST_LENGTH,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        SHA384_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SHA384_DIGEST_LENGTH,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_bytes_t
sky__sha512_sha512_digest(sky_object_t self)
{
    SHA512_CTX  *self_data = sky__sha512_sha512_data(self);

    uint8_t     *raw_bytes;
    SHA512_CTX  final_data;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(SHA512_CTX));

        raw_bytes = sky_asset_malloc(SHA512_DIGEST_LENGTH,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        SHA512_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SHA512_DIGEST_LENGTH,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_object_t
sky__sha384_sha384_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA384_DIGEST_LENGTH);
}

static sky_object_t
sky__sha512_sha512_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA512_DIGEST_LENGTH);
}

static sky_string_t
sky__sha384_sha384_hexdigest(sky_object_t self)
{
    SHA384_CTX  *self_data = sky__sha384_sha384_data(self);

    char        hexdigest[SHA384_DIGEST_LENGTH * 2], *p;
    size_t      i;
    uint8_t     digest[SHA384_DIGEST_LENGTH];
    const char  *hexdigits;
    SHA384_CTX  final_data;

    memcpy(&final_data, self_data, sizeof(SHA384_CTX));
    SHA384_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_string_t
sky__sha512_sha512_hexdigest(sky_object_t self)
{
    SHA512_CTX  *self_data = sky__sha512_sha512_data(self);

    char        hexdigest[SHA512_DIGEST_LENGTH * 2], *p;
    size_t      i;
    uint8_t     digest[SHA512_DIGEST_LENGTH];
    const char  *hexdigits;
    SHA512_CTX  final_data;

    memcpy(&final_data, self_data, sizeof(SHA512_CTX));
    SHA512_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_object_t
sky__sha384_sha384_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("SHA384");
}

static sky_object_t
sky__sha512_sha512_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("SHA512");
}

static void
sky__sha384_sha384_update(sky_object_t self, sky_object_t string)
{
    SHA384_CTX  *self_data = sky__sha384_sha384_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        SHA384_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}

static void
sky__sha512_sha512_update(sky_object_t self, sky_object_t string)
{
    SHA512_CTX  *self_data = sky__sha512_sha512_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        SHA512_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t
sky__sha384_sha384(sky_object_t string)
{
    sky_object_t    sha384;

    sha384 = sky_object_allocate(sky__sha384_sha384_type);
    if (!sky_object_isnull(string)) {
        sky__sha384_sha384_update(sha384, string);
    }

    return sha384;
}

static sky_object_t
sky__sha512_sha512(sky_object_t string)
{
    sky_object_t    sha512;

    sha512 = sky_object_allocate(sky__sha512_sha512_type);
    if (!sky_object_isnull(string)) {
        sky__sha512_sha512_update(sha512, string);
    }

    return sha512;
}


void
skython_module__sha512_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__sha384_sha384_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("sha384"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(SHA384_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__sha384_sha384_block_size_getter, NULL,
            "digest_size",  NULL, sky__sha384_sha384_digest_size_getter, NULL,
            "name",         NULL, sky__sha384_sha384_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "sha384.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__sha384_sha384_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "sha384.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__sha384_sha384_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "sha384.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__sha384_sha384_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "sha384.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__sha384_sha384_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__sha384_sha384_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "sha384",
            sky_function_createbuiltin(
                    "sha384",
                    "Return a new SHA384 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__sha384_sha384,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__sha512_sha512_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("sha512"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(SHA512_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__sha512_sha512_block_size_getter, NULL,
            "digest_size",  NULL, sky__sha512_sha512_digest_size_getter, NULL,
            "name",         NULL, sky__sha512_sha512_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "sha512.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__sha512_sha512_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "sha512.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__sha512_sha512_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "sha512.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__sha512_sha512_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "sha512.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__sha512_sha512_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__sha512_sha512_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "sha512",
            sky_function_createbuiltin(
                    "sha512",
                    "Return a new SHA512 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__sha512_sha512,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
}
