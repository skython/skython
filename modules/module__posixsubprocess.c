#include "../core/sky_private.h"

SKY_MODULE_EXTERN const char *skython_module__posixsubprocess_doc;

SKY_MODULE_EXTERN void
skython_module__posixsubprocess_initialize(sky_module_t module);


static void
sky__posixsubprocess_cloexec_pipe_cleanup(void *arg)
{
    int *fildes = arg;

    close(fildes[1]);
    close(fildes[0]);
}

static sky_tuple_t
sky__posixsubprocess_cloexec_pipe(void)
{
    int         fd, fildes[2], flags;
    sky_tuple_t result;

    if (-1 == pipe(fildes)) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(fildes,
                       sky__posixsubprocess_cloexec_pipe_cleanup,
                       SKY_ASSET_CLEANUP_ON_ERROR);

        flags = fcntl(fildes[0], F_GETFD, 0);
        if (flags < 0 || fcntl(fildes[0], F_SETFD, flags | FD_CLOEXEC) < 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        flags = fcntl(fildes[1], F_GETFD, 0);
        if (flags < 0 || fcntl(fildes[1], F_SETFD, flags | FD_CLOEXEC) < 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        /* We always want the write end of the pipe to avoid fds 0, 1, and 2 as
         * our child may claim those for stdio connections.
         */
        if (fildes[1] < 3) {
#ifdef F_DUPFD_CLOEXEC
            fd = fcntl(fildes[1], F_DUPFD_CLOEXEC, 3);
#else
            if ((fd = fcntl(fildes[1], F_DUPFD, 3)) >= 0) {
                flags = fcntl(fd, F_GETFD, 0);
                if (flags < 0 || fcntl(fd, F_SETFD, flags | FD_CLOEXEC) < 0) {
                    int save_errno = errno;

                    close(fd);
                    sky_error_raise_errno(sky_OSError, save_errno);
                }
            }
#endif

            close(fildes[1]);
            fildes[1] = fd;
        }

        result = sky_object_build("(ii)", fildes[0], fildes[1]);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__posixsubprocess_cloexec_pipe_doc[] =
"cloexec_pipe() -> (read_end, write_end)\n\n"
"Create a pipe whose ends have the cloexec flag set; write_end will be >= 3.";


static int  sky__posixsubprocess_maxfd = 256;

static char *
sky__posixsubprocess_fsencode(sky_object_t object)
{
    char            *result;
    sky_buffer_t    buffer;

    if (sky_object_isa(object, sky_string_type)) {
        object = sky_codec_encodefilename(object);
    }
    if (!sky_object_isa(object, sky_bytes_type) &&
        !sky_object_isa(object, sky_bytearray_type))
    {
        sky_error_raise_format(sky_TypeError,
                               "expected string or bytes; got %#@",
                               sky_type_name(sky_object_type(object)));
    }

    sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    result = sky_asset_malloc(buffer.len + 1, SKY_ASSET_CLEANUP_ALWAYS);
    memcpy(result, buffer.buf, buffer.len);
    result[buffer.len] = '\0';
    sky_buffer_release(&buffer);

    return result;
}


static char **
sky__posixsubprocess_fsencoded_array(sky_object_t object)
{
    char            **array;
    ssize_t         i, len;
    sky_object_t    item, iter;

    if (!sky_object_isiterable(object)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(object)));
    }

    len = sky_object_len(object);
    array = sky_asset_malloc(sizeof(char *) * (len + 1),
                             SKY_ASSET_CLEANUP_ALWAYS);

    i = 0;
    iter = sky_object_iter(object);
    while ((item = sky_object_next(iter, NULL)) != NULL) {
        if (i >= len) {
            sky_error_raise_string(sky_RuntimeError,
                                   "sequence modified during iteration");
        }
        array[i++] = sky__posixsubprocess_fsencode(item);
    }
    if (i != len) {
        sky_error_raise_string(sky_RuntimeError,
                               "sequence modified during iteration");
    }
    array[len] = NULL;

    return array;
}


static int *
sky__posixsubprocess_fds_to_keep(sky_object_t fds_to_keep)
{
    int             *array, fd, i, len;
    sky_object_t    item;

    if (!sky_sequence_check(fds_to_keep)) {
        sky_error_raise_format(sky_TypeError,
                               "expected sequence for fds_to_keep; got %#@",
                               sky_type_name(sky_object_type(fds_to_keep)));
    }

    fds_to_keep = sky_list_create(fds_to_keep);
    sky_list_sort(fds_to_keep, NULL, SKY_FALSE);

    len = sky_object_len(fds_to_keep);
    array = sky_asset_malloc(sizeof(int) * (len + 1), SKY_ASSET_CLEANUP_ALWAYS);

    i = 0;
    SKY_SEQUENCE_FOREACH(fds_to_keep, item) {
        fd = sky_integer_value(item, 0, INT_MAX, NULL);

        if (i >= len) {
            sky_error_raise_string(sky_RuntimeError,
                                   "sequence modified during iteration");
        }
        array[i++] = fd;
    } SKY_SEQUENCE_FOREACH_END;
    if (i != len) {
        sky_error_raise_string(sky_RuntimeError,
                               "sequence modified during iteration");
    }
    array[len] = -1;

    return array;
}


static void
sky__posixsubprocess_close_fds(int *fds_to_keep)
{
    int empty_fds_to_keep[] = { -1 };

    int fd, i;

    if (!fds_to_keep) {
        fds_to_keep = empty_fds_to_keep;
    }

    for (i = 0; fds_to_keep[i] != -1 && fds_to_keep[i] < 3; ++i);
    for (fd = 3; fd < sky__posixsubprocess_maxfd; ++fd) {
        if (fds_to_keep[i] == fd) {
            ++i;
            continue;
        }
        while (close(fd) == -1 && EINTR == errno);
        if (fds_to_keep[i] != -1 && fd > fds_to_keep[i]) {
            ++i;
        }
    }
}


static void
sky__posixsubprocess_fork_exec_child(
                char **         executable_list,
                char **         args,
                char **         env,
                char *          cwd,
                int             p2cread,
                int             p2cwrite,
                int             c2pread,
                int             c2pwrite,
                int             errread,
                int             errwrite,
                int             errpipe_read,
                int             errpipe_write,
                sky_bool_t      close_fds,
                sky_bool_t      restore_signals,
                sky_bool_t      call_setsid,
                int *           fds_to_keep,
                sky_object_t    preexec_fn)
{
    int         e, i, v;
    const char  *message;
    sky_bool_t  reached_preexec;

    message = NULL;
    reached_preexec = SKY_FALSE;

    if (p2cwrite >= 0 && close(p2cwrite) == -1) {
        goto error;
    }
    if (c2pread >= 0 && close(c2pread) == -1) {
        goto error;
    }
    if (errread >= 0 && close(errread) == -1) {
        goto error;
    }
    if (errpipe_read >= 0 && close(errpipe_read) == -1) {
        goto error;
    }

    if (!c2pwrite && (c2pwrite = dup(c2pwrite) == -1)) {
        goto error;
    }
    if ((!errwrite || 1 == errwrite) && (errwrite = dup(errwrite)) == -1) {
        goto error;
    }

    if (0 == p2cread) {
        if ((v = fcntl(p2cread, F_GETFD)) != -1) {
            fcntl(p2cread, F_SETFD, v & ~FD_CLOEXEC);
        }
    }
    else if (p2cread != -1 && dup2(p2cread, 0) == -1) {
        goto error;
    }
    if (1 == c2pwrite) {
        if ((v = fcntl(c2pwrite, F_GETFD)) != -1) {
            fcntl(c2pwrite, F_SETFD, v & ~FD_CLOEXEC);
        }
    }
    else if (c2pwrite != -1 && dup2(c2pwrite, 1) == -1) {
        goto error;
    }
    if (2 == errwrite) {
        if ((v = fcntl(errwrite, F_GETFD)) != -1) {
            fcntl(errwrite, F_SETFD, v & ~FD_CLOEXEC);
        }
    }
    else if (errwrite != -1 && dup2(errwrite, 2) == -1) {
        goto error;
    }

    if (p2cread > 2 && close(p2cread) == -1) {
        goto error;
    }
    if (c2pwrite > 2 && c2pwrite != p2cread && close(c2pwrite) == -1) {
        goto error;
    }
    if (errwrite != c2pwrite && errwrite != p2cread && errwrite > 2 &&
        close(errwrite) == -1)
    {
        goto error;
    }

    if (cwd && chdir(cwd) == -1) {
        goto error;
    }

    if (restore_signals) {
        sky_signal_restore_defaults();
    }

    if (call_setsid && setsid() == -1) {
        goto error;
    }

    reached_preexec = SKY_TRUE;
    if (!sky_object_isnull(preexec_fn)) {
        SKY_ERROR_TRY {
            sky_object_call(preexec_fn, NULL, NULL);
        } SKY_ERROR_EXCEPT_ANY {
            message = "Exception occurred in preexec_fn";
        } SKY_ERROR_TRY_END;
        if (message) {
            goto error;
        }
    }

    /* Close FDs after executing preexec_fn, which might open FDs */
    if (close_fds) {
        sky__posixsubprocess_close_fds(fds_to_keep);
    }

    e = 0;
    for (i = 0; executable_list[i]; ++i) {
        if (env) {
            execve(executable_list[i], args, env);
        }
        else {
            execv(executable_list[i], args);
        }
        if (!e && errno != ENOENT && errno != ENOTDIR) {
            e = errno;
        }
    }
    errno = e;

error:
    if (errpipe_write != -1) {
        if (!message && (e = errno) != 0) {
            char    buffer[(sizeof(e) * 2) + 1], *outc;

            outc = buffer + sizeof(buffer);
            while (e && outc > buffer) {
                *--outc = sky_ctype_upper_hexdigits[e % 16];
                e /= 16;
            }

            write(errpipe_write, "OSError:", 8);
            write(errpipe_write, outc, buffer + sizeof(buffer) - outc);
            if (!reached_preexec) {
                write(errpipe_write, "noexec", 6);
            }
        }
        else {
            write(errpipe_write, "RuntimeError:0:", 15);
            if (message) {
                write(errpipe_write, message, strlen(message));
            }
        }
    }
}


static pid_t
sky__posixsubprocess_fork_exec(
                sky_object_t    args,
                sky_object_t    executable_list,
                sky_bool_t      close_fds,
                sky_object_t    fds_to_keep,
                sky_object_t    cwd,
                sky_object_t    env,
                int             p2cread,
                int             p2cwrite,
                int             c2pread,
                int             c2pwrite,
                int             errread,
                int             errwrite,
                int             errpipe_read,
                int             errpipe_write,
                sky_bool_t      restore_signals,
                sky_bool_t      call_setsid,
                sky_object_t    preexec_fn)
{
    int     *cfds_to_keep;
    pid_t   pid;
    char    **cargs = NULL, *ccwd = NULL, **cenv = NULL, **cexecutable_list;

    if (close_fds && errpipe_write < 3) {
        sky_error_raise_string(sky_ValueError, "errpipe_write must be >= 3");
    }

    
    SKY_ASSET_BLOCK_BEGIN {
        cfds_to_keep = sky__posixsubprocess_fds_to_keep(fds_to_keep);

        cexecutable_list =
                sky__posixsubprocess_fsencoded_array(executable_list);

        if (!sky_object_isnull(args)) {
            cargs = sky__posixsubprocess_fsencoded_array(args);
        }

        if (!sky_object_isnull(env)) {
            cenv = sky__posixsubprocess_fsencoded_array(env);
        }

        if (!sky_object_isnull(cwd)) {
            ccwd = sky__posixsubprocess_fsencode(cwd);
        }

        if ((pid = sky_system_fork()) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!pid) {
            sky__posixsubprocess_fork_exec_child(cexecutable_list,
                                                 cargs,
                                                 cenv,
                                                 ccwd,
                                                 p2cread,
                                                 p2cwrite,
                                                 c2pread,
                                                 c2pwrite,
                                                 errread,
                                                 errwrite,
                                                 errpipe_read,
                                                 errpipe_write,
                                                 close_fds,
                                                 restore_signals,
                                                 call_setsid,
                                                 cfds_to_keep,
                                                 preexec_fn);
            _exit(255);
        }
    } SKY_ASSET_BLOCK_END;

    return pid;
}

static const char sky__posixsubprocess_fork_exec_doc[] =
"fork_exec(args, executable_list, close_fds, cwd, env,\n"
"         p2cread, p2cwrite, c2pread, c2pwrite,\n"
"         errread, errwrite, errpipe_read, errpipe_write,\n"
"         restore_signals, call_setsid, preexec_fn)\n"
"\n"
"Forks a child process, closes parent file descriptors as appropriate in the\n"
"child and dups the few that are needed before calling exec() in the child\n"
"process.\n"
"\n"
"The preexec_fn, if supplied, will be called immediately before exec.\n"
"WARNING: preexec_fn is NOT SAFE if your application uses threads.\n"
"         It may trigger infrequent, difficult to debug deadlocks.\n"
"\n"
"If an error occurs in the child process before the exec, it is\n"
"serialized and written to the errpipe_write fd per subprocess.py.\n"
"\n"
"Returns: the child process's PID.\n"
"\n"
"Raises: Only on an error in the parent process.\n";


const char *skython_module__posixsubprocess_doc =
"A POSIX helper for the subprocess module.";

void
skython_module__posixsubprocess_initialize(SKY_UNUSED sky_module_t module)
{
    sky__posixsubprocess_maxfd = 256;
#ifdef _SC_OPEN_MAX
    if ((sky__posixsubprocess_maxfd = sysconf(_SC_OPEN_MAX)) == -1) {
        sky__posixsubprocess_maxfd = 256;
    }
#endif

    sky_module_setattr(
            module,
            "cloexec_pipe",
            sky_function_createbuiltin(
                    "cloexec_pipe",
                    sky__posixsubprocess_cloexec_pipe_doc,
                    (sky_native_code_function_t)sky__posixsubprocess_cloexec_pipe,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(
            module,
            "fork_exec",
            sky_function_createbuiltin(
                    "fork_exec",
                    sky__posixsubprocess_fork_exec_doc,
                    (sky_native_code_function_t)sky__posixsubprocess_fork_exec,
                    SKY_DATA_TYPE_PID_T,
                    "args", SKY_DATA_TYPE_OBJECT, NULL,
                    "executable_list", SKY_DATA_TYPE_OBJECT, NULL,
                    "close_fds", SKY_DATA_TYPE_BOOL, NULL,
                    "fds_to_keep", SKY_DATA_TYPE_OBJECT, NULL,
                    "cwd", SKY_DATA_TYPE_OBJECT, NULL,
                    "env", SKY_DATA_TYPE_OBJECT, NULL,
                    "p2cread", SKY_DATA_TYPE_FD, NULL,
                    "p2cwrite", SKY_DATA_TYPE_FD, NULL,
                    "c2pread", SKY_DATA_TYPE_FD, NULL,
                    "c2pwrite", SKY_DATA_TYPE_FD, NULL,
                    "errread", SKY_DATA_TYPE_FD, NULL,
                    "errwrite", SKY_DATA_TYPE_FD, NULL,
                    "errpipe_read", SKY_DATA_TYPE_FD, NULL,
                    "errpipe_write", SKY_DATA_TYPE_FD, NULL,
                    "restore_signals", SKY_DATA_TYPE_BOOL, NULL,
                    "call_setsid", SKY_DATA_TYPE_BOOL, NULL,
                    "preexec_fn", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
}
