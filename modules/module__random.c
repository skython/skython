/* Adapted for use with Skython from CPython 3.3.2 */

/* Random objects */

/* ------------------------------------------------------------------
   The code in this module was based on a download from:
      http://www.math.keio.ac.jp/~matumoto/MT2002/emt19937ar.html

   It was modified in 2002 by Raymond Hettinger as follows:

    * the principal computational lines untouched.

    * renamed genrand_res53() to random_random() and wrapped
      in python calling/return code.

    * genrand_int32() and the helper functions, init_genrand()
      and init_by_array(), were declared static, wrapped in
      Python calling/return code.  also, their global data
      references were replaced with structure references.

    * unused functions from the original were deleted.
      new, original C python code was added to implement the
      Random() interface.

   The following are the verbatim comments from the original code:

   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
    products derived from this software without specific prior written
    permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.keio.ac.jp/matumoto/emt.html
   email: matumoto@math.keio.ac.jp
*/

/* ---------------------------------------------------------------*/

#include "../core/skython.h"
#include <time.h>               /* for seeding to current time */


SKY_MODULE_EXTERN const char *skython_module__random_doc;

SKY_MODULE_EXTERN void
skython_module__random_initialize(sky_module_t module);


/* Period parameters -- These are all magic.  Don't change. */
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

static sky_type_t sky__random_Random_type;

static const char sky__random_Random_type_doc[] =
"Random() -> create a random number generator with its own internal state.";

typedef struct sky__random_Random_data_s {
    uint32_t                            state[N];
    int                                 index;
} sky__random_Random_data_t;

SKY_EXTERN_INLINE sky__random_Random_data_t *
sky__random_Random_data(sky_object_t object)
{
    return sky_object_data(object, sky__random_Random_type);
}


/* Random methods */

/* generates a random number on [0,0xffffffff]-interval */
static uint32_t
genrand_int32(sky__random_Random_data_t *self_data)
{
    /* mag01[x] = x * MATRIX_A  for x=0,1 */
    static uint32_t mag01[2] = { 0x0UL, MATRIX_A };

    uint32_t    *mt, y;

    mt = self_data->state;
    if (self_data->index >= N) { /* generate N words at one time */
        int kk;

        for (kk = 0; kk < N - M; ++kk) {
            y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
            mt[kk] = mt[kk + M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        for (; kk < N - 1; ++kk) {
            y = (mt[kk] & UPPER_MASK) | (mt[kk + 1] & LOWER_MASK);
            mt[kk] = mt[kk + (M - N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }
        y = (mt[N - 1] & UPPER_MASK) | (mt[0] & LOWER_MASK);
        mt[N - 1] = mt[M - 1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        self_data->index = 0;
    }

    y = mt[self_data->index++];
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);
    return y;
}

/* random_random is the function named genrand_res53 in the original code;
 * generates a random number on [0,1) with 53-bit resolution; note that
 * 9007199254740992 == 2**53; I assume they're spelling "/2**53" as
 * multiply-by-reciprocal in the (likely vain) hope that the compiler will
 * optimize the division away at compile-time.  67108864 is 2**26.  In
 * effect, a contains 27 random bits shifted left 26, and b fills in the
 * lower 26 bits of the 53-bit numerator.
 * The orginal code credited Isaku Wada for this algorithm, 2002/01/09.
 */
static double
sky__random_Random_random(sky_object_t self)
{
    sky__random_Random_data_t   *self_data = sky__random_Random_data(self);

    uint32_t    a, b;
    
    a = genrand_int32(self_data) >> 5;
    b = genrand_int32(self_data) >> 6;

    return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0);
}

/* initializes mt[N] with a seed */
static void
init_genrand(sky__random_Random_data_t *self_data, uint32_t s)
{
    int         mti;
    uint32_t    *mt;

    mt = self_data->state;
    mt[0]= s & 0xffffffffUL;
    for (mti = 1; mti < N; ++mti) {
        mt[mti] = (1812433253UL * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti);
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                                */
        /* 2002/01/09 modified by Makoto Matsumoto                     */
        mt[mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
    self_data->index = mti;
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
static void
init_by_array(
                sky__random_Random_data_t * self_data,
                uint32_t                    init_key[],
                ssize_t                     key_length)
{
    uint32_t        *mt;
    unsigned int    i, j, k;    /* was signed in the original code. RDH 12/16/2002 */

    mt = self_data->state;
    init_genrand(self_data, 19650218UL);
    i = 1;
    j = 0;
    k = (N > key_length ? N : key_length);
    for (; k; --k) {
        mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >> 30)) * 1664525UL))
                 + init_key[j] + j; /* non linear */
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        j++;
        if (i >= N) {
            mt[0] = mt[N - 1];
            i = 1;
        }
        if (j >= key_length) {
            j = 0;
        }
    }
    for (k = N - 1; k; --k) {
        /* non linear */
        mt[i] = (mt[i] ^ ((mt[i - 1] ^ (mt[i - 1] >> 30)) * 1566083941UL)) - i;
        mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        ++i;
        if (i >= N) {
            mt[0] = mt[N - 1];
            i = 1;
        }
    }

    mt[0] = 0x80000000UL; /* MSB is 1; assuring non-zero initial array */
}

/*
 * The rest is Python-specific code, neither part of, nor derived from, the
 * Twister download.
 */

static void
sky__random_Random_seed(sky_object_t self, sky_object_t n)
{
    sky__random_Random_data_t   *self_data = sky__random_Random_data(self);

    size_t          keymax, keyused;
    uint32_t        chunk, *key;
    sky_integer_t   masklower, thirtytwo;

    if (sky_object_isnull(n)) {
        time_t now;

        time(&now);
        init_genrand(self_data, now);
    }
    /* This algorithm relies on the number being unsigned.
     * So: if the arg is a PyLong, use its absolute value.
     * Otherwise use its hash value, cast to unsigned.
     */
    if (sky_object_isa(n, sky_integer_type)) {
        n = sky_number_absolute(n);
    }
    else {
        n = sky_integer_createfromunsigned(sky_object_hash(n));
    }

    masklower = sky_integer_createfromunsigned(0xffffffffU);
    thirtytwo = sky_integer_create(32L);

    SKY_ASSET_BLOCK_BEGIN {
        /* Now split n into 32-bit chunks, from the right.  Each piece is
         * stored into key, which has a capacity of keymax chunks, of which
         * keyused are filled.  Alas, the repeated shifting makes this a
         * quadratic-time algorithm; we'd really like to use
         * _PyLong_AsByteArray here, but then we'd have to break into the
         * long representation to figure out how big an array was needed
         * in advance.
         */
        keymax = 8;         /* arbitrary; grows later if needed */
        keyused = 0;
        key = sky_asset_malloc(keymax * sizeof(*key), SKY_ASSET_CLEANUP_ALWAYS);

        while (sky_object_bool(n)) {
            chunk = sky_integer_value(sky_number_and(n, masklower),
                                      0,
                                      UINT32_MAX,
                                      NULL);
            n = sky_number_rshift(n, thirtytwo);
            if (keyused >= keymax) {
                size_t  bigger = keymax << 1;

                if ((bigger >> 1) != keymax ||
                    bigger > SSIZE_MAX / sizeof(*key))
                {
                    sky_error_raise_string(sky_MemoryError, "list too big");
                }
                key = sky_asset_realloc(key,
                                        bigger * sizeof(*key),
                                        SKY_ASSET_CLEANUP_ALWAYS,
                                        SKY_ASSET_UPDATE_FIRST_CURRENT);
                keymax = bigger;
            }
            sky_error_validate_debug(keyused < keymax);
            key[keyused++] = chunk;
        }

        if (keyused == 0) {
            key[keyused++] = 0UL;
        }
        init_by_array(self_data, key, keyused);
    } SKY_ASSET_BLOCK_END;
}

static sky_tuple_t
sky__random_Random_getstate(sky_object_t self)
{
    sky__random_Random_data_t   *self_data = sky__random_Random_data(self);

    ssize_t         i;
    sky_tuple_t     state;
    sky_object_t    *objects;

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_calloc(N + 1,
                                   sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);
        for (i = 0; i < N; ++i) {
            objects[i] = sky_integer_createfromunsigned(self_data->state[i]);
        }
        objects[N] = sky_integer_create(self_data->index);
        state = sky_tuple_createwitharray(N + 1, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return state;
}

static void
sky__random_Random_setstate(sky_object_t self, sky_tuple_t state)
{
    sky__random_Random_data_t   *self_data = sky__random_Random_data(self);

    ssize_t i;

    if (sky_tuple_len(state) != N + 1) {
        sky_error_raise_string(sky_ValueError,
                               "state vector is the wrong size");
    }

    for (i = 0; i < N; ++i) {
        self_data->state[i] = sky_integer_value(sky_tuple_get(state, i),
                                                0,
                                                UINT32_MAX,
                                                NULL);
    }

    self_data->index = sky_integer_value(sky_tuple_get(state, N), 0, N, NULL);
}

static sky_integer_t
sky__random_Random_getrandbits(sky_object_t self, ssize_t k)
{
    sky__random_Random_data_t   *self_data = sky__random_Random_data(self);

    size_t          bytes, i;
    uint32_t        r;
    sky_integer_t   result;
    unsigned char   *bytearray;

    if (k <= 0) {
        sky_error_raise_string(sky_ValueError,
                               "number of bits must be greater than zero");
    }

    SKY_ASSET_BLOCK_BEGIN {
        bytes = ((k - 1) / 32 + 1) * 4;
        bytearray = (unsigned char *)sky_asset_malloc(bytes,
                                                      SKY_ASSET_CLEANUP_ALWAYS);

        /* Fill-out whole words, byte-by-byte to avoid endianness issues */
        for (i = 0; i < bytes; i += 4, k -= 32) {
            r = genrand_int32(self_data);
            if (k < 32) {
                r >>= (32 - k);
            }
            bytearray[i + 0] = (unsigned char)r;
            bytearray[i + 1] = (unsigned char)(r >> 8);
            bytearray[i + 2] = (unsigned char)(r >> 16);
            bytearray[i + 3] = (unsigned char)(r >> 24);
        }

        /* little endian order to match bytearray assignment order */
        result = sky_integer_createfrombytes(bytearray,
                                             bytes,
                                             SKY_TRUE,
                                             SKY_FALSE);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static void
sky__random_Random_init(sky_object_t self, sky_object_t n)
{
    sky__random_Random_seed(self, n);
}


const char *skython_module__random_doc =
"Module implements the Mersenne Twister random number generator.";


void
skython_module__random_initialize(sky_module_t module)
{
    sky_type_t  type;

    type = sky_type_createbuiltin(SKY_STRING_LITERAL("Random"),
                                   sky__random_Random_type_doc,
                                   0,
                                   sizeof(sky__random_Random_data_t),
                                   0,
                                   NULL,
                                   NULL);

    sky_type_setattr_builtin(
            type,
            "getrandbits",
            sky_function_createbuiltin(
                    "Random.getrandbits",
                    "getrandbits(k) -> x.  Generates a long int with k random bits.",
                    (sky_native_code_function_t)sky__random_Random_getrandbits,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "k", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "getstate",
            sky_function_createbuiltin(
                    "Random.getstate",
                    "getstate() -> tuple containing the current state.",
                    (sky_native_code_function_t)sky__random_Random_getstate,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "random",
            sky_function_createbuiltin(
                    "Random.random",
                    "random() -> x in the interval [0, 1].",
                    (sky_native_code_function_t)sky__random_Random_random,
                    SKY_DATA_TYPE_DOUBLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "seed",
            sky_function_createbuiltin(
                    "Random.seed",
                    "seed([n]) -> None.  Defaults to current time.",
                    (sky_native_code_function_t)sky__random_Random_seed,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "setstate",
            sky_function_createbuiltin(
                    "Random.setstate",
                    "setstate(state) -> None.  Restores generator state.",
                    (sky_native_code_function_t)sky__random_Random_setstate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "state", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__random_Random_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "n", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__random_Random_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "Random", type);
}
