/*
 * Secret Labs' Regular Expression Engine
 *
 * regular expression matching engine
 *
 * Copyright (c) 1997-2001 by Secret Labs AB.  All rights reserved.
 *
 * See the _sre.c file for information on usage and redistribution.
 */

#ifndef __SKYTHON_MODULES_MODULE__SRE_H__
#define __SKYTHON_MODULES_MODULE__SRE_H__ 1


#include "sre_constants.h"


/* size of a code word (must be unsigned short or larger, and
   large enough to hold a UCS4 character) */
#define SRE_CODE uint32_t
#if SIZE_MAX > UINT32_MAX
#   define SRE_MAXREPEAT (~(SRE_CODE)0)
#else
#   define SRE_MAXREPEAT ((SRE_CODE)SSIZE_MAX)
#endif


extern sky_type_t   sky__sre_match_type;
extern sky_type_t   sky__sre_pattern_type;
extern sky_type_t   sky__sre_scanner_type;

typedef struct sky__sre_match_s *sky__sre_match_t;
typedef struct sky__sre_pattern_s *sky__sre_pattern_t;
typedef struct sky__sre_scanner_s *sky__sre_scanner_t;


typedef struct sky__sre_pattern_data_s {
    ssize_t groups; /* must be first! */
    sky_object_t groupindex;
    sky_object_t indexgroup;
    /* compatibility */
    sky__sre_pattern_t pattern; /* pattern source (or None) */
    int flags; /* flags used when compiling pattern source */
    int logical_charsize; /* pattern charsize (or -1) */
    int charsize;
    sky_buffer_t view;
    sky_string_data_t tagged_string_data;
    /* pattern code */
    ssize_t codesize;
    SRE_CODE *code;
} sky__sre_pattern_data_t;

SKY_EXTERN_INLINE sky__sre_pattern_data_t *
sky__sre_pattern_data(sky_object_t self)
{
    return sky_object_data(self, sky__sre_pattern_type);
}


typedef struct sky__sre_match_data_s {
    sky_object_t string; /* link to the target string (must be first) */
    sky_object_t regs; /* cached list of matching spans */
    sky__sre_pattern_t pattern; /* link to the regex (pattern) object */
    ssize_t pos, endpos; /* current target slice */
    ssize_t lastindex; /* last index marker seen by the engine (-1 if none) */
    ssize_t groups; /* number of groups (start/end marks) */
    ssize_t *mark;
} sky__sre_match_data_t;

SKY_EXTERN_INLINE sky__sre_match_data_t *
sky__sre_match_data(sky_object_t self)
{
    return sky_object_data(self, sky__sre_match_type);
}

typedef unsigned int (*SRE_TOLOWER_HOOK)(unsigned int ch);

/* FIXME: <fl> shouldn't be a constant, really... */
#define SRE_MARK_SIZE 200

typedef struct SRE_REPEAT_T {
    ssize_t count;
    SRE_CODE* pattern; /* points to REPEAT operator arguments */
    void* last_ptr; /* helper to check for infinite loops */
    struct SRE_REPEAT_T *prev; /* points to previous repeat context */
} SRE_REPEAT;

typedef struct {
    /* string pointers */
    void* ptr; /* current position (also end of current slice) */
    void* beginning; /* start of original string */
    void* start; /* start of current slice */
    void* end; /* end of original string */
    /* attributes for the match object */
    sky_object_t string;
    sky_string_data_t tagged_string_data;
    ssize_t pos, endpos;
    /* character size */
    int logical_charsize; /* kind of thing: 1 - bytes, 2/4 - unicode */
    int charsize;
    /* registers */
    ssize_t lastindex;
    ssize_t lastmark;
    void* mark[SRE_MARK_SIZE];
    /* dynamically allocated stuff */
    char* data_stack;
    size_t data_stack_size;
    size_t data_stack_base;
    sky_buffer_t buffer;
    /* current repeat context */
    SRE_REPEAT *repeat;
    /* hooks */
    SRE_TOLOWER_HOOK lower;
} SRE_STATE;


typedef struct sky__sre_scanner_data_s {
    sky__sre_pattern_t                  pattern;
    SRE_STATE                           state;
} sky__sre_scanner_data_t;

SKY_EXTERN_INLINE sky__sre_scanner_data_t *
sky__sre_scanner_data(sky_object_t object)
{
    return sky_object_data(object, sky__sre_scanner_type);
}


#endif  /* __SKYTHON_MODULES_MODULE__SRE_H__ */
