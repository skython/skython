#include "../core/skython.h"


static sky_type_t sky__functools_partial_type;

typedef struct sky__functools_partial_s *sky__functools_partial_t;

static const char sky__functools_partial_doc[] =
"partial(func, *args, **keywords) - new function with partial application\n"
"of the given arguments and keywords.\n";

typedef struct sky__functools_partial_data_s {
    sky_object_t                        function;
    sky_tuple_t                         args;
    sky_dict_t                          kws;
} sky__functools_partial_data_t;

SKY_EXTERN_INLINE sky__functools_partial_data_t *
sky__functools_partial_data(sky_object_t object)
{
    return sky_object_data(object, sky__functools_partial_type);
}

static void
sky__functools_partial_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__functools_partial_data_t   *self_data = data;

    sky_object_visit(self_data->function, visit_data);
    sky_object_visit(self_data->args, visit_data);
    sky_object_visit(self_data->kws, visit_data);
}

static sky_object_t
sky__functools_partial_call(
                sky__functools_partial_t    self,
                sky_tuple_t                 args,
                sky_dict_t                  kws)
{
    sky__functools_partial_data_t   *self_data;

    self_data = sky__functools_partial_data(self);
    if (!args || !sky_tuple_len(args)) {
        args = self_data->args;
    }
    else if (self_data->args && sky_tuple_len(self_data->args)) {
        args = sky_tuple_add(self_data->args, args);
    }
    if (!kws || !sky_dict_len(kws)) {
        kws = self_data->kws;
    }
    else if (self_data->kws && sky_dict_len(self_data->kws)) {
        kws = sky_dict_copy(kws);
        sky_dict_merge(kws, self_data->kws, SKY_FALSE);
    }

    return sky_object_call(self_data->function, args, kws);
}

static void
sky__functools_partial_init(
                sky__functools_partial_t    self,
                sky_tuple_t                 args,
                sky_dict_t                  kws)
{
    sky_object_t                    function;
    sky__functools_partial_data_t   *self_data;

    self_data = sky__functools_partial_data(self);
    if (!args || sky_tuple_len(args) < 1) {
        sky_error_raise_string(sky_TypeError,
                               "type 'partial' takes at least one argument");
    }
    function = sky_tuple_get(args, 0);
    if (!sky_object_callable(function)) {
        sky_error_raise_string(sky_TypeError,
                               "the first argument must be callable");
    }
    args = sky_tuple_slice(args, 1, SSIZE_MAX, 1);
    if (sky_object_isnull(kws)) {
        kws = NULL;
    }
    else {
        kws = sky_dict_copy(kws);
    }

    sky_object_gc_set(&(self_data->function), function, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->args)), args, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->kws)), kws, self);
}

static sky_object_t
sky__functools_partial_reduce(sky__functools_partial_t self)
{
    sky__functools_partial_data_t   *self_data;

    self_data = sky__functools_partial_data(self);
    return sky_object_build("(O(O)(OOOO))",
                            sky_object_type(self),
                            self_data->function,
                            self_data->function,
                            self_data->args,
                            self_data->kws,
                            sky_object_dict(self));
}

static sky_string_t
sky__functools_partial_repr(sky__functools_partial_t self)
{
    sky_list_t                      list;
    sky_object_t                    key, value;
    sky_string_builder_t            builder;
    sky__functools_partial_data_t   *self_data;

    self_data = sky__functools_partial_data(self);
    list = sky_list_createwithcapacity(
                    (self_data->args ? sky_object_len(self_data->args) : 0) +
                    (self_data->kws ? sky_object_len(self_data->kws) : 0) +
                    1);

    sky_list_append(list, sky_object_repr(self_data->function));

    if (self_data->args) {
        SKY_SEQUENCE_FOREACH(self_data->args, value) {
            sky_list_append(list, sky_object_repr(value));
        } SKY_SEQUENCE_FOREACH_END;
    }
    if (self_data->kws) {
        SKY_SEQUENCE_FOREACH(self_data->kws, key) {
            value = sky_dict_getitem(self_data->kws, key);
            sky_list_append(list,
                            sky_string_createfromformat("%@=%#@",
                                                        key,
                                                        value));
        } SKY_SEQUENCE_FOREACH_END;
    }

    builder = sky_string_builder_create();
    sky_string_builder_append(builder, sky_type_name(sky_object_type(self)));
    sky_string_builder_appendcodepoint(builder, '(', 1);
    sky_string_builder_appendjoined(builder, list, SKY_STRING_LITERAL(", "));
    sky_string_builder_appendcodepoint(builder, ')', 1);
    return sky_string_builder_finalize(builder);
}

static void
sky__functools_partial_setstate(
                sky__functools_partial_t    self,
                sky_object_t                state)
{
    sky__functools_partial_data_t   *self_data;

    self_data = sky__functools_partial_data(self);
    if (!sky_object_isa(state, sky_tuple_type) || sky_tuple_len(state) != 4) {
        sky_error_raise_string(sky_TypeError,
                               "tuple of len 4 expected");
    }
    sky_object_gc_set(&(self_data->function),
                      sky_tuple_get(state, 0),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->args)),
                      sky_tuple_get(state, 1),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->kws)),
                      sky_tuple_get(state, 2),
                      self);
    sky_object_setdict(self, sky_tuple_get(state, 3));
}


static sky_type_t sky__functools_keyobject_type;

typedef struct sky__functools_keyobject_s *sky__functools_keyobject_t;

typedef struct sky__functools_keyobject_data_s {
    sky_object_t                        cmp;
    sky_object_t                        object;
} sky__functools_keyobject_data_t;

SKY_EXTERN_INLINE sky__functools_keyobject_data_t *
sky__functools_keyobject_data(sky_object_t object)
{
    return sky_object_data(object, sky__functools_keyobject_type);
}

static void
sky__functools_keyobject_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__functools_keyobject_data_t *self_data = data;

    sky_object_visit(self_data->cmp, visit_data);
    sky_object_visit(self_data->object, visit_data);
}


static sky_object_t
sky__functools_keyobject_call(
                sky__functools_keyobject_t  self,
                sky_object_t                object)
{
    sky__functools_keyobject_data_t *self_data =
                                    sky__functools_keyobject_data(self);

    sky__functools_keyobject_t      keyobject;
    sky__functools_keyobject_data_t *keyobject_data;

    keyobject = sky_object_allocate(sky__functools_keyobject_type);
    keyobject_data = sky__functools_keyobject_data(keyobject);
    sky_object_gc_set(&(keyobject_data->cmp), self_data->cmp, keyobject);
    sky_object_gc_set(&(keyobject_data->object), object, keyobject);

    return keyobject;
}


static sky_object_t
sky__functools_keyobject_compare(
                sky__functools_keyobject_t  self,
                sky_object_t                other,
                sky_compare_op_t            compare_op)
{
    sky_object_t                    result;
    sky__functools_keyobject_data_t *other_data, *self_data;

    if (!sky_object_isa(other, sky__functools_keyobject_type)) {
        return sky_NotImplemented;
    }

    self_data = sky__functools_keyobject_data(self);
    other_data = sky__functools_keyobject_data(other);
    if (!self_data->object || !other_data->object) {
        sky_error_raise_string(sky_AttributeError, "object");
    }

    result = sky_object_call(self_data->cmp,
                             sky_tuple_pack(2, self_data->object,
                                               other_data->object),
                             NULL);

    if (sky_object_compare(result, sky_integer_zero, compare_op)) {
        return sky_True;
    }
    return sky_False;
}

static sky_object_t
sky__functools_keyobject_eq(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_EQUAL);
}

static sky_object_t
sky__functools_keyobject_ge(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_GREATER_EQUAL);
}

static sky_object_t
sky__functools_keyobject_gt(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_GREATER);
}

static sky_object_t
sky__functools_keyobject_le(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_LESS_EQUAL);
}

static sky_object_t
sky__functools_keyobject_lt(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_LESS);
}

static sky_object_t
sky__functools_keyobject_ne(
                sky__functools_keyobject_t  self,
                sky_object_t                other)
{
    return sky__functools_keyobject_compare(self,
                                            other,
                                            SKY_COMPARE_OP_NOT_EQUAL);
}

static sky_object_t
sky__functools_cmp_to_key(sky_object_t mycmp)
{
    sky__functools_keyobject_t          object;
    sky__functools_keyobject_data_t *   object_data;

    object = sky_object_allocate(sky__functools_keyobject_type);
    object_data = sky__functools_keyobject_data(object);
    sky_object_gc_set(&(object_data->cmp), mycmp, object);

    return object;
}

static const char sky__functools_cmp_to_key_doc[] =
"Convert a cmp= function into a key= function.";


static sky_object_t
sky__functools_reduce(
                sky_object_t    function,
                sky_object_t    sequence,
                sky_object_t    initial)
{
    sky_object_t    iter, object, result;

    result = initial;
    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, object) {
            if (!result) {
                result = object;
            }
            else {
                result = sky_object_call(function,
                                         sky_tuple_pack(2, result, object),
                                         NULL);
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(sequence);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            if (!result) {
                result = object;
            }
            else {
                result = sky_object_call(function,
                                         sky_tuple_pack(2, result, object),
                                         NULL);
            }
        }
    }

    if (!result) {
        sky_error_raise_string(
                sky_TypeError,
                "reduce() of empty sequence with no initial value");
    }

    return result;
}

static const char sky__functools_reduce_doc[] =
"reduce(function, sequence[, initial]) -> value\n"
"\n"
"Apply a function of two arguments cumulatively to the items of a sequence,\n"
"from left to right, so as to reduce the sequence to a single value.\n"
"For example, reduce(lambda x, y: x+y, [1, 2, 3, 4, 5]) calculates\n"
"((((1+2)+3)+4)+5).  If initial is present, it is placed before the items\n"
"of the sequence in the calculation, and serves as a default when the\n"
"sequence is empty.";


void
skython_module__functools_initialize(sky_module_t module)
{
    static const char doc[] = "Tools that operate on functions.";

    sky_type_t          type;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__functools_keyobject_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("KeyWrapper"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky__functools_keyobject_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_member(
            type,
            "obj",
            "Value wrapped by a key function.",
            offsetof(sky__functools_keyobject_data_t, object),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_DELETEABLE |
            SKY_MEMBER_FLAG_ALLOW_NONE |
            SKY_MEMBER_FLAG_NONE_IS_NULL);

    sky_type_setmethodslotwithparameters(
            type,
            "__call__",
            (sky_native_code_function_t)sky__functools_keyobject_call,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "obj", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(type,
            "__lt__", sky__functools_keyobject_lt,
            "__le__", sky__functools_keyobject_le,
            "__eq__", sky__functools_keyobject_eq,
            "__ne__", sky__functools_keyobject_ne,
            "__gt__", sky__functools_keyobject_gt,
            "__ge__", sky__functools_keyobject_ge,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__functools_keyobject_type),
            type,
            SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__functools_partial_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("partial"),
                                   sky__functools_partial_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   sizeof(sky__functools_partial_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmembers(type,
            "func",
                    "function object to use in future partial calls",
                    offsetof(sky__functools_partial_data_t, function),
                    SKY_DATA_TYPE_OBJECT,
                    SKY_MEMBER_FLAG_READONLY,
            "args",
                    "tuple of arguments to future partial calls",
                    offsetof(sky__functools_partial_data_t, args),
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    SKY_MEMBER_FLAG_READONLY,
            "keywords",
                    "dictionary of keyword arguments to future partial calls",
                    offsetof(sky__functools_partial_data_t, kws),
                    SKY_DATA_TYPE_OBJECT_DICT,
                    SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setmethodslots(type,
            "__init__", sky__functools_partial_init,
            "__repr__", sky__functools_partial_repr,
            "__call__", sky__functools_partial_call,
            "__reduce__", sky__functools_partial_reduce,
            "__setstate__", sky__functools_partial_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__functools_partial_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "partial", type);


    sky_module_setattr(
            module,
            "cmp_to_key",
            sky_function_createbuiltin(
                    "cmp_to_key",
                    sky__functools_cmp_to_key_doc,
                    (sky_native_code_function_t)sky__functools_cmp_to_key,
                    SKY_DATA_TYPE_OBJECT,
                    "mycmp", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "reduce",
            sky_function_createbuiltin(
                    "reduce",
                    sky__functools_reduce_doc,
                    (sky_native_code_function_t)sky__functools_reduce,
                    SKY_DATA_TYPE_OBJECT,
                    "function", SKY_DATA_TYPE_OBJECT, NULL,
                    "sequence", SKY_DATA_TYPE_OBJECT, NULL,
                    "initial", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
}
