#include "module__ssl.h"


/* Wrap error strings with filename and line # to match CPython's errors */
#define STRINGIFY1(x)       #x
#define STRINGIFY2(x)       STRINGIFY1(x)
#define ERRSTR1(x, y, z)    (x ":" y ":" z)
#define ERRSTR(x)           ERRSTR1("_ssl.c", STRINGIFY2(__LINE__), x)


typedef struct sky__ssl_openssl_error_code_s {
    const char *                        mnemonic;
    int                                 library;
    int                                 reason;
} sky__ssl_openssl_error_code_t;

typedef struct sky__ssl_openssl_library_code_s {
    const char *                        library;
    int                                 code;
} sky__ssl_openssl_library_code_t;

#include "module__ssl_openssl.h"

static sky_dict_t   sky__ssl_openssl_err_codes_to_names = NULL;
static sky_dict_t   sky__ssl_openssl_err_names_to_codes = NULL;
static sky_dict_t   sky__ssl_openssl_lib_codes_to_names = NULL;


/* This is needed for assets because OPENSSL_free is a macro. */
static void
sky__ssl_openssl_OPENSSL_free(void *pointer)
{
    OPENSSL_free(pointer);
}

static void SKY_NORETURN
sky__ssl_openssl_raise_(
                sky_type_t          type,
                const char *        message,
                int                 code,
                sky__ssl_error_t    error,
    SKY_UNUSED  const char *        filename,
                int                 lineno)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t    exception, lib, reason;
        sky_string_t    string;

        sky_asset_save(NULL,
                       (sky_free_t)ERR_clear_error,
                       SKY_ASSET_CLEANUP_ALWAYS);

        lib = reason = NULL;
        if (code) {
            sky_object_t    key;
            
            key = sky_object_build("(ii)", ERR_GET_LIB(code),
                                           ERR_GET_REASON(code));
            reason = sky_dict_get(sky__ssl_openssl_err_codes_to_names,
                                  key,
                                  NULL);

            key = sky_integer_create(ERR_GET_LIB(code));
            lib = sky_dict_get(sky__ssl_openssl_lib_codes_to_names,
                               key,
                               NULL);
            if (!message) {
                message = ERR_reason_error_string(code);
            }
        }
        if (!message) {
            message = "unknown error";
        }

        if (reason && lib) {
            string = sky_string_createfromformat("[%@: %@] %s (_ssl.c:%d)",
                                                 lib, reason, message, lineno);
        }
        else if (lib) {
            string = sky_string_createfromformat("[%@] %s (_ssl.c:%d)",
                                                 lib, message, lineno);
        }
        else {
            string = sky_string_createfromformat("%s (_ssl.c:%d)",
                                                 message, lineno);
        }

        exception = sky_object_call(type,
                                    sky_object_build("(iO)", error, string),
                                    NULL);
        sky_object_setattr(exception,
                           SKY_STRING_LITERAL("reason"),
                           (reason ? reason : sky_None));
        sky_object_setattr(exception,
                           SKY_STRING_LITERAL("library"),
                           (lib ? lib : sky_None));

        sky_error_raise_object(type, exception);
    } SKY_ASSET_BLOCK_END;
}

#define sky__ssl_openssl_raise(message)                             \
    sky__ssl_openssl_raise_(sky__ssl_SSLError,                      \
                            message,                                \
                            (message ? 0 : ERR_peek_last_error()),  \
                            (message ? 0 : ERR_peek_last_error()),  \
                            __FILE__,                               \
                            __LINE__)


static long
sky__ssl_openssl_bio_ctrl(
                BIO *   bio,
                int     cmd,
                long    larg,
    SKY_UNUSED  void *  parg)
{
    switch (cmd) {
        case BIO_CTRL_GET_CLOSE:
            return bio->shutdown;

        case BIO_CTRL_SET_CLOSE:
            bio->shutdown = (int)larg;
            return 1;

        case BIO_CTRL_DUP:
        case BIO_CTRL_FLUSH:
            return 1;
    }

    return 0;
}

static int
sky__ssl_openssl_bio_destroy(BIO *bio)
{
    sky__ssl__SSLSocket_data_t  *socket_data =
                                sky__ssl__SSLSocket_data(bio->ptr);

    if (socket_data->error) {
        sky_error_record_release(socket_data->error);
        socket_data->error = NULL;
    }

    if (bio->shutdown && bio->init) {
        bio->ptr = NULL;
        bio->init = 0;

        SKY_ERROR_TRY {
            sky_socket_close(socket_data->socket);
        } SKY_ERROR_EXCEPT_ANY {
            socket_data->error = sky_error_current();
            sky_error_record_retain(socket_data->error);
        } SKY_ERROR_TRY_END;
    }

    return 1;
}

static int
sky__ssl_openssl_bio_read(BIO *bio, char *bytes, int nbytes)
{
    sky__ssl__SSLSocket_data_t  *socket_data =
                                sky__ssl__SSLSocket_data(bio->ptr);

    int result;

    sky_error_validate_debug(NULL == socket_data->error);
    if (socket_data->error) {
        sky_error_record_release(socket_data->error);
        socket_data->error = NULL;
    }

    SKY_ERROR_TRY {
        BIO_clear_retry_flags(bio);
        result = sky_socket_recv_native(socket_data->socket, bytes, nbytes, 0);
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_record_t  *error = sky_error_current();

        errno = 0;
        if (sky_type_issubtype(error->type, sky_OSError)) {
            errno = sky_OSError_errno(error->value);
        }
        if (!errno) {
            socket_data->error = error;
            sky_error_record_retain(socket_data->error);
        }
        else if (BIO_sock_non_fatal_error(errno)) {
            BIO_set_retry_read(bio);
        }
        result = -1;
    } SKY_ERROR_TRY_END;

    return result;
}

static int
sky__ssl_openssl_bio_write(BIO *bio, const char *bytes, int nbytes)
{
    sky__ssl__SSLSocket_data_t  *socket_data =
                                sky__ssl__SSLSocket_data(bio->ptr);

    int result;

    sky_error_validate_debug(NULL == socket_data->error);
    if (socket_data->error) {
        sky_error_record_release(socket_data->error);
        socket_data->error = NULL;
    }

    SKY_ERROR_TRY {
        BIO_clear_retry_flags(bio);
        result = sky_socket_send_native(socket_data->socket, bytes, nbytes, 0);
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_record_t  *error = sky_error_current();

        errno = 0;
        if (sky_type_issubtype(error->type, sky_OSError)) {
            errno = sky_OSError_errno(error->value);
        }
        if (!errno) {
            socket_data->error = error;
            sky_error_record_retain(socket_data->error);
        }
        else if (BIO_sock_non_fatal_error(errno)) {
            BIO_set_retry_read(bio);
        }
        result = -1;
    } SKY_ERROR_TRY_END;

    return result;
}

static BIO_METHOD *
sky__ssl_openssl_bio_methods(void)
{
    static BIO_METHOD methods = {
        BIO_TYPE_SOCKET,
        "_SSLSocket",
        sky__ssl_openssl_bio_write,
        sky__ssl_openssl_bio_read,
        NULL,                                       /* puts */
        NULL,                                       /* gets */
        sky__ssl_openssl_bio_ctrl,
        NULL,                                       /* create */
        sky__ssl_openssl_bio_destroy,
        NULL,                                       /* callback_ctrl */
    };

    return &methods;
}


static sky_tuple_t
sky__ssl_openssl_attribute_tuple(ASN1_OBJECT *name, ASN1_STRING *value)
{
    sky_tuple_t tuple;

    SKY_ASSET_BLOCK_BEGIN {
        int             buflen;
        char            namebuf[256];
        sky_string_t    name_string, value_string;
        unsigned char   *valuebuf;

        if ((buflen = OBJ_obj2txt(namebuf, sizeof(namebuf), name, 0)) < 0) {
            sky__ssl_openssl_raise(NULL);
        }
        name_string = sky_string_createfrombytes(namebuf, buflen, NULL, NULL);

        valuebuf = NULL;
        if ((buflen = ASN1_STRING_to_UTF8(&valuebuf, value)) < 0) {
            sky__ssl_openssl_raise(NULL);
        }
        sky_asset_save(valuebuf,
                       sky__ssl_openssl_OPENSSL_free,
                       SKY_ASSET_CLEANUP_ALWAYS);
        value_string = sky_string_createfrombytes(valuebuf, buflen, NULL, NULL);

        tuple = sky_tuple_pack(2, name_string, value_string);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static sky_tuple_t
sky__ssl_openssl_X509_NAME_tuple(X509_NAME *x509_name)
{
    int         entry_count, i, rdn_level;
    sky_list_t  dn, rdn;

    dn = sky_list_create(NULL);
    rdn = sky_list_create(NULL);

    rdn_level = -1;
    entry_count = X509_NAME_entry_count(x509_name);
    for (i = 0; i < entry_count; ++i) {
        X509_NAME_ENTRY *entry;

        entry = X509_NAME_get_entry(x509_name, i);
        if (rdn_level >= 0 && rdn_level != entry->set) {
            sky_list_append(dn, sky_tuple_create(rdn));
            rdn = sky_list_create(NULL);
        }
        rdn_level = entry->set;

        sky_list_append(
                rdn,
                sky__ssl_openssl_attribute_tuple(
                        X509_NAME_ENTRY_get_object(entry),
                        X509_NAME_ENTRY_get_data(entry)));
    }

    if (sky_list_len(rdn) > 0) {
        sky_list_append(dn, sky_tuple_create(rdn));
    }

    return sky_tuple_create(dn);
}

static void
sky__ssl_openssl_GENERAL_NAMES_cleanup(void *pointer)
{
    sk_GENERAL_NAME_pop_free(pointer, GENERAL_NAME_free);
}

static sky_tuple_t
sky__ssl_openssl_peeraltnames(X509 *x509)
    /* this code follows the procedure outlined in OpenSSL's
     * crypto/x509v3/v3_prn.c:X509v3_EXT_print() function to
     * extract the STACK_OF(GENERAL_NAME), then iterates
     * through the stack to add the names.
     */
{
    sky_tuple_t tuple;

    SKY_ASSET_BLOCK_BEGIN {
        BIO         *bio;
        int         i;
        sky_list_t  list;

        bio = BIO_new(BIO_s_mem());
        sky_asset_save(bio, (sky_free_t)BIO_free, SKY_ASSET_CLEANUP_ALWAYS);

        i = -1;
        list = NULL;
        while ((i = X509_get_ext_by_NID(x509, NID_subject_alt_name, i)) >= 0) {
            X509_EXTENSION          *ext;
            const X509V3_EXT_METHOD *method;

            if (!list) {
                list = sky_list_create(NULL);
            }

            ext = X509_get_ext(x509, i);
            if (!(method = X509V3_EXT_get(ext))) {
                sky_error_raise_string(
                        sky__ssl_SSLError,
                        ERRSTR("No method for internalizing subjectAltName!"));
            }

            SKY_ASSET_BLOCK_BEGIN {
                int                 j;
                GENERAL_NAMES       *names;
#if OPENSSL_VERSION_NUMBER < 0x009060dfL
                unsigned char       *p;
#else
                const unsigned char *p;
#endif

                p = ext->value->data;
                if (method->it) {
                    names = (GENERAL_NAMES *)
                            ASN1_item_d2i(NULL,
                                          &p,
                                          ext->value->length,
                                          ASN1_ITEM_ptr(method->it));
                }
                else {
                    names = (GENERAL_NAMES *)
                            method->d2i(NULL,
                                        &p,
                                        ext->value->length);
                }
                sky_asset_save(names,
                               sky__ssl_openssl_GENERAL_NAMES_cleanup,
                               SKY_ASSET_CLEANUP_ALWAYS);

                for (j = 0; j < sk_GENERAL_NAME_num(names); ++j) {
                    int             buflen, gntype;
                    char            buf[2048], *vptr;
                    sky_tuple_t     t;
                    ASN1_STRING     *as;
                    GENERAL_NAME    *name;
                    sky_object_t    v;
                    sky_string_t    s;

                    name = sk_GENERAL_NAME_value(names, j);
                    gntype = name->type;
                    switch (gntype) {
                        case GEN_DIRNAME:
                            /* we special-case DirName as a tuple of
                             * tuples of attributes
                             */
                            v = sky__ssl_openssl_X509_NAME_tuple(
                                        name->d.dirn);
                            t = sky_tuple_pack(2,
                                               SKY_STRING_LITERAL("DirName"),
                                               v);
                            break;

                        case GEN_EMAIL:
                        case GEN_DNS:
                        case GEN_URI:
                            /* GENERAL_NAME_print() doesn't handle NULL bytes
                             * in ASN1_string correctly, CVE-2013-4238
                             */
                            switch (gntype) {
                                case GEN_EMAIL:
                                    v = SKY_STRING_LITERAL("email");
                                    as = name->d.rfc822Name;
                                    break;
                                case GEN_DNS:
                                    v = SKY_STRING_LITERAL("DNS");
                                    as = name->d.dNSName;
                                    break;
                                case GEN_URI:
                                    v = SKY_STRING_LITERAL("URI");
                                    as = name->d.uniformResourceIdentifier;
                                    break;
                            }
                            s = sky_string_createfrombytes(
                                        ASN1_STRING_data(as),
                                        ASN1_STRING_length(as),
                                        NULL,
                                        NULL);
                            t = sky_tuple_pack(2, v, s);
                            break;

                        default:
                            /* for everything else, we use the OpenSSL
                             * print form
                             */
                            switch (gntype) {
                                case GEN_OTHERNAME:
                                case GEN_X400:
                                case GEN_EDIPARTY:
                                case GEN_IPADD:
                                case GEN_RID:
                                    break;
                                default:
                                    sky_error_warn_format(
                                            sky_RuntimeWarning,
                                            "Unknown general name type %d",
                                            gntype);
                            }

                            BIO_reset(bio);
                            GENERAL_NAME_print(bio, name);
                            buflen = BIO_gets(bio, buf, sizeof(buf) - 1);
                            if (buflen < 0) {
                                sky__ssl_openssl_raise(NULL);
                            }

                            if (!(vptr = strchr(buf, ':'))) {
                                t = NULL;
                            }
                            else {
                                t = sky_object_build("(s#s#)",
                                                     buf,
                                                     vptr - buf,
                                                     vptr + 1,
                                                     buflen - (vptr - buf + 1));
                            }
                            break;
                    }
                    if (t) {
                        sky_list_append(list, t);
                    }
                }
            } SKY_ASSET_BLOCK_END;
        }

        tuple = (sky_object_bool(list) ? sky_tuple_create(list) : NULL);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static sky_dict_t
sky__ssl_openssl_decode_X509(X509 *x509)
{
    sky_dict_t  dict = sky_dict_create();

    SKY_ASSET_BLOCK_BEGIN {
        BIO         *bio;
        int         buflen;
        char        buf[2048];
        sky_tuple_t names;

        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("subject"),
                sky__ssl_openssl_X509_NAME_tuple(X509_get_subject_name(x509)));
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("issuer"),
                sky__ssl_openssl_X509_NAME_tuple(X509_get_issuer_name(x509)));
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("version"),
                sky_integer_create(X509_get_version(x509) + 1));

        bio = BIO_new(BIO_s_mem());
        sky_asset_save(bio, (sky_free_t)BIO_free, SKY_ASSET_CLEANUP_ALWAYS);

        BIO_reset(bio);
        i2a_ASN1_INTEGER(bio, X509_get_serialNumber(x509));
        if ((buflen = BIO_gets(bio, buf, sizeof(buf) - 1)) < 0) {
            sky__ssl_openssl_raise(NULL);
        }
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("serialNumber"),
                sky_string_createfrombytes(buf, buflen, NULL, NULL));

        BIO_reset(bio);
        ASN1_TIME_print(bio, X509_get_notBefore(x509));
        if ((buflen = BIO_gets(bio, buf, sizeof(buf) - 1)) < 0) {
            sky__ssl_openssl_raise(NULL);
        }
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("notBefore"),
                sky_string_createfrombytes(buf, buflen, NULL, NULL));

        BIO_reset(bio);
        ASN1_TIME_print(bio, X509_get_notAfter(x509));
        if ((buflen = BIO_gets(bio, buf, sizeof(buf) - 1)) < 0) {
            sky__ssl_openssl_raise(NULL);
        }
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("notAfter"),
                sky_string_createfrombytes(buf, buflen, NULL, NULL));

        if ((names = sky__ssl_openssl_peeraltnames(x509)) != NULL) {
            sky_dict_setitem(dict, SKY_STRING_LITERAL("subjectAltName"), names);
        }
    } SKY_ASSET_BLOCK_END;

    return dict;
}


void
sky__ssl_openssl__SSLContext_instance_initialize(sky_object_t self, void *data)
{
    sky__ssl__SSLContext_data_t *self_data = data;

    sky_spinlock_init(&(self_data->spinlock));
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->openssl_freelist)),
                      sky_list_create(NULL),
                      self);
}

void
sky__ssl_openssl__SSLContext_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__ssl__SSLContext_data_t *self_data = data;

    SSL_CTX_free(self_data->ctx);
    sky_free(self_data->npn_protocols);
}

void
sky__ssl_openssl__SSLContext_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__ssl__SSLContext_data_t *self_data = data;

    sky_object_visit(self_data->openssl_freelist, visit_data);
}


sky_object_t
sky__ssl_openssl__SSLContext_create(
                sky_type_t          cls,
                sky__ssl_protocol_t protocol)
{
    sky_object_t    context;

    SKY_ASSET_BLOCK_BEGIN {
        long        options;
        SSL_CTX *   ctx;

        switch (protocol) {
#if !defined(OPENSSL_NO_SSL2)
            case SKY__SSL_PROTOCOL_SSLv2:
                ctx = SSL_CTX_new(SSLv2_method());
                break;
#endif
            case SKY__SSL_PROTOCOL_SSLv23:
                ctx = SSL_CTX_new(SSLv23_method());
                break;
            case SKY__SSL_PROTOCOL_SSLv3:
                ctx = SSL_CTX_new(SSLv3_method());
                break;
            case SKY__SSL_PROTOCOL_TLSv1:
                ctx = SSL_CTX_new(TLSv1_method());
                break;
            default:
                sky_error_raise_string(sky_ValueError,
                                       "invalid protocol version");
        }
        sky_asset_save(ctx,
                       (sky_free_t)SSL_CTX_free,
                       SKY_ASSET_CLEANUP_ON_ERROR);

        options = SSL_OP_ALL & ~SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS;
        if (protocol != SKY__SSL_PROTOCOL_SSLv2) {
            options |= SSL_OP_NO_SSLv2;
        }
        SSL_CTX_set_options(ctx, options);
        SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL);
        SSL_CTX_set_session_id_context(ctx,
                                       (const unsigned char *)"Python",
                                       sizeof("Python"));

        context = sky_object_allocate(cls);
        sky__ssl__SSLContext_data(context)->ctx = ctx;
    } SKY_ASSET_BLOCK_END;

    return context;
}

typedef struct sky__ssl_openssl_passwd_data_s {
    SSL_CTX *                           ctx;
    sky_object_t                        password;
    sky_error_record_t *                error;
    pem_password_cb *                   default_passwd_callback;
    void *                              default_passwd_callback_userdata;
} sky__ssl_openssl_passwd_data_t;

static int
sky__ssl_openssl_passwd_cb(char *buf,
                int     size,
    SKY_UNUSED  int     rwflag,
                void *  userdata)
{
    sky__ssl_openssl_passwd_data_t  *passwd_data = userdata;

    int return_value;

    SKY_ERROR_TRY {
        sky_buffer_t    buffer;
        sky_object_t    password;

        if (!sky_object_callable(passwd_data->password)) {
            password = passwd_data->password;
        }
        else {
            password = sky_object_call(passwd_data->password, NULL, NULL);
            if (!sky_object_isa(password, sky_bytes_type) &&
                !sky_object_isa(password, sky_bytearray_type) &&
                !sky_object_isa(password, sky_string_type))
            {
                sky_error_raise_string(
                        sky_TypeError,
                        "password callback must return a string");
            }
        }
        if (sky_object_isa(password, sky_string_type)) {
            password = sky_codec_encode(NULL, password, NULL, NULL, 0);
        }
        sky_buffer_acquire(&buffer, password, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (buffer.len > size) {
            sky_error_raise_format(sky_ValueError,
                                   "password cannot be longer than %d bytes",
                                   size);
        }
        memcpy(buf, buffer.buf, buffer.len);
        return_value = buffer.len;
    } SKY_ERROR_EXCEPT_ANY {
        passwd_data->error = sky_error_current();
        sky_error_record_retain(passwd_data->error);
        return_value = -1;
    } SKY_ERROR_TRY_END;

    return return_value;
}

static void
sky__ssl_openssl_passwd_data_cleanup(
                sky__ssl_openssl_passwd_data_t *passwd_data)
{
    if (passwd_data->error) {
        sky_error_record_release(passwd_data->error);
        passwd_data->error = NULL;
    }
    SSL_CTX_set_default_passwd_cb(
            passwd_data->ctx,
            passwd_data->default_passwd_callback);
    SSL_CTX_set_default_passwd_cb_userdata(
            passwd_data->ctx,
            passwd_data->default_passwd_callback_userdata);
}

void
sky__ssl_openssl__SSLContext_load_cert_chain(
                sky_object_t    self,
                sky_bytes_t     certfile,
                sky_bytes_t     keyfile,
                sky_object_t    password)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    SKY_ASSET_BLOCK_BEGIN {
        const char                      *ccertfile, *ckeyfile;
        sky__ssl_openssl_passwd_data_t  passwd_data;

        memset(&passwd_data, 0, sizeof(passwd_data));
        passwd_data.ctx = ctx;
        passwd_data.default_passwd_callback =
                ctx->default_passwd_callback;
        passwd_data.default_passwd_callback_userdata =
                ctx->default_passwd_callback_userdata;
        sky_asset_save(&passwd_data,
                       (sky_free_t)sky__ssl_openssl_passwd_data_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (!sky_object_isnull(password)) {
            if (!sky_object_callable(password) &&
                !sky_object_isa(password, sky_bytes_type) &&
                !sky_object_isa(password, sky_bytearray_type) &&
                !sky_object_isa(password, sky_string_type))
            {
                sky_error_raise_string(
                        sky_TypeError,
                        "password should be a string or callable");
            }
            passwd_data.password = password;
            SSL_CTX_set_default_passwd_cb(ctx,
                                          sky__ssl_openssl_passwd_cb);
            SSL_CTX_set_default_passwd_cb_userdata(ctx, &passwd_data);
        }

        ccertfile = (const char *)sky_bytes_cstring(certfile, SKY_TRUE);
        errno = 0;
        if (SSL_CTX_use_certificate_chain_file(ctx, ccertfile) != 1) {
            if (passwd_data.error) {
                sky_error_raise_record(passwd_data.error);
            }
            if (errno) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            sky__ssl_openssl_raise(NULL);
        }

        if (sky_object_isnull(keyfile)) {
            ckeyfile = ccertfile;
        }
        else {
            ckeyfile = (const char *)sky_bytes_cstring(keyfile, SKY_TRUE);
        }
        errno = 0;
        if (SSL_CTX_use_PrivateKey_file(ctx, ckeyfile, SSL_FILETYPE_PEM) != 1) {
            if (passwd_data.error) {
                sky_error_raise_record(passwd_data.error);
            }
            if (errno) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            sky__ssl_openssl_raise(NULL);
        }

        if (SSL_CTX_check_private_key(ctx) != 1) {
            sky__ssl_openssl_raise(NULL);
        }
    } SKY_ASSET_BLOCK_END;
}

void
sky__ssl_openssl__SSLContext_load_dh_params(
                sky_object_t    self,
                sky_bytes_t     filename)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    SKY_ASSET_BLOCK_BEGIN {
        DH      *dh;
        FILE    *fp;

        if (!(fp = fopen((const char *)sky_bytes_cstring(filename, SKY_TRUE),
                         "rb")))
        {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_asset_save(fp, (sky_free_t)fclose, SKY_ASSET_CLEANUP_ALWAYS);

        errno = 0;
        if (!(dh = PEM_read_DHparams(fp, NULL, NULL, NULL))) {
            if (errno) {
                ERR_clear_error();
                sky_error_raise_errno(sky_OSError, errno);
            }
            sky__ssl_openssl_raise(NULL);
        }
        sky_asset_save(dh, (sky_free_t)DH_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (!SSL_CTX_set_tmp_dh(ctx, dh)) {
            sky__ssl_openssl_raise(NULL);
        }
    } SKY_ASSET_BLOCK_END;
}

void
sky__ssl_openssl__SSLContext_load_verify_locations(
                sky_object_t    self,
                sky_bytes_t     cafile,
                sky_bytes_t     capath)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    if (sky_object_isnull(cafile)) {
        cafile = NULL;
    }
    if (sky_object_isnull(capath)) {
        capath = NULL;
    }
    if (!cafile && !capath) {
        sky_error_raise_string(sky_TypeError,
                               "cafile and capath cannot be both omitted");
    }

    SKY_ASSET_BLOCK_BEGIN {
        const char  *ccafile = NULL, *ccapath = NULL;

        if (cafile) {
            ccafile = (const char *)sky_bytes_cstring(cafile, SKY_TRUE);
        }
        if (capath) {
            ccapath = (const char *)sky_bytes_cstring(capath, SKY_TRUE);
        }

        errno = 0;
        if (SSL_CTX_load_verify_locations(ctx, ccafile, ccapath) != 1) {
            if (errno) {
                ERR_clear_error();
                sky_error_raise_errno(sky_OSError, errno);
            }
            sky__ssl_openssl_raise(NULL);
        }
    } SKY_ASSET_BLOCK_END;
}

sky_object_t
sky__ssl_openssl__SSLContext_options(sky_object_t self)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    return sky_integer_create(SSL_CTX_get_options(ctx));
}

sky_object_t
sky__ssl_openssl__SSLContext_options_getter(
                sky_object_t    self,
    SKY_UNUSED  sky_type_t      type)
{
    return sky__ssl_openssl__SSLContext_options(self);
}

void
sky__ssl_openssl__SSLContext_options_setter(
                sky_object_t    self,
                sky_object_t    value,
    SKY_UNUSED  sky_type_t      type)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    long    clear, new_options, options, set;

    sky_data_type_store(SKY_DATA_TYPE_LONG, NULL, value, &new_options, NULL);
    options = SSL_CTX_get_options(ctx);
    clear = options & ~new_options;
    set = ~options & new_options;
    if (clear) {
#if defined(HAVE_SSL_CTX_CLEAR_OPTIONS)
        SSL_CTX_clear_options(ctx, clear);
#else
        sky_error_raise_string(sky_ValueError,
                               "can't clear options before OpenSSL 0.9.8m");
#endif
    }
    if (set) {
        SSL_CTX_set_options(ctx, set);
    }
}

sky_dict_t
sky__ssl_openssl__SSLContext_session_stats(sky_object_t self)
{
    SSL_CTX     *ctx = sky__ssl__SSLContext_SSL_CTX(self);
    sky_dict_t  dict = sky_dict_create();

#define set_session_stat(_n)                                        \
        sky_dict_setitem(dict,                                      \
                         SKY_STRING_LITERAL(#_n),                   \
                         sky_integer_create(SSL_CTX_sess_##_n(ctx)))

    set_session_stat(number);
    set_session_stat(connect);
    set_session_stat(connect_good);
    set_session_stat(connect_renegotiate);
    set_session_stat(accept);
    set_session_stat(accept_good);
    set_session_stat(accept_renegotiate);
    set_session_stat(hits);
    set_session_stat(misses);
    set_session_stat(timeouts);
    set_session_stat(cache_full);

    return dict;
}

void
sky__ssl_openssl__SSLContext_set_ciphers(
                sky_object_t    self,
                sky_string_t    ciphers)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    SKY_ASSET_BLOCK_BEGIN {
        char    *cciphers;

        cciphers = sky_codec_encodetocstring(ciphers, 0);
        sky_asset_save(cciphers, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        if (!SSL_CTX_set_cipher_list(ctx, cciphers)) {
            /* Clearing the error queue is necessary on some OpenSSL versions,
             * otherwise the error will be reported again when another SSL call
             * is done.
             */
            ERR_clear_error();
            sky_error_raise_string(sky__ssl_SSLError,
                                   "No cipher can be selected.");
        }
    } SKY_ASSET_BLOCK_END;
}

void
sky__ssl_openssl__SSLContext_set_default_verify_paths(sky_object_t self)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    if (!SSL_CTX_set_default_verify_paths(ctx)) {
        sky__ssl_openssl_raise(NULL);
    }
}

#if !defined(OPENSSL_NO_ECDH)
void
sky__ssl_openssl__SSLContext_set_ecdh_curve(
                sky_object_t    self,
                sky_bytes_t     name)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    SKY_ASSET_BLOCK_BEGIN {
        int     nid;
        EC_KEY  *key;

        if (!(nid = OBJ_sn2nid((const char *)sky_bytes_cstring(name,
                                                               SKY_TRUE))))
        {
            sky_error_raise_format(sky_ValueError,
                                   "unknown elliptic curve name %#@", name);
        }
        if (!(key = EC_KEY_new_by_curve_name(nid))) {
            sky__ssl_openssl_raise(NULL);
        }
        SSL_CTX_set_tmp_ecdh(ctx, key);
        EC_KEY_free(key);
    } SKY_ASSET_BLOCK_END;
}
#endif

#if defined(OPENSSL_NPN_NEGOTIATED)
static int
sky__ssl_openssl_next_protos_advertised_cb(
    SKY_UNUSED  SSL *                   s,
                const unsigned char **  buf,
                unsigned int *          len,
                void *                  arg)
{
    sky__ssl__SSLContext_data_t *self_data = sky__ssl__SSLContext_data(arg);

    int result = SSL_TLSEXT_ERR_OK;

    if (!self_data->npn_protocols) {
        *buf = (unsigned char *)"";
        *len = 0;
    }
    else {
        /* Make a copy of the data, create a bytes object to wrapper it for
         * later release sometime after the SSLContext is destroyed, and pass
         * the copy to OpenSSL. OpenSSL will make its own copy of the data so
         * two copies will float around for a while, but otherwise there's a
         * race condition that could result in use after free. OpenSSL's API
         * for this is ridiculous.
         */
        SKY_ERROR_TRY {
            sky_spinlock_lock(&(self_data->spinlock));
            *buf = (unsigned char *)
                   sky_asset_memdup(self_data->npn_protocols,
                                    self_data->npn_protocols_len,
                                    SKY_ASSET_CLEANUP_ON_ERROR);
            *len = self_data->npn_protocols_len;
            sky_spinlock_unlock(&(self_data->spinlock));

            sky_list_append(self_data->openssl_freelist,
                            sky_bytes_createwithbytes(*buf, *len, sky_free));
        } SKY_ERROR_EXCEPT_ANY {
            sky_error_display(sky_error_current());
            result = SSL_TLSEXT_ERR_ALERT_FATAL;    /* XXX is this right?! */
        } SKY_ERROR_TRY_END;
    }

    return result;
}

static int
sky__ssl_openssl_next_proto_select_cb(
    SKY_UNUSED  SSL *                   s,
                unsigned char **        out,
                unsigned char *         outlen,
                const unsigned char *   in,
                unsigned int            inlen,
                void *                  arg)
{
    sky__ssl__SSLContext_data_t *self_data = sky__ssl__SSLContext_data(arg);

    int             protocols_len, result;
    unsigned char   *protocols;

    result = SSL_TLSEXT_ERR_OK;

    if (!self_data->npn_protocols) {
        protocols = (unsigned char *)"";
        protocols_len = 0;
    }
    else {
        sky_spinlock_lock(&(self_data->spinlock));
        protocols = sky_memdup(self_data->npn_protocols,
                               self_data->npn_protocols_len);
        protocols_len = self_data->npn_protocols_len;
        sky_spinlock_unlock(&(self_data->spinlock));

        SKY_ERROR_TRY {
            sky_list_append(self_data->openssl_freelist,
                            sky_bytes_createwithbytes(protocols,
                                                      protocols_len,
                                                      sky_free));
        } SKY_ERROR_EXCEPT_ANY {
            sky_error_display(sky_error_current());
            result = SSL_TLSEXT_ERR_ALERT_FATAL;
        } SKY_ERROR_TRY_END;
    }

    if (SSL_TLSEXT_ERR_OK == result) {
        SSL_select_next_proto(out, outlen, in, inlen, protocols, protocols_len);
    }

    return result;
}
#endif

void
sky__ssl_openssl__SSLContext__set_npn_protocols(
                sky_object_t    self,
                sky_object_t    protocols)
{
    sky__ssl__SSLContext_data_t *self_data = sky__ssl__SSLContext_data(self);

    char            *new_npn_protocols, *old_npn_protocols;
    sky_buffer_t    buffer;

    sky_buffer_acquire(&buffer, protocols, SKY_BUFFER_FLAG_SIMPLE);
    new_npn_protocols = sky_memdup(buffer.buf, buffer.len);

    sky_spinlock_lock(&(self_data->spinlock));
    old_npn_protocols = self_data->npn_protocols;
    self_data->npn_protocols = new_npn_protocols;
    self_data->npn_protocols_len = buffer.len;
    sky_spinlock_unlock(&(self_data->spinlock));

    sky_buffer_release(&buffer);
    sky_free(old_npn_protocols);

#if defined(OPENSSL_NPN_NEGOTIATED)
    /* Passing self for userdata is safe here, because SSL_CTX will exist
     * only as long as self does.
     */
    SSL_CTX_set_next_protos_advertised_cb(
            self_data->ctx,
            sky__ssl_openssl_next_protos_advertised_cb,
            self);
    SSL_CTX_set_next_proto_select_cb(
            self_data->ctx,
            sky__ssl_openssl_next_proto_select_cb,
            self);
#else
    sky_error_raise_string(
            sky_NotImplementedError,
            "The NPN extension requires OpenSSL 1.0.1 or later.");
#endif
}

sky_object_t
sky__ssl_openssl__SSLContext_verify_mode_getter(
                sky_object_t    self,
    SKY_UNUSED  sky_type_t      type)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    sky__ssl_cert_t mode;

    switch (SSL_CTX_get_verify_mode(ctx)) {
        case SSL_VERIFY_NONE:
            mode = SKY__SSL_CERT_NONE;
            break;
        case SSL_VERIFY_PEER:
            mode = SKY__SSL_CERT_OPTIONAL;
            break;
        case SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT:
            mode = SKY__SSL_CERT_REQUIRED;
            break;
        default:
            sky_error_raise_string(
                    sky__ssl_SSLError,
                    "invalid return value from SSL_CTX_get_verify_mode");
    }

    return sky_integer_create(mode);
}

void
sky__ssl_openssl__SSLContext_verify_mode_setter(
                sky_object_t    self,
                sky_object_t    value,
    SKY_UNUSED  sky_type_t      type)
{
    SSL_CTX *ctx = sky__ssl__SSLContext_SSL_CTX(self);

    int mode;

    sky_data_type_store(SKY_DATA_TYPE_INT, NULL, value, &mode, NULL);
    switch (mode) {
        case SKY__SSL_CERT_NONE:
            mode = SSL_VERIFY_NONE;
            break;
        case SKY__SSL_CERT_OPTIONAL:
            mode = SSL_VERIFY_PEER;
            break;
        case SKY__SSL_CERT_REQUIRED:
            mode = SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT;
            break;
        default:
            sky_error_raise_string(sky_ValueError,
                                   "invalid value for verify_mode");
    }

    SSL_CTX_set_verify(ctx, mode, NULL);
}


void
sky__ssl_openssl__SSLSocket_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__ssl__SSLSocket_data_t  *self_data = data;

    if (self_data->peer_cert) {
        X509_free(self_data->peer_cert);
    }
    if (self_data->ssl) {
        SSL_free(self_data->ssl);
    }
    if (self_data->error) {
        sky_error_record_release(self_data->error);
        self_data->error = NULL;
    }
}

static void SKY_NORETURN
sky__ssl_openssl__SSLSocket_raise_(
                sky_object_t    self,
                int             return_code,
                const char *    filename,
                int             lineno)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    char                *message;
    sky_type_t          type;
    unsigned long       last_error;
    sky__ssl_error_t    error;

    sky_error_validate_debug(return_code <= 0);
    last_error = ERR_peek_last_error();
    type = sky__ssl_SSLError;
    error = SKY__SSL_ERROR_NONE;
    message = NULL;

    if (self_data->ssl) {
        switch (SSL_get_error(self_data->ssl, return_code)) {
            case SSL_ERROR_ZERO_RETURN:
                message = "TLS/SSL connection has been closed (EOF)";
                type = sky__ssl_SSLZeroReturnError;
                error = SKY__SSL_ERROR_ZERO_RETURN;
                break;
            case SSL_ERROR_WANT_READ:
                message = "The operation did not complete (read)";
                type = sky__ssl_SSLWantReadError;
                error = SKY__SSL_ERROR_WANT_READ;
                break;
            case SSL_ERROR_WANT_WRITE:
                message = "The operation did not complete (write)";
                type = sky__ssl_SSLWantWriteError;
                error = SKY__SSL_ERROR_WANT_WRITE;
                break;
            case SSL_ERROR_WANT_X509_LOOKUP:
                message = "The operation did not complete (X509 lookup)";
                error = SKY__SSL_ERROR_WANT_X509_LOOKUP;
                break;
            case SSL_ERROR_WANT_CONNECT:
                message = "The operation did not complete (connect)";
                error = SKY__SSL_ERROR_WANT_CONNECT;
                break;
            case SSL_ERROR_SYSCALL:
                if (last_error) {
                    error = SKY__SSL_ERROR_SYSCALL;
                }
                else {
                    if (!return_code) {
                        message = "EOF occurred in violation of protocol";
                        type = sky__ssl_SSLEOFError;
                        error = SKY__SSL_ERROR_EOF;
                    }
                    else if (-1 == return_code) {
                        sky_error_record_t  *error_record;

                        if ((error_record = self_data->error) != NULL) {
                            self_data->error = NULL;
                            SKY_ASSET_BLOCK_BEGIN {
                                sky_asset_save(
                                        error_record,
                                        (sky_free_t)sky_error_record_release,
                                        SKY_ASSET_CLEANUP_ALWAYS);
                                sky_error_raise_record(error_record);
                            } SKY_ASSET_BLOCK_END;
                        }

                        /* underlying BIO reported an I/O error */
                        ERR_clear_error();
                        sky_error_raise_errno(sky_OSError, errno);
                    }
                    else {
                        message = "Some I/O error occurred";
                        type = sky__ssl_SSLSyscallError;
                        error = SKY__SSL_ERROR_SYSCALL;
                    }
                }
                break;
            case SSL_ERROR_SSL:
                if (!last_error) {
                    message = "A failure in the SSL library occurred";
                }
                error = SKY__SSL_ERROR_SSL;
                break;
            default:
                message = "Invalid error code";
                error = SKY__SSL_ERROR_INVALID_ERROR_CODE;
                break;
        }
    }

    sky__ssl_openssl_raise_(type,
                            message,
                            last_error,
                            error,
                            filename,
                            lineno);
}

#define sky__ssl_openssl__SSLSocket_raise(self, return_code)    \
        sky__ssl_openssl__SSLSocket_raise_(self,                \
                                           return_code,         \
                                           __FILE__,            \
                                           __LINE__)

sky_object_t
sky__ssl_openssl__SSLSocket_cipher(sky_object_t self)
{
    SSL *ssl = sky__ssl__SSLSocket_SSL(self);

    const char          *p;
    sky_object_t        bits, name, protocol;
    const SSL_CIPHER    *cipher;

    if (!ssl) {
        return sky_None;
    }

    if (!(cipher = SSL_get_current_cipher(ssl))) {
        return sky_None;
    }

    if (!(p = (const char *)SSL_CIPHER_get_name(cipher))) {
        name = sky_None;
    }
    else {
        name = sky_string_createfrombytes(p, strlen(p), NULL, NULL);
    }

    if (!(p = SSL_CIPHER_get_version(cipher))) {
        protocol = sky_None;
    }
    else {
        protocol = sky_string_createfrombytes(p, strlen(p), NULL, NULL);
    }

    bits = sky_integer_create(SSL_CIPHER_get_bits(cipher, NULL));

    return sky_tuple_pack(3, name, protocol, bits);
}

sky_object_t
sky__ssl_openssl__SSLSocket_compression(sky_object_t self)
{
    SSL *ssl = sky__ssl__SSLSocket_SSL(self);

    const char          *name;
    sky_bytes_t         bytes;
    const COMP_METHOD   *method;

    if (!ssl) {
        return sky_None;
    }

    method = SSL_get_current_compression(ssl);
    if (!method || NID_undef == method->type) {
        return sky_None;
    }
    if (!(name = OBJ_nid2sn(method->type))) {
        return sky_None;
    }

    bytes = sky_bytes_createfrombytes(name, strlen(name));
    return sky_codec_decodefilename(bytes);
}

void
sky__ssl_openssl__SSLSocket_do_handshake(sky_object_t self)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    int result;

    if ((result = SSL_do_handshake(self_data->ssl)) <= 0) {
        sky__ssl_openssl__SSLSocket_raise(self, result);
    }

    if (self_data->peer_cert) {
        X509_free(self_data->peer_cert);
        self_data->peer_cert = NULL;
    }
    self_data->peer_cert = SSL_get_peer_certificate(self_data->ssl);
}

void
sky__ssl_openssl__SSLSocket_initialize(
                sky_object_t    self,
                sky_object_t    context,
                sky_bytes_t     server_hostname)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    BIO *bio;

    /* Make sure the SSL error state is initialized */
    ERR_get_state();
    ERR_clear_error();

    bio = BIO_new(sky__ssl_openssl_bio_methods());
    bio->ptr = self;
    bio->init = 1;

    self_data->ssl = SSL_new(sky__ssl__SSLContext_SSL_CTX(context));
    SSL_set_bio(self_data->ssl, bio, bio);
#if defined(SSL_MODE_AUTO_RETRY)
    SSL_set_mode(self_data->ssl, SSL_MODE_AUTO_RETRY);
#endif

#if defined(SSL_CTRL_SET_TLSEXT_HOSTNAME)
    if (!sky_object_isnull(server_hostname)) {
        SKY_ASSET_BLOCK_BEGIN {
            SSL_set_tlsext_host_name(self_data->ssl,
                                     sky_bytes_cstring(server_hostname,
                                                       SKY_TRUE));
        } SKY_ASSET_BLOCK_END;
    }
#endif

    if (self_data->server_flag) {
        SSL_set_accept_state(self_data->ssl);
    }
    else {
        SSL_set_connect_state(self_data->ssl);
    }
}

sky_object_t
sky__ssl_openssl__SSLSocket_peer_certificate(sky_object_t self, sky_bool_t der)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    if (!self_data->peer_cert) {
        return sky_None;
    }

    if (der) {
        int             nbytes;
        unsigned char   *bytes = NULL;

        if ((nbytes = i2d_X509(self_data->peer_cert, &bytes)) < 0) {
            sky__ssl_openssl__SSLSocket_raise(self, nbytes);
        }
        sky_asset_save(bytes,
                       sky__ssl_openssl_OPENSSL_free,
                       SKY_ASSET_CLEANUP_ON_ERROR);
        return sky_bytes_createwithbytes(bytes,
                                         nbytes,
                                         sky__ssl_openssl_OPENSSL_free);
    }
    else {
        int verify_mode;

        verify_mode = SSL_CTX_get_verify_mode(SSL_get_SSL_CTX(self_data->ssl));
        if (!(verify_mode & SSL_VERIFY_PEER)) {
            return sky_dict_create();
        }
        return sky__ssl_openssl_decode_X509(self_data->peer_cert);
    }
}

int
sky__ssl_openssl__SSLSocket_pending(sky_object_t self)
{
    SSL *ssl = sky__ssl__SSLSocket_SSL(self);

    int pending;

    if ((pending = SSL_pending(ssl)) < 0) {
        sky__ssl_openssl__SSLSocket_raise(self, pending);
    }

    return pending;
}

sky_object_t
sky__ssl_openssl__SSLSocket_read(
                sky_object_t    self,
                ssize_t         len,
                sky_object_t    buf)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    sky_object_t    object = NULL;

    SKY_ASSET_BLOCK_BEGIN {
        int             error, result;
        void            *bytes;
        sky_buffer_t    buffer;

        if (len > INT_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "maximum length can't fit in a C 'int'");
        }
        if (!buf) {
            if (len < 0) {
                sky_error_raise_string(sky_SystemError, "Negative size");
            }
            bytes = (len ? sky_asset_malloc(len, SKY_ASSET_CLEANUP_ON_ERROR)
                         : "");
        }
        else {
            sky_buffer_acquirecbuffer(
                    &buffer,
                    buf,
                    SKY_BUFFER_FLAG_SIMPLE | SKY_BUFFER_FLAG_WRITABLE,
                    "read() argument 1",
                    NULL);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            if (len <= 0 || len > buffer.len) {
                len = buffer.len;
            }
            bytes = buffer.buf;
        }

        if (len > INT_MAX) {
            sky_error_raise_format(sky_OverflowError,
                                   "string longer than %d bytes", INT_MAX);
        }

        result = SSL_read(self_data->ssl, bytes, len);
        error = SSL_get_error(self_data->ssl, result);
        if (SSL_ERROR_ZERO_RETURN == error &&
            SSL_get_shutdown(self_data->ssl) == SSL_RECEIVED_SHUTDOWN)
        {
            object = sky_integer_zero;
        }

        if (!object) {
            if (result <= 0) {
                sky__ssl_openssl__SSLSocket_raise(self, result);
            }
            if (!buf) {
                object = sky_bytes_createwithbytes(bytes, result, sky_free);
            }
            else {
                object = sky_integer_create(result);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return object;
}

#if defined(OPENSSL_NPN_NEGOTIATED)
sky_object_t
sky__ssl_openssl__SSLSocket_selected_npn_protocol(sky_object_t self)
{
    SSL *ssl = sky__ssl__SSLSocket_SSL(self);

    unsigned int        len;
    const unsigned char *data;

    SSL_get0_next_proto_negotiated(ssl, &data, &len);
    if (!len) {
        return sky_None;
    }

    return sky_string_createfrombytes(data, len, NULL, NULL);
}
#endif

sky_socket_t
sky__ssl_openssl__SSLSocket_shutdown(sky_object_t self)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    int result, zeros = 0;

    for (;;) {
        if (self_data->shutdown_seen_zero) {
            SSL_set_read_ahead(self_data->ssl, 0);
        }
        if ((result = SSL_shutdown(self_data->ssl)) != 0) {
            break;
        }

        /* Don't loop endlessly; instead preserve legacy behavior of trying
         * SSL_shutdown() only twice.
         * This looks necessary for OpenSSL < 0.9.8m
         */
        if (++zeros > 1) {
            break;
        }
        /* Shutdown was sent, now try receiving */
        self_data->shutdown_seen_zero = SKY_TRUE;
    }

    if (result <= 0) {
        sky__ssl_openssl__SSLSocket_raise(self, result);
    }

    return self_data->socket;
}

#if defined(HAVE_OPENSSL_FINISHED)
sky_object_t
sky__ssl_openssl__SSLSocket_tls_unique_cb(sky_object_t self)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    char    buf[128];
    size_t  len;

    if (SSL_session_reused(self_data->ssl) ^ !self_data->server_flag) {
        /* if session is resumed XOR we are the client */
        len = SSL_get_finished(self_data->ssl, buf, sizeof(buf));
    }
    else {
        /* if a new session XOR we are the server */
        len = SSL_get_peer_finished(self_data->ssl, buf, sizeof(buf));
    }

    if (!len) {
        return sky_None;
    }
    return sky_bytes_createfrombytes(buf, len);
}
#endif

ssize_t
sky__ssl_openssl__SSLSocket_write(sky_object_t self, sky_object_t s)
{
    sky__ssl__SSLSocket_data_t  *self_data = sky__ssl__SSLSocket_data(self);

    ssize_t nbytes;

    SKY_ASSET_BLOCK_BEGIN {
        int             result;
        sky_buffer_t    buffer;

        sky_buffer_acquirecbuffer(&buffer,
                                  s,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "write() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len > INT_MAX) {
            sky_error_raise_format(sky_OverflowError,
                                   "string longer than %d bytes", INT_MAX);
        }

        if ((result = SSL_write(self_data->ssl, buffer.buf, buffer.len)) <= 0) {
            sky__ssl_openssl__SSLSocket_raise(self, result);
        }
        nbytes = result;
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}


static void
sky__ssl_openssl_RAND_add(sky_object_t string, double entropy)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        if (sky_object_isa(string, sky_string_type)) {
            string = sky_codec_encode(NULL, string, NULL, NULL, 0);
        }
        sky_buffer_acquirecbuffer(&buffer,
                                  string,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "RAND_add() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        RAND_add(buffer.buf, buffer.len, entropy);
    } SKY_ASSET_BLOCK_END;
}

static const char sky__ssl_openssl_RAND_add_doc[] =
"RAND_add(string, entropy)\n\n"
"Mix string into the OpenSSL PRNG state.  entropy (a float) is a lower\n"
"bound on the entropy contained in string.  See RFC 1750.";


static sky_bytes_t
sky__ssl_openssl_RAND_bytes(int n)
{
    sky_bytes_t bytes;

    if (n <= 0) {
        if (!n) {
            return sky_bytes_empty;
        }
        sky_error_raise_string(sky_ValueError, "num must be positive");
    }

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t *buffer;

        buffer = sky_asset_malloc(n, SKY_ASSET_CLEANUP_ON_ERROR);
        if (1 == RAND_bytes(buffer, n)) {
            bytes = sky_bytes_createwithbytes(buffer, n, sky_free);
        }
        else {
            long    err = ERR_get_error();
            
            sky_error_raise_object(
                    sky__ssl_SSLError,
                    sky_object_build("(ijs)",
                                     err,
                                     ERR_reason_error_string(err)));
        }
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char sky__ssl_openssl_RAND_bytes_doc[] =
"RAND_bytes(n) -> bytes\n\n"
"Generate n cryptographically strong pseudo-random bytes.";


static int
sky__ssl_openssl_RAND_egd(sky_object_t path)
{
    int n;

    if (sky_object_isa(path, sky_string_type)) {
        path = sky_codec_encodefilename(path);
    }

    SKY_ASSET_BLOCK_BEGIN {
        n = RAND_egd((char *)sky_bytes_cstring(path, SKY_TRUE));
    } SKY_ASSET_BLOCK_END;

    if (-1 == n) {
        sky_error_raise_string(
                sky__ssl_SSLError,
                "EGD connection failed or EGD did not return "
                "enough data to seed the PRNG");
    }
    return n;
}


static const char sky__ssl_openssl_RAND_egd_doc[] =
"RAND_egd(path) -> bytes\n\n"
"Queries the entropy gather daemon (EGD) on the socket named by 'path'.\n"
"Returns number of bytes read.  Raises SSLError if connection to EGD\n"
"fails or if it does not provide enough data to seed PRNG.";


static sky_tuple_t
sky__ssl_openssl_RAND_pseudo_bytes(int n)
{
    sky_tuple_t tuple;

    if (n <= 0) {
        if (!n) {
            return sky_tuple_pack(2, sky_bytes_empty, sky_True);
        }
        sky_error_raise_string(sky_ValueError, "num must be positive");
    }

    SKY_ASSET_BLOCK_BEGIN {
        int     strong;
        uint8_t *buffer;

        buffer = sky_asset_malloc(n, SKY_ASSET_CLEANUP_ON_ERROR);
        strong = RAND_pseudo_bytes(buffer, n);
        if (0 == strong || 1 == strong) {
            tuple = sky_tuple_pack(2,
                                   sky_bytes_createwithbytes(buffer,
                                                             n,
                                                             sky_free),
                                   (strong ? sky_True : sky_False));
        }
        else {
            long    err = ERR_get_error();
            
            sky_error_raise_object(
                    sky__ssl_SSLError,
                    sky_object_build("(ijs)",
                                     err,
                                     ERR_reason_error_string(err)));
        }
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static const char sky__ssl_openssl_RAND_pseudo_bytes_doc[] =
"RAND_pseudo_bytes(n) -> (bytes, is_cryptographic)\n\n"
"Generate n pseudo-random bytes. is_cryptographic is True if the bytes\n"
"generated are cryptographically strong.";


static const char sky__ssl_openssl_RAND_status_doc[] =
"RAND_status() -> 0 or 1\n\n"
"Returns 1 if the OpenSSL PRNG has been seeded with enough data and 0 if not.\n"
"It is necessary to seed the PRNG with RAND_add() on some platforms before\n"
"using the ssl() function.";


sky_dict_t
sky__ssl_openssl__test_decode_cert(const char *filename)
{
    sky_dict_t  dict;

    SKY_ASSET_BLOCK_BEGIN {
        BIO     *bio;
        X509    *x509;

        if (!(bio = BIO_new(BIO_s_file()))) {
            sky_error_raise_string(sky__ssl_SSLError,
                                   "Can't malloc memory to read file");
        }
        sky_asset_save(bio, (sky_free_t)BIO_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (BIO_read_filename(bio, filename) <= 0) {
            sky_error_raise_string(sky__ssl_SSLError,
                                   "Can't open file");
        }

        if (!(x509 = PEM_read_bio_X509_AUX(bio, NULL, NULL, NULL))) {
            sky_error_raise_string(sky__ssl_SSLError,
                                   "Error decoding PEM-encoded file");
        }
        sky_asset_save(x509, (sky_free_t)X509_free, SKY_ASSET_CLEANUP_ALWAYS);

        dict = sky__ssl_openssl_decode_X509(x509);
    } SKY_ASSET_BLOCK_END;

    return dict;
}


static void
sky__ssl_openssl_version(
                unsigned long   version,
                unsigned int *  major,
                unsigned int *  minor,
                unsigned int *  fix,
                unsigned int *  patch,
                unsigned int *  status)
{
    *status = version & 0xF;
    version >>= 4;
    *patch = version & 0xFF;
    version >>= 8;
    *fix = version & 0xFF;
    version >>= 8;
    *minor = version & 0xFF;
    version >>= 8;
    *major = version & 0xFF;
}

static int          sky__ssl_openssl_nlocks = 0;
static sky_mutex_t *sky__ssl_openssl_locks = NULL;

#if OPENSSL_VERSION_NUMBER >= 0x10000000
static void
sky__ssl_openssl_threadid_callback(CRYPTO_THREADID *id)
{
    CRYPTO_THREADID_set_numeric(id, sky_thread_id());
}
#else
static unsigned long
sky__ssl_openssl_id_callback(void)
{
    return sky_thread_id();
}
#endif

static void
sky__ssl_openssl_locking_callback(
                int         mode,
                int         n,
    SKY_UNUSED  const char *file,
    SKY_UNUSED  int         line)
{
    sky_error_validate_debug(sky__ssl_openssl_locks != NULL);
    sky_error_validate_debug(n >= 0 && n < sky__ssl_openssl_nlocks);

    if (!sky__ssl_openssl_locks[n]) {
        /* A NULL lock is possible at shutdown during finalization, in which
         * case skipping locking is no big deal since there shouldn't be any
         * other threads running anyway.
         */
        return;
    }

    if (mode & CRYPTO_LOCK) {
        sky_mutex_lock(sky__ssl_openssl_locks[n], SKY_TIME_INFINITE);
    }
    else {
        sky_mutex_unlock(sky__ssl_openssl_locks[n]);
    }
}

static void
sky__ssl_openssl_initialize_locks(void)
{
    if ((sky__ssl_openssl_nlocks = CRYPTO_num_locks()) > 0) {
        int i;

        sky__ssl_openssl_locks = sky_calloc(sky__ssl_openssl_nlocks,
                                            sizeof(sky_mutex_t));
        for (i = 0; i < sky__ssl_openssl_nlocks; ++i) {
            sky_object_gc_setlibraryroot(
                    SKY_AS_OBJECTP(&(sky__ssl_openssl_locks[i])),
                    sky_mutex_create(SKY_FALSE, SKY_TRUE),
                    SKY_TRUE);
        }
    }

    CRYPTO_set_locking_callback(sky__ssl_openssl_locking_callback);
#if OPENSSL_VERSION_NUMBER >= 0x10000000
    CRYPTO_THREADID_set_callback(sky__ssl_openssl_threadid_callback);
#else
    CRYPTO_set_id_callback(sky__ssl_openssl_id_callback);
#endif
}

void
sky__ssl_openssl_initialize(sky_module_t module)
{
    const char                      *version;
    sky_dict_t                      codes_to_names, names_to_codes;
    unsigned int                    fix, major, minor, patch, status;
    unsigned long                   int_version;
    sky__ssl_openssl_error_code_t   *error_code;
    sky__ssl_openssl_library_code_t *library_code;

    CRYPTO_set_mem_functions(sky_malloc, sky_realloc, sky_free);
    CRYPTO_set_locked_mem_functions(sky_malloc, sky_free);

    SSL_load_error_strings();
    SSL_library_init();
    sky__ssl_openssl_initialize_locks();
    OpenSSL_add_all_algorithms();

#if defined(HAVE_OPENSSL_RAND)
    sky_module_setattr(
            module,
            "RAND_add",
            sky_function_createbuiltin(
                    "RAND_add",
                    sky__ssl_openssl_RAND_add_doc,
                    (sky_native_code_function_t)sky__ssl_openssl_RAND_add,
                    SKY_DATA_TYPE_VOID,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "entropy", SKY_DATA_TYPE_DOUBLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "RAND_bytes",
            sky_function_createbuiltin(
                    "RAND_bytes",
                    sky__ssl_openssl_RAND_bytes_doc,
                    (sky_native_code_function_t)sky__ssl_openssl_RAND_bytes,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "n", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "RAND_egd",
            sky_function_createbuiltin(
                    "RAND_egd",
                    sky__ssl_openssl_RAND_egd_doc,
                    (sky_native_code_function_t)sky__ssl_openssl_RAND_egd,
                    SKY_DATA_TYPE_INT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "RAND_pseudo_bytes",
            sky_function_createbuiltin(
                    "RAND_pseudo_bytes",
                    sky__ssl_openssl_RAND_pseudo_bytes_doc,
                    (sky_native_code_function_t)sky__ssl_openssl_RAND_pseudo_bytes,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "n", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "RAND_status",
            sky_function_createbuiltin(
                    "RAND_status",
                    sky__ssl_openssl_RAND_status_doc,
                    (sky_native_code_function_t)RAND_status,
                    SKY_DATA_TYPE_INT,
                    NULL));
#endif

#define int_const(_n, _v) \
        sky_module_setattr(module, _n, sky_integer_create(_v));

    int_const("OP_ALL", SSL_OP_ALL & ~SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS);
    int_const("OP_NO_SSLv2", SSL_OP_NO_SSLv2);
    int_const("OP_NO_SSLv3", SSL_OP_NO_SSLv3);
    int_const("OP_NO_TLSv1", SSL_OP_NO_TLSv1);
    int_const("OP_CIPHER_SERVER_PREFERENCE", SSL_OP_CIPHER_SERVER_PREFERENCE);
    int_const("OP_SINGLE_DH_USE", SSL_OP_SINGLE_DH_USE);
#if defined(SSL_OP_SINGLE_ECDH_USE)
    int_const("OP_SINGLE_ECDH_USE", SSL_OP_SINGLE_ECDH_USE);
#endif
#if defined(SSL_OP_NO_COMPRESSION)
    int_const("OP_NO_COMPRESSION", SSL_OP_NO_COMPRESSION);
#endif

#if defined(SSL_CTRL_SET_TLSEXT_HOSTNAME)
    sky_module_setattr(module, "HAS_SNI", sky_True);
#else
    sky_module_setattr(module, "HAS_SNI", sky_False);
#endif

#if defined(HAVE_OPENSSL_FINISHED)
    sky_module_setattr(module, "HAS_TLS_UNIQUE", sky_True);
#else
    sky_module_setattr(module, "HAS_TLS_UNIQUE", sky_False);
#endif

#if defined(OPENSSL_NO_ECDH)
    sky_module_setattr(module, "HAS_ECDH", sky_False);
#else
    sky_module_setattr(module, "HAS_ECDH", sky_True);
#endif

#if defined(OPENSSL_NPN_NEGOTIATED)
    sky_module_setattr(module, "HAS_NPN", sky_True);
#else
    sky_module_setattr(module, "HAS_NPN", sky_False);
#endif

    codes_to_names = sky_dict_create();
    names_to_codes = sky_dict_create();
    for (error_code = sky__ssl_openssl_error_codes;
         error_code->mnemonic;
         ++error_code)
    {
        sky_tuple_t     key;
        sky_string_t    mnemonic;

        mnemonic = sky_string_createfrombytes(error_code->mnemonic,
                                              strlen(error_code->mnemonic),
                                              NULL,
                                              NULL);
        key = sky_object_build("(ii)", error_code->library,
                                       error_code->reason);
        sky_dict_setitem(codes_to_names, key, mnemonic);
        sky_dict_setitem(names_to_codes, mnemonic, key);
    }
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_openssl_err_codes_to_names),
            codes_to_names,
            SKY_TRUE);
    sky_module_setattr(module, "err_codes_to_names", codes_to_names);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_openssl_err_names_to_codes),
            names_to_codes,
            SKY_TRUE);
    sky_module_setattr(module, "err_names_to_codes", names_to_codes);

    codes_to_names = sky_dict_create();
    for (library_code = sky__ssl_openssl_library_codes;
         library_code->library;
         ++library_code)
    {
        sky_dict_setitem(
                codes_to_names,
                sky_integer_create(library_code->code),
                sky_string_createfrombytes(library_code->library,
                                           strlen(library_code->library),
                                           NULL,
                                           NULL));
    }
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_openssl_lib_codes_to_names),
            codes_to_names,
            SKY_TRUE);
    sky_module_setattr(module, "lib_codes_to_names", codes_to_names);

    int_version = SSLeay();
    sky_module_setattr(module,
                       "OPENSSL_VERSION_NUMBER", 
                       sky_integer_createfromunsigned(int_version));
    sky__ssl_openssl_version(int_version,
                             &major, &minor, &fix, &patch, &status);
    sky_module_setattr(module,
                       "OPENSSL_VERSION_INFO",
                       sky_object_build("(uuuuu)",
                                        major, minor, fix, patch, status));
    version = SSLeay_version(SSLEAY_VERSION);
    sky_module_setattr(module,
                       "OPENSSL_VERSION",
                       sky_string_createfrombytes(version,
                                                  strlen(version),
                                                  NULL,
                                                  NULL));

    int_version = OPENSSL_VERSION_NUMBER;
    sky__ssl_openssl_version(int_version,
                             &major, &minor, &fix, &patch, &status);
    sky_module_setattr(module,
                       "_OPENSSL_API_VERSION",
                       sky_object_build("(uuuuuu)",
                                        major, minor, fix, patch, status));
}

#if defined(__APPLE__)
#   pragma GCC diagnostic error "-Wdeprecated-declarations"
#endif
