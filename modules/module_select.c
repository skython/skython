#include "../core/sky_private.h"

#include <limits.h>

#ifndef PIPE_BUF
#   define PIPE_BUF 512
#endif


SKY_MODULE_EXTERN const char *skython_module_select_doc;

SKY_MODULE_EXTERN void
skython_module_select_initialize(sky_module_t module);


static sky_object_t
sky_select_fromfdset(fd_set *set, sky_object_t sequence, int hint)
{
    int             fd;
    sky_list_t      list;
    sky_object_t    item;

    list = sky_list_createwithcapacity(hint);
    SKY_SEQUENCE_FOREACH(sequence, item) {
        fd = sky_file_fileno(item);     /* will never return negative */
        if (fd < FD_SETSIZE && FD_ISSET(fd, set)) {
            sky_list_append(list, item);
        }
    } SKY_SEQUENCE_FOREACH_END;

    return list;
}

static void
sky_select_tofdset(fd_set *set, sky_object_t sequence)
{
    int             fd;
    sky_object_t    item;

    FD_ZERO(set);
    SKY_SEQUENCE_FOREACH(sequence, item) {
        fd = sky_file_fileno(item);     /* will never return negative */
        if (fd < FD_SETSIZE) {
            FD_SET(fd, set);
        }
    } SKY_SEQUENCE_FOREACH_END;
}

static sky_tuple_t
sky_select_select(
                sky_object_t    rlist,
                sky_object_t    wlist,
                sky_object_t    xlist,
                sky_object_t    timeout)
{
    int             result;
    fd_set          rset, wset, xset;
    struct timeval  *ctimeout, tv;

    if (sky_object_isnull(timeout)) {
        ctimeout = NULL;
    }
    else {
        tv = sky_time_totimeval(sky_time_fromobject(timeout));
        if (tv.tv_sec < 0 || tv.tv_usec < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "timeout must be non-negative");
        }
        ctimeout = &tv;
    }

    sky_select_tofdset(&rset, rlist);
    sky_select_tofdset(&wset, wlist);
    sky_select_tofdset(&xset, xlist);

    if ((result = select(FD_SETSIZE, &rset, &wset, &xset, ctimeout)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    rlist = sky_select_fromfdset(&rset, rlist, result);
    wlist = sky_select_fromfdset(&wset, wlist, result);
    xlist = sky_select_fromfdset(&xset, xlist, result);

    return sky_tuple_pack(3, rlist, wlist, xlist);
}

static const char sky_select_select_doc[] =
"select(rlist, wlist, xlist[, timeout]) -> (rlist, wlist, xlist)\n"
"\n"
"Wait until one or more file descriptors are ready for some kind of I/O.\n"
"The first three arguments are sequences of file descriptors to be waited for:\n"
"rlist -- wait until ready for reading\n"
"wlist -- wait until ready for writing\n"
"xlist -- wait for an ``exceptional condition''\n"
"If only one kind of condition is required, pass [] for the other lists.\n"
"A file descriptor is either a socket or file object, or a small integer\n"
"gotten from a fileno() method call on one of those.\n"
"\n"
"The optional 4th argument specifies a timeout in seconds; it may be\n"
"a floating point number to specify fractions of seconds.  If it is absent\n"
"or None, the call will never time out.\n"
"\n"
"The return value is a tuple of three lists corresponding to the first three\n"
"arguments; each contains the subset of the corresponding file descriptors\n"
"that are ready.\n"
"\n"
"*** IMPORTANT NOTICE ***\n"
"On Windows and OpenVMS, only sockets are supported; on Unix, all file\n"
"descriptors can be used.";


const char *skython_module_select_doc =
"This module supports asynchronous I/O on multiple file descriptors.\n"
"\n"
"*** IMPORTANT NOTICE ***\n"
"On Windows and OpenVMS, only sockets are supported; on Unix, all file\n"
"descriptors.";

void
skython_module_select_initialize(sky_module_t module)
{
    sky_module_setattr(module, "error", sky_OSError);

    /* TODO devpoll() */
    /* TODO poll() */
    sky_module_setattr(
            module,
            "select",
            sky_function_createbuiltin(
                    "select",
                    sky_select_select_doc,
                    (sky_native_code_function_t)sky_select_select,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "rlist", SKY_DATA_TYPE_OBJECT, NULL,
                    "wlist", SKY_DATA_TYPE_OBJECT, NULL,
                    "xlist", SKY_DATA_TYPE_OBJECT, NULL,
                    "timeout", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n));

    int_const(PIPE_BUF);

#if defined(HAVE_POLL)
    /* TODO poll type */

    int_const(POLLIN);
    int_const(POLLPRI);
    int_const(POLLOUT);
    int_const(POLLERR);
    int_const(POLLHUP);
    int_const(POLLNVAL);
#   ifdef POLLRDNORM
    int_const(POLLRDNORM);
#   endif
#   ifdef POLLRDBAND
    int_const(POLLRDBAND);
#   endif
#   ifdef POLLWRNORM
    int_const(POLLWRNORM);
#   endif
#   ifdef POLLWRBAND
    int_const(POLLWRBAND);
#   endif
#   ifdef POLLMSG
    int_const(POLLMSG);
#   endif
#endif

    /* TODO epoll stuff */

    /* TODO kqueue stuff */
}
