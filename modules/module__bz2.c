#include "../core/sky_private.h"

#include "bzlib.h"


SKY_MODULE_EXTERN void skython_module__bz2_initialize(sky_module_t module);


#if BUFSIZ < 8192
#   define SMALLCHUNK   8192
#else
#   define SMALLCHUNK   BUFSIZ
#endif


static void
sky__bz2_raise(int error)
{
    switch (error) {
        case BZ_OK:
        case BZ_RUN_OK:
        case BZ_FLUSH_OK:
        case BZ_FINISH_OK:
        case BZ_STREAM_END:
            return;

        case BZ_CONFIG_ERROR:
            sky_error_raise_string(
                    sky_SystemError,
                    "libbzip2 was not compiled correctly");

        case BZ_PARAM_ERROR:
            sky_error_raise_string(
                    sky_ValueError,
                    "Internal error - invalid parameters passed to libbzip2");

        case BZ_MEM_ERROR:
            sky_error_raise_object(sky_MemoryError, sky_None);

        case BZ_DATA_ERROR:
        case BZ_DATA_ERROR_MAGIC:
            sky_error_raise_string(sky_OSError, "Invalid data stream");
        case BZ_IO_ERROR:
            sky_error_raise_string(sky_OSError, "Unknown I/O error");
        case BZ_UNEXPECTED_EOF:
            sky_error_raise_string(
                    sky_EOFError,
                    "Compressed file ended before the logical "
                    "end-of-stream was detected");
        case BZ_SEQUENCE_ERROR:
            sky_error_raise_string(
                    sky_RuntimeError,
                    "Internal error - "
                    "Invalid sequence of commands sent to libbzip2");
    }

    sky_error_raise_format(sky_OSError,
                           "Unrecognized error from libbzip2: %d",
                           error);
}


static sky_type_t   sky__bz2_Compressor_type = NULL;

static const char sky__bz2_Compressor_type_doc[] =
"BZ2Compressor(compresslevel=9)\n\n"
"Create a compressor object for compressing data incrementally.\n\n"
"compresslevel, if given, must be a number between 1 and 9.\n\n"
"For one-shot compression, use the compress() function instead.\n";

typedef struct sky__bz2_Compressor_data_s {
    sky_bool_t                          initialized;
    sky_bool_t                          flushed;
    bz_stream                           bzs;
} sky__bz2_Compressor_data_t;

SKY_EXTERN_INLINE sky__bz2_Compressor_data_t *
sky__bz2_Compressor_data(sky_object_t self)
{
    return sky_object_data(self, sky__bz2_Compressor_type);
}

static void
sky__bz2_Compressor_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__bz2_Compressor_data_t  *self_data = data;

    if (self_data->initialized) {
        BZ2_bzCompressEnd(&(self_data->bzs));
        self_data->initialized = SKY_FALSE;
    }
}


static sky_bytes_t
sky__bz2_Compressor_data_compress(
                sky__bz2_Compressor_data_t *self_data,
                char *                      input,
                ssize_t                     length,
                int                         action)
{
    int         err;
    char        *output, *saved_next_out;
    ssize_t     navail, new_output_size, output_length, output_size;
    sky_bytes_t result;

    if (self_data->flushed) {
        sky_error_raise_string(sky_ValueError,
                               "Compressor has been flushed");
    }

    SKY_ASSET_BLOCK_BEGIN {
        output_size = SMALLCHUNK;
        output = sky_asset_malloc(output_size, SKY_ASSET_CLEANUP_ON_ERROR);
        output_length = 0;

        self_data->bzs.next_in = input;
        self_data->bzs.avail_in = 0;
        self_data->bzs.next_out = output;
        self_data->bzs.avail_out = SMALLCHUNK;

        for (;;) {
            if (!self_data->bzs.avail_in && length) {
                self_data->bzs.avail_in = SKY_MIN(length, UINT_MAX);
                length -= self_data->bzs.avail_in;
            }

            if (BZ_RUN == action && !self_data->bzs.avail_in) {
                break;
            }

            if (!self_data->bzs.avail_out) {
                navail = output_size - output_length;
                if (!navail) {
                    new_output_size = output_size + (output_size >> 3) + 6;
                    if (new_output_size <= output_size) {
                        sky_error_raise_string(
                                sky_OverflowError,
                                "Unable to allocate buffer - output too large");
                    }
                    output = sky_asset_realloc(output,
                                               new_output_size,
                                               SKY_ASSET_CLEANUP_ON_ERROR,
                                               SKY_ASSET_UPDATE_FIRST_CURRENT);
                    output_size = new_output_size;
                    self_data->bzs.next_out = output + output_length;
                    navail = output_size - output_length;
                }
                self_data->bzs.avail_out = SKY_MIN(navail, UINT_MAX);
            }

            saved_next_out = self_data->bzs.next_out;
            err = BZ2_bzCompress(&(self_data->bzs), action);
            sky__bz2_raise(err);

            output_length += self_data->bzs.next_out - saved_next_out;
            if (BZ_FINISH == action && BZ_STREAM_END == err) {
                break;
            }
        }

        result = sky_bytes_createwithbytes(output, output_length, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_bytes_t
sky__bz2_Compressor_compress(sky_object_t self, sky_object_t data)
{
    sky__bz2_Compressor_data_t  *self_data = sky__bz2_Compressor_data(self);

    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky__bz2_Compressor_data_compress(self_data,
                                                   buffer.buf,
                                                   buffer.len,
                                                   BZ_RUN);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__bz2_Compressor_compress_doc[] =
"compress(data) -> bytes\n\n"
"Provide data to the compressor object. Returns a chunk of\n"
"compressed data if possible, or b'' otherwise.\n\n"
"When you have finished providing data to the compressor, call the\n"
"flush() method to finish the compression process.\n";


static sky_bytes_t
sky__bz2_Compressor_flush(sky_object_t self)
{
    sky__bz2_Compressor_data_t  *self_data = sky__bz2_Compressor_data(self);

    return sky__bz2_Compressor_data_compress(self_data, NULL, 0, BZ_FINISH);
}

static const char sky__bz2_Compressor_flush_doc[] =
"flush() -> bytes\n\n"
"Finish the compression process. Returns the compressed data left\n"
"in internal buffers.\n\n"
"The compressor object may not be used after this method is called.\n";


static sky_object_t
sky__bz2_Compressor_getstate(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "cannot serialize %#@ object",
                           sky_type_name(sky_object_type(self)));
}


static void
sky__bz2_Compressor_init(sky_object_t self, int compresslevel)
{
    sky__bz2_Compressor_data_t  *self_data = sky__bz2_Compressor_data(self);

    int err;

    if (compresslevel < 1 || compresslevel > 9) {
        sky_error_raise_string(sky_ValueError,
                               "compresslevel must be between 1 and 9");
    }

    if (self_data->initialized) {
        BZ2_bzCompressEnd(&(self_data->bzs));
        self_data->initialized = SKY_FALSE;
    }

    memset(&(self_data->bzs), 0, sizeof(self_data->bzs));
    err = BZ2_bzCompressInit(&(self_data->bzs), compresslevel, 0, 0);
    sky__bz2_raise(err);

    self_data->initialized = SKY_TRUE;
    self_data->flushed = SKY_FALSE;
}


static sky_type_t   sky__bz2_Decompressor_type = NULL;

static const char sky__bz2_Decompressor_type_doc[] =
"BZ2Decompressor()\n\n"
"Create a decompressor object for decompressing data incrementally.\n\n"
"For one-shot decompression, use the decompress() function instead.\n";

typedef struct sky__bz2_Decompressor_data_s {
    sky_bool_t                          initialized;
    sky_bool_t                          eof;
    sky_bytes_t                         unused_data;
    bz_stream                           bzs;
} sky__bz2_Decompressor_data_t;

SKY_EXTERN_INLINE sky__bz2_Decompressor_data_t *
sky__bz2_Decompressor_data(sky_object_t self)
{
    return sky_object_data(self, sky__bz2_Decompressor_type);
}

static void
sky__bz2_Decompressor_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__bz2_Decompressor_data_t    *self_data = data;

    if (self_data->initialized) {
        BZ2_bzDecompressEnd(&(self_data->bzs));
        self_data->initialized = SKY_FALSE;
    }
}

static void
sky__bz2_Decompressor_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__bz2_Decompressor_data_t    *self_data = data;

    sky_object_visit(self_data->unused_data, visit_data);
}


static sky_bytes_t
sky__bz2_Decompressor_decompress(sky_object_t self, sky_object_t data)
{
    sky__bz2_Decompressor_data_t    *self_data =
                                    sky__bz2_Decompressor_data(self);

    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        char            *output, *saved_next_out;
        ssize_t         length, navail, new_output_size, output_length,
                        output_size;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        length = buffer.len;

        if (self_data->eof) {
            sky_error_raise_string(sky_EOFError,
                                   "End of stream already reached");
        }

        output_size = SMALLCHUNK;
        output = sky_asset_malloc(output_size, SKY_ASSET_CLEANUP_ON_ERROR);
        output_length = 0;

        self_data->bzs.next_in = buffer.buf;
        self_data->bzs.avail_in = SKY_MIN(length, UINT_MAX);
        length -= self_data->bzs.avail_in;
        self_data->bzs.next_out = output;
        self_data->bzs.avail_out = output_size;

        for (;;) {
            saved_next_out = self_data->bzs.next_out;
            err = BZ2_bzDecompress(&(self_data->bzs));
            sky__bz2_raise(err);

            output_length += self_data->bzs.next_out - saved_next_out;

            if (BZ_STREAM_END == err) {
                self_data->eof = SKY_TRUE;
                length += self_data->bzs.avail_in;
                if (length > 0) {
                    sky_object_gc_set(
                            SKY_AS_OBJECTP(&(self_data->unused_data)),
                            sky_bytes_createfrombytes(self_data->bzs.next_in,
                                                      length),
                            self);
                }
                break;
            }
            if (!self_data->bzs.avail_in) {
                if (!length) {
                    break;
                }
                self_data->bzs.avail_in = SKY_MIN(length, UINT_MAX);
                length -= self_data->bzs.avail_in;
            }
            if (!self_data->bzs.avail_out) {
                navail = output_size - output_length;
                if (!navail) {
                    new_output_size = output_size + (output_size >> 3) + 6;
                    if (new_output_size <= output_size) {
                        sky_error_raise_string(
                                sky_OverflowError,
                                "Unable to allocate buffer - output too large");
                    }
                    output = sky_asset_realloc(output,
                                               new_output_size,
                                               SKY_ASSET_CLEANUP_ON_ERROR,
                                               SKY_ASSET_UPDATE_FIRST_CURRENT);
                    output_size = new_output_size;
                    self_data->bzs.next_out = output + output_length;
                    navail = output_size - output_length;
                }
                self_data->bzs.avail_out = SKY_MIN(navail, UINT_MAX);
            }
        }

        result = sky_bytes_createwithbytes(output, output_length, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__bz2_Decompressor_decompress_doc[] =
"decompress(data) -> bytes\n\n"
"Provide data to the decompressor object. Returns a chunk of\n"
"decompressed data if possible, or b'' otherwise.\n\n"
"Attempting to decompress data after the end of stream is reached\n"
"raises an EOFError. Any data found after the end of the stram\n"
"is ignored and saved in the unused_dat attribute.\n";


static sky_object_t
sky__bz2_Decompressor_getstate(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "cannot serialize %#@ object",
                           sky_type_name(sky_object_type(self)));
}


static void
sky__bz2_Decompressor_init(sky_object_t self)
{
    sky__bz2_Decompressor_data_t    *self_data =
                                    sky__bz2_Decompressor_data(self);

    int err;

    if (self_data->initialized) {
        BZ2_bzDecompressEnd(&(self_data->bzs));
        self_data->initialized = SKY_FALSE;
    }

    memset(&(self_data->bzs), 0, sizeof(self_data->bzs));
    err = BZ2_bzDecompressInit(&(self_data->bzs), 0, 0);
    sky__bz2_raise(err);

    self_data->initialized = SKY_TRUE;
    self_data->eof = SKY_FALSE;

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->unused_data)),
                      sky_bytes_empty,
                      self);
}


void
skython_module__bz2_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__bz2_Compressor_instance_finalize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("BZ2Compressor"),
                                   sky__bz2_Compressor_type_doc,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky__bz2_Compressor_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__bz2_Compressor_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "compresslevel", SKY_DATA_TYPE_INT, sky_integer_create(9),
            NULL);
    sky_type_setmethodslots(type,
            "__getstate__", sky__bz2_Compressor_getstate,
            NULL);
    sky_type_setattr_builtin(
            type,
            "compress",
            sky_function_createbuiltin(
                    "BZ2Compressor.compress",
                    sky__bz2_Compressor_compress_doc,
                    (sky_native_code_function_t)sky__bz2_Compressor_compress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "flush",
            sky_function_createbuiltin(
                    "BZ2Compressor.flush",
                    sky__bz2_Compressor_flush_doc,
                    (sky_native_code_function_t)sky__bz2_Compressor_flush,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__bz2_Compressor_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "BZ2Compressor", type);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__bz2_Decompressor_instance_finalize;
    template.visit = sky__bz2_Decompressor_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("BZ2Decompressor"),
                                   sky__bz2_Decompressor_type_doc,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky__bz2_Decompressor_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__bz2_Decompressor_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(type,
            "__getstate__", sky__bz2_Decompressor_getstate,
            NULL);
    sky_type_setmembers(type,
            "eof",          "True if the end-of-stream marker has been reached.",
                            offsetof(sky__bz2_Decompressor_data_t,
                                     eof),
                            SKY_DATA_TYPE_BOOL,
                            SKY_MEMBER_FLAG_READONLY,
            "unused_data",  "Data found after the end of the compressed stream.",
                            offsetof(sky__bz2_Decompressor_data_t,
                                     unused_data),
                            SKY_DATA_TYPE_OBJECT_BYTES,
                            SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setattr_builtin(
            type,
            "decompress",
            sky_function_createbuiltin(
                    "BZ2Decompressor.decompress",
                    sky__bz2_Decompressor_decompress_doc,
                    (sky_native_code_function_t)sky__bz2_Decompressor_decompress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__bz2_Decompressor_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "BZ2Decompressor", type);
}
