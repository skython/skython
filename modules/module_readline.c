#include "../core/sky_private.h"

#if defined(HAVE_HISTEDIT_H)
#   include <histedit.h>
#endif
#if defined(HAVE_READLINE_H)
#   include <readline.h>
#endif


static char *sky_readline_completer_word_break_characters;

static int  sky_readline_begidx;
static int  sky_readline_endidx;

static int  sky_readline_history_length;
static int  sky_readline_history_start;

static sky_bool_t   sky_readline_using_editline;

static sky_object_t sky_readline_completer;
static sky_object_t sky_readline_completion_display_matches;
static sky_object_t sky_readline_pre_input;
static sky_object_t sky_readline_startup;


static int
sky_readline_get_current_history_length(void);


static void
sky_readline_free_hist_entry(HIST_ENTRY *entry)
{
#if defined(RL_READLINE_VERSION) && RL_READLINE_VERSION >= 0x0500
    histdata_t  data = free_history_entry(entry);
    free(data);
#else
    free((void *)entry->line);
    free(entry->data);
    free(entry);
#endif
}


static char *
sky_readline_completion_hook(const char *text, int state)
{
    char            *match;
    sky_bytes_t     bytes;
    sky_buffer_t    buffer;
    sky_object_t    result;
    sky_string_t    text_string;

    if (sky_object_isnull(sky_readline_completer)) {
        return NULL;
    }

    text_string = sky_string_createfrombytes(text, strlen(text), NULL, NULL);
    result = sky_object_call(sky_readline_completer,
                             sky_object_build("(Oi)", text_string, state),
                             NULL);
    if (sky_object_isnull(result)) {
        return NULL;
    }

    bytes = sky_codec_encode(NULL, result, NULL, NULL, 0);
    sky_buffer_acquire(&buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
    match = memcpy(sky_malloc(buffer.len + 1), buffer.buf, buffer.len);
    sky_buffer_release(&buffer);
    match[buffer.len] = '\0';

    return match;
}


static void
sky_readline_completion_display_matches_hook(
                char ** matches,
                int     num_matches,
                int     max_length)
{
    int         i;
    sky_list_t  list;

    list = sky_list_createwithcapacity(num_matches);
    for (i = 0; i < num_matches; ++i) {
        sky_list_append(list,
                        sky_string_createfrombytes(matches[i],
                                                   strlen(matches[i]),
                                                   NULL,
                                                   NULL));
    }

    SKY_ERROR_TRY {
        sky_object_call(sky_readline_completion_display_matches,
                        sky_object_build("(OOi)", sky_list_get(list, 0),
                                                  list,
                                                  max_length),
                        NULL);
    } SKY_ERROR_EXCEPT_ANY {
        /* pass */
    } SKY_ERROR_TRY_END;
}


static sky_bool_t   sky_readline_done;
static char         *sky_readline_line;

static void
sky_readline_handler(char *line)
    /* This is called by readline when a full line of text has been read. */
{
    sky_readline_line = line;
    sky_readline_done = SKY_TRUE;
}

static char *
sky_readline(const char *prompt)
{
    int     n;
    fd_set  rmask;

    FD_ZERO(&rmask);
    rl_callback_handler_install(prompt, sky_readline_handler);

    SKY_ERROR_TRY {
        sky_readline_done = SKY_FALSE;
        while (!sky_readline_done) {
            FD_SET(fileno(rl_instream), &rmask);
            n = select(fileno(rl_instream) + 1, &rmask, NULL, NULL, NULL);
            sky_signal_check_pending();
            if (n > 0) {
                rl_callback_read_char();
            }
        }
    } SKY_ERROR_EXCEPT_ANY {
        rl_free_line_state();
        rl_cleanup_after_signal();
        rl_callback_handler_remove();
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    rl_callback_handler_remove();
    return sky_readline_line;
}


static sky_string_t
sky_readline_input_hook(sky_string_t prompt)
{
    char            *cinput, *cprompt;
    int             length;
    sky_string_t    input;

    SKY_ASSET_BLOCK_BEGIN {
        if (!prompt) {
            cprompt = NULL;
        }
        else {
            cprompt = sky_codec_encodetocstring(prompt, 0);
            sky_asset_save(cprompt, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        if (!(cinput = sky_readline(cprompt))) {
            input = sky_string_empty;
        }
        else {
            sky_asset_save(cinput, free, SKY_ASSET_CLEANUP_ALWAYS);
            if (!*cinput) {
                input = SKY_STRING_LITERAL("\n");
            }
            else {
                input = sky_string_createfromformat("%s\n", cinput);
                length = sky_readline_get_current_history_length() +
                         sky_readline_history_start;
                if (length <= sky_readline_history_start ||
                    strcmp(history_get(length - 1)->line, cinput))
                {
                    add_history(cinput);
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return input;
}


static int
sky_readline_pre_input_hook(void)
{
    int             value;
    sky_object_t    result;

    if (sky_object_isnull(sky_readline_pre_input)) {
        return 0;
    }
    result = sky_object_call(sky_readline_pre_input, NULL, NULL);
    if (sky_object_isnull(result)) {
        return 0;
    }
    SKY_ERROR_TRY {
        value = sky_integer_value(result, INT_MIN, INT_MAX, NULL);
    }
    SKY_ERROR_EXCEPT_ANY {
        value = 0;
    } SKY_ERROR_TRY_END;

    return value;
}


static int
sky_readline_startup_hook(void)
{
    int             value;
    sky_object_t    result;

    if (sky_object_isnull(sky_readline_startup)) {
        return 0;
    }
    result = sky_object_call(sky_readline_startup, NULL, NULL);
    if (sky_object_isnull(result)) {
        return 0;
    }
    SKY_ERROR_TRY {
        value = sky_integer_value(result, INT_MIN, INT_MAX, NULL);
    }
    SKY_ERROR_EXCEPT_ANY {
        value = 0;
    } SKY_ERROR_TRY_END;

    return value;
}


static char **
sky_readline_flex_complete(const char *text, int start, int end)
    /* A more flexible constructor that saves the "begidx" and "endidx"
     * before calling the normal completer.
     */
{
#if defined(HAVE_RL_COMPLETION_APPEND_CHARACTER)
    rl_completion_append_character = '\0';
#endif
#if defined(HAVE_RL_COMPLETION_SUPPRESS_APPEND)
    rl_completion_suppress_append = 0;
#endif
    sky_readline_begidx = start;
    sky_readline_endidx = end;

    return rl_completion_matches(text, sky_readline_completion_hook);
}


static void
sky_readline_add_history(sky_string_t string)
{
    char    *cstring;

    cstring = sky_codec_encodetocstring(string, 0);
    add_history(cstring);
    sky_free(cstring);
}

static const char sky_readline_add_history_doc[] =
"add_history(string) -> None\n"
"add a line to the history buffer";


static const char sky_readline_clear_history_doc[] =
"clear_history() -> None\n"
"Clear the current readline history.";


static int
sky_readline_get_begidx(void)
{
    return sky_readline_begidx;
}

static const char sky_readline_get_begidx_doc[] =
"get_begidx() -> int\n"
"get the beginning index of the readline tab-completion scope";


static sky_object_t
sky_readline_get_completer(void)
{
    return sky_readline_completer;
}

static const char sky_readline_get_completer_doc[] =
"get_completer() -> function\n\n"
"Returns current completer function.";


static sky_string_t
sky_readline_get_completer_delims(void)
{
    sky_string_t            result;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(
                        (void **)&rl_completer_word_break_characters);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_string_createfrombytes(hazard->pointer,
                                            strlen(hazard->pointer),
                                            NULL,
                                            NULL);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_readline_get_completer_delims_doc[] =
"get_completer_delims() -> string\n"
"get the readline word delimiters for tab-completion";


static int
sky_readline_get_completion_type(void)
{
    return rl_completion_type;
}

static const char sky_readline_get_completion_type_doc[] =
"get_completion_type() -> int\n"
"Get the type of completion being attempted.";


static int
sky_readline_get_current_history_length(void)
{
    int             length;
    HISTORY_STATE   *hist_st;

    /* It may be possible to replace this with a direct use of history_length
     * instead, but it's not clear whether BSD's libedit keeps history_length
     * up to date. See CPython issue #8065.
     */
    hist_st = history_get_history_state();
    length = hist_st->length;
    /* The history docs don't say so, but the address of hist_st changes each
     * time history_get_history_state is called which makes me think it's
     * freshly malloc'd memory...  on the other hand, the address of the last
     * line stays the same as long as history isn't extended, so it appears to
     * be malloc'd but managed by the history package...
     */
    free(hist_st);

    return length;
}

static const char sky_readline_get_current_history_length_doc[] =
"get_current_history_length() -> integer\n"
"return the current (not the maximum) length of history.";


static int
sky_readline_get_endidx(void)
{
    return sky_readline_endidx;
}

static const char sky_readline_get_endidx_doc[] =
"get_endidx() -> int\n"
"get the ending index of the readline tab-completion scope";


static sky_string_t
sky_readline_get_history_item(int index)
{
    HIST_ENTRY  *entry;

    if (sky_readline_using_editline) {
        /* Older versions of libedit's readline emulation use 0-based indexes,
         * while readline and newer versions of libedit use 1-based indexes.
         */
        index = index - 1 + sky_readline_history_start;

        /* Apple's readline emulation crashes when the index is out of range,
         * therefore test for that and fail gracefully.
         */
        if (index < sky_readline_history_start ||
            index > (history_length + sky_readline_history_start))
        {
            return (sky_string_t)sky_None;
        }
    }

    if (!(entry = history_get(index))) {
        return (sky_string_t)sky_None;
    }
    return sky_string_createfrombytes(entry->line,
                                      strlen(entry->line),
                                      NULL,
                                      NULL);
}

static const char sky_readline_get_history_item_doc[] =
"get_history_item() -> string\n"
"return the current contents of history item at index.";


static int
sky_readline_get_history_length(void)
{
    return sky_readline_history_length;
}

static const char sky_readline_get_history_length_doc[] =
"get_history_length() -> int\n"
"return the maximum number of items that will be written to\n"
"the history file.";


static sky_string_t
sky_readline_get_line_buffer(void)
{
    return sky_string_createfrombytes(rl_line_buffer,
                                      strlen(rl_line_buffer),
                                      NULL,
                                      NULL);
}

static const char sky_readline_get_line_buffer_doc[] =
"get_line_buffer() -> string\n"
"return the current contents of the line buffer.";


static void
sky_readline_insert_text(sky_string_t string)
{
    char    *cstring;

    cstring = sky_codec_encodetocstring(string, 0);
    rl_insert_text(cstring);
    sky_free(cstring);
}

static const char sky_readline_insert_text_doc[] =
"insert_text(string) -> None\n"
"Insert text into the command line.";


static void
sky_readline_parse_and_bind(sky_string_t string)
{
    char    *cstring;

    cstring = sky_codec_encodetocstring(string, 0);
    rl_parse_and_bind(cstring);
    sky_free(cstring);
}

static const char sky_readline_parse_and_bind_doc[] =
"parse_and_bind(string) -> None\n"
"Parse and execute single line of a readline init file.";


static void
sky_readline_read_history_file(sky_string_t filename)
{
    SKY_ASSET_BLOCK_BEGIN {
        int             error;
        const uint8_t   *cfilename;

        if (sky_object_isnull(filename)) {
            cfilename = NULL;
        }
        else {
            sky_bytes_t bytes;

            bytes = sky_codec_encodefilename(filename);
            cfilename = sky_bytes_cstring(bytes, SKY_TRUE);
        }

        if ((error = read_history((char *)cfilename)) != 0) {
            sky_error_raise_errno(sky_OSError, error);
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_readline_read_history_file_doc[] =
"read_history_file([filename]) -> None\n"
"Load a readline history file.\n"
"The default filename is ~/.history.";


static void
sky_readline_read_init_file(sky_string_t filename)
{
    SKY_ASSET_BLOCK_BEGIN {
        int             error;
        const uint8_t   *cfilename;

        if (sky_object_isnull(filename)) {
            cfilename = NULL;
        }
        else {
            sky_bytes_t bytes;

            bytes = sky_codec_encodefilename(filename);
            cfilename = sky_bytes_cstring(bytes, SKY_TRUE);
        }

        if ((error = rl_read_init_file((char *)cfilename)) != 0) {
            sky_error_raise_errno(sky_OSError, error);
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_readline_read_init_file_doc[] =
"read_init_file([filename]) -> None\n"
"Parse a readline initialization file.\n"
"The default filename is the last filename used.";


static const char sky_readline_redisplay_doc[] =
"redisplay() -> None\n"
"Change what's displayed on the screen to reflect the current\n"
"contents of the line buffer.";


static void
sky_readline_remove_history_item(int pos)
{
    HIST_ENTRY  *entry;

    if (pos < 0) {
        sky_error_raise_string(sky_ValueError,
                               "History index cannot be negative");
    }
    if (!(entry = remove_history(pos))) {
        sky_error_raise_format(sky_ValueError,
                               "No history item at position %d",
                               pos);
    }
    sky_readline_free_hist_entry(entry);
}

static const char sky_readline_remove_history_item_doc[] =
"remove_history_item(pos) -> None\n"
"remove history item given by its position";


static void
sky_readline_replace_history_item(int pos, sky_string_t line)
{
    char        *cline;
    HIST_ENTRY  *entry;

    if (pos < 0) {
        sky_error_raise_string(sky_ValueError,
                               "History index cannot be negative");
    }

    cline = sky_codec_encodetocstring(line, 0);
    entry = replace_history_entry(pos, cline, NULL);
    sky_free(cline);

    if (!entry) {
        sky_error_raise_format(sky_ValueError,
                               "No history item at position %d",
                               pos);
    }
    sky_readline_free_hist_entry(entry);
}

static const char sky_readline_replace_history_item_doc[] =
"replace_history_item(pos, line) -> None\n"
"replaces history item given by its position with contents of line";


static void
sky_readline_set_completer(sky_object_t function)
{
    if (sky_object_isnull(function)) {
        function = NULL;
    }
    else if (!sky_object_callable(function)) {
        sky_error_raise_string(sky_TypeError,
                               "set_completer(func): argument not callable");
    }
    sky_object_gc_setlibraryroot(&sky_readline_completer,
                                 function,
                                 SKY_TRUE);
}

static const char sky_readline_set_completer_doc[] =
"set_completer([function]) -> None\n"
"Set or remove the completer function.\n"
"The function is called as function(text, state),\n"
"for state in 0, 1, 2, ..., until it returns a non-string.\n"
"It should return the next possible completion starting with 'text'.";


static void
sky_readline_set_completer_delims(sky_string_t delims)
{
    char    *cdelims, *old_cdelims;

    cdelims = sky_codec_encodetocstring(delims, 0);

    rl_completer_word_break_characters = cdelims;
    old_cdelims = sky_atomic_exchange(
                        cdelims,
                        (void **)&sky_readline_completer_word_break_characters);

    if (old_cdelims) {
        sky_hazard_pointer_retire(old_cdelims, sky_free);
    }
}

static const char sky_readline_set_completer_delims_doc[] =
"set_completer_delims(string) -> None\n"
"set the readline word delimiters for tab-completion";


static void
sky_readline_set_completion_display_matches_hook(sky_object_t function)
{
    if (sky_object_isnull(function)) {
        function = NULL;
    }
    else if (!sky_object_callable(function)) {
        sky_error_raise_string(
                sky_TypeError,
                "set_completion_display_matches_hook(func): argument not callable");
    }
    sky_object_gc_setlibraryroot(&sky_readline_completion_display_matches,
                                 function,
                                 SKY_TRUE);

    if (sky_object_isnull(sky_readline_completion_display_matches)) {
        rl_completion_display_matches_hook = NULL;
    }
    else {
        rl_completion_display_matches_hook =
                (VFunction *)sky_readline_completion_display_matches_hook;
    }
}

static const char sky_readline_set_completion_display_matches_hook_doc[] =
"set_completion_display_matches_hook([function]) -> None\n"
"Set or remove the completion display function.\n"
"The function is called as\n"
"  function(substitution, [matches], longest_match_length)\n"
"once each time matches need to be displayed.";


static void
sky_readline_set_history_length(int length)
{
    sky_readline_history_length = length;
}

static const char sky_readline_set_history_length_doc[] =
"set_history_length(length) -> None\n"
"set the maximal number of items which will be written to\n"
"the history file. A negative length is used to inhibit\n"
"history truncation.";


static void
sky_readline_set_pre_input_hook(sky_object_t function)
{
    if (sky_object_isnull(function)) {
        function = NULL;
    }
    else if (!sky_object_callable(function)) {
        sky_error_raise_string(
                sky_TypeError,
                "set_pre_input_hook(func): argument not callable");
    }
    sky_object_gc_setlibraryroot(&sky_readline_pre_input,
                                 function,
                                 SKY_TRUE);
}

static const char sky_readline_set_pre_input_hook_doc[] =
"set_pre_input_hook([function]) -> None\n"
"Set or remove the pre_input_hook function.\n"
"The function is called with no arguments after the first prompt\n"
"has been printed and just before readline starts reading input\n"
"characters.";


static void
sky_readline_set_startup_hook(sky_object_t function)
{
    if (sky_object_isnull(function)) {
        function = NULL;
    }
    else if (!sky_object_callable(function)) {
        sky_error_raise_string(
                sky_TypeError,
                "set_startup_hook(func): argument not callable");
    }
    sky_object_gc_setlibraryroot(&sky_readline_startup,
                                 function,
                                 SKY_TRUE);
}

static const char sky_readline_set_startup_hook_doc[] =
"set_startup_hook([function]) -> None\n"
"Set or remove the startup_hook function.\n"
"The function is called with no arguments just\n"
"before readline prints the first prompt.";


static void
sky_readline_write_history_file(sky_string_t filename)
{
    SKY_ASSET_BLOCK_BEGIN {
        int             error;
        const uint8_t   *cfilename;

        if (sky_object_isnull(filename)) {
            cfilename = NULL;
        }
        else {
            sky_bytes_t bytes;

            bytes = sky_codec_encodefilename(filename);
            cfilename = sky_bytes_cstring(bytes, SKY_TRUE);
        }

        if ((error = write_history((char *)cfilename)) != 0) {
            sky_error_raise_errno(sky_OSError, error);
        }
        else if (sky_readline_history_length >= 0) {
            history_truncate_file((char *)cfilename,
                                  sky_readline_history_length);
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_readline_write_history_file_doc[] =
"write_history_file([filename]) -> None\n"
"Save a readline history file.\n"
"The default filename is ~/.history.";


void
skython_module_readline_initialize(sky_module_t module)
{
    static const char module_doc[] =
"Importing this module enables command line editing using GNU readline.";
#if defined(HAVE_LIBEDIT)
    static const char module_libedit_doc[] =
"Importing this module enables command line editing using libedit readline.";
#endif

    sky_string_t    doc;

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);

    sky_module_setattr(
            module,
            "add_history",
            sky_function_createbuiltin(
                    "add_history",
                    sky_readline_add_history_doc,
                    (sky_native_code_function_t)sky_readline_add_history,
                    SKY_DATA_TYPE_VOID,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "clear_history",
            sky_function_createbuiltin(
                    "clear_history",
                    sky_readline_clear_history_doc,
                    (sky_native_code_function_t)clear_history,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "get_begidx",
            sky_function_createbuiltin(
                    "get_begidx",
                    sky_readline_get_begidx_doc,
                    (sky_native_code_function_t)sky_readline_get_begidx,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "get_completer",
            sky_function_createbuiltin(
                    "get_completer",
                    sky_readline_get_completer_doc,
                    (sky_native_code_function_t)sky_readline_get_completer,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "get_completer_delims",
            sky_function_createbuiltin(
                    "get_completer_delims",
                    sky_readline_get_completer_delims_doc,
                    (sky_native_code_function_t)sky_readline_get_completer_delims,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "get_completion_type",
            sky_function_createbuiltin(
                    "get_completion_type",
                    sky_readline_get_completion_type_doc,
                    (sky_native_code_function_t)sky_readline_get_completion_type,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "get_current_history_length",
            sky_function_createbuiltin(
                    "get_current_history_length",
                    sky_readline_get_current_history_length_doc,
                    (sky_native_code_function_t)sky_readline_get_current_history_length,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "get_endidx",
            sky_function_createbuiltin(
                    "get_endidx",
                    sky_readline_get_endidx_doc,
                    (sky_native_code_function_t)sky_readline_get_endidx,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "get_history_item",
            sky_function_createbuiltin(
                    "get_history_item",
                    sky_readline_get_history_item_doc,
                    (sky_native_code_function_t)sky_readline_get_history_item,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "index", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "get_history_length",
            sky_function_createbuiltin(
                    "get_history_length",
                    sky_readline_get_history_length_doc,
                    (sky_native_code_function_t)sky_readline_get_history_length,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "get_line_buffer",
            sky_function_createbuiltin(
                    "get_line_buffer",
                    sky_readline_get_line_buffer_doc,
                    (sky_native_code_function_t)sky_readline_get_line_buffer,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "insert_text",
            sky_function_createbuiltin(
                    "insert_text",
                    sky_readline_insert_text_doc,
                    (sky_native_code_function_t)sky_readline_insert_text,
                    SKY_DATA_TYPE_VOID,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "parse_and_bind",
            sky_function_createbuiltin(
                    "parse_and_bind",
                    sky_readline_parse_and_bind_doc,
                    (sky_native_code_function_t)sky_readline_parse_and_bind,
                    SKY_DATA_TYPE_VOID,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "read_history_file",
            sky_function_createbuiltin(
                    "read_history_file",
                    sky_readline_read_history_file_doc,
                    (sky_native_code_function_t)sky_readline_read_history_file,
                    SKY_DATA_TYPE_VOID,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "read_init_file",
            sky_function_createbuiltin(
                    "read_init_file",
                    sky_readline_read_init_file_doc,
                    (sky_native_code_function_t)sky_readline_read_init_file,
                    SKY_DATA_TYPE_VOID,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "redisplay",
            sky_function_createbuiltin(
                    "redisplay",
                    sky_readline_redisplay_doc,
                    (sky_native_code_function_t)rl_redisplay,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "remove_history_item",
            sky_function_createbuiltin(
                    "remove_history_item",
                    sky_readline_remove_history_item_doc,
                    (sky_native_code_function_t)sky_readline_remove_history_item,
                    SKY_DATA_TYPE_VOID,
                    "pos", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "replace_history_item",
            sky_function_createbuiltin(
                    "replace_history_item",
                    sky_readline_replace_history_item_doc,
                    (sky_native_code_function_t)sky_readline_replace_history_item,
                    SKY_DATA_TYPE_VOID,
                    "pos", SKY_DATA_TYPE_INT, NULL,
                    "line", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "set_completer",
            sky_function_createbuiltin(
                    "set_completer",
                    sky_readline_set_completer_doc,
                    (sky_native_code_function_t)sky_readline_set_completer,
                    SKY_DATA_TYPE_VOID,
                    "function", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "set_completer_delims",
            sky_function_createbuiltin(
                    "set_completer_delims",
                    sky_readline_set_completer_delims_doc,
                    (sky_native_code_function_t)sky_readline_set_completer_delims,
                    SKY_DATA_TYPE_VOID,
                    "delims", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "set_completion_display_matches_hook",
            sky_function_createbuiltin(
                    "set_completion_display_matches_hook",
                    sky_readline_set_completion_display_matches_hook_doc,
                    (sky_native_code_function_t)sky_readline_set_completion_display_matches_hook,
                    SKY_DATA_TYPE_VOID,
                    "function", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "set_history_length",
            sky_function_createbuiltin(
                    "set_history_length",
                    sky_readline_set_history_length_doc,
                    (sky_native_code_function_t)sky_readline_set_history_length,
                    SKY_DATA_TYPE_VOID,
                    "length", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "set_pre_input_hook",
            sky_function_createbuiltin(
                    "set_pre_input_hook",
                    sky_readline_set_pre_input_hook_doc,
                    (sky_native_code_function_t)sky_readline_set_pre_input_hook,
                    SKY_DATA_TYPE_VOID,
                    "function", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "set_startup_hook",
            sky_function_createbuiltin(
                    "set_startup_hook",
                    sky_readline_set_startup_hook_doc,
                    (sky_native_code_function_t)sky_readline_set_startup_hook,
                    SKY_DATA_TYPE_VOID,
                    "function", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "write_history_file",
            sky_function_createbuiltin(
                    "write_history_file",
                    sky_readline_write_history_file_doc,
                    (sky_native_code_function_t)sky_readline_write_history_file,
                    SKY_DATA_TYPE_VOID,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));


#if defined(HAVE_LIBEDIT)
    sky_readline_using_editline = SKY_FALSE;
    if (!strcmp(rl_library_version, "EditLine wrapper")) {
        sky_readline_using_editline = SKY_TRUE;

        doc = sky_string_createfrombytes(module_libedit_doc,
                                         sizeof(module_libedit_doc) - 1,
                                         NULL,
                                         NULL);
        sky_module_setattr(module, "__doc__", doc);
    }
#endif

    /* the libedit readline emulation resets key bindings etc
     * when calling rl_initialize.  So call it upfront
     */
    if (sky_readline_using_editline) {
        rl_initialize();

        /* Detect if libedit's readline emulation uses 0-based indexing or
         * 1-based indexing.
         */
        add_history("1");
        if (history_get(1)) {
            sky_readline_history_start = 1;
        }
        else {
            sky_readline_history_start = 0;
        }
        clear_history();
    }

    using_history();
    sky_readline_history_length = -1;

    rl_readline_name = "skython";
    rl_bind_key('\t', rl_insert);
    rl_bind_key_in_map('\t', rl_complete, emacs_meta_keymap);
    rl_bind_key_in_map('\033', rl_complete, emacs_meta_keymap);
    rl_startup_hook = (Function *)sky_readline_startup_hook;
    rl_pre_input_hook = (Function *)sky_readline_pre_input_hook;

    sky_readline_begidx = sky_readline_endidx = 0;
    rl_attempted_completion_function = (CPPFunction *)
                                       sky_readline_flex_complete;

    sky_readline_completer_word_break_characters =
    rl_completer_word_break_characters =
            sky_strdup(" \t\n`~!@#$%^&*()-=+[{]}\\|;:'\",<>/?");

    if (sky_readline_using_editline) {
        rl_read_init_file(NULL);
    }
    rl_initialize();

    sky_interpreter_setinputhook(sky_readline_input_hook);
}
