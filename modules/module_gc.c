#include "../core/sky_private.h"


static ssize_t
sky_gc_collect(int generation)
{
    /* For compatibility with CPython; however, generation is ignored. */
    if (generation < 0 || generation > 2) {
        sky_error_raise_string(sky_ValueError, "invalid generation");
    }

    /* Skython garbage collection runs in the background without stopping any
     * threads from running. There's no infrastructure in place for performing
     * a synchronous collection. Request that a collection runs, and return 0.
     */
    sky_object_gc_collect();

    return 0;
}

static const char sky_gc_collect_doc[] =
"collect([generation]) -> n\n"
"\n"
"With no arguments, run a full collection.  The optional argument\n"
"may be an integer specifying which generation to collect.  A ValueError\n"
"is raised if the generation number is invalid.\n"
"\n"
"The number of unreachable objects is returned.\n";


static const char sky_gc_disable_doc[] =
"disable() -> None\n"
"\n"
"Disable automatic garbage collection.\n";


static const char sky_gc_enable_doc[] =
"enable() -> None\n"
"\n"
"Enable automatic garbage collection.\n";


static sky_tuple_t
sky_gc_get_count(void)
{
    uintmax_t   count = sky_object_gc_count();

    /* Skython only has one generation, but CPython has three. */
    return sky_tuple_pack(3, sky_integer_createfromunsigned(count),
                             sky_integer_zero,
                             sky_integer_zero);
}

static const char sky_gc_get_count_doc[] =
"get_count() -> (count0, count1, count2)\n"
"\n"
"Return the current collection counts\n";


static sky_tuple_t
sky_gc_get_threshold(void)
{
    uint32_t    threshold = sky_object_gc_getthreshold();

    return sky_tuple_pack(3, sky_integer_createfromunsigned(threshold),
                             sky_integer_zero,
                             sky_integer_zero);
}

static const char sky_gc_get_threshold_doc[] =
"get_threshold() -> (threshold0, threshold1, threshold2)\n"
"\n"
"Return the current collection thresholds\n";


static const char sky_gc_isenabled_doc[] =
"isenabled() -> status\n"
"\n"
"Returns true if automatic garbage collection is enabled.\n";


static sky_bool_t
sky_gc_is_tracked(sky_object_t obj)
{
    return !sky_object_istagged(obj);
}

static const char sky_gc_is_tracked_doc[] =
"is_tracked(obj) -> bool\n"
"\n"
"Returns true if the object is tracked by the garbage collector.\n"
"Simple atomic objects will return false.\n";


static void
sky_gc_set_threshold(
                uint32_t    threshold0,
    SKY_UNUSED  uint32_t    threshold1,
    SKY_UNUSED  uint32_t    threshold2)
{
    sky_object_gc_setthreshold(threshold0);
}

static const char sky_gc_set_threshold_doc[] =
"set_threshold(threshold0, [threshold1, threshold2]) -> None\n"
"\n"
"Sets the collection thresholds.  Setting threshold to zero disables\n"
"collection.\n";


void
skython_module_gc_initialize(sky_module_t module)
{
    const char doc[] =
"This module provides access to the garbage collector for reference cycles.\n"
"\n"
"enable() -- Enable automatic garbage collection.\n"
"disable() -- Disable automatic garbage collection.\n"
"isenabled() -- Returns true if automatic collection is enabled.\n"
"collect() -- Do a full collection right now.\n"
"get_count() -- Return the current collection counts.\n"
"set_debug() -- Set debugging flags.\n"
"get_debug() -- Get debugging flags.\n"
"set_threshold() -- Set the collection thresholds.\n"
"get_threshold() -- Return the current the collection thresholds.\n"
"get_objects() -- Return a list of all objects tracked by the collector.\n"
"is_tracked() -- Returns true if a given object is tracked.\n"
"get_referrers() -- Return the list of objects that refer to an object.\n"
"get_referents() -- Return the list of objects that an object refers to.\n";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_module_setattr(
            module,
            "collect",
            sky_function_createbuiltin(
                    "collect",
                    sky_gc_collect_doc,
                    (sky_native_code_function_t)sky_gc_collect,
                    SKY_DATA_TYPE_SSIZE_T,
                    "generation", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "disable",
            sky_function_createbuiltin(
                    "disable",
                    sky_gc_disable_doc,
                    (sky_native_code_function_t)sky_object_gc_disable,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "enable",
            sky_function_createbuiltin(
                    "enable",
                    sky_gc_enable_doc,
                    (sky_native_code_function_t)sky_object_gc_enable,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "get_count",
            sky_function_createbuiltin(
                    "get_count",
                    sky_gc_get_count_doc,
                    (sky_native_code_function_t)sky_gc_get_count,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    /* TODO get_debug() */
    /* TODO get_objects() */
    /* TODO get_referrers() */
    /* TODO get_referents() */
    sky_module_setattr(
            module,
            "get_threshold",
            sky_function_createbuiltin(
                    "get_threshold",
                    sky_gc_get_threshold_doc,
                    (sky_native_code_function_t)sky_gc_get_threshold,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(
            module,
            "isenabled",
            sky_function_createbuiltin(
                    "isenabled",
                    sky_gc_isenabled_doc,
                    (sky_native_code_function_t)sky_object_gc_isenabled,
                    SKY_DATA_TYPE_BOOL,
                    NULL));
    sky_module_setattr(
            module,
            "is_tracked",
            sky_function_createbuiltin(
                    "is_tracked",
                    sky_gc_is_tracked_doc,
                    (sky_native_code_function_t)sky_gc_is_tracked,
                    SKY_DATA_TYPE_BOOL,
                    "obj", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    /* TODO set_debug() */
    sky_module_setattr(
            module,
            "set_threshold",
            sky_function_createbuiltin(
                    "set_threshold",
                    sky_gc_set_threshold_doc,
                    (sky_native_code_function_t)sky_gc_set_threshold,
                    SKY_DATA_TYPE_VOID,
                    "threshold0", SKY_DATA_TYPE_UINT32, NULL,
                    "threshold1", SKY_DATA_TYPE_UINT32, sky_integer_zero,
                    "threshold2", SKY_DATA_TYPE_UINT32, sky_integer_zero,
                    NULL));

    /* Not used, but here for compatibility with CPython. */
    sky_module_setattr(module, "garbage", sky_list_create(NULL));

    /* TODO callbacks */

    /* TODO DEBUG_STATS */
    /* TODO DEBUG_COLLECTABLE */
    /* TODO DEBUG_UNCOLLECTABLE */
    /* TODO DEBUG_SAVEALL */
    /* TODO DEBUG_LEAK */
}
