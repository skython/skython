#include "../core/sky_private.h"
#include "../core/sky_code_private.h"
#include "../core/sky_frame_private.h"
#include "../core/sky_module_private.h"

#include "../core/sky_python_parser.h"
#include "../core/sky_python_compiler.h"
#include "../core/sky_traceback.h"


#if !defined(NDEBUG)
static void
sky_builtins___breakpoint__(void)
{
    sky_format_fprintf(stderr, "*** BREAKPOINT ***\n");
}
#endif


static sky_module_t
sky_builtins___import__(
                sky_string_t    name,
                sky_dict_t      globals,
    SKY_UNUSED  sky_object_t    locals,
                sky_object_t    fromlist,
                ssize_t         level)
{
    ssize_t         last_dot, length, level_up;
    sky_dict_t      modules;
    sky_tuple_t     partition;
    sky_module_t    module;
    sky_object_t    builtins, builtins_import, importlib, initializing, object,
                    package;
    sky_string_t    absolute_name, dot, head, import_name;

    if (sky_object_isnull(globals)) {
        globals = sky_dict_create();
    }
    if (sky_object_isnull(fromlist)) {
        fromlist = sky_tuple_empty;
    }
    else {
        fromlist = sky_tuple_create(fromlist);
    }
    if (level < 0) {
        sky_error_raise_string(sky_ValueError, "level must be >= 0");
    }
    dot = SKY_STRING_LITERAL(".");

    builtins = sky_interpreter_builtins();
    modules = sky_interpreter_modules();
    if (!(importlib = sky_interpreter_importlib())) {
        sky_error_raise_string(sky_RuntimeError,
                               "module importing is disabled.");
    }

    if (!level) {
        if (!sky_object_bool(name)) {
            sky_error_raise_string(sky_ValueError, "Empty module name");
        }
        package = sky_None;
        absolute_name = name;
    }
    else {
        package = sky_dict_get(globals,
                               SKY_STRING_LITERAL("__package__"),
                               NULL);
        if (!sky_object_isnull(package)) {
            if (!sky_object_isa(package, sky_string_type)) {
                sky_error_raise_string(sky_TypeError,
                                       "__package__ must be a string");
            }
        }
        else {
            package = sky_dict_getitem(globals,
                                       SKY_STRING_LITERAL("__name__"));
            if (!package) {
                sky_error_raise_string(sky_KeyError,
                                       "'__name__' not in globals");
            }
            if (!sky_object_isa(package, sky_string_type)) {
                sky_error_raise_string(sky_TypeError,
                                       "__name__ must be a string");
            }
            if (!sky_dict_get(globals, SKY_STRING_LITERAL("__path__"), NULL)) {
                partition = sky_string_rpartition(package, dot);
                package = sky_tuple_get(partition, 0);
            }
        }
        if (!sky_dict_get(modules, package, NULL)) {
            sky_error_raise_format(
                    sky_SystemError,
                    "Parent module %#@ not loaded, cannot perform relative import",
                    package);
        }

        last_dot = sky_string_len(package);
        for (level_up = 1; level_up < level; ++level_up) {
            last_dot = sky_string_rfind(package, dot, 0, last_dot);
            if (-1 == last_dot) {
                sky_error_raise_string(
                        sky_ValueError,
                        "attempted relative import beyond top-level package");
            }
        }
        if (!sky_object_bool(name)) {
            absolute_name = sky_string_slice(package, 0, last_dot, 1);
        }
        else {
            sky_string_builder_t    builder;

            length = last_dot + 1 + sky_object_len(name);
            builder = sky_string_builder_createwithcapacity(length);
            sky_string_builder_appendslice(builder, package, last_dot, 0, 1);
            sky_string_builder_append(builder, dot);
            sky_string_builder_append(builder, name);
            absolute_name = sky_string_builder_finalize(builder);
        }
    }

    import_name = SKY_STRING_LITERAL("__import__");
    if (!(builtins_import = sky_dict_get(globals, import_name, NULL))) {
        builtins_import = sky_object_getitem(builtins, import_name);
    }

    SKY_ERROR_TRY {
        if (!sky_module_import_lock_acquire()) {
            sky_error_raise_string(sky_RuntimeError,
                                   "could not acquire import lock");
        }
        if (!(module = sky_dict_get(modules, absolute_name, NULL))) {
            /* _find_and_load() releases the import lock */
            module = sky_object_callmethod(importlib,
                                           SKY_STRING_LITERAL("_find_and_load"),
                                           sky_tuple_pack(2, absolute_name,
                                                             builtins_import),
                                           NULL);
        }
        else if (sky_object_isnull(module)) {
            if (!sky_module_import_lock_release()) {
                sky_error_raise_string(sky_RuntimeError,
                                       "not holding the import lock");
            }
            sky_error_raise_format(sky_ImportError,
                                   "import of %#@ halted; None in sys.modules",
                                   absolute_name);
        }
        else {
            initializing =
                    sky_object_getattr(module, 
                                       SKY_STRING_LITERAL("__initializing__"),
                                       sky_False);
            if (sky_object_bool(initializing)) {
                /* _lock_unlock_module() releases the import lock */
                sky_object_callmethod(importlib,
                                      SKY_STRING_LITERAL("_lock_unlock_module"),
                                      sky_tuple_pack(1, absolute_name),
                                      NULL);
            }
            else if (!sky_module_import_lock_release()) {
                sky_error_raise_string(sky_RuntimeError,
                                       "not holding the import lock");
            }
        }
        /* the import lock is no longer held */
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_record_t  *error = sky_error_current();

        sky_object_gc_setroot(
                SKY_AS_OBJECTP(&(error->traceback)),
                sky_traceback_filter_importlib(error->traceback, error->type),
                SKY_FALSE);
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    if (sky_object_bool(fromlist)) {
        SKY_ERROR_TRY {
            object = sky_object_callmethod(importlib,
                                         SKY_STRING_LITERAL("_handle_fromlist"),
                                         sky_tuple_pack(3, module,
                                                           fromlist,
                                                           builtins_import),
                                         NULL);
        } SKY_ERROR_EXCEPT_ANY {
            sky_error_record_t  *error = sky_error_current();

            sky_object_gc_setroot(
                    SKY_AS_OBJECTP(&(error->traceback)),
                    sky_traceback_filter_importlib(error->traceback,
                                                   error->type),
                    SKY_FALSE);
            sky_error_raise();
        } SKY_ERROR_TRY_END;

        return object;
    }

    /* There's no fromlist, but it was a relative import, so simply return the
     * module.
     */
    if (level && !sky_object_bool(name)) {
        return module;
    }

    /* Return the root module. That is, for "module.submodule", return "module"
     * rather than "submodule".
     */
    partition = sky_string_partition(name, dot);
    if (!sky_object_bool(sky_tuple_get(partition, 1))) {
        return module;
    }
    head = sky_tuple_get(partition, 0);

    if (!level) {
        return sky_object_call(builtins_import, sky_tuple_pack(1, head), NULL);
    }

    length = sky_object_len(absolute_name) -
             (sky_object_len(name) - sky_object_len(head));
    name = sky_string_slice(absolute_name, 0, length, 1);
    if (!(module = sky_dict_get(modules, name, NULL))) {
        sky_error_raise_format(sky_KeyError,
                               "%#@ not in sys.modules as expected",
                               name);
    }
    return module;
}


static sky_string_t
sky_builtins_bin(sky_object_t number)
{
    return sky_object_format(sky_number_index(number),
                             SKY_STRING_LITERAL("#b"));
}


static sky_string_t
sky_builtins_chr(int32_t i)
{
    if (i < SKY_UNICODE_CHAR_MIN || i > SKY_UNICODE_CHAR_MAX) {
        sky_error_raise_format(sky_ValueError,
                               "chr() arg not in range(%d, %d)",
                               SKY_UNICODE_CHAR_MIN,
                               SKY_UNICODE_CHAR_MAX + 1);
    }
    return sky_string_createfromcodepoint(i);
}


static sky_object_t
sky_builtins_compile(
                sky_object_t    source,
                sky_string_t    filename,
                sky_string_t    mode,
                unsigned int    flags,
                sky_bool_t      dont_inherit,
                int             optimize)
{
    const char                  *mode_cstring;
    sky_object_t                ast, result;
    sky_tlsdata_t               *tlsdata;
    sky_code_data_t             *code_data;
    sky_frame_data_t            *frame_data;
    sky_python_parser_t         parser;
    sky_python_compiler_mode_t  compiler_mode;

    if (!sky_object_isa(source, sky_string_type) &&
        !sky_buffer_check(source) &&
        !sky_object_isa(source, sky_python_ast_AST_type))
    {
        sky_error_raise_string(
                sky_TypeError,
                "compile() arg 1 must be a string, bytes, or AST object");
    }

    if ((mode_cstring = sky_string_cstring(mode)) != NULL) {
        if (!strcmp(mode_cstring, "exec")) {
            compiler_mode = SKY_PYTHON_COMPILER_MODE_FILE;
        }
        else if (!strcmp(mode_cstring, "eval")) {
            compiler_mode = SKY_PYTHON_COMPILER_MODE_EVAL;
        }
        else if (!strcmp(mode_cstring, "single")) {
            compiler_mode = SKY_PYTHON_COMPILER_MODE_INTERACTIVE;
        }
        else {
            sky_error_raise_string(
                    sky_ValueError,
                    "compile() arg 3 must be 'exec', 'eval' or 'single'");
        }
    }
    else {
        sky_error_raise_string(
                sky_ValueError,
                "compile() arg 3 must be 'exec', 'eval' or 'single'");
    }

    if (flags & ~(SKY_CODE_FLAG_FUTURE_MASK |
                  SKY_CODE_FLAG_OBSOLETE_MASK |
                  SKY_PYTHON_COMPILER_FLAG_DONT_IMPLY_DEDENT |
                  SKY_PYTHON_COMPILER_FLAG_ONLY_AST))
    {
        sky_error_raise_string(sky_ValueError,
                               "compile(): unrecognized flags");
    }

    if (-1 == optimize) {
        optimize = sky_interpreter_optlevel();
    }
    else if (optimize < -1 || optimize > 2) {
        sky_error_raise_string(sky_ValueError,
                               "compile(): invalid optimize value");
    }
    if (!dont_inherit) {
        tlsdata = sky_tlsdata_get();
        if (tlsdata->current_frame) {
            frame_data = sky_frame_data(tlsdata->current_frame);
            code_data = sky_code_data(frame_data->f_code);
            flags |= (code_data->co_flags & SKY_CODE_FLAG_FUTURE_MASK);
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isa(source, sky_python_ast_AST_type)) {
            if (!(flags & SKY_PYTHON_COMPILER_FLAG_ONLY_AST)) {
                sky_python_ast_validate(source);
            }
            ast = source;
        }
        else {
            sky_python_parser_init(&parser,
                                   source,
                                   NULL,
                                   filename,
                                   NULL,
                                   NULL,
                                   flags);
            sky_asset_save(&parser,
                           (sky_free_t)sky_python_parser_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);

            switch (compiler_mode) {
                case SKY_PYTHON_COMPILER_MODE_FILE:
                    ast = sky_python_parser_file_input(&parser);
                    break;
                case SKY_PYTHON_COMPILER_MODE_EVAL:
                    ast = sky_python_parser_eval_input(&parser);
                    break;
                case SKY_PYTHON_COMPILER_MODE_INTERACTIVE:
                    ast = sky_python_parser_interactive_input(&parser);
                    break;
            }
        }
        if (flags & SKY_PYTHON_COMPILER_FLAG_ONLY_AST) {
            result = ast;
        }
        else {
            result = sky_python_compiler_compile(ast,
                                                 filename,
                                                 compiler_mode,
                                                 flags,
                                                 optimize);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_list_t
sky_builtins_dir(sky_object_t object)
{
    sky_list_t      list;
    sky_object_t    keys, locals;

    if (!object) {
        locals = sky_interpreter_locals();
        if (sky_dict_type == sky_object_type(locals)) {
            keys = sky_dict_keys(locals);
        }
        else {
            keys = sky_object_callmethod(locals,
                                         SKY_STRING_LITERAL("keys"),
                                         NULL,
                                         NULL);
        }
        list = sky_list_create(keys);
    }
    else {
        list = sky_object_dir(object);
    }
    sky_list_sort(list, NULL, SKY_FALSE);

    return list;
}


static sky_string_t
sky_builtins_hex(sky_object_t number)
{
    return sky_object_format(sky_number_index(number),
                             SKY_STRING_LITERAL("#x"));
}


static sky_object_t
sky_builtins_id(sky_object_t object)
{
    return sky_integer_create((intmax_t)(intptr_t)object);
}


static sky_object_t
sky_builtins_iter(sky_object_t iterable, sky_object_t sentinel)
    /* iter(iterable)
     * iter(callable, sentinel)
     *
     * The second form really is insane. It's specifically called out in the
     * original PEP for iterators (PEP 234) as being ugly, but since nobody
     * came up with a better idea, it's what it is.
     */
{
    if (!sentinel) {
        return sky_object_iter(iterable);
    }

    if (!sky_object_callable(iterable)) {
        sky_error_raise_string(sky_TypeError,
                               "iter(v, w): v must be callable");
    }

    return sky_callable_iterator_create(iterable, sentinel);
}


static sky_object_t
sky_builtins_max(sky_tuple_t args, sky_object_t key)
{
    if (!sky_tuple_len(args)) {
        sky_error_raise_string(sky_TypeError,
                               "max() requires at least 1 argument");
    }
    if (1 == sky_object_len(args)) {
        return sky_sequence_max(sky_tuple_get(args, 0), key);
    }
    return sky_sequence_max(args, key);
}


static sky_object_t
sky_builtins_min(sky_tuple_t args, sky_object_t key)
{
    if (!sky_tuple_len(args)) {
        sky_error_raise_string(sky_TypeError,
                               "min() requires at least 1 argument");
    }
    if (1 == sky_object_len(args)) {
        return sky_sequence_min(sky_tuple_get(args, 0), key);
    }
    return sky_sequence_min(args, key);
}


static sky_string_t
sky_builtins_oct(sky_object_t number)
{
    return sky_object_format(sky_number_index(number),
                             SKY_STRING_LITERAL("#o"));
}


static uint32_t
sky_builtins_ord(sky_object_t c)
{
    uint32_t        byte;
    sky_buffer_t    buffer;

    if (sky_object_isa(c, sky_string_type)) {
        if (1 == sky_object_len(c)) {
            return sky_string_charat(c, 0);
        }
    }
    else if (sky_buffer_check(c)) {
        sky_buffer_acquire(&buffer, c, SKY_BUFFER_FLAG_SIMPLE);
        if (1 == buffer.len) {
            byte = *(uint8_t *)buffer.buf;
            sky_buffer_release(&buffer);
            return byte;
        }
        sky_buffer_release(&buffer);
    }
    sky_error_raise_format(sky_TypeError,
                           "ord() expected string of length 1, but %@ found",
                           sky_type_name(sky_object_type(c)));
}


static void
sky_builtins_print(
                sky_tuple_t     args,
                sky_object_t    sep,
                sky_object_t    end,
                sky_object_t    file,
                sky_bool_t      flush)
{
    ssize_t         count, i;
    sky_object_t    write_method;
    sky_string_t    s;

    if (!file) {
        file = sky_object_getattr(sky_interpreter_module_sys(),
                                  SKY_STRING_LITERAL("stdout"),
                                  sky_NotSpecified);
    }

    write_method = sky_object_getattr(file,
                                      SKY_STRING_LITERAL("write"),
                                      sky_NotSpecified);
    if ((count = sky_tuple_len(args)) > 0) {
        if (sky_object_isnull(sep)) {
            sep = SKY_STRING_LITERAL(" ");
        }
        sep = (sky_object_bool(sep) ? sky_tuple_pack(1, sep) : NULL);
        s = sky_object_str(sky_tuple_get(args, 0));
        sky_object_call(write_method, sky_tuple_pack(1, s), NULL);
        for (i = 1; i < count; ++i) {
            if (sep) {
                sky_object_call(write_method, sep, NULL);
            }
            s = sky_object_str(sky_tuple_get(args, i));
            sky_object_call(write_method, sky_tuple_pack(1, s), NULL);
        }
    }
    if (sky_object_isnull(end)) {
        end = SKY_STRING_LITERAL("\n");
    }
    if (sky_object_bool(end)) {
        sky_object_call(write_method, sky_tuple_pack(1, end), NULL);
    }
    if (flush) {
        sky_object_callmethod(file, SKY_STRING_LITERAL("flush"), NULL, NULL);
    }
}


static sky_list_t
sky_builtins_sorted(sky_object_t iterable, sky_tuple_t args, sky_dict_t kws)
{
    sky_list_t  list;

    list = sky_list_create(iterable);
    sky_object_callmethod(list, SKY_STRING_LITERAL("sort"), args, kws);

    return list;
}


static sky_object_t
sky_builtins_vars(sky_object_t object)
{
    sky_dict_t  dict;

    if (!object) {
        return sky_interpreter_locals();
    }
    if (!(dict = sky_object_getattr(object,
                                    SKY_STRING_LITERAL("__dict__"),
                                    NULL)))
    {
        sky_error_raise_string(sky_TypeError,
                               "vars() argument must have __dict__ attribute");
    }

    return dict;
}


void
skython_module_builtins_initialize(sky_module_t module)
{
    static const char module_doc[] =
"Built-in functions, exceptions, and other objects.\n"
"\n"
"Noteworthy: None is the `nil' object; Ellipsis represents `...' in slices.";

    sky_string_t    doc;

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);
    if (!sky_interpreter_optlevel()) {
        sky_module_setattr(module, "__debug__", sky_True);
    }
    else {
        sky_module_setattr(module, "__debug__", sky_False);
    }

    /* Built-in methods */
#if !defined(NDEBUG)
    sky_module_setattr(
            module,
            "__breakpoint__",
            sky_function_createbuiltin(
                    "__breakpoint__",
                    NULL,
                    (sky_native_code_function_t)sky_builtins___breakpoint__,
                    SKY_DATA_TYPE_VOID,
                    NULL));
#endif
    /* TODO __build_class__ */
    sky_module_setattr(
            module,
            "__import__",
            sky_function_createbuiltin(
                    "__import__",
                    "__import__(name, globals=None, locals=None, fromlist=(), level=0) -> module\n\n"
                    "Import a module. Because this function is meant for use by the Python\n"
                    "interpreter and not for general use it is better to use\n"
                    "importlib.import_module() to programmatically import a module.\n"
                    "\n"
                    "The globals argument is only used to determine the context;\n"
                    "they are not modified.  The locals argument is unused.  The fromlist\n"
                    "should be a list of names to emulate ``from name import ...'', or an\n"
                    "empty list to emulate ``import name''.\n"
                    "When importing a module from a package, note that __import__('A.B', ...)\n"
                    "returns package A when fromlist is empty, but its sobumodule B when\n"
                    "fromlist is not empty.  Level is used to determine whether to perform\n"
                    "absolute or relative imports. 0 is absolute while a positive number\n"
                    "is the number of parent directories to search relative to the current module.",
                    (sky_native_code_function_t)sky_builtins___import__,
                    SKY_DATA_TYPE_OBJECT_MODULE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "globals", SKY_DATA_TYPE_OBJECT_DICT, sky_None,
                    "locals", SKY_DATA_TYPE_OBJECT, sky_None,
                    "fromlist", SKY_DATA_TYPE_OBJECT, sky_None,
                    "level", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "abs",
            sky_function_createbuiltin(
                    "abs",
                    "abs(number) -> number\n\n"
                    "Return the absolute value of the argument.",
                    (sky_native_code_function_t)sky_number_absolute,
                    SKY_DATA_TYPE_OBJECT,
                    "number", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "all",
            sky_function_createbuiltin(
                    "all",
                    "all(iterable) -> bool\n\n"
                    "Return True if bool(x) is True for all values x in the iterable.\n"
                    "if the iterable is empty, return True.",
                    (sky_native_code_function_t)sky_sequence_all,
                    SKY_DATA_TYPE_BOOL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "any",
            sky_function_createbuiltin(
                    "any",
                    "any(iterable) -> bool\n\n"
                    "Return True if bool(x) is True for any x in the iterable.\n"
                    "If the iterable is empty, return False.",
                    (sky_native_code_function_t)sky_sequence_any,
                    SKY_DATA_TYPE_BOOL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ascii",
            sky_function_createbuiltin(
                    "ascii",
                    "ascii(object) -> string\n\n"
                    "As repr(), return a string containing a printable representation of an\n"
                    "object, but escape the non-ASCII characters in the string returned by\n"
                    "repr() using \\x, \\u or \\U escapes.  This generates a string similar\n"
                    "to that returned by repr() in Python 2.",
                    (sky_native_code_function_t)sky_object_ascii,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "bin",
            sky_function_createbuiltin(
                    "bin",
                    "bin(number) -> string\n\n"
                    "Return the binary representation of an integer.",
                    (sky_native_code_function_t)sky_builtins_bin,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "number", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "callable",
            sky_function_createbuiltin(
                    "callable",
                    "callable(object) -> bool\n\n"
                    "Return whether the object is callable (i.e., some kind of function).\n"
                    "Note that classes are callable, as are instances of classes with a\n"
                    "__call__() method.",
                    (sky_native_code_function_t)sky_object_callable,
                    SKY_DATA_TYPE_BOOL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "chr",
            sky_function_createbuiltin(
                    "chr",
                    "chr(i) -> Unicode character\n\n"
                    "Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.",
                    (sky_native_code_function_t)sky_builtins_chr,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "i", SKY_DATA_TYPE_INT32, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "compile",
            sky_function_createbuiltin(
                    "compile",
                    "compile(source, filename, mode[, flags[, dont_inherit]]) -> code object\n\n"
                    "Compile the source (a Python module, statement or expression)\n"
                    "into a code object that can be executed by exec() or eval().\n"
                    "The filename will be used for run-time error messages.\n"
                    "The mode must be 'exec' to compile a module, 'single' to compile a\n"
                    "single (interactive) statement, or 'eval' to compile an expression.\n"
                    "The flags argument, if present, controls which future statements influence\n"
                    "the compilation of the code.\n"
                    "The dont_inherit argument, if non-zero, stops the compilation inheriting\n"
                    "the effects of any future statements in effect in the code calling\n"
                    "compile; if absent or zero these statements do influence the compilation,\n"
                    "in addition to any features explicitly specified.",
                    (sky_native_code_function_t)sky_builtins_compile,
                    SKY_DATA_TYPE_OBJECT,
                    "source", SKY_DATA_TYPE_OBJECT, NULL,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "mode", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "flags", SKY_DATA_TYPE_UINT, sky_integer_zero,
                    "dont_inherit", SKY_DATA_TYPE_BOOL, sky_False,
                    "optimize", SKY_DATA_TYPE_INT, sky_integer_create(-1),
                    NULL));
    sky_module_setattr(
            module,
            "delattr",
            sky_function_createbuiltin(
                    "delattr",
                    "delattr(object, name)\n\n"
                    "Delete a named attribute on an object; delattr(x, 'y') is equivalent to\n"
                    "``del x.y''.",
                    (sky_native_code_function_t)sky_object_delattr,
                    SKY_DATA_TYPE_VOID,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "dir",
            sky_function_createbuiltin(
                    "dir",
                    "dir([object]) -> list of strings\n\n"
                    "If called without an argument, return the names in the current scope.\n"
                    "Else, return an alphabetized list of names comprising (some of) the attributes\n"
                    "of the given object, and of attributes reachable from it.\n"
                    "If the object supplies a method named __dir__, it will be used; otherwise\n"
                    "the default dir() logic is used and returns:\n"
                    "  for a module object: the module's attributes.\n"
                    "  for a class object:  its attributes, and recursively the attributes\n"
                    "    of its bases.\n"
                    "  for any other object: its attributes, its class's attributes, and\n"
                    "    recursively the attributes of its class's base classes.",
                    (sky_native_code_function_t)sky_builtins_dir,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "object", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "divmod",
            sky_function_createbuiltin(
                    "divmod",
                    "divmod(x, y) -> (div, mod)\n\n"
                    "Return the tuple((x-x%y)/y, x%y).  Invariant: div*y + mod == x.",
                    (sky_native_code_function_t)sky_number_divmod,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    "y", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "eval",
            sky_function_createbuiltin(
                    "eval",
                    "eval(source[, globals[, locals]]) -> value\n\n"
                    "Evaluate the source in the context of globals and locals.\n"
                    "The source may be a string representing a Python expression\n"
                    "or a code object as returned by compile().\n"
                    "The globals must be a dictionary and locals can be any mapping,\n"
                    "defaulting to the current globals and locals.\n"
                    "If only globals is given, locals defaults to it.",
                    (sky_native_code_function_t)sky_interpreter_eval,
                    SKY_DATA_TYPE_OBJECT,
                    "source", SKY_DATA_TYPE_OBJECT, NULL,
                    "globals", SKY_DATA_TYPE_OBJECT_DICT, sky_None,
                    "locals", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "exec",
            sky_function_createbuiltin(
                    "exec",
                    "exec(object[, globals[, locals]])\n\n"
                    "Read and execute code from an object, which can be a string or a code\n"
                    "object.\n"
                    "The globals and locals are dictionaries, defaulting to the current\n"
                    "globals and locals.  If only globals is given, locals defaults to it.",
                    (sky_native_code_function_t)sky_interpreter_exec,
                    SKY_DATA_TYPE_VOID,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "globals", SKY_DATA_TYPE_OBJECT_DICT, sky_None,
                    "locals", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "format",
            sky_function_createbuiltin(
                    "format",
                    "format(value[, format_spec]) -> string\n\n"
                    "Returns value.__format__(format_spec)\n"
                    "format_spec defaults to \"\"",
                    (sky_native_code_function_t)sky_object_format,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "format_spec", SKY_DATA_TYPE_OBJECT_STRING, sky_string_empty,
                    NULL));
    sky_module_setattr(
            module,
            "getattr",
            sky_function_createbuiltin(
                    "getattr",
                    "getattr(object, name[, default]) -> value\n\n"
                    "Get a named attribute from an object; getattr(x, 'y') is equivalent to x.y.\n"
                    "When a default argument is given, it is returned when the attribute doesn't\n"
                    "exist; without it, an exception is raised in that case.",
                    (sky_native_code_function_t)sky_object_getattr,
                    SKY_DATA_TYPE_OBJECT,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "default", (SKY_DATA_TYPE_OBJECT |
                                SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED),
                               sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "globals",
            sky_function_createbuiltin(
                    "globals",
                    "globals() -> dictionary\n\n"
                    "Return the dictionary containing the current scope's global variables.",
                    (sky_native_code_function_t)sky_interpreter_globals,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    NULL));
    sky_module_setattr(
            module,
            "hasattr",
            sky_function_createbuiltin(
                    "hasattr",
                    "hasattr(object, name) -> bool\n\n"
                    "Return whether the object has an attribute with the given name.\n"
                    "(This is done by calling getattr(object, name) and catching AttributeError.)",
                    (sky_native_code_function_t)sky_object_hasattr,
                    SKY_DATA_TYPE_BOOL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "hash",
            sky_function_createbuiltin(
                    "hash",
                    "hash(object) -> integer\n\n"
                    "Return a hash value for the object. Two objects with the same value have\n"
                    "the same hash value. The reverse is not necessarily true, but likely.",
                    (sky_native_code_function_t)sky_object_hash,
                    SKY_DATA_TYPE_UINTPTR_T,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "hex",
            sky_function_createbuiltin(
                    "hex",
                    "hex(number) -> string\n\n"
                    "Return the hexadecimal representation of an integer.",
                    (sky_native_code_function_t)sky_builtins_hex,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "number", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "id",
            sky_function_createbuiltin(
                    "id",
                    "id(object) -> integer\n\n"
                    "Return the identity of an object.  This is guaranteed to be unique among\n"
                    "simultaneously existing objects.  (Hint: it's the object's memory address.)",
                    (sky_native_code_function_t)sky_builtins_id,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "input",
            sky_function_createbuiltin(
                    "input",
                    "input([prompt]) -> string\n\n"
                    "Read a string from standard input.  The trailing newline is stripped.\n"
                    "If the user hits EOF (Unix: Ctl-D, Windows: Ctl-Z+Return), raise EOFError.\n"
                    "On Unix, GNU readline is used if enabled.  The prompt string, if given,\n"
                    "is printed without a trailing newline before reading.",
                    (sky_native_code_function_t)sky_interpreter_input,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "prompt", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "isinstance",
            sky_function_createbuiltin(
                    "isinstance",
                    "isinstance(object, class-or-type-or-tuple) -> bool\n\n"
                    "Return whether an object is an instance of a class or of a subclass thereof.\n"
                    "With a type as second argument, return whether that is the object's type.\n"
                    "The form using a tuple, isinstance(x, (A, B, ...)), is a shortcut for\n"
                    "isinstance(x, A) or isinstance(x, B) or ... (etc.).",
                    (sky_native_code_function_t)sky_object_isinstance,
                    SKY_DATA_TYPE_BOOL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "cls", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "issubclass",
            sky_function_createbuiltin(
                    "issubclass",
                    "issubclass(C, B) -> bool\n\n"
                    "Return whether class C is a subclass (i.e., a derived class) of class B.\n"
                    "When using a tuple as the second argument issubclass(X, (A, B, ...)),\n"
                    "is a shortcut for issubclass(X, A) or issubclass(X, B) or ... (etc.).",
                    (sky_native_code_function_t)sky_object_issubclass,
                    SKY_DATA_TYPE_BOOL,
                    "C", SKY_DATA_TYPE_OBJECT, NULL,
                    "B", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "iter",
            sky_function_createbuiltin(
                    "iter",
                    "iter(iterable) -> iterator\n"
                    "iter(callable, sentinel) -> iterator\n\n"
                    "Get an iterator from an object.  In the first form, the argument must\n"
                    "supply its own iterator, or be a sequence.\n"
                    "In the second form, the callable is called until it returns the sentinel.",
                    (sky_native_code_function_t)sky_builtins_iter,
                    SKY_DATA_TYPE_OBJECT,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    "sentinel", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "len",
            sky_function_createbuiltin(
                    "len",
                    "len(object) -> integer\n\n"
                    "Return the number of items of a sequence or mapping.",
                    (sky_native_code_function_t)sky_object_len,
                    SKY_DATA_TYPE_SSIZE_T,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "locals",
            sky_function_createbuiltin(
                    "locals",
                    "locals() -> dictionary\n\n"
                    "Update and return a dictionary containing the current scope's local variables.",
                    (sky_native_code_function_t)sky_interpreter_locals,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    NULL));
    sky_module_setattr(
            module,
            "max",
            sky_function_createbuiltin(
                    "max",
                    "max(iterable[, key=func]) -> value\n"
                    "max(a, b, c, ...[, key=func]) -> value\n\n"
                    "With a single iterable argument, return its largest item.\n"
                    "With two or more arguments, return the largest argument.\n",
                    (sky_native_code_function_t)sky_builtins_max,
                    SKY_DATA_TYPE_OBJECT,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "min",
            sky_function_createbuiltin(
                    "min",
                    "min(iterable[, key=func]) -> value\n"
                    "min(a, b, c, ...[, key=func]) -> value\n\n"
                    "With a single iterable argument, return its smallest item.\n"
                    "With two or more arguments, return the smallest argument.\n",
                    (sky_native_code_function_t)sky_builtins_min,
                    SKY_DATA_TYPE_OBJECT,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "next",
            sky_function_createbuiltin(
                    "next",
                    "next(iterator[, default])\n\n"
                    "Return the next item from the iterator. If default is given and the iterator\n"
                    "is exhausted, it is returned instead of raising StopIteration.",
                    (sky_native_code_function_t)sky_object_next,
                    SKY_DATA_TYPE_OBJECT,
                    "iterator", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", (SKY_DATA_TYPE_OBJECT |
                                SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED),
                               sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "oct",
            sky_function_createbuiltin(
                    "oct",
                    "oct(number) -> string\n\n"
                    "Return the octal representation of an integer.",
                    (sky_native_code_function_t)sky_builtins_oct,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "number", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    /* "open" is set by the built-in _io module */
    sky_module_setattr(
            module,
            "ord",
            sky_function_createbuiltin(
                    "ord",
                    "ord(c) -> integer\n\n"
                    "Return the integer ordinal of a one-character string.",
                    (sky_native_code_function_t)sky_builtins_ord,
                    SKY_DATA_TYPE_UINT32,
                    "c", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "pow",
            sky_function_createbuiltin(
                    "pow",
                    "pow(x, y[, z]) -> number\n\n"
                    "With two arguments, equivalent to x**y.  With three arguments,\n"
                    "equivalent to (x**y) % z, but may be more efficient (e.g. for ints).",
                    (sky_native_code_function_t)sky_number_pow,
                    SKY_DATA_TYPE_OBJECT,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    "y", SKY_DATA_TYPE_OBJECT, NULL,
                    "z", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "print",
            sky_function_createbuiltin(
                    "print",
                    "print(value, ..., sep=' ', end='\\n', file=sys.stdout, flush=False)\n\n"
                    "Prints the values to a stream, or to sys.stdout by default.\n"
                    "Optional keyword arguments:\n"
                    "file:  a file-like object (stream); defaults to the current sys.stdout.\n"
                    "sep:   string inserted between values, default a space.\n"
                    "end:   string appended after the last value, default a newline.\n"
                    "flush: whether to forcibly flush the stream.",
                    (sky_native_code_function_t)sky_builtins_print,
                    SKY_DATA_TYPE_VOID,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL(" "),
                    "end", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("\n"),
                    "file", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "flush", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_module_setattr(
            module,
            "repr",
            sky_function_createbuiltin(
                    "repr",
                    "repr(object) -> string\n\n"
                    "Return the canonical string representation of the object.\n"
                    "For most object types, eval(repr(object)) == object.",
                    (sky_native_code_function_t)sky_object_repr,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "round",
            sky_function_createbuiltin(
                    "round",
                    "round(number[, ndigits]) -> number\n\n"
                    "Round a number to a given precision in decimal digits (default 0 digits).\n"
                    "This returns an int when called with one argument, otherwise the\n"
                    "same type as the number. ndigits may be negative.",
                    (sky_native_code_function_t)sky_number_round,
                    SKY_DATA_TYPE_OBJECT,
                    "number", SKY_DATA_TYPE_OBJECT, NULL,
                    "ndigits", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "setattr",
            sky_function_createbuiltin(
                    "setattr",
                    "setattr(object, name, value)\n\n"
                    "Set a named attribute on an object; setattr(x, 'y', v) is equivalent to\n"
                    "``x.y = v''.",
                    (sky_native_code_function_t)sky_object_setattr,
                    SKY_DATA_TYPE_VOID,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "sorted",
            sky_function_createbuiltin(
                    "sorted",
                    "sorted(iterable, key=None, reverse=False) --> new sorted list",
                    (sky_native_code_function_t)sky_builtins_sorted,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "sum",
            sky_function_createbuiltin(
                    "sum",
                    "sum(iterable[, start]) -> value\n\n"
                    "Return the sum of an iterable of numbers (NOT strings) plus the value\n"
                    "of parameter 'start' (which defaults to 0).  When the iterable is\n"
                    "empty, return start.",
                    (sky_native_code_function_t)sky_sequence_sum,
                    SKY_DATA_TYPE_OBJECT,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "vars",
            sky_function_createbuiltin(
                    "vars",
                    "vars([object]) -> dictionary\n\n"
                    "Without arguments, equivalent to locals().\n"
                    "With an argument, equivalent to object.__dict__.",
                    (sky_native_code_function_t)sky_builtins_vars,
                    SKY_DATA_TYPE_OBJECT,
                    "object", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));

    /* Singletons */
    sky_module_setattr(module, "None", sky_None);
    sky_module_setattr(module, "Ellipsis", sky_Ellipsis);
    sky_module_setattr(module, "NotImplemented", sky_NotImplemented);
    sky_module_setattr(module, "False", sky_False);
    sky_module_setattr(module, "True", sky_True);

    /* Built-in types that are publicly exposed. */
    sky_module_setattr(module, "bool", sky_boolean_type);
    sky_module_setattr(module, "memoryview", sky_memoryview_type);
    sky_module_setattr(module, "bytearray", sky_bytearray_type);
    sky_module_setattr(module, "bytes", sky_bytes_type);
    sky_module_setattr(module, "classmethod", sky_classmethod_type);
    sky_module_setattr(module, "complex", sky_complex_type);
    sky_module_setattr(module, "dict", sky_dict_type);
    sky_module_setattr(module, "enumerate", sky_enumerate_type);
    sky_module_setattr(module, "filter", sky_filter_type);
    sky_module_setattr(module, "float", sky_float_type);
    sky_module_setattr(module, "frozenset", sky_frozenset_type);
    sky_module_setattr(module, "property", sky_property_type);
    sky_module_setattr(module, "int", sky_integer_type);
    sky_module_setattr(module, "list", sky_list_type);
    sky_module_setattr(module, "map", sky_map_type);
    sky_module_setattr(module, "object", sky_base_object_type);
    sky_module_setattr(module, "range", sky_range_type);
    sky_module_setattr(module, "reversed", sky_reversed_type);
    sky_module_setattr(module, "set", sky_set_type);
    sky_module_setattr(module, "slice", sky_slice_type);
    sky_module_setattr(module, "staticmethod", sky_staticmethod_type);
    sky_module_setattr(module, "str", sky_string_type);
    sky_module_setattr(module, "super", sky_super_type);
    sky_module_setattr(module, "tuple", sky_tuple_type);
    sky_module_setattr(module, "type", sky_type_type);
    sky_module_setattr(module, "zip", sky_zip_type);

    /* Exceptions */
    sky_module_setattr(module, "BaseException", sky_BaseException);
    sky_module_setattr(module, "Exception", sky_Exception);
    sky_module_setattr(module, "ArithmeticError", sky_ArithmeticError);
    sky_module_setattr(module, "BufferError", sky_BufferError);
    sky_module_setattr(module, "LookupError", sky_LookupError);
    sky_module_setattr(module, "AssertionError", sky_AssertionError);
    sky_module_setattr(module, "AttributeError", sky_AttributeError);
    sky_module_setattr(module, "EOFError", sky_EOFError);
    sky_module_setattr(module, "FloatingPointError", sky_FloatingPointError);
    sky_module_setattr(module, "GeneratorExit", sky_GeneratorExit);
    sky_module_setattr(module, "ImportError", sky_ImportError);
    sky_module_setattr(module, "IndexError", sky_IndexError);
    sky_module_setattr(module, "KeyError", sky_KeyError);
    sky_module_setattr(module, "KeyboardInterrupt", sky_KeyboardInterrupt);
    sky_module_setattr(module, "MemoryError", sky_MemoryError);
    sky_module_setattr(module, "NameError", sky_NameError);
    sky_module_setattr(module, "NotImplementedError", sky_NotImplementedError);
    sky_module_setattr(module, "OSError", sky_OSError);
    sky_module_setattr(module, "OverflowError", sky_OverflowError);
    sky_module_setattr(module, "ReferenceError", sky_ReferenceError);
    sky_module_setattr(module, "RuntimeError", sky_RuntimeError);
    sky_module_setattr(module, "StopIteration", sky_StopIteration);
    sky_module_setattr(module, "SyntaxError", sky_SyntaxError);
    sky_module_setattr(module, "IndentationError", sky_IndentationError);
    sky_module_setattr(module, "TabError", sky_TabError);
    sky_module_setattr(module, "SystemError", sky_SystemError);
    sky_module_setattr(module, "SystemExit", sky_SystemExit);
    sky_module_setattr(module, "TypeError", sky_TypeError);
    sky_module_setattr(module, "UnboundLocalError", sky_UnboundLocalError);
    sky_module_setattr(module, "UnicodeError", sky_UnicodeError);
    sky_module_setattr(module, "UnicodeEncodeError", sky_UnicodeEncodeError);
    sky_module_setattr(module, "UnicodeDecodeError", sky_UnicodeDecodeError);
    sky_module_setattr(module, "UnicodeTranslateError", sky_UnicodeTranslateError);
    sky_module_setattr(module, "ValueError", sky_ValueError);
    sky_module_setattr(module, "ZeroDivisionError", sky_ZeroDivisionError);
    sky_module_setattr(module, "BlockingIOError", sky_BlockingIOError);
    sky_module_setattr(module, "ChildProcessError", sky_ChildProcessError);
    sky_module_setattr(module, "ConnectionError", sky_ConnectionError);
    sky_module_setattr(module, "BrokenPipeError", sky_BrokenPipeError);
    sky_module_setattr(module, "ConnectionAbortedError", sky_ConnectionAbortedError);
    sky_module_setattr(module, "ConnectionRefusedError", sky_ConnectionRefusedError);
    sky_module_setattr(module, "ConnectionResetError", sky_ConnectionResetError);
    sky_module_setattr(module, "FileExistsError", sky_FileExistsError);
    sky_module_setattr(module, "FileNotFoundError", sky_FileNotFoundError);
    sky_module_setattr(module, "InterruptedError", sky_InterruptedError);
    sky_module_setattr(module, "IsADirectoryError", sky_IsADirectoryError);
    sky_module_setattr(module, "NotADirectoryError", sky_NotADirectoryError);
    sky_module_setattr(module, "PermissionError", sky_PermissionError);
    sky_module_setattr(module, "ProcessLookupError", sky_ProcessLookupError);
    sky_module_setattr(module, "TimeoutError", sky_TimeoutError);

    /* Exception Aliases */
    sky_module_setattr(module, "EnvironmentError", sky_OSError);
    sky_module_setattr(module, "IOError", sky_OSError);
#if defined(_WIN32)
    sky_module_setattr(module, "WindowsError", sky_OSError);
#endif

    /* Warnings */
    sky_module_setattr(module, "Warning", sky_Warning);
    sky_module_setattr(module, "UserWarning", sky_UserWarning);
    sky_module_setattr(module, "DeprecationWarning", sky_DeprecationWarning);
    sky_module_setattr(module, "PendingDeprecationWarning", sky_PendingDeprecationWarning);
    sky_module_setattr(module, "SyntaxWarning", sky_SyntaxWarning);
    sky_module_setattr(module, "RuntimeWarning", sky_RuntimeWarning);
    sky_module_setattr(module, "FutureWarning", sky_FutureWarning);
    sky_module_setattr(module, "ImportWarning", sky_ImportWarning);
    sky_module_setattr(module, "UnicodeWarning", sky_UnicodeWarning);
    sky_module_setattr(module, "BytesWarning", sky_BytesWarning);
    sky_module_setattr(module, "ResourceWarning", sky_ResourceWarning);
}
