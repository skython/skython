#include "../core/sky_private.h"
#include "../core/sky_traceback.h"


static sky_object_t
sky_faulthandler_fileno(sky_object_t file, int *fd)
{
    if (sky_object_isnull(file)) {
        file = sky_object_getattr(sky_module_import(SKY_STRING_LITERAL("sys")),
                                  SKY_STRING_LITERAL("stderr"),
                                  NULL);
        if (!file) {
            sky_error_raise_string(sky_RuntimeError,
                                   "unable to get sys.stderr");
        }
    }

    if ((*fd = sky_file_fileno(file)) < 0) {
        sky_error_raise_string(sky_RuntimeError,
                               "file.fileno() is not a valid file descriptor");
    }

    sky_file_flush(file);

    return file;
}


static void
sky_faulthandler_dump_traceback(sky_object_t file, sky_bool_t all_threads)
{
    int fd;

    file = sky_faulthandler_fileno(file, &fd);
    if (!all_threads) {
        sky_traceback_dump(fd);
    }
    else {
        sky_traceback_dump_threads(fd);
    }
}

static const char sky_faulthandler_dump_traceback_doc[] =
"dump_traceback(file=sys.stderr, all_threads=True): "
"dump the traceback of the current thread, or of all threads "
"if all_threads is True, into file";


static void
sky_faulthandler_enable(sky_object_t file, sky_bool_t all_threads)   /* TODO */
{
    int fd;

    file = sky_faulthandler_fileno(file, &fd);
    if (all_threads) {
    }
}

static const char sky_faulthandler_enable_doc[] =
"enable(file=sys.stderr, all_threads=True): enable the fault handler";


static void
sky_faulthandler__fatal_error(sky_string_t message)
{
    ssize_t nbytes;
    uint8_t *bytes;

    bytes = sky_codec_encodetocbytes(NULL, message, NULL, NULL, 0, &nbytes);
    bytes = sky_realloc(bytes, nbytes + 1);
    bytes[nbytes] = '\0';

    sky_error_fatal((const char *)bytes);
}

static const char sky_faulthandler__fatal_error_doc[] =
"_fatal_error(message): call sky_error_fatal(message)";


static void
sky_faulthandler_register(
                int             signum,
                sky_object_t    file,
                sky_bool_t      all_threads,
                sky_bool_t      chain)
{
    int fd;

    file = sky_faulthandler_fileno(file, &fd);
    if (all_threads) {
    }
    if (chain) {
    }
    if (signum) {
    }
}

static const char sky_faulthandler_register_doc[] =
"register(signum, file=sys.stderr, all_threads=True, chain=False): "
"register an handler for the signal 'signum': dump the "
"traceback of the current thread, or of all threads if "
"all_threads is True, into file";


static int
sky_faulthandler__read_null(SKY_UNUSED sky_bool_t release_gil)
{
    volatile int *x, y;

    x = NULL;
    y = *x;

    return y;
}

static const char sky_faulthandler__read_null_doc[] =
"_read_null(release_gil=False): read from NULL, raise "
"a SIGSEGV or SIGBUS signal depending on the platform";


static void
sky_faulthandler__sigabrt(void)
{
    raise(SIGABRT);
}

#if defined(SIGBUS)
static void
sky_faulthandler__sigbus(void)
{
    raise(SIGBUS);
}
#endif

static void
sky_faulthandler__sigfpe(void)
{
    raise(SIGFPE);
}

static void
sky_faulthandler__sigill(void)
{
    raise(SIGILL);
}

static void
sky_faulthandler__sigsegv(void)
{
    raise(SIGSEGV);
}


#define SKY_FAULTHANDLER_STACK_OVERFLOW_MAX_SIZE (100 * 1024 * 1024)

static void *
sky_faulthandler__stack_overflow_worker(
                void *  min_sp,
                void *  max_sp,
                size_t *depth)
{
    /* allocate 4096 bytes on the stack at each call */
    unsigned char   buffer[4096];
    void            *sp = &buffer;

    ++(*depth);
    if (sp < min_sp || max_sp < sp) {
        return sp;
    }

    buffer[0] = 1;
    buffer[sizeof(buffer) - 1] = 0;

    return sky_faulthandler__stack_overflow_worker(min_sp, max_sp, depth);
}

static void
sky_faulthandler__stack_overflow(void)
{
    size_t  depth, size;
    char    *sp = (char *)&depth, *stop;

    depth = 0;
    stop = sky_faulthandler__stack_overflow_worker(
                sp - SKY_FAULTHANDLER_STACK_OVERFLOW_MAX_SIZE,
                sp + SKY_FAULTHANDLER_STACK_OVERFLOW_MAX_SIZE,
                &depth);

    if (sp < stop) {
        size = stop - sp;
    }
    else {
        size = sp - stop;
    }

    sky_error_raise_format(
            sky_RuntimeError,
            "unable to raise a stack overflow (allocated %zu bytes "
            "on the stack, %zu recursive calls)",
            size,
            depth);
}

static const char sky_faulthandler__stack_overflow_doc[] =
"_stackoverflow(): recursive call to raise a stack overflow";


void
skython_module_faulthandler_initialize(sky_module_t module)
{
    static const char doc[] = "faulthandler module.";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    /* TODO disable() */
    sky_module_setattr(
            module,
            "dump_traceback",
            sky_function_createbuiltin(
                    "dump_traceback",
                    sky_faulthandler_dump_traceback_doc,
                    (sky_native_code_function_t)sky_faulthandler_dump_traceback,
                    SKY_DATA_TYPE_VOID,
                    "file", SKY_DATA_TYPE_OBJECT, sky_None,
                    "all_threads", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "enable",
            sky_function_createbuiltin(
                    "enable",
                    sky_faulthandler_enable_doc,
                    (sky_native_code_function_t)sky_faulthandler_enable,
                    SKY_DATA_TYPE_VOID,
                    "file", SKY_DATA_TYPE_OBJECT, sky_None,
                    "all_threads", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    /* TODO is_enabled() */

    /* TODO cancel_dump_traceback_later() */
    /* TODO dump_traceback_later() */

    sky_module_setattr(
            module,
            "register",
            sky_function_createbuiltin(
                    "register",
                    sky_faulthandler_register_doc,
                    (sky_native_code_function_t)sky_faulthandler_register,
                    SKY_DATA_TYPE_VOID,
                    "signum", SKY_DATA_TYPE_INT, NULL,
                    "file", SKY_DATA_TYPE_OBJECT, sky_None,
                    "all_threads", SKY_DATA_TYPE_BOOL, sky_True,
                    "chain", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    /* TODO unregister() */

    sky_module_setattr(
            module,
            "_fatal_error",
            sky_function_createbuiltin(
                    "_fatal_error",
                    sky_faulthandler__fatal_error_doc,
                    (sky_native_code_function_t)sky_faulthandler__fatal_error,
                    SKY_DATA_TYPE_VOID,
                    "message", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "_read_null",
            sky_function_createbuiltin(
                    "_read_null",
                    sky_faulthandler__read_null_doc,
                    (sky_native_code_function_t)sky_faulthandler__read_null,
                    SKY_DATA_TYPE_INT,
                    "release_gil", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_module_setattr(
            module,
            "_sigabrt",
            sky_function_createbuiltin(
                    "_sigabrt",
                    "_sigabrt(): raise a SIGABRT signal",
                    (sky_native_code_function_t)sky_faulthandler__sigabrt,
                    SKY_DATA_TYPE_VOID,
                    NULL));
#if defined(SIGBUS)
    sky_module_setattr(
            module,
            "_sigbus",
            sky_function_createbuiltin(
                    "_sigbus",
                    "_sigbus(): raise a SIGBUS signal",
                    (sky_native_code_function_t)sky_faulthandler__sigbus,
                    SKY_DATA_TYPE_VOID,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "_sigfpe",
            sky_function_createbuiltin(
                    "_sigfpe",
                    "_sigfpe(): raise a SIGFPE signal",
                    (sky_native_code_function_t)sky_faulthandler__sigfpe,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "_sigill",
            sky_function_createbuiltin(
                    "_sigill",
                    "_sigill(): raise a SIGILL signal",
                    (sky_native_code_function_t)sky_faulthandler__sigill,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "_sigsegv",
            sky_function_createbuiltin(
                    "_sigsegv",
                    "_sigsegv(): raise a SIGSEGV signal",
                    (sky_native_code_function_t)sky_faulthandler__sigsegv,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "_stack_overflow",
            sky_function_createbuiltin(
                    "_stack_overflow",
                    sky_faulthandler__stack_overflow_doc,
                    (sky_native_code_function_t)sky_faulthandler__stack_overflow,
                    SKY_DATA_TYPE_VOID,
                    NULL));

}
