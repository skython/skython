#ifndef __SKYTHON_MODULES_MODULE__SSL_H__
#define __SKYTHON_MODULES_MODULE__SSL_H__ 1


#include "../core/sky_private.h"


#if defined(HAVE_OPENSSL)
/* The system OpenSSL is deprecated on Mac OS X since 10.7, but this source
 * file wants to use it explicitly, so disable deprecatation warnings/errors.
 * Note that OpenSSL will likely disappear completely from Mac OS X before too
 * long, but as of 10.9 it's still there. This source file will not get used at
 * all if OpenSSL is not available.
 */ 
#if defined(__APPLE__)
#   pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif              
                    
#   include <openssl/crypto.h>
#   include <openssl/err.h>
#   include <openssl/rand.h>
#   include <openssl/ssl.h>
#   include <openssl/x509v3.h>

/* RAND_* APIs and SSL_get_finished got added to OpenSSL in 0.9.5 */
#   if OPENSSL_VERSION_NUMBER >= 0x0090500f
#       define HAVE_OPENSSL_FINISHED    1
#       define HAVE_OPENSSL_RAND        1
#   else
#       undef HAVE_OPENSSL_FINISHED
#       undef HAVE_OPENSSL_RAND
#   endif
    
/* SSL_ctx_clear_options() and SSL_clear_options() were first added in
 * OpenSSL 0.9.8m but do not appear in some 0.9.9-dev versions such as
 * the 0.9.9 from "May 2008" that NetBSD 5.0 uses.
 */                                
#   if OPENSSL_VERSION_NUMBER >= 0x009080df && \
       OPENSSL_VERSION_NUMBER != 0x00909000
#       define HAVE_SSL_CTX_CLEAR_OPTIONS
#   else
#       undef HAVE_SSL_CTX_CLEAR_OPTIONS
#   endif

/* ECDH support got added to OpenSSL in 0.9.8 */
#   if OPENSSL_VERSION_NUMBER < 0x0090800f && !defined(OPENSSL_NO_ECDH)
#       define OPENSSL_NO_ECDH
#   endif
    
/* compression support got added to OpenSSL in 0.9.8 */
#   if OPENSSL_VERSION_NUMBER < 0x0090800f && !defined(OPENSSL_NO_COMP)
#       define OPENSSL_NO_COMP
#   endif
#endif


SKY_CDECLS_BEGIN


typedef enum sky__ssl_error_e {
    /* these mirror ssl.h */
    SKY__SSL_ERROR_NONE,
    SKY__SSL_ERROR_SSL,
    SKY__SSL_ERROR_WANT_READ,
    SKY__SSL_ERROR_WANT_WRITE,
    SKY__SSL_ERROR_WANT_X509_LOOKUP,
    SKY__SSL_ERROR_SYSCALL,
    SKY__SSL_ERROR_ZERO_RETURN,
    SKY__SSL_ERROR_WANT_CONNECT,

    /* start of non ssl.h errorcodes */
    SKY__SSL_ERROR_EOF,
    SKY__SSL_ERROR_NO_SOCKET,
    SKY__SSL_ERROR_INVALID_ERROR_CODE,
} sky__ssl_error_t;

typedef enum sky__ssl_cert_e {
    SKY__SSL_CERT_NONE,
    SKY__SSL_CERT_OPTIONAL,
    SKY__SSL_CERT_REQUIRED,
} sky__ssl_cert_t;

typedef enum sky__ssl_protocol_e {
    SKY__SSL_PROTOCOL_SSLv2,
    SKY__SSL_PROTOCOL_SSLv3,
    SKY__SSL_PROTOCOL_SSLv23,
    SKY__SSL_PROTOCOL_TLSv1,
} sky__ssl_protocol_t;


extern sky_type_t sky__ssl_SSLError;

extern sky_type_t sky__ssl_SSLEOFError;
extern sky_type_t sky__ssl_SSLSyscallError;
extern sky_type_t sky__ssl_SSLWantReadError;
extern sky_type_t sky__ssl_SSLWantWriteError;
extern sky_type_t sky__ssl_SSLZeroReturnError;


extern sky_type_t sky__ssl__SSLContext_type;

typedef struct sky__ssl__SSLContext_data_s {
#if defined(HAVE_OPENSSL)
    SSL_CTX *                           ctx;
    char *                              npn_protocols;
    int                                 npn_protocols_len;
    sky_list_t                          openssl_freelist;
    sky_spinlock_t                      spinlock;
#endif
} sky__ssl__SSLContext_data_t;

SKY_STATIC_INLINE sky__ssl__SSLContext_data_t *
sky__ssl__SSLContext_data(sky_object_t object)
{
    return sky_object_data(object, sky__ssl__SSLContext_type);
}

#if defined(HAVE_OPENSSL)
SKY_STATIC_INLINE SSL_CTX *
sky__ssl__SSLContext_SSL_CTX(sky_object_t self)
{
    return sky__ssl__SSLContext_data(self)->ctx;
}

extern void
sky__ssl_openssl__SSLContext_instance_initialize(
                sky_object_t    self,
                void *          data);

extern void
sky__ssl_openssl__SSLContext_instance_finalize(
                sky_object_t    self,
                void *          data);

extern void
sky__ssl_openssl__SSLContext_instance_visit(
                sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data);

extern sky_object_t
sky__ssl_openssl__SSLContext_create(
                sky_type_t          cls,
                sky__ssl_protocol_t protocol);

extern sky_object_t
sky__ssl_openssl__SSLContext_options(sky_object_t self);

extern sky_object_t
sky__ssl_openssl__SSLContext_options_getter(
                sky_object_t    self,
                sky_type_t      type);

extern void
sky__ssl_openssl__SSLContext_load_cert_chain(
                sky_object_t    self,
                sky_bytes_t     certfile,
                sky_bytes_t     keyfile,
                sky_object_t    password);

extern void
sky__ssl_openssl__SSLContext_load_dh_params(
                sky_object_t    self,
                sky_bytes_t     filename);

extern void
sky__ssl_openssl__SSLContext_load_verify_locations(
                sky_object_t    self,
                sky_bytes_t     cafile,
                sky_bytes_t     capath);

extern sky_dict_t
sky__ssl_openssl__SSLContext_session_stats(sky_object_t self);

extern void
sky__ssl_openssl__SSLContext_set_ciphers(
                sky_object_t    self,
                sky_string_t    ciphers);

extern void
sky__ssl_openssl__SSLContext_set_default_verify_paths(sky_object_t self);

#   if !defined(OPENSSL_NO_ECDH)
extern void
sky__ssl_openssl__SSLContext_set_ecdh_curve(
                sky_object_t    self,
                sky_bytes_t     name);
#   endif

extern void
sky__ssl_openssl__SSLContext__set_npn_protocols(
                sky_object_t    self,
                sky_object_t    protocols);

extern void
sky__ssl_openssl__SSLContext_options_setter(
                sky_object_t    self,
                sky_object_t    value,
                sky_type_t      type);

extern sky_object_t
sky__ssl_openssl__SSLContext_verify_mode_getter(
                sky_object_t    self,
                sky_type_t      type);

extern void
sky__ssl_openssl__SSLContext_verify_mode_setter(
                sky_object_t    self,
                sky_object_t    value,
                sky_type_t      type);
#endif


extern sky_type_t sky__ssl__SSLSocket_type;

typedef struct sky__ssl__SSLSocket_data_s {
    sky_socket_t                        socket;
#if defined(HAVE_OPENSSL)
    SSL *                               ssl;
    X509 *                              peer_cert;
    sky_error_record_t *                error;
    sky_bool_t                          shutdown_seen_zero;
#endif
    sky_bool_t                          server_flag;
} sky__ssl__SSLSocket_data_t;

SKY_STATIC_INLINE sky__ssl__SSLSocket_data_t *
sky__ssl__SSLSocket_data(sky_object_t object)
{
    return sky_object_data(object, sky__ssl__SSLSocket_type);
}

#if defined(HAVE_OPENSSL)
SKY_STATIC_INLINE SSL *
sky__ssl__SSLSocket_SSL(sky_object_t object)
{
    return sky__ssl__SSLSocket_data(object)->ssl;
}

extern void
sky__ssl_openssl__SSLSocket_instance_finalize(
                sky_object_t    self,
                void *          data);

extern sky_object_t
sky__ssl_openssl__SSLSocket_cipher(sky_object_t self);

extern sky_object_t
sky__ssl_openssl__SSLSocket_compression(sky_object_t self);

extern void
sky__ssl_openssl__SSLSocket_do_handshake(sky_object_t self);

extern void
sky__ssl_openssl__SSLSocket_initialize(
                sky_object_t    self,
                sky_object_t    context,
                sky_bytes_t     server_hostname);

extern sky_object_t
sky__ssl_openssl__SSLSocket_peer_certificate(
                sky_object_t    self,
                sky_bool_t      der);

extern int
sky__ssl_openssl__SSLSocket_pending(sky_object_t self);

extern sky_object_t
sky__ssl_openssl__SSLSocket_read(
                sky_object_t    self,
                ssize_t         len,
                sky_object_t    buf);

#   if defined(OPENSSL_NPN_NEGOTIATED)
extern sky_object_t
sky__ssl_openssl__SSLSocket_selected_npn_protocol(sky_object_t self);
#   endif

extern sky_socket_t
sky__ssl_openssl__SSLSocket_shutdown(sky_object_t self);

#   if defined(HAVE_OPENSSL_FINISHED)
extern sky_object_t
sky__ssl_openssl__SSLSocket_tls_unique_cb(sky_object_t self);
#   endif
#endif

extern ssize_t
sky__ssl_openssl__SSLSocket_write(sky_object_t self, sky_object_t s);


#if defined(HAVE_OPENSSL)
extern sky_dict_t
sky__ssl_openssl__test_decode_cert(const char *filename);

extern void
sky__ssl_openssl_initialize(sky_module_t module);
#endif


SKY_CDECLS_END


#endif  /* __SKYTHON_MODULES_MODULE__SSL_H__ */
