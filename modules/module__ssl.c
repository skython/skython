#include "module__ssl.h"
#include "../core/sky_exceptions_private.h"


SKY_MODULE_EXTERN const char *skython_module__ssl_doc;

SKY_MODULE_EXTERN void
skython_module__ssl_initialize(sky_module_t module);


sky_type_t  sky__ssl_SSLError = NULL;
sky_type_t  sky__ssl_SSLEOFError = NULL;
sky_type_t  sky__ssl_SSLSyscallError = NULL;
sky_type_t  sky__ssl_SSLWantReadError = NULL;
sky_type_t  sky__ssl_SSLWantWriteError = NULL;
sky_type_t  sky__ssl_SSLZeroReturnError = NULL;


sky_type_t  sky__ssl__SSLContext_type = NULL;
sky_type_t  sky__ssl__SSLSocket_type = NULL;


static sky_string_t
sky__ssl_SSLError_str(sky_object_t self)
{
    sky_OSError_data_t  *OSError_data = sky_OSError_data(self);

    if (sky_object_isa(OSError_data->strerror, sky_string_type)) {
        return OSError_data->strerror;
    }
    return sky_object_str(sky_BaseException_data(self)->args);
}


static void
sky__ssl__SSLSocket_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__ssl__SSLSocket_data_t  *self_data = data;

    sky_object_visit(self_data->socket, visit_data);
}


static sky_object_t
sky__ssl__SSLContext_new(sky_type_t cls, int protocol)
{
#if defined(HAVE_OPENSSL)
    return sky__ssl_openssl__SSLContext_create(cls, protocol);
#endif
}

static sky_object_t
sky__ssl__SSLContext__wrap_socket(
                sky_object_t    self,
                sky_object_t    sock,
                sky_bool_t      server_side,
                sky_bytes_t     server_hostname)
{
    sky_object_t                sslsocket;
    sky__ssl__SSLSocket_data_t  *sslsocket_data;

    sslsocket = sky_object_allocate(sky__ssl__SSLSocket_type);
    sslsocket_data = sky__ssl__SSLSocket_data(sslsocket);
    sslsocket_data->server_flag = server_side;
    sky_object_gc_set(SKY_AS_OBJECTP(&(sslsocket_data->socket)),
                      sock,
                      sslsocket);

#if defined(HAVE_OPENSSL)
    sky__ssl_openssl__SSLSocket_initialize(sslsocket, self, server_hostname);
#endif

    return sslsocket;
}


static const char sky__ssl__SSLSocket_peer_certificate_doc[] =
"peer_certificate([der=False]) -> certificate\n\n"
"Returns the certificate for the peer.  If no certificate was provided,\n"
"returns None.  If a certificate was provided, but not validated, returns\n"
"an empty dictionary.  Otherwise returns a dict containing information\n"
"about the peer certificate.\n"
"\n"
"If the optional argument is True, returns a DER-encoded copy of the\n"
"peer certificate; or None if no certificate was provided.  This will\n"
"return the certificate even if it wasn't validated.";

static const char sky__ssl__SSLSocket_pending_doc[] =
"pending() -> count\n\n"
"Returns the number of already decrypted bytes available for read,\n"
"pending on the connection.\n";

static const char sky__ssl__SSLSocket_read_doc[] =
"read([len]) -> string\n\n"
"Read up to len bytes from the SSL socket.";

static const char sky__ssl__SSLSocket_shutdown_doc[] =
"shutdown() -> socket\n\n"
"Does the SSL shutdown handshake with the remote end, and returns\n"
"the underlying socket object.";

#if defined(HAVE_OPENSSL_FINISHED)
static const char sky__ssl__SSLSocket_tls_unique_cb_doc[] =
"tls_unique_cb() -> bytes\n\n"
"Returns the 'tls-unique' channel binding data, as defined by RFC 5929.\n"
"\n"
"If the TLS handshake is not yet complete, None is returned.";
#endif

static const char sky__ssl__SSLSocket_write_doc[] =
"write(s) -> len\n\n"
"Writes the string s into the SSL object.  Returns the number\n"
"of bytes written.";


static sky_dict_t
sky__ssl__test_decode_cert(sky_object_t filename)
{
    sky_dict_t  dict;

    SKY_ASSET_BLOCK_BEGIN {
        const char  *cfilename;

        if (sky_object_isa(filename, sky_string_type)) {
            filename = sky_codec_encodefilename(filename);
        }

        cfilename = (const char *)sky_bytes_cstring(filename, SKY_TRUE);
#if defined(HAVE_OPENSSL)
        dict = sky__ssl_openssl__test_decode_cert(cfilename);
#endif
    } SKY_ASSET_BLOCK_END;

    return dict;
}


const char *skython_module__ssl_doc =
"Implementation module for SSL socket operations.  See the socket module\n"
"for documentation.";


void
skython_module__ssl_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

#if defined(HAVE_OPENSSL)
    sky__ssl_openssl_initialize(module);
#endif

#define int_const(_n, _v) \
    sky_module_setattr(module, _n, sky_integer_create(SKY__SSL_##_v))

    int_const("SSL_ERROR_SSL", ERROR_SSL);
    int_const("SSL_ERROR_WANT_READ", ERROR_WANT_READ);
    int_const("SSL_ERROR_WANT_WRITE", ERROR_WANT_WRITE);
    int_const("SSL_ERROR_WANT_X509_LOOKUP", ERROR_WANT_X509_LOOKUP);
    int_const("SSL_ERROR_SYSCALL", ERROR_SYSCALL);
    int_const("SSL_ERROR_ZERO_RETURN", ERROR_ZERO_RETURN);
    int_const("SSL_ERROR_WANT_CONNECT", ERROR_WANT_CONNECT);
    int_const("SSL_ERROR_EOF", ERROR_EOF);
    int_const("SSL_ERROR_NO_SOCKET", ERROR_NO_SOCKET);
    int_const("SSL_ERROR_INVALID_ERROR_CODE", ERROR_INVALID_ERROR_CODE);

    int_const("CERT_NONE", CERT_NONE);
    int_const("CERT_OPTIONAL", CERT_OPTIONAL);
    int_const("CERT_REQUIRED", CERT_REQUIRED);

#if !defined(OPENSSL_NO_SSL2)
    int_const("PROTOCOL_SSLv2", PROTOCOL_SSLv2);
#endif
    int_const("PROTOCOL_SSLv3", PROTOCOL_SSLv3);
    int_const("PROTOCOL_SSLv23", PROTOCOL_SSLv23);
    int_const("PROTOCOL_TLSv1", PROTOCOL_TLSv1);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLError"),
                    "An error occurred in the SSL implementation.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_OSError),
                    NULL),
            SKY_TRUE);
    sky_type_setmethodslots(sky__ssl_SSLError,
            "__str__", sky__ssl_SSLError_str,
            NULL);
    sky_module_setattr(module, "SSLError", sky__ssl_SSLError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLEOFError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLEOFError"),
                    "An error occurred in the SSL implementation.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__ssl_SSLError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "SSLEOFError", sky__ssl_SSLEOFError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLSyscallError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLSyscallError"),
                    "System error when attempting SSL operation.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__ssl_SSLError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "SSLSyscallError", sky__ssl_SSLSyscallError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLWantReadError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLWantReadError"),
                    "Non-blocking SSL socket needs to read more data\n"
                    "before the requested operation can be completed.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__ssl_SSLError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "SSLWantReadError", sky__ssl_SSLWantReadError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLWantWriteError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLWantWriteError"),
                    "Non-blocking SSL socket needs to write more data\n"
                    "before the requested operation can be completed.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__ssl_SSLError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "SSLWantWriteError", sky__ssl_SSLWantWriteError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl_SSLZeroReturnError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SSLZeroReturnError"),
                    "SSL/TLS session closed cleanly.",
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__ssl_SSLError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "SSLZeroReturnError", sky__ssl_SSLZeroReturnError);

    sky_module_setattr(
            module,
            "_test_decode_cert",
            sky_function_createbuiltin(
                    "_test_decode_cert",
                    NULL,
                    (sky_native_code_function_t)sky__ssl__test_decode_cert,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    "filename", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
#if defined(HAVE_OPENSSL)
    template.initialize = sky__ssl_openssl__SSLContext_instance_initialize;
    template.finalize = sky__ssl_openssl__SSLContext_instance_finalize;
    template.visit = sky__ssl_openssl__SSLContext_instance_visit;
#endif
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_SSLContext"),
                                   NULL,
                                   0,
                                   sizeof(sky__ssl__SSLContext_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__new__",
            (sky_native_code_function_t)sky__ssl__SSLContext_new,
            "cls", SKY_DATA_TYPE_OBJECT, NULL,
            "protocol", SKY_DATA_TYPE_INT, NULL,
            NULL);
    sky_type_setgetsets(type,
            "options",
                    NULL,
#if defined(HAVE_OPENSSL)
                    sky__ssl_openssl__SSLContext_options_getter,
                    sky__ssl_openssl__SSLContext_options_setter,
#else
#   error implementation missing
#endif
            "verify_mode",
                    NULL,
#if defined(HAVE_OPENSSL)
                    sky__ssl_openssl__SSLContext_verify_mode_getter,
                    sky__ssl_openssl__SSLContext_verify_mode_setter,
#else
#   error implementation missing
#endif
            NULL);
    sky_type_setattr_builtin(
            type,
            "_wrap_socket",
            sky_function_createbuiltin(
                    "_SSLContext._wrap_socket",
                    NULL,
                    (sky_native_code_function_t)sky__ssl__SSLContext__wrap_socket,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "sock", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "server_side", SKY_DATA_TYPE_BOOL, sky_False,
                    "server_hostname", (SKY_DATA_TYPE_OBJECT_BYTES | 
                                        SKY_DATA_TYPE_CREATE_FROM_BUFFER |
                                        SKY_DATA_TYPE_ENCODE_STRING),
                                       sky_None,
                                       SKY_STRING_LITERAL("idna"),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "set_ciphers",
            sky_function_createbuiltin(
                    "_SSLContext.set_ciphers",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_set_ciphers,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "ciphers", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "_set_npn_protocols",
            sky_function_createbuiltin(
                    "_SSLContext._set_npn_protocols",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext__set_npn_protocols,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "protocols", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "load_cert_chain",
            sky_function_createbuiltin(
                    "_SSLContext.load_cert_chain",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_load_cert_chain,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "certfile", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, NULL,
                    "keyfile", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, sky_None,
                    "password", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "load_dh_params",
            sky_function_createbuiltin(
                    "_SSLContext.load_dh_params",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_load_dh_params,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "filename", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "load_verify_locations",
            sky_function_createbuiltin(
                    "_SSLContext.load_verify_locations",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_load_verify_locations,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "cafile", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, sky_None,
                    "capath", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "session_stats",
            sky_function_createbuiltin(
                    "_SSLContext.session_stats",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_session_stats,
#endif
                    SKY_DATA_TYPE_OBJECT_DICT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "set_default_verify_paths",
            sky_function_createbuiltin(
                    "_SSLContext.set_default_verify_paths",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_set_default_verify_paths,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
#if defined(HAVE_OPENSSL) && !defined(OPENSSL_NO_ECDH)
    sky_type_setattr_builtin(
            type,
            "set_ecdh_curve",
            sky_function_createbuiltin(
                    "_SSLContext.set_ecdh_curve",
                    NULL,
                    (sky_native_code_function_t)sky__ssl_openssl__SSLContext_set_ecdh_curve,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "name", SKY_DATA_TYPE_OBJECT_BYTES_FSNAME, NULL,
                    NULL));
#endif
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl__SSLContext_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "_SSLContext", type);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
#if defined(HAVE_OPENSSL)
    template.finalize = sky__ssl_openssl__SSLSocket_instance_finalize;
#endif
    template.visit = sky__ssl__SSLSocket_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_SSLSocket"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky__ssl__SSLSocket_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setattr_builtin(
            type,
            "cipher",
            sky_function_createbuiltin(
                    "_SSLSocket.cipher",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_cipher,
#endif
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "compression",
            sky_function_createbuiltin(
                    "_SSLSocket.compression",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_compression,
#endif
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "do_handshake",
            sky_function_createbuiltin(
                    "_SSLSocket.do_handshake",
                    NULL,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_do_handshake,
#endif
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "peer_certificate",
            sky_function_createbuiltin(
                    "_SSLSocket.peer_certificate",
                    sky__ssl__SSLSocket_peer_certificate_doc,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_peer_certificate,
#endif
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "der", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "pending",
            sky_function_createbuiltin(
                    "_SSLSocket.pending",
                    sky__ssl__SSLSocket_pending_doc,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_pending,
#endif
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "read",
            sky_function_createbuiltin(
                    "_SSLSocket.read",
                    sky__ssl__SSLSocket_read_doc,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_read,
#endif
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "len", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "buf", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
#if defined(OPENSSL_NPN_NEGOTIATED)
    sky_type_setattr_builtin(
            type,
            "selected_npn_protocol",
            sky_function_createbuiltin(
                    "_SSLSocket.selected_npn_protocol",
                    NULL,
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_selected_npn_protocol,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
#endif
    sky_type_setattr_builtin(
            type,
            "shutdown",
            sky_function_createbuiltin(
                    "_SSLSocket.shutdown",
                    sky__ssl__SSLSocket_shutdown_doc,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_shutdown,
#endif
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, sky_socket_type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
#if defined(HAVE_OPENSSL_FINISHED)
    sky_type_setattr_builtin(
            type,
            "tls_unique_cb",
            sky_function_createbuiltin(
                    "_SSLSocket.tls_unique_cb",
                    sky__ssl__SSLSocket_tls_unique_cb_doc,
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_tls_unique_cb,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
#endif
    sky_type_setattr_builtin(
            type,
            "write",
            sky_function_createbuiltin(
                    "_SSLSocket.write",
                    sky__ssl__SSLSocket_write_doc,
#if defined(HAVE_OPENSSL)
                    (sky_native_code_function_t)sky__ssl_openssl__SSLSocket_write,
#endif
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "s", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__ssl__SSLSocket_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "_SSLSocket", type);
}
