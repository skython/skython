#include "../core/skython.h"
#include "../core/sky_string_private.h"
#include "../core/sky_unicode_private.h"

#include "sky_unicode_ctype-3.2.0.h"
#include "sky_unicode_decomp-3.2.0.h"

SKY_MODULE_EXTERN const char *skython_module_unicodedata_doc;

SKY_MODULE_EXTERN void
skython_module_unicodedata_initialize(sky_module_t module);


static const char *sky_unicodedata_category_names[] = {
    "Cn", "Lu", "Ll", "Lt", "Mn", "Mc", "Me", "Nd", "Nl", "No", "Zs", "Zl",
    "Zp", "Cc", "Cf", "Cs", "Co", "Cn", "Lm", "Lo", "Pc", "Pd", "Ps", "Pe",
    "Pi", "Pf", "Po", "Sm", "Sc", "Sk", "So",
};
#if !defined(NDEBUG)
static size_t sky_unicodedata_category_name_count =
              (sizeof(sky_unicodedata_category_names) /
               sizeof(sky_unicodedata_category_names[0]));
#endif

static const char *sky_unicodedata_bidirectional_names[] = {
    "", "L", "LRE", "LRO", "R", "AL", "RLE", "RLO", "PDF", "EN", "ES", "ET",
    "AN", "CS", "NSM", "BN", "B", "S", "WS", "ON",
};
#if !defined(NDEBUG)
static size_t sky_unicodedata_bidirectional_name_count =
              (sizeof(sky_unicodedata_bidirectional_names) /
               sizeof(sky_unicodedata_bidirectional_names[0]));
#endif

static const char *sky_unicodedata_east_asian_width_names[] = {
    "F", "H", "W", "Na", "A", "N"
};
#if !defined(NDEBUG)
static size_t sky_unicodedata_east_asian_width_name_count =
              (sizeof(sky_unicodedata_east_asian_width_names) /
               sizeof(sky_unicodedata_east_asian_width_names[0]));
#endif


SKY_STATIC_INLINE sky_unicode_char_t
sky_unicodedata_codepoint(sky_string_t unichr)
{
    if (sky_string_len(unichr) != 1) {
        sky_error_raise_string(sky_TypeError,
                               "need a single Unicode character as parameter");
    }
    return sky_string_charat(unichr, 0);
}


static sky_object_t sky_unicodedata_UCD = NULL;

static sky_type_t   sky_unicodedata_UCD_type = NULL;

typedef struct sky_unicodedata_UCD_data_s {
    sky_string_t                        version;
    const sky_unicode_ctype_data_t *    ctype_data;
    const sky_unicode_decomp_data_t *   decomp_data;
} sky_unicodedata_UCD_data_t;

SKY_STATIC_INLINE sky_unicodedata_UCD_data_t *
sky_unicodedata_UCD_data(sky_object_t object)
{
    return sky_object_data(object, sky_unicodedata_UCD_type);
}

static void
sky_unicodedata_UCD_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_unicodedata_UCD_data_t  *self_data = data;

    sky_object_visit(self_data->version, visit_data);
}

SKY_STATIC_INLINE sky_unicode_ctype_t *
sky_unicodedata_UCD_ctype_lookup(sky_object_t self, sky_unicode_char_t c)
{
    sky_unicodedata_UCD_data_t      *self_data = sky_unicodedata_UCD_data(self);
    const sky_unicode_ctype_data_t  *table = self_data->ctype_data;

    int index, shift = table->shift;

    sky_error_validate_debug(c >= SKY_UNICODE_CHAR_MIN);
    sky_error_validate_debug(c <= SKY_UNICODE_CHAR_MAX);
    index = table->index_1[c >> shift] << shift;
    index = table->index_2[index + (c & ((1 << shift) - 1))];
    return &(table->data[index]);
}

SKY_STATIC_INLINE sky_unicode_decomp_t *
sky_unicodedata_UCD_decomp_lookup(sky_object_t self, sky_unicode_char_t c)
{
    sky_unicodedata_UCD_data_t      *self_data = sky_unicodedata_UCD_data(self);
    const sky_unicode_decomp_data_t *data = self_data->decomp_data;

    int index;

    sky_error_validate_debug(c >= SKY_UNICODE_CHAR_MIN);
    sky_error_validate_debug(c <= SKY_UNICODE_CHAR_MAX);
    index = data->index_1[c >> data->shift] << data->shift;
    index = data->index_2[index + (c & ((1 << data->shift) - 1))];
    return &(data->table[index]);
}


static sky_string_t
sky_unicodedata_UCD_bidirectional(sky_object_t self, sky_string_t unichr)
{
    const char          *name;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    cp = sky_unicodedata_codepoint(unichr);
    ctype = sky_unicodedata_UCD_ctype_lookup(self, cp);
    sky_error_validate_debug(ctype->bidirectional <
                             sky_unicodedata_bidirectional_name_count);

    name = sky_unicodedata_bidirectional_names[ctype->bidirectional];
    return sky_string_createfrombytes(name, strlen(name), NULL, NULL);
}

static sky_object_t
sky_unicodedata_UCD_create(
                const char *                        version,
                const sky_unicode_ctype_data_t *    ctype_data,
                const sky_unicode_decomp_data_t *   decomp_data)
{
    sky_object_t                self;
    sky_unicodedata_UCD_data_t  *self_data;

    self = sky_object_allocate(sky_unicodedata_UCD_type);
    self_data = sky_unicodedata_UCD_data(self);
    self_data->ctype_data = ctype_data;
    self_data->decomp_data = decomp_data;

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->version)),
                      sky_string_createfrombytes(version,
                                                 strlen(version),
                                                 NULL,
                                                 NULL),
                      self);

    return self;
}

static sky_string_t
sky_unicodedata_UCD_category(sky_object_t self, sky_string_t unichr)
{
    const char          *name;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    cp = sky_unicodedata_codepoint(unichr);
    ctype = sky_unicodedata_UCD_ctype_lookup(self, cp);
    sky_error_validate_debug(ctype->category <
                             sky_unicodedata_category_name_count);

    name = sky_unicodedata_category_names[ctype->category];
    return sky_string_createfrombytes(name, strlen(name), NULL, NULL);
}

static int
sky_unicodedata_UCD_combining(sky_object_t self, sky_string_t unichr)
{
    sky_unicode_char_t      cp;
    sky_unicode_decomp_t    *decomp;

    cp = sky_unicodedata_codepoint(unichr);
    decomp = sky_unicodedata_UCD_decomp_lookup(self, cp);

    return decomp->ccc;
}

static sky_string_t
sky_unicodedata_UCD_east_asian_width(sky_object_t self, sky_string_t unichr)
{
    const char          *name;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    cp = sky_unicodedata_codepoint(unichr);
    ctype = sky_unicodedata_UCD_ctype_lookup(self, cp);
    sky_error_validate_debug(ctype->east_asian_width <
                             sky_unicodedata_east_asian_width_name_count);

    name = sky_unicodedata_east_asian_width_names[ctype->east_asian_width];
    return sky_string_createfrombytes(name, strlen(name), NULL, NULL);
}

static int
sky_unicodedata_UCD_mirrored(sky_object_t self, sky_string_t unichr)
{
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    cp = sky_unicodedata_codepoint(unichr);
    ctype = sky_unicodedata_UCD_ctype_lookup(self, cp);

    return (ctype->flags & SKY_UNICODE_CTYPE_FLAG_MIRRORED ? 1 : 0);
}

static sky_string_t
sky_unicodedata_UCD_normalize(
                sky_object_t    self,
                sky_string_t    form,
                sky_string_t    unistr)
{
    sky_unicodedata_UCD_data_t  *self_data = sky_unicodedata_UCD_data(self);

    const char                  *cstring;
    sky_string_normalize_form_t normalize_form;

    if (!(cstring = sky_string_cstring(form))) {
        sky_error_raise_string(sky_ValueError,
                               "invalid normalization form");
    }
    if (!strcmp(cstring, "NFC")) {
        normalize_form = SKY_STRING_NORMALIZE_FORM_NFC;
    }
    else if (!strcmp(cstring, "NFD")) {
        normalize_form = SKY_STRING_NORMALIZE_FORM_NFD;
    }
    else if (!strcmp(cstring, "NFKC")) {
        normalize_form = SKY_STRING_NORMALIZE_FORM_NFKC;
    }
    else if (!strcmp(cstring, "NFKD")) {
        normalize_form = SKY_STRING_NORMALIZE_FORM_NFKD;
    }
    else {
        sky_error_raise_string(sky_ValueError,
                               "invalid normalization form");
    }

    return sky_string_normalizewithdata(unistr,
                                        normalize_form,
                                        self_data->decomp_data);
}


static sky_string_t
sky_unicodedata_bidirectional(sky_string_t unichr)
{
    return sky_unicodedata_UCD_bidirectional(sky_unicodedata_UCD, unichr);
}

static const char sky_unicodedata_bidirectional_doc[] =
"bidirectional(unichr)\n\n"
"Returns the bidirectional class assigned to the Unicode character\n"
"unichr as string. If no such value is defined, an empty string is returned.";

static sky_string_t
sky_unicodedata_category(sky_string_t unichr)
{
    return sky_unicodedata_UCD_category(sky_unicodedata_UCD, unichr);
}

static const char sky_unicodedata_category_doc[] =
"category(unichr)\n\n"
"Returns the general category assigned to the Unicode character\n"
"unichr as string.";

static int
sky_unicodedata_combining(sky_string_t unichr)
{
    return sky_unicodedata_UCD_combining(sky_unicodedata_UCD, unichr);
}

static const char sky_unicodedata_combining_doc[] =
"combining(unichr)\n\n"
"Returns the canonical combining class assigned to the Unicode\n"
"character unichr as integer. Returns 0 if no combining class is\n"
"defined.";

static sky_string_t
sky_unicodedata_east_asian_width(sky_string_t unichr)
{
    return sky_unicodedata_UCD_east_asian_width(sky_unicodedata_UCD, unichr);
}

static const char sky_unicodedata_east_asian_width_doc[] =
"east_asian_width(unichr)\n\n"
"Returns the east asian width assigned to the Unicode character\n\n"
"unichr as string.";

static int
sky_unicodedata_mirrored(sky_string_t unichr)
{
    return sky_unicodedata_UCD_mirrored(sky_unicodedata_UCD, unichr);
}

static const char sky_unicodedata_mirrored_doc[] =
"mirrored(unichr)\n\n"
"Returns the mirrored property assigned to the Unicode character\n"
"unichr as integer. Returns 1 if the character has been identified as\n"
"a \"mirrored\" character in bidirectional text, 0 otherwise.";

static sky_string_t
sky_unicodedata_normalize(sky_string_t form, sky_string_t unistr)
{
    return sky_unicodedata_UCD_normalize(sky_unicodedata_UCD, form, unistr);
}

static const char sky_unicodedata_normalize_doc[] =
"normalize(form, unistr)\n\n"
"Return the normal form 'form' for the Unicode string unistr.  Valid\n"
"values for form are 'NFC', 'NFKC', 'NFD', and 'NFKD'.";


static const sky_unicode_ctype_data_t sky_unicodedata_ctype_tables_3_2_0 =
{
    sky_unicode_ctypes_3_2_0,
    sky_unicode_ctype_shift_3_2_0,
    sky_unicode_ctype_index_3_2_0_1,
    sky_unicode_ctype_index_3_2_0_2,
    sky_unicode_ctypes_latin1_3_2_0
};

static const sky_unicode_decomp_data_t sky_unicodedata_decomp_tables_3_2_0 =
{
    sky_unicode_decomp_data_3_2_0,
    sky_unicode_decomp_table_3_2_0,
    sky_unicode_decomp_shift_3_2_0,
    sky_unicode_decomp_index_3_2_0_1,
    sky_unicode_decomp_index_3_2_0_2,
    sky_unicode_compose_table_3_2_0,
    sky_unicode_compose_shift_3_2_0,
    sky_unicode_compose_index_3_2_0_1,
    sky_unicode_compose_index_3_2_0_2,
};


const char *skython_module_unicodedata_doc =
"This module provides access to the Unicode Character Database which\n"
"defines character properties for all Unicode characters. The data in\n"
"this database is based on the UnicodeData.txt file version\n"
SKY_UNICODE_VERSION_STRING " which is publically available from ftp://ftp.unicode.org/.\n"
"\n"
"The module uses the same names and symbols as defined by the\n"
"UnicodeData File Format " SKY_UNICODE_VERSION_STRING ".";


void
skython_module_unicodedata_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "unidata_version",
            sky_string_createfromformat("%d.%d.%d",
                                        SKY_UNICODE_VERSION_MAJOR,
                                        SKY_UNICODE_VERSION_MINOR,
                                        SKY_UNICODE_VERSION_UPDATE));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_unicodedata_UCD_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("UCD"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_unicodedata_UCD_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_member(
            type,
            "unidata_version",
            NULL,
            offsetof(sky_unicodedata_UCD_data_t, version),
            SKY_DATA_TYPE_OBJECT_STRING,
            SKY_MEMBER_FLAG_READONLY);
    sky_type_setattr_builtin(
            type,
            "bidirectional",
            sky_function_createbuiltin(
                    "UCD.bidirectional",
                    sky_unicodedata_bidirectional_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_bidirectional,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "category",
            sky_function_createbuiltin(
                    "UCD.category",
                    sky_unicodedata_category_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_category,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "combining",
            sky_function_createbuiltin(
                    "UCD.combining",
                    sky_unicodedata_combining_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_combining,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO decimal() */
    /* TODO decomposition() */
    /* TODO digit() */
    sky_type_setattr_builtin(
            type,
            "east_asian_width",
            sky_function_createbuiltin(
                    "UCD.east_asian_width",
                    sky_unicodedata_east_asian_width_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_east_asian_width,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO lookup() */
    sky_type_setattr_builtin(
            type,
            "mirrored",
            sky_function_createbuiltin(
                    "UCD.mirrored",
                    sky_unicodedata_mirrored_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_mirrored,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO name() */
    sky_type_setattr_builtin(
            type,
            "normalize",
            sky_function_createbuiltin(
                    "UCD.normalize",
                    sky_unicodedata_normalize_doc,
                    (sky_native_code_function_t)sky_unicodedata_UCD_normalize,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "form", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "unistr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO numeric() */
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_unicodedata_UCD_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "UCD", type);

    sky_object_gc_setlibraryroot(
            &sky_unicodedata_UCD,
            sky_unicodedata_UCD_create(
                    SKY_UNICODE_VERSION_STRING,
                    sky_unicode_ctype_tables,
                    sky_unicode_decomp_tables),
            SKY_TRUE);

    sky_module_setattr(
            module,
            "ucd_3_2_0",
            sky_unicodedata_UCD_create(
                    "3.2.0",
                    &sky_unicodedata_ctype_tables_3_2_0,
                    &sky_unicodedata_decomp_tables_3_2_0));

    /* TODO ucnhash_CAPI */

    sky_module_setattr(
            module,
            "bidirectional",
            sky_function_createbuiltin(
                    "bidirectional",
                    sky_unicodedata_bidirectional_doc,
                    (sky_native_code_function_t)sky_unicodedata_bidirectional,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "category",
            sky_function_createbuiltin(
                    "category",
                    sky_unicodedata_category_doc,
                    (sky_native_code_function_t)sky_unicodedata_category,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "combining",
            sky_function_createbuiltin(
                    "combining",
                    sky_unicodedata_combining_doc,
                    (sky_native_code_function_t)sky_unicodedata_combining,
                    SKY_DATA_TYPE_INT,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO decimal() */
    /* TODO decomposition() */
    /* TODO digit() */
    sky_module_setattr(
            module,
            "east_asian_width",
            sky_function_createbuiltin(
                    "east_asian_width",
                    sky_unicodedata_east_asian_width_doc,
                    (sky_native_code_function_t)sky_unicodedata_east_asian_width,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO lookup() */
    sky_module_setattr(
            module,
            "mirrored",
            sky_function_createbuiltin(
                    "mirrored",
                    sky_unicodedata_mirrored_doc,
                    (sky_native_code_function_t)sky_unicodedata_mirrored,
                    SKY_DATA_TYPE_INT,
                    "unichr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO name() */
    sky_module_setattr(
            module,
            "normalize",
            sky_function_createbuiltin(
                    "normalize",
                    sky_unicodedata_normalize_doc,
                    (sky_native_code_function_t)sky_unicodedata_normalize,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "form", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "unistr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    /* TODO numeric() */
}
