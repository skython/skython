#include "../core/sky_private.h"


void
skython_module__warnings_initialize(sky_module_t module)
{
    static const char doc[] =
"_warnings provides basic warning filtering support.\n"
"It is a helper module to speed up interpreter start-up.";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_module_setattr(module, "filters", sky_error_warn_filters);
    sky_module_setattr(module, "_onceregistry", sky_error_warn__onceregistry);
    sky_module_setattr(module, "_defaultaction", sky_error_warn__defaultaction);

    sky_module_setattr(
            module,
            "warn",
            sky_function_createbuiltin(
                    "warn",
                    "Issue a warning, or maybe ignore it or raise an exception.",
                    (sky_native_code_function_t)sky_error_warn,
                    SKY_DATA_TYPE_VOID,
                    "message", SKY_DATA_TYPE_OBJECT, NULL,
                    "category", SKY_DATA_TYPE_OBJECT_TYPE, sky_None,
                    "stacklevel", SKY_DATA_TYPE_SSIZE_T, sky_integer_one,
                    NULL));
    sky_module_setattr(
            module,
            "warn_explicit",
            sky_function_createbuiltin(
                    "warn_explicit",
                    "Low-level interface to warnings functionality.",
                    (sky_native_code_function_t)sky_error_warn_explicit,
                    SKY_DATA_TYPE_VOID,
                    "message", SKY_DATA_TYPE_OBJECT, NULL,
                    "category", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "lineno", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "module", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    "registry", SKY_DATA_TYPE_OBJECT_DICT, sky_None,
                    "module_globals", SKY_DATA_TYPE_OBJECT_DICT, sky_None,
                    NULL));
}
