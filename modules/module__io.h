#ifndef __SKYTHON_MODULES_MODULE__IO_H__
#define __SKYTHON_MODULES_MODULE__IO_H__ 1


#include "../core/skython.h"


SKY_CDECLS_BEGIN


#define SKY__IO_DEFAULT_BUFFER_SIZE         8192
#define SKY__IO_TEXTIOWRAPPER_CHUNK_SIZE    2048


extern sky_string_t sky__io_cr;
extern sky_string_t sky__io_crlf;
extern sky_string_t sky__io_lf;


SKY_EXTERN sky_type_t sky_UnsupportedOperation;


typedef ssize_t (*sky__io_readinto_native_t)(sky_object_t   self,
                                             void *         bytes,
                                             ssize_t        nbytes);


extern ssize_t
sky__io_line_ending(sky_string_t    string,
                    ssize_t         start,
                    sky_bool_t      read_translate,
                    sky_bool_t      read_universal,
                    sky_string_t    read_newline);

extern void
sky__io_initialize_library(void);


/* _io._IOBase */
extern sky_type_t sky__io__IOBase_type;

extern sky_type_t
sky__io__IOBase_initialize(void);


/* _io._RawIOBase */
extern sky_type_t sky__io__RawIOBase_type;

extern sky_type_t
sky__io__RawIOBase_initialize(void);


/* _io.FileIO */
extern sky_type_t sky__io_FileIO_type;

typedef struct sky__io_FileIO_data_s {
    int                                 fd;
    unsigned                            closefd : 1;
    unsigned                            created : 1;
    unsigned                            readable : 1;
    unsigned                            seekable : 1;
    unsigned                            seekable_set : 1;
    unsigned                            writable : 1;
    unsigned                            appending : 1;
} sky__io_FileIO_data_t;

static inline sky__io_FileIO_data_t *
sky__io_FileIO_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_FileIO_type);
}

typedef struct sky__io_FileIO_s {
    sky_object_data_t                   object_data;
    sky__io_FileIO_data_t               fileio_data;
} sky__io_FileIO_t;


extern ssize_t
sky__io_FileIO_readinto_native(sky_object_t self,
                               void *       bytes,
                               ssize_t      nbytes);

extern ssize_t
sky__io_FileIO_seek_native(sky_object_t self,
                           ssize_t      pos,
                           int          whence);

extern ssize_t
sky__io_FileIO_write_native(sky_object_t    self,
                            const void *    bytes,
                            ssize_t         nbytes);


extern sky_type_t
sky__io_FileIO_initialize(void);


/* _io.BytesIO */
extern sky_type_t sky__io_BytesIO_type;

typedef struct sky__io_BytesIO_data_s {
    sky_bytearray_t                     buffer;
    ssize_t                             pos;
} sky__io_BytesIO_data_t;

static inline sky__io_BytesIO_data_t *
sky__io_BytesIO_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_BytesIO_type);
}

typedef struct sky__io_BytesIO_s {
    sky_object_data_t                   object_data;
    sky__io_BytesIO_data_t              bytesio_data;
} sky__io_BytesIO_t;


extern ssize_t
sky__io_BytesIO_readinto_native(sky_object_t    self,
                                void *          bytes,
                                ssize_t         nbytes);

extern ssize_t
sky__io_BytesIO_seek_native(sky_object_t    self,
                            ssize_t         pos,
                            int             whence);

extern ssize_t
sky__io_BytesIO_write_native(sky_object_t   self,
                             const void *   bytes,
                             ssize_t        nbytes);


extern sky_type_t
sky__io_BytesIO_initialize(void);


/* _io._BufferedIOBase */
extern sky_type_t sky__io__BufferedIOBase_type;

extern sky_type_t
sky__io__BufferedIOBase_initialize(void);

/* _io._BufferedIOMixin */
extern sky_type_t sky__io__BufferedIOMixin_type;

typedef struct sky__io__BufferedIOMixin_data_s {
    sky_object_t                        raw;

    /* This is the position in raw for the start of the data that is in buffer.
     * The actual file pointer will typically be raw_pos + buffer_size.
     */
    ssize_t                             raw_pos;
    ssize_t                             buffer_pos;

    ssize_t                             buffer_start;
    ssize_t                             buffer_end;
    ssize_t                             buffer_size;
    char *                              buffer;

    ssize_t                             dirty_start;
    ssize_t                             dirty_end;

    unsigned                            readable : 1;
    unsigned                            seekable : 1;
    unsigned                            dirty : 1;
    unsigned                            stable : 1;

    sky_mutex_t                         mutex;
} sky__io__BufferedIOMixin_data_t;

static inline sky__io__BufferedIOMixin_data_t *
sky__io__BufferedIOMixin_data(sky_object_t object)
{
    return sky_object_data(object, sky__io__BufferedIOMixin_type);
}

static inline sky_object_t
sky__io__BufferedIOMixin_raw(sky_object_t object)
{
    return sky__io__BufferedIOMixin_data(object)->raw;
}

typedef struct sky__io__BufferedIOMixin_s {
    sky_object_data_t                       object_data;
    sky__io__BufferedIOMixin_data_t         mixin_data;
} sky__io__BufferedIOMixin_t;


extern sky_type_t
sky__io__BufferedIOMixin_initialize(void);


/* _io.BufferedReader */
extern sky_type_t sky__io_BufferedReader_type;

typedef struct sky__io_BufferReader_s {
    sky_object_data_t                       object_data;
    sky__io__BufferedIOMixin_data_t         mixin_data;
} sky__io_BufferReader_t;


extern ssize_t
sky__io_BufferedReader_readinto_native(sky_object_t self,
                                       void *       bytes,
                                       ssize_t      nbytes);


extern sky_type_t
sky__io_BufferedReader_initialize(void);


/* _io.BufferedWriter */
extern sky_type_t sky__io_BufferedWriter_type;

typedef struct sky__io_BufferWriter_s {
    sky_object_data_t                       object_data;
    sky__io__BufferedIOMixin_data_t         mixin_data;
} sky__io_BufferWriter_t;


extern sky_type_t
sky__io_BufferedWriter_initialize(void);


/* _io.BufferedRWPair */
extern sky_type_t sky__io_BufferedRWPair_type;

typedef struct sky__io_BufferedRWPair_data_s {
    sky_object_t                        reader;
    sky_object_t                        writer;
} sky__io_BufferedRWPair_data_t;

static inline sky__io_BufferedRWPair_data_t *
sky__io_BufferedRWPair_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_BufferedRWPair_type);
} 

static inline sky_object_t
sky__io_BufferedRWPair_reader(sky_object_t object)
{
    return sky__io_BufferedRWPair_data(object)->reader;
}

static inline sky_object_t
sky__io_BufferedRWPair_writer(sky_object_t object)
{
    return sky__io_BufferedRWPair_data(object)->reader;
}

typedef struct sky__io_BufferedRWPair_s {
    sky_object_data_t                       object_data;
    sky__io_BufferedRWPair_data_t           rwpair_data;
} sky__io_BufferedRWPair_t;


extern sky_type_t
sky__io_BufferedRWPair_initialize(void);


/* _io.BufferedRandom */
extern sky_type_t sky__io_BufferedRandom_type;

typedef struct sky__io_BufferedRandom_s {
    sky_object_data_t                       object_data;
    sky__io__BufferedIOMixin_data_t         mixin_data;
} sky__io_BufferedRandom_t;


extern sky_type_t
sky__io_BufferedRandom_initialize(void);


/* _io._TextIOBase */
extern sky_type_t sky__io__TextIOBase_type;

extern sky_type_t
sky__io__TextIOBase_initialize(void);


/* _io.TextIOWrapper */
extern sky_type_t sky__io_TextIOWrapper_type;

typedef struct sky__io_TextIOWrapper_data_s {
    sky_object_t                        buffer;
    sky_object_t                        decoder;
    sky_object_t                        encoder;
    sky_string_t                        encoding;
    sky_string_t                        errors;
    sky_string_t                        read_newline;
    sky_string_t                        write_newline;

    sky_string_t                        decoded_input;
    ssize_t                             decoded_input_used;
    sky_tuple_t                         snapshot;

    ssize_t                             chunk_size;
    ssize_t                             nwrite_bytes;

    sky_list_t                          write_lines;
    sky_list_t                          write_bytes;

    const sky_codec_t *                 codec;
    uint32_t                            encoder_flags;

    double                              b2cratio;

    /* Not a bit field so that it can be set as a member */
    sky_bool_t                          line_buffering;

    uint32_t                            has_read1       : 1;
    uint32_t                            read_translate  : 1;
    uint32_t                            read_universal  : 1;
    uint32_t                            seekable        : 1;
    uint32_t                            telling         : 1;
    uint32_t                            write_through   : 1;
} sky__io_TextIOWrapper_data_t;

static inline sky__io_TextIOWrapper_data_t *
sky__io_TextIOWrapper_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_TextIOWrapper_type);
}

typedef struct sky__io_TextIOWrapper_s {
    sky_object_data_t                   object_data;
    sky__io_TextIOWrapper_data_t        wrapper_data;
} sky__io_TextIOWrapper_t;


extern void
sky__io_TextIOWrapper_init(
        sky_object_t            self,
        sky_object_t volatile   buffer,
        sky_string_t volatile   encoding,
        sky_string_t volatile   errors,
        sky_string_t            newline,
        volatile sky_bool_t     line_buffering,
        volatile sky_bool_t     write_through);

extern sky_type_t
sky__io_TextIOWrapper_initialize(void);


/* _io.IncrementalNewlineDecoder */
extern sky_type_t sky__io_IncrementalNewlineDecoder_type;

typedef enum sky__io_IncrementalNewlineDecoder_seen_e {
    SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR   = 0x1,
    SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF = 0x2,
    SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF   = 0x4,
} sky__io_IncrementalNewlineDecoder_seen_t;

typedef struct sky__io_IncrementalNewlineDecoder_data_s {
    sky_object_t                        decoder;
    sky_string_t                        errors;
    unsigned                            translate   : 1;
    unsigned                            pendingcr   : 1;
    unsigned                            seen        : 3;
} sky__io_IncrementalNewlineDecoder_data_t;

static inline sky__io_IncrementalNewlineDecoder_data_t *
sky__io_IncrementalNewlineDecoder_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_IncrementalNewlineDecoder_type);
}

typedef struct sky__io_IncrementalNewlineDecoder_s {
    sky_object_data_t                               object_data;
    sky__io_IncrementalNewlineDecoder_data_t        decoder_data;
} sky__io_IncrementalNewlineDecoder_t;


extern sky_type_t
sky__io_IncrementalNewlineDecoder_initialize(void);

extern sky_object_t
sky__io_IncrementalNewlineDecoder_decode(sky_object_t   self,
                                         sky_object_t   input,
                                         sky_bool_t     final);


/* _io.StringIO */
extern sky_type_t sky__io_StringIO_type;

typedef struct sky__io_StringIO_data_s {
    ssize_t                             pos;

    sky_string_builder_t                builder;
    sky_object_t                        decoder;
    sky_string_t                        read_newline;
    sky_string_t                        write_newline;

    sky_unicode_char_t *                buffer;
    ssize_t                             buffer_len;

    unsigned                            closed          : 1;
    unsigned                            read_universal  : 1;
    unsigned                            read_translate  : 1;

    SKY_CACHE_LINE_ALIGNED
    sky_spinlock_t                      spinlock;
} sky__io_StringIO_data_t;

static inline sky__io_StringIO_data_t *
sky__io_StringIO_data(sky_object_t object)
{
    return sky_object_data(object, sky__io_StringIO_type);
}

typedef struct sky__io_StringIO_s {
    sky_object_data_t                   object_data;
    sky__io_StringIO_data_t             stringio_data;
} sky__io_StringIO_t;


extern ssize_t
sky__io_StringIO_readinto_native(sky_object_t   self,
                                 void *         bytes,
                                 ssize_t        nbytes);

extern ssize_t
sky__io_StringIO_seek_native(sky_object_t   self,
                             ssize_t        pos,
                             int            whence);

extern ssize_t
sky__io_StringIO_write_native(sky_object_t  self,
                              const void *  bytes,
                              ssize_t       nbytes);


extern sky_type_t
sky__io_StringIO_initialize(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_MODULES_MODULE__IO_H__ */
