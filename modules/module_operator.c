#include "../core/skython.h"


static sky_type_t sky_operator_attrgetter_type;

static const char sky_operator_attrgetter_type_doc[] =
"attrgetter(attr, ...) --> attrgetter object\n\n"
"Return a callable object that fetches the given attribute(s) from its operand.\n"
"After f = attrgetter('name'), the call f(r) returns r.name.\n"
"After g = attrgetter('name', 'date'), the call g(r) returns (r.name, r.date).\n"
"After h = attrgetter('name.first', 'name.last'), the call h(r) returns\n"
"(r.name.first, r.name.last)";

typedef struct sky_operator_attrgetter_s *sky_operator_attrgetter_t;

typedef struct sky_operator_attrgetter_data_s {
    union {
        sky_object_t                    attr;
        sky_tuple_t                     attrs;
    } u;
    ssize_t                             nattrs;
} sky_operator_attrgetter_data_t;

SKY_EXTERN_INLINE sky_operator_attrgetter_data_t *
sky_operator_attrgetter_data(sky_object_t object)
{
    return sky_object_data(object, sky_operator_attrgetter_type);
}

static void
sky_operator_attrgetter_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_operator_attrgetter_data_t  *self_data = data;

    sky_object_visit(self_data->u.attr, visit_data);
}


static sky_object_t
sky_operator_attrgetter_getattr(sky_object_t object, sky_object_t attr)
{
    sky_string_t    name;

    if (!sky_sequence_isiterable(attr)) {
        object = sky_object_getattr(object, attr, sky_NotSpecified);
    }
    else {
        SKY_SEQUENCE_FOREACH(attr, name) {
            object = sky_object_getattr(object, name, sky_NotSpecified);
        } SKY_SEQUENCE_FOREACH_END;
    }

    return object;
}


static sky_object_t
sky_operator_attrgetter_call(
                sky_object_t    self,
                sky_tuple_t     args,
    SKY_UNUSED  sky_dict_t      kws)
{
    ssize_t                         i;
    sky_object_t                    item, object, *objects, result;
    sky_operator_attrgetter_data_t  *self_data;

    if (sky_tuple_len(args) != 1) {
        sky_error_raise_format(sky_TypeError,
                               "%@ expected 1 argument, got %zd",
                               sky_type_name(sky_object_type(self)),
                               sky_tuple_len(args));
    }
    object = sky_tuple_get(args, 0);

    self_data = sky_operator_attrgetter_data(self);
    if (1 == self_data->nattrs) {
        return sky_operator_attrgetter_getattr(object, self_data->u.attr);
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_malloc(self_data->nattrs * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < self_data->nattrs; ++i) {
            item = sky_tuple_get(self_data->u.attrs, i);
            objects[i] = sky_operator_attrgetter_getattr(object, item);
        }

        result = sky_tuple_createwitharray(self_data->nattrs,
                                           objects,
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static void
sky_operator_attrgetter_init(
                sky_object_t    self,
                sky_tuple_t     args,
    SKY_UNUSED  sky_dict_t      kws)
{
    ssize_t                         i, nattrs;
    sky_object_t                    attr, *attrs;
    sky_string_t                    dot;
    sky_operator_attrgetter_data_t  *self_data;

    if (sky_object_bool(kws)) {
        sky_error_raise_format(sky_TypeError,
                               "%@() does not take keyword arguments",
                               sky_type_name(sky_object_type(self)));
    }
    if ((nattrs = sky_tuple_len(args)) < 1) {
        sky_error_raise_format(sky_TypeError,
                               "%@ expected 1 argument, got 0",
                               sky_type_name(sky_object_type(self)));
    }

    self_data = sky_operator_attrgetter_data(self);
    dot = SKY_STRING_LITERAL(".");
    if (1 == nattrs) {
        attr = sky_string_split(sky_tuple_get(args, 0), dot, -1);
        if (sky_object_len(attr) == 1) {
            attr = sky_sequence_get(attr, 0);
        }
        sky_object_gc_set(&(self_data->u.attr), attr, self);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            i = 0;
            attrs = sky_asset_malloc(nattrs * sizeof(sky_object_t),
                                     SKY_ASSET_CLEANUP_ALWAYS);
            SKY_SEQUENCE_FOREACH(attrs, attr) {
                attr = sky_string_split(attr, dot, -1);
                if (sky_object_len(attr) == 1) {
                    attr = sky_sequence_get(attr, 0);
                }
                attrs[i++] = attr;
            } SKY_SEQUENCE_FOREACH_END;

            sky_object_gc_set(
                    SKY_AS_OBJECTP(&(self_data->u.attrs)),
                    sky_tuple_createwitharray(nattrs, attrs, sky_free),
                    self);
        } SKY_ASSET_BLOCK_END;
    }
    self_data->nattrs = nattrs;
}


static sky_type_t sky_operator_itemgetter_type;

static const char sky_operator_itemgetter_type_doc[] =
"itemgetter(item, ...) --> itemgetter object\n\n"
"Return a callable object that fetches the given item(s) from its operand.\n"
"After f = itemgetter(2), the call f(r) returns r[2].\n"
"After g = itemgetter(2,5,3), the call g(r) returns (r[2], r[5], r[3])";

typedef struct sky_operator_itemgetter_s *sky_operator_itemgetter_t;

typedef struct sky_operator_itemgetter_data_s {
    union {
        sky_object_t                    item;
        sky_tuple_t                     items;
    } u;
    ssize_t                             nitems;
} sky_operator_itemgetter_data_t;

SKY_EXTERN_INLINE sky_operator_itemgetter_data_t *
sky_operator_itemgetter_data(sky_object_t object)
{
    return sky_object_data(object, sky_operator_itemgetter_type);
}

static void
sky_operator_itemgetter_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_operator_itemgetter_data_t  *self_data = data;
    
    sky_object_visit(self_data->u.item, visit_data);
}


static sky_object_t
sky_operator_itemgetter_call(
                sky_object_t    self,
                sky_tuple_t     args,
    SKY_UNUSED  sky_dict_t      kws)
{
    ssize_t                         i;
    sky_object_t                    item, object, *objects, result;
    sky_operator_itemgetter_data_t  *self_data;

    if (sky_tuple_len(args) != 1) {
        sky_error_raise_format(sky_TypeError,
                               "%@ expected 1 argument, got %zd",
                               sky_type_name(sky_object_type(self)),
                               sky_tuple_len(args));
    }
    object = sky_tuple_get(args, 0);

    self_data = sky_operator_itemgetter_data(self);
    if (1 == self_data->nitems) {
        return sky_object_getitem(object, self_data->u.item);
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_malloc(self_data->nitems * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < self_data->nitems; ++i) {
            item = sky_tuple_get(self_data->u.items, i);
            objects[i] = sky_object_getitem(object, item);
        }

        result = sky_tuple_createwitharray(self_data->nitems,
                                           objects,
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static void
sky_operator_itemgetter_init(
                sky_object_t    self,
                sky_tuple_t     args,
                sky_dict_t      kws)
{
    ssize_t                         nitems;
    sky_operator_itemgetter_data_t  *self_data;

    if (sky_object_bool(kws)) {
        sky_error_raise_format(sky_TypeError,
                               "%@() does not take keyword arguments",
                               sky_type_name(sky_object_type(self)));
    }
    if ((nitems = sky_tuple_len(args)) < 1) {
        sky_error_raise_format(sky_TypeError,
                               "%@ expected 1 argument, got 0",
                               sky_type_name(sky_object_type(self)));
    }

    self_data = sky_operator_itemgetter_data(self);
    if (1 == nitems) {
        sky_object_gc_set(&(self_data->u.item), sky_tuple_get(args, 0), self);
    }
    else {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->u.items)), args, self);
    }
    self_data->nitems = nitems;
}


static sky_type_t sky_operator_methodcaller_type;

static const char sky_operator_methodcaller_type_doc[] =
"methodcaller(name, ...) --> methodcaller object\n\n"
"Return a callable object that calls the given method on its operand.\n"
"After f = methodcaller('name'), the call f(r) returns r.name().\n"
"After g = methodcaller('name', 'date', foo=1), the call g(r) returns\n"
"r.name('date', foo=1).";

typedef struct sky_operator_methodcaller_s *sky_operator_methodcaller_t;

typedef struct sky_operator_methodcaller_data_s {
    sky_string_t                        name;
    sky_tuple_t                         args;
    sky_dict_t                          kws;
} sky_operator_methodcaller_data_t;

SKY_EXTERN_INLINE sky_operator_methodcaller_data_t *
sky_operator_methodcaller_data(sky_object_t object)
{
    return sky_object_data(object, sky_operator_methodcaller_type);
}

static void
sky_operator_methodcaller_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_operator_methodcaller_data_t    *self_data = data;

    sky_object_visit(self_data->name, visit_data);
    sky_object_visit(self_data->args, visit_data);
    sky_object_visit(self_data->kws, visit_data);
}


static sky_object_t
sky_operator_methodcaller_call(
                sky_object_t  self,
                sky_tuple_t   args,
    SKY_UNUSED  sky_dict_t    kws)
{
    sky_operator_methodcaller_data_t    *self_data;

    if (sky_tuple_len(args) != 1) {
        sky_error_raise_format(sky_TypeError,
                               "%@ expected 1 argument, got %zd",
                               sky_type_name(sky_object_type(self)),
                               sky_tuple_len(args));
    }

    self_data = sky_operator_methodcaller_data(self);
    return sky_object_callmethod(sky_tuple_get(args, 0),
                                 self_data->name,
                                 self_data->args,
                                 self_data->kws);
}


static void
sky_operator_methodcaller_init(
                sky_object_t    self,
                sky_tuple_t     args,
                sky_dict_t      kws)
{
    sky_string_t                        name;
    sky_operator_methodcaller_data_t    *self_data;

    if (sky_tuple_len(args) < 1) {
        sky_error_raise_format(
                sky_TypeError,
                "%@ needs at least one argument, the method name",
                sky_type_name(sky_object_type(self)));
    }
    name = sky_tuple_get(args, 0);
    args = sky_tuple_slice(args, 1, SSIZE_MAX, 1);

    self_data = sky_operator_methodcaller_data(self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->name)), name, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->args)), args, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->kws)), kws, self);
}


typedef sky_object_t
        (*sky_operator_unaryop_t)(sky_object_t);

typedef sky_object_t
        (*sky_operator_binaryop_t)(sky_object_t, sky_object_t);

typedef sky_object_t
        (*sky_operator_ternaryop_t)(sky_object_t, sky_object_t, sky_object_t);


static void
sky_operator_binaryop(
                sky_module_t            module,
                const char *            name,
                const char *            name2,
                const char *            doc,
                sky_operator_binaryop_t cfunction)
{
    sky_module_setattr(
            module,
            name,
            sky_function_createbuiltin(
                    name,
                    doc,
                    (sky_native_code_function_t)cfunction,
                    SKY_DATA_TYPE_OBJECT,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    if (name2) {
        sky_module_setattr(
                module,
                name2,
                sky_function_createbuiltin(
                        name2,
                        doc,
                        (sky_native_code_function_t)cfunction,
                        SKY_DATA_TYPE_OBJECT,
                        "a", SKY_DATA_TYPE_OBJECT, NULL,
                        "b", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL));
    }
}


static sky_bool_t
sky_operator_eq(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_EQUAL);
}


static sky_bool_t
sky_operator_ge(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_GREATER_EQUAL);
}


static sky_bool_t
sky_operator_gt(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_GREATER);
}


static sky_object_t
sky_operator_inplace_pow(sky_object_t a, sky_object_t b)
{
    return sky_number_inplace_pow(a, b, NULL);
}


static sky_bool_t
sky_operator_is(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_IS);
}


static sky_bool_t
sky_operator_is_not(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_IS_NOT);
}


static sky_bool_t
sky_operator_le(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_LESS_EQUAL);
}


static sky_bool_t
sky_operator_lt(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_LESS);
}


static sky_bool_t
sky_operator_ne(sky_object_t a, sky_object_t b)
{
    return sky_object_compare(a, b, SKY_COMPARE_OP_NOT_EQUAL);
}


static sky_object_t
sky_operator_not(sky_object_t a)
{
    return (sky_object_bool(a) ? sky_False : sky_True);
}


static sky_object_t
sky_operator_pow(sky_object_t a, sky_object_t b)
{
    return sky_number_pow(a, b, NULL);
}


static void
sky_operator_ternaryop(
                sky_module_t                module,
                const char *                name,
                const char *                name2,
                const char *                doc,
                sky_operator_ternaryop_t    cfunction)
{
    sky_module_setattr(
            module,
            name,
            sky_function_createbuiltin(
                    name,
                    doc,
                    (sky_native_code_function_t)cfunction,
                    SKY_DATA_TYPE_OBJECT,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    "c", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    if (name2) {
        sky_module_setattr(
                module,
                name2,
                sky_function_createbuiltin(
                        name2,
                        doc,
                        (sky_native_code_function_t)cfunction,
                        SKY_DATA_TYPE_OBJECT,
                        "a", SKY_DATA_TYPE_OBJECT, NULL,
                        "b", SKY_DATA_TYPE_OBJECT, NULL,
                        "c", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL));
    }
}


static void
sky_operator_unaryop(
                sky_module_t            module,
                const char *            name,
                const char *            name2,
                const char *            doc,
                sky_operator_unaryop_t  cfunction)
{
    sky_module_setattr(
            module,
            name,
            sky_function_createbuiltin(
                    name,
                    doc,
                    (sky_native_code_function_t)cfunction,
                    SKY_DATA_TYPE_OBJECT,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    if (name2) {
        sky_module_setattr(
                module,
                name2,
                sky_function_createbuiltin(
                        name2,
                        doc,
                        (sky_native_code_function_t)cfunction,
                        SKY_DATA_TYPE_OBJECT,
                        "a", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL));
    }
}


void
skython_module_operator_initialize(sky_module_t module)
{
    static const char doc[] =
"Operator interface.\n"
"\n"
"This module exports a set of functions implemented in C corresponding\n"
"to the intrinsic operators of Python.  For example, operator.add(x, y)\n"
"is equivalent to the expression x+y.  The function names are those\n"
"used for special methods; variants without leading and trailing\n"
"'__' are also provided for convenience.";

    sky_type_t          type;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_module_setattr(
            module,
            "truth",
            sky_function_createbuiltin(
                    "truth",
                    "truth(a) -- Return True if a is true, False otherwise.",
                    (sky_native_code_function_t)sky_object_bool,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "contains",
            sky_function_createbuiltin(
                    "contains",
                    "contains(a, b) -- Same as b in a (note reversed operands).",
                    (sky_native_code_function_t)sky_object_contains,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "__contains__",
            sky_function_createbuiltin(
                    "__contains__",
                    "contains(a, b) -- Same as b in a (note reversed operands).",
                    (sky_native_code_function_t)sky_object_contains,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "countOf",
            sky_function_createbuiltin(
                    "countOf",
                    "countOf(a, b) -- Return the number of times b occurs in a.",
                    (sky_native_code_function_t)sky_sequence_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "indexOf",
            sky_function_createbuiltin(
                    "indexOf",
                    "indexOf(a, b) -- Return the first index of b in a.",
                    (sky_native_code_function_t)sky_sequence_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_module_setattr(
            module,
            "is_",
            sky_function_createbuiltin(
                    "is_",
                    "is_(a, b) -- Same as a is b.",
                    (sky_native_code_function_t)sky_operator_is,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "is_not",
            sky_function_createbuiltin(
                    "is_not",
                    "is_not(a, b) -- Same as a is not b.",
                    (sky_native_code_function_t)sky_operator_is_not,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_operator_unaryop(module,
            "index",
            "__index__",
            "index(a) -- Same as a.__index__()",
            (sky_operator_unaryop_t)sky_number_index);
    sky_operator_unaryop(module,
            "neg",
            "__neg__",
            "neg(a) -- Same as -a.",
            sky_number_negative);
    sky_operator_unaryop(module,
            "pos",
            "__pos__",
            "pos(a) -- Same as +a.",
            sky_number_positive);
    sky_operator_unaryop(module,
            "abs",
            "__abs__",
            "abs(a) -- Same as abs(a).",
            sky_number_absolute);
    sky_operator_unaryop(module,
            "inv",
            "__inv__",
            "inv(a) -- Same as ~a.",
            sky_number_invert);
    sky_operator_unaryop(module,
            "invert",
            "__invert__",
            "invert(a) -- Same as ~a.",
            sky_number_invert);
    sky_operator_unaryop(module,
            "not_",
            "__not__",
            "not_(a) -- Same as not a.",
            sky_operator_not);

    sky_operator_binaryop(module,
            "concat",
            "__concat__",
            "concat(a, b) -- Same as a + b, for a and b sequences.",
            sky_sequence_concatenate);
    sky_operator_binaryop(module,
            "iconcat",
            "__iconcat__",
            "iconcat(a, b) -- Same as a = a + b, for a and b sequences.",
            sky_sequence_inplace_concatenate);
    sky_operator_binaryop(module,
            "getitem",
            "__getitem__",
            "getitem(a, b) -- Same as a[b].",
            sky_object_getitem);
    sky_operator_ternaryop(module,
            "setitem",
            "__setitem__",
            "setitem(a, b, c) -- Same as a[b] = c.",
            (sky_operator_ternaryop_t)sky_object_setitem);
    sky_operator_binaryop(module,
            "delitem",
            "__delitem__",
            "delitem(a, b) -- Same as del a[b].",
            (sky_operator_binaryop_t)sky_object_delitem);

    sky_module_setattr(module,
            "lt",
            sky_function_createbuiltin(
                    "lt",
                    "lt(a, b) -- Same as a < b.",
                    (sky_native_code_function_t)sky_operator_lt,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "le",
            sky_function_createbuiltin(
                    "le",
                    "le(a, b) -- Same as a <= b.",
                    (sky_native_code_function_t)sky_operator_le,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "eq",
            sky_function_createbuiltin(
                    "eq",
                    "eq(a, b) -- Same as a == b.",
                    (sky_native_code_function_t)sky_operator_eq,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "ne",
            sky_function_createbuiltin(
                    "ne",
                    "ne(a, b) -- Same as a != b.",
                    (sky_native_code_function_t)sky_operator_ne,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "gt",
            sky_function_createbuiltin(
                    "gt",
                    "gt(a, b) -- Same as a > b.",
                    (sky_native_code_function_t)sky_operator_gt,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "ge",
            sky_function_createbuiltin(
                    "ge",
                    "ge(a, b) -- Same as a >= b.",
                    (sky_native_code_function_t)sky_operator_ge,
                    SKY_DATA_TYPE_BOOL,
                    "a", SKY_DATA_TYPE_OBJECT, NULL,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_operator_binaryop(module,
            "add",
            "__add__",
            "add(a, b) -- Same as a + b.",
            sky_number_add);
    sky_operator_binaryop(module,
            "sub",
            "__sub__",
            "sub(a, b) -- Same as a - b.",
            sky_number_subtract);
    sky_operator_binaryop(module,
            "mul",
            "__mul__",
            "mul(a, b) -- Same as a * b.",
            sky_number_multiply);
    sky_operator_binaryop(module,
            "truediv",
            "__truediv__",
            "truediv(a, b) -- Same as a / b.",
            sky_number_truedivide);
    sky_operator_binaryop(module,
            "floordiv",
            "__floordiv__",
            "floordiv(a, b) -- Same as a // b.",
            sky_number_floordivide);
    sky_operator_binaryop(module,
            "mod",
            "__mod__",
            "mod(a, b) -- Same as a % b.",
            sky_number_modulo);
    sky_operator_binaryop(module,
            "pow",
            "__pow__",
            "pow(a, b) -- Same as a ** b.",
            sky_operator_pow);
    sky_operator_binaryop(module,
            "lshift",
            "__lshift__",
            "lshift(a, b) -- Same as a << b.",
            sky_number_lshift);
    sky_operator_binaryop(module,
            "rshift",
            "__rshift__",
            "rshift(a, b) -- Same as a >> b.",
            sky_number_rshift);
    sky_operator_binaryop(module,
            "and_",
            "__and__",
            "and_(a, b) -- Same as a & b.",
            sky_number_and);
    sky_operator_binaryop(module,
            "xor",
            "__xor__",
            "xor(a, b) -- Same as a ^ b.",
            sky_number_xor);
    sky_operator_binaryop(module,
            "or_",
            "__or__",
            "or_(a, b) -- Same as a | b.",
            sky_number_or);

    sky_operator_binaryop(module,
            "iadd",
            "__iadd__",
            "a = iadd(a, b) -- Same as a += b.",
            sky_number_inplace_add);
    sky_operator_binaryop(module,
            "isub",
            "__isub__",
            "a = isub(a, b) -- Same as a -= b.",
            sky_number_inplace_subtract);
    sky_operator_binaryop(module,
            "imul",
            "__imul__",
            "a = imul(a, b) -- Same as a *= b.",
            sky_number_inplace_multiply);
    sky_operator_binaryop(module,
            "itruediv",
            "__itruediv__",
            "a = itruediv(a, b) -- Same as a /= b.",
            sky_number_inplace_truedivide);
    sky_operator_binaryop(module,
            "ifloordiv",
            "__ifloordiv__",
            "a = ifloordiv(a, b) -- Same as a //= b.",
            sky_number_inplace_floordivide);
    sky_operator_binaryop(module,
            "imod",
            "__imod__",
            "a = imod(a, b) -- Same as a %= b.",
            sky_number_inplace_modulo);
    sky_operator_binaryop(module,
            "ipow",
            "__ipow__",
            "a = ipow(a, b) -- Same as a **= b.",
            sky_operator_inplace_pow);
    sky_operator_binaryop(module,
            "ilshift",
            "__ilshift__",
            "a = ilshift(a, b) -- Same as a <<= b.",
            sky_number_inplace_lshift);
    sky_operator_binaryop(module,
            "irshift",
            "__irshift__",
            "a = irshift(a, b) -- Same as a >>= b.",
            sky_number_inplace_rshift);
    sky_operator_binaryop(module,
            "iand",
            "__iand__",
            "a = iand(a, b) -- Same as a &= b.",
            sky_number_inplace_and);
    sky_operator_binaryop(module,
            "ixor",
            "__ixor__",
            "a = ixor(a, b) -- Same as a ^= b.",
            sky_number_inplace_xor);
    sky_operator_binaryop(module,
            "ior",
            "__ior__",
            "a = ior(a, b) -- Same as a |= b.",
            sky_number_inplace_or);


    /* TODO _compare_digest() */


    /* attrgetter type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_operator_attrgetter_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("attrgetter"),
                    sky_operator_attrgetter_type_doc,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky_operator_attrgetter_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslots(type,
            "__init__", sky_operator_attrgetter_init,
            "__call__", sky_operator_attrgetter_call,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_operator_attrgetter_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "attrgetter", type);


    /* itemgetter type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_operator_itemgetter_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("itemgetter"),
                    sky_operator_itemgetter_type_doc,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky_operator_itemgetter_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslots(type,
            "__init__", sky_operator_itemgetter_init,
            "__call__", sky_operator_itemgetter_call,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_operator_itemgetter_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "itemgetter", type);


    /* methodcaller type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_operator_methodcaller_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("methodcaller"),
                    sky_operator_methodcaller_type_doc,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky_operator_methodcaller_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslots(type,
            "__init__", sky_operator_methodcaller_init,
            "__call__", sky_operator_methodcaller_call,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_operator_methodcaller_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "methodcaller", type);
}
