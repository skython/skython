#include "../core/skython.h"


SKY_MODULE_EXTERN const char *skython_module_atexit_doc;

SKY_MODULE_EXTERN void
skython_module_atexit_initialize(sky_module_t module);


/* TODO a mutex is needed to protect the registration list */
static sky_list_t sky_atexit_list = NULL;


static void
sky_atexit_run(sky_bool_t raise)
{
    sky_error_record_t  *error = NULL;

    ssize_t                 i;
    sky_tuple_t             record;
    sky_object_t            o;
    sky_object_t volatile   message, write;

    SKY_ERROR_TRY {
        for (i = sky_list_len(sky_atexit_list) - 1; i >= 0; --i) {
            record = sky_list_get(sky_atexit_list, i);
            SKY_ERROR_TRY {
                sky_object_call(sky_tuple_get(record, 0),
                                sky_tuple_get(record, 1),
                                sky_tuple_get(record, 2));
            } SKY_ERROR_EXCEPT_ANY {
                if (error) {
                    sky_error_record_release(error);
                    error = NULL;
                }
                else {
                    o = sky_module_import(SKY_STRING_LITERAL("sys"));
                    o = sky_object_getattr(o,
                                           SKY_STRING_LITERAL("stderr"),
                                           sky_NotSpecified);
                    write = sky_object_getattr(o,
                                               SKY_STRING_LITERAL("write"),
                                               sky_NotSpecified);
                    message = sky_tuple_pack(1, SKY_STRING_LITERAL("Error in atexit._run_exitfuncs\n"));
                }
                error = sky_error_current();
                sky_error_record_retain(error);
                sky_object_call(write, message, NULL);
                sky_error_display(sky_error_current());
            } SKY_ERROR_TRY_END;
        }
        if (error) {
            if (raise) {
                sky_error_raise_record(error);
            }
            sky_error_record_release(error);
        }
    } SKY_ERROR_EXCEPT_ANY {
        if (error) {
            sky_error_record_release(error);
        }
        sky_error_raise();
    } SKY_ERROR_TRY_END;
}


static void
sky_atexit_finalize(void)
{
    sky_atexit_run(SKY_FALSE);
}


static void
sky_atexit__clear(void)
{
    sky_list_clear(sky_atexit_list);
}

static const char sky_atexit__clear_doc[] =
"_clear() -> None\n"
"\n"
"Clear the list of previously registered exit functions.";


static void
sky_atexit__run_exitfuncs(void)
{
    sky_atexit_run(SKY_TRUE);
}

static const char sky_atexit__run_exitfuncs_doc[] =
"_run_exitfuncs() -> None\n"
"\n"
"Run all registered exit functions.";


static sky_object_t
sky_atexit_register(sky_object_t func, sky_tuple_t args, sky_dict_t kws)
{
    sky_tuple_t record;

    if (!sky_object_callable(func)) {
        sky_error_raise_string(sky_TypeError,
                               "the first argument must be callable");
    }
    record = sky_tuple_pack(3, func, args, kws);

    sky_list_append(sky_atexit_list, record);

    return func;
}

static const char sky_atexit_register_doc[] =
"register(func, *args, **kwargs) -> func\n"
"\n"
"Register a function to be executed upon normal program termination\n"
"\n"
"    func - function to be called at exit\n"
"    args - optional arguments to pass to func\n"
"    kwargs - optional keyword arguments to pass to func\n"
"\n"
"    func is returned to facilitate usage as a decorator.";


static void
sky_atexit_unregister(sky_object_t func)
{
    ssize_t     i;
    sky_tuple_t record;

    for (i = sky_list_len(sky_atexit_list) - 1; i >= 0; --i) {
        record = sky_list_get(sky_atexit_list, i);
        if (sky_object_compare(func,
                               sky_tuple_get(record, 0),
                               SKY_COMPARE_OP_EQUAL))
        {
            sky_list_delete(sky_atexit_list, i, i + 1, 1);
        }
    }
}

static const char sky_atexit_unregister_doc[] =
"unregister(func) -> None\n"
"\n"
"Unregister a exit function which was previously registered using\n"
"atexit.register\n"
"\n"
"    func - function to be unregistered";


const char *skython_module_atexit_doc =
"allow programmer to define multiple exit functions to be executed\n"
"upon normal program termination.\n"
"\n"
"Two public functions, register and unregister, are defined.\n";


void
skython_module_atexit_initialize(sky_module_t module)
{
    sky_library_atfinalize(sky_atexit_finalize);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_atexit_list),
            sky_list_create(NULL),
            SKY_TRUE);

    sky_module_setattr(
            module,
            "_clear",
            sky_function_createbuiltin(
                    "_clear",
                    sky_atexit__clear_doc,
                    (sky_native_code_function_t)sky_atexit__clear,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "_run_exitfuncs",
            sky_function_createbuiltin(
                    "_run_exitfuncs",
                    sky_atexit__run_exitfuncs_doc,
                    (sky_native_code_function_t)sky_atexit__run_exitfuncs,
                    SKY_DATA_TYPE_VOID,
                    NULL));

    sky_module_setattr(
            module,
            "register",
            sky_function_createbuiltin(
                    "register",
                    sky_atexit_register_doc,
                    (sky_native_code_function_t)sky_atexit_register,
                    SKY_DATA_TYPE_OBJECT,
                    "func", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "unregister",
            sky_function_createbuiltin(
                    "unregister",
                    sky_atexit_unregister_doc,
                    (sky_native_code_function_t)sky_atexit_unregister,
                    SKY_DATA_TYPE_VOID,
                    "func", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
}
