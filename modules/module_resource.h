#ifndef __SKYTHON_MODULES_MODULE_RESOURCE_H__
#define __SKYTHON_MODULES_MODULE_RESOURCE_H__ 1


#include "../core/skython.h"

#include <sys/resource.h>


SKY_CDECLS_BEGIN


extern sky_object_t
sky_resource_struct_rusage_create(struct rusage *r);


SKY_CDECLS_END


#endif  /* __SKYTHON_MODULES_MODULE_RESOURCE_H__ */
