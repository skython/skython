#include "../core/sky_private.h"
#include "module_resource.h"


static sky_type_t sky_resource_struct_rusage_type;

static const char sky_resource_struct_rusage_doc[] =
"struct_rusage: Result from getrusage.\n\n"
"This object may be accessed either as a tuple of\n"
"    (utime,stime,maxrss,ixrss,idrss,isrss,minflt,majflt,\n"
"    nswap,inblock,oublock,msgsnd,msgrcv,nsignals,nvcsw,nivcsw)\n"
"or via the attributes ru_utime, ru_stime, ru_maxrss, and so on.";

static sky_struct_sequence_field_t sky_resource_struct_rusage_fields[] =
{
    { "ru_utime",       "user time used", 0 },
    { "ru_stime",       "system time used", 0 },
    { "ru_maxrss",      "max. resident set size", 0 },
    { "ru_ixrss",       "shared memory size", 0 },
    { "ru_idrss",       "unshared data size", 0 },
    { "ru_isrss",       "unshared stack size", 0 },
    { "ru_minflt",      "page faults not requiring I/O", 0 },
    { "ru_majflt",      "page faults requiring I/O", 0 },
    { "ru_nswap",       "number of swap outs", 0 },
    { "ru_inblock",     "block input operations", 0 },
    { "ru_oublock",     "block output operations", 0 },
    { "ru_msgsnd",      "IPC messages sent", 0 },
    { "ru_msgrcv",      "IPC messages received", 0 },
    { "ru_nsignals",    "signals received", 0 },
    { "ru_nvcsw",       "voluntary context switches", 0 },
    { "ru_nivcsw",      "involuntary context switches", 0 },
};
static size_t sky_resource_struct_rusage_nfields =
        sizeof(sky_resource_struct_rusage_fields) /
        sizeof(sky_resource_struct_rusage_fields[0]);

sky_object_t
sky_resource_struct_rusage_create(struct rusage *r)
{
    double          stime, utime;
    sky_object_t    values[16];

    stime = (double)r->ru_stime.tv_sec + (r->ru_stime.tv_sec * 0.000001);
    utime = (double)r->ru_utime.tv_sec + (r->ru_utime.tv_sec * 0.000001);

    values[ 0] = sky_float_create(utime);
    values[ 1] = sky_float_create(stime);
    values[ 2] = sky_integer_create(r->ru_maxrss);
    values[ 3] = sky_integer_create(r->ru_ixrss);
    values[ 4] = sky_integer_create(r->ru_idrss);
    values[ 5] = sky_integer_create(r->ru_isrss);
    values[ 6] = sky_integer_create(r->ru_minflt);
    values[ 7] = sky_integer_create(r->ru_majflt);
    values[ 8] = sky_integer_create(r->ru_nswap);
    values[ 9] = sky_integer_create(r->ru_inblock);
    values[10] = sky_integer_create(r->ru_oublock);
    values[11] = sky_integer_create(r->ru_msgsnd);
    values[12] = sky_integer_create(r->ru_msgrcv);
    values[13] = sky_integer_create(r->ru_nsignals);
    values[14] = sky_integer_create(r->ru_nvcsw);
    values[15] = sky_integer_create(r->ru_nivcsw);

    return sky_struct_sequence_create(sky_resource_struct_rusage_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}


static sky_tuple_t
sky_resource_getrlimit(int resource)
{
    struct rlimit   r;

    if (resource < 0 || resource >= RLIM_NLIMITS) {
        sky_error_raise_string(sky_ValueError, "invalid resource specified");
    }
    if (getrlimit(resource, &r) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_tuple_pack(2, sky_integer_create(r.rlim_cur),
                             sky_integer_create(r.rlim_max));
}


static sky_object_t
sky_resource_getrusage(int who)
{
    struct rusage   r;

    if (getrusage(who, &r) == -1) {
        if (EINVAL == errno) {
            sky_error_raise_string(sky_ValueError, "invalid who parameter");
        }
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_resource_struct_rusage_create(&r);
}


static int
sky_resource_setrlimit(int resource, sky_object_t limits)
{
    struct rlimit   r;

    if (resource < 0 || resource >= RLIM_NLIMITS) {
        sky_error_raise_string(sky_ValueError, "invalid resource specified");
    }

    if (sizeof(r.rlim_cur) > sizeof(long)) {
        r.rlim_cur = sky_integer_value(sky_sequence_get(limits, 0),
                                       LLONG_MIN, LLONG_MAX,
                                       NULL);
        r.rlim_max = sky_integer_value(sky_sequence_get(limits, 1),
                                       LLONG_MIN, LLONG_MAX,
                                       NULL);
    }
    else {
        r.rlim_cur = sky_integer_value(sky_sequence_get(limits, 0),
                                       LONG_MIN, LONG_MAX,
                                       NULL);
        r.rlim_max = sky_integer_value(sky_sequence_get(limits, 1),
                                       LONG_MIN, LONG_MAX,
                                       NULL);
    }
    r.rlim_cur &= RLIM_INFINITY;
    r.rlim_max &= RLIM_INFINITY;

    if (setrlimit(resource, &r) == -1) {
        if (EINVAL == errno) {
            sky_error_raise_string(sky_ValueError,
                                   "current limit exceeds maximum limit");
        }
        if (EPERM == errno) {
            sky_error_raise_string(sky_ValueError,
                                   "not allowed to raise maximum limit");
        }
        return -1;
    }
    return 0;
}


void
skython_module_resource_initialize(sky_module_t module)
{
    sky_module_setattr(module, "error", sky_OSError);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_resource_struct_rusage_type),
            sky_struct_sequence_define(
                    "struct_rusage",
                    sky_resource_struct_rusage_doc,
                    sky_resource_struct_rusage_nfields,
                    sky_resource_struct_rusage_fields),
            SKY_TRUE);
    sky_module_setattr(
            module,
            "struct_rusage",
            sky_resource_struct_rusage_type);

    sky_module_setattr(
            module,
            "getpagesize",
            sky_function_createbuiltin(
                    "getpagesize",
                    NULL,
                    (sky_native_code_function_t)sky_system_pagesize,
                    SKY_DATA_TYPE_SIZE_T,
                    NULL));
    sky_module_setattr(
            module,
            "getrlimit",
            sky_function_createbuiltin(
                    "getrlimit",
                    NULL,
                    (sky_native_code_function_t)sky_resource_getrlimit,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "resource", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getrusage",
            sky_function_createbuiltin(
                    "getrusage",
                    NULL,
                    (sky_native_code_function_t)sky_resource_getrusage,
                    SKY_DATA_TYPE_OBJECT,
                    "who", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setrlimit",
            sky_function_createbuiltin(
                    "setrlimit",
                    NULL,
                    (sky_native_code_function_t)sky_resource_setrlimit,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "resource", SKY_DATA_TYPE_INT, NULL,
                    "limits", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

#define int_const(_n) \
        sky_module_setattr(module, #_n, sky_integer_create(_n));

#ifdef RLIMIT_CPU
    int_const(RLIMIT_CPU);
#endif
#ifdef RLIMIT_FSIZE
    int_const(RLIMIT_FSIZE);
#endif
#ifdef RLIMIT_DATA
    int_const(RLIMIT_DATA);
#endif
#ifdef RLIMIT_STACK
    int_const(RLIMIT_STACK);
#endif
#ifdef RLIMIT_CORE
    int_const(RLIMIT_CORE);
#endif
#ifdef RLIMIT_NOFILE
    int_const(RLIMIT_NOFILE);
#endif
#ifdef RLIMIT_OFILE
    int_const(RLIMIT_OFILE);
#endif
#ifdef RLIMIT_VMEM
    int_const(RLIMIT_VMEM);
#endif
#ifdef RLIMIT_AS
    int_const(RLIMIT_AS);
#endif
#ifdef RLIMIT_RSS
    int_const(RLIMIT_RSS);
#endif
#ifdef RLIMIT_NPROC
    int_const(RLIMIT_NPROC);
#endif
#ifdef RLIMIT_MEMLOCK
    int_const(RLIMIT_MEMLOCK);
#endif
#ifdef RLIMIT_SBSIZE
    int_const(RLIMIT_SBSIZE);
#endif

#ifdef RUSAGE_SELF
    int_const(RUSAGE_SELF);
#endif
#ifdef RUSAGE_CHILDREN
    int_const(RUSAGE_CHILDREN);
#endif
#ifdef RUSAGE_BOTH
    int_const(RUSAGE_BOTH);
#endif
#ifdef RUSAGE_THREAD
    int_const(RUSAGE_THREAD);
#endif

#ifdef RLIM_INFINITY
    int_const(RLIM_INFINITY);
#endif
}
