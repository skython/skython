#include "../core/skython.h"


SKY_MODULE_EXTERN const char *skython_module_binascii_doc;

SKY_MODULE_EXTERN void
skython_module_binascii_initialize(sky_module_t module);


static sky_type_t sky_binascii_Error = NULL;
static sky_type_t sky_binascii_Incomplete = NULL;


static const uint8_t *
sky_binascii_ascii_bytes(sky_object_t object, ssize_t *nbytes)
{
    if (sky_object_isa(object, sky_string_type)) {
        object = sky_string_createfromobject(object);
        if (!sky_string_isascii(object)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "string argument should contain only ASCII characters");
        }
        *nbytes = sky_object_len(object);
        return sky_string_cstring(object);
    }

    if (sky_buffer_check(object)) {
        sky_buffer_t    *buffer;

        buffer = sky_asset_malloc(sizeof(sky_buffer_t),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        *nbytes = buffer->len;
        return buffer->buf;
    }

    sky_error_raise_format(
            sky_TypeError,
            "argument should be bytes, buffer or ASCII string, not %@",
            sky_type_name(sky_object_type(object)));
}

static const uint8_t *
sky_binascii_binary_bytes(sky_object_t object, ssize_t *nbytes)
{
    if (sky_buffer_check(object)) {
        sky_buffer_t    *buffer;

        buffer = sky_asset_malloc(sizeof(sky_buffer_t),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        *nbytes = buffer->len;
        return buffer->buf;
    }

    sky_error_raise_format(
            sky_TypeError,
            "argument should be bytes or buffer, not %@",
            sky_type_name(sky_object_type(object)));
}


static const int8_t sky_binascii_a2b_base64_table[] = {
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1,  0, -1, -1,
    -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
    -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
};

static int
sky_binascii_a2b_base64_find_valid(const uint8_t *string, const uint8_t *end)
{
    int             count;
    const uint8_t   *c;

    count = 1;
    for (c = string; c < end; ++c) {
        if (*c <= 0x7f && 
            sky_binascii_a2b_base64_table[*c] != -1)
        {
            if (!count) {
                return *c;
            }
            --count;
        }
    }

    return -1;
}

static sky_bytes_t
sky_binascii_a2b_base64(sky_object_t ascii)
{
    static const uint8_t pad = '=';

    int             left_bits, q;
    ssize_t         ascii_nbytes, bin_nbytes;
    uint8_t         b, *bin_bytes, *outb;
    sky_bytes_t     bin;
    unsigned int    left_char;
    const uint8_t   *ascii_bytes, *c, *end;

    SKY_ASSET_BLOCK_BEGIN {
        ascii_bytes = sky_binascii_ascii_bytes(ascii, &ascii_nbytes);
        if (!ascii_nbytes) {
            bin = sky_bytes_empty;
        }
        else {
            if (ascii_nbytes > SSIZE_MAX - 3) {
                sky_error_raise_object(sky_MemoryError, "out of memory");
            }
            bin_nbytes = ((ascii_nbytes + 3) / 4) * 3;
            bin_bytes = sky_asset_malloc(bin_nbytes,
                                         SKY_ASSET_CLEANUP_ON_ERROR);
            outb = bin_bytes;

            left_char = 0;
            left_bits = q = 0;
            end = ascii_bytes + ascii_nbytes;
            for (c = ascii_bytes; c < end; ++c) {
                b = *c;
                if (b > 0x7f || b == '\r' || b == '\n' || b == ' ') {
                    continue;
                }

                /* Check for pad sequences and ignore the invalid ones. */
                if (b == pad) {
                    if (q < 2 ||
                        (2 == q &&
                         sky_binascii_a2b_base64_find_valid(c, end) != pad))
                    {
                        continue;
                    }

                    /* A pad sequence means no more input. */
                    left_bits = 0;
                    break;
                }

                b = sky_binascii_a2b_base64_table[b];
                if ((uint8_t)-1 == b) {
                    continue;
                }

                q = (q + 1) & 0x03;
                left_char = (left_char << 6) | b;
                left_bits += 6;
                if (left_bits >= 8) {
                    left_bits -= 8;
                    *outb++ = (left_char >> left_bits) & 0xff;
                    left_char &= ((1 << left_bits) - 1);
                }
            }

            if (left_bits != 0) {
                sky_error_raise_string(sky_binascii_Error,
                                       "Incorrect padding");
            }

            bin_nbytes = outb - bin_bytes;
            if (bin_nbytes < SSIZE_MAX) {
                bin_bytes = sky_asset_realloc(bin_bytes,
                                              bin_nbytes + 1,
                                              SKY_ASSET_CLEANUP_ON_ERROR,
                                              SKY_ASSET_UPDATE_FIRST_CURRENT);
                bin_bytes[bin_nbytes] = '\0';
            }
            bin = sky_bytes_createwithbytes(bin_bytes, bin_nbytes, sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return bin;
}

static const char sky_binascii_a2b_base64_doc[] =
"(ascii) -> bin. Decode a line of base64 data";


static sky_bytes_t
sky_binascii_a2b_hex(sky_object_t hexstr)
{
    ssize_t         nbytes;
    uint8_t         *buffer, *outb;
    sky_bytes_t     result;
    const uint8_t   *bytes, *c, *end;

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_binascii_ascii_bytes(hexstr, &nbytes);
        if (nbytes & 1) {
            sky_error_raise_string(sky_binascii_Error,
                                   "Odd-length string");
        }

        buffer = sky_asset_malloc((nbytes / 2) + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        buffer[nbytes / 2] = '\0';
        outb = buffer;

        end = bytes + nbytes;
        for (c = bytes; c < end; c += 2) {
            if (!sky_ctype_isxdigit(c[0]) || !sky_ctype_isxdigit(c[1])) {
                sky_error_raise_string(sky_binascii_Error,
                                       "Non-hexadecimal digit found");
            }
            *outb++ = (sky_ctype_hexdigit(c[0]) << 4) |
                       sky_ctype_hexdigit(c[1]);
        }

        result = sky_bytes_createwithbytes(buffer, nbytes / 2, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_binascii_a2b_hex_doc[] =
"a2b_hex(hexstr) -> s; Binary data of hexadecimal representation.\n\n"
"hexstr must contain an even number of hex digits (upper or lower case).\n"
"This function is also available as \"unhexlify()\"";


static sky_bytes_t
sky_binascii_a2b_uu(sky_object_t ascii)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             leftbits;
        ssize_t         ascii_nbytes, binary_nbytes, result_nbytes;
        uint8_t         *binary_bytes, c, *result_bytes;
        unsigned int    leftchar;
        const uint8_t   *ascii_bytes;

        ascii_bytes = sky_binascii_ascii_bytes(ascii, &ascii_nbytes);
        binary_nbytes = (*ascii_bytes++ - ' ') & 0x3F;
        --ascii_nbytes;

        if (!binary_nbytes) {
            result_nbytes = 0;
        }
        else {
            binary_bytes = sky_asset_malloc(binary_nbytes,
                                            SKY_ASSET_CLEANUP_ON_ERROR);
            result_bytes = binary_bytes;
            result_nbytes = binary_nbytes;

            leftbits = 0;
            leftchar = 0;
            while (binary_nbytes > 0) {
                c = (ascii_nbytes > 0 ? *ascii_bytes : 0);
                if ('\n' == c || '\r' == c || ascii_nbytes <= 0) {
                    c = 0;
                }
                else if (c < ' ' || c > (' ' + 64)) {
                    sky_error_raise_string(sky_binascii_Error,
                                           "Illegal char");
                }
                else {
                    c = (c - ' ') & 0x3F;
                }

                leftchar = (leftchar << 6) | c;
                leftbits += 6;
                if (leftbits >= 8) {
                    leftbits -= 8;
                    *binary_bytes++ = (leftchar >> leftbits) & 0xFF;
                    leftchar &= ((1 << leftbits) - 1);
                    --binary_nbytes;
                }

                ++ascii_bytes;
                --ascii_nbytes;
            }
        }

        /* Check for trailing garbage. Whitespace is fine, as is a '`'
         * character that is sometimes used as padding.
         */
        while (ascii_nbytes-- > 0) {
            c = *ascii_bytes++;
            if (c != ' ' && c != ' ' + 64 && c != '\n' && c != '\r') {
                sky_error_raise_string(sky_binascii_Error,
                                       "Trailing garbage");
            }
        }

        if (!result_nbytes) {
            result = sky_bytes_empty;
        }
        else {
            result = sky_bytes_createwithbytes(result_bytes,
                                               result_nbytes,
                                               sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_binascii_a2b_uu_doc[] =
"(ascii) -> bin. Decode a line of uuencoded data";


static sky_bytes_t
sky_binascii_b2a_base64(sky_object_t bin)
{
    static const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz"
                                "0123456789+/";

    int             left_bits;
    char            *ascii_bytes, *outc;
    ssize_t         ascii_nbytes, bin_nbytes;
    sky_bytes_t     ascii;
    unsigned int    left_char;
    const uint8_t   *bin_bytes, *c, *end;

    SKY_ASSET_BLOCK_BEGIN {
        bin_bytes = sky_binascii_binary_bytes(bin, &bin_nbytes);
        if (bin_nbytes > (SSIZE_MAX - 3) / 2) {
            sky_error_raise_string(sky_binascii_Error,
                                   "Too much data for base64 line");
        }

        ascii_nbytes = (bin_nbytes * 2) + 3;
        ascii_bytes = sky_asset_malloc(ascii_nbytes,
                                       SKY_ASSET_CLEANUP_ON_ERROR);
        outc = ascii_bytes;

        left_bits = 0;
        left_char = 0;
        end = bin_bytes + bin_nbytes;
        for (c = bin_bytes; c < end; ++c) {
            left_char = (left_char << 8) | *c;
            left_bits += 8;
            while (left_bits >= 6) {
                left_bits -= 6;
                *outc++ = table[(left_char >> left_bits) & 0x3F];
            }
        }
        if (2 == left_bits) {
            *outc++ = table[(left_char & 0x3) << 4];
            *outc++ = '=';
            *outc++ = '=';
        }
        else if (4 == left_bits) {
            *outc++ = table[(left_char & 0xF) << 2];
            *outc++ = '=';
        }
        *outc++ = '\n';

        ascii_nbytes = outc - ascii_bytes;
        if (ascii_nbytes < SSIZE_MAX) {
            ascii_bytes = sky_asset_realloc(ascii_bytes,
                                            ascii_nbytes + 1,
                                            SKY_ASSET_CLEANUP_ON_ERROR,
                                            SKY_ASSET_UPDATE_FIRST_CURRENT);
        }
        ascii = sky_bytes_createwithbytes(ascii_bytes, ascii_nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return ascii;
}

static const char sky_binascii_b2a_base64_doc[] =
"(bin) -> ascii. Base64-code line of data";


static sky_bytes_t
sky_binascii_b2a_hex(sky_object_t data)
{
    char            *buffer, *outc;
    ssize_t         nbytes;
    sky_bytes_t     result;
    const uint8_t   *bytes, *c, *end;

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_binascii_binary_bytes(data, &nbytes);

        buffer = sky_asset_malloc((nbytes * 2) + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        buffer[nbytes * 2] = '\0';
        outc = buffer;

        end = bytes + nbytes;
        for (c = bytes; c < end; ++c) {
            outc[0] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
            outc[1] = sky_ctype_lower_hexdigits[ *c       & 0xF];
            outc += 2;
        }

        result = sky_bytes_createwithbytes(buffer, nbytes * 2, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_binascii_b2a_hex_doc[] =
"b2a_hex(data) -> s; Hexadecimal representatino of binary data.\n\n"
"The return value is a bytes object.  This function is also\n"
"available as \"hexlify()\".";


static sky_bytes_t
sky_binascii_b2a_uu(sky_object_t bin)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             leftbits;
        char            *buffer, *p;
        ssize_t         nbytes;
        uint8_t         c;
        unsigned int    leftchar;
        const uint8_t   *bytes;

        bytes = sky_binascii_binary_bytes(bin, &nbytes);
        if (nbytes > 45) {
            /* A line of uuencoded data is at most 45 bytes. */
            sky_error_raise_string(sky_binascii_Error,
                                   "At most 45 bytes at once");
        }

        buffer = sky_asset_malloc(2 + (((nbytes + 2) / 3) * 4),
                                  SKY_ASSET_CLEANUP_ON_ERROR);
        p = buffer;

        leftbits = 0;
        leftchar = 0;
        *p++ = ' ' + (nbytes & 0x3F);
        while (nbytes > 0 || leftbits) {
            if (nbytes > 0) {
                leftchar = (leftchar << 8) | *bytes;
            }
            else {
                leftchar <<= 8;
            }

            for (leftbits += 8; leftbits >= 6; leftbits -= 6) {
                c = (leftchar >> (leftbits - 6)) & 0x3F;
                *p++ = c + ' ';
            }

            ++bytes;
            --nbytes;
        }
        *p++ = '\n';

        result = sky_bytes_createwithbytes(buffer, p - buffer, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_binascii_b2a_uu_doc[] =
"b2a_uu(bin) -> ascii. Uuencode line of data";


static uint32_t
sky_binascii_crc32(sky_object_t data, uint32_t oldcrc)
{
    ssize_t         nbytes;
    uint32_t        crc32;
    const uint8_t   *bytes;

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_binascii_binary_bytes(data, &nbytes);
        crc32 = sky_util_crc32(bytes, nbytes, oldcrc);
    } SKY_ASSET_BLOCK_END;

    return crc32;
}

static const char sky_binascii_crc32_doc[] =
"(data, oldcrc = 0) -> newcrc. Compute CRC-32 incrementally";


const char *skython_module_binascii_doc =
"Conversion between binary data and ASCII";

void
skython_module_binascii_initialize(sky_module_t module)
{
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_binascii_Error),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("Error"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_ValueError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "Error", sky_binascii_Error);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_binascii_Incomplete),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("Incomplete"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_Exception),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "Incomplete", sky_binascii_Incomplete);

    sky_module_setattr(
            module,
            "a2b_base64",
            sky_function_createbuiltin(
                    "a2b_base64",
                    sky_binascii_a2b_base64_doc,
                    (sky_native_code_function_t)sky_binascii_a2b_base64,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "ascii", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "b2a_base64",
            sky_function_createbuiltin(
                    "b2a_base64",
                    sky_binascii_b2a_base64_doc,
                    (sky_native_code_function_t)sky_binascii_b2a_base64,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "bin", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_module_setattr(
            module,
            "a2b_hex",
            sky_function_createbuiltin(
                    "a2b_hex",
                    sky_binascii_a2b_hex_doc,
                    (sky_native_code_function_t)sky_binascii_a2b_hex,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "hexstr", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "unhexlify",
            sky_function_createbuiltin(
                    "unhexlify",
                    sky_binascii_a2b_hex_doc,
                    (sky_native_code_function_t)sky_binascii_a2b_hex,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "hexstr", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "b2a_hex",
            sky_function_createbuiltin(
                    "b2a_hex",
                    sky_binascii_b2a_hex_doc,
                    (sky_native_code_function_t)sky_binascii_b2a_hex,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "hexlify",
            sky_function_createbuiltin(
                    "hexlify",
                    sky_binascii_b2a_hex_doc,
                    (sky_native_code_function_t)sky_binascii_b2a_hex,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    /* TODO a2b_hqx() */
    /* TODO b2a_hqx() */
    /* TODO crc_hqx() */
    /* TODO rlecode_hqx() */
    /* TODO rledecode_hqx() */

    /* TODO a2b_qp() */
    /* TODO b2a_qp() */

    sky_module_setattr(
            module,
            "a2b_uu",
            sky_function_createbuiltin(
                    "a2b_uu",
                    sky_binascii_a2b_uu_doc,
                    (sky_native_code_function_t)sky_binascii_a2b_uu,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "ascii", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "b2a_uu",
            sky_function_createbuiltin(
                    "b2a_uu",
                    sky_binascii_b2a_uu_doc,
                    (sky_native_code_function_t)sky_binascii_b2a_uu,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "bin", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_module_setattr(
            module,
            "crc32",
            sky_function_createbuiltin(
                    "crc32",
                    sky_binascii_crc32_doc,
                    (sky_native_code_function_t)sky_binascii_crc32,
                    SKY_DATA_TYPE_UINT32,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "oldcrc", SKY_DATA_TYPE_UINT32, sky_integer_zero,
                    NULL));
}
