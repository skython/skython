#include "../core/skython.h"
#include "../core/sky_marshal.h"


static const char sky_marshal_dump_doc[] =
"dump(value, file[, version])\n"
"\n"
"Write the value on the open file. The value must be a supported type.\n"
"The file must be an open file object such as sys.stdout or returned by\n"
"open() or os.popen(). It must be opened in binary mode ('wb' or 'w+b').\n"
"\n"
"If the value has (or contains an object that has) an unsupported type, a\n"
"ValueError exception is raised — but garbage data will also be written\n"
"to the file. The object will not be properly read back by load()\n"
"\n"
"The version argument indicates the data format that dump should use.";


static const char sky_marshal_dumps_doc[] =
"dumps(value[, version])\n"
"\n"
"Return the string that would be written to a file by dump(value, file).\n"
"The value must be a supported type. Raise a ValueError exception if\n"
"value has (or contains an object that has) an unsupported type.\n"
"\n"
"The version argument indicates the data format that dumps should use.";


static const char sky_marshal_load_doc[] =
"load(file)\n"
"\n"
"Read one value from the open file and return it. If no valid value is\n"
"read (e.g. because the data has a different Python version’s\n"
"incompatible marshal format), raise EOFError, ValueError or TypeError.\n"
"The file must be an open file object opened in binary mode ('rb' or\n"
"'r+b').\n"
"\n"
"Note: If an object containing an unsupported type was marshalled with\n"
"dump(), load() will substitute None for the unmarshallable type.";


static const char sky_marshal_loads_doc[] =
"loads(bytes)\n"
"\n"
"Convert the bytes object to a value. If no valid value is found, raise\n"
"EOFError, ValueError or TypeError. Extra characters in the input are\n"
"ignored.";


void
skython_module_marshal_initialize(sky_module_t module)
{
    static const char doc[] =
"This module contains functions that can read and write Python values in\n"
"a binary format. The format is specific to Python, but independent of\n"
"machine architecture issues.\n"
"\n"
"Not all Python object types are supported; in general, only objects\n"
"whose value is independent from a particular invocation of Python can be\n"
"written and read by this module. The following types are supported:\n"
"None, integers, floating point numbers, strings, bytes, bytearrays,\n"
"tuples, lists, sets, dictionaries, and code objects, where it\n"
"should be understood that tuples, lists and dictionaries are only\n"
"supported as long as the values contained therein are themselves\n"
"supported; and recursive lists and dictionaries should not be written\n"
"(they will cause infinite loops).\n"
"\n"
"Variables:\n"
"\n"
"version -- indicates the format that the module uses.\n"
"\n"
"Functions:\n"
"\n"
"dump() -- write value to a file\n"
"load() -- read value from a file\n"
"dumps() -- write value to a string\n"
"loads() -- read value from a string";

    sky_integer_t   version = sky_integer_create(SKY_MARSHAL_FORMAT_VERSION);

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_module_setattr(
            module,
            "dump",
            sky_function_createbuiltin(
                    "dump",
                    sky_marshal_dump_doc,
                    (sky_native_code_function_t)sky_marshal_dump,
                    SKY_DATA_TYPE_VOID,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "file", SKY_DATA_TYPE_OBJECT, NULL,
                    "version", SKY_DATA_TYPE_UINT32, version,
                    NULL));
    sky_module_setattr(
            module,
            "dumps",
            sky_function_createbuiltin(
                    "dumps",
                    sky_marshal_dumps_doc,
                    (sky_native_code_function_t)sky_marshal_dumps,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "version", SKY_DATA_TYPE_UINT32, version,
                    NULL));

    sky_module_setattr(
            module,
            "load",
            sky_function_createbuiltin(
                    "load",
                    sky_marshal_load_doc,
                    (sky_native_code_function_t)sky_marshal_load,
                    SKY_DATA_TYPE_OBJECT,
                    "file", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "loads",
            sky_function_createbuiltin(
                    "loads",
                    sky_marshal_loads_doc,
                    (sky_native_code_function_t)sky_marshal_loads,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_module_setattr(module, "version", version);
}
