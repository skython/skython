#if defined(__APPLE__)
#   define _DARWIN_C_SOURCE
#endif

#include "../core/sky_private.h"
#include "../core/sky_codec_private.h"
#include "module_resource.h"

#include <fcntl.h>
#include <sys/stat.h>

#if defined(__APPLE__)
#   include <crt_externs.h>
#endif
#if defined(HAVE_DIRENT_H)
#   include <dirent.h>
#endif
#if defined(HAVE_DLFCN_H)
#   include <dlfcn.h>
#endif
#if defined(HAVE_GRP_H)
#   include <grp.h>
#endif
#if defined(HAVE_PTY_H)
#   include <pty.h>
#endif
#if defined(HAVE_SCHED_H)
#   include <sched.h>
#endif
#if defined(HAVE_UTIL_H)
#   include <util.h>
#endif
#if defined(HAVE_SYSEXITS_H)
#   include <sysexits.h>
#endif
#if defined(HAVE_SYS_IOCTL_H)
#   include <sys/ioctl.h>
#endif
#if defined(HAVE_SYS_LOCK_H)
#   include <sys/lock.h>
#endif
#if defined(HAVE_SYS_PARAM_H)
#   include <sys/param.h>
#endif
#if defined(HAVE_SYS_SENDFILE_H)
#   include <sys/sendfile.h>
#endif
#if defined(HAVE_SYS_STATVFS_H)
#   include <sys/statvfs.h>
#endif
#if defined(HAVE_SYS_TIME_H)
#   include <sys/time.h>
#endif
#if defined(HAVE_SYS_TIMES_H)
#   include <sys/times.h>
#endif
#if defined(HAVE_SYS_UIO_H)
#   include <sys/uio.h>
#endif
#if defined(HAVE_SYS_UTSNAME_H)
#   include <sys/utsname.h>
#endif
#if defined(HAVE_SYS_WAIT_H)
#   include <sys/wait.h>
#endif
#if defined(HAVE_SYS_XATTR_H)
#   include <sys/xattr.h>
#endif

#if defined(HAVE_SCHED_SETAFFINITY) && !defined(CPU_ALLOC)
#   undef HAVE_SCHED_SETAFFINITY
#endif
#if defined(HAVE_WAITID) && defined(__APPLE__)
#   undef HAVE_WAITID
#endif

#if defined(__linux__)
#   undef HAVE_CHFLAGS
#   undef HAVE_LCHMOD
#endif


static long sky_posix_ticks_per_second;


typedef struct sky_posix_constant_s {
    sky_hashtable_item_t                hash;
    const char *                        name;
    int                                 value;
} sky_posix_constant_t;

#define SKY_POSIX_CONST(_name)  { SKY_HASHTABLE_ITEM_INITIALIZER,   \
                                  &(#_name[1]),                     \
                                  _name }

static sky_hashtable_t sky_posix_pathconf_table;
static sky_posix_constant_t sky_posix_pathconf_consts[] = {
#ifdef _PC_ABI_AIO_XFER_MAX
    SKY_POSIX_CONST(_PC_ABI_AIO_XFER_MAX),
#endif
#ifdef _PC_ABI_ASYNC_IO
    SKY_POSIX_CONST(_PC_ABI_ASYNC_IO),
#endif
#ifdef _PC_ASYNC_IO
    SKY_POSIX_CONST(_PC_ASYNC_IO),
#endif
#ifdef _PC_CHOWN_RESTRICTED
    SKY_POSIX_CONST(_PC_CHOWN_RESTRICTED),
#endif
#ifdef _PC_FILESIZEBITS
    SKY_POSIX_CONST(_PC_FILESIZEBITS),
#endif
#ifdef _PC_LAST
    SKY_POSIX_CONST(_PC_LAST),
#endif
#ifdef _PC_LINK_MAX
    SKY_POSIX_CONST(_PC_LINK_MAX),
#endif
#ifdef _PC_MAX_CANON
    SKY_POSIX_CONST(_PC_MAX_CANON),
#endif
#ifdef _PC_MAX_INPUT
    SKY_POSIX_CONST(_PC_MAX_INPUT),
#endif
#ifdef _PC_NAME_MAX
    SKY_POSIX_CONST(_PC_NAME_MAX),
#endif
#ifdef _PC_NO_TRUNC
    SKY_POSIX_CONST(_PC_NO_TRUNC),
#endif
#ifdef _PC_PATH_MAX
    SKY_POSIX_CONST(_PC_PATH_MAX),
#endif
#ifdef _PC_PIPE_BUF
    SKY_POSIX_CONST(_PC_PIPE_BUF),
#endif
#ifdef _PC_PRIO_IO
    SKY_POSIX_CONST(_PC_PRIO_IO),
#endif
#ifdef _PC_SOCK_MAXBUF
    SKY_POSIX_CONST(_PC_SOCK_MAXBUF),
#endif
#ifdef _PC_SYNC_IO
    SKY_POSIX_CONST(_PC_SYNC_IO),
#endif
#ifdef _PC_VDISABLE
    SKY_POSIX_CONST(_PC_VDISABLE),
#endif
#ifdef _PC_ACL_ENABLED
    SKY_POSIX_CONST(_PC_ACL_ENABLED),
#endif
#ifdef _PC_MIN_HOLE_SIZE
    SKY_POSIX_CONST(_PC_MIN_HOLE_SIZE),
#endif
#ifdef _PC_ALLOC_SIZ_MIN
    SKY_POSIX_CONST(_PC_ALLOC_SIZ_MIN),
#endif
#ifdef _PC_REC_INCR_XFER_SIZE
    SKY_POSIX_CONST(_PC_REC_INCR_XFER_SIZE),
#endif
#ifdef _PC_REC_MAX_XFER_SIZE
    SKY_POSIX_CONST(_PC_REC_MAX_XFER_SIZE),
#endif
#ifdef _PC_REC_XFER_ALIGN
    SKY_POSIX_CONST(_PC_REC_XFER_ALIGN),
#endif
#ifdef _PC_SYMLINK_MAX
    SKY_POSIX_CONST(_PC_SYMLINK_MAX),
#endif
#ifdef _PC_XATTR_ENABLED
    SKY_POSIX_CONST(_PC_XATTR_ENABLED),
#endif
#ifdef _PC_XATTR_EXISTS
    SKY_POSIX_CONST(_PC_XATTR_EXISTS),
#endif
#ifdef _PC_TIMESTAMP_RESOLUTION
    SKY_POSIX_CONST(_PC_TIMESTAMP_RESOLUTION),
#endif
    { SKY_HASHTABLE_ITEM_INITIALIZER, NULL, 0 }
};

static sky_hashtable_t sky_posix_confstr_table;
static sky_posix_constant_t sky_posix_confstr_consts[] = {
#ifdef _CS_ARCHITECTURE
    SKY_POSIX_CONST(_CS_ARCHITECTURE),
#endif
#ifdef _CS_GNU_LIBC_VERSION
    SKY_POSIX_CONST(_CS_GNU_LIBC_VERSION),
#endif
#ifdef _CS_GNU_LIBPTHREAD_VERSION
    SKY_POSIX_CONST(_CS_GNU_LIBPTHREAD_VERSION),
#endif
#ifdef _CS_HOSTNAME
    SKY_POSIX_CONST(_CS_HOSTNAME),
#endif
#ifdef _CS_HW_PROVIDER
    SKY_POSIX_CONST(_CS_HW_PROVIDER),
#endif
#ifdef _CS_HW_SERIAL
    SKY_POSIX_CONST(_CS_HW_SERIAL),
#endif
#ifdef _CS_INITTAB_NAME
    SKY_POSIX_CONST(_CS_INITTAB_NAME),
#endif
#ifdef _CS_LFS64_CFLAGS
    SKY_POSIX_CONST(_CS_LFS64_CFLAGS),
#endif
#ifdef _CS_LFS64_LDFLAGS
    SKY_POSIX_CONST(_CS_LFS64_LDFLAGS),
#endif
#ifdef _CS_LFS64_LIBS
    SKY_POSIX_CONST(_CS_LFS64_LIBS),
#endif
#ifdef _CS_LFS64_LINTFLAGS
    SKY_POSIX_CONST(_CS_LFS64_LINTFLAGS),
#endif
#ifdef _CS_LFS_CFLAGS
    SKY_POSIX_CONST(_CS_LFS_CFLAGS),
#endif
#ifdef _CS_LFS_LDFLAGS
    SKY_POSIX_CONST(_CS_LFS_LDFLAGS),
#endif
#ifdef _CS_LFS_LIBS
    SKY_POSIX_CONST(_CS_LFS_LIBS),
#endif
#ifdef _CS_LFS_LINTFLAGS
    SKY_POSIX_CONST(_CS_LFS_LINTFLAGS),
#endif
#ifdef _CS_MACHINE
    SKY_POSIX_CONST(_CS_MACHINE),
#endif
#ifdef _CS_PATH
    SKY_POSIX_CONST(_CS_PATH),
#endif
#ifdef _CS_RELEASE
    SKY_POSIX_CONST(_CS_RELEASE),
#endif
#ifdef _CS_SRPC_DOMAIN
    SKY_POSIX_CONST(_CS_SRPC_DOMAIN),
#endif
#ifdef _CS_SYSNAME
    SKY_POSIX_CONST(_CS_SYSNAME),
#endif
#ifdef _CS_VERSION
    SKY_POSIX_CONST(_CS_VERSION),
#endif
#ifdef _CS_XBS5_ILP32_OFF32_CFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFF32_CFLAGS),
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LDFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFF32_LDFLAGS),
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LIBS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFF32_LIBS),
#endif
#ifdef _CS_XBS5_ILP32_OFF32_LINTFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFF32_LINTFLAGS),
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_CFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFFBIG_CFLAGS),
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LDFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFFBIG_LDFLAGS),
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LIBS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFFBIG_LIBS),
#endif
#ifdef _CS_XBS5_ILP32_OFFBIG_LINTFLAGS
    SKY_POSIX_CONST(_CS_XBS5_ILP32_OFFBIG_LINTFLAGS),
#endif
#ifdef _CS_XBS5_LP64_OFF64_CFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LP64_OFF64_CFLAGS),
#endif
#ifdef _CS_XBS5_LP64_OFF64_LDFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LP64_OFF64_LDFLAGS),
#endif
#ifdef _CS_XBS5_LP64_OFF64_LIBS
    SKY_POSIX_CONST(_CS_XBS5_LP64_OFF64_LIBS),
#endif
#ifdef _CS_XBS5_LP64_OFF64_LINTFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LP64_OFF64_LINTFLAGS),
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_CFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LPBIG_OFFBIG_CFLAGS),
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LDFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LPBIG_OFFBIG_LDFLAGS),
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LIBS
    SKY_POSIX_CONST(_CS_XBS5_LPBIG_OFFBIG_LIBS),
#endif
#ifdef _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS
    SKY_POSIX_CONST(_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS),
#endif
#ifdef _MIPS_CS_AVAIL_PROCESSORS
    SKY_POSIX_CONST(_MIPS_CS_AVAIL_PROCESSORS),
#endif
#ifdef _MIPS_CS_BASE
    SKY_POSIX_CONST(_MIPS_CS_BASE),
#endif
#ifdef _MIPS_CS_HOSTID
    SKY_POSIX_CONST(_MIPS_CS_HOSTID),
#endif
#ifdef _MIPS_CS_HW_NAME
    SKY_POSIX_CONST(_MIPS_CS_HW_NAME),
#endif
#ifdef _MIPS_CS_NUM_PROCESSORS
    SKY_POSIX_CONST(_MIPS_CS_NUM_PROCESSORS),
#endif
#ifdef _MIPS_CS_OSREL_MAJ
    SKY_POSIX_CONST(_MIPS_CS_OSREL_MAJ),
#endif
#ifdef _MIPS_CS_OSREL_MIN
    SKY_POSIX_CONST(_MIPS_CS_OSREL_MIN),
#endif
#ifdef _MIPS_CS_OSREL_PATCH
    SKY_POSIX_CONST(_MIPS_CS_OSREL_PATCH),
#endif
#ifdef _MIPS_CS_OS_NAME
    SKY_POSIX_CONST(_MIPS_CS_OS_NAME),
#endif
#ifdef _MIPS_CS_OS_PROVIDER
    SKY_POSIX_CONST(_MIPS_CS_OS_PROVIDER),
#endif
#ifdef _MIPS_CS_PROCESSORS
    SKY_POSIX_CONST(_MIPS_CS_PROCESSORS),
#endif
#ifdef _MIPS_CS_SERIAL
    SKY_POSIX_CONST(_MIPS_CS_SERIAL),
#endif
#ifdef _MIPS_CS_VENDOR
    SKY_POSIX_CONST(_MIPS_CS_VENDOR),
#endif
    { SKY_HASHTABLE_ITEM_INITIALIZER, NULL, 0 }
};

static sky_hashtable_t sky_posix_sysconf_table;
static sky_posix_constant_t sky_posix_sysconf_consts[] = {
#ifdef _SC_2_CHAR_TERM
    SKY_POSIX_CONST(_SC_2_CHAR_TERM),
#endif
#ifdef _SC_2_C_BIND
    SKY_POSIX_CONST(_SC_2_C_BIND),
#endif
#ifdef _SC_2_C_DEV
    SKY_POSIX_CONST(_SC_2_C_DEV),
#endif
#ifdef _SC_2_C_VERSION
    SKY_POSIX_CONST(_SC_2_C_VERSION),
#endif
#ifdef _SC_2_FORT_DEV
    SKY_POSIX_CONST(_SC_2_FORT_DEV),
#endif
#ifdef _SC_2_FORT_RUN
    SKY_POSIX_CONST(_SC_2_FORT_RUN),
#endif
#ifdef _SC_2_LOCALEDEF
    SKY_POSIX_CONST(_SC_2_LOCALEDEF),
#endif
#ifdef _SC_2_SW_DEV
    SKY_POSIX_CONST(_SC_2_SW_DEV),
#endif
#ifdef _SC_2_UPE
    SKY_POSIX_CONST(_SC_2_UPE),
#endif
#ifdef _SC_2_VERSION
    SKY_POSIX_CONST(_SC_2_VERSION),
#endif
#ifdef _SC_ABI_ASYNCHRONOUS_IO
    SKY_POSIX_CONST(_SC_ABI_ASYNCHRONOUS_IO),
#endif
#ifdef _SC_ACL
    SKY_POSIX_CONST(_SC_ACL),
#endif
#ifdef _SC_AIO_LISTIO_MAX
    SKY_POSIX_CONST(_SC_AIO_LISTIO_MAX),
#endif
#ifdef _SC_AIO_MAX
    SKY_POSIX_CONST(_SC_AIO_MAX),
#endif
#ifdef _SC_AIO_PRIO_DELTA_MAX
    SKY_POSIX_CONST(_SC_AIO_PRIO_DELTA_MAX),
#endif
#ifdef _SC_ARG_MAX
    SKY_POSIX_CONST(_SC_ARG_MAX),
#endif
#ifdef _SC_ASYNCHRONOUS_IO
    SKY_POSIX_CONST(_SC_ASYNCHRONOUS_IO),
#endif
#ifdef _SC_ATEXIT_MAX
    SKY_POSIX_CONST(_SC_ATEXIT_MAX),
#endif
#ifdef _SC_AUDIT
    SKY_POSIX_CONST(_SC_AUDIT),
#endif
#ifdef _SC_AVPHYS_PAGES
    SKY_POSIX_CONST(_SC_AVPHYS_PAGES),
#endif
#ifdef _SC_BC_BASE_MAX
    SKY_POSIX_CONST(_SC_BC_BASE_MAX),
#endif
#ifdef _SC_BC_DIM_MAX
    SKY_POSIX_CONST(_SC_BC_DIM_MAX),
#endif
#ifdef _SC_BC_SCALE_MAX
    SKY_POSIX_CONST(_SC_BC_SCALE_MAX),
#endif
#ifdef _SC_BC_STRING_MAX
    SKY_POSIX_CONST(_SC_BC_STRING_MAX),
#endif
#ifdef _SC_CAP
    SKY_POSIX_CONST(_SC_CAP),
#endif
#ifdef _SC_CHARCLASS_NAME_MAX
    SKY_POSIX_CONST(_SC_CHARCLASS_NAME_MAX),
#endif
#ifdef _SC_CHAR_BIT
    SKY_POSIX_CONST(_SC_CHAR_BIT),
#endif
#ifdef _SC_CHAR_MAX
    SKY_POSIX_CONST(_SC_CHAR_MAX),
#endif
#ifdef _SC_CHAR_MIN
    SKY_POSIX_CONST(_SC_CHAR_MIN),
#endif
#ifdef _SC_CHILD_MAX
    SKY_POSIX_CONST(_SC_CHILD_MAX),
#endif
#ifdef _SC_CLK_TCK
    SKY_POSIX_CONST(_SC_CLK_TCK),
#endif
#ifdef _SC_COHER_BLKSZ
    SKY_POSIX_CONST(_SC_COHER_BLKSZ),
#endif
#ifdef _SC_COLL_WEIGHTS_MAX
    SKY_POSIX_CONST(_SC_COLL_WEIGHTS_MAX),
#endif
#ifdef _SC_DCACHE_ASSOC
    SKY_POSIX_CONST(_SC_DCACHE_ASSOC),
#endif
#ifdef _SC_DCACHE_BLKSZ
    SKY_POSIX_CONST(_SC_DCACHE_BLKSZ),
#endif
#ifdef _SC_DCACHE_LINESZ
    SKY_POSIX_CONST(_SC_DCACHE_LINESZ),
#endif
#ifdef _SC_DCACHE_SZ
    SKY_POSIX_CONST(_SC_DCACHE_SZ),
#endif
#ifdef _SC_DCACHE_TBLKSZ
    SKY_POSIX_CONST(_SC_DCACHE_TBLKSZ),
#endif
#ifdef _SC_DELAYTIMER_MAX
    SKY_POSIX_CONST(_SC_DELAYTIMER_MAX),
#endif
#ifdef _SC_EQUIV_CLASS_MAX
    SKY_POSIX_CONST(_SC_EQUIV_CLASS_MAX),
#endif
#ifdef _SC_EXPR_NEST_MAX
    SKY_POSIX_CONST(_SC_EXPR_NEST_MAX),
#endif
#ifdef _SC_FSYNC
    SKY_POSIX_CONST(_SC_FSYNC),
#endif
#ifdef _SC_GETGR_R_SIZE_MAX
    SKY_POSIX_CONST(_SC_GETGR_R_SIZE_MAX),
#endif
#ifdef _SC_GETPW_R_SIZE_MAX
    SKY_POSIX_CONST(_SC_GETPW_R_SIZE_MAX),
#endif
#ifdef _SC_ICACHE_ASSOC
    SKY_POSIX_CONST(_SC_ICACHE_ASSOC),
#endif
#ifdef _SC_ICACHE_BLKSZ
    SKY_POSIX_CONST(_SC_ICACHE_BLKSZ),
#endif
#ifdef _SC_ICACHE_LINESZ
    SKY_POSIX_CONST(_SC_ICACHE_LINESZ),
#endif
#ifdef _SC_ICACHE_SZ
    SKY_POSIX_CONST(_SC_ICACHE_SZ),
#endif
#ifdef _SC_INF
    SKY_POSIX_CONST(_SC_INF),
#endif
#ifdef _SC_INT_MAX
    SKY_POSIX_CONST(_SC_INT_MAX),
#endif
#ifdef _SC_INT_MIN
    SKY_POSIX_CONST(_SC_INT_MIN),
#endif
#ifdef _SC_IOV_MAX
    SKY_POSIX_CONST(_SC_IOV_MAX),
#endif
#ifdef _SC_IP_SECOPTS
    SKY_POSIX_CONST(_SC_IP_SECOPTS),
#endif
#ifdef _SC_JOB_CONTROL
    SKY_POSIX_CONST(_SC_JOB_CONTROL),
#endif
#ifdef _SC_KERN_POINTERS
    SKY_POSIX_CONST(_SC_KERN_POINTERS),
#endif
#ifdef _SC_KERN_SIM
    SKY_POSIX_CONST(_SC_KERN_SIM),
#endif
#ifdef _SC_LINE_MAX
    SKY_POSIX_CONST(_SC_LINE_MAX),
#endif
#ifdef _SC_LOGIN_NAME_MAX
    SKY_POSIX_CONST(_SC_LOGIN_NAME_MAX),
#endif
#ifdef _SC_LOGNAME_MAX
    SKY_POSIX_CONST(_SC_LOGNAME_MAX),
#endif
#ifdef _SC_LONG_BIT
    SKY_POSIX_CONST(_SC_LONG_BIT),
#endif
#ifdef _SC_MAC
    SKY_POSIX_CONST(_SC_MAC),
#endif
#ifdef _SC_MAPPED_FILES
    SKY_POSIX_CONST(_SC_MAPPED_FILES),
#endif
#ifdef _SC_MAXPID
    SKY_POSIX_CONST(_SC_MAXPID),
#endif
#ifdef _SC_MB_LEN_MAX
    SKY_POSIX_CONST(_SC_MB_LEN_MAX),
#endif
#ifdef _SC_MEMLOCK
    SKY_POSIX_CONST(_SC_MEMLOCK),
#endif
#ifdef _SC_MEMLOCK_RANGE
    SKY_POSIX_CONST(_SC_MEMLOCK_RANGE),
#endif
#ifdef _SC_MEMORY_PROTECTION
    SKY_POSIX_CONST(_SC_MEMORY_PROTECTION),
#endif
#ifdef _SC_MESSAGE_PASSING
    SKY_POSIX_CONST(_SC_MESSAGE_PASSING),
#endif
#ifdef _SC_MMAP_FIXED_ALIGNMENT
    SKY_POSIX_CONST(_SC_MMAP_FIXED_ALIGNMENT),
#endif
#ifdef _SC_MQ_OPEN_MAX
    SKY_POSIX_CONST(_SC_MQ_OPEN_MAX),
#endif
#ifdef _SC_MQ_PRIO_MAX
    SKY_POSIX_CONST(_SC_MQ_PRIO_MAX),
#endif
#ifdef _SC_NACLS_MAX
    SKY_POSIX_CONST(_SC_NACLS_MAX),
#endif
#ifdef _SC_NGROUPS_MAX
    SKY_POSIX_CONST(_SC_NGROUPS_MAX),
#endif
#ifdef _SC_NL_ARGMAX
    SKY_POSIX_CONST(_SC_NL_ARGMAX),
#endif
#ifdef _SC_NL_LANGMAX
    SKY_POSIX_CONST(_SC_NL_LANGMAX),
#endif
#ifdef _SC_NL_MSGMAX
    SKY_POSIX_CONST(_SC_NL_MSGMAX),
#endif
#ifdef _SC_NL_NMAX
    SKY_POSIX_CONST(_SC_NL_NMAX),
#endif
#ifdef _SC_NL_SETMAX
    SKY_POSIX_CONST(_SC_NL_SETMAX),
#endif
#ifdef _SC_NL_TEXTMAX
    SKY_POSIX_CONST(_SC_NL_TEXTMAX),
#endif
#ifdef _SC_NPROCESSORS_CONF
    SKY_POSIX_CONST(_SC_NPROCESSORS_CONF),
#endif
#ifdef _SC_NPROCESSORS_ONLN
    SKY_POSIX_CONST(_SC_NPROCESSORS_ONLN),
#endif
#ifdef _SC_NPROC_CONF
    SKY_POSIX_CONST(_SC_NPROC_CONF),
#endif
#ifdef _SC_NPROC_ONLN
    SKY_POSIX_CONST(_SC_NPROC_ONLN),
#endif
#ifdef _SC_NZERO
    SKY_POSIX_CONST(_SC_NZERO),
#endif
#ifdef _SC_OPEN_MAX
    SKY_POSIX_CONST(_SC_OPEN_MAX),
#endif
#ifdef _SC_PAGESIZE
    SKY_POSIX_CONST(_SC_PAGESIZE),
#endif
#ifdef _SC_PAGE_SIZE
    SKY_POSIX_CONST(_SC_PAGE_SIZE),
#endif
#ifdef _SC_PASS_MAX
    SKY_POSIX_CONST(_SC_PASS_MAX),
#endif
#ifdef _SC_PHYS_PAGES
    SKY_POSIX_CONST(_SC_PHYS_PAGES),
#endif
#ifdef _SC_PII
    SKY_POSIX_CONST(_SC_PII),
#endif
#ifdef _SC_PII_INTERNET
    SKY_POSIX_CONST(_SC_PII_INTERNET),
#endif
#ifdef _SC_PII_INTERNET_DGRAM
    SKY_POSIX_CONST(_SC_PII_INTERNET_DGRAM),
#endif
#ifdef _SC_PII_INTERNET_STREAM
    SKY_POSIX_CONST(_SC_PII_INTERNET_STREAM),
#endif
#ifdef _SC_PII_OSI
    SKY_POSIX_CONST(_SC_PII_OSI),
#endif
#ifdef _SC_PII_OSI_CLTS
    SKY_POSIX_CONST(_SC_PII_OSI_CLTS),
#endif
#ifdef _SC_PII_OSI_COTS
    SKY_POSIX_CONST(_SC_PII_OSI_COTS),
#endif
#ifdef _SC_PII_OSI_M
    SKY_POSIX_CONST(_SC_PII_OSI_M),
#endif
#ifdef _SC_PII_SOCKET
    SKY_POSIX_CONST(_SC_PII_SOCKET),
#endif
#ifdef _SC_PII_XTI
    SKY_POSIX_CONST(_SC_PII_XTI),
#endif
#ifdef _SC_POLL
    SKY_POSIX_CONST(_SC_POLL),
#endif
#ifdef _SC_PRIORITIZED_IO
    SKY_POSIX_CONST(_SC_PRIORITIZED_IO),
#endif
#ifdef _SC_PRIORITY_SCHEDULING
    SKY_POSIX_CONST(_SC_PRIORITY_SCHEDULING),
#endif
#ifdef _SC_REALTIME_SIGNALS
    SKY_POSIX_CONST(_SC_REALTIME_SIGNALS),
#endif
#ifdef _SC_RE_DUP_MAX
    SKY_POSIX_CONST(_SC_RE_DUP_MAX),
#endif
#ifdef _SC_RTSIG_MAX
    SKY_POSIX_CONST(_SC_RTSIG_MAX),
#endif
#ifdef _SC_SAVED_IDS
    SKY_POSIX_CONST(_SC_SAVED_IDS),
#endif
#ifdef _SC_SCHAR_MAX
    SKY_POSIX_CONST(_SC_SCHAR_MAX),
#endif
#ifdef _SC_SCHAR_MIN
    SKY_POSIX_CONST(_SC_SCHAR_MIN),
#endif
#ifdef _SC_SELECT
    SKY_POSIX_CONST(_SC_SELECT),
#endif
#ifdef _SC_SEMAPHORES
    SKY_POSIX_CONST(_SC_SEMAPHORES),
#endif
#ifdef _SC_SEM_NSEMS_MAX
    SKY_POSIX_CONST(_SC_SEM_NSEMS_MAX),
#endif
#ifdef _SC_SEM_VALUE_MAX
    SKY_POSIX_CONST(_SC_SEM_VALUE_MAX),
#endif
#ifdef _SC_SHARED_MEMORY_OBJECTS
    SKY_POSIX_CONST(_SC_SHARED_MEMORY_OBJECTS),
#endif
#ifdef _SC_SHRT_MAX
    SKY_POSIX_CONST(_SC_SHRT_MAX),
#endif
#ifdef _SC_SHRT_MIN
    SKY_POSIX_CONST(_SC_SHRT_MIN),
#endif
#ifdef _SC_SIGQUEUE_MAX
    SKY_POSIX_CONST(_SC_SIGQUEUE_MAX),
#endif
#ifdef _SC_SIGRT_MAX
    SKY_POSIX_CONST(_SC_SIGRT_MAX),
#endif
#ifdef _SC_SIGRT_MIN
    SKY_POSIX_CONST(_SC_SIGRT_MIN),
#endif
#ifdef _SC_SOFTPOWER
    SKY_POSIX_CONST(_SC_SOFTPOWER),
#endif
#ifdef _SC_SPLIT_CACHE
    SKY_POSIX_CONST(_SC_SPLIT_CACHE),
#endif
#ifdef _SC_SSIZE_MAX
    SKY_POSIX_CONST(_SC_SSIZE_MAX),
#endif
#ifdef _SC_STACK_PROT
    SKY_POSIX_CONST(_SC_STACK_PROT),
#endif
#ifdef _SC_STREAM_MAX
    SKY_POSIX_CONST(_SC_STREAM_MAX),
#endif
#ifdef _SC_SYNCHRONIZED_IO
    SKY_POSIX_CONST(_SC_SYNCHRONIZED_IO),
#endif
#ifdef _SC_THREADS
    SKY_POSIX_CONST(_SC_THREADS),
#endif
#ifdef _SC_THREAD_ATTR_STACKADDR
    SKY_POSIX_CONST(_SC_THREAD_ATTR_STACKADDR),
#endif
#ifdef _SC_THREAD_ATTR_STACKSIZE
    SKY_POSIX_CONST(_SC_THREAD_ATTR_STACKSIZE),
#endif
#ifdef _SC_THREAD_DESTRUCTOR_ITERATIONS
    SKY_POSIX_CONST(_SC_THREAD_DESTRUCTOR_ITERATIONS),
#endif
#ifdef _SC_THREAD_KEYS_MAX
    SKY_POSIX_CONST(_SC_THREAD_KEYS_MAX),
#endif
#ifdef _SC_THREAD_PRIORITY_SCHEDULING
    SKY_POSIX_CONST(_SC_THREAD_PRIORITY_SCHEDULING),
#endif
#ifdef _SC_PRIO_INHERIT
    SKY_POSIX_CONST(_SC_PRIO_INHERIT),
#endif
#ifdef _SC_THREAD_PRIO_PROTECT
    SKY_POSIX_CONST(_SC_THREAD_PRIO_PROTECT),
#endif
#ifdef _SC_THREAD_PROCESS_SHARED
    SKY_POSIX_CONST(_SC_THREAD_PROCESS_SHARED),
#endif
#ifdef _SC_THREAD_SAFE_FUNCTIONS
    SKY_POSIX_CONST(_SC_THREAD_SAFE_FUNCTIONS),
#endif
#ifdef _SC_THREAD_STACK_MIN
    SKY_POSIX_CONST(_SC_THREAD_STACK_MIN),
#endif
#ifdef _SC_THREAD_THREADS_MAX
    SKY_POSIX_CONST(_SC_THREAD_THREADS_MAX),
#endif
#ifdef _SC_TIMERS
    SKY_POSIX_CONST(_SC_TIMERS),
#endif
#ifdef _SC_TIMER_MAX
    SKY_POSIX_CONST(_SC_TIMER_MAX),
#endif
#ifdef _SC_TTY_NAME_MAX
    SKY_POSIX_CONST(_SC_TTY_NAME_MAX),
#endif
#ifdef _SC_TZNAME_MAX
    SKY_POSIX_CONST(_SC_TZNAME_MAX),
#endif
#ifdef _SC_T_IOV_MAX
    SKY_POSIX_CONST(_SC_T_IOV_MAX),
#endif
#ifdef _SC_UCHAR_MAX
    SKY_POSIX_CONST(_SC_UCHAR_MAX),
#endif
#ifdef _SC_UINT_MAX
    SKY_POSIX_CONST(_SC_UINT_MAX),
#endif
#ifdef _SC_UIO_MAXIOV
    SKY_POSIX_CONST(_SC_UIO_MAXIOV),
#endif
#ifdef _SC_ULONG_MAX
    SKY_POSIX_CONST(_SC_ULONG_MAX),
#endif
#ifdef _SC_USHRT_MAX
    SKY_POSIX_CONST(_SC_USHRT_MAX),
#endif
#ifdef _SC_VERSION
    SKY_POSIX_CONST(_SC_VERSION),
#endif
#ifdef _SC_WORD_BIT
    SKY_POSIX_CONST(_SC_WORD_BIT),
#endif
#ifdef _SC_XBS5_ILP32_OFF32
    SKY_POSIX_CONST(_SC_XBS5_ILP32_OFF32),
#endif
#ifdef _SC_XBS5_ILP32_OFFBIG
    SKY_POSIX_CONST(_SC_XBS5_ILP32_OFFBIG),
#endif
#ifdef _SC_XBS5_LP64_OFF64
    SKY_POSIX_CONST(_SC_XBS5_LP64_OFF64),
#endif
#ifdef _SC_XBS5_LPBIG_OFFBIG
    SKY_POSIX_CONST(_SC_XBS5_LPBIG_OFFBIG),
#endif
#ifdef _SC_XOPEN_CRYPT
    SKY_POSIX_CONST(_SC_XOPEN_CRYPT),
#endif
#ifdef _SC_XOPEN_ENH_I18N
    SKY_POSIX_CONST(_SC_XOPEN_ENH_I18N),
#endif
#ifdef _SC_XOPEN_LEGACY
    SKY_POSIX_CONST(_SC_XOPEN_LEGACY),
#endif
#ifdef _SC_XOPEN_REALTIME
    SKY_POSIX_CONST(_SC_XOPEN_REALTIME),
#endif
#ifdef _SC_XOPEN_REALTIME_THREADS
    SKY_POSIX_CONST(_SC_XOPEN_REALTIME_THREADS),
#endif
#ifdef _SC_XOPEN_SHM
    SKY_POSIX_CONST(_SC_XOPEN_SHM),
#endif
#ifdef _SC_XOPEN_UNIX
    SKY_POSIX_CONST(_SC_XOPEN_UNIX),
#endif
#ifdef _SC_XOPEN_VERSION
    SKY_POSIX_CONST(_SC_XOPEN_VERSION),
#endif
#ifdef _SC_XOPEN_XCU_VERSION
    SKY_POSIX_CONST(_SC_XOPEN_XCU_VERSION),
#endif
#ifdef _SC_XOPEN_XPG2
    SKY_POSIX_CONST(_SC_XOPEN_XPG2),
#endif
#ifdef _SC_XOPEN_XPG3
    SKY_POSIX_CONST(_SC_XOPEN_XPG3),
#endif
#ifdef _SC_XOPEN_XPG4
    SKY_POSIX_CONST(_SC_XOPEN_XPG4),
#endif
    { SKY_HASHTABLE_ITEM_INITIALIZER, NULL, 0 }
};


static const char *sky_posix_have_functions[] = {
#if defined(HAVE_FACCESSAT)
    "HAVE_FACCESSAT",
#endif
#if defined(HAVE_FCHDIR)
    "HAVE_FCHDIR",
#endif
#if defined(HAVE_FCHMOD)
    "HAVE_FCHMOD",
#endif
#if defined(HAVE_FCHMODAT)
    "HAVE_FCHMODAT",
#endif
#if defined(HAVE_FCHOWN)
    "HAVE_FCHOWN",
#endif
#if defined(HAVE_FDOPENDIR)
    "HAVE_FDOPENDIR",
#endif
#if defined(HAVE_FCHOWNAT)
    "HAVE_FCHOWNAT",
#endif
#if defined(HAVE_FEXECVE)
    "HAVE_FEXECVE",
#endif
#if defined(HAVE_FPATHCONF)
    "HAVE_FPATHCONF",
#endif
#if defined(HAVE_FSTATAT)
    "HAVE_FSTATAT",
#endif
#if defined(HAVE_FSTATVFS)
    "HAVE_FSTATVFS",
#endif
#if defined(HAVE_FTRUNCATE)
    "HAVE_FTRUNCATE",
#endif
#if defined(HAVE_FUTIMENS)
    "HAVE_FUTIMENS",
#endif
#if defined(HAVE_FUTIMES)
    "HAVE_FUTIMES",
#endif
#if defined(HAVE_FUTIMESAT)
    "HAVE_FUTIMESAT",
#endif
#if defined(HAVE_LINKAT)
    "HAVE_LINKAT",
#endif
#if defined(HAVE_LCHFLAGS)
    "HAVE_LCHFLAGS",
#endif
#if defined(HAVE_LCHMOD)
    "HAVE_LCHMOD",
#endif
#if defined(HAVE_LCHOWN)
    "HAVE_LCHOWN",
#endif
#if defined(HAVE_LSTAT)
    "HAVE_LSTAT",
#endif
#if defined(HAVE_LUTIMES)
    "HAVE_LUTIMES",
#endif
#if defined(HAVE_MKDIRAT)
    "HAVE_MKDIRAT",
#endif
#if defined(HAVE_MKFIFOAT)
    "HAVE_MKFIFOAT",
#endif
#if defined(HAVE_MKNODAT)
    "HAVE_MKNODAT",
#endif
#if defined(HAVE_OPENAT)
    "HAVE_OPENAT",
#endif
#if defined(HAVE_READLINKAT)
    "HAVE_READLINKAT",
#endif
#if defined(HAVE_RENAMEAT)
    "HAVE_RENAMEAT",
#endif
#if defined(HAVE_SYMLINKAT)
    "HAVE_SYMLINKAT",
#endif
#if defined(HAVE_UNLINKAT)
    "HAVE_UNLINKAT",
#endif
#if defined(HAVE_UTIMENSAT)
    "HAVE_UTIMENSAT",
#endif
#if defined(_WIN32)
    "MS_WINDOWS",
#endif
    NULL,
};


static char **
sky_posix_argv(sky_object_t args)
{
    char            **argv;
    ssize_t         argc, i, nbytes;
    sky_object_t    arg;

    argc = sky_object_len(args);
    argv = sky_asset_malloc(sizeof(char *) * (argc + 1),
                            SKY_ASSET_CLEANUP_ALWAYS);
    argv[argc] = NULL;

    i = 0;
    SKY_SEQUENCE_FOREACH(args, arg) {
        arg = sky_string_createfromobject(arg);

        argv[i] = (char *)
                  sky_codec_encodetocbytes(sky_codec_filesystemencoding_codec,
                                           arg,
                                           sky_codec_filesystemencoding_string,
                                           sky_codec_filesystemerrors_string,
                                           0,
                                           &nbytes);
        argv[i] = sky_realloc(argv[i], nbytes + 1);
        argv[i][nbytes] = '\0';

        sky_asset_save(argv[i], sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
        ++i;
    } SKY_SEQUENCE_FOREACH_END;

    for (i = 0; i < argc; ++i) {
        sky_asset_save(argv[i], sky_free, SKY_ASSET_CLEANUP_ALWAYS);
    }

    return argv;
}


static char **
sky_posix_envp(sky_object_t dict)
{
    char            **envp;
    ssize_t         envc, i, nbytes;
    sky_object_t    key, keys, value;
    sky_string_t    string;

    envc = sky_object_len(dict);
    envp = sky_asset_malloc(sizeof(char *) * (envc + 1),
                            SKY_ASSET_CLEANUP_ALWAYS);
    envp[envc] = NULL;

    i = 0;
    keys = sky_object_callmethod(dict, SKY_STRING_LITERAL("keys"), NULL, NULL);
    SKY_SEQUENCE_FOREACH(keys, key) {
        value = sky_object_getitem(dict, key);
        string = sky_string_createfromformat("%@=%@", key, value);
        envp[i] = (char *)
                  sky_codec_encodetocbytes(sky_codec_filesystemencoding_codec,
                                           string,
                                           sky_codec_filesystemencoding_string,
                                           sky_codec_filesystemerrors_string,
                                           0,
                                           &nbytes);
        envp[i] = sky_realloc(envp[i], nbytes + 1);
        envp[i][nbytes] = '\0';

        sky_asset_save(envp[i], sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
        ++i;
    } SKY_SEQUENCE_FOREACH_END;

    for (i = 0; i < envc; ++i) {
        sky_asset_save(envp[i], sky_free, SKY_ASSET_CLEANUP_ALWAYS);
    }

    return envp;
}


#if defined(HAVE_READV) || defined(HAVE_WRITEV)
static ssize_t
sky_posix_iov(struct iovec **iov, sky_object_t buffers, unsigned int flags)
{
    ssize_t         i, len, result;
    sky_buffer_t    buffer;
    sky_object_t    object;

    len = sky_object_len(buffers);
    *iov = sky_asset_malloc(sizeof(struct iovec) * len,
                            SKY_ASSET_CLEANUP_ALWAYS);

    i = 0;
    result = 0;
    SKY_SEQUENCE_FOREACH(buffers, object) {
        sky_buffer_acquire(&buffer, object, flags);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (SSIZE_MAX - result < buffer.len) {
            sky_error_raise_string(sky_OverflowError, "buffer too big");
        }
        (*iov)[i].iov_base = buffer.buf;
        (*iov)[i].iov_len = buffer.len;
        result += buffer.len;
        ++i;
    } SKY_SEQUENCE_FOREACH_END;

    return result;
}
#endif


static int
sky_posix_constant_lookup(sky_hashtable_t *table, sky_object_t name)
{
    uintptr_t               hash;
    const char              *cname;
    sky_posix_constant_t    *constant;

    if (sky_object_isa(name, sky_integer_type)) {
        return sky_integer_value(name, LONG_MIN, LONG_MAX, NULL);
    }
    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "configuration names must be strings or integers");
    }

    if ((cname = sky_string_cstring(name)) != NULL) {
        hash = sky_util_hashbytes(cname, strlen(cname), 0);
        if ((constant = (sky_posix_constant_t *)
                        sky_hashtable_lookup(table,
                                             hash,
                                             (void *)cname)) != NULL)
        {
            return constant->value;
        }
    }

    sky_error_raise_string(sky_ValueError, "unrecognized configuration name");
}


#if defined(AT_FDCWD)
    /* The (int) cast is for Solaris, which defines AT_FDCWD as 0xffd19553.
     * It's needed to keep the compiler quiet about assignments to int.
     */
#   define SKY_POSIX_DIR_FD_DEFAULT     (int)AT_FDCWD
#else
#   define SKY_POSIX_DIR_FD_DEFAULT     (-100)
#endif

static inline int
sky_posix_dir_fd_convert(sky_object_t dir_fd)
{
    if (sky_object_isnull(dir_fd)) {
        return SKY_POSIX_DIR_FD_DEFAULT;
    }
    return sky_integer_value(dir_fd, INT_MIN, INT_MAX, NULL);
}


static inline void
sky_posix_dir_fd_unavailable(sky_object_t dir_fd)
{
    if (!sky_object_isnull(dir_fd)) {
        sky_error_raise_string(sky_NotImplementedError,
                               "dir_fd unavailable on this platform");
    }
}


static inline sky_string_t
sky_posix_fsname(const char *fsname, ssize_t fsname_len)
{
    if (fsname_len < 0) {
        fsname_len = strlen(fsname);
    }
    return sky_string_createfromfilename(fsname, fsname_len);
}


static const char *
sky_posix_fspath(sky_object_t path)
{
    sky_buffer_t    buffer;
    const uint8_t   *bytes;

    if (sky_object_isa(path, sky_bytes_type) ||
        sky_object_isa(path, sky_bytearray_type))
    {
        sky_buffer_acquire(&buffer, path, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        return buffer.buf;
    }

    path = sky_codec_encodefilename(path);
    bytes = sky_bytes_cstring(path, SKY_TRUE);

    return (const char *)bytes;
}


typedef struct sky_posix_path_s {
    const char *                        bytes;
    ssize_t                             nbytes;
    int                                 fd;
} sky_posix_path_t;

static void
sky_posix_path_convert(
                sky_posix_path_t *  path,
                sky_object_t        object,
                sky_bool_t          allow_fd)
{
    sky_buffer_t    buffer;

    memset(path, 0, sizeof(sky_posix_path_t));
    path->fd = -1;

    if (sky_object_isa(object, sky_string_type)) {
        object = sky_codec_encodefilename(object);
    }
    if (sky_buffer_check(object)) {
        sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        path->bytes = sky_asset_strndup(buffer.buf, buffer.len,
                                        SKY_ASSET_CLEANUP_ALWAYS);
        path->nbytes = buffer.len;
        sky_buffer_release(&buffer);
    }
    else if (allow_fd) {
        if (sky_object_isa(object, sky_integer_type)) {
            path->fd = sky_integer_value(object, INT_MIN, INT_MAX, NULL);
        }
        else {
            sky_error_raise_format(
                    sky_TypeError,
                    "expected str, bytes, or int; got %#@",
                    sky_type_name(sky_object_type(object)));
        }
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "expected str or bytes; got %#@",
                               sky_type_name(sky_object_type(object)));
    }
}


static inline void
sky_posix_validate_path_and_dir_fd(
                const char *        function_name,
                sky_posix_path_t *  path,
                int                 dir_fd)
{
    if (!path->bytes && SKY_POSIX_DIR_FD_DEFAULT != dir_fd) {
        sky_error_raise_format(
                sky_ValueError,
                "%s: can't specify dir_fd without matching path",
                function_name);
    }
}


static inline void
sky_posix_validate_dir_fd_and_fd(
                const char *function_name,
                int         dir_fd,
                int         fd)
{
    if (SKY_POSIX_DIR_FD_DEFAULT != dir_fd && -1 != fd) {
        sky_error_raise_format(
                sky_ValueError,
                "%s: can't specify both dir_fd and fd",
                function_name);
    }
}


static inline void
sky_posix_validate_dir_fd_and_follow_symlinks(
                const char *function_name,
                int         dir_fd,
                sky_bool_t  follow_symlinks)
{
    if (SKY_POSIX_DIR_FD_DEFAULT != dir_fd && !follow_symlinks) {
        sky_error_raise_format(
                sky_ValueError,
                "%s: cannot use dir_fd and follow_symlinks together",
                function_name);
    }
}


static inline void
sky_posix_validate_fd_and_follow_symlinks(
                const char *function_name,
                int         fd,
                sky_bool_t  follow_symlinks)
{
    if (-1 != fd && !follow_symlinks) {
        sky_error_raise_format(
                sky_ValueError,
                "%s: cannot use fd and follow_symlinks together",
                function_name);
    }
}


static inline void
sky_posix_validate_follow_symlinks(
                const char *function_name,
                sky_bool_t  follow_symlinks)
{
    if (!follow_symlinks) {
        sky_error_raise_format(
                sky_NotImplementedError,
                "%s: follow_symlinks unavailable on this platform",
                function_name);
    }
}


static void
sky_posix_addconstants(sky_module_t module)
{
#define set_integer(k)  sky_module_setattr(module, #k, sky_integer_create(k));

    set_integer(F_OK);
    set_integer(R_OK);
    set_integer(W_OK);
    set_integer(X_OK);
    set_integer(NGROUPS_MAX);
    set_integer(TMP_MAX);
#if defined(WCONTINUED)
    set_integer(WCONTINUED);
#endif
#if defined(WNOHANG)
    set_integer(WNOHANG);
#endif
#if defined(WUNTRACED)
    set_integer(WUNTRACED);
#endif
    set_integer(O_RDONLY);
    set_integer(O_WRONLY);
    set_integer(O_RDWR);
    set_integer(O_NDELAY);
    set_integer(O_NONBLOCK);
    set_integer(O_APPEND);
#if defined(O_DSYNC)
    set_integer(O_DSYNC);
#endif
#if defined(O_RSYNC)
    set_integer(O_RSYNC);
#endif
    set_integer(O_SYNC);
    set_integer(O_NOCTTY);
    set_integer(O_CREAT);
    set_integer(O_EXCL);
    set_integer(O_TRUNC);
#if defined(O_BINARY)
    set_integer(O_BINARY);
#endif
#if defined(O_TEXT)
    set_integer(O_TEXT);
#endif
#if defined(O_XATTR)
    set_integer(O_XATTR);
#endif
#if defined(O_LARGEFILE)
    set_integer(O_LARGEFILE);
#endif
#if defined(O_SHLOCK)
    set_integer(O_SHLOCK);
#endif
#if defined(O_EXLOCK)
    set_integer(O_EXLOCK);
#endif
#if defined(O_EXEC)
    set_integer(O_EXEC);
#endif
#if defined(O_SEARCH)
    set_integer(O_SEARCH);
#endif
#if defined(O_TTY_INIT)
    set_integer(O_TTY_INIT);
#endif
    set_integer(PRIO_PROCESS);
    set_integer(PRIO_PGRP);
    set_integer(PRIO_USER);
#if defined(O_CLOEXEC)
    set_integer(O_CLOEXEC);
#endif
    set_integer(O_ACCMODE);
#if defined(SEEK_HOLE)
    set_integer(SEEK_HOLE);
#endif
#if defined(SEEK_DATA)
    set_integer(SEEK_DATA);
#endif
#if defined(O_NOINHERIT)
    set_integer(O_NOINHERIT);
#endif
#if defined(_O_SHORT_LIVED)
    sky_module_setattr(module, "O_SHORT_LIVED",
                               sky_integer_create(_O_SHORT_LIVED));
#endif
#if defined(O_TEMPORARY)
    set_integer(O_TEMPORARY);
#endif
#if defined(O_RANDOM)
    set_integer(O_RANDOM);
#endif
#if defined(O_SEQUENTIAL)
    set_integer(O_SEQUENTIAL);
#endif
#if defined(O_ASYNC)
    set_integer(O_ASYNC);
#endif
#if defined(O_DIRECT)
    set_integer(O_DIRECT);
#endif
#if defined(O_DIRECTORY)
    set_integer(O_DIRECTORY);
#endif
#if defined(O_NOFOLLOW)
    set_integer(O_NOFOLLOW);
#endif
#if defined(O_NOLINKS)
    set_integer(O_NOLINKS);
#endif
#if defined(O_NOATIME)
    set_integer(O_NOATIME);
#endif

#if defined(HAVE_SYSEXITS_H)
    set_integer(EX_OK);
    set_integer(EX_USAGE);
    set_integer(EX_DATAERR);
    set_integer(EX_NOINPUT);
    set_integer(EX_NOUSER);
    set_integer(EX_NOHOST);
    set_integer(EX_UNAVAILABLE);
    set_integer(EX_SOFTWARE);
    set_integer(EX_OSERR);
    set_integer(EX_OSFILE);
    set_integer(EX_CANTCREAT);
    set_integer(EX_IOERR);
    set_integer(EX_TEMPFAIL);
    set_integer(EX_PROTOCOL);
    set_integer(EX_NOPERM);
    set_integer(EX_CONFIG);
#   if defined(EX_NOTFOUND)
    set_integer(EX_NOTFOUND);
#   endif
#endif

#if defined(HAVE_SYS_STATVFS_H)
    set_integer(ST_RDONLY);
    set_integer(ST_NOSUID);
#endif

#if defined(SF_NODISKIO)
    set_integer(SF_NODISKIO);
#endif
#if defined(SF_MNOWAIT)
    set_integer(SF_MNOWAIT);
#endif
#if defined(SF_SYNC)
    set_integer(SF_SYNC);
#endif

#if defined(POSIX_FADV_NORMAL)
    set_integer(POSIX_FADV_NORMAL);
#endif
#if defined(POSIX_FADV_SEQUENTIAL)
    set_integer(POSIX_FADV_SEQUENTIAL);
#endif
#if defined(POSIX_FADV_RANDOM)
    set_integer(POSIX_FADV_RANDOM);
#endif
#if defined(POSIX_FADV_NOREUSE)
    set_integer(POSIX_FADV_NOREUSE);
#endif
#if defined(POSIX_FADV_WILLNEED)
    set_integer(POSIX_FADV_WILLNEED);
#endif
#if defined(POSIX_ADV_DONTNEED)
    set_integer(POSIX_FADV_DONTNEED);
#endif

#if defined(HAVE_SYS_WAIT_H) && defined(HAVE_WAITID)
    set_integer(P_PID);
    set_integer(P_PGID);
    set_integer(P_ALL);
#endif
#if defined(WEXITED)
    set_integer(WEXITED);
#endif
#if defined(WNOWAIT)
    set_integer(WNOWAIT);
#endif
#if defined(WSTOPPED)
    set_integer(WSTOPPED);
#endif
#if defined(CLD_EXITED)
    set_integer(CLD_EXITED);
#endif
#if defined(CLD_DUMPED)
    set_integer(CLD_DUMPED);
#endif
#if defined(CLD_TRAPPED)
    set_integer(CLD_TRAPPED);
#endif
#if defined(CLD_CONTINUED)
    set_integer(CLD_CONTINUED);
#endif

#if defined(F_LOCK)
    set_integer(F_LOCK);
#endif
#if defined(F_TLOCK)
    set_integer(F_TLOCK);
#endif
#if defined(F_ULOCK)
    set_integer(F_ULOCK);
#endif
#if defined(F_TEST)
    set_integer(F_TEST);
#endif

#if defined(HAVE_SPAWNV)
    set_integer(P_WAIT);
    set_integer(P_NOWAIT);
    set_integer(P_OVERLAY);
    set_integer(P_NOWAITO);
    set_integer(P_DETACH);
#endif

#if defined(HAVE_SCHED_H)
    set_integer(SCHED_OTHER);
    set_integer(SCHED_FIFO);
    set_integer(SCHED_RR);
#   if defined(SCHED_SPORADIC)
    set_integer(SCHED_SPORADIC);
#   endif
#   if defined(SCHED_BATCH)
    set_integer(SCHED_BATCH);
#   endif
#   if defined(SCHED_IDLE)
    set_integer(SCHED_IDLE);
#   endif
#   if defined(SCHED_RESET_ON_FORK)
    set_integer(SCHED_RESET_ON_FORK);
#   endif
#   if defined(SCHED_SYS)
    set_integer(SCHED_SYS);
#   endif
#   if defined(SCHED_IA)
    set_integer(SCHED_IA);
#   endif
#   if defined(SCHED_FSS)
    set_integer(SCHED_FSS);
#   endif
#   if defined(SCHED_FX)
    set_integer(SCHED_FX);
#   endif
#endif

#if defined(HAVE_SYS_XATTR_H)
    set_integer(XATTR_CREATE);
    set_integer(XATTR_REPLACE);
#   if defined(XATTR_SIZE_MAX)
    set_integer(XATTR_SIZE_MAX);
#   endif
#   if defined(XATTR_NOFOLLOW)
    set_integer(XATTR_NOFOLLOW);
#   endif
#   if defined(XATTR_NOSECURITY)
    set_integer(XATTR_NOSECURITY);
#   endif
#   if defined(XATTR_NODEFAULT)
    set_integer(XATTR_NODEFAULT);
#   endif
#   if defined(XATTR_SHOWCOMPRESSION)
    set_integer(XATTR_SHOWCOMPRESSION);
#   endif
#endif

#if defined(RTLD_LAZY)
    set_integer(RTLD_LAZY);
#endif
#if defined(RTLD_NOW)
    set_integer(RTLD_NOW);
#endif
#if defined(RTLD_GLOBAL)
    set_integer(RTLD_GLOBAL);
#endif
#if defined(RTLD_LOCAL)
    set_integer(RTLD_LOCAL);
#endif
#if defined(RTLD_NODELETE)
    set_integer(RTLD_NODELETE);
#endif
#if defined(RTLD_NOLOAD)
    set_integer(RTLD_NOLOAD);
#endif
#if defined(RTLD_DEEPBIND)
    set_integer(RTLD_DEEPBIND);
#endif
}


static sky_dict_t
sky_posix_environ(void)
{
#if defined(__APPLE__)
    static char **environ;
#else
    extern char **environ;
#endif

    char        **env, *p;
    sky_dict_t  dict;
    sky_bytes_t key, value;

#if defined(__APPLE__)
    if (!environ) {
        environ = *_NSGetEnviron();
    }
#endif

    dict = sky_dict_create();
    if (!environ) {
        return dict;
    }

    for (env = environ; *env; ++env) {
        if (!(p = strchr(*env, '='))) {
            continue;
        }
        key = sky_bytes_createfrombytes(*env, p - *env);
        value = sky_bytes_createfrombytes(p + 1, strlen(p + 1));
        sky_dict_setitem(dict, key, value);
    }

    return dict;
}


static sky_bool_t sky_posix_stat_float_times_flag = SKY_TRUE;

static sky_type_t sky_posix_stat_result_type = NULL;

static const char sky_posix_stat_result_doc[] =
"stat_result: Result from stat, fstat, or lstat.\n\n"
"This object may be access either as a tuple of\n"
"  (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime)\n"
"or via the attributes st_mode, st_ino, st_dev, st_nlink, st_uid, and so on.\n"
"\n"
"Posix/windows: If your platform supports st_blksize, st_blocks, st_rdev,\n"
"or st_flags, they are available as attributes only.\n"
"\n"
"See os.stat for more information.";

static sky_struct_sequence_field_t sky_posix_stat_result_fields[] = {
    { "st_mode",        "protection bits",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_ino",         "inode",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_dev",         "device",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_nlink",       "number of hard links",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_uid",         "user ID of owner",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_gid",         "group ID of owner",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { "st_size",        "total size, in bytes",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE },
    { NULL,             "integer time of last access",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_SEQUENCE_ONLY },
    { NULL,             "integer time of last modification",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_SEQUENCE_ONLY },
    { NULL,             "integer time of last change",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_SEQUENCE_ONLY },
    { "st_atime",       "time of last access",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "st_mtime",       "time of last modification",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "st_ctime",       "time of last change",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "st_atime_ns",    "time of last access in nanoseconds",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "st_mtime_ns",    "time of last modification in nanoseconds",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "st_ctime_ns",    "time of last change in nanoseconds",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#if defined(HAVE_STRUCT_STAT_ST_BLKSIZE)
    { "st_blksize",     "blocksize for filesystem I/O",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
#if defined(HAVE_STRUCT_STAT_ST_BLOCKS)
    { "st_blocks",      "number of blocks allocated",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
#if defined(HAVE_STRUCT_STAT_ST_RDEV)
    { "st_rdev",        "device type (if inode device)",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
#if defined(HAVE_STRUCT_STAT_ST_FLAGS)
    { "st_flags",       "user defined flags for file",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
#if defined(HAVE_STRUCT_STAT_ST_GEN)
    { "st_gen",         "generation number",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
#if defined(HAVE_STRUCT_STAT_ST_BIRTHTIME)
    { "st_birthtime",   "time of creation",
                        SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
#endif
};
static const size_t sky_posix_stat_result_nfields =
        sizeof(sky_posix_stat_result_fields) /
        sizeof(sky_posix_stat_result_fields[0]);


static void
sky_posix_stat_time(
                sky_object_t *  values,
                size_t          index,
                long            sec,
                unsigned long   nsec)
{
    sky_integer_t   i;

    values[index] = sky_integer_create(sec);

    if (sky_posix_stat_float_times_flag) {
        values[index + 3] = sky_float_create(sec + (nsec * 1e-9));
    }
    else {
        values[index + 3] = sky_float_create(sec);
    }

    i = values[index];
    i = sky_number_multiply(i, sky_integer_create(1000000000));
    values[index + 6] = sky_number_add(i, sky_integer_createfromunsigned(nsec));
}

static sky_object_t
sky_posix_stat_result(struct stat *st)
{
    size_t          i = 15;
    sky_object_t    values[sizeof(sky_posix_stat_result_fields) /
                           sizeof(sky_posix_stat_result_fields[0])];

    values[0] = sky_integer_create(st->st_mode);
    values[1] = sky_integer_create(st->st_ino);
    values[2] = sky_integer_create(st->st_dev);
    values[3] = sky_integer_create(st->st_nlink);
    values[4] = sky_integer_create(st->st_uid);
    values[5] = sky_integer_create(st->st_gid);
    values[6] = sky_integer_create(st->st_size);
#if defined(HAVE_STRUCT_STAT_TIM_TV_NSEC)
    sky_posix_stat_time(values, 7, st->st_atime, st->st_atim.tv_nsec);
    sky_posix_stat_time(values, 8, st->st_mtime, st->st_mtim.tv_nsec);
    sky_posix_stat_time(values, 9, st->st_ctime, st->st_ctim.tv_nsec);
#elif defined(HAVE_STRUCT_STAT_TIMESPEC_TV_NSEC)
    sky_posix_stat_time(values, 7, st->st_atime, st->st_atimespec.tv_nsec);
    sky_posix_stat_time(values, 8, st->st_mtime, st->st_mtimespec.tv_nsec);
    sky_posix_stat_time(values, 9, st->st_ctime, st->st_ctimespec.tv_nsec);
#elif defined(HAVE_STRUCT_STAT_NSEC)
    sky_posix_stat_time(values, 7, st->st_atime, st->st_atime_nsec);
    sky_posix_stat_time(values, 8, st->st_mtime, st->st_mtime_nsec);
    sky_posix_stat_time(values, 9, st->st_ctime, st->st_ctime_nsec);
#else
    sky_posix_stat_time(values, 7, st->st_atime, 0);
    sky_posix_stat_time(values, 8, st->st_mtime, 0);
    sky_posix_stat_time(values, 9, st->st_ctime, 0);
#endif
#if defined(HAVE_STRUCT_STAT_ST_BLKSIZE)
    values[++i] = sky_integer_create(st->st_blksize);
#endif
#if defined(HAVE_STRUCT_STAT_ST_BLOCKS)
    values[++i] = sky_integer_create(st->st_blocks);
#endif
#if defined(HAVE_STRUCT_STAT_ST_RDEV)
    values[++i] = sky_integer_create(st->st_rdev);
#endif
#if defined(HAVE_STRUCT_STAT_ST_FLAGS)
    values[++i] = sky_integer_create(st->st_flags);
#endif
#if defined(HAVE_STRUCT_STAT_ST_GEN)
    values[++i] = sky_integer_create(st->st_gen);
#endif
#if defined(HAVE_STRUCT_STAT_ST_BIRTHTIME)
    if (sky_posix_stat_float_times_flag) {
#   if defined(HAVE_STRUCT_STAT_TIMESPEC_TV_NSEC)
        values[++i] = sky_float_create(st->st_birthtime +
                                       st->st_birthtimespec.tv_nsec * 1e-9);
#   else
        values[++i] = sky_float_create(st->st_birthtime);
#   endif
    }
    else {
        values[++i] = sky_integer_create(st->st_birthtime);
    }
#endif

    return sky_struct_sequence_create(sky_posix_stat_result_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}


static sky_object_t
sky_posix_stat_common(
                const char *        function_name,
                sky_posix_path_t *  path,
                int                 dir_fd,
                sky_bool_t          follow_symlinks)
{
    int         x;
    struct stat st;

    sky_posix_validate_path_and_dir_fd(function_name, path, dir_fd);
    sky_posix_validate_dir_fd_and_fd(function_name, dir_fd, path->fd);
    sky_posix_validate_fd_and_follow_symlinks(function_name,
                                              path->fd,
                                              follow_symlinks);

    if (-1 != path->fd) {
        x = fstat(path->fd, &st);
    }
    else if (!follow_symlinks && SKY_POSIX_DIR_FD_DEFAULT == dir_fd) {
        x = lstat(path->bytes, &st);
    }
#if defined(HAVE_FSTATAT)
    else if (SKY_POSIX_DIR_FD_DEFAULT != dir_fd && !follow_symlinks) {
        x = fstatat(dir_fd,
                    path->bytes,
                    &st,
                    (follow_symlinks ? 0 : AT_SYMLINK_NOFOLLOW));
    }
#endif
    else {
        x = stat(path->bytes, &st);
    }

    if (-1 == x) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return sky_posix_stat_result(&st);
}




static int
sky_posix_access(
                sky_object_t    path,
                int             mode,
                sky_object_t    dir_fd,
                sky_bool_t      effective_ids,
                sky_bool_t      follow_symlinks)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_FACCESSAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("access", &cpath, fd);
#if !defined(HAVE_FCHMODAT)
        sky_posix_validate_follow_symlinks("access", follow_symlinks);
        if (effective_ids) {
            sky_error_raise_string(
                    sky_NotImplementedError,
                    "access: effective_ids unavailable on this platform");
        }
#endif

        if (follow_symlinks &&
            !effective_ids &&
            SKY_POSIX_DIR_FD_DEFAULT == fd)
        {
            result = access(cpath.bytes, mode);
        }
        else {
#if defined(HAVE_FACCESSAT)
            int flags = 0;

            if (!follow_symlinks) {
                flags |= AT_SYMLINK_NOFOLLOW;
            }
            if (effective_ids) {
                flags |= AT_EACCESS;
            }
            result = faccessat(fd, cpath.bytes, mode, flags);
#else
            result = 1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_access_doc[] =
"access(path, mode, *, dir_fd=None, effective_ids=False,"
" follow_symlinks=True)\n\n"
"Use the real uid/gid to test for access to a path.  Returns True if granted,\n"
"False otherwise.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"If effective_ids is True, access will use the effective uid/gid instead of\n"
"  the real uid/gid.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, access will examine the symbolic link itself instead of the file the\n"
"  link points to.\n"
"dir_fd, effective_ids, and follow_symlinks may not be implemented\n"
"  on your platform.  If they are unavailable, using them will raise a\n"
"  NotImplementedError.\n"
"\n"
"Note that most operations will use the effective uid/gid, therefore this\n"
"  routine can be used in a suid/sgid environment to test if the invoking user\n"
"  has the specified access to the path.\n"
"The mode argument can be F_OK to test existence, or the inclusive-OR\n"
"  of R_OK, W_OK, and X_OK.";

static int
sky_posix_chdir(sky_object_t path)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        if (cpath.fd != -1) {
            result = fchdir(cpath.fd);
        }
        else {
            result = chdir(cpath.bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_chdir_doc[] =
"chdir(path)\n\n"
"Change the current working directory to the specified path.\n"
"\n"
"path may always be specified as a string.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailable, using it raises an exception.";


#if defined(HAVE_CHFLAGS)
static int
sky_posix_chflags(
                sky_object_t    path,
                unsigned int    flags,
                sky_bool_t      follow_symlinks)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if !defined(HAVE_LCHFLAGS)
        sky_posix_validate_follow_symlinks("chflags", follow_symlinks);
#else
        if (!follow_symlinks) {
            result = lchflags(cpath.bytes, flags);
        }
        else
#endif
        {
            result = chflags(cpath.bytes, flags);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_chflags_doc[] =
"chflags(path, flags, *, follow_symlinks=True)\n\n"
"Set file flags.\n\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, chflags will change flags on the symbolic link itself instead of the\n"
"  file the link points to.\n"
"follow_symlinks may not be implemented on your platform.  If it is\n"
"unavailable, using it will raise a NotImplementedError.";
#endif


static int
sky_posix_chmod(
                sky_object_t    path,
                mode_t          mode,
                sky_object_t    dir_fd,
                sky_bool_t      follow_symlinks)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#if defined(HAVE_FCHMODAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("chmod", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("chmod", fd, cpath.fd);
#if !defined(HAVE_FCHMODAT) && !defined(HAVE_LCHMOD)
        sky_posix_validate_follow_symlinks("chmod", follow_symlinks);
#endif

        if (cpath.fd != -1) {
            result = fchmod(cpath.fd, mode);
        }
#if defined(HAVE_LCHMOD)
        else if (!follow_symlinks && SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = lchmod(cpath.bytes, mode);
        }
#endif
#if defined(HAVE_FCHMODAT)
        else if (!follow_symlinks || SKY_POSIX_DIR_FD_DEFAULT != fd) {
            result = fchmodat(fd,
                              cpath.bytes,
                              mode,
                              (follow_symlinks ? 0 : AT_SYMLINK_NOFOLLOW));
            if (-1 == result &&
                (ENOTSUP == errno || EOPNOTSUPP == errno) &&
                !follow_symlinks)
            {
                sky_posix_validate_dir_fd_and_follow_symlinks("chmod", fd, follow_symlinks);
                sky_posix_validate_follow_symlinks("chmod", follow_symlinks);
            }
        }
#endif
        else {
            result = chmod(cpath.bytes, mode);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_chmod_doc[] =
"chmod(path, mode, *, dir_fd=None, follow_symlinks=True)\n\n"
"Change the access permissions of a file.\n\n"
"path may always be specified as a string.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavaiable, using it raises an exception.\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, chmod will modify the symbolic link itself instead of the file the\n"
"  link points to.\n"
"It is an error to use dir_fd or follow_symlinks when specifying path as\n"
"  an open file descriptor.\n"
"dir_fd and follow_symlinks may not be implemented on your platform.\n"
"  If they are unavailable, using them will raise a NotImplementedError.";


static int
sky_posix_chown(
                sky_object_t    path,
                uid_t           uid,
                gid_t           gid,
                sky_object_t    dir_fd,
                sky_bool_t      follow_symlinks)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#if defined(HAVE_FCHOWNAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("chown", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("chown", fd, cpath.fd);
#if !defined(HAVE_FCHOWNAT) && !defined(HAVE_LCHOWN)
        sky_posix_validate_follow_symlinks("chown", follow_symlinks);
#endif

        if (cpath.fd != -1) {
            result = fchown(cpath.fd, uid, gid);
        }
#if defined(HAVE_LCHOWN)
        else if (!follow_symlinks && SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = lchown(cpath.bytes, uid, gid);
        }
#endif
#if defined(HAVE_FCHOWNAT)
        else if (!follow_symlinks || SKY_POSIX_DIR_FD_DEFAULT != fd) {
            result = fchownat(fd,
                              cpath.bytes,
                              uid,
                              gid,
                              (follow_symlinks ? 0 : AT_SYMLINK_NOFOLLOW));
            if (-1 == result &&
                (ENOTSUP == errno || EOPNOTSUPP == errno) &&
                !follow_symlinks)
            {
                sky_posix_validate_dir_fd_and_follow_symlinks("chown", fd, follow_symlinks);
                sky_posix_validate_follow_symlinks("chown", follow_symlinks);
            }
        }
#endif
        else {
            result = chown(cpath.bytes, uid, gid);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_chown_doc[] =
"chown(path, uid, gid, *, dir_fd=None, follow_symlinks=True)\n\n"
"Change the owner and group id of path to the numeric uid and gid.\n\n"
"path may always be specified as a string.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailabe, using it raises an exception.\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, chown will modify the symbolic link itself instead of the file the\n"
"  link points to.\n"
"It is an error to use dir_fd or follow_symlinks when specifying path as\n"
"  an open file descriptor.\n"
"dir_fd and follow_symlinks may not be implemented on your platform.\n"
"  If they are unavailable, using them will raise a NotImplementedError.";


static int
sky_posix_chroot(sky_object_t path)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
        result = chroot(path);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_chroot_doc[] =
"chroot(path)\n\n"
"Change root directory to path.";


static void
sky_posix_closerange(int fd_low, int fd_high)
{
    int fd;

    for (fd = fd_low; fd < fd_high; ++fd) {
        close(fd);
    }
}

static const char sky_posix_closerange_doc[] =
"closerange(fd_low, fd_high)\n\n"
"Closes all file descriptors in [fd_low, fd_high), ignoring errors.";


static sky_string_t
sky_posix_confstr(sky_object_t name)
{
    int             cname;
    char            *buffer, small_buffer[256];
    size_t          len;
    sky_object_t    result;

    cname = sky_posix_constant_lookup(&sky_posix_confstr_table, name);

    errno = 0;
    if (!(len = confstr(cname, small_buffer, sizeof(small_buffer)))) {
        if (errno) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        return NULL;
    }

    if (len <= sizeof(small_buffer)) {
        return sky_posix_fsname(small_buffer, len - 1);
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(len, SKY_ASSET_CLEANUP_ALWAYS);
        confstr(cname, buffer, len);
        result = sky_posix_fsname(buffer, len - 1);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_confstr_doc[] =
"confstr(name) -> string\n\n"
"Return a string-valued system configuration variable.";


static sky_string_t
sky_posix_ctermid(void)
{
    char    buffer[L_ctermid], *id;

    if (!(id = ctermid(buffer))) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_fsname(id, -1);
}

static const char sky_posix_ctermid_doc[] =
"ctermid() -> string\n\n"
"Return the name of the controlling terminal for this process.";


static void
sky_posix_execv(sky_object_t path, sky_object_t args)
{
    char        **argv;
    const char  *cpath;

    SKY_ASSET_BLOCK_BEGIN {
        cpath = sky_posix_fspath(path);

        if (!sky_object_isa(args, sky_list_type) &&
            !sky_object_isa(args, sky_tuple_type))
        {
            sky_error_raise_string(sky_TypeError,
                                   "execv() arg 2 must be a tuple or list");
        }
        if (!sky_object_len(args)) {
            sky_error_raise_string(sky_ValueError,
                                   "execv() arg 2 must not be empty");
        }

        argv = sky_posix_argv(args);

        execv(cpath, argv);

        /* execv() will not return unless an error occurs. */
        sky_error_raise_errno(sky_OSError, errno);
    } SKY_ASSET_BLOCK_END;
}

static const char sky_posix_execv_doc[] =
"execv(path, args)\n\n"
"Execute an executable path with arguments, replacing current process.\n\n"
"    path: path of executable file\n"
"    args: tuple or list of strings\n";


static void
sky_posix_execve(sky_object_t path, sky_object_t args, sky_object_t env)
{
    char                **argv, **envp;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
#if defined(HAVE_FEXECVE)
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#else
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#endif

        if (!sky_object_isa(args, sky_list_type) &&
            !sky_object_isa(args, sky_tuple_type))
        {
            sky_error_raise_string(sky_TypeError,
                                   "execve: argv must be a tuple or list");
        }
        if (!sky_object_hasattr(env, SKY_STRING_LITERAL("keys"))) {
            sky_error_raise_string(
                    sky_TypeError,
                    "execve: environment must be a mapping object");
        }

        argv = sky_posix_argv(args);
        envp = sky_posix_envp(env);

        if (-1 != cpath.fd) {
#if defined(HAVE_FEXECVE)
            fexecve(cpath.fd, argv, envp);
#else
            errno = ENOSYS;
#endif
        }
        else {
            execve(cpath.bytes, argv, envp);
        }

        /* execve() will not return unless an error occurs. */
        sky_error_raise_errno(sky_OSError, errno);
    } SKY_ASSET_BLOCK_END;
}

static const char sky_posix_execve_doc[] =
"execve(path, args, env)\n\n"
"Execute a path with arguments and environment, replacing current process.\n\n"
"    path: path of executable file\n"
"    args: tuple or list of arguments\n"
"    env: dictionary of strings mapping to strings\n\n"
"On some platforms, you may specify an open file descriptor for path;\n"
"  execve will execute the progarm the file descriptor is open to.\n"
"  If this functionality is unavailable, using it raises NotImplementedError.";


static sky_tuple_t
sky_posix_forkpty(void)
{
    int     master_fd;
    pid_t   pid;

    if ((pid = sky_system_forkpty(&master_fd, NULL, NULL, NULL)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_tuple_pack(2, sky_integer_create(pid),
                             sky_integer_create(master_fd));
}

static const char sky_posix_forkpty_doc[] =
"forkpty() -> (pid, master_fd)\n\n"
"Fork a new process with a new pseudo-terminal as controlling tty.\n\n"
"Like fork(), return 0 as pid to child process, and PID of child to parent.\n"
"To both, return fd of newly opened pseudo-termianl.\n";


static long
sky_posix_fpathconf(int fd, sky_object_t name)
{
    int     cname;
    long    result;

    cname = sky_posix_constant_lookup(&sky_posix_pathconf_table, name);

    errno = 0;
    result = fpathconf(fd, cname);
    if (-1 == result && errno) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return result;
}

static const char sky_posix_fpathconf_doc[] =
"fpathconf(fd, name) -> integer\n\n"
"Return the configuration limit name for the file descriptor fd.\n"
"If there is no limit, return -1.";


static sky_object_t
sky_posix_fstat(int fd)
{
    sky_posix_path_t    cpath;

    cpath.fd = fd;
    return sky_posix_stat_common("fstat",
                                 &cpath,
                                 SKY_POSIX_DIR_FD_DEFAULT,
                                 SKY_TRUE);
}

static const char sky_posix_fstat_doc[] =
"fstat(fd) -> stat result\n\n"
"Like stat(), but for an open file descriptor.\n"
"Equivalent to stat(fd=fd).";


#if defined(HAVE_FSTATVFS) || defined(HAVE_STATVFS)
static sky_type_t sky_posix_statvfs_result_type;

static const char sky_posix_statvfs_result_doc[] =
"statvfs_result: Result from statvfs or fstatvfs.\n\n"
"This object may be accessed either as a tuple of\n"
"  (bsize, frsize, blocks, bfree, bavail, files, ffree, favail, flag, namemax),\n"
"or via the attributes f_bsize, f_frsize, f_blocks, f_bfree, and so on.\n\n"
"See os.statvfs for more information.";

static sky_struct_sequence_field_t sky_posix_statvfs_result_fields[] = {
    { "f_bsize",    NULL, 0 },
    { "f_frsize",   NULL, 0 },
    { "f_blocks",   NULL, 0 },
    { "f_bfree",    NULL, 0 },
    { "f_bavail",   NULL, 0 },
    { "f_files",    NULL, 0 },
    { "f_ffree",    NULL, 0 },
    { "f_favail",   NULL, 0 },
    { "f_flag",     NULL, 0 },
    { "f_namemax",  NULL, 0 },
};
static const size_t sky_posix_statvfs_result_nfields =
        sizeof(sky_posix_statvfs_result_fields) /
        sizeof(sky_posix_statvfs_result_fields[0]);

static sky_object_t
sky_posix_statvfs_create(struct statvfs *buf)
{
    sky_object_t    values[10];

    values[0] = sky_integer_create(buf->f_bsize);
    values[1] = sky_integer_create(buf->f_frsize);
    values[2] = sky_integer_create(buf->f_blocks);
    values[3] = sky_integer_create(buf->f_bfree);
    values[4] = sky_integer_create(buf->f_bavail);
    values[5] = sky_integer_create(buf->f_files);
    values[6] = sky_integer_create(buf->f_ffree);
    values[7] = sky_integer_create(buf->f_favail);
    values[8] = sky_integer_create(buf->f_flag);
    values[9] = sky_integer_create(buf->f_namemax);

    return sky_struct_sequence_create(sky_posix_statvfs_result_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}
#endif


#if defined(HAVE_FSTATVFS)
static sky_object_t
sky_posix_fstatvfs(int fd)
{
    struct statvfs  st;

    if (fstatvfs(fd, &st) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_statvfs_create(&st);
}

static const char sky_posix_fstatvfs_doc[] =
"fstatvfs(fd) -> statvfs result\n\n"
"Perform an fstatvfs system call on the given fd.\n"
"Equivalent to statvfs(fd).";
#endif


static sky_string_t
sky_posix_getcwd(void)
{
    char        buf[MAXPATHLEN];
    const char  *cwd;

    if (!(cwd = getcwd(buf, sizeof(buf)))) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_fsname(cwd, -1);
}

static const char sky_posix_getcwd_doc[] =
"getcwd() -> path\n\n"
"Return a unicode string representing the current working directory.";


static sky_bytes_t
sky_posix_getcwdb(void)
{
    char        buf[MAXPATHLEN];
    const char  *cwd;

    if (!(cwd = getcwd(buf, sizeof(buf)))) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_bytes_createfrombytes(cwd, strlen(cwd));
}

static const char sky_posix_getcwdb_doc[] =
"getcwdb() -> path\n\n"
"Return a bytes string representing the current working directory.";


/* Apple uses int instead of gid_t for a couple of group related calls. Use a
 * typedef to deal with the issue, since sizeof(int) is not guaranteed to be
 * the same as sizeof(gid_t).
 */
#if defined(__APPLE__)
typedef int sky_posix_gid_t;
#else
typedef gid_t sky_posix_gid_t;
#endif


static sky_list_t
sky_posix_getgrouplist(sky_string_t user, sky_posix_gid_t group)
{
    int             i, ngroups;
    sky_list_t      list;
    const char *    cuser;
    sky_posix_gid_t *groups;

    SKY_ASSET_BLOCK_BEGIN {
        if (!(cuser = sky_string_cstring(user))) {
            cuser = sky_codec_encodetocstring(user, 0);
            sky_asset_save((void *)cuser, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        ngroups = NGROUPS_MAX;
        groups = sky_asset_malloc(sizeof(*groups) * ngroups,
                                  SKY_ASSET_CLEANUP_ALWAYS);

        for (;;) {
            errno = 0;
            ngroups = sky_memsize(groups) / sizeof(*groups);
            if (getgrouplist(cuser, group, groups, &ngroups) != -1) {
                break;
            }
            if (errno) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            groups = sky_asset_realloc(groups,
                                       sizeof(*groups) * ngroups,
                                       SKY_ASSET_CLEANUP_ALWAYS,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
        }

        list = sky_list_createwithcapacity(ngroups);
        for (i = 0; i < ngroups; ++i) {
            sky_list_append(list, sky_integer_createfromunsigned(groups[i]));
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky_posix_getgrouplist_doc[] =
"getgrouplist(user, group) -> list of groups to which a user belongs\n\n"
"Returns a list of groups to which a user belongs.\n\n"
"    user: username to lookup\n"
"    group: base group id of the user";


static sky_list_t
sky_posix_getgroups(void)
{
    int         i, ngroups;
    gid_t       *groups, static_groups[NGROUPS_MAX];
    sky_list_t  list;

    SKY_ASSET_BLOCK_BEGIN {
#if defined(__APPLE__)
        /* As of OS X 10.8, getgroups(2) no longer raises EINVAL if there are
         * more groups than can fit in grouplist. Therefore, on OS X always
         * first call getgroups with length 0 to get the actual number of
         * groups.
         */
        ngroups = getgroups(0, NULL);
        if (-1 == ngroups) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (ngroups <= NGROUPS_MAX) {
            /* groups will fit in existing array */
            groups = static_groups;
        }
        else {
            groups = sky_asset_malloc(sizeof(gid_t) * ngroups,
                                      SKY_ASSET_CLEANUP_ALWAYS);
        }
#else
        groups = static_groups;
        ngroups = NGROUPS_MAX;
#endif

        ngroups = getgroups(ngroups, groups);
        if (-1 == ngroups) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        list = sky_list_createwithcapacity(ngroups);
        for (i = 0; i < ngroups; ++i) {
            sky_list_append(list, sky_integer_createfromunsigned(groups[i]));
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky_posix_getgroups_doc[] =
"getgroups() -> list of group IDs\n\n"
"Return list of supplemental group IDs for the process.";


static sky_tuple_t
sky_posix_getloadavg(void)
{
    double  loadavg[3];

    if (getloadavg(loadavg, sizeof(loadavg) / sizeof(loadavg[0])) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_object_build("(ddd)", loadavg[0], loadavg[1], loadavg[2]);
}

static const char sky_posix_getloadavg_doc[] =
"getloadavg() -> (float, float, float)\n\n"
"Return the number of processes in the system run queue averaged over\n"
"the last 1, 5, and 15 minutes or raises OSError if the load average\n"
"was unobtainable";


static sky_string_t
sky_posix_getlogin(void)
{
    char    *name;

    if (!(name = getlogin())) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_fsname(name, -1);
}

static const char sky_posix_getlogin_doc[] =
"getlogin() -> string\n\n"
"Return the actual login name.";


#if defined(HAVE_GETRESGID)
static sky_tuple_t
sky_posix_getresgid(void)
{
    gid_t   egid, rgid, sgid;

    if (getresgid(&rgid, &egid, &sgid) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_tuple_pack(3, sky_integer_create(rgid),
                             sky_integer_create(egid),
                             sky_integer_create(sgid));
}

static const char sky_posix_getresgid_doc[] =
"getresgid() -> (rgid, egid, sgid)\n\n"
"Get tuple of the current process's real, effective, and saved group ids.";
#endif


#if defined(HAVE_GETRESUID)
static sky_tuple_t
sky_posix_getresuid(void)
{
    uid_t   euid, ruid, suid;

    if (getresuid(&ruid, &euid, &suid) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_tuple_pack(3, sky_integer_create(ruid),
                             sky_integer_create(euid),
                             sky_integer_create(suid));
}

static const char sky_posix_getresuid_doc[] =
"getresuid() -> (ruid, euid, suid)\n\n"
"Get tuple of the current process's real, effective, and saved user ids.";
#endif


#if defined(HAVE_SYS_XATTR_H)
static sky_bytes_t
sky_posix_getxattr(
                sky_object_t    path,
                sky_object_t    attribute,
                sky_bool_t      follow_symlinks)
{
#if defined(__APPLE__)
    int                 options;
    ssize_t             size;
#endif
    char                *buffer;
    ssize_t             result;
    sky_bytes_t         bytes;
    sky_posix_path_t    cattribute, cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        sky_posix_path_convert(&cattribute, attribute, SKY_FALSE);
        sky_posix_validate_fd_and_follow_symlinks("getxattr",
                                                  cpath.fd,
                                                  follow_symlinks);

#if defined(__APPLE__)
        buffer = NULL;
        options = (follow_symlinks ? 0 : XATTR_NOFOLLOW);
retry:
        if (cpath.fd >= 0) {
            size = fgetxattr(cpath.fd, cattribute.bytes, NULL, 0, 0, options);
            if (-1 == size) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            buffer = sky_asset_realloc(buffer,
                                       size,
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            result = fgetxattr(cpath.fd,
                               cattribute.bytes,
                               buffer,
                               sky_memsize(buffer),
                               0,
                               options);
        }
        else {
            size = getxattr(cpath.bytes, cattribute.bytes, NULL, 0, 0, options);
            if (-1 == size) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            buffer = sky_asset_realloc(buffer,
                                       size,
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            result = getxattr(cpath.bytes,
                              cattribute.bytes,
                              buffer,
                              sky_memsize(buffer),
                              0,
                              options);
        }
        if (-1 == result && ERANGE == errno) {
            goto retry;
        }
#else
        buffer = sky_asset_malloc(XATTR_SIZE_MAX, SKY_ASSET_CLEANUP_ON_ERROR);
        if (cpath.fd >= 0) {
            result = fgetxattr(cpath.fd,
                               cattribute.bytes,
                               buffer,
                               sky_memsize(buffer));
        }
        else if (follow_symlinks) {
            result = getxattr(cpath.bytes,
                              cattribute.bytes,
                              buffer,
                              sky_memsize(buffer));
        }
        else {
            result = lgetxattr(cpath.bytes,
                               cattribute.bytes,
                               buffer,
                               sky_memsize(buffer));
        }
#endif
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, result);
        }
        buffer = sky_asset_realloc(buffer,
                                   result + 1,
                                   SKY_ASSET_CLEANUP_ON_ERROR,
                                   SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer[result] = '\0';

        bytes = sky_bytes_createwithbytes(buffer, result, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char sky_posix_getxattr_doc[] =
"getxattr(path, attribute, *, follow_symlinks=True) -> value\n\n"
"Return the value of extended attribute attribute on path.\n\n"
"path may be either a string or an open file descriptor.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, getxattr will examine the symbolic link itself instead of the file\n"
"  the link points to.";
#endif


static void
sky_posix_initgroups(sky_string_t username, sky_posix_gid_t gid)
{
    const char  *cuser;

    SKY_ASSET_BLOCK_BEGIN {
        if (!(cuser = sky_string_cstring(username))) {
            cuser = sky_codec_encodetocstring(username, 0);
            sky_asset_save((void *)cuser, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (initgroups(cuser, gid) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_posix_initgroups_doc[] =
"initgroups(username, gid) -> None\n\n"
"Call the system initgroups() to initialize the group access list with all of\n"
"the groups of which the specified username is a member, plus the specified\n"
"group id.";


#if defined(HAVE_LCHFLAGS)
static int
sky_posix_lchflags(sky_object_t path, unsigned int flags)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
        result = lchflags(cpath.bytes, flags);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_lchflags_doc[] =
"lchflags(path, flags)\n\n"
"Set file flags.\n"
"This function will not follow symbolic links.\n"
"Equivalent to chflags(path, flags, follow_symlinks=False).";
#endif


#if defined(HAVE_LCHOWN)
static int
sky_posix_lchown(sky_object_t path, uid_t uid, gid_t gid)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
        result = lchown(cpath.bytes, uid, gid);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_lchown_doc[] =
"lchown(path, uid, gid)\n\n"
"Change the owner and group id of path to the numeric uid and gid.\n"
"This function will not follow symbolic links.\n"
"Equivalent to os.chown(path, uid, gid, follow_symlinks=False).";
#endif


static int
sky_posix_link(
                sky_object_t    src,
                sky_object_t    dst,
                sky_object_t    src_dir_fd,
                sky_object_t    dst_dir_fd,
                sky_bool_t      follow_symlinks)
{
    int dst_fd = SKY_POSIX_DIR_FD_DEFAULT,
        src_fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cdst, csrc;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&csrc, src, SKY_FALSE);
        sky_posix_path_convert(&cdst, dst, SKY_FALSE);

#if defined(HAVE_LINKAT)
        src_fd = sky_posix_dir_fd_convert(src_dir_fd);
        dst_fd = sky_posix_dir_fd_convert(dst_dir_fd);
#else
        if (!sky_object_isnull(src_dir_fd) || !sky_object_isnull(dst_dir_fd)) {
            sky_error_raise_string(sky_NotImplementedError,
                                   "link: src_dir_fd and dst_dir_fd "
                                   "unavailable on this platform");
        }
        sky_posix_validate_follow_symlinks("link", follow_symlinks);
#endif

        if (SKY_POSIX_DIR_FD_DEFAULT != src_fd ||
            SKY_POSIX_DIR_FD_DEFAULT != dst_fd ||
            !follow_symlinks)
        {
#if defined(HAVE_LINKAT)
            result = linkat(src_fd, csrc.bytes,
                            dst_fd, cdst.bytes,
                            (follow_symlinks ? AT_SYMLINK_FOLLOW : 0));
#else
            sky_error_fatal("unreachable");
#endif
        }
        else {
            result = link(csrc.bytes, cdst.bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_link_doc[] =
"link(src, dst, *, src_dir_fd=None, dst_dir_fd=None, follow_symlinks=True)\n\n"
"Create a hard link to a file.\n\n"
"If either src_dir_fd or dst_dir_fd is not None, it should be a file\n"
"  descriptor open to a directory, and the respective path string (src or dst)\n"
"  should be relative; the path will then be relative to that directory.\n"
"If follow_symlinks is False, and the last element of src is a symbolic\n"
"  link, link will create a link to the symbolic link itself instead of the\n"
"  file the link points to.\n"
"src_dir_fd, dst_dir_fd, and follow_symlinks may not be implemented on your\n"
"  platform.  If they are unavailable, using them will raise a\n"
"  NotImplementedError.";


static sky_list_t
sky_posix_listdir(sky_object_t path)
{
    DIR                 *dirp;
    size_t              name_len;
    sky_bool_t          return_string;
    sky_list_t          list;
    sky_object_t        name;
    struct dirent       *dirent;
    sky_posix_path_t    cpath;

    if (sky_object_isnull(path)) {
        path = SKY_STRING_LITERAL(".");
    }
    return_string = !sky_buffer_check(path);
    SKY_ASSET_BLOCK_BEGIN {
#if defined(HAVE_FDOPENDIR)
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        if (-1 != cpath.fd) {
            int fd;

            if ((fd = dup(cpath.fd)) == -1) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (!(dirp = fdopendir(fd))) {
                int saved_errno = errno;
                close(fd);
                sky_error_raise_errno(sky_OSError, saved_errno);
            }
            sky_asset_save(dirp,
                           (sky_free_t)rewinddir,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }
        else if (!(dirp = opendir(cpath.bytes))) {
            sky_error_raise_errno(sky_OSError, errno);
        }
#else
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
        if (!(dirp = opendir(cpath.bytes))) {
            sky_error_raise_errno(sky_OSError, errno);
        }
#endif
        sky_asset_save(dirp, (sky_free_t)closedir, SKY_ASSET_CLEANUP_ALWAYS);

        list = sky_list_create(NULL);
        for (;;) {
            errno = 0;
            if (!(dirent = readdir(dirp))) {
                if (!errno) {
                    break;
                }
                sky_error_raise_errno(sky_OSError, errno);
            }
            name_len = strlen(dirent->d_name);
            if (dirent->d_name[0] == '.' &&
                (1 == name_len ||
                 (dirent->d_name[1] == '.' && 2 == name_len)))
            {
                continue;
            }
            if (return_string) {
                name = sky_posix_fsname(dirent->d_name, name_len);
            }
            else {
                name = sky_bytes_createfrombytes(dirent->d_name, name_len);
            }
            sky_list_append(list, name);
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky_posix_listdir_doc[] =
"listdir(path='.') -> list_of_filenames\n"
"\n"
"Return a list containing the names of the files in the directory.\n"
"The list is in arbitrary order.  It does not include the special\n"
"entries '.' and '..' even if they are present in the directory.\n"
"\n"
"path can be specified as either str or bytes.  If path is bytes,\n"
"  the filenames returned will also be bytes; in all other circumstances\n"
"  the filenames returned will be str.\n"
"On some platforms, path may also be specified as an open file descriptor;\n"
"  the file descriptor must refer to a directory.\n"
"  If this functionality is unavailable, using it raises NotImplementedError.";


#if defined(HAVE_SYS_XATTR_H)
static sky_list_t
sky_posix_listxattr(sky_object_t path, sky_bool_t follow_symlinks)
{
#if defined(__APPLE__)
    int                 options;
#endif
    char                *buffer, *c, *end, *start;
    ssize_t             size;
    sky_list_t          list;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isnull(path)) {
            path = SKY_STRING_LITERAL(".");
        }
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        sky_posix_validate_fd_and_follow_symlinks("listxattr",
                                                  cpath.fd,
                                                  follow_symlinks);

#if defined(__APPLE__)
        buffer = NULL;
        options = (follow_symlinks ? 0 : XATTR_NOFOLLOW);
        for (;;) {
            if (cpath.fd >= 0) {
                size = flistxattr(cpath.fd, NULL, 0, options);
            }
            else {
                size = listxattr(cpath.bytes, NULL, 0, options);
            }
            if (-1 == size) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (size > 0) {
                buffer = sky_asset_realloc(buffer,
                                           size,
                                           SKY_ASSET_CLEANUP_ALWAYS,
                                           SKY_ASSET_UPDATE_FIRST_CURRENT);
                size = sky_memsize(buffer);
                if (cpath.fd >= 0) {
                    size = flistxattr(cpath.fd, buffer, size, options);
                }
                else {
                    size = listxattr(cpath.bytes, buffer, size, options);
                }
                if (-1 == size) {
                    if (ERANGE == errno) {
                        continue;
                    }
                    sky_error_raise_errno(sky_OSError, errno);
                }
            }
            break;
        }
#else
        buffer = sky_asset_malloc(XATTR_LIST_MAX, SKY_ASSET_CLEANUP_ALWAYS);
        size = sky_memsize(buffer);
        if (cpath.fd >= 0) {
            size = flistxattr(cpath.fd, buffer, size);
        }
        else if (follow_symlinks) {
            size = listxattr(cpath.bytes, buffer, size);
        }
        else {
            size = llistxattr(cpath.bytes, buffer, size);
        }
        if (-1 == size) {
            sky_error_raise_errno(sky_OSError, errno);
        }
#endif

        list = sky_list_create(NULL);
        end = buffer + size;
        for (c = start = buffer; c < end; ++c) {
            if (!*c) {
                sky_list_append(list,
                                sky_string_createfrombytes(start,
                                                           c - start,
                                                           NULL,
                                                           NULL));
                start = c + 1;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky_posix_listxattr_doc[] =
"listxattr(path='.', *, follow_symlinks=True)\n\n"
"Return a list of extended attributes on path.\n\n"
"path may be either None, a string, or an open file descriptor.\n"
"if path is None, listxattr will examine the current directory.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, listxattr will examine the symbolic link itself instead of the file\n"
"  the link points to.";
#endif


static off_t
sky_posix_lseek(int fd, off_t pos, int how)
{
    int whence = how;

#if defined(SEEK_SET)
    switch (how) {
        case 0:
            whence = SEEK_SET;
            break;
        case 1:
            whence = SEEK_CUR;
            break;
        case 2:
            whence = SEEK_END;
            break;
    }
#endif

    return lseek(fd, pos, whence);
}

static const char sky_posix_lseek_doc[] =
"lseek(fd, pos, how) -> newpos\n\n"
"Set the current position of a file descriptor.\n"
"Return the new cursor position in bytes, starting from the beginning.";


static sky_object_t
sky_posix_lstat(sky_object_t path, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    sky_object_t        result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#if defined(HAVE_FSTATAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        result = sky_posix_stat_common("stat",
                                       &cpath,
                                       fd,
                                       SKY_FALSE);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_lstat_doc[] =
"lstat(path, *, dir_fd=None) -> stat result\n\n"
"Like stat(), but do not follow symbolic links.\n"
"Equivalent to stat(path, follow_symlinks=False).";


static int
sky_posix_mkdir(sky_object_t path, int mode, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_MKDIRAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("mkdir", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("mkdir", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = mkdir(cpath.bytes, mode);
        }
        else {
#if defined(HAVE_MKDIRAT)
            result = mkdirat(fd, cpath.bytes, mode);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_mkdir_doc[] =
"mkdir(path, mode=0o777, *, dir_fd=None)\n\n"
"Create a directory.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"If it is unavailable, using it will raise a NotImplementedError.\n"
"\n"
"The mode argument is ignored on Windows.";


static int
sky_posix_mkfifo(sky_object_t path, mode_t mode, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_MKFIFOAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("mkfifo", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("mkfifo", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = mkfifo(cpath.bytes, mode);
        }
        else {
#if defined(HAVE_MKFIFOAT)
            result = mkfifoat(fd, cpath.bytes, mode);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_mkfifo_doc[] =
"mkfifo(path, mode=0o666, *, dir_fd=None)\n\n"
"Create a FIFO (a POSIX named pipe).\n\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


static int
sky_posix_mknod(
                sky_object_t    path,
                mode_t          mode,
                dev_t           device,
                sky_object_t    dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_MKNODAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("mknod", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("mknod", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = mknod(cpath.bytes, mode, device);
        }
        else {
#if defined(HAVE_MKNODAT)
            result = mknodat(fd, cpath.bytes, mode, device);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_mknod_doc[] =
"mknod(filename, mode=0o666, device=0, *, dir_fd=None)\n\n"
"Create a filesystem node (file, device special file or named pipe)\n"
"named filename. mode specifies both the permissions to use and the\n"
"type of node to be created, being combined (bitwise OR) with one of\n"
"S_IFREG, S_IFCHR, S_IFBLK, and S_IFIFO. For S_IFCHR and S_IFBLK,\n"
"device defines the newly created device special file (probably using\n"
"os.makedev()), otherwise it is ignored.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


static int
sky_posix_nice(int inc)
{
    int value;

    errno = 0;
    value = nice(inc);
    if (!value) {
        value = getpriority(PRIO_PROCESS, 0);
    }
    if (-1 == value && errno) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return value;
}

static const char sky_posix_nice_doc[] =
"nice(inc) -> new_priority\n\n"
"Decrease the priority of process by inc and return the new priority.";


static int
sky_posix_open(
                sky_object_t    path,
                int             flags,
                int             mode,
                sky_object_t    dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_OPENAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("open", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("open", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = open(cpath.bytes, flags, mode);
        }
        else {
#if defined(HAVE_OPENAT)
            result = openat(fd, cpath.bytes, flags, mode);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_open_doc[] =
"open(path, flags, mode=0o777, *, dir_fd=None)\n\n"
"Open a file for low level IO.  Returns a file handle (integer).\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


#if defined(HAVE_OPENPTY)
static sky_tuple_t
sky_posix_openpty(void)
{
    int amaster, aslave;

    if (openpty(&amaster, &aslave, NULL, NULL, NULL) != 0) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_object_build("(ii)", amaster, aslave);
}

static const char sky_posix_openpty_doc[] =
"openpty() -> (master_fd, slave_fd)\n\n"
"Open a pseudo-terminal, returning open fd's for both master and slave end.\n";
#endif


static long
sky_posix_pathconf(sky_object_t path, sky_object_t name)
{
    int                 cname;
    long                result;
    sky_posix_path_t    cpath;

    cname = sky_posix_constant_lookup(&sky_posix_pathconf_table, name);

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);

        errno = 0;
        if (-1 != cpath.fd) {
            result = fpathconf(cpath.fd, cname);
        }
        else {
            result = pathconf(cpath.bytes, cname);
        }

        if (-1 == result && errno) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_pathconf_doc[] =
"pathconf(path, name) -> integer\n\n"
"Return the configuration limit name for the file or directory path.\n"
"If there is no limit, return -1.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailable, using it raises an exception.";


static sky_tuple_t
sky_posix_pipe(void)
{
    int fds[2];

    if (pipe(fds) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_object_build("(ii)", fds[0], fds[1]);
}

static const char sky_posix_pipe_doc[] =
"pipe() -> (read_end, write_end)\n\n"
"Create a pipe.";


#if defined(HAVE_PIPE2)
static sky_tuple_t
sky_posix_pipe2(int flags)
{
    int fds[2];

    if (pipe2(fds, flags) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_object_build("(ii)", fds[0], fds[1]);
}

static const char sky_posix_pipe2_doc[] =
"pipe2(flags) -> (read_end, write_end)\n\n"
"Create a pipe with flags set atomically.\n"
"flags can be constructed by ORing together one or more of these values:\n"
"O_NONBLOCK, O_CLOEXEC.\n";
#endif


#if defined(HAVE_PREAD)
static sky_bytes_t
sky_posix_pread(int fd, ssize_t size, off_t offset)
{
    char        *buffer;
    ssize_t     nbytes;
    sky_bytes_t result;

    if (size < 0) {
        sky_error_raise_errno(sky_OSError, EINVAL);
    }
    if (!size) {
        return sky_bytes_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(size, SKY_ASSET_CLEANUP_ON_ERROR);
        nbytes = pread(fd, buffer, size, offset);
        if (-1 == nbytes) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        buffer = sky_asset_realloc(buffer,
                                   nbytes + 1,
                                   SKY_ASSET_CLEANUP_ON_ERROR,
                                   SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer[nbytes] = '\0';

        result = sky_bytes_createwithbytes(buffer, nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_pread_doc[] =
"pread(fd, buffersize, offset) -> string\n\n"
"Read from a file descriptor, fd, at a position of offset. It will read up\n"
"to buffersize number of bytes. The file offset remains unchanged.";
#endif


#if !defined(HAVE_SETENV)
/* putenv() does not make a copy of the string passed to it, but it keeps it
 * referenced in the environment table. This is a horrible interface, but it
 * is what it is.
 */
static sky_spinlock_t   sky_posix_putenv_spinlock;
static sky_dict_t       sky_posix_putenv_dict;
#endif

static void
sky_posix_putenv(sky_object_t key, sky_object_t value)
{
#if defined(HAVE_SETENV)
    sky_posix_path_t ckey, cvalue;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&ckey, key, SKY_FALSE);
        sky_posix_path_convert(&cvalue, value, SKY_FALSE);

        if (setenv(ckey.bytes, cvalue.bytes, 1) != 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;
#else
    char                *string;
    sky_capsule_t       capsule;
    sky_posix_path_t    ckey, cvalue;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&ckey, key, SKY_FALSE);
        sky_posix_path_convert(&cvalue, value, SKY_FALSE);

        sky_format_asprintf(&string, "%s=%s", ckey.bytes, cvalue.bytes);
        sky_asset_save(string, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        capsule = sky_capsule_create(string, NULL, sky_free, NULL);
    } SKY_ASSET_BLOCK_END;

    SKY_ASSET_BLOCK_BEGIN {
        sky_spinlock_lock(&sky_posix_putenv_spinlock);
        sky_asset_save(&sky_posix_putenv_spinlock,
                       (sky_free_t)sky_spinlock_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (putenv(string) != 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_dict_setitem(sky_posix_putenv_dict, key, capsule);
    } SKY_ASSET_BLOCK_END;
#endif
}

static const char sky_posix_putenv_doc[] =
"putenv(key, value)\n\n"
"Change or add an environment variable.";


#if defined(HAVE_PWRITE)
static ssize_t
sky_posix_pwrite(int fd, sky_object_t string, off_t offset)
{
    ssize_t         result;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = pwrite(fd, buffer.buf, buffer.len, offset);
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_pwrite_doc[] =
"pwrite(fd, string, offset) -> byteswritten\n\n"
"Write string to a file descriptor, fd, from offset, leaving the file\n"
"offset unchanged.";
#endif


static sky_bytes_t
sky_posix_read(int fd, ssize_t size)
{
    char        *buffer;
    ssize_t     nbytes;
    sky_bytes_t result;

    if (size < 0) {
        sky_error_raise_errno(sky_OSError, EINVAL);
    }
    if (!size) {
        return sky_bytes_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(size, SKY_ASSET_CLEANUP_ON_ERROR);
        nbytes = read(fd, buffer, size);
        if (-1 == nbytes) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        buffer = sky_asset_realloc(buffer,
                                   nbytes + 1,
                                   SKY_ASSET_CLEANUP_ON_ERROR,
                                   SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer[nbytes] = '\0';

        result = sky_bytes_createwithbytes(buffer, nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_read_doc[] =
"read(fd, buffersize) -> bytes\n\n"
"Read a file descriptor.";


static sky_object_t
sky_posix_readlink(sky_object_t path, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    char                buffer[MAXPATHLEN];
    ssize_t             length;
    sky_object_t        result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_READLINKAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            length = readlink(cpath.bytes, buffer, sizeof(buffer));
        }
        else {
#if defined(HAVE_READLINKAT)
            length = readlinkat(fd, cpath.bytes, buffer, sizeof(buffer));
#else
            length = -1;
#endif
        }
        if (length < 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        if (sky_object_isa(path, sky_string_type)) {
            result = sky_posix_fsname(buffer, length);
        }
        else {
            result = sky_bytes_createfrombytes(buffer, length);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_readlink_doc[] =
"readlink(path, *, dir_fd=None)\n\n"
"Return a string representing the path to which the symbolic link points.\n\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


#if defined(HAVE_READV)
static ssize_t
sky_posix_readv(int fd, sky_object_t buffers)
{
    ssize_t         result;
    struct iovec    *iov;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_iov(&iov, buffers, SKY_BUFFER_FLAG_WRITABLE);
        if ((result = readv(fd, iov, sky_object_len(buffers))) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_readv_doc[] =
"readv(fd, buffers) -> bytesread\n\n"
"Read from a file descriptor fd into a number of mutable, bytes-like\n"
"objects (\"buffers\").  readv will transfer data into each buffer\n"
"until it is full and then move on to the next buffer in the sequence\n"
"to hold the rest of the data.\n\n"
"readv returns the total number of bytes read (which may be less than\n"
"the total capacity of all the buffers.";
#endif


#if defined(HAVE_SYS_XATTR_H)
static int
sky_posix_removexattr(
                sky_object_t    path,
                sky_object_t    attribute,
                sky_bool_t      follow_symlinks)
{
#if defined(__APPLE__)
    int                 options;
#endif
    int                 result;
    sky_posix_path_t    cattribute, cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        sky_posix_path_convert(&cattribute, attribute, SKY_FALSE);

#if defined(__APPLE__)
        options = (follow_symlinks ? 0 : XATTR_NOFOLLOW);
        if (cpath.fd >= 0) {
            result = fremovexattr(cpath.fd, cattribute.bytes, options);
        }
        else {
            result = removexattr(cpath.bytes, cattribute.bytes, options);
        }
#else
        if (cpath.fd >= 0) {
            result = fremovexattr(cpath.fd, cattribute.bytes);
        }
        else if (follow_symlinks) {
            result = removexattr(cpath.bytes, cattribute.bytes);
        }
        else {
            result = lremovexattr(cpath.bytes, cattribute.bytes);
        }
#endif
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_removexattr_doc[] =
"removexattr(path, attribute, *, follow_symlinks=True)\n\n"
"Remove extended attribute attribute on path.\n"
"path may be either a string an open file descriptor.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, removexattr will modify the symbolic link itself instead of the file\n"
"  the link points to.";
#endif


static int
sky_posix_rename_common(
                sky_object_t    src,
                sky_object_t    dst,
                sky_object_t    src_dir_fd,
                sky_object_t    dst_dir_fd,
                sky_bool_t      is_rename)
{
    int src_fd = SKY_POSIX_DIR_FD_DEFAULT,
        dst_fd = SKY_POSIX_DIR_FD_DEFAULT;

    const char  *name = (is_rename ? "rename" : "replace");

    int                 result;
    sky_posix_path_t    dst_cpath, src_cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&src_cpath, src, SKY_FALSE);
        sky_posix_path_convert(&dst_cpath, dst, SKY_FALSE);

        src_fd = sky_posix_dir_fd_convert(src_dir_fd);
        sky_posix_validate_path_and_dir_fd(name, &src_cpath, src_fd);
        sky_posix_validate_dir_fd_and_fd(name, src_fd, src_cpath.fd);
        dst_fd = sky_posix_dir_fd_convert(dst_dir_fd);
        sky_posix_validate_path_and_dir_fd(name, &dst_cpath, dst_fd);
        sky_posix_validate_dir_fd_and_fd(name, dst_fd, dst_cpath.fd);
        if (SKY_POSIX_DIR_FD_DEFAULT != src_fd ||
            SKY_POSIX_DIR_FD_DEFAULT != dst_fd)
        {
#if !defined(HAVE_RENAMEAT)
            sky_error_raise_format(
                    sky_NotImplementedError,
                    "%s: src_dir_fd and dst_dir_fd unavailable on this platform",
                    name);
#else
            result = renameat(src_fd, src_cpath.bytes, dst_fd, dst_cpath.bytes);
#endif
        }
        else {
            result = rename(src_cpath.bytes, dst_cpath.bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static int
sky_posix_rename(
                sky_object_t    src,
                sky_object_t    dst,
                sky_object_t    src_dir_fd,
                sky_object_t    dst_dir_fd)
{
    return sky_posix_rename_common(src, dst,
                                   src_dir_fd, dst_dir_fd,
                                   SKY_TRUE);
}

static const char sky_posix_rename_doc[] =
"rename(src, dst, *, src_dir_fd=None, dst_dir_fd=None)\n\n"
"Rename a file or directory.\n"
"\n"
"If either src_dir_fd or dst_dir_fd is not None, it should be a file\n"
"  descriptor open to a directory, and the respective path string (src or dst)\n"
"  should be relative; the path will then be relative to that directory.\n"
"src_dir_fd and dst_dir_fd, may not be implemented on your platform.\n"
"  If they are unavailable, using them will raise a NotImplementedError.";


static int
sky_posix_replace(
                sky_object_t    src,
                sky_object_t    dst,
                sky_object_t    src_dir_fd,
                sky_object_t    dst_dir_fd)
{
    return sky_posix_rename_common(src, dst,
                                   src_dir_fd, dst_dir_fd,
                                   SKY_FALSE);
}

static const char sky_posix_replace_doc[] =
"replace(src, dst, *, src_dir_fd=None, dst_dir_fd=None)\n\n"
"Rename a file or directory, overwriting the destination.\n"
"\n"
"If either src_dir_fd or dst_dir_fd is not None, it should be a file\n"
"  descriptor open to a directory, and the respective path string (src or dst)\n"
"  should be relative; the path will then be relative to that directory.\n"
"src_dir_fd and dst_dir_fd, may not be implemented on your platform.\n"
"  If they are unavailable, using them will raise a NotImplementedError.";


static int
sky_posix_rmdir(sky_object_t path, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_UNLINKAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("rmdir", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("rmdir", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = rmdir(cpath.bytes);
        }
        else {
#if defined(HAVE_MKDIRAT)
            result = unlinkat(fd, cpath.bytes, AT_REMOVEDIR);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_rmdir_doc[] =
"rmdir(path, *, dir_fd=None)\n\n"
"Remove a directory.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


#if defined(HAVE_SCHED_SETAFFINITY)
static const int NCPUS_START = sizeof(unsigned long) * CHAR_BIT;

static sky_set_t
sky_posix_sched_getaffinity(pid_t pid)
{
    int         count, cpu, ncpus;
    size_t      setsize;
    cpu_set_t   *mask;
    sky_set_t   set;

    for (ncpus = NCPUS_START; ; ncpus *= 2) {
        setsize = CPU_ALLOC_SIZE(ncpus);
        if (!(mask = CPU_ALLOC(ncpus))) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        if (sched_getaffinity(pid, setsize, mask) == 0) {
            break;
        }
        CPU_FREE(mask);
        if (EINVAL != errno) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (ncpus > INT_MAX / 2) {
            sky_error_raise_string(
                    sky_OverflowError,
                    "could not allocate a large enough CPU set");
        }
    }

    SKY_ERROR_TRY {
        set = sky_set_createwithcapacity(ncpus);
        count = CPU_COUNT_S(setsize, mask);
        for (cpu = 0; count; ++cpu) {
            if (CPU_ISSET_S(cpu, setsize, mask)) {
                --count;
                sky_set_add(set, sky_integer_create(cpu));
            }
        }
    } SKY_ERROR_EXCEPT_ANY {
        CPU_FREE(mask);
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    CPU_FREE(mask);
    return set;
}

static const char sky_posix_sched_getaffinity_doc[] =
"sched_getaffinity(pid, ncpus) -> cpu_set\n\n"
"Return the affinity of the process with PID *pid*\n"
"The returned cpu_set will be of size *ncpus*.";


static int
sky_posix_sched_setaffinity(pid_t pid, sky_object_t cpu_set)
{
    int             cpu, ncpus, result;
    size_t          setsize;
    cpu_set_t       *mask;
    sky_object_t    iter, object;

    ncpus = NCPUS_START;
    setsize = CPU_ALLOC_SIZE(ncpus);
    if (!(mask = CPU_ALLOC(ncpus))) {
        sky_error_raise_object(sky_MemoryError, sky_None);
    }
    CPU_ZERO_S(setsize, mask);

    SKY_ERROR_TRY {
        iter = sky_object_iter(cpu_set);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            if (!sky_object_isa(object, sky_integer_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "expected an iterator of ints, but iterator yielded %#@",
                        sky_type_name(sky_object_type(object)));
            }
            cpu = sky_integer_value(object, INT_MIN, INT_MAX, NULL);
            if (cpu < 0) {
                sky_error_raise_string(sky_ValueError, "negative CPU number");
            }
            if (cpu > INT_MAX - 1) {
                sky_error_raise_string(sky_OverflowError,
                                       "CPU number too large");
            }
            if (cpu >= ncpus) {
                int         new_ncpus;
                size_t      new_setsize;
                cpu_set_t   *new_mask;

                while (new_ncpus <= cpu) {
                    if (new_ncpus > INT_MAX / 2) {
                        new_ncpus = cpu + 1;
                    }
                    else {
                        new_ncpus *= 2;
                    }
                }
                new_setsize = CPU_ALLOC_SIZE(new_ncpus);
                if (!(new_mask = CPU_ALLOC(new_ncpus))) {
                    sky_error_raise_object(sky_MemoryError, sky_None);
                }
                CPU_ZERO_S(new_setsize, new_mask);
                memcpy(new_mask, mask, setsize);
                CPU_FREE(mask);

                ncpus = new_ncpus;
                setsize = new_setsize;
                mask = new_mask;
            }
            CPU_SET_S(cpu, setsize, mask);
        }
    } SKY_ERROR_EXCEPT_ANY {
        CPU_FREE(mask);
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    result = sched_setaffinity(pid, setsize, mask);
    CPU_FREE(mask);
    return result;
}

static const char sky_posix_sched_setaffinity_doc[] =
"sched_setaffinity(pid, cpu_set)\n\n"
"Set the affinity of the process with PID *pid* to *cpu_set*.";
#endif


#if defined(HAVE_SCHED_SETPARAM) || defined(HAVE_SCHED_SETSCHEDULER)
static sky_type_t sky_posix_sched_param_type;

static const char sky_posix_sched_param_doc[] =
"sched_param(sched_priority): A scheduling parameter.\n\n"
"Current has only one field: sched_priority";

static sky_struct_sequence_field_t sky_posix_sched_param_fields[] = {
    { "sched_priority", "the scheduling priority", 0 },
};
static const size_t sky_posix_sched_param_nfields =
        sizeof(sky_posix_sched_param_fields) /
        sizeof(sky_posix_sched_param_fields[0]);

static void
sky_posix_sched_param_convert(struct sched_param *param, sky_object_t tuple)
{
    if (sky_object_type(tuple) != sky_posix_sched_param_type) {
        sky_error_raise_string(sky_TypeError,
                               "must have a sched_param object");
    }

    param->sched_priority = sky_integer_value(sky_tuple_get(tuple, 0),
                                              INT_MIN, INT_MAX,
                                              NULL);
}

#if defined(HAVE_SCHED_SETPARAM)
static sky_object_t
sky_posix_sched_param_create(struct sched_param *param)
{
    sky_object_t    values[1];

    values[0] = sky_integer_create(param->sched_priority);

    return sky_struct_sequence_create(sky_posix_sched_param_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}
#endif

static sky_object_t
sky_posix_sched_param_new(
        SKY_UNUSED  sky_type_t      cls,
                    sky_object_t    sched_priority)
{
    return sky_struct_sequence_create(sky_posix_sched_param_type,
                                      1,
                                      &sched_priority);
}
#endif


#if defined(HAVE_SCHED_SETPARAM)
static sky_object_t
sky_posix_sched_getparam(pid_t pid)
{
    struct sched_param  param;

    if (sched_getparam(pid, &param) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_sched_param_create(&param);
}

static const char sky_posix_sched_getparam_doc[] =
"sched_getparam(pid) -> sched_param\n\n"
"Returns scheduling parameters for the process with *pid* as an instance of the\n"
"sched_param class. A PID of 0 means the calling process.";
#endif


#if defined(HAVE_SCHED_RR_GET_INTERVAL)
static double
sky_posix_sched_rr_get_interval(pid_t pid)
{
    struct timespec interval;

    if (sched_rr_get_interval(pid, &interval) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return (double)interval.tv_sec + (1000000000.0 * interval.tv_nsec);
}

static const char sky_posix_sched_rr_get_interval_doc[] =
"sched_rr_get_interval(pid) -> float\n\n"
"Return the round-robin quantum for the process with PID *pid* in seconds.";
#endif


#if defined(HAVE_SCHED_SETPARAM)
static int
sky_posix_sched_setparam(pid_t pid, sky_object_t param)
{
    struct sched_param  p;

    sky_posix_sched_param_convert(&p, param);
    return sched_setparam(pid, &p);
}

static const char sky_posix_sched_setparam_doc[] =
"sched_setparam(pid, param)\n\n"
"Set scheduling parameters for a process with PID *pid*.\n"
"A PID of 0 means the calling process.";
#endif


#if defined(HAVE_SCHED_SETSCHEDULER)
static int
sky_posix_sched_setscheduler(pid_t pid, int policy, sky_object_t param)
{
    struct sched_param  p;

    sky_posix_sched_param_convert(&p, param);
    return sched_setscheduler(pid, policy, &p);
}

static const char sky_posix_sched_setscheduler_doc[] =
"sched_setscheduler(pid, policy, param)\n\n"
"Set the scheduling policy, *policy*, for *pid*.\n"
"If *pid* is 0, the calling process is changed.\n"
"*param* is an instance of sched_param.";
#endif


#if defined(HAVE_SENDFILE)
#if defined(__APPLE__) || defined(__DragonFly__) || defined(__FreeBSD__)
static ssize_t
sky_posix_sendfile(
                int             out,
                int             in,
                off_t           offset,
                off_t           count,
                sky_object_t    headers,
                sky_object_t    trailers,
                int             flags)
{
    int             result;
    ssize_t         n, nbytes, sbytes;
    struct sf_hdtr  sf;

    SKY_ASSET_BLOCK_BEGIN {
        nbytes = count;
        if (!sky_object_isnull(headers)) {
            if (sky_object_len(headers) > INT_MAX) {
                sky_error_raise_string(sky_ValueError, "too many headers");
            }
            sf.hdr_cnt = sky_object_len(headers);
            n = sky_posix_iov(&(sf.headers), headers, SKY_BUFFER_FLAG_SIMPLE);
#if defined(__APPLE__)
            if (count) {
                nbytes += n;
            }
#endif
        }
        if (!sky_object_isnull(trailers)) {
            if (sky_object_len(trailers) > INT_MAX) {
                sky_error_raise_string(sky_ValueError, "too many trailers");
            }
            sf.trl_cnt = sky_object_len(trailers);
            n = sky_posix_iov(&(sf.trailers), trailers, SKY_BUFFER_FLAG_SIMPLE);
            /* APPLE: trailers are always sent, regardless of 'len' param */
        }

#if defined(__APPLE__)
        result = sendfile(in, out, offset, (off_t *)&nbytes, &sf, flags);
        sbytes = nbytes;
#else
        result = sendfile(in, out, offset, nbytes, &sf, &sbytes, flags);
#endif
        if (-1 == result) {
            if ((EAGAIN != errno && EBUSY != errno) || !sbytes) {
                sky_error_raise_errno(sky_OSError, errno);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return sbytes;
}
#elif defined(linux)
static ssize_t
sky_posix_sendfile(int out, int in, sky_object_t offset, off_t count)
{
    off_t   off;
    ssize_t result;

    if (sky_object_isnull(offset)) {
        result = sendfile(out, in, NULL, count);
    }
    else {
        sky_data_type_store(SKY_DATA_TYPE_OFF_T, NULL, offset, &off, NULL);
        result = sendfile(out, in, &off, count);
    }
    if (-1 == result) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return result;
}
#else
static ssize_t
sky_posix_sendfile(int out, int in, off_t offset, off_t count)
{
    ssize_t result;

    if ((result = sendfile(out, in, &offset, count)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return result;
}
#endif

static const char sky_posix_sendfile_doc[] =
"sendfile(out, in, offset, nbytes) -> byteswritten\n"
"sendfile(out, in, offset, nbytes, headers=None, trailers=None, flags=0)\n"
"            -> byteswritten\n"
"Copy nbytes bytes from file descriptor in to file descriptor out.";
#endif


static void
sky_posix_setgroups(sky_list_t list)
{
    int             ngroups;
    gid_t           groups[NGROUPS_MAX];
    sky_object_t    group;

    if (sky_list_len(list) > NGROUPS_MAX) {
        sky_error_raise_string(sky_ValueError, "too many groups");
    }

    ngroups = 0;
    SKY_SEQUENCE_FOREACH(list, group) {
        if (!sky_object_isa(group, sky_integer_type)) {
            sky_error_raise_string(sky_TypeError, "groups must be integers");
        }
        sky_data_type_store(SKY_DATA_TYPE_GID_T,
                            NULL,
                            group,
                            &(groups[ngroups]),
                            NULL);
        ++ngroups;
    } SKY_SEQUENCE_FOREACH_END;

    if (setgroups(ngroups, groups) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_posix_setgroups_doc[] =
"setgroups(list)\n\n"
"Set the groups of the current process to list.";


#if defined(HAVE_SYS_XATTR_H)
static int
sky_posix_setxattr(
                sky_object_t    path,
                sky_object_t    attribute,
                sky_object_t    value,
                int             flags,
                sky_bool_t      follow_symlinks)
{
    int                 result;
    sky_buffer_t        buffer;
    sky_posix_path_t    cattribute, cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        sky_posix_path_convert(&cattribute, attribute, SKY_FALSE);

        sky_buffer_acquire(&buffer, value, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

#if defined(__APPLE__)
        if (!follow_symlinks) {
            flags |= XATTR_NOFOLLOW;
        }
        if (cpath.fd >= 0) {
            result = fsetxattr(cpath.fd,
                               cattribute.bytes,
                               buffer.buf,
                               buffer.len,
                               0,
                               flags);
        }
        else {
            result = setxattr(cpath.bytes,
                              cattribute.bytes,
                              buffer.buf,
                              buffer.len,
                              0,
                              flags);
        }
#else
        if (cpath.fd >= 0) {
            result = fsetxattr(cpath.fd,
                               cattribute.bytes,
                               buffer.buf,
                               buffer.len,
                               flags);
        }
        else if (follow_symlinks) {
            result = setxattr(cpath.bytes,
                              cattribute.bytes,
                              buffer.buf,
                              buffer.len,
                              flags);
        }
        else {
            result = lsetxattr(cpath.bytes,
                               cattribute.bytes,
                               buffer.buf,
                               buffer.len,
                               flags);
        }
#endif
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_setxattr_doc[] =
"setxattr(path, attribute, value, flags=0, *, follow_symlinks=True)\n\n"
"Set extended attribute attribute on path to value.\n"
"path may be either a string or an open file descriptor.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, setxattr will modify the symbolic link itself instead of the file\n"
"  the link points to.";
#endif


#if defined(HAVE_SPAWNV)
static int
sky_posix_spawnv(int mode, sky_object_t path, sky_object_t args)
{
    int         result;
    char        **argv;
    const char  *cpath;

    SKY_ASSET_BLOCK_BEGIN {
        cpath = sky_posix_fspath(path);

        if (!sky_object_isa(args, sky_list_type) &&
            !sky_object_isa(args, sky_tuple_type))
        {
            sky_error_raise_string(sky_TypeError,
                                   "spawnv() arg 3 must be a tuple or list");
        }
        if (!sky_object_len(args)) {
            sky_error_raise_string(sky_ValueError,
                                   "spawnv() arg 3 must not be empty");
        }

        argv = sky_posix_argv(args);

        if (_OLD_P_OVERLAY == mode) {
            mode = _P_OVERLAY;
        }

        result = spawnv(mode, cpath, argv);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_spawnv_doc[] =
"spawnv(mode, path, args)\n\n"
"Execute the program 'path' in a new process.\n\n"
"    mode: mode of process creation\n"
"    path: path of executable file\n"
"    args: tuple or list of strings";

static int
sky_posix_spawnve(
                int             mode,
                sky_object_t    path,
                sky_object_t    args,
                sky_object_t    env)
{
    int         result;
    char        **argv, **envp;
    const char  *cpath;

    SKY_ASSET_BLOCK_BEGIN {
        cpath = sky_posix_fspath(path);

        if (!sky_object_isa(args, sky_list_type) &&
            !sky_object_isa(args, sky_tuple_type))
        {
            sky_error_raise_string(sky_TypeError,
                                   "spawnve() arg 3 must be a tuple or list");
        }
        if (!sky_object_len(args)) {
            sky_error_raise_string(sky_ValueError,
                                   "spawnve() arg 3 must not be empty");
        }

        argv = sky_posix_argv(args);
        envp = sky_posix_envp(env);

        if (_OLD_P_OVERLAY == mode) {
            mode = _P_OVERLAY;
        }

        result = spawnve(mode, cpath, argv, envp);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_spawnve_doc[] =
"spawnve(mode, path, args, env)\n\n"
"Execute the program 'path' in a new process.\n\n"
"    mode: omde of process creation\n"
"    path: path of executable file\n"
"    args: tuple or list of arguments\n"
"    env: dictionary of strings mapping to strings";
#endif


static sky_object_t
sky_posix_stat(
                sky_object_t    path,
                sky_object_t    dir_fd,
                sky_bool_t      follow_symlinks)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    sky_object_t        result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#if defined(HAVE_FSTATAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        result = sky_posix_stat_common("stat",
                                       &cpath,
                                       fd,
                                       follow_symlinks);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_stat_doc[] =
"stat(path, *, dir_fd=None, follow_symlinks=True) -> stat result\n\n"
"Perform a stat system call on the given path.\n"
"\n"
"path may be specified as either a string or as an open file descriptor.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be supported on your platform; if it is unavailable, using\n"
"it will raise a NotImplementedError.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"link, stat will examine the symbolic link itself instead of the file the\n"
"link points to.\n"
"It is an error to use dir_fd or follow_symlinks when specifying path as\n"
"an open file descriptor.";


static sky_object_t
sky_posix_stat_float_times(sky_object_t newval)
{
    if (sky_object_isnull(newval)) {
        return (sky_posix_stat_float_times_flag ? sky_True : sky_False);
    }
    sky_posix_stat_float_times_flag = sky_object_bool(newval);
    return sky_None;
}

static const char
sky_posix_stat_float_times_doc[] =
"stat_float_times([newval]) -> oldval\n\n"
"Determine whether os.[lf]stat represents time stamps as float objects.\n"
"If newval is True, future calls to stat() return floats, if it is False,\n"
"future calls return ints. \n"
"If newval is omitted, return the current setting.\n";


#if defined(HAVE_STATVFS)
static sky_object_t
sky_posix_statvfs(sky_object_t path)
{
    int                 result;
    struct statvfs      st;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
#if defined(HAVE_FSTATVFS)
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#else
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#endif

        if (-1 == cpath.fd) {
            result = statvfs(cpath.bytes, &st);
        }
        else {
#if defined(HAVE_FSTATVFS)
            result = fstatvfs(cpath.fd, &st);
#else
            result = -1;
#endif
        }
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_posix_statvfs_create(&st);
}

static const char sky_posix_statvfs_doc[] =
"statvfs(path)\n\n"
"Perform a statvfs system call on the given path.\n\n"
"path may always be specified as a string.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailable, using it raises an exception.";
#endif


static sky_string_t
sky_posix_strerror(int code)
{
    const char  *message;

    if (!(message = strerror(code))) {
        sky_error_raise_string(sky_ValueError,
                               "strerror() argument out of range");
    }

    return sky_codec_decode(sky_codec_locale,
                            NULL,
                            SKY_STRING_LITERAL("surrogateescape"),
                            NULL,
                            sky_bytes_createwithbytes(message,
                                                      strlen(message),
                                                      NULL));
}

static const char sky_posix_strerror_doc[] =
"strerror(code) -> string\n\n"
"Translate an error code to a message string.";


static int
sky_posix_symlink(
                sky_object_t    src,
                sky_object_t    dst,
    SKY_UNUSED  sky_bool_t      target_is_directory,
                sky_object_t    dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cdst, csrc;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&csrc, src, SKY_FALSE);
        sky_posix_path_convert(&cdst, dst, SKY_FALSE);

#if defined(HAVE_SYMLINKAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = symlink(csrc.bytes, cdst.bytes);
        }
        else {
#if defined(HAVE_SYMLINKAT)
            result = symlinkat(csrc.bytes, fd, cdst.bytes);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_symlink_doc[] =
"symlink(src, dst, target_is_directory=False, *, dir_fd=None)\n\n"
"Create a symbolic link pointing to src named dst.\n\n"
"target_is_directory is required on Windows if the target is to be\n"
"  interpreted as a directory.  (On Windows, symlink requires\n"
"  Windows 6.0 or greater, and raises a NotImplementedError otherwise.)\n"
"  target_is_directory is ignored on non-Windows platforms.\n\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";

    
#if defined(HAVE_SYSCONF)
static long
sky_posix_sysconf(sky_object_t name)
{
    int     cname;
    long    result;

    cname = sky_posix_constant_lookup(&sky_posix_sysconf_table,
                                             name);

    errno = 0;
    result = sysconf(cname);
    if (-1 == result && errno) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return result;
}

static const char sky_posix_sysconf_doc[] =
"sysconf(name) -> integer\n\n"
"Return an integer-valued system configuration variable.";
#endif


static int
sky_posix_system(sky_object_t command)
{
    int                 result;
    sky_posix_path_t    ccommand;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&ccommand, command, SKY_FALSE);
        result = system(ccommand.bytes);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_system_doc[] =
"system(command) -> exit_status\n\n"
"Execute the command (a string) in a subshell.";


#if defined(TIOCGWINSZ)
static sky_type_t sky_posix_terminal_size_type;

static const char sky_posix_terminal_size_doc[] =
"A tuple of (columns, lines) for holding terminal window size";

static sky_struct_sequence_field_t sky_posix_terminal_size_fields[] = {
    { "columns",    "width of the terminal window in characters",   0 },
    { "lines",      "height of the terminal window in characters",  0 },
};
static const size_t sky_posix_terminal_size_nfields =
        sizeof(sky_posix_terminal_size_fields) /
        sizeof(sky_posix_terminal_size_fields[0]);

static sky_object_t
sky_posix_get_terminal_size(sky_object_t fd_object)
{
    int             fd;
    sky_object_t    values[2];
    struct winsize  winsz;

    fd = (sky_object_isnull(fd_object) ? fileno(stdout)
                                       : sky_integer_value(fd_object,
                                                           INT_MIN, INT_MAX,
                                                           NULL));

    if (ioctl(fd, TIOCGWINSZ, &winsz) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    values[0] = sky_integer_create(winsz.ws_col);
    values[1] = sky_integer_create(winsz.ws_row);

    return sky_struct_sequence_create(sky_posix_terminal_size_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}

static const char sky_posix_get_terminal_size_doc[] =
"Return the size of the terminal window as (columns, lines).\n\n"
"The optional argument fd (default standard output) specifies\n"
"which file descriptor should be queried.\n\n"
"If the file descriptor is not connected to a terminal, an OSError\n"
"is thrown.\n\n"
"This function will only be defined if an implementation is\n"
"available for this system.\n\n"
"shutil.get_terminal_size is the high-level function which should \n"
"normally be used, os.get_terminal_size is the low-level implementation.";
#endif


static sky_type_t sky_posix_times_result_type;

static const char sky_posix_times_result_doc[] =
"times_result: Result from os.times().\n\n"
"This object may be accessed either as a tuple of\n"
"  (user, system, children_user, children_system, elapsed),\n"
"or via the attributes user, system, children_user, children_system,\n"
"and elapsed.\n\n"
"See os.times for more information.";

static sky_struct_sequence_field_t sky_posix_times_result_fields[] = {
    { "user",               "user time", 0 },
    { "system",             "system time", 0 },
    { "children_user",      "user time of children", 0 },
    { "children_system",    "system time of children", 0 },
    { "elapsed",            "elapsed time since an abitrary point in the past", 0 },
};
static const size_t sky_posix_times_result_nfields =
        sizeof(sky_posix_times_result_fields) /
        sizeof(sky_posix_times_result_fields[0]);

static sky_object_t
sky_posix_times(void)
{
    clock_t         c;
    struct tms      t;
    sky_object_t    values[5];

    if ((c = times(&t)) == (clock_t)-1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    values[0] = sky_float_create((double)t.tms_utime /
                                 sky_posix_ticks_per_second);
    values[1] = sky_float_create((double)t.tms_stime /
                                 sky_posix_ticks_per_second);
    values[2] = sky_float_create((double)t.tms_cutime /
                                 sky_posix_ticks_per_second);
    values[3] = sky_float_create((double)t.tms_cstime /
                                 sky_posix_ticks_per_second);
    values[4] = sky_float_create((double)c /
                                 sky_posix_ticks_per_second);

    return sky_struct_sequence_create(sky_posix_times_result_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}

static const char sky_posix_times_doc[] =
"times() -> times_result\n\n"
"Return an object containing floating point numbers indicating process\n"
"times.  The object behaves like a named tuple with these fields:\n"
"  (utime, stime, cutime, cstime, elapsed_time)";


static int
sky_posix_truncate(sky_object_t path, off_t length)
{
    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
        if (-1 == cpath.fd) {
            result = truncate(cpath.bytes, length);
        }
        else {
            result = ftruncate(cpath.fd, length);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_truncate_doc[] =
"truncate(path, length)\n\n"
"Truncate the file given by path to length bytes.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailable, using it raises an exception.";


static sky_string_t
sky_posix_ttyname(int fd)
{
    char    *name;

    if (!(name = ttyname(fd))) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_posix_fsname(name, -1);
}

static const char sky_posix_ttyname_doc[] =
"ttyname(fd) -> string\n\n"
"Return the name of the terminal device connected to 'fd'.";


static sky_type_t sky_posix_uname_result_type;

static const char sky_posix_uname_result_doc[] =
"uname_result: Result from os.uname().\n\n"
"This object may be access either as a tuple of\n"
"  (sysname, nodename, release, version, machine),\n"
"or via the attributes sysname, nodename, release, version, and machine.\n\n"
"See os.uname for more information.";

static sky_struct_sequence_field_t sky_posix_uname_result_fields[] = {
    { "sysname",    "operating system name", 0 },
    { "nodename",   "name of machine on network (implementation-defined)", 0 },
    { "release",    "operating system release", 0 },
    { "version",    "operating system version", 0 },
    { "machine",    "hardware identifier", 0 },
};
static const size_t sky_posix_uname_result_nfields =
        sizeof(sky_posix_uname_result_fields) /
        sizeof(sky_posix_uname_result_fields[0]);

static sky_object_t
sky_posix_uname(void)
{
    sky_object_t    values[5];
    struct utsname  u;

    if (uname(&u) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    values[0] = sky_posix_fsname(u.sysname, -1);
    values[1] = sky_posix_fsname(u.nodename, -1);
    values[2] = sky_posix_fsname(u.release, -1);
    values[3] = sky_posix_fsname(u.version, -1);
    values[4] = sky_posix_fsname(u.machine, -1);

    return sky_struct_sequence_create(sky_posix_uname_result_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}

static const char sky_posix_uname_doc[] =
"uname() -> uname_result\n\n"
"Return an object identifying the current operating system.\n"
"The object behaves like a numed tuple with the following fields:\n"
"  (sysname, nodename, release, version, machine)";


static int
sky_posix_unlink(sky_object_t path, sky_object_t dir_fd)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_FALSE);
#if defined(HAVE_UNLINKAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("unlink", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("unlink", fd, cpath.fd);

        if (SKY_POSIX_DIR_FD_DEFAULT == fd) {
            result = unlink(cpath.bytes);
        }
        else {
#if defined(HAVE_UNLINKAT)
            result = unlinkat(fd, cpath.bytes, 0);
#else
            result = -1;
#endif
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_unlink_doc[] =
"unlink(path, *, dir_fd=None)\n\n"
"Remove a file (same as remove()).\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";

static const char sky_posix_remove_doc[] =
"remove(path, *, dir_fd=None)\n\n"
"Remove a file (same as unlink()).\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"dir_fd may not be implemented on your platform.\n"
"  If it is unavailable, using it will raise a NotImplementedError.";


static void
sky_posix_unsetenv(sky_object_t key)
{
    sky_posix_path_t    ckey;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&ckey, key, SKY_FALSE);

#if !defined(HAVE_SETENV)
        sky_spinlock_lock(&sky_posix_putenv_spinlock);
        sky_asset_save(&sky_posix_putenv_spinlock,
                       (sky_free_t)sky_spinlock_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);
#endif

        if (unsetenv(ckey.bytes) != 0) {
            sky_error_raise_errno(sky_OSError, errno);
        }

#if !defined(HAVE_SETENV)
        sky_dict_delete(sky_posix_putenv_dict, key);
#endif
    } SKY_ASSET_BLOCK_END;
}

static const char sky_posix_unsetenv_doc[] =
"unsetenv(key)\n\n"
"Delete an environment variable.";


static sky_bytes_t
sky_posix_urandom(ssize_t n)
{
    void        *raw_bytes;
    sky_bytes_t bytes;

    if (n < 0) {
        sky_error_raise_string(sky_ValueError,
                               "negative argument not allowed");
    }
    if (!n) {
        return sky_bytes_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        raw_bytes = sky_asset_malloc(n, SKY_ASSET_CLEANUP_ON_ERROR);
        sky_util_randombytes(raw_bytes, n);
        bytes = sky_bytes_createwithbytes(raw_bytes, n, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char sky_posix_urandom_doc[] =
"urandom(n) -> bytes\n\n"
"Return n random bytes suitable for cryptographic use.";


static int
sky_posix_utime(
                sky_object_t    path,
                sky_tuple_t     times,
                sky_tuple_t     ns,
                sky_object_t    dir_fd,
                sky_bool_t      follow_symlinks)
{
    int fd = SKY_POSIX_DIR_FD_DEFAULT;

    int                 result;
    sky_time_t          atime, mtime;
    struct timeval      timevals[2];
#if defined(HAVE_UTIMENSAT)
    struct timespec     timespecs[2];
#endif
    sky_posix_path_t    cpath;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_path_convert(&cpath, path, SKY_TRUE);
#if defined(HAVE_UTIMENSAT) || defined(HAVE_FUTIMESAT)
        fd = sky_posix_dir_fd_convert(dir_fd);
#else
        sky_posix_dir_fd_unavailable(dir_fd);
#endif

        sky_posix_validate_path_and_dir_fd("utime", &cpath, fd);
        sky_posix_validate_dir_fd_and_fd("utime", fd, cpath.fd);
        sky_posix_validate_fd_and_follow_symlinks("utime", cpath.fd, follow_symlinks);
#if !defined(HAVE_UTIMENSAT) && !defined(HAVE_LUTIMES)
        sky_posix_validate_follow_symlinks("utime", follow_symlinks);
#endif
#if !defined(HAVE_UTIMENSAT)
        sky_posix_validate_dir_fd_and_follow_symlinks("utime", fd, follow_symlinks);
#endif

        if (!sky_object_isnull(times)) {
            if (!sky_object_isnull(ns)) {
                sky_error_raise_string(
                        sky_ValueError,
                        "utime: you may specify either 'times' or 'ns' "
                        "but not both");
            }
            if (sky_tuple_len(times) != 2) {
                sky_error_raise_string(
                        sky_TypeError,
                        "utime: 'times' must be either a tuple of two ints "
                        "or None");
            }
            atime = sky_time_fromobject(sky_tuple_get(times, 0));
            mtime = sky_time_fromobject(sky_tuple_get(times, 1));
        }
        else if (!sky_object_isnull(ns)) {
            if (sky_tuple_len(ns) != 2) {
                sky_error_raise_string(
                        sky_TypeError,
                        "utime: 'ns' must be a tuple of two ints");
            }
            atime = sky_integer_value(sky_tuple_get(times, 0),
                                      0,
                                      UINT64_MAX,
                                      NULL);
            mtime = sky_integer_value(sky_tuple_get(times, 1),
                                      0,
                                      UINT64_MAX,
                                      NULL);
        }
        else {
            /* Neither times nor ns - use current time */
            atime = mtime = sky_time_current();
        }

        if (!follow_symlinks && SKY_POSIX_DIR_FD_DEFAULT == fd) {
#if defined(HAVE_UTIMENSAT)
            timespecs[0] = sky_time_totimespec(atime);
            timespecs[1] = sky_time_totimespec(mtime);
            result = utimensat(SKY_POSIX_DIR_FD_DEFAULT,
                               cpath.bytes,
                               timespecs,
                               AT_SYMLINK_NOFOLLOW);
#else
            sky_error_fatal("unreachable");
#endif
        }
        else if (SKY_POSIX_DIR_FD_DEFAULT != fd || !follow_symlinks) {
#if defined(HAVE_UTIMENSAT)
            timespecs[0] = sky_time_totimespec(atime);
            timespecs[1] = sky_time_totimespec(mtime);
            result = utimensat(fd,
                               cpath.bytes,
                               timespecs,
                               (follow_symlinks ? 0 : AT_SYMLINK_NOFOLLOW));
#elif defined(HAVE_FUTIMESAT)
            timevals[0] = sky_time_totimeval(atime);
            timevals[1] = sky_time_totimeval(mtime);
            result = futimesat(fd, cpath.bytes, timevals);
#else
            sky_error_fatal("unreachable");
#endif
        }
        else {
            timevals[0] = sky_time_totimeval(atime);
            timevals[1] = sky_time_totimeval(mtime);
            if (cpath.fd >= 0) {
                result = futimes(cpath.fd, timevals);
            }
            else {
                result = utimes(cpath.bytes, timevals);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_utime_doc[] =
"utime(path, times=None, *, ns=None, dir_fd=None, follow_symlinks=True)\n\n"
"Set the access and modified time of path.\n"
"\n"
"path may always be specified as a string.\n"
"On some platforms, path may also be specified as an open file descriptor.\n"
"  If this functionality is unavailable, using it raises an exception.\n"
"If times is not None, it must be a tuple (atime, mtime);\n"
"  atime and mtime should be expressed as float seconds since the epoch.\n"
"If ns is not None, it must be a tuple (atime_ns, mtime_ns);\n"
"  atime_ns and mtime_ns should be expressed as integer nanoseconds\n"
"  since the epoch.\n"
"If both times and ns are None, utime uses the current time.\n"
"Specifying tuples for both times and ns is an error.\n"
"\n"
"If dir_fd is not None, it should be a file descriptor open to a directory,\n"
"  and path should be relative; path will then be relative to that directory.\n"
"If follow_symlinks is False, and the last element of the path is a symbolic\n"
"  link, utime will modify the symbolic link itself instead of the file the\n"
"  link points to.\n"
"It is an error to use dir_fd or follow_symlinks when specifying path\n"
"  as an open file descriptor.\n"
"dir_fd and follow_symlinks may not be available on your platform.\n"
"  If they are unavailable, using them will raise a NotImplementedError.";


static sky_tuple_t
sky_posix_wait(void)
{
    int     status;
    pid_t   pid;

    if ((pid = wait(&status)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_tuple_pack(2, sky_integer_create(pid),
                             sky_integer_create(status));
}

static const char sky_posix_wait_doc[] =
"wait() -> (pid, status)\n\n"
"Wait for completion of a child process.";


static sky_object_t
sky_posix_wait_result(pid_t pid, int status, struct rusage *r)
{
    if (-1 == pid) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    sky_module_import(SKY_STRING_LITERAL("resource"));
    return sky_tuple_pack(3, sky_integer_create(pid),
                             sky_integer_create(status),
                             sky_resource_struct_rusage_create(r));
}

static sky_object_t
sky_posix_wait3(int options)
{
    int             status;
    pid_t           pid;
    struct rusage   r;

    pid = wait3(&status, options, &r);
    return sky_posix_wait_result(pid, status, &r);
}

static const char sky_posix_wait3_doc[] =
"wait3(options) -> (pid, status, rusage)\n\n"
"Wait for completion of a child process.";


static sky_object_t
sky_posix_wait4(pid_t pid, int options)
{
    int             status;
    struct rusage   r;

    pid = wait4(pid, &status, options, &r);
    return sky_posix_wait_result(pid, status, &r);
}

static const char sky_posix_wait4_doc[] =
"wait4(pid, options) -> (pid, status, rusage)\n\n"
"Wait for completion of a given child process.";


#if defined(HAVE_WAITID)
static sky_type_t sky_posix_waitid_result_type;

static const char sky_posix_waitid_result_doc[] =
"waitid_result: Result from waitid.\n\n"
"This object may be accessed either as a tuple of\n"
"  (si_pid, si_uid, si_signo, si_status, si_code),\n"
"or via the attributes si_pid, si_uid, and so on.\n\n"
"See os.waitid for more information.";

static sky_struct_sequence_field_t sky_posix_waitid_result_fields[] = {
    { "si_pid",     NULL, 0 },
    { "si_uid",     NULL, 0 },
    { "si_signo",   NULL, 0 },
    { "si_status",  NULL, 0 },
    { "si_code",    NULL, 0 },
};
static const size_t sky_posix_waitid_result_nfields =
        sizeof(sky_posix_waitid_result_fields) /
        sizeof(sky_posix_waitid_result_fields[0]);


static sky_object_t
sky_posix_waitid(idtype_t idtype, pid_t id, int options)
{
    siginfo_t       si;
    sky_object_t    values[5];

    si.si_pid = 0;
    if (waitid(idtype, id, &si, options) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    if (!si.si_pid) {
        return sky_None;
    }

    values[0] = sky_integer_create(si.si_pid);
    values[1] = sky_integer_createfromunsigned(si.si_uid);
    values[2] = sky_integer_create(si.si_signo);
    values[3] = sky_integer_create(si.si_status);
    values[4] = sky_integer_create(si.si_code);
    return sky_struct_sequence_create(sky_posix_waitid_result_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}

static const char sky_posix_waitid_doc[] =
"waitid(idtype, id, options) -> waitid_result\n\n"
"Wait for the completion of one or more child processes.\n\n"
"idtype can be P_PID, P_PGID or P_ALL.\n"
"id specifies the pid to wait on.\n"
"options is constructed from the ORing of one or more of WEXITED, WSTOPPED\n"
"or WCONTINUED and additionally may be ORed with WNOHANG or WNOWAIT.\n"
"Returns either waitid_result or None if WNOHANG is specified and there are\n"
"no children in a waitable state.";
#endif


static sky_tuple_t
sky_posix_waitpid(pid_t pid, int options)
{
    int     status;
    pid_t   result;

    if ((result = waitpid(pid, &status, options)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return sky_tuple_pack(2, sky_integer_create(result),
                             sky_integer_create(status));
}

static const char sky_posix_waitpid_doc[] =
"waitpid(pid, options) -> (pid, status)\n\n"
"Wait for completion of a given child process.";


static ssize_t
sky_posix_write(int fd, sky_object_t data)
{
    ssize_t         result;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = write(fd, buffer.buf, buffer.len);
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_write_doc[] =
"write(fd, data) -> byteswritten\n\n"
"Write bytes to a file descriptor.";


static ssize_t
sky_posix_writev(int fd, sky_object_t buffers)
{
    ssize_t         result;
    struct iovec    *iov;

    SKY_ASSET_BLOCK_BEGIN {
        sky_posix_iov(&iov, buffers, SKY_BUFFER_FLAG_SIMPLE);
        if ((result = writev(fd, iov, sky_object_len(buffers))) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_posix_writev_doc[] =
"writev(fd, buffers) -> byteswritten\n\n"
"Write the contents of *buffers* to file descriptor *fd*. *buffers*\n"
"must be a sequence of bytes-like objects.\n\n"
"writev writes the contents of each object to the file descriptor\n"
"and returns the total number of bytes written.";


static sky_bool_t
sky_posix_WCOREDUMP(int status)
{
    return (WCOREDUMP(status) ? SKY_TRUE : SKY_FALSE);
}

static const char sky_posix_WCOREDUMP_doc[] =
"WCOREDUMP(status) -> bool\n\n"
"Return True if the process returning 'status' was dumped to a core file.";


static int
sky_posix_WEXITSTATUS(int status)
{
    return WEXITSTATUS(status);
}

static const char sky_posix_WEXITSTATUS_doc[] =
"WEXITSTATUS(status) -> integer\n\n"
"Return the process return code from 'status'.";


static sky_bool_t
sky_posix_WIFCONTINUED(int status)
{
    return (WIFCONTINUED(status) ? SKY_TRUE : SKY_FALSE);
}

static const char sky_posix_WIFCONTINUED_doc[] =
"WIFCONTINUED(status) -> bool\n\n"
"Return True if the process returning 'status' was continued from a\n"
"job control stop.";


static sky_bool_t
sky_posix_WIFEXITED(int status)
{
    return (WIFEXITED(status) ? SKY_TRUE : SKY_FALSE);
}

static const char sky_posix_WIFEXITED_doc[] =
"WIFEXITED(status) -> bool\n\n"
"Return true if the process returning 'status' exited using the exit()\n"
"system call.";


static sky_bool_t
sky_posix_WIFSIGNALED(int status)
{
    return (WIFSIGNALED(status) ? SKY_TRUE : SKY_FALSE);
}

static const char sky_posix_WIFSIGNALED_doc[] =
"WIFSIGNALED(status) -> bool\n\n"
"Return True if the process returning 'status' was terminated by a signal.";


static sky_bool_t
sky_posix_WIFSTOPPED(int status)
{
    return (WIFSTOPPED(status) ? SKY_TRUE : SKY_FALSE);
}

static const char sky_posix_WIFSTOPPED_doc[] =
"WIFSTOPPED(status) -> bool\n\n"
"Return True if the process returning 'status' was stopped.";


static int
sky_posix_WSTOPSIG(int status)
{
    return WSTOPSIG(status);
}

static const char sky_posix_WSTOPSIG_doc[] =
"WSTOPSIG(status) -> integer\n\n"
"Return the signal that stopped the process that provided\n"
"the 'status' value.";


static int
sky_posix_WTERMSIG(int status)
{
    return WTERMSIG(status);
}

static const char sky_posix_WTERMSIG_doc[] =
"WTERMSIG(status) -> integer\n\n"
"Return the signal that terminated the process that provided the 'status'\n"
"value.";


/* Doc strings for system calls not requiring wrappers */

static const char sky_posix_abort_doc[] =
"abort() -> does not return!\n\n"
"Abort the interpreter immediately.  This 'dumps core' or otherwise fails\n"
"in the hardest way possible on the hosting operating system.";

static const char sky_posix_close_doc[] =
"close(fd)\n\n"
"Close a file descriptor (for low level IO).";

static const char sky_posix_device_encoding_doc[] =
"device_encoding(fd) -> str\n\n"
"Return a string describing the encoding of the device\n"
"if the output is a terminal; else return None.";

static const char sky_posix_dup_doc[] =
"dup(fd) -> fd2\n\n"
"Return a duplicate of a file descriptor.";

static const char sky_posix_dup2_doc[] =
"dup2(old_fd, new_fd)\n\n"
"Duplicate file descriptor.";

static const char sky_posix__exit_doc[] =
"_exit(status)\n\n"
"Exit to the system with specified status, without normal exit processing.";

#if defined(HAVE_FCHDIR)
static const char sky_posix_fchdir_doc[] =
"fchdir(fd)\n\n"
"Change to the directory of the given file descriptor.  fd must be\n"
"opened on a directory, not a file.  Equivalent to os.chdir(fd).";
#endif

#if defined(HAVE_FCHMOD)
static const char sky_posix_fchmod_doc[] =
"fchmod(fd)\n\n"
"Change the access permissions of the file given by file\n"
"descriptor fd.  Equivalent to os.chmod(fd, mode).";
#endif

#if defined(HAVE_FCHOWN)
static const char sky_posix_fchown_doc[] =
"fchown(fd, uid, gid)\n\n"
"Change the owner and group id of the file given by file descriptor\n"
"fd to the numeric uid and gid.  Equivalent to os.chown(fd, uid, gid).";
#endif

#if defined(HAVE_FDATASYNC) && \
    defined(_POSIX_SYNCHRONIZED_IO) && _POSIX_SYNCHRONIZED_IO > 0
static const char sky_posix_fdatasync_doc[] =
"fdatasync(fd)\n\n"
"force write of file with filedescriptor to disk."
" does not force update of metadata.";
#endif


static const char sky_posix_fork_doc[] =
"fork() -> pid\n\n"
"Fork a child process.\n"
"Return 0 to child process and PID of child to parent process.";

#if defined(HAVE_FORK1)
static const char sky_posix_fork1_doc[] =
"fork1() -> pid\n\n"
"Fork a child process with a single multiplexed (i.e., not bound) thread.\n\n"
"Return 0 to child process and PID of child to parent process.";
#endif

static const char sky_posix_fsync_doc[] =
"fsync(fd)\n\n"
"force write of file with filedescriptor to disk.";

static const char sky_posix_ftruncate_doc[] =
"ftruncate(fd, length)\n\n"
"Truncate a file to a specified length.";

static const char sky_posix_getegid_doc[] =
"getegid() -> egid\n\n"
"Return the current process's effective group id.";

static const char sky_posix_geteuid_doc[] =
"geteuid() -> euid\n\n"
"Return the current process's effective user id.";

static const char sky_posix_getgid_doc[] =
"getgid() -> gid\n\n"
"Return the current process's group id.";

static const char sky_posix_getpgid_doc[] =
"getpgid(pid) -> pgid\n\n"
"Call the system call getpgid().";

static const char sky_posix_getpgrp_doc[] =
"getpgrp() -> pgrp\n\n"
"Return the current process group id.";

static const char sky_posix_getpid_doc[] =
"getpid() -> pid\n\n"
"Return the current process id";

static const char sky_posix_getppid_doc[] =
"getppid() -> ppid\n\n"
"Return the parent's process id.  If the parent process has already exited,\n"
"Windows machines will still return its id; others systems will return the id\n"
"of the 'init' process (1).";

static const char sky_posix_getpriority_doc[] =
"getpriority(which, who) -> current_priority\n\n"
"Get the program scheduling priority.";

static const char sky_posix_getsid_doc[] =
"getsid(pid) -> sid\n\n"
"Call the system call getsid().";

static const char sky_posix_getuid_doc[] =
"getuid() -> uid\n\n"
"Return the current process's user id.";

static const char sky_posix_isatty_doc[] =
"isatty(fd) -> bool\n\n"
"Return True if the file descriptor 'fd' is an open file descriptor\n"
"connected to the slave end of a terminal.";

static const char sky_posix_kill_doc[] =
"kill(pid, sig)\n\n"
"Kill a process with a signal.";

static const char sky_posix_killpg_doc[] =
"killpg(pgid, sig)\n\n"
"Kill a process group with a signal.";

static const char sky_posix_lockf_doc[] =
"lockf(fd, cmd, len)\n\n"
"Apply, test or remove a POSIX lock on an open file descriptor.\n\n"
"fd is an open file descriptor.\n"
"cmd specifies the command to use - one of F_LOCK, F_TLOCK, F_ULOCK or\n"
"F_TEST.\n"
"len specifies the section of the file to lock.";

#if defined(HAVE_MAJOR)
static const char sky_posix_major_doc[] =
"major(device) -> major number\n"
"Extracts a device major number from a raw device number.";
#endif

#if defined(HAVE_MAKEDEV)
static const char sky_posix_makedev_doc[] =
"makedev(major, minor) -> device number\n\n"
"Composes a raw device number from the major and minor device numbers.";
#endif

#if defined(HAVE_MINOR)
static const char sky_posix_minor_doc[] =
"minor(device) -> minor number\n"
"Extracts a device minor number from a raw device number.";
#endif

#if defined(HAVE_PLOCK)
static const char sky_posix_plock_doc[] =
"plock(op)\n\n"
"Lock program segments into memory.";
#endif

#if defined(HAVE_POSIX_FADVISE)
static const char sky_posix_posix_fadvise_doc[] =
"posix_fadvise(fd, offset, len, advice)\n\n"
"Announces an intention to access data in a specific pattern thus allowing\n"
"the kernel to make optimizations.\n"
"The advice applies to the region of the file specified by fd starting at\n"
"offset and continuing for len bytes.\n"
"advice is one of POSIX_FADV_NORMAL, POSIX_FADV_SEQUENTIAL,\n"
"POSIX_FADV_RANDOM, POSIX_FADV_NOREUSE, POSIX_FADV_WILLNEED or\n"
"POSIX_FADV_DONTNEED.";
#endif

#if defined(HAVE_POSIX_FALLOCATE)
static const char sky_posix_posix_fallocate_doc[] =
"posix_fallocate(fd, offset, len)\n\n"
"Ensures that enough disk space is allocated for the file specified by fd\n"
"starting from offset and continuing for len bytes.";
#endif

#if defined(HAVE_SCHED_H)
static const char sky_posix_sched_get_priority_max_doc[] =
"sched_get_priority_max(policy)\n\n"
"Get the maximum scheduling priority for *policy*.";

static const char sky_posix_sched_get_priority_min_doc[] =
"sched_get_priority_min(policy)\n\n"
"Get the minimum scheduling priority for *policy*.";

#if defined(HAVE_SCHED_SETSCHEDULER)
static const char sky_posix_sched_getscheduler_doc[] =
"sched_getscheduler(pid)\n\n"
"Get the scheduling policy for the process with a PID of *pid*.\n"
"Passing a PID of 0 returns the scheduling policy for the calling process.";
#endif

#if defined(HAVE_SCHED_YIELD)
static const char sky_posix_sched_yield_doc[] =
"sched_yield()\n\n"
"Voluntarily relinquish the CPU.";
#endif
#endif

static const char sky_posix_setegid_doc[] =
"setegid(gid)\n\n"
"Set the current process's effective group id.";

static const char sky_posix_seteuid_doc[] =
"seteuid(uid)\n\n"
"Set the current process's effective user id.";

static const char sky_posix_setgid_doc[] =
"setgid(gid)\n\n"
"Set the current process's group id.";

static const char sky_posix_setpgid_doc[] =
"setpgid(pid, pgrp)\n\n"
"Call the system call setpgid().";

static const char sky_posix_setpgrp_doc[] =
"setpgrp()\n\n"
"Make this process the process group leader.";

static const char sky_posix_setpriority_doc[] =
"setpriority(which, who, prio) -> None\n\n"
"Set program scheduling priority.";

static const char sky_posix_setregid_doc[] =
"setregid(rgid, egid)\n\n"
"Set the current process's real and effective group ids.";

#if defined(HAVE_SETRESGID)
static const char sky_posix_setresgid_doc[] =
"setresgid(rgid, egid, sgid)\n\n"
"Set the current process's real, effective, and saved group ids.";
#endif

#if defined(HAVE_SETRESUID)
static const char sky_posix_setresuid_doc[] =
"setresuid(ruid, euid, suid)\n\n"
"Set the current process's real, effective, and saved user ids.";
#endif

static const char sky_posix_setreuid_doc[] =
"setreuid(ruid, euid)\n\n"
"Set the current process's real and effective user ids.";

static const char sky_posix_setsid_doc[] =
"setsid()\n\n"
"Call the system call setsid().";

static const char sky_posix_setuid_doc[] =
"setuid(uid)\n\n"
"Set the current process's user id.";

static const char sky_posix_sync_doc[] =
"sync()\n\n"
"Force write of everything to disk.";

static const char sky_posix_tcgetpgrp_doc[] =
"tcgetpgrp(fd) -> pgid\n\n"
"Return the process group associated with the terminal given by a fd.";

static const char sky_posix_tcsetpgrp_doc[] =
"tcsetpgrp(fd, pgid)\n\n"
"Set the process group associated with the terminal given by a fd.";

static const char sky_posix_umask_doc[] =
"umask(new_mask) -> old_mask\n\n"
"Set the current numeric umask and return the previous umask.";


static sky_dict_t
sky_posix_load_constants(sky_posix_constant_t *constants)
{
    size_t          i;
    sky_dict_t      dict;
    sky_string_t    name;
    sky_integer_t   value;

    dict = sky_dict_create();
    for (i = 0; constants[i].name; ++i) {
        name = sky_string_createfrombytes(constants[i].name,
                                          strlen(constants[i].name),
                                          NULL,
                                          NULL);
        value = sky_integer_create(constants[i].value);
        sky_dict_setitem(dict, name, value);
    }

    return dict;
}


static sky_bool_t
sky_posix_constant_table_compare(sky_hashtable_item_t *item, void *key)
{
    sky_posix_constant_t *constant = (sky_posix_constant_t *)item;

    return (strcmp(constant->name, key) == 0 ? SKY_TRUE : SKY_FALSE);
}


static void
sky_posix_load_constant_table(
                sky_hashtable_t *       table,
                sky_posix_constant_t *  constants)
{
    size_t      i;
    uintptr_t   hash;

    sky_hashtable_init(table, sky_posix_constant_table_compare);
    for (i = 0; constants[i].name; ++i) {
        hash = sky_util_hashbytes(constants[i].name,
                                  strlen(constants[i].name),
                                  0);
        if (sky_hashtable_insert(table,
                                 &(constants[i].hash),
                                 hash,
                                 (void *)constants[i].name) !=
            &(constants[i].hash))
        {
            sky_error_fatal("duplicate constant");
        }
    }
}


static sky_once_t sky_posix_once = SKY_ONCE_INITIALIZER;

static void
sky_posix_once_runner(SKY_UNUSED void *arg)
{
    sky_posix_load_constant_table(&sky_posix_pathconf_table,
                                  sky_posix_pathconf_consts);
    sky_posix_load_constant_table(&sky_posix_confstr_table,
                                  sky_posix_confstr_consts);
    sky_posix_load_constant_table(&sky_posix_sysconf_table,
                                  sky_posix_sysconf_consts);

#if defined(HAVE_SYSCONF) && defined(_SC_CLK_TCK)
    sky_posix_ticks_per_second = sysconf(_SC_CLK_TCK);
#elif defined(HZ)
    sky_posix_ticks_per_second = HZ;
#else
    sky_posix_ticks_per_second = 60;
#endif
}


void
skython_module_posix_initialize(sky_module_t module)
{
    static const char doc[] =
"This module provides access to operating system functionality that is\n"
"standardized by the C Standard and the POSIX standard (a thinly\n"
"disguised Unix interface).  Refer to the library manual and\n"
"corresponding Unix manual entries for more information on calls.";

    int             i;
    sky_list_t      have_functions;
    sky_function_t  f;

    sky_posix_stat_float_times_flag = SKY_TRUE;
    sky_once_run(&sky_posix_once, sky_posix_once_runner, NULL);

#if !defined(HAVE_SETENV)
    sky_spinlock_init(&sky_posix_putenv_spinlock);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_putenv_dict),
            sky_dict_create(),
            NULL);
#endif

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_error_validate_debug(NULL == sky_posix_stat_result_type);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_stat_result_type),
            sky_struct_sequence_define(
                    "stat_result",
                    sky_posix_stat_result_doc,
                    sky_posix_stat_result_nfields,
                    sky_posix_stat_result_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "stat_result",
                       sky_posix_stat_result_type);

#if defined(HAVE_SCHED_SETPARAM) || defined(HAVE_SCHED_SETSCHEDULER)
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_sched_param_type),
            sky_struct_sequence_define(
                    "sched_param",
                    sky_posix_sched_param_doc,
                    sky_posix_sched_param_nfields,
                    sky_posix_sched_param_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "sched_param",
                       sky_posix_sched_param_type);

    sky_type_setmethodslotwithparameters(
            sky_posix_sched_param_type,
            "__new__",
            (sky_native_code_function_t)sky_posix_sched_param_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "sched_priority", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
#endif
                       
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_statvfs_result_type),
            sky_struct_sequence_define(
                    "statvfs_result",
                    sky_posix_statvfs_result_doc,
                    sky_posix_statvfs_result_nfields,
                    sky_posix_statvfs_result_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "statvfs_result",
                       sky_posix_statvfs_result_type);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_times_result_type),
            sky_struct_sequence_define(
                    "times_result",
                    sky_posix_times_result_doc,
                    sky_posix_times_result_nfields,
                    sky_posix_times_result_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "times_result",
                       sky_posix_times_result_type);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_uname_result_type),
            sky_struct_sequence_define(
                    "uname_result",
                    sky_posix_uname_result_doc,
                    sky_posix_uname_result_nfields,
                    sky_posix_uname_result_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "uname_result",
                       sky_posix_uname_result_type);

#if defined(TIOCGWINSZ)
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_terminal_size_type),
            sky_struct_sequence_define(
                    "terminal_size",
                    sky_posix_terminal_size_doc,
                    sky_posix_terminal_size_nfields,
                    sky_posix_terminal_size_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "terminal_size",
                       sky_posix_terminal_size_type);
#endif

#if defined(HAVE_WAITID)
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_posix_waitid_result_type),
            sky_struct_sequence_define(
                    "waitid_result",
                    sky_posix_waitid_result_doc,
                    sky_posix_waitid_result_nfields,
                    sky_posix_waitid_result_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "waitid_result",
                       sky_posix_waitid_result_type);
#endif

    have_functions = sky_list_create(NULL);
    for (i = 0; sky_posix_have_functions[i]; ++i) {
        sky_list_append(have_functions,
                        sky_string_createfrombytes(
                                sky_posix_have_functions[i],
                                strlen(sky_posix_have_functions[i]),
                                NULL,
                                NULL));
    }
    sky_module_setattr(module, "_have_functions", have_functions);

    sky_posix_addconstants(module);

    sky_module_setattr(module, "environ", sky_posix_environ());
    sky_module_setattr(module, "error", sky_OSError);

    sky_module_setattr(
            module,
            "pathconf_names",
            sky_posix_load_constants(sky_posix_pathconf_consts));
    sky_module_setattr(
            module,
            "confstr_names",
            sky_posix_load_constants(sky_posix_confstr_consts));
    sky_module_setattr(
            module,
            "sysconf_names",
            sky_posix_load_constants(sky_posix_sysconf_consts));

    sky_module_setattr(
            module,
            "abort",
            sky_function_createbuiltin(
                    "abort",
                    sky_posix_abort_doc,
                    (sky_native_code_function_t)abort,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "access",
            sky_function_createbuiltin(
                    "access",
                    sky_posix_access_doc,
                    (sky_native_code_function_t)sky_posix_access,
                    SKY_DATA_TYPE_SYSCALL_BOOL,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "mode", SKY_DATA_TYPE_INT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, NULL,
                    "effective_ids", SKY_DATA_TYPE_BOOL, sky_False,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "chdir",
            sky_function_createbuiltin(
                    "chdir",
                    sky_posix_chdir_doc,
                    (sky_native_code_function_t)sky_posix_chdir,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_CHFLAGS)
    sky_module_setattr(
            module,
            "chflags",
            sky_function_createbuiltin(
                    "chflags",
                    sky_posix_chflags_doc,
                    (sky_native_code_function_t)sky_posix_chflags,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_UINT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "chmod",
            sky_function_createbuiltin(
                    "chmod",
                    sky_posix_chmod_doc,
                    (sky_native_code_function_t)sky_posix_chmod,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "mode", SKY_DATA_TYPE_MODE_T, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "chown",
            sky_function_createbuiltin(
                    "chown",
                    sky_posix_chown_doc,
                    (sky_native_code_function_t)sky_posix_chown,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "uid", SKY_DATA_TYPE_UID_T, NULL,
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "chroot",
            sky_function_createbuiltin(
                    "chroot",
                    sky_posix_chroot_doc,
                    (sky_native_code_function_t)sky_posix_chroot,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "close",
            sky_function_createbuiltin(
                    "close",
                    sky_posix_close_doc,
                    (sky_native_code_function_t)close,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "closerange",
            sky_function_createbuiltin(
                    "closerange",
                    sky_posix_closerange_doc,
                    (sky_native_code_function_t)sky_posix_closerange,
                    SKY_DATA_TYPE_VOID,
                    "fd_low", SKY_DATA_TYPE_INT, NULL,
                    "fd_high", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "confstr",
            sky_function_createbuiltin(
                    "confstr",
                    sky_posix_confstr_doc,
                    (sky_native_code_function_t)sky_posix_confstr,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ctermid",
            sky_function_createbuiltin(
                    "ctermid",
                    sky_posix_ctermid_doc,
                    (sky_native_code_function_t)sky_posix_ctermid,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "device_encoding",
            sky_function_createbuiltin(
                    "device_encoding",
                    sky_posix_device_encoding_doc,
                    (sky_native_code_function_t)sky_file_device_encoding,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "dup",
            sky_function_createbuiltin(
                    "dup",
                    sky_posix_dup_doc,
                    (sky_native_code_function_t)dup,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "dup2",
            sky_function_createbuiltin(
                    "dup2",
                    sky_posix_dup2_doc,
                    (sky_native_code_function_t)dup2,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "old_fd", SKY_DATA_TYPE_FD, NULL,
                    "new_fd", SKY_DATA_TYPE_FD2, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "_exit",
            sky_function_createbuiltin(
                    "_exit",
                    sky_posix__exit_doc,
                    (sky_native_code_function_t)_exit,
                    SKY_DATA_TYPE_VOID,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "execv",
            sky_function_createbuiltin(
                    "execv",
                    sky_posix_execv_doc,
                    (sky_native_code_function_t)sky_posix_execv,
                    SKY_DATA_TYPE_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "args", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "execve",
            sky_function_createbuiltin(
                    "execve",
                    sky_posix_execve_doc,
                    (sky_native_code_function_t)sky_posix_execve,
                    SKY_DATA_TYPE_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "argv", SKY_DATA_TYPE_OBJECT, NULL,
                    "environment", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_FCHDIR)
    sky_module_setattr(
            module,
            "fchdir",
            sky_function_createbuiltin(
                    "fchdir",
                    sky_posix_fchdir_doc,
                    (sky_native_code_function_t)fchdir,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
#endif
#if defined(HAVE_FCHMOD)
    sky_module_setattr(
            module,
            "fchmod",
            sky_function_createbuiltin(
                    "fchmod",
                    sky_posix_fchmod_doc,
                    (sky_native_code_function_t)fchmod,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "mode", SKY_DATA_TYPE_MODE_T, NULL,
                    NULL));
#endif
#if defined(HAVE_FCHOWN)
    sky_module_setattr(
            module,
            "fchown",
            sky_function_createbuiltin(
                    "fchown",
                    sky_posix_fchown_doc,
                    (sky_native_code_function_t)fchown,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "uid", SKY_DATA_TYPE_UID_T, NULL,
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_FDATASYNC) && \
    defined(_POSIX_SYNCHRONIZED_IO) && _POSIX_SYNCHRONIZED_IO > 0
    sky_module_setattr(
            module,
            "fdatasync",
            sky_function_createbuiltin(
                    "fdatasync",
                    sky_posix_fdatasync_doc,
                    (sky_native_code_function_t)fdatasync,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "fork",
            sky_function_createbuiltin(
                    "fork",
                    sky_posix_fork_doc,
                    (sky_native_code_function_t)sky_system_fork,
                    SKY_DATA_TYPE_SYSCALL_PID_T,
                    NULL));
#if defined(HAVE_FORK1)
    sky_module_setattr(
            module,
            "fork1",
            sky_function_createbuiltin(
                    "fork1",
                    sky_posix_fork1_doc,
                    (sky_native_code_function_t)sky_system_fork1,
                    SKY_DATA_TYPE_SYSCALL_PID_T,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "forkpty",
            sky_function_createbuiltin(
                    "forkpty",
                    sky_posix_forkpty_doc,
                    (sky_native_code_function_t)sky_posix_forkpty,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(
            module,
            "fpathconf",
            sky_function_createbuiltin(
                    "fpathconf",
                    sky_posix_fpathconf_doc,
                    (sky_native_code_function_t)sky_posix_fpathconf,
                    SKY_DATA_TYPE_LONG,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "fstat",
            sky_function_createbuiltin(
                    "fstat",
                    sky_posix_fstat_doc,
                    (sky_native_code_function_t)sky_posix_fstat,
                    SKY_DATA_TYPE_OBJECT,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "fstatvfs",
            sky_function_createbuiltin(
                    "fstatvfs",
                    sky_posix_fstatvfs_doc,
                    (sky_native_code_function_t)sky_posix_fstatvfs,
                    SKY_DATA_TYPE_OBJECT,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "fsync",
            sky_function_createbuiltin(
                    "fsync",
                    sky_posix_fsync_doc,
                    (sky_native_code_function_t)fsync,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ftruncate",
            sky_function_createbuiltin(
                    "ftruncate",
                    sky_posix_ftruncate_doc,
                    (sky_native_code_function_t)ftruncate,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "length", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
#if defined(TIOCGWINSZ)
    sky_module_setattr(
            module,
            "get_terminal_size",
            sky_function_createbuiltin(
                    "get_terminal_size",
                    sky_posix_get_terminal_size_doc,
                    (sky_native_code_function_t)sky_posix_get_terminal_size,
                    SKY_DATA_TYPE_OBJECT,
                    "fd", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "getcwd",
            sky_function_createbuiltin(
                    "getcwd",
                    sky_posix_getcwd_doc,
                    (sky_native_code_function_t)sky_posix_getcwd,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "getcwdb",
            sky_function_createbuiltin(
                    "getcwdb",
                    sky_posix_getcwdb_doc,
                    (sky_native_code_function_t)sky_posix_getcwdb,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    NULL));
    sky_module_setattr(
            module,
            "getegid",
            sky_function_createbuiltin(
                    "getegid",
                    sky_posix_getegid_doc,
                    (sky_native_code_function_t)getegid,
                    SKY_DATA_TYPE_GID_T,
                    NULL));
    sky_module_setattr(
            module,
            "geteuid",
            sky_function_createbuiltin(
                    "geteuid",
                    sky_posix_geteuid_doc,
                    (sky_native_code_function_t)geteuid,
                    SKY_DATA_TYPE_UID_T,
                    NULL));
    sky_module_setattr(
            module,
            "getgid",
            sky_function_createbuiltin(
                    "getgid",
                    sky_posix_getgid_doc,
                    (sky_native_code_function_t)getgid,
                    SKY_DATA_TYPE_GID_T,
                    NULL));
    sky_module_setattr(
            module,
            "getgrouplist",
            sky_function_createbuiltin(
                    "getgrouplist",
                    sky_posix_getgrouplist_doc,
                    (sky_native_code_function_t)sky_posix_getgrouplist,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "user", SKY_DATA_TYPE_OBJECT_STRING, NULL,
#if defined(__APPLE__)
                    "group", SKY_DATA_TYPE_INT, NULL,
#else
                    "group", SKY_DATA_TYPE_GID_T, NULL,
#endif
                    NULL));
    sky_module_setattr(
            module,
            "getgroups",
            sky_function_createbuiltin(
                    "getgroups",
                    sky_posix_getgroups_doc,
                    (sky_native_code_function_t)sky_posix_getgroups,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    NULL));
    sky_module_setattr(
            module,
            "getloadavg",
            sky_function_createbuiltin(
                    "getloadavg",
                    sky_posix_getloadavg_doc,
                    (sky_native_code_function_t)sky_posix_getloadavg,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(
            module,
            "getlogin",
            sky_function_createbuiltin(
                    "getlogin",
                    sky_posix_getlogin_doc,
                    (sky_native_code_function_t)sky_posix_getlogin,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "getpid",
            sky_function_createbuiltin(
                    "getpid",
                    sky_posix_getpid_doc,
                    (sky_native_code_function_t)getpid,
                    SKY_DATA_TYPE_PID_T,
                    NULL));
    sky_module_setattr(
            module,
            "getpgid",
            sky_function_createbuiltin(
                    "getpgid",
                    sky_posix_getpgid_doc,
                    (sky_native_code_function_t)getpgid,
                    SKY_DATA_TYPE_PID_T,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getpgrp",
            sky_function_createbuiltin(
                    "getpgrp",
                    sky_posix_getpgrp_doc,
                    (sky_native_code_function_t)getpgrp,
                    SKY_DATA_TYPE_PID_T,
                    NULL));
    sky_module_setattr(
            module,
            "getppid",
            sky_function_createbuiltin(
                    "getppid",
                    sky_posix_getppid_doc,
                    (sky_native_code_function_t)getppid,
                    SKY_DATA_TYPE_PID_T,
                    NULL));
    sky_module_setattr(
            module,
            "getpriority",
            sky_function_createbuiltin(
                    "getpriority",
                    sky_posix_getpriority_doc,
                    (sky_native_code_function_t)getpriority,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "which", SKY_DATA_TYPE_INT, NULL,
                    "who", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#if defined(HAVE_GETRESGID)
    sky_module_setattr(
            module,
            "getresgid",
            sky_function_createbuiltin(
                    "getresgid",
                    sky_posix_getresgid_doc,
                    (sky_native_code_function_t)sky_posix_getresgid,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
#endif
#if defined(HAVE_GETRESUID)
    sky_module_setattr(
            module,
            "getresuid",
            sky_function_createbuiltin(
                    "getresuid",
                    sky_posix_getresuid_doc,
                    (sky_native_code_function_t)sky_posix_getresuid,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "getsid",
            sky_function_createbuiltin(
                    "getsid",
                    sky_posix_getsid_doc,
                    (sky_native_code_function_t)getsid,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getuid",
            sky_function_createbuiltin(
                    "getuid",
                    sky_posix_getuid_doc,
                    (sky_native_code_function_t)getuid,
                    SKY_DATA_TYPE_UID_T,
                    NULL));
#if defined(HAVE_SYS_XATTR_H)
    sky_module_setattr(
            module,
            "getxattr",
            sky_function_createbuiltin(
                    "getxattr",
                    sky_posix_getxattr_doc,
                    (sky_native_code_function_t)sky_posix_getxattr,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "attribute", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "initgroups",
            sky_function_createbuiltin(
                    "initgroups",
                    sky_posix_initgroups_doc,
                    (sky_native_code_function_t)sky_posix_initgroups,
                    SKY_DATA_TYPE_VOID,
                    "username", SKY_DATA_TYPE_OBJECT_STRING, NULL,
#if defined(__APPLE__)
                    "gid", SKY_DATA_TYPE_INT, NULL,
#else
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
#endif
                    NULL));
    sky_module_setattr(
            module,
            "isatty",
            sky_function_createbuiltin(
                    "isatty",
                    sky_posix_isatty_doc,
                    (sky_native_code_function_t)isatty,
                    SKY_DATA_TYPE_BOOL,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "kill",
            sky_function_createbuiltin(
                    "kill",
                    sky_posix_kill_doc,
                    (sky_native_code_function_t)kill,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "killpg",
            sky_function_createbuiltin(
                    "killpg",
                    sky_posix_killpg_doc,
                    (sky_native_code_function_t)killpg,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pgid", SKY_DATA_TYPE_PID_T, NULL,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#if defined(HAVE_LCHFLAGS)
    sky_module_setattr(
            module,
            "lchflags",
            sky_function_createbuiltin(
                    "lchflags",
                    sky_posix_lchflags_doc,
                    (sky_native_code_function_t)sky_posix_lchflags,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_UINT, NULL,
                    NULL));
#endif
#if defined(HAVE_LCHOWN)
    sky_module_setattr(
            module,
            "lchown",
            sky_function_createbuiltin(
                    "lchown",
                    sky_posix_lchown_doc,
                    (sky_native_code_function_t)sky_posix_lchown,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "uid", SKY_DATA_TYPE_UID_T, NULL,
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "link",
            sky_function_createbuiltin(
                    "link",
                    sky_posix_link_doc,
                    (sky_native_code_function_t)sky_posix_link,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "src", SKY_DATA_TYPE_OBJECT, NULL,
                    "dst", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "src_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "dst_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "listdir",
            sky_function_createbuiltin(
                    "listdir",
                    sky_posix_listdir_doc,
                    (sky_native_code_function_t)sky_posix_listdir,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "path", SKY_DATA_TYPE_OBJECT, SKY_STRING_LITERAL("."),
                    NULL));
    sky_module_setattr(
            module,
            "listxattr",
            sky_function_createbuiltin(
                    "listxattr",
                    sky_posix_listxattr_doc,
                    (sky_native_code_function_t)sky_posix_listxattr,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "path", SKY_DATA_TYPE_OBJECT, sky_None,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "lockf",
            sky_function_createbuiltin(
                    "lockf",
                    sky_posix_lockf_doc,
                    (sky_native_code_function_t)lockf,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "cmd", SKY_DATA_TYPE_INT, NULL,
                    "len", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "lseek",
            sky_function_createbuiltin(
                    "lseek",
                    sky_posix_lseek_doc,
                    (sky_native_code_function_t)sky_posix_lseek,
                    SKY_DATA_TYPE_SYSCALL_OFF_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "pos", SKY_DATA_TYPE_OFF_T, NULL,
                    "how", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "lstat",
            sky_function_createbuiltin(
                    "lstat",
                    sky_posix_lstat_doc,
                    (sky_native_code_function_t)sky_posix_lstat,
                    SKY_DATA_TYPE_OBJECT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
#if defined(HAVE_MAJOR)
    sky_module_setattr(
            module,
            "major",
            sky_function_createbuiltin(
                    "major",
                    sky_posix_major_doc,
                    (sky_native_code_function_t)major,
                    SKY_DATA_TYPE_INT,
                    "device", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
#if defined(HAVE_MAKEDEV)
    sky_module_setattr(
            "makedev",
            sky_function_createbuiltin(
                    "makedev",
                    sky_posix_makedev_doc,
                    (sky_native_code_function_t)makedev,
                    SKY_DATA_TYPE_INT,
                    "major", SKY_DATA_TYPE_INT, NULL,
                    "minor", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
#if defined(HAVE_MINOR)
    sky_module_setattr(
            module,
            "minor",
            sky_function_createbuiltin(
                    "minor",
                    sky_posix_minor_doc,
                    (sky_native_code_function_t)minor,
                    SKY_DATA_TYPE_INT,
                    "device", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "mkdir",
            sky_function_createbuiltin(
                    "mkdir",
                    sky_posix_mkdir_doc,
                    (sky_native_code_function_t)sky_posix_mkdir,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "mode", SKY_DATA_TYPE_INT, sky_integer_create(0777),
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "mkfifo",
            sky_function_createbuiltin(
                    "mkfifo",
                    sky_posix_mkfifo_doc,
                    (sky_native_code_function_t)sky_posix_mkfifo,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "mode", SKY_DATA_TYPE_MODE_T, sky_integer_create(0666),
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "mknod",
            sky_function_createbuiltin(
                    "mknod",
                    sky_posix_mknod_doc,
                    (sky_native_code_function_t)sky_posix_mknod,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "mode", SKY_DATA_TYPE_MODE_T, sky_integer_create(0666),
                    "device", SKY_DATA_TYPE_DEV_T, sky_integer_zero,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "nice",
            sky_function_createbuiltin(
                    "nice",
                    sky_posix_nice_doc,
                    (sky_native_code_function_t)sky_posix_nice,
                    SKY_DATA_TYPE_INT,
                    "inc", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "open",
            sky_function_createbuiltin(
                    "open",
                    sky_posix_open_doc,
                    (sky_native_code_function_t)sky_posix_open,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, NULL,
                    "mode", SKY_DATA_TYPE_INT, sky_integer_create(0777),
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
#if defined(HAVE_OPENPTY)
    sky_module_setattr(
            module,
            "openpty",
            sky_function_createbuiltin(
                    "openpty",
                    sky_posix_openpty_doc,
                    (sky_native_code_function_t)sky_posix_openpty,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "pathconf",
            sky_function_createbuiltin(
                    "pathconf",
                    sky_posix_pathconf_doc,
                    (sky_native_code_function_t)sky_posix_pathconf,
                    SKY_DATA_TYPE_LONG,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "pipe",
            sky_function_createbuiltin(
                    "pipe",
                    sky_posix_pipe_doc,
                    (sky_native_code_function_t)sky_posix_pipe,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
#if defined(HAVE_PIPE2)
    sky_module_setattr(
            module,
            "pipe2",
            sky_function_createbuiltin(
                    "pipe2",
                    sky_posix_pipe2_doc,
                    (sky_native_code_function_t)sky_posix_pipe2,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "flags", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
#if defined(HAVE_PLOCK)
    sky_module_setattr(
            module,
            "plock",
            sky_function_createbuiltin(
                    "plock",
                    sky_posix_plock_doc,
                    (sky_native_code_function_t)plock,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "op", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
#if defined(HAVE_POSIX_FADVISE)
    sky_module_setattr(
            module,
            "posix_fadvise",
            sky_function_createbuiltin(
                    "posix_fadvise",
                    sky_posix_posix_fadvise_doc,
                    (sky_native_code_function_t)posix_fadvise,
                    SKY_DATA_TYPE_ERRNO,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "offset", SKY_DATA_TYPE_OFF_T, NULL,
                    "len", SKY_DATA_TYPE_OFF_T, NULL,
                    "advice", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
#if defined(HAVE_POSIX_FALLOCATE)
    sky_module_setattr(
            module,
            "posix_fallocate",
            sky_function_createbuiltin(
                    "posix_fallocate",
                    sky_posix_posix_fallocate_doc,
                    (sky_native_code_function_t)posix_fallocate,
                    SKY_DATA_TYPE_ERRNO,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "offset", SKY_DATA_TYPE_OFF_T, NULL,
                    "len", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
#endif
#if defined(HAVE_PREAD)
    sky_module_setattr(
            module,
            "pread",
            sky_function_createbuiltin(
                    "pread",
                    sky_posix_pread_doc,
                    (sky_native_code_function_t)sky_posix_pread,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "size", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "offset", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "putenv",
            sky_function_createbuiltin(
                    "putenv",
                    sky_posix_putenv_doc,
                    (sky_native_code_function_t)sky_posix_putenv,
                    SKY_DATA_TYPE_VOID,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_PWRITE)
    sky_module_setattr(
            module,
            "pwrite",
            sky_function_createbuiltin(
                    "pwrite",
                    sky_posix_pwrite_doc,
                    (sky_native_code_function_t)sky_posix_pwrite,
                    SKY_DATA_TYPE_SSIZE_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "offset", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "read",
            sky_function_createbuiltin(
                    "read",
                    sky_posix_read_doc,
                    (sky_native_code_function_t)sky_posix_read,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "size", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "readlink",
            sky_function_createbuiltin(
                    "readlink",
                    sky_posix_readlink_doc,
                    (sky_native_code_function_t)sky_posix_readlink,
                    SKY_DATA_TYPE_OBJECT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
#if defined(HAVE_READV)
    sky_module_setattr(
            module,
            "readv",
            sky_function_createbuiltin(
                    "readv",
                    sky_posix_readv_doc,
                    (sky_native_code_function_t)sky_posix_readv,
                    SKY_DATA_TYPE_SSIZE_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "buffers", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "remove",
            sky_function_createbuiltin(
                    "remove",
                    sky_posix_remove_doc,
                    (sky_native_code_function_t)sky_posix_unlink,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
#if defined(HAVE_SYS_XATTR_H)
    sky_module_setattr(
            module,
            "removexattr",
            sky_function_createbuiltin(
                    "removexattr",
                    sky_posix_removexattr_doc,
                    (sky_native_code_function_t)sky_posix_removexattr,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "attribute", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "rename",
            sky_function_createbuiltin(
                    "rename",
                    sky_posix_rename_doc,
                    (sky_native_code_function_t)sky_posix_rename,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "src", SKY_DATA_TYPE_OBJECT, NULL,
                    "dst", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "src_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "dst_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "replace",
            sky_function_createbuiltin(
                    "replace",
                    sky_posix_replace_doc,
                    (sky_native_code_function_t)sky_posix_replace,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "src", SKY_DATA_TYPE_OBJECT, NULL,
                    "dst", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "src_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "dst_dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "rmdir",
            sky_function_createbuiltin(
                    "rmdir",
                    sky_posix_rmdir_doc,
                    (sky_native_code_function_t)sky_posix_rmdir,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
#if defined(HAVE_SCHED_H)
    sky_module_setattr(
            module,
            "sched_get_priority_max",
            sky_function_createbuiltin(
                    "sched_get_priority_max",
                    sky_posix_sched_get_priority_max_doc,
                    (sky_native_code_function_t)sched_get_priority_max,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "policy", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "sched_get_priority_min",
            sky_function_createbuiltin(
                    "sched_get_priority_min",
                    sky_posix_sched_get_priority_min_doc,
                    (sky_native_code_function_t)sched_get_priority_min,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "policy", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#if defined(HAVE_SCHED_SETAFFINITY)
    sky_module_setattr(
            module,
            "sched_getaffinity",
            sky_function_createbuiltin(
                    "sched_getaffinity",
                    sky_posix_sched_getaffinity_doc,
                    (sky_native_code_function_t)sky_posix_sched_getaffinity,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_SETPARAM)
    sky_module_setattr(
            module,
            "sched_getparam",
            sky_function_createbuiltin(
                    "sched_getparam",
                    sky_posix_sched_getparam_doc,
                    (sky_native_code_function_t)sky_posix_sched_getparam,
                    SKY_DATA_TYPE_OBJECT,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_SETSCHEDULER)
    sky_module_setattr(
            module,
            "sched_getscheduler",
            sky_function_createbuiltin(
                    "sched_getscheduler",
                    sky_posix_sched_getscheduler_doc,
                    (sky_native_code_function_t)sched_getscheduler,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_RR_GET_INTERVAL)
    sky_module_setattr(
            module,
            "sched_rr_get_interval",
            sky_function_createbuiltin(
                    "sched_rr_get_interval",
                    sky_posix_sched_rr_get_interval_doc,
                    (sky_native_code_function_t)sky_posix_sched_rr_get_interval,
                    SKY_DATA_TYPE_DOUBLE,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_SETAFFINITY)
    sky_module_setattr(
            module,
            "sched_setaffinity",
            sky_function_createbuiltin(
                    "sched_setaffinity",
                    sky_posix_sched_setaffinity_doc,
                    (sky_native_code_function_t)sky_posix_sched_setaffinity,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "cpu_set", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_SETPARAM)
    sky_module_setattr(
            module,
            "sched_setparam",
            sky_function_createbuiltin(
                    "sched_setparam",
                    sky_posix_sched_setparam_doc,
                    (sky_native_code_function_t)sky_posix_sched_setparam,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "param", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_SETSCHEDULER)
    sky_module_setattr(
            module,
            "sched_setscheduler",
            sky_function_createbuiltin(
                    "sched_setscheduler",
                    sky_posix_sched_setscheduler_doc,
                    (sky_native_code_function_t)sky_posix_sched_setscheduler,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "policy", SKY_DATA_TYPE_INT, NULL,
                    "param", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
#if defined(HAVE_SCHED_YIELD)
    sky_module_setattr(
            module,
            "sched_yield",
            sky_function_createbuiltin(
                    "sched_yield",
                    sky_posix_sched_yield_doc,
                    (sky_native_code_function_t)sched_yield,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    NULL));
#endif
#endif
#if defined(HAVE_SENDFILE)
    sky_module_setattr(
            module,
            "sendfile",
            sky_function_createbuiltin(
                    "sendfile",
                    sky_posix_sendfile_doc,
                    (sky_native_code_function_t)sky_posix_sendfile,
                    SKY_DATA_TYPE_SSIZE_T,
                    "out", SKY_DATA_TYPE_FD, NULL,
                    "in", SKY_DATA_TYPE_FD, NULL,
#if defined(linux)
                    "offset", SKY_DATA_TYPE_OBJECT_INTEGER | SKY_DATA_TYPE_OBJECT_OR_NONE, NULL,
#else
                    "offset", SKY_DATA_TYPE_OFF_T, NULL,
#endif
                    "count", SKY_DATA_TYPE_OFF_T, NULL,
#if defined(__APPLE__) || defined(__DragonFly__) || defined(__FreeBSD__)
                    "headers", SKY_DATA_TYPE_OBJECT, NULL,
                    "trailers", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, NULL,
#endif
                    NULL));
#endif
    sky_module_setattr(
            module,
            "setegid",
            sky_function_createbuiltin(
                    "setegid",
                    sky_posix_setegid_doc,
                    (sky_native_code_function_t)setegid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "seteuid",
            sky_function_createbuiltin(
                    "seteuid",
                    sky_posix_seteuid_doc,
                    (sky_native_code_function_t)seteuid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "uid", SKY_DATA_TYPE_UID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setgid",
            sky_function_createbuiltin(
                    "setgid",
                    sky_posix_setgid_doc,
                    (sky_native_code_function_t)setgid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "gid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setgroups",
            sky_function_createbuiltin(
                    "setgroups",
                    sky_posix_setgroups_doc,
                    (sky_native_code_function_t)sky_posix_setgroups,
                    SKY_DATA_TYPE_VOID,
                    "list", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setpgid",
            sky_function_createbuiltin(
                    "setpgid",
                    sky_posix_setpgid_doc,
                    (sky_native_code_function_t)setpgid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "pgrp", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setpgrp",
            sky_function_createbuiltin(
                    "setpgrp",
                    sky_posix_setpgrp_doc,
                    (sky_native_code_function_t)setpgrp,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "setpriority",
            sky_function_createbuiltin(
                    "setpriority",
                    sky_posix_setpriority_doc,
                    (sky_native_code_function_t)setpriority,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "which", SKY_DATA_TYPE_INT, NULL,
                    "who", SKY_DATA_TYPE_INT, NULL,
                    "prio", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setregid",
            sky_function_createbuiltin(
                    "setregid",
                    sky_posix_setregid_doc,
                    (sky_native_code_function_t)setregid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "rgid", SKY_DATA_TYPE_GID_T, NULL,
                    "egid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
#if defined(HAVE_SETRESGID)
    sky_module_setattr(
            module,
            "setresgid",
            sky_function_createbuiltin(
                    "setresgid",
                    sky_posix_setresgid_doc,
                    (sky_native_code_function_t)setresgid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "rgid", SKY_DATA_TYPE_GID_T, NULL,
                    "egid", SKY_DATA_TYPE_GID_T, NULL,
                    "sgid", SKY_DATA_TYPE_GID_T, NULL,
                    NULL));
#endif
#if defined(HAVE_SETRESUID)
    sky_module_setattr(
            module,
            "setresuid",
            sky_function_createbuiltin(
                    "setresuid",
                    sky_posix_setresuid_doc,
                    (sky_native_code_function_t)setresuid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "ruid", SKY_DATA_TYPE_UID_T, NULL,
                    "euid", SKY_DATA_TYPE_UID_T, NULL,
                    "suid", SKY_DATA_TYPE_UID_T, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "setreuid",
            sky_function_createbuiltin(
                    "setreuid",
                    sky_posix_setreuid_doc,
                    (sky_native_code_function_t)setreuid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "ruid", SKY_DATA_TYPE_UID_T, NULL,
                    "euid", SKY_DATA_TYPE_UID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setsid",
            sky_function_createbuiltin(
                    "setsid",
                    sky_posix_setsid_doc,
                    (sky_native_code_function_t)setsid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "setuid",
            sky_function_createbuiltin(
                    "setuid",
                    sky_posix_setuid_doc,
                    (sky_native_code_function_t)setuid,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "uid", SKY_DATA_TYPE_UID_T, NULL,
                    NULL));
#if defined(HAVE_SYS_XATTR_H)
    sky_module_setattr(
            module,
            "setxattr",
            sky_function_createbuiltin(
                    "setxattr",
                    sky_posix_setxattr_doc,
                    (sky_native_code_function_t)sky_posix_setxattr,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "attribute", SKY_DATA_TYPE_OBJECT, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
#endif
#if defined(HAVE_SPAWNV)
    sky_module_setattr(
            module,
            "spawnv",
            sky_function_createbuiltin(
                    "spawnv",
                    sky_posix_spawnv_doc,
                    (sky_native_code_function_t)sky_posix_spawnv,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "mode", SKY_DATA_TYPE_INT, NULL,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "args", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "spawnve",
            sky_function_createbuiltin(
                    "spawnve",
                    sky_posix_spawnve_doc,
                    (sky_native_code_function_t)sky_posix_spawnve,
                    SKY_DATA_TYPE_SYSCALL_INT,
                    "mode", SKY_DATA_TYPE_INT, NULL,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "args", SKY_DATA_TYPE_OBJECT, NULL,
                    "env", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "stat",
            sky_function_createbuiltin(
                    "stat",
                    sky_posix_stat_doc,
                    (sky_native_code_function_t)sky_posix_stat,
                    SKY_DATA_TYPE_OBJECT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    f = sky_function_createbuiltin(
                "stat_float_times",
                sky_posix_stat_float_times_doc,
                (sky_native_code_function_t)sky_posix_stat_float_times,
                SKY_DATA_TYPE_OBJECT,
                "newval", SKY_DATA_TYPE_OBJECT_BOOLEAN, sky_NotSpecified,
                NULL);
    sky_function_setdeprecated(f, NULL);
    sky_module_setattr(module, "stat_float_times", f);
    sky_module_setattr(
            module,
            "statvfs",
            sky_function_createbuiltin(
                    "statvfs",
                    sky_posix_statvfs_doc,
                    (sky_native_code_function_t)sky_posix_statvfs,
                    SKY_DATA_TYPE_OBJECT,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "strerror",
            sky_function_createbuiltin(
                    "strerror",
                    sky_posix_strerror_doc,
                    (sky_native_code_function_t)sky_posix_strerror,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "code", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "symlink",
            sky_function_createbuiltin(
                    "symlink",
                    sky_posix_symlink_doc,
                    (sky_native_code_function_t)sky_posix_symlink,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "src", SKY_DATA_TYPE_OBJECT, NULL,
                    "dst", SKY_DATA_TYPE_OBJECT, NULL,
                    "target_is_directory", SKY_DATA_TYPE_BOOL, sky_False,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "sync",
            sky_function_createbuiltin(
                    "sync",
                    sky_posix_sync_doc,
                    (sky_native_code_function_t)sync,
                    SKY_DATA_TYPE_VOID,
                    NULL));
#if defined(HAVE_SYSCONF)
    sky_module_setattr(
            module,
            "sysconf",
            sky_function_createbuiltin(
                    "sysconf",
                    sky_posix_sysconf_doc,
                    (sky_native_code_function_t)sky_posix_sysconf,
                    SKY_DATA_TYPE_LONG,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "system",
            sky_function_createbuiltin(
                    "system",
                    sky_posix_system_doc,
                    (sky_native_code_function_t)sky_posix_system,
                    SKY_DATA_TYPE_INT,
                    "command", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "tcgetpgrp",
            sky_function_createbuiltin(
                    "tcgetpgrp",
                    sky_posix_tcgetpgrp_doc,
                    (sky_native_code_function_t)tcgetpgrp,
                    SKY_DATA_TYPE_SYSCALL_PID_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "tcsetpgrp",
            sky_function_createbuiltin(
                    "tcsetpgrp",
                    sky_posix_tcsetpgrp_doc,
                    (sky_native_code_function_t)tcsetpgrp,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "pgid", SKY_DATA_TYPE_PID_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "times",
            sky_function_createbuiltin(
                    "times",
                    sky_posix_times_doc,
                    (sky_native_code_function_t)sky_posix_times,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "truncate",
            sky_function_createbuiltin(
                    "truncate",
                    sky_posix_truncate_doc,
                    (sky_native_code_function_t)sky_posix_truncate,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "length", SKY_DATA_TYPE_OFF_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ttyname",
            sky_function_createbuiltin(
                    "ttyname",
                    sky_posix_ttyname_doc,
                    (sky_native_code_function_t)sky_posix_ttyname,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "umask",
            sky_function_createbuiltin(
                    "umask",
                    sky_posix_umask_doc,
                    (sky_native_code_function_t)umask,
                    SKY_DATA_TYPE_INT,
                    "new_mask", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "uname",
            sky_function_createbuiltin(
                    "uname",
                    sky_posix_uname_doc,
                    (sky_native_code_function_t)sky_posix_uname,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "unlink",
            sky_function_createbuiltin(
                    "unlink",
                    sky_posix_unlink_doc,
                    (sky_native_code_function_t)sky_posix_unlink,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "unsetenv",
            sky_function_createbuiltin(
                    "unsetenv",
                    sky_posix_unsetenv_doc,
                    (sky_native_code_function_t)sky_posix_unsetenv,
                    SKY_DATA_TYPE_VOID,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "urandom",
            sky_function_createbuiltin(
                    "urandom",
                    sky_posix_urandom_doc,
                    (sky_native_code_function_t)sky_posix_urandom,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "utime",
            sky_function_createbuiltin(
                    "utime",
                    sky_posix_utime_doc,
                    (sky_native_code_function_t)sky_posix_utime,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "path", SKY_DATA_TYPE_OBJECT, NULL,
                    "times", SKY_DATA_TYPE_OBJECT_TUPLE, sky_None,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "ns", SKY_DATA_TYPE_OBJECT_TUPLE, sky_None,
                    "dir_fd", SKY_DATA_TYPE_OBJECT, sky_None,
                    "follow_symlinks", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "wait",
            sky_function_createbuiltin(
                    "wait",
                    sky_posix_wait_doc,
                    (sky_native_code_function_t)sky_posix_wait,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(
            module,
            "wait3",
            sky_function_createbuiltin(
                    "wait3",
                    sky_posix_wait3_doc,
                    (sky_native_code_function_t)sky_posix_wait3,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "options", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "wait4",
            sky_function_createbuiltin(
                    "wait4",
                    sky_posix_wait4_doc,
                    (sky_native_code_function_t)sky_posix_wait4,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "options", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#if defined(HAVE_WAITID)
    sky_module_setattr(
            module,
            "waitid",
            sky_function_createbuiltin(
                    "waitid",
                    sky_posix_waitid_doc,
                    (sky_native_code_function_t)sky_posix_waitid,
                    SKY_DATA_TYPE_OBJECT,
                    "idtype", SKY_DATA_TYPE_INT, NULL,
                    "id", SKY_DATA_TYPE_PID_T, NULL,
                    "options", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "waitpid",
            sky_function_createbuiltin(
                    "waitpid",
                    sky_posix_waitpid_doc,
                    (sky_native_code_function_t)sky_posix_waitpid,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "pid", SKY_DATA_TYPE_PID_T, NULL,
                    "options", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WCOREDUMP",
            sky_function_createbuiltin(
                    "WCOREDUMP",
                    sky_posix_WCOREDUMP_doc,
                    (sky_native_code_function_t)sky_posix_WCOREDUMP,
                    SKY_DATA_TYPE_BOOL,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WEXITSTATUS",
            sky_function_createbuiltin(
                    "WEXITSTATUS",
                    sky_posix_WEXITSTATUS_doc,
                    (sky_native_code_function_t)sky_posix_WEXITSTATUS,
                    SKY_DATA_TYPE_INT,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WIFCONTINUED",
            sky_function_createbuiltin(
                    "WIFCONTINUED",
                    sky_posix_WIFCONTINUED_doc,
                    (sky_native_code_function_t)sky_posix_WIFCONTINUED,
                    SKY_DATA_TYPE_BOOL,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WIFEXITED",
            sky_function_createbuiltin(
                    "WIFEXITED",
                    sky_posix_WIFEXITED_doc,
                    (sky_native_code_function_t)sky_posix_WIFEXITED,
                    SKY_DATA_TYPE_BOOL,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WIFSIGNALED",
            sky_function_createbuiltin(
                    "WIFSIGNALED",
                    sky_posix_WIFSIGNALED_doc,
                    (sky_native_code_function_t)sky_posix_WIFSIGNALED,
                    SKY_DATA_TYPE_BOOL,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WIFSTOPPED",
            sky_function_createbuiltin(
                    "WIFSTOPPED",
                    sky_posix_WIFSTOPPED_doc,
                    (sky_native_code_function_t)sky_posix_WIFSTOPPED,
                    SKY_DATA_TYPE_BOOL,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WSTOPSIG",
            sky_function_createbuiltin(
                    "WSTOPSIG",
                    sky_posix_WSTOPSIG_doc,
                    (sky_native_code_function_t)sky_posix_WSTOPSIG,
                    SKY_DATA_TYPE_INT,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "WTERMSIG",
            sky_function_createbuiltin(
                    "WTERMSIG",
                    sky_posix_WTERMSIG_doc,
                    (sky_native_code_function_t)sky_posix_WTERMSIG,
                    SKY_DATA_TYPE_INT,
                    "status", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "write",
            sky_function_createbuiltin(
                    "write",
                    sky_posix_write_doc,
                    (sky_native_code_function_t)sky_posix_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_WRITEV)
    sky_module_setattr(
            module,
            "writev",
            sky_function_createbuiltin(
                    "writev",
                    sky_posix_writev_doc,
                    (sky_native_code_function_t)sky_posix_writev,
                    SKY_DATA_TYPE_SSIZE_T,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "buffers", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
}
