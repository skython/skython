/* Originally written by Tom St Denis as part of LibTomCrypt, which is in the
 * public domain (see below; original source header retained). Adapted for use
 * in Skython.
 */

/* LibTomCrypt, modular cryptographic library -- Tom St Denis
 *
 * LibTomCrypt is a library that provides various cryptographic
 * algorithms in a highly modular and flexible manner.
 *
 * The library is free for all purposes without any express
 * guarantee it works.
 *
 * Tom St Denis, tomstdenis@gmail.com, http://libtom.org
 */

#include "sha2.h"

void
SHA384_Init(SHA384_CTX *ctx)
{
    ctx->curlen = 0;
    ctx->length = 0;
    ctx->state[0] = CONST64(0xcbbb9d5dc1059ed8);
    ctx->state[1] = CONST64(0x629a292a367cd507);
    ctx->state[2] = CONST64(0x9159015a3070dd17);
    ctx->state[3] = CONST64(0x152fecd8f70e5939);
    ctx->state[4] = CONST64(0x67332667ffc00b31);
    ctx->state[5] = CONST64(0x8eb44a8768581511);
    ctx->state[6] = CONST64(0xdb0c2e0d64f98fa7);
    ctx->state[7] = CONST64(0x47b5481dbefa4fa4);
}

void
SHA384_Final(uint8_t *out, SHA384_CTX *ctx)
{
    uint8_t buf[SHA512_DIGEST_LENGTH];

    sky_error_validate_debug(ctx->curlen < sizeof(ctx->buf));

    SHA512_Final(buf, ctx);
    memcpy(out, buf, SHA384_DIGEST_LENGTH);
}
