#include "../core/sky_private.h"

#include <locale.h>
#if defined(HAVE_LANGINFO_H)
#   include "langinfo.h"
#endif


static sky_type_t sky__locale_Error = NULL;

static sky_list_t
sky__locale_localeconv_grouping(const char *grouping)
{
    sky_list_t  result;

    result = sky_list_create(NULL);
    if (grouping && *grouping) {
        for (;;) {
            sky_list_append(result, sky_integer_create(*grouping));
            if (!*grouping || CHAR_MAX == *grouping) {
                break;
            }
            ++grouping;
        }
    }

    return result;
}

static sky_dict_t
sky__locale_localeconv(void)
{
    sky_dict_t      result;
    struct lconv    *lc;

    lc = localeconv();
    result = sky_dict_create();

#define set_int_result(name)                                        \
    sky_dict_setitem(result,                                        \
                     SKY_STRING_LITERAL(#name),                     \
                     sky_integer_create(lc->name))
#define set_str_result(name)                                        \
    sky_dict_setitem(result,                                        \
                     SKY_STRING_LITERAL(#name),                     \
                     sky_string_createfrombytes(lc->name,           \
                                                strlen(lc->name),   \
                                                NULL,               \
                                                NULL))

    set_str_result(decimal_point);
    set_str_result(thousands_sep);
    sky_dict_setitem(result,
                     SKY_STRING_LITERAL("grouping"),
                     sky__locale_localeconv_grouping(lc->grouping));
    set_str_result(int_curr_symbol);
    set_str_result(currency_symbol);
    set_str_result(mon_decimal_point);
    sky_dict_setitem(result,
                     SKY_STRING_LITERAL("mon_grouping"),
                     sky__locale_localeconv_grouping(lc->mon_grouping));
    set_str_result(positive_sign);
    set_str_result(negative_sign);
    set_int_result(int_frac_digits);
    set_int_result(p_cs_precedes);
    set_int_result(p_sep_by_space);
    set_int_result(n_cs_precedes);
    set_int_result(n_sep_by_space);
    set_int_result(p_sign_posn);
    set_int_result(n_sign_posn);
    set_int_result(int_p_cs_precedes);
    set_int_result(int_n_cs_precedes);
    set_int_result(int_p_sep_by_space);
    set_int_result(int_n_sep_by_space);
    set_int_result(int_p_sign_posn);
    set_int_result(int_n_sign_posn);


#undef set_str_result
#undef set_int_result

    return result;
}

static const char sky__locale_localeconv_doc[] =
"() -> dict. Returns numeric and monetary locale-specific parameters.";


#if defined(HAVE_LANGINFO_H)
static sky_string_t
sky__locale_nl_langinfo(int item)
{
    const char  *result;

    if (!(result = nl_langinfo(item))) {
        sky_error_raise_string(sky__locale_Error,
                               "unsupported langinfo constant");
    }
    return sky_string_createfrombytes(result, strlen(result), NULL, NULL);
}

static const char sky__locale_nl_langinfo_doc[] =
"nl_langinfo(item) -> string\n"
"Return the value for the locale information associated with item.";
#endif


static int
sky__locale_strcoll(                                                /* TODO */
    SKY_UNUSED  sky_string_t    s1,
    SKY_UNUSED  sky_string_t    s2)
{
    /* Need sky_string support for converting to wchar_t */
    sky_error_raise_string(sky_NotImplementedError, "strcoll");
}

static const char sky__locale_strcoll_doc[] =
"string,string -> int. Compares two strings according to the locale.";


static sky_string_t
sky__locale_strxfrm(SKY_UNUSED sky_string_t string)                 /* TODO */
{
    /* Need sky_string support for converting to wchar_t */
    sky_error_raise_string(sky_NotImplementedError, "strxfrm");
}

static const char sky__locale_strxfrm_doc[] =
"strxfrm(string) -> string.\n\n"
"Return a string that can be used as a key for locale-aware comparisons.";


static sky_string_t
sky__locale_setlocale(int category, sky_string_t locale)
{
    char        *result;
    const char  *cstring;

#if defined(LC_MIN) && defined(LC_MAX)
    if (category < LC_MIN || category > LC_MAX) {
        sky_error_raise_string(sky__locale_Error, "invalid locale category");
    }
#endif

    if (sky_object_isnull(locale)) {
        if (!(result = setlocale(category, NULL))) {
            sky_error_raise_string(sky__locale_Error, "locale query failed");
        }
        return sky_string_createfrombytes(result, strlen(result), NULL, NULL);
    }

    if ((cstring = sky_string_cstring(locale)) != NULL &&
        (result = setlocale(category, cstring)) != NULL)
    {
        return sky_string_createfrombytes(result, strlen(result), NULL, NULL);
    }

    sky_error_raise_string(sky__locale_Error, "unsupported locale setting");
}

static const char sky__locale_setlocale_doc[] =
"(integer,string=None) -> string. Activates/queries locale processing.";


void
skython_module__locale_initialize(sky_module_t module)
{
    const char doc[] = "Support for POSIX locales.";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

#if defined(_WIN32)
    /* TODO _getdefaultlocale() */
#endif

    sky_module_setattr(
            module,
            "localeconv",
            sky_function_createbuiltin(
                    "localeconv",
                    sky__locale_localeconv_doc,
                    (sky_native_code_function_t)sky__locale_localeconv,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    NULL));
#if defined(HAVE_LANGINFO_H)
    sky_module_setattr(
            module,
            "nl_langinfo",
            sky_function_createbuiltin(
                    "nl_langinfo",
                    sky__locale_nl_langinfo_doc,
                    (sky_native_code_function_t)sky__locale_nl_langinfo,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "item", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "setlocale",
            sky_function_createbuiltin(
                    "setlocale",
                    sky__locale_setlocale_doc,
                    (sky_native_code_function_t)sky__locale_setlocale,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "category", SKY_DATA_TYPE_INT, NULL,
                    "locale", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "strcoll",
            sky_function_createbuiltin(
                    "strcoll",
                    sky__locale_strcoll_doc,
                    (sky_native_code_function_t)sky__locale_strcoll,
                    SKY_DATA_TYPE_INT,
                    "s1", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "s2", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "strxfrm",
            sky_function_createbuiltin(
                    "strxfrm",
                    sky__locale_strxfrm_doc,
                    (sky_native_code_function_t)sky__locale_strxfrm,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

#if defined(HAVE_GETTEXT)
    /* TODO bindtextdomain() */
    /* TODO bind_textdomain_codeset() */
    /* TODO dgettext() */
    /* TODO dcgettext() */
    /* TODO gettext() */
    /* TODO textdomain() */
#endif

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__locale_Error),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("Error"),
                    NULL,
                    SKY_TYPE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_Exception),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "Error", sky__locale_Error);

#define set_int_const(name) \
    sky_module_setattr(module, #name, sky_integer_create(name))

    set_int_const(CHAR_MAX);

    set_int_const(LC_ALL);
    set_int_const(LC_COLLATE);
    set_int_const(LC_CTYPE);
#if defined(LC_MESSAGES)
    set_int_const(LC_MESSAGES);
#endif
    set_int_const(LC_MONETARY);
    set_int_const(LC_NUMERIC);
    set_int_const(LC_TIME);

#if defined(HAVE_LANGINFO_H)
    set_int_const(CODESET);
    set_int_const(D_T_FMT);
    set_int_const(D_FMT);
    set_int_const(T_FMT);
    set_int_const(T_FMT_AMPM);
    set_int_const(AM_STR);
    set_int_const(PM_STR);

    set_int_const(DAY_1);
    set_int_const(DAY_2);
    set_int_const(DAY_3);
    set_int_const(DAY_4);
    set_int_const(DAY_5);
    set_int_const(DAY_6);
    set_int_const(DAY_7);

    set_int_const(ABDAY_1);
    set_int_const(ABDAY_2);
    set_int_const(ABDAY_3);
    set_int_const(ABDAY_4);
    set_int_const(ABDAY_5);
    set_int_const(ABDAY_6);
    set_int_const(ABDAY_7);

    set_int_const(MON_1);
    set_int_const(MON_2);
    set_int_const(MON_3);
    set_int_const(MON_4);
    set_int_const(MON_5);
    set_int_const(MON_6);
    set_int_const(MON_7);
    set_int_const(MON_8);
    set_int_const(MON_9);
    set_int_const(MON_10);
    set_int_const(MON_11);
    set_int_const(MON_12);

    set_int_const(ABMON_1);
    set_int_const(ABMON_2);
    set_int_const(ABMON_3);
    set_int_const(ABMON_4);
    set_int_const(ABMON_5);
    set_int_const(ABMON_6);
    set_int_const(ABMON_7);
    set_int_const(ABMON_8);
    set_int_const(ABMON_9);
    set_int_const(ABMON_10);
    set_int_const(ABMON_11);
    set_int_const(ABMON_12);

    set_int_const(ERA);
    set_int_const(ERA_D_FMT);
    set_int_const(ERA_D_T_FMT);
    set_int_const(ERA_T_FMT);
    set_int_const(ALT_DIGITS);
    set_int_const(RADIXCHAR);
    set_int_const(THOUSEP);
    set_int_const(YESEXPR);
    set_int_const(NOEXPR);
    set_int_const(CRNCYSTR);
#endif
}
