#include "../core/skython.h"

#include <limits.h>

SKY_MODULE_EXTERN const char *skython_module__struct_doc;

SKY_MODULE_EXTERN void
skython_module__struct_initialize(sky_module_t module);


typedef long long llong, sllong;
typedef unsigned long long ullong;
typedef void *pointer;


static sky_dict_t sky__struct_cache = NULL;


typedef void (*sky__struct_pack_t)(char *, sky_object_t);
typedef sky_object_t (*sky__struct_unpack_t)(const char *);

typedef struct sky__struct_format_s {
    int                                 fmt;
    ssize_t                             size;
    ssize_t                             alignment;
    sky__struct_pack_t                  pack;
    sky__struct_unpack_t                unpack;
} sky__struct_format_t;


static sky__struct_format_t *sky__struct_big_table[128];
static sky__struct_format_t *sky__struct_host_table[128];
static sky__struct_format_t *sky__struct_little_table[128];
static sky__struct_format_t *sky__struct_native_table[128];


static sky_type_t sky__struct_error = NULL;

static const char sky__struct_Struct_type_doc[] =
"Struct(fmt) --> compiled struct object\n"
"\n"
"Return a new Struct object which writes and reads binary data according to\n"
"the format string fmt.  See help(struct) for more on format strings.";

static sky_type_t sky__struct_Struct_type = NULL;

typedef struct sky__struct_Struct_s *sky__struct_Struct_t;

typedef struct sky__struct_Struct_data_s {
    sky_bytes_t                         format;
    ssize_t                             size;
    sky__struct_format_t **             table;
    uint8_t *                           code;
    ssize_t                             codelen;
    ssize_t                             codesize;
    ssize_t                             count;
} sky__struct_Struct_data_t;

SKY_EXTERN_INLINE sky__struct_Struct_data_t *
sky__struct_Struct_data(sky_object_t self)
{
    return sky_object_data(self, sky__struct_Struct_type);
}

static void
sky__struct_Struct_instance_finalize(SKY_UNUSED sky_object_t    self,
                                                void *          data)
{
    sky__struct_Struct_data_t   *self_data = data;

    sky_free(self_data->code);
}

static void
sky__struct_Struct_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__struct_Struct_data_t   *self_data = data;

    sky_object_visit(self_data->format, visit_data);
}


static inline void
sky__struct_Struct__appendCode(
                sky__struct_Struct_data_t * self_data,
                size_t                      repeat,
                char                        fmt)
{
    ssize_t nbytes;
    uint8_t *code;

    nbytes = (repeat <= 127 ? 2 : sky_util_varint_size(repeat) + 1);
    if (self_data->codelen + nbytes > self_data->codesize) {
        self_data->code = sky_realloc(self_data->code,
                                      self_data->codelen + nbytes);
        self_data->codesize = sky_memsize(self_data->code);
    }

    code = self_data->code + self_data->codelen;
    code = sky_util_varint_encode(repeat,
                                  code,
                                  self_data->codesize - self_data->codelen);
    *code = fmt;
    self_data->codelen += nbytes;
}

static void
sky__struct_Struct__parseFormat(
                sky__struct_Struct_t    self,
                const char *            bytes,
                ssize_t                 nbytes)
{
    const char                  *c = bytes,
                                *end = bytes + nbytes;
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    while (c < end && sky_ctype_isspace(*c)) {
        ++c;
    }
    if (c >= end) {
        return;
    }

    switch (*c) {
        case '=':
            self_data->table = sky__struct_host_table;
            if (++c >= end) {
                return;
            }
            break;
        case '<':
            self_data->table = sky__struct_little_table;
            if (++c >= end) {
                return;
            }
            break;
        case '>':
        case '!':
            self_data->table = sky__struct_big_table;
            if (++c >= end) {
                return;
            }
            break;
        case '@':
            if (++c >= end) {
                return;
            }
            break;
    }

    while (c < end) {
        size_t                  n, repeat;
        sky__struct_format_t    *format;

        while (c < end && sky_ctype_isspace(*c)) {
            ++c;
        }
        if (c >= end) {
            return;
        }

        /* Optional repeat count. Accumulate digits. */
        if (!sky_ctype_isdigit(*c)) {
            repeat = 1;
        }
        else {
            repeat = 0;
            while (c < end && sky_ctype_isdigit(*c)) {
                repeat = (repeat * 10) + (*c - '0');
                ++c;
            }
            if (c >= end) {
                sky_error_raise_string(
                        sky__struct_error,
                        "repeat count given without format specifier");
            }
        }

        /* Format code. Must be defined in self_data->table. */
        if (*(uint8_t *)c >= 128 || !(format = self_data->table[(int)*c])) {
            sky_error_raise_string(
                    sky__struct_error,
                    "bad char in struct format");
        }
        ++c;
        if (format->size) {
            while (c < end) {
                while (c < end && sky_ctype_isspace(*c)) {
                    ++c;
                }
                if (c >= end || *c != format->fmt) {
                    break;
                }
                while (c < end && *c == format->fmt) {
                    ++c;
                    ++repeat;
                }
            }
        }

        /* 'p' and 's' still require an input, even for a repeat of 0.
         * Everything else skips. Note that CPython has problems dealing with
         * '0p' in practice, but it accepts it at this stage.
         */
        if (!repeat && format->size) {
            continue;
        }

        if (format->alignment > 1 &&
            (self_data->size & (format->alignment - 1)))
        {
            n = format->alignment - (self_data->size & (format->alignment - 1));
            sky__struct_Struct__appendCode(self_data, n, 'x');
        }

        sky__struct_Struct__appendCode(self_data, repeat, format->fmt);
        self_data->size += (repeat * (format->size ? format->size : 1));
        if ('p' == format->fmt || 's' == format->fmt) {
            ++self_data->count;
        }
        else if (format->fmt != 'x') {
            self_data->count += (repeat ? repeat : 1);
        }
    }
}


static void
sky__struct_Struct_init(sky_object_t self, sky_object_t format)
{
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    if (sky_object_isa(format, sky_string_type)) {
        /* Convert the string to a bytes object. */
        format = sky_codec_encode(sky_codec_ascii, format, NULL, NULL, 0);
    }
    if (!sky_buffer_check(format)) {
        sky_error_raise_format(
                sky_TypeError,
                "Struct() argument 1 must be a bytes object, not %@",
                sky_type_name(sky_object_type(format)));
    }
    format = sky_bytes_createfromobject(format);

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->format)), format, self);
    self_data->size = 0;
    self_data->table = sky__struct_native_table;
    sky_free(self_data->code);
    self_data->codelen = 0;
    self_data->codesize = 0;
    self_data->count = 0;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, format, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky__struct_Struct__parseFormat(self, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static void
sky__struct_Struct_packpascal(
                char *          raw_bytes,
                size_t          repeat,
                sky_object_t    object)
{
    size_t          nbytes;
    sky_buffer_t    buffer;

    if (!sky_buffer_check(object)) {
        sky_error_raise_string(sky__struct_error,
                               "argument for 'p' must be a bytes object");
    }
    if (!repeat) {
        return;
    }

    nbytes = SKY_MIN(255, repeat - 1);
    *(uint8_t *)raw_bytes = nbytes;

    sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    memcpy(&(raw_bytes[1]), buffer.buf, SKY_MIN(nbytes, (size_t)buffer.len));
    if (nbytes > (size_t)buffer.len) {
        memset(&(raw_bytes[buffer.len + 1]), 0, nbytes - buffer.len);
    }
    sky_buffer_release(&buffer);
}

static void
sky__struct_Struct_packstring(
                char *          raw_bytes,
                size_t          repeat,
                sky_object_t    object)
{
    size_t          nbytes;
    sky_buffer_t    buffer;

    if (!sky_buffer_check(object)) {
        sky_error_raise_string(sky__struct_error,
                               "argument for 's' must be a bytes object");
    }
    if (!repeat) {
        return;
    }

    sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    nbytes = SKY_MIN(repeat, (size_t)buffer.len);
    memcpy(&(raw_bytes[1]), buffer.buf, nbytes);
    if (nbytes > (size_t)buffer.len) {
        memset(&(raw_bytes[buffer.len]), 0, nbytes - buffer.len);
    }
    sky_buffer_release(&buffer);
}

static void
sky__struct_Struct_packintobytes(
                sky__struct_Struct_data_t * self_data,
                char *                      raw_bytes,
                sky_tuple_t                 args,
                const char *                function)
{
    size_t                  repeat;
    ssize_t                 idx;
    const uint8_t           *c, *end;
    sky__struct_format_t    *format;

    if (sky_tuple_len(args) != self_data->count) {
        sky_error_raise_format(sky__struct_error,
                               "%s expected %zd items for packing (got %zd)",
                               function,
                               self_data->count,
                               sky_tuple_len(args));
    }

    idx = 0;
    c = self_data->code;
    end = self_data->code + self_data->codelen;
    while (c < end) {
        if (!(*c & 0x80)) {
            repeat = *c++;
        }
        else {
            c = sky_util_varint_decode(c, end - c, &repeat);
        }
        format = self_data->table[*c++];
        switch (format->fmt) {
            case 'x':
                raw_bytes += repeat;
                break;
            case 'p':
                sky__struct_Struct_packpascal(raw_bytes,
                                              repeat,
                                              sky_tuple_get(args, idx));
                raw_bytes += repeat;
                ++idx;
                break;
            case 's':
                sky__struct_Struct_packstring(raw_bytes,
                                              repeat,
                                              sky_tuple_get(args, idx));
                raw_bytes += repeat;
                ++idx;
                break;
            default:
                while (repeat-- > 0) {
                    format->pack(raw_bytes, sky_tuple_get(args, idx));
                    raw_bytes += format->size;
                    ++idx;
                }
                break;
        }
    }
}


static sky_bytes_t
sky__struct_Struct_pack(sky_object_t self, sky_tuple_t args)
{
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    char        *raw_bytes;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        raw_bytes = sky_asset_malloc(self_data->size,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        sky__struct_Struct_packintobytes(self_data,
                                         raw_bytes,
                                         args,
                                         "pack");

        bytes = sky_bytes_createwithbytes(raw_bytes, self_data->size, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char sky__struct_Struct_pack_doc[] =
"S.pack(v1, v2, ...) -> bytes\n\n"
"Return a bytes object containing values v1, v2, ... packed according\n"
"to the format string S.format.  See help(struct) for more on format\n"
"strings.";


static void
sky__struct_Struct_pack_into(
                sky_object_t    self,
                sky_object_t    buffer,
                ssize_t         offset,
                sky_tuple_t     args)
{
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    char            *raw_bytes;
    sky_buffer_t    raw_buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&raw_buffer, 
                           buffer,
                           SKY_BUFFER_FLAG_SIMPLE | SKY_BUFFER_FLAG_WRITABLE);
        sky_asset_save(&raw_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (raw_buffer.len - offset < self_data->size) {
            sky_error_raise_format(
                    sky__struct_error,
                    "pack_into requires a buffer of at least %zd byte%s",
                    self_data->size,
                    (self_data->size == 1 ? "" : "s"));
        }
        raw_bytes = (char *)raw_buffer.buf + offset;

        sky__struct_Struct_packintobytes(self_data,
                                         raw_bytes,
                                         args,
                                         "pack_into");
    } SKY_ASSET_BLOCK_END;
}

static const char sky__struct_Struct_pack_into_doc[] =
"S.pack_into(buffer, offset, v1, v2, ...)\n\n"
"Pack the values v1, v2, ... according to the format string S.format\n"
"and write the packed bytes into the writable buffer buf starting at\n"
"offset.  Note that the offset is a required argument.  See\n"
"help(struct) for more on format strings.";


static size_t
sky__struct_Struct_sizeof(sky_object_t self)
{
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    return sky_memsize(self) + sky_memsize(self_data->code);
}


static sky_tuple_t
sky__struct_Struct_unpack_from(
                sky_object_t    self,
                sky_object_t    buffer,
                ssize_t         offset)
{
    sky__struct_Struct_data_t   *self_data = sky__struct_Struct_data(self);

    char                    *raw_bytes;
    size_t                  repeat;
    ssize_t                 idx;
    sky_tuple_t             result;
    sky_buffer_t            raw_buffer;
    sky_object_t            *objects;
    const uint8_t           *c, *end;
    sky__struct_format_t    *format;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&raw_buffer, buffer, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&raw_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (raw_buffer.len - offset < self_data->size) {
            sky_error_raise_format(
                    sky__struct_error,
                    "unpack_from requires a buffer of at least %zd byte%s",
                    self_data->size,
                    (self_data->size == 1 ? "" : "s"));
        }
        raw_bytes = (char *)raw_buffer.buf + offset;

        if (!self_data->count) {
            objects = NULL;
        }
        else {
            objects = sky_asset_calloc(self_data->count,
                                       sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);
        }

        idx = 0;
        c = self_data->code;
        end = self_data->code + self_data->codelen;
        while (c < end) {
            if (!(*c & 0x80)) {
                repeat = *c++;
            }
            else {
                c = sky_util_varint_decode(c, end - c, &repeat);
            }
            format = self_data->table[*c++];
            switch (format->fmt) {
                case 'x':
                    raw_bytes += repeat;
                    break;
                case 'p':
                    objects[idx++] =
                            sky_bytes_createfrombytes(&(raw_bytes[1]),
                                                      *(uint8_t *)raw_bytes);
                    raw_bytes += repeat;
                    break;
                case 's':
                    objects[idx++] =
                            sky_bytes_createfrombytes(raw_bytes, repeat);
                    raw_bytes += repeat;
                    break;
                default:
                    while (repeat-- > 0) {
                        objects[idx++] = format->unpack(raw_bytes);
                        raw_bytes += format->size;
                    }
                    break;
            }
        }
        result = sky_tuple_createwitharray(self_data->count, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__struct_Struct_unpack_from_doc[] =
"S.unpack_from(buffer, offset=0) -> (v1, v2, ...)\n\n"
"Return a tuple containing values unpacked according to the format\n"
"string S.format.  Requires len(buffer[offset:]) == S.size.  See\n"
"help(struct) for more on format strings.";


static sky_tuple_t
sky__struct_Struct_unpack(sky_object_t self, sky_object_t buffer)
{
    return sky__struct_Struct_unpack_from(self, buffer, 0);
}

static const char sky__struct_Struct_unpack_doc[] =
"S.unpack(buffer) -> (v1, v2, ...)\n\n"
"Return a tuple containing values unpacked according to the format\n"
"string S.format.  Requires len(buffer) == S.size.  See help(struct)\n"
"for more on format strings.";


static sky__struct_Struct_t
sky__struct__lookupcache(sky_object_t format)
{
    sky__struct_Struct_t    s;

    if (sky_object_isa(format, sky_string_type)) {
        format = sky_codec_encode(sky_codec_ascii, format, NULL, NULL, 0);
    }
    if (!sky_buffer_check(format)) {
        sky_error_raise_format(
                sky_TypeError,
                "Struct() argument 1 must be a bytes object, not %@",
                sky_type_name(sky_object_type(format)));
    }
    format = sky_bytes_createfromobject(format);

    if (!(s = sky_dict_get(sky__struct_cache, format, NULL))) {
        s = sky_object_call(sky__struct_Struct_type,
                            sky_tuple_pack(1, format),
                            NULL);
        sky_dict_setitem(sky__struct_cache, format, s);
    }

    return s;
}


static ssize_t
sky__struct_calcsize(sky_object_t format)
{
    sky__struct_Struct_t        s;
    sky__struct_Struct_data_t   *s_data;

    s = sky__struct__lookupcache(format);
    s_data = sky__struct_Struct_data(s);

    return s_data->size;
}

static const char sky__struct_calcsize_doc[] =
"calcsize(fmt) -> integer\n\n"
"Return size in bytes of the struct described by the format string fmt.";


static void
sky__struct__clearcache(void)
{
    sky_dict_clear(sky__struct_cache);
}

static const char
sky__struct__clearcache_doc[] =
"Clear the internal cache.";


static sky_bytes_t
sky__struct_pack(sky_object_t fmt, sky_tuple_t args)
{
    sky__struct_Struct_t    s;

    s = sky__struct__lookupcache(fmt);
    return sky__struct_Struct_pack(s, args);
}

static const char sky__struct_pack_doc[] =
"pack(fmt, v1, v2, ...) -> bytes\n\n"
"Return a bytes object containing the values v1, v2, ... packed according\n"
"to the format string fmt.  See help(struct) for more on format strings.";


static void
sky__struct_pack_into(
                sky_object_t    fmt,
                sky_object_t    buffer,
                ssize_t         offset,
                sky_tuple_t     args)
{
    sky__struct_Struct_t    s;

    s = sky__struct__lookupcache(fmt);
    sky__struct_Struct_pack_into(s, buffer, offset, args);
}

static const char sky__struct_pack_into_doc[] =
"pack_into(fmt, buffer, offset, v1, v2, ...)\n\n"
"Pack the values v1, v2, ... according to the format string fmt and write\n"
"the packed bytes into the writable buffer buf starting at offset.  Note\n"
"that the offset is a required argument.  See help(struct) for more\n"
"on format strings.";


static sky_tuple_t
sky__struct_unpack(sky_object_t fmt, sky_object_t buffer)
{
    sky__struct_Struct_t    s;

    s = sky__struct__lookupcache(fmt);
    return sky__struct_Struct_unpack_from(s, buffer, 0);
}

static const char sky__struct_unpack_doc[] =
"unpack(fmt, buffer) -> (v1, v2, ...)\n\n"
"Return a tuple containing values unpacked according to the format string\n"
"fmt.  Requires len(buffer) == calcsize(fmt). See help(struct) for more\n"
"on format strings.";


static sky_tuple_t
sky__struct_unpack_from(
                sky_object_t    fmt,
                sky_object_t    buffer,
                ssize_t         offset)
{
    sky__struct_Struct_t    s;

    s = sky__struct__lookupcache(fmt);
    return sky__struct_Struct_unpack_from(s, buffer, offset);
}

static const char sky__struct_unpack_from_doc[] =
"unpack_from(fmt, buffer, offset=0) -> (v1, v2, ...)\n\n"
"Return a tuple containing values unpacked according to the format string\n"
"fmt.  Requires len(buffer[offset:]) >= calcsize(fmt).  See help(struct)\n"
"for more on format strings.";


#define native(x)   (x)

#define PACK_INTEGER(_name, _type, _min, _max, _endian)                     \
    static void                                                             \
    sky__struct_pack_##_name(char *buffer, sky_object_t object)             \
    {                                                                       \
        _type value = _endian(sky_integer_value(sky_number_index(object),   \
                                                _min,                       \
                                                _max,                       \
                                                sky__struct_error));        \
                                                                            \
        if ((uintptr_t)buffer & (sizeof(_type) - 1)) {                      \
            memcpy(buffer, &value, sizeof(value));                          \
        }                                                                   \
        else {                                                              \
            *(_type *)buffer = value;                                       \
        }                                                                   \
    }

#define PACK_FLOAT(_name, _type, _endian)                                   \
    static void                                                             \
    sky__struct_pack_##_name(char *buffer, sky_object_t object)             \
    {                                                                       \
        _type value = _endian((_type)sky_float_value(object));              \
                                                                            \
        if ((uintptr_t)buffer & (sizeof(value) - 1)) {                      \
            memcpy(buffer, &value, sizeof(value));                          \
        }                                                                   \
        else {                                                              \
            *(_type *)buffer = value;                                       \
        }                                                                   \
    }

PACK_INTEGER(schar, signed char, SCHAR_MIN, SCHAR_MAX, native)
PACK_INTEGER(uchar, unsigned char, 0, UCHAR_MAX, native);
PACK_INTEGER(sshort, signed short, SHRT_MIN, SHRT_MAX, native)
PACK_INTEGER(ushort, unsigned short, 0, USHRT_MAX, native)
PACK_INTEGER(sint, signed int, INT_MIN, INT_MAX, native)
PACK_INTEGER(uint, unsigned int, 0, UINT_MAX, native)
PACK_INTEGER(slong, signed long, LONG_MIN, LONG_MAX, native)
PACK_INTEGER(ulong, unsigned long, 0, ULONG_MAX, native)
PACK_INTEGER(sllong, signed long long, LLONG_MAX, LLONG_MAX, native)
PACK_INTEGER(ullong, signed long long, 0, ULLONG_MAX, native)
PACK_INTEGER(ssize_t, ssize_t, SSIZE_MIN, SSIZE_MAX, native)
PACK_INTEGER(size_t, size_t, 0, SIZE_MAX, native)
PACK_INTEGER(pointer, intptr_t, INTPTR_MIN, INTPTR_MAX, native)

PACK_INTEGER(int16_be, int16_t, INT16_MIN, INT16_MAX, sky_endian_htob16)
PACK_INTEGER(int16_le, int16_t, INT16_MIN, INT16_MAX, sky_endian_htol16)
PACK_INTEGER(uint16_be, uint16_t, 0, UINT16_MAX, sky_endian_htob16)
PACK_INTEGER(uint16_le, uint16_t, 0, UINT16_MAX, sky_endian_htol16)

PACK_INTEGER(int32_be, int32_t, INT32_MIN, INT32_MAX, sky_endian_htob32)
PACK_INTEGER(int32_le, int32_t, INT32_MIN, INT32_MAX, sky_endian_htol32)
PACK_INTEGER(uint32_be, uint32_t, 0, UINT32_MAX, sky_endian_htob32)
PACK_INTEGER(uint32_le, uint32_t, 0, UINT32_MAX, sky_endian_htol32)

PACK_INTEGER(int64_be, int64_t, INT64_MIN, INT64_MAX, sky_endian_htob64)
PACK_INTEGER(int64_le, int64_t, INT64_MIN, INT64_MAX, sky_endian_htol64)
PACK_INTEGER(uint64_be, uint64_t, 0, UINT64_MAX, sky_endian_htob64)
PACK_INTEGER(uint64_le, uint64_t, 0, UINT64_MAX, sky_endian_htol64)

PACK_FLOAT(float, float, native)
PACK_FLOAT(double, double, native)
PACK_FLOAT(float_be, float, sky_endian_btohf)
PACK_FLOAT(double_be, double, sky_endian_btohd)
PACK_FLOAT(float_le, float, sky_endian_ltohf)
PACK_FLOAT(double_le, double, sky_endian_ltohd)

#undef PACK_FLOAT
#undef PACK_INTEGER


#define UNPACK_SIGNED(_name, _type, _endian)                                \
    static sky_object_t                                                     \
    sky__struct_unpack_##_name(const char *buffer)                          \
    {                                                                       \
        _type   value;                                                      \
                                                                            \
        if ((uintptr_t)buffer & (sizeof(_type) - 1)) {                      \
            memcpy(&value, buffer, sizeof(value));                          \
        }                                                                   \
        else {                                                              \
            value = *(const _type *)buffer;                                 \
        }                                                                   \
        return sky_integer_create(_endian(value));                          \
    }

#define UNPACK_UNSIGNED(_name, _type, _endian)                              \
    static sky_object_t                                                     \
    sky__struct_unpack_##_name(const char *buffer)                          \
    {                                                                       \
        _type   value;                                                      \
                                                                            \
        if ((uintptr_t)buffer & (sizeof(_type) - 1)) {                      \
            memcpy(&value, buffer, sizeof(value));                          \
        }                                                                   \
        else {                                                              \
            value = *(const _type *)buffer;                                 \
        }                                                                   \
        return sky_integer_createfromunsigned(_endian(value));              \
    }

#define UNPACK_FLOAT(_name, _type, _endian)                                 \
    static sky_object_t                                                     \
    sky__struct_unpack_##_name(const char *buffer)                          \
    {                                                                       \
        _type   value;                                                      \
                                                                            \
        if ((uintptr_t)buffer & (sizeof(value) - 1)) {                      \
            memcpy(&value, buffer, sizeof(value));                          \
        }                                                                   \
        else {                                                              \
            value = *(_type *)buffer;                                       \
        }                                                                   \
        return sky_float_create(_endian(value));                            \
    }

UNPACK_SIGNED(schar, signed char, native)
UNPACK_UNSIGNED(uchar, unsigned char, native);
UNPACK_SIGNED(sshort, signed short, native)
UNPACK_UNSIGNED(ushort, unsigned short, native)
UNPACK_SIGNED(sint, signed int, native)
UNPACK_UNSIGNED(uint, unsigned int, native)
UNPACK_SIGNED(slong, signed long, native)
UNPACK_UNSIGNED(ulong, unsigned long, native)
UNPACK_SIGNED(sllong, signed long long, native)
UNPACK_UNSIGNED(ullong, signed long long, native)
UNPACK_SIGNED(ssize_t, ssize_t, native)
UNPACK_UNSIGNED(size_t, size_t, native)
UNPACK_SIGNED(pointer, intptr_t, native)

UNPACK_SIGNED(int16_be, int16_t, sky_endian_btoh16)
UNPACK_SIGNED(int16_le, int16_t, sky_endian_ltoh16)
UNPACK_UNSIGNED(uint16_be, uint16_t, sky_endian_btoh16)
UNPACK_UNSIGNED(uint16_le, uint16_t, sky_endian_ltoh16)

UNPACK_SIGNED(int32_be, int32_t, sky_endian_btoh32)
UNPACK_SIGNED(int32_le, int32_t, sky_endian_ltoh32)
UNPACK_UNSIGNED(uint32_be, uint32_t, sky_endian_btoh32)
UNPACK_UNSIGNED(uint32_le, uint32_t, sky_endian_ltoh32)

UNPACK_SIGNED(int64_be, int64_t, sky_endian_btoh64)
UNPACK_SIGNED(int64_le, int64_t, sky_endian_ltoh64)
UNPACK_UNSIGNED(uint64_be, uint64_t, sky_endian_btoh64)
UNPACK_UNSIGNED(uint64_le, uint64_t, sky_endian_ltoh64)

UNPACK_FLOAT(float, float, native)
UNPACK_FLOAT(double, double, native)
UNPACK_FLOAT(float_be, float, sky_endian_htobf)
UNPACK_FLOAT(double_be, double, sky_endian_htobd)
UNPACK_FLOAT(float_le, float, sky_endian_htolf)
UNPACK_FLOAT(double_le, double, sky_endian_htold)

#undef UNPACK_FLOAT
#undef UNPACK_UNSIGNED
#undef UNPACK_SIGNED

#undef native


static void
sky__struct_pack_char(char *buffer, sky_object_t object)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    raw_buffer;

        sky_buffer_acquire(&raw_buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&raw_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (raw_buffer.len != 1) {
            sky_error_raise_string(
                    sky__struct_error,
                    "char format requires a bytes object of length 1");
        }
        *buffer = ((char *)raw_buffer.buf)[0];
    } SKY_ASSET_BLOCK_END;
}

static sky_object_t
sky__struct_unpack_char(const char *buffer)
{
    return sky_bytes_createfrombytes(buffer, 1);
}

static void
sky__struct_pack__Bool(char *buffer, sky_object_t object)
{
    *buffer = (sky_object_bool(object) ? 1 : 0);
}

static sky_object_t
sky__struct_unpack__Bool(const char *buffer)
{
    return (*buffer ? sky_True : sky_False);
}


struct sky__struct_align_char_s { char x; char y; };
#define sky__struct_align_char \
        offsetof(struct sky__struct_align_char_s, y)

struct sky__struct_align__Bool_s { char x; _Bool y; };
#define sky__struct_align__Bool \
        offsetof(struct sky__struct_align__Bool_s, y)

struct sky__struct_align_short_s { char x; short y; };
#define sky__struct_align_short \
        offsetof(struct sky__struct_align_short_s, y)

struct sky__struct_align_int_s { char x; int y; };
#define sky__struct_align_int \
        offsetof(struct sky__struct_align_int_s, y)

struct sky__struct_align_long_s { char x; long y; };
#define sky__struct_align_long \
        offsetof(struct sky__struct_align_long_s, y)

struct sky__struct_align_llong_s { char x; long long y; };
#define sky__struct_align_llong \
        offsetof(struct sky__struct_align_llong_s, y)

struct sky__struct_align_size_t_s { char x; size_t y; };
#define sky__struct_align_size_t \
        offsetof(struct sky__struct_align_size_t_s, y)

struct sky__struct_align_float_s { char x; float y; };
#define sky__struct_align_float \
        offsetof(struct sky__struct_align_float_s, y)

struct sky__struct_align_double_s { char x; double y; };
#define sky__struct_align_double \
        offsetof(struct sky__struct_align_double_s, y)

struct sky__struct_align_pointer_s { char x; void *y; };
#define sky__struct_align_pointer \
        offsetof(struct sky__struct_align_pointer_s, y)


#define STRUCT_FMT(_c, _t, _n)  \
    { _c,                       \
      sizeof(_t),               \
      sky__struct_align_##_t,   \
      sky__struct_pack_##_n,    \
      sky__struct_unpack_##_n,  \
    }

/* Native endian, native size, native alignment. */
static sky__struct_format_t sky__struct_native_table_data[] = {
    STRUCT_FMT('c', char, char),
    STRUCT_FMT('b', char, schar),
    STRUCT_FMT('B', char, uchar),
    STRUCT_FMT('?', _Bool, _Bool),
    STRUCT_FMT('h', short, sshort),
    STRUCT_FMT('H', short, ushort),
    STRUCT_FMT('i', int, sint),
    STRUCT_FMT('I', int, uint),
    STRUCT_FMT('l', long, slong),
    STRUCT_FMT('L', long, ulong),
    STRUCT_FMT('q', llong, sllong),
    STRUCT_FMT('Q', llong, ullong),
    STRUCT_FMT('n', size_t, ssize_t),
    STRUCT_FMT('N', size_t, size_t),
    STRUCT_FMT('f', float, float),
    STRUCT_FMT('d', double, double),
    STRUCT_FMT('P', pointer, pointer),
    { 0, 0, 0, NULL, NULL }
};

#undef STRUCT_FMT


#define STRUCT_FMT(_c, _s, _n)  \
    { _c,                       \
      _s,                       \
      1,                        \
      sky__struct_pack_##_n,    \
      sky__struct_unpack_##_n,  \
    }

/* Big endian, standard size, no alignment. */
static sky__struct_format_t sky__struct_little_table_data[] = {
    STRUCT_FMT('c', 1, char),
    STRUCT_FMT('b', 1, schar),
    STRUCT_FMT('B', 1, uchar),
    STRUCT_FMT('?', 1, _Bool),
    STRUCT_FMT('h', 2, int16_le),
    STRUCT_FMT('H', 2, uint16_le),
    STRUCT_FMT('i', 4, int32_le),
    STRUCT_FMT('I', 4, uint32_le),
    STRUCT_FMT('l', 4, int32_le),
    STRUCT_FMT('L', 4, uint32_le),
    STRUCT_FMT('q', 8, int64_le),
    STRUCT_FMT('Q', 8, uint64_le),
    STRUCT_FMT('f', 4, float_le),
    STRUCT_FMT('d', 8, double_le),
    { 0, 0, 0, NULL, NULL }
};

/* Little endian, standard size, no alignment. */
static sky__struct_format_t sky__struct_big_table_data[] = {
    STRUCT_FMT('c', 1, char),
    STRUCT_FMT('b', 1, schar),
    STRUCT_FMT('B', 1, uchar),
    STRUCT_FMT('?', 1, _Bool),
    STRUCT_FMT('h', 2, int16_be),
    STRUCT_FMT('H', 2, uint16_be),
    STRUCT_FMT('i', 4, int32_be),
    STRUCT_FMT('I', 4, uint32_be),
    STRUCT_FMT('l', 4, int32_be),
    STRUCT_FMT('L', 4, uint32_be),
    STRUCT_FMT('q', 8, int64_be),
    STRUCT_FMT('Q', 8, uint64_be),
    STRUCT_FMT('f', 4, float_be),
    STRUCT_FMT('d', 8, double_be),
    { 0, 0, 0, NULL, NULL }
};

#undef STRUCT_FMT

static sky__struct_format_t sky__struct_common_table_data[] = {
    { 'x', 1, 1, NULL, NULL },
    { 'p', 0, 1, NULL, NULL },
    { 's', 0, 1, NULL, NULL },
    { 0, 0, 0, NULL, NULL }
};


static void
sky__struct_table_initialize(
                sky__struct_format_t ** table,
                sky__struct_format_t *  data)
{
    sky__struct_format_t    *d;

    memset(table, 0, 128 * sizeof(sky__struct_format_t *));
    for (d = sky__struct_common_table_data; d->fmt; ++d) {
        table[d->fmt] = d;
    }
    while (data->fmt) {
        table[data->fmt] = data;
        ++data;
    }
}


const char *skython_module__struct_doc =
"Functions to convert between Python values and C structs.\n"
"Python bytes objects are used to hold the data representing the C struct\n"
"and also as format strings (explained below) to describe the layout of data\n"
"in the C struct.\n"
"\n"
"The optional first format char indicates byte order, size and alignment:\n"
"  @: native order, size & alignment (default)\n"
"  =: native order, std. size & alignment\n"
"  <: little-endian, std. size & alignment\n"
"  >: big-endian, std. size & alignment\n"
"  !: same as >\n"
"\n"
"The remaining chars indicate types of args and must match exactly;\n"
"these can be preceded by a decimal repeat count:\n"
"  x: pad byte (no data); c:char; b:signed byte; B:unsigned byte;\n"
"  ?: _Bool (requires C99; if not available, char is used instead)\n"
"  h:short; H:unsigned short; i:int; I:unsigned int;\n"
"  l:long; L:unsigned long; f:float; d:double.\n"
"Special cases (preceding decimal count indicates length):\n"
"  s:string (array of char); p: pascal string (with count byte).\n"
"Special cases (only available in native format):\n"
"  n:ssize_t; N:size_t;\n"
"  P:an integer type that is wide enough to hold a pointer.\n"
"Special case (not in native mode unless 'long long' in platform C):\n"
"  q:long long; Q:unsigned long long\n"
"Whitespace between formats is ignored.\n"
"\n"
"The variable struct.error is an exception raised on errors.\n";


void
skython_module__struct_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky__struct_table_initialize(sky__struct_big_table,
                                 sky__struct_big_table_data);
#if SKY_ENDIAN == SKY_ENDIAN_BIG
    sky__struct_table_initialize(sky__struct_host_table,
                                 sky__struct_big_table_data);
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
    sky__struct_table_initialize(sky__struct_host_table,
                                 sky__struct_little_table_data);
#else
#   error implementation missing
#endif
    sky__struct_table_initialize(sky__struct_little_table,
                                 sky__struct_little_table_data);
    sky__struct_table_initialize(sky__struct_native_table,
                                 sky__struct_native_table_data);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__struct_error),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("error"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_Exception),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "error", sky__struct_error);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__struct_Struct_instance_finalize;
    template.visit = sky__struct_Struct_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("Struct"),
                                   sky__struct_Struct_type_doc,
                                   0,
                                   sizeof(sky__struct_Struct_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmembers(type,
            "format",   "struct format string",
                        offsetof(sky__struct_Struct_data_t, format),
                        SKY_DATA_TYPE_OBJECT_BYTES,
                        SKY_MEMBER_FLAG_READONLY,
            "size",     "struct size in bytes",
                        offsetof(sky__struct_Struct_data_t, size),
                        SKY_DATA_TYPE_SSIZE_T,
                        SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__struct_Struct_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "format", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(type,
            "__sizeof__", sky__struct_Struct_sizeof,
            NULL);
    sky_type_setattr_builtin(
            type,
            "pack",
            sky_function_createbuiltin(
                    "Struct.pack",
                    sky__struct_Struct_pack_doc,
                    (sky_native_code_function_t)sky__struct_Struct_pack,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "pack_into",
            sky_function_createbuiltin(
                    "Struct.pack_into",
                    sky__struct_Struct_pack_into_doc,
                    (sky_native_code_function_t)sky__struct_Struct_pack_into,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "unpack",
            sky_function_createbuiltin(
                    "Struct.unpack",
                    sky__struct_Struct_unpack_doc,
                    (sky_native_code_function_t)sky__struct_Struct_unpack,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "unpack_from",
            sky_function_createbuiltin(
                    "Struct.unpack_from",
                    sky__struct_Struct_unpack_from_doc,
                    (sky_native_code_function_t)sky__struct_Struct_unpack_from,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "offset", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__struct_Struct_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "Struct", type);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__struct_cache),
            sky_dict_create(),
            SKY_TRUE);

    sky_module_setattr(
            module,
            "calcsize",
            sky_function_createbuiltin(
                    "calcsize",
                    sky__struct_calcsize_doc,
                    (sky_native_code_function_t)sky__struct_calcsize,
                    SKY_DATA_TYPE_SSIZE_T,
                    "fmt", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "_clearcache",
            sky_function_createbuiltin(
                    "_clearcache",
                    sky__struct__clearcache_doc,
                    (sky_native_code_function_t)sky__struct__clearcache,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "pack",
            sky_function_createbuiltin(
                    "pack",
                    sky__struct_pack_doc,
                    (sky_native_code_function_t)sky__struct_pack,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "fmt", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "pack_into",
            sky_function_createbuiltin(
                    "pack_into",
                    sky__struct_pack_into_doc,
                    (sky_native_code_function_t)sky__struct_pack_into,
                    SKY_DATA_TYPE_VOID,
                    "fmt", SKY_DATA_TYPE_OBJECT, NULL,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "unpack",
            sky_function_createbuiltin(
                    "unpack",
                    sky__struct_unpack_doc,
                    (sky_native_code_function_t)sky__struct_unpack,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "fmt", SKY_DATA_TYPE_OBJECT, NULL,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "unpack_from",
            sky_function_createbuiltin(
                    "unpack_from",
                    sky__struct_unpack_from_doc,
                    (sky_native_code_function_t)sky__struct_unpack_from,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "fmt", SKY_DATA_TYPE_OBJECT, NULL,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "offset", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
}
