#include "../core/skython.h"


static sky_type_t sky__collections_defaultdict_type;

static const char sky__collections_defaultdict_type_doc[] =
"defaultdict(default_factory[, ...])) --> dict with default factory\n\n"
"The default factory is called without arguments to produce\n"
"a new value when a key is not present, in __getitem__ only.\n"
"A defaultdict compares equal to a dict with the same items.\n"
"All remaining arguments are treated the same as if they were\n"
"passed to the dict constructor, including keyword arguments.\n";

typedef struct sky__collections_defaultdict_data_s {
    sky_object_t                        default_factory;
} sky__collections_defaultdict_data_t;

SKY_EXTERN_INLINE sky__collections_defaultdict_data_t *
sky__collections_defaultdict_data(sky_object_t object)
{
    return sky_object_data(object, sky__collections_defaultdict_type);
}

static void
sky__collections_defaultdict_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__collections_defaultdict_data_t *self_data = data;

    sky_object_visit(self_data->default_factory, visit_data);
}


static sky_object_t
sky__collections_defaultdict_copy(sky_object_t self)
{
    sky__collections_defaultdict_data_t *self_data;

    self_data = sky__collections_defaultdict_data(self);
    return sky_object_call(sky_object_type(self),
                           sky_tuple_pack(2, self_data->default_factory, self),
                           NULL);
}

static const char sky__collections_defaultdict_copy_doc[] =
"D.copy() -> a shallow copy of D.";


static void
sky__collections_defaultdict_init(
                sky_object_t    self,
                sky_tuple_t     args,
                sky_dict_t      kws)
{
    sky_object_t                        default_factory;
    sky__collections_defaultdict_data_t *self_data;

    self_data = sky__collections_defaultdict_data(self);
    if (sky_object_bool(args)) {
        default_factory = sky_tuple_get(args, 0);
        if (!sky_object_callable(default_factory)) {
            sky_error_raise_string(sky_TypeError,
                                   "first argument must be callable");
        }
        self_data->default_factory = default_factory;
        args = sky_tuple_slice(args, 1, SSIZE_MAX, 1);
    }

    sky_dict_init(self, args, kws);
}


static sky_object_t
sky__collections_defaultdict_missing(
                sky_object_t    self,
                sky_object_t    key)
{
    sky_object_t                        value;
    sky__collections_defaultdict_data_t *self_data;

    self_data = sky__collections_defaultdict_data(self);
    if (sky_object_isnull(self_data->default_factory)) {
        sky_error_raise_object(sky_KeyError, sky_tuple_pack(1, key));
    }
    value = sky_object_call(self_data->default_factory, NULL, NULL);
    sky_object_setitem(self, key, value);

    return value;
}

static const char sky__collections_defaultdict_missing_doc[] =
"__missing__(key) # Called by __getitem__ for missing key; pseudo-code:\n"
"  if self.default_factory is None: raise KeyError((key,))\n"
"  self[key] = value = self.default_factory()\n"
"  return value\n";


static sky_object_t
sky__collections_defaultdict_reduce(sky_object_t self)
{
    sky_object_t                        args, items;
    sky__collections_defaultdict_data_t *self_data;

    self_data = sky__collections_defaultdict_data(self);
    if (sky_object_isnull(self_data->default_factory)) {
        args = sky_tuple_empty;
    }
    else {
        args = sky_tuple_pack(1, self_data->default_factory);
    }
    items = sky_object_callmethod(self,
                                  SKY_STRING_LITERAL("items"),
                                  NULL,
                                  NULL);

    return sky_tuple_pack(5, sky_object_type(self),
                             args,
                             sky_None,
                             sky_None,
                             sky_object_iter(items));
}


static sky_string_t
sky__collections_defaultdict_repr(sky_object_t self)
{
    sky_string_t                        base_repr, factory_repr;
    sky__collections_defaultdict_data_t *self_data;

    base_repr = sky_dict_repr(self);

    self_data = sky__collections_defaultdict_data(self);
    if (sky_object_isnull(self_data->default_factory)) {
        factory_repr = sky_object_repr(sky_None);
    }
    else if (sky_object_stack_push(self)) {
        factory_repr = SKY_STRING_LITERAL("...");
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);
            factory_repr = sky_object_repr(self_data->default_factory);
        } SKY_ASSET_BLOCK_END;
    }

    return sky_string_createfromformat("defaultdict(%@, %@)",
                                       factory_repr,
                                       base_repr);
}




static void
sky__collections__count_elements(
                sky_object_t    mapping,
                sky_object_t    iterable)
{
    sky_object_t    iter, key, value;

    iter = sky_object_iter(iterable);
    if (sky_dict_type == sky_object_type(mapping)) {
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            if (!(value = sky_dict_get(mapping, key, NULL))) {
                value = sky_integer_one;
            }
            else {
                value = sky_number_add(value, sky_integer_one);
            }
            sky_dict_setitem(mapping, key, value);
        }
    }
    else {
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            SKY_ERROR_TRY {
                value = sky_object_getitem(mapping, key);
            } SKY_ERROR_EXCEPT(sky_KeyError) {
                value = sky_integer_zero;
            } SKY_ERROR_TRY_END;
            sky_object_setitem(mapping,
                               key,
                               sky_number_add(value, sky_integer_one));
        }
    }
}

static const char sky__collections__count_elements_doc[] =
"_count_elements(mapping, iterable) -> None\n\n"
"Count elements in the iterable, updating the mapping";


void
skython_module__collections_initialize(sky_module_t module)
{
    static const char doc[] =
"High performance data structures.\n"
"- deque:        ordered collection accessible from endpoints only\n"
"- defaultdict:  dict subclass with a default value factory\n";

    sky_type_t          type;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__collections_defaultdict_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("defaultdict"),
                    sky__collections_defaultdict_type_doc,
                    0,
                    sizeof(sky__collections_defaultdict_data_t),
                    0,
                    sky_tuple_pack(1, sky_dict_type),
                    &template);

    sky_type_setattr_member(
            type,
            "default_factory",
            "Factory for default value called by __missing__().",
            offsetof(sky__collections_defaultdict_data_t, default_factory),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_DELETEABLE |
            SKY_MEMBER_FLAG_ALLOW_NONE |
            SKY_MEMBER_FLAG_NONE_IS_NULL);

    sky_type_setattr_builtin(
            type,
            "__missing__",
            sky_function_createbuiltin(
                    "__missing__",
                    sky__collections_defaultdict_missing_doc,
                    (sky_native_code_function_t)sky__collections_defaultdict_missing,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "__copy__",
            sky_function_createbuiltin(
                    "__copy__",
                    sky__collections_defaultdict_copy_doc,
                    (sky_native_code_function_t)sky__collections_defaultdict_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "copy",
                    sky__collections_defaultdict_copy_doc,
                    (sky_native_code_function_t)sky__collections_defaultdict_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_type_setmethodslots(type,
            "__init__", sky__collections_defaultdict_init,
            "__repr__", sky__collections_defaultdict_repr,
            "__reduce__", sky__collections_defaultdict_reduce,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__collections_defaultdict_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "defaultdict", type);


    sky_module_setattr(module,
                       "deque",
                       sky_deque_type);
    sky_module_setattr(module,
                       "_deque_iterator",
                       sky_deque_iterator_type);
    sky_module_setattr(module,
                       "_deque_reverse_iterator",
                       sky_deque_reverse_iterator_type);


    sky_module_setattr(
            module,
            "_count_elements",
            sky_function_createbuiltin(
                    "_count_elements",
                    sky__collections__count_elements_doc,
                    (sky_native_code_function_t)sky__collections__count_elements,
                    SKY_DATA_TYPE_VOID,
                    "mapping", SKY_DATA_TYPE_OBJECT, NULL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
}
