#include "../core/sky_private.h"

#include "module__io.h"

#if defined(HAVE_SYS_PARAM_H)
#   include <sys/param.h>
#endif
#include <sys/stat.h>


static inline void
sky__io_FileIO__checkClosed(sky_object_t self)
{
    if (sky__io_FileIO_data(self)->fd < 0) {
        sky_error_raise_string(sky_ValueError,
                               "I/O operation on closed file");
    }
}


static inline void
sky__io_FileIO__checkReadable(sky_object_t self)
{
    if (!sky__io_FileIO_data(self)->readable) {
        sky_error_raise_string(sky_ValueError,
                               "File not open for reading");
    }
}


static inline void
sky__io_FileIO__checkWritable(sky_object_t self)
{
    if (!sky__io_FileIO_data(self)->writable) {
        sky_error_raise_string(sky_ValueError,
                               "File not open for writing");
    }
}


static void
sky__io_FileIO__close(sky__io_FileIO_data_t *fileio_data)
{
    int fd = fileio_data->fd;

    fileio_data->fd = -1;
    if (fileio_data->closefd && fd != -1) {
        if (close(fd) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    }
}


static void
sky__io_FileIO_instance_initialize(SKY_UNUSED sky_object_t  object, void *data)
{
    sky__io_FileIO_data_t   *fileio_data = data;

    fileio_data->fd = -1;
}


static const char sky__io_FileIO_close_doc[] =
"close() -> None.  Close the file.\n"
"\n"
"A closed file cannot be used for further I/O operations.  close() may be\n"
"called more than once without error.  Changes the fileno to -1.";


static void
sky__io_FileIO_close(sky_object_t self)
{
    sky__io_FileIO__close(sky__io_FileIO_data(self));
}


static sky_object_t
sky__io_FileIO_closed_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return (sky__io_FileIO_data(self)->fd < 0 ? sky_True : sky_False);
}


static sky_object_t
sky__io_FileIO_closefd_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return (sky__io_FileIO_data(self)->closefd ? sky_True : sky_False);
}


static const char sky__io_FileIO_fileno_doc[] =
"fileno() -> int. \"file descriptor\".\n"
"\n"
"This is needed for lower-level file interfaces, such as the fcntl module.";

static int
sky__io_FileIO_fileno(sky_object_t self)
{
    sky__io_FileIO__checkClosed(self);
    return sky__io_FileIO_data(self)->fd;
}


static const char sky__io_FileIO_isatty_doc[] =
"isatty() -> bool.  True if the file is connected to a tty device.";

static sky_bool_t
sky__io_FileIO_isatty(sky_object_t self)
{
    sky__io_FileIO__checkClosed(self);
    return (isatty(sky__io_FileIO_data(self)->fd) ? SKY_TRUE : SKY_FALSE);
}


static sky_object_t
sky__io_FileIO_mode_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky__io_FileIO_data_t   *self_data = sky__io_FileIO_data(self);

    if (self_data->created) {
        if (self_data->readable) {
            return SKY_STRING_LITERAL("xb+");
        }
        return SKY_STRING_LITERAL("xb");
    }
    if (self_data->readable) {
        if (self_data->writable) {
            return SKY_STRING_LITERAL("rb+");
        }
        return SKY_STRING_LITERAL("rb");
    }
    return SKY_STRING_LITERAL("wb");
}


static sky_object_t
sky__io_FileIO_getstate(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "cannot serialize %#@ object",
                           sky_type_name(sky_object_type(self)));
}


static void
sky__io_FileIO_init(sky_object_t    self,
                    sky_object_t    file,
                    sky_string_t    mode,
                    sky_bool_t      closefd,
                    sky_object_t    opener)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    int             fd, flags;
    char            *cfilename;
    sky_bool_t      crwa, plus;
    const char      *c, *cmode;
    struct stat     st;
    sky_buffer_t    buffer;
    sky_object_t    filename, value;

    /* If there's already an open file descriptor for this file object,
     * make sure to close it first; otherwise, the file descriptor will
     * likely be leaked (not if closefd is True, but that's rare).
     */
    sky__io_FileIO__close(fileio_data);

    fd = -1;
    if (sky_object_isa(file, sky_integer_type)) {
        fd = sky_integer_value(file, INT_MIN, INT_MAX, NULL);
        if (fd < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "negative file descriptor");
        }
    }
    else if (sky_object_isa(file, sky_string_type)) {
        filename = sky_codec_encodefilename(file);
    }
    else if (!sky_buffer_check(file)) {
        sky_error_raise_format(sky_TypeError,
                               "expected int or str; got %#@",
                               sky_type_name(sky_object_type(file)));
    }
    else {
        filename = file;
    }

    fileio_data->created = 0;
    fileio_data->readable = 0;
    fileio_data->seekable = 0;
    fileio_data->seekable_set = 0;
    fileio_data->writable = 0;
    fileio_data->appending = 0;
    if (!(cmode = sky_string_cstring(mode))) {
invalid_mode:
        sky_error_raise_string(sky_ValueError,
                "Must have exactly one of create/read/write/append mode and "
                "at most one plus");
    }
    flags = 0;
    crwa = plus = SKY_FALSE;
    for (c = cmode; *c; ++c) {
        switch (*c) {
            case '+':
                if (plus) {
                    goto invalid_mode;
                }
                plus = SKY_TRUE;
                fileio_data->readable = fileio_data->writable = 1;
                break;
            case 'a':
                if (crwa) {
                    goto invalid_mode;
                }
                crwa = SKY_TRUE;
                fileio_data->writable = 1;
                fileio_data->appending = 1;
                flags |= O_APPEND | O_CREAT;
                break;
            case 'r':
                if (crwa) {
                    goto invalid_mode;
                }
                crwa = SKY_TRUE;
                fileio_data->readable = 1;
                break;
            case 'w':
                if (crwa) {
                    goto invalid_mode;
                }
                crwa = SKY_TRUE;
                fileio_data->writable = 1;
                flags |= O_CREAT | O_TRUNC;
                break;
            case 'x':
                if (crwa) {
                    goto invalid_mode;
                }
                crwa = SKY_TRUE;
                fileio_data->created = fileio_data->writable = 1;
                flags |= O_EXCL | O_CREAT;
                break;
        }
    }
    if (!crwa) {
        goto invalid_mode;
    }
    if (fileio_data->readable && fileio_data->writable) {
        flags |= O_RDWR;
    }
    else if (fileio_data->readable) {
        flags |= O_RDONLY;
    }
    else {
        flags |= O_WRONLY;
    }

#if defined(O_BINARY)
    flags |= O_BINARY;
#endif

    SKY_ASSET_BLOCK_BEGIN {
        if (fd >= 0) {
            if (fstat(fd, &st) == -1 && EBADF == errno) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (!fstat(fileio_data->fd, &st) && S_ISDIR(st.st_mode)) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            fileio_data->fd = fd;
            fileio_data->closefd = (closefd ? 1 : 0);
            if (closefd) {
                sky_asset_save(self,
                               (sky_free_t)sky__io_FileIO_close,
                               SKY_ASSET_CLEANUP_ON_ERROR);
            }
        }
        else {
            fileio_data->closefd = 1;
            if (!closefd) {
                sky_error_raise_string(sky_ValueError,
                                       "Cannot use closefd=False with file name");
            }

            errno = 0;
            if (sky_object_isnull(opener)) {
                sky_buffer_acquire(&buffer, filename, SKY_BUFFER_FLAG_SIMPLE);
                cfilename = sky_asset_strndup(buffer.buf,
                                              buffer.len,
                                              SKY_ASSET_CLEANUP_ALWAYS);
                sky_buffer_release(&buffer);
                fileio_data->fd = open(cfilename, flags, 0666);
            }
            else {
                value = sky_object_call(opener,
                                        sky_object_build("(Oi)", file, flags),
                                        NULL);
                fileio_data->fd = sky_integer_value(value,
                                                    INT_MIN,
                                                    INT_MAX,
                                                    NULL);
            }
            sky_asset_save(self,
                           (sky_free_t)sky__io_FileIO_close,
                           SKY_ASSET_CLEANUP_ON_ERROR);

            if (fileio_data->fd < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (!fstat(fileio_data->fd, &st) && S_ISDIR(st.st_mode)) {
                /* sky_error_raise_errno_filename(sky_OSError, errno, file); */
                sky_error_raise_errno(sky_OSError, errno);
            }
        }

        sky_object_setattr(self, SKY_STRING_LITERAL("name"), file);
        if (fileio_data->appending) {
            sky__io_FileIO_seek_native(self, 0, 2);
        }
    } SKY_ASSET_BLOCK_END;
}


static const char sky__io_FileIO_readall_doc[] =
"readall() -> bytes.  read all data from the file, returned as bytes.\n"
"\n"
"In non-blocking mode, returns as much as is immediately available,\n"
"or None if no data is available.  On end-of-file, returns ''.";

static sky_bytes_t
sky__io_FileIO_readall(sky_object_t self)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    int             errno_code;
    char            *memory;
    size_t          new_size;
    ssize_t         nread, offset, size, used;
    struct stat     st;
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_FileIO__checkClosed(self);
        sky__io_FileIO__checkReadable(self);

        if (!fileio_data->seekable) {
            size = SKY__IO_DEFAULT_BUFFER_SIZE;
        }
        else {
            offset = sky__io_FileIO_seek_native(self, 0, 1);
            if (fstat(fileio_data->fd, &st) == -1) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (offset > st.st_size) {
                size = SKY__IO_DEFAULT_BUFFER_SIZE;
            }
            else {
                size = (st.st_size + 1) - offset;
            }
        }
        memory = sky_asset_malloc(size, SKY_ASSET_CLEANUP_ON_ERROR);
        size = sky_memsize(memory) - 1;

        used = 0;
        result = NULL;
        for (;;) {
            nread = read(fileio_data->fd, memory + used, size - used);
            errno_code = errno;

            if (nread < 0) {
                if (EINTR == errno_code) {
                    sky_signal_check_pending();
                    continue;
                }
                if (used) {
                    break;
                }
                if (EAGAIN != errno_code) {
                    sky_error_raise_errno(sky_OSError, errno_code);
                }
                result = sky_None;
                break;
            }
            if (!nread) {
                break;
            }
            if (nread > 0) {
                used += nread;
                if (size - used < SKY__IO_DEFAULT_BUFFER_SIZE) {
                    new_size = ((used + SKY__IO_DEFAULT_BUFFER_SIZE - 1) / SKY__IO_DEFAULT_BUFFER_SIZE) * SKY__IO_DEFAULT_BUFFER_SIZE;
                    new_size += SKY__IO_DEFAULT_BUFFER_SIZE;
                    if (new_size >= SSIZE_MAX) {
                        sky_error_raise_string(
                                sky_OverflowError,
                                "unbounded read returned more "
                                "bytes than a string can hold.");
                    }
                    memory = sky_asset_realloc(memory,
                                               new_size,
                                               SKY_ASSET_CLEANUP_ON_ERROR,
                                               SKY_ASSET_UPDATE_FIRST_CURRENT);
                    size = sky_memsize(memory) - 1;
                }
            }
        }

        if (!result) {
            memory[used] = '\0';
            if ((used + 1) < size / 2) {
                memory = sky_asset_realloc(memory,
                                           used + 1,
                                           SKY_ASSET_CLEANUP_ON_ERROR,
                                           SKY_ASSET_UPDATE_FIRST_CURRENT);
            }
            result = sky_bytes_createwithbytes(memory, used, sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io_FileIO_read_doc[] =
"read(n: int) -> bytes.  read at most n bytes, returned as bytes.\n"
"\n"
"Only makes one system call, so less data may be returned than requested\n"
"In non-blocking mode, returns None if no data is available.\n"
"On end-of-file, returns ''.";

static sky_bytes_t
sky__io_FileIO_read(sky_object_t self, ssize_t n)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    int             errno_code;
    char            *memory;
    ssize_t         nread;
    sky_object_t    result;

    if (n < 0) {
        return sky__io_FileIO_readall(self);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_FileIO__checkClosed(self);
        sky__io_FileIO__checkReadable(self);

        memory = sky_asset_malloc(n, SKY_ASSET_CLEANUP_ON_ERROR);
        nread = read(fileio_data->fd, memory, n);
        errno_code = errno;

        if (nread >= 0) {
            result = sky_bytes_createwithbytes(memory, nread, sky_free);
        }
        else {
            if (EAGAIN != errno_code) {
                sky_error_raise_errno(sky_OSError, errno_code);
            }
            result = sky_None;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io_FileIO_readable_doc[] =
"readable() -> bool.  True if file was opened in a read mode.";

static sky_bool_t
sky__io_FileIO_readable(sky_object_t self)
{
    sky__io_FileIO__checkClosed(self);
    return (sky__io_FileIO_data(self)->readable ? SKY_TRUE : SKY_FALSE);
}


static const char sky__io_FileIO_readinto_doc[] =
"readinto() -> Same as RawIOBase.readinto().";

static sky_object_t
sky__io_FileIO_readinto(sky_object_t self, sky_object_t b)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    int             errno_code;
    ssize_t         nread;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_FileIO__checkClosed(self);
        sky__io_FileIO__checkReadable(self);

        sky_buffer_acquire(&buffer, b, SKY_BUFFER_FLAG_SIMPLE |
                                       SKY_BUFFER_FLAG_WRITABLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nread = read(fileio_data->fd, buffer.buf, buffer.len);
        errno_code = errno;
    } SKY_ASSET_BLOCK_END;

    if (nread < 0) {
        if (EAGAIN == errno_code) {
            return sky_None;
        }
        sky_error_raise_errno(sky_OSError, errno_code);
    }
    return sky_integer_create(nread);
}


ssize_t
sky__io_FileIO_readinto_native(sky_object_t self, void *bytes, ssize_t nbytes)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    ssize_t nread;

    sky_error_validate_debug(bytes != NULL);
    sky_error_validate_debug(nbytes >= 0);

    sky__io_FileIO__checkClosed(self);
    sky__io_FileIO__checkReadable(self);

    if ((nread = read(fileio_data->fd, bytes, nbytes)) < 0) {
        if (EAGAIN == errno) {
            return -1;
        }
        if (EINTR == errno) {
            sky_signal_check_pending();
            return -1;
        }
        sky_error_raise_errno(sky_OSError, errno);
    }
    return nread;
}


static sky_string_t
sky__io_FileIO_repr(sky_object_t self)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    const char      *mode_string;
    sky_string_t    name, repr;

    if (fileio_data->fd < 0) {
        repr = SKY_STRING_LITERAL("<_io.FileIO [closed]>");
    }
    else {
        if (fileio_data->created) {
            mode_string = (fileio_data->readable ? "xb+" : "xb");
        }
        else if (fileio_data->appending) {
            mode_string = (fileio_data->readable ? "ab+" : "ab");
        }
        else if (fileio_data->readable) {
            mode_string = (fileio_data->writable ? "rb+" : "rb");
        }
        else {
            mode_string = "wb";
        }

        if (!(name = sky_object_getattr(self, SKY_STRING_LITERAL("name"), NULL))) {
            repr = sky_string_createfromformat(
                            "<_io.FileIO fd=%d mode='%s'>",
                            fileio_data->fd,
                            mode_string);
        }
        else {
            repr = sky_string_createfromformat(
                            "<_io.FileIO name=%#@ mode='%s'>",
                            name,
                            mode_string);
        }
    }

    return repr;
}


static const char sky__io_FileIO_seek_doc[] =
"seek(offset: int[, whence: int]) -> None.  Move to new file position.\n"
"\n"
"Argument offset is a byte count.  Optional argument whence defaults to\n"
"0 (offset from start of file, offset should be >= 0); other values are 1\n"
"(move relative to current position, positive or negative), and 2 (move\n"
"relative to end of file, usually negative, although many platforms allow\n"
"seeking beyond the end of a file)."
"\n"
"Note that not all file objects are seekable.";

static ssize_t
sky__io_FileIO_seek(sky_object_t self, ssize_t offset, int whence)
{
    sky__io_FileIO__checkClosed(self);
    return sky__io_FileIO_seek_native(self, offset, whence);
}


ssize_t
sky__io_FileIO_seek_native(sky_object_t  self, ssize_t offset, int whence)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    ssize_t result;

    if (whence < 0 || whence > 2) {
        sky_error_raise_string(sky_ValueError, "whence must be 0, 1, or 2");
    }
#if SEEK_SET != 0 || SEEK_CUR != 1 || SEEK_END != 2
    switch (whence) {
#   if SEEK_SET != 0
        case 0:
            whence = SEEK_SET;
            break;
#   endif
#   if SEEK_CUR != 1
        case 1:
            whence = SEEK_CUR;
            break;
#   endif
#   if SEEK_SET != 2
        case 2:
            whence = SEEK_END;
            break;
#   endif
    }
#endif

    if ((result = lseek(fileio_data->fd, offset, whence)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    if (!fileio_data->seekable_set) {
        fileio_data->seekable_set = 1;
        fileio_data->seekable = 1;
    }

    return result;
}


static const char sky__io_FileIO_seekable_doc[] =
"seekable() -> bool.  True if file supports random-access.";

static sky_bool_t
sky__io_FileIO_seekable(sky_object_t self)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    sky__io_FileIO__checkClosed(self);
    if (!fileio_data->seekable_set) {
        fileio_data->seekable_set = 1;
        if (lseek(fileio_data->fd, 0, SEEK_CUR) == -1) {
            fileio_data->seekable = 0;
        }
        else {
            fileio_data->seekable = 1;
        }
    }
    return (fileio_data->seekable ? SKY_TRUE : SKY_FALSE);
}


static const char sky__io_FileIO_tell_doc[] =
"tell() -> int.  Current file position";

static ssize_t
sky__io_FileIO_tell(sky_object_t self)
{
    return sky__io_FileIO_seek(self, 0, 1);
}


static const char sky__io_FileIO_truncate_doc[] =
"truncate([size: int]) -> None.  Truncate the file to at most size bytes.\n"
"\n"
"Size defaults to the current file position, as returned by tell().\n"
"The current file position is changed to the value of size.";

static void
sky__io_FileIO_truncate(sky_object_t self, sky_object_t size)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    ssize_t pos;

    sky__io_FileIO__checkClosed(self);

    if (sky_object_isnull(size)) {
        pos = sky__io_FileIO_seek(self, 0, 1);
    }
    else {
        pos = sky_integer_value(size, SSIZE_MIN, SSIZE_MAX, NULL);
    }
    if (ftruncate(fileio_data->fd, pos) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}


static const char sky__io_FileIO_writable_doc[] =
"writable() -> bool.  True if file was opened in a write mode.";

static sky_bool_t
sky__io_FileIO_writable(sky_object_t self)
{
    sky__io_FileIO__checkClosed(self);

    return (sky__io_FileIO_data(self)->writable ? SKY_TRUE : SKY_FALSE);
}


static const char sky__io_FileIO_write_doc[] =
"write(b: bytes) -> int.  Write bytes b to file, return number written.\n"
"\n"
"Only makes one system call, so not all of the data may be written.\n"
"The number of bytes actually written is returned.";

static sky_object_t
sky__io_FileIO_write(sky_object_t self, sky_object_t b)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    int             errno_code;
    ssize_t         nwritten;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_FileIO__checkClosed(self);
        sky__io_FileIO__checkWritable(self);

        sky_buffer_acquire(&buffer, b, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nwritten = write(fileio_data->fd, buffer.buf, buffer.len);
        errno_code = errno;
    } SKY_ASSET_BLOCK_END;

    if (nwritten < 0) {
        if (EAGAIN == errno) {
            return sky_None;
        }
        sky_error_raise_errno(sky_OSError, errno_code);
    }
    return sky_integer_create(nwritten);
}


ssize_t
sky__io_FileIO_write_native(
                sky_object_t    self,
                const void *    bytes,
                ssize_t         nbytes)
{
    sky__io_FileIO_data_t   *fileio_data = sky__io_FileIO_data(self);

    ssize_t nwritten;

    sky_error_validate_debug(bytes != NULL);
    sky_error_validate_debug(nbytes >= 0);

    sky__io_FileIO__checkClosed(self);
    sky__io_FileIO__checkWritable(self);

    if ((nwritten = write(fileio_data->fd, bytes, nbytes)) < 0) {
        if (EAGAIN == errno) {
            return -1;
        }
        sky_error_raise_errno(sky_OSError, errno);
    }
    return nwritten;
}


sky_type_t sky__io_FileIO_type = NULL;

sky_type_t
sky__io_FileIO_initialize(void)
{
    static const char type_doc[] =
"file(name: str[, mode: str][, opener: None]) -> file IO object\n"
"\n"
"Open a file.  The mode can be 'r', 'w', 'x' or 'a' for reading (default),\n"
"writing, exclusive creation or appending.  The file will be created if it\n"
"doesn't exist when opened for writing or appending; it will be truncated\n"
"when opened for writing.  A `FileExistsError` will be raised if it already\n"
"exists when opened for creating. Opening a file for creating implies\n"
"writing so this mode behaves in a similar way to 'w'.Add a '+' to the mode\n"
"to allow simultaneous reading and writing. A custom opener can be used by\n"
"passing a callable as *opener*. The underlying file descriptor for the file\n"
"object is then obtained by calling opener with (*name*, *flags*).\n"
"*opener* must return an open file descriptor (passing os.open as *opener*\n"
"results in functionality similar to passing None).";

    sky_type_t          type;
    sky_tuple_t         bases;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__io_FileIO_instance_initialize;

    bases = sky_tuple_pack(1, sky__io__RawIOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("FileIO"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   sizeof(sky__io_FileIO_data_t),
                                   0,
                                   bases,
                                   &template);

    sky_type_setattr_getset(type,
            "closed",
            "True if the file is closed",
            sky__io_FileIO_closed_getter,
            NULL);
    sky_type_setattr_getset(type,
            "closefd",
            "True if the file descriptor will be closed",
            sky__io_FileIO_closefd_getter,
            NULL);
    sky_type_setattr_getset(type,
            "mode",
            "String giving the file mode",
            sky__io_FileIO_mode_getter,
            NULL);

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "FileIO.close",
                    sky__io_FileIO_close_doc,
                    (sky_native_code_function_t)sky__io_FileIO_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "fileno",
            sky_function_createbuiltin(
                    "FileIO.fileno",
                    sky__io_FileIO_fileno_doc,
                    (sky_native_code_function_t)sky__io_FileIO_fileno,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "isatty",
            sky_function_createbuiltin(
                    "FileIO.isatty",
                    sky__io_FileIO_isatty_doc,
                    (sky_native_code_function_t)sky__io_FileIO_isatty,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "FileIO.read",
                    sky__io_FileIO_read_doc,
                    (sky_native_code_function_t)sky__io_FileIO_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "FileIO.readable",
                    sky__io_FileIO_readable_doc,
                    (sky_native_code_function_t)sky__io_FileIO_readable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readall",
            sky_function_createbuiltin(
                    "FileIO.readall",
                    sky__io_FileIO_readall_doc,
                    (sky_native_code_function_t)sky__io_FileIO_readall,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readinto",
            sky_function_createbuiltin(
                    "FileIO.readinto",
                    sky__io_FileIO_readinto_doc,
                    (sky_native_code_function_t)sky__io_FileIO_readinto,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "FileIO.seek",
                    sky__io_FileIO_seek_doc,
                    (sky_native_code_function_t)sky__io_FileIO_seek,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "FileIO.seekable",
                    sky__io_FileIO_seekable_doc,
                    (sky_native_code_function_t)sky__io_FileIO_seekable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "FileIO.seek",
                    sky__io_FileIO_tell_doc,
                    (sky_native_code_function_t)sky__io_FileIO_tell,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "FileIO.truncate",
                    sky__io_FileIO_truncate_doc,
                    (sky_native_code_function_t)sky__io_FileIO_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "size", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "FileIO.writable",
                    sky__io_FileIO_writable_doc,
                    (sky_native_code_function_t)sky__io_FileIO_writable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "FileIO.write",
                    sky__io_FileIO_write_doc,
                    (sky_native_code_function_t)sky__io_FileIO_write,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_FileIO_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "file", SKY_DATA_TYPE_OBJECT, NULL,
            "mode", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("r"),
            "closefd", SKY_DATA_TYPE_BOOL, sky_True,
            "opener", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);

    sky_type_setmethodslots(type,
            "__repr__", sky__io_FileIO_repr,
            "__getstate__", sky__io_FileIO_getstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_FileIO_type),
            type,
            SKY_TRUE);
    return sky__io_FileIO_type;
}
