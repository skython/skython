#include "../core/sky_private.h"
#include "sky_python_ast.h"
#include "../core/sky_python_compiler.h"


void
skython_module__ast_initialize(sky_module_t module)
{
    sky_module_setattr(module,
                       "PyCF_ONLY_AST",
                       sky_integer_create(SKY_PYTHON_COMPILER_FLAG_ONLY_AST));

    sky_python_ast_initialize_module(module);
}
