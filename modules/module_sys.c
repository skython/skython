#include "../core/sky_private.h"
#include "../core/sky_frame_private.h"

#include "builtin_modules.h"

#include <float.h>
#include <gmp.h>
#include <sys/stat.h>


static double sky_sys_switchinterval = 0.005;


static sky_type_t sky_sys_version_info_type = NULL;


static sky_type_t sky_sys_writer_type = NULL;

typedef struct sky_sys_writer_data_s {
    FILE *                              fp;
} sky_sys_writer_data_t;

SKY_EXTERN_INLINE sky_sys_writer_data_t *
sky_sys_writer_data(sky_object_t object)
{
    return sky_object_data(object, sky_sys_writer_type);
}

static void
sky_sys_writer_write_callback(
                const void *    bytes,
                ssize_t         nbytes,
    SKY_UNUSED  size_t          width,
                void *          arg)
{
    fwrite(bytes, nbytes, 1, (FILE *)arg);
}

static ssize_t
sky_sys_writer_write(sky_object_t self, sky_object_t s)
{
    FILE    *fp = sky_sys_writer_data(self)->fp;

    off_t   offset;

    offset = ftell(fp);
    sky_codec_encodewithcallback(NULL,
                                 sky_object_str(s),
                                 NULL,
                                 NULL,
                                 0,
                                 sky_sys_writer_write_callback,
                                 fp);
    return (ssize_t)(ftell(fp) - offset);
}


static sky_object_t
sky_sys_version_info(uint32_t, uint32_t, uint32_t, const char *, uint32_t);


static sky_list_t
sky_sys_argv(void)
{
    int             argc, i;
    char            **argv;
    sky_list_t      list;
    sky_string_t    s;

    list = sky_list_create(NULL);
    argc = sky_interpreter_argc();
    argv = sky_interpreter_argv();
    if (!argc || !argv) {
        return list;
    }

    for (i = 0; i < argc; ++i) {
        s = sky_string_createfrombytes(argv[i], strlen(argv[i]), NULL, NULL);
        sky_list_append(list, s);
    }

    return list;
}


static sky_list_t
sky_sys_builtin_module_names(void)
{
    sky_list_t              list;
    sky_builtin_module_t    *builtin;

    list = sky_list_create(NULL);
    for (builtin = sky_builtin_modules; builtin->name; ++builtin) {
        if (!builtin->frozen_bytes) {
            sky_list_append(list,
                            sky_string_createfrombytes(builtin->name,
                                                       strlen(builtin->name),
                                                       NULL,
                                                       NULL));
        }
    }

    return list;
}


static void
sky_sys_displayhook(sky_object_t object)
{
    sky_object_t    buffer;
    sky_string_t    encoding;

    sky_bytes_t volatile    bytes;
    sky_object_t volatile   sys_stdout, write_method;
    sky_string_t volatile   text;

    if (sky_object_isnull(object)) {
        return;
    }

    /* Set '_' to None to avoid recursion */
    sky_object_setitem(sky_interpreter_builtins(),
                       SKY_STRING_LITERAL("_"),
                       sky_None);

    sys_stdout = sky_object_getattr(sky_interpreter_module_sys(),
                                    SKY_STRING_LITERAL("stdout"),
                                    sky_NotSpecified);
    write_method = sky_object_getattr(sys_stdout,
                                      SKY_STRING_LITERAL("write"),
                                      sky_NotSpecified);

    text = sky_object_repr(object);
    SKY_ERROR_TRY {
        sky_object_call(write_method, sky_tuple_pack(1, text), NULL);
    } SKY_ERROR_EXCEPT(sky_UnicodeEncodeError) {
        encoding = sky_object_getattr(sys_stdout,
                                      SKY_STRING_LITERAL("encoding"),
                                      sky_NotSpecified);
        bytes = sky_codec_encode(NULL,
                                 text,
                                 encoding,
                                 SKY_STRING_LITERAL("backslashreplace"),
                                 0);
        SKY_ERROR_TRY {
            buffer = sky_object_getattr(sys_stdout,
                                        SKY_STRING_LITERAL("buffer"),
                                        sky_NotSpecified);
            sky_object_callmethod(buffer,
                                  SKY_STRING_LITERAL("write"),
                                  sky_tuple_pack(1, bytes),
                                  NULL);
        } SKY_ERROR_EXCEPT_ANY {
            text = sky_bytes_decode(bytes, encoding, NULL);
            sky_object_call(write_method, sky_tuple_pack(1, text), NULL);
        } SKY_ERROR_TRY_END;
    } SKY_ERROR_TRY_END;
    sky_object_call(write_method,
                    sky_tuple_pack(1, SKY_STRING_LITERAL("\n")),
                    NULL);

    sky_object_setitem(sky_interpreter_builtins(),
                       SKY_STRING_LITERAL("_"),
                       object);
}


static sky_tuple_t
sky_sys_exc_info(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_frame_t         frame;
    sky_frame_data_t    *frame_data;
    sky_frame_block_t   *block;
    sky_error_record_t  *error;

    if (!tlsdata->current_frame) {
        if ((error = sky_error_current()) != NULL) {
            return sky_tuple_pack(3, error->type,
                                     error->value,
                                     error->traceback);
        }
    }
    else {
        for (frame = tlsdata->current_frame;
             frame;
             frame = frame_data->f_back)
        {
            frame_data = sky_frame_data(frame);
            for (block = frame_data->blocks; block; block = block->next) {
                if (block->error) {
                    return sky_tuple_pack(3, block->error->type,
                                             block->error->value,
                                             block->error->traceback);
                }
            }
        }
    }

    return sky_tuple_pack(3, sky_None, sky_None, sky_None);
}


static void
sky_sys_exit(sky_object_t status)
{
    sky_error_raise_object(sky_SystemExit, status);
}


static sky_object_t
sky_sys_flags(void)
{
    static sky_struct_sequence_field_t fields[] = {
        { "debug",                  "-d",           0 },
        { "inspect",                "-i",           0 },
        { "interactive",            "-i",           0 },
        { "optimize",               "-O or -OO",    0 },
        { "dont_write_bytecode",    "-B",           0 },
        { "no_user_site",           "-s",           0 },
        { "no_site",                "-S",           0 },
        { "ignore_environment",     "-E",           0 },
        { "verbose",                "-v",           0 },
        { "bytes_warning",          "-b",           0 },
        { "quiet",                  "-q",           0 },
        { "hash_randomization",     "-R",           0 },
    };

    static const char doc[] =
"sys.flags\n"
"\n"
"Flags provided through command line arguments or environment vars.";

    size_t          i;
    sky_type_t      type;
    sky_object_t    values[sizeof(fields) / sizeof(fields[0])];
    unsigned int    flags;

    type = sky_struct_sequence_define("sys.flags",
                                      doc,
                                      sizeof(fields) / sizeof(fields[0]),
                                      fields);
    for (i = 0; i < sizeof(values) / sizeof(values[0]); ++i) {
        values[i] = sky_integer_zero;
    }
    flags = sky_interpreter_flags();

    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_DEBUG) {
        values[0] = sky_integer_one;
    }
    /* TODO flags.inspect */
    /* TODO flags.interactive */
    values[3] = sky_integer_create(sky_interpreter_optlevel());
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_DONT_WRITE_BYTECODE) {
        values[4] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_USER_SITE) {
        values[5] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_SITE) {
        values[6] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT) {
        values[7] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_VERY_VERBOSE) {
        values[8] = sky_integer_create(2);
    }
    else if (flags & SKY_LIBRARY_INITIALIZE_FLAG_VERBOSE) {
        values[8] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS) {
        values[9] = sky_integer_create(2);
    }
    else if (flags & SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS) {
        values[9] = sky_integer_one;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_QUIET) {
        values[10] = sky_integer_one;
    }
    /* TODO flags.hash_randomization */

    return sky_struct_sequence_create(type,
                                      sizeof(fields) / sizeof(fields[0]),
                                      values);
}


static sky_object_t
sky_sys_float_info(void)
{
    static sky_struct_sequence_field_t fields[] = {
        { "max",        "DBL_MAX -- maximum representable finite float", 0 },
        { "max_exp",    "DBL_MAX_EXP -- maximum int e such that radix**(e-1) "
                        "is representable", 0 },
        { "max_10_exp", "DBL_MAX_10_EXP -- maximum int e such that 10**e is "
                        "representable", 0 },
        { "min",        "DBL_MIN -- Minimum positive normalizer float", 0 },
        { "min_exp",    "DBL_MIN_EXP -- minimum int e such that radix**(e - 1) "
                        "is a normalized float", 0 },
        { "min_10_exp", "DBL_MIN_10_EXP -- minimum int e such that 10**e is a "
                        "normalized float", 0 },
        { "dig",        "DBL_DIG -- digits", 0 },
        { "mant_dig",   "DBL_MANT_DIG -- mantissa digits", 0 },
        { "epsilon",    "DBL_EPSILON -- Difference between 1 and the next "
                        "representable float", 0 },
        { "radix",      "FLT_RADIX -- radix of exponent", 0 },
        { "rounds",     "FLT_ROUNDS -- addition rounds", 0 },
    };

    static const char doc[] =
"sys.float_info\n"
"\n"
"A structseq holding information about the float type. It contains low level\n"
"information about the precision and internal representation. Please study\n"
"your system's :file:`float.h` for more information.";

    sky_type_t      type;
    sky_object_t    values[sizeof(fields) / sizeof(fields[0])];

    type = sky_struct_sequence_define("sys.float_info",
                                      doc,
                                      sizeof(fields) / sizeof(fields[0]),
                                      fields);

    values[0] = sky_float_create(DBL_MAX);
    values[1] = sky_integer_create(DBL_MAX_EXP);
    values[2] = sky_integer_create(DBL_MAX_10_EXP);
    values[3] = sky_float_create(DBL_MIN);
    values[4] = sky_integer_create(DBL_MIN_EXP);
    values[5] = sky_integer_create(DBL_MIN_10_EXP);
    values[6] = sky_integer_create(DBL_DIG);
    values[7] = sky_integer_create(DBL_MANT_DIG);
    values[8] = sky_float_create(DBL_EPSILON);
    values[9] = sky_integer_create(FLT_RADIX);
    values[10] = sky_integer_create(FLT_ROUNDS);

    return sky_struct_sequence_create(type,
                                      sizeof(fields) / sizeof(fields[0]),
                                      values);
}


static sky_string_t
sky_sys_getdefaultencoding(void)
{
    return SKY_STRING_LITERAL("utf-8");
}


static sky_object_t
sky_sys__getframe(ssize_t depth)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_frame_t         frame;
    sky_frame_data_t    *frame_data;

    for (frame = tlsdata->current_frame; frame; frame = frame_data->f_back) {
        frame_data = sky_frame_data(frame);
        if (!sky_object_isa(frame_data->f_code, sky_code_type)) {
            continue;
        }
        if (depth <= 0) {
            return frame;
        }
        --depth;
    }

    sky_error_raise_string(sky_ValueError, "call stack is not deep enough");
}


static size_t
sky_sys_getsizeof(sky_object_t object, SKY_UNUSED sky_object_t dfault)
{
    return sky_object_sizeof(object);
}


static double
sky_sys_getswitchinterval(void)
{
    return sky_sys_switchinterval;
}

static const char sky_sys_getswitchinterval_doc[] =
"getswitchinterval() -> current thread switch interval; see setswitchinterval().";


static sky_integer_t
sky_sys_hexversion(
                uint32_t        major,
                uint32_t        minor,
                uint32_t        micro,
                const char *    releaselevel,
                uint32_t        serial)
{
    uint32_t    hexversion;

    hexversion = (major << 24) | (minor << 16) | (micro << 8) | serial;
    switch (releaselevel[0]) {
        case 'a':
            sky_error_validate(!strcmp(releaselevel, "alpha"));
            hexversion |= 0xA0;
            break;
        case 'b':
            sky_error_validate(!strcmp(releaselevel, "beta"));
            hexversion |= 0xB0;
            break;
        case 'c':
            sky_error_validate(!strcmp(releaselevel, "candidate"));
            hexversion |= 0xC0;
            break;
        case 'f':
            sky_error_validate(!strcmp(releaselevel, "final"));
            hexversion |= 0xF0;
            break;
        default:
            sky_error_fatal("invalid release level");
    }

    return sky_integer_createfromunsigned(hexversion);
}


static sky_namespace_t
sky_sys_implementation(void)
{
    sky_object_t objects[8];

    objects[0] = SKY_STRING_LITERAL("cache_tag");
    objects[1] = sky_string_createfromformat("skython-%d%d",
                                             SKYTHON_VERSION_MAJOR,
                                             SKYTHON_VERSION_MINOR);
    objects[2] = SKY_STRING_LITERAL("hexversion");
    objects[3] = sky_sys_hexversion(SKYTHON_VERSION_MAJOR,
                                    SKYTHON_VERSION_MINOR,
                                    SKYTHON_VERSION_MICRO,
                                    SKYTHON_VERSION_RELEASELEVEL,
                                    SKYTHON_VERSION_SERIAL);
    objects[4] = SKY_STRING_LITERAL("name");
    objects[5] = SKY_STRING_LITERAL("skython");
    objects[6] = SKY_STRING_LITERAL("version");
    objects[7] = sky_sys_version_info(SKYTHON_VERSION_MAJOR,
                                      SKYTHON_VERSION_MINOR,
                                      SKYTHON_VERSION_MICRO,
                                      SKYTHON_VERSION_RELEASELEVEL,
                                      SKYTHON_VERSION_SERIAL);

    return sky_namespace_createfromarray(sizeof(objects) / sizeof(objects[0]),
                                         objects);
}


static sky_object_t
sky_sys_int_info(void)
{
    static sky_struct_sequence_field_t fields[] = {
        { "bits_per_digit", "size of a digit in bits", 0 },
        { "sizeof_digit",   "size in bytes of the C type used to represent a "
                            "digit", 0 },
    };

    static const char doc[] =
"sys.int_info\n"
"\n"
"A struct sequence that holds information about Skython's\n"
"internal representation of integers. The attributes are read only.";

    sky_type_t      type;
    sky_object_t    values[sizeof(fields) / sizeof(fields[0])];

    type = sky_struct_sequence_define("sys.int_info",
                                      doc,
                                      sizeof(fields) / sizeof(fields[0]),
                                      fields);

    values[0] = sky_integer_create(GMP_NUMB_BITS);
    values[1] = sky_integer_create(GMP_LIMB_BITS / 8);

    return sky_struct_sequence_create(type,
                                      sizeof(fields) / sizeof(fields[0]),
                                      values);
}


static const char sky_sys_intern_doc[] =
"intern(string) -> string\n"
"\n"
"``Intern'' the given string.  This enters the string in the (global)\n"
"table of interned strings whose purpose is to speed up dictionary lookups.\n"
"Return the string itself or the previously interned string object with the\n"
"same value.\n";


static void
sky_sys_setswitchinterval(double value)
{
    if (value <= 0.0) {
        sky_error_raise_string(sky_ValueError,
                               "switch interval must be strictly positive");
    }
    sky_sys_switchinterval = value;
}

static const char sky_sys_setswitchinterval_doc[] =
"setswitchinterval(n)\n\n"
"Set the ideal thread switching delay inside the Python interpreter\n"
"The actual frequency of switching threads can be lower if the\n"
"interpreter executes long sequences of uninterruptible code\n"
"(this is implementation-specific and workload-dependent).\n"
"\n"
"The parameter must represent the desired switching delay in seconds\n"
"A typical value is 0.005 (5 milliseconds).";


static sky_object_t
sky_sys_thread_info(void)
{
    static sky_struct_sequence_field_t fields[] = {
        { "name",       "name of the thread implementation", 0 },
        { "lock",       "name of the lock implementation", 0 },
        { "version",    "name and version of the thread library", 0 },
    };

    static const char doc[] =
"sys.thread_info\n"
"\n"
"A struct sequence holding information about the thread implementation.";

    sky_type_t      type;
    sky_object_t    values[sizeof(fields) / sizeof(fields[0])];

    type = sky_struct_sequence_define("sys.thread_info",
                                      doc,
                                      sizeof(fields) / sizeof(fields[0]),
                                      fields);

    values[0] = SKY_STRING_LITERAL("pthread");
    values[1] = SKY_STRING_LITERAL("mutex+cond");
    values[2] = sky_None;

    return sky_struct_sequence_create(type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}


static sky_object_t
sky_sys_version_info(
                uint32_t        major,
                uint32_t        minor,
                uint32_t        micro,
                const char *    releaselevel,
                uint32_t        serial)
{
    static sky_struct_sequence_field_t fields[] = {
        { "major",          "Major release number", 0 },
        { "minor",          "Minor release number", 0 },
        { "micro",          "Patch release number", 0 },
        { "releaselevel",   "'alpha', 'beta', 'candidate', or 'final'", 0 },
        { "serial",         "Serial release number", 0 },
    };

    static const char doc[] =
"sys.version_info\n"
"\n"
"Version information as a named tuple.";

    sky_type_t      type;
    sky_object_t    values[sizeof(fields) / sizeof(fields[0])];

    if (!(type = sky_sys_version_info_type)) {
        type = sky_struct_sequence_define("sys.version_info",
                                          doc,
                                          sizeof(fields) / sizeof(fields[0]),
                                          fields);
        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&sky_sys_version_info_type),
                type,
                SKY_TRUE);
    }

    values[0] = sky_integer_createfromunsigned(major);
    values[1] = sky_integer_createfromunsigned(minor);
    values[2] = sky_integer_createfromunsigned(micro);
    values[3] = sky_string_createfrombytes(releaselevel,
                                           strlen(releaselevel),
                                           NULL,
                                           NULL);
    values[4] = sky_integer_createfromunsigned(serial);

    return sky_struct_sequence_create(type,
                                      sizeof(fields) / sizeof(fields[0]),
                                      values);
}


static sky_list_t
sky_sys_warnoptions(void)
{
    ssize_t             i;
    sky_list_t          list;
    const char * const *options;

    list = sky_list_create(NULL);
    if ((options = sky_interpreter_warnoptions()) != NULL) {
        for (i = 0; options[i]; ++i) {
            sky_list_append(list,
                            sky_string_createfrombytes(options[i],
                                                       strlen(options[i]),
                                                       NULL,
                                                       NULL));
        }
    }

    return list;
}


static sky_dict_t
sky_sys_xoptions(void)
{
    char                *p;
    ssize_t             i, len;
    sky_dict_t          dict;
    sky_object_t        value;
    sky_string_t        key;
    const char * const *options;

    dict = sky_dict_create();
    if ((options = sky_interpreter_xoptions()) != NULL) {
        for (i = 0; options[i]; ++i) {
            len = strlen(options[i]);
            if (!(p = strchr(options[i], '='))) {
                key = sky_string_createfrombytes(options[i], len, NULL, NULL);
                value = sky_True;
            }
            else {
                key = sky_string_createfrombytes(options[i],
                                                 p - options[i],
                                                 NULL,
                                                 NULL);
                value = sky_string_createfrombytes(p + 1,
                                                   len - ((p + 1) - options[i]),
                                                   NULL,
                                                   NULL);
            }
            sky_dict_setitem(dict, key, value);
        }
    }
    return dict;
}


void
skython_module_sys_initialize(sky_module_t module)
{
    static const char module_doc[] =
"This module provides access to some objects used or maintained by the\n"
"interpreter and to functions that interact strongly with the interpreter.\n"
"\n"
"Dynamic objects:\n"
"\n"
"argv -- command line arguments; argv[0] is the script pathname if known\n"
"path -- module search path; path[0] is the script directory, else ''\n"
"modules -- dictionary of loaded modules\n"
"\n"
"displayhook -- called to show results in an interactive session\n"
"excepthook -- called to handle any uncaught exception other than SystemExit\n"
"  To customize printing in an interactive session or to install a custom\n"
"  top-level exception handler, assign other functions to replace these.\n"
"\n"
"stdin -- standard input file object; used by input()\n"
"stdout -- standard output file object; used by print()\n"
"stderr -- standard error object; used for error messages\n"
"  By assigning other file objects (or objects that behave like files)\n"
"  to these, it is possible to redirect all of the interpreter's I/O.\n"
"\n"
"last_type -- type of last uncaught exception\n"
"last_value -- value of last uncaught exception\n"
"last_traceback -- traceback of last uncaught exception\n"
"  These three are only available in an interactive session after a\n"
"  traceback has been printed.\n"
"\n"
"Static objects:\n"
"\n"
"builtin_module_names -- tuple of module names built into this interpreter\n"
"copyright -- copyright notice pertaining to this interpreter\n"
"exec_prefix -- prefix used to find the machine-specific Python library\n"
"executable -- absolute path of the executable binary of the Python interpreter\n"
"float_info -- a struct sequence with information about the float implementation.\n"
"float_repr_style -- string indicating the style of repr() output for floats\n"
"hexversion -- version information encoded as a single integer\n"
"implementation -- Python implementation information.\n"
"int_info -- a struct sequence with information about the int implementation.\n"
"maxsize -- the largest supported length of containers.\n"
"maxunicode -- the value of the largest Unicode codepoint\n"
"platform -- platform identifier\n"
"prefix -- prefix used to find the Python library\n"
"thread_info -- a struct sequence with information about the thread implementation.\n"
"version -- the version of this interpreter as a string\n"
"version_info -- version information as a named tuple\n"
#if defined(_WIN32)
"dllhandle -- [Windows only] integer handle of the Python DLL\n"
"winver -- [Windows only] version number of the Python DLL\n"
#endif
"__stdin__ -- the original stdin; don't touch!\n"
"__stdout__ -- the original stdout; don't touch!\n"
"__stderr__ -- the original stderr; don't touch!\n"
"__displayhook__ -- the original displayhook; don't touch!\n"
"__excepthook__ -- the original excepthook; don't touch!\n"
"\n"
"Functions:\n"
"\n"
"displayhook() -- print an object to the screen, and save it in builtins._\n"
"excepthook() -- print an exception and its traceback to sys.stderr\n"
"exc_info() -- return thread-safe information about the current exception\n"
"exit() -- exit the interpreter by raising SystemExit\n"
"getdlopenflags() -- returns flags to be used for dlopen() calls\n"
"getprofile() -- get the global profiling function\n"
"getrefcount() -- return the reference count for an object (plus one :-)\n"
"getrecursionlimit() -- return the max recursion depth for the interpreter\n"
"getsizeof() -- return the size of an object in bytes\n"
"gettrace() -- get the global debug tracing function\n"
"setcheckinterval() -- control how often the interpreter checks for events\n"
"setdlopenflags() -- set the flags to be used for dlopen() calls\n"
"setprofile() -- set the global profiling function\n"
"setrecursionlimit() -- set the max recursion depth for the interpreter\n"
"settrace() -- set the global debug tracing functioni\n";

    unsigned int    flags = sky_interpreter_flags();

    const char      *abiflags;
    sky_type_t      type;
    sky_object_t    writer;
    sky_string_t    doc;

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);

    /* Set up a simple writer object for stdout and stderr. It'll get replaced
     * by the real deal later on during initialization after the _io module is
     * initialized.
     */
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("std_writer"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_sys_writer_data_t),
                                   0,
                                   NULL,
                                   NULL);
    sky_type_setattr_builtin(
            type,
            "write",
            sky_function_createbuiltin(
                    "std_writer.write",
                    NULL,
                    (sky_native_code_function_t)sky_sys_writer_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "s", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_sys_writer_type),
            type,
            SKY_TRUE);

    writer = sky_object_allocate(sky_sys_writer_type);
    sky_sys_writer_data(writer)->fp = stdout;
    sky_module_setattr(module, "stdout", writer);

    writer = sky_object_allocate(sky_sys_writer_type);
    sky_sys_writer_data(writer)->fp = stderr;
    sky_module_setattr(module, "stderr", writer);


#if defined(NDEBUG)
    abiflags = "mu";
#else
    abiflags = "dmu";
#endif
    sky_module_setattr(module,
                       "abiflags",
                       sky_string_createfrombytes(abiflags,
                                                  strlen(abiflags),
                                                  NULL,
                                                  NULL));

    sky_module_setattr(module,
                       "api_version",
                       sky_integer_create(SKYTHON_API_VERSION));
    sky_module_setattr(module, "argv", sky_sys_argv());

#if SKY_ENDIAN == SKY_ENDIAN_BIG
    sky_module_setattr(module, "byteorder", SKY_STRING_LITERAL("big"));
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
    sky_module_setattr(module, "byteorder", SKY_STRING_LITERAL("little"));
#else
#   error implementation missing
#endif
    sky_module_setattr(module,
                       "builtin_module_names",
                       sky_sys_builtin_module_names());

    /* TODO call_tracing() */
    sky_module_setattr(
            module,
            "copyright",
            SKY_STRING_LITERAL("Copyright (c) 2016 BAE Systems. All Rights Reserved."));

#if defined(_WIN32)
    /* TODO dllhandle */
#endif
    sky_module_setattr(
            module,
            "displayhook",
            sky_function_createbuiltin(
                    "displayhook",
                    "displayhook(object) -> None\n\n"
                    "Print an object to sys.stdout and also save it in builtins._",
                    (sky_native_code_function_t)sky_sys_displayhook,
                    SKY_DATA_TYPE_VOID,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));


    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_DONT_WRITE_BYTECODE) {
        sky_module_setattr(module, "dont_write_bytecode", sky_True);
    }
    else {
        sky_module_setattr(module, "dont_write_bytecode", sky_False);
    }

    sky_module_setattr(
            module,
            "excepthook",
            sky_function_createbuiltin(
                    "excepthook",
                    "excepthook(exctype, value, traceback) -> None\n\n"
                    "Handle an exceptoin by displaying it with a traceback on sys.stderr.",
                    (sky_native_code_function_t)sky_error_display,
                    SKY_DATA_TYPE_VOID,
                    "exctype", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "traceback", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_traceback_type,
                    NULL));
    sky_module_setattr(
            module,
            "exc_info",
            sky_function_createbuiltin(
                    "exc_info",
                    "exc_info() -> (type, value, traceback)\n\n"
                    "Return information about the most recent exception caught by an except\n"
                    "clause in the current stack frame or in an older stack frame.",
                    (sky_native_code_function_t)sky_sys_exc_info,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    NULL));
    sky_module_setattr(module, "exec_prefix", sky_interpreter_exec_prefix());
    sky_module_setattr(module, "executable", sky_interpreter_executable());
    sky_module_setattr(
            module,
            "exit",
            sky_function_createbuiltin(
                    "exit",
                    "exit([status])\n\n"
                    "Exit the interpreter by raising SystemExit(status).\n"
                    "If the status is omitted or None, it defaults to zero (i.e., success).\n"
                    "If the status is an integer, it will be used as the system exit status.\n"
                    "If it is another kind of object, it will be printed and the system\n"
                    "exit status will be one (i.e., failure).",
                    (sky_native_code_function_t)sky_sys_exit,
                    SKY_DATA_TYPE_VOID,
                    "status", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));


    sky_module_setattr(module, "flags", sky_sys_flags());
    sky_module_setattr(module, "float_info", sky_sys_float_info());
    sky_module_setattr(module,
                       "float_repr_style",
                       SKY_STRING_LITERAL("short"));

    sky_module_setattr(
            module,
            "getdefaultencoding",
            sky_function_createbuiltin(
                    "getdefaultencoding",
                    "getdefaultencoding() -> string\n\n"
                    "Return the current default string encoding used by the Unicode\n"
                    "implementation.",
                    (sky_native_code_function_t)sky_sys_getdefaultencoding,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "getdlopenflags",
            sky_function_createbuiltin(
                    "getdlopenflags",
                    "getdlopenflags() -> int\n\n"
                    "Return the current value of the flags that are used for dlopen calls.\n"
                    "The flag constants are defined in the os module.",
                    (sky_native_code_function_t)sky_interpreter_dlopenflags,
                    SKY_DATA_TYPE_INT,
                    NULL));
    sky_module_setattr(
            module,
            "getfilesystemencoding",
            sky_function_createbuiltin(
                    "getfilesystemencoding",
                    "getfilesystemencoding() -> string\n\n"
                    "Return the encoding used to convert Unicode filenames into\n"
                    "operating system filenames.",
                    (sky_native_code_function_t)sky_codec_filesystemencoding,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "getrecursionlimit",
            sky_function_createbuiltin(
                    "getrecursionlimit",
                    "getrecursionlimit()\n\n"
                    "Return the current value of the recursion limit, the maximum depth\n"
                    "of the Python interpreter stack.  This limit prevents infinite\n"
                    "recursion from causing an overflow of the C stack and crashing Python.",
                    (sky_native_code_function_t)sky_interpreter_recursionlimit,
                    SKY_DATA_TYPE_SSIZE_T,
                    NULL));
#if defined(_WIN32)
    /* TODO getwindowsversion() */
#endif
    sky_module_setattr(
            module,
            "getsizeof",
            sky_function_createbuiltin(
                    "getsizeof",
                    "getsizeof(object, default) -> int\n\n"
                    "Return the size of object in bytes.",
                    (sky_native_code_function_t)sky_sys_getsizeof,
                    SKY_DATA_TYPE_SIZE_T,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "_getframe",
            sky_function_createbuiltin(
                    "_getframe",
                    "_getframe([depth]) -> frameobject\n\n"
                    "Return a frame object from the call stack.  If optional integer depth is\n"
                    "given, return the frame object that many calls below the top of the stack.\n"
                    "If that is deeper than the call stack, ValueError is raised.  The default\n"
                    "for depth is zero, returning the frame at the top of the call stack.\n\n"
                    "This function should be used for internal and specialized\n"
                    "purposes only.",
                    (sky_native_code_function_t)sky_sys__getframe,
                    SKY_DATA_TYPE_OBJECT,
                    "depth", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    /* TODO getprofile() */
    sky_module_setattr(
            module,
            "getswitchinterval",
            sky_function_createbuiltin(
                    "getswitchinterval",
                    sky_sys_getswitchinterval_doc,
                    (sky_native_code_function_t)sky_sys_getswitchinterval,
                    SKY_DATA_TYPE_DOUBLE,
                    NULL));
    sky_module_setattr(
            module,
            "gettrace",
            sky_function_createbuiltin(
                    "gettrace",
                    "gettrace()\n\n"
                    "Return the global debug tracing function set with sys.settrace.\n"
                    "See the debugger chapter in the library manual.",
                    (sky_native_code_function_t)sky_interpreter_gettrace,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));

    /* TODO hash_info */
    sky_module_setattr(
            module,
            "hexversion",
            sky_sys_hexversion(SKYTHON_PYTHON_VERSION_MAJOR,
                               SKYTHON_PYTHON_VERSION_MINOR,
                               SKYTHON_PYTHON_VERSION_MICRO,
                               SKYTHON_PYTHON_VERSION_RELEASELEVEL,
                               SKYTHON_PYTHON_VERSION_SERIAL));

    sky_module_setattr(module,
                       "implementation",
                       sky_sys_implementation());
    sky_module_setattr(module, "int_info", sky_sys_int_info());

    sky_module_setattr(
            module,
            "intern",
            sky_function_createbuiltin(
                    "intern",
                    sky_sys_intern_doc,
                    (sky_native_code_function_t)sky_string_intern,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

    /* last_traceback - set once an unhandled exception has been raised */
    /* last_type - set once an unhandled exception has been raised */
    /* last_value - set once an unhandled exception has been raised */

    sky_module_setattr(module, "maxsize", sky_integer_create(SSIZE_MAX));
    sky_module_setattr(module, "maxunicode", sky_integer_create(SKY_UNICODE_CHAR_MAX));
    sky_module_setattr(module, "meta_path", sky_list_create(NULL));
    sky_module_setattr(module, "modules", sky_interpreter_modules());

    sky_module_setattr(module, "path", sky_interpreter_path());
    sky_module_setattr(module, "path_hooks", sky_list_create(NULL));
    sky_module_setattr(module, "path_importer_cache", sky_dict_create());
    sky_module_setattr(module, "platform", SKY_STRING_LITERAL(SKYTHON_PLATFORM_NAME));
    sky_module_setattr(module, "prefix", sky_interpreter_prefix());
    /* ps1 - set only if running interactively */
    /* ps2 - set only if running interactively */

    sky_module_setattr(
            module,
            "setdlopenflags",
            sky_function_createbuiltin(
                    "setdlopenflags",
                    "setdlopenflags(n) -> None\n\n"
                    "Set the flags used by the interpreter for dlopen calls, such as when the\n"
                    "interpreter loads extension modules.  Among other things, this will enable\n"
                    "a lazy resolving of symbols when importing a module, if called as\n"
                    "sys.setdlopenflags(0).  To share symbols across extension modules, call as\n"
                    "sys.setdlopenflags(ctypes.RTLD_GLOBAL).  Symbolic names for the flag modules\n"
                    "can be found in the os module (RTLD_xxx constants, e.g. os.RTLD_LAZY).",
                    (sky_native_code_function_t)sky_interpreter_setdlopenflags,
                    SKY_DATA_TYPE_VOID,
                    "n", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    /* TODO setprofile() */
    sky_module_setattr(
            module,
            "setrecursionlimit",
            sky_function_createbuiltin(
                    "setrecursionlimit",
                    "setrecursionlimit(n)\n\n"
                    "Set the maximum depth of the Python interpreter stack to n.  This\n"
                    "limit prevents infinite recursion from causing an overflow of the C\n"
                    "stack and crashing Python.  The highest possible limit is platform-\n"
                    "dependent.",
                    (sky_native_code_function_t)sky_interpreter_setrecursionlimit,
                    SKY_DATA_TYPE_VOID,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setswitchinterval",
            sky_function_createbuiltin(
                    "setswitchinterval",
                    sky_sys_setswitchinterval_doc,
                    (sky_native_code_function_t)sky_sys_setswitchinterval,
                    SKY_DATA_TYPE_VOID,
                    "n", SKY_DATA_TYPE_DOUBLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "settrace",
            sky_function_createbuiltin(
                    "settrace",
                    "settrace(function)\n\n"
                    "Set the global debug tracing function.  It will be called on each\n"
                    "function call.  See the debugger chapter in the library manual.",
                    (sky_native_code_function_t)sky_interpreter_settrace,
                    SKY_DATA_TYPE_VOID,
                    "function", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    /* stdin - set by sky_interpreter_initialize_stdio() */
    /* stdout - set by sky_interpreter_initialize_stdio() */
    /* stderr - set by sky_interpreter_initialize_stdio() */

    sky_module_setattr(module, "thread_info", sky_sys_thread_info());
    sky_module_setattr(module, "tracebacklimit", sky_integer_create(1000));

    sky_module_setattr(module,
                       "version",
                       sky_string_createfromformat("%s (Revision %s, %s, %s)\n"
#if defined(__GNUC__)
                                                   "[GCC %s]",
#else
#   error implementation missing
#endif
                                                   SKYTHON_PYTHON_VERSION_STRING,
                                                   SKYTHON_VERSION_BUILD,
                                                   __DATE__,
                                                   __TIME__,
                                                   __VERSION__));
    sky_module_setattr(
            module,
            "version_info",
            sky_sys_version_info(SKYTHON_PYTHON_VERSION_MAJOR,
                                 SKYTHON_PYTHON_VERSION_MINOR,
                                 SKYTHON_PYTHON_VERSION_MICRO,
                                 SKYTHON_PYTHON_VERSION_RELEASELEVEL,
                                 SKYTHON_PYTHON_VERSION_SERIAL));

    sky_module_setattr(module, "warnoptions", sky_sys_warnoptions());
#if defined(_WIN32)
    /* TODO winver */
#endif

    sky_module_setattr(module, "_xoptions", sky_sys_xoptions());


    /* Undocumented: mdebug */
    /* Undocumented: gettotalrefcount() */
    /* Undocumented: getcounts() */
    /* Undocumented: callstats() */
    /* Undocumented: getdxp() */
    /* Undocumented: getobjects() */

    /* CPython specific: _clear_type_cache() */
    /* CPython specific: _current_frames() */
    /* CPython specific: _debugmallocstats() */
    /* CPython specific: getcheckinterval() */
    /* CPython specific: getrefcount() */
    /* CPython specific: _mercurial */
    /* CPython specific: setcheckinterval() */
    /* CPython specific: settscdump() */

    /* __stdin__ - set by sky_interpreter_initialize_stdio() */
    /* __stdout__ - set by sky_interpreter_initialize_stdio() */
    /* __stderr__ - set by sky_interpreter_initialize_stdio() */
    sky_module_setattr(
            module,
            "__displayhook__",
            sky_object_getattr(module,
                               SKY_STRING_LITERAL("displayhook"),
                               sky_NotSpecified));
    sky_module_setattr(
            module,
            "__excepthook__",
            sky_object_getattr(module,
                               SKY_STRING_LITERAL("excepthook"),
                               sky_NotSpecified));
    sky_module_setattr(
            module,
            "base_exec_prefix",
            sky_object_getattr(module,
                               SKY_STRING_LITERAL("exec_prefix"),
                               sky_NotSpecified));
    sky_module_setattr(
            module,
            "base_prefix",
            sky_object_getattr(module,
                               SKY_STRING_LITERAL("prefix"),
                               sky_NotSpecified));
}
