#include "../core/sky_private.h"
#include "../core/sky_string_private.h"

#include "module__io.h"


sky_type_t sky__io_StringIO_type = NULL;


/* Forward declaration for sky__io_StringIO_next() */
static sky_string_t
sky__io_StringIO_readline(sky_object_t self, sky_object_t limit);


static void
sky__io_StringIO_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky__io_StringIO_data_t *self_data = data;

    sky_free(self_data->buffer);
}

static void
sky__io_StringIO_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky__io_StringIO_data_t *self_data = data;

    sky_spinlock_init(&(self_data->spinlock));
}

static void
sky__io_StringIO_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io_StringIO_data_t *self_data = data;

    sky_object_visit(self_data->builder, visit_data);
}


static void
sky__io_StringIO__checkClosed(sky_object_t self)
{
    if (sky_file_closed(self)) {
        sky_error_raise_string(sky_ValueError, "I/O operation on closed file");
    }
}


static inline void
sky__io_StringIO__lock(sky_object_t self)
{
    sky__io_StringIO_data_t *self_data;

    if (sky_object_gc_isthreaded() && sky_object_isglobal(self)) {
        self_data = sky__io_StringIO_data(self);
        sky_spinlock_lock(&(self_data->spinlock));
        sky_asset_save(&(self_data->spinlock),
                       (sky_free_t)sky_spinlock_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);
    }
}


static void
sky__io_StringIO__convertToBuilder(sky_object_t self, ssize_t limit)
{
    sky__io_StringIO_data_t *self_data;

    self_data = sky__io_StringIO_data(self);
    if (limit < 0 || limit > self_data->buffer_len) {
        limit = self_data->buffer_len;
    }
    sky_object_gc_set(
            SKY_AS_OBJECTP(&(self_data->builder)),
            sky_string_builder_createwithcapacity(self_data->buffer_len),
            self);
    sky_string_builder_appendcodepoints(self_data->builder,
                                        self_data->buffer,
                                        limit,
                                        sizeof(sky_unicode_char_t));
    sky_free(self_data->buffer);
    self_data->buffer = NULL;
}


static void
sky__io_StringIO__convertToBuffer(sky__io_StringIO_data_t *self_data)
{
    sky_string_data_t   *builder_data;
    sky_unicode_char_t  *outcp;

    builder_data = sky_object_data(self_data->builder, sky_string_type);
    self_data->builder = NULL;

    self_data->buffer =
            sky_malloc(builder_data->len * sizeof(sky_unicode_char_t));
    self_data->buffer_len = builder_data->len;

    outcp = self_data->buffer;
    if (builder_data->highest_codepoint < 0x100) {
        const uint8_t   *end, *incp;

        end = builder_data->data.latin1 + builder_data->len;
        for (incp = builder_data->data.latin1; incp < end; ++incp) {
            *outcp++ = *incp;
        }
    }
    else if (builder_data->highest_codepoint < 0x10000) {
        const uint16_t  *end, *incp;

        end = builder_data->data.ucs2 + builder_data->len;
        for (incp = builder_data->data.ucs2; incp < end; ++incp) {
            *outcp++ = *incp;
        }
    }
    else {
        memcpy(outcp,
               builder_data->data.ucs4,
               builder_data->len * sizeof(sky_unicode_char_t));
    }
}


static sky_string_t
sky__io_StringIO__readline(sky_object_t self, ssize_t limit)
{
    ssize_t                 ncodepoints, pos;
    sky_string_data_t       *builder_data;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    self_data = sky__io_StringIO_data(self);
    if (self_data->buffer) {
        if (self_data->pos >= self_data->buffer_len) {
            return sky_string_empty;
        }
        sky__io_StringIO__convertToBuilder(self, -1);
    }
    else if (!self_data->builder) {
        return sky_string_empty;
    }

    builder_data = sky_string_data(self_data->builder, NULL);
    if (self_data->pos >= builder_data->len) {
        return sky_string_empty;
    }
    if (limit < 0 || limit > builder_data->len - self_data->pos) {
        limit = builder_data->len - self_data->pos;
    }
    pos = sky__io_line_ending((sky_string_t)self_data->builder,
                              self_data->pos,
                              self_data->read_translate,
                              self_data->read_universal,
                              self_data->read_newline);
    if (-1 == pos) {
        ncodepoints = limit;
    }
    else {
        ncodepoints = (pos + 1) - self_data->pos;
        if (ncodepoints > limit) {
            ncodepoints = limit;
        }
    }
    builder = sky_string_builder_createwithcapacity(ncodepoints);
    sky_string_builder_appendslice(builder,
                                   (sky_string_t)self_data->builder,
                                   ncodepoints,
                                   self_data->pos,
                                   1);
    self_data->pos += ncodepoints;
    return sky_string_builder_finalize(builder);
}


static void
sky__io_StringIO__writeStringBuffer(
                sky__io_StringIO_data_t *   self_data,
                sky_string_t                string)
{
    sky_string_data_t   *string_data, tagged_string_data;
    sky_unicode_char_t  *outcp;

    outcp = &(self_data->buffer[self_data->pos]);

    string_data = sky_string_data(string, &tagged_string_data);
    self_data->pos += string_data->len;

    if (string_data->highest_codepoint < 0x100) {
        const uint8_t   *end, *incp;

        end = string_data->data.latin1 + string_data->len;
        for (incp = string_data->data.latin1; incp < end; ++incp) {
            *outcp++ = *incp;
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        const uint16_t  *end, *incp;

        end = string_data->data.ucs2 + string_data->len;
        for (incp = string_data->data.ucs2; incp < end; ++incp) {
            *outcp++ = *incp;
        }
    }
    else {
        memcpy(outcp,
               string_data->data.ucs4,
               string_data->len * sizeof(sky_unicode_char_t));
    }
}


static void
sky__io_StringIO__writeString(sky_object_t self, sky_string_t string)
{
    ssize_t                 builder_len, len, ncodepoints;
    sky_object_t            decoder;
    sky_string_t            write_newline;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    self_data = sky__io_StringIO_data(self);
    if ((decoder = self_data->decoder) != NULL) {
        string = sky__io_IncrementalNewlineDecoder_decode(decoder,
                                                          string,
                                                          SKY_TRUE);
    }
    if ((write_newline = self_data->write_newline) != NULL) {
        string = sky_string_replace(string, sky__io_lf, write_newline, -1);
    }

    len = sky_string_len(string);
    if (self_data->pos > SSIZE_MAX - len) {
        sky_error_raise_string(sky_OverflowError, "new position too large");
    }

    if (self_data->buffer) {
        if (self_data->pos + len < self_data->buffer_len) {
            sky__io_StringIO__writeStringBuffer(self_data, string);
            return;
        }

        /* Convert buffer to string_builder and fall through to normal
         * string_builder write.
         */
        ncodepoints = SKY_MIN(self_data->buffer_len, self_data->pos);
        sky__io_StringIO__convertToBuilder(self, ncodepoints);
    }
    else if (!self_data->builder) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->builder)),
                          sky_string_builder_create(),
                          self);
    }
    builder_len = sky_object_len(self_data->builder);
    if (self_data->pos < builder_len) {
        if (self_data->pos + len < builder_len) {
            /* Convert builder to buffer and write into the buffer. */
            sky__io_StringIO__convertToBuffer(self_data);
            sky__io_StringIO__writeStringBuffer(self_data, string);
            return;
        }

        /* Replace the builder with a slice of the original builder and
         * fall through to normal string_builder write.
         */
        builder = sky_string_builder_createwithcapacity(self_data->pos + len);
        sky_string_builder_appendslice(builder,
                                       (sky_string_t)self_data->builder,
                                       self_data->pos,
                                       0,
                                       1);
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->builder)),
                          builder,
                          self);
    }

    /* Add padding for an overseek. */
    if ((ncodepoints = self_data->pos - builder_len) > 0) {
        sky_string_builder_appendcodepoint(self_data->builder, 0, ncodepoints);
    }

    /* Write the new data into the builder and adjust the file position. */
    sky_string_builder_append(self_data->builder, string);
    self_data->pos += len;
}


static void
sky__io_StringIO_close(sky_object_t self)
{
    sky__io_StringIO_data_t *self_data;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);

        self_data = sky__io_StringIO_data(self);
        if (!self_data->closed) {
            self_data->closed = SKY_TRUE;
            self_data->builder = NULL;
            self_data->decoder = NULL;
            self_data->read_newline = NULL;
            self_data->write_newline = NULL;
            sky_free(self_data->buffer);
            self_data->buffer = NULL;
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky__io_StringIO_close_doc[] =
"Close the IO object. Attempting any further operation after the\n"
"object is closed will raise a ValueError.\n"
"\n"
"This method has no effect if the file is already closed.\n";


static sky_object_t
sky__io_StringIO_closed_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return (sky__io_StringIO_data(self)->closed ? sky_True : sky_False);
}


static sky_object_t
sky__io_StringIO_getstate(sky_object_t self)
{
    sky_object_t            value;
    sky__io_StringIO_data_t *self_data;

    value = sky_object_callmethod(self,
                                  SKY_STRING_LITERAL("getvalue"),
                                  NULL,
                                  NULL);

    self_data = sky__io_StringIO_data(self);
    return sky_object_build("(OOizO)",
                            value,
                            self_data->read_newline,
                            self_data->pos,
                            sky_dict_copy(sky_object_dict(self)));
}


static sky_string_t
sky__io_StringIO_getvalue(sky_object_t self)
{
    ssize_t                 len;
    sky_string_t            string;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        self_data = sky__io_StringIO_data(self);
        if (self_data->buffer) {
            if (!(len = self_data->buffer_len)) {
                string = sky_string_empty;
            }
            else {
                builder = sky_string_builder_createwithcapacity(len);
                sky_string_builder_appendcodepoints(builder,
                                                    self_data->buffer,
                                                    len,
                                                    sizeof(sky_unicode_char_t));
                string = sky_string_builder_finalize(builder);
            }
        }
        else if (!sky_object_bool(self_data->builder)) {
            string = sky_string_empty;
        }
        else {
            builder = sky_string_builder_copy(self_data->builder);
            string = sky_string_builder_finalize(builder);
        }
    } SKY_ASSET_BLOCK_END;

    return string;
}

static const char sky__io_StringIO_getvalue_doc[] =
"Retrieve the entire contents of the object.";


static void
sky__io_StringIO_init(sky_object_t  self,
                      sky_string_t  initial_value,
                      sky_string_t  newline)
{
    sky__io_StringIO_data_t *self_data = sky__io_StringIO_data(self);

    if (sky_object_isnull(newline)) {
        newline = NULL;
    }
    else if (!sky_object_compare(newline,
                                 sky_string_empty,
                                 SKY_COMPARE_OP_EQUAL) &&
             !sky_object_compare(newline,
                                 sky__io_cr,
                                 SKY_COMPARE_OP_EQUAL) &&
             !sky_object_compare(newline,
                                 sky__io_crlf,
                                 SKY_COMPARE_OP_EQUAL) &&
             !sky_object_compare(newline,
                                 sky__io_lf,
                                 SKY_COMPARE_OP_EQUAL))
    {
        sky_error_raise_format(sky_ValueError,
                               "illegal newline value: %#@",
                               newline);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);

        self_data->builder = NULL;
        self_data->decoder = NULL;
        self_data->read_newline = NULL;
        self_data->write_newline = NULL;

        sky_free(self_data->buffer);
        self_data->buffer = NULL;

        if (newline) {
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->read_newline)),
                              newline,
                              self);
        }
        self_data->read_universal = !sky_object_bool(newline);
        self_data->read_translate = sky_object_isnull(newline);

        /* if newline == "": don't translate anything
         * if newline == "\n" or newline is None: translate to "\n"
         * (for newline is NOne, TextIOWrapper translates to os.sepline, but it
         * is pointless for StringIO).
         */
        if (newline &&
            sky_object_compare(newline, sky__io_lf, SKY_COMPARE_OP_NOT_EQUAL))
        {
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_newline)),
                              self_data->read_newline,
                              self);
        }

        if (self_data->read_universal) {
            sky_object_gc_set(
                    &(self_data->decoder),
                    sky_object_call(
                            sky__io_IncrementalNewlineDecoder_type,
                            sky_object_build("(Oi)",
                                             sky_None,
                                             self_data->read_translate),
                            NULL),
                    self);
        }

        if (sky_object_bool(initial_value)) {
            self_data->pos = 0;
            sky__io_StringIO__writeString(self, initial_value);
        }

        self_data->pos = 0;
        self_data->closed = SKY_FALSE;
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t
sky__io_StringIO_line_buffering_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_StringIO__checkClosed(self);
    return sky_False;
}


static sky_object_t
sky__io_StringIO_newlines_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_object_t            decoder;
    sky__io_StringIO_data_t *self_data;

    sky__io_StringIO__checkClosed(self);

    self_data = sky__io_StringIO_data(self);
    if (!(decoder = self_data->decoder)) {
        return sky_None;
    }
    return sky_object_getattr(decoder,
                              SKY_STRING_LITERAL("newlines"),
                              sky_NotSpecified);
}


static sky_object_t
sky__io_StringIO_next(sky_object_t self)
{
    sky_string_t    line;

    if (sky_object_type(self) == sky__io_StringIO_type) {
        line = sky__io_StringIO_readline(self, NULL);
    }
    else {
        line = sky_file_readline(self, -1);
        if (!sky_object_isa(line, sky_string_type)) {
            sky_error_raise_format(
                    sky_OSError,
                    "readline() should have returned an str object, not %#@",
                    sky_type_name(sky_object_type(line)));
        }
    }

    /* StopIteration if EOF reached (line is an empty string) */
    return (sky_object_bool(line) ? line : NULL);
}


static sky_string_t
sky__io_StringIO_read(sky_object_t self, sky_object_t n)
{
    ssize_t                 navail, size;
    sky_string_t            string;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    if (sky_object_isnull(n)) {
        size = -1;
    }
    else if (!sky_object_isa(n, sky_integer_type)) {
        sky_error_raise_format(sky_TypeError,
                               "integer argument expected, got %#@",
                               sky_type_name(sky_object_type(n)));
    }
    else {
        size = sky_integer_value(n, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    self_data = sky__io_StringIO_data(self);
    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        if (!self_data->builder) {
            string = sky_string_empty;
        }
        else {
            navail = (self_data->buffer ? self_data->buffer_len
                                        : sky_object_len(self_data->builder)) -
                     self_data->pos;
            if (size < 0 || size > navail) {
                if ((size = navail) < 0) {
                    size = 0;
                }
            }

            builder = sky_string_builder_createwithcapacity(size);
            if (self_data->buffer) {
                sky_string_builder_appendcodepoints(
                        builder,
                        &(self_data->buffer[self_data->pos]),
                        size,
                        sizeof(sky_unicode_char_t));
            }
            else {
                sky_string_builder_appendslice(
                        builder,
                        (sky_string_t)self_data->builder,
                        size,
                        self_data->pos,
                        1);
            }

            self_data->pos += size;
            string = sky_string_builder_finalize(builder);
        }
    } SKY_ASSET_BLOCK_END;

    return string;
}

static const char sky__io_StringIO_read_doc[] =
"Read at most n characters, returned as a string.\n"
"\n"
"If the argument is negative or omitted, read until EOF\n"
"is reached. Return an empty string at EOF.\n";


static sky_bool_t
sky__io_StringIO_readable(sky_object_t self)
{
    sky__io_StringIO__checkClosed(self);
    return SKY_TRUE;
}

static const char sky__io_StringIO_readable_doc[] =
"readable() -> bool. Returns True if the IO object can be read.";


ssize_t
sky__io_StringIO_readinto_native(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  void *          bytes,
    SKY_UNUSED  ssize_t         nbytes)
{
    sky_error_raise_string(sky_NotImplementedError,
                           "StringIO.readinto_native()");
}


static sky_string_t
sky__io_StringIO_readline(sky_object_t self, sky_object_t limit)
{
    ssize_t         n;
    sky_string_t    line;

    if (sky_object_isnull(limit)) {
        n = -1;
    }
    else if (!sky_object_isa(limit, sky_integer_type)) {
        sky_error_raise_format(sky_TypeError,
                               "integer argument expected, got %#@",
                               sky_type_name(sky_object_type(limit)));
    }
    else {
        n = sky_integer_value(limit, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        line = sky__io_StringIO__readline(self, n);
    } SKY_ASSET_BLOCK_END;

    return line;
}

static const char sky__io_StringIO_readline_doc[] =
"Read until newline or EOF.\n"
"\n"
"Returns an empty string if EOF is hit immediately.\n";


ssize_t
sky__io_StringIO_seek_native(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  ssize_t         pos,
    SKY_UNUSED  int             whence)
{
    sky_error_raise_string(sky_NotImplementedError,
                           "StringIO.seek_native()");
}


static ssize_t
sky__io_StringIO_seek(sky_object_t self, ssize_t offset, int whence)
{
    ssize_t                 pos;
    sky__io_StringIO_data_t *self_data;

    switch (whence) {
        case 0:
            if (offset < 0) {
                sky_error_raise_format(sky_ValueError,
                                       "Negative seek position %zd",
                                       offset);
            }
            break;
        case 1:
        case 2:
            if (offset) {
                sky_error_raise_string(sky_ValueError,
                                       "Can't do nonzero cur-relative seeks");
            }
            break;
        default:
            sky_error_raise_format(sky_ValueError,
                                   "Invalid whence (%d, should be 0, 1 or 2)",
                                   whence);
    }

    self_data = sky__io_StringIO_data(self);
    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        switch (whence) {
            case 0:
                pos = self_data->pos = offset;
                break;
            case 1:
                pos = self_data->pos;
                break;
            case 2:
                if (self_data->buffer) {
                    pos = self_data->pos = self_data->buffer_len;
                }
                else if (!self_data->buffer) {
                    pos = self_data->pos = 0;
                }
                else {
                    pos = self_data->pos = sky_object_len(self_data->builder);
                }
                break;
            default:
                sky_error_fatal("internal error");
        }
    } SKY_ASSET_BLOCK_END;

    return pos;
}

static const char sky__io_StringIO_seek_doc[] =
"Change stream position.\n"
"\n"
"Seek to character offset pos relative to position indicated by whence:\n"
"    0  Start of stream (the default).  pos should be >= 0;\n"
"    1  Current position - pos must be 0;\n"
"    2  End of stream - pos must be 0.\n"
"Returns the new absolute position.\n";


static sky_bool_t
sky__io_StringIO_seekable(sky_object_t self)
{
    sky__io_StringIO__checkClosed(self);
    return SKY_TRUE;
}

static const char sky__io_StringIO_seekable_doc[] =
"seekable() -> bool. Returns True if the IO object can be seeked.";


static void
sky__io_StringIO_setstate(sky_object_t self, sky_object_t state)
{
    ssize_t                 pos;
    sky_bool_t              read_translate, read_universal;
    sky_dict_t              dict;
    sky_object_t            decoder;
    sky_string_t            read_newline, value, write_newline;
    sky_integer_t           number;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    /* state: (value, read_newline, pos, dict) */
    if (!sky_object_isa(state, sky_tuple_type) || sky_tuple_len(state) != 4) {
        sky_error_raise_format(
                sky_TypeError,
                "%@.__setstate__ argument should be 4-tuple, got %#@",
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_object_type(state)));
    }

    value = sky_tuple_get(state, 0);
    if (!sky_object_isa(value, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "%@.__setstate__ tuple element 0 should be %#@; got %#@",
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(value)));
    }
    builder = sky_string_builder_createwithcapacity(sky_string_len(value));
    sky_string_builder_append(builder, value);

    read_newline = sky_tuple_get(state, 1);
    if (sky_object_isnull(read_newline)) {
        read_newline = NULL;
    }
    else if (!sky_object_isa(read_newline, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "%@.__setstate__ tuple element 1 should be %#@; got %#@",
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(read_newline)));
    }
    else if (!sky_object_compare(read_newline,
                                 sky__io_cr,
                                 SKY_COMPARE_OP_EQUAL) &&
             !sky_object_compare(read_newline,
                                 sky__io_crlf,
                                 SKY_COMPARE_OP_EQUAL) &&
             !sky_object_compare(read_newline,
                                 sky__io_lf,
                                 SKY_COMPARE_OP_EQUAL))
    {
        sky_error_raise_format(
                sky_ValueError,
                "%@.__setstate__ tuple element 1 illegal value: %#@",
                sky_type_name(sky_object_type(self)),
                read_newline);
    }
    write_newline = (read_newline &&
                     sky_object_compare(
                            read_newline,
                            sky__io_lf,
                            SKY_COMPARE_OP_NOT_EQUAL) ? read_newline
                                                      : NULL);
    read_universal = !sky_object_bool(read_newline);
    read_translate = sky_object_isnull(read_newline);

    if (!read_universal) {
        decoder = NULL;
    }
    else {
        decoder = sky_object_call(sky__io_IncrementalNewlineDecoder_type,
                                  sky_object_build("(OB)", sky_None,
                                                           read_translate),
                                  NULL);
    }

    number = sky_tuple_get(state, 2);
    if (!sky_object_isa(number, sky_integer_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "%@.__setstate__ tuple element 2 should be %#@; got %#@",
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_integer_type),
                sky_type_name(sky_object_type(number)));
    }
    pos = sky_integer_value(number, 0, SSIZE_MAX, NULL);

    dict = sky_tuple_get(state, 3);
    if (!sky_object_isnull(dict)) {
        if (!sky_object_isa(dict, sky_dict_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%@.__setstate__ tuple element 3 should be %#@; got %#@",
                    sky_type_name(sky_object_type(self)),
                    sky_type_name(sky_dict_type),
                    sky_type_name(sky_object_type(dict)));
        }
        sky_dict_update(sky_object_dict(self), NULL, dict);
    }

    self_data = sky__io_StringIO_data(self);
    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);

        self_data->pos = pos;
        self_data->closed = SKY_FALSE;
        self_data->read_universal = read_universal;
        self_data->read_translate = read_translate;

        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->builder)),
                builder,
                self);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->decoder)),
                decoder,
                self);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->read_newline)),
                read_newline,
                self);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->write_newline)),
                write_newline,
                self);

        sky_free(self_data->buffer);
        self_data->buffer = NULL;
    } SKY_ASSET_BLOCK_END;
}


static size_t
sky__io_StringIO_sizeof(sky_object_t self)
{
    size_t                  nbytes;
    sky__io_StringIO_data_t *self_data;

    nbytes = sky_memsize(self);
    self_data = sky__io_StringIO_data(self);
    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        if (self_data->buffer) {
            nbytes += sky_memsize(self_data->buffer);
        }
    } SKY_ASSET_BLOCK_END;
    return nbytes;
}


static ssize_t
sky__io_StringIO_tell(sky_object_t self)
{
    sky__io_StringIO__checkClosed(self);
    return sky__io_StringIO_data(self)->pos;
}

static const char sky__io_StringIO_tell_doc[] =
"Tell the current file position.";


static ssize_t
sky__io_StringIO_truncate(sky_object_t self, sky_object_t pos)
{
    ssize_t                 size;
    sky_string_builder_t    builder;
    sky__io_StringIO_data_t *self_data;

    if (sky_object_isnull(pos)) {
        size = -1;
    }
    else if (!sky_object_isa(pos, sky_integer_type)) {
        sky_error_raise_format(sky_TypeError,
                               "integer argument expected, got %#@",
                               sky_type_name(sky_object_type(pos)));
    }
    else {
        size = sky_integer_value(pos, SSIZE_MIN, SSIZE_MAX, NULL);
        if (size < 0) {
            sky_error_raise_format(sky_ValueError,
                                   "Negative size value %zd",
                                   size);
        }
    }

    self_data = sky__io_StringIO_data(self);
    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        if (size < 0) {
            size = self_data->pos;
        }
        if (self_data->buffer) {
            if (size < self_data->buffer_len) {
                self_data->buffer_len = size;
            }
        }
        else if (self_data->builder &&
                 size < sky_object_len(self_data->builder))
        {
            builder = sky_string_builder_createwithcapacity(size);
            sky_string_builder_appendslice(builder,
                                           (sky_string_t)self_data->builder,
                                           size,
                                           0,
                                           1);
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->builder)),
                              builder,
                              self);
        }
    } SKY_ASSET_BLOCK_END;

    return size;
}

static const char sky__io_StringIO_truncate_doc[] =
"Truncate size to pos.\n"
"\n"
"The pos argument defaults to the current file position, as\n"
"returned by tell().  The current file position is unchanged.\n"
"Returns the new absolute position.\n";


static sky_bool_t
sky__io_StringIO_writable(sky_object_t self)
{
    sky__io_StringIO__checkClosed(self);
    return SKY_TRUE;
}

static const char sky__io_StringIO_writable_doc[] =
"writable() -> bool. Returns True if the IO object can be written.";


static ssize_t
sky__io_StringIO_write(sky_object_t self, sky_string_t s)
{
    ssize_t len;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io_StringIO__lock(self);
        sky__io_StringIO__checkClosed(self);

        if ((len = sky_object_len(s)) > 0) {
            sky__io_StringIO__writeString(self, s);
        }
    } SKY_ASSET_BLOCK_END;

    return len;
}

static const char sky__io_StringIO_write_doc[] =
"Write string to file.\n\n"
"Returns the number of characters written, which is always equal to\n"
"the length of the string.\n";


ssize_t
sky__io_StringIO_write_native(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  const void *    bytes,
    SKY_UNUSED  ssize_t         nbytes)
{
    sky_error_raise_string(sky_NotImplementedError,
                           "StringIO.write_native()");
}


sky_type_t
sky__io_StringIO_initialize(void)
{
    static const char type_doc[] =
"Text I/O implementation using an in-memory buffer.\n\n"
"The initial_value argument sets the value of object.  The newline\n"
"argument is like the one of TextIOWrapper's constructor.";

    sky_type_t          type;
    sky_tuple_t         bases;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__io_StringIO_instance_initialize;
    template.finalize = sky__io_StringIO_instance_finalize;
    template.visit = sky__io_StringIO_instance_visit;
    bases = sky_tuple_pack(1, sky__io__TextIOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("StringIO"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   sizeof(sky__io_StringIO_data_t),
                                   SKY_CACHE_LINE_SIZE,
                                   bases,
                                   &template);

    sky_type_setgetsets(type,
            "closed",
                    NULL,
                    sky__io_StringIO_closed_getter,
                    NULL,
            "newlines",
                    NULL,
                    sky__io_StringIO_newlines_getter,
                    NULL,
            "line_buffering",
                    NULL,
                    sky__io_StringIO_line_buffering_getter,
                    NULL,
            NULL);

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "StringIO.close",
                    sky__io_StringIO_close_doc,
                    (sky_native_code_function_t)sky__io_StringIO_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "getvalue",
            sky_function_createbuiltin(
                    "StringIO.getvalue",
                    sky__io_StringIO_getvalue_doc,
                    (sky_native_code_function_t)sky__io_StringIO_getvalue,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "StringIO.read",
                    sky__io_StringIO_read_doc,
                    (sky_native_code_function_t)sky__io_StringIO_read,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "StringIO.readable",
                    sky__io_StringIO_readable_doc,
                    (sky_native_code_function_t)sky__io_StringIO_readable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readline",
            sky_function_createbuiltin(
                    "StringIO.readline",
                    sky__io_StringIO_readline_doc,
                    (sky_native_code_function_t)sky__io_StringIO_readline,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "limit", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "StringIO.seek",
                    sky__io_StringIO_seek_doc,
                    (sky_native_code_function_t)sky__io_StringIO_seek,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "StringIO.seekable",
                    sky__io_StringIO_seekable_doc,
                    (sky_native_code_function_t)sky__io_StringIO_seekable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "StringIO.tell",
                    sky__io_StringIO_tell_doc,
                    (sky_native_code_function_t)sky__io_StringIO_tell,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "StringIO.truncate",
                    sky__io_StringIO_truncate_doc,
                    (sky_native_code_function_t)sky__io_StringIO_truncate,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "StringIO.writable",
                    sky__io_StringIO_writable_doc,
                    (sky_native_code_function_t)sky__io_StringIO_writable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "StringIO.write",
                    sky__io_StringIO_write_doc,
                    (sky_native_code_function_t)sky__io_StringIO_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "s", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_StringIO_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "initial_value", (SKY_DATA_TYPE_OBJECT_STRING |
                              SKY_DATA_TYPE_OBJECT_OR_NONE), sky_string_empty,
            "newline", (SKY_DATA_TYPE_OBJECT_STRING |
                        SKY_DATA_TYPE_OBJECT_OR_NONE), SKY_STRING_LITERAL("\n"),
            NULL);

    sky_type_setmethodslots(type,
            "__sizeof__", sky__io_StringIO_sizeof,
            "__next__", sky__io_StringIO_next,
            "__getstate__", sky__io_StringIO_getstate,
            "__setstate__", sky__io_StringIO_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_StringIO_type),
            type,
            SKY_TRUE);
    return sky__io_StringIO_type;
}
