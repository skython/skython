/* Originally written by Tom St Denis as part of LibTomCrypt, which is in the
 * public domain (see below; original source header retained). Adapted for use
 * in Skython.
 */

/* LibTomCrypt, modular cryptographic library -- Tom St Denis
 *
 * LibTomCrypt is a library that provides various cryptographic
 * algorithms in a highly modular and flexible manner.
 *
 * The library is free for all purposes without any express
 * guarantee it works.
 *
 * Tom St Denis, tomstdenis@gmail.com, http://libtom.org
 */

#include "sha2.h"

void
SHA224_Init(SHA224_CTX *ctx)
{
    ctx->curlen = 0;
    ctx->length = 0;
    ctx->state[0] = 0xc1059ed8UL;
    ctx->state[1] = 0x367cd507UL;
    ctx->state[2] = 0x3070dd17UL;
    ctx->state[3] = 0xf70e5939UL;
    ctx->state[4] = 0xffc00b31UL;
    ctx->state[5] = 0x68581511UL;
    ctx->state[6] = 0x64f98fa7UL;
    ctx->state[7] = 0xbefa4fa4UL;
}

void
SHA224_Final(uint8_t *out, SHA224_CTX *ctx)
{
    uint8_t buf[SHA256_DIGEST_LENGTH];

    SHA256_Final(buf, ctx);
    memcpy(out, buf, SHA224_DIGEST_LENGTH);
}
