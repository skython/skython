/*
 * Secret Labs' Regular Expression Engine
 *
 * regular expression matching engine
 *
 * partial history:
 * 1999-10-24 fl  created (based on existing template matcher code)
 * 2000-03-06 fl  first alpha, sort of
 * 2000-08-01 fl  fixes for 1.6b1
 * 2000-08-07 fl  use PyOS_CheckStack() if available
 * 2000-09-20 fl  added expand method
 * 2001-03-20 fl  lots of fixes for 2.1b2
 * 2001-04-15 fl  export copyright as Python attribute, not global
 * 2001-04-28 fl  added __copy__ methods (work in progress)
 * 2001-05-14 fl  fixes for 1.5.2 compatibility
 * 2001-07-01 fl  added BIGCHARSET support (from Martin von Loewis)
 * 2001-10-18 fl  fixed group reset issue (from Matthew Mueller)
 * 2001-10-20 fl  added split primitive; reenable unicode for 1.6/2.0/2.1
 * 2001-10-21 fl  added sub/subn primitive
 * 2001-10-24 fl  added finditer primitive (for 2.2 only)
 * 2001-12-07 fl  fixed memory leak in sub/subn (Guido van Rossum)
 * 2002-11-09 fl  fixed empty sub/subn return type
 * 2003-04-18 mvl fully support 4-byte codes
 * 2003-10-17 gn  implemented non recursive scheme
 *
 * Copyright (c) 1997-2001 by Secret Labs AB.  All rights reserved.
 *
 * This version of the SRE library can be redistributed under CNRI's
 * Python 1.6 license.  For any other use, please contact Secret Labs
 * AB (info@pythonware.com).
 *
 * Portions of this engine have been developed in cooperation with
 * CNRI.  Hewlett-Packard provided funding for 1.6 integration and
 * other compatibility work.
 */

#ifndef SRE_RECURSIVE

static char sky__sre_copyright[] =
    " SRE 2.2.2 Copyright (c) 1997-2002 by Secret Labs AB ";


#include "../core/skython.h"
#include "../core/sky_string_private.h"

#include "module__sre.h"

#include <ctype.h>
#include <limits.h>

/* name of this module, minus the leading underscore */
#if !defined(SRE_MODULE)
#define SRE_MODULE "sre"
#endif

#define SRE_PY_MODULE "re"

/* defining this one enables tracing */
#undef VERBOSE

/* defining this enables unicode support (default under 1.6a1 and later) */
#define HAVE_UNICODE

/* -------------------------------------------------------------------- */
/* optional features */

/* enables fast searching */
#define USE_FAST_SEARCH

/* enables copy/deepcopy handling (work in progress) */
#undef USE_BUILTIN_COPY

/* -------------------------------------------------------------------- */

#if defined(_MSC_VER)
#pragma optimize("agtw", on) /* doesn't seem to make much difference... */
#pragma warning(disable: 4710) /* who cares if functions are not inlined ;-) */
/* fastest possible local call under MSVC */
#define LOCAL(type) static __inline type __fastcall
#elif defined(USE_INLINE)
#define LOCAL(type) static inline type
#else
#define LOCAL(type) static type
#endif

/* error codes */
#define SRE_ERROR_ILLEGAL -1 /* illegal opcode */
#define SRE_ERROR_STATE -2 /* illegal state */
#define SRE_ERROR_RECURSION_LIMIT -3 /* runaway recursion */
#define SRE_ERROR_MEMORY -9 /* out of memory */
#define SRE_ERROR_INTERRUPTED -10 /* signal handler raised exception */

#if defined(VERBOSE)
#define TRACE(v) printf v
#else
#define TRACE(v)
#endif

/* -------------------------------------------------------------------- */
/* search engine state */

/* default character predicates (run sre_chars.py to regenerate tables) */

#define SRE_DIGIT_MASK 1
#define SRE_SPACE_MASK 2
#define SRE_LINEBREAK_MASK 4
#define SRE_ALNUM_MASK 8
#define SRE_WORD_MASK 16

/* FIXME: this assumes ASCII.  create tables in init_sre() instead */

static char sre_char_info[128] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 6, 2,
2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 25, 25, 25, 25, 25, 25, 25,
25, 25, 0, 0, 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 0, 0,
0, 0, 16, 0, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 0, 0, 0, 0, 0 };

static char sre_char_lower[128] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
61, 62, 63, 64, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107,
108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
122, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105,
106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
120, 121, 122, 123, 124, 125, 126, 127 };

#define SRE_IS_DIGIT(ch)\
    ((ch) < 128 ? (sre_char_info[(ch)] & SRE_DIGIT_MASK) : 0)
#define SRE_IS_SPACE(ch)\
    ((ch) < 128 ? (sre_char_info[(ch)] & SRE_SPACE_MASK) : 0)
#define SRE_IS_LINEBREAK(ch)\
    ((ch) < 128 ? (sre_char_info[(ch)] & SRE_LINEBREAK_MASK) : 0)
#define SRE_IS_ALNUM(ch)\
    ((ch) < 128 ? (sre_char_info[(ch)] & SRE_ALNUM_MASK) : 0)
#define SRE_IS_WORD(ch)\
    ((ch) < 128 ? (sre_char_info[(ch)] & SRE_WORD_MASK) : 0)

static unsigned int sre_lower(unsigned int ch)
{
    return ((ch) < 128 ? (unsigned int)sre_char_lower[ch] : ch);
}

/* locale-specific character predicates */
/* !(c & ~N) == (c < N+1) for any unsigned c, this avoids
 * warnings when c's type supports only numbers < N+1 */
#define SRE_LOC_IS_DIGIT(ch) (!((ch) & ~255) ? isdigit((ch)) : 0)
#define SRE_LOC_IS_SPACE(ch) (!((ch) & ~255) ? isspace((ch)) : 0)
#define SRE_LOC_IS_LINEBREAK(ch) ((ch) == '\n')
#define SRE_LOC_IS_ALNUM(ch) (!((ch) & ~255) ? isalnum((ch)) : 0)
#define SRE_LOC_IS_WORD(ch) (SRE_LOC_IS_ALNUM((ch)) || (ch) == '_')

static unsigned int sre_lower_locale(unsigned int ch)
{
    return ((ch) < 256 ? (unsigned int)tolower((ch)) : ch);
}

/* unicode-specific character predicates */

#define SRE_UNI_IS_DIGIT(ch) sky_unicode_isdecimal(ch)
#define SRE_UNI_IS_SPACE(ch) sky_unicode_isspace(ch)
#define SRE_UNI_IS_LINEBREAK(ch) sky_unicode_islinebreak(ch)
#define SRE_UNI_IS_ALNUM(ch) sky_unicode_isalnum(ch)
#define SRE_UNI_IS_WORD(ch) (SRE_UNI_IS_ALNUM(ch) || (ch) == '_')

static unsigned int sre_lower_unicode(unsigned int ch)
{
    return (unsigned int) sky_unicode_tolower(ch);
}

LOCAL(int)
sre_category(SRE_CODE category, unsigned int ch)
{
    switch (category) {

    case SRE_CATEGORY_DIGIT:
        return SRE_IS_DIGIT(ch);
    case SRE_CATEGORY_NOT_DIGIT:
        return !SRE_IS_DIGIT(ch);
    case SRE_CATEGORY_SPACE:
        return SRE_IS_SPACE(ch);
    case SRE_CATEGORY_NOT_SPACE:
        return !SRE_IS_SPACE(ch);
    case SRE_CATEGORY_WORD:
        return SRE_IS_WORD(ch);
    case SRE_CATEGORY_NOT_WORD:
        return !SRE_IS_WORD(ch);
    case SRE_CATEGORY_LINEBREAK:
        return SRE_IS_LINEBREAK(ch);
    case SRE_CATEGORY_NOT_LINEBREAK:
        return !SRE_IS_LINEBREAK(ch);

    case SRE_CATEGORY_LOC_WORD:
        return SRE_LOC_IS_WORD(ch);
    case SRE_CATEGORY_LOC_NOT_WORD:
        return !SRE_LOC_IS_WORD(ch);

    case SRE_CATEGORY_UNI_DIGIT:
        return SRE_UNI_IS_DIGIT(ch);
    case SRE_CATEGORY_UNI_NOT_DIGIT:
        return !SRE_UNI_IS_DIGIT(ch);
    case SRE_CATEGORY_UNI_SPACE:
        return SRE_UNI_IS_SPACE(ch);
    case SRE_CATEGORY_UNI_NOT_SPACE:
        return !SRE_UNI_IS_SPACE(ch);
    case SRE_CATEGORY_UNI_WORD:
        return SRE_UNI_IS_WORD(ch);
    case SRE_CATEGORY_UNI_NOT_WORD:
        return !SRE_UNI_IS_WORD(ch);
    case SRE_CATEGORY_UNI_LINEBREAK:
        return SRE_UNI_IS_LINEBREAK(ch);
    case SRE_CATEGORY_UNI_NOT_LINEBREAK:
        return !SRE_UNI_IS_LINEBREAK(ch);
    }
    return 0;
}

/* helpers */

static void
data_stack_dealloc(SRE_STATE* state)
{
    if (state->data_stack) {
        sky_free(state->data_stack);
        state->data_stack = NULL;
    }
    state->data_stack_size = state->data_stack_base = 0;
}

static int
data_stack_grow(SRE_STATE* state, ssize_t size)
{
    ssize_t minsize, cursize;
    minsize = state->data_stack_base+size;
    cursize = state->data_stack_size;
    if (cursize < minsize) {
        void* stack;
        cursize = minsize+minsize/4+1024;
        TRACE(("allocate/grow stack %zd\n", cursize));
        stack = sky_realloc(state->data_stack, cursize);
        if (!stack) {
            data_stack_dealloc(state);
            return SRE_ERROR_MEMORY;
        }
        state->data_stack = (char *)stack;
        state->data_stack_size = cursize;
    }
    return 0;
}

/* generate 8-bit version */

#define SRE_CHAR unsigned char
#define SRE_CHARGET(state, buf, index) ((unsigned char*)buf)[index]
#define SRE_AT sre_at
#define SRE_COUNT sre_count
#define SRE_CHARSET sre_charset
#define SRE_INFO sre_info
#define SRE_MATCH sre_match
#define SRE_MATCH_CONTEXT sre_match_context
#define SRE_SEARCH sre_search

#define SRE_RECURSIVE
#include "module__sre.c"
#undef SRE_RECURSIVE

#undef SRE_SEARCH
#undef SRE_MATCH
#undef SRE_MATCH_CONTEXT
#undef SRE_INFO
#undef SRE_CHARSET
#undef SRE_COUNT
#undef SRE_AT
#undef SRE_CHAR
#undef SRE_CHARGET

/* generate 8/16/32-bit unicode version */

#define SRE_CHAR void
#define SRE_CHARGET(state, buf, index) \
    ((state->charsize==1) ? ((uint8_t*)buf)[index] : \
     (state->charsize==2) ? ((uint16_t*)buf)[index] : \
     ((uint32_t*)buf)[index])
#define SRE_AT sre_uat
#define SRE_COUNT sre_ucount
#define SRE_CHARSET sre_ucharset
#define SRE_INFO sre_uinfo
#define SRE_MATCH sre_umatch
#define SRE_MATCH_CONTEXT sre_umatch_context
#define SRE_SEARCH sre_usearch

#endif /* SRE_RECURSIVE */

/* -------------------------------------------------------------------- */
/* String matching engine */

/* the following section is compiled twice, with different character
   settings */

LOCAL(int)
SRE_AT(SRE_STATE* state, char* ptr, SRE_CODE at)
{
    /* check if pointer is at given position */

    ssize_t thisp, thatp;

    switch (at) {

    case SRE_AT_BEGINNING:
    case SRE_AT_BEGINNING_STRING:
        return ((void*) ptr == state->beginning);

    case SRE_AT_BEGINNING_LINE:
        return ((void*) ptr == state->beginning ||
                SRE_IS_LINEBREAK((int) SRE_CHARGET(state, ptr, -1)));

    case SRE_AT_END:
        return (((void*) (ptr+state->charsize) == state->end &&
                 SRE_IS_LINEBREAK((int) SRE_CHARGET(state, ptr, 0))) ||
                ((void*) ptr == state->end));

    case SRE_AT_END_LINE:
        return ((void*) ptr == state->end ||
                SRE_IS_LINEBREAK((int) SRE_CHARGET(state, ptr, 0)));

    case SRE_AT_END_STRING:
        return ((void*) ptr == state->end);

    case SRE_AT_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp != thatp;

    case SRE_AT_NON_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp == thatp;

    case SRE_AT_LOC_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_LOC_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_LOC_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp != thatp;

    case SRE_AT_LOC_NON_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_LOC_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_LOC_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp == thatp;

    case SRE_AT_UNI_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_UNI_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_UNI_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp != thatp;

    case SRE_AT_UNI_NON_BOUNDARY:
        if (state->beginning == state->end)
            return 0;
        thatp = ((void*) ptr > state->beginning) ?
            SRE_UNI_IS_WORD((int) SRE_CHARGET(state, ptr, -1)) : 0;
        thisp = ((void*) ptr < state->end) ?
            SRE_UNI_IS_WORD((int) SRE_CHARGET(state, ptr, 0)) : 0;
        return thisp == thatp;

    }

    return 0;
}

LOCAL(int)
SRE_CHARSET(SRE_CODE* set, SRE_CODE ch)
{
    /* check if character is a member of the given set */

    int ok = 1;

    for (;;) {
        switch (*set++) {

        case SRE_OP_FAILURE:
            return !ok;

        case SRE_OP_LITERAL:
            /* <LITERAL> <code> */
            if (ch == set[0])
                return ok;
            set++;
            break;

        case SRE_OP_CATEGORY:
            /* <CATEGORY> <code> */
            if (sre_category(set[0], (int) ch))
                return ok;
            set += 1;
            break;

        case SRE_OP_CHARSET:
            if (sizeof(SRE_CODE) == 2) {
                /* <CHARSET> <bitmap> (16 bits per code word) */
                if (ch < 256 && (set[ch >> 4] & (1 << (ch & 15))))
                    return ok;
                set += 16;
            }
            else {
                /* <CHARSET> <bitmap> (32 bits per code word) */
                if (ch < 256 && (set[ch >> 5] & (1u << (ch & 31))))
                    return ok;
                set += 8;
            }
            break;

        case SRE_OP_RANGE:
            /* <RANGE> <lower> <upper> */
            if (set[0] <= ch && ch <= set[1])
                return ok;
            set += 2;
            break;

        case SRE_OP_NEGATE:
            ok = !ok;
            break;

        case SRE_OP_BIGCHARSET:
            /* <BIGCHARSET> <blockcount> <256 blockindices> <blocks> */
        {
            ssize_t count, block;
            count = *(set++);

            if (sizeof(SRE_CODE) == 2) {
                block = ((unsigned char*)set)[ch >> 8];
                set += 128;
                if (set[block*16 + ((ch & 255)>>4)] & (1 << (ch & 15)))
                    return ok;
                set += count*16;
            }
            else {
                /* !(c & ~N) == (c < N+1) for any unsigned c, this avoids
                 * warnings when c's type supports only numbers < N+1 */
                if (!(ch & ~65535))
                    block = ((unsigned char*)set)[ch >> 8];
                else
                    block = -1;
                set += 64;
                if (block >=0 &&
                    (set[block*8 + ((ch & 255)>>5)] & (1u << (ch & 31))))
                    return ok;
                set += count*8;
            }
            break;
        }

        default:
            /* internal error -- there's not much we can do about it
               here, so let's just pretend it didn't match... */
            return 0;
        }
    }
}

LOCAL(ssize_t) SRE_MATCH(SRE_STATE* state, SRE_CODE* pattern);

LOCAL(ssize_t)
SRE_COUNT(SRE_STATE* state, SRE_CODE* pattern, ssize_t maxcount)
{
    SRE_CODE chr;
    char* ptr = (char *)state->ptr;
    char* end = (char *)state->end;
    ssize_t i;

    /* adjust end */
    if (maxcount < (end - ptr) / state->charsize && maxcount != SRE_MAXREPEAT)
        end = ptr + maxcount*state->charsize;

    switch (pattern[0]) {

    case SRE_OP_IN:
        /* repeated set */
        TRACE(("|%p|%p|COUNT IN\n", pattern, ptr));
        while (ptr < end &&
               SRE_CHARSET(pattern + 2, SRE_CHARGET(state, ptr, 0)))
            ptr += state->charsize;
        break;

    case SRE_OP_ANY:
        /* repeated dot wildcard. */
        TRACE(("|%p|%p|COUNT ANY\n", pattern, ptr));
        while (ptr < end && !SRE_IS_LINEBREAK(SRE_CHARGET(state, ptr, 0)))
            ptr += state->charsize;
        break;

    case SRE_OP_ANY_ALL:
        /* repeated dot wildcard.  skip to the end of the target
           string, and backtrack from there */
        TRACE(("|%p|%p|COUNT ANY_ALL\n", pattern, ptr));
        ptr = end;
        break;

    case SRE_OP_LITERAL:
        /* repeated literal */
        chr = pattern[1];
        TRACE(("|%p|%p|COUNT LITERAL %d\n", pattern, ptr, chr));
        while (ptr < end && (SRE_CODE) SRE_CHARGET(state, ptr, 0) == chr)
            ptr += state->charsize;
        break;

    case SRE_OP_LITERAL_IGNORE:
        /* repeated literal */
        chr = pattern[1];
        TRACE(("|%p|%p|COUNT LITERAL_IGNORE %d\n", pattern, ptr, chr));
        while (ptr < end && (SRE_CODE) state->lower(SRE_CHARGET(state, ptr, 0)) == chr)
            ptr += state->charsize;
        break;

    case SRE_OP_NOT_LITERAL:
        /* repeated non-literal */
        chr = pattern[1];
        TRACE(("|%p|%p|COUNT NOT_LITERAL %d\n", pattern, ptr, chr));
        while (ptr < end && (SRE_CODE) SRE_CHARGET(state, ptr, 0) != chr)
            ptr += state->charsize;
        break;

    case SRE_OP_NOT_LITERAL_IGNORE:
        /* repeated non-literal */
        chr = pattern[1];
        TRACE(("|%p|%p|COUNT NOT_LITERAL_IGNORE %d\n", pattern, ptr, chr));
        while (ptr < end && (SRE_CODE) state->lower(SRE_CHARGET(state, ptr, 0)) != chr)
            ptr += state->charsize;
        break;

    default:
        /* repeated single character pattern */
        TRACE(("|%p|%p|COUNT SUBPATTERN\n", pattern, ptr));
        while ((char*) state->ptr < end) {
            i = SRE_MATCH(state, pattern);
            if (i < 0)
                return i;
            if (!i)
                break;
        }
        TRACE(("|%p|%p|COUNT %zd\n", pattern, ptr,
               ((char*)state->ptr - ptr)/state->charsize));
        return ((char*)state->ptr - ptr)/state->charsize;
    }

    TRACE(("|%p|%p|COUNT %zd\n", pattern, ptr,
           (ptr - (char*) state->ptr)/state->charsize));
    return (ptr - (char*) state->ptr)/state->charsize;
}

#if 0 /* not used in this release */
LOCAL(int)
SRE_INFO(SRE_STATE* state, SRE_CODE* pattern)
{
    /* check if an SRE_OP_INFO block matches at the current position.
       returns the number of SRE_CODE objects to skip if successful, 0
       if no match */

    char* end = state->end;
    char* ptr = state->ptr;
    ssize_t i;

    /* check minimal length */
    if (pattern[3] && (end - ptr)/state->charsize < pattern[3])
        return 0;

    /* check known prefix */
    if (pattern[2] & SRE_INFO_PREFIX && pattern[5] > 1) {
        /* <length> <skip> <prefix data> <overlap data> */
        for (i = 0; i < pattern[5]; i++)
            if ((SRE_CODE) SRE_CHARGET(state, ptr, i) != pattern[7 + i])
                return 0;
        return pattern[0] + 2 * pattern[6];
    }
    return pattern[0];
}
#endif

/* The macros below should be used to protect recursive SRE_MATCH()
 * calls that *failed* and do *not* return immediately (IOW, those
 * that will backtrack). Explaining:
 *
 * - Recursive SRE_MATCH() returned true: that's usually a success
 *   (besides atypical cases like ASSERT_NOT), therefore there's no
 *   reason to restore lastmark;
 *
 * - Recursive SRE_MATCH() returned false but the current SRE_MATCH()
 *   is returning to the caller: If the current SRE_MATCH() is the
 *   top function of the recursion, returning false will be a matching
 *   failure, and it doesn't matter where lastmark is pointing to.
 *   If it's *not* the top function, it will be a recursive SRE_MATCH()
 *   failure by itself, and the calling SRE_MATCH() will have to deal
 *   with the failure by the same rules explained here (it will restore
 *   lastmark by itself if necessary);
 *
 * - Recursive SRE_MATCH() returned false, and will continue the
 *   outside 'for' loop: must be protected when breaking, since the next
 *   OP could potentially depend on lastmark;
 *
 * - Recursive SRE_MATCH() returned false, and will be called again
 *   inside a local for/while loop: must be protected between each
 *   loop iteration, since the recursive SRE_MATCH() could do anything,
 *   and could potentially depend on lastmark.
 *
 * For more information, check the discussion at SF patch #712900.
 */
#define LASTMARK_SAVE()     \
    do { \
        ctx->lastmark = state->lastmark; \
        ctx->lastindex = state->lastindex; \
    } while (0)
#define LASTMARK_RESTORE()  \
    do { \
        state->lastmark = ctx->lastmark; \
        state->lastindex = ctx->lastindex; \
    } while (0)

#define RETURN_ERROR(i) do { return i; } while(0)
#define RETURN_FAILURE do { ret = 0; goto exit; } while(0)
#define RETURN_SUCCESS do { ret = 1; goto exit; } while(0)

#define RETURN_ON_ERROR(i) \
    do { if (i < 0) RETURN_ERROR(i); } while (0)
#define RETURN_ON_SUCCESS(i) \
    do { RETURN_ON_ERROR(i); if (i > 0) RETURN_SUCCESS; } while (0)
#define RETURN_ON_FAILURE(i) \
    do { RETURN_ON_ERROR(i); if (i == 0) RETURN_FAILURE; } while (0)

#define SFY(x) #x

#define DATA_STACK_ALLOC(state, type, ptr) \
do { \
    alloc_pos = state->data_stack_base; \
    TRACE(("allocating %s in %zd (%d)\n", \
           SFY(type), alloc_pos, sizeof(type))); \
    if (sizeof(type) > state->data_stack_size - alloc_pos) { \
        int j = data_stack_grow(state, sizeof(type)); \
        if (j < 0) return j; \
        if (ctx_pos != -1) \
            DATA_STACK_LOOKUP_AT(state, SRE_MATCH_CONTEXT, ctx, ctx_pos); \
    } \
    ptr = (type*)(state->data_stack+alloc_pos); \
    state->data_stack_base += sizeof(type); \
} while (0)

#define DATA_STACK_LOOKUP_AT(state, type, ptr, pos) \
do { \
    TRACE(("looking up %s at %zd\n", SFY(type), pos)); \
    ptr = (type*)(state->data_stack+pos); \
} while (0)

#define DATA_STACK_PUSH(state, data, size) \
do { \
    TRACE(("copy data in %p to %zd (%zd)\n", \
           data, state->data_stack_base, size)); \
    if (size > state->data_stack_size - state->data_stack_base) { \
        int j = data_stack_grow(state, size); \
        if (j < 0) return j; \
        if (ctx_pos != -1) \
            DATA_STACK_LOOKUP_AT(state, SRE_MATCH_CONTEXT, ctx, ctx_pos); \
    } \
    memcpy(state->data_stack+state->data_stack_base, data, size); \
    state->data_stack_base += size; \
} while (0)

#define DATA_STACK_POP(state, data, size, discard) \
do { \
    TRACE(("copy data to %p from %zd (%zd)\n", \
           data, state->data_stack_base-size, size)); \
    memcpy(data, state->data_stack+state->data_stack_base-size, size); \
    if (discard) \
        state->data_stack_base -= size; \
} while (0)

#define DATA_STACK_POP_DISCARD(state, size) \
do { \
    TRACE(("discard data from %zd (%zd)\n", \
           state->data_stack_base-size, size)); \
    state->data_stack_base -= size; \
} while(0)

#define DATA_PUSH(x) \
    DATA_STACK_PUSH(state, (x), sizeof(*(x)))
#define DATA_POP(x) \
    DATA_STACK_POP(state, (x), sizeof(*(x)), 1)
#define DATA_POP_DISCARD(x) \
    DATA_STACK_POP_DISCARD(state, sizeof(*(x)))
#define DATA_ALLOC(t,p) \
    DATA_STACK_ALLOC(state, t, p)
#define DATA_LOOKUP_AT(t,p,pos) \
    DATA_STACK_LOOKUP_AT(state,t,p,pos)

#define MARK_PUSH(lastmark) \
    do if (lastmark > 0) { \
        i = lastmark; /* ctx->lastmark may change if reallocated */ \
        DATA_STACK_PUSH(state, state->mark, (i+1)*sizeof(void*)); \
    } while (0)
#define MARK_POP(lastmark) \
    do if (lastmark > 0) { \
        DATA_STACK_POP(state, state->mark, (lastmark+1)*sizeof(void*), 1); \
    } while (0)
#define MARK_POP_KEEP(lastmark) \
    do if (lastmark > 0) { \
        DATA_STACK_POP(state, state->mark, (lastmark+1)*sizeof(void*), 0); \
    } while (0)
#define MARK_POP_DISCARD(lastmark) \
    do if (lastmark > 0) { \
        DATA_STACK_POP_DISCARD(state, (lastmark+1)*sizeof(void*)); \
    } while (0)

#define JUMP_NONE            0
#define JUMP_MAX_UNTIL_1     1
#define JUMP_MAX_UNTIL_2     2
#define JUMP_MAX_UNTIL_3     3
#define JUMP_MIN_UNTIL_1     4
#define JUMP_MIN_UNTIL_2     5
#define JUMP_MIN_UNTIL_3     6
#define JUMP_REPEAT          7
#define JUMP_REPEAT_ONE_1    8
#define JUMP_REPEAT_ONE_2    9
#define JUMP_MIN_REPEAT_ONE  10
#define JUMP_BRANCH          11
#define JUMP_ASSERT          12
#define JUMP_ASSERT_NOT      13

#define DO_JUMP(jumpvalue, jumplabel, nextpattern) \
    DATA_ALLOC(SRE_MATCH_CONTEXT, nextctx); \
    nextctx->last_ctx_pos = ctx_pos; \
    nextctx->jump = jumpvalue; \
    nextctx->pattern = nextpattern; \
    ctx_pos = alloc_pos; \
    ctx = nextctx; \
    goto entrance; \
    jumplabel: \
    while (0) /* gcc doesn't like labels at end of scopes */ \

typedef struct {
    ssize_t last_ctx_pos;
    ssize_t jump;
    char* ptr;
    SRE_CODE* pattern;
    ssize_t count;
    ssize_t lastmark;
    ssize_t lastindex;
    union {
        SRE_CODE chr;
        SRE_REPEAT* rep;
    } u;
} SRE_MATCH_CONTEXT;

/* check if string matches the given pattern.  returns <0 for
   error, 0 for failure, and 1 for success */
LOCAL(ssize_t)
SRE_MATCH(SRE_STATE* state, SRE_CODE* pattern)
{
    char* end = (char*)state->end;
    ssize_t alloc_pos, ctx_pos = -1;
    ssize_t i, ret = 0;
    ssize_t jump;
    unsigned int sigcount=0;

    SRE_MATCH_CONTEXT* ctx;
    SRE_MATCH_CONTEXT* nextctx;

    TRACE(("|%p|%p|ENTER\n", pattern, state->ptr));

    DATA_ALLOC(SRE_MATCH_CONTEXT, ctx);
    ctx->last_ctx_pos = -1;
    ctx->jump = JUMP_NONE;
    ctx->pattern = pattern;
    ctx_pos = alloc_pos;

entrance:

    ctx->ptr = (char *)state->ptr;

    if (ctx->pattern[0] == SRE_OP_INFO) {
        /* optimization info block */
        /* <INFO> <1=skip> <2=flags> <3=min> ... */
        if (ctx->pattern[3] && (end - ctx->ptr)/state->charsize < ctx->pattern[3]) {
            TRACE(("reject (got %zd chars, need %zd)\n",
                   (end - ctx->ptr)/state->charsize, ctx->pattern[3]));
            RETURN_FAILURE;
        }
        ctx->pattern += ctx->pattern[1] + 1;
    }

    for (;;) {
        ++sigcount;
        if ((0 == (sigcount & 0xfff))) {
            sky_signal_check_pending();
        }

        switch (*ctx->pattern++) {

        case SRE_OP_MARK:
            /* set mark */
            /* <MARK> <gid> */
            TRACE(("|%p|%p|MARK %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[0]));
            i = ctx->pattern[0];
            if (i & 1)
                state->lastindex = i/2 + 1;
            if (i > state->lastmark) {
                /* state->lastmark is the highest valid index in the
                   state->mark array.  If it is increased by more than 1,
                   the intervening marks must be set to NULL to signal
                   that these marks have not been encountered. */
                ssize_t j = state->lastmark + 1;
                while (j < i)
                    state->mark[j++] = NULL;
                state->lastmark = i;
            }
            state->mark[i] = ctx->ptr;
            ctx->pattern++;
            break;

        case SRE_OP_LITERAL:
            /* match literal string */
            /* <LITERAL> <code> */
            TRACE(("|%p|%p|LITERAL %d\n", ctx->pattern,
                   ctx->ptr, *ctx->pattern));
            if (ctx->ptr >= end || (SRE_CODE) SRE_CHARGET(state, ctx->ptr, 0) != ctx->pattern[0])
                RETURN_FAILURE;
            ctx->pattern++;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_NOT_LITERAL:
            /* match anything that is not literal character */
            /* <NOT_LITERAL> <code> */
            TRACE(("|%p|%p|NOT_LITERAL %d\n", ctx->pattern,
                   ctx->ptr, *ctx->pattern));
            if (ctx->ptr >= end || (SRE_CODE) SRE_CHARGET(state, ctx->ptr, 0) == ctx->pattern[0])
                RETURN_FAILURE;
            ctx->pattern++;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_SUCCESS:
            /* end of pattern */
            TRACE(("|%p|%p|SUCCESS\n", ctx->pattern, ctx->ptr));
            state->ptr = ctx->ptr;
            RETURN_SUCCESS;

        case SRE_OP_AT:
            /* match at given position */
            /* <AT> <code> */
            TRACE(("|%p|%p|AT %d\n", ctx->pattern, ctx->ptr, *ctx->pattern));
            if (!SRE_AT(state, ctx->ptr, *ctx->pattern))
                RETURN_FAILURE;
            ctx->pattern++;
            break;

        case SRE_OP_CATEGORY:
            /* match at given category */
            /* <CATEGORY> <code> */
            TRACE(("|%p|%p|CATEGORY %d\n", ctx->pattern,
                   ctx->ptr, *ctx->pattern));
            if (ctx->ptr >= end || !sre_category(ctx->pattern[0], SRE_CHARGET(state, ctx->ptr, 0)))
                RETURN_FAILURE;
            ctx->pattern++;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_ANY:
            /* match anything (except a newline) */
            /* <ANY> */
            TRACE(("|%p|%p|ANY\n", ctx->pattern, ctx->ptr));
                if (ctx->ptr >= end || SRE_IS_LINEBREAK(SRE_CHARGET(state, ctx->ptr, 0)))
                    RETURN_FAILURE;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_ANY_ALL:
            /* match anything */
            /* <ANY_ALL> */
            TRACE(("|%p|%p|ANY_ALL\n", ctx->pattern, ctx->ptr));
            if (ctx->ptr >= end)
                RETURN_FAILURE;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_IN:
            /* match set member (or non_member) */
            /* <IN> <skip> <set> */
            TRACE(("|%p|%p|IN\n", ctx->pattern, ctx->ptr));
                if (ctx->ptr >= end || !SRE_CHARSET(ctx->pattern + 1, SRE_CHARGET(state, ctx->ptr, 0)))
                    RETURN_FAILURE;
            ctx->pattern += ctx->pattern[0];
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_LITERAL_IGNORE:
            TRACE(("|%p|%p|LITERAL_IGNORE %d\n",
                   ctx->pattern, ctx->ptr, ctx->pattern[0]));
            if (ctx->ptr >= end ||
                state->lower(SRE_CHARGET(state, ctx->ptr, 0)) != state->lower(*ctx->pattern))
                RETURN_FAILURE;
            ctx->pattern++;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_NOT_LITERAL_IGNORE:
            TRACE(("|%p|%p|NOT_LITERAL_IGNORE %d\n",
                   ctx->pattern, ctx->ptr, *ctx->pattern));
            if (ctx->ptr >= end ||
                state->lower(SRE_CHARGET(state, ctx->ptr, 0)) == state->lower(*ctx->pattern))
                RETURN_FAILURE;
            ctx->pattern++;
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_IN_IGNORE:
            TRACE(("|%p|%p|IN_IGNORE\n", ctx->pattern, ctx->ptr));
            if (ctx->ptr >= end
                || !SRE_CHARSET(ctx->pattern+1,
                                (SRE_CODE)state->lower(SRE_CHARGET(state, ctx->ptr, 0))))
                RETURN_FAILURE;
            ctx->pattern += ctx->pattern[0];
            ctx->ptr += state->charsize;
            break;

        case SRE_OP_JUMP:
        case SRE_OP_INFO:
            /* jump forward */
            /* <JUMP> <offset> */
            TRACE(("|%p|%p|JUMP %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[0]));
            ctx->pattern += ctx->pattern[0];
            break;

        case SRE_OP_BRANCH:
            /* alternation */
            /* <BRANCH> <0=skip> code <JUMP> ... <NULL> */
            TRACE(("|%p|%p|BRANCH\n", ctx->pattern, ctx->ptr));
            LASTMARK_SAVE();
            ctx->u.rep = state->repeat;
            if (ctx->u.rep)
                MARK_PUSH(ctx->lastmark);
            for (; ctx->pattern[0]; ctx->pattern += ctx->pattern[0]) {
                if (ctx->pattern[1] == SRE_OP_LITERAL &&
                    (ctx->ptr >= end ||
                     (SRE_CODE) SRE_CHARGET(state, ctx->ptr, 0) != ctx->pattern[2]))
                    continue;
                if (ctx->pattern[1] == SRE_OP_IN &&
                    (ctx->ptr >= end ||
                     !SRE_CHARSET(ctx->pattern + 3, (SRE_CODE) SRE_CHARGET(state, ctx->ptr, 0))))
                    continue;
                state->ptr = ctx->ptr;
                DO_JUMP(JUMP_BRANCH, jump_branch, ctx->pattern+1);
                if (ret) {
                    if (ctx->u.rep)
                        MARK_POP_DISCARD(ctx->lastmark);
                    RETURN_ON_ERROR(ret);
                    RETURN_SUCCESS;
                }
                if (ctx->u.rep)
                    MARK_POP_KEEP(ctx->lastmark);
                LASTMARK_RESTORE();
            }
            if (ctx->u.rep)
                MARK_POP_DISCARD(ctx->lastmark);
            RETURN_FAILURE;

        case SRE_OP_REPEAT_ONE:
            /* match repeated sequence (maximizing regexp) */

            /* this operator only works if the repeated item is
               exactly one character wide, and we're not already
               collecting backtracking points.  for other cases,
               use the MAX_REPEAT operator */

            /* <REPEAT_ONE> <skip> <1=min> <2=max> item <SUCCESS> tail */

            TRACE(("|%p|%p|REPEAT_ONE %d %d\n", ctx->pattern, ctx->ptr,
                   ctx->pattern[1], ctx->pattern[2]));

            if ((ssize_t) ctx->pattern[1] > (end - ctx->ptr) / state->charsize)
                RETURN_FAILURE; /* cannot match */

            state->ptr = ctx->ptr;

            ret = SRE_COUNT(state, ctx->pattern+3, ctx->pattern[2]);
            RETURN_ON_ERROR(ret);
            DATA_LOOKUP_AT(SRE_MATCH_CONTEXT, ctx, ctx_pos);
            ctx->count = ret;
            ctx->ptr += state->charsize * ctx->count;

            /* when we arrive here, count contains the number of
               matches, and ctx->ptr points to the tail of the target
               string.  check if the rest of the pattern matches,
               and backtrack if not. */

            if (ctx->count < (ssize_t) ctx->pattern[1])
                RETURN_FAILURE;

            if (ctx->pattern[ctx->pattern[0]] == SRE_OP_SUCCESS) {
                /* tail is empty.  we're finished */
                state->ptr = ctx->ptr;
                RETURN_SUCCESS;
            }

            LASTMARK_SAVE();

            if (ctx->pattern[ctx->pattern[0]] == SRE_OP_LITERAL) {
                /* tail starts with a literal. skip positions where
                   the rest of the pattern cannot possibly match */
                ctx->u.chr = ctx->pattern[ctx->pattern[0]+1];
                for (;;) {
                    while (ctx->count >= (ssize_t) ctx->pattern[1] &&
                           (ctx->ptr >= end ||
                            SRE_CHARGET(state, ctx->ptr, 0) != ctx->u.chr)) {
                        ctx->ptr -= state->charsize;
                        ctx->count--;
                    }
                    if (ctx->count < (ssize_t) ctx->pattern[1])
                        break;
                    state->ptr = ctx->ptr;
                    DO_JUMP(JUMP_REPEAT_ONE_1, jump_repeat_one_1,
                            ctx->pattern+ctx->pattern[0]);
                    if (ret) {
                        RETURN_ON_ERROR(ret);
                        RETURN_SUCCESS;
                    }

                    LASTMARK_RESTORE();

                    ctx->ptr -= state->charsize;
                    ctx->count--;
                }

            } else {
                /* general case */
                while (ctx->count >= (ssize_t) ctx->pattern[1]) {
                    state->ptr = ctx->ptr;
                    DO_JUMP(JUMP_REPEAT_ONE_2, jump_repeat_one_2,
                            ctx->pattern+ctx->pattern[0]);
                    if (ret) {
                        RETURN_ON_ERROR(ret);
                        RETURN_SUCCESS;
                    }
                    ctx->ptr -= state->charsize;
                    ctx->count--;
                    LASTMARK_RESTORE();
                }
            }
            RETURN_FAILURE;

        case SRE_OP_MIN_REPEAT_ONE:
            /* match repeated sequence (minimizing regexp) */

            /* this operator only works if the repeated item is
               exactly one character wide, and we're not already
               collecting backtracking points.  for other cases,
               use the MIN_REPEAT operator */

            /* <MIN_REPEAT_ONE> <skip> <1=min> <2=max> item <SUCCESS> tail */

            TRACE(("|%p|%p|MIN_REPEAT_ONE %d %d\n", ctx->pattern, ctx->ptr,
                   ctx->pattern[1], ctx->pattern[2]));

            if (ctx->ptr + state->charsize * ctx->pattern[1] > end)
                RETURN_FAILURE; /* cannot match */

            state->ptr = ctx->ptr;

            if (ctx->pattern[1] == 0)
                ctx->count = 0;
            else {
                /* count using pattern min as the maximum */
                ret = SRE_COUNT(state, ctx->pattern+3, ctx->pattern[1]);
                RETURN_ON_ERROR(ret);
                DATA_LOOKUP_AT(SRE_MATCH_CONTEXT, ctx, ctx_pos);
                if (ret < (ssize_t) ctx->pattern[1])
                    /* didn't match minimum number of times */
                    RETURN_FAILURE;
                /* advance past minimum matches of repeat */
                ctx->count = ret;
                ctx->ptr += state->charsize * ctx->count;
            }

            if (ctx->pattern[ctx->pattern[0]] == SRE_OP_SUCCESS) {
                /* tail is empty.  we're finished */
                state->ptr = ctx->ptr;
                RETURN_SUCCESS;

            } else {
                /* general case */
                LASTMARK_SAVE();
                while ((ssize_t)ctx->pattern[2] == SRE_MAXREPEAT
                       || ctx->count <= (ssize_t)ctx->pattern[2]) {
                    state->ptr = ctx->ptr;
                    DO_JUMP(JUMP_MIN_REPEAT_ONE,jump_min_repeat_one,
                            ctx->pattern+ctx->pattern[0]);
                    if (ret) {
                        RETURN_ON_ERROR(ret);
                        RETURN_SUCCESS;
                    }
                    state->ptr = ctx->ptr;
                    ret = SRE_COUNT(state, ctx->pattern+3, 1);
                    RETURN_ON_ERROR(ret);
                    DATA_LOOKUP_AT(SRE_MATCH_CONTEXT, ctx, ctx_pos);
                    if (ret == 0)
                        break;
                    sky_error_validate_debug(ret == 1);
                    ctx->ptr += state->charsize;
                    ctx->count++;
                    LASTMARK_RESTORE();
                }
            }
            RETURN_FAILURE;

        case SRE_OP_REPEAT:
            /* create repeat context.  all the hard work is done
               by the UNTIL operator (MAX_UNTIL, MIN_UNTIL) */
            /* <REPEAT> <skip> <1=min> <2=max> item <UNTIL> tail */
            TRACE(("|%p|%p|REPEAT %d %d\n", ctx->pattern, ctx->ptr,
                   ctx->pattern[1], ctx->pattern[2]));

            /* install new repeat context */
            ctx->u.rep = (SRE_REPEAT*) sky_malloc(sizeof(*ctx->u.rep));
            ctx->u.rep->count = -1;
            ctx->u.rep->pattern = ctx->pattern;
            ctx->u.rep->prev = state->repeat;
            ctx->u.rep->last_ptr = NULL;
            state->repeat = ctx->u.rep;

            state->ptr = ctx->ptr;
            DO_JUMP(JUMP_REPEAT, jump_repeat, ctx->pattern+ctx->pattern[0]);
            state->repeat = ctx->u.rep->prev;
            sky_free(ctx->u.rep);

            if (ret) {
                RETURN_ON_ERROR(ret);
                RETURN_SUCCESS;
            }
            RETURN_FAILURE;

        case SRE_OP_MAX_UNTIL:
            /* maximizing repeat */
            /* <REPEAT> <skip> <1=min> <2=max> item <MAX_UNTIL> tail */

            /* FIXME: we probably need to deal with zero-width
               matches in here... */

            ctx->u.rep = state->repeat;
            if (!ctx->u.rep)
                RETURN_ERROR(SRE_ERROR_STATE);

            state->ptr = ctx->ptr;

            ctx->count = ctx->u.rep->count+1;

            TRACE(("|%p|%p|MAX_UNTIL %zd\n", ctx->pattern,
                   ctx->ptr, ctx->count));

            if (ctx->count < (ssize_t) ctx->u.rep->pattern[1]) {
                /* not enough matches */
                ctx->u.rep->count = ctx->count;
                DO_JUMP(JUMP_MAX_UNTIL_1, jump_max_until_1,
                        ctx->u.rep->pattern+3);
                if (ret) {
                    RETURN_ON_ERROR(ret);
                    RETURN_SUCCESS;
                }
                ctx->u.rep->count = ctx->count-1;
                state->ptr = ctx->ptr;
                RETURN_FAILURE;
            }

            if ((ctx->count < (ssize_t) ctx->u.rep->pattern[2] ||
                ctx->u.rep->pattern[2] == SRE_MAXREPEAT) &&
                state->ptr != ctx->u.rep->last_ptr) {
                /* we may have enough matches, but if we can
                   match another item, do so */
                ctx->u.rep->count = ctx->count;
                LASTMARK_SAVE();
                MARK_PUSH(ctx->lastmark);
                /* zero-width match protection */
                DATA_PUSH(&ctx->u.rep->last_ptr);
                ctx->u.rep->last_ptr = state->ptr;
                DO_JUMP(JUMP_MAX_UNTIL_2, jump_max_until_2,
                        ctx->u.rep->pattern+3);
                DATA_POP(&ctx->u.rep->last_ptr);
                if (ret) {
                    MARK_POP_DISCARD(ctx->lastmark);
                    RETURN_ON_ERROR(ret);
                    RETURN_SUCCESS;
                }
                MARK_POP(ctx->lastmark);
                LASTMARK_RESTORE();
                ctx->u.rep->count = ctx->count-1;
                state->ptr = ctx->ptr;
            }

            /* cannot match more repeated items here.  make sure the
               tail matches */
            state->repeat = ctx->u.rep->prev;
            DO_JUMP(JUMP_MAX_UNTIL_3, jump_max_until_3, ctx->pattern);
            RETURN_ON_SUCCESS(ret);
            state->repeat = ctx->u.rep;
            state->ptr = ctx->ptr;
            RETURN_FAILURE;

        case SRE_OP_MIN_UNTIL:
            /* minimizing repeat */
            /* <REPEAT> <skip> <1=min> <2=max> item <MIN_UNTIL> tail */

            ctx->u.rep = state->repeat;
            if (!ctx->u.rep)
                RETURN_ERROR(SRE_ERROR_STATE);

            state->ptr = ctx->ptr;

            ctx->count = ctx->u.rep->count+1;

            TRACE(("|%p|%p|MIN_UNTIL %zd %p\n", ctx->pattern,
                   ctx->ptr, ctx->count, ctx->u.rep->pattern));

            if (ctx->count < (ssize_t) ctx->u.rep->pattern[1]) {
                /* not enough matches */
                ctx->u.rep->count = ctx->count;
                DO_JUMP(JUMP_MIN_UNTIL_1, jump_min_until_1,
                        ctx->u.rep->pattern+3);
                if (ret) {
                    RETURN_ON_ERROR(ret);
                    RETURN_SUCCESS;
                }
                ctx->u.rep->count = ctx->count-1;
                state->ptr = ctx->ptr;
                RETURN_FAILURE;
            }

            LASTMARK_SAVE();

            /* see if the tail matches */
            state->repeat = ctx->u.rep->prev;
            DO_JUMP(JUMP_MIN_UNTIL_2, jump_min_until_2, ctx->pattern);
            if (ret) {
                RETURN_ON_ERROR(ret);
                RETURN_SUCCESS;
            }

            state->repeat = ctx->u.rep;
            state->ptr = ctx->ptr;

            LASTMARK_RESTORE();

            if ((ctx->count >= (ssize_t) ctx->u.rep->pattern[2]
                && ctx->u.rep->pattern[2] != SRE_MAXREPEAT) ||
                state->ptr == ctx->u.rep->last_ptr)
                RETURN_FAILURE;

            ctx->u.rep->count = ctx->count;
            /* zero-width match protection */
            DATA_PUSH(&ctx->u.rep->last_ptr);
            ctx->u.rep->last_ptr = state->ptr;
            DO_JUMP(JUMP_MIN_UNTIL_3,jump_min_until_3,
                    ctx->u.rep->pattern+3);
            DATA_POP(&ctx->u.rep->last_ptr);
            if (ret) {
                RETURN_ON_ERROR(ret);
                RETURN_SUCCESS;
            }
            ctx->u.rep->count = ctx->count-1;
            state->ptr = ctx->ptr;
            RETURN_FAILURE;

        case SRE_OP_GROUPREF:
            /* match backreference */
            TRACE(("|%p|%p|GROUPREF %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[0]));
            i = ctx->pattern[0];
            {
                ssize_t groupref = i+i;
                if (groupref >= state->lastmark) {
                    RETURN_FAILURE;
                } else {
                    char* p = (char*) state->mark[groupref];
                    char* e = (char*) state->mark[groupref+1];
                    if (!p || !e || e < p)
                        RETURN_FAILURE;
                    while (p < e) {
                        if (ctx->ptr >= end ||
                            SRE_CHARGET(state, ctx->ptr, 0) != SRE_CHARGET(state, p, 0))
                            RETURN_FAILURE;
                        p += state->charsize;
                        ctx->ptr += state->charsize;
                    }
                }
            }
            ctx->pattern++;
            break;

        case SRE_OP_GROUPREF_IGNORE:
            /* match backreference */
            TRACE(("|%p|%p|GROUPREF_IGNORE %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[0]));
            i = ctx->pattern[0];
            {
                ssize_t groupref = i+i;
                if (groupref >= state->lastmark) {
                    RETURN_FAILURE;
                } else {
                    char* p = (char*) state->mark[groupref];
                    char* e = (char*) state->mark[groupref+1];
                    if (!p || !e || e < p)
                        RETURN_FAILURE;
                    while (p < e) {
                        if (ctx->ptr >= end ||
                            state->lower(SRE_CHARGET(state, ctx->ptr, 0)) !=
                            state->lower(SRE_CHARGET(state, p, 0)))
                            RETURN_FAILURE;
                        p += state->charsize;
                        ctx->ptr += state->charsize;
                    }
                }
            }
            ctx->pattern++;
            break;

        case SRE_OP_GROUPREF_EXISTS:
            TRACE(("|%p|%p|GROUPREF_EXISTS %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[0]));
            /* <GROUPREF_EXISTS> <group> <skip> codeyes <JUMP> codeno ... */
            i = ctx->pattern[0];
            {
                ssize_t groupref = i+i;
                if (groupref >= state->lastmark) {
                    ctx->pattern += ctx->pattern[1];
                    break;
                } else {
                    SRE_CHAR* p = (SRE_CHAR*) state->mark[groupref];
                    SRE_CHAR* e = (SRE_CHAR*) state->mark[groupref+1];
                    if (!p || !e || e < p) {
                        ctx->pattern += ctx->pattern[1];
                        break;
                    }
                }
            }
            ctx->pattern += 2;
            break;

        case SRE_OP_ASSERT:
            /* assert subpattern */
            /* <ASSERT> <skip> <back> <pattern> */
            TRACE(("|%p|%p|ASSERT %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[1]));
            state->ptr = ctx->ptr - state->charsize * ctx->pattern[1];
            if (state->ptr < state->beginning)
                RETURN_FAILURE;
            DO_JUMP(JUMP_ASSERT, jump_assert, ctx->pattern+2);
            RETURN_ON_FAILURE(ret);
            ctx->pattern += ctx->pattern[0];
            break;

        case SRE_OP_ASSERT_NOT:
            /* assert not subpattern */
            /* <ASSERT_NOT> <skip> <back> <pattern> */
            TRACE(("|%p|%p|ASSERT_NOT %d\n", ctx->pattern,
                   ctx->ptr, ctx->pattern[1]));
            state->ptr = ctx->ptr - state->charsize * ctx->pattern[1];
            if (state->ptr >= state->beginning) {
                DO_JUMP(JUMP_ASSERT_NOT, jump_assert_not, ctx->pattern+2);
                if (ret) {
                    RETURN_ON_ERROR(ret);
                    RETURN_FAILURE;
                }
            }
            ctx->pattern += ctx->pattern[0];
            break;

        case SRE_OP_FAILURE:
            /* immediate failure */
            TRACE(("|%p|%p|FAILURE\n", ctx->pattern, ctx->ptr));
            RETURN_FAILURE;

        default:
            TRACE(("|%p|%p|UNKNOWN %d\n", ctx->pattern, ctx->ptr,
                   ctx->pattern[-1]));
            RETURN_ERROR(SRE_ERROR_ILLEGAL);
        }
    }

exit:
    ctx_pos = ctx->last_ctx_pos;
    jump = ctx->jump;
    DATA_POP_DISCARD(ctx);
    if (ctx_pos == -1)
        return ret;
    DATA_LOOKUP_AT(SRE_MATCH_CONTEXT, ctx, ctx_pos);

    switch (jump) {
        case JUMP_MAX_UNTIL_2:
            TRACE(("|%p|%p|JUMP_MAX_UNTIL_2\n", ctx->pattern, ctx->ptr));
            goto jump_max_until_2;
        case JUMP_MAX_UNTIL_3:
            TRACE(("|%p|%p|JUMP_MAX_UNTIL_3\n", ctx->pattern, ctx->ptr));
            goto jump_max_until_3;
        case JUMP_MIN_UNTIL_2:
            TRACE(("|%p|%p|JUMP_MIN_UNTIL_2\n", ctx->pattern, ctx->ptr));
            goto jump_min_until_2;
        case JUMP_MIN_UNTIL_3:
            TRACE(("|%p|%p|JUMP_MIN_UNTIL_3\n", ctx->pattern, ctx->ptr));
            goto jump_min_until_3;
        case JUMP_BRANCH:
            TRACE(("|%p|%p|JUMP_BRANCH\n", ctx->pattern, ctx->ptr));
            goto jump_branch;
        case JUMP_MAX_UNTIL_1:
            TRACE(("|%p|%p|JUMP_MAX_UNTIL_1\n", ctx->pattern, ctx->ptr));
            goto jump_max_until_1;
        case JUMP_MIN_UNTIL_1:
            TRACE(("|%p|%p|JUMP_MIN_UNTIL_1\n", ctx->pattern, ctx->ptr));
            goto jump_min_until_1;
        case JUMP_REPEAT:
            TRACE(("|%p|%p|JUMP_REPEAT\n", ctx->pattern, ctx->ptr));
            goto jump_repeat;
        case JUMP_REPEAT_ONE_1:
            TRACE(("|%p|%p|JUMP_REPEAT_ONE_1\n", ctx->pattern, ctx->ptr));
            goto jump_repeat_one_1;
        case JUMP_REPEAT_ONE_2:
            TRACE(("|%p|%p|JUMP_REPEAT_ONE_2\n", ctx->pattern, ctx->ptr));
            goto jump_repeat_one_2;
        case JUMP_MIN_REPEAT_ONE:
            TRACE(("|%p|%p|JUMP_MIN_REPEAT_ONE\n", ctx->pattern, ctx->ptr));
            goto jump_min_repeat_one;
        case JUMP_ASSERT:
            TRACE(("|%p|%p|JUMP_ASSERT\n", ctx->pattern, ctx->ptr));
            goto jump_assert;
        case JUMP_ASSERT_NOT:
            TRACE(("|%p|%p|JUMP_ASSERT_NOT\n", ctx->pattern, ctx->ptr));
            goto jump_assert_not;
        case JUMP_NONE:
            TRACE(("|%p|%p|RETURN %zd\n", ctx->pattern, ctx->ptr, ret));
            break;
    }

    return ret; /* should never get here */
}

LOCAL(ssize_t)
SRE_SEARCH(SRE_STATE* state, SRE_CODE* pattern)
{
    char* ptr = (char*)state->start;
    char* end = (char*)state->end;
    ssize_t status = 0;
    ssize_t prefix_len = 0;
    ssize_t prefix_skip = 0;
    SRE_CODE* prefix = NULL;
    SRE_CODE* charset = NULL;
    SRE_CODE* overlap = NULL;
    int flags = 0;

    if (pattern[0] == SRE_OP_INFO) {
        /* optimization info block */
        /* <INFO> <1=skip> <2=flags> <3=min> <4=max> <5=prefix info>  */

        flags = pattern[2];

        if (pattern[3] > 1) {
            /* adjust end point (but make sure we leave at least one
               character in there, so literal search will work) */
            end -= (pattern[3]-1) * state->charsize;
            if (end <= ptr)
                end = ptr + state->charsize;
        }

        if (flags & SRE_INFO_PREFIX) {
            /* pattern starts with a known prefix */
            /* <length> <skip> <prefix data> <overlap data> */
            prefix_len = pattern[5];
            prefix_skip = pattern[6];
            prefix = pattern + 7;
            overlap = prefix + prefix_len - 1;
        } else if (flags & SRE_INFO_CHARSET)
            /* pattern starts with a character from a known set */
            /* <charset> */
            charset = pattern + 5;

        pattern += 1 + pattern[1];
    }

    TRACE(("prefix = %p %zd %zd\n", prefix, prefix_len, prefix_skip));
    TRACE(("charset = %p\n", charset));

#if defined(USE_FAST_SEARCH)
    if (prefix_len > 1) {
        /* pattern starts with a known prefix.  use the overlap
           table to skip forward as fast as we possibly can */
        ssize_t i = 0;
        end = (char *)state->end;
        while (ptr < end) {
            for (;;) {
                if ((SRE_CODE) SRE_CHARGET(state, ptr, 0) != prefix[i]) {
                    if (!i)
                        break;
                    else
                        i = overlap[i];
                } else {
                    if (++i == prefix_len) {
                        /* found a potential match */
                        TRACE(("|%p|%p|SEARCH SCAN\n", pattern, ptr));
                        state->start = ptr - (prefix_len - 1) * state->charsize;
                        state->ptr = ptr - (prefix_len - prefix_skip - 1) * state->charsize;
                        if (flags & SRE_INFO_LITERAL)
                            return 1; /* we got all of it */
                        status = SRE_MATCH(state, pattern + 2*prefix_skip);
                        if (status != 0)
                            return status;
                        /* close but no cigar -- try again */
                        i = overlap[i];
                    }
                    break;
                }
            }
            ptr += state->charsize;
        }
        return 0;
    }
#endif

    if (pattern[0] == SRE_OP_LITERAL) {
        /* pattern starts with a literal character.  this is used
           for short prefixes, and if fast search is disabled */
        SRE_CODE chr = pattern[1];
        end = (char*)state->end;
        for (;;) {
            while (ptr < end && (SRE_CODE) SRE_CHARGET(state, ptr, 0) != chr)
                ptr += state->charsize;
            if (ptr >= end)
                return 0;
            TRACE(("|%p|%p|SEARCH LITERAL\n", pattern, ptr));
            state->start = ptr;
            ptr += state->charsize;
            state->ptr = ptr;
            if (flags & SRE_INFO_LITERAL)
                return 1; /* we got all of it */
            status = SRE_MATCH(state, pattern + 2);
            if (status != 0)
                break;
        }
    } else if (charset) {
        /* pattern starts with a character from a known set */
        end = (char*)state->end;
        for (;;) {
            while (ptr < end && !SRE_CHARSET(charset, SRE_CHARGET(state, ptr, 0)))
                ptr += state->charsize;
            if (ptr >= end)
                return 0;
            TRACE(("|%p|%p|SEARCH CHARSET\n", pattern, ptr));
            state->start = ptr;
            state->ptr = ptr;
            status = SRE_MATCH(state, pattern);
            if (status != 0)
                break;
            ptr += state->charsize;
        }
    } else
        /* general case */
        while (ptr <= end) {
            TRACE(("|%p|%p|SEARCH\n", pattern, ptr));
            state->start = state->ptr = ptr;
            ptr += state->charsize;
            status = SRE_MATCH(state, pattern);
            if (status != 0)
                break;
        }

    return status;
}

#if !defined(SRE_RECURSIVE)

/* -------------------------------------------------------------------- */
/* factories and destructors */

/* see sre.h for object declarations */
static sky_object_t
sky__sre_pattern_new_match(
                sky__sre_pattern_t  self,
                SRE_STATE *         state,
                int                 status);

static sky__sre_scanner_t
sky__sre_pattern_scanner(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             pos,
                ssize_t             endpos,
                sky_object_t        source);

static sky_object_t
sky__sre_parameter_fixup(
                sky_object_t    string,
                sky_object_t    string2,
                const char *    oldname)
{
    if (string2) {
        if (string) {
            sky_error_raise_format(
                    sky_TypeError,
                    "Argument given by name ('%s') and position (1)",
                    oldname);
        }
        sky_error_warn_format(
                sky_DeprecationWarning,
                "The '%s' keyword parameter name is deprecated.  "
                "Use 'string' instead.",
                oldname);
        return string2;
    }
    if (!string) {
        sky_error_raise_string(sky_TypeError,
                               "Required argument 'string' (pos 1) not found");
    }
    return string;
}

static size_t
sky__sre_getcodesize(void)
{
    return sizeof(SRE_CODE);
}


static int
sky__sre_getlower(int character, int flags)
{
    if (flags & SRE_FLAG_LOCALE) {
        return sre_lower_locale(character);
    }
    if (flags & SRE_FLAG_UNICODE) {
        return sre_lower_unicode(character);
    }
    return sre_lower(character);
}


LOCAL(void)
state_reset(SRE_STATE* state)
{
    /* FIXME: dynamic! */
    /*memset(state->mark, 0, sizeof(*state->mark) * SRE_MARK_SIZE);*/

    state->lastmark = -1;
    state->lastindex = -1;

    state->repeat = NULL;

    data_stack_dealloc(state);
}

static void *
getstring(sky_object_t          string,
          ssize_t *             p_length,
          int *                 p_logical_charsize,
          int *                 p_charsize,
          sky_buffer_t *        view,
          sky_string_data_t *   tagged_string_data)
{
    /* given a python object, return a data pointer, a length (in
       characters), and a character size.  return NULL if the object
       is not a string (or not compatible) */

    int                 charsize;
    void                *ptr;
    ssize_t             bytes, size;
    sky_string_data_t   *string_data;

    /* Unicode objects do not support the buffer API. So, get the data
       directly instead. */
    if (sky_object_isa(string, sky_string_type)) {
        string = sky_string_copy(string);
        string_data = sky_string_data(string, tagged_string_data);
        *p_length = string_data->len;
        *p_logical_charsize = 4;
        if (string_data->highest_codepoint < 0x100) {
            *p_charsize = 1;
            return string_data->data.latin1;
        }
        if (string_data->highest_codepoint < 0x10000) {
            *p_charsize = 2;
            return string_data->data.ucs2;
        }
        *p_charsize = 4;
        return string_data->data.ucs4;
    }

    if (!sky_buffer_check(string)) {
        sky_error_raise_string(sky_TypeError, "expected string or buffer");
    }
    sky_buffer_acquire(view, string, SKY_BUFFER_FLAG_SIMPLE);
    sky_asset_save(view,
                   (sky_free_t)sky_buffer_release,
                   SKY_ASSET_CLEANUP_ON_ERROR);

    /* determine buffer size */
    bytes = view->len;
    ptr = view->buf;

    if (bytes < 0) {
        sky_error_raise_string(sky_TypeError, "buffer has negative size");
    }

    /* determine character size */
    size = sky_object_len(string);

    if (sky_object_isa(string, sky_bytes_type) ||
        sky_object_isa(string, sky_bytearray_type) ||
        bytes == size)
    {
        charsize = 1;
    }
    else {
        sky_error_raise_string(sky_TypeError, "buffer size mismatch");
    }

    *p_length = size;
    *p_charsize = charsize;
    *p_logical_charsize = charsize;

    if (ptr == NULL) {
        sky_error_raise_string(sky_ValueError, "Buffer is NULL");
    }

    return ptr;
}

LOCAL(sky_object_t)
state_init(SRE_STATE* state, sky__sre_pattern_t pattern,
           sky_object_t string, ssize_t start, ssize_t end)
{
    /* prepare state object */

    sky__sre_pattern_data_t *pattern_data = sky__sre_pattern_data(pattern);

    ssize_t length;
    int logical_charsize, charsize;
    void* ptr;

    memset(state, 0, sizeof(SRE_STATE));

    state->lastmark = -1;
    state->lastindex = -1;

    SKY_ASSET_BLOCK_BEGIN {
        ptr = getstring(string, &length, &logical_charsize, &charsize,
                        &state->buffer, &state->tagged_string_data);

        if (logical_charsize == 1 && pattern_data->logical_charsize > 1) {
            sky_error_raise_string(
                    sky_TypeError,
                    "can't use a string pattern on a bytes-like object");
        }
        if (logical_charsize > 1 && pattern_data->logical_charsize == 1) {
            sky_error_raise_string(
                    sky_TypeError,
                    "can't use a bytes pattern on a string-like object");
        }

        /* adjust boundaries */
        if (start < 0)
            start = 0;
        else if (start > length)
            start = length;

        if (end < 0)
            end = 0;
        else if (end > length)
            end = length;

        state->logical_charsize = logical_charsize;
        state->charsize = charsize;

        state->beginning = ptr;

        state->start = (void*) ((char*) ptr + start * state->charsize);
        state->end = (void*) ((char*) ptr + end * state->charsize);

        sky_object_gc_set(&(state->string), string, pattern);

        state->pos = start;
        state->endpos = end;

        if (pattern_data->flags & SRE_FLAG_LOCALE)
            state->lower = sre_lower_locale;
        else if (pattern_data->flags & SRE_FLAG_UNICODE)
            state->lower = sre_lower_unicode;
        else
            state->lower = sre_lower;
    } SKY_ASSET_BLOCK_END;

    return string;
}


LOCAL(void)
state_fini(SRE_STATE* state)
{
    if (state->buffer.buf) {
        sky_buffer_release(&(state->buffer));
    }
    data_stack_dealloc(state);
}


/* calculate offset from start of string */
#define STATE_OFFSET(state, member)\
    (((char*)(member) - (char*)(state)->beginning) / (state)->charsize)

LOCAL(sky_object_t)
state_getslice(SRE_STATE* state, ssize_t index, sky_object_t string, int empty)
{
    ssize_t     i, j;
    sky_slice_t slice;

    index = (index - 1) * 2;
    if (sky_object_isnull(string) ||
        index >= state->lastmark ||
        !state->mark[index] ||
        !state->mark[index + 1])
    {
        if (empty) {
            /* want empty string */
            i = j = 0;
        }
        else {
            return sky_None;
        }
    } else {
        i = STATE_OFFSET(state, state->mark[index]);
        j = STATE_OFFSET(state, state->mark[index + 1]);
    }

    slice = sky_slice_create(sky_integer_create(i),
                             sky_integer_create(j),
                             sky_integer_one);
    return sky_object_getitem(string, slice);
}

static void
pattern_error(int status)
{
    switch (status) {
        case SRE_ERROR_RECURSION_LIMIT:
            sky_error_raise_string(sky_RuntimeError,
                                   "maximum recursion limit exceeded");
            break;
        case SRE_ERROR_MEMORY:
            sky_error_raise_string(sky_MemoryError, "out of memory");
            break;
        case SRE_ERROR_INTERRUPTED:
            /* An exception has already been raised, so let it fly */
            break;
        default:
            /* other error codes indicate compiler/engine bugs */
            sky_error_raise_string(
                    sky_RuntimeError,
                    "internal error in regular expression engine");
    }
}

static void
sky__sre_pattern_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__sre_pattern_data_t *self_data = data;

    if (self_data->view.buf) {
        sky_buffer_release(&(self_data->view));
    }
    sky_free(self_data->code);
}

static void
sky__sre_pattern_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__sre_pattern_data_t *self_data = data;

    sky_object_visit(self_data->groupindex, visit_data);
    sky_object_visit(self_data->indexgroup, visit_data);
    sky_object_visit(self_data->pattern, visit_data);
    sky_object_visit(self_data->view.object, visit_data);
}

static sky_object_t
sky__sre_pattern_match(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             pos,
                ssize_t             endpos,
                sky_object_t        pattern)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int             status;
    SRE_STATE       state;
    sky_object_t    match;

    string = sky__sre_parameter_fixup(string, pattern, "pattern");

    state_init(&state, self, string, pos, endpos);
    state.ptr = state.start;

    TRACE(("|%p|%p|MATCH\n", self_data->code, state.ptr));

    if (state.logical_charsize == 1) {
        status = sre_match(&state, self_data->code);
    } else {
        status = sre_umatch(&state, self_data->code);
    }

    TRACE(("|%p|%p|END\n", self_data->code, state.ptr));

    match = sky__sre_pattern_new_match(self, &state, status);
    state_fini(&state);

    return match;
}

const char sky__sre_pattern_match_doc[] =
"match(pattern[, pos[, endpos]]) -> match object or None.\n"
"    Matches zero or more characters at the beginning of the string";

static sky_object_t
sky__sre_pattern_search(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             pos,
                ssize_t             endpos,
                sky_object_t        pattern)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int             status;
    SRE_STATE       state;
    sky_object_t    match;

    string = sky__sre_parameter_fixup(string, pattern, "pattern");

    state_init(&state, self, string, pos, endpos);

    TRACE(("|%p|%p|SEARCH\n", self_data->code, state.ptr));

    if (state.logical_charsize == 1) {
        status = sre_search(&state, self_data->code);
    } else {
        status = sre_usearch(&state, self_data->code);
    }

    TRACE(("|%p|%p|END\n", self_data->code, state.ptr));

    match = sky__sre_pattern_new_match(self, &state, status);
    state_fini(&state);

    return match;
}

const char sky__sre_pattern_search_doc[] =
"search(pattern[, pos[, endpos]]) -> match object or None.\n"
"    Scan through string looking for a match, and return a corresponding\n"
"    match object instance. Return None if no position in the string matches.";

static sky_object_t
sky__sre_call(
                const char *    module_name,
                const char *    function_name,
                sky_object_t    args)
{
    sky_module_t    module;
    sky_string_t    name;

    name = sky_string_createfrombytes(module_name,
                                      strlen(module_name),
                                      NULL,
                                      NULL);
    module = sky_module_import(name);

    name = sky_string_createfrombytes(function_name,
                                      strlen(function_name),
                                      NULL,
                                      NULL);
    return sky_object_callmethod(module, name, args, NULL);
}

#ifdef USE_BUILTIN_COPY
static sky_object_t
sky__sre_deepcopy(sky_object_t copy, sky_object_t memo)
{
    return sky__sre_call("copy",
                         "deepcopy",
                         sky_tuple_pack(2, *object, memo));
}
#endif

static sky_list_t
sky__sre_pattern_findall(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             pos,
                ssize_t             endpos,
                sky_object_t        source)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int             status;
    ssize_t         b, e, i;
    SRE_STATE       state;
    sky_list_t      list;
    sky_slice_t     slice;
    sky_object_t    item, *items;

    string = sky__sre_parameter_fixup(string, source, "source");

    SKY_ASSET_BLOCK_BEGIN {
        state_init(&state, self, string, pos, endpos);
        sky_asset_save(&state,
                       (sky_free_t)state_fini,
                       SKY_ASSET_CLEANUP_ALWAYS);

        list = sky_list_create(NULL);
        while (state.start <= state.end) {
            state_reset(&state);
            state.ptr = state.start;

            if (state.logical_charsize == 1) {
                status = sre_search(&state, self_data->code);
            } else {
                status = sre_usearch(&state, self_data->code);
            }

            if (status <= 0) {
                if (status == 0) {
                    break;
                }
                pattern_error(status);
            }

            /* don't bother to build a match object */
            switch (self_data->groups) {
                case 0:
                    b = STATE_OFFSET(&state, state.start);
                    e = STATE_OFFSET(&state, state.ptr);
                    slice = sky_slice_create(sky_integer_create(b),
                                             sky_integer_create(e),
                                             sky_integer_one);
                    item = sky_object_getitem(string, slice);
                    break;
                case 1:
                    item = state_getslice(&state, 1, string, 1);
                    break;
                default:
                    SKY_ASSET_BLOCK_BEGIN {
                        items = sky_asset_malloc(self_data->groups *
                                                 sizeof(sky_object_t),
                                                 SKY_ASSET_CLEANUP_ON_ERROR);
                        for (i = 0; i < self_data->groups; ++i) {
                            items[i] = state_getslice(&state, i + 1, string, 1);
                        }
                        item = sky_tuple_createwitharray(self_data->groups,
                                                         items,
                                                         sky_free);
                    } SKY_ASSET_BLOCK_END;
                    break;
            }

            sky_list_append(list, item);

            if (state.ptr == state.start) {
                state.start = (void*) ((char*) state.ptr + state.charsize);
            }
            else {
                state.start = state.ptr;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

const char sky__sre_pattern_findall_doc[] =
"findall(string[, pos[, endpos]]) -> list.\n"
"   Return a list of all non-overlapping matches of pattern in string.";

static sky_object_t
sky__sre_pattern_finditer(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             pos,
                ssize_t             endpos)
{
    sky_object_t    scanner, search;

    scanner = sky__sre_pattern_scanner(self, string, pos, endpos, NULL);

    search = sky_object_getattr(scanner,
                                SKY_STRING_LITERAL("search"),
                                sky_NotSpecified);

    /* return a callable iterator (search, None) */
    return sky_callable_iterator_create(search, sky_None);
}

const char sky__sre_pattern_finditer_doc[] =
"finditer(string[, pos[, endpos]]) -> iterator.\n"
"    Return an iterator over all non-overlapping matches for the \n"
"    RE pattern in string. For each match, the iterator returns a\n"
"    match object.";

static sky_list_t
sky__sre_pattern_split(
                sky__sre_pattern_t  self,
                sky_object_t        string,
                ssize_t             maxsplit,
                sky_object_t        source)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int             status;
    void            *last;
    ssize_t         i, n;
    SRE_STATE       state;
    sky_list_t      list;
    sky_slice_t     slice;
    sky_object_t    item;

    string = sky__sre_parameter_fixup(string, source, "source");

    SKY_ASSET_BLOCK_BEGIN {
        state_init(&state, self, string, 0, SSIZE_MAX);
        sky_asset_save(&state,
                       (sky_free_t)state_fini,
                       SKY_ASSET_CLEANUP_ALWAYS);

        list = sky_list_create(NULL);

        n = 0;
        last = state.start;
        while (!maxsplit || n < maxsplit) {
            state_reset(&state);
            state.ptr = state.start;

            if (state.logical_charsize == 1) {
                status = sre_search(&state, self_data->code);
            } else {
                status = sre_usearch(&state, self_data->code);
            }

            if (status <= 0) {
                if (status == 0) {
                    break;
                }
                pattern_error(status);
            }

            if (state.start == state.ptr) {
                if (last == state.end) {
                    break;
                }
                /* skip one character */
                state.start = (void*) ((char*) state.ptr + state.charsize);
                continue;
            }

            /* get segment before this match */
            slice = sky_slice_create(
                            sky_integer_create(STATE_OFFSET(&state, last)),
                            sky_integer_create(STATE_OFFSET(&state, state.start)),
                            sky_integer_one);
            item = sky_object_getitem(string, slice);
            sky_list_append(list, item);

            /* add groups (if any) */
            for (i = 0; i < self_data->groups; i++) {
                item = state_getslice(&state, i + 1, string, 0);
                sky_list_append(list, item);
            }

            n = n + 1;
            last = state.start = state.ptr;
        }

        slice = sky_slice_create(
                        sky_integer_create(STATE_OFFSET(&state, last)),
                        sky_integer_create(state.endpos),
                        sky_integer_one);
        item = sky_object_getitem(string, slice);
        sky_list_append(list, item);
    } SKY_ASSET_BLOCK_END;

    return list;
}

const char sky__sre_pattern_split_doc[] =
"split(string[, maxsplit = 0])  -> list.\n"
"    Split string by the occurrences of pattern.";

static int
sky__sre_literal_template(int charsize, char* ptr, ssize_t len)
{
    /* check if given string is a literal template (i.e. no escapes) */
    struct {
        int charsize;
    } state = {
        charsize
    };
    while (len-- > 0) {
        if (SRE_CHARGET((&state), ptr, 0) == '\\')
            return 0;
        ptr += charsize;
    }
    return 1;
}

static sky_object_t
sky__sre_join_list(sky_list_t list, sky_object_t string)
{
    /* join list elements */

    sky_object_t    joiner;

    joiner = sky_object_getitem(string,
                                sky_slice_create(sky_integer_zero,
                                                 sky_integer_zero,
                                                 sky_integer_one));
    if (!sky_object_len(list)) {
        return joiner;
    }

    return sky_object_callmethod(joiner,
                                 SKY_STRING_LITERAL("join"),
                                 sky_tuple_pack(1, list),
                                 NULL);
}

static sky_object_t
sky__sre_pattern_subx(
                sky__sre_pattern_t  self,
                sky_object_t        template,
                sky_object_t        string,
                ssize_t             count,
                sky_bool_t          subn)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int                 charsize, literal, logical_charsize, status;
    void                *ptr;
    ssize_t             b, e, i, n;
    SRE_STATE           state;
    sky_bool_t          filter_is_callable;
    sky_list_t          list;
    sky_slice_t         slice;
    sky_buffer_t        view;
    sky_object_t        filter, item, match, result;
    sky_string_data_t   tagged_string_data;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_callable(template)) {
            /* sub/subn takes either a function or a template */
            filter = template;
            filter_is_callable = SKY_TRUE;
        }
        else {
            SKY_ERROR_TRY {
                ptr = getstring(template,
                                &n,
                                &logical_charsize,
                                &charsize,
                                &view,
                                &tagged_string_data);
                literal = sky__sre_literal_template(charsize, ptr, n);
            } SKY_ERROR_EXCEPT_ANY {
                literal = 0;
            } SKY_ERROR_TRY_END;

            if (literal) {
                filter = template;
                filter_is_callable = SKY_FALSE;
            }
            else {
                /* not a literal; hand it over to the template compiler */
                filter = sky__sre_call(SRE_PY_MODULE,
                                       "_subx",
                                       sky_tuple_pack(2, self, template));
                filter_is_callable = sky_object_callable(filter);
            }
        }

        state_init(&state, self, string, 0, SSIZE_MAX);
        sky_asset_save(&state,
                       (sky_free_t)state_fini,
                       SKY_ASSET_CLEANUP_ALWAYS);

        list = sky_list_create(NULL);
        n = i = 0;
        while (!count || n < count) {
            state_reset(&state);
            state.ptr = state.start;

            if (state.logical_charsize == 1) {
                status = sre_search(&state, self_data->code);
            } else {
                status = sre_usearch(&state, self_data->code);
            }
            if (status <= 0) {
                if (status == 0) {
                    break;
                }
                pattern_error(status);
            }

            b = STATE_OFFSET(&state, state.start);
            e = STATE_OFFSET(&state, state.ptr);

            if (i < b) {
                /* get segment before this match */
                slice = sky_slice_create(sky_integer_create(i),
                                         sky_integer_create(b),
                                         sky_integer_one);
                item = sky_object_getitem(string, slice);
                sky_list_append(list, item);
            } else if (i == b && i == e && n > 0) {
                /* ignore empty match on latest position */
                goto next;
            }

            if (filter_is_callable) {
                /* pass match object through filter */
                match = sky__sre_pattern_new_match(self, &state, 1);
                item = sky_object_call(filter, sky_tuple_pack(1, match), NULL);
            } else {
                /* filter is literal string */
                item = filter;
            }

            /* add to list */
            if (!sky_object_isnull(item)) {
                sky_list_append(list, item);
            }

            i = e;
            n = n + 1;

next:
            /* move on */
            if (state.ptr == state.start)
                state.start = (void*) ((char*) state.ptr + state.charsize);
            else
                state.start = state.ptr;
        }

        /* get segment following last match */
        if (i < state.endpos) {
            slice = sky_slice_create(sky_integer_create(i),
                                     sky_integer_create(state.endpos),
                                     sky_integer_one);
            item = sky_object_getitem(string, slice);
            sky_list_append(list, item);
        }
    } SKY_ASSET_BLOCK_END;

    /* convert list to single string (also removes list) */
    result = sky__sre_join_list(list, string);
    if (subn) {
        result = sky_object_build("(Oiz)", result, n);
    }
    return result;
}

static sky_object_t
sky__sre_pattern_sub(
                sky__sre_pattern_t  self,
                sky_object_t        repl,
                sky_object_t        string,
                ssize_t             count)
{
    return sky__sre_pattern_subx(self, repl, string, count, SKY_FALSE);
}

const char sky__sre_pattern_sub_doc[] =
"sub(repl, string[, count = 0]) -> newstring.\n"
"    Return the string obtained by replacing the leftmost non-overlapping\n"
"    occurrences of pattern in string by the replacement repl.";

static sky_object_t
sky__sre_pattern_subn(
                sky__sre_pattern_t  self,
                sky_object_t        repl,
                sky_object_t        string,
                ssize_t             count)
{
    return sky__sre_pattern_subx(self, repl, string, count, SKY_TRUE);
}

const char sky__sre_pattern_subn_doc[] =
"subn(repl, string[, count = 0]) -> (newstring, number of subs)\n"
"    Return the tuple (new_string, number_of_subs_made) found by replacing\n"
"    the leftmost non-overlapping occurrences of pattern with the\n"
"    replacement repl.";

static sky__sre_pattern_t
sky__sre_pattern_copy(SKY_UNUSED sky__sre_pattern_t self)
{
#ifdef USE_BUILTIN_COPY
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    sky__sre_pattern_t      copy;
    sky__sre_pattern_data_t *copy_data;

    copy = sky_object_allocate(sky__sre_pattern_type);
    copy_data = sky__sre_pattern_data(copy);
    copy_data->groups = self_data->groups;
    copy_data->flags = self_data->flags;
    copy_data->logical_charsize = self_data->logical_charsize;
    copy_data->charsize = self_data->charsize;
    copy_data->codesize = self_data->codesize;

    /* FIXME view is bad */
    /* FIXME tagged_string_data is bad */

    sky_object_gc_set(&(copy_data->groupindex),
                      &(self_data->groupindex),
                      copy);
    sky_object_gc_set(&(copy_data->indexgroup),
                      &(self_data->indexgroup),
                      copy);
    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->pattern)),
                      self_data->pattern,
                      copy);

    copy_data->code = sky_memdup(self_data->code,
                                 self_data->codesize * sizeof(SRE_CODE));

    return copy;


    Py_XINCREF(self->groupindex);
    Py_XINCREF(self->indexgroup);
    Py_XINCREF(self->pattern);
#else
    sky_error_raise_string(sky_TypeError, "cannot copy this pattern object");
#endif
}

static sky__sre_pattern_t
sky__sre_pattern_deepcopy(
    SKY_UNUSED  sky__sre_pattern_t  self,
    SKY_UNUSED  sky_object_t        memo)
{
#ifdef USE_BUILTIN_COPY
    sky__sre_pattern_t      copy;
    sky__sre_pattern_data_t *copy_data;

    copy = sky__sre_pattern_copy(self);
    copy_data = sky__sre_pattern_data(copy);

    sky_object_gc_set(&(copy_data->groupindex),
                      sky__sre_deepcopy(copy_data->groupindex, memo),
                      copy);
    sky_object_gc_set(&(copy_data->indexgroup),
                      sky__sre_deepcopy(copy_data->indexgroup, memo),
                      copy);
    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->pattern)),
                      sky__sre_deepcopy(copy_data->pattern, memo),
                      copy);
#else
    sky_error_raise_string(sky_TypeError, "cannot deepcopy this pattern object");
#endif
}

static void
sky__sre_pattern_validate(sky__sre_pattern_t self);

static sky_object_t
sky__sre_compile(
                sky_object_t    pattern,
                int             flags,
                sky_list_t      code,
                ssize_t         groups,
                sky_object_t    groupindex,
                sky_object_t    indexgroup)
{
    /* "compile" pattern descriptor to pattern object */

    sky__sre_pattern_t      new_pattern;
    sky__sre_pattern_data_t *new_pattern_data;

    ssize_t         i, n;
    sky_object_t    o;

    n = sky_list_len(code);

    new_pattern = sky_object_allocate(sky__sre_pattern_type);
    new_pattern_data = sky__sre_pattern_data(new_pattern);

    new_pattern_data->codesize = n;
    new_pattern_data->code = sky_malloc(n * sizeof(SRE_CODE));

    i = 0;
    SKY_SEQUENCE_FOREACH(code, o) {
        unsigned long value = sky_integer_value(o, 0, ULONG_MAX, NULL);
        new_pattern_data->code[i] = (SRE_CODE) value;
        if ((unsigned long) new_pattern_data->code[i] != value) {
            sky_error_raise_string(
                    sky_OverflowError,
                    "regular expression code size limit exceeded");
        }
        ++i;
    } SKY_SEQUENCE_FOREACH_END;

    if (sky_object_isnull(pattern)) {
        new_pattern_data->logical_charsize = -1;
        new_pattern_data->charsize = -1;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            ssize_t p_length;
            getstring(pattern,
                      &p_length,
                      &(new_pattern_data->logical_charsize),
                      &(new_pattern_data->charsize),
                      &(new_pattern_data->view),
                      &(new_pattern_data->tagged_string_data));
            if (new_pattern_data->view.object) {
                sky_object_gc_set(&(new_pattern_data->view.object),
                                  new_pattern_data->view.object,
                                  new_pattern);
            }
        } SKY_ASSET_BLOCK_END;
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(new_pattern_data->pattern)),
                      pattern,
                      new_pattern);
    sky_object_gc_set(&(new_pattern_data->groupindex), groupindex, new_pattern);
    sky_object_gc_set(&(new_pattern_data->indexgroup), indexgroup, new_pattern);

    new_pattern_data->flags = flags;

    new_pattern_data->groups = groups;

    sky__sre_pattern_validate(new_pattern);

    return new_pattern;
}

/* -------------------------------------------------------------------- */
/* Code validation */

/* To learn more about this code, have a look at the _compile() function in
   Lib/sre_compile.py.  The validation functions below checks the code array
   for conformance with the code patterns generated there.

   The nice thing about the generated code is that it is position-independent:
   all jumps are relative jumps forward.  Also, jumps don't cross each other:
   the target of a later jump is always earlier than the target of an earlier
   jump.  IOW, this is okay:

   J---------J-------T--------T
    \         \_____/        /
     \______________________/

   but this is not:

   J---------J-------T--------T
    \_________\_____/        /
               \____________/

   It also helps that SRE_CODE is always an unsigned type.
*/

/* Defining this one enables tracing of the validator */
#undef VVERBOSE

/* Trace macro for the validator */
#if defined(VVERBOSE)
#define VTRACE(v) printf v
#else
#define VTRACE(v) do {} while(0)  /* do nothing */
#endif

/* Report failure */
#define FAIL do { VTRACE(("FAIL: %d\n", __LINE__)); return 0; } while (0)

/* Extract opcode, argument, or skip count from code array */
#define GET_OP                                          \
    do {                                                \
        VTRACE(("%p: ", code));                         \
        if (code >= end) FAIL;                          \
        op = *code++;                                   \
        VTRACE(("%lu (op)\n", (unsigned long)op));      \
    } while (0)
#define GET_ARG                                         \
    do {                                                \
        VTRACE(("%p= ", code));                         \
        if (code >= end) FAIL;                          \
        arg = *code++;                                  \
        VTRACE(("%lu (arg)\n", (unsigned long)arg));    \
    } while (0)
#define GET_SKIP_ADJ(adj)                               \
    do {                                                \
        VTRACE(("%p= ", code));                         \
        if (code >= end) FAIL;                          \
        skip = *code;                                   \
        VTRACE(("%lu (skip to %p)\n",                   \
               (unsigned long)skip, code+skip));        \
        if (skip-adj > end-code)                        \
            FAIL;                                       \
        code++;                                         \
    } while (0)
#define GET_SKIP GET_SKIP_ADJ(0)

static int
_validate_charset(SRE_CODE *code, SRE_CODE *end)
{
    /* Some variables are manipulated by the macros above */
    SRE_CODE op;
    SRE_CODE arg;
    SRE_CODE offset;
    int i;

    while (code < end) {
        GET_OP;
        switch (op) {

        case SRE_OP_NEGATE:
            break;

        case SRE_OP_LITERAL:
            GET_ARG;
            break;

        case SRE_OP_RANGE:
            GET_ARG;
            GET_ARG;
            break;

        case SRE_OP_CHARSET:
            offset = 32/sizeof(SRE_CODE); /* 32-byte bitmap */
            if (offset > end-code)
                FAIL;
            code += offset;
            break;

        case SRE_OP_BIGCHARSET:
            GET_ARG; /* Number of blocks */
            offset = 256/sizeof(SRE_CODE); /* 256-byte table */
            if (offset > end-code)
                FAIL;
            /* Make sure that each byte points to a valid block */
            for (i = 0; i < 256; i++) {
                if (((unsigned char *)code)[i] >= arg)
                    FAIL;
            }
            code += offset;
            offset = arg * 32/sizeof(SRE_CODE); /* 32-byte bitmap times arg */
            if (offset > end-code)
                FAIL;
            code += offset;
            break;

        case SRE_OP_CATEGORY:
            GET_ARG;
            switch (arg) {
            case SRE_CATEGORY_DIGIT:
            case SRE_CATEGORY_NOT_DIGIT:
            case SRE_CATEGORY_SPACE:
            case SRE_CATEGORY_NOT_SPACE:
            case SRE_CATEGORY_WORD:
            case SRE_CATEGORY_NOT_WORD:
            case SRE_CATEGORY_LINEBREAK:
            case SRE_CATEGORY_NOT_LINEBREAK:
            case SRE_CATEGORY_LOC_WORD:
            case SRE_CATEGORY_LOC_NOT_WORD:
            case SRE_CATEGORY_UNI_DIGIT:
            case SRE_CATEGORY_UNI_NOT_DIGIT:
            case SRE_CATEGORY_UNI_SPACE:
            case SRE_CATEGORY_UNI_NOT_SPACE:
            case SRE_CATEGORY_UNI_WORD:
            case SRE_CATEGORY_UNI_NOT_WORD:
            case SRE_CATEGORY_UNI_LINEBREAK:
            case SRE_CATEGORY_UNI_NOT_LINEBREAK:
                break;
            default:
                FAIL;
            }
            break;

        default:
            FAIL;

        }
    }

    return 1;
}

static int
_validate_inner(SRE_CODE *code, SRE_CODE *end, ssize_t groups)
{
    /* Some variables are manipulated by the macros above */
    SRE_CODE op;
    SRE_CODE arg;
    SRE_CODE skip;

    VTRACE(("code=%p, end=%p\n", code, end));

    if (code > end)
        FAIL;

    while (code < end) {
        GET_OP;
        switch (op) {

        case SRE_OP_MARK:
            /* We don't check whether marks are properly nested; the
               sre_match() code is robust even if they don't, and the worst
               you can get is nonsensical match results. */
            GET_ARG;
            if (arg > 2*groups+1) {
                VTRACE(("arg=%d, groups=%d\n", (int)arg, (int)groups));
                FAIL;
            }
            break;

        case SRE_OP_LITERAL:
        case SRE_OP_NOT_LITERAL:
        case SRE_OP_LITERAL_IGNORE:
        case SRE_OP_NOT_LITERAL_IGNORE:
            GET_ARG;
            /* The arg is just a character, nothing to check */
            break;

        case SRE_OP_SUCCESS:
        case SRE_OP_FAILURE:
            /* Nothing to check; these normally end the matching process */
            break;

        case SRE_OP_AT:
            GET_ARG;
            switch (arg) {
            case SRE_AT_BEGINNING:
            case SRE_AT_BEGINNING_STRING:
            case SRE_AT_BEGINNING_LINE:
            case SRE_AT_END:
            case SRE_AT_END_LINE:
            case SRE_AT_END_STRING:
            case SRE_AT_BOUNDARY:
            case SRE_AT_NON_BOUNDARY:
            case SRE_AT_LOC_BOUNDARY:
            case SRE_AT_LOC_NON_BOUNDARY:
            case SRE_AT_UNI_BOUNDARY:
            case SRE_AT_UNI_NON_BOUNDARY:
                break;
            default:
                FAIL;
            }
            break;

        case SRE_OP_ANY:
        case SRE_OP_ANY_ALL:
            /* These have no operands */
            break;

        case SRE_OP_IN:
        case SRE_OP_IN_IGNORE:
            GET_SKIP;
            /* Stop 1 before the end; we check the FAILURE below */
            if (!_validate_charset(code, code+skip-2))
                FAIL;
            if (code[skip-2] != SRE_OP_FAILURE)
                FAIL;
            code += skip-1;
            break;

        case SRE_OP_INFO:
            {
                /* A minimal info field is
                   <INFO> <1=skip> <2=flags> <3=min> <4=max>;
                   If SRE_INFO_PREFIX or SRE_INFO_CHARSET is in the flags,
                   more follows. */
                SRE_CODE flags, i;
                SRE_CODE *newcode;
                GET_SKIP;
                newcode = code+skip-1;
                GET_ARG; flags = arg;
                GET_ARG;
                GET_ARG;
                /* Check that only valid flags are present */
                if ((flags & ~(SRE_INFO_PREFIX |
                               SRE_INFO_LITERAL |
                               SRE_INFO_CHARSET)) != 0)
                    FAIL;
                /* PREFIX and CHARSET are mutually exclusive */
                if ((flags & SRE_INFO_PREFIX) &&
                    (flags & SRE_INFO_CHARSET))
                    FAIL;
                /* LITERAL implies PREFIX */
                if ((flags & SRE_INFO_LITERAL) &&
                    !(flags & SRE_INFO_PREFIX))
                    FAIL;
                /* Validate the prefix */
                if (flags & SRE_INFO_PREFIX) {
                    SRE_CODE prefix_len;
                    GET_ARG; prefix_len = arg;
                    GET_ARG;
                    /* Here comes the prefix string */
                    if (prefix_len > newcode-code)
                        FAIL;
                    code += prefix_len;
                    /* And here comes the overlap table */
                    if (prefix_len > newcode-code)
                        FAIL;
                    /* Each overlap value should be < prefix_len */
                    for (i = 0; i < prefix_len; i++) {
                        if (code[i] >= prefix_len)
                            FAIL;
                    }
                    code += prefix_len;
                }
                /* Validate the charset */
                if (flags & SRE_INFO_CHARSET) {
                    if (!_validate_charset(code, newcode-1))
                        FAIL;
                    if (newcode[-1] != SRE_OP_FAILURE)
                        FAIL;
                    code = newcode;
                }
                else if (code != newcode) {
                  VTRACE(("code=%p, newcode=%p\n", code, newcode));
                    FAIL;
                }
            }
            break;

        case SRE_OP_BRANCH:
            {
                SRE_CODE *target = NULL;
                for (;;) {
                    GET_SKIP;
                    if (skip == 0)
                        break;
                    /* Stop 2 before the end; we check the JUMP below */
                    if (!_validate_inner(code, code+skip-3, groups))
                        FAIL;
                    code += skip-3;
                    /* Check that it ends with a JUMP, and that each JUMP
                       has the same target */
                    GET_OP;
                    if (op != SRE_OP_JUMP)
                        FAIL;
                    GET_SKIP;
                    if (target == NULL)
                        target = code+skip-1;
                    else if (code+skip-1 != target)
                        FAIL;
                }
            }
            break;

        case SRE_OP_REPEAT_ONE:
        case SRE_OP_MIN_REPEAT_ONE:
            {
                SRE_CODE min, max;
                GET_SKIP;
                GET_ARG; min = arg;
                GET_ARG; max = arg;
                if (min > max)
                    FAIL;
                if (max > SRE_MAXREPEAT)
                    FAIL;
                if (!_validate_inner(code, code+skip-4, groups))
                    FAIL;
                code += skip-4;
                GET_OP;
                if (op != SRE_OP_SUCCESS)
                    FAIL;
            }
            break;

        case SRE_OP_REPEAT:
            {
                SRE_CODE min, max;
                GET_SKIP;
                GET_ARG; min = arg;
                GET_ARG; max = arg;
                if (min > max)
                    FAIL;
                if (max > SRE_MAXREPEAT)
                    FAIL;
                if (!_validate_inner(code, code+skip-3, groups))
                    FAIL;
                code += skip-3;
                GET_OP;
                if (op != SRE_OP_MAX_UNTIL && op != SRE_OP_MIN_UNTIL)
                    FAIL;
            }
            break;

        case SRE_OP_GROUPREF:
        case SRE_OP_GROUPREF_IGNORE:
            GET_ARG;
            if (arg >= groups)
                FAIL;
            break;

        case SRE_OP_GROUPREF_EXISTS:
            /* The regex syntax for this is: '(?(group)then|else)', where
               'group' is either an integer group number or a group name,
               'then' and 'else' are sub-regexes, and 'else' is optional. */
            GET_ARG;
            if (arg >= groups)
                FAIL;
            GET_SKIP_ADJ(1);
            code--; /* The skip is relative to the first arg! */
            /* There are two possibilities here: if there is both a 'then'
               part and an 'else' part, the generated code looks like:

               GROUPREF_EXISTS
               <group>
               <skipyes>
               ...then part...
               JUMP
               <skipno>
               (<skipyes> jumps here)
               ...else part...
               (<skipno> jumps here)

               If there is only a 'then' part, it looks like:

               GROUPREF_EXISTS
               <group>
               <skip>
               ...then part...
               (<skip> jumps here)

               There is no direct way to decide which it is, and we don't want
               to allow arbitrary jumps anywhere in the code; so we just look
               for a JUMP opcode preceding our skip target.
            */
            if (skip >= 3 && skip-3 < end-code &&
                code[skip-3] == SRE_OP_JUMP)
            {
                VTRACE(("both then and else parts present\n"));
                if (!_validate_inner(code+1, code+skip-3, groups))
                    FAIL;
                code += skip-2; /* Position after JUMP, at <skipno> */
                GET_SKIP;
                if (!_validate_inner(code, code+skip-1, groups))
                    FAIL;
                code += skip-1;
            }
            else {
                VTRACE(("only a then part present\n"));
                if (!_validate_inner(code+1, code+skip-1, groups))
                    FAIL;
                code += skip-1;
            }
            break;

        case SRE_OP_ASSERT:
        case SRE_OP_ASSERT_NOT:
            GET_SKIP;
            GET_ARG; /* 0 for lookahead, width for lookbehind */
            code--; /* Back up over arg to simplify math below */
            if (arg & 0x80000000)
                FAIL; /* Width too large */
            /* Stop 1 before the end; we check the SUCCESS below */
            if (!_validate_inner(code+1, code+skip-2, groups))
                FAIL;
            code += skip-2;
            GET_OP;
            if (op != SRE_OP_SUCCESS)
                FAIL;
            break;

        default:
            FAIL;

        }
    }

    VTRACE(("okay\n"));
    return 1;
}

static int
_validate_outer(SRE_CODE *code, SRE_CODE *end, ssize_t groups)
{
    if (groups < 0 || groups > 100 || code >= end || end[-1] != SRE_OP_SUCCESS)
        FAIL;
    if (groups == 0)  /* fix for simplejson */
        groups = 100; /* 100 groups should always be safe */
    return _validate_inner(code, end-1, groups);
}

static void
sky__sre_pattern_validate(sky__sre_pattern_t self)
{
    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    if (!_validate_outer(self_data->code,
                         self_data->code + self_data->codesize,
                         self_data->groups))
    {
        sky_error_raise_string(sky_RuntimeError, "invalid SRE code");
    }

    VTRACE(("Success!\n"));
}

/* -------------------------------------------------------------------- */
/* match methods */

static void
sky__sre_match_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky__sre_match_data_t   *self_data = data;

    sky_free(self_data->mark);
}

static void
sky__sre_match_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__sre_match_data_t   *self_data = data;

    sky_object_visit(self_data->string, visit_data);
    sky_object_visit(self_data->regs, visit_data);
    sky_object_visit(self_data->pattern, visit_data);
}

static sky_object_t
sky__sre_match_getslice_by_index(
                sky__sre_match_t    self,
                ssize_t             index,
                sky_object_t        dfault)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    sky_slice_t slice;

    if (index < 0 || index >= self_data->groups) {
        /* raise IndexError if we were given a bad group number */
        sky_error_raise_string(sky_IndexError, "no such group");
    }

    index *= 2;

    if (sky_object_isnull(self_data->string) || self_data->mark[index] < 0) {
        /* return default value if the string or group is undefined */
        return dfault;
    }

    slice = sky_slice_create(sky_integer_create(self_data->mark[index]),
                             sky_integer_create(self_data->mark[index + 1]),
                             sky_integer_one);
    return sky_object_getitem(self_data->string, slice);
}

static ssize_t
sky__sre_match_getindex(sky__sre_match_t self, sky_object_t index)
{
    sky__sre_match_data_t   *self_data;
    sky__sre_pattern_data_t *pattern_data;

    if (sky_object_isnull(index)) {
        return 0;
    }

    if (sky_object_isa(index, sky_integer_type)) {
        return sky_integer_value(index, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    self_data = sky__sre_match_data(self);
    pattern_data = sky__sre_pattern_data(self_data->pattern);

    if (pattern_data->groupindex) {
        SKY_ERROR_TRY {
            index = sky_object_getitem(pattern_data->groupindex, index);
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;

        if (sky_object_isa(index, sky_integer_type)) {
            return sky_integer_value(index, SSIZE_MIN, SSIZE_MAX, NULL);
        }
    }

    return -1;
}

static sky_object_t
sky__sre_match_getslice(
                sky__sre_match_t    self,
                sky_object_t        index,
                sky_object_t        dfault)
{
    return sky__sre_match_getslice_by_index(
                    self,
                    sky__sre_match_getindex(self, index),
                    dfault);
}

static sky_object_t
sky__sre_match_expand(sky__sre_match_t self, sky_object_t template)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    /* delegate to Python code */
    return sky__sre_call(
        SRE_PY_MODULE,
        "_expand",
        sky_tuple_pack(3, self_data->pattern, self, template));
}

static const char sky__sre_match_expand_doc[] =
"expand(template) -> str.\n"
"    Return the string obtained by doing backslash substitution\n"
"    on the string template, as done by the sub() method.";

static sky_object_t
sky__sre_match_group(sky__sre_match_t self, sky_tuple_t args)
{
    ssize_t         i, size;
    sky_tuple_t     group;
    sky_object_t    *items;

    if (!(size = sky_tuple_len(args))) {
        return sky__sre_match_getslice(self, sky_False, sky_None);
    }
    if (1 == size) {
        return sky__sre_match_getslice(self, sky_tuple_get(args, 0), sky_None);
    }

    SKY_ASSET_BLOCK_BEGIN {
        items = sky_asset_malloc(size * sizeof(sky_object_t),
                                 SKY_ASSET_CLEANUP_ON_ERROR);
        for (i = 0; i < size; ++i) {
            items[i] = sky__sre_match_getslice(self,
                                               sky_tuple_get(args, i),
                                               sky_None);
        }
        group = sky_tuple_createwitharray(size, items, sky_free);
    } SKY_ASSET_BLOCK_END;

    return group;
}

static const char sky__sre_match_group_doc[] =
"group([group1, ...]) -> str or tuple.\n"
"    Return subgroup(s) of the match by indices or names.\n"
"    For 0 returns the entire match.";

static sky_tuple_t
sky__sre_match_groups(sky__sre_match_t self, sky_object_t dfault)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    ssize_t         index, size;
    sky_tuple_t     groups;
    sky_object_t    *items;

    SKY_ASSET_BLOCK_BEGIN {
        size = self_data->groups - 1;
        items = sky_asset_malloc(size * sizeof(sky_object_t),
                                 SKY_ASSET_CLEANUP_ON_ERROR);
        for (index = 1; index < self_data->groups; ++index) {
            items[index - 1] =
                    sky__sre_match_getslice_by_index(self, index, dfault);
        }
        groups = sky_tuple_createwitharray(size, items, sky_free);
    } SKY_ASSET_BLOCK_END;

    return groups;
}

static const char sky__sre_match_groups_doc[] =
"groups([default=None]) -> tuple.\n"
"    Return a tuple containing all the subgroups of the match, from 1.\n"
"    The default argument is used for groups\n"
"    that did not participate in the match";

static sky_dict_t
sky__sre_match_groupdict(sky__sre_match_t self, sky_object_t dfault)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);
    sky__sre_pattern_data_t *pattern_data = sky__sre_pattern_data(self_data->pattern);

    sky_dict_t      groupdict;
    sky_object_t    key, keys, value;

    groupdict = sky_dict_create();
    if (!pattern_data->groupindex) {
        return groupdict;
    }

    keys = sky_object_callmethod(pattern_data->groupindex,
                                 SKY_STRING_LITERAL("keys"),
                                 NULL,
                                 NULL);
    SKY_SEQUENCE_FOREACH(keys, key) {
        value = sky__sre_match_getslice(self, key, dfault);
        sky_dict_setitem(groupdict, key, value);
    } SKY_SEQUENCE_FOREACH_END;

    return groupdict;
}

static const char sky__sre_match_groupdict_doc[] =
"groupdict([default=None]) -> dict.\n"
"    Return a dictionary containing all the named subgroups of the match,\n"
"    keyed by the subgroup name. The default argument is used for groups\n"
"    that did not participate in the match";

static ssize_t
sky__sre_match_start(sky__sre_match_t self, sky_object_t group)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    ssize_t index;

    index = sky__sre_match_getindex(self, group);
    if (index < 0 || index >= self_data->groups) {
        sky_error_raise_string(sky_IndexError, "no such group");
    }

    /* mark is -1 if group is undefined */
    return self_data->mark[index * 2];
}

static const char sky__sre_match_start_doc[] =
"start([group=0]) -> int.\n"
"    Return index of the start of the substring matched by group.";

static ssize_t
sky__sre_match_end(sky__sre_match_t self, sky_object_t group)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    ssize_t index;

    index = sky__sre_match_getindex(self, group);
    if (index < 0 || index >= self_data->groups) {
        sky_error_raise_string(sky_IndexError, "no such group");
    }

    /* mark is -1 if group is undefined */
    return self_data->mark[index * 2 + 1];
}

static const char sky__sre_match_end_doc[] =
"end([group=0]) -> int.\n"
"    Return index of the end of the substring matched by group.";

LOCAL(sky_tuple_t)
_pair(ssize_t i1, ssize_t i2)
{
    return sky_object_build("(iziz)", i1, i2);
}

static sky_tuple_t
sky__sre_match_span(sky__sre_match_t self, sky_object_t group)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    ssize_t index;

    index = sky__sre_match_getindex(self, group);
    if (index < 0 || index >= self_data->groups) {
        sky_error_raise_string(sky_IndexError, "no such group");
    }

    /* marks are -1 if group is undefined */
    return _pair(self_data->mark[index * 2], self_data->mark[index * 2 + 1]);
}

static const char sky__sre_match_span_doc[] =
"span([group]) -> tuple.\n"
"    For MatchObject m, return the 2-tuple (m.start(group), m.end(group)).";

static sky_object_t
sky__sre_match_regs(sky__sre_match_t self)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    ssize_t         index;
    sky_tuple_t     regs;
    sky_object_t    *items;

    SKY_ASSET_BLOCK_BEGIN {
        items = sky_asset_malloc(sizeof(sky_object_t) * self_data->groups,
                                 SKY_ASSET_CLEANUP_ON_ERROR);
        for (index = 0; index < self_data->groups; ++index) {
            items[index] = _pair(self_data->mark[index * 2],
                                 self_data->mark[index * 2 + 1]);
        }
        regs = sky_tuple_createwitharray(self_data->groups, items, sky_free);
    } SKY_ASSET_BLOCK_END;

    return regs;
}

static sky__sre_match_t
sky__sre_match_copy(SKY_UNUSED sky__sre_match_t self)
{
#ifdef USE_BUILTIN_COPY
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);
    sky__sre_pattern_data_t *pattern_data =
                            sky__sre_pattern_data(self_data->pattern);

    ssize_t                 slots;
    sky__sre_match_t        new_match;
    sky__sre_match_data_t   *new_match_data;

    slots = 2 * (pattern_data->groups + 1);

    new_match = sky_object_allocate(sky__sre_match_type);
    new_match_data = sky__sre_match_data(new_match);
    sky_object_gc_set(&(new_match_data->string), self_data->string, new_match);
    sky_object_gc_set(&(new_match_data->regs), self_data->regs, new_match);
    sky_object_gc_set(SKY_AS_OBJECTP(&(new_match_data->pattern)),
                      self_data->pattern,
                      new_match);

    new_match_data->pos = match_data->pos;
    new_match_data->endpos = match_data->endpos;
    new_match_data->lastindex = match_data->lastindex;
    new_match_data->groups = match_data->groups;
    new_match_data->mark = sky_memdup(match_data->mark, slots);

    return new_match;
#else
    sky_error_raise_string(sky_TypeError, "cannot copy this match object");
#endif
}

static sky__sre_match_t
sky__sre_match_deepcopy(
    SKY_UNUSED  sky__sre_match_t    self,
    SKY_UNUSED  sky_object_t        memo)
{
#ifdef USE_BUILTIN_COPY
    sky__sre_match_t        copy;
    sky__sre_match_data_t   *copy_data;

    copy = sky__sre_match_copy(self);
    copy_data = sky__sre_match_data(copy);

    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->pattern)),
                      sky__sre_deepcopy(copy_data->pattern, memo),
                      copy);
    sky_object_gc_set(copy_data->string,
                      sky__sre_copy(copy_data->string, memo),
                      copy);
    sky_object_gc_set(copy_data->string,
                      sky__sre_copy(copy_data->regs, memo),
                      copy);
#else
    sky_error_raise_string(sky_TypeError, "cannot deepcopy this match object");
#endif
}


static sky_object_t
sky__sre_match_lastindex_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    if (self_data->lastindex >= 0)
        return sky_integer_create(self_data->lastindex);
    return sky_None;
}

static sky_object_t
sky__sre_match_lastgroup_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);
    sky__sre_pattern_data_t *pattern_data =
                            sky__sre_pattern_data(self_data->pattern);

    sky_object_t    result = sky_None;

    if (pattern_data->indexgroup && self_data->lastindex >= 0) {
        SKY_ERROR_TRY {
            result = sky_sequence_get(pattern_data->indexgroup,
                                      self_data->lastindex);
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;
    }

    return result;
}

static sky_object_t
sky__sre_match_regs_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky__sre_match_data_t   *self_data = sky__sre_match_data(self);

    if (self_data->regs) {
        return self_data->regs;
    }
    return sky__sre_match_regs(self);
}


static sky_object_t
sky__sre_pattern_new_match(
                sky__sre_pattern_t  self,
                SRE_STATE *         state,
                int                 status)
{
    /* create match object (from state object) */

    sky__sre_pattern_data_t *self_data = sky__sre_pattern_data(self);

    int                     n;
    char                    *base;
    ssize_t                 i, j;
    sky__sre_match_t        match;
    sky__sre_match_data_t   *match_data;

    if (!status) {
        return sky_None;
    }
    if (status < 0) {
        /* internal error */
    }

    match = sky_object_allocate(sky__sre_match_type);
    match_data = sky__sre_match_data(match);
    sky_object_gc_set(SKY_AS_OBJECTP(&(match_data->pattern)), self, match);
    sky_object_gc_set(&(match_data->string), state->string, match);

    match_data->groups = self_data->groups + 1;
    match_data->mark = sky_malloc(2 * (self_data->groups + 1) * sizeof(ssize_t));

    /* fill in group slices */

    base = (char *)state->beginning;
    n = state->charsize;

    match_data->mark[0] = ((char *)state->start - base) / n;
    match_data->mark[1] = ((char *)state->ptr - base) / n;

    for (i = j = 0; i < self_data->groups; i++, j += 2) {
        if (j + 1 <= state->lastmark && state->mark[j] && state->mark[j + 1]) {
            match_data->mark[j + 2] = ((char *)state->mark[j] - base) / n;
            match_data->mark[j + 3] = ((char *)state->mark[j + 1] - base) / n;
        } else {
            /* undefined */
            match_data->mark[j + 2] = match_data->mark[j + 3] = -1;
        }
    }

    match_data->pos = state->pos;
    match_data->endpos = state->endpos;

    match_data->lastindex = state->lastindex;

    return match;
}


/* -------------------------------------------------------------------- */
/* scanner methods (experimental) */


static void
sky__sre_scanner_instance_finalize(SKY_UNUSED sky_object_t  self, void *data)
{
    sky__sre_scanner_data_t *self_data = data;

    state_fini(&(self_data->state));
}

static void
sky__sre_scanner_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__sre_scanner_data_t *self_data = data;

    sky_object_visit(self_data->pattern, visit_data);
}


static sky_object_t
sky__sre_scanner_match(sky__sre_scanner_t self)
{
    sky__sre_scanner_data_t *self_data = sky__sre_scanner_data(self);

    int                     status;
    SRE_STATE               *state;
    sky_object_t            match;
    sky__sre_pattern_data_t *pattern_data;

    state = &(self_data->state);
    pattern_data = sky__sre_pattern_data(self_data->pattern);

    state_reset(state);

    state->ptr = state->start;

    if (state->logical_charsize == 1) {
        status = sre_match(state, pattern_data->code);
    } else {
        status = sre_umatch(state, pattern_data->code);
    }

    match = sky__sre_pattern_new_match(self_data->pattern, state, status);

    if (status == 0 || state->ptr == state->start)
        state->start = (void*) ((char*) state->ptr + state->charsize);
    else
        state->start = state->ptr;

    return match;
}


static sky_object_t
sky__sre_scanner_search(sky__sre_scanner_t self)
{
    sky__sre_scanner_data_t *self_data = sky__sre_scanner_data(self);

    int                     status;
    SRE_STATE               *state;
    sky_object_t            match;
    sky__sre_pattern_data_t *pattern_data;

    state = &(self_data->state);
    pattern_data = sky__sre_pattern_data(self_data->pattern);

    state_reset(state);

    state->ptr = state->start;

    if (state->logical_charsize == 1) {
        status = sre_search(state, pattern_data->code);
    } else {
        status = sre_usearch(state, pattern_data->code);
    }

    match = sky__sre_pattern_new_match(self_data->pattern, state, status);

    if (status == 0 || state->ptr == state->start)
        state->start = (void*) ((char*) state->ptr + state->charsize);
    else
        state->start = state->ptr;

    return match;
}


static sky__sre_scanner_t
sky__sre_pattern_scanner(sky__sre_pattern_t self,
                         sky_object_t       string,
                         ssize_t            pos,
                         ssize_t            endpos,
                         sky_object_t       source)
{
    sky__sre_scanner_t      scanner;
    sky__sre_scanner_data_t *scanner_data;

    string = sky__sre_parameter_fixup(string, source, "source");

    scanner = sky_object_allocate(sky__sre_scanner_type);
    scanner_data = sky__sre_scanner_data(scanner);

    state_init(&(scanner_data->state), self, string, pos, endpos);
    sky_object_gc_set(SKY_AS_OBJECTP(&(scanner_data->pattern)), self, scanner);

    return scanner;
}


sky_type_t sky__sre_match_type = NULL;
sky_type_t sky__sre_pattern_type = NULL;
sky_type_t sky__sre_scanner_type = NULL;


void
skython_module__sre_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__sre_pattern_instance_finalize;
    template.visit = sky__sre_pattern_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SRE_Pattern"),
                    "Compiled regular expression objects",
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky__sre_pattern_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmembers(type,
            "pattern",      NULL,
                            offsetof(sky__sre_pattern_data_t, pattern),
                            SKY_DATA_TYPE_OBJECT,
                            SKY_MEMBER_FLAG_READONLY,
            "flags",        NULL,
                            offsetof(sky__sre_pattern_data_t, flags),
                            SKY_DATA_TYPE_INT,
                            SKY_MEMBER_FLAG_READONLY,
            "groups",       NULL,
                            offsetof(sky__sre_pattern_data_t, groups),
                            SKY_DATA_TYPE_SSIZE_T,
                            SKY_MEMBER_FLAG_READONLY,
            "groupindex",   NULL,
                            offsetof(sky__sre_pattern_data_t, groupindex),
                            SKY_DATA_TYPE_OBJECT,
                            SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(
            type,
            "__copy__",
            sky_function_createbuiltin(
                    "SRE_Pattern.__copy__",
                    NULL,
                    (sky_native_code_function_t)sky__sre_pattern_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "__deepcopy__",
            sky_function_createbuiltin(
                    "SRE_Pattern.__deepcopy__",
                    NULL,
                    (sky_native_code_function_t)sky__sre_pattern_deepcopy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "memo", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "findall",
            sky_function_createbuiltin(
                    "SRE_Pattern.findall",
                    sky__sre_pattern_findall_doc,
                    (sky_native_code_function_t)sky__sre_pattern_findall,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "pos", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "endpos", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    "source", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "finditer",
            sky_function_createbuiltin(
                    "SRE_Pattern.finditer",
                    sky__sre_pattern_finditer_doc,
                    (sky_native_code_function_t)sky__sre_pattern_finditer,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "pos", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "endpos", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "match",
            sky_function_createbuiltin(
                    "SRE_Pattern.match",
                    sky__sre_pattern_match_doc,
                    (sky_native_code_function_t)sky__sre_pattern_match,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "pos", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "endpos", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "pattern", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "scanner",
            sky_function_createbuiltin(
                    "SRE_Pattern.scanner",
                    NULL,
                    (sky_native_code_function_t)sky__sre_pattern_scanner,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "pos", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "endpos", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    "source", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "search",
            sky_function_createbuiltin(
                    "SRE_Pattern.search",
                    sky__sre_pattern_search_doc,
                    (sky_native_code_function_t)sky__sre_pattern_search,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "pos", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "endpos", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    "pattern", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "split",
            sky_function_createbuiltin(
                    "SRE_Pattern.split",
                    sky__sre_pattern_split_doc,
                    (sky_native_code_function_t)sky__sre_pattern_split,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "maxsplit", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "source", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "sub",
            sky_function_createbuiltin(
                    "SRE_Pattern.sub",
                    sky__sre_pattern_sub_doc,
                    (sky_native_code_function_t)sky__sre_pattern_sub,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "repl", SKY_DATA_TYPE_OBJECT, NULL,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "count", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "subn",
            sky_function_createbuiltin(
                    "SRE_Pattern.subn",
                    sky__sre_pattern_subn_doc,
                    (sky_native_code_function_t)sky__sre_pattern_subn,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "repl", SKY_DATA_TYPE_OBJECT, NULL,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "count", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));

    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__sre_pattern_type),
                                 type,
                                 SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__sre_match_instance_finalize;
    template.visit = sky__sre_match_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SRE_Match"),
                    "The result of re.match() and re.search().\n"
                    "Match objects always have a boolean value of True.",
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky__sre_match_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmembers(type,
            "string",   NULL,
                        offsetof(sky__sre_match_data_t, string),
                        SKY_DATA_TYPE_OBJECT,
                        SKY_MEMBER_FLAG_READONLY,
            "re",       NULL,
                        offsetof(sky__sre_match_data_t, pattern),
                        SKY_DATA_TYPE_OBJECT,
                        SKY_MEMBER_FLAG_READONLY,
            "pos",      NULL,
                        offsetof(sky__sre_match_data_t, pos),
                        SKY_DATA_TYPE_SSIZE_T,
                        SKY_MEMBER_FLAG_READONLY,
            "endpos",   NULL,
                        offsetof(sky__sre_match_data_t, endpos),
                        SKY_DATA_TYPE_SSIZE_T,
                        SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setgetsets(type,
            "lastindex",    NULL, sky__sre_match_lastindex_getter, NULL,
            "lastgroup",    NULL, sky__sre_match_lastgroup_getter, NULL,
            "regs",         NULL, sky__sre_match_regs_getter, NULL,
            NULL);

    sky_type_setattr_builtin(
            type,
            "__copy__",
            sky_function_createbuiltin(
                    "SRE_Match.__copy__",
                    NULL,
                    (sky_native_code_function_t)sky__sre_match_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "__deepcopy__",
            sky_function_createbuiltin(
                    "SRE_Match.__deepcopy__",
                    NULL,
                    (sky_native_code_function_t)sky__sre_match_deepcopy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "memo", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "end",
            sky_function_createbuiltin(
                    "SRE_Match.end",
                    sky__sre_match_end_doc,
                    (sky_native_code_function_t)sky__sre_match_end,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "group", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "expand",
            sky_function_createbuiltin(
                    "SRE_Match.expand",
                    sky__sre_match_expand_doc,
                    (sky_native_code_function_t)sky__sre_match_expand,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "template", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "group",
            sky_function_createbuiltin(
                    "SRE_Match.group",
                    sky__sre_match_group_doc,
                    (sky_native_code_function_t)sky__sre_match_group,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "groupdict",
            sky_function_createbuiltin(
                    "SRE_Match.groupdict",
                    sky__sre_match_groupdict_doc,
                    (sky_native_code_function_t)sky__sre_match_groupdict,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "default", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "groups",
            sky_function_createbuiltin(
                    "SRE_Match.groups",
                    sky__sre_match_groups_doc,
                    (sky_native_code_function_t)sky__sre_match_groups,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "default", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "span",
            sky_function_createbuiltin(
                    "SRE_Match.span",
                    sky__sre_match_span_doc,
                    (sky_native_code_function_t)sky__sre_match_span,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "group", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "start",
            sky_function_createbuiltin(
                    "SRE_Match.start",
                    sky__sre_match_start_doc,
                    (sky_native_code_function_t)sky__sre_match_start,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "group", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));

    /* FIXME: implement setattr("string", None) as a special case (to
       detach the associated string, if any */

    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__sre_match_type),
                                 type,
                                 SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky__sre_scanner_instance_finalize;
    template.visit = sky__sre_scanner_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("SRE_Scanner"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky__sre_scanner_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setattr_member(type,
                            "pattern",
                            NULL,
                            offsetof(sky__sre_scanner_data_t, pattern),
                            SKY_DATA_TYPE_OBJECT,
                            SKY_MEMBER_FLAG_READONLY);

    sky_type_setattr_builtin(
            type,
            "match",
            sky_function_createbuiltin(
                    "SRE_Scanner.match",
                    NULL,
                    (sky_native_code_function_t)sky__sre_scanner_match,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "search",
            sky_function_createbuiltin(
                    "SRE_Scanner.search",
                    NULL,
                    (sky_native_code_function_t)sky__sre_scanner_search,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__sre_scanner_type),
                                 type,
                                 SKY_TRUE);


    sky_module_setattr(
            module,
            "compile",
            sky_function_createbuiltin(
                    "compile",
                    NULL,
                    (sky_native_code_function_t)sky__sre_compile,
                    SKY_DATA_TYPE_OBJECT,
                    "pattern", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, NULL,
                    "code", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "groups", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "groupindex", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "indexgroup", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "getcodesize",
            sky_function_createbuiltin(
                    "getcodesize",
                    NULL,
                    (sky_native_code_function_t)sky__sre_getcodesize,
                    SKY_DATA_TYPE_SIZE_T,
                    NULL));
    sky_module_setattr(
            module,
            "getlower",
            sky_function_createbuiltin(
                    "getlower",
                    NULL,
                    (sky_native_code_function_t)sky__sre_getlower,
                    SKY_DATA_TYPE_INT,
                    "character", SKY_DATA_TYPE_INT, NULL,
                    "flags", SKY_DATA_TYPE_INT, NULL,
                    NULL));

    sky_module_setattr(module,
                       "CODESIZE",
                       sky_integer_createfromunsigned(sizeof(SRE_CODE)));
    sky_module_setattr(module,
                       "MAGIC",
                       sky_integer_createfromunsigned(SRE_MAGIC));
    sky_module_setattr(module,
                       "MAXREPEAT",
                       sky_integer_createfromunsigned(SRE_MAXREPEAT));

    sky_module_setattr(
            module,
            "copyright",
            sky_string_createfrombytes(sky__sre_copyright,
                                       strlen(sky__sre_copyright),
                                       NULL,
                                       NULL));
}


#endif /* !defined(SRE_RECURSIVE) */

/* vim:ts=4:sw=4:et
*/
