#include "../core/skython.h"

#include "sha.h"


SKY_MODULE_EXTERN void
skython_module__sha1_initialize(sky_module_t module);


static sky_type_t sky__sha1_sha1_type;

SKY_EXTERN_INLINE SHA_CTX *
sky__sha1_sha1_data(sky_object_t object)
{
    return sky_object_data(object, sky__sha1_sha1_type);
}

static void
sky__sha1_sha1_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    SHA_CTX *self_data = data;

    SHA1_Init(self_data);
}


static sky_object_t
sky__sha1_sha1_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA1_BLOCK_LENGTH);
}

static sky_object_t
sky__sha1_sha1_copy(sky_object_t self)
{
    sky_object_t    new_sha1;

    new_sha1 = sky_object_allocate(sky__sha1_sha1_type);
    memcpy(sky__sha1_sha1_data(new_sha1),
           sky__sha1_sha1_data(self),
           sizeof(SHA_CTX));

    return new_sha1;
}

static sky_bytes_t
sky__sha1_sha1_digest(sky_object_t self)
{
    SHA_CTX *self_data = sky__sha1_sha1_data(self);

    SHA_CTX     final_data;
    uint8_t     *raw_bytes;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(SHA_CTX));

        raw_bytes = sky_asset_malloc(SHA1_DIGEST_LENGTH,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        SHA1_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SHA1_DIGEST_LENGTH,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_object_t
sky__sha1_sha1_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA1_DIGEST_LENGTH);
}

static sky_string_t
sky__sha1_sha1_hexdigest(sky_object_t self)
{
    SHA_CTX *self_data = sky__sha1_sha1_data(self);

    char        hexdigest[SHA1_DIGEST_LENGTH * 2], *p;
    size_t      i;
    SHA_CTX     final_data;
    uint8_t     digest[SHA1_DIGEST_LENGTH];
    const char  *hexdigits;

    memcpy(&final_data, self_data, sizeof(SHA_CTX));
    SHA1_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_object_t
sky__sha1_sha1_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("SHA1");
}

static void
sky__sha1_sha1_update(sky_object_t self, sky_object_t string)
{
    SHA_CTX *self_data = sky__sha1_sha1_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        SHA1_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t
sky__sha1_sha1(sky_object_t string)
{
    sky_object_t    sha1;

    sha1 = sky_object_allocate(sky__sha1_sha1_type);
    if (!sky_object_isnull(string)) {
        sky__sha1_sha1_update(sha1, string);
    }

    return sha1;
}


void
skython_module__sha1_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__sha1_sha1_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("sha1"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(SHA_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__sha1_sha1_block_size_getter, NULL,
            "digest_size",  NULL, sky__sha1_sha1_digest_size_getter, NULL,
            "name",         NULL, sky__sha1_sha1_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "sha1.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__sha1_sha1_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "sha1.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__sha1_sha1_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "sha1.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__sha1_sha1_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "sha1.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__sha1_sha1_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__sha1_sha1_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "sha1",
            sky_function_createbuiltin(
                    "sha1",
                    "Return a new SHA1 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__sha1_sha1,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
}
