#include "../core/sky_private.h"

#include "module__io.h"


static sky_bool_t
sky__io__IOBase_isclosed(sky_object_t self)
{
    return sky_object_bool(sky_object_getattr(self,
                                              SKY_STRING_LITERAL("closed"),
                                              sky_NotSpecified));
}


void
sky__io__IOBase__checkClosed(sky_object_t self)
{
    if (sky__io__IOBase_isclosed(self)) {
        sky_error_raise_string(sky_ValueError,
                               "I/O operation on closed file.");
    }
}


void
sky__io__IOBase__checkReadable(sky_object_t self)
{
    sky_object_t    readable;

    readable = sky_object_callmethod(self,
                                     SKY_STRING_LITERAL("readable"),
                                     NULL,
                                     NULL);
    if (!sky_object_bool(readable)) {
        sky_error_raise_string(sky_UnsupportedOperation,
                               "File or stream is not readable.");
    }
}


void
sky__io__IOBase__checkSeekable(sky_object_t self)
{
    sky_object_t    seekable;

    seekable = sky_object_callmethod(self,
                                     SKY_STRING_LITERAL("seekable"),
                                     NULL,
                                     NULL);
    if (!sky_object_bool(seekable)) {
        sky_error_raise_string(sky_UnsupportedOperation,
                               "File or stream is not seekable.");
    }
}


void
sky__io__IOBase__checkWritable(sky_object_t self)
{
    sky_object_t    writable;

    writable = sky_object_callmethod(self,
                                     SKY_STRING_LITERAL("writable"),
                                     NULL,
                                     NULL);
    if (!sky_object_bool(writable)) {
        sky_error_raise_string(sky_UnsupportedOperation,
                               "File or stream is not writable.");
    }
}


static sky_bool_t
sky__io__IOBase_closed(sky_object_t self)
{
    return sky_object_hasattr(self, SKY_STRING_LITERAL("__IOBase_closed"));
}


static sky_object_t
sky__io__IOBase_closed_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return (sky__io__IOBase_closed(self) ? sky_True : sky_False);
}


static const char sky__io__IOBase_close_doc[] =
"Flush and close the IO object.\n"
"\n"
"This method has no effect if the file is already closed.\n";

static void
sky__io__IOBase_close(sky_object_t self)
{
    if (sky__io__IOBase_closed(self)) {
        return;
    }

    sky_object_callmethod(self, SKY_STRING_LITERAL("flush"), NULL, NULL);
    sky_object_setattr(self, SKY_STRING_LITERAL("__IOBase_closed"), sky_True);
}


static void
sky__io__IOBase_del(sky_object_t self)
{
    sky_bool_t  closed;

    SKY_ERROR_TRY {
        closed = sky__io__IOBase_isclosed(self);
    } SKY_ERROR_EXCEPT_ANY {
        closed = SKY_TRUE;
    } SKY_ERROR_TRY_END;

    if (!closed) {
        SKY_ERROR_TRY {
            sky_object_callmethod(self,
                                  SKY_STRING_LITERAL("close"),
                                  NULL,
                                  NULL);
        } SKY_ERROR_EXCEPT_ANY {
            /* silence errors */
        } SKY_ERROR_TRY_END;
    }
}


static sky_object_t
sky__io__IOBase_enter(sky_object_t self)
{
    sky__io__IOBase__checkClosed(self);
    return self;
}


static sky_bool_t
sky__io__IOBase_exit(
                sky_object_t    self,
    SKY_UNUSED  sky_type_t      exc_type,
    SKY_UNUSED  sky_object_t    exc_value,
    SKY_UNUSED  sky_object_t    traceback)
{
    sky_object_callmethod(self, SKY_STRING_LITERAL("close"), NULL, NULL);
    return SKY_FALSE;
}


static const char sky__io__IOBase_fileno_doc[] =
"Returns underlying file descriptor if one exists.\n"
"\n"
"An IOError is raised if the IO object does not use a file descriptor.\n";

static int
sky__io__IOBase_fileno(SKY_UNUSED sky_object_t self)
{
    sky_error_raise_string(sky_UnsupportedOperation, "fileno");
}


static const char sky__io__IOBase_flush_doc[] =
"Flush write buffers, if applicable.\n"
"\n"
"This is not implemented for read-only and non-blocking streams.\n";

static void
sky__io__IOBase_flush(sky_object_t self)
{
    if (sky__io__IOBase_closed(self)) {
        sky_error_raise_string(sky_ValueError,
                               "I/O operation on closed file.\n");
    }
}


static const char sky__io__IOBase_isatty_doc[] =
"Return whether this is an 'interactive' stream.\n"
"\n"
"Return False if it can't be determined.\n";

static sky_bool_t
sky__io__IOBase_isatty(sky_object_t self)
{
    sky__io__IOBase__checkClosed(self);
    return SKY_FALSE;
}


static sky_object_t
sky__io__IOBase_iter(sky_object_t self)
{
    sky__io__IOBase__checkClosed(self);
    return self;
}


static sky_object_t
sky__io__IOBase_next(sky_object_t self)
{
    sky_object_t    line;

    line = sky_object_callmethod(self,
                                 SKY_STRING_LITERAL("readline"),
                                 NULL,
                                 NULL);
    if (!sky_object_bool(line)) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    return line;
}


static const char sky__io__IOBase_readable_doc[] =
"Return whether object was opened for reading.\n"
"\n"
"If False, read() will raise UnsupportedOperation.";

static sky_bool_t
sky__io__IOBase_readable(SKY_UNUSED sky_object_t self)
{
    return SKY_FALSE;
}


static const char sky__io__IOBase_readline_doc[] =
"Read and return a line from the stream.\n"
"\n"
"If limit is specified, at most limit bytes will be read.\n"
"\n"
"The line terminator is always b'\\n' for binary files; for text\n"
"files, the newlines argument to open can be used to select the line\n"
"terminator(s) recognized.\n";

static sky_bytes_t
sky__io__IOBase_readline(sky_object_t self, ssize_t limit)
{
    ssize_t                 data_len, n;
    const char              *c, *end;
    sky_bytes_t             data, line;
    sky_buffer_t            data_buffer;
    sky_object_t            read_args, value;
    char * volatile         buffer;
    volatile ssize_t        buffer_size, buffer_used;
    sky_object_t volatile   peek_args, peek, read;

    peek_args = sky_tuple_pack(1, sky_integer_one);
    read_args = peek_args;
    peek = sky_object_getattr(self,
                              SKY_STRING_LITERAL("peek"),
                              NULL);
    read = sky_object_getattr(self,
                              SKY_STRING_LITERAL("read"),
                              sky_NotSpecified);

    if (limit < 0) {
        limit = SSIZE_MAX;
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(64, SKY_ASSET_CLEANUP_ON_ERROR);
        buffer_size = sky_memsize(buffer);
        buffer_used = 0;

        while (buffer_used < limit) {
            if (peek) {
                data = sky_object_call(peek, peek_args, NULL);
                SKY_ASSET_BLOCK_BEGIN {
                    sky_buffer_acquire(&data_buffer,
                                       data,
                                       SKY_BUFFER_FLAG_SIMPLE);
                    sky_asset_save(&data_buffer,
                                   (sky_free_t)sky_buffer_release,
                                   SKY_ASSET_CLEANUP_ALWAYS);

                    end = (const char *)data_buffer.buf + data_buffer.len;
                    for (c = data_buffer.buf; c < end; ++c) {
                        if (c - end >= limit) {
                            break;
                        }
                        if ('\n' == *c) {
                            ++c;
                            break;
                        }
                    }
                    n = c - (const char *)data_buffer.buf;
                    read_args = sky_tuple_pack(1, sky_integer_create(n));
                } SKY_ASSET_BLOCK_END;
            }

            SKY_ERROR_TRY {
                data = sky_object_call(read, read_args, NULL);
            } SKY_ERROR_EXCEPT(sky_OSError) {
                value = sky_object_getattr(sky_error_current()->value,
                                           SKY_STRING_LITERAL("errno"),
                                           NULL);
                if (!sky_object_isa(value, sky_integer_type) ||
                    sky_integer_value(value, INT_MIN, INT_MAX, NULL) != EINTR)
                {
                    sky_error_raise();
                }
                data = NULL;
            } SKY_ERROR_TRY_END;
            if (!data) {    /* EINTR */
                continue;
            }
            if (!(data_len = sky_object_len(data))) {
                break;
            }
            if (buffer_used + data_len + 1 > buffer_size) {
                buffer = sky_asset_realloc(buffer,
                                           buffer_used + data_len + 1,
                                           SKY_ASSET_CLEANUP_ON_ERROR,
                                           SKY_ASSET_UPDATE_FIRST_CURRENT);
                buffer_size = sky_memsize(buffer);
            }
            if (1 == data_len) {
                buffer[buffer_used++] = sky_bytes_byteat(data, 0);
            }
            else {
                SKY_ASSET_BLOCK_BEGIN {
                    sky_buffer_acquire(&data_buffer,
                                       data,
                                       SKY_BUFFER_FLAG_SIMPLE);
                    sky_asset_save(&data_buffer,
                                   (sky_free_t)sky_buffer_release,
                                   SKY_ASSET_CLEANUP_ALWAYS);
                    memcpy(buffer + buffer_used, data_buffer.buf, data_len);
                    buffer_used += data_len;
                } SKY_ASSET_BLOCK_END;
            }
            if (buffer[buffer_used - 1] == '\n') {
                break;
            }
        }

        buffer[buffer_used] = '\0';
        line = sky_bytes_createwithbytes(buffer, buffer_used, sky_free);
    } SKY_ASSET_BLOCK_END;

    return line;
}


static const char sky__io__IOBase_readlines_doc[] =
"Return a list of lines from the stream.\n"
"\n"
"hint can be specified to control the number of lines read: no more\n"
"lines will be read if the total size (in bytes/characters) of all\n"
"lines so far exceeds hint.";

static sky_list_t
sky__io__IOBase_readlines(sky_object_t self, ssize_t hint)
{
    ssize_t         length;
    sky_list_t      lines;
    sky_object_t    line;

    lines = sky_list_create(NULL);
    if (hint <= 0) {
        sky_list_extend(lines, self);
    }
    else {
        length = 0;
        while ((line = sky_object_next(self, NULL)) != NULL) {
            sky_list_append(lines, line);
            length += sky_object_len(line);
            if (length > hint) {
                break;
            }
        }
    }

    return lines;
}


static const char sky__io__IOBase_seek_doc[] =
"Change stream position.\n"
"\n"
"Change the stream position to the given byte offset offset. The offset is\n"
"interpreted relative to the position indicated by whence.  Values\n"
"for whence are:\n"
"\n"
"* 0 -- start of stream (the default); offset should be zero or positive\n"
"* 1 -- current stream position; offset may be negative\n"
"* 2 -- end of stream; offset is usually negative\n"
"\n"
"Return the new absolute position.";

static ssize_t
sky__io__IOBase_seek(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  ssize_t         offset,
    SKY_UNUSED  int             whence)
{
    sky_error_raise_string(sky_UnsupportedOperation, "seek");
}


static const char sky__io__IOBase_seekable_doc[] =
"Return whether object supports random access.\n"
"\n"
"If False, seek(), tell() and truncate() will raise UnsupportedOperation.\n"
"This method may need to do a test seek().";

static sky_bool_t
sky__io__IOBase_seekable(SKY_UNUSED sky_object_t self)
{
    return SKY_FALSE;
}


static const char sky__io__IOBase_tell_doc[] =
"Return current stream position.";

static ssize_t
sky__io__IOBase_tell(sky_object_t self)
{
    sky_object_t    value;

    value = sky_object_callmethod(self, 
                                  SKY_STRING_LITERAL("seek"),
                                  sky_object_build("(ii)", 0, 1),
                                  NULL);
    return sky_integer_value(value, SSIZE_MIN, SSIZE_MAX, NULL);
}


static const char sky__io__IOBase_truncate_doc[] =
"Truncate file to size bytes.\n"
"\n"
"File pointer is left unchanged.  Size defaults to the current IO\n"
"position as reported by tell().  Returns the new size.";

static void
sky__io__IOBase_truncate(SKY_UNUSED sky_object_t self, SKY_UNUSED ssize_t size)
{
    sky_error_raise_string(sky_UnsupportedOperation, "truncate");
}


static const char sky__io__IOBase_writable_doc[] =
"Return whether object was opened for writing.\n"
"\n"
"If False, write() will raise UnsupportedOperation.";

static sky_bool_t
sky__io__IOBase_writable(SKY_UNUSED sky_object_t self)
{
    return SKY_FALSE;
}


static void
sky__io__IOBase_writelines(sky_object_t self, sky_object_t lines)
{
    sky_object_t    iter, line, write;

    sky__io__IOBase__checkClosed(self);

    write = sky_object_getattr(self,
                               SKY_STRING_LITERAL("write"),
                               sky_NotSpecified);
    
    iter = sky_object_iter(lines);
    while ((line = sky_object_next(iter, NULL)) != NULL) {
        sky_object_call(write, sky_tuple_pack(1, line), NULL);
    }
}


sky_type_t sky__io__IOBase_type = NULL;

sky_type_t
sky__io__IOBase_initialize(void)
{
    static const char type_doc[] =
"The abstract base class for all I/O classes, acting on streams of\n"
"bytes. There is no public constructor.\n"
"\n"
"This class provides dummy implementations for many methods that\n"
"derived classes can override selectively; the default implementations\n"
"represent a file that cannot be read, written or seeked.\n"
"\n"
"Even though IOBase does not declare read, readinto, or write because\n"
"their signatures will vary, implementations and clients should\n"
"consider those methods part of the interface. Also, implementations\n"
"may raise UnsupportedOperation when operations they do not support are\n"
"called.\n"
"\n"
"The basic type used for binary data read from or written to a file is\n"
"bytes. bytearrays are accepted too, and in some cases (such as\n"
"readinto) needed. Text I/O classes work with str data.\n"
"\n"
"Note that calling any method (even inquiries) on a closed stream is\n"
"undefined. Implementations may raise IOError in this case.\n"
"\n"
"IOBase (and its subclasses) support the iterator protocol, meaning\n"
"that an IOBase object can be iterated over yielding the lines in a\n"
"stream.\n"
"\n"
"IOBase also supports the :keyword:`with` statement. In this example,\n"
"fp is closed after the suite of the with statement is complete:\n"
"\n"
"with open('spam.txt', 'r') as fp:\n"
"    fp.write('Spam and eggs!')\n";

    sky_type_t  type;

    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_IOBase"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   0,
                                   0,
                                   NULL,
                                   NULL);

    sky_type_setattr_getset(type,
            "closed",
            NULL,
            sky__io__IOBase_closed_getter,
            NULL);

    sky_type_setattr_builtin(type,
            "_checkClosed",
            sky_function_createbuiltin(
                    "_IOBase._checkClosed",
                    NULL,
                    (sky_native_code_function_t)sky__io__IOBase__checkClosed,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "_checkReadable",
            sky_function_createbuiltin(
                    "_IOBase._checkReadable",
                    NULL,
                    (sky_native_code_function_t)sky__io__IOBase__checkReadable,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "_checkSeekable",
            sky_function_createbuiltin(
                    "_IOBase._checkSeekable",
                    NULL,
                    (sky_native_code_function_t)sky__io__IOBase__checkSeekable,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "_checkWritable",
            sky_function_createbuiltin(
                    "_IOBase._checkWritable",
                    NULL,
                    (sky_native_code_function_t)sky__io__IOBase__checkWritable,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "_IOBase.close",
                    sky__io__IOBase_close_doc,
                    (sky_native_code_function_t)sky__io__IOBase_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "fileno",
            sky_function_createbuiltin(
                    "_IOBase.fileno",
                    sky__io__IOBase_fileno_doc,
                    (sky_native_code_function_t)sky__io__IOBase_fileno,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "flush",
            sky_function_createbuiltin(
                    "_IOBase.flush",
                    sky__io__IOBase_flush_doc,
                    (sky_native_code_function_t)sky__io__IOBase_flush,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "isatty",
            sky_function_createbuiltin(
                    "_IOBase.isatty",
                    sky__io__IOBase_isatty_doc,
                    (sky_native_code_function_t)sky__io__IOBase_isatty,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "_IOBase.readable",
                    sky__io__IOBase_readable_doc,
                    (sky_native_code_function_t)sky__io__IOBase_readable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readline",
            sky_function_createbuiltin(
                    "_IOBase.readline",
                    sky__io__IOBase_readline_doc,
                    (sky_native_code_function_t)sky__io__IOBase_readline,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "limit", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(type,
            "readlines",
            sky_function_createbuiltin(
                    "_IOBase.readlines",
                    sky__io__IOBase_readlines_doc,
                    (sky_native_code_function_t)sky__io__IOBase_readlines,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "hint", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "_IOBase.seek",
                    sky__io__IOBase_seek_doc,
                    (sky_native_code_function_t)sky__io__IOBase_seek,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "_IOBase.seekable",
                    sky__io__IOBase_seekable_doc,
                    (sky_native_code_function_t)sky__io__IOBase_seekable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "_IOBase.tell",
                    sky__io__IOBase_tell_doc,
                    (sky_native_code_function_t)sky__io__IOBase_tell,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "_IOBase.truncate",
                    sky__io__IOBase_truncate_doc,
                    (sky_native_code_function_t)sky__io__IOBase_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "size", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "_IOBase.writable",
                    sky__io__IOBase_writable_doc,
                    (sky_native_code_function_t)sky__io__IOBase_writable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "writelines",
            sky_function_createbuiltin(
                    "_IOBase.writelines",
                    NULL,
                    (sky_native_code_function_t)sky__io__IOBase_writelines,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "lines", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslots(type,
            "__del__", sky__io__IOBase_del,
            "__iter__", sky__io__IOBase_iter,
            "__next__", sky__io__IOBase_next,
            "__enter__", sky__io__IOBase_enter,
            "__exit__", sky__io__IOBase_exit,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io__IOBase_type),
            type,
            SKY_TRUE);
    return sky__io__IOBase_type;
}


static sky_bytes_t
sky__io__RawIOBase_read(sky_object_t self,
                               ssize_t      n)
{
    char                *memory;
    ssize_t             nread;
    sky_type_t          self_type;
    sky_buffer_t        *buffer;
    sky_object_t        result;
    sky_memoryview_t    view;

    if (n < 0) {
        return sky_object_callmethod(self,
                                     SKY_STRING_LITERAL("readall"),
                                     NULL,
                                     NULL);
    }
    if (!n) {
        return sky_bytes_empty;
    }

    nread = 0;
    result = NULL;

    SKY_ASSET_BLOCK_BEGIN {
        memory = sky_asset_malloc(n + 1, SKY_ASSET_CLEANUP_ON_ERROR);

        self_type = sky_object_type(self);
        if (sky__io_FileIO_type == self_type) {
            do {
                nread = sky__io_FileIO_readinto_native(self, memory, n);
            } while (-1 == nread);
        }
        else if (sky__io_BytesIO_type == self_type) {
            do {
                nread = sky__io_BytesIO_readinto_native(self, memory, n);
            } while (-1 == nread);
        }
        else if (sky__io_StringIO_type == self_type) {
            do {
                nread = sky__io_StringIO_readinto_native(self, memory, n);
            } while (-1 == nread);
        }
        else {
            SKY_ASSET_BLOCK_BEGIN {
                buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                          SKY_ASSET_CLEANUP_ON_ERROR);
                sky_buffer_initialize(buffer,
                                      memory,
                                      n,
                                      SKY_FALSE,
                                      SKY_BUFFER_FLAG_CONTIG);
                view = sky_memoryview_createwithbuffer(buffer);
            } SKY_ASSET_BLOCK_END;
            SKY_ASSET_BLOCK_BEGIN {
                sky_asset_save(view,
                               (sky_free_t)sky_memoryview_release,
                               SKY_ASSET_CLEANUP_ALWAYS);
                result = sky_object_callmethod(self,
                                               SKY_STRING_LITERAL("readinto"),
                                               sky_tuple_pack(1, view),
                                               NULL);
            } SKY_ASSET_BLOCK_END;

            /* If result is None, return None; otherwise, create a bytes object
             * with it the amount of data that was read (it should be an int).
             */
            if (sky_object_isnull(result)) {
                /* Just in case it's somehow NULL */
                result = sky_None;
            }
            else {
                nread = sky_integer_value(result,
                                          0, SSIZE_MAX - 1,
                                          sky_ValueError);
                if (nread < 0 || nread > n) {
                    sky_error_raise_format(sky_ValueError,
                            "readinto() returned invalid length %zd "
                            "(should have been between 0 and %zd)",
                            nread,
                            n);
                }
                result = NULL;
            }
        }

        if (!result) {
            memory[nread] = 0;
            result = sky_bytes_createwithbytes(memory, nread, sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io__RawIOBase_readall_doc[] =
"Read until EOF, using multiple read() calls.";


static sky_object_t
sky__io__RawIOBase_readall(sky_object_t self)
{
    sky_bytes_t             data;
    sky_object_t            value;
    sky_list_t volatile     chunks;
    sky_tuple_t volatile    args;
    sky_object_t volatile   read;

    args = sky_object_build("(i)", SKY__IO_DEFAULT_BUFFER_SIZE);
    read = sky_object_getattr(self,
                              SKY_STRING_LITERAL("read"),
                              sky_NotSpecified);

    chunks = sky_list_create(NULL);
    for (;;) {
        SKY_ERROR_TRY {
            data = sky_object_call(read, args, NULL);
        } SKY_ERROR_EXCEPT(sky_OSError) {
            value = sky_object_getattr(sky_error_current()->value,
                                       SKY_STRING_LITERAL("errno"),
                                       NULL);
            if (!sky_object_isa(value, sky_integer_type) ||
                sky_integer_value(value, INT_MIN, INT_MAX, NULL) != EINTR)
            {
                sky_error_raise();
            }
            data = NULL;
        } SKY_ERROR_TRY_END;
        if (!data) {    /* EINTR */
            continue;
        }
        if (!sky_object_bool(data)) {
            if (sky_object_isnull(data) && !sky_list_len(chunks)) {
                return sky_None;
            }
            break;
        }
        if (!sky_object_isa(data, sky_bytes_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "read() should return bytes");
        }
        sky_list_append(chunks, data);
    }

    return sky_bytes_join(sky_bytes_empty, chunks);
}


sky_type_t sky__io__RawIOBase_type = NULL;

sky_type_t
sky__io__RawIOBase_initialize(void)
{
    static const char type_doc[] =
"Base class for raw binary I/O.";

    sky_type_t  type;
    sky_tuple_t bases;

    bases = sky_tuple_pack(1, sky__io__IOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_RawIOBase"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   0,
                                   0,
                                   bases,
                                   NULL);

    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "_RawIOBase.read",
                    NULL,
                    (sky_native_code_function_t)sky__io__RawIOBase_read,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(type,
            "readall",
            sky_function_createbuiltin(
                    "_RawIOBase.readall",
                    sky__io__RawIOBase_readall_doc,
                    (sky_native_code_function_t)sky__io__RawIOBase_readall,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io__RawIOBase_type),
            type,
            SKY_TRUE);
    return sky__io__RawIOBase_type;
}
