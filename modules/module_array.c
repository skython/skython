#include "../core/sky_private.h"


#define SKY_ARRAY_ARRAY_MINIMUM_CAPACITY    8


SKY_MODULE_EXTERN const char *skython_module_array_doc;

SKY_MODULE_EXTERN void
skython_module_array_initialize(sky_module_t module);


static sky_object_t sky_array__array_reconstructor = NULL;


typedef struct sky_array_array_table_s {
    ssize_t                             len;
    ssize_t                             capacity;
    char                                data[0];
} sky_array_array_table_t;

static sky_array_array_table_t sky_array_array_table_empty =
{
    0,
    0,
    {},
};


typedef struct sky_array_type_s {
    sky_unicode_char_t                  typecode;
    size_t                              itemsize;
    sky_data_type_t                     data_type;

    /* used by the buffer protocol */
    const char *                        formats;

    int                                 is_signed;
    int                                 is_integer_type;
} sky_array_type_t;


static sky_type_t sky_array_array_type = NULL;

static const char sky_array_array_type_doc[] =
"array(typecode [, initializer]) -> array\n"
"\n"
"Return a new array whose items are restricted by typecode, and\n"
"initialized from the optional initializer value, which must be a list,\n"
"string or iterable over elements of the appropriate type.\n"
"\n"
"Arrays represent basic values and behave very much like lists, except\n"
"the type of objects stored in them is constrained.\n"
"\n"
"Methods:\n"
"\n"
"append() -- append a new item to the end of the array\n"
"buffer_info() -- return information giving the current memory info\n"
"byteswap() -- byteswap all the items of the array\n"
"count() -- return number of occurrences of an object\n"
"extend() -- extend array by appending multiple elements from an iterable\n"
"fromfile() -- read items from a file object\n"
"fromlist() -- append items from the list\n"
"frombytes() -- append items from the string\n"
"index() -- return index of first occurrence of an object\n"
"insert() -- insert a new item into the array at a provided position\n"
"pop() -- remove and return item (default last)\n"
"remove() -- remove first occurrence of an object\n"
"reverse() -- reverse the order of the items in the array\n"
"tofile() -- write all items to a file object\n"
"tolist() -- return the array converted to an ordinary list\n"
"tobytes() -- return the array converted to a string\n"
"\n"
"Attributes:\n"
"\n"
"typecode -- the typecode character used to create the array\n"
"itemsize -- the length in bytes of one array item\n";


typedef struct sky_array_array_data_s {
    const sky_array_type_t *            type;
    sky_array_array_table_t * volatile  table;
    volatile int32_t                    buffer_count;
} sky_array_array_data_t;

static inline sky_array_array_data_t *
sky_array_array_data(sky_object_t object)
{
    return sky_object_data(object, sky_array_array_type);
}


static sky_type_t sky_array_array_iterator_type = NULL;

typedef struct sky_array_array_iterator_data_s {
    sky_object_t                        array;
    ssize_t                             next_index;
} sky_array_array_iterator_data_t;

static inline sky_array_array_iterator_data_t *
sky_array_array_iterator_data(sky_object_t object)
{
    return sky_object_data(object, sky_array_array_iterator_type);
}


/* Normally these would be ordered alphabetically, but they're not so that
 * array.typecodes matches CPython. Note that 'u' is deprecated since v3.3,
 * and is to be removed in v4.0.
 *
 * >>> Don't forget to update mformat if this list is changed! <<<
 */
static sky_array_type_t sky_array_types[] =
{
    { 'b',  sizeof(char), SKY_DATA_TYPE_CHAR, "b", 1, 1, },
    { 'B',  sizeof(unsigned char), SKY_DATA_TYPE_UCHAR, "B", 0, 1, },
    { 'u',  sizeof(sky_unicode_char_t), SKY_DATA_TYPE_CODEPOINT, "w", 1, 0, },
    { 'h',  sizeof(short), SKY_DATA_TYPE_SHORT, "h", 1, 1, },
    { 'H',  sizeof(unsigned short), SKY_DATA_TYPE_USHORT, "H", 0, 1, },
    { 'i',  sizeof(int), SKY_DATA_TYPE_INT, "i", 1, 1, },
    { 'I',  sizeof(unsigned int), SKY_DATA_TYPE_UINT, "I", 0, 1, },
    { 'l',  sizeof(long), SKY_DATA_TYPE_LONG, "l", 1, 1, },
    { 'L',  sizeof(unsigned long), SKY_DATA_TYPE_ULONG, "L", 0, 1, },
    { 'q',  sizeof(long long), SKY_DATA_TYPE_LONG_LONG, "q", 1, 1, },
    { 'Q',  sizeof(unsigned long long), SKY_DATA_TYPE_ULONG_LONG, "Q", 0, 1, },
    { 'f',  sizeof(float), SKY_DATA_TYPE_FLOAT, "f", 1, 0, },
    { 'd',  sizeof(double), SKY_DATA_TYPE_DOUBLE, "d", 1, 0, },
};

static const size_t sky_array_types_count =
        (sizeof(sky_array_types) / sizeof(sky_array_types[0]));


typedef enum sky_array_mformat_code_e {
    /* UNKNOWN_FORMAT is used to indicate that the machine format for an
     * array type code cannot be interpreted. When this occurs, a list of
     * Python objects is used to represent the content of the array
     * instead of using the memory content of the array directly. In that
     * case, the array_reconstructor mechanism is bypassed completely, and
     * the standard array constructor is used instead.
     *
     * This is will most likely occur when the machine doesn't use IEEE
     * floating-point numbers.
     */
    SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT       =   -1,

    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT8        =   0,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT8          =   1,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT16_LE    =   2,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT16_BE    =   3,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT16_LE      =   4,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT16_BE      =   5,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT32_LE    =   6,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT32_BE    =   7,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT32_LE      =   8,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT32_BE      =   9,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT64_LE    =   10,
    SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT64_BE    =   11,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT64_LE      =   12,
    SKY_ARRAY_MFORMAT_CODE_SIGNED_INT64_BE      =   13,
    SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_LE    =   14,
    SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_BE    =   15,
    SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_LE   =   16,
    SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_BE   =   17,
    SKY_ARRAY_MFORMAT_CODE_UTF16_LE             =   18,
    SKY_ARRAY_MFORMAT_CODE_UTF16_BE             =   19,
    SKY_ARRAY_MFORMAT_CODE_UTF32_LE             =   20,
    SKY_ARRAY_MFORMAT_CODE_UTF32_BE             =   21,

    SKY_ARRAY_MFORMAT_CODE_MIN                  =   0,
    SKY_ARRAY_MFORMAT_CODE_MAX                  =   21,
} sky_array_mformat_code_t;

typedef struct sky_array_mformat_type_s {
    size_t                              size;
    sky_bool_t                          is_signed;
    sky_bool_t                          is_big_endian;
} sky_array_mformat_type_t;

static const sky_array_mformat_type_t sky_array_mformat_types[] =
{
    { 1, SKY_FALSE, SKY_FALSE },        /* UNSIGNED_INT8      */
    { 1, SKY_TRUE,  SKY_FALSE },        /* SIGNED_INT8        */
    { 2, SKY_FALSE, SKY_FALSE },        /* UNSIGNED_INT16_LE  */
    { 2, SKY_FALSE, SKY_TRUE  },        /* UNSIGNED_INT16_BE  */
    { 2, SKY_TRUE,  SKY_FALSE },        /* SIGNED_INT16_LE    */
    { 2, SKY_TRUE,  SKY_TRUE  },        /* SIGNED_INT16_BE    */
    { 4, SKY_FALSE, SKY_FALSE },        /* UNSIGNED_INT32_LE  */
    { 4, SKY_FALSE, SKY_TRUE  },        /* UNSIGNED_INT32_BE  */
    { 4, SKY_TRUE,  SKY_FALSE },        /* SIGNED_INT32_LE    */
    { 4, SKY_TRUE,  SKY_TRUE  },        /* SIGNED_INT32_BE    */
    { 8, SKY_FALSE, SKY_FALSE },        /* UNSIGNED_INT64_LE  */
    { 8, SKY_FALSE, SKY_TRUE  },        /* UNSIGNED_INT64_BE  */
    { 8, SKY_TRUE,  SKY_FALSE },        /* SIGNED_INT64_LE    */
    { 8, SKY_TRUE,  SKY_TRUE  },        /* SIGNED_INT64_BE    */
    { 4, SKY_FALSE, SKY_FALSE },        /* IEEE_754_FLOAT_LE  */
    { 4, SKY_FALSE, SKY_TRUE  },        /* IEEE_754_FLOAT_BE  */
    { 8, SKY_FALSE, SKY_FALSE },        /* IEEE_754_DOUBLE_LE */
    { 8, SKY_FALSE, SKY_TRUE  },        /* IEEE_754_DOUBLE_BE */
    { 2, SKY_FALSE, SKY_FALSE },        /* UTF16_LE           */
    { 2, SKY_FALSE, SKY_TRUE  },        /* UTF16_BE           */
    { 4, SKY_FALSE, SKY_FALSE },        /* UTF32_LE           */
    { 4, SKY_FALSE, SKY_TRUE  },        /* UTF32_BE           */
};

static sky_array_mformat_code_t
sky_array_mformat_code(const sky_array_type_t *type)
{
#if SKY_ENDIAN == SKY_ENDIAN_BIG
    static const int    endian = 1;
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
    static const int    endian = 0;
#else
#   error implementation missing
#endif

    switch (type->typecode) {
        case 'b':
            return SKY_ARRAY_MFORMAT_CODE_SIGNED_INT8;
        case 'B':
            return SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT8;
        case 'u':
#if SKY_ENDIAN == SKY_ENDIAN_BIG
            return SKY_ARRAY_MFORMAT_CODE_UTF32_BE;
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
            return SKY_ARRAY_MFORMAT_CODE_UTF32_LE;
#else
#   error implementation missing
#endif
        case 'f':
            if (sizeof(float) == 4) {
                const float y = 16711938.0;

                if (!memcmp(&y, "\x4b\x7f\x01\x02", 4)) {
                    return SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_BE;
                }
                if (!memcmp(&y, "\x02\x01\x7f\x4b", 4)) {
                    return SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_LE;
                }
            }
            return SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT;
        case 'd':
            if (sizeof(double) == 8) {
                const double    x = 9006104071832581.0;

                if (!memcmp(&x, "\x43\x3f\xff\x01\x02\x03\x04\x05", 8)) {
                    return SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_BE;
                }
                if (!memcmp(&x, "\x05\x04\x03\x02\x01\xff\x3f\x43", 8)) {
                    return SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_LE;
                }
            }
            return SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT;
        case 'h':
        case 'H':
        case 'i':
        case 'I':
        case 'l':
        case 'L':
        case 'q':
        case 'Q':
            break;
        default:
            return SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT;
    }

    switch (type->itemsize) {
        case 2:
            return (int)SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT16_LE +
                   (2 * type->is_signed) + endian;
        case 4:
            return (int)SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT32_LE +
                   (2 * type->is_signed) + endian;
        case 8:
            return (int)SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT64_LE +
                   (2 * type->is_signed) + endian;
    }

    return SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT;
}


static inline sky_bool_t
sky_array_array_isshared(sky_object_t self)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(self));
}


static inline sky_array_array_table_t *
sky_array_array_table(sky_object_t self, sky_bool_t fast)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    sky_hazard_pointer_t    *hazard;

    if (fast && !sky_array_array_isshared(self)) {
        return (self_data->table ? self_data->table
                                 : &sky_array_array_table_empty);
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
    if (!hazard->pointer) {
        sky_hazard_pointer_release(hazard);
        return &sky_array_array_table_empty;
    }
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);
    return hazard->pointer;
}

static inline void
sky_array_array_table_copyitem(
                sky_array_array_table_t *   table,
                const sky_array_type_t *    type,
                ssize_t                     di,
                ssize_t                     si)
{
    memcpy(table->data + (di * type->itemsize),
           table->data + (si * type->itemsize),
           type->itemsize);
}

static inline void
sky_array_array_table_copyitem8(
                sky_array_array_table_t *   table,
    SKY_UNUSED  const sky_array_type_t *    type,
                ssize_t                     di,
                ssize_t                     si)
{
    ((uint8_t *)table->data)[di] = ((uint8_t *)table->data)[si];
}

static inline void
sky_array_array_table_copyitem16(
                sky_array_array_table_t *   table,
    SKY_UNUSED  const sky_array_type_t *    type,
                ssize_t                     di,
                ssize_t                     si)
{
    ((uint16_t *)table->data)[di] = ((uint16_t *)table->data)[si];
}

static inline void
sky_array_array_table_copyitem32(
                sky_array_array_table_t *   table,
    SKY_UNUSED  const sky_array_type_t *    type,
                ssize_t                     di,
                ssize_t                     si)
{
    ((uint32_t *)table->data)[di] = ((uint32_t *)table->data)[si];
}

static inline void
sky_array_array_table_copyitem64(
                sky_array_array_table_t *   table,
    SKY_UNUSED  const sky_array_type_t *    type,
                ssize_t                     di,
                ssize_t                     si)
{
    ((uint64_t *)table->data)[di] = ((uint64_t *)table->data)[si];
}

static sky_object_t
sky_array_array_table_getitem(
                sky_array_array_table_t *   table,
                const sky_array_type_t *    type,
                ssize_t                     index)
{
    return sky_data_type_load(type->data_type,
                              table->data + (index * type->itemsize),
                              NULL);
}

static void
sky_array_array_table_setitem(
                sky_array_array_table_t *   table,
                const sky_array_type_t *    type,
                ssize_t                     index,
                sky_object_t                value)
{
    sky_data_type_store(type->data_type,
                        NULL,
                        value,
                        table->data + (index * type->itemsize),
                        NULL);
}

static inline ssize_t
sky_array_index_normalize(ssize_t index, ssize_t len, const char *name)
{
    if (index < 0) {
        index += len;
        if (index < 0) {
            sky_error_raise_format(sky_IndexError,
                                   "%s index out of range",
                                   name);
        }
    }
    if (index >= len) {
        sky_error_raise_format(sky_IndexError,
                               "%s index out of range",
                               name);
    }
    return index;
}

static sky_bool_t
sky_array_array_table_compare(
                sky_array_array_table_t *   self_table,
                const sky_array_type_t *    self_type,
                sky_array_array_table_t *   other_table,
                const sky_array_type_t *    other_type,
                sky_compare_op_t            compare_op)
{
    ssize_t i, len, other_len, self_len;

    if ((self_len = self_table->len) != (other_len = other_table->len)) {
        if (SKY_COMPARE_OP_EQUAL == compare_op) {
            return SKY_FALSE;
        }
        if (SKY_COMPARE_OP_NOT_EQUAL == compare_op) {
            return SKY_TRUE;
        }
    }

    len = SKY_MIN(self_len, other_len);
    for (i = 0; i < len; ++i) {
        sky_object_t    other_item, self_item;

        self_item = sky_array_array_table_getitem(self_table, self_type, i);
        other_item = sky_array_array_table_getitem(other_table, other_type, i);

        if (!sky_object_compare(self_item, other_item, SKY_COMPARE_OP_EQUAL)) {
            switch (compare_op) {
                case SKY_COMPARE_OP_EQUAL:
                    return SKY_FALSE;
                case SKY_COMPARE_OP_NOT_EQUAL:
                    return SKY_TRUE;
                default:
                    break;
            }
            return sky_object_compare(self_item, other_item, compare_op);
        }
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            return (self_len == other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (self_len != other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_LESS:
            return (self_len < other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (self_len <= other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_GREATER:
            return (self_len > other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (self_len >= other_len ? SKY_TRUE : SKY_FALSE);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }

    sky_error_fatal("internal error");
}

static sky_array_array_table_t *
sky_array_array_table_resize(
                sky_array_array_table_t *   old_table,
                const sky_array_type_t *    type,
                size_t                      capacity)
    /* NOTE: this does NOT free old_table! */
{
    ssize_t                 i, new_capacity;
    sky_array_array_table_t *new_table;

    i = (sizeof(sky_array_array_table_t) + type->itemsize - 1) /
        type->itemsize;
    if (capacity < SKY_ARRAY_ARRAY_MINIMUM_CAPACITY) {
        capacity = SKY_ARRAY_ARRAY_MINIMUM_CAPACITY;
    }
    new_capacity = sky_util_roundpow2(capacity + i) - i;
    if (old_table && (new_capacity == old_table->capacity ||
                      new_capacity < old_table->len))
    {
        return old_table;
    }
    new_table = sky_malloc(sizeof(sky_array_array_table_t) +
                           (new_capacity * type->itemsize));
    new_table->capacity = new_capacity;

    if (!old_table) {
        new_table->len = 0;
    }
    else if ((new_table->len = old_table->len) > 0) {
        memcpy(new_table->data,
               old_table->data,
               new_table->len * type->itemsize);
    }

    return new_table;
}

static sky_array_array_table_t *
sky_array_array_data_settable(
                sky_array_array_data_t *    array_data,
                sky_array_array_table_t *   new_table,
                sky_bool_t                  shared)
{
    sky_array_array_table_t *old_table;

    if (array_data->buffer_count) {
        sky_error_raise_string(
                sky_BufferError,
                "cannot resize an array that is exporting buffers");
    }

    if (!shared) {
        old_table = array_data->table;
        array_data->table = new_table;
    }
    else {
        old_table = sky_atomic_exchange(new_table,
                                        SKY_AS_VOIDP(&(array_data->table)));
    }
    if (old_table && old_table != new_table) {
        sky_error_validate_debug(&sky_array_array_table_empty != old_table);
        sky_hazard_pointer_retire(old_table, sky_free);
    }

    return new_table;
}

static sky_array_array_table_t *
sky_array_array_resize(sky_object_t self, ssize_t capacity)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    sky_hazard_pointer_t    *hazard;
    sky_array_array_table_t *new_table, *old_table;

    /* This is unnecessary in Skython, but CPython does it and the unit tests
     * check for it. It's not an unreasonable restriction, really, but our
     * ability to catch it in all cases is not perfect due to the lack of
     * locks, and adding locks just to catch the edge cases of programming
     * errors is out of the question.
     */
    if (self_data->buffer_count) {
        sky_error_raise_string(
                sky_BufferError,
                "cannot resize an array that is exporting buffers");
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);

    old_table = hazard->pointer;
    new_table = sky_array_array_table_resize(old_table,
                                             self_data->type,
                                             capacity);
    sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));
    sky_array_array_data_settable(self_data,
                                  new_table,
                                  sky_array_array_isshared(self));

    return new_table;
}


static void
sky_array_array_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky_free(((sky_array_array_data_t *)data)->table);
}

static void
sky_array_array_instance_buffer_acquire(
    SKY_UNUSED  sky_object_t    self,
                void *          data,
                sky_buffer_t *  buffer,
                unsigned int    flags)
{
    sky_array_array_data_t  *self_data = data;

    sky_hazard_pointer_t    *hazard;
    sky_array_array_table_t *table;

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
    if (!(table = hazard->pointer)) {
        sky_hazard_pointer_release(hazard);
        hazard = NULL;
        table = &sky_array_array_table_empty;
    }

    buffer->buf = table->data;
    buffer->len = table->len * self_data->type->itemsize;
    buffer->readonly = SKY_FALSE;
    buffer->format = NULL;
    buffer->ndim = 1;
    buffer->shape = NULL;
    buffer->strides = NULL;
    buffer->suboffsets = NULL;
    buffer->itemsize = self_data->type->itemsize;
    buffer->internal = hazard;

    if ((flags & SKY_BUFFER_FLAG_ND) == SKY_BUFFER_FLAG_ND) {
        buffer->shape = &(table->len);
    }
    if ((flags & SKY_BUFFER_FLAG_STRIDES) == SKY_BUFFER_FLAG_STRIDES) {
        buffer->strides = (ssize_t *)&(self_data->type->itemsize);
    }
    if ((flags & SKY_BUFFER_FLAG_FORMAT) == SKY_BUFFER_FLAG_FORMAT) {
        buffer->format = self_data->type->formats;
    }

    if (sky_array_array_isshared(self)) {
        sky_atomic_increment32(&(self_data->buffer_count));
    }
    else {
        ++self_data->buffer_count;
    }
}

static void
sky_array_array_instance_buffer_release(
    SKY_UNUSED  sky_object_t    self,
                void *          data,
                sky_buffer_t *  buffer)
{
    sky_array_array_data_t  *self_data = data;

    if (buffer->internal) {
        sky_hazard_pointer_release(buffer->internal);
        buffer->internal = NULL;
    }

    if (sky_array_array_isshared(self)) {
        sky_atomic_decrement32(&(self_data->buffer_count));
    }
    else {
        --self_data->buffer_count;
    }
}


static void
sky_array_array_iterator_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_array_array_iterator_data_t *self_data = data;

    sky_object_visit(self_data->array, visit_data);
}


static sky_object_t
sky_array_array_repeat(sky_object_t self, ssize_t count, sky_bool_t inplace);

static void
sky_array_array_setitem(
                sky_object_t    self,
                sky_object_t    item,
                sky_object_t    value);

static sky_bytes_t
sky_array_array_tobytes(sky_object_t self);

static sky_list_t
sky_array_array_tolist(sky_object_t self);

static sky_string_t
sky_array_array_tounicode(sky_object_t self);


static sky_object_t
sky_array_array_add(sky_object_t self, sky_object_t other)
{
    sky_object_t    new_array;

    if (!sky_object_isa(other, sky_array_array_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only append array (not %#@) to array",
                sky_type_name(sky_object_type(other)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 new_len, other_len, self_len;
        sky_array_array_data_t  *new_data, *other_data, *self_data;
        sky_array_array_table_t *new_table, *other_table, *self_table;

        self_data = sky_array_array_data(self);
        other_data = sky_array_array_data(other);
        if (self_data->type != other_data->type) {
            sky_error_raise_string(sky_TypeError, "array types do not match");
        }

        self_table = sky_array_array_table(self, SKY_TRUE);
        self_len = self_table->len;
        other_table = sky_array_array_table(other, SKY_TRUE);
        other_len = other_table->len;

        if (SSIZE_MAX - self_len < other_len) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        new_len = self_len + other_len;
        new_table = sky_array_array_table_resize(NULL,
                                                 self_data->type,
                                                 new_len);
        sky_asset_save(new_table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        sky_error_validate_debug(new_len <= new_table->capacity);
        new_table->len = new_len;
        memcpy(new_table->data,
               self_table->data,
               self_len * self_data->type->itemsize);
        memcpy(new_table->data + (self_len * self_data->type->itemsize),
               other_table->data,
               other_len * other_data->type->itemsize);

        new_array = sky_object_allocate(sky_array_array_type);
        new_data = sky_array_array_data(new_array);
        new_data->type = self_data->type;
        sky_array_array_data_settable(new_data, new_table, SKY_FALSE);
    } SKY_ASSET_BLOCK_END;

    return new_array;
}

static void
sky_array_array_append(sky_object_t self, sky_object_t item)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);

        ssize_t                 index;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        index = table->len;
        table = sky_array_array_resize(self, index);
        sky_error_validate_debug(index <= table->capacity);
        sky_array_array_table_setitem(table,
                                             self_data->type,
                                             index,
                                             item);
        sky_error_validate_debug(index + 1 <= table->capacity);
        table->len = index + 1;
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_append_doc[] =
"append(x)\n\n"
"Append new value x to the end of the array.";

static sky_tuple_t
sky_array_array_buffer_info(sky_object_t self)
{
    sky_tuple_t tuple;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_table_t  *table;

        table = sky_array_array_table(self, SKY_TRUE);
        tuple = sky_object_build(
                        "(Oiz)",
                        sky_integer_createfromunsigned((uintptr_t)table),
                        table->len);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static const char sky_array_array_buffer_info_doc[] =
"buffer_info() -> (address, length)\n\n"
"Return a tuple (address, length) giving the current memory address and\n"
"the length in items of the buffer used to hold array's contents\n"
"The length should be multiplied by the itemsize attribute to calculate\n"
"the buffer length in bytes.";

static void
sky_array_array_byteswap(sky_object_t self)
{
    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        char                    *data;
        ssize_t                 i, len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        len = table->len;
        data = table->data;

#if SKY_ENDIAN == SKY_ENDIAN_BIG
#   define swap16(x)    sky_endian_htol16(x)
#   define swap32(x)    sky_endian_htol32(x)
#   define swap64(x)    sky_endian_htol64(x)
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
#   define swap16(x)    sky_endian_htob16(x)
#   define swap32(x)    sky_endian_htob32(x)
#   define swap64(x)    sky_endian_htob64(x)
#else
#   error implementation missing
#endif

        switch (type->itemsize) {
            case 1:
                break;
            case 2:
                for (i = 0; i < len; ++i) {
                    ((uint16_t *)data)[i] = swap16(((uint16_t *)data)[i]);
                }
                break;
            case 4:
                for (i = 0; i < len; ++i) {
                    ((uint32_t *)data)[i] = swap32(((uint32_t *)data)[i]);
                }
                break;
            case 8:
                for (i = 0; i < len; ++i) {
                    ((uint64_t *)data)[i] = swap64(((uint64_t *)data)[i]);
                }
                break;
            default:
                sky_error_raise_string(
                        sky_RuntimeError,
                        "don't know how to byteswap this array type");
        }

#undef swap64
#undef swap32
#undef swap16

    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_byteswap_doc[] =
"byteswap()\n\n"
"Byteswap all items of the array.  If the items in the array are not 1, 2,\n"
"4, or 8 bytes in size, RuntimeError is raised.";

static void
sky_array_array_clear(sky_object_t self)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    sky_array_array_data_settable(
            sky_array_array_data(self),
            sky_array_array_table_resize(NULL,
                                         self_data->type,
                                         SKY_ARRAY_ARRAY_MINIMUM_CAPACITY),
                                         sky_array_array_isshared(self));
}


static sky_object_t
sky_array_array_compare(
                sky_object_t        self,
                sky_object_t        other,
                sky_compare_op_t    compare_op)
{
    sky_bool_t  result;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_array_array_type)) {
        return sky_NotImplemented;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *other_data, *self_data;
        sky_array_array_table_t *other_table, *self_table;

        self_data = sky_array_array_data(self);
        self_table = sky_array_array_table(self, SKY_FALSE);
        other_data = sky_array_array_data(other);
        other_table = sky_array_array_table(other, SKY_FALSE);

        result = sky_array_array_table_compare(self_table,
                                               self_data->type,
                                               other_table,
                                               other_data->type,
                                               compare_op);
    } SKY_ASSET_BLOCK_END;

    return (result ? sky_True : sky_False);
}

static sky_bool_t
sky_array_array_contains(sky_object_t self, sky_object_t item)
{
    sky_bool_t  result = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_FALSE);
        if ((len = table->len) > 0) {
            ssize_t i;

            for (i = 0; i < len; ++i) {
                sky_object_t    object;

                object = sky_array_array_table_getitem(table,
                                                       self_data->type,
                                                       i);
                if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
                    result = SKY_TRUE;
                    break;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_array_array_copy(sky_object_t self)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    sky_object_t            copy;
    sky_array_array_data_t  *copy_data;

    copy = sky_object_allocate(sky_array_array_type);
    copy_data = sky_array_array_data(copy);
    copy_data->type = self_data->type;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_table_t *copy_table, *self_table;

        self_table = sky_array_array_table(self, SKY_TRUE);
        copy_table = sky_array_array_resize(copy, self_table->len);

        copy_table->len = self_table->len;
        memcpy(copy_table->data,
               self_table->data,
               copy_table->len * copy_data->type->itemsize);
    } SKY_ASSET_BLOCK_END;

    return copy;
}

static const char sky_array_array_copy_doc[] =
"copy(array)\n\n"
"Return a copy of the array.";

static ssize_t
sky_array_array_count(sky_object_t self, sky_object_t item)
{
    ssize_t result = 0;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_FALSE);
        if ((len = table->len) > 0) {
            ssize_t i;

            for (i = 0; i < len; ++i) {
                sky_object_t    object;

                object = sky_array_array_table_getitem(table,
                                                       self_data->type,
                                                       i);
                if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
                    ++result;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_array_array_count_doc[] =
"count(x)\n\n"
"Return number of occurrences of x in the array.";

static sky_object_t
sky_array_array_deepcopy(sky_object_t self, SKY_UNUSED sky_object_t memo)
{
    return sky_array_array_copy(self);
}

static void
sky_array_array_delitem(sky_object_t self, sky_object_t item)
{
    sky_array_array_setitem(self, item, NULL);
}

static sky_object_t
sky_array_array_eq(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_EQUAL);
}

static void
sky_array_array_extendwitharray(sky_object_t self, sky_object_t other)
{
    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 new_len, other_len, self_len;
        const sky_array_type_t  *type;
        sky_array_array_table_t *other_table, *self_table;

        type = sky_array_array_data(self)->type;
        if (type != sky_array_array_data(other)->type) {
            sky_error_raise_string(
                    sky_TypeError,
                    "can only extend with array of same kind");
        }

        self_table = sky_array_array_table(self, self != other);
        self_len = self_table->len;
        other_table = sky_array_array_table(other, self != other);
        other_len = other_table->len;

        if (SSIZE_MAX - self_len < other_len) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        new_len = self_len + other_len;
        self_table = sky_array_array_resize(self, new_len);
        memcpy(self_table->data + (self_len * type->itemsize),
               other_table->data,
               other_len * type->itemsize);
        sky_error_validate_debug(new_len <= self_table->capacity);
        self_table->len = new_len;
    } SKY_ASSET_BLOCK_END;
}

static void
sky_array_array_extend(sky_object_t self, sky_object_t sequence)
{
    if (sky_object_isa(sequence, sky_array_array_type)) {
        sky_array_array_extendwitharray(self, sequence);
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);
        const sky_array_type_t  *type = self_data->type;

        ssize_t                 capacity, hint, len;
        sky_object_t            item, iter;
        sky_hazard_pointer_t    *hazard;
        sky_array_array_table_t *new_table, *table;

        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (!(table = hazard->pointer)) {
            table = &sky_array_array_table_empty;
        }
        len = table->len;

        if (!sky_object_isiterable(sequence)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "expected sequence; got %#@",
                    sky_type_name(sky_object_type(sequence)));
        }
        hint = sky_object_length_hint(sequence,
                                      SKY_ARRAY_ARRAY_MINIMUM_CAPACITY);
        if (len + hint > table->capacity) {
            new_table = sky_array_array_table_resize(table,
                                                     type,
                                                     len + hint);
            sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));
            sky_error_validate_debug(hazard->pointer == new_table);
            table = sky_array_array_data_settable(
                            self_data,
                            new_table,
                            sky_array_array_isshared(self));
        }
        capacity = table->capacity;
        sky_error_validate_debug(len <= capacity);

#define add_item(_item)                                                     \
    do {                                                                    \
        if (len >= capacity) {                                              \
            sky_error_validate_debug(len <= table->capacity);               \
            table->len = len;                                               \
            new_table = sky_array_array_table_resize(table,                 \
                                                     type,                  \
                                                     capacity << 1);        \
            sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));    \
            sky_error_validate_debug(hazard->pointer == new_table);         \
            table = sky_array_array_data_settable(                          \
                            self_data,                                      \
                            new_table,                                      \
                            sky_array_array_isshared(self));                \
            capacity = table->capacity;                                     \
        }                                                                   \
        SKY_ERROR_TRY {                                                     \
            sky_array_array_table_setitem(table, type, len++, item);        \
        } SKY_ERROR_EXCEPT_ANY {                                            \
            sky_error_validate_debug(len <= table->capacity);               \
            table->len = len;                                               \
            sky_error_raise();                                              \
        } SKY_ERROR_TRY_END;                                                \
    } while (0)

        if (sky_sequence_isiterable(sequence)) {
            SKY_SEQUENCE_FOREACH(sequence, item) {
                add_item(item);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(sequence);
            while ((item = sky_object_next(iter, NULL)) != NULL) {
                add_item(item);
            }
        }
        sky_error_validate_debug(len <= table->capacity);
        table->len = len;

#undef add_item

    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_extend_doc[] =
"extend(array or iterable)\n\n"
"Append items to the end of the array.";

static void
sky_array_array_fromarray(sky_object_t self, sky_object_t other)
{
    const sky_array_type_t  *other_type, *self_type;

    /* array.extend() only allows extension with other arrays of the same type,
     * so we need this special function to be used by array.__new__() to
     * initialize the array with another array of any type. This is just a
     * wrapper around array.extend() that uses an iter(other) for the latter
     * case.
     */

    self_type = sky_array_array_data(self)->type;
    other_type = sky_array_array_data(other)->type;
    if (self_type->typecode == other_type->typecode) {
        sky_array_array_extendwitharray(self, other);
    }
    else {
        sky_array_array_extend(self, sky_object_iter(other));
    }
}

static void
sky_array_array_frombytes(sky_object_t self, sky_object_t bytestring)
{
    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 new_len, other_len, self_len;
        sky_buffer_t            buffer;
        sky_array_array_table_t *table;

        sky_buffer_acquire(&buffer, bytestring, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.itemsize != 1) {
            sky_error_raise_string(sky_TypeError,
                                   "string/buffer of bytes required.");
        }
        if (buffer.len % type->itemsize) {
            sky_error_raise_string(sky_ValueError,
                                   "string length not a multiple of item size");
        }
        other_len = buffer.len / type->itemsize;

        table = sky_array_array_table(self, SKY_TRUE);
        self_len = table->len;

        if (SSIZE_MAX - self_len < other_len) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        new_len = self_len + other_len;
        table = sky_array_array_resize(self, new_len);

        memcpy(table->data + (self_len * type->itemsize),
               buffer.buf,
               other_len * type->itemsize);
        sky_error_validate_debug(self_len + other_len <= table->capacity);
        table->len = self_len + other_len;
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_frombytes_doc[] =
"frombytes(bytestring)\n\n"
"Appends items from the string, interpreting it as an array of machine\n"
"values, as if it had been read from a file using the fromfile() method).";

static void
sky_array_array_fromfile(sky_object_t self, sky_object_t f, ssize_t n)
{
    const sky_array_type_t  *type = sky_array_array_data(self)->type;

    ssize_t     nbytes;
    sky_bytes_t bytes;

    if (n < 0) {
        sky_error_raise_string(sky_ValueError, "negative count");
    }
    if ((size_t)n > SSIZE_MAX / type->itemsize) {
        sky_error_raise_object(sky_MemoryError, sky_None);
    }
    nbytes = n * type->itemsize;

    bytes = sky_file_read(f, nbytes);
    if (!sky_object_isa(bytes, sky_bytes_type)) {
        sky_error_raise_string(sky_TypeError, "read() didn't return bytes");
    }

    sky_array_array_frombytes(self, bytes);
    if (sky_object_len(bytes) != nbytes) {
        sky_error_raise_string(sky_EOFError,
                               "read() didn't return enough bytes");
    }
}

static const char sky_array_array_fromfile_doc[] =
"fromfile(f, n)\n\n"
"Read n objects from the file object f and append them to the end of the\n"
"array.";

static void
sky_array_array_fromlist(sky_object_t self, sky_list_t list)
    /* This is mostly the same as .extend(), except that it works only with
     * a list, and it will restore the table's original length if an error is
     * raised while extending.
     */
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);
        const sky_array_type_t  *type = self_data->type;

        ssize_t                 capacity, hint, len, start;
        sky_object_t            item;
        sky_hazard_pointer_t    *hazard;
        sky_array_array_table_t *new_table, *table;

        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (!(table = hazard->pointer)) {
            table = &sky_array_array_table_empty;
        }
        len = table->len;

        hint = sky_list_len(list);
        if (self_data->buffer_count && hint > 0) {
            sky_error_raise_string(
                    sky_BufferError,
                    "cannot resize an array that is exporting buffers");
        }
        if (len + hint > table->capacity) {
            new_table = sky_array_array_table_resize(table,
                                                     type,
                                                     len + hint);
            sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));
            sky_error_validate_debug(hazard->pointer == new_table);
            table = sky_array_array_data_settable(
                            self_data,
                            new_table,
                            sky_array_array_isshared(self));
        }
        capacity = table->capacity;
        sky_error_validate_debug(len <= capacity);

        start = len;
        SKY_ERROR_TRY {
            SKY_SEQUENCE_FOREACH(list, item) {
                if (len >= capacity) {
                    sky_error_validate_debug(len <= table->capacity);
                    table->len = len;
                    new_table = sky_array_array_table_resize(
                                        table,
                                        type,
                                        capacity << 1);
                    sky_hazard_pointer_update(hazard,
                                              SKY_AS_VOIDP(&new_table));
                    sky_error_validate_debug(hazard->pointer == new_table);
                    table = sky_array_array_data_settable(
                                    self_data,
                                    new_table,
                                    sky_array_array_isshared(self));
                    capacity = table->capacity;
                }
                sky_array_array_table_setitem(table, type, len++, item);
            } SKY_SEQUENCE_FOREACH_END;
        } SKY_ERROR_EXCEPT_ANY {
            sky_error_validate_debug(start <= table->capacity);
            table->len = start;
            sky_error_raise();
        } SKY_ERROR_TRY_END;
        sky_error_validate_debug(len <= table->capacity);
        table->len = len;
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_fromlist_doc[] =
"fromlist(list)\n\n"
"Append items to array from list.";

static void
sky_array_array_fromstring(sky_object_t self, sky_object_t string)
{
    if (sky_object_isa(string, sky_string_type)) {
        string = sky_codec_encode(NULL, string, NULL, NULL, 0);
    }
    sky_array_array_frombytes(self, string);
}

static const char sky_array_array_fromstring_doc[] =
"fromstring(string)\n\n"
"Appends items from the string, interpreting it as an array of machine\n"
"values, as if it had been read from a file using the fromfile() method).\n"
"\n"
"This method is deprecated. Use frombytes instead.";

static void
sky_array_array_fromunicode(sky_object_t self, sky_string_t string)
{
    const sky_array_type_t  *type = sky_array_array_data(self)->type;

    if (type->typecode != 'u') {
        sky_error_raise_string(
                sky_ValueError,
                "fromunicode() may only be called on unicode type arrays");
    }

    SKY_ASSET_BLOCK_BEGIN {
        void                    *bytes;
        ssize_t                 nbytes, new_len, other_len, self_len;
        sky_array_array_table_t *table;

        bytes = sky_codec_encodetocbytes(
#if SKY_ENDIAN == SKY_ENDIAN_BIG
                        sky_codec_utf32_be,
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
                        sky_codec_utf32_le,
#else
#   error implementation missing
#endif
                        string,
                        NULL,
                        NULL,
                        0,
                        &nbytes);
        sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        other_len = nbytes / type->itemsize;

        table = sky_array_array_table(self, SKY_TRUE);
        self_len = table->len;

        if (SSIZE_MAX - self_len < other_len) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        new_len = self_len + other_len;
        table = sky_array_array_resize(self, new_len);

        memcpy(table->data + (self_len * type->itemsize), bytes, nbytes);
        sky_error_validate_debug(self_len + other_len <= table->capacity);
        table->len = self_len + other_len;
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_fromunicode_doc[] =
"fromunicode(ustr)\n\n"
"Extends this array with data from the unicode string ustr.\n"
"The array msut be a unicode type array; otherwise a ValueError\n"
"is raised.  Use array.frombytes(ustr.encode(...)) to\n"
"append Unicode data to an array of some other type.";

static sky_object_t
sky_array_array_ge(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}

static sky_object_t
sky_array_array_getitem(sky_object_t self, sky_object_t item)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 i, j, len, start, step, stop, table_len;
        sky_array_array_data_t  *result_data;
        sky_array_array_table_t *slice_table, *table;

        table = sky_array_array_table(self, SKY_TRUE);
        table_len = table->len;

        if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            i = sky_integer_value_clamped(sky_number_index(item),
                                          SSIZE_MIN, SSIZE_MAX);
            if (i < 0) {
                i += table_len;
            }
            if (i < 0 || i >= table_len) {
                sky_error_raise_string(sky_IndexError,
                                       "array index out of range");
            }
            result = sky_array_array_table_getitem(table, type, i);
        }
        else if (sky_object_isa(item, sky_slice_type)) {
            len = sky_slice_range(item, table_len, &start, &stop, &step);
            slice_table = sky_array_array_table_resize(NULL, type, len);
            slice_table->len = len;
            if (1 == step) {
                memcpy(slice_table->data,
                       table->data + (start * type->itemsize),
                       len * type->itemsize);
            }
            else {
                switch (type->itemsize) {
                    case 1:
                        for (i = start, j = 0; j < len; i += step, ++j) {
                            ((uint8_t *)slice_table->data)[j] =
                                    ((uint8_t *)table->data)[i];
                        }
                        break;
                    case 2:
                        for (i = start, j = 0; j < len; i += step, ++j) {
                            ((uint16_t *)slice_table->data)[j] =
                                    ((uint16_t *)table->data)[i];
                        }
                        break;
                    case 4:
                        for (i = start, j = 0; j < len; i += step, ++j) {
                            ((uint32_t *)slice_table->data)[j] =
                                    ((uint32_t *)table->data)[i];
                        }
                        break;
                    case 8:
                        for (i = start, j = 0; j < len; i += step, ++j) {
                            ((uint64_t *)slice_table->data)[j] =
                                    ((uint64_t *)table->data)[i];
                        }
                        break;
                    default:
                        for (i = start, j = 0; j < len; i += step, ++j) {
                            memcpy(slice_table->data + (j * type->itemsize),
                                   table->data + (i * type->itemsize),
                                   type->itemsize);
                        }
                        break;
                }
            }
            result = sky_object_allocate(sky_array_array_type);
            result_data = sky_array_array_data(result);
            result_data->type = type;
            sky_array_array_data_settable(result_data,
                                                 slice_table,
                                                 SKY_FALSE);
        }
        else {
            sky_error_raise_string(sky_TypeError,
                                   "array indices must be integers");
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_array_array_gt(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_GREATER);
}

static sky_object_t
sky_array_array_iadd(sky_object_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_array_array_type)) {
        sky_error_raise_format(sky_TypeError,
                               "can only extend array with array (not %#@)",
                               sky_type_name(sky_object_type(other)));
    }
    sky_array_array_extend(self, other);
    return self;
}

static sky_object_t
sky_array_array_imul(sky_object_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(
                sky_TypeError,
                "can't multiply array by non-int of type %#@",
                sky_type_name(sky_object_type(other)));
    }
    count = sky_integer_value(sky_number_index(other),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);

    return sky_array_array_repeat(self, count, SKY_TRUE);
}

static ssize_t
sky_array_array_index(sky_object_t self, sky_object_t item)
{
    ssize_t result = -1;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_FALSE);
        if ((len = table->len) > 0) {
            ssize_t i;

            for (i = 0; i < len; ++i) {
                sky_object_t    object;

                object = sky_array_array_table_getitem(table,
                                                       self_data->type,
                                                       i);
                if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
                    result = i;
                    break;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    if (result < 0) {
        sky_error_raise_string(sky_ValueError, "array.index(x): x not in list");
    }
    return result;
}

static const char sky_array_array_index_doc[] =
"index(x)\n\n"
"Return index of first occurrence of x in the array.";

static void
sky_array_array_insert(sky_object_t self, ssize_t index, sky_object_t item)
{
    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        len = table->len;

        if (index < 0) {
            index += len;
            if (index < 0) {
                index = 0;
            }
        }
        if (index > len) {
            index = len;
        }
        table = sky_array_array_resize(self, table->capacity << 1);
        sky_error_validate_debug(index <= table->capacity);

        memmove(table->data + ((index + 1) * type->itemsize),
                table->data + (index * type->itemsize),
                (len - index) * type->itemsize);
        sky_array_array_table_setitem(table, type, index, item);
        sky_error_validate_debug(len + 1 <= table->capacity);
        table->len = ++len;
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_insert_doc[] =
"insert(i,x)\n\n"
"Insert a new item x into the array before position i.";

static sky_object_t
sky_array_array_itemsize_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    return sky_integer_createfromunsigned(self_data->type->itemsize);
}

static sky_object_t
sky_array_array_iter(sky_object_t self)
{
    sky_object_t                    iterator;
    sky_array_array_iterator_data_t *iterator_data;

    iterator = sky_object_allocate(sky_array_array_iterator_type);
    iterator_data = sky_array_array_iterator_data(iterator);
    sky_object_gc_set(&(iterator_data->array), self, iterator);

    return iterator;
}

static sky_object_t
sky_array_array_le(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}

static ssize_t
sky_array_array_len(sky_object_t self)
{
    ssize_t len;

    SKY_ASSET_BLOCK_BEGIN {
        len = sky_array_array_table(self, SKY_TRUE)->len;
    } SKY_ASSET_BLOCK_END;

    return len;
}

static sky_object_t
sky_array_array_lt(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_LESS);
}

static sky_object_t
sky_array_array_mul(sky_object_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(
                sky_TypeError,
                "can't multiply sequence by non-int of type %#@",
                sky_type_name(sky_object_type(other)));
    }
    count = sky_integer_value(sky_number_index(other),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    return sky_array_array_repeat(self, count, SKY_FALSE);
}

static sky_object_t
sky_array_array_ne(sky_object_t self, sky_object_t other)
{
    return sky_array_array_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}

static sky_object_t
sky_array_array_new(sky_type_t          cls,
                    sky_unicode_char_t  typecode,
                    sky_object_t        initializer,
                    sky_dict_t          kws)
{
    size_t                  i;
    sky_object_t            self;
    const sky_array_type_t  *type;
    sky_array_array_data_t  *self_data;

    if (sky_array_array_type == cls && sky_object_bool(kws)) {
        sky_error_raise_string(sky_TypeError,
                               "array.array() does not take keyword arguments");
    }

    type = NULL;
    for (i = 0; i < sky_array_types_count; ++i) {
        if (sky_array_types[i].typecode == typecode) {
            type = &(sky_array_types[i]);
            break;
        }
    }
    if (!type) {
        sky_error_raise_string(sky_ValueError, "bad typecode");
    }

    self = sky_object_allocate(cls);
    self_data = sky_array_array_data(self);
    self_data->type = type;

    if (initializer) {
        if (sky_object_isa(initializer, sky_list_type)) {
            sky_array_array_fromlist(self, initializer);
        }
        else if (sky_object_isa(initializer, sky_string_type) &&
                 'u' == typecode)
        {
            sky_array_array_fromunicode(self, initializer);
        }
        else if (sky_object_isa(initializer, sky_array_array_type)) {
            sky_array_array_fromarray(self, initializer);
        }
        else if (sky_buffer_check(initializer)) {
            sky_array_array_frombytes(self, initializer);
        }
        else {
            sky_array_array_extend(self, initializer);
        }
    }

    return self;
}

static sky_object_t
sky_array_array_pop(sky_object_t self, ssize_t index)
{
    sky_object_t    item;

    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        if (!(len = table->len)) {
            sky_error_raise_string(sky_IndexError, "pop from empty array");
        }

        index = sky_array_index_normalize(index, len, "pop");
        item = sky_array_array_table_getitem(table, type, index);
        table = sky_array_array_resize(self, len - 1);
        sky_error_validate_debug(len - 1 <= table->capacity);
        table->len = len - 1;

        memmove(table->data + (index * type->itemsize),
                table->data + ((index + 1) * type->itemsize),
                (len - 1 - index) * type->itemsize);
    } SKY_ASSET_BLOCK_END;

    return item;
}

static const char sky_array_array_pop_doc[] =
"pop([i])\n\n"
"Return the i-th element and delete it from the array. i defaults to -1.";

static sky_object_t
sky_array_array_reconstructor(
                sky_type_t          cls,
                sky_unicode_char_t  typecode,
                int                 mformat_code,
                sky_bytes_t         items)
{
    size_t                          i;
    sky_object_t                    converted_items;
    sky_string_t                    encoding;
    const sky_array_type_t          *type;
    const sky_array_mformat_type_t  *mformat_type;

    if (!sky_type_issubtype(cls, sky_array_array_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%@ is not a subtype of %@",
                               sky_type_name(cls),
                               sky_type_name(sky_array_array_type));
    }

    type = NULL;
    for (i = 0; i < sky_array_types_count; ++i) {
        if (sky_array_types[i].typecode == typecode) {
            type = &(sky_array_types[i]);
            break;
        }
    }
    if (!type) {
        sky_error_raise_string(sky_ValueError,
                               "second argument must be a valid type code");
    }

    if (mformat_code < SKY_ARRAY_MFORMAT_CODE_MIN ||
        mformat_code > SKY_ARRAY_MFORMAT_CODE_MAX)
    {
        sky_error_raise_string(
                sky_ValueError,
                "third argument must be a valid machine format code.");
    }

    /* Fast path: No decoding has to be done. */
    if (sky_array_mformat_code(type) == mformat_code ||
        SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT == mformat_code)
    {
        return sky_array_array_new(cls, typecode, items, NULL);
    }

    /* Slow path: Decode the byte string according to the given machine
     * format code. This occurs when the computer unpickling the array
     * object is architecturally different from the one that pickled the
     * array.
     */

    mformat_type = &(sky_array_mformat_types[mformat_code]);
    if (sky_object_len(items) % mformat_type->size != 0) {
        sky_error_raise_string(sky_ValueError,
                               "string length not a multiple of item size");
    }

    switch (mformat_code) {
        case SKY_ARRAY_MFORMAT_CODE_UTF16_LE:
            encoding = SKY_STRING_LITERAL("utf-16-le");
            break;
        case SKY_ARRAY_MFORMAT_CODE_UTF16_BE:
            encoding = SKY_STRING_LITERAL("utf-16-be");
            break;
        case SKY_ARRAY_MFORMAT_CODE_UTF32_LE:
            encoding = SKY_STRING_LITERAL("utf-32-le");
            break;
        case SKY_ARRAY_MFORMAT_CODE_UTF32_BE:
            encoding = SKY_STRING_LITERAL("utf-32-be");
            break;
        default:
            encoding = NULL;
            break;
    }
    if (encoding) {
        return sky_array_array_new(cls,
                                   typecode,
                                   sky_codec_decode(NULL,
                                                    encoding,
                                                    NULL,
                                                    NULL,
                                                    items),
                                   NULL);
    }

    SKY_ASSET_BLOCK_BEGIN {
        int             endian;
        size_t          nitems;
        ssize_t         nbytes;
        sky_buffer_t    buffer;
        const uint8_t   *bytes;

        sky_buffer_acquire(&buffer, items, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nbytes = buffer.len;
        nitems = nbytes / mformat_type->size;
        bytes = buffer.buf;
        converted_items = sky_list_createwithcapacity(nitems);

        if (mformat_type->is_big_endian) {
            endian = SKY_ENDIAN_BIG;
        }
        else {
            endian = SKY_ENDIAN_LITTLE;
        }

        switch (mformat_code) {
            case SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_LE:
            case SKY_ARRAY_MFORMAT_CODE_IEEE_754_FLOAT_BE:
                for (i = 0; i < nitems; ++i) {
                    float   value;

                    if (!(bytes = sky_util_float_decode(bytes,
                                                        nbytes,
                                                        endian,
                                                        &value)))
                    {
                        sky_error_raise_string(
                                sky_ValueError,
                                "cannot convert IEEE 754 float");
                    }
                    nbytes -= 4;
                    sky_list_append(converted_items, sky_float_create(value));
                }
                break;

            case SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_LE:
            case SKY_ARRAY_MFORMAT_CODE_IEEE_754_DOUBLE_BE:
                for (i = 0; i < nitems; ++i) {
                    double  value;

                    if (!(bytes = sky_util_double_decode(bytes,
                                                         nbytes,
                                                         endian,
                                                         &value)))
                    {
                        sky_error_raise_string(
                                sky_ValueError,
                                "cannot convert IEEE 754 float");
                    }
                    nbytes -= 8;
                    sky_list_append(converted_items, sky_float_create(value));
                }
                break;

            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT8:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT8:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT16_LE:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT16_BE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT16_LE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT16_BE:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT32_LE:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT32_BE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT32_LE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT32_BE:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT64_LE:
            case SKY_ARRAY_MFORMAT_CODE_UNSIGNED_INT64_BE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT64_LE:
            case SKY_ARRAY_MFORMAT_CODE_SIGNED_INT64_BE:
                /* If possible, try to pack array's items using a data type
                 * that fits better. This may result in an array with narrower
                 * or wider elements.
                 *
                 * For example, if a 32-bit machine pickles a L-code array of
                 * unsigned longs, then the array will be unpickled by 64-bit
                 * machine as an I-code array of unsigned ints.
                 */
                for (i = 0; i < sky_array_types_count; ++i) {
                    type = &(sky_array_types[i]);
                    if (type->is_integer_type &&
                        type->itemsize == mformat_type->size &&
                        type->is_signed == mformat_type->is_signed)
                    {
                        typecode = type->typecode;
                        break;
                    }
                }

                for (i = 0; i < nitems; ++i) {
                    sky_list_append(
                            converted_items,
                            sky_integer_createfrombytes(
                                    bytes,
                                    mformat_type->size,
                                    !mformat_type->is_big_endian,
                                    mformat_type->is_signed));
                    bytes += mformat_type->size;
                }
                break;

            case SKY_ARRAY_MFORMAT_CODE_UTF16_LE:
            case SKY_ARRAY_MFORMAT_CODE_UTF16_BE:
            case SKY_ARRAY_MFORMAT_CODE_UTF32_LE:
            case SKY_ARRAY_MFORMAT_CODE_UTF32_BE:
            case SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT:
                sky_error_fatal("internal error");
                break;
        }
    } SKY_ASSET_BLOCK_END;

    return sky_array_array_new(cls, typecode, converted_items, NULL);
}

static sky_object_t
sky_array_array_reduce_ex(sky_object_t self, int proto)
{
    const sky_array_type_t  *type = sky_array_array_data(self)->type;

    sky_object_t                dict;
    sky_array_mformat_code_t    mformat_code;

    dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), sky_None);
    mformat_code = sky_array_mformat_code(type);

    if (SKY_ARRAY_MFORMAT_CODE_UNKNOWN_FORMAT == mformat_code ||
        proto < 3)
    {
        return sky_object_build("(O(OO)O)",
                                sky_object_type(self),
                                sky_string_createfromcodepoint(type->typecode),
                                sky_array_array_tolist(self),
                                dict);
    }

    return sky_object_build("(O(OOiO)O)",
                            sky_array__array_reconstructor,
                            sky_object_type(self),
                            sky_string_createfromcodepoint(type->typecode),
                            (int)mformat_code,
                            sky_array_array_tobytes(self),
                            dict);
}

static void
sky_array_array_remove(sky_object_t self, sky_object_t item)
{
    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 i, index, len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_FALSE);
        len = table->len;

        index = -1;
        for (i = 0; i < len; ++i) {
            sky_object_t    object;

            object = sky_array_array_table_getitem(table, type, i);
            if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
                index = i;
                break;
            }
        }
        if (-1 == index) {
            sky_error_raise_string(sky_ValueError,
                                   "array.remove(x): x not in list");
        }

        table = sky_array_array_resize(self, len - 1);
        sky_error_validate_debug(len - 1 <= table->capacity);
        table->len = len - 1;
        memmove(table->data + (index * type->itemsize),
                table->data + ((index + 1) * type->itemsize),
                (len - 1 - index) * type->itemsize);
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_remove_doc[] =
"remove(x)\n\n"
"Remove the first occurrence of x in the array.";

static sky_object_t
sky_array_array_repeat(sky_object_t self, ssize_t count, sky_bool_t inplace)
{
    const sky_array_type_t  *type = sky_array_array_data(self)->type;

    sky_object_t            target_array;
    sky_array_array_data_t  *target_data;

    if (count <= 0) {
        if (inplace) {
            sky_array_array_clear(self);
            return self;
        }
        target_array = sky_object_allocate(sky_array_array_type);
        target_data = sky_array_array_data(self);
        target_data->type = type;
        return target_array;
    }
    if (1 == count && inplace) {
        return self;
    }

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 i, source_len, target_len;
        sky_array_array_table_t *source_table, *target_table;

        source_table = sky_array_array_table(self, SKY_TRUE);
        source_len = source_table->len;

        if (source_len > SSIZE_MAX / count) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        target_len = source_len * count;
        target_table = sky_array_array_table_resize(NULL,
                                                           type,
                                                           target_len);
        sky_asset_save(target_table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < count; ++i) {
            memcpy(target_table->data + (target_table->len * type->itemsize),
                   source_table->data,
                   source_len * type->itemsize);
            target_table->len += source_len;
        }

        if (inplace) {
            target_array = self;
        }
        else {
            target_array = sky_object_allocate(sky_array_array_type);
        }
        target_data = sky_array_array_data(target_array);
        target_data->type = type;
        sky_array_array_data_settable(
                target_data,
                target_table,
                sky_array_array_isshared(target_array));
    } SKY_ASSET_BLOCK_END;

    return target_array;
}

static sky_string_t
sky_array_array_repr(sky_object_t self)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    sky_string_builder_t    builder;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 len;
        sky_array_array_table_t *table;

        builder = sky_string_builder_create();
        sky_string_builder_appendformat(builder,
                                        "array('%c'",
                                        self_data->type->typecode);

        table = sky_array_array_table(self, SKY_FALSE);
        if ((len = table->len) > 0) {
            sky_object_t    items;

            sky_string_builder_appendcodepoint(builder, ',', 1);
            sky_string_builder_appendcodepoint(builder, ' ', 1);

            if ('u' == self_data->type->typecode) {
                items = sky_array_array_tounicode(self);
            }
            else {
                items = sky_array_array_tolist(self);
            }

            sky_string_builder_append(builder, sky_object_repr(items));
        }

        sky_string_builder_appendcodepoint(builder, ')', 1);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}

static void
sky_array_array_reverse(sky_object_t self)
{
    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        if ((len = table->len) > 1) {
            if (1 == type->itemsize) {
                uint8_t *head, item, *tail;

                head = (uint8_t *)table->data;
                tail = &(((uint8_t *)table->data)[len - 1]);
                while (head < tail) {
                    item = *head;
                    *head++ = *tail;
                    *tail-- = item;
                }
            }
            else if (2 == type->itemsize) {
                uint16_t    *head, item, *tail;

                head = (uint16_t *)table->data;
                tail = &(((uint16_t *)table->data)[len - 1]);
                while (head < tail) {
                    item = *head;
                    *head++ = *tail;
                    *tail-- = item;
                }
            }
            else if (4 == type->itemsize) {
                uint32_t    *head, item, *tail;

                head = (uint32_t *)table->data;
                tail = &(((uint32_t *)table->data)[len - 1]);
                while (head < tail) {
                    item = *head;
                    *head++ = *tail;
                    *tail-- = item;
                }
            }
            else if (8 == type->itemsize) {
                uint64_t    *head, item, *tail;

                head = (uint64_t *)table->data;
                tail = &(((uint64_t *)table->data)[len - 1]);
                while (head < tail) {
                    item = *head;
                    *head++ = *tail;
                    *tail-- = item;
                }
            }
            else {
                char    buffer[256];
                char    *head, *item, *tail;

                if (type->itemsize <= sizeof(buffer)) {
                    item = buffer;
                }
                else {
                    item = sky_asset_malloc(type->itemsize,
                                            SKY_ASSET_CLEANUP_ALWAYS);
                }
                head = table->data;
                tail = table->data + ((len - 1) * type->itemsize);
                while (head < tail) {
                    memcpy(item, head, type->itemsize);
                    memcpy(head, tail, type->itemsize);
                    memcpy(tail, item, type->itemsize);
                    head += type->itemsize;
                    tail -= type->itemsize;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_reverse_doc[] =
"reverse()\n\n"
"Reverse the order of the items in the array.";

static sky_array_array_table_t *
sky_array_array_setitem_slice(
                sky_object_t                self,
                sky_array_array_table_t *   table,
                ssize_t                     table_len,
                ssize_t                     start,
                ssize_t                     stop,
                sky_object_t                value)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);
    const sky_array_type_t  *type = self_data->type;

    ssize_t delta, i;

    /* value must be either NULL or an array of the same type as self */
    sky_error_validate_debug(
            !value ||
            (sky_object_isa(value, sky_array_array_type) &&
             sky_array_array_data(value)->type == type));

    if (self == value) {
        /* Special case "self[start:stop] = self" */
        value = sky_array_array_copy(value);
    }

    if (start < 0) {
        start = 0;
    }
    else if (start > table_len) {
        start = table_len;
    }
    if (stop < start) {
        stop = start;
    }
    else if (stop > table_len) {
        stop = table_len;
    }

    if (!value) {
        delta = -(stop - start);
    }
    else {
        delta = sky_object_len(value) - (stop - start);
    }
    if (!(table_len + delta)) {
        sky_array_array_clear(self);
        return table;
    }
    if (self_data->buffer_count) {
        sky_error_raise_string(
                sky_BufferError,
                "cannot resize an array that is exporting buffers");
    }

#define delete_loop(_f)                                     \
    do {                                                    \
        for (i = 0; i < table_len - stop; ++i) {            \
            sky_array_array_table_##_f(table,               \
                                       type,                \
                                       stop + delta + i,    \
                                       stop + i);           \
        }                                                   \
    } while (0)

#define insert_loop(_f)                                     \
    do {                                                    \
        for (i = table_len - stop - 1; i >= 0; --i) {       \
            sky_array_array_table_##_f(table,               \
                                       type,                \
                                       stop + delta + i,    \
                                       stop + i);           \
        }                                                   \
    } while (0)

    if (delta < 0) {
        /* Delete -delta items */
        switch (type->itemsize) {
            case 1:
                delete_loop(copyitem8);
                break;
            case 2:
                delete_loop(copyitem16);
                break;
            case 4:
                delete_loop(copyitem32);
                break;
            case 8:
                delete_loop(copyitem64);
                break;
            default:
                delete_loop(copyitem);
                break;
        }
    }
    else if (delta > 0) {
        /* Insert delta items */
        table = sky_array_array_resize(self, table_len + delta);
        switch (type->itemsize) {
            case 1:
                insert_loop(copyitem8);
                break;
            case 2:
                insert_loop(copyitem16);
                break;
            case 4:
                insert_loop(copyitem32);
                break;
            case 8:
                insert_loop(copyitem64);
                break;
            default:
                insert_loop(copyitem);
                break;
        }
    }
    sky_error_validate_debug(table_len + delta <= table->capacity);
    table->len = table_len + delta;

#undef insert_loop
#undef delete_loop

    if (value) {
        ssize_t                 j, other_len;
        sky_object_t            item;
        sky_array_array_table_t *other_table;

        other_table = sky_array_array_table(value, SKY_TRUE);
        other_len = other_table->len;

        for (i = start, j = 0; j < other_len; ++i, ++j) {
            item = sky_array_array_table_getitem(other_table, type, j);
            sky_array_array_table_setitem(table, type, i, item);
        }
    }

    return table;
}

static void
sky_array_array_setitem(
                sky_object_t    self,
                sky_object_t    item,
                sky_object_t    value)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_data_t  *self_data = sky_array_array_data(self);
        const sky_array_type_t  *type = self_data->type;

        size_t                  j, limit, x;
        ssize_t                 i, index, k, len, start, step, stop, table_len,
                                value_len;
        sky_object_t            object;
        sky_array_array_table_t *table, *value_table;

        table = sky_array_array_table(self, SKY_TRUE);
        table_len = table->len;

        if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            index = sky_integer_value_clamped(sky_number_index(item),
                                              SSIZE_MIN, SSIZE_MAX);
            index = sky_array_index_normalize(index,
                                              table_len,
                                              "array assignment");
            if (value) {
                sky_array_array_table_setitem(table, type, index, value);
            }
            else {
                table = sky_array_array_resize(self, table_len - 1);
                sky_error_validate_debug(table_len - 1 <= table->capacity);
                table->len = table_len - 1;
                memmove(table->data + (index * type->itemsize),
                        table->data + ((index + 1) * type->itemsize),
                        (table_len - 1 - index) * type->itemsize);
            }
        }
        else if (sky_object_isa(item, sky_slice_type)) {
            len = sky_slice_range(item, table_len, &start, &stop, &step);
            if (value) {
                if (!sky_object_isa(value, sky_array_array_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "can only assign array (not %#@) to array slice",
                            sky_type_name(sky_object_type(value)));
                }
                if (type != sky_array_array_data(value)->type) {
                    sky_error_raise_string(
                            sky_TypeError,
                            "can only assign array of the same type to array slice");
                }
            }
            if (1 == step) {
                table = sky_array_array_setitem_slice(self,
                                                      table,
                                                      table_len,
                                                      start,
                                                      stop,
                                                      value);
            }
            else {
                if (len && self_data->buffer_count) {
                    sky_error_raise_string(
                            sky_BufferError,
                            "cannot resize an array that is exporting buffers");
                }
                if ((step < 0 && start < stop) || (step > 0 && start > stop)) {
                    stop = start;
                }
                if (!value) {   /* __delitem__ */
                    if (len > 0) {
                        if (step < 0) {
                            stop = start + 1;
                            start = stop + (step * (len - 1) - 1);
                            step = -step;
                        }

#define delitem_loop(_f)                                            \
    do {                                                            \
        for (j = start, i = 0; j < (size_t)stop; j += step, ++i) {  \
            limit = step - 1;                                       \
            if (j + step >= (size_t)table_len) {                    \
                limit = table_len - j - 1;                          \
            }                                                       \
            for (x = 0; x < limit; ++x) {                           \
                sky_array_array_table_##_f(table,                   \
                                           type,                    \
                                           j - i + x,               \
                                           j + 1 + x);              \
            }                                                       \
        }                                                           \
        j = start + ((size_t)len * step);                           \
        if (j < (size_t)table_len) {                                \
            for (x = 0; x < table_len - j; ++x) {                   \
                sky_array_array_table_##_f(table,                   \
                                           type,                    \
                                           j - len + x,             \
                                           j + x);                  \
            }                                                       \
        }                                                           \
    } while (0)
                        switch (type->itemsize) {
                            case 1:
                                delitem_loop(copyitem8);
                                break;
                            case 2:
                                delitem_loop(copyitem16);
                                break;
                            case 4:
                                delitem_loop(copyitem32);
                                break;
                            case 8:
                                delitem_loop(copyitem64);
                                break;
                            default:
                                delitem_loop(copyitem);
                                break;
                        }
#undef delitem_loop

                        sky_error_validate_debug(table_len - len <= table->capacity);
                        table->len = table_len - len;
                    }
                }
                else {          /* __setitem__ */
                    /* protect against a[::-1] = a */
                    if (self == value) {
                        value = sky_array_array_copy(value);
                    }
                    value_table = sky_array_array_table(value, SKY_TRUE);
                    value_len = value_table->len;
                    if (value_len != len) {
                        sky_error_raise_format(
                                sky_ValueError,
                                "attempt to assign sequence of size %zd ",
                                "to extended slice of size %zd",
                                value_len,
                                len);
                    }
                    if (len > 0) {
                        for (i = start, k = 0; k < value_len; i += step, ++k) {
                            object = sky_array_array_table_getitem(
                                            value_table,
                                            type,
                                            k);
                            sky_array_array_table_setitem(
                                            table,
                                            type,
                                            i,
                                            object);
                        }
                    }
                }
            }
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "list indices must be integers, not %#@",
                                   sky_type_name(sky_object_type(item)));
        }
    } SKY_ASSET_BLOCK_END;
}

static size_t
sky_array_array_sizeof(sky_object_t self)
{
    size_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        result = sky_memsize(self);
        if (table != &sky_array_array_table_empty) {
            result += sky_memsize(table);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_bytes_t
sky_array_array_tobytes(sky_object_t self)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        len = table->len;

        result = sky_bytes_createfrombytes(table->data, len * type->itemsize);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_array_array_tobytes_doc[] =
"tobytes() -> bytes\n\n"
"Convert the array to an array of machine values and return the bytes\n"
"representation.";

static void
sky_array_array_tofile(sky_object_t self, sky_object_t f)
{
    static const size_t BLOCKSIZE = 64 * 1024;

    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        size_t                  i, nblocks, nbytes;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_FALSE);

        nbytes = table->len * type->itemsize;
        nblocks = (nbytes + BLOCKSIZE - 1) / BLOCKSIZE;
        for (i = 0; i < nblocks; ++i) {
            SKY_ASSET_BLOCK_BEGIN {
                sky_memoryview_t    view;

                SKY_ASSET_BLOCK_BEGIN {
                    sky_buffer_t    *buffer;

                    buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                              SKY_ASSET_CLEANUP_ON_ERROR);
                    sky_buffer_initialize(buffer,
                                          table->data + (i * BLOCKSIZE),
                                          nbytes,
                                          SKY_TRUE,
                                          SKY_BUFFER_FLAG_CONTIG_RO);
                    view = sky_memoryview_createwithbuffer(buffer);
                } SKY_ASSET_BLOCK_END;

                sky_asset_save(view,
                               (sky_free_t)sky_memoryview_release,
                               SKY_ASSET_CLEANUP_ALWAYS);
                sky_file_write(f, view);
            } SKY_ASSET_BLOCK_END;
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_array_array_tofile_doc[] =
"tofile(f)\n\n"
"Write all items (as machine values) to the file object f.";

static sky_list_t
sky_array_array_tolist(sky_object_t self)
{
    sky_list_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        const sky_array_type_t  *type = sky_array_array_data(self)->type;

        ssize_t                 i, len;
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        len = table->len;

        result = sky_list_createwithcapacity(len);
        for (i = 0; i < len; ++i) {
            sky_list_append(
                    result,
                    sky_array_array_table_getitem(table, type, i));
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_array_array_tolist_doc[] =
"tolist() -> list\n\n"
"Convert array to an ordinary list with the same items.";

static sky_bytes_t
sky_array_array_tostring(sky_object_t self)
{
    return sky_array_array_tobytes(self);
}

static const char sky_array_array_tostring_doc[] =
"tostring() -> bytes\n\n"
"Convert the array to an array of machine values and return the bytes\n"
"representation.\n"
"\n"
"This method is deprecated. Use tobytes instead.";

static sky_string_t
sky_array_array_tounicode(sky_object_t self)
{
    const sky_array_type_t  *type = sky_array_array_data(self)->type;

    sky_string_t    result;

    if (type->typecode != 'u') {
        sky_error_raise_string(
                sky_ValueError,
                "tounicode() may only be called on unicode type arrays");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_array_array_table_t *table;

        table = sky_array_array_table(self, SKY_TRUE);
        result = sky_string_createfrombytes(table->data,
                                            table->len * type->itemsize,
#if SKY_ENDIAN == SKY_ENDIAN_BIG
                                            SKY_STRING_LITERAL("utf-32-be"),
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
                                            SKY_STRING_LITERAL("utf-32-le"),
#else
#   error implementation missing
#endif
                                            NULL);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_array_array_tounicode_doc[] =
"tounicode() -> unicode\n\n"
"Convert the array to a unicode string.  The array must be\n"
"a unicode type array; otherwise a ValueError is raised.  Use\n"
"array.tobytes().decode() to obtain a unicode string from\n"
"an array of some other type.";

static sky_object_t
sky_array_array_typecode_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_array_array_data_t  *self_data = sky_array_array_data(self);

    return sky_string_createfromcodepoint(self_data->type->typecode);
}


static sky_object_t
sky_array_array_iterator_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_array_array_iterator_length_hint(sky_object_t self)
{
    sky_array_array_iterator_data_t *self_data =
                                    sky_array_array_iterator_data(self);

    ssize_t result = 0;

    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t            array;
        sky_array_array_table_t *table;

        if ((array = self_data->array) != NULL) {
            table = sky_array_array_table(array, SKY_TRUE);
            result = table->len - self_data->next_index;
        }
    } SKY_ASSET_BLOCK_END;

    return (result > 0 ? sky_integer_create(result) : sky_integer_zero);
}

static sky_object_t
sky_array_array_iterator_next(sky_object_t self)
{
    sky_array_array_iterator_data_t *self_data =
                                    sky_array_array_iterator_data(self);

    sky_object_t    array, result;

    if (!(array = self_data->array)) {
        return NULL;
    }

    result = NULL;
    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 next_index;
        const sky_array_type_t  *type;
        sky_array_array_table_t *table;

        table = sky_array_array_table(array, SKY_TRUE);
        if ((next_index = self_data->next_index) < table->len) {
            type = sky_array_array_data(array)->type;
            result = sky_array_array_table_getitem(table,
                                                   type,
                                                   next_index);
            self_data->next_index = next_index + 1;
        }
    } SKY_ASSET_BLOCK_END;

    if (!result) {
        self_data->array = NULL;
    }
    return result;
}

static sky_object_t
sky_array_array_iterator_reduce(sky_object_t self)
{
    sky_array_array_iterator_data_t *self_data =
                                    sky_array_array_iterator_data(self);

    sky_object_t    array;

    if (!(array = self_data->array)) {
        return sky_object_build("(O([]))", sky_module_getbuiltin("iter"));
    }

    return sky_object_build("(O(O)iz)",
                            sky_module_getbuiltin("iter"),
                            array,
                            self_data->next_index);
}

static void
sky_array_array_iterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_array_array_iterator_data_t *self_data =
                                    sky_array_array_iterator_data(self);

    self_data->next_index = sky_integer_value(sky_number_index(state),
                                              SSIZE_MIN, SSIZE_MAX,
                                              NULL);
    if (self_data->next_index < 0) {
        self_data->next_index = 0;
    }
    else if (self_data->next_index > sky_object_len(self_data->array)) {
        self_data->next_index = sky_object_len(self_data->array);
    }
}


const char *skython_module_array_doc =
"This module defines an object type which can efficiently represent\n"
"an array of basic values: characters, integers, floating point\n"
"numbers.  Arrays are sequence types and behave very much like lists,\n"
"except that the type of objects stored in them is constrained.  The\n"
"type is specified at object creation time by using a type code, which\n"
"is a single character.  The following type codes are defined:\n"
"\n"
"    Type code   C Type             Minimum size in bytes \n"
"    'b'         signed integer     1 \n"
"    'B'         unsigned integer   1 \n"
"    'u'         Unicode character  2 (see note) \n"
"    'h'         signed integer     2 \n"
"    'H'         unsigned integer   2 \n"
"    'i'         signed integer     2 \n"
"    'I'         unsigned integer   2 \n"
"    'l'         signed integer     4 \n"
"    'L'         unsigned integer   4 \n"
"    'q'         signed integer     8 (see note) \n"
"    'Q'         unsigned integer   8 (see note) \n"
"    'f'         floating point     4 \n"
"    'd'         floating point     8 \n"
"\n"
"NOTE: The 'u' typecode corresponds to Python's unicode character. On \n"
"narrow builds this is 2-bytes on wide builds this is 4-bytes.\n"
"\n"
"NOTE: The 'q' and 'Q' type codes are only available if the platform \n"
"C compiler used to build Python supports 'long long', or, on Windows, \n"
"'__int64'.\n"
"\n"
"The constructor is:\n"
"\n"
"array(typecode [, initializer]) -- create a new array\n";


void
skython_module_array_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_string_t        typecodes;
    sky_function_t      f;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky_array_array_instance_finalize;
    template.buffer_acquire = sky_array_array_instance_buffer_acquire;
    template.buffer_release = sky_array_array_instance_buffer_release;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("array"),
                                   sky_array_array_type_doc,
                                   0,
                                   sizeof(sky_array_array_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setgetsets(type,
            "itemsize", "the size, in bytes, of one array item",
                        sky_array_array_itemsize_getter,
                        NULL,
            "typecode", "the typecode character used to create the array",
                        sky_array_array_typecode_getter,
                        NULL,
            NULL);

    sky_type_setattr_builtin(type, "__hash__", sky_None);
    sky_type_setmethodslotwithparameters(
            type,
            "__new__",
            (sky_native_code_function_t)sky_array_array_new,
            "cls", SKY_DATA_TYPE_OBJECT, NULL,
            "typecode", SKY_DATA_TYPE_CODEPOINT, NULL,
            "initializer", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
            NULL);
    sky_type_setmethodslots(type,
            "__repr__", sky_array_array_repr,
            "__lt__", sky_array_array_lt,
            "__le__", sky_array_array_le,
            "__eq__", sky_array_array_eq,
            "__ne__", sky_array_array_ne,
            "__gt__", sky_array_array_gt,
            "__ge__", sky_array_array_ge,
            "__sizeof__", sky_array_array_sizeof,
            "__len__", sky_array_array_len,
            "__getitem__", sky_array_array_getitem,
            "__setitem__", sky_array_array_setitem,
            "__delitem__", sky_array_array_delitem,
            "__iter__", sky_array_array_iter,
            "__contains__", sky_array_array_contains,
            "__add__", sky_array_array_add,
            "__mul__", sky_array_array_mul,
            "__rmul__", sky_array_array_mul,
            "__iadd__", sky_array_array_iadd,
            "__imul__", sky_array_array_imul,
            "__reduce_ex__", sky_array_array_reduce_ex,
            NULL);

    sky_type_setattr_builtin(
            type,
            "append",
            sky_function_createbuiltin(
                    "array.append",
                    sky_array_array_append_doc,
                    (sky_native_code_function_t)sky_array_array_append,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "buffer_info",
            sky_function_createbuiltin(
                    "array.buffer_info",
                    sky_array_array_buffer_info_doc,
                    (sky_native_code_function_t)sky_array_array_buffer_info,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "byteswap",
            sky_function_createbuiltin(
                    "array.byteswap",
                    sky_array_array_byteswap_doc,
                    (sky_native_code_function_t)sky_array_array_byteswap,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "__copy__",
            sky_function_createbuiltin(
                    "array.__copy__",
                    sky_array_array_copy_doc,
                    (sky_native_code_function_t)sky_array_array_copy,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "count",
            sky_function_createbuiltin(
                    "array.count",
                    sky_array_array_count_doc,
                    (sky_native_code_function_t)sky_array_array_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "__deepcopy__",
            sky_function_createbuiltin(
                    "array.__deepcopy__",
                    sky_array_array_copy_doc,
                    (sky_native_code_function_t)sky_array_array_deepcopy,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "memo", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "extend",
            sky_function_createbuiltin(
                    "array.extend",
                    sky_array_array_extend_doc,
                    (sky_native_code_function_t)sky_array_array_extend,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "sequence", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "frombytes",
            sky_function_createbuiltin(
                    "array.frombytes",
                    sky_array_array_frombytes_doc,
                    (sky_native_code_function_t)sky_array_array_frombytes,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "bytestring", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "fromfile",
            sky_function_createbuiltin(
                    "array.fromfile",
                    sky_array_array_fromfile_doc,
                    (sky_native_code_function_t)sky_array_array_fromfile,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "f", SKY_DATA_TYPE_OBJECT, NULL,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "fromlist",
            sky_function_createbuiltin(
                    "array.fromlist",
                    sky_array_array_fromlist_doc,
                    (sky_native_code_function_t)sky_array_array_fromlist,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "list", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    NULL));
    f = sky_function_createbuiltin(
                "array.fromstring",
                sky_array_array_fromstring_doc,
                (sky_native_code_function_t)sky_array_array_fromstring,
                SKY_DATA_TYPE_VOID,
                "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                "string", SKY_DATA_TYPE_OBJECT, NULL,
                NULL);
    sky_function_setdeprecated(
            f,
            "fromstring() is deprecated. Use frombytes() instead.");
    sky_type_setattr_builtin(type, "fromstring", f);
    sky_type_setattr_builtin(
            type,
            "fromunicode",
            sky_function_createbuiltin(
                    "array.fromunicode",
                    sky_array_array_fromunicode_doc,
                    (sky_native_code_function_t)sky_array_array_fromunicode,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "ustr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "index",
            sky_function_createbuiltin(
                    "array.index",
                    sky_array_array_index_doc,
                    (sky_native_code_function_t)sky_array_array_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "insert",
            sky_function_createbuiltin(
                    "array.insert",
                    sky_array_array_insert_doc,
                    (sky_native_code_function_t)sky_array_array_insert,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "i", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, NULL,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "pop",
            sky_function_createbuiltin(
                    "array.pop",
                    sky_array_array_pop_doc,
                    (sky_native_code_function_t)sky_array_array_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "i", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "remove",
            sky_function_createbuiltin(
                    "array.remove",
                    sky_array_array_remove_doc,
                    (sky_native_code_function_t)sky_array_array_remove,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "reverse",
            sky_function_createbuiltin(
                    "array.reverse",
                    sky_array_array_reverse_doc,
                    (sky_native_code_function_t)sky_array_array_reverse,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "tobytes",
            sky_function_createbuiltin(
                    "array.tobytes",
                    sky_array_array_tobytes_doc,
                    (sky_native_code_function_t)sky_array_array_tobytes,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "tofile",
            sky_function_createbuiltin(
                    "array.tofile",
                    sky_array_array_tofile_doc,
                    (sky_native_code_function_t)sky_array_array_tofile,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "f", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "tolist",
            sky_function_createbuiltin(
                    "array.tolist",
                    sky_array_array_tolist_doc,
                    (sky_native_code_function_t)sky_array_array_tolist,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    f = sky_function_createbuiltin(
                "array.tostring",
                sky_array_array_tostring_doc,
                (sky_native_code_function_t)sky_array_array_tostring,
                SKY_DATA_TYPE_OBJECT_BYTES,
                "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                NULL);
    sky_function_setdeprecated(
            f,
            "tostring() is deprecated. Use tobytes() instead.");
    sky_type_setattr_builtin(type, "tostring", f);
    sky_type_setattr_builtin(
            type,
            "tounicode",
            sky_function_createbuiltin(
                    "array.tounicode",
                    sky_array_array_tounicode_doc,
                    (sky_native_code_function_t)sky_array_array_tounicode,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_module_setattr(module, "ArrayType", type);
    sky_module_setattr(module, "array", type);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_array_array_type),
            type,
            SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_array_array_iterator_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("arrayiterator"),
                    NULL,
                    0,
                    sizeof(sky_array_array_iterator_data_t),
                    0,
                    NULL,
                    &template);

    sky_type_setmethodslots(type,
            "__length_hint__", sky_array_array_iterator_length_hint,
            "__iter__", sky_array_array_iterator_iter,
            "__next__", sky_array_array_iterator_next,
            "__reduce__", sky_array_array_iterator_reduce,
            "__setstate__", sky_array_array_iterator_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_array_array_iterator_type),
            type,
            SKY_TRUE);

    sky_array__array_reconstructor =
            sky_function_createbuiltin(
                    "_array_reconstructor",
                    "Internal. Used for pickling support.",
                    (sky_native_code_function_t)sky_array_array_reconstructor,
                    SKY_DATA_TYPE_OBJECT,
                    "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    "typecode", SKY_DATA_TYPE_CODEPOINT, NULL,
                    "mformat_code", SKY_DATA_TYPE_INT, NULL,
                    "items", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL);
    sky_module_setattr(module,
                       "_array_reconstructor",
                       sky_array__array_reconstructor);

    SKY_ASSET_BLOCK_BEGIN {
        char    *bytes;
        size_t  i;

        bytes = sky_asset_malloc(sky_array_types_count,
                                 SKY_ASSET_CLEANUP_ALWAYS);
        for (i = 0; i < sky_array_types_count; ++i) {
            bytes[i] = sky_array_types[i].typecode;
        }

        typecodes = sky_string_createfrombytes(bytes,
                                               sky_array_types_count,
                                               SKY_STRING_LITERAL("ASCII"),
                                               NULL);
    } SKY_ASSET_BLOCK_END;
    sky_module_setattr(module, "typecodes", typecodes);
}
