#include "../core/sky_private.h"
#include "../core/sky_code_private.h"
#include "../core/sky_module_private.h"

#include "../core/sky_marshal.h"

#include "builtin_modules.h"


static sky_builtin_module_t *
sky__imp_find_builtin(sky_string_t name)
{
    const char              *cstring;
    sky_builtin_module_t    *builtin;

    if ((cstring = sky_string_cstring(name)) != NULL) {
        for (builtin = sky_builtin_modules; builtin->name; ++builtin) {
            if (!strcmp(builtin->name, cstring)) {
                return builtin;
            }
        }
    }

    return NULL;
}


sky_list_t
sky__imp_extension_suffixes(void)
{
#if defined(NDEBUG)
    const char  *abiflags = "mu";
#else
    const char  *abiflags = "dmu";
#endif

    sky_list_t  suffixes;

    suffixes = sky_list_create(NULL);
#if defined(_WIN32)
    sky_list_append(suffixes, SKY_STRING_LITERAL(".dll"));
#elif defined(HAVE_DLFCN_H)
    sky_list_append(suffixes,
                    sky_string_createfromformat(".skython-%d%d%s.so",
                                                SKYTHON_VERSION_MAJOR,
                                                SKYTHON_VERSION_MINOR,
                                                abiflags));
    sky_list_append(suffixes,
                    sky_string_createfromformat(".abi%d.so",
                                                SKYTHON_ABI_VERSION));
    sky_list_append(suffixes, SKY_STRING_LITERAL(".so"));
#endif

    return suffixes;
}


void
sky__imp__fix_co_filename_worker(
                sky_code_t      code,
                sky_string_t    old_filename,
                sky_string_t    new_filename)
{
    sky_code_data_t *code_data = sky_code_data(code);

    sky_object_t    object;

    if (sky_object_compare(code_data->co_filename,
                           old_filename,
                           SKY_COMPARE_OP_EQUAL))
    {
        return;
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(code_data->co_filename)),
                      new_filename,
                      code);
    SKY_SEQUENCE_FOREACH(code_data->co_consts, object) {
        if (sky_object_isa(object, sky_code_type)) {
            sky__imp__fix_co_filename_worker(object,
                                             old_filename,
                                             new_filename);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


void
sky__imp__fix_co_filename(sky_code_t code, sky_string_t filename)
{
    sky_code_data_t *code_data = sky_code_data(code);

    sky__imp__fix_co_filename_worker(code,
                                     code_data->co_filename,
                                     filename);
}


sky_module_t
sky__imp_get_frozen_object(sky_string_t name)
{
    sky_bytes_t             bytes;
    sky_builtin_module_t    *builtin;

    if (!(builtin = sky__imp_find_builtin(name)) || !builtin->frozen_bytes) {
        sky_error_raise_format(sky_ImportError,
                               "No such frozen object named %#@",
                               name);
    }

    bytes = sky_bytes_createwithbytes(builtin->frozen_bytes,
                                      builtin->frozen_nbytes,
                                      NULL);

    return sky_marshal_loads(bytes);
}


sky_bool_t
sky__imp_is_builtin(sky_string_t name)
{
    sky_builtin_module_t    *builtin;

    if (!(builtin = sky__imp_find_builtin(name))) {
        return SKY_FALSE;
    }
    if (builtin->frozen_bytes) {
        return SKY_FALSE;
    }

    return SKY_TRUE;
}


sky_bool_t
sky__imp_is_frozen(sky_string_t name)
{
    sky_builtin_module_t    *builtin;

    if (!(builtin = sky__imp_find_builtin(name))) {
        return SKY_FALSE;
    }
    if (!builtin->frozen_bytes) {
        return SKY_FALSE;
    }

    return SKY_TRUE;
}


sky_bool_t
sky__imp_is_frozen_package(sky_string_t name)
{
    sky_builtin_module_t    *builtin;

    if (!(builtin = sky__imp_find_builtin(name)) || !builtin->frozen_bytes) {
        sky_error_raise_format(sky_ImportError,
                               "No such frozen object named %#@",
                               name);
    }
    if (builtin->frozen_nbytes >= 0) {
        return SKY_FALSE;
    }

    return SKY_TRUE;
}


void
sky__imp_release_lock(void)
{
    if (!sky_module_import_lock_release()) {
        sky_error_raise_string(sky_RuntimeError,
                               "not holding the import lock");
    }
}


static const char sky_module_imp_doc[] =
"(Extremely low-level import machinery bits as used by importlib and imp.";

void
skython_module__imp_initialize(sky_module_t module)
{
    sky_module_setattr(module,
            "__doc__",
            sky_string_createfrombytes(sky_module_imp_doc,
                                       sizeof(sky_module_imp_doc) - 1,
                                       NULL,
                                       NULL));

    sky_module_setattr(module,
            "acquire_lock",
            sky_function_createbuiltin(
                    "acquire_lock",
                    "acquire_lock() -> None\n"
                    "Acquires the interpreter's import lock for the current thread.\n"
                    "This lock should be used by import hooks to ensure thread-safety\n"
                    "when importing modules.\n"
                    "On platforms without threads, this function does nothing.",
                    (sky_native_code_function_t)sky_module_import_lock_acquire,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(module,
            "extension_suffixes",
            sky_function_createbuiltin(
                    "extension_suffixes",
                    "extension_suffixes() -> list of strings\n"
                    "Returns the list of file suffixes used to identify extension modules.",
                    (sky_native_code_function_t)sky__imp_extension_suffixes,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    NULL));
    sky_module_setattr(module,
            "_fix_co_filename",
            sky_function_createbuiltin(
                    "_fix_co_filename",
                    NULL,
                    (sky_native_code_function_t)sky__imp__fix_co_filename,
                    SKY_DATA_TYPE_VOID,
                    "code", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_code_type,
                    "filename", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "get_frozen_object",
            sky_function_createbuiltin(
                    "get_frozen_object",
                    NULL,
                    (sky_native_code_function_t)sky__imp_get_frozen_object,
                    SKY_DATA_TYPE_OBJECT_MODULE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "init_builtin",
            sky_function_createbuiltin(
                    "init_builtin",
                    NULL,
                    (sky_native_code_function_t)sky_module_import_builtin,
                    SKY_DATA_TYPE_OBJECT_MODULE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "init_frozen",
            sky_function_createbuiltin(
                    "init_frozen",
                    NULL,
                    (sky_native_code_function_t)sky_module_import_frozen,
                    SKY_DATA_TYPE_OBJECT_MODULE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "is_builtin",
            sky_function_createbuiltin(
                    "is_builtin",
                    NULL,
                    (sky_native_code_function_t)sky__imp_is_builtin,
                    SKY_DATA_TYPE_BOOL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "is_frozen",
            sky_function_createbuiltin(
                    "is_frozen",
                    NULL,
                    (sky_native_code_function_t)sky__imp_is_frozen,
                    SKY_DATA_TYPE_BOOL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "is_frozen_package",
            sky_function_createbuiltin(
                    "is_frozen_package",
                    NULL,
                    (sky_native_code_function_t)sky__imp_is_frozen_package,
                    SKY_DATA_TYPE_BOOL,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "load_dynamic",
            sky_function_createbuiltin(
                    "load_dynamic",
                    NULL,
                    (sky_native_code_function_t)sky_module_import_dynamic,
                    SKY_DATA_TYPE_OBJECT_MODULE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "pathname", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "fob", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(module,
            "lock_held",
            sky_function_createbuiltin(
                    "lock_held",
                    "lock_held() -> boolean\n"
                    "Return True if the import lock is currently held, else False.\n"
                    "On platforms without threads, return False.",
                    (sky_native_code_function_t)sky_module_import_lock_held,
                    SKY_DATA_TYPE_BOOL,
                    NULL));
    sky_module_setattr(module,
            "release_lock",
            sky_function_createbuiltin(
                    "release_lock",
                    "release_lock() -> None\n"
                    "Release the interpreter's import lock.\n"
                    "On platforms without threads, this function does nothing.",
                    (sky_native_code_function_t)sky__imp_release_lock,
                    SKY_DATA_TYPE_VOID,
                    NULL));
}
