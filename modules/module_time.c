#include "../core/sky_private.h"
#include "../core/sky_thread_private.h"

#include <limits.h>
#include <math.h>
#include <time.h>


SKY_MODULE_EXTERN const char *skython_module_time_doc;

SKY_MODULE_EXTERN void
skython_module_time_initialize(sky_module_t module);

static sky_type_t sky_time_struct_time_type = NULL;

static const char sky_time_struct_time_doc[] =
"The time value as returned by gmtime(), localtime(), and strptime(), and\n"
" accepted by asctime(), mktime() and strftime().  May be considered as a\n"
" sequence of 9 integers.\n"
"\n"
" Note that several fields' values are not the same as those defined by\n"
" the C language standard for struct tm.  For example, the value of the\n"
" field tm_year is the actual year, not year - 1900.  See individual\n"
" fields' descriptions for details.\n";

static sky_struct_sequence_field_t sky_time_struct_time_fields[] = {
    { "tm_year",    "year, for example, 1993", 0 },
    { "tm_mon",     "month of yera, range [1, 12]", 0 },
    { "tm_mday",    "day of month, range [1, 31]", 0 },
    { "tm_hour",    "hours, range [0, 23]", 0 },
    { "tm_min",     "minutes, range [0, 59]", 0 },
    { "tm_sec",     "seconds, range [0, 61]", 0 },
    { "tm_wday",    "day of week, range [0, 6], Monday is 0", 0 },
    { "tm_yday",    "day of year, range [1, 366]", 0 },
    { "tm_isdst",   "1 if summer time is in effect, 0 if not, and -1 if unknown", 0 },
    { "tm_zone",    "abbreviation of timezone name",
                    SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
    { "tm_gmtoff",  "offset from UTC in seconds",
                    SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY },
};


static sky_object_t
sky_time_struct_time(struct tm *tm)
{
    sky_object_t    values[sizeof(sky_time_struct_time_fields) /
                           sizeof(sky_time_struct_time_fields[0])];

    values[0] = sky_integer_create(tm->tm_year + 1900);
    values[1] = sky_integer_create(tm->tm_mon + 1);
    values[2] = sky_integer_create(tm->tm_mday);
    values[3] = sky_integer_create(tm->tm_hour);
    values[4] = sky_integer_create(tm->tm_min);
    values[5] = sky_integer_create(tm->tm_sec);
    values[6] = sky_integer_create((tm->tm_wday + 6) % 7);
    values[7] = sky_integer_create(tm->tm_yday + 1);
    values[8] = sky_integer_create(tm->tm_isdst);
    values[9] = sky_string_createfrombytes(tm->tm_zone,
                                           strlen(tm->tm_zone),
                                           NULL,
                                           NULL);
    values[10] = sky_integer_create(tm->tm_gmtoff);

    return sky_struct_sequence_create(sky_time_struct_time_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}


static void
sky_time_struct_tm(sky_object_t structseq, struct tm *tm)
{
    ssize_t         len;
    sky_object_t    value;

    memset(tm, 0, sizeof(struct tm));
    if (!sky_object_isa(structseq, sky_tuple_type)) {
        sky_error_raise_string(sky_TypeError,
                               "Tuple or struct_time argument required");
    }

#define int_value(_i)                                                       \
        sky_integer_value(sky_number_integer(sky_tuple_get(structseq, _i)), \
                          INT_MIN, INT_MAX,                                 \
                          NULL)

    tm->tm_year = int_value(0) - 1900;
    tm->tm_mon = int_value(1) - 1;
    tm->tm_mday = int_value(2);
    tm->tm_hour = int_value(3);
    tm->tm_min = int_value(4);
    tm->tm_sec = int_value(5);
    tm->tm_wday = (int_value(6) + 1) % 7;
    tm->tm_yday = int_value(7) - 1;
    tm->tm_isdst = int_value(8);

    if (sky_object_isa(structseq, sky_time_struct_time_type)) {
        len = sky_tuple_len(structseq);
        if (len > 9) {
            tm->tm_zone =
                    sky_codec_encodetocstring(sky_tuple_get(structseq, 9), 0);
            sky_asset_save((void *)tm->tm_zone,
                           sky_free,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (len > 10) {
            value = sky_number_integer(sky_tuple_get(structseq, 10));
            tm->tm_gmtoff = sky_integer_value(value, LONG_MIN, LONG_MAX, NULL);
        }
    }

    /* Normalize and validate */
    if (-1 == tm->tm_mon) {
        tm->tm_mon = 0;
    }
    else if (tm->tm_mon < 0 || tm->tm_mon > 11) {
        sky_error_raise_string(sky_ValueError, "month out of range");
    }
    if (!tm->tm_mday) {
        tm->tm_mday = 1;
    }
    else if (tm->tm_mday < 0 || tm->tm_mday > 31) {
        sky_error_raise_string(sky_ValueError, "day of month out of range");
    }
    if (tm->tm_hour < 0 || tm->tm_hour > 23) {
        sky_error_raise_string(sky_ValueError, "hour out of range");
    }
    if (tm->tm_min < 0 || tm->tm_min > 59) {
        sky_error_raise_string(sky_ValueError, "minute out of range");
    }
    if (tm->tm_sec < 0 || tm->tm_sec > 61) {
        sky_error_raise_string(sky_ValueError, "seconds out of range");
    }
    if (tm->tm_wday < 0) {
        sky_error_raise_string(sky_ValueError, "day of week out of range");
    }
    if (-1 == tm->tm_yday) {
        tm->tm_yday = 0;
    }
    else if (tm->tm_yday < 0 || tm->tm_yday > 366) {
        sky_error_raise_string(sky_ValueError, "day of year out of range");
    }
    if (tm->tm_isdst < -1) {
        tm->tm_isdst = -1;
    }
    if (tm->tm_isdst > 1) {
        tm->tm_isdst = 1;
    }

#undef int_value
}


static time_t
sky_time_time_t(sky_object_t seconds)
{
    time_t  t;

    if (sky_object_isnull(seconds)) {
        return time(NULL);
    }
    if (sky_object_isa(seconds, sky_float_type)) {
        double  d, err, intpart;

        d = sky_float_value(seconds);
        modf(d, &intpart);
        t = (time_t)intpart;
        err = intpart - (double)t;
        if (err <= -1.0 || err >= 1.0) {
            sky_error_raise_string(
                    sky_OverflowError,
                    "timestamp out of range for platform time_t");
        }
    }
    else {
        t = sky_integer_value(seconds, LONG_MIN, LONG_MAX, NULL);
    }
    return t;
}


static sky_string_t
sky_time_asctime_string(struct tm *tm)
{
    /* Inspired by Open Group reference implementation available at
     * http://pubs.opengroup.org/onlinepubs/009695399/functions/asctime.html
     */
    static char wday_name[7][4] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static char mon_name[12][4] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };

    return sky_string_createfromformat("%s %s%3d %.2d:%.2d:%.2d %d",
                                       wday_name[tm->tm_wday],
                                       mon_name[tm->tm_mon],
                                       tm->tm_mday,
                                       tm->tm_hour,
                                       tm->tm_min,
                                       tm->tm_sec,
                                       tm->tm_year + 1900);
}


static sky_string_t
sky_time_asctime(sky_object_t tuple)
{
    struct tm   tm;

    if (!sky_object_isnull(tuple)) {
        sky_time_struct_tm(tuple, &tm);
    }
    else {
        time_t  t = time(NULL);

        if (!localtime_r(&t, &tm)) {
            if (!errno) {
                sky_error_raise_errno(sky_OSError, EINVAL);
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
    }

    return sky_time_asctime_string(&tm);
}

static const char sky_time_asctime_doc[] =
"asctime([tuple]) -> string\n\n"
"Convert a time tuple to a string, e.g. 'Sat Jun 06 16:26:11 1998'.\n"
"When the time tuple is not present, current time as returned by localtime()\n"
"is used.";


static sky_string_t
sky_time_ctime(sky_object_t seconds)
{
    time_t      t;
    struct tm   tm;

    t = sky_time_time_t(seconds);
    if (!localtime_r(&t, &tm)) {
        if (!errno) {
            sky_error_raise_errno(sky_OSError, EINVAL);
        }
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_time_asctime_string(&tm);
}

static const char sky_time_ctime_doc[] =
"ctime(seconds) -> string\n\n"
"Convert a time in seconds since the Epoch to a string in local time.\n"
"This is equivalent to asctime(localtime(seconds)). When the time tuple is\n"
"not present, current time as returned by localtime() is used.";


static sky_object_t
sky_time_gmtime(sky_object_t seconds)
{
    time_t      t;
    struct tm   tm;

    t = sky_time_time_t(seconds);
    if (!gmtime_r(&t, &tm)) {
        if (!errno) {
            sky_error_raise_errno(sky_OSError, EINVAL);
        }
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_time_struct_time(&tm);
}

static const char sky_time_gmtime_doc[] =
"gmtime([seconds]) -> (tm_year, tm_mon, tm_mday, tm_hour, tm_min,\n"
"                       tm_sec, tm_wday, tm_yday, tm_isdst)\n"
"\n"
"Convert seconds since the Epoch to a time tuple expressing UTC (a.k.a.\n"
"GMT).  When 'seconds' is not passed in, convert the current time instead.\n"
"\n"
"If the platform supports the tm_gmtoff and tm_zone, they are available as\n"
"attributes only.\n";


static sky_object_t
sky_time_localtime(sky_object_t seconds)
{
    time_t      t;
    struct tm   tm;

    t = sky_time_time_t(seconds);
    if (!localtime_r(&t, &tm)) {
        if (!errno) {
            sky_error_raise_errno(sky_OSError, EINVAL);
        }
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_time_struct_time(&tm);
}

static const char sky_time_localtime_doc[] =
"localtime([seconds]) -> (tm_year,tm_mon,tm_mday,tm_hour,tm_min,\n"
"                          tm_sec,tm_wday,tm_yday,tm_isdst)\n"
"\n"
"Convert seconds since the Epoch to a time tuple expressing local time.\n"
"When 'seconds' is not passed in, convert the current time instead.\n";


static double
sky_time_mktime(sky_object_t tuple)
{
    time_t      t;
    struct tm   tm;

    sky_time_struct_tm(tuple, &tm);
    tm.tm_wday = -1;    /* sentinel; original value ignored */
    t = mktime(&tm);
    if ((time_t)-1 == t && -1 == tm.tm_wday) {
        sky_error_raise_string(sky_OverflowError,
                               "mktime argument out of range");
    }

    return (double)t;
}

static const char sky_time_mktime_doc[] =
"mktime(tuple) -> floating point number\n\n"
"Convert a time tuple in local time to seconds since the Epoch.\n"
"Note that mktime(gmtime(0)) will not generally return zero for most\n"
"time zones; instead the returned value will either be equal to that\n"
"of the timezone or altzone attributes on the time module.";


static double
sky_time_monotonic(void)
{
    return (double)sky_time_current() * 1e-9;
}

static const char sky_time_monotonic_doc[] =
"monotonic() -> float\n\n"
"Monotonic clock, cannot go backward.";


static void
sky_time_sleep(double seconds)
{
    double      floatpart, intpart;
    sky_time_t  timeout;

    if (seconds < 0.0) {
        sky_error_raise_string(sky_ValueError,
                               "sleep length must be non-negative");
    }

    floatpart = modf(seconds, &intpart);
    timeout = ((uint64_t)intpart * UINT64_C(1000000000)) +
              (uint64_t)(floatpart * 1000000000.0);

    /* Use sky_thread_select() so that signals will be reliably handled. */
    if (sky_thread_wait_select(NULL, 0, NULL, NULL, NULL, timeout) < 0) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_time_sleep_doc[] =
"sleep(seconds)\n\n"
"Delay execution for a given number of seconds.  The argument may be\n"
"a floating point number for subsecond precision.";


static sky_string_t
sky_time_strftime(sky_string_t format, sky_tuple_t tuple)
{
    sky_string_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        int         tries;
        char        *buf, *fmt, *newbuf;
        size_t      buflen, bufsize;
        ssize_t     fmtlen;
        struct tm   tm;

        if (!sky_object_isnull(tuple)) {
            sky_time_struct_tm(tuple, &tm);
        }
        else {
            time_t  now;

            time(&now);
            localtime_r(&now, &tm);
        }

        fmt = (char *)
              sky_codec_encodetocbytes(sky_codec_locale,
                                       format,
                                       NULL,
                                       SKY_STRING_LITERAL("surrogateescape"),
                                       0,
                                       &fmtlen);
        fmt = sky_realloc(fmt, fmtlen + 1);
        fmt[fmtlen] = '\0';
        sky_asset_save(fmt, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        /* There's no (good) way to know how big of a buffer is needed, so try
         * a few times, growing the buffer each time if strftime() fails. The
         * failure could be some other error, but strftime() does not reliably
         * give any indication.
         */
        bufsize = strlen(fmt) * 2;
        buf = sky_asset_malloc(bufsize, SKY_ASSET_CLEANUP_ALWAYS);
        for (tries = 0; ; ++tries) {
            if ((buflen = strftime(buf, bufsize, fmt, &tm)) != 0 || tries > 4) {
                break;
            }
            /* The buffer is too small or there's some other error. Grow the
             * buffer, but don't use realloc to do it, because we don't want
             * to waste time copying junk.
             */
            bufsize *= 2;
            newbuf = sky_malloc(bufsize);
            sky_asset_update(buf, newbuf, SKY_ASSET_UPDATE_FIRST_CURRENT);
            sky_free(buf);
            buf = newbuf;
        }
        result = sky_string_createfrombytes(
                        buf,
                        buflen,
                        SKY_STRING_LITERAL("locale"),
                        SKY_STRING_LITERAL("surrogateescape"));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_time_strftime_doc[] =
"strftime(format[, tuple]) -> string\n\n"
"Convert a time tuple to a string according to a format specification.\n"
"See the library reference manual for formatting codes. When the time tuple\n"
"is not present, current time as returned by localtime() is used.";


static sky_object_t
sky_time_strptime(sky_tuple_t args)
{
    return sky_object_callmethod(
                    sky_module_import(SKY_STRING_LITERAL("_strptime")),
                    SKY_STRING_LITERAL("_strptime_time"),
                    args,
                    NULL);
}

static const char sky_time_strptime_doc[] =
"strptime(string, format) -> struct_time\n\n"
"Parse a string to a time tuple according to a format specification.\n"
"See the library reference manual for formatting codes (same as strftime()).";


static double
sky_time_time(void)
{
    return (double)sky_system_time() / 1.0e+9;
}


static void
sky_time_tzsetwithmodule(sky_module_t module)
{
    tzset();
    sky_module_setattr(
            module,
            "timezone",
            sky_integer_create(timezone));
    sky_module_setattr(
            module,
            "altzone",
            sky_integer_create(timezone - 3600));
    sky_module_setattr(
            module,
            "daylight",
            sky_integer_create(daylight));
    sky_module_setattr(
            module,
            "tzname",
            sky_object_build("(ss)", tzname[0], tzname[1]));
}


static void
sky_time_tzset(void)
{
    sky_module_t    module;

    module = sky_module_import(SKY_STRING_LITERAL("time"));
    sky_time_tzsetwithmodule(module);
}

static const char sky_time_tzset_doc[] =
"tzset()\n"
"\n"
"Initialize, or reinitialize, the local timezone to the value stored in\n"
"os.environ['TZ']. The TZ environment variable should be specified in\n"
"standard Unix timezone format as documented in the tzset man page\n"
"(eg. 'US/Eastern', 'Europe/Amsterdam'). Unknown timezones will silently\n"
"fall back to UTC. If the TZ environment variable is not set, the local\n"
"timezone is set to the systems best guess of wallclock time.\n"
"Changing the TZ environment variable without calling tzset *may* change\n"
"the local timezone used by methods such as localtime, but this behaviour\n"
"should not be relied on.\n";


static const char sky_time_time_doc[] =
"time() -> floating pointer number\n\n"
"Return the current time in seconds since the Epoch.\n"
"Fractions of a second may be present if the system clock provides them.";


const char *skython_time_doc =
"This module provides various functions to manipulate time values.\n"
"\n"
"There are two standard representations of time.  One is the number\n"
"of seconds since the Epoch, in UTC (a.k.a. GMT).  It may be an integer\n"
"or a floating point number (to represent fractions of seconds).\n"
"The Epoch is system-defined; on Unix, it is generally January 1st, 1970.\n"
"The actual value can be retrieved by calling gmtime(0).\n"
"\n"
"The other representation is a tuple of 9 integers giving local time.\n"
"The tuple items are:\n"
"  year (including century, e.g. 1998)\n"
"  month (1-12)\n"
"  day (1-31)\n"
"  hours (0-23)\n"
"  minutes (0-59)\n"
"  seconds (0-59)\n"
"  weekday (0-6, Monday is 0)\n"
"  Julian day (day in the year, 1-366)\n"
"  DST (Daylight Savings Time) flag (-1, 0 or 1)\n"
"If the DST flag is 0, the time is given in the regular time zone;\n"
"if it is 1, the time is given in the DST time zone;\n"
"if it is -1, mktime() should guess based on the date and time.\n"
"\n"
"Variables:\n"
"\n"
"timezone -- difference in seconds between UTC and local standard time\n"
"altzone -- difference in  seconds between UTC and local DST time\n"
"daylight -- whether local time should reflect DST\n"
"tzname -- tuple of (standard time zone name, DST time zone name)\n"
"\n"
"Functions:\n"
"\n"
"time() -- return current time in seconds since the Epoch as a float\n"
"clock() -- return CPU time since process start as a float\n"
"sleep() -- delay for a number of seconds given as a float\n"
"gmtime() -- convert seconds since Epoch to UTC tuple\n"
"localtime() -- convert seconds since Epoch to local time tuple\n"
"asctime() -- convert time tuple to string\n"
"ctime() -- convert time in seconds to string\n"
"mktime() -- convert local time tuple to seconds since Epoch\n"
"strftime() -- convert time tuple to string according to format specification\n"
"strptime() -- parse string to time tuple according to format specification\n"
"tzset() -- change the local timezone\n";

void
skython_module_time_initialize(sky_module_t module)
{
    sky_type_t  type;

    sky_module_setattr(
            module,
            "_STRUCT_TM_ITEMS",
            sky_integer_create(sizeof(sky_time_struct_time_fields) /
                               sizeof(sky_time_struct_time_fields[0])));

    type = sky_struct_sequence_define("struct_time",
                                      sky_time_struct_time_doc,
                                      sizeof(sky_time_struct_time_fields) /
                                      sizeof(sky_time_struct_time_fields[0]),
                                      sky_time_struct_time_fields);
    sky_module_setattr(module, "struct_time", type);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_time_struct_time_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "asctime",
            sky_function_createbuiltin(
                    "asctime",
                    sky_time_asctime_doc,
                    (sky_native_code_function_t)sky_time_asctime,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "tuple", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    /* TODO clock() */
    /* TODO clock_getres() */
    /* TODO clock_gettime() */
    /* TODO clock_settime() */
    sky_module_setattr(
            module,
            "ctime",
            sky_function_createbuiltin(
                    "ctime",
                    sky_time_ctime_doc,
                    (sky_native_code_function_t)sky_time_ctime,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "seconds", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    /* TODO get_clock_info() */
    sky_module_setattr(
            module,
            "gmtime",
            sky_function_createbuiltin(
                    "gmtime",
                    sky_time_gmtime_doc,
                    (sky_native_code_function_t)sky_time_gmtime,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "seconds", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "localtime",
            sky_function_createbuiltin(
                    "localtime",
                    sky_time_localtime_doc,
                    (sky_native_code_function_t)sky_time_localtime,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "seconds", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "mktime",
            sky_function_createbuiltin(
                    "mktime",
                    sky_time_mktime_doc,
                    (sky_native_code_function_t)sky_time_mktime,
                    SKY_DATA_TYPE_DOUBLE,
                    "tuple", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "monotonic",
            sky_function_createbuiltin(
                    "monotonic",
                    sky_time_monotonic_doc,
                    (sky_native_code_function_t)sky_time_monotonic,
                    SKY_DATA_TYPE_DOUBLE,
                    NULL));
    /* TODO perf_counter() */
    /* TODO process_time() */
    sky_module_setattr(
            module,
            "sleep",
            sky_function_createbuiltin(
                    "sleep",
                    sky_time_sleep_doc,
                    (sky_native_code_function_t)sky_time_sleep,
                    SKY_DATA_TYPE_VOID,
                    "seconds", SKY_DATA_TYPE_DOUBLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "strftime",
            sky_function_createbuiltin(
                    "strftime",
                    sky_time_strftime_doc,
                    (sky_native_code_function_t)sky_time_strftime,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "format", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "tuple", SKY_DATA_TYPE_OBJECT_TUPLE, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "strptime",
            sky_function_createbuiltin(
                    "strptime",
                    sky_time_strptime_doc,
                    (sky_native_code_function_t)sky_time_strptime,
                    SKY_DATA_TYPE_OBJECT,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "time",
            sky_function_createbuiltin(
                    "time",
                    sky_time_time_doc,
                    (sky_native_code_function_t)sky_time_time,
                    SKY_DATA_TYPE_DOUBLE,
                    NULL));
    sky_module_setattr(
            module,
            "tzset",
            sky_function_createbuiltin(
                    "tzset",
                    sky_time_tzset_doc,
                    (sky_native_code_function_t)sky_time_tzset,
                    SKY_DATA_TYPE_VOID,
                    NULL));

    sky_time_tzsetwithmodule(module);
}
