#ifndef __SKYTHON_MODULES_BUILTIN_MODULES_H__
#define __SKYTHON_MODULES_BUILTIN_MODULES_H__ 1


#include "../core/sky_private.h"

#include "module__io.h"


typedef void
        (*sky_builtin_module_initialize_t)(sky_module_t);

typedef struct sky_builtin_module_s {
    const char *                        name;
    sky_builtin_module_initialize_t     initialize;
    const uint8_t *                     frozen_bytes;
    ssize_t                             frozen_nbytes;
} sky_builtin_module_t;

extern sky_builtin_module_t sky_builtin_modules[];


/* Built-in Module Initializers */

extern void skython_module__ast_initialize(sky_module_t module);
extern void skython_module_builtins_initialize(sky_module_t module);
extern void skython_module__codecs_initialize(sky_module_t module);
extern void skython_module__collections_initialize(sky_module_t module);
extern void skython_module_errno_initialize(sky_module_t module);
extern void skython_module_faulthandler_initialize(sky_module_t module);
extern void skython_module__functools_initialize(sky_module_t module);
extern void skython_module_gc_initialize(sky_module_t module);
extern void skython_module__imp_initialize(sky_module_t module);
extern void skython_module__io_initialize(sky_module_t module);
extern void skython_module_itertools_initialize(sky_module_t module);
extern void skython_module__locale_initialize(sky_module_t module);
extern void skython_module_marshal_initialize(sky_module_t module);
extern void skython_module_operator_initialize(sky_module_t module);
extern void skython_module_posix_initialize(sky_module_t module);
#if defined(HAVE_PWD_H)
extern void skython_module_pwd_initialize(sky_module_t module);
#endif
#if defined(HAVE_READLINE_H)
extern void skython_module_readline_initialize(sky_module_t module);
#endif
extern void skython_module_resource_initialize(sky_module_t module);
extern void skython_module_signal_initialize(sky_module_t module);
extern void skython_module__sre_initialize(sky_module_t module);
extern void skython_module__string_initialize(sky_module_t module);
extern void skython_module_sys_initialize(sky_module_t module);
extern void skython_module__thread_initialize(sky_module_t module);
extern void skython_module__warnings_initialize(sky_module_t module);
extern void skython_module__weakref_initialize(sky_module_t module);


#endif  /* __SKYTHON_MODULES_BUILTIN_MODULES_H__ */
