#include "../core/skython.h"
#include "../core/sky_object_private.h"


static ssize_t
sky__weakref_getweakrefcount(sky_object_t object)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    ssize_t                 count;
    sky_object_reference_t  *ref;

    count = 0;
    for (ref = object_data->refs; ref; ref = ref->next) {
        if (ref->ref && (sky_weakproxy_callback == ref->callback ||
                         sky_weakref_callback == ref->callback))
        {
            ++count;
        }
    }

    return count;
}

static const char sky__weakref_getweakrefcount_doc[] =
"getweakrefcount(object) -- return the number of weak references\n"
"to 'object'.";


static sky_object_t
sky__weakref_getweakrefs(sky_object_t object)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    sky_list_t              list;
    sky_object_t            arg;
    sky_object_reference_t  *ref;

    list = sky_list_create(NULL);
    for (ref = object_data->refs; ref; ref = ref->next) {
        if (ref->ref &&
            (sky_weakproxy_callback == ref->callback ||
             sky_weakref_callback == ref->callback) &&
            (arg = ref->arg) != NULL)
        {
            sky_list_append(list, arg);
        }
    }
    return list;
}

static const char sky__weakref_getweakrefs_doc[] =
"getweakrefs(object) -- return a list of all weak reference objects\n"
"that point to 'object'.";


static sky_object_t
sky__weakref_proxy(sky_object_t object, sky_object_t callback)
{
    return sky_weakproxy_create(object, callback);
}

static const char sky__weakref_proxy_doc[] =
"proxy(object[, callback]) -- create a proxy object that weakly\n"
"references 'object'.  'callback', if given, is called with a\n"
"reference to the proxy when 'object' is about to be finalized.";


void
skython_module__weakref_initialize(sky_module_t module)
{
    static const char doc[] =
"Weak-reference support module.";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    sky_module_setattr(
            module,
            "getweakrefcount",
            sky_function_createbuiltin(
                    "getweakrefcount",
                    sky__weakref_getweakrefcount_doc,
                    (sky_native_code_function_t)sky__weakref_getweakrefcount,
                    SKY_DATA_TYPE_SSIZE_T,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getweakrefs",
            sky_function_createbuiltin(
                    "getweakrefs",
                    sky__weakref_getweakrefs_doc,
                    (sky_native_code_function_t)sky__weakref_getweakrefs,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "proxy",
            sky_function_createbuiltin(
                    "proxy",
                    sky__weakref_proxy_doc,
                    (sky_native_code_function_t)sky__weakref_proxy,
                    SKY_DATA_TYPE_OBJECT,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    "callback", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

    sky_module_setattr(module, "ref", sky_weakref_type);
    sky_module_setattr(module, "ReferenceType", sky_weakref_type);

    sky_module_setattr(module, "ProxyType", sky_weakproxy_type);
    sky_module_setattr(module, "CallableProxyType", sky_callableweakproxy_type);
}
