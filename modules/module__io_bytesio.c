#include "../core/sky_private.h"
#include "../core/sky_bytes_private.h"

#include "module__io.h"


sky_type_t sky__io_BytesIO_type = NULL;


static void
sky__io_BytesIO_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io_BytesIO_data_t  *self_data = data;
    
    sky_object_visit(self_data->buffer, visit_data);
}


static inline void
sky__io_BytesIO__checkClosed(sky_object_t self)
{
    if (sky_file_closed(self)) {
        sky_error_raise_string(sky_ValueError, "file is closed");
    }
}


static sky_memoryview_t
sky__io_BytesIO_getbuffer(sky_object_t self)
{
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);

    self_data = sky__io_BytesIO_data(self);
    return sky_memoryview_createwithobject(self_data->buffer);
}


static sky_object_t
sky__io_BytesIO_getstate(sky_object_t self)
{
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);

    self_data = sky__io_BytesIO_data(self);
    return sky_object_build("(OizO)",
                            sky_bytes_createfromobject(self_data->buffer),
                            self_data->pos,
                            sky_dict_copy(sky_object_dict(self)));
}


static sky_bytes_t
sky__io_BytesIO_getvalue(sky_object_t self)
{
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);
    
    self_data = sky__io_BytesIO_data(self);
    return sky_bytes_createfromobject(self_data->buffer);
}


static void
sky__io_BytesIO_init(sky_object_t self, sky_object_t initial_bytes)
{
    sky__io_BytesIO_data_t  *self_data;

    self_data = sky__io_BytesIO_data(self);
    if (sky_object_bool(initial_bytes)) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->buffer)),
                          sky_bytearray_createfromobject(initial_bytes),
                          self);
    }
    else {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->buffer)),
                          sky_bytearray_createwithbytes("", 0, NULL),
                          self);
    }
    self_data->pos = 0;
}


static sky_bytes_t
sky__io_BytesIO_read(sky_object_t self, sky_object_t n)
{
    ssize_t                 buffer_len, nbytes, newpos;
    sky_bytes_t             b;
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);

    if (sky_object_isnull(n)) {
        nbytes = -1;
    }
    else {
        nbytes = sky_integer_value(sky_number_index(n),
                                   SSIZE_MIN, SSIZE_MAX,
                                   NULL);
    }

    self_data = sky__io_BytesIO_data(self);
    buffer_len = sky_bytearray_len(self_data->buffer);
    if (nbytes < 0) {
        nbytes = buffer_len;
    }
    if (buffer_len <= self_data->pos) {
        return sky_bytes_empty;
    }

    newpos = SKY_MIN(buffer_len, self_data->pos + nbytes);
    b = sky_bytearray_slice_asbytes(self_data->buffer,
                                    self_data->pos,
                                    newpos,
                                    1);
    self_data->pos = newpos;
    return b;
}


static sky_bool_t
sky__io_BytesIO_readable(SKY_UNUSED sky_object_t self)
{
    return SKY_TRUE;
}


ssize_t
sky__io_BytesIO_readinto_native(
                sky_object_t    self,
                void *          bytes,
                ssize_t         nbytes)
{
    ssize_t                 newpos, nread;
    sky_buffer_t            buffer;
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);
    if (nbytes <= 0) {
        return nbytes;
    }
    self_data = sky__io_BytesIO_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, self_data->buffer, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (buffer.len <= self_data->pos) {
            nread = 0;
        }
        else {
            newpos = SKY_MIN(buffer.len, self_data->pos + nbytes);
            nread = newpos - self_data->pos;
            memcpy(bytes, buffer.buf + self_data->pos, nread);
            self_data->pos = newpos;
        }
    } SKY_ASSET_BLOCK_END;

    return nread;
}


static sky_bool_t
sky__io_BytesIO_seekable(SKY_UNUSED sky_object_t self)
{
    return SKY_TRUE;
}


ssize_t
sky__io_BytesIO_seek_native(sky_object_t self, ssize_t offset, int whence)
{
    ssize_t                 newpos;
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);

    self_data = sky__io_BytesIO_data(self);
    if (0 == whence) {
        if (offset < 0) {
            sky_error_raise_format(sky_ValueError,
                                   "negative seek position %zd",
                                   offset);
        }
        self_data->pos = offset;
    }
    else if (1 == whence) {
        newpos = self_data->pos + offset;
        self_data->pos = SKY_MAX(0, newpos);
    }
    else if (2 == whence) {
        newpos = sky_bytearray_len(self_data->buffer) + offset;
        self_data->pos = SKY_MAX(0, newpos);
    }
    else {
        sky_error_raise_string(sky_ValueError, "unsupported whence value");
    }

    return self_data->pos;
}


static void
sky__io_BytesIO_setstate(sky_object_t self, sky_object_t state)
{
    ssize_t                 pos;
    sky_dict_t              dict;
    sky_object_t            value;
    sky_integer_t           number;
    sky__io_BytesIO_data_t  *self_data;

    /* state: (value, pos, dict) */
    if (!sky_object_isa(state, sky_tuple_type) || sky_tuple_len(state) != 3) {
        sky_error_raise_format(
                sky_TypeError,
                "%@.__setstate__ argument should be 3-tuple, got %#@",
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_object_type(state)));
    }

    value = sky_bytearray_createfromobject(sky_tuple_get(state, 0));

    number = sky_tuple_get(state, 1);
    if (!sky_object_isa(number, sky_integer_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "second item of state must be an integer, not %#@",
                sky_type_name(sky_object_type(number)));
    }
    pos = sky_integer_value(number, SSIZE_MIN, SSIZE_MAX, NULL);
    if (pos < 0) {
        sky_error_raise_string(sky_ValueError,
                               "position value cannot be negative");
    }

    dict = sky_tuple_get(state, 2);
    if (!sky_object_isnull(dict)) {
        if (!sky_object_isa(dict, sky_dict_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "third item of state should be a dict, got %#@",
                    sky_type_name(sky_object_type(dict)));
        }
        sky_dict_update(sky_object_dict(self), NULL, dict);
    }

    self_data = sky__io_BytesIO_data(self);
    self_data->pos = pos;
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->buffer)), value, self);
}


ssize_t
sky__io_BytesIO_tell(sky_object_t self)
{
    sky__io_BytesIO__checkClosed(self);
    return sky__io_BytesIO_data(self)->pos;
}


sky_object_t
sky__io_BytesIO_truncate(sky_object_t self, sky_object_t pos)
{
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);

    self_data = sky__io_BytesIO_data(self);
    if (sky_object_isnull(pos)) {
        pos = sky_integer_create(self_data->pos);
    }
    else {
        pos = sky_number_index(pos);
        if (sky_integer_isnegative(pos)) {
            sky_error_raise_format(sky_ValueError,
                                   "negative truncate position %@",
                                   pos);
        }
    }

    sky_bytearray_delitem(self_data->buffer,
                          sky_slice_create(pos, sky_None, sky_None));

    return pos;
}


static sky_bool_t
sky__io_BytesIO_writable(SKY_UNUSED sky_object_t self)
{
    return SKY_TRUE;
}


static ssize_t
sky__io_BytesIO_write(sky_object_t self, sky_object_t b)
{
    ssize_t                 buffer_len, n, pos;
    sky_slice_t             slice;
    sky__io_BytesIO_data_t  *self_data;

    sky__io_BytesIO__checkClosed(self);
    if (sky_object_isa(b, sky_string_type)) {
        sky_error_raise_string(sky_TypeError,
                               "can't write str to binary stream");
    }
    if (!(n = sky_object_len(b))) {
        return 0;
    }

    self_data = sky__io_BytesIO_data(self);
    pos = self_data->pos;
    buffer_len = sky_bytearray_len(self_data->buffer);
    if (pos > buffer_len) {
        /* Insert null bytes between the current end of the file and the new
         * write position.
         */
        sky_bytearray_extend(self_data->buffer,
                             sky_bytes_repeat(SKY_BYTES_SINGLE('\0'),
                                              pos - buffer_len));
    }
    slice = sky_slice_create(sky_integer_create(pos),
                             sky_integer_create(pos + n),
                             sky_integer_one);
    sky_bytearray_setitem(self_data->buffer, slice, b);
    self_data->pos += n;

    return n;
}


ssize_t
sky__io_BytesIO_write_native(
                sky_object_t    self,
                const void *    bytes,
                ssize_t         nbytes)
{
    ssize_t             result;
    sky_buffer_t        *buffer;
    sky_memoryview_t    view;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                  SKY_ASSET_CLEANUP_ON_ERROR);
        sky_buffer_initialize(buffer,
                              (void *)bytes,
                              nbytes,
                              SKY_FALSE,
                              SKY_BUFFER_FLAG_CONTIG_RO);
        view = sky_memoryview_createwithbuffer(buffer);
    } SKY_ASSET_BLOCK_END;
    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        result = sky__io_BytesIO_write(self, view);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_type_t
sky__io_BytesIO_initialize(void)
{
    static const char type_doc[] =
"Buffered I/O implementation using an in-memory bytes buffer.";

    sky_type_t          type;
    sky_tuple_t         bases;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__io_BytesIO_instance_visit;
    bases = sky_tuple_pack(1, sky__io__BufferedIOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("BytesIO"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   sizeof(sky__io_BytesIO_data_t),
                                   0,
                                   bases,
                                   &template);

    sky_type_setattr_builtin(type,
            "getbuffer",
            sky_function_createbuiltin(
                    "BytesIO.getbuffer",
                    "Return a readable and writable view of the buffer.",
                    (sky_native_code_function_t)sky__io_BytesIO_getbuffer,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "getvalue",
            sky_function_createbuiltin(
                    "BytesIO.getvalue",
                    "Return the bytes value (contents) of the buffer",
                    (sky_native_code_function_t)sky__io_BytesIO_getvalue,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "BytesIO.read",
                    NULL,
                    (sky_native_code_function_t)sky__io_BytesIO_read,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "read1",
            sky_function_createbuiltin(
                    "BytesIO.read1",
                    "This is the same as read.",
                    (sky_native_code_function_t)sky__io_BytesIO_read,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "BytesIO.readable",
                    "readable() -> bool. Returns True if the IO object can be read.",
                    (sky_native_code_function_t)sky__io_BytesIO_readable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "BytesIO.seek",
                    NULL,
                    (sky_native_code_function_t)sky__io_BytesIO_seek_native,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "offset", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "BytesIO.seekable",
                    "seekable() -> bool. Returns True if the IO object can be seeked.",
                    (sky_native_code_function_t)sky__io_BytesIO_seekable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "BytesIO.tell",
                    NULL,
                    (sky_native_code_function_t)sky__io_BytesIO_tell,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "BytesIO.truncate",
                    NULL,
                    (sky_native_code_function_t)sky__io_BytesIO_truncate,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "BytesIO.writable",
                    "writable() -> bool. Returns True if the IO object can be written.",
                    (sky_native_code_function_t)sky__io_BytesIO_writable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "BytesIO.write",
                    NULL,
                    (sky_native_code_function_t)sky__io_BytesIO_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_BytesIO_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "initial_bytes", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(type,
            "__getstate__", sky__io_BytesIO_getstate,
            "__setstate__", sky__io_BytesIO_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_BytesIO_type),
            type,
            SKY_TRUE);
    return sky__io_BytesIO_type;
}
