#include "../core/sky_private.h"

#include "module__io.h"


static const char sky__io__TextIOBase_detach_doc[] =
"Separate the underlying buffer from the TextIOBase and return it.\n"
"\n"
"After the underlying buffer has been detached, the TextIO is in an\n"
"unusable state.\n";

static sky_object_t
sky__io__TextIOBase_detach(SKY_UNUSED sky_object_t self)
{
    sky_error_raise_string(sky_UnsupportedOperation, "detach");
}


static const char sky__io__TextIOBase_encoding_doc[] =
"Subclasses should override.";

static sky_object_t
sky__io__TextIOBase_encoding_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_None;
}


static const char sky__io__TextIOBase_errors_doc[] =
"Error setting of the decoder or encoder.\n"
"\n"
"Subclasses should override.\n";

static sky_object_t
sky__io__TextIOBase_errors_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_None;
}


static const char sky__io__TextIOBase_newlines_doc[] =
"Line endings translated so far.\n"
"\n"
"Only line endings translated during reading are considered.\n"
"\n"
"Subclasses should override.\n";

static sky_object_t
sky__io__TextIOBase_newlines_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_None;
}


static const char sky__io__TextIOBase_read_doc[] =
"Read at most n characters from stream, where n is an int.\n"
"\n"
"Read from underlying buffer until we have n characters or we hit EOF.\n"
"If n is negative or omitted, read until EOF.\n"
"\n"
"Returns a string.\n";

static sky_object_t
sky__io__TextIOBase_read(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  sky_object_t    n)
{
    sky_error_raise_string(sky_UnsupportedOperation, "read");
}


static const char sky__io__TextIOBase_readline_doc[] =
"Read until newline or EOF.\n"
"\n"
"Returns an empty string if EOF is hit immediately.\n";

static sky_object_t
sky__io__TextIOBase_readline(SKY_UNUSED sky_object_t self)
{
    sky_error_raise_string(sky_UnsupportedOperation, "readline");
}


static const char sky__io__TextIOBase_truncate_doc[] =
"Truncate size to pos, where pos is an int.";

static void
sky__io__TextIOBase_truncate(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  sky_object_t    pos)
{
    sky_error_raise_string(sky_UnsupportedOperation, "truncate");
}


static const char sky__io__TextIOBase_write_doc[] =
"Write string s to stream and returning an int.";

static ssize_t
sky__io__TextIOBase_write(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  sky_object_t    s)
{
    sky_error_raise_string(sky_UnsupportedOperation, "write");
}


sky_type_t sky__io__TextIOBase_type = NULL;

sky_type_t
sky__io__TextIOBase_initialize(void)
{
    static const char type_doc[] =
"Base class for text I/O.\n"
"\n"
"This class provides a character and line based interface to stream\n"
"I/O. There is no readinto method because Python's character strings\n"
"are immutable. There is no public constructor.\n";

    sky_type_t  type;
    sky_tuple_t bases;

    bases = sky_tuple_pack(1, sky__io__IOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_TextIOBase"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   0,
                                   0,
                                   bases,
                                   NULL);

    sky_type_setgetsets(type,
            "encoding", sky__io__TextIOBase_encoding_doc,
                        sky__io__TextIOBase_encoding_getter,
                        NULL,
            "errors",   sky__io__TextIOBase_errors_doc,
                        sky__io__TextIOBase_errors_getter,
                        NULL,
            "newlines", sky__io__TextIOBase_newlines_doc,
                        sky__io__TextIOBase_newlines_getter,
                        NULL,
            NULL);

    sky_type_setattr_builtin(type,
            "detach",
            sky_function_createbuiltin(
                    "_TextIOBase.detach",
                    sky__io__TextIOBase_detach_doc,
                    (sky_native_code_function_t)sky__io__TextIOBase_detach,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "_TextIOBase.read",
                    sky__io__TextIOBase_read_doc,
                    (sky_native_code_function_t)sky__io__TextIOBase_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "readline",
            sky_function_createbuiltin(
                    "_TextIOBase.readline",
                    sky__io__TextIOBase_readline_doc,
                    (sky_native_code_function_t)sky__io__TextIOBase_readline,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "_TextIOBase.truncate",
                    sky__io__TextIOBase_truncate_doc,
                    (sky_native_code_function_t)sky__io__TextIOBase_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "_TextIOBase.write",
                    sky__io__TextIOBase_write_doc,
                    (sky_native_code_function_t)sky__io__TextIOBase_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "s", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io__TextIOBase_type),
            type,
            SKY_TRUE);
    return sky__io__TextIOBase_type;
}


static inline void
sky__io_TextIOWrapper__checkClosed(sky_object_t self)
{
    if (sky_file_closed(self)) {
        sky_error_raise_string(sky_ValueError, "file is closed");
    }
}


static inline void
sky__io_TextIOWrapper__checkDetached(sky_object_t self)
{
    if (sky_object_isnull(sky__io_TextIOWrapper_data(self)->buffer)) {
        sky_error_raise_string(sky_ValueError, "buffer has been detached");
    }
}


static inline void
sky__io_TextIOWrapper__checkReadable(sky_object_t self)
{
    if (!sky_file_readable(self)) {
        sky_error_raise_string(sky_ValueError, "file is not readable");
    }
}


static inline void
sky__io_TextIOWrapper__checkSeekable(sky_object_t self)
{
    if (!sky_file_seekable(self)) {
        sky_error_raise_string(sky_ValueError, "file is not seekable");
    }
}


static inline void
sky__io_TextIOWrapper__checkWritable(sky_object_t self)
{
    if (!sky_file_writable(self)) {
        sky_error_raise_string(sky_ValueError, "file is not writable");
    }
}


static sky_string_t
sky__io_TextIOWrapper__decodedInput(sky_object_t self, ssize_t nbytes)
{
    ssize_t                         avail;
    sky_string_t                    slice;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (!self_data->decoded_input ||
        !(avail = sky_object_len(self_data->decoded_input) -
                  self_data->decoded_input_used))
    {
        return NULL;
    }
    avail = SKY_MIN(avail, nbytes);
    slice = sky_string_slice(self_data->decoded_input,
                             self_data->decoded_input_used,
                             self_data->decoded_input_used + avail,
                             1);
    self_data->decoded_input_used += avail;

    return slice;
}


static void
sky__io_TextIOWrapper__setDecodedInput(sky_object_t self, sky_string_t string)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (!sky_object_bool(string)) {
        self_data->decoded_input = NULL;
    }
    else {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->decoded_input)),
                          string,
                          self);
    }
    self_data->decoded_input_used = 0;
}


static void
sky__io_TextIOWrapper__flush(sky_object_t self)
{
    sky_bytes_t                     bytes;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);

    bytes = sky_bytes_join(sky_bytes_empty, self_data->write_lines);
    sky_file_write(self_data->buffer, bytes);
    sky_list_clear(self_data->write_lines);
    sky_file_flush(self_data->buffer);
}


static inline sky_object_t
sky__io_TextIOWrapper__forward(
                sky_object_t    self,
                sky_string_t    method_name,
                sky_tuple_t     args,
                sky_dict_t      kws)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_object_callmethod(self_data->buffer, method_name, args, kws);
}


static sky_integer_t
sky__io_TextIOWrapper__packCookie(
                ssize_t     position,
                size_t      decoder_flags,
                ssize_t     bytes_to_feed,
                sky_bool_t  need_eof,
                ssize_t     chars_to_skip)
{
    sky_integer_t   cookie;

    cookie = (need_eof ? sky_number_lshift(sky_integer_one,
                                           sky_integer_create(256))
                       : sky_integer_zero);
    if (chars_to_skip) {
        sky_error_validate(chars_to_skip > 0);
        sky_number_or(cookie,
                      sky_integer_lshift(sky_integer_create(chars_to_skip),
                                         sky_integer_create(192)));
    }
    if (bytes_to_feed) {
        sky_error_validate(bytes_to_feed > 0);
        sky_number_or(cookie,
                      sky_integer_lshift(sky_integer_create(bytes_to_feed),
                                         sky_integer_create(128)));
    }
    if (decoder_flags) {
        sky_error_validate(decoder_flags > 0);
        sky_number_or(cookie,
                      sky_integer_lshift(sky_integer_create(decoder_flags),
                                         sky_integer_create(64)));
    }

    return sky_number_or(cookie, sky_integer_create(position));
}


static sky_bool_t
sky__io_TextIOWrapper__readChunk(sky_object_t self)
{
    ssize_t                         ndecoded, nread;
    sky_bool_t                      final;
    sky_bytes_t                     bytes, decoder_buffer;
    sky_tuple_t                     decoder_state;
    sky_object_t                    decoder_flags;
    sky_string_t                    string;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (self_data->telling) {
        decoder_state = sky_object_callmethod(self_data->decoder,
                                              SKY_STRING_LITERAL("getstate"),
                                              NULL,
                                              NULL);
        decoder_buffer = sky_tuple_get(decoder_state, 0);
        decoder_flags = sky_tuple_get(decoder_state, 1);
    }

    if (self_data->has_read1) {
        bytes = sky__io_TextIOWrapper__forward(
                        self,
                        SKY_STRING_LITERAL("read1"),
                        sky_object_build("(iz)", self_data->chunk_size),
                        NULL);
    }
    else {
        bytes = sky_file_read(self_data->buffer, self_data->chunk_size);
    }
    final = !sky_object_bool(bytes);
    nread = sky_object_len(bytes);

    string = sky_object_callmethod(self_data->decoder,
                                   SKY_STRING_LITERAL("decode"),
                                   sky_object_build("(OB)", bytes, final),
                                   NULL);

    sky__io_TextIOWrapper__setDecodedInput(self, string);
    if (nread >= 0) {
        ndecoded = sky_object_len(string);
        if (ndecoded) {
            self_data->b2cratio = (double)nread / (double)ndecoded;
        }
        else {
            self_data->b2cratio = 0.0;
        }
    }
    if (self_data->telling) {
        decoder_buffer = sky_number_add(decoder_buffer, bytes);
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->snapshot)),
                          sky_tuple_pack(2, decoder_flags, decoder_buffer),
                          self);
    }

    return final;
}


static void
sky__io_TextIOWrapper__resetDecoder(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (self_data->decoder) {
        sky_object_callmethod(self_data->decoder,
                              SKY_STRING_LITERAL("reset"),
                              NULL,
                              NULL);
    }
}


static void
sky__io_TextIOWrapper__unpackCookie(
                sky_integer_t   cookie,
                ssize_t *       position,
                ssize_t *       decoder_flags,
                ssize_t *       bytes_to_feed,
                sky_bool_t *    need_eof,
                ssize_t *       chars_to_skip)
{
    sky_tuple_t     divmod;
    sky_integer_t   divisor;

    *need_eof = SKY_FALSE;
    *decoder_flags = *bytes_to_feed = *chars_to_skip = 0;
    divisor = sky_number_lshift(sky_integer_one, sky_integer_create(64));

    divmod = sky_number_divmod(cookie, divisor);
    *position = sky_integer_value(sky_tuple_get(divmod, 1),
                                  SSIZE_MIN, SSIZE_MAX,
                                  NULL);

    cookie = sky_tuple_get(divmod, 0);
    if (sky_integer_zero == cookie) {
        return;
    }
    divmod = sky_number_divmod(cookie, divisor);
    *decoder_flags = sky_integer_value(sky_tuple_get(divmod, 1),
                                       SSIZE_MIN, SSIZE_MAX,
                                       NULL);

    cookie = sky_tuple_get(divmod, 0);
    if (sky_integer_zero == cookie) {
        return;
    }
    divmod = sky_number_divmod(cookie, divisor);
    *bytes_to_feed = sky_integer_value(sky_tuple_get(divmod, 1),
                                       SSIZE_MIN, SSIZE_MAX,
                                       NULL);

    cookie = sky_tuple_get(divmod, 0);
    if (sky_integer_zero == cookie) {
        return;
    }
    divmod = sky_number_divmod(cookie, divisor);
    *chars_to_skip = sky_integer_value(sky_tuple_get(divmod, 1),
                                       SSIZE_MIN, SSIZE_MAX,
                                       NULL);

    *need_eof = sky_object_bool(sky_tuple_get(divmod, 0));
}


static sky_object_t
sky__io_TextIOWrapper_chunk_size_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_integer_create(self_data->chunk_size);
}


static void
sky__io_TextIOWrapper_chunk_size_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    ssize_t                         chunk_size;
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    self_data = sky__io_TextIOWrapper_data(self);
    chunk_size = sky_integer_value(sky_number_index(value),
                                   SSIZE_MIN, SSIZE_MAX,
                                   NULL);
    if (chunk_size <= 0) {
        sky_error_raise_string(
                sky_ValueError,
                "a strictly positive integer is required");
    }

    self_data->chunk_size = chunk_size;
}


static void
sky__io_TextIOWrapper_close(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    if (!sky_file_closed(self)) {
        self_data = sky__io_TextIOWrapper_data(self);

        sky_file_flush(self);
        sky_file_close(self_data->buffer);
    }
}


static sky_object_t
sky__io_TextIOWrapper_closed_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return (sky_file_closed(self_data->buffer) ? sky_True : sky_False);
}


static sky_object_t
sky__io_TextIOWrapper_detach(sky_object_t self)
{
    sky_object_t                    old_buffer;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (sky_object_isnull(self_data->buffer)) {
        sky_error_raise_string(sky_ValueError,
                               "buffer is already detached");
    }
    sky_file_flush(self);

    old_buffer = self_data->buffer;
    self_data->buffer = NULL;

    return old_buffer;
}


static int
sky__io_TextIOWrapper_fileno(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_file_fileno(self_data->buffer);
}


static void
sky__io_TextIOWrapper_flush(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);
    if (sky_file_writable(self)) {
        sky_list_extend(self_data->write_lines, self_data->write_bytes);
        sky_list_clear(self_data->write_bytes);

        sky__io_TextIOWrapper__flush(self);
    }
    self_data->telling = self_data->seekable;
}


void
sky__io_TextIOWrapper_init(
                sky_object_t            self,
                sky_object_t volatile   buffer,
                sky_string_t volatile   encoding,
                sky_string_t volatile   errors,
                sky_string_t            newline,
                volatile sky_bool_t     line_buffering,
                volatile sky_bool_t     write_through)
{
    const char                      *cstring;
    sky_module_t                    locale;
    sky_object_t                    codec, decoder;
    sky__io_TextIOWrapper_data_t    *self_data;

    /* If newline is supplied, make sure it's one of '\n', '\r', or '\r\n' */
    cstring = sky_string_cstring(newline);
    if (sky_object_bool(newline) &&
        (!cstring ||
         ((cstring[0] != '\n' || cstring[1]) &&
          (cstring[0] != '\r' || cstring[1]) &&
          (cstring[0] != '\r' || cstring[1] != '\n' || cstring[2]))))
    {
        sky_error_raise_format(sky_ValueError,
                               "illegal newline value: %@",
                               newline);
    }

    if (sky_object_isnull(encoding)) {
        SKY_ERROR_TRY {
            encoding = sky_file_device_encoding(sky_file_fileno(buffer));
        }
        SKY_ERROR_EXCEPT(sky_AttributeError) {
            encoding = NULL;
        }
        SKY_ERROR_EXCEPT(sky_UnsupportedOperation) {
            encoding = NULL;
        }
        SKY_ERROR_TRY_END;

        if (sky_object_isnull(encoding)) {
            SKY_ERROR_TRY {
                locale = sky_module_import(SKY_STRING_LITERAL("locale"));
                encoding = sky_object_callmethod(
                                    locale,
                                    SKY_STRING_LITERAL("getpreferredencoding"),
                                    sky_tuple_pack(1, sky_False),
                                    NULL);
            }
            SKY_ERROR_EXCEPT(sky_ImportError) {
                /* pass */
            } SKY_ERROR_TRY_END;
        }
        if (!sky_object_bool(encoding)) {
            encoding = SKY_STRING_LITERAL("ascii");
        }
    }
    codec = sky_codec_lookup_text(encoding, "codecs.open()");
    if (sky_object_isnull(errors)) {
        errors = SKY_STRING_LITERAL("strict");
    }

    self_data = sky__io_TextIOWrapper_data(self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->buffer)),
                      buffer,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->encoding)),
                      encoding,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->errors)),
                      errors,
                      self);

    if (sky_object_isnull(newline)) {
        self_data->read_translate = 1;
        self_data->read_universal = 1;
        self_data->read_newline = NULL;
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_newline)),
#if !defined(_WIN32)
                          sky__io_lf,
#else
                          sky__io_crlf,
#endif
                          self);
    }
    else {
        self_data->read_translate = 0;
        self_data->read_universal = (sky_object_bool(newline) ? 0 : 1);
        if (self_data->read_universal) {
            self_data->read_newline = NULL;
        }
        else {
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->read_newline)),
                              newline,
                              self);
        }
        if (!cstring[0] || '\n' == cstring[0]) {
            self_data->write_newline = NULL;
        }
        else {
            newline = (cstring[1] ? sky__io_crlf : sky__io_cr);
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_newline)),
                              newline,
                              self);
        }
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_lines)),
                      sky_list_create(NULL),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_bytes)),
                      sky_list_create(NULL),
                      self);

    self_data->chunk_size = SKY__IO_TEXTIOWRAPPER_CHUNK_SIZE;
    self_data->nwrite_bytes = 0;
    self_data->line_buffering = (line_buffering ? 1 : 0);
    self_data->has_read1 = sky_object_hasattr(buffer,
                                              SKY_STRING_LITERAL("read1"));
    self_data->seekable = (sky_file_seekable(buffer) ? 1 : 0);
    self_data->telling = self_data->seekable;
    self_data->write_through = (write_through ? 1 : 0);

    self_data->codec = sky_codec_builtin(encoding);
    self_data->encoder_flags = 0;

    if (sky_file_readable(buffer)) {
        decoder = sky_codec_incremental_decoder(codec, errors);
        if (self_data->read_universal) {
            decoder = sky_object_call(
                        sky__io_IncrementalNewlineDecoder_type,
                        sky_object_build("(Oi)",
                                         decoder,
                                         self_data->read_translate),
                        NULL);
        }
        sky_object_gc_set(&(self_data->decoder), decoder, self);
    }

    if (self_data->codec) {
        if (self_data->seekable && !sky_file_tell(buffer)) {
            self_data->encoder_flags |= SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM;
        }
    }
    else if (sky_file_writable(buffer)) {
        sky_object_gc_set(
                &(self_data->encoder),
                sky_codec_incremental_encoder(codec, errors),
                self);
    }
}


static sky_bool_t
sky__io_TextIOWrapper_isatty(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_file_isatty(self_data->buffer);
}


static sky_object_t
sky__io_TextIOWrapper_iter(sky_object_t self)
{
    return self;
}


static sky_object_t
sky__io_TextIOWrapper_name_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_object_getattr(self_data->buffer,
                              SKY_STRING_LITERAL("name"),
                              sky_NotSpecified);
}


static sky_object_t
sky__io_TextIOWrapper_newlines_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    if (sky_object_isnull(self_data->decoder)) {
        return sky_None;
    }

    return sky_object_getattr(self_data->decoder,
                              SKY_STRING_LITERAL("newlines"),
                              sky_NotSpecified);
}


static sky_object_t
sky__io_TextIOWrapper_next(sky_object_t self)
{
    sky_string_t                    line;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    self_data->telling = 0;
    line = sky_file_readline(self, -1);
    if (!sky_object_bool(line)) {
        self_data->telling = self_data->seekable;
        self_data->snapshot = NULL;
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    return line;
}


static sky_string_t
sky__io_TextIOWrapper_read(sky_object_t self, sky_object_t n)
{
    ssize_t                 nbytes;
    sky_bool_t              final;
    sky_string_t            s, string;
    sky_string_builder_t    builder;

    if (sky_object_isnull(n)) {
        nbytes = -1;
    }
    else {
        nbytes = sky_integer_value(n, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    if (nbytes < 0) {
        return sky_file_readall(self);
    }

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);
    sky__io_TextIOWrapper__checkReadable(self);

    if (!nbytes) {
        string = sky_string_empty;
    }
    else {
        /* Keep reading chunks until we have n characters to return. */
        builder = sky_string_builder_createwithcapacity(nbytes);

        s = sky__io_TextIOWrapper__decodedInput(self, nbytes);
        if (s) {
            sky_string_builder_append(builder, s);
        }

        final = SKY_FALSE;
        while (!final && sky_object_len(builder) < nbytes) {
            final = sky__io_TextIOWrapper__readChunk(self);
            s = sky__io_TextIOWrapper__decodedInput(self, nbytes);
            if (s) {
                sky_string_builder_append(builder, s);
            }
        }

        string = sky_string_builder_finalize(builder);
    }

    return string;
}


static sky_bool_t
sky__io_TextIOWrapper_readable(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_file_readable(self_data->buffer);
}


static sky_string_t
sky__io_TextIOWrapper_readall(sky_object_t self)
{
    sky_bytes_t                     bytes;
    sky_string_t                    slice, string;
    sky_string_builder_t            builder;
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);
    sky__io_TextIOWrapper__checkReadable(self);

    builder = sky_string_builder_create();
    slice = sky__io_TextIOWrapper__decodedInput(self, SSIZE_MAX);
    if (slice) {
        sky_string_builder_append(builder, slice);
    }

    self_data = sky__io_TextIOWrapper_data(self);
    bytes = sky__io_TextIOWrapper__forward(self,
                                           SKY_STRING_LITERAL("read"),
                                           NULL,
                                           NULL);
    string = sky_object_callmethod(self_data->decoder,
                                   SKY_STRING_LITERAL("decode"),
                                   sky_object_build("(OB)",
                                                    bytes,
                                                    SKY_TRUE),
                                   NULL);
    sky_string_builder_append(builder, string);
    string = sky_string_builder_finalize(builder);
    self_data->snapshot = NULL;

    return string;
}


static sky_string_t
sky__io_TextIOWrapper_readline(sky_object_t self, sky_object_t limit)
{
    ssize_t                         avail, limit_nbytes, pos;
    sky_string_builder_t            builder;
    sky__io_TextIOWrapper_data_t    *self_data;

    if (sky_object_isnull(limit)) {
        limit_nbytes = SSIZE_MAX;
    }
    else {
        limit_nbytes = sky_integer_value(limit, SSIZE_MIN, SSIZE_MAX, NULL);
        if (limit_nbytes < 0) {
            limit_nbytes = SSIZE_MAX;
        }
        else if (!limit_nbytes) {
            return sky_string_empty;
        }
    }

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);
    sky__io_TextIOWrapper__checkReadable(self);

    self_data = sky__io_TextIOWrapper_data(self);
    builder = sky_string_builder_create();

    if (!self_data->decoded_input ||
        !(sky_object_len(self_data->decoded_input) -
          self_data->decoded_input_used))
    {
        sky__io_TextIOWrapper__readChunk(self);
    }

    for (;;) {
        if (!self_data->decoded_input ||
            !(avail = sky_object_len(self_data->decoded_input) -
                      self_data->decoded_input_used))
        {
            self_data->snapshot = NULL;
            break;
        }

        /* Find the line ending in the chunk. */
        pos = sky__io_line_ending(self_data->decoded_input,
                                  self_data->decoded_input_used,
                                  self_data->read_translate,
                                  self_data->read_universal,
                                  self_data->read_newline);
        if (-1 == pos) {
            if (sky_object_len(builder) + avail > limit_nbytes) {
                avail = limit_nbytes - sky_object_len(builder);
            }
            sky_string_builder_appendslice(builder,
                                           self_data->decoded_input,
                                           avail,
                                           self_data->decoded_input_used,
                                           1);
            sky__io_TextIOWrapper__readChunk(self);
            continue;
        }

        pos = (pos + 1) - self_data->decoded_input_used;
        if (sky_object_len(builder) + pos > limit_nbytes) {
            pos = limit_nbytes - sky_object_len(builder);
        }
        sky_string_builder_appendslice(builder,
                                       self_data->decoded_input,
                                       pos,
                                       self_data->decoded_input_used,
                                       1);
        self_data->decoded_input_used += pos;
        break;
    }

    return sky_string_builder_finalize(builder);
}


static sky_string_t
sky__io_TextIOWrapper_repr(sky_object_t self)
{
    sky_string_t                    mode, name;
    sky__io_TextIOWrapper_data_t    *self_data;

    self_data = sky__io_TextIOWrapper_data(self);
    mode = sky_object_getattr(self, SKY_STRING_LITERAL("mode"), NULL);
    name = sky_object_getattr(self, SKY_STRING_LITERAL("name"), NULL);

    sky__io_TextIOWrapper__checkDetached(self);

    if (name && mode) {
        return sky_string_createfromformat(
                        "<_io.TextIOWrapper name=%#@ mode=%#@ encoding=%#@>",
                        name,
                        mode,
                        self_data->encoding);
    }
    if (name) {
        return sky_string_createfromformat(
                        "<_io.TextIOWrapper name=%#@ encoding=%#@>",
                        name,
                        self_data->encoding);
    }
    if (mode) {
        return sky_string_createfromformat(
                        "<_io.TextIOWrapper mode=%#@ encoding=%#@>",
                        mode,
                        self_data->encoding);
    }
    return sky_string_createfromformat(
                    "<_io.TextIOWrapper encoding=%#@>",
                    self_data->encoding);
}


static sky_integer_t
sky__io_TextIOWrapper_seek(sky_object_t self, sky_integer_t offset, int whence)
{
    ssize_t                         bytes_to_feed, chars_to_skip,
                                    decoder_flags, position;
    sky_bool_t                      need_eof;
    sky_bytes_t                     input_chunk;
    sky_string_t                    decoded_input;
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);

    if (1 == whence) {
        if (sky_object_compare(offset,
                               sky_integer_zero,
                               SKY_COMPARE_OP_NOT_EQUAL))
        {
            sky_error_raise_string(
                    sky_UnsupportedOperation,
                    "can't do nonzero cur-relative seeks");
        }
        whence = 0;
        offset = sky_object_callmethod(self,
                                       SKY_STRING_LITERAL("tell"),
                                       NULL,
                                       NULL);
    }
    else if (2 == whence) {
        if (sky_object_compare(offset,
                               sky_integer_zero,
                               SKY_COMPARE_OP_NOT_EQUAL))
        {
            sky_error_raise_string(
                    sky_UnsupportedOperation,
                    "can't do nonzero end-relative seeks");
        }
        sky_file_flush(self);
        offset = sky_object_callmethod(self_data->buffer,
                                       SKY_STRING_LITERAL("seek"),
                                       sky_object_build("(ii)", 0, 2),
                                       NULL);
        sky__io_TextIOWrapper__setDecodedInput(self, NULL);
        self_data->snapshot = NULL;
        sky__io_TextIOWrapper__resetDecoder(self);
        return offset;
    }
    else if (whence != 0) {
        sky_error_raise_format(sky_ValueError,
                               "unsupported whence (%d)",
                               whence);
    }
    if (sky_integer_isnegative(offset)) {
        sky_error_raise_format(sky_ValueError,
                               "negative seek position %@",
                               offset);
    }
    sky_file_flush(self);

    sky__io_TextIOWrapper__unpackCookie(offset,
                                        &position,
                                        &decoder_flags,
                                        &bytes_to_feed,
                                        &need_eof,
                                        &chars_to_skip);

    sky_file_seek(self_data->buffer, position, 0);
    sky__io_TextIOWrapper__setDecodedInput(self, NULL);
    self_data->snapshot = NULL;

    if (!sky_object_isnull(self_data->decoder) &&
        sky_object_compare(offset, sky_integer_zero, SKY_COMPARE_OP_EQUAL))
    {
        sky_object_callmethod(self_data->decoder,
                              SKY_STRING_LITERAL("reset"),
                              NULL,
                              NULL);
    }
    else if (!sky_object_isnull(self_data->decoder) ||
             decoder_flags ||
             chars_to_skip)
    {
        sky_object_callmethod(self_data->decoder,
                              SKY_STRING_LITERAL("setstate"),
                              sky_object_build("((Oiz))", sky_bytes_empty,
                                                          decoder_flags),
                              NULL);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->snapshot)),
                sky_tuple_pack(2, sky_integer_create(decoder_flags),
                                  sky_bytes_empty),
                self);
    }

    if (chars_to_skip) {
        input_chunk = sky_file_read(self_data->buffer, bytes_to_feed);
        decoded_input = sky_object_callmethod(self_data->decoder, 
                                              SKY_STRING_LITERAL("decode"),
                                              sky_object_build("(OB)",
                                                               input_chunk,
                                                               need_eof),
                                              NULL);
        sky__io_TextIOWrapper__setDecodedInput(self, decoded_input);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->snapshot)),
                sky_tuple_pack(2, sky_integer_create(decoder_flags),
                                  input_chunk),
                self);
    }

    if (!sky_object_isnull(self_data->encoder)) {
        if (sky_object_compare(offset,
                               sky_integer_zero,
                               SKY_COMPARE_OP_EQUAL))
        {
            sky_object_callmethod(self_data->encoder,
                                  SKY_STRING_LITERAL("reset"),
                                  NULL,
                                  NULL);
        }
        else {
            sky_object_callmethod(self_data->encoder,
                                  SKY_STRING_LITERAL("setstate"),
                                  sky_tuple_pack(1, sky_integer_zero),
                                  NULL);
        }
    }

    return offset;
}


static sky_bool_t
sky__io_TextIOWrapper_seekable(sky_object_t self)
{
    sky__io_TextIOWrapper__checkDetached(self);
    return sky__io_TextIOWrapper_data(self)->seekable;
}


static void
sky__io_TextIOWrapper_tell_cleanup(void *arg)
{
    sky_tuple_t     tuple = arg;
    sky_object_t    decoder = sky_tuple_get(tuple, 0),
                    saved_state = sky_tuple_get(tuple, 1);

    sky_object_callmethod(decoder,
                          SKY_STRING_LITERAL("setstate"),
                          sky_tuple_pack(1, saved_state),
                          NULL);
}

static sky_integer_t
sky__io_TextIOWrapper_tell(sky_object_t self)
{
    ssize_t                         bytes_fed, chars_decoded, chars_to_skip,
                                    decoder_flags, i, n, position, skip_back,
                                    skip_bytes, start_flags, start_pos;
    sky_bool_t                      need_eof;
    sky_bytes_t                     decoder_buffer, next_input;
    sky_tuple_t                     empty_state, saved_state, state;
    sky_object_t                    b, cookie, d, slice;
    sky_string_t                    decode, getstate, setstate;
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);
    sky__io_TextIOWrapper__checkSeekable(self);

    self_data = sky__io_TextIOWrapper_data(self);
    if (!self_data->telling) {
        sky_error_raise_string(sky_OSError,
                               "telling position disabled by next() call");
    }
    sky_file_flush(self);

    position = sky_file_tell(self_data->buffer);
    if (sky_object_isnull(self_data->decoder) ||
        sky_object_isnull(self_data->snapshot))
    {
        return sky__io_TextIOWrapper__packCookie(position, 0, 0, SKY_FALSE, 0);
    }

    decoder_flags = sky_integer_value(
                            sky_tuple_get(self_data->snapshot, 0),
                            SSIZE_MIN, SSIZE_MAX,
                            NULL);
    next_input = sky_tuple_get(self_data->snapshot, 1);
    position -= sky_object_len(next_input);

    chars_to_skip = self_data->decoded_input_used;
    if (!chars_to_skip) {
        return sky__io_TextIOWrapper__packCookie(position,
                                                 decoder_flags,
                                                 0,
                                                 SKY_FALSE,
                                                 0);
    }

    SKY_ASSET_BLOCK_BEGIN {
        decode = SKY_STRING_LITERAL("decode");
        getstate = SKY_STRING_LITERAL("getstate");
        setstate = SKY_STRING_LITERAL("setstate");

        saved_state = sky_object_callmethod(self_data->decoder,
                                            getstate,
                                            NULL,
                                            NULL);
        sky_asset_save(sky_tuple_pack(2, self_data->decoder, saved_state),
                       sky__io_TextIOWrapper_tell_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        /* Starting from the snapshot position, walk the decoder forward until
         * it gives us enough decoded characters.
         */
        empty_state = sky_object_build("((Oiz))", sky_bytes_empty,
                                                  decoder_flags);
        skip_bytes = self_data->b2cratio * chars_to_skip;
        skip_back = 1;
        while (skip_bytes > 0) {
            sky_object_callmethod(self_data->decoder,
                                  setstate,
                                  empty_state,
                                  NULL);

            slice = sky_slice_create(sky_None,
                                     sky_integer_create(skip_bytes),
                                     sky_integer_one);
            slice = sky_object_getitem(next_input, slice);
            d = sky_object_callmethod(self_data->decoder,
                                      decode,
                                      sky_tuple_pack(1, slice),
                                      NULL);
            n = sky_object_len(d);
            if (n <= chars_to_skip) {
                state = sky_object_callmethod(self_data->decoder,
                                              getstate,
                                              NULL,
                                              NULL);
                b = sky_tuple_get(state, 0);
                if (!sky_object_bool(b)) {
                    d = sky_tuple_get(state, 1);
                    decoder_flags = sky_integer_value(d,
                                                      SSIZE_MIN, SSIZE_MAX,
                                                      NULL);
                    chars_to_skip -= n;
                    break;
                }
                skip_bytes -= sky_object_len(b);
                skip_back = 1;
            }
            else {
                skip_bytes -= skip_back;
                skip_back *= 2;
            }
        }
        if (skip_bytes <= 0) {
            skip_bytes = 0;
            sky_object_callmethod(
                    self_data->decoder,
                    setstate,
                    sky_object_build("((Oiz))", sky_bytes_empty, decoder_flags),
                    NULL);
        }

        start_pos = position + skip_bytes;
        start_flags = decoder_flags;
        if (!chars_to_skip) {
            cookie = sky__io_TextIOWrapper__packCookie(start_pos,
                                                       start_flags,
                                                       0,
                                                       SKY_FALSE,
                                                       0);
        }
        else {
            bytes_fed = 0;
            need_eof = SKY_FALSE;
            chars_decoded = 0;
            for (i = skip_bytes, n = sky_object_len(next_input); i < n; ++i) {
                ++bytes_fed;
                slice = sky_slice_create(sky_integer_create(i),
                                         sky_integer_create(i + 1),
                                         sky_integer_one);
                slice = sky_object_getitem(next_input, slice);
                d = sky_object_callmethod(self_data->decoder,
                                          decode,
                                          sky_tuple_pack(1, slice),
                                          NULL);
                chars_decoded += sky_object_len(d);
                state = sky_object_callmethod(self_data->decoder,
                                              getstate,
                                              NULL,
                                              NULL);
                decoder_buffer = sky_tuple_get(state, 0);
                decoder_flags = sky_integer_value(sky_tuple_get(state, 1),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
                if (!sky_object_bool(decoder_buffer) &&
                    chars_decoded <= chars_to_skip)
                {
                    start_pos += bytes_fed;
                    chars_to_skip -= chars_decoded;
                    start_flags = decoder_flags;
                    bytes_fed = chars_decoded = 0;
                }
                if (chars_decoded >= chars_to_skip) {
                    break;
                }
            }
            if (i >= n) {
                d = sky_object_callmethod(self_data->decoder,
                                          decode,
                                          sky_tuple_pack(1, sky_bytes_empty),
                                          sky_object_build("{sB}",
                                                           "final", SKY_TRUE));
                chars_decoded += sky_object_len(d);
                need_eof = SKY_TRUE;
                if (chars_decoded < chars_to_skip) {
                    sky_error_raise_string(
                            sky_OSError,
                            "can't reconstruct logical file position");
                }
            }
            cookie = sky__io_TextIOWrapper__packCookie(start_pos,
                                                       start_flags,
                                                       bytes_fed,
                                                       need_eof,
                                                       chars_to_skip);
        }
    } SKY_ASSET_BLOCK_END;

    return cookie;
}


static void
sky__io_TextIOWrapper_truncate(sky_object_t self, sky_object_t pos)
{
    ssize_t                         value;
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);

    sky_file_flush(self);
    if (sky_object_isnull(pos)) {
        value = sky_file_tell(self);
    }
    else {
        value = sky_integer_value(pos, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    sky_file_truncate(self_data->buffer, value);
}


static sky_bool_t
sky__io_TextIOWrapper_writable(sky_object_t self)
{
    sky__io_TextIOWrapper_data_t    *self_data;

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);

    self_data = sky__io_TextIOWrapper_data(self);
    return sky_file_writable(self_data->buffer);
}


static const char sky__io_TextIOWrapper_write_doc[] =
"Write data, where s is a str";

static ssize_t
sky__io_TextIOWrapper_write(sky_object_t self, sky_object_t s)
{
    ssize_t         index, len, nextra;
    uint32_t        flags;
    sky_bytes_t     bytes;
    sky_tuple_t     args;
    sky_object_t    extra, line, method, o;

    sky_object_t volatile                   state;
    sky__io_TextIOWrapper_data_t * volatile self_data;

    len = sky_object_len(s);
    self_data = sky__io_TextIOWrapper_data(self);

    if (self_data->write_through) {
        line = s;
        extra = NULL;
    }
    else if (!self_data->line_buffering) {
        line = NULL;
        extra = s;
    }
    else {
        if (sky_object_type(s) == sky_string_type) {
            index = sky_string_rfind(s, sky__io_lf, 0, SSIZE_MAX);
        }
        else {
            o = sky_object_callmethod(s,
                                      SKY_STRING_LITERAL("rfind"),
                                      sky_tuple_pack(1, sky__io_lf),
                                      NULL);
            index = sky_integer_value(sky_number_index(o),
                                      SSIZE_MIN,
                                      SSIZE_MAX,
                                      NULL);
        }
        if (-1 == index) {
            line = NULL;
            extra = s;
        }
        else {
            line = sky_string_slice(s, 0, index + 1, 1);
            extra = sky_string_slice(s, index + 1, SSIZE_MAX, 1);
        }
    }

    /* If newline translation is required, it must be done before encoding. */
    if (self_data->write_newline) {
        if (sky_object_type(s) == sky_string_type) {
            if (line) {
                line = sky_string_replace(line,
                                          sky__io_lf,
                                          self_data->write_newline,
                                          -1);
            }
            if (extra) {
                extra = sky_string_replace(extra,
                                           sky__io_lf,
                                           self_data->write_newline,
                                           -1);
            }
        }
        else {
            method = SKY_STRING_LITERAL("replace");
            args = sky_tuple_pack(2, sky__io_lf,
                                     self_data->write_newline);
            if (line) {
                line = sky_object_callmethod(line, method, args, NULL);
            }
            if (extra) {
                extra = sky_object_callmethod(extra, method, args, NULL);
            }
        }
    }

    /* None of the built-in codecs support incremental encoding, so it's safe
     * to do the encoding early. This is the most common case, and it's best to
     * do as little as possible while the lock is held.
     */
    if (self_data->codec) {
        flags = self_data->encoder_flags;
        if (line) {
            line = sky_codec_encode(self_data->codec,
                                    line,
                                    self_data->encoding,
                                    self_data->errors,
                                    flags);
            flags &= ~SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM;
        }
        if (extra) {
            extra = sky_codec_encode(self_data->codec,
                                     extra,
                                     self_data->encoding,
                                     self_data->errors,
                                     flags);
        }
    }

    sky__io_TextIOWrapper__checkDetached(self);
    sky__io_TextIOWrapper__checkClosed(self);
    sky__io_TextIOWrapper__checkWritable(self);

    nextra = self_data->nwrite_bytes + (extra ? sky_object_len(extra) : 0);
    if ((line && !self_data->write_lines) || nextra >= self_data->chunk_size) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_lines)),
                          sky_list_create(NULL),
                          self);
    }
    if (extra && !self_data->write_bytes) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->write_bytes)),
                          sky_list_create(NULL),
                          self);
    }

    if (!self_data->codec) {
        state = sky_object_callmethod(self_data->encoder,
                                      SKY_STRING_LITERAL("getstate"),
                                      NULL,
                                      NULL);

        SKY_ERROR_TRY {
            method = SKY_STRING_LITERAL("encode");
            if (line) {
                line = sky_object_callmethod(self_data->encoder,
                                             method,
                                             sky_tuple_pack(1, line),
                                             NULL);
            }
            if (extra) {
                extra = sky_object_callmethod(self_data->encoder,
                                              method,
                                              sky_tuple_pack(1, extra),
                                              NULL);
            }
        } SKY_ERROR_EXCEPT_ANY {
            sky_object_callmethod(self_data->encoder,
                                  SKY_STRING_LITERAL("setstate"),
                                  sky_tuple_pack(1, state),
                                  NULL);
            sky_error_raise();
        } SKY_ERROR_TRY_END;
    }

    if (sky_object_bool(line)) {
        sky_list_extend(self_data->write_lines, self_data->write_bytes);
        sky_list_append(self_data->write_lines, line);
        sky_list_clear(self_data->write_bytes);
        self_data->nwrite_bytes = 0;
    }
    if (sky_object_bool(extra)) {
        sky_list_append(self_data->write_bytes, extra);
        self_data->nwrite_bytes += sky_object_len(extra);
        if (nextra >= self_data->chunk_size) {
            sky_list_extend(self_data->write_lines, self_data->write_bytes);
            sky_list_clear(self_data->write_bytes);
            self_data->nwrite_bytes = 0;
        }
    }
    self_data->encoder_flags &= ~SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM;
    sky__io_TextIOWrapper__resetDecoder(self);

    sky__io_TextIOWrapper__flush(self);
    self_data->snapshot = NULL;

    if (self_data->write_through &&
        !sky_object_isnull(self_data->write_bytes))
    {
        bytes = sky_bytes_join(sky_bytes_empty, self_data->write_bytes);
        sky_file_write(self_data->buffer, bytes);
        sky_list_clear(self_data->write_bytes);
    }

    return len;
}


static void
sky__io_TextIOWrapper_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io_TextIOWrapper_data_t    *self_data = data;

    sky_object_visit(self_data->buffer, visit_data);
    sky_object_visit(self_data->decoder, visit_data);
    sky_object_visit(self_data->encoder, visit_data);
    sky_object_visit(self_data->encoding, visit_data);
    sky_object_visit(self_data->errors, visit_data);
    sky_object_visit(self_data->read_newline, visit_data);
    sky_object_visit(self_data->write_newline, visit_data);
    sky_object_visit(self_data->decoded_input, visit_data);
    sky_object_visit(self_data->snapshot, visit_data);
    sky_object_visit(self_data->write_lines, visit_data);
    sky_object_visit(self_data->write_bytes, visit_data);
}


sky_type_t sky__io_TextIOWrapper_type;

sky_type_t
sky__io_TextIOWrapper_initialize(void)
{
    static const char type_doc[] =
"Character and line based layer over a BufferedIOBase object, buffer.\n"
"\n"
"encoding gives the name of the encoding that the stream will be\n"
"decoded or encoded with. It defaults to locale.getpreferredencoding(False).\n"
"\n"
"errors determines the strictness of encoding and decoding (see\n"
"help(codecs.Codec) or the documentation for codecs.register) and\n"
"defaults to \"strict\".\n"
"\n"
"newline can be None, '', '\\n', '\\r', or '\\r\\n'.  It controls the\n"
"handling of line endings. If it is None, universal newlines is\n"
"enabled.  With this enabled, on input, the lines endings '\\n', '\\r',\n"
"or '\\r\\n' are translated to '\\n' before being returned to the\n"
"caller. Conversely, on output, '\\n' is translated to the system\n"
"default line separator, os.linesep. If newline is any other of its\n"
"legal values, that newline becomes the newline when the file is read\n"
"and it is returned untranslated. On output, '\\n' is converted to the\n"
"newline.\n"
"\n"
"If line_buffering is True, a call to flush is implied when a call to\n"
"write contains a newline character.\n";

    sky_type_t          type;
    sky_tuple_t         bases;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__io_TextIOWrapper_instance_visit;

    bases = sky_tuple_pack(1, sky__io__TextIOBase_type);
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("TextIOWrapper"),
                                   type_doc,
                                   SKY_TYPE_CREATE_FLAG_HAS_DICT,
                                   sizeof(sky__io_TextIOWrapper_data_t),
                                   0,
                                   bases,
                                   &template);

    sky_type_setgetsets(type,
            "closed",       NULL,
                            sky__io_TextIOWrapper_closed_getter,
                            NULL,
            "name",         NULL,
                            sky__io_TextIOWrapper_name_getter,
                            NULL,
            "newlines",     NULL,
                            sky__io_TextIOWrapper_newlines_getter,
                            NULL,
            "_CHUNK_SIZE",  NULL,
                            sky__io_TextIOWrapper_chunk_size_getter,
                            sky__io_TextIOWrapper_chunk_size_setter,
            NULL);

    sky_type_setmembers(type,
            "buffer",
                    NULL,
                    offsetof(sky__io_TextIOWrapper_data_t, buffer),
                    SKY_DATA_TYPE_OBJECT,
                    SKY_MEMBER_FLAG_READONLY,
            "encoding",
                    NULL,
                    offsetof(sky__io_TextIOWrapper_data_t, encoding),
                    SKY_DATA_TYPE_OBJECT_STRING,
                    SKY_MEMBER_FLAG_READONLY,
            "errors",
                    NULL,
                    offsetof(sky__io_TextIOWrapper_data_t, errors),
                    SKY_DATA_TYPE_OBJECT_STRING,
                    SKY_MEMBER_FLAG_READONLY,
            "line_buffering",
                    NULL,
                    offsetof(sky__io_TextIOWrapper_data_t, line_buffering),
                    SKY_DATA_TYPE_BOOL,
                    SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "TextIOWrapper.close",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "detach",
            sky_function_createbuiltin(
                    "TextIOWrapper.detach",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_detach,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "fileno",
            sky_function_createbuiltin(
                    "TextIOWrapper.fileno",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_fileno,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "flush",
            sky_function_createbuiltin(
                    "TextIOWrapper.flush",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_flush,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "isatty",
            sky_function_createbuiltin(
                    "TextIOWrapper.isatty",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_isatty,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "TextIOWrapper.read",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_read,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "TextIOWrapper.readable",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_readable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readall",
            sky_function_createbuiltin(
                    "TextIOWrapper.readall",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_readall,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readline",
            sky_function_createbuiltin(
                    "TextIOWrapper.readline",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_readline,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "limit", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "TextIOWrapper.seek",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_seek,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "offset", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "TextIOWrapper.seekable",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_seekable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "TextIOWrapper.tell",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_tell,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "TextIOWrapper.truncate",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "TextIOWrapper.writable",
                    NULL,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_writable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "TextIOWrapper.write",
                    sky__io_TextIOWrapper_write_doc,
                    (sky_native_code_function_t)sky__io_TextIOWrapper_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "s", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_TextIOWrapper_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer", SKY_DATA_TYPE_OBJECT, NULL,
            "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "newline", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "line_buffering", SKY_DATA_TYPE_BOOL, sky_False,
            "write_through", SKY_DATA_TYPE_BOOL, sky_False,
            NULL);

    sky_type_setmethodslots(type,
            "__repr__", sky__io_TextIOWrapper_repr,
            "__iter__", sky__io_TextIOWrapper_iter,
            "__next__", sky__io_TextIOWrapper_next,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_TextIOWrapper_type),
            type,
            SKY_TRUE);
    return sky__io_TextIOWrapper_type;
}


sky_object_t
sky__io_IncrementalNewlineDecoder_decode(
                sky_object_t    self,
                sky_object_t    input,
                sky_bool_t      final)
{
    ssize_t                                     cr, crlf, lf;
    sky_string_t                                output;
    sky_string_builder_t                        builder;
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    /* decode input (with the eventual \r from a previous pass */
    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    if (sky_object_isnull(self_data->decoder)) {
        output = input;
    }
    else {
        output = sky_object_callmethod(
                        self_data->decoder,
                        SKY_STRING_LITERAL("decode"),
                        sky_tuple_pack(1, input),
                        sky_object_build("{sB}", "final", final));
    }
    if (self_data->pendingcr && (final || sky_object_bool(output))) {
        builder = sky_string_builder_createwithcapacity(
                            sky_object_len(output) + 1);
        sky_string_builder_appendcodepoint(builder, '\r', 1);
        sky_string_builder_append(builder, output);
        output = sky_string_builder_finalize(builder);
        self_data->pendingcr = 0;
    }

    /* retain last \r even when not translating data:
     * then readline() is sure to get \r\n in one pass
     */
    if (!final &&
        sky_string_endswith(output, sky__io_cr, 0, SSIZE_MAX))
    {
        output = sky_string_slice(output, 0, -1, 1);
        self_data->pendingcr = 1;
    }

    /* Record which newlines are read */ 
    crlf = sky_string_count(output, sky__io_crlf, 0, SSIZE_MAX, -1);
    cr = sky_string_count(output, sky__io_cr, 0, SSIZE_MAX, -1) - crlf;
    lf = sky_string_count(output, sky__io_lf, 0, SSIZE_MAX, -1) - crlf;
    if (cr) {
        self_data->seen |= SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR;
    }
    if (crlf) {
        self_data->seen |= SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF;
    }
    if (lf) {
        self_data->seen |= SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF;
    }

    if (self_data->translate) {
        if (crlf) {
            output = sky_string_replace(output,
                                        sky__io_crlf,
                                        sky__io_lf,
                                        -1);
        }
        if (cr) {
            output = sky_string_replace(output,
                                        sky__io_cr,
                                        sky__io_lf,
                                        -1);
        }
    }

    return output;
}


static sky_object_t
sky__io_IncrementalNewlineDecoder_getstate(sky_object_t self)
{
    sky_object_t                                buf, flag, state;
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    if (sky_object_isnull(self_data->decoder)) {
        buf = sky_bytes_empty;
        flag = sky_integer_zero;
    }
    else {
        state = sky_object_callmethod(self_data->decoder,
                                      SKY_STRING_LITERAL("getstate"),
                                      NULL,
                                      NULL);
        buf = sky_sequence_get(state, 0);
        flag = sky_sequence_get(state, 1);
    }

    flag = sky_number_inplace_lshift(flag, sky_integer_one);
    if (self_data->pendingcr) {
        flag = sky_number_inplace_or(flag, sky_integer_one);
    }

    return sky_tuple_pack(2, buf, flag);
}


static void
sky__io_IncrementalNewlineDecoder_init(
                sky_object_t    self,
                sky_object_t    decoder,
                sky_bool_t      translate,
                sky_string_t    errors)
{
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    sky_object_gc_set(&(self_data->decoder), decoder, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->errors)), errors, self);
    self_data->translate = (translate ? 1 : 0);
    self_data->pendingcr = 0;
    self_data->seen = 0;
}


static sky_object_t
sky__io_IncrementalNewlineDecoder_newlines_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    switch (self_data->seen) {
        case SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR:
            return sky__io_cr;
        case SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF:
            return sky__io_crlf;
        case SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF:
            return sky__io_lf;
        case (SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR |
              SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF):
            return sky_tuple_pack(2, sky__io_cr,
                                     sky__io_crlf);
        case (SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR |
              SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF):
            return sky_tuple_pack(2, sky__io_cr,
                                     sky__io_lf);
        case (SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF |
              SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF):
            return sky_tuple_pack(2, sky__io_lf,
                                     sky__io_crlf);
        case (SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CR |
              SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_CRLF |
              SKY__IO_INCREMENTALNEWLINEDECODER_SEEN_LF):
            return sky_tuple_pack(3, sky__io_cr,
                                     sky__io_lf,
                                     sky__io_crlf);
    }

    return sky_None;
}


static void
sky__io_IncrementalNewlineDecoder_reset(sky_object_t self)
{
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    self_data->pendingcr = 0;
    self_data->seen = 0;
    if (!sky_object_isnull(self_data->decoder)) {
        sky_object_callmethod(self_data->decoder,
                              SKY_STRING_LITERAL("reset"),
                              NULL,
                              NULL);
    }
}


static void
sky__io_IncrementalNewlineDecoder_setstate(
                sky_object_t    self,
                sky_object_t    state)
{
    sky_object_t                                buf, flag, mask;
    sky__io_IncrementalNewlineDecoder_data_t    *self_data;

    self_data = sky__io_IncrementalNewlineDecoder_data(self);
    buf = sky_sequence_get(state, 0);
    flag = sky_sequence_get(state, 1);

    mask = sky_number_and(flag, sky_integer_one);
    self_data->pendingcr = (sky_object_bool(mask) ? 1 : 0);
    if (self_data->decoder) {
        flag = sky_number_inplace_rshift(flag, sky_integer_one);
        sky_object_callmethod(self_data->decoder,
                              SKY_STRING_LITERAL("setstate"),
                              sky_object_build("((OO))", buf, flag),
                              NULL);
    }
}


static void
sky__io_IncrementalNewlineDecoder_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io_IncrementalNewlineDecoder_data_t    *self_data = data;

    sky_object_visit(self_data->decoder, visit_data);
    sky_object_visit(self_data->errors, visit_data);
}

sky_type_t sky__io_IncrementalNewlineDecoder_type;

sky_type_t
sky__io_IncrementalNewlineDecoder_initialize(void)
{
    static const char type_doc[] =
"Codec used when reading a file in universal newlines mode.  It wraps\n"
"another incremental decoder, translating \\r\\n and \\r into \\n.  It also\n"
"records the types of newlines encountered.  When used with\n"
"translate=False, it ensures that the newline sequence is returned in\n"
"one piece. When used with decoder=None, it expects unicode strinsg as\n"
"decode input and translates newlines without first invoking an external\n"
"decoder.\n";

    sky_type_t          type;
    sky_type_template_t template;

    /* Ideally, IncrementalNewlineDecoder should use codecs.IncrementalDecoder
     * as a base, but that's not really possible since the codecs module
     * depends on the _codecs module. Circular reference. CPython doesn't use
     * codecs.IncrementalDecoder as a base here either, so it's fine that we
     * don't either.
     */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__io_IncrementalNewlineDecoder_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("IncrementalNewlineDecoder"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    sizeof(sky__io_IncrementalNewlineDecoder_data_t),
                    0,
                    NULL,
                    &template);

    sky_type_setattr_getset(type,
            "newlines",
            NULL,
            sky__io_IncrementalNewlineDecoder_newlines_getter,
            NULL);

    sky_type_setattr_builtin(type,
            "decode",
            sky_function_createbuiltin(
                    "IncrementalNewlineDecoder.decode",
                    NULL,
                    (sky_native_code_function_t)sky__io_IncrementalNewlineDecoder_decode,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "input", SKY_DATA_TYPE_OBJECT, NULL,
                    "final", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(type,
            "getstate",
            sky_function_createbuiltin(
                    "IncrementalNewlineDecoder.getstate",
                    NULL,
                    (sky_native_code_function_t)sky__io_IncrementalNewlineDecoder_getstate,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "reset",
            sky_function_createbuiltin(
                    "IncrementalNewlineDecoder.reset",
                    NULL,
                    (sky_native_code_function_t)sky__io_IncrementalNewlineDecoder_reset,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "setstate",
            sky_function_createbuiltin(
                    "IncrementalNewlineDecoder.setstate",
                    NULL,
                    (sky_native_code_function_t)sky__io_IncrementalNewlineDecoder_setstate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "state", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_IncrementalNewlineDecoder_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "decoder", SKY_DATA_TYPE_OBJECT, NULL,
            "translate", SKY_DATA_TYPE_BOOL, NULL,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("strict"),
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_IncrementalNewlineDecoder_type),
            type,
            SKY_TRUE);
    return sky__io_IncrementalNewlineDecoder_type;
}
