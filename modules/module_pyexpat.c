#include "../core/sky_private.h"
#include "../core/sky_string_private.h"

#include <wchar.h>

#include <expat.h>
#define XML_COMBINED_VERSION \
    (10000 * XML_MAJOR_VERSION + 100 * XML_MINOR_VERSION + XML_MICRO_VERSION)


SKY_MODULE_EXTERN const char *skython_module_pyexpat_doc;

SKY_MODULE_EXTERN void
skython_module_pyexpat_initialize(sky_module_t module);


enum HandlerTypes {
    StartElement,
    EndElement,
    ProcessingInstruction,
    CharacterData,
    UnparsedEntityDecl,
    NotationDecl,
    StartNamespaceDecl,
    EndNamespaceDecl,
    Comment,
    StartCdataSection,
    EndCdataSection,
    Default,
    DefaultHandlerExpand,
    NotStandalone,
    ExternalEntityRef,
    StartDoctypeDecl,
    EndDoctypeDecl,
    EntityDecl,
    XmlDecl,
    ElementDecl,
    AttlistDecl,
#if XML_COMBINED_VERSION >= 19504
    SkippedEntity,
#endif
};


static sky_type_t   sky_pyexpat_ExpatError = NULL;

static sky_type_t   sky_pyexpat_xmlparser_type = NULL;

typedef struct sky_pyexpat_xmlparser_data_s {
    XML_Parser                          parser;

    sky_error_record_t *                error;

    XML_Char *                          buffer;
    ssize_t                             buffer_size;
    ssize_t                             buffer_used;

    unsigned int                        ordered_attributes : 1;
    unsigned int                        specified_attributes : 1;
    unsigned int                        in_callback : 1;
    unsigned int                        ns_prefixes : 1;

    sky_dict_t                          intern;
    sky_object_t *                      handlers;
} sky_pyexpat_xmlparser_data_t;

SKY_STATIC_INLINE sky_pyexpat_xmlparser_data_t *
sky_pyexpat_xmlparser_data(sky_object_t object)
{
    return sky_object_data(object, sky_pyexpat_xmlparser_type);
}

static sky_object_t
sky_pyexpat_xmlparser_callhandler(
                sky_object_t    self,
                sky_object_t    handler,
                sky_tuple_t     args);

static void
sky_pyexpat_xmlparser_error(sky_object_t self, sky_error_record_t *error);

static sky_object_t
sky_pyexpat_xmlparser_handler(sky_object_t self, unsigned type);

static void
sky_pyexpat_xmlparser_flush(sky_object_t self);

static sky_string_t
sky_pyexpat_xmlparser_string(
                sky_object_t    self,
                const XML_Char *xml_string,
                ssize_t         len);


SKY_STATIC_INLINE sky_string_t
sky_pyexpat_XML_Char(const XML_Char *xml_string, ssize_t len)
{
    if (!xml_string) {
        return (sky_string_t)sky_None;
    }
#if defined(XML_UNICODE)
    sky_error_validate_debug(sizeof(XML_Char) == 2);
    len = (len >= 0 ? len : wcslen(xml_string)) * sizeof(wchar_t);
    return sky_string_createfrombytes(xml_string,
                                      len,
                                      SKY_STRING_LITERAL("UTF-16"),
                                      NULL);
#else
    sky_error_validate_debug(sizeof(XML_Char) == 1);
    len = (len >= 0 ? len : (ssize_t)(strlen(xml_string) * sizeof(char)));
    return sky_string_createfrombytes(xml_string,
                                      len,
                                      NULL,
                                      NULL);
#endif
}

static sky_tuple_t
sky_pyexpat_XML_Content(const XML_Content *model)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_tuple_t     children;
        sky_object_t    *objects;
        unsigned int    i;

        if (!model->numchildren) {
            children = sky_tuple_empty;
        }
        else {
            objects = sky_asset_malloc(model->numchildren * sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);
            for (i = 0; i < model->numchildren; ++i) {
                objects[i] = sky_pyexpat_XML_Content(&(model->children[i]));
            }
            children = sky_tuple_createwitharray(model->numchildren,
                                                 objects,
                                                 sky_free);
        }
        result = sky_object_build("(uuOO)",
                                  model->type,
                                  model->quant,
                                  sky_pyexpat_XML_Char(model->name, -1),
                                  children);
    } SKY_ASSET_BLOCK_END;

    return result;
}

SKY_STATIC_INLINE sky_string_t
sky_pyexpat_XML_LChar(const XML_LChar *xml_string)
{
    if (!xml_string) {
        return (sky_string_t)sky_None;
    }
#if defined(XML_UNICODE_WCHAR_T)
    sky_error_validate_debug(sizeof(XML_LChar) == 2);
    return sky_string_createfrombytes(xml_string,
                                      wcslen(xml_string) * sizeof(wchar_t),
                                      SKY_STRING_LITERAL("UTF-16"),
                                      NULL);
#else
    sky_error_validate_debug(sizeof(XML_LChar) == 1);
    return sky_string_createfrombytes(xml_string,
                                      strlen(xml_string),
                                      NULL,
                                      NULL);
#endif
}


static int
sky_pyexpat_ExternalEntityRefHandler_NOOP(
    SKY_UNUSED  XML_Parser      parser,
    SKY_UNUSED  const XML_Char *context,
    SKY_UNUSED  const XML_Char *base,
    SKY_UNUSED  const XML_Char *systemId,
    SKY_UNUSED  const XML_Char *publicId)
{
    return XML_STATUS_ERROR;
}

static int
sky_pyexpat_UnknownEncodingHandler(
    SKY_UNUSED  void *          encodingHandlerData,
                const XML_Char *name,
                XML_Encoding *  info)
{
    static uint8_t  template_buffer[256];

    int i, result;

    if (!template_buffer[1]) {
        for (i = 0; i < 256; ++i) {
            template_buffer[i] = i;
        }
    }

    SKY_ERROR_TRY {
        sky_bytes_t         bytes;
        sky_string_t        string;
        sky_string_data_t   *string_data;

        bytes = sky_bytes_createwithbytes(template_buffer,
                                          sizeof(template_buffer),
                                          NULL);
        string = sky_codec_decode(NULL,
                                  sky_pyexpat_XML_Char(name, -1),
                                  SKY_STRING_LITERAL("replace"),
                                  NULL,
                                  bytes);
        string_data = sky_string_data(string, NULL);

        if (string_data->len != 256) {
            sky_error_raise_string(sky_ValueError,
                                   "multi-byte encodings are not supported");
        }
        if (string_data->highest_codepoint < 0x100) {
            for (i = 0; i < 256; ++i) {
                info->map[i] = string_data->data.latin1[i];
            }
        }
        else if (string_data->highest_codepoint < 0x10000) {
            for (i = 0; i < 256; ++i) {
                if (0xFFFD == string_data->data.ucs2[i]) {
                    info->map[i] = -1;
                }
                else {
                    info->map[i] = string_data->data.ucs2[i];
                }
            }
        }
        else {
            for (i = 0; i < 256; ++i) {
                if (0xFFFD == string_data->data.ucs4[i]) {
                    info->map[i] = -1;
                }
                else {
                    info->map[i] = string_data->data.ucs4[i];
                }
            }
        }

        info->data = NULL;
        info->convert = NULL;
        info->release = NULL;

        result = XML_STATUS_OK;
    } SKY_ERROR_EXCEPT_ANY {
        result = XML_STATUS_ERROR;
    } SKY_ERROR_TRY_END;

    return result;
}


static void
sky_pyexpat_StartElementHandler(
                void *          userData,
                const XML_Char *name,
                const XML_Char *atts[])
    /* atts is array of name/value pairs, terminated by 0;
     * names and values are 0 terminated.
     */
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(userData);

    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  StartElement)))
    {
        return;
    }

    SKY_ERROR_TRY {
        int             count, i;
        sky_tuple_t     args;
        sky_object_t    attributes;
        sky_string_t    string;

        sky_pyexpat_xmlparser_flush(userData);

        if (self_data->specified_attributes) {
            count = XML_GetSpecifiedAttributeCount(self_data->parser);
        }
        else {
            for (count = 0; atts[count]; count += 2);
        }
        if (self_data->ordered_attributes) {
            attributes = sky_list_createwithcapacity(count / 2);
        }
        else {
            attributes = sky_dict_createwithcapacity(count / 2);
        }
        for (i = 0; i < count; i += 2) {
            sky_string_t    k, v;

            k = sky_pyexpat_xmlparser_string(userData, atts[i], -1);
            v = sky_pyexpat_xmlparser_string(userData, atts[i + 1], -1);
            if (!self_data->ordered_attributes) {
                sky_dict_setitem(attributes, k, v);
            }
            else {
                sky_list_append(attributes, k);
                sky_list_append(attributes, v);
            }
        }
        string = sky_pyexpat_xmlparser_string(userData, name, -1);
        args = sky_tuple_pack(2, string, attributes);
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_EndElementHandler(
                void *          userData,
                const XML_Char *name)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  EndElement)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(1,
                sky_pyexpat_xmlparser_string(userData, name, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_ProcessingInstructionHandler(
                void *          userData,
                const XML_Char *target,
                const XML_Char *data)
    /* target and data are 0 terminated */
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  ProcessingInstruction)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(2,
                sky_pyexpat_xmlparser_string(userData, target, -1),
                sky_pyexpat_XML_Char(data, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_CharacterDataHandler(
                void *          userData,
                const XML_Char *s,
                int             len)
    /* s is not 0 terminated. */
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(userData);

    SKY_ERROR_TRY {
        sky_tuple_t     args = NULL;
        sky_object_t    handler;

        if (!self_data->buffer) {
            handler = sky_pyexpat_xmlparser_handler(userData, CharacterData);
            args = sky_tuple_pack(1, sky_pyexpat_XML_Char(s, len));
        }
        else {
            if (self_data->buffer_used + len > self_data->buffer_size) {
                sky_pyexpat_xmlparser_flush(userData);
            }
            handler = sky_pyexpat_xmlparser_handler(userData, CharacterData);
            if (handler) {
                if (len > self_data->buffer_size) {
                    args = sky_tuple_pack(1, sky_pyexpat_XML_Char(s, len));
                }
                else {
                    memcpy(self_data->buffer + self_data->buffer_used,
                           s,
                           len * sizeof(XML_Char));
                    self_data->buffer_used += len;
                }
            }
        }

        if (handler && args) {
            sky_pyexpat_xmlparser_callhandler(userData, handler, args);
        }
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_UnparsedEntityDeclHandler(
                void *          userData,
                const XML_Char *entityName,
                const XML_Char *base,
                const XML_Char *systemId,
                const XML_Char *publicId,
                const XML_Char *notationName)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  UnparsedEntityDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(5,
                sky_pyexpat_xmlparser_string(userData, entityName, -1),
                sky_pyexpat_xmlparser_string(userData, base, -1),
                sky_pyexpat_xmlparser_string(userData, systemId, -1),
                sky_pyexpat_xmlparser_string(userData, publicId, -1),
                sky_pyexpat_xmlparser_string(userData, notationName, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_NotationDeclHandler(
                void *          userData,
                const XML_Char *notationName,
                const XML_Char *base,
                const XML_Char *systemId,
                const XML_Char *publicId)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  NotationDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(4,
                sky_pyexpat_xmlparser_string(userData, notationName, -1),
                sky_pyexpat_xmlparser_string(userData, base, -1),
                sky_pyexpat_xmlparser_string(userData, systemId, -1),
                sky_pyexpat_xmlparser_string(userData, publicId, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_StartNamespaceDeclHandler(
                void *          userData,
                const XML_Char *prefix,
                const XML_Char *uri)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  StartNamespaceDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(2,
                sky_pyexpat_xmlparser_string(userData, prefix, -1),
                sky_pyexpat_xmlparser_string(userData, uri, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_EndNamespaceDeclHandler(
                void *          userData,
                const XML_Char *prefix)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  EndNamespaceDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(1,
                sky_pyexpat_xmlparser_string(userData, prefix, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_CommentHandler(
                void *userData,
                const XML_Char *data)
    /* data is 0 terminated */
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  Comment)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(1,
                sky_pyexpat_xmlparser_string(userData, data, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_StartCdataSectionHandler(void *userData)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  StartCdataSection)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_pyexpat_xmlparser_flush(userData);
        sky_pyexpat_xmlparser_callhandler(userData, handler, NULL);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_EndCdataSectionHandler(void *userData)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  EndCdataSection)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_pyexpat_xmlparser_flush(userData);
        sky_pyexpat_xmlparser_callhandler(userData, handler, NULL);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_DefaultHandler(
                void *          userData,
                const XML_Char *s,
                int             len)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  Default)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(1,
                sky_pyexpat_XML_Char(s, len));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_DefaultHandlerExpand(
                void *          userData,
                const XML_Char *s,
                int             len)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  DefaultHandlerExpand)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(1,
                sky_pyexpat_XML_Char(s, len));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static int
sky_pyexpat_NotStandaloneHandler(void *userData)
{
    int             result;
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  NotStandalone)))
    {
        return XML_STATUS_ERROR;
    }

    SKY_ERROR_TRY {
        sky_object_t    o;

        sky_pyexpat_xmlparser_flush(userData);
        o = sky_pyexpat_xmlparser_callhandler(userData, handler, NULL);
        result = sky_integer_value(o, INT_MIN, INT_MAX, NULL);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;

    return result;
}

static int
sky_pyexpat_ExternalEntityRefHandler(
                XML_Parser      parser,
                const XML_Char *context,
                const XML_Char *base,
                const XML_Char *systemId,
                const XML_Char *publicId)
{
    void    *userData = XML_GetUserData(parser);

    int             result;
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  ExternalEntityRef)))
    {
        return XML_STATUS_ERROR;
    }

    SKY_ERROR_TRY {
        sky_tuple_t     args;
        sky_object_t    o;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(4,
                sky_pyexpat_XML_Char(context, -1),
                sky_pyexpat_xmlparser_string(userData, base, -1),
                sky_pyexpat_xmlparser_string(userData, systemId, -1),
                sky_pyexpat_xmlparser_string(userData, publicId, -1));
        o = sky_pyexpat_xmlparser_callhandler(userData, handler, args);
        result = sky_integer_value(o, INT_MIN, INT_MAX, NULL);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;

    return result;
}

static void
sky_pyexpat_StartDoctypeDeclHandler(
                void *          userData,
                const XML_Char *doctypeName,
                const XML_Char *sysid,
                const XML_Char *pubid,
                int             has_internal_subset)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  StartDoctypeDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(4,
                sky_pyexpat_xmlparser_string(userData, doctypeName, -1),
                sky_pyexpat_xmlparser_string(userData, sysid, -1),
                sky_pyexpat_xmlparser_string(userData, pubid, -1),
                sky_integer_create(has_internal_subset));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_EndDoctypeDeclHandler(void *userData)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  EndDoctypeDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_pyexpat_xmlparser_flush(userData);
        sky_pyexpat_xmlparser_callhandler(userData, handler, NULL);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_EntityDeclHandler(
                void *          userData,
                const XML_Char *entityName,
                int             is_parameter_entity,
                const XML_Char *value,
                int             value_length,
                const XML_Char *base,
                const XML_Char *systemId,
                const XML_Char *publicId,
                const XML_Char *notationName)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  EntityDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(7,
                sky_pyexpat_xmlparser_string(userData, entityName, -1),
                sky_integer_create(is_parameter_entity),
                sky_pyexpat_XML_Char(value, value_length),
                sky_pyexpat_xmlparser_string(userData, base, -1),
                sky_pyexpat_xmlparser_string(userData, systemId, -1),
                sky_pyexpat_xmlparser_string(userData, publicId, -1),
                sky_pyexpat_xmlparser_string(userData, notationName, -1));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_XmlDeclHandler(
                void *          userData,
                const XML_Char *version,
                const XML_Char *encoding,
                int             standalone)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  XmlDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(3,
                sky_pyexpat_XML_Char(version, -1),
                sky_pyexpat_XML_Char(encoding, -1),
                sky_integer_create(standalone));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_ElementDeclHandler(
                void *          userData,
                const XML_Char *name,
                XML_Content *   model)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  ElementDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(2,
                sky_pyexpat_xmlparser_string(userData, name, -1),
                sky_pyexpat_XML_Content(model));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

static void
sky_pyexpat_AttlistDeclHandler(
                void *          userData,
                const XML_Char *elname,
                const XML_Char *attname,
                const XML_Char *att_type,
                const XML_Char *dflt,
                int             isrequired)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  AttlistDecl)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(5,
                sky_pyexpat_xmlparser_string(userData, elname, -1),
                sky_pyexpat_xmlparser_string(userData, attname, -1),
                sky_pyexpat_XML_Char(att_type, -1),
                sky_pyexpat_XML_Char(dflt, -1),
                sky_integer_create(isrequired));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}

#if XML_COMBINED_VERSION >= 19504
static void
sky_pyexpat_SkippedEntityHandler(
                void *          userData,
                const XML_Char *entityName,
                int             is_parameter_entity)
{
    sky_object_t    handler;

    if (!(handler = sky_pyexpat_xmlparser_handler(userData,
                                                  SkippedEntity)))
    {
        return;
    }

    SKY_ERROR_TRY {
        sky_tuple_t args;

        sky_pyexpat_xmlparser_flush(userData);
        args = sky_tuple_pack(2,
                sky_pyexpat_xmlparser_string(userData, entityName, -1),
                sky_integer_create(is_parameter_entity));
        sky_pyexpat_xmlparser_callhandler(userData, handler, args);
    } SKY_ERROR_EXCEPT_ANY {
        sky_pyexpat_xmlparser_error(userData, sky_error_current());
    } SKY_ERROR_TRY_END;
}
#endif


typedef void (*sky_pyexpat_handler_setter_t)(XML_Parser parser,
                                             void *     method);

typedef struct sky_pyexpat_handler_s {
    const char *                        name;
    sky_pyexpat_handler_setter_t        setter;
    void *                              handler;
} sky_pyexpat_handler_t;

static sky_pyexpat_handler_t sky_pyexpat_handlers[] = {
    {
        "StartElementHandler",
        (sky_pyexpat_handler_setter_t)XML_SetStartElementHandler,
        sky_pyexpat_StartElementHandler,
    },
    {
        "EndElementHandler",
        (sky_pyexpat_handler_setter_t)XML_SetEndElementHandler,
        sky_pyexpat_EndElementHandler,
    },
    {
        "ProcessingInstructionHandler",
        (sky_pyexpat_handler_setter_t)XML_SetProcessingInstructionHandler,
        sky_pyexpat_ProcessingInstructionHandler,
    },
    {
        "CharacterDataHandler",
        (sky_pyexpat_handler_setter_t)XML_SetCharacterDataHandler,
        sky_pyexpat_CharacterDataHandler,
    },
    {
        "UnparsedEntityDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetUnparsedEntityDeclHandler,
        sky_pyexpat_UnparsedEntityDeclHandler,
    },
    {
        "NotationDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetNotationDeclHandler,
        sky_pyexpat_NotationDeclHandler,
    },
    {
        "StartNamespaceDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetStartNamespaceDeclHandler,
        sky_pyexpat_StartNamespaceDeclHandler,
    },
    {
        "EndNamespaceDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetEndNamespaceDeclHandler,
        sky_pyexpat_EndNamespaceDeclHandler,
    },
    {
        "CommentHandler",
        (sky_pyexpat_handler_setter_t)XML_SetCommentHandler,
        sky_pyexpat_CommentHandler,
    },
    {
        "StartCdataSectionHandler",
        (sky_pyexpat_handler_setter_t)XML_SetStartCdataSectionHandler,
        sky_pyexpat_StartCdataSectionHandler,
    },
    {
        "EndCdataSectionHandler",
        (sky_pyexpat_handler_setter_t)XML_SetEndCdataSectionHandler,
        sky_pyexpat_EndCdataSectionHandler,
    },
    {
        "DefaultHandler",
        (sky_pyexpat_handler_setter_t)XML_SetDefaultHandler,
        sky_pyexpat_DefaultHandler,
    },
    {
        "DefaultHandlerExpand",
        (sky_pyexpat_handler_setter_t)XML_SetDefaultHandlerExpand,
        sky_pyexpat_DefaultHandlerExpand,
    },
    {
        "NotStandaloneHandler",
        (sky_pyexpat_handler_setter_t)XML_SetNotStandaloneHandler,
        sky_pyexpat_NotStandaloneHandler,
    },
    {
        "ExternalEntityRefHandler",
        (sky_pyexpat_handler_setter_t)XML_SetExternalEntityRefHandler,
        sky_pyexpat_ExternalEntityRefHandler,
    },
    {
        "StartDoctypeDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetStartDoctypeDeclHandler,
        sky_pyexpat_StartDoctypeDeclHandler,
    },
    {
        "EndDoctypeDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetEndDoctypeDeclHandler,
        sky_pyexpat_EndDoctypeDeclHandler,
    },
    {
        "EntityDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetEntityDeclHandler,
        sky_pyexpat_EntityDeclHandler,
    },
    {
        "XmlDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetXmlDeclHandler,
        sky_pyexpat_XmlDeclHandler,
    },
    {
        "ElementDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetElementDeclHandler,
        sky_pyexpat_ElementDeclHandler,
    },
    {
        "AttlistDeclHandler",
        (sky_pyexpat_handler_setter_t)XML_SetAttlistDeclHandler,
        sky_pyexpat_AttlistDeclHandler,
    },
#if XML_COMBINED_VERSION >= 19504
    {
        "SkippedEntityHandler",
        (sky_pyexpat_handler_setter_t)XML_SetSkippedEntityHandler,
        sky_pyexpat_SkippedEntityHandler,
    },
#endif
};
static size_t sky_pyexpat_handler_count =
                        (sizeof(sky_pyexpat_handlers) /
                         sizeof(sky_pyexpat_handlers[0]));

static void
sky_pyexpat_xmlparser_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky_pyexpat_xmlparser_data_t    *self_data = data;

    self_data->buffer_size = 8192;
}

static void
sky_pyexpat_xmlparser_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky_pyexpat_xmlparser_data_t    *self_data = data;

    if (self_data->error) {
        sky_error_record_release(self_data->error);
        self_data->error = NULL;
    }
    if (self_data->parser) {
        XML_ParserFree(self_data->parser);
        self_data->parser = NULL;
    }
    if (self_data->handlers) {
        sky_free(self_data->handlers);
        self_data->handlers = NULL;
    }
    if (self_data->buffer) {
        sky_free(self_data->buffer);
        self_data->buffer = NULL;
    }
}

static void
sky_pyexpat_xmlparser_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_pyexpat_xmlparser_data_t    *self_data = data;

    sky_object_visit(self_data->intern, visit_data);
    if (self_data->handlers) {
        size_t  i;

        for (i = 0;  i < sky_pyexpat_handler_count; ++i) {
            sky_object_visit(self_data->handlers[i], visit_data);
        }
    }
}


static void SKY_NORETURN
sky_pyexpat_xmlparser_raise_(
                sky_object_t    self,
                enum XML_Error  error_code,
    SKY_UNUSED  const char *    filename,
    SKY_UNUSED  uint32_t        lineno)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    XML_Size        column, line;
    sky_object_t    exception;
    sky_string_t    msg;

    line = XML_GetErrorLineNumber(self_data->parser);
    column = XML_GetErrorColumnNumber(self_data->parser);
    msg = sky_string_createfromformat("%s: line %d, column %d",
                                      XML_ErrorString(error_code),
                                      line,
                                      column);

    exception = sky_object_call(sky_pyexpat_ExpatError,
                                sky_tuple_pack(1, msg),
                                NULL);

    sky_object_setattr(exception,
                       SKY_STRING_LITERAL("code"),
                       sky_integer_createfromunsigned(error_code));
    sky_object_setattr(exception,
                       SKY_STRING_LITERAL("offset"),
                       sky_integer_createfromunsigned(column));
    sky_object_setattr(exception,
                       SKY_STRING_LITERAL("lineno"),
                       sky_integer_createfromunsigned(line));

    sky_error_raise_object(sky_pyexpat_ExpatError, exception);
}

#define sky_pyexpat_xmlparser_raise(_s, _e) \
        sky_pyexpat_xmlparser_raise_(_s, _e, __FILE__, __LINE__)

static sky_object_t
sky_pyexpat_xmlparser_callhandler(
                sky_object_t    self,
                sky_object_t    handler,
                sky_tuple_t     args)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    sky_object_t    result;

    SKY_ERROR_TRY {
        sky_error_validate_debug(0 == self_data->in_callback);
        self_data->in_callback = 1;
        result = sky_object_call(handler, args, NULL);
        self_data->in_callback = 0;
    } SKY_ERROR_EXCEPT_ANY {
        self_data->in_callback = 0;
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    return result;
}

static void
sky_pyexpat_xmlparser_delattr(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  sky_string_t    name)
{
    sky_error_raise_string(sky_RuntimeError, "Cannot delete attribute");
}

static sky_list_t
sky_pyexpat_xmlparser_dir(SKY_UNUSED sky_object_t self)
{
    size_t      i;
    sky_list_t  list;

    list = sky_list_createwithcapacity(sky_pyexpat_handler_count + 14);
    for (i = 0; i < sky_pyexpat_handler_count; ++i) {
        sky_list_append(
                list,
                SKY_STRING_LITERAL(sky_pyexpat_handlers[i].name));
    }

    sky_list_append(list, SKY_STRING_LITERAL("ErrorCode"));
    sky_list_append(list, SKY_STRING_LITERAL("ErrorLineNumber"));
    sky_list_append(list, SKY_STRING_LITERAL("ErrorColumnNumber"));
    sky_list_append(list, SKY_STRING_LITERAL("ErrorByteIndex"));
    sky_list_append(list, SKY_STRING_LITERAL("CurrentLineNumber"));
    sky_list_append(list, SKY_STRING_LITERAL("CurrentColumnNumber"));
    sky_list_append(list, SKY_STRING_LITERAL("CurrentByteInde"));
    sky_list_append(list, SKY_STRING_LITERAL("buffer_size"));
    sky_list_append(list, SKY_STRING_LITERAL("buffer_text"));
    sky_list_append(list, SKY_STRING_LITERAL("buffer_used"));
    sky_list_append(list, SKY_STRING_LITERAL("namespace_prefixes"));
    sky_list_append(list, SKY_STRING_LITERAL("ordered_attributes"));
    sky_list_append(list, SKY_STRING_LITERAL("specified_attributes"));
    sky_list_append(list, SKY_STRING_LITERAL("intern"));

    return list;
}

static void
sky_pyexpat_xmlparser_error(sky_object_t self, sky_error_record_t *error)
    /* This is called when an exception is raised inside of a handler. The
     * normal exception machinery cannot force its way out of expat, so the
     * exception must be deferred. The idea is to get expat to stop parsing as
     * quickly as possible, which means clearing out all handlers to try to
     * prevent any more Python code from getting executed. The error record is
     * stored for later, chaining with any existing stored error record. When
     * the parser returns control to the call that invoked it, it will be safe
     * to finally raise the exception.
     */
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    size_t  i;

    if (self_data->error) {
        sky_BaseException_setcause(error->value, self_data->error->value);
        sky_error_record_release(self_data->error);
    }
    self_data->error = error;
    sky_error_record_retain(self_data->error);

    for (i = 0; i < sky_pyexpat_handler_count; ++i) {
        self_data->handlers[i] = NULL;
    }
    XML_SetExternalEntityRefHandler(
            self_data->parser,
            sky_pyexpat_ExternalEntityRefHandler_NOOP);
}

static sky_object_t
sky_pyexpat_xmlparser_ExternalEntityParserCreate(
                sky_object_t    self,
                sky_string_t    context,
                sky_string_t    encoding)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    sky_object_t                    xmlparser;
    sky_pyexpat_xmlparser_data_t    *xmlparser_data;

    SKY_ASSET_BLOCK_BEGIN {
        char    *ccontext, *cencoding;
        size_t  i;

        ccontext = sky_codec_encodetocstring(context, 0);
        sky_asset_save(ccontext, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (!encoding) {
            cencoding = NULL;
        }
        else {
            cencoding = sky_codec_encodetocstring(encoding, 0);
            sky_asset_save(cencoding, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        xmlparser = sky_object_allocate(sky_pyexpat_xmlparser_type);
        xmlparser_data = sky_pyexpat_xmlparser_data(xmlparser);

        sky_object_gc_set(SKY_AS_OBJECTP(&(xmlparser_data->intern)),
                          self_data->intern,
                          xmlparser);

        xmlparser_data->buffer_size = self_data->buffer_size;
        xmlparser_data->ordered_attributes = self_data->ordered_attributes;
        xmlparser_data->specified_attributes = self_data->specified_attributes;
        xmlparser_data->ns_prefixes = self_data->ns_prefixes;

        xmlparser_data->parser =
                XML_ExternalEntityParserCreate(self_data->parser,
                                               ccontext,
                                               cencoding);
        if (!xmlparser_data->parser) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        XML_SetUserData(xmlparser_data->parser, xmlparser);

        if (self_data->buffer) {
            xmlparser_data->buffer = sky_malloc(xmlparser_data->buffer_size);
        }

        xmlparser_data->handlers = sky_calloc(sky_pyexpat_handler_count,
                                              sizeof(sky_object_t));
        for (i = 0; i < sky_pyexpat_handler_count; ++i) {
            sky_object_gc_set(&(xmlparser_data->handlers[i]),
                              self_data->handlers[i],
                              xmlparser);
        }
    } SKY_ASSET_BLOCK_END;

    return xmlparser;
}

static const char sky_pyexpat_xmlparser_ExternalEntityParserCreate_doc[] =
"ExternalEntityParserCreate(context[, encoding])\n"
"Create a parser for parsing an external entity based on the\n"
"information passed to the ExternalEntityRefHandler.";

static void
sky_pyexpat_xmlparser_flush(sky_object_t self)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    if (self_data->buffer_used) {
        sky_object_t    handler;

        handler = sky_pyexpat_xmlparser_handler(self, CharacterData);
        if (handler) {
            sky_string_t    string;

            string = sky_pyexpat_XML_Char(self_data->buffer,
                                          self_data->buffer_used);
            sky_pyexpat_xmlparser_callhandler(self,
                                              handler,
                                              sky_tuple_pack(1, string));
        }
        self_data->buffer_used = 0;
    }
}

static sky_object_t
sky_pyexpat_xmlparser_getattribute(sky_object_t self, sky_string_t name)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    const char  *cname;

    if (!(cname = (const char *)sky_string_cstring(name))) {
        return sky_base_object_getattribute(self, name);
    }

    if (!strcmp(cname, "buffer_size")) {
        return sky_integer_create(self_data->buffer_size);
    }
    if (!strcmp(cname, "buffer_text")) {
        return (self_data->buffer ? sky_True : sky_False);
    }
    if (!strcmp(cname, "buffer_used")) {
        return sky_integer_create(self_data->buffer_used);
    }

    if (!strcmp(cname, "CurrentLineNumber")) {
        return sky_integer_createfromunsigned(XML_GetCurrentLineNumber(self_data->parser));
    }
    if (!strcmp(cname, "CurrentColumnNumber")) {
        return sky_integer_createfromunsigned(XML_GetCurrentColumnNumber(self_data->parser));
    }
    if (!strcmp(cname, "CurrentByteIndex")) {
        return sky_integer_createfromunsigned(XML_GetCurrentByteIndex(self_data->parser));
    }

    if (!strcmp(cname, "ErrorCode")) {
        return sky_integer_createfromunsigned(XML_GetErrorCode(self_data->parser));
    }
    if (!strcmp(cname, "ErrorLineNumber")) {
        return sky_integer_createfromunsigned(XML_GetErrorLineNumber(self_data->parser));
    }
    if (!strcmp(cname, "ErrorColumnNumber")) {
        return sky_integer_createfromunsigned(XML_GetErrorColumnNumber(self_data->parser));
    }
    if (!strcmp(cname, "ErrorByteIndex")) {
        return sky_integer_createfromunsigned(XML_GetErrorByteIndex(self_data->parser));
    }

    if (!strcmp(cname, "intern")) {
        if (!self_data->intern) {
            return sky_None;
        }
        return self_data->intern;
    }
    if (!strcmp(cname, "namespace_prefixes")) {
        return (self_data->ns_prefixes ? sky_True : sky_False);
    }
    if (!strcmp(cname, "ordered_attributes")) {
        return (self_data->ordered_attributes ? sky_True : sky_False);
    }
    if (!strcmp(cname, "specified_attributes")) {
        return (self_data->specified_attributes ? sky_True : sky_False);
    }

    return sky_base_object_getattribute(self, name);
}

static sky_string_t
sky_pyexpat_xmlparser_GetBase(sky_object_t self)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    return sky_pyexpat_XML_Char(XML_GetBase(self_data->parser), -1);
}

static const char sky_pyexpat_xmlparser_GetBase_doc[] =
"GetBase() -> url\n"
"Return base URL string for the parser.";

static sky_bytes_t
sky_pyexpat_xmlparser_GetInputContext(sky_object_t self)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    int         offset, size;
    const char  *buffer;

    if (!self_data->in_callback) {
        return (sky_bytes_t)sky_None;
    }

    if (!(buffer = XML_GetInputContext(self_data->parser, &offset, &size))) {
        return (sky_bytes_t)sky_None;
    }
    return sky_bytes_createfrombytes(buffer + offset, size - offset);
}

static const char sky_pyexpat_xmlparser_GetInputContext_doc[] =
"GetInputContext() -> string\n"
"Return the untranslated text of the input that caused the current event.\n"
"If the event was generated by a large amount of text (such as a start tag\n"
"for an element with many attributes), not all of the text may be available.";

static sky_object_t
sky_pyexpat_xmlparser_handler(sky_object_t self, unsigned type)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    if (self_data->error || !self_data->handlers) {
        return NULL;
    }

    return self_data->handlers[type];
}


static int
sky_pyexpat_xmlparser_ParseResult(sky_object_t self, enum XML_Status rc)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    sky_error_record_t  *error;

    if ((error = self_data->error) != NULL) {
        SKY_ASSET_BLOCK_BEGIN {
            self_data->error = NULL;
            sky_asset_save(error,
                           (sky_free_t)sky_error_record_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            sky_error_raise_record(error);
        } SKY_ASSET_BLOCK_END;
    }
    if (XML_STATUS_OK != rc) {
        sky_pyexpat_xmlparser_raise(self, XML_GetErrorCode(self_data->parser));
    }

    return (int)rc;
}

static int
sky_pyexpat_xmlparser_Parse(
                sky_object_t    self,
                sky_object_t    data,
                sky_bool_t      isfinal)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    int result;

    if (sky_object_isa(data, sky_string_type)) {
        data = sky_codec_encode(NULL, data, NULL, NULL, 0);
        XML_SetEncoding(self_data->parser, "utf-8");
    }

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         len;
        const char      *buf;
        sky_buffer_t    buffer;
        enum XML_Status rc;

        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

#define MAX_CHUNK_SIZE (1 << 20)

        buf = buffer.buf;
        len = buffer.len;
        while (len > MAX_CHUNK_SIZE) {
            rc = XML_Parse(self_data->parser, buf, MAX_CHUNK_SIZE, 0);
            sky_pyexpat_xmlparser_ParseResult(self, rc);
            buf += MAX_CHUNK_SIZE;
            len -= MAX_CHUNK_SIZE;
        }

#undef MAX_CHUNK_SIZE

        rc = XML_Parse(self_data->parser, buf, len, isfinal);
        result = sky_pyexpat_xmlparser_ParseResult(self, rc);
        sky_pyexpat_xmlparser_flush(self);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_pyexpat_xmlparser_Parse_doc[] =
"Parse(data[, isfinal])\n"
"Parse XML data.  `isfinal' should be true at end of input.";

static int
sky_pyexpat_xmlparser_ParseFile(sky_object_t self, sky_object_t file)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    int             result;
    ssize_t         nbytes;
    sky_tuple_t     read_args;
    sky_object_t    read;

#define BUF_SIZE 2048

    if (!(read = sky_object_getattr(file, SKY_STRING_LITERAL("read"), NULL))) {
        sky_error_raise_string(sky_TypeError,
                               "argument must have 'read' attribute");
    }
    read_args = sky_tuple_pack(1, sky_integer_create(BUF_SIZE));

    do {
        void            *expat_buffer;
        sky_object_t    data;
        enum XML_Status rc;

        if (!(expat_buffer = XML_GetBuffer(self_data->parser, BUF_SIZE))) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }

        data = sky_object_call(read, read_args, NULL);
        if (!sky_object_isa(data, sky_bytes_type) &&
            !sky_object_isa(data, sky_bytearray_type))
        {
            sky_error_raise_format(
                    sky_TypeError,
                    "read() did not return a bytes object (type=%@)",
                    sky_type_name(sky_object_type(data)));
        }

        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t    buffer;

            sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
            if (buffer.len > BUF_SIZE) {
                sky_error_raise_format(
                        sky_TypeError,
                        "read() returned too much data: "
                        "%d bytes requested, %zd returned",
                        BUF_SIZE,
                        nbytes);
            }
            memcpy(expat_buffer, buffer.buf, buffer.len);
            nbytes = buffer.len;
        } SKY_ASSET_BLOCK_END;

        rc = XML_ParseBuffer(self_data->parser, nbytes, nbytes == 0);
        result = sky_pyexpat_xmlparser_ParseResult(self, rc);
    } while (nbytes != 0);

#undef BUF_SIZE

    return result;
}

static const char sky_pyexpat_xmlparser_ParseFile_doc[] =
"Parse(file)\n"
"Parse XML data from file-like object.";

static void
sky_pyexpat_xmlparser_setattr(
                sky_object_t    self,
                sky_string_t    name,
                sky_object_t    value)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    const char      *cname;
    unsigned int    handler;

    if (!(cname = (const char *)sky_string_cstring(name))) {
        sky_error_raise_object(sky_AttributeError, name);
        return;
    }

    if (!strcmp(cname, "buffer_size")) {
        ssize_t buffer_size;

        if (!sky_object_isa(value, sky_integer_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "buffer_size must be an integer");
        }
        buffer_size = sky_integer_value(value, SSIZE_MIN, SSIZE_MAX, NULL);
        if (buffer_size == self_data->buffer_size) {
            return;
        }
        if (buffer_size <= 0) {
            sky_error_raise_string(sky_ValueError,
                                   "buffer_size must be greater than zero");
        }
        if (buffer_size > INT_MAX) {
            sky_error_raise_format(sky_ValueError,
                                   "buffer_size must not be greater than %d",
                                   INT_MAX);
        }
        if (self_data->buffer) {
            sky_pyexpat_xmlparser_flush(self);
            sky_free(self_data->buffer);
        }
        self_data->buffer = sky_malloc(buffer_size);
        self_data->buffer_size = buffer_size;
        return;
    }

    if (!strcmp(cname, "buffer_text")) {
        if (sky_object_bool(value)) {
            if (!self_data->buffer) {
                self_data->buffer = sky_malloc(self_data->buffer_size);
            }
        }
        else {
            if (self_data->buffer) {
                sky_pyexpat_xmlparser_flush(self);
                sky_free(self_data->buffer);
                self_data->buffer = NULL;
            }
        }
        return;
    }

    if (!strcmp(cname, "namespace_prefixes")) {
        self_data->ns_prefixes = (sky_object_bool(value) ? 1 : 0);
        return;
    }
    if (!strcmp(cname, "ordered_attributes")) {
        self_data->ordered_attributes = (sky_object_bool(value) ? 1 : 0);
        return;
    }
    if (!strcmp(cname, "specified_attributes")) {
        self_data->specified_attributes = (sky_object_bool(value) ? 1 : 0);
        return;
    }

    if (!strcmp(cname, "CharacterDataHandler")) {
        sky_pyexpat_xmlparser_flush(self);
        handler = CharacterData;
    }
    else {
        size_t  i;

        for (i = 0; i < sky_pyexpat_handler_count; ++i) {
            if (!strcmp(sky_pyexpat_handlers[i].name, cname)) {
                handler = i;
                break;
            }
        }
        if (i >= sky_pyexpat_handler_count) {
            sky_error_raise_object(sky_AttributeError, name);
            return;
        }
    }

    if (sky_object_isnull(value)) {
        self_data->handlers[handler] = NULL;
    }
    else {
        sky_object_gc_set(&(self_data->handlers[handler]), value, self);
    }
    sky_pyexpat_handlers[handler].setter(
            self_data->parser,
            sky_pyexpat_handlers[handler].handler);
}

static void
sky_pyexpat_xmlparser_SetBase(sky_object_t self, sky_string_t base_url)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        char    *cbase_url;

        cbase_url = sky_codec_encodetocstring(base_url, 0);
        sky_asset_save(cbase_url, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        XML_SetBase(self_data->parser, cbase_url);
    } SKY_ASSET_BLOCK_END;
}

static const char sky_pyexpat_xmlparser_SetBase_doc[] =
"SetBase(base_url)\n"
"Set the base URL for the parser.";

static int
sky_pyexpat_xmlparser_SetParamEntityParsing(sky_object_t self, int flag)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    return XML_SetParamEntityParsing(self_data->parser, flag);
}

static const char sky_pyexpat_xmlparser_SetParamEntityParsing_doc[] =
"SetParamEntityParsing(flag) -> success\n"
"Controls parsing of parameter entities (including the external DTD\n"
"subset). Possible flag values are XML_PARAM_ENTITY_PARSING_NEVER,\n"
"XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE and\n"
"XML_PARAM_ENTITY_PARSING_ALWAYS. Returns true if setting the flag\n"
"was successful.";

static sky_string_t
sky_pyexpat_xmlparser_string(
                sky_object_t    self,
                const XML_Char *xml_string,
                ssize_t         len)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    sky_string_t    string, value;

    string = sky_pyexpat_XML_Char(xml_string, len);
    if (!self_data->intern) {
        return string;
    }
    if (!(value = sky_dict_get(self_data->intern, string, NULL))) {
        sky_dict_setitem(self_data->intern, string, string);
        return string;
    }

    return value;
}

#if XML_COMBINED_VERSION >= 19505
static void
sky_pyexpat_xmlparser_UseForeignDTD(sky_object_t self, sky_bool_t flag)
{
    sky_pyexpat_xmlparser_data_t    *self_data =
                                    sky_pyexpat_xmlparser_data(self);

    enum XML_Error  rc;

    rc = XML_UseForeignDTD(self_data->parser, (flag ? XML_TRUE : XML_FALSE));
    if (XML_ERROR_NONE != rc) {
        sky_pyexpat_xmlparser_raise(self, rc);
    }
}

static const char sky_pyexpat_xmlparser_UseForeignDTD_doc[] =
"UseForeignDTD([flag])\n"
"Allows the application to provide an artificial external subset if one is\n"
"not specified as part of the document instance.  This readily allows the\n"
"use of a 'default' document type controlled by the application, while still\n"
"getting the advantage of providing document type information to the parser.\n"
"'flag' defaults to True if not provided.";
#endif


static sky_string_t
sky_pyexpat_ErrorString(int errno)
{
    return sky_pyexpat_XML_LChar(XML_ErrorString(errno));
}

static const char sky_pyexpat_ErrorString_doc[] =
"ErrorString(errno) -> string\n"
"Returns string error for given number.";


static sky_object_t
sky_pyexpat_ParserCreate(
                sky_string_t    encoding,
                sky_object_t    namespace_separator,
                sky_object_t    intern)
{
    static XML_Memory_Handling_Suite    memory_suite =
                                        { sky_malloc, sky_realloc, sky_free };

    char                            *cencoding, *cnamespace_separator;
    sky_object_t                    xmlparser;
    sky_pyexpat_xmlparser_data_t    *xmlparser_data;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isnull(encoding)) {
            cencoding = NULL;
        }
        else {
            cencoding = sky_codec_encodetocstring(encoding, 0);
            sky_asset_save(cencoding, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (sky_object_isnull(namespace_separator)) {
            cnamespace_separator = NULL;
        }
        /* XXX satisfy the unit test's error message check */
        else if (!sky_object_isa(namespace_separator, sky_string_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "ParserCreate() argument 2 must be str or None, not %@",
                    sky_type_name(sky_object_type(namespace_separator)));
        }
        else if (sky_string_len(namespace_separator) > 1) {
            sky_error_raise_string(sky_ValueError,
                                   "namespace_separator must be "
                                   "at most one character, omitted, or None");
        }
        else {
            cnamespace_separator =
                    sky_codec_encodetocstring(namespace_separator, 0);
            sky_asset_save(cnamespace_separator,
                           sky_free,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (sky_None == intern) {
            intern = NULL;
        }
        else if (sky_object_isnull(intern)) {
            intern = sky_dict_create();
        }

        xmlparser = sky_object_allocate(sky_pyexpat_xmlparser_type);
        xmlparser_data = sky_pyexpat_xmlparser_data(xmlparser);
        sky_object_gc_set(SKY_AS_OBJECTP(&(xmlparser_data->intern)),
                          intern,
                          xmlparser);

        if (!(xmlparser_data->parser =
                    XML_ParserCreate_MM(cencoding,
                                        &memory_suite,
                                        cnamespace_separator)))
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "XML_ParserCreate failed");
        }
        XML_SetUserData(xmlparser_data->parser, xmlparser);
        XML_SetUnknownEncodingHandler(xmlparser_data->parser,
                                      sky_pyexpat_UnknownEncodingHandler,
                                      NULL);

        xmlparser_data->handlers = sky_calloc(sky_pyexpat_handler_count,
                                              sizeof(sky_object_t));
    } SKY_ASSET_BLOCK_END;

    return xmlparser;
}

static const char sky_pyexpat_ParserCreate_doc[] =
"ParserCreate([encoding[, namespace_separator]]) -> parser\n"
"Return a new XML parser object.";


const char *skython_module_pyexpat_doc =
"Python wrapper for Expat parser.";


void
skython_module_pyexpat_initialize(sky_module_t module)
{
    static const char   errors_module_doc[] =
                        "Constants used to describe error conditions.";
    static const char   model_module_doc[] =
                        "Constants used to interpret content model information.";

    sky_dict_t          codes, messages, modules;
    sky_type_t          type;
    sky_module_t        errors_module, model_module;
    sky_string_t        string;
    XML_Expat_Version   version_info;
    sky_type_template_t template;

#define error_const(_n)                                             \
        do {                                                        \
            sky_string_t    string;                                 \
            sky_integer_t   number;                                 \
                                                                    \
            number = sky_integer_createfromunsigned(_n);            \
            string = sky_pyexpat_XML_LChar(XML_ErrorString(_n));    \
            sky_module_setattr(errors_module, #_n, string);         \
            sky_dict_setitem(codes, string, number);                \
            sky_dict_setitem(messages, number, string);             \
        } while (0)
#define int_const(_n) \
        sky_module_setattr(module, #_n, sky_integer_create(_n))
#define model_const(_n) \
        sky_module_setattr(model_module, #_n, sky_integer_create(_n))

    sky_module_setattr(module,
                       "EXPAT_VERSION",
                       sky_pyexpat_XML_LChar(XML_ExpatVersion()));

    version_info = XML_ExpatVersionInfo();
    sky_module_setattr(
            module,
            "version_info",
            sky_object_build("(iii)",
                    (int)version_info.major,
                    (int)version_info.minor,
                    (int)version_info.micro));

    /* XXX  When Expat supports some way of figuring out how it was compiled,
     *      this should check and set native_encoding appropriately.
     */
    sky_module_setattr(module, "native_encoding", SKY_STRING_LITERAL("UTF-8"));

    int_const(XML_PARAM_ENTITY_PARSING_NEVER);
    int_const(XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE);
    int_const(XML_PARAM_ENTITY_PARSING_ALWAYS);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_pyexpat_ExpatError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("ExpatError"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_Exception),
                    NULL),
            SKY_TRUE);
    sky_type_setattr_builtin(sky_pyexpat_ExpatError,
                             "__module__",
                             SKY_STRING_LITERAL("xml.parsers.expat"));
    sky_module_setattr(module, "error", sky_pyexpat_ExpatError);
    sky_module_setattr(module, "ExpatError", sky_pyexpat_ExpatError);

    /* Set up the pyexpat.errors and pyexpat.model modules */
    modules = sky_interpreter_modules();

    string = SKY_STRING_LITERAL("pyexpat.errors");
    if (!(errors_module = sky_dict_get(modules, string, NULL))) {
        errors_module = sky_module_create(string);
        sky_interpreter_addmodule(errors_module);
        sky_module_setattr(module, "errors", errors_module);
    }
    sky_module_setattr(errors_module,
                       "__doc__",
                       sky_string_createfrombytes(errors_module_doc,
                                                  sizeof(errors_module_doc) - 1,
                                                  NULL,
                                                  NULL));

    codes = sky_dict_create();
    sky_module_setattr(errors_module, "codes", codes);
    messages = sky_dict_create();
    sky_module_setattr(errors_module, "messages", messages);

    error_const(XML_ERROR_NO_MEMORY);
    error_const(XML_ERROR_SYNTAX);
    error_const(XML_ERROR_NO_ELEMENTS);
    error_const(XML_ERROR_INVALID_TOKEN);
    error_const(XML_ERROR_UNCLOSED_TOKEN);
    error_const(XML_ERROR_PARTIAL_CHAR);
    error_const(XML_ERROR_TAG_MISMATCH);
    error_const(XML_ERROR_DUPLICATE_ATTRIBUTE);
    error_const(XML_ERROR_JUNK_AFTER_DOC_ELEMENT);
    error_const(XML_ERROR_PARAM_ENTITY_REF);
    error_const(XML_ERROR_UNDEFINED_ENTITY);
    error_const(XML_ERROR_RECURSIVE_ENTITY_REF);
    error_const(XML_ERROR_ASYNC_ENTITY);
    error_const(XML_ERROR_BAD_CHAR_REF);
    error_const(XML_ERROR_BINARY_ENTITY_REF);
    error_const(XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF);
    error_const(XML_ERROR_MISPLACED_XML_PI);
    error_const(XML_ERROR_UNKNOWN_ENCODING);
    error_const(XML_ERROR_INCORRECT_ENCODING);
    error_const(XML_ERROR_UNCLOSED_CDATA_SECTION);
    error_const(XML_ERROR_EXTERNAL_ENTITY_HANDLING);
    error_const(XML_ERROR_NOT_STANDALONE);
    error_const(XML_ERROR_UNEXPECTED_STATE);
    error_const(XML_ERROR_ENTITY_DECLARED_IN_PE);
    error_const(XML_ERROR_FEATURE_REQUIRES_XML_DTD);
    error_const(XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING);
#if XML_COMBINED_VERSION >= 19507
    /* Added in Expat 1.95.7. */
    error_const(XML_ERROR_UNBOUND_PREFIX);
#endif
#if XML_COMBINED_VERSION >= 19508
    /* Added in Expat 1.95.8. */
    error_const(XML_ERROR_UNDECLARING_PREFIX);
    error_const(XML_ERROR_INCOMPLETE_PE);
    error_const(XML_ERROR_XML_DECL);
    error_const(XML_ERROR_TEXT_DECL);
    error_const(XML_ERROR_PUBLICID);
    error_const(XML_ERROR_SUSPENDED);
    error_const(XML_ERROR_NOT_SUSPENDED);
    error_const(XML_ERROR_ABORTED);
    error_const(XML_ERROR_FINISHED);
    error_const(XML_ERROR_SUSPEND_PE);
#endif

#undef error_const

    string = SKY_STRING_LITERAL("pyexpat.model");
    if (!(model_module = sky_dict_get(modules, string, NULL))) {
        model_module = sky_module_create(string);
        sky_interpreter_addmodule(model_module);
        sky_module_setattr(module, "model", model_module);
    }
    sky_module_setattr(model_module,
                       "__doc__",
                       sky_string_createfrombytes(model_module_doc,
                                                  sizeof(model_module_doc) - 1,
                                                  NULL,
                                                  NULL));

    model_const(XML_CTYPE_EMPTY);
    model_const(XML_CTYPE_ANY);
    model_const(XML_CTYPE_MIXED);
    model_const(XML_CTYPE_NAME);
    model_const(XML_CTYPE_CHOICE);
    model_const(XML_CTYPE_SEQ);

    model_const(XML_CQUANT_NONE);
    model_const(XML_CQUANT_OPT);
    model_const(XML_CQUANT_REP);
    model_const(XML_CQUANT_PLUS);

#if XML_COMBINED_VERSION > 19505
    do {
        size_t              i;
        sky_list_t          list;
        const XML_Feature   *features;

        list = sky_list_create(NULL);
        features = XML_GetFeatureList();
        for (i = 0; XML_FEATURE_END != features[i].feature; ++i) {
            sky_list_append(
                    list,
                    sky_object_build(
                            "(Oij)",
                            sky_pyexpat_XML_LChar(features[i].name),
                            (intmax_t)features[i].value));
        }
        sky_module_setattr(module, "features", list);
    } while (0);
#endif

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky_pyexpat_xmlparser_instance_initialize;
    template.finalize = sky_pyexpat_xmlparser_instance_finalize;
    template.visit = sky_pyexpat_xmlparser_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("xmlparser"),
                                   "XML parser",
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_pyexpat_xmlparser_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__getattribute__", sky_pyexpat_xmlparser_getattribute,
            "__setattr__", sky_pyexpat_xmlparser_setattr,
            "__delattr__", sky_pyexpat_xmlparser_delattr,
            "__dir__", sky_pyexpat_xmlparser_dir,
            NULL);
    sky_type_setattr_builtin(
            type,
            "ExternalEntityParserCreate",
            sky_function_createbuiltin(
                    "xmlparser.ExternalEntityParserCreate",
                    sky_pyexpat_xmlparser_ExternalEntityParserCreate_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_ExternalEntityParserCreate,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "context", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "GetBase",
            sky_function_createbuiltin(
                    "xmlparser.GetBase",
                    sky_pyexpat_xmlparser_GetBase_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_GetBase,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "GetInputContext",
            sky_function_createbuiltin(
                    "xmlparser.GetInputContext",
                    sky_pyexpat_xmlparser_GetInputContext_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_GetInputContext,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "Parse",
            sky_function_createbuiltin(
                    "xmlparser.Parse",
                    sky_pyexpat_xmlparser_Parse_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_Parse,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "isfinal", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "ParseFile",
            sky_function_createbuiltin(
                    "xmlparser.ParseFile",
                    sky_pyexpat_xmlparser_ParseFile_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_ParseFile,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "file", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "SetBase",
            sky_function_createbuiltin(
                    "xmlparser.SetBase",
                    sky_pyexpat_xmlparser_SetBase_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_SetBase,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "base_url", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "SetParamEntityParsing",
            sky_function_createbuiltin(
                    "xmlparser.SetParamEntityParsing",
                    sky_pyexpat_xmlparser_SetParamEntityParsing_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_SetParamEntityParsing,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "flag", SKY_DATA_TYPE_INT, NULL,
                    NULL));
#if XML_COMBINED_VERSION >= 19505
    sky_type_setattr_builtin(
            type,
            "UseForeignDTD",
            sky_function_createbuiltin(
                    "xmlparser.UseForeignDTD",
                    sky_pyexpat_xmlparser_UseForeignDTD_doc,
                    (sky_native_code_function_t)sky_pyexpat_xmlparser_UseForeignDTD,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "flag", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
#endif
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_pyexpat_xmlparser_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "XMLParserType", type);

    sky_module_setattr(
            module,
            "ErrorString",
            sky_function_createbuiltin(
                    "ErrorString",
                    sky_pyexpat_ErrorString_doc,
                    (sky_native_code_function_t)sky_pyexpat_ErrorString,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "errno", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ParserCreate",
            sky_function_createbuiltin(
                    "ParserCreate",
                    sky_pyexpat_ParserCreate_doc,
                    (sky_native_code_function_t)sky_pyexpat_ParserCreate,
                    SKY_DATA_TYPE_OBJECT,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    "namespace_separator", SKY_DATA_TYPE_OBJECT, sky_None,
                    "intern", SKY_DATA_TYPE_OBJECT_DICT | SKY_DATA_TYPE_OBJECT_OR_NONE, sky_NotSpecified,
                    NULL));
}
