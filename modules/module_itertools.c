#include "../core/skython.h"
#include "../core/sky_object_private.h"


static sky_type_t sky_itertools_chain_type;

static const char sky_itertools_chain_type_doc[] =
"chain(*iterables) --> chain object\n\n"
"Return a chain object whose .__next__() method returns elements from the\n"
"first iterable until it is exhausted, then elements from the next\n"
"iterable, until all of the iterables are exhausted.";

typedef struct sky_itertools_chain_s *sky_itertools_chain_t;

typedef struct sky_itertools_chain_data_s {
    sky_object_t                        source;
    sky_object_t                        iterator;
} sky_itertools_chain_data_t;

SKY_EXTERN_INLINE sky_itertools_chain_data_t *
sky_itertools_chain_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_chain_type);
}

static void
sky_itertools_chain_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_chain_data_t  *self_data = data;

    sky_object_visit(self_data->source, visit_data);
    sky_object_visit(self_data->iterator, visit_data);
}

static const char sky_itertools_chain_from_iterable_doc[] =
"chain.from_iterable(iterable) --> chain object\n\n"
"Alternate chain() constructor taking a single iterable argument\n"
"that evaluates lazily.";

static sky_object_t
sky_itertools_chain_from_iterable(sky_type_t cls, sky_object_t iterable)
{
    sky_object_t                self;
    sky_itertools_chain_data_t  *self_data;

    self = sky_object_allocate(cls);
    self_data = sky_itertools_chain_data(self);
    sky_object_gc_set(&(self_data->source), sky_object_iter(iterable), self);

    return self;
}

static void
sky_itertools_chain_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_itertools_chain_data_t  *self_data;

    if (sky_object_bool(kws)) {
        sky_error_raise_format(sky_TypeError,
                               "%@() does not take keyword arguments",
                               sky_type_name(sky_object_type(self)));
    }

    self_data = sky_itertools_chain_data(self);
    self_data->iterator = NULL;
    sky_object_gc_set(&(self_data->source),
                      sky_object_iter((args ? args : sky_tuple_empty)),
                      self);
}

static sky_object_t
sky_itertools_chain_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_chain_next(sky_object_t self)
{
    sky_object_t                object;
    sky_itertools_chain_data_t  *self_data;

    self_data = sky_itertools_chain_data(self);
    if (!self_data->source) {
        return NULL;
    }
    for (;;) {
        if (self_data->iterator) {
            if ((object = sky_object_next(self_data->iterator, NULL)) != NULL) {
                break;
            }
            self_data->iterator = NULL;
        }

        if (!(object = sky_object_next(self_data->source, NULL))) {
            break;
        }
        sky_object_gc_set(&(self_data->iterator),
                          sky_object_iter(object),
                          self);
    }

    return object;
}

static sky_object_t
sky_itertools_chain_reduce(sky_object_t self)
{
    sky_itertools_chain_data_t  *self_data;

    self_data = sky_itertools_chain_data(self);
    if (!self_data->source) {
        return sky_object_build("(O())", sky_object_type(self));
    }
    if (self_data->iterator) {
        return sky_object_build("(O()(OO))", sky_object_type(self),
                                             self_data->source,
                                             self_data->iterator);
    }
    return sky_object_build("(O()(O))", sky_object_type(self),
                                        self_data->source);
}

static void
sky_itertools_chain_setstate(sky_object_t self, sky_object_t state)
{
    sky_itertools_chain_data_t  *self_data;

    self_data = sky_itertools_chain_data(self);
    if (sky_tuple_len(state) > 0) {
        sky_object_gc_set(&(self_data->source),
                          sky_tuple_get(state, 0),
                          self);
        if (sky_tuple_len(state) > 1) {
            sky_object_gc_set(&(self_data->iterator),
                              sky_tuple_get(state, 1),
                              self);
        }
    }
}


static sky_type_t sky_itertools_count_type;

static const char sky_itertools_count_type_doc[] =
"count(start=0, step=1) --> count object\n\n"
"Return a count object whose .__next__() method returns consecutive values.\n"
"Equivalent to:\n\n"
"    def count(start=0, step=1):\n"
"        x = start\n"
"        while True:\n"
"            yield x\n"
"            x += step\n";

typedef struct sky_itertools_count_s *sky_itertools_count_t;

typedef struct sky_itertools_count_data_s {
    intmax_t                            ivalue;
    intmax_t                            istep;
    sky_object_t                        value;
    sky_object_t                        step;
} sky_itertools_count_data_t;

SKY_EXTERN_INLINE sky_itertools_count_data_t *
sky_itertools_count_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_count_type);
}

static void
sky_itertools_count_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_count_data_t  *self_data = data;

    sky_object_visit(self_data->value, visit_data);
    sky_object_visit(self_data->step, visit_data);
}

static void
sky_itertools_count_init(sky_object_t   self,
                         sky_object_t   start,
                         sky_object_t   step)
{
    intmax_t                    istart, istep;
    sky_bool_t                  fast;
    sky_itertools_count_data_t  *self_data;

    if (!sky_number_check(start) || !sky_number_check(step)) {
        sky_error_raise_string(sky_TypeError, "a number is required");
    }

    fast = SKY_TRUE;
    if (!sky_object_isa(start, sky_integer_type)) {
        fast = SKY_FALSE;
    }
    else {
        SKY_ERROR_TRY {
            istart = sky_integer_value(start, INTMAX_MIN, INTMAX_MAX, NULL);
        } SKY_ERROR_EXCEPT(sky_OverflowError) {
            fast = SKY_FALSE;
        } SKY_ERROR_TRY_END;

        if (fast && !sky_object_isa(step, sky_integer_type)) {
            fast = SKY_FALSE;
        }
        else {
            SKY_ERROR_TRY {
                istep = sky_integer_value(step, INTMAX_MIN, INTMAX_MAX, NULL);
            } SKY_ERROR_EXCEPT(sky_OverflowError) {
                fast = SKY_FALSE;
            } SKY_ERROR_TRY_END;
        }
    }

    self_data = sky_itertools_count_data(self);
    if (fast) {
        self_data->ivalue = istart;
        self_data->istep = istep;
        self_data->value = NULL;
    }
    else {
        self_data->ivalue = INTMAX_MAX;
        self_data->istep = INTMAX_MAX;
        sky_object_gc_set(&(self_data->value), start, self);
    }
    sky_object_gc_set(&(self_data->step), step, self);
}

static sky_object_t
sky_itertools_count_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_count_next(sky_object_t self)
{
    sky_object_t                result;
    sky_itertools_count_data_t  *self_data;

    self_data = sky_itertools_count_data(self);
    if (!self_data->value) {
        result = sky_integer_create(self_data->ivalue++);
        if (INTMAX_MAX - self_data->ivalue < self_data->istep ||
            INTMAX_MIN + self_data->ivalue > self_data->istep)
        {
            /* switch to slow mode */
            sky_object_gc_set(&(self_data->value),
                              sky_number_add(result, self_data->step),
                              self);
        }
        return result;
    }

    result = self_data->value;
    sky_object_gc_set(&(self_data->value),
                      sky_number_add(result, self_data->step),
                      self);
    return result;
}

static sky_object_t
sky_itertools_count_reduce(sky_object_t self)
{
    sky_itertools_count_data_t  *self_data;

    self_data = sky_itertools_count_data(self);
    if (self_data->value) {
        return sky_object_build("(O(OO))", self_data->value, self_data->step);
    }
    return sky_object_build("(O(ijij))", self_data->ivalue, self_data->istep);
}

static sky_string_t
sky_itertools_count_repr(sky_object_t self)
{
    sky_itertools_count_data_t  *self_data;

    self_data = sky_itertools_count_data(self);
    if (!self_data->value) {
        if (1 == self_data->istep) {
            return sky_string_createfromformat("count(%jd)",
                                               self_data->ivalue);
        }
        return sky_string_createfromformat("count(%jd, %jd)",
                                           self_data->ivalue,
                                           self_data->istep);
    }
    return sky_string_createfromformat("count(%#@, %#@)",
                                       self_data->value,
                                       self_data->step);
}




static sky_type_t sky_itertools_islice_type;

static const char sky_itertools_islice_type_doc[] =
"islice(iterable, stop) --> islice object\n"
"islice(iterable, start, stop[, step]) --> islice object\n"
"\n"
"Return an iterator whose .next() method returns selected values from an\n"
"iterable.  If start is specified, will skip all preceding elements;\n"
"otherwise, start defaults to zero.  Step defaults to one.  If\n"
"specified as another value, step determines how many values are \n"
"skipped between successive calls.  Works like a slice() on a list\n"
"but returns an iterator.";

typedef struct sky_itertools_islice_s *sky_itertools_islice_t;

typedef struct sky_itertools_islice_data_s {
    sky_object_t                        iterator;
    ssize_t                             next;
    ssize_t                             stop;
    ssize_t                             step;
    ssize_t                             count;
} sky_itertools_islice_data_t;

SKY_EXTERN_INLINE sky_itertools_islice_data_t *
sky_itertools_islice_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_islice_type);
}

static void
sky_itertools_islice_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_islice_data_t *self_data = data;

    sky_object_visit(self_data->iterator, visit_data);
}

static void
sky_itertools_islice_init(sky_object_t  self,
                          sky_object_t  iterable,
                          sky_object_t  start,
                          sky_object_t  stop,
                          sky_object_t  step)
{
    ssize_t                     istart, istep, istop;
    sky_object_t                iterator;
    sky_itertools_islice_data_t *self_data;

    if (sky_object_isnull(start)) {
        start = sky_integer_zero;
    }
    else {
        start = sky_number_index(start);
    }
    istart = sky_integer_value(start, SSIZE_MIN, SSIZE_MAX, NULL);

    if (sky_object_isnull(stop)) {
        stop = sky_integer_create(SSIZE_MAX);
    }
    else {
        stop = sky_number_index(stop);
    }
    istop = sky_integer_value(start, SSIZE_MIN, SSIZE_MAX, NULL);

    if (istart < 0 || istop < 0) {
        sky_error_raise_string(
                sky_ValueError,
                "Indices for islice() must be None or an integer: 0 <= x <= sys.maxsize");
    }

    if (sky_object_isnull(step)) {
        step = sky_integer_one;
    }
    else {
        step = sky_number_index(step);
    }
    istep = sky_integer_value(step, SSIZE_MIN, SSIZE_MAX, NULL);
    if (istep < 1) {
        sky_error_raise_string(
                sky_ValueError,
                "Step for islice() must be a positive integer or None.");
    }

    iterator = sky_object_iter(iterable);

    self_data = sky_itertools_islice_data(self);
    sky_object_gc_set(&(self_data->iterator), iterator, self);
    self_data->next = istart;
    self_data->stop = istop;
    self_data->step = istep;
    self_data->count = 0;
}

static sky_object_t
sky_itertools_islice_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_islice_next(sky_object_t self)
{
    sky_object_t                object;
    sky_itertools_islice_data_t *self_data;

    self_data = sky_itertools_islice_data(self);
    if (!self_data->iterator) {
        return NULL;
    }

    while (self_data->count < self_data->next) {
        if (!sky_object_next(self_data->iterator, NULL)) {
            self_data->iterator = NULL;
            return NULL;
        }
        ++self_data->count;
    }
    if (self_data->count >= self_data->stop) {
        self_data->iterator = NULL;
        return NULL;
    }
    if (!(object = sky_object_next(self_data->iterator, NULL))) {
        self_data->iterator = NULL;
        return NULL;
    }
    ++self_data->count;
    if (SSIZE_MAX - self_data->next < self_data->step) {
        self_data->iterator = NULL;
        return NULL;
    }

    self_data->next += self_data->step;
    if (self_data->next > self_data->stop) {
        self_data->iterator = NULL;
    }

    return object;
}

static sky_object_t
sky_itertools_islice_reduce(sky_object_t self)
{
    sky_itertools_islice_data_t *self_data;

    self_data = sky_itertools_islice_data(self);
    return sky_object_build("(O(Oiziziz)iz)", sky_object_type(self),
                                              self_data->iterator,
                                              self_data->next,
                                              self_data->stop,
                                              self_data->step,
                                              self_data->count);
}

static void
sky_itertools_islice_setstate(sky_object_t self, sky_object_t state)
{
    sky_itertools_islice_data_t *self_data;

    self_data = sky_itertools_islice_data(self);
    self_data->count = sky_integer_value(state, SSIZE_MIN, SSIZE_MAX, NULL);
}


static sky_type_t sky_itertools_product_type;

static const char sky_itertools_product_type_doc[] =
"product(*iterables, repeat=1) --> product object\n"
"\n"
"Cartesian product of input iterables.  Equivalent to nested for-loops.\n\n"
"For example, product(A, B) returns the same as:  ((x,y) for x in A for y in B).\n"
"The leftmost iterators are in the outermost for-loop, so the output tuples\n"
"cycle in a manner similar to an odometer (with the rightmost element changing\n"
"on every iteration).\n\n"
"To compute the product of an iterable with itself, specify the number\n"
"of repetitions with the optional repeat keyword argument. For example,\n"
"product(A, repeat=4) means the same as product(A, A, A, A).\n\n"
"product('ab', range(3)) --> ('a',0) ('a',1) ('a',2) ('b',0) ('b',1) ('b',2)\n"
"product((0,1), (0,1), (0,1)) --> (0,0,0) (0,0,1) (0,1,0) (0,1,1) (1,0,0) ...";

typedef struct sky_itertools_product_s *sky_itertools_product_t;

typedef struct sky_itertools_product_data_s {
    sky_tuple_t                         pools;
    ssize_t *                           indices;
    sky_tuple_t                         result;
} sky_itertools_product_data_t;

SKY_EXTERN_INLINE sky_itertools_product_data_t *
sky_itertools_product_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_product_type);
}

static void
sky_itertools_product_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky_itertools_product_data_t    *self_data = data;

    sky_free(self_data->indices);
}

static void
sky_itertools_product_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_product_data_t    *self_data = data;

    sky_object_visit(self_data->pools, visit_data);
    sky_object_visit(self_data->result, visit_data);
}

static void
sky_itertools_product_init(sky_object_t self,
                           sky_tuple_t  iterables,
                           ssize_t      repeat)
{
    ssize_t                         i, *indices, niterables, npools;
    sky_tuple_t                     pools;
    sky_object_t                    *objects;
    sky_itertools_product_data_t    *self_data;

    self_data = sky_itertools_product_data(self);

    if (repeat < 0) {
        sky_error_raise_string(sky_ValueError,
                               "repeat argument cannot be negative");
    }

    niterables = sky_tuple_len(iterables);
    if (!(npools = niterables * repeat)) {
        pools = sky_tuple_empty;
        indices = NULL;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            indices = sky_asset_calloc(npools,
                                       sizeof(ssize_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);
            objects = sky_asset_calloc(npools,
                                       sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);

            for (i = 0; i < niterables; ++i) {
                objects[i] = sky_tuple_create(sky_tuple_get(iterables, i));
            }
            while (i < npools) {
                objects[i] = objects[i - niterables];
                ++i;
            }

            pools = sky_tuple_createwitharray(npools, objects, sky_free);
        } SKY_ASSET_BLOCK_END;
    }

    self_data->result = NULL;
    sky_free(self_data->indices);
    self_data->indices = indices;
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->pools)), pools, self);
}

static sky_object_t
sky_itertools_product_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_product_next(sky_object_t self)
{
    ssize_t                         i, *indices, npools;
    sky_bool_t                      empty;
    sky_tuple_t                     pool, pools;
    sky_object_t                    object, *objects;
    sky_itertools_product_data_t    *self_data;

    /* The obvious way to implement this is to produce a tuple with the
     * current indices and then increment the indices for the next iteration.
     * Unfortunately, this is not how CPython does it, and we have to match so
     * that the indices will be correct for pickling. To be fair, CPython does
     * it this way to get a performance boost out of re-using the result tuple,
     * but we can't do that.
     */

    self_data = sky_itertools_product_data(self);
    if (!self_data->pools) {
        return NULL;
    }

    pools = self_data->pools;
    if (!(npools = sky_tuple_len(pools))) {
        self_data->pools = NULL;
        return sky_tuple_empty;
    }
    indices = self_data->indices;
    empty = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_calloc(npools,
                                   sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        if (!self_data->result) {
            /* For the first pass, return a tuple with the first element from
             * each of the pools.
             */
            i = 0;
            SKY_SEQUENCE_FOREACH(pools, pool) {
                if (!sky_tuple_len(pool)) {
                    empty = SKY_TRUE;
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
                objects[i++] = sky_tuple_get(pool, 0);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            i = 0;
            SKY_SEQUENCE_FOREACH(self_data->result, object) {
                objects[i++] = object;
            } SKY_SEQUENCE_FOREACH_END;

            for (i = npools - 1; i >= 0; --i) {
                pool = sky_tuple_get(pools, i);
                if (sky_tuple_len(pool) == ++indices[i]) {
                    indices[i] = 0;
                    objects[i] = sky_tuple_get(pool, 0);
                }
                else {
                    objects[i] = sky_tuple_get(pool, indices[i]);
                    break;
                }
            }
            if (i < 0) {
                empty = SKY_TRUE;
            }
        }

        if (empty) {
            sky_free(objects);
        }
        else {
            sky_object_gc_set(
                    SKY_AS_OBJECTP(&(self_data->result)),
                    sky_tuple_createwitharray(npools, objects, sky_free),
                    self);
        }
    } SKY_ASSET_BLOCK_END;

    if (empty) {
        self_data->pools = NULL;
        self_data->result = NULL;
    }

    return self_data->result;
}

static sky_object_t
sky_itertools_product_reduce(sky_object_t self)
{
    ssize_t                         i, npools;
    sky_tuple_t                     indices;
    sky_object_t                    *objects;
    sky_itertools_product_data_t    *self_data;

    self_data = sky_itertools_product_data(self);
    if (!self_data->pools) {
        return sky_object_build("(O(()))", sky_object_type(self));
    }
    if (!self_data->result) {
        return sky_object_build("(OO)", sky_object_type(self),
                                        self_data->pools);
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!(npools = sky_tuple_len(self_data->pools))) {
            indices = sky_tuple_empty;
        }
        else {
            objects = sky_asset_calloc(npools,
                                       sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);
            for (i = 0; i < npools; ++i) {
                objects[i] = sky_integer_create(self_data->indices[i]);
            }
            indices = sky_tuple_createwitharray(npools, objects, sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_object_build("(OOO)", sky_object_type(self),
                                     self_data->pools,
                                     indices);
}

static void
sky_itertools_product_setstate(sky_object_t self, sky_object_t state)
{
    ssize_t                         i, npools;
    sky_tuple_t                     pool;
    sky_object_t                    *objects;
    sky_itertools_product_data_t    *self_data;

    self_data = sky_itertools_product_data(self);
    npools = sky_tuple_len(self_data->pools);

    if (!sky_object_isa(state, sky_tuple_type) ||
        sky_tuple_len(state) != npools)
    {
        sky_error_raise_string(sky_ValueError, "invalid arguments");
    }

    for (i = 0; i < npools; ++i) {
        self_data->indices[i] = sky_integer_value(sky_tuple_get(state, i),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
        if (self_data->indices[i] < 0) {
            self_data->indices[i] = 0;
        }
        else if (self_data->indices[i] > npools - 1) {
            self_data->indices[i] = npools - 1;
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_calloc(npools,
                                   sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        i = 0;
        SKY_SEQUENCE_FOREACH(self_data->pools, pool) {
            objects[i] = sky_tuple_get(pool, self_data->indices[i]);
            ++i;
        } SKY_SEQUENCE_FOREACH_END;

        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->result)),
                sky_tuple_createwitharray(npools, objects, sky_free),
                self);
    } SKY_ASSET_BLOCK_END;
}


static sky_type_t sky_itertools_repeat_type;

static const char sky_itertools_repeat_type_doc[] =
"repeat(object[, times]) -> create an iterator that returns the object\n"
"for the specified number of times. If not specified, returns the object\n"
"endlessly.";

typedef struct sky_itertools_repeat_s *sky_itertools_repeat_t;

typedef struct sky_itertools_repeat_data_s {
    sky_object_t                        object;
    ssize_t                             times;
} sky_itertools_repeat_data_t;

SKY_EXTERN_INLINE sky_itertools_repeat_data_t *
sky_itertools_repeat_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_repeat_type);
}

static void
sky_itertools_repeat_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_repeat_data_t *self_data = data;

    sky_object_visit(self_data->object, visit_data);
}

static void
sky_itertools_repeat_init(sky_object_t self, sky_object_t object, ssize_t times)
{
    sky_itertools_repeat_data_t *self_data;

    self_data = sky_itertools_repeat_data(self);
    sky_object_gc_set(&(self_data->object), object, self);
    self_data->times = times;
}

static sky_object_t
sky_itertools_repeat_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_repeat_length_hint(sky_object_t self)
{
    sky_itertools_repeat_data_t *self_data;

    self_data = sky_itertools_repeat_data(self);
    if (self_data->times < 0) {
        sky_error_raise_string(sky_TypeError, "len() of unsized object");
    }
    return sky_integer_create(self_data->times);
}

static sky_object_t
sky_itertools_repeat_next(sky_object_t self)
{
    sky_itertools_repeat_data_t *self_data;

    self_data = sky_itertools_repeat_data(self);
    if (self_data->times < 0) {
        return self_data->object;
    }
    if (!self_data->times) {
        return NULL;
    }
    --self_data->times;
    return self_data->object;
}

static sky_object_t
sky_itertools_repeat_reduce(sky_object_t self)
{
    sky_itertools_repeat_data_t *self_data;

    self_data = sky_itertools_repeat_data(self);
    if (self_data->times < 0) {
        return sky_object_build("(O(O))", self_data->object);
    }
    return sky_object_build("(O(Oiz))", self_data->object, self_data->times);
}

static sky_string_t
sky_itertools_repeat_repr(sky_object_t self)
{
    sky_itertools_repeat_data_t *self_data;

    self_data = sky_itertools_repeat_data(self);
    if (self_data->times < 0) {
        return sky_string_createfromformat("repeat(%#@)", self_data->object);
    }
    return sky_string_createfromformat("repeat(%#@, %zd)", self_data->object,
                                                           self_data->times);
}


static sky_type_t sky_itertools_starmap_type;

static const char sky_itertools_starmap_type_doc[] =
"starmap(function, sequence) --> starmap object\n\n"
"Return an iterator whose values are returned from the function evaluated\n"
"with an argument tuple taken from the given sequence.";

typedef struct sky_itertools_starmap_s *sky_itertools_starmap_t;

typedef struct sky_itertools_starmap_data_s {
    sky_object_t                        function;
    sky_object_t                        iterator;
} sky_itertools_starmap_data_t;

SKY_EXTERN_INLINE sky_itertools_starmap_data_t *
sky_itertools_starmap_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools_starmap_type);
}

static void
sky_itertools_starmap_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools_starmap_data_t    *self_data = data;

    sky_object_visit(self_data->function, visit_data);
    sky_object_visit(self_data->iterator, visit_data);
}

static void
sky_itertools_starmap_init(sky_object_t self,
                           sky_object_t function,
                           sky_object_t sequence)
{
    sky_object_t                    iterator;
    sky_itertools_starmap_data_t    *self_data;

    iterator = sky_object_iter(sequence);

    self_data = sky_itertools_starmap_data(self);
    sky_object_gc_set(&(self_data->function), function, self);
    sky_object_gc_set(&(self_data->iterator), iterator, self);
}

static sky_object_t
sky_itertools_starmap_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools_starmap_next(sky_object_t self)
{
    sky_object_t                    args;
    sky_itertools_starmap_data_t    *self_data;

    self_data = sky_itertools_starmap_data(self);
    if (!(args = sky_object_next(self_data->iterator, NULL))) {
        return NULL;
    }

    return sky_object_call(self_data->function, sky_tuple_create(args), NULL);
}

static sky_object_t
sky_itertools_starmap_reduce(sky_object_t self)
{
    sky_itertools_starmap_data_t    *self_data;

    self_data = sky_itertools_starmap_data(self);
    return sky_object_build("(O(OO))", sky_object_type(self),
                                       self_data->function,
                                       self_data->iterator);
}


static sky_type_t sky_itertools__tee_dataobject_type;

static const char sky_itertools__tee_dataobject_type_doc[] =
"Data container common to multiple tee objects.";

typedef struct sky_itertools__tee_dataobject_s *sky_itertools__tee_dataobject_t;


#define SKY_ITERTOOLS__TEE_DATAOBJECT_SIZE  \
        (ssize_t)((SKY_CACHE_LINE_SIZE / sizeof(sky_object_t)))

typedef struct sky_itertools__tee_dataobject_data_s {
    sky_object_t                        next;
    sky_object_t                        iterator;
    ssize_t                             refs;
    ssize_t                             used;

    uint8_t alignment[SKY_CACHE_LINE_SIZE -
                      ((sizeof(sky_object_data_t) +
                        (sizeof(sky_object_t) * 2) +
                        (sizeof(ssize_t) * 2)) % SKY_CACHE_LINE_SIZE)];
    sky_object_t    values[SKY_ITERTOOLS__TEE_DATAOBJECT_SIZE];
} sky_itertools__tee_dataobject_data_t;

SKY_EXTERN_INLINE sky_itertools__tee_dataobject_data_t *
sky_itertools__tee_dataobject_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools__tee_dataobject_type);
}

static void
sky_itertools__tee_dataobject_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools__tee_dataobject_data_t    *self_data = data;

    ssize_t i;

    sky_object_visit(self_data->next, visit_data);
    sky_object_visit(self_data->iterator, visit_data);
    for (i = 0; i < self_data->used; ++i) {
        sky_object_visit(self_data->values[i], visit_data);
    }
}

static sky_object_t
sky_itertools__tee_dataobject_from_iterable(sky_object_t iterable)
{
    sky_object_t                            iterator, self;
    sky_itertools__tee_dataobject_data_t    *self_data;

    iterator = sky_object_iter(iterable);

    self = sky_object_allocate(sky_itertools__tee_dataobject_type);
    self_data = sky_itertools__tee_dataobject_data(self);
    sky_object_gc_set(&(self_data->iterator), iterator, self);

    return self;
}

static void
sky_itertools__tee_dataobject_init(sky_object_t self,
                                   sky_list_t   list,
                                   sky_object_t next)
{
    ssize_t                                 i, n;
    sky_itertools__tee_dataobject_data_t    *self_data;

    self_data = sky_itertools__tee_dataobject_data(self);
    n = sky_object_len(list);
    if (n > SKY_ITERTOOLS__TEE_DATAOBJECT_SIZE) {
        sky_error_raise_string(sky_ValueError, "too many values");
    }

    if (!sky_object_isnull(next)) {
        if (!sky_object_isa(next, sky_object_type(self))) {
            sky_error_raise_format(sky_ValueError, "next must be %#@",
                                   sky_object_type(self));
        }
        if (SKY_ITERTOOLS__TEE_DATAOBJECT_SIZE != n) {
            sky_error_raise_string(sky_ValueError, "next must be None");
        }
    }

    for (i = 0; i < n; ++i) {
        sky_object_gc_set(&(self_data->values[i]),
                          sky_sequence_get(list, i),
                          self);
    }
    self_data->used = n;
    sky_object_gc_set(&(self_data->next), next, self);
}

static sky_object_t
sky_itertools__tee_dataobject_next(sky_object_t self)
{
    sky_object_t                            new_data;
    sky_itertools__tee_dataobject_data_t    *new_data_data, *self_data;

    self_data = sky_itertools__tee_dataobject_data(self);
    if (self_data->next) {
        return self_data->next;
    }

    new_data = sky_object_allocate(sky_itertools__tee_dataobject_type);
    new_data_data = sky_itertools__tee_dataobject_data(new_data);
    sky_object_gc_set(&(new_data_data->iterator),
                      self_data->iterator,
                      new_data);
    sky_object_gc_set(&(self_data->next), new_data, self);

    return new_data;
}

static sky_object_t
sky_itertools__tee_dataobject_reduce(sky_object_t self)
{
    sky_list_t                              values;
    sky_itertools__tee_dataobject_data_t    *self_data;

    self_data = sky_itertools__tee_dataobject_data(self);
    values = sky_list_createfromarray(self_data->used, self_data->values);
    return sky_object_build("(O(OOO))", sky_object_type(self),
                                        self_data->iterator,
                                        values,
                                        self_data->next);
}

static void
sky_itertools__tee_dataobject_release(sky_object_t self)
{
    sky_itertools__tee_dataobject_data_t    *self_data;

    self_data = sky_itertools__tee_dataobject_data(self);
    if (!--self_data->refs) {
        self_data->used = 0;
    }
}

static sky_object_t
sky_itertools__tee_dataobject_retain(sky_object_t self)
{
    sky_itertools__tee_dataobject_data_t    *self_data;

    self_data = sky_itertools__tee_dataobject_data(self);
    ++self_data->refs;

    return self;
}


static sky_type_t sky_itertools__tee_type;

static const char sky_itertools__tee_type_doc[] =
"Iterator wrapped to make it copyable";

typedef struct sky_itertools__tee_s *sky_itertools__tee_t;

typedef struct sky_itertools__tee_data_s {
    sky_object_t                        data;
    ssize_t                             next_index;
} sky_itertools__tee_data_t;

SKY_EXTERN_INLINE sky_itertools__tee_data_t *
sky_itertools__tee_data(sky_object_t object)
{
    return sky_object_data(object, sky_itertools__tee_type);
}

static void
sky_itertools__tee_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_itertools__tee_data_t   *self_data = data;

    sky_object_visit(self_data->data, visit_data);
}

static sky_object_t
sky_itertools__tee_copy(sky_object_t self)
{
    sky_object_t                copy;
    sky_itertools__tee_data_t   *copy_data, *self_data;

    self_data = sky_itertools__tee_data(self);

    copy = sky_object_allocate(sky_itertools__tee_type);
    copy_data = sky_itertools__tee_data(copy);
    copy_data->next_index = self_data->next_index;

    sky_object_gc_set(&(copy_data->data), self_data->data, copy);
    sky_itertools__tee_dataobject_retain(copy_data->data);

    return copy;
}

static sky_object_t
sky_itertools__tee_from_iterable(sky_object_t iterable)
{
    sky_object_t                self;
    sky_itertools__tee_data_t   *self_data;

    self = sky_object_allocate(sky_itertools__tee_type);
    self_data = sky_itertools__tee_data(self);
    sky_object_gc_set(&(self_data->data),
                      sky_itertools__tee_dataobject_from_iterable(iterable),
                      self);
    sky_itertools__tee_dataobject_retain(self_data->data);

    return self;
}

static void
sky_itertools__tee_init(sky_object_t self, sky_object_t iterable)
{
    sky_itertools__tee_data_t   *self_data;

    self_data = sky_itertools__tee_data(self);
    if (self_data->data) {
        sky_itertools__tee_dataobject_release(self_data->data);
        self_data->data = NULL;
    }
    sky_object_gc_set(&(self_data->data),
                      sky_itertools__tee_dataobject_from_iterable(iterable),
                      self);
    sky_itertools__tee_dataobject_retain(self_data->data);
    self_data->next_index = 0;
}

static sky_object_t
sky_itertools__tee_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_itertools__tee_next(sky_object_t self)
{
    sky_object_t                            new_data, object;
    sky_itertools__tee_data_t               *self_data;
    sky_itertools__tee_dataobject_data_t    *data_data;

    self_data = sky_itertools__tee_data(self);
    if (!self_data->data) {
        return NULL;
    }

    data_data = sky_itertools__tee_dataobject_data(self_data->data);
    if (self_data->next_index > SKY_ITERTOOLS__TEE_DATAOBJECT_SIZE) {
        new_data = sky_itertools__tee_dataobject_next(self_data->data);

        sky_itertools__tee_dataobject_release(self_data->data);
        sky_object_gc_set(&(self_data->data), new_data, self);
        sky_itertools__tee_dataobject_retain(self_data->data);

        data_data = sky_itertools__tee_dataobject_data(self_data->data);
        self_data->next_index = 0;
    }

    if (self_data->next_index > data_data->used) {
        if (!(object = sky_object_next(data_data->iterator, NULL))) {
            sky_itertools__tee_dataobject_release(self_data->data);
            self_data->data = NULL;
            return NULL;
        }
        sky_object_gc_set(&(data_data->values[data_data->used]),
                          object,
                          self_data->data);
        ++data_data->used;
    }

    return data_data->values[self_data->next_index++];
}

static sky_object_t
sky_itertools__tee_reduce(sky_object_t self)
{
    sky_itertools__tee_data_t   *self_data;

    self_data = sky_itertools__tee_data(self);
    return sky_object_build("(O(())(Oiz))", sky_object_type(self),
                                            self_data->data,
                                            self_data->next_index);
}

static void
sky_itertools__tee_setstate(sky_object_t self, sky_object_t state)
{
    sky_itertools__tee_data_t   *self_data;

    self_data = sky_itertools__tee_data(self);
    self_data->next_index = sky_integer_value(sky_tuple_get(state, 1),
                                              SSIZE_MIN,
                                              SSIZE_MAX,
                                              NULL);
    sky_object_gc_set(&(self_data->data), sky_tuple_get(state, 0), self);
}


static sky_tuple_t
sky_itertools_tee(sky_object_t iterable, ssize_t n)
{
    ssize_t         i;
    sky_tuple_t     tuple;
    sky_object_t    copy, *iterators, iterator;

    if (n <= 0) {
        if (!n) {
            return sky_tuple_empty;
        }
        sky_error_raise_string(sky_ValueError, "n must be >= 0");
    }

    iterator = sky_object_iter(iterable);
    if (!(copy = sky_object_getattr(iterator,
                                    SKY_STRING_LITERAL("__copy__"),
                                    NULL)))
    {
        iterator = sky_itertools__tee_from_iterable(iterator);
        copy = sky_object_getattr(iterator,
                                  SKY_STRING_LITERAL("__copy__"),
                                  sky_NotSpecified);
    }

    SKY_ASSET_BLOCK_BEGIN {
        iterators = sky_asset_malloc(n * sizeof(sky_object_t),
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        iterators[0] = iterator;
        for (i = 1; i < n; ++i) {
            iterators[i] = sky_object_call(copy, NULL, NULL);
        }
        tuple = sky_tuple_createwitharray(n, iterators, sky_free);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}


void
skython_module_itertools_initialize(sky_module_t module)
{
    static const char doc[] =
"Functional tools for creating and using iterators.\n"
"\n"
"Infinite iterators:\n"
"count(start=0, step=1) --> start, start+step, start+2*step, ...\n"
"cycle(p) --> p0, p1, ... plast, p0, p1, ...\n"
"repeat(elem [,n]) --> elem, elem, elem, ... endlessly or up to n times\n"
"\n"
"Iterators terminating on the shortest input sequence:\n"
"accumulate(p[, func]) --> p0, p0+p1, p0+p1+p2\n"
"chain(p, q, ...) --> p0, p1, ... plast, q0, q1, ... \n"
"chain.from_iterable([p, q, ...]) --> p0, p1, ... plast, q, q1, ... \n"
"compress(data, selectors) --> (d[0] if s[0]), (d[1] if s[1]), ...\n"
"dropwhile(pred, seq) --> seq[n], seq[n+1], starting when pred fails\n"
"groupby(iterable[, keyfunc]) --> sub-iterators grouped by value of keyfunc(v)\n"
"filterfalse(pred, seq) --> elements of seq where pred(elem) is False\n"
"islice(seq, [start,] stop [, step]) --> elements from\n"
"       seq[start:stop:step]\n"
"starmap(fun, seq) --> fun(*seq[0]), fun(*seq[1]), ...\n"
"tee(it, n=2) --> (it1, it2 , ... itn) splits one iterator into n\n"
"takewhile(pred, seq) --> seq[0], seq[1], until pred fails\n"
"zip_longest(p, q, ...) --> (p[0], q[0]), (p[1], q[1]), ... \n"
"\n"
"Combinatoric generators:\n"
"product(p, q, ... [repeat=1]) --> cartesian product\n"
"permutations(p[, r])\n"
"combinations(p, r)\n"
"combinations_with_replacement(p, r)\n";

    sky_type_t          type;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    /* TODO accumulate type */

    /* chain type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools_chain_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("chain"),
                    sky_itertools_chain_type_doc,
                    0,
                    sizeof(sky_itertools_chain_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setattr_builtin(type,
            "from_iterable",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "chain.from_iterable",
                            sky_itertools_chain_from_iterable_doc,
                            (sky_native_code_function_t)sky_itertools_chain_from_iterable,
                            SKY_DATA_TYPE_OBJECT,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                            NULL)));
    sky_type_setmethodslots(type,
            "__init__", sky_itertools_chain_init,
            "__iter__", sky_itertools_chain_iter,
            "__next__", sky_itertools_chain_next,
            "__reduce__", sky_itertools_chain_reduce,
            "__setstate__", sky_itertools_chain_setstate,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_chain_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "chain", type);

    /* TODO combinations type */
    /* TODO combinations_with_replacement type */
    /* TODO compress type */

    /* count type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools_count_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("count"),
                    sky_itertools_count_type_doc,
                    0,
                    sizeof(sky_itertools_count_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools_count_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "start", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
            "step", SKY_DATA_TYPE_OBJECT, sky_integer_one,
            NULL);

    sky_type_setmethodslots(type,
            "__repr__", sky_itertools_count_repr,
            "__iter__", sky_itertools_count_iter,
            "__next__", sky_itertools_count_next,
            "__reduce__", sky_itertools_count_reduce,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_count_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "count", type);

    /* TODO cycle type */
    /* TODO dropwhile type */
    /* TODO filterfalse type */
    /* TODO groupby type */

    /* islice type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools_islice_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("islice"),
                    sky_itertools_islice_type_doc,
                    0,
                    sizeof(sky_itertools_islice_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools_islice_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
            "start", SKY_DATA_TYPE_OBJECT, sky_None,
            "stop", SKY_DATA_TYPE_OBJECT, sky_None,
            "step", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);
    sky_type_setmethodslots(type,
            "__iter__", sky_itertools_islice_iter,
            "__next__", sky_itertools_islice_next,
            "__reduce__", sky_itertools_islice_reduce,
            "__setstate__", sky_itertools_islice_setstate,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_islice_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "islice", type);

    /* TODO permutations type */

    /* product type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky_itertools_product_instance_finalize;
    template.visit = sky_itertools_product_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("product"),
                    sky_itertools_product_type_doc,
                    0,
                    sizeof(sky_itertools_product_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools_product_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "*iterables", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
            "repeat", SKY_DATA_TYPE_SSIZE_T, sky_integer_one,
            NULL);
    sky_type_setmethodslots(type,
            "__iter__", sky_itertools_product_iter,
            "__next__", sky_itertools_product_next,
            "__reduce__", sky_itertools_product_reduce,
            "__setstate__", sky_itertools_product_setstate,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_product_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "product", type);

    /* repeat type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools_repeat_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("repeat"),
                    sky_itertools_repeat_type_doc,
                    0,
                    sizeof(sky_itertools_repeat_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools_repeat_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "object", SKY_DATA_TYPE_OBJECT, NULL,
            "times", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
            NULL);
    sky_type_setmethodslots(type,
            "__repr__", sky_itertools_repeat_repr,
            "__length_hint__", sky_itertools_repeat_length_hint,
            "__iter__", sky_itertools_repeat_iter,
            "__next__", sky_itertools_repeat_next,
            "__reduce__", sky_itertools_repeat_reduce,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_repeat_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "repeat", type);

    /* starmap type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools_starmap_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("starmap"),
                    sky_itertools_starmap_type_doc,
                    0,
                    sizeof(sky_itertools_starmap_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools_starmap_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "function", SKY_DATA_TYPE_OBJECT, NULL,
            "sequence", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(type,
            "__iter__", sky_itertools_starmap_iter,
            "__next__", sky_itertools_starmap_next,
            "__reduce__", sky_itertools_starmap_reduce,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools_starmap_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "starmap", type);

    /* TODO takewhile type */

    /* tee */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools__tee_dataobject_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("_tee_dataobject"),
                    sky_itertools__tee_dataobject_type_doc,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky_itertools__tee_dataobject_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools__tee_dataobject_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterator", SKY_DATA_TYPE_OBJECT, NULL,
            "values", SKY_DATA_TYPE_OBJECT_LIST, NULL,
            "next", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(type,
            "__reduce__", sky_itertools__tee_dataobject_reduce,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools__tee_dataobject_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "_tee_dataobject", type);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_itertools__tee_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("_tee"),
                    sky_itertools__tee_type_doc,
                    SKY_TYPE_CREATE_FLAG_FINAL,
                    sizeof(sky_itertools__tee_data_t),
                    0,
                    NULL,
                    &template);
    sky_type_setattr_builtin(type,
            "__copy__",
            sky_function_createbuiltin(
                    "_tee.__copy__",
                    "Returns an independent iterator.",
                    (sky_native_code_function_t)sky_itertools__tee_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky_itertools__tee_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(type,
            "__init__", sky_itertools__tee_init,
            "__iter__", sky_itertools__tee_iter,
            "__next__", sky_itertools__tee_next,
            "__reduce__", sky_itertools__tee_reduce,
            "__setstate__", sky_itertools__tee_setstate,
            NULL);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_itertools__tee_type),
            type,
            SKY_TRUE);
    sky_module_setattr(module, "_tee", type);

    sky_module_setattr(
            module,
            "tee",
            sky_function_createbuiltin(
                    "tee",
                    "tee(iterable, n=2) --> tuple of n independent iterators.",
                    (sky_native_code_function_t)sky_itertools_tee,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(2),
                    NULL));

    /* TODO zip_longest type */
}
