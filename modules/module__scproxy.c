#include "../core/skython.h"

#include <SystemConfiguration/SystemConfiguration.h>


SKY_MODULE_EXTERN void
skython_module__scproxy_initialize(sky_module_t module);


static int32_t
CFNumberAsInt32(CFNumberRef number)
{
    int32_t result;

    CFNumberGetValue(number, kCFNumberSInt32Type, &result);
    return result;
}

static sky_string_t
CFStringAsString(CFStringRef string)
{
    char            *buf;
    CFIndex         len;
    const char      *s;
    sky_string_t    result;

    if ((s = CFStringGetCStringPtr(string, kCFStringEncodingUTF8)) != NULL) {
        return sky_string_createfrombytes(s, strlen(s), NULL, NULL);
    }

    SKY_ASSET_BLOCK_BEGIN {
        len = CFStringGetLength(string);
        buf = sky_asset_malloc(len * 4, SKY_ASSET_CLEANUP_ALWAYS);
        if (!CFStringGetCString(string, buf, len * 4, kCFStringEncodingUTF8)) {
            result = NULL;
        }
        else {
            result = sky_string_createfrombytes(buf, strlen(buf), NULL, NULL);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static void
sky__scproxy__get_proxies_set(
                sky_dict_t      dict,
                sky_string_t    protocol,
                CFDictionaryRef proxyDict,
                CFStringRef     enabledKey,
                CFStringRef     hostKey,
                CFStringRef     portKey)
{
    int32_t         port;
    CFStringRef     string;
    CFNumberRef     number;
    sky_object_t    value;

    number = CFDictionaryGetValue(proxyDict, enabledKey);
    if (number && CFNumberAsInt32(number)) {
        string = CFDictionaryGetValue(proxyDict, hostKey);
        number = CFDictionaryGetValue(proxyDict, portKey);
        if (string) {
            if (!number) {
                value = sky_string_createfromformat("http://%@", string);
            }
            else {
                port = CFNumberAsInt32(number);
                value = sky_string_createfromformat("http://%@:%d",
                                                    string,
                                                    (int)port);
            }
            sky_dict_setitem(dict, protocol, value);
        }
    }
}


static sky_object_t
sky__scproxy__get_proxies(void)
{
    sky_dict_t      result;
    CFDictionaryRef proxyDict;

    if (!(proxyDict = SCDynamicStoreCopyProxies(NULL))) {
        return NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save((void *)proxyDict,
                       (sky_free_t)CFRelease,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_dict_create();
        sky__scproxy__get_proxies_set(result,
                                      SKY_STRING_LITERAL("http"),
                                      proxyDict,
                                      kSCPropNetProxiesHTTPEnable,
                                      kSCPropNetProxiesHTTPProxy,
                                      kSCPropNetProxiesHTTPPort);
        sky__scproxy__get_proxies_set(result,
                                      SKY_STRING_LITERAL("https"),
                                      proxyDict,
                                      kSCPropNetProxiesHTTPSEnable,
                                      kSCPropNetProxiesHTTPSProxy,
                                      kSCPropNetProxiesHTTPSPort);
        sky__scproxy__get_proxies_set(result,
                                      SKY_STRING_LITERAL("ftp"),
                                      proxyDict,
                                      kSCPropNetProxiesFTPEnable,
                                      kSCPropNetProxiesFTPProxy,
                                      kSCPropNetProxiesFTPPort);
        sky__scproxy__get_proxies_set(result,
                                      SKY_STRING_LITERAL("gopher"),
                                      proxyDict,
                                      kSCPropNetProxiesGopherEnable,
                                      kSCPropNetProxiesGopherProxy,
                                      kSCPropNetProxiesGopherPort);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_object_t
sky__scproxy__get_proxy_settings(void)
{
    CFIndex         i, len;
    CFArrayRef      array;
    sky_dict_t      result;
    CFNumberRef     number;
    CFStringRef     string;
    sky_object_t    *objects, value;
    CFDictionaryRef proxyDict;

    if (!(proxyDict = SCDynamicStoreCopyProxies(NULL))) {
        return sky_None;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save((void *)proxyDict,
                       (sky_free_t)CFRelease,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_dict_create();

#if MAC_OS_X_VERSION_MIN_REQUIRED < MAC_OS_X_VERSION_10_4
        if (!&kSCPropNetProxiesExcludeSimpleHostnames) {
            value = sky_False;
        }
        else {
#endif
            number = CFDictionaryGetValue(
                            proxyDict,
                            kSCPropNetProxiesExcludeSimpleHostnames);
            if (number) {
                value = (CFNumberAsInt32(number) ? sky_True : sky_False);
            }
            else {
                value = sky_False;
            }
#if MAC_OS_X_VERSION_MIN_REQUIRED < MAC_OS_X_VERSION_10_4
        }
#else
        value = sky_False;
#endif
        sky_dict_setitem(result, SKY_STRING_LITERAL("exclude_simple"), value);

        array = CFDictionaryGetValue(proxyDict,
                                     kSCPropNetProxiesExceptionsList);
        if (array) {
            SKY_ASSET_BLOCK_BEGIN {
                len = CFArrayGetCount(array);
                objects = sky_asset_calloc(len, sizeof(sky_object_t),
                                           SKY_ASSET_CLEANUP_ON_ERROR);
                for (i = 0; i < len; ++i) {
                    if (!(string = CFArrayGetValueAtIndex(array, i))) {
                        objects[i] = sky_None;
                    }
                    else {
                        objects[i] = CFStringAsString(string);
                    }
                }
                value = sky_tuple_createwitharray(len, objects, sky_free);
            } SKY_ASSET_BLOCK_END;
            sky_dict_setitem(result, SKY_STRING_LITERAL("exceptions"), value);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
skython_module__scproxy_initialize(sky_module_t module)
{
    sky_module_setattr(
            module,
            "_get_proxies",
            sky_function_createbuiltin(
                    "_get_proxies",
                    NULL,
                    (sky_native_code_function_t)sky__scproxy__get_proxies,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    NULL));
    sky_module_setattr(
            module,
            "_get_proxy_settings",
            sky_function_createbuiltin(
                    "_get_proxy_settings",
                    NULL,
                    (sky_native_code_function_t)sky__scproxy__get_proxy_settings,
                    SKY_DATA_TYPE_OBJECT_DICT,
                    NULL));
}
