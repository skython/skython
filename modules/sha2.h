#include "../core/skython.h"


SKY_CDECLS_BEGIN


#define SHA224_BLOCK_LENGTH             64
#define SHA256_BLOCK_LENGTH             64
#define SHA384_BLOCK_LENGTH             128
#define SHA512_BLOCK_LENGTH             128

#define SHA224_DIGEST_LENGTH            28
#define SHA256_DIGEST_LENGTH            32
#define SHA384_DIGEST_LENGTH            48
#define SHA512_DIGEST_LENGTH            64


typedef struct sha256_state {
    uint64_t                            length;
    uint32_t                            state[8];
    size_t                              curlen;
    uint8_t                             buf[64];
} SHA224_CTX, SHA256_CTX;

typedef struct sha512_state {
    uint64_t                            length;
    uint64_t                            state[8];
    size_t                              curlen;
    uint8_t                             buf[128];
} SHA384_CTX, SHA512_CTX;


extern void SHA224_Init(SHA224_CTX *);
extern void SHA224_Final(uint8_t *, SHA224_CTX *);

extern void SHA256_Init(SHA256_CTX *);
extern void SHA256_Update(SHA256_CTX *, const void *, size_t);
extern void SHA256_Final(uint8_t *, SHA256_CTX *);

extern void SHA384_Init(SHA384_CTX *);
extern void SHA384_Final(uint8_t *, SHA384_CTX *);

extern void SHA512_Init(SHA512_CTX *);
extern void SHA512_Update(SHA512_CTX *, const void *, size_t);
extern void SHA512_Final(uint8_t *, SHA512_CTX *);

SKY_EXTERN_INLINE void
SHA224_Update(SHA224_CTX *ctx, const void *bytes, size_t nbytes)
{
    SHA256_Update(ctx, bytes, nbytes);
}

SKY_EXTERN_INLINE void
SHA384_Update(SHA384_CTX *ctx, const void *bytes, size_t nbytes)
{
    SHA512_Update(ctx, bytes, nbytes);
}


#define CONST64(_v) _v##ULL

#define RORc(x, y) \
    ( ((((uint32_t)(x)&0xFFFFFFFFUL)>>(uint32_t)((y)&31)) | \
      ((uint32_t)(x)<<(uint32_t)(32-((y)&31)))) & 0xFFFFFFFFUL)

#define ROR64c(x, y) \
    ( ((((x)&CONST64(0xFFFFFFFFFFFFFFFF))>>((uint64_t)(y)&CONST64(63))) | \
      ((x)<<((uint64_t)(64-((y)&CONST64(63)))))) & CONST64(0xFFFFFFFFFFFFFFFF))


SKY_CDECLS_END
