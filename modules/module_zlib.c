#include "../core/sky_private.h"

#include "zlib.h"


/* The following parameters are copied from zutil.h, version 0.95 */
#define DEFLATED    8
#if MAX_MEM_LEVEL >= 8
#   define DEF_MEM_LEVEL    8
#else
#   define DEF_MEM_LEVEL    MAX_MEM_LEVEL
#endif
#define DEF_WBITS   MAX_WBITS

/* The output buffer will be increased in chunks of DEFAULTALLOC bytes. */
#define DEFAULTALLOC    (16 * 1024)


SKY_MODULE_EXTERN const char *skython_module_zlib_doc;

SKY_MODULE_EXTERN void
skython_module_zlib_initialize(sky_module_t module);


static sky_object_t sky_zlib_error = NULL;

static void
sky_zlib_raise(z_stream zst, int err, const char *msg)
{
    const char  *zmsg;

    if (Z_VERSION_ERROR == err) {
        zmsg = "library version mismatch";
    }
    else if ((zmsg = zst.msg) == Z_NULL) {
        switch (err) {
            case Z_BUF_ERROR:
                zmsg = "incomplete or truncated stream";
                break;
            case Z_STREAM_ERROR:
                zmsg = "inconsistent stream state";
                break;
            case Z_DATA_ERROR:
                zmsg = "invalid input data";
                break;
            default:
                sky_error_raise_format(sky_zlib_error,
                                       "Error %d %s",
                                       err,
                                       msg);
        }
    }
    sky_error_raise_format(sky_zlib_error,
                           "Error %d %s: %.200s",
                           err,
                           msg,
                           zmsg);
}


static sky_type_t   sky_zlib_Compress_type = NULL;

typedef struct sky_zlib_Compress_data_s {
    sky_bool_t                          initialized;
    z_stream                            zst;
} sky_zlib_Compress_data_t;

SKY_EXTERN_INLINE sky_zlib_Compress_data_t *
sky_zlib_Compress_data(sky_object_t self)
{
    return sky_object_data(self, sky_zlib_Compress_type);
}

static void
sky_zlib_Compress_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_zlib_Compress_data_t    *self_data = data;

    if (self_data->initialized) {
        deflateEnd(&(self_data->zst));
        self_data->initialized = SKY_FALSE;
    }
}


static sky_bytes_t
sky_zlib_Compress_compress(sky_object_t self, sky_object_t data)
{
    sky_zlib_Compress_data_t    *self_data = sky_zlib_Compress_data(self);

    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        Byte            *input, *output;
        ssize_t         length;
        sky_buffer_t    buffer;
        unsigned int    input_length;
        unsigned long   start_total_out;

        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len > UINT_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "Size does not fit in an unsigned int");
        }
        input = buffer.buf;
        input_length = buffer.len;

        length = DEFAULTALLOC;
        output = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);

        start_total_out = self_data->zst.total_out;
        self_data->zst.avail_in = input_length;
        self_data->zst.next_in = input;
        self_data->zst.avail_out = length;
        self_data->zst.next_out = output;

        err = deflate(&(self_data->zst), Z_NO_FLUSH);

        /* whie Z_OK and the output buffer is full, there might be more output,
         * so extend the output buffer and try again
         */
        while (Z_OK == err && !self_data->zst.avail_out) {
            output = sky_asset_realloc(output,
                                       (length << 1),
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            self_data->zst.next_out = output + length;
            self_data->zst.avail_out = length;
            length <<= 1;

            err = deflate(&(self_data->zst), Z_NO_FLUSH);
        }

        /* We will only get Z_BUF_ERROR if the output buffer was full but there
         * wasn't more output when we tried again, so it is not an error
         * condition.
         */
        if (Z_OK != err && Z_BUF_ERROR != err) {
            sky_zlib_raise(self_data->zst,
                           err,
                           "while compressing data");
        }

        result = sky_bytes_createwithbytes(output,
                                           (self_data->zst.total_out -
                                            start_total_out),
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_Compress_compress_doc[] =
"compress(data) -- Return a string containing data compressed.\n\n"
"After calling this function, some of the input data may still\n"
"be stored in internal buffers for later processing.\n"
"Call the flush() method to clear these buffers.";


static sky_object_t
sky_zlib_Compress_copy(sky_object_t self)
{
    sky_zlib_Compress_data_t    *self_data = sky_zlib_Compress_data(self);

    int                         err;
    sky_object_t                copy;
    sky_zlib_Compress_data_t    *copy_data;

    copy = sky_object_allocate(sky_zlib_Compress_type);
    copy_data = sky_zlib_Compress_data(copy);

    err = deflateCopy(&(copy_data->zst), &(self_data->zst));
    switch (err) {
        case Z_OK:
            break;
        case Z_STREAM_ERROR:
            sky_error_raise_string(sky_ValueError,
                                   "Inconsistent stream state");
            break;
        case Z_MEM_ERROR:
            sky_error_raise_string(sky_MemoryError,
                                   "Can't allocate memory for compression object");
            break;
        default:
            sky_zlib_raise(self_data->zst,
                           err,
                           "while copying compression object");
            break;
    }

    copy_data->initialized = SKY_TRUE;

    return copy;
}

static const char sky_zlib_Compress_copy_doc[] =
"copy() -- Return a copy of the compression object.";


static sky_bytes_t
sky_zlib_Compress_flush(sky_object_t self, int mode)
{
    sky_zlib_Compress_data_t    *self_data = sky_zlib_Compress_data(self);

    sky_bytes_t result;

    if (Z_NO_FLUSH == mode) {
        return sky_bytes_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        int             err, length;
        Byte            *output;
        unsigned long   start_total_out;

        length = DEFAULTALLOC;
        output = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);

        start_total_out = self_data->zst.total_out;
        self_data->zst.avail_in = 0;
        self_data->zst.avail_out = length;
        self_data->zst.next_out = output;

        err = deflate(&(self_data->zst), mode);

        /* while Z_OK and the output buffer is full, there might be more output,
         * so extend the output buffer and try again
         */
        while (Z_OK == err && !self_data->zst.avail_out) {
            output = sky_asset_realloc(output,
                                       (length << 1),
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            self_data->zst.next_out = output + length;
            self_data->zst.avail_out = length;
            length <<= 1;

            err = deflate(&(self_data->zst), mode);
        }

        /* If mode is Z_FINISH, we also have to call deflateEnd() to free various
         * data structures. Note we should only get Z_STREAM_END when mode is
         * Z_FINISH, but checking both for safety
         */
        if (Z_STREAM_END == err && Z_FINISH == mode) {
            err = deflateEnd(&(self_data->zst));
            if (Z_OK != err) {
                sky_zlib_raise(self_data->zst,
                               err,
                               "while finishing compression");
            }
            self_data->initialized = SKY_FALSE;
        }
        /* We will only get Z_BUF_ERROR if the output buffer was full but there
         * wasn't more output when we tried again, so it is not an error
         * condition.
         */
        else if (Z_OK != err && Z_BUF_ERROR != err) {
            sky_zlib_raise(self_data->zst,
                           err,
                           "while flushing");
        }

        result = sky_bytes_createwithbytes(output,
                                           (self_data->zst.total_out -
                                            start_total_out),
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_Compress_flush_doc[] =
"flush( [mode] ) -- Return a string containing any remaining compressed data.\n\n"
"mode can be one of the constants Z_SYNC_FLUSH, Z_FULL_FLUSH, Z_FINISH; the\n"
"default value used when mode is not specified is Z_FINISH.\n"
"If mode == Z_FINISH, the compressor object can no longer be used after\n"
"calling the flush method().  Otherwise, more data can still be compressed.";


static sky_type_t   sky_zlib_Decompress_type = NULL;

typedef struct sky_zlib_Decompress_data_s {
    sky_bytes_t                         unused_data;
    sky_bytes_t                         unconsumed_tail;
    sky_bytes_t                         zdict;
    sky_bool_t                          eof;
    sky_bool_t                          initialized;
    z_stream                            zst;
} sky_zlib_Decompress_data_t;

SKY_EXTERN_INLINE sky_zlib_Decompress_data_t *
sky_zlib_Decompress_data(sky_object_t self)
{
    return sky_object_data(self, sky_zlib_Decompress_type);
}

static void
sky_zlib_Decompress_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_zlib_Decompress_data_t  *self_data = data;

    if (self_data->initialized) {
        inflateEnd(&(self_data->zst));
        self_data->initialized = SKY_FALSE;
    }
}

static void
sky_zlib_Decompress_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky_zlib_Decompress_data_t  *self_data = data;

    sky_object_visit(self_data->unused_data, visit_data);
    sky_object_visit(self_data->unconsumed_tail, visit_data);
    sky_object_visit(self_data->zdict, visit_data);
}


static sky_object_t
sky_zlib_Decompress_copy(sky_object_t self)
{
    sky_zlib_Decompress_data_t  *self_data = sky_zlib_Decompress_data(self);

    int                         err;
    sky_object_t                copy;
    sky_zlib_Decompress_data_t  *copy_data;

    copy = sky_object_allocate(sky_zlib_Decompress_type);
    copy_data = sky_zlib_Decompress_data(copy);

    err = inflateCopy(&(copy_data->zst), &(self_data->zst));
    switch (err) {
        case Z_OK:
            break;
        case Z_STREAM_ERROR:
            sky_error_raise_string(sky_ValueError,
                                   "Inconsistent stream state");
            break;
        case Z_MEM_ERROR:
            sky_error_raise_string(
                    sky_MemoryError,
                    "Can't allocate memory for decompression object");
            break;
        default:
            sky_zlib_raise(self_data->zst,
                           err,
                           "while copying decompression object");
    }

    copy_data->initialized = SKY_TRUE;
    copy_data->eof = self_data->eof;
    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->unused_data)),
                      self_data->unused_data,
                      copy);
    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->unconsumed_tail)),
                      self_data->unconsumed_tail,
                      copy);
    sky_object_gc_set(SKY_AS_OBJECTP(&(copy_data->zdict)),
                      self_data->zdict,
                      copy);

    return copy;
}

static const char sky_zlib_Decompress_copy_doc[] =
"copy() -- Return a copy of the decompression object";


static void
sky_zlib_Decompress_save_input(sky_object_t self, int err)
{
    sky_zlib_Decompress_data_t  *self_data = sky_zlib_Decompress_data(self);

    if (Z_STREAM_END == err) {
        /* The end of the compressed data has been reached. Store the leftover
         * input data in self->unused_data.
         */
        if (self_data->zst.avail_in > 0) {
            sky_bytes_t new_data;

            SKY_ASSET_BLOCK_BEGIN {
                char            *bytes;
                ssize_t         new_size;
                sky_buffer_t    buffer;

                sky_buffer_acquire(&buffer,
                                   self_data->unused_data,
                                   SKY_BUFFER_FLAG_SIMPLE);
                sky_asset_save(&buffer,
                               (sky_free_t)sky_buffer_release,
                               SKY_ASSET_CLEANUP_ALWAYS);

                if (self_data->zst.avail_in > SSIZE_MAX - buffer.len) {
                    sky_error_raise_object(sky_MemoryError, sky_None);
                }

                new_size = buffer.len + self_data->zst.avail_in;
                bytes = sky_asset_malloc(new_size, SKY_ASSET_CLEANUP_ON_ERROR);
                memcpy(bytes, buffer.buf, buffer.len);
                memcpy(bytes + buffer.len,
                       self_data->zst.next_in,
                       self_data->zst.avail_in);
                new_data = sky_bytes_createwithbytes(bytes, new_size, sky_free);
            } SKY_ASSET_BLOCK_END;

            sky_object_gc_set(
                    SKY_AS_OBJECTP(&(self_data->unused_data)),
                    new_data,
                    self);
            self_data->zst.avail_in = 0;
        }
    }
    if (self_data->zst.avail_in > 0 ||
        sky_object_len(self_data->unconsumed_tail))
    {
        /* This code handles two distinct cases:
         * 1. Output limit was reached. Save leftover input in unconsumed_tail.
         * 2. All input data was consumed. Clear unconsumed_tail.
         */
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->unconsumed_tail)),
                sky_bytes_createfrombytes(self_data->zst.next_in,
                                          self_data->zst.avail_in),
                self);
    }
}


static sky_bytes_t
sky_zlib_Decompress_decompress(
                sky_object_t    self,
                sky_object_t    data,
                int             max_length)
{
    sky_zlib_Decompress_data_t  *self_data = sky_zlib_Decompress_data(self);
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        Byte            *input, *output;
        ssize_t         length, old_length;
        sky_buffer_t    buffer, zdict_buffer;
        unsigned int    input_length;
        unsigned long   start_total_out;

        sky_buffer_acquire(&buffer, data, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len > UINT_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "Size does not fit in an unsigned int");
        }
        input = buffer.buf;
        input_length = buffer.len;

        if (max_length < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "max_length must be greater than zero");
        }

        length = DEFAULTALLOC;
        if (max_length && length > max_length) {
            length = max_length;
        }
        output = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);

        start_total_out = self_data->zst.total_out;
        self_data->zst.avail_in = input_length;
        self_data->zst.next_in = input;
        self_data->zst.avail_out = length;
        self_data->zst.next_out = output;
        err = inflate(&(self_data->zst), Z_SYNC_FLUSH);

        if (Z_NEED_DICT == err && self_data->zdict) {
            sky_buffer_acquire(&zdict_buffer,
                               self_data->zdict,
                               SKY_BUFFER_FLAG_SIMPLE);
            err = inflateSetDictionary(&(self_data->zst),
                                       zdict_buffer.buf,
                                       zdict_buffer.len);
            sky_buffer_release(&zdict_buffer);

            if (Z_OK != err) {
                sky_zlib_raise(self_data->zst,
                               err,
                               "while decompressing data");
            }

            /* Repeat the call to inflate. */
            err = inflate(&(self_data->zst), Z_SYNC_FLUSH);
        }

        /* While Z_OK and the output buffer is full, there might be more output.
         * So extend the output buffer and try again.
         */
        while (Z_OK == err && !self_data->zst.avail_out) {
            /* If max_length set, don't continue decompressing if we've already
             * reached the limit.
             */
            if (max_length && length >= max_length) {
                break;
            }

            old_length = length;
            length <<= 1;
            if (max_length && length > max_length) {
                length = max_length;
            }
            output = sky_asset_realloc(output,
                                       length,
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            self_data->zst.next_out = output + old_length;
            self_data->zst.avail_out = length - old_length;

            err = inflate(&(self_data->zst), Z_SYNC_FLUSH);
        }

        sky_zlib_Decompress_save_input(self, err);

        if (Z_STREAM_END == err) {
            /* This is the logical place to call inflateEnd, but the old
             * behavior of only calling it on flush() is preserved.
             */
            self_data->eof = SKY_TRUE;
        }
        /* We will only get Z_BUF_ERROR if the output buffer was full but there
         * wasn't more output when we tried again, so it is not an error
         * condition.
         */
        else if (Z_OK != err && Z_BUF_ERROR != err)  {
            sky_zlib_raise(self_data->zst,
                           err,
                           "while decompressing data");
        }

        result = sky_bytes_createwithbytes(output,
                                           (self_data->zst.total_out -
                                            start_total_out),
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_Decompress_decompress_doc[] =
"decompress(data, max_length) -- Return a string containing the decompressed\n"
"version of the data.\n\n"
"After calling this function, some of the input data may still be stored in\n"
"internal buffers for later processing.\n"
"Call the flush() method to clear these buffers.\n"
"If the max_length parameter is specified then the return value will be\n"
"no longer than max_length.  Unconsumed input data will be stored in\n"
"the unconsumed_tail attribute.";


static sky_bytes_t
sky_zlib_Decompress_flush(sky_object_t self, int length)
{
    sky_zlib_Decompress_data_t  *self_data = sky_zlib_Decompress_data(self);

    sky_bytes_t result;

    if (length <= 0) {
        sky_error_raise_string(sky_ValueError,
                               "length must be greater than zero");
    }

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        Byte            *output;
        sky_buffer_t    buffer;
        unsigned long   start_total_out;

        sky_buffer_acquire(&buffer,
                           self_data->unconsumed_tail,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        output = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);

        start_total_out = self_data->zst.total_out;
        self_data->zst.avail_in = buffer.len;
        self_data->zst.next_in = buffer.buf;
        self_data->zst.avail_out = length;
        self_data->zst.next_out = output;
        err = inflate(&(self_data->zst), Z_FINISH);

        /* while Z_OK and the output buffer is full, there might be more output,
         * so extend the output buffer and try again
         */
        while ((Z_OK == err || Z_BUF_ERROR == err) && !self_data->zst.avail_out) {
            output = sky_asset_realloc(output,
                                       (length << 1),
                                       SKY_ASSET_CLEANUP_ON_ERROR,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
            self_data->zst.next_out = output + length;
            self_data->zst.avail_out = length;
            length <<= 1;
            err = inflate(&(self_data->zst), Z_FINISH);
        }

        /* If at end of stream, clean up any memory allocated by zlib. */
        if (Z_STREAM_END == err) {
            self_data->eof = SKY_TRUE;
            err = inflateEnd(&(self_data->zst));
            if (Z_OK != err) {
                sky_zlib_raise(self_data->zst,
                               err,
                               "while finishing decompression");
            }
            self_data->initialized = SKY_FALSE;
        }

        result = sky_bytes_createwithbytes(output,
                                           (self_data->zst.total_out -
                                            start_total_out),
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_Decompress_flush_doc[] =
"flush( [length] ) -- Return a string containing any remaining\n"
"decompressed data. length, if given, is the initial size of the\n"
"output buffer.\n\n"
"The decompressor object can no longer be used after this call.";


static unsigned int
sky_zlib_adler32(sky_object_t string, unsigned int start)
{
    unsigned int    result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         len;
        sky_buffer_t    buffer;
        unsigned char   *buf;

        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        buf = buffer.buf;
        len = buffer.len;

        result = start;
        while (len > UINT_MAX) {
            result = adler32(result, buf, UINT_MAX);
            buf += UINT_MAX;
            len -= UINT_MAX;
        }
        result = adler32(result, buf, len);
    } SKY_ASSET_BLOCK_END;

    return result & 0xFFFFFFFF;
}

static const char sky_zlib_adler32_doc[] =
"adler32(string[, start]) -- Compute an Adler-32 checksum of string.\n\n"
"An optional starting value can be specified.  The returned checksum is\n"
"an integer.";


static sky_bytes_t
sky_zlib_compress(sky_object_t string, int level)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        Byte            *input, *output;
        z_stream        zst;
        sky_buffer_t    buffer;
        unsigned int    length;

        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len > UINT_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "Size does not fit in an unsigned int");
        }
        input = buffer.buf;
        length = buffer.len;

        zst.avail_out = length + length / 1000 + 12 + 1;
        output = sky_asset_malloc(zst.avail_out, SKY_ASSET_CLEANUP_ON_ERROR);

        zst.zalloc = NULL;
        zst.zfree = Z_NULL;
        zst.next_out = output;
        zst.next_in = input;
        zst.avail_in = length;
        err = deflateInit(&zst, level);

        switch (err) {
            case Z_OK:
                break;
            case Z_MEM_ERROR:
                sky_error_raise_string(sky_MemoryError,
                                       "Out of memory while compressing data");
                break;
            case Z_STREAM_ERROR:
                sky_error_raise_string(sky_zlib_error,
                                       "Bad compression level");
                break;
            default:
                deflateEnd(&zst);
                sky_zlib_raise(zst, err, "while compressing data");
                break;
        }

        err = deflate(&zst, Z_FINISH);
        if (err != Z_STREAM_END) {
            sky_asset_save(&zst,
                           (sky_free_t)deflateEnd,
                           SKY_ASSET_CLEANUP_ALWAYS);
            sky_zlib_raise(zst, err, "while compressing data");
        }

        err = deflateEnd(&zst);
        if (err != Z_OK) {
            sky_zlib_raise(zst, err, "while finishing compression");
        }
        result = sky_bytes_createwithbytes(output, zst.total_out, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_compress_doc[] =
"compress(string[, level]) -- Returned compressed string.\n\n"
"Optional arg level is the compression level, in 0-9.";


static sky_object_t
sky_zlib_compressobj(
                int             level,
                int             method,
                int             wbits,
                int             memLevel,
                int             strategy,
                sky_object_t    zdict)
{
    int                         err;
    sky_buffer_t                buffer;
    sky_object_t                self;
    sky_zlib_Compress_data_t    *self_data;

    self = sky_object_allocate(sky_zlib_Compress_type);
    self_data = sky_zlib_Compress_data(self);

    self_data->zst.zalloc = NULL;
    self_data->zst.zfree = Z_NULL;
    self_data->zst.next_in = NULL;
    self_data->zst.avail_in = 0;

    err = deflateInit2(&(self_data->zst), level, method, wbits, memLevel, strategy);
    switch (err) {
        case Z_OK:
            self_data->initialized = SKY_TRUE;
            if (zdict) {
                sky_buffer_acquire(&buffer, zdict, SKY_BUFFER_FLAG_SIMPLE);
                err = deflateSetDictionary(&(self_data->zst),
                                           buffer.buf,
                                           buffer.len);
                sky_buffer_release(&buffer);
                switch (err) {
                    case Z_OK:
                        break;
                    case Z_STREAM_ERROR:
                        sky_error_raise_string(sky_ValueError,
                                               "Invalid dictionary");
                        break;
                    default:
                        sky_error_raise_string(sky_ValueError,
                                               "defaultSetDictionary()");
                        break;
                }
            }
            break;
        case Z_MEM_ERROR:
            sky_error_raise_string(sky_MemoryError,
                                   "Can't allocate memory for compression object");
            break;
        case Z_STREAM_ERROR:
            sky_error_raise_string(sky_ValueError,
                                   "Invalid initialization option");
            break;
        default:
            sky_zlib_raise(self_data->zst,
                           err,
                           "while creating compression object");
            break;
    }

    return self;
}

static const char sky_zlib_compressobj_doc[] =
"compressobj(level=-1, method=DEFLATED, wbits=15, memlevel=8,\n"
"            strategy=Z_DEFAULT_STRATEGY[, zdict])\n"
" -- Return a compressor object.\n"
"\n"
"level is the compression level (an integer in the range 0-9; default is 6).\n"
"Higher compression levels are slower, but produce smaller results.\n"
"\n"
"method is the compression algorithm. If given, this must be DEFLATED.\n"
"\n"
"wbits is the base two logarithm of the window size (range: 8..15).\n"
"\n"
"memlevel controls the amount of memory used for internal compression state.\n"
"Valid values range from 1 to 9. Higher values result in higher memory usage,\n"
"faster compression, and smaller output.\n"
"\n"
"strategy is used to tune the compression algorithm. Possible values are\n"
"Z_DEFAULT_STRATEGY, Z_FILTERED, and Z_HUFFMAN_ONLY.\n"
"\n"
"zdict is the predefined compression dictionary - a sequence of bytes\n"
"containing subsequences that are likely to occur in the input data.";


static unsigned int
sky_zlib_crc32(sky_object_t string, unsigned int start)
{
    unsigned int    result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         len;
        sky_buffer_t    buffer;
        unsigned char   *buf;

        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        buf = buffer.buf;
        len = buffer.len;

        result = start;
        while (len > UINT_MAX) {
            result = crc32(result, buf, UINT_MAX);
            buf += UINT_MAX;
            len -= UINT_MAX;
        }
        result = crc32(result, buf, len);
    } SKY_ASSET_BLOCK_END;

    return result & 0xFFFFFFFF;
}

static const char sky_zlib_crc32_doc[] =
"crc32(string[, start]) -- Compute a CRC-32 checksum of string.\n\n"
"An optional starting value can be specified.  The returned checksum is\n"
"an integer.";


static sky_bytes_t
sky_zlib_decompress(sky_object_t string, int wbits, ssize_t bufsize)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        int             err;
        Byte            *input, *output;
        z_stream        zst;
        sky_buffer_t    buffer;
        unsigned int    length;

        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len > UINT_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "Size does not fit in an unsigned int");
        }
        input = buffer.buf;
        length = buffer.len;

        if (bufsize <= 0) {
            bufsize = 1;
        }

        output = sky_asset_malloc(bufsize, SKY_ASSET_CLEANUP_ON_ERROR);

        zst.avail_in = length;
        zst.avail_out = bufsize;
        zst.zalloc = NULL;
        zst.zfree = Z_NULL;
        zst.next_out = output;
        zst.next_in = input;
        err = inflateInit2(&zst, wbits);

        switch (err) {
            case Z_OK:
                break;
            case Z_MEM_ERROR:
                sky_error_raise_string(sky_MemoryError,
                                       "Out of memory while decompressing data");
                break;
            default:
                inflateEnd(&zst);
                sky_zlib_raise(zst,
                               err,
                               "while preparing to decompress data");
                break;
        }

        do {
            err = inflate(&zst, Z_FINISH);
            switch (err) {
                case Z_STREAM_END:
                    break;
                case Z_BUF_ERROR:
                    /* If there is at least 1 byte of room according to
                     * zst.avail_out and we get this error, assume that it means
                     * zlib cannot process the inflate call() due to an error in
                     * the data.
                     */
                    if (zst.avail_out > 0) {
                        inflateEnd(&zst);
                        sky_zlib_raise(zst,
                                       err,
                                       "while decompressing data");
                    }
                    /* fall through */
                case Z_OK:
                    /* need more memory */
                    output = sky_asset_realloc(output,
                                               bufsize << 1,
                                               SKY_ASSET_CLEANUP_ON_ERROR,
                                               SKY_ASSET_UPDATE_FIRST_CURRENT);
                    zst.next_out = output + bufsize;
                    zst.avail_out = bufsize;
                    bufsize <<= 1;
                    break;
                default:
                    inflateEnd(&zst);
                    sky_zlib_raise(zst, err, "while decompressing data");
                    break;
            }
        } while (err != Z_STREAM_END);

        err = inflateEnd(&zst);
        if (err != Z_OK) {
            sky_zlib_raise(zst, err, "while finishing decompression");
        }

        result = sky_bytes_createwithbytes(output, zst.total_out, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_zlib_decompress_doc[] =
"decompress(string[, wbits[, bufsize]]) -- Return decompressed string.\n\n"
"Optional arg wbits is the window buffer size.  Optional arg bufsize is\n"
"the initial output buffer size.";


static sky_object_t
sky_zlib_decompressobj(int wbits, sky_object_t zdict)
{
    int                         err;
    sky_object_t                self;
    sky_zlib_Decompress_data_t  *self_data;

    if (!sky_object_isnull(zdict) && !sky_buffer_check(zdict)) {
        sky_error_raise_string(sky_TypeError,
                               "zdict argument must support the buffer protocol");
    }

    self = sky_object_allocate(sky_zlib_Decompress_type);
    self_data = sky_zlib_Decompress_data(self);
    self_data->zst.zalloc = NULL;
    self_data->zst.zfree = Z_NULL;
    self_data->zst.next_in = NULL;
    self_data->zst.avail_in = 0;
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->unused_data)),
                      sky_bytes_empty,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->unconsumed_tail)),
                      sky_bytes_empty,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->zdict)),
                      zdict,
                      self);

    err = inflateInit2(&(self_data->zst), wbits);
    switch (err) {
        case Z_OK:
            self_data->initialized = SKY_TRUE;
            break;
        case Z_STREAM_ERROR:
            sky_error_raise_string(sky_ValueError,
                                   "Invalid initialization option");
            break;
        case Z_MEM_ERROR:
            sky_error_raise_string(
                    sky_MemoryError,
                    "Can't allocate memory for decompression object");
            break;
        default:
            sky_zlib_raise(self_data->zst,
                           err,
                           "while creating decompression object");
            break;
    }

    return self;
}

static const char sky_zlib_decompressobj_doc[] =
"decompressobj([wbits[, zdict]]) -- Return a decompressor object.\n"
"\n"
"Optional arg wbits is the window buffer size.\n"
"\n"
"Optional arg zdict is the predefined compression dictionary. This must be\n"
"the same dictionary as used by the compressor that produced the input data.";


const char *skython_module_zlib_doc =
"The functions in this module allow compression and decompression using the\n"
"zlib library, which is based on GNU zip.\n"
"\n"
"adler32(string[, start]) -- Compute an Adler-32 checksum.\n"
"compress(string[, level]) -- Compress string, with compression level in 0-9.\n"
"compressobj([level[, ...]]) -- Return a compressor object.\n"
"crc32(string[, start]) -- Compute a CRC-32 checksum.\n"
"decompress(string,[wbits],[bufsize]) -- Decompresses a compressed string.\n"
"decompressobj([wbits[, zdict]]]) -- Return a decompressor object.\n"
"\n"
"'wbits' is window buffer size.\n"
"Compressor objects support compress() and flush() methods; decompressor\n"
"objects support decompress() and flush().";


void
skython_module_zlib_initialize(sky_module_t module)
{
    sky_type_t          type;
    const char          *v;
    sky_type_template_t template;

    sky_module_setattr(
            module,
            "adler32",
            sky_function_createbuiltin(
                    "adler32",
                    sky_zlib_adler32_doc,
                    (sky_native_code_function_t)sky_zlib_adler32,
                    SKY_DATA_TYPE_UINT,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_UINT, sky_integer_one,
                    NULL));
    sky_module_setattr(
            module,
            "compress",
            sky_function_createbuiltin(
                    "compress",
                    sky_zlib_compress_doc,
                    (sky_native_code_function_t)sky_zlib_compress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "level", SKY_DATA_TYPE_INT,
                             sky_integer_create(Z_DEFAULT_COMPRESSION),
                    NULL));
    sky_module_setattr(
            module,
            "compressobj",
            sky_function_createbuiltin(
                    "compressobj",
                    sky_zlib_compressobj_doc,
                    (sky_native_code_function_t)sky_zlib_compressobj,
                    SKY_DATA_TYPE_OBJECT,
                    "level", SKY_DATA_TYPE_INT,
                             sky_integer_create(Z_DEFAULT_COMPRESSION),
                    "method", SKY_DATA_TYPE_INT,
                              sky_integer_create(DEFLATED),
                    "wbits", SKY_DATA_TYPE_INT,
                             sky_integer_create(MAX_WBITS),
                    "memLevel", SKY_DATA_TYPE_INT,
                                sky_integer_create(DEF_MEM_LEVEL),
                    "strategy", SKY_DATA_TYPE_INT,
                                sky_integer_zero,
                    "zdict", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "crc32",
            sky_function_createbuiltin(
                    "crc32",
                    sky_zlib_crc32_doc,
                    (sky_native_code_function_t)sky_zlib_crc32,
                    SKY_DATA_TYPE_UINT,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_UINT, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "decompress",
            sky_function_createbuiltin(
                    "decompress",
                    sky_zlib_decompress_doc,
                    (sky_native_code_function_t)sky_zlib_decompress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    "wbits", SKY_DATA_TYPE_INT,
                             sky_integer_create(DEF_WBITS),
                    "bufsize", SKY_DATA_TYPE_SSIZE_T,
                               sky_integer_create(DEFAULTALLOC),
                    NULL));
    sky_module_setattr(
            module,
            "decompressobj",
            sky_function_createbuiltin(
                    "decompressobj",
                    sky_zlib_decompressobj_doc,
                    (sky_native_code_function_t)sky_zlib_decompressobj,
                    SKY_DATA_TYPE_OBJECT,
                    "wbits", SKY_DATA_TYPE_INT, sky_integer_create(DEF_WBITS),
                    "zdict", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));

    /* Compress type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky_zlib_Compress_instance_finalize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("Compress"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_zlib_Compress_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setattr_builtin(
            type,
            "compress",
            sky_function_createbuiltin(
                    "Compress.compress",
                    sky_zlib_Compress_compress_doc,
                    (sky_native_code_function_t)sky_zlib_Compress_compress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "Compress.copy",
                    sky_zlib_Compress_copy_doc,
                    (sky_native_code_function_t)sky_zlib_Compress_copy,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "flush",
            sky_function_createbuiltin(
                    "Compress.flush",
                    sky_zlib_Compress_flush_doc,
                    (sky_native_code_function_t)sky_zlib_Compress_flush,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "mode", SKY_DATA_TYPE_INT, sky_integer_create(Z_FINISH),
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_zlib_Compress_type),
            type,
            SKY_TRUE);

    /* Decompress type */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky_zlib_Decompress_instance_finalize;
    template.visit = sky_zlib_Decompress_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("Decompress"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_zlib_Decompress_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmembers(type,
            "eof",              NULL,
                                offsetof(sky_zlib_Decompress_data_t,
                                         eof),
                                SKY_DATA_TYPE_BOOL,
                                SKY_MEMBER_FLAG_READONLY,
            "unconsumed_tail",  NULL,
                                offsetof(sky_zlib_Decompress_data_t,
                                         unconsumed_tail),
                                SKY_DATA_TYPE_OBJECT,
                                SKY_MEMBER_FLAG_READONLY,
            "unused_data",      NULL,
                                offsetof(sky_zlib_Decompress_data_t,
                                         unused_data),
                                SKY_DATA_TYPE_OBJECT,
                                SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "Decompress.copy",
                    sky_zlib_Decompress_copy_doc,
                    (sky_native_code_function_t)sky_zlib_Decompress_copy,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "decompress",
            sky_function_createbuiltin(
                    "Decompress.decompress",
                    sky_zlib_Decompress_decompress_doc,
                    (sky_native_code_function_t)sky_zlib_Decompress_decompress,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "max_length", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "flush",
            sky_function_createbuiltin(
                    "Decompress.flush",
                    sky_zlib_Decompress_flush_doc,
                    (sky_native_code_function_t)sky_zlib_Decompress_flush,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "length", SKY_DATA_TYPE_INT, sky_integer_create(DEFAULTALLOC),
                    NULL));
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_zlib_Decompress_type),
            type,
            SKY_TRUE);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_zlib_error),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("error"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_Exception),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "error", sky_zlib_error);

#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n))

    int_const(MAX_WBITS);
    int_const(DEFLATED);
    int_const(DEF_MEM_LEVEL);
    int_const(Z_BEST_SPEED);
    int_const(Z_BEST_COMPRESSION);
    int_const(Z_DEFAULT_COMPRESSION);
    int_const(Z_FILTERED);
    int_const(Z_HUFFMAN_ONLY);
    int_const(Z_DEFAULT_STRATEGY);
    int_const(Z_FINISH);
    int_const(Z_NO_FLUSH);
    int_const(Z_SYNC_FLUSH);
    int_const(Z_FULL_FLUSH);

    v = ZLIB_VERSION;
    sky_module_setattr(module,
                       "ZLIB_VERSION",
                       sky_string_createfrombytes(v, strlen(v), NULL, NULL));

    v = zlibVersion();
    sky_module_setattr(module,
                       "ZLIB_RUNTIME_VERSION",
                       sky_string_createfrombytes(v, strlen(v), NULL, NULL));

    sky_module_setattr(module, "__version__", SKY_STRING_LITERAL("1.0"));
}
