#include "../core/sky_private.h"

#if defined(HAVE_STROPTS_H)
#   include <stropts.h>
#endif
#if defined(HAVE_SYS_FILE_H)
#   include <sys/file.h>
#endif
#if defined(HAVE_SYS_IOCTL_H)
#   include <sys/ioctl.h>
#endif


#if !defined(LOCK_SH)
#   define LOCK_SH  1
#   define LOCK_EX  2
#   define LOCK_NB  4
#   define LOCK_UN  8
#endif


SKY_EXTERN const char *skython_module_fcntl_doc;

SKY_EXTERN void
skython_module_fcntl_initialize(sky_module_t module);


static sky_object_t
sky_fcntl_fcntl(int fd, int op, sky_object_t arg)
{
    int             result;
    sky_object_t    object;

    if (sky_object_isa(arg, sky_integer_type)) {
        result = fcntl(fd,
                       op,
                       (long)sky_integer_value(arg, LONG_MIN, LONG_MAX, NULL));
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, result);
        }
        object = sky_integer_create(result);
    }
    else if (sky_object_isa(arg, sky_string_type)) {
        SKY_ASSET_BLOCK_BEGIN {
            char    *cstring;
            size_t  cstring_len;

            cstring = sky_codec_encodetocstring(arg, 0);
            cstring_len = strlen(cstring);
            sky_asset_save(cstring, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

            result = fcntl(fd, op, cstring);
            if (-1 == result) {
                sky_error_raise_errno(sky_OSError, result);
            }
            object = sky_bytes_createwithbytes(cstring, cstring_len, sky_free);
        } SKY_ASSET_BLOCK_END;
    }
    else if (sky_buffer_check(arg)) {
        SKY_ASSET_BLOCK_BEGIN {
            char            *cstring;
            size_t          cstring_len;
            sky_buffer_t    buffer;

            sky_buffer_acquire(&buffer, arg, SKY_BUFFER_FLAG_SIMPLE);
            cstring = sky_asset_memdup(buffer.buf, buffer.len,
                                       SKY_ASSET_CLEANUP_ON_ERROR);
            cstring_len = buffer.len;
            sky_buffer_release(&buffer);

            result = fcntl(fd, op, cstring);
            if (-1 == result) {
                sky_error_raise_errno(sky_OSError, result);
            }
            object = sky_bytes_createwithbytes(cstring, cstring_len, sky_free);
        } SKY_ASSET_BLOCK_END;
    }
    else {
        sky_error_raise_string(
                sky_TypeError,
                "fcntl requires a file or file descriptor, an integer and "
                "optionally a third integer or a string");
    }

    return object;
}

static const char sky_fcntl_fcntl_doc[] =
"fcntl(fd, op, [arg])\n\n"
"Perform the operation op on file descriptor fd.  The values used\n"
"for op are operating system dependent, and are available\n"
"as constants in the fcntl module, using the same names as used in\n"
"the relevant C header files.  The argument arg is optional, and\n"
"defaults to 0; it may be an int or a string.  If arg is given as a string,\n"
"the return value of fcntl is a string of that length, containing the\n"
"resulting value put in the arg buffer by the operating system.  The length\n"
"of the arg string is not allowed to exceed 1024 bytes.  If the arg given\n"
"is an integer or if none is specified, the result value is an integer\n"
"corresponding to the return value of the fcntl call in the C code.";


#if !defined(HAVE_FLOCK)
static int
sky_fcntl_flock(int fd, int operation)
{
    struct flock    l;

    switch (operation & ~LOCK_NB) {
        case LOCK_UN:
            l.l_type = F_UNLCK;
            break;
        case LOCK_SH:
            l.l_type = F_RDLCK;
            break;
        case LOCK_EX:
            l.l_type = F_WRLCK;
            break;
        default:
            sky_error_raise_string(sky_ValueError,
                                   "unrecognized flock argument");
    }
    l.l_whence = l.l_start = l.l_len = 0;
    return fcntl(fd, ((operation & LOCK_NB) ? F_SETLK : F_SETLKW), &l);
}
#endif

static const char sky_fcntl_flock_doc[] =
"flock(fd, operation)\n\n"
"Perform the lock operation op on file descriptor fd.  See the Unix \n"
"manual page for flock(2) for details.  (On some systems, this function is\n"
"emulated using fcntl().)";


static sky_object_t
sky_fcntl_ioctl(
                int             fd,
                int             op,
                sky_object_t    arg,
                sky_bool_t      mutate_flag)
{
    int             result;
    sky_object_t    object;

    if (sky_object_isa(arg, sky_integer_type)) {
        result = ioctl(fd, op, sky_integer_value(arg, INT_MIN, INT_MAX, NULL));
        if (-1 == result) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        object = sky_integer_create(result);
    }
    else if (sky_object_isa(arg, sky_string_type)) {
        SKY_ASSET_BLOCK_BEGIN {
            char    *cstring;
            size_t  cstring_len;

            cstring = sky_codec_encodetocstring(arg, 0);
            cstring_len = strlen(cstring);
            sky_asset_save(cstring, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

            result = ioctl(fd, op, cstring);
            if (-1 == result) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            object = sky_bytes_createwithbytes(cstring, cstring_len, sky_free);
        } SKY_ASSET_BLOCK_END;
    }
    else if (sky_buffer_check(arg)) {
        SKY_ASSET_BLOCK_BEGIN {
            char            *cstring;
            size_t          cstring_len;
            sky_buffer_t    buffer;

            SKY_ERROR_TRY {
                sky_buffer_acquire(&buffer, arg, SKY_BUFFER_FLAG_SIMPLE |
                                                 SKY_BUFFER_FLAG_WRITABLE);
            } SKY_ERROR_EXCEPT(sky_BufferError) {
                sky_buffer_acquire(&buffer, arg, SKY_BUFFER_FLAG_SIMPLE);
                mutate_flag = SKY_FALSE;
            } SKY_ERROR_TRY_END;

            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);

            cstring_len = buffer.len;
            if (mutate_flag) {
                cstring = buffer.buf;
            }
            else {
                cstring = sky_asset_memdup(buffer.buf, buffer.len,
                                           SKY_ASSET_CLEANUP_ON_ERROR);
            }

            result = ioctl(fd, op, cstring);
            if (-1 == result) {
                sky_error_raise_errno(sky_OSError, errno);
            }

            if (mutate_flag) {
                object = sky_integer_create(result);
            }
            else {
                object = sky_bytes_createwithbytes(cstring,
                                                   cstring_len,
                                                   sky_free);
            }
        } SKY_ASSET_BLOCK_END;
    }
    else {
        sky_error_raise_string(
                sky_TypeError,
                "ioctl requires a file or file descriptor, an integer and "
                "optionally an integer or buffer argument");
    }

    return object;
}


static const char sky_fcntl_ioctl_doc[] =
"ioctl(fd, op[, arg[, mutate_flag]])\n\n"
"Perform the operation op on file descriptor fd.  The values used for op\n"
"are operating system dependent, and are available as constants in the\n"
"fcntl or termios library modules, using the same names as used in the\n"
"relevant C header files.\n"
"\n"
"The argument arg is optional, and defaults to 0; it may be an int or a\n"
"buffer containing character data (most likely a string or an array). \n"
"\n"
"If the argument is a mutable buffer (such as an array) and if the\n"
"mutate_flag argument (which is only allowed in this case) is true then the\n"
"buffer is (in effect) passed to the operating system and changes made by\n"
"the OS will be reflected in the contents of the buffer after the call has\n"
"returned.  The return value is the integer returned by the ioctl system\n"
"call.\n"
"\n"
"If the argument is a mutable buffer and the mutable_flag argument is not\n"
"passed or is false, the behavior is as if a string had been passed.  This\n"
"behavior will change in future releases of Python.\n"
"\n"
"If the argument is an immutable buffer (most likely a string) then a copy\n"
"of the buffer is passed to the operating system and the return value is a\n"
"string of the same length containing whatever the operating system put in\n"
"the buffer.  The length of the arg buffer in this case is not allowed to\n"
"exceed 1024 bytes.\n"
"\n"
"If the arg given is an integer or if none is specified, the result value is\n"
"an integer corresponding to the return value of the ioctl call in the C\n"
"code.";


static int
sky_fcntl_lockf(
                int     fd,
                int     operation,
                off_t   length,
                off_t   start,
                int     whence)
{
    struct flock    l;

    switch (operation & ~LOCK_NB) {
        case LOCK_UN:
            l.l_type = F_UNLCK;
            break;
        case LOCK_SH:
            l.l_type = F_RDLCK;
            break;
        case LOCK_EX:
            l.l_type = F_WRLCK;
            break;
        default:
            sky_error_raise_string(sky_ValueError,
                                   "unrecognized flock argument");
    }
    l.l_whence = whence;
    l.l_start = start;
    l.l_len = length;
    return fcntl(fd, ((operation & LOCK_NB) ? F_SETLK : F_SETLKW), &l);
}

static const char sky_fcntl_lockf_doc[] =
"lockf (fd, operation, length=0, start=0, whence=0)\n\n"
"This is essentially a wrapper around the fcntl() locking calls.  fd is the\n"
"file descriptor of the file to lock or unlock, and operation is one of the\n"
"following values:\n"
"\n"
"    LOCK_UN - unlock\n"
"    LOCK_SH - acquire a shared lock\n"
"    LOCK_EX - acquire an exclusive lock\n"
"\n"
"When operation is LOCK_SH or LOCK_EX, it can also be bitwise ORed with\n"
"LOCK_NB to avoid blocking on lock acquisition.  If LOCK_NB is used and the\n"
"lock cannot be acquired, an IOError will be raised and the exception will\n"
"have an errno attribute set to EACCES or EAGAIN (depending on the operating\n"
"system -- for portability, check for either value).\n"
"\n"
"length is the number of bytes to lock, with the default meaning to lock to\n"
"EOF.  start is the byte offset, relative to whence, to that the lock\n"
"start.  whence is as with fileobj.seek(), specifically:\n"
"\n"
"    0 - relative to the start of the file (SEEK_SET)\n"
"    1 - relative to the current buffer position (SEEK_CUR)\n"
"    2 - relative to the end of the file (SEEK_END)";


const char *skython_module_fcntl_doc =
"This module performs file control and I/O control on file \n"
"descriptors.  It is an interface to the fcntl() and ioctl() Unix\n"
"routines.  File descriptors can be obtained with the fileno() method of\n"
"a file or socket object.";

void
skython_module_fcntl_initialize(sky_module_t module)
{
    sky_module_setattr(
            module,
            "fcntl",
            sky_function_createbuiltin(
                    "fcntl",
                    sky_fcntl_fcntl_doc,
                    (sky_native_code_function_t)sky_fcntl_fcntl,
                    SKY_DATA_TYPE_OBJECT,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "op", SKY_DATA_TYPE_INT, NULL,
                    "arg", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "flock",
            sky_function_createbuiltin(
                    "flock",
                    sky_fcntl_flock_doc,
#if defined(HAVE_FLOCK)
                    (sky_native_code_function_t)flock,
#else
                    (sky_native_code_function_t)sky_fcntl_flock,
#endif
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "operation", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ioctl",
            sky_function_createbuiltin(
                    "ioctl",
                    sky_fcntl_ioctl_doc,
                    (sky_native_code_function_t)sky_fcntl_ioctl,
                    SKY_DATA_TYPE_OBJECT,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "op", SKY_DATA_TYPE_UINT, NULL,
                    "arg", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
                    "mutate_flag", SKY_DATA_TYPE_BOOL, sky_True,
                    NULL));
    sky_module_setattr(
            module,
            "lockf",
            sky_function_createbuiltin(
                    "lockf",
                    sky_fcntl_lockf_doc,
                    (sky_native_code_function_t)sky_fcntl_lockf,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "fd", SKY_DATA_TYPE_FD, NULL,
                    "operation", SKY_DATA_TYPE_INT, NULL,
                    "length", SKY_DATA_TYPE_OFF_T, sky_integer_zero,
                    "start", SKY_DATA_TYPE_OFF_T, sky_integer_zero,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));

#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n))

    int_const(LOCK_SH);
    int_const(LOCK_EX);
    int_const(LOCK_NB);
    int_const(LOCK_UN);
#if defined(LOCK_MAND)
    int_const(LOCK_MAND);
#endif
#if defined(LOCK_READ)
    int_const(LOCK_READ);
#endif
#if defined(LOCK_WRITE)
    int_const(LOCK_WRITE);
#endif
#if defined(LOCK_RW)
    int_const(LOCK_RW);
#endif

    int_const(F_DUPFD);
    int_const(F_GETFD);
    int_const(F_SETFD);
    int_const(F_GETFL);
    int_const(F_SETFL);
    int_const(FD_CLOEXEC);
    int_const(F_GETLK);
    int_const(F_SETLK);
    int_const(F_SETLKW);
    int_const(F_GETOWN);
    int_const(F_SETOWN);

#if defined(F_GETSIG)
    int_const(F_GETSIG);
#endif
#if defined(F_SETSIG)
    int_const(F_SETSIG);
#endif
#if defined(F_RDLCK)
    int_const(F_RDLCK);
#endif
#if defined(F_WRLCK)
    int_const(F_WRLCK);
#endif
#if defined(F_UNLCK)
    int_const(F_UNLCK);
#endif
#if defined(F_GETLK64)
    int_const(F_GETLK64);
#endif
#if defined(F_SETLK64)
    int_const(F_SETLK64);
#endif
#if defined(F_SETLW64)
    int_const(F_SETLW64);
#endif
#if defined(FASYNC)
    int_const(FASYNC);
#endif
#if defined(F_SETLEASE)
    int_const(F_SETLEASE);
#endif
#if defined(F_GETLEASE)
    int_const(F_GETLEASE);
#endif
#if defined(F_NOTIFY)
    int_const(F_NOTIFY);
    int_const(DN_ACCESS);
    int_const(DN_MODIFY);
    int_const(DN_CREATE);
    int_const(DN_DELETE);
    int_const(DN_RENAME);
    int_const(DN_ATTRIB);
    int_const(DN_MULTISHOT);
#endif
#if defined(F_EXLCK)
    int_const(F_EXLCK);
#endif
#if defined(F_SHLCK)
    int_const(F_SHLCK);
#endif
#if defined(F_DUPFD_CLOEXEC)
    int_const(F_DUPFD_CLOEXEC);
#endif
#if defined(F_FULLFSYNC)
    int_const(F_FULLFSYNC);
#endif
#if defined(F_NOCACHE)
    int_const(F_NOCACHE);
#endif
#if defined(F_GETPATH)
    int_const(F_GETPATH);
#endif
#if defined(F_SETNOSIGPIPE)
    int_const(F_SETNOSIGPIPE);
#endif
#if defined(F_GETNOSIGPIPE)
    int_const(F_GETNOSIGPIPE);
#endif

#if defined(HAVE_STROPTS_H)
    int_const(I_PUSH);
    int_const(I_POP);
    int_const(I_LOOK);
    int_const(I_FLUSH);
    int_const(I_FLUSHBAND);
    int_const(I_SETSIG);
    int_const(I_GETSIG);
    int_const(I_FIND);
    int_const(I_PEEK);
    int_const(I_SRDOPT);
    int_const(I_GRDOPT);
    int_const(I_NREAD);
    int_const(I_FDINSERT);
    int_const(I_STR);
    int_const(I_SWROPT);
    int_const(I_GWROPT);
#if defined(ISENDFD)
    int_const(ISENDFD);
#endif
    int_const(I_RECVFD);
    int_const(I_LIST);
    int_const(I_ATMARK);
    int_const(I_CKBAND);
    int_const(I_GETBAND);
    int_const(I_CANPUT);
    int_const(I_SETCLTIME);
    int_const(I_GETCLTIME);
    int_const(I_LINK);
    int_const(I_UNLINK);
    int_const(I_PLINK);
    int_const(I_PUNLINK);
#endif
}
