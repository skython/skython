/* csv module */

/*

This module provides the low-level underpinnings of a CSV reading/writing
module.  Users should not use this module directly, but import the csv.py
module instead.

*/

#define SKY__CSV_MODULE_VERSION "1.0"

#include "../core/sky_private.h"
#include "../core/sky_string_private.h"


SKY_MODULE_EXTERN const char *skython_module__csv_doc;

SKY_MODULE_EXTERN void
skython_module__csv_initialize(sky_module_t module);


typedef enum sky__csv_ParserState_e {
    SKY__CSV_PARSERSTATE_START_RECORD,
    SKY__CSV_PARSERSTATE_START_FIELD,
    SKY__CSV_PARSERSTATE_ESCAPED_CHAR,
    SKY__CSV_PARSERSTATE_IN_FIELD,
    SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD,
    SKY__CSV_PARSERSTATE_ESCAPE_IN_QUOTED_FIELD,
    SKY__CSV_PARSERSTATE_QUOTE_IN_QUOTED_FIELD,
    SKY__CSV_PARSERSTATE_EAT_CRNL,
} sky__csv_ParserState_t;

typedef enum sky__csv_QuoteStyle_e {
    SKY__CSV_QUOTESTYLE_MINIMAL,
    SKY__CSV_QUOTESTYLE_ALL,
    SKY__CSV_QUOTESTYLE_NONNUMERIC,
    SKY__CSV_QUOTESTYLE_NONE,
} sky__csv_QuoteStyle_t;

typedef struct sky__csv_StyleDesc_s {
    sky__csv_QuoteStyle_t               style;
    const char *                        name;
} sky__csv_StyleDesc_t;

static sky__csv_StyleDesc_t sky__csv_quote_styles[] = {
    { SKY__CSV_QUOTESTYLE_MINIMAL,      "QUOTE_MINIMAL"     },
    { SKY__CSV_QUOTESTYLE_ALL,          "QUOTE_ALL"         },
    { SKY__CSV_QUOTESTYLE_NONNUMERIC,   "QUOTE_NONNUMERIC"  },
    { SKY__CSV_QUOTESTYLE_NONE,         "QUOTE_NONE"        },
    { 0,                                NULL                }
};


static ssize_t      sky__csv_field_limit = 128 * 1024;  /* max parsed field size */
static sky_dict_t   sky__csv_dialects = NULL;           /* Dialect registry */
static sky_mutex_t  sky__csv_dialects_lock = NULL;
static sky_object_t sky__csv_error = NULL;              /* CSV exception */

static sky_object_t sky__csv_get_dialect(sky_object_t name);


static sky_type_t sky__csv_Dialect_type = NULL;

static const char sky__csv_Dialect_type_doc[] =
"CSV dialect\n"
"\n"
"The Dialect type records CSV parsing and generation options.\n";

typedef struct sky__csv_Dialect_data_s {
    sky_string_t    lineterminator;     /* string to write between records */

    sky_unicode_char_t  delimiter;      /* field separator */
    sky_unicode_char_t  quotechar;      /* quote character */
    sky_unicode_char_t  escapechar;     /* escape character */

    sky__csv_QuoteStyle_t   quoting;    /* style of quoting to write */

    sky_bool_t  doublequote;            /* is " represented by ""? */
    sky_bool_t  skipinitialspace;       /* ignore spaces following delimiter? */
    sky_bool_t  strict;                 /* raise exception on bad CSV */

    /* are lineterminator, delimiter, quotechar, and escapechar all in the
     * latin-1 character set (i.e., 0x01-0xFF)? if so, use the table for
     * writing to determine escape needs
     */
    sky_bool_t  latin1;
    uint8_t     latin1_table[32];
} sky__csv_Dialect_data_t;

static inline sky__csv_Dialect_data_t *
sky__csv_Dialect_data(sky_object_t object)
{
    return sky_object_data(object, sky__csv_Dialect_type);
}


static sky_type_t sky__csv_reader_type = NULL;

static const char sky__csv_reader_type_doc[] =
"CSV reader\n"
"\n"
"Reader objects are responsible for reading and parsing tabular data\n"
"in CSV format.\n";

typedef struct sky__csv_reader_data_s {
    sky_object_t            input_iter; /* iterate over this for input lines */
    sky_object_t            dialect;    /* parsing dialect */
    sky_list_t              fields;         /* field list for current record */
    sky_string_builder_t    field;          /* temporary buffer */
    ssize_t                 field_len;      /* length of current field */
    sky_bool_t              numeric_field;  /* treat field as numeric */
    sky__csv_ParserState_t  state;          /* current CSV parse state */
    uint64_t                line_num;       /* Source-file line number */
} sky__csv_reader_data_t;

static inline sky__csv_reader_data_t *
sky__csv_reader_data(sky_object_t object)
{
    return sky_object_data(object, sky__csv_reader_type);
}


static sky_type_t sky__csv_writer_type = NULL;

static const char sky__csv_writer_type_doc[] =
"CSV writer\n"
"\n"
"Writer objects are responsible for generating tabular data\n"
"in CSV format from sequence input.\n";

typedef struct sky__csv_writer_data_s {
    sky_object_t        writeline;      /* write output lines to this file */
    sky_object_t        dialect;        /* parsing dialect */
    int                 num_fields;     /* number of fields in record */
} sky__csv_writer_data_t;

static inline sky__csv_writer_data_t *
sky__csv_writer_data(sky_object_t object)
{
    return sky_object_data(object, sky__csv_writer_type);
}


/*
 * DIALECT class
 */

static void
sky__csv_Dialect_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__csv_Dialect_data_t *self_data = data;

    sky_object_visit(self_data->lineterminator, visit_data);
}


static sky_object_t
sky__csv_Dialect_delimiter_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_unicode_char_t  c = sky__csv_Dialect_data(self)->delimiter;

    return (c ? SKY_AS_TYPE(sky_string_createfromcodepoint(c), sky_object_t) :
                SKY_AS_TYPE(sky_None, sky_object_t));
}

static sky_object_t
sky__csv_Dialect_escapechar_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_unicode_char_t  c = sky__csv_Dialect_data(self)->escapechar;

    return (c ? SKY_AS_TYPE(sky_string_createfromcodepoint(c), sky_object_t)
              : SKY_AS_TYPE(sky_None, sky_object_t));
}

static sky_object_t
sky__csv_Dialect_quotechar_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_unicode_char_t  c = sky__csv_Dialect_data(self)->quotechar;

    return (c ? SKY_AS_TYPE(sky_string_createfromcodepoint(c), sky_object_t)
              : SKY_AS_TYPE(sky_None, sky_object_t));
}


static void
sky__csv_Dialect_new_checkquoting(const sky__csv_QuoteStyle_t quoting)
{
    sky__csv_StyleDesc_t    *qs;

    for (qs = sky__csv_quote_styles; qs->name; qs++) {
        if (quoting == qs->style) {
            return;
        }
    }
    sky_error_raise_string(sky_TypeError, "bad \"quoting\" value");
}

static void
sky__csv_Dialect_new_setbool(
        SKY_UNUSED  const char *    name,
                    sky_bool_t *    target,
                    sky_object_t    source,
                    sky_bool_t      default_value)
{
    *target = (source ? sky_object_bool(source) : default_value);
}

static void
sky__csv_Dialect_new_setchar(
                    const char *        name,
                    sky_unicode_char_t *target,
                    sky_object_t        source,
                    sky_unicode_char_t  default_value)
{
    if (!source) {
        *target = default_value;
    }
    else {
        *target = '\0';
        if (!sky_object_isnull(source)) {
            ssize_t len;

            if (!sky_object_isa(source, sky_string_type)) {
                sky_error_raise_format(sky_TypeError,
                                       "\"%s\" must be string, not %@",
                                       name,
                                       sky_type_name(sky_object_type(source)));
            }
            len = sky_string_len(source);
            if (len > 1) {
                sky_error_raise_format(sky_TypeError,
                                       "\"%s\" must be an 1-character string",
                                       name);
            }
            if (len > 0) {
                *target = sky_string_charat(source, 0);
            }
        }
    }
}

static void
sky__csv_Dialect_new_setquoting(
                    const char *            name,
                    sky__csv_QuoteStyle_t * target,
                    sky_object_t            source,
                    sky__csv_QuoteStyle_t   default_value)
{
    if (!source) {
        *target = default_value;
    }
    else {
        if (sky_object_type(source) != sky_integer_type) {
            sky_error_raise_format(sky_TypeError,
                                   "\"%s\" must be an integer",
                                   name);
        }

        SKY_ERROR_TRY {
            *target = sky_integer_value(source, INT_MIN, INT_MAX, NULL);
        } SKY_ERROR_EXCEPT(sky_OverflowError) {
            sky_error_raise_format(sky_ValueError,
                                   "integer out of range for \"%s\"",
                                   name);
        } SKY_ERROR_TRY_END;
    }
}

static void
sky__csv_Dialect_new_setstring(
                    const char *    name,
                    sky_string_t *  target,
                    sky_object_t    source,
                    const char *    default_value)
{
    if (!source) {
        *target = sky_string_createfrombytes(default_value,
                                             strlen(default_value),
                                             NULL,
                                             NULL);
    }
    else {
        *target = NULL;
        if (!sky_object_isnull(source)) {
        }
    }
    if (!source) {
        *target = sky_string_createfrombytes(default_value,
                                             strlen(default_value),
                                             NULL,
                                             NULL);
    }
    else {
        if (sky_object_isnull(source)) {
            *target = NULL;
        }
        else if (sky_object_isa(source, sky_string_type)) {
            *target = source;
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "\"%s\" must be a string",
                                   name);
        }
    }
}


static sky_object_t
sky__csv_Dialect_new(sky_type_t     cls,
                     sky_object_t   dialect,
                     sky_object_t   delimiter,
                     sky_object_t   doublequote,
                     sky_object_t   escapechar,
                     sky_object_t   lineterminator,
                     sky_object_t   quotechar,
                     sky_object_t   quoting,
                     sky_object_t   skipinitialspace,
                     sky_object_t   strict)
{
    sky_object_t            self;
    sky__csv_Dialect_data_t *self_data;

    if (dialect) {
        if (sky_object_isa(dialect, sky_string_type)) {
            dialect = sky__csv_get_dialect(dialect);
        }
        /* Can we reuse this instance? */
        if (sky_object_isa(dialect, sky__csv_Dialect_type) &&
            !delimiter && !doublequote && !escapechar && !lineterminator &&
            !quotechar && !quoting && !skipinitialspace && !strict)
        {
            return dialect;
        }
    }

    self = sky_object_allocate(cls);
    self_data = sky__csv_Dialect_data(self);

    if (dialect) {
#define getattr(_n)                                                 \
        do {                                                        \
            if (!_n) {                                              \
                _n = sky_object_getattr(dialect,                    \
                                        SKY_STRING_LITERAL(#_n),    \
                                        NULL);                      \
            }                                                       \
        } while (0)

        getattr(delimiter);
        getattr(doublequote);
        getattr(escapechar);
        getattr(lineterminator);
        getattr(quotechar);
        getattr(quoting);
        getattr(skipinitialspace);
        getattr(strict);
#undef getattr
    }

    /* check types and convert to C values */
#define setattr(_n, _d, _m)                                         \
        sky__csv_Dialect_new_##_m(#_n, &(self_data->_n), _n, _d)

    setattr(delimiter, ',', setchar);
    setattr(doublequote, SKY_TRUE, setbool);
    setattr(escapechar, '\0', setchar);
    setattr(lineterminator, "\r\n", setstring);
    setattr(quotechar, '"', setchar);
    setattr(quoting, SKY__CSV_QUOTESTYLE_MINIMAL, setquoting);
    setattr(skipinitialspace, SKY_FALSE, setbool);
    setattr(strict, SKY_FALSE, setbool);
#undef setattr

    /* validate options */
    sky__csv_Dialect_new_checkquoting(self_data->quoting);
    if (!self_data->delimiter) {
        sky_error_raise_string(sky_TypeError,
                               "\"delimiter\" must be an 1-character string");
    }
    if (sky_None == quotechar && !quoting) {
        self_data->quoting = SKY__CSV_QUOTESTYLE_NONE;
    }
    if (self_data->quoting != SKY__CSV_QUOTESTYLE_NONE &&
        !self_data->quotechar)
    {
        sky_error_raise_string(sky_TypeError,
                               "\"quotechar\" must be set if quoting enabled");
    }

    if (!self_data->lineterminator) {
        sky_error_raise_string(sky_TypeError,
                               "\"lineterminator\" must be set");
    }

    self_data->latin1 = (self_data->delimiter <= 0xFF &&
                         self_data->doublequote <= 0xFF &&
                         self_data->escapechar <= 0xFF);
    if (self_data->latin1) {
        sky_string_data_t   *string_data, tagged_string_data;

        string_data = sky_string_data(self_data->lineterminator,
                                      &tagged_string_data);
        if (string_data->highest_codepoint > 0xFF) {
            self_data->latin1 = SKY_FALSE;
        }
        else {
            ssize_t i;

#define setbit(_c)                                                      \
        do {                                                            \
            if (_c) {                                                   \
                self_data->latin1_table[(_c) / 8] |= (1 << ((_c) % 8)); \
            }                                                           \
        } while (0)

            setbit(self_data->delimiter);
            setbit(self_data->quotechar);
            setbit(self_data->escapechar);
            for (i = 0; i < string_data->len; ++i) {
                setbit(string_data->data.latin1[i]);
            }
#undef setbit
        }
    }

    return self;
}

/*
 * Return an instance of the dialect type, given a Python instance or kwarg
 * description of the dialect
 */
static sky_object_t
sky__csv__call_dialect(sky_object_t dialect, sky_dict_t kws)
{
    sky_tuple_t     args;

    args = (dialect ? sky_tuple_pack(1, dialect) : sky_tuple_empty);
    return sky_object_call(sky__csv_Dialect_type, args, kws);
}


/*
 * READER
 */

static void
sky__csv_reader_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__csv_reader_data_t  *self_data = data;

    sky_object_visit(self_data->input_iter, visit_data);
    sky_object_visit(self_data->dialect, visit_data);
    sky_object_visit(self_data->fields, visit_data);
    sky_object_visit(self_data->field, visit_data);
}


static sky_object_t
sky__csv_reader_iter(sky_object_t self)
{
    return self;
}

static void
sky__csv_reader_parse_save_field(sky__csv_reader_data_t *self_data)
{
    sky_object_t    field;

    if (!self_data->field) {
        field = sky_string_empty;
    }
    else {
        field = sky_string_builder_finalize(self_data->field);
        self_data->field = NULL;
        self_data->field_len = 0;
    }

    if (self_data->numeric_field) {
        self_data->numeric_field = SKY_FALSE;
        field = sky_number_float(field);
    }
    sky_list_append(self_data->fields, field);
}

static void
sky__csv_reader_parse_add_char(sky_object_t             self,
                               sky__csv_reader_data_t * self_data,
                               sky_unicode_char_t       c)
{
    if (!self_data->field) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->field)),
                          sky_string_builder_create(),
                          self);
    }
    else if (self_data->field_len >= sky__csv_field_limit) {
        sky_error_raise_format(sky__csv_error,
                               "field larger than field limit (%zd)",
                               sky__csv_field_limit);
    }
    sky_string_builder_appendcodepoint(self_data->field, c, 1);
    ++self_data->field_len;
}

static void
sky__csv_reader_parse_process_char(sky_object_t             self,
                                   sky__csv_reader_data_t * self_data,
                                   sky__csv_Dialect_data_t *dialect_data,
                                   sky_unicode_char_t       c)
{
    switch (self_data->state) {
        case SKY__CSV_PARSERSTATE_START_RECORD:
            /* start of record */
            if ('\0' == c) {
                /* empty line - return [] */
                break;
            }
            else if ('\n' == c || '\r' == c) {
                self_data->state = SKY__CSV_PARSERSTATE_EAT_CRNL;
                break;
            }
            /* normal character - handle as START_FIELD */
            self_data->state = SKY__CSV_PARSERSTATE_START_FIELD;
            /* fallthru */

        case SKY__CSV_PARSERSTATE_START_FIELD:
            /* expecting field */
            if ('\n' == c || '\r' == c || '\0' == c) {
                /* save empty field - return [fields] */
                sky__csv_reader_parse_save_field(self_data);
                self_data->state = (c ? SKY__CSV_PARSERSTATE_EAT_CRNL
                                      : SKY__CSV_PARSERSTATE_START_RECORD);
            }
            else if (c == dialect_data->quotechar &&
                     dialect_data->quoting != SKY__CSV_QUOTESTYLE_NONE)
            {
                /* start quoted field */
                self_data->state = SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD;
            }
            else if (c == dialect_data->escapechar) {
                /* possible escaped character */
                self_data->state = SKY__CSV_PARSERSTATE_ESCAPED_CHAR;
            }
            else if (' ' == c && dialect_data->skipinitialspace) {
                /* ignore space at start of field */
            }
            else if (c == dialect_data->delimiter) {
                /* save empty field */
                sky__csv_reader_parse_save_field(self_data);
            }
            else {
                /* begin new unquoted field */
                if (dialect_data->quoting == SKY__CSV_QUOTESTYLE_NONNUMERIC) {
                    self_data->numeric_field = SKY_TRUE;
                }
                sky__csv_reader_parse_add_char(self, self_data, c);
                self_data->state = SKY__CSV_PARSERSTATE_IN_FIELD;
            }
            break;

        case SKY__CSV_PARSERSTATE_ESCAPED_CHAR:
            if ('\0' == c) {
                c = '\n';
            }
            sky__csv_reader_parse_add_char(self, self_data, c);
            self_data->state = SKY__CSV_PARSERSTATE_IN_FIELD;
            break;

        case SKY__CSV_PARSERSTATE_IN_FIELD:
            /* in unquoted field */
            if ('\n' == c || '\r' == c || '\0' == c) {
                /* end of line - return [fields] */
                sky__csv_reader_parse_save_field(self_data);
                self_data->state = (c ? SKY__CSV_PARSERSTATE_EAT_CRNL
                                      : SKY__CSV_PARSERSTATE_START_RECORD);
            }
            else if (c == dialect_data->escapechar) {
                /* possible escaped character */
                self_data->state = SKY__CSV_PARSERSTATE_ESCAPED_CHAR;
            }
            else if (c == dialect_data->delimiter) {
                /* save field - wait for new field */
                sky__csv_reader_parse_save_field(self_data);
                self_data->state = SKY__CSV_PARSERSTATE_START_FIELD;
            }
            else {
                /* normal character - save in field */
                sky__csv_reader_parse_add_char(self, self_data, c);
            }
            break;

        case SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD:
            /* in quoted field */
            if ('\0' == c) {
            }
            else if (c == dialect_data->escapechar) {
                /* Possible escape character */
                self_data->state = SKY__CSV_PARSERSTATE_ESCAPE_IN_QUOTED_FIELD;
            }
            else if (c == dialect_data->quotechar &&
                     dialect_data->quoting != SKY__CSV_QUOTESTYLE_NONE)
            {
                if (dialect_data->doublequote) {
                    /* doublequote; " represented by "" */
                    self_data->state = SKY__CSV_PARSERSTATE_QUOTE_IN_QUOTED_FIELD;
                }
                else {
                    /* end of quote part of field */
                    self_data->state = SKY__CSV_PARSERSTATE_IN_FIELD;
                }
            }
            else {
                /* normal character - save in field */
                sky__csv_reader_parse_add_char(self, self_data, c);
            }
            break;

        case SKY__CSV_PARSERSTATE_ESCAPE_IN_QUOTED_FIELD:
            if ('\0' == c) {
                c = '\n';
            }
            sky__csv_reader_parse_add_char(self, self_data, c);
            self_data->state = SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD;
            break;

        case SKY__CSV_PARSERSTATE_QUOTE_IN_QUOTED_FIELD:
            /* doublequote - seen a quote in an quoted field */
            if (dialect_data->quoting != SKY__CSV_QUOTESTYLE_NONE &&
                c == dialect_data->quotechar)
            {
                /* save "" as " */
                sky__csv_reader_parse_add_char(self, self_data, c);
                self_data->state = SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD;
            }
            else if (c == dialect_data->delimiter) {
                /* save field - wait for new field */
                sky__csv_reader_parse_save_field(self_data);
                self_data->state = SKY__CSV_PARSERSTATE_START_FIELD;
            }
            else if ('\n' == c || '\r' == c || '\0' == c) {
                /* end of line - return [fields] */
                sky__csv_reader_parse_save_field(self_data);
                self_data->state = (c ? SKY__CSV_PARSERSTATE_EAT_CRNL
                                      : SKY__CSV_PARSERSTATE_START_RECORD);
            }
            else if (!dialect_data->strict) {
                sky__csv_reader_parse_add_char(self, self_data, c);
                self_data->state = SKY__CSV_PARSERSTATE_IN_FIELD;
            }
            else {
                /* illegal */
                /* FIXME - sky_format has no wide character support */
                sky_error_raise_format(sky__csv_error,
                                       "'%c' expected after '%c'",
                                       dialect_data->delimiter,
                                       dialect_data->quotechar);
            }
            break;

        case SKY__CSV_PARSERSTATE_EAT_CRNL:
            if ('\n' == c || '\r' == c) {
            }
            else if ('\0' == c) {
                self_data->state = SKY__CSV_PARSERSTATE_START_RECORD;
            }
            else {
                sky_error_raise_string(
                        sky__csv_error,
                        "new-line character seen in unquoted field - "
                        "do you need to open the file in universal-newline mode?");
            }
            break;
    }
}

static void
sky__csv_parse_reset(sky_object_t self, sky__csv_reader_data_t *self_data)
{
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->fields)),
                      sky_list_create(NULL),
                      self);
    self_data->field_len = 0;
    self_data->state = SKY__CSV_PARSERSTATE_START_RECORD;
    self_data->numeric_field = SKY_FALSE;
}

static sky_object_t
sky__csv_reader_next(sky_object_t self)
{
    sky__csv_reader_data_t  *self_data = sky__csv_reader_data(self);
    sky__csv_Dialect_data_t *dialect_data =
                            sky__csv_Dialect_data(self_data->dialect);

    ssize_t             i;
    sky_object_t        fields, line;
    sky_string_data_t   *line_data, line_tagged_data;
    sky_unicode_char_t  c;

    sky__csv_parse_reset(self, self_data);
    do {
        if (!(line = sky_object_next(self_data->input_iter, NULL))) {
            if (self_data->field_len ||
                SKY__CSV_PARSERSTATE_IN_QUOTED_FIELD == self_data->state)
            {
                if (dialect_data->strict) {
                    sky_error_raise_string(sky__csv_error,
                                           "unexpected end of data");
                }
                sky__csv_reader_parse_save_field(self_data);
                break;
            }
            return NULL;
        }

        if (!sky_object_isa(line, sky_string_type)) {
            sky_error_raise_format(
                    sky__csv_error,
                    "iterator should return strings, not %@ "
                    "(did you open the file in text mode?)",
                    sky_type_name(sky_object_type(line)));
        }

#define process_line(_f)                                                \
        do {                                                            \
            for (i = 0; i < line_data->len; ++i) {                      \
                c = line_data->data._f[i];                              \
                if (!c) {                                               \
                    sky_error_raise_string(sky__csv_error,              \
                                           "line contains NULL byte");  \
                }                                                       \
                sky__csv_reader_parse_process_char(self,                \
                                                   self_data,           \
                                                   dialect_data,        \
                                                   c);                  \
            }                                                           \
        } while (0)

        ++self_data->line_num;
        line_data = sky_string_data(line, &line_tagged_data);
        if (line_data->highest_codepoint < 0x100) {
            process_line(latin1);
        }
        else if (line_data->highest_codepoint < 0x10000) {
            process_line(ucs2);
        }
        else {
            process_line(ucs4);
        }
        sky__csv_reader_parse_process_char(self, self_data, dialect_data, 0);
    } while (self_data->state != SKY__CSV_PARSERSTATE_START_RECORD);

    fields = self_data->fields;
    self_data->fields = NULL;

    return fields;
}


static sky_object_t
sky__csv_reader(sky_object_t iterable, sky_object_t dialect, sky_dict_t kws)
{
    sky_object_t            reader;
    sky__csv_reader_data_t  *reader_data;

    reader = sky_object_allocate(sky__csv_reader_type);
    reader_data = sky__csv_reader_data(reader);

    sky__csv_parse_reset(reader, reader_data);
    sky_object_gc_set(SKY_AS_OBJECTP(&(reader_data->input_iter)),
                      sky_object_iter(iterable),
                      reader);
    sky_object_gc_set(SKY_AS_OBJECTP(&(reader_data->dialect)),
                      sky__csv__call_dialect(dialect, kws),
                      reader);

    return reader;
}

static const char sky__csv_reader_doc[] =
"    csv_reader = reader(iterable [, dialect='excel']\n"
"                        [optional keyword args])\n"
"    for row in csv_reader:\n"
"        process(row)\n"
"\n"
"The \"iterable\" argument can be any object that returns a line\n"
"of input for each iteration, such as a file object or a list.  The\n"
"optional \"dialect\" parameter is discussed below.  The function\n"
"also accepts optional keyword arguments which override settings\n"
"provided by the dialect.\n"
"\n"
"The returned object is an iterator.  Each iteration returns a row\n"
"of the CSV file (which can span multiple input lines):\n";


/*
 * WRITER
 */

static void
sky__csv_writer_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__csv_writer_data_t  *self_data = data;

    sky_object_visit(self_data->writeline, visit_data);
    sky_object_visit(self_data->dialect, visit_data);
}


static ssize_t
sky__csv_writer_writefield(sky__csv_writer_data_t *    self_data,
                           sky__csv_Dialect_data_t *   dialect_data,
                           sky_string_builder_t        builder,
                           sky_object_t                field)
{
    ssize_t             i, result;
    sky_bool_t          need_escape, quoted;
    sky_string_data_t   *field_data, field_tagged_data;
    sky_unicode_char_t  c;

    result = 0;
    switch (dialect_data->quoting) {
        case SKY__CSV_QUOTESTYLE_NONNUMERIC:
            quoted = !sky_number_check(field);
            break;
        case SKY__CSV_QUOTESTYLE_ALL:
            quoted = SKY_TRUE;
            break;
        default:
            quoted = SKY_FALSE;
            break;
    }
    if (sky_object_isnull(field)) {
        field = NULL;
    }
    else if (!sky_object_isa(field, sky_string_type)) {
        field = sky_object_str(field);
    }
    field_data = (field ? sky_string_data(field, &field_tagged_data) : NULL);

    if (self_data->num_fields) {
        sky_string_builder_appendcodepoint(builder, dialect_data->delimiter, 1);
    }

    if (!quoted &&
        SKY__CSV_QUOTESTYLE_NONE != dialect_data->quoting &&
        sky_object_bool(field))
    {
#define check_quoting(_f)                                                   \
        do {                                                                \
            for (i = 0; i < field_data->len; ++i) {                         \
                c = field_data->data._f[i];                                 \
                if (dialect_data->latin1) {                                 \
                    need_escape =                                           \
                            dialect_data->latin1_table[c / 8] &             \
                            (1 << (c % 8));                                 \
                }                                                           \
                else {                                                      \
                    need_escape =                                           \
                            (dialect_data->delimiter == c) ||               \
                            (dialect_data->escapechar == c) ||              \
                            (dialect_data->quotechar == c) ||               \
                            sky_object_contains(                            \
                                    dialect_data->lineterminator,           \
                                    sky_string_createfromcodepoint(c));     \
                }                                                           \
                if (need_escape &&                                          \
                    SKY__CSV_QUOTESTYLE_NONE != dialect_data->quoting &&    \
                    (dialect_data->quotechar != c ||                        \
                     dialect_data->doublequote))                            \
                {                                                           \
                    quoted = SKY_TRUE;                                      \
                    break;                                                  \
                }                                                           \
            }                                                               \
        } while (0)

        if (field_data->highest_codepoint < 0x100) {
            check_quoting(latin1);
        }
        else if (field_data->highest_codepoint < 0x10000) {
            check_quoting(ucs2);
        }
        else {
            check_quoting(ucs4);
        }
#undef check_quoting
    }

    if (quoted) {
        sky_string_builder_appendcodepoint(builder, dialect_data->quotechar, 1);
    }

    if (sky_object_bool(field)) {
        result += field_data->len;

#define write_field(_f)                                                     \
        do {                                                                \
            for (i = 0; i < field_data->len; ++i) {                         \
                c = field_data->data._f[i];                                 \
                if (dialect_data->latin1) {                                 \
                    need_escape =                                           \
                            dialect_data->latin1_table[c / 8] &             \
                            (1 << (c % 8));                                 \
                }                                                           \
                else {                                                      \
                    need_escape =                                           \
                            (dialect_data->delimiter == c) ||               \
                            (dialect_data->escapechar == c) ||              \
                            (dialect_data->quotechar == c) ||               \
                            sky_object_contains(                            \
                                    dialect_data->lineterminator,           \
                                    sky_string_createfromcodepoint(c));     \
                }                                                           \
                if (need_escape) {                                          \
                    sky_bool_t  want_escape = SKY_FALSE;                    \
                                                                            \
                    if (SKY__CSV_QUOTESTYLE_NONE ==                         \
                            dialect_data->quoting)                          \
                    {                                                       \
                        want_escape = SKY_TRUE;                             \
                    }                                                       \
                    else {                                                  \
                        if (dialect_data->quotechar == c) {                 \
                            if (!dialect_data->doublequote) {               \
                                want_escape = SKY_TRUE;                     \
                            }                                               \
                            else {                                          \
                                sky_string_builder_appendcodepoint(         \
                                        builder,                            \
                                        dialect_data->quotechar,            \
                                        1);                                 \
                            }                                               \
                        }                                                   \
                    }                                                       \
                    if (want_escape) {                                      \
                        if (!dialect_data->escapechar) {                    \
                            sky_error_raise_string(                         \
                                    sky__csv_error,                         \
                                    "need to escape, "                      \
                                    "but no escapechar set");               \
                        }                                                   \
                        sky_string_builder_appendcodepoint(                 \
                                builder,                                    \
                                dialect_data->escapechar,                   \
                                1);                                         \
                    }                                                       \
                }                                                           \
                sky_string_builder_appendcodepoint(builder, c, 1);          \
            }                                                               \
        } while (0)

        if (field_data->highest_codepoint < 0x100) {
            write_field(latin1);
        }
        else if (field_data->highest_codepoint < 0x10000) {
            write_field(ucs2);
        }
        else {
            write_field(ucs4);
        }
#undef write_field
    }

    if (quoted) {
        sky_string_builder_appendcodepoint(builder, dialect_data->quotechar, 1);
    }
    ++self_data->num_fields;
    return result;
}

static sky_object_t
sky__csv_writer_writerow(sky_object_t self, sky_object_t sequence)
{
    sky__csv_writer_data_t  *self_data = sky__csv_writer_data(self);
    sky__csv_Dialect_data_t *dialect_data =
                            sky__csv_Dialect_data(self_data->dialect);

    ssize_t                 n;
    sky_tuple_t             args;
    sky_object_t            field;
    sky_string_builder_t    builder;

    builder = sky_string_builder_create();
    self_data->num_fields = 0;
    n = 0;

    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, field) {
            n += sky__csv_writer_writefield(self_data,
                                            dialect_data,
                                            builder,
                                            field);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else if (sky_sequence_check(sequence)) {
        sky_object_t    iter;

        iter = sky_object_iter(sequence);
        while ((field = sky_object_next(iter, NULL)) != NULL) {
            n += sky__csv_writer_writefield(self_data,
                                            dialect_data,
                                            builder,
                                            field);
        }
    }
    else {
        sky_error_raise_string(sky__csv_error, "sequence expected");
    }

    if (!n && 1 == self_data->num_fields) {
        if (SKY__CSV_QUOTESTYLE_NONE == dialect_data->quoting) {
            sky_error_raise_string(sky__csv_error,
                                   "single empty field record must be quoted");
        }
        sky_string_builder_appendcodepoint(builder, dialect_data->quotechar, 2);
    }

    sky_string_builder_append(builder, dialect_data->lineterminator);
    args = sky_tuple_pack(1, sky_string_builder_finalize(builder));
    return sky_object_call(self_data->writeline, args, NULL);
}

static const char sky__csv_writer_writerow_doc[] =
"writerow(sequence)\n"
"\n"
"Construct and write a CSV record from a sequence of fields.  Non-string\n"
"elements will be converted to string.";

static void
sky__csv_writer_writerows(sky_object_t self, sky_object_t sequence)
{
    sky_object_t    iter, object;

    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, object) {
            sky__csv_writer_writerow(self, object);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(sequence);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            sky__csv_writer_writerow(self, object);
        }
    }
}

static const char sky__csv_writer_writerows_doc[] =
"writerows(sequence of sequences)\n"
"\n"
"Construct and write a series of sequences to a csv file.  Non-string\n"
"elements will be converted to string.";

static sky_object_t
sky__csv_writer(sky_object_t fileobj, sky_object_t dialect, sky_dict_t kws)
{
    sky_object_t            writer;
    sky__csv_writer_data_t  *writer_data;

    writer = sky_object_allocate(sky__csv_writer_type);
    writer_data = sky__csv_writer_data(writer);

    sky_object_gc_set(SKY_AS_OBJECTP(&(writer_data->writeline)),
                      sky_object_getattr(fileobj,
                                         SKY_STRING_LITERAL("write"),
                                         NULL),
                      writer);
    if (!writer_data->writeline ||
        !sky_object_callable(writer_data->writeline))
    {
        sky_error_raise_string(sky_TypeError,
                               "argument 1 must have a \"write\" method");
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(writer_data->dialect)),
                      sky__csv__call_dialect(dialect, kws),
                      writer);

    return writer;
}

static const char sky__csv_writer_doc[] =
"    csv_writer = csv.writer(fileobj [, dialect='excel']\n"
"                            [optional keyword args])\n"
"    for row in sequence:\n"
"        csv_writer.writerow(row)\n"
"\n"
"    [or]\n"
"\n"
"    csv_writer = csv.writer(fileobj [, dialect='excel']\n"
"                            [optional keyword args])\n"
"    csv_writer.writerows(rows)\n"
"\n"
"The \"fileobj\" argument can be any object that supports the file API.\n";

/*
 * DIALECT REGISTRY
 */

static ssize_t
sky__csv_field_size_limit(sky_object_t limit)
{
    ssize_t old_limit = sky__csv_field_limit;

    if (!sky_object_isnull(limit)) {
        sky__csv_field_limit = sky_integer_value(limit,
                                                 SSIZE_MIN,
                                                 SSIZE_MAX,
                                                 NULL);
    }

    return old_limit;
}

static const char sky__csv_field_size_limit_doc[] =
"Sets an upper limit on parsed fields.\n"
"    csv.field_size_limit([limit])\n"
"\n"
"Returns old limit. If limit is not given, no new limit is set and\n"
"the old limit is returned";

static sky_object_t
sky__csv_get_dialect(sky_object_t name)
{
    sky_object_t    dialect;

    if (!(dialect = sky_dict_get(sky__csv_dialects, name, NULL))) {
        sky_error_raise_string(sky__csv_error, "unknown dialect");
    }

    return dialect;
}

static const char sky__csv_get_dialect_doc[] =
"Return the dialect instance associated with name.\n"
"    dialect = csv.get_dialect(name)";

static sky_object_t
sky__csv_list_dialects(void)
{
    return sky_list_create(sky_dict_keys(sky__csv_dialects));
}

static const char sky__csv_list_dialects_doc[] =
"Return a list of all know dialect names.\n"
"    names = csv.list_dialects()";

static void
sky__csv_register_dialect(sky_string_t name, sky_object_t dialect, sky_dict_t kws)
{
    dialect = sky__csv__call_dialect(dialect, kws);

    SKY_ASSET_BLOCK_BEGIN {
        sky_mutex_lock(sky__csv_dialects_lock, SKY_TIME_INFINITE);
        sky_asset_save(sky__csv_dialects_lock,
                       (sky_free_t)sky_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_dict_setitem(sky__csv_dialects, name, dialect);
    } SKY_ASSET_BLOCK_END;
}

static const char sky__csv_register_dialect_doc[] =
"Create a mapping from a string name to a dialect class.\n"
"    dialect = csv.register_dialect(name, dialect)";

static void
sky__csv_unregister_dialect(sky_object_t name)
{
    SKY_ERROR_TRY {
        sky_dict_delitem(sky__csv_dialects, name);
    } SKY_ERROR_EXCEPT(sky_KeyError) {
        sky_error_raise_string(sky__csv_error, "unknown dialect");
    } SKY_ERROR_TRY_END;
}

static const char sky__csv_unregister_dialect_doc[] =
"Delete the name/dialect mapping associated with a string name.\n"
"    csv.unregister_dialect(name)";


/*
 * MODULE
 */

const char *skython_module__csv_doc =
"CSV parsing and writing.\n"
"\n"
"This module provides classes that assist in the reading and writing\n"
"of Comma Separated Value (CSV) files, and implements the interface\n"
"described by PEP 305.  Although many CSV files are simple to parse,\n"
"the format is not formally defined by a stable specification and\n"
"is subtle enough that parsing lines of a CSV file with something\n"
"like line.split(\",\") is bound to fail.  The module supports three\n"
"basic APIs: reading, writing, and registration of dialects.\n"
"\n"
"\n"
"DIALECT REGISTRATION:\n"
"\n"
"Readers and writers support a dialect argument, which is a convenient\n"
"handle on a group of settings.  When the dialect argument is a string,\n"
"it identifies one of the dialects previously registered with the module.\n"
"If it is a class or instance, the attributes of the argument are used as\n"
"the settings for the reader or writer:\n"
"\n"
"    class excel:\n"
"        delimiter = ','\n"
"        quotechar = '\"'\n"
"        escapechar = None\n"
"        doublequote = True\n"
"        skipinitialspace = False\n"
"        lineterminator = '\\r\\n'\n"
"        quoting = QUOTE_MINIMAL\n"
"\n"
"SETTINGS:\n"
"\n"
"    * quotechar - specifies a one-character string to use as the \n"
"        quoting character.  It defaults to '\"'.\n"
"    * delimiter - specifies a one-character string to use as the \n"
"        field separator.  It defaults to ','.\n"
"    * skipinitialspace - specifies how to interpret whitespace which\n"
"        immediately follows a delimiter.  It defaults to False, which\n"
"        means that whitespace immediately following a delimiter is part\n"
"        of the following field.\n"
"    * lineterminator -  specifies the character sequence which should \n"
"        terminate rows.\n"
"    * quoting - controls when quotes should be generated by the writer.\n"
"        It can take on any of the following module constants:\n"
"\n"
"        csv.QUOTE_MINIMAL means only when required, for example, when a\n"
"            field contains either the quotechar or the delimiter\n"
"        csv.QUOTE_ALL means that quotes are always placed around fields.\n"
"        csv.QUOTE_NONNUMERIC means that quotes are always placed around\n"
"            fields which do not parse as integers or floating point\n"
"            numbers.\n"
"        csv.QUOTE_NONE means that quotes are never placed around fields.\n"
"    * escapechar - specifies a one-character string used to escape \n"
"        the delimiter when quoting is set to QUOTE_NONE.\n"
"    * doublequote - controls the handling of quotes inside fields.  When\n"
"        True, two consecutive quotes are interpreted as one during read,\n"
"        and when writing, each quote character embedded in the data is\n"
"        written as two quotes\n";

void
skython_module__csv_initialize(sky_module_t module)
{
    sky_type_t              type;
    sky_type_template_t     template;
    sky__csv_StyleDesc_t    *style;

    sky__csv_dialects = sky_dict_create();
    for (style = sky__csv_quote_styles; style->name; style++) {
        sky_module_setattr(module,
                           style->name,
                           sky_integer_create(style->style));
    }
    sky_module_setattr(module, "_dialects", sky__csv_dialects);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__csv_dialects_lock),
                                 sky_mutex_create(SKY_FALSE, SKY_TRUE),
                                 SKY_TRUE);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__csv_Dialect_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("Dialect"),
                                   sky__csv_Dialect_type_doc,
                                   0,
                                   sizeof(sky__csv_Dialect_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmembers(type,
            "doublequote",      NULL,
                                offsetof(sky__csv_Dialect_data_t, doublequote),
                                SKY_DATA_TYPE_BOOL,
                                SKY_MEMBER_FLAG_READONLY,
            "lineterminator",   NULL,
                                offsetof(sky__csv_Dialect_data_t, lineterminator),
                                SKY_DATA_TYPE_OBJECT_STRING,
                                SKY_MEMBER_FLAG_READONLY,
            "quoting",          NULL,
                                offsetof(sky__csv_Dialect_data_t, quoting),
                                SKY_DATA_TYPE_UINT,
                                SKY_MEMBER_FLAG_READONLY,
            "skipinitialspace", NULL,
                                offsetof(sky__csv_Dialect_data_t, skipinitialspace),
                                SKY_DATA_TYPE_BOOL,
                                SKY_MEMBER_FLAG_READONLY,
            "strict",           NULL,
                                offsetof(sky__csv_Dialect_data_t, strict),
                                SKY_DATA_TYPE_BOOL,
                                SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setgetsets(type,
            "delimiter",    NULL, sky__csv_Dialect_delimiter_getter, NULL,
            "escapechar",   NULL, sky__csv_Dialect_escapechar_getter, NULL,
            "quotechar",    NULL, sky__csv_Dialect_quotechar_getter, NULL,
            NULL);

    sky_type_setmethodslotwithparameters(
            type,
            "__new__",
            (sky_native_code_function_t)sky__csv_Dialect_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "dialect", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "delimiter", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "doublequote", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "escapechar", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "lineterminator", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "quotechar", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "quoting", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "skipinitialspace", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "strict", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__csv_Dialect_type),
                                 type,
                                 SKY_TRUE);
    sky_module_setattr(module, "Dialect", sky__csv_Dialect_type);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__csv_reader_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("reader"),
                                   sky__csv_reader_type_doc,
                                   0,
                                   sizeof(sky__csv_reader_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmembers(type,
            "dialect",  NULL,
                        offsetof(sky__csv_reader_data_t, dialect),
                        SKY_DATA_TYPE_OBJECT,
                        SKY_MEMBER_FLAG_READONLY,
            "line_num", NULL,
                        offsetof(sky__csv_reader_data_t, line_num),
                        SKY_DATA_TYPE_UINT64,
                        SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__iter__", sky__csv_reader_iter,
            "__next__", sky__csv_reader_next,
            NULL);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__csv_reader_type),
                                 type,
                                 SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__csv_writer_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("writer"),
                                   sky__csv_writer_type_doc,
                                   0,
                                   sizeof(sky__csv_writer_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setmembers(type,
            "dialect",  NULL,
                        offsetof(sky__csv_writer_data_t, dialect),
                        SKY_DATA_TYPE_OBJECT,
                        SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setattr_builtin(
            type,
            "writerow",
            sky_function_createbuiltin(
                    "writer.writerow",
                    sky__csv_writer_writerow_doc,
                    (sky_native_code_function_t)sky__csv_writer_writerow,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "sequence", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "writerows",
            sky_function_createbuiltin(
                    "writer.writerows",
                    sky__csv_writer_writerows_doc,
                    (sky_native_code_function_t)sky__csv_writer_writerows,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "sequence", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky__csv_writer_type),
                                 type,
                                 SKY_TRUE);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__csv_error),
            sky_object_call(sky_type_type,
                            sky_object_build("(s(O){})",
                                             "Error",
                                             sky_Exception),
                            NULL),
            SKY_TRUE);
    sky_module_setattr(module, "Error", sky__csv_error);

    sky_module_setattr(
            module,
            "field_size_limit",
            sky_function_createbuiltin(
                    "field_size_limit",
                    sky__csv_field_size_limit_doc,
                    (sky_native_code_function_t)sky__csv_field_size_limit,
                    SKY_DATA_TYPE_SSIZE_T,
                    "limit", SKY_DATA_TYPE_OBJECT_INTEGER, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "get_dialect",
            sky_function_createbuiltin(
                    "get_dialect",
                    sky__csv_get_dialect_doc,
                    (sky_native_code_function_t)sky__csv_get_dialect,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, sky__csv_Dialect_type,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "list_dialects",
            sky_function_createbuiltin(
                    "list_dialects",
                    sky__csv_list_dialects_doc,
                    (sky_native_code_function_t)sky__csv_list_dialects,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "reader",
            sky_function_createbuiltin(
                    "reader",
                    sky__csv_reader_doc,
                    (sky_native_code_function_t)sky__csv_reader,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, sky__csv_reader_type,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    "dialect", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "register_dialect",
            sky_function_createbuiltin(
                    "register_dialect",
                    sky__csv_register_dialect_doc,
                    (sky_native_code_function_t)sky__csv_register_dialect,
                    SKY_DATA_TYPE_VOID,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "dialect", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "unregister_dialect",
            sky_function_createbuiltin(
                    "unregister_dialect",
                    sky__csv_unregister_dialect_doc,
                    (sky_native_code_function_t)sky__csv_unregister_dialect,
                    SKY_DATA_TYPE_VOID,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "writer",
            sky_function_createbuiltin(
                    "writer",
                    sky__csv_writer_doc,
                    (sky_native_code_function_t)sky__csv_writer,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, sky__csv_writer_type,
                    "fileobj", SKY_DATA_TYPE_OBJECT, NULL,
                    "dialect", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));

    sky_module_setattr(module,
                       "__version__",
                       SKY_STRING_LITERAL(SKY__CSV_MODULE_VERSION));
}
