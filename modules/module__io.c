#include "../core/sky_private.h"
#include "../core/sky_string_private.h"

#include "module__io.h"

#include <sys/stat.h>


sky_string_t sky__io_cr = NULL;
sky_string_t sky__io_crlf = NULL;
sky_string_t sky__io_lf = NULL;

sky_type_t sky_UnsupportedOperation = NULL;


ssize_t
sky__io_line_ending(
                sky_string_t    string,
                ssize_t         start,
                sky_bool_t      read_translate,
                sky_bool_t      read_universal,
                sky_string_t    read_newline)
{
    ssize_t             pos;
    sky_string_data_t   *string_data, tagged_data;

    if (read_translate) {
        /* Newlines are already translated, only search for \n */
        return sky_string_find(string, sky__io_lf, start, SSIZE_MAX);
    }
    if (!read_universal) {
        pos = sky_string_find(string, read_newline, start, SSIZE_MAX);
        if (-1 != pos) {
            pos += sky_object_len(read_newline) - 1;
        }
        return pos;
    }

    /* Universal newline search. Find any of \r, \r\n, \n
     * The decoder ensures that \r\n are not split in two pieces
     */
    string_data = sky_string_data(string, &tagged_data);
    if (string_data->highest_codepoint < 0x100) {
        const uint8_t   *c, *end;

        end = string_data->data.latin1 + string_data->len;
        for (c = &(string_data->data.latin1[start]); c < end; ++c) {
            if ('\n' == *c) {
                return c - string_data->data.latin1;
            }
            if ('\r' == *c) {
                return ('\n' == c[1] ? (c + 1) - string_data->data.latin1
                                     : c - string_data->data.latin1);
            }
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        const uint16_t  *c, *end;

        end = string_data->data.ucs2 + string_data->len;
        for (c = &(string_data->data.ucs2[start]); c < end; ++c) {
            if ('\n' == *c) {
                return c - string_data->data.ucs2;
            }
            if ('\r' == *c) {
                return ('\n' == c[1] ? (c + 1) - string_data->data.ucs2
                                     : c - string_data->data.ucs2);
            }
        }
    }
    else {
        const sky_unicode_char_t    *c, *end;

        end = string_data->data.ucs4 + string_data->len;
        for (c = string_data->data.ucs4; c < end; ++c) {
            if ('\n' == *c) {
                return c - string_data->data.ucs4;
            }
            if ('\r' == *c) {
                return ('\n' == c[1] ? (c + 1) - string_data->data.ucs4
                                     : c - string_data->data.ucs4);
            }
        }
    }

    return -1;
}


static const char sky__io_open_doc[] =
"open(file, mode='r', buffering=-1, encoding=None,\n"
"     errors=None, newline=None, closefd=True, opener=None) -> file object\n"
"\n"
"Open file and return a stream.  Raise IOError upon failure.\n"
"\n"
"file is either a text or byte string giving the name (and the path\n"
"if the file isn't in the current working directory) of the file to\n"
"be opened or an integer file descriptor of the file to be\n"
"wrapped. (If a file descriptor is given, it is closed when the\n"
"returned I/O object is closed, unless closefd is set to False.)\n"
"\n"
"mode is an optional string that specifies the mode in which the file\n"
"is opened. It defaults to 'r' which means open for reading in text\n"
"mode.  Other common values are 'w' for writing (truncating the file if\n"
"it already exists), 'x' for creating and writing to a new file, and\n"
"'a' for appending (which on some Unix systems, means that all writes\n"
"append to the end of the file regardless of the current seek position).\n"
"In text mode, if encoding is not specified the encoding used is platform\n"
"dependent: locale.getpreferredencoding(False) is called to get the\n"
"current locale encoding. (For reading and writing raw bytes use binary\n"
"mode and leave encoding unspecified.) The available modes are:\n"
"\n"
"========= ===============================================================\n"
"Character Meaning\n"
"--------- ---------------------------------------------------------------\n"
"'r'       open for reading (default)\n"
"'w'       open for writing, truncating the file first\n"
"'x'       create a new file and open it for writing\n"
"'a'       open for writing, appending to the end of the file if it exists\n"
"'b'       binary mode\n"
"'t'       text mode (default)\n"
"'+'       open a disk file for updating (reading and writing)\n"
"'U'       universal newline mode (for backwards compatibility; unneeded\n"
"          for new code)\n"
"========= ===============================================================\n"
"\n"
"The default mode is 'rt' (open for reading text). For binary random\n"
"access, the mode 'w+b' opens and truncates the file to 0 bytes, while\n"
"'r+b' opens the file without truncation. The 'x' mode implies 'w' and\n"
"raises an `FileExistsError` if the file already exists.\n"
"\n"
"Python distinguishes between files opened in binary and text modes,\n"
"even when the underlying operating system doesn't. Files opened in\n"
"binary mode (appending 'b' to the mode argument) return contents as\n"
"bytes objects without any decoding. In text mode (the default, or when\n"
"'t' is appended to the mode argument), the contents of the file are\n"
"returned as strings, the bytes having been first decoded using a\n"
"platform-dependent encoding or using the specified encoding if given.\n"
"\n"
"buffering is an optional integer used to set the buffering policy.\n"
"Pass 0 to switch buffering off (only allowed in binary mode), 1 to select\n"
"line buffering (only usable in text mode), and an integer > 1 to indicate\n"
"the size of a fixed-size chunk buffer.  When no buffering argument is\n"
"given, the default buffering policy works as follows:\n"
"\n"
"* Binary files are buffered in fixed-size chunks; the size of the buffer\n"
"  is chosen using a heuristic trying to determine the underlying device's\n"
"  \"block size\" and falling back on `io.DEFAULT_BUFFER_SIZE`.\n"
"  On many systems, the buffer will typically be 4096 or 8192 bytes long.\n"
"\n"
"* \"Interactive\" text files (files for which isatty() returns True)\n"
"  use line buffering.  Other text files use the policy described above\n"
"  for binary files.\n"
"\n"
"encoding is the name of the encoding used to decode or encode the\n"
"file. This should only be used in text mode. The default encoding is\n"
"platform dependent, but any encoding supported by Python can be\n"
"passed.  See the codecs module for the list of supported encodings.\n"
"\n"
"errors is an optional string that specifies how encoding errors are to\n"
"be handled---this argument should not be used in binary mode. Pass\n"
"'strict' to raise a ValueError exception if there is an encoding error\n"
"(the default of None has the same effect), or pass 'ignore' to ignore\n"
"errors. (Note that ignoring encoding errors can lead to data loss.)\n"
"See the documentation for codecs.register or run 'help(codecs.Codec)'\n"
"for a list of the permitted encoding error strings.\n"
"\n"
"newline controls how universal newlines works (it only applies to text\n"
"mode). It can be None, '', '\\n', '\\r', and '\\r\\n'.  It works as\n"
"follows:\n"
"\n"
"* On input, if newline is None, universal newlines mode is\n"
"  enabled. Lines in the input can end in '\\n', '\\r', or '\\r\\n', and\n"
"  these are translated into '\\n' before being returned to the\n"
"  caller. If it is '', universal newline mode is enabled, but line\n"
"  endings are returned to the caller untranslated. If it has any of\n"
"  the other legal values, input lines are only terminated by the given\n"
"  string, and the line ending is returned to the caller untranslated.\n"
"\n"
"* On output, if newline is None, any '\\n' characters written are\n"
"  translated to the system default line separator, os.linesep. If\n"
"  newline is '' or '\n', no translation takes place. If newline is any\n"
"  of the other legal values, any '\\n' characters written are translated\n"
"  to the given string.\n"
"\n"
"If closefd is False, the underlying file descriptor will be kept open\n"
"when the file is closed. This does not work when a file name is given\n"
"and must be True in that case.\n"
"\n"
"A custom opener can be used by passing a callable as *opener*. The\n"
"underlying file descriptor for the file object is then obtained by\n"
"calling *opener* with (*file*, *flags*). *opener* must return an open\n"
"file descriptor (passing os.open as *opener* results in functionality\n"
"similar to passing None).\n"
"\n"
"open() returns a file object whose type depends on the mode, and\n"
"through which the standard file operations such as reading and writing\n"
"are performed. When open() is used to open a file in a text mode ('w',\n"
"'r', 'wt', 'rt', etc.), it returns a TextIOWrapper. When used to open\n"
"a file in a binary mode, the returned class varies: in read binary\n"
"mode, it returns a BufferedReader; in write binary and append binary\n"
"modes, it returns a BufferedWriter, and in read/write mode, it returns\n"
"a BufferedRandom.\n"
"\n"
"It is also possible to use a string or bytearray as a file for both\n"
"reading and writing. For strings StringIO can be used like a file\n"
"opened in a text mode, and for bytes a BytesIO can be used like a file\n"
"opened in a binary mode.\n";


sky_object_t
sky__io_open(
                sky_object_t    file,
                sky_string_t    mode,
                int             buffering,
                sky_string_t    encoding,
                sky_string_t    errors,
                sky_string_t    newline,
                sky_bool_t      closefd,
                sky_object_t    opener)
{
    int             fileno;
    char            fileio_mode[3];
    const char      *c, *cstring;
    sky_bool_t      binary, isatty, line_buffering, text, universal;
    sky_tuple_t     args;
    struct stat     st;
    sky_object_t    value;

    if (!sky_object_isa(file, sky_string_type) &&
        !sky_object_isa(file, sky_bytes_type) &&
        !SKY_OBJECT_METHOD_SLOT(file, INDEX))
    {
        sky_error_raise_format(sky_TypeError, "invalid file: %#@", file);
    }

    binary = line_buffering = text = universal = SKY_FALSE;
    fileio_mode[0] = fileio_mode[1] = fileio_mode[2] = '\0';
    if (!(cstring = sky_string_cstring(mode))) {
invalid_mode:
        sky_error_raise_format(sky_ValueError, "invalid mode: %#@", mode);
    }
    for (c = cstring; *c; ++c) {
        if (memchr(cstring, *c, c - cstring)) {
            goto invalid_mode;
        }
        switch (*c) {
            case '+':
                fileio_mode[1] = *c;
                break;
            case 'a':
            case 'r':
            case 'w':
            case 'x':
                if (fileio_mode[0]) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "must have exactly one of create/read/write/append mode");
                }
                fileio_mode[0] = *c;
                break;
            case 'b':
                if (text) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "can't have text and binary mode at once");
                }
                if (!sky_object_isnull(encoding)) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "binary mode doesn't take an encoding argument");
                }
                if (!sky_object_isnull(errors)) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "binary mode doesn't take an errors argument");
                }
                if (!sky_object_isnull(newline)) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "binary mode doesn't take a newline argument");
                }
                binary = SKY_TRUE;
                break;
            case 't':
                if (binary) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "can't have text and binary mode at once");
                }
                text = SKY_TRUE;
                break;
            case 'U':
                universal = SKY_TRUE;
                break;
            default:
                goto invalid_mode;
        }
    }
    if (!fileio_mode[0]) {
        fileio_mode[0] = 'r';
    }
    if (universal && fileio_mode[0] != 'r') {
        sky_error_raise_string(
                sky_ValueError,
                "can't use U and writing mode at once");
    }
    if (!binary) {
        text = SKY_TRUE;
    }

    args = sky_object_build("(OsBO)", file, fileio_mode, closefd, opener);
    file = sky_object_call(sky__io_FileIO_type, args, NULL);

    /* Once the file is open, it must be closed if an error occurs; otherwise,
     * the descriptor will be left hanging around until the next garbage
     * collection cycle.
     */
    SKY_ERROR_TRY {
        value = sky_object_callmethod(file,
                                      SKY_STRING_LITERAL("isatty"),
                                      NULL,
                                      NULL);
        isatty = sky_object_bool(value);

        if (1 == buffering || (buffering < 0 && isatty)) {
            buffering = -1;
            line_buffering = SKY_TRUE;
        }
        if (buffering < 0) {
            buffering = SKY__IO_DEFAULT_BUFFER_SIZE;
            value = sky_object_callmethod(file,
                                          SKY_STRING_LITERAL("fileno"),
                                          NULL,
                                          NULL);
            fileno = sky_integer_value(value, INT_MIN, INT_MAX, NULL);
            if (fstat(fileno, &st) >= 0 && st.st_blksize > 1) {
                buffering = st.st_blksize;
            }
        }

        if (!buffering) {
            if (!binary) {
                sky_error_raise_string(sky_ValueError,
                                       "can't have unbuffered text I/O");
            }
        }
        else {
            args = sky_object_build("(Oi)", file, buffering);
            if ('+' == fileio_mode[1]) {
                file = sky_object_call(sky__io_BufferedRandom_type,
                                       args,
                                       NULL);
            }
            else if ('r' == fileio_mode[0]) {
                file = sky_object_call(sky__io_BufferedReader_type,
                                       args,
                                       NULL);
            }
            else {
                file = sky_object_call(sky__io_BufferedWriter_type,
                                       args,
                                       NULL);
            }

            if (!binary) {
                args = sky_object_build("(OOOOB)",
                                        file,
                                        encoding,
                                        errors,
                                        newline,
                                        line_buffering);
                file = sky_object_call(sky__io_TextIOWrapper_type,
                                       args,
                                       NULL);
                sky_object_setattr(file, SKY_STRING_LITERAL("mode"), mode);
            }
        }
    } SKY_ERROR_EXCEPT_ANY {
        sky_object_callmethod(file,
                              SKY_STRING_LITERAL("close"),
                              NULL,
                              NULL);
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    return file;
}


void
skython_module__io_initialize(sky_module_t module)
{
    static const char module_doc[] =
"The io module provides the Python interfaces to stream handling. The\n"
"builtin open function is defined in this module.\n"
"\n"
"At the top of the I/O hierarchy is the abstract base class IOBase. It\n"
"defines the basic interface to a stream. Note, however, that there is no\n"
"separation between reading and writing to streams; implementations are\n"
"allowed to throw an IOError if they do not support a given operation.\n"
"\n"
"Extending IOBase is RawIOBase which deals simply with the reading and\n"
"writing of raw bytes to a stream. FileIO subclasses RawIOBase to provide\n"
"an interface to OS files.\n"
"\n"
"BufferedIOBase deals with buffering on a raw byte stream (RawIOBase). Its\n"
"subclasses, BufferedWriter, BufferedReader, and BufferedRWPair buffer\n"
"streams that are readable, writable, and both respectively.\n"
"BufferedRandom provides a buffered interface to random access\n"
"streams. BytesIO is a simple stream of in-memory bytes.\n"
"\n"
"Another IOBase subclass, TextIOBase, deals with the encoding and decoding\n"
"of streams into text. TextIOWrapper, which extends it, is a buffered text\n"
"interface to a buffered raw stream (`BufferedIOBase`). Finally, StringIO\n"
"is a in-memory stream for text.\n"
"\n"
"Argument names are not part of the specification, and only the arguments\n"
"of open() are intended to be used as keyword arguments.\n"
"\n"
"data:\n"
"\n"
"DEFAULT_BUFFER_SIZE\n"
"\n"
"   An int containing the default buffer size used by the module's buffered\n"
"   I/O classes. open() uses the file's blksize (as obtained by os.stat) if\n"
"   possible.\n";

    sky_string_t    doc;
    sky_function_t  open;

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_cr),
            SKY_STRING_LITERAL("\r"),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_crlf),
            SKY_STRING_LITERAL("\r\n"),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_lf),
            SKY_STRING_LITERAL("\n"),
            SKY_TRUE);

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);

    sky_module_setattr(module,
                       "DEFAULT_BUFFER_SIZE",
                       sky_integer_create(SKY__IO_DEFAULT_BUFFER_SIZE));

    /* Create sky_UnsupportedOperation by calling type(), because io.py is
     * going to want to treat it as a user-defined type rather than a built-in
     * type (it'll set __module__ = 'io').
     */
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_UnsupportedOperation),
            sky_object_call(sky_type_type,
                            sky_object_build("(s(OO){})",
                                             "UnsupportedOperation",
                                             sky_ValueError, sky_OSError),
                            NULL),
            SKY_TRUE);
    sky_module_setattr(module,
                       "UnsupportedOperation",
                       sky_UnsupportedOperation);
    sky_module_setattr(module,
                       "BlockingIOError",
                       sky_BlockingIOError);

    sky_module_setattr(module,
                       "_IOBase",
                       sky__io__IOBase_initialize());
    sky_module_setattr(module,
                       "_RawIOBase",
                       sky__io__RawIOBase_initialize());
    sky_module_setattr(module,
                       "_BufferedIOBase",
                       sky__io__BufferedIOBase_initialize());
    sky_module_setattr(module,
                       "_BufferedIOMixin",
                       sky__io__BufferedIOMixin_initialize());
    sky_module_setattr(module,
                       "_TextIOBase",
                       sky__io__TextIOBase_initialize());
    sky_module_setattr(module,
                       "FileIO",
                       sky__io_FileIO_initialize());
    sky_module_setattr(module,
                       "BytesIO",
                       sky__io_BytesIO_initialize());
    sky_module_setattr(module,
                       "BufferedReader",
                       sky__io_BufferedReader_initialize());
    sky_module_setattr(module,
                       "BufferedWriter",
                       sky__io_BufferedWriter_initialize());
    sky_module_setattr(module,
                       "BufferedRWPair",
                       sky__io_BufferedRWPair_initialize());
    sky_module_setattr(module,
                       "BufferedRandom",
                       sky__io_BufferedRandom_initialize());
    sky_module_setattr(module,
                       "TextIOWrapper",
                       sky__io_TextIOWrapper_initialize());
    sky_module_setattr(module,
                       "IncrementalNewlineDecoder",
                       sky__io_IncrementalNewlineDecoder_initialize());
    sky_module_setattr(module,
                       "StringIO",
                       sky__io_StringIO_initialize());

    open = sky_function_createbuiltin(
            "open",
            sky__io_open_doc,
            (sky_native_code_function_t)sky__io_open,
            SKY_DATA_TYPE_OBJECT,
            "file", SKY_DATA_TYPE_OBJECT, NULL,
            "mode", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("rt"),
            "buffering", SKY_DATA_TYPE_INT, sky_integer_create(-1),
            "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "newline", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "closefd", SKY_DATA_TYPE_BOOL, sky_True,
            "opener", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);
    sky_module_setattr(sky_interpreter_module_builtins(), "open", open);
    sky_module_setattr(module, "open", open);
}
