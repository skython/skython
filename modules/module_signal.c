#include "../core/sky_private.h"

#include <math.h>
#include <sys/time.h>


#if defined(HAVE_SIGTIMEDWAIT) || defined(HAVE_SIGWAITINFO)
static sky_type_t   sky_signal_struct_siginfo;
#endif
static sky_object_t sky_signal_ItimerError;

static sky_object_t sky_signal_SIG_DFL;
static sky_object_t sky_signal_SIG_IGN;


static void
sky_signal_module_handler(int sig, sky_object_t object)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_object_call(object,
                    sky_object_build("(iO)", sig, tlsdata->current_frame),
                    NULL);
}


#if defined(HAVE_SIGTIMEDWAIT) || defined(HAVE_SIGWAITINFO)
static sky_object_t
sky_signal_siginfo(siginfo_t *si)
{
    sky_object_t    values[7];

    values[0] = sky_integer_create(si->si_signo);
    values[1] = sky_integer_create(si->si_code);
    values[2] = sky_integer_create(si->si_errno);
    values[3] = sky_integer_create(si->si_pid);
    values[4] = sky_integer_create(si->si_uid);
    values[5] = sky_integer_create(si->si_status);
    values[6] = sky_integer_create(si->si_band);

    return sky_struct_sequence_create(sky_signal_struct_siginfo,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}
#endif


static void
sky_signal_sigset(sigset_t *sigset, sky_object_t sequence)
{
    int             sig;
    sky_object_t    iter, object;

    sigemptyset(sigset);
    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, object) {
            sig = sky_integer_value(sky_number_integer(object),
                                    INT_MIN, INT_MAX,
                                    NULL);
            if (sig > 0 && sig < NSIG) {
                sigaddset(sigset, sig);
            }
            else {
                sky_error_raise_format(sky_ValueError,
                                       "signal number %d out of range",
                                       sig);
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(sequence);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            sig = sky_integer_value(sky_number_integer(object),
                                    INT_MIN, INT_MAX,
                                    NULL);
            if (sig > 0 && sig < NSIG) {
                sigaddset(sigset, sig);
            }
            else {
                sky_error_raise_format(sky_ValueError,
                                       "signal number %d out of range",
                                       sig);
            }
        }
    }
}


static sky_set_t
sky_signal_sigset_set(const sigset_t *sigset)
{
    int         i;
    sky_set_t   set;

    set = sky_set_create(NULL);
    for (i = 1; i < NSIG; ++i) {
        if (sigismember(sigset, i)) {
            sky_set_add(set, sky_integer_create(i));
        }
    }

    return set;
}


static inline sky_float_t
sky_signal_timeval(const struct timeval *value)
{
    double  d;
    
    d = value->tv_sec + (double)(value->tv_usec / 1000000.0);
    return sky_float_create(d);
}


static inline sky_tuple_t
sky_signal_itimer_value(const struct itimerval *value)
{
    return sky_tuple_pack(2, sky_signal_timeval(&(value->it_value)),
                             sky_signal_timeval(&(value->it_interval)));
}


static const char sky_signal_alarm_doc[] =
"alarm(seconds)\n\n"
"Arrange for SIGALRM to arrive after the given number of seconds.";


static void
sky_signal_default_int_handler(SKY_UNUSED int           sig,
                               SKY_UNUSED sky_frame_t   frame)
{
    sky_error_raise_object(sky_KeyboardInterrupt, sky_None);
}

static const char sky_signal_default_int_handler_doc[] =
"default_int_handler(...)\n\n"
"The default handler for SIGINT installed by Skython.\n"
"It raises KeyboardInterrupt.";


static sky_tuple_t
sky_signal_getitimer(int which)
{
    struct itimerval    value;

    if (getitimer(which, &value) == -1) {
        sky_error_raise_errno(sky_signal_ItimerError, errno);
    }
    return sky_signal_itimer_value(&value);
}

static const char sky_signal_getitimer_doc[] =
"getitimer(which)\n\n"
"Returns current value of given itimer.";


static sky_object_t
sky_signal_getsignal(int sig)
{
    sky_object_t            object;
    sky_signal_handler_t    handler;

    handler = sky_signal_handler(sig, &object);
    if ((sky_signal_handler_t)SIG_DFL == handler) {
        return sky_signal_SIG_DFL;
    }
    if ((sky_signal_handler_t)SIG_IGN == handler) {
        return sky_signal_SIG_IGN;
    }
    if (sky_signal_module_handler == handler) {
        return object;
    }
    return sky_None;
}

static const char sky_signal_getsignal_doc[] =
"getsignal(sig) -> action\n\n"
"Return the current action for the given signal.  The return value can be:\n"
"SIG_IGN -- if the signal is being ignored\n"
"SIG_DFL -- if the default action for the signal is in effect\n"
"None -- if an unknown handler is in effect\n"
"anything else -- the callable Python object used as a handler";


static void
sky_signal_pause(void)
{
    pause();
    sky_signal_check_pending();
}

static const char sky_signal_pause_doc[] =
"pause()\n\n"
"Wait until a signal arrives.";


static void
sky_signal_pthread_kill(intptr_t thread_id, int signum)
{
    int rc;

    if ((rc = pthread_kill((pthread_t)thread_id, signum)) != 0) {
        sky_error_raise_errno(sky_OSError, rc);
    }
    sky_signal_check_pending();
}

static const char sky_signal_pthread_kill_doc[] =
"pthread_kill(thread_id, signum)\n\n"
"Send a signal to a thread.";


static sky_set_t
sky_signal_pthread_sigmask(int how, sky_object_t mask)
{
    int         rc;
    sigset_t    oset, set;

    sky_signal_sigset(&set, mask);
    if ((rc = pthread_sigmask(how, &set, &oset)) != 0) {
        sky_error_raise_errno(sky_OSError, rc);
    }
    sky_signal_check_pending();

    return sky_signal_sigset_set(&oset);
}

static const char sky_signal_pthread_sigmask_doc[] =
"pthread_sigmask(how, mask) -> old mask\n\n"
"Fetch and/or change the signal mask of the calling thread.";


static sky_tuple_t
sky_signal_setitimer(int            which,
                     sky_object_t   seconds,
                     sky_object_t   interval)
{
    double              d;
    struct itimerval    ovalue, value;

    seconds = sky_number_float(seconds);
    d = sky_float_value(seconds);
    value.it_value.tv_sec = floor(d);
    value.it_value.tv_usec = fmod(d, 1.0) * 1000000.0;

    if (sky_object_isnull(interval)) {
        value.it_interval.tv_sec = 0;
        value.it_interval.tv_usec = 0;
    }
    else {
        interval = sky_number_float(interval);
        d = sky_float_value(interval);
        value.it_interval.tv_sec = floor(d);
        value.it_interval.tv_usec = fmod(d, 1.0) * 1000000.0;
    }

    if (setitimer(which, &value, &ovalue) == -1) {
        sky_error_raise_errno(sky_signal_ItimerError, errno);
    }
    return sky_signal_itimer_value(&ovalue);
}

static const char sky_signal_setitimer_doc[] =
"setitimer(which, seconds[, interval])\n\n"
"Sets given itimer (one of ITIMER_REAL, ITIMER_VIRTUAL\n"
"or ITIMER_PROF) to fire after value seconds and after\n"
"that every interval seconds.\n"
"The itimer can be cleared by setting seconds to zero.\n"
"\n"
"Returns old values as a tuple: (delay, interval).";


static int
sky_signal_set_wakeup_fd(int fd)
{
    int previous_fd;

    previous_fd = sky_signal_wakeupfd();
    sky_signal_setwakeupfd(fd);

    return previous_fd;
}

static const char sky_signal_set_wakeup_fd_doc[] =
"set_wakeup_fd(fd) -> fd\n\n"
"Sets the fd to be written to (with ('\\0') when a signal\n"
"comes in.  A library can use this to wakeup select or poll.\n"
"The previous fd is returned.\n";


static void
sky_signal_siginterrupt(int sig, int flag)
{
    if (sig < 1 || sig >= NSIG) {
        sky_error_raise_string(sky_ValueError, "signal number out of range");
    }
    if (siginterrupt(sig, flag) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_signal_siginterrupt_doc[] =
"siginterrupt(sig, flag) -> None\n\n"
"change system call restart behaviour: if flag is False, system calls\n"
"will be restarted when interrupted by signal sig, else system calls\n"
"will be interrupted.";


static sky_object_t
sky_signal_signal(int sig, sky_object_t action)
{
    sky_object_t            previous_action;
    sky_signal_handler_t    handler;

    if (sky_object_compare(action, sky_signal_SIG_DFL, SKY_COMPARE_OP_EQUAL)) {
        handler = (sky_signal_handler_t)SIG_DFL;
    }
    else if (sky_object_compare(action,
                                sky_signal_SIG_IGN,
                                SKY_COMPARE_OP_EQUAL))
    {
        handler = (sky_signal_handler_t)SIG_IGN;
    }
    else if (sky_object_callable(action)) {
        handler = sky_signal_module_handler;
    }
    else {
        sky_error_raise_string(
                sky_TypeError,
                "signal handler must be signal.SIG_IGN, signal.SIG_DFL, or "
                "a callable object");
    }

    /* These calls will validate sig and ensure that the main thread is the
     * caller.
     */
    previous_action = sky_signal_getsignal(sig);
    sky_signal_sethandler(sig, handler, action);

    return previous_action;
}

static const char sky_signal_signal_doc[] =
"signal(sig, action) -> action\n\n"
"Set the action for the given signal.  The action can be SIG_DFL,\n"
"SIG_IGN, or a callable Python object.  The previous action is\n"
"returned.  See getsignal() for possible return values.\n"
"\n"
"*** IMPORTANT NOTICE ***\n"
"A signal handler function is called with two arguments:\n"
"the first is the signal number, the second is the interrupted stack frame.";


static sky_set_t
sky_signal_sigpending(void)
{
    sigset_t    sigset;

    if (sigpending(&sigset) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return sky_signal_sigset_set(&sigset);
}

static const char sky_signal_sigpending_doc[] =
"sigpending() -> list\n\n"
"Examine pending signals.";


#if defined(HAVE_SIGTIMEDWAIT)
static sky_object_t
sky_signal_sigtimedwait(sky_object_t sigset, sky_object_t timeout)
{
    sigset_t        set;
    siginfo_t       si;
    struct timespec ts;

    ts = sky_time_totimespec(sky_time_fromobject(timeout));
    if (ts.tv_sec < 0 || ts.tv_nsec < 0) {
        sky_error_raise_string(sky_ValueError, "timeout must be non-negative");
    }

    sky_signal_sigset(&set, sigset);

    if (sigtimedwait(&set, &si, &ts) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_signal_siginfo(&si);
}

static const char sky_signal_sigtimedwait_doc[] =
"sigtimedwait(sigset, (timeout_sec, timeout_nsec)) -> struct_siginfo\n\n"
"Like sigwaitinfo(), but with a timeout specified as a tuple of (seconds,\n"
"nanoseconds).";
#endif


static int
sky_signal_sigwait(sky_object_t sigset)
{
    int         sig;
    sigset_t    set;

    sky_signal_sigset(&set, sigset);
    if (sigwait(&set, &sig) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sig;
}

static const char sky_signal_sigwait_doc[] =
"sigwait(sigset) -> signum\n\n"
"Wait a signal.";


#if defined(HAVE_SIGWAITINFO)
static sky_object_t
sky_signal_sigwaitinfo(sky_object_t sigset)
{
    sigset_t    set;
    siginfo_t   si;

    sky_signal_sigset(&set, sigset);
    if (sigwaitinfo(&set, &si) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_signal_siginfo(&si);
}

static const char sky_signal_sigwaitinfo_doc[] =
"sigwaitinfo(sigset) -> struct_siginfo\n\n"
"Wait synchronously for a signal until one of the signals in *sigset* is\n"
"delivered.\n"
"Returns a struct_siginfo containing information about the signal.";
#endif


void
skython_module_signal_initialize(sky_module_t module)
{
    static const char doc[] =
"This module provides mechanisms to use signal handlers in Python.\n"
"\n"
"Functions:\n"
"\n"
"alarm() -- cause SIGALRM after a specified time [Unix only]\n"
"setitimer() -- cause a signal (described below) after a specified\n"
"               float time and the timer may restart then [Unix only]\n"
"getitimer() -- get current value of timer [Unix only]\n"
"signal() -- set the action for a given signal\n"
"getsignal() -- get the signal action for a given signal\n"
"pause() -- wait until a signal arrives [Unix only]\n"
"default_int_handler() -- default SIGINT handler\n"
"\n"
"signal constants:\n"
"SIG_DFL -- used to refer to the system default handler\n"
"SIG_IGN -- used to ignore the signal\n"
"NSIG -- number of defined signals\n"
"SIGINT, SIGTERM, etc. -- signal numbers\n"
"\n"
"itimer constants:\n"
"ITIMER_REAL -- decrements in real time, and delivers SIGALRM upon\n"
"               expiration\n"
"ITIMER_VIRTUAL -- decrements only when the process is executing,\n"
"               and delivers SIGVTALRM upon expiration\n"
"ITIMER_PROF -- decrements both when the process is executing and\n"
"               when the system is executing on behalf of the process.\n"
"               Coupled with ITIMER_VIRTUAL, this timer is usually\n"
"               used to profile the time spent by the application\n"
"               in user and kernel space. SIGPROF is delivered upon\n"
"               expiration.\n"
"\n"
"\n"
"*** IMPORTANT NOTICE ***\n"
"A signal handler function is called with two arguments:\n"
"the first is the signal number, the second is the interrupted stack frame.\n";

#if defined(HAVE_SIGTIMEDWAIT) || defined(HAVE_SIGWAITINFO)
    static sky_struct_sequence_field_t siginfo_fields[] = {
        { "si_signo",   "signal number",                        0 },
        { "si_code",    "signal code",                          0 },
        { "si_errno",   "errno associated with this signal",    0 },
        { "si_pid",     "sending process ID",                   0 },
        { "si_uid",     "real user ID of sending process",      0 },
        { "si_status",  "exit value or signal",                 0 },
        { "si_band",    "band event for SIGPOLL",               0 },
    };

    static const char siginfo_doc[] =
"struct_siginfo: Result from sigwaitinfo or sigtimedwait.\n\n"
"This object may be accessed either as a tuple of\n"
"(si_signo, si_code, si_errno, si_pid, si_uid, si_status, si_band),\n"
"or via the attributes si_signo, si_code, and so on.";
#endif

    sky_object_t    default_int_handler;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

#if defined(HAVE_SIGTIMEDWAIT) || defined(HAVE_SIGWAITINFO)
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_signal_struct_siginfo),
            sky_struct_sequence_define(
                    "signal.struct_siginfo",
                    siginfo_doc,
                    sizeof(siginfo_fields) / sizeof(siginfo_fields[0]),
                    siginfo_fields),
            SKY_TRUE);
    sky_module_setattr(module,
                       "struct_siginfo",
                       sky_signal_struct_siginfo);
#endif

    default_int_handler =
            sky_function_createbuiltin(
                    "default_int_handler",
                    sky_signal_default_int_handler_doc,
                    (sky_native_code_function_t)sky_signal_default_int_handler,
                    SKY_DATA_TYPE_VOID,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    "frame", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL);
    sky_module_setattr(module, "default_int_handler", default_int_handler);

    sky_module_setattr(
            module,
            "alarm",
            sky_function_createbuiltin(
                    "alarm",
                    sky_signal_alarm_doc,
                    (sky_native_code_function_t)alarm,
                    SKY_DATA_TYPE_UINT,
                    "seconds", SKY_DATA_TYPE_UINT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getitimer",
            sky_function_createbuiltin(
                    "getitimer",
                    sky_signal_getitimer_doc,
                    (sky_native_code_function_t)sky_signal_getitimer,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "which", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getsignal",
            sky_function_createbuiltin(
                    "getsignal",
                    sky_signal_getsignal_doc,
                    (sky_native_code_function_t)sky_signal_getsignal,
                    SKY_DATA_TYPE_OBJECT,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "pause",
            sky_function_createbuiltin(
                    "pause",
                    sky_signal_pause_doc,
                    (sky_native_code_function_t)sky_signal_pause,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "pthread_kill",
            sky_function_createbuiltin(
                    "pthread_kill",
                    sky_signal_pthread_kill_doc,
                    (sky_native_code_function_t)sky_signal_pthread_kill,
                    SKY_DATA_TYPE_VOID,
                    "thread_id", SKY_DATA_TYPE_INTPTR_T, NULL,
                    "signum", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "pthread_sigmask",
            sky_function_createbuiltin(
                    "pthread_sigmask",
                    sky_signal_pthread_sigmask_doc,
                    (sky_native_code_function_t)sky_signal_pthread_sigmask,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "how", SKY_DATA_TYPE_INT, NULL,
                    "mask", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setitimer",
            sky_function_createbuiltin(
                    "setitimer",
                    sky_signal_setitimer_doc,
                    (sky_native_code_function_t)sky_signal_setitimer,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "which", SKY_DATA_TYPE_INT, NULL,
                    "seconds", SKY_DATA_TYPE_OBJECT, NULL,
                    "interval", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_module_setattr(
            module,
            "set_wakeup_fd",
            sky_function_createbuiltin(
                    "set_wakeup_fd",
                    sky_signal_set_wakeup_fd_doc,
                    (sky_native_code_function_t)sky_signal_set_wakeup_fd,
                    SKY_DATA_TYPE_INT,
                    "fd", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "siginterrupt",
            sky_function_createbuiltin(
                    "siginterrupt",
                    sky_signal_siginterrupt_doc,
                    (sky_native_code_function_t)sky_signal_siginterrupt,
                    SKY_DATA_TYPE_VOID,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    "flag", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "signal",
            sky_function_createbuiltin(
                    "signal",
                    sky_signal_signal_doc,
                    (sky_native_code_function_t)sky_signal_signal,
                    SKY_DATA_TYPE_OBJECT,
                    "sig", SKY_DATA_TYPE_INT, NULL,
                    "action", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "sigpending",
            sky_function_createbuiltin(
                    "sigpending",
                    sky_signal_sigpending_doc,
                    (sky_native_code_function_t)sky_signal_sigpending,
                    SKY_DATA_TYPE_OBJECT_SET,
                    NULL));
#if defined(HAVE_SIGTIMEDWAIT)
    sky_module_setattr(
            module,
            "sigtimedwait",
            sky_function_createbuiltin(
                    "sigtimedwait",
                    sky_signal_sigtimedwait_doc,
                    (sky_native_code_function_t)sky_signal_sigtimedwait,
                    SKY_DATA_TYPE_OBJECT,
                    "sigset", SKY_DATA_TYPE_OBJECT, NULL,
                    "timeout", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "sigwait",
            sky_function_createbuiltin(
                    "sigwait",
                    sky_signal_sigwait_doc,
                    (sky_native_code_function_t)sky_signal_sigwait,
                    SKY_DATA_TYPE_INT,
                    "sigset", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_SIGWAITINFO)
    sky_module_setattr(
            module,
            "sigwaitinfo",
            sky_function_createbuiltin(
                    "sigwaitinfo",
                    sky_signal_sigwaitinfo_doc,
                    (sky_native_code_function_t)sky_signal_sigwaitinfo,
                    SKY_DATA_TYPE_OBJECT,
                    "sigset", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_signal_ItimerError),
            sky_type_createbuiltin(
                    SKY_STRING_LITERAL("error"),
                    NULL,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky_OSError),
                    NULL),
            SKY_TRUE);
    sky_module_setattr(module, "ItimerError", sky_signal_ItimerError);

    sky_object_gc_setroot(&sky_signal_SIG_DFL,
                          sky_integer_create((intmax_t)SIG_DFL),
                          SKY_TRUE);
    sky_module_setattr(module, "SIG_DFL", sky_signal_SIG_DFL);

    sky_object_gc_setroot(&sky_signal_SIG_IGN,
                          sky_integer_create((intmax_t)SIG_IGN),
                          SKY_TRUE);
    sky_module_setattr(module, "SIG_IGN", sky_signal_SIG_IGN);

#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n));

    int_const(NSIG);

#ifdef SIG_BLOCK
    int_const(SIG_BLOCK);
#endif
#ifdef SIG_UNBLOCK
    int_const(SIG_UNBLOCK);
#endif
#ifdef SIG_SETMASK
    int_const(SIG_SETMASK);
#endif

#ifdef SIGHUP
    int_const(SIGHUP);
#endif
#ifdef SIGINT
    int_const(SIGINT);
#endif
#ifdef SIGBREAK
    int_const(SIGBREAK);
#endif
#ifdef SIGQUIT
    int_const(SIGQUIT);
#endif
#ifdef SIGILL
    int_const(SIGILL);
#endif
#ifdef SIGTRAP
    int_const(SIGTRAP);
#endif
#ifdef SIGIOT
    int_const(SIGIOT);
#endif
#ifdef SIGABRT
    int_const(SIGABRT);
#endif
#ifdef SIGEMT
    int_const(SIGEMT);
#endif
#ifdef SIGFPE
    int_const(SIGFPE);
#endif
#ifdef SIGKILL
    int_const(SIGKILL);
#endif
#ifdef SIGBUS
    int_const(SIGBUS);
#endif
#ifdef SIGSEGV
    int_const(SIGSEGV);
#endif
#ifdef SIGSYS
    int_const(SIGSYS);
#endif
#ifdef SIGPIPE
    int_const(SIGPIPE);
#endif
#ifdef SIGALRM
    int_const(SIGALRM);
#endif
#ifdef SIGTERM
    int_const(SIGTERM);
#endif
#ifdef SIGUSR1
    int_const(SIGUSR1);
#endif
#ifdef SIGUSR2
    int_const(SIGUSR2);
#endif
#ifdef SIGCLD
    int_const(SIGCLD);
#endif
#ifdef SIGCHLD
    int_const(SIGCHLD);
#endif
#ifdef SIGPWR
    int_const(SIGPWR);
#endif
#ifdef SIGIO
    int_const(SIGIO);
#endif
#ifdef SIGURG
    int_const(SIGURG);
#endif
#ifdef SIGWINCH
    int_const(SIGWINCH);
#endif
#ifdef SIGPOLL
    int_const(SIGPOLL);
#endif
#ifdef SIGSTOP
    int_const(SIGSTOP);
#endif
#ifdef SIGTSTP
    int_const(SIGTSTP);
#endif
#ifdef SIGCONT
    int_const(SIGCONT);
#endif
#ifdef SIGTTIN
    int_const(SIGTTIN);
#endif
#ifdef SIGTTOU
    int_const(SIGTTOU);
#endif
#ifdef SIGVTALRM
    int_const(SIGVTALRM);
#endif
#ifdef SIGPROF
    int_const(SIGPROF);
#endif
#ifdef SIGXCPU
    int_const(SIGXCPU);
#endif
#ifdef SIGXFSZ
    int_const(SIGXFSZ);
#endif
#ifdef SIGRTMIN
    int_const(SIGRTMIN);
#endif
#ifdef SIGRTMAX
    int_const(SIGRTMAX);
#endif
#ifdef SIGINFO
    int_const(SIGINFO);
#endif

#ifdef ITIMER_REAL
    int_const(ITIMER_REAL);
#endif
#ifdef ITIMER_VIRTUAL
    int_const(ITIMER_VIRTUAL);
#endif
#ifdef ITIMER_PROF
    int_const(ITIMER_PROF);
#endif

#ifdef CTRL_C_EVENT
    int_const(CTRL_C_EVENT);
#endif
#ifdef CTRL_BREAK_EVENT
    int_const(CTRL_BREAK_EVENT);
#endif

    /* WARNING: This won't work if the module is first loaded on a thread other
     * than the main thread. sky_signal_sethandler() will raise an exception.
     * However, this is what CPython does, so ... It's likely that startup code
     * loads the signal handler anyway, which solves the problem. It's still
     * worth noting, however.
     */
    if ((sky_signal_handler_t)SIG_DFL == sky_signal_handler(SIGINT, NULL)) {
        sky_signal_sethandler(SIGINT,
                              sky_signal_module_handler,
                              default_int_handler);
    }
}
