#include "../core/skython.h"

#include <pwd.h>


static sky_type_t sky_pwd_passwd_type = NULL;

static const char sky_pwd_passwd_doc[] =
"pwd.struct_passwd: Results from getpw*() routines.\n\n"
"This object may be accessed either as a tuple of\n"
"  (pw_name,pw_passwd,pw_uid,pw_gid,pw_gecos,pw_dir,pw_shell)\n"
"or via the object attributes as named in the above tuple.";

static sky_struct_sequence_field_t sky_pwd_passwd_fields[] = {
    { "pw_name",    "user name",        0, },
    { "pw_passwd",  "password",         0, },
    { "pw_uid",     "user id",          0, },
    { "pw_gid",     "group id",         0, },
    { "pw_gecos",   "real name",        0, },
    { "pw_dir",     "home directory",   0, },
    { "pw_shell",   "shell program",    0, },
};


static sky_tuple_t
sky_pwd_passwd(struct passwd *pw)
{
    sky_object_t    values[sizeof(sky_pwd_passwd_fields) /
                           sizeof(sky_pwd_passwd_fields[0])];

    values[0] = sky_string_createfromfilename(pw->pw_name,
                                              strlen(pw->pw_name));
    values[1] = sky_string_createfromfilename(pw->pw_passwd,
                                              strlen(pw->pw_passwd));
    values[2] = sky_integer_create(pw->pw_uid);
    values[3] = sky_integer_create(pw->pw_gid);
    values[4] = sky_string_createfromfilename(pw->pw_gecos,
                                              strlen(pw->pw_gecos));
    values[5] = sky_string_createfromfilename(pw->pw_dir,
                                              strlen(pw->pw_dir));
    values[6] = sky_string_createfromfilename(pw->pw_shell,
                                              strlen(pw->pw_shell));

    return sky_struct_sequence_create(sky_pwd_passwd_type,
                                      sizeof(values) / sizeof(values[0]),
                                      values);
}


sky_list_t
sky_pwd_getpwall(void)
{
    sky_list_t      list;
    struct passwd   *pw;

    list = sky_list_create(NULL);

    setpwent();
    while ((pw = getpwent()) != NULL) {
        sky_list_append(list, sky_pwd_passwd(pw));
    }
    endpwent();

    return list;
}

static const char sky_pwd_getpwall_doc[] =
"getpwall() -> list_of_entries\n"
"Return a list of all available password database entries, in arbitrary order.\n"
"See help(pwd) for more on password database entries.";


sky_tuple_t
sky_pwd_getpwnam(sky_string_t name)
{
    char            *nam;
    struct passwd   *pw;

    SKY_ASSET_BLOCK_BEGIN {
        nam = sky_codec_encodetocstring(name, 0);
        sky_asset_save(name, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        if (!(pw = getpwnam(nam))) {
            sky_error_raise_format(sky_KeyError,
                                   "getpwnam(): name not found: %@", name);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_pwd_passwd(pw);
}

static const char sky_pwd_getpwnam_doc[] =
"getpwnam(name) -> (pw_name,pw_passwd,pw_uid,\n"
"                  pw_gid,pw_gecos,pw_dir,pw_shell)\n"
"Return the password database entry for the given user name.\n"
"See help(pwd) for more on password database entries.";


sky_tuple_t
sky_pwd_getpwuid(int uid)
{
    struct passwd   *pw;

    if (!(pw = getpwuid(uid))) {
        sky_error_raise_format(sky_KeyError,
                               "getpwuid(): uid not found: %d\n", uid);
    }

    return sky_pwd_passwd(pw);
}

static const char sky_pwd_getpwuid_doc[] =
"getpwuid(uid) -> (pw_name,pw_passwd,pw_uid,\n"
"                 pw_gid,pw_gecos,pw_dir,pw_shell)\n"
"Return the password database entry for the given numeric user ID.\n"
"See help(pwd) for more on password database entries.";


void
skython_module_pwd_initialize(sky_module_t module)
{
    static const char module_doc[] =
"This module provides access to the Unix password database.\n"
"It is available on all Unix versions.\n"
"\n"
"Password database entries are reported as 7-tuples containing the following\n"
"items from the password database (see `<pwd.h>'), in order:\n"
"pw_name, pw_passwd, pw_uid, pw_gid, pw_gecos, pw_dir, pw_shell.\n"
"The uid and gid items are integers, all others are strings. An\n"
"exception is raised if the entry asked for cannot be found.";

    sky_string_t    doc;

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);

    sky_module_setattr(
            module,
            "getpwall",
            sky_function_createbuiltin(
                    "getpwall",
                    sky_pwd_getpwall_doc,
                    (sky_native_code_function_t)sky_pwd_getpwall,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    NULL));
    sky_module_setattr(
            module,
            "getpwnam",
            sky_function_createbuiltin(
                    "getpwnam",
                    sky_pwd_getpwnam_doc,
                    (sky_native_code_function_t)sky_pwd_getpwnam,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getpwuid",
            sky_function_createbuiltin(
                    "getpwuid",
                    sky_pwd_getpwuid_doc,
                    (sky_native_code_function_t)sky_pwd_getpwuid,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "uid", SKY_DATA_TYPE_INT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_pwd_passwd_type),
            sky_struct_sequence_define(
                    "struct_passwd",
                    sky_pwd_passwd_doc,
                    (sizeof(sky_pwd_passwd_fields) /
                     sizeof(sky_pwd_passwd_fields[0])),
                    sky_pwd_passwd_fields),
            SKY_TRUE);
}
