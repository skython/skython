#include "../core/skython.h"


/* Windows socket errors (WSA*)  */
#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
/* The following constants were added to errno.h in VS2010 but have
      preferred WSA equivalents. */
#undef EADDRINUSE
#undef EADDRNOTAVAIL
#undef EAFNOSUPPORT
#undef EALREADY
#undef ECONNABORTED
#undef ECONNREFUSED
#undef ECONNRESET
#undef EDESTADDRREQ
#undef EHOSTUNREACH
#undef EINPROGRESS
#undef EISCONN
#undef ELOOP
#undef EMSGSIZE
#undef ENETDOWN
#undef ENETRESET
#undef ENETUNREACH
#undef ENOBUFS
#undef ENOPROTOOPT
#undef ENOTCONN
#undef ENOTSOCK
#undef EOPNOTSUPP
#undef EPROTONOSUPPORT
#undef EPROTOTYPE
#undef ETIMEDOUT
#undef EWOULDBLOCK
#endif


static void
sky_errno_set_(
                sky_module_t    module,
                sky_dict_t      errorcode,
                const char *    name,
                int             code)
{
    sky_string_t    errno_name;
    sky_integer_t   errno_code;

    errno_code = sky_integer_create(code);
    errno_name = sky_string_createfrombytes(name, strlen(name), NULL, NULL);

    sky_dict_setitem(errorcode, errno_code, errno_name);
    sky_object_setattr(module, errno_name, errno_code);
}


void
skython_module_errno_initialize(sky_module_t module)
{
    static const char doc[] =
"This module makes available standard errno system symbols.\n"
"\n"
"The value of each symbol is the corresponding integer value,\n"
"e.g., on most systems, errno.ENOENT equals the integer 2.\n"
"\n"
"The dictionary errno.errorcode maps numeric codes to symbol names,\n"
"e.g., errno.errorcode[2] could be the string 'ENOENT'.\n"
"\n"
"Symbols that are not relevant to the underlying system are not defined.\n"
"\n"
"To map error codes to error messages, use the function os.strerror(),\n"
"e.g., os.strerror(2) could return 'No such file or directory'.";

    sky_dict_t  errorcode;

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

    errorcode = sky_dict_create();
    sky_module_setattr(module, "errorcode", errorcode);

#define sky_errno_set(_code, _comment)                      \
        sky_errno_set_(module, errorcode, #_code, _code)

    /* This list of errno codes is taken straight from CPython. */

    /*
     * The names and comments are borrowed from linux/include/errno.h,
     * which should be pretty all-inclusive.  However, the Solaris specific
     * names and comments are borrowed from sys/errno.h in Solaris.
     * MacOSX specific names and comments are borrowed from sys/errno.h in
     * MacOSX.
     */

#ifdef ENODEV
    sky_errno_set(ENODEV, "No such device");
#endif
#ifdef ENOCSI
    sky_errno_set(ENOCSI, "No CSI structure available");
#endif
#ifdef EHOSTUNREACH
    sky_errno_set(EHOSTUNREACH, "No route to host");
#else
#ifdef WSAEHOSTUNREACH
    sky_errno_set(WSAEHOSTUNREACH, "No route to host");
#endif
#endif
#ifdef ENOMSG
    sky_errno_set(ENOMSG, "No message of desired type");
#endif
#ifdef EUCLEAN
    sky_errno_set(EUCLEAN, "Structure needs cleaning");
#endif
#ifdef EL2NSYNC
    sky_errno_set(EL2NSYNC, "Level 2 not synchronized");
#endif
#ifdef EL2HLT
    sky_errno_set(EL2HLT, "Level 2 halted");
#endif
#ifdef ENODATA
    sky_errno_set(ENODATA, "No data available");
#endif
#ifdef ENOTBLK
    sky_errno_set(ENOTBLK, "Block device required");
#endif
#ifdef ENOSYS
    sky_errno_set(ENOSYS, "Function not implemented");
#endif
#ifdef EPIPE
    sky_errno_set(EPIPE, "Broken pipe");
#endif
#ifdef EINVAL
    sky_errno_set(EINVAL, "Invalid argument");
#else
#ifdef WSAEINVAL
    sky_errno_set(WSAEINVAL, "Invalid argument");
#endif
#endif
#ifdef EOVERFLOW
    sky_errno_set(EOVERFLOW, "Value too large for defined data type");
#endif
#ifdef EADV
    sky_errno_set(EADV, "Advertise error");
#endif
#ifdef EINTR
    sky_errno_set(EINTR, "Interrupted system call");
#else
#ifdef WSAEINTR
    sky_errno_set(WSAEINTR, "Interrupted system call");
#endif
#endif
#ifdef EUSERS
    sky_errno_set(EUSERS, "Too many users");
#else
#ifdef WSAEUSERS
    sky_errno_set(WSAEUSERS, "Too many users");
#endif
#endif
#ifdef ENOTEMPTY
    sky_errno_set(ENOTEMPTY, "Directory not empty");
#else
#ifdef WSAENOTEMPTY
    sky_errno_set(WSAENOTEMPTY, "Directory not empty");
#endif
#endif
#ifdef ENOBUFS
    sky_errno_set(ENOBUFS, "No buffer space available");
#else
#ifdef WSAENOBUFS
    sky_errno_set(WSAENOBUFS, "No buffer space available");
#endif
#endif
#ifdef EPROTO
    sky_errno_set(EPROTO, "Protocol error");
#endif
#ifdef EREMOTE
    sky_errno_set(EREMOTE, "Object is remote");
#else
#ifdef WSAEREMOTE
    sky_errno_set(WSAEREMOTE, "Object is remote");
#endif
#endif
#ifdef ENAVAIL
    sky_errno_set(ENAVAIL, "No XENIX semaphores available");
#endif
#ifdef ECHILD
    sky_errno_set(ECHILD, "No child processes");
#endif
#ifdef ELOOP
    sky_errno_set(ELOOP, "Too many symbolic links encountered");
#else
#ifdef WSAELOOP
    sky_errno_set(WSAELOOP, "Too many symbolic links encountered");
#endif
#endif
#ifdef EXDEV
    sky_errno_set(EXDEV, "Cross-device link");
#endif
#ifdef E2BIG
    sky_errno_set(E2BIG, "Arg list too long");
#endif
#ifdef ESRCH
    sky_errno_set(ESRCH, "No such process");
#endif
#ifdef EMSGSIZE
    sky_errno_set(EMSGSIZE, "Message too long");
#else
#ifdef WSAEMSGSIZE
    sky_errno_set(WSAEMSGSIZE, "Message too long");
#endif
#endif
#ifdef EAFNOSUPPORT
    sky_errno_set(EAFNOSUPPORT, "Address family not supported by protocol");
#else
#ifdef WSAEAFNOSUPPORT
    sky_errno_set(WSAEAFNOSUPPORT, "Address family not supported by protocol");
#endif
#endif
#ifdef EBADR
    sky_errno_set(EBADR, "Invalid request descriptor");
#endif
#ifdef EHOSTDOWN
    sky_errno_set(EHOSTDOWN, "Host is down");
#else
#ifdef WSAEHOSTDOWN
    sky_errno_set(WSAEHOSTDOWN, "Host is down");
#endif
#endif
#ifdef EPFNOSUPPORT
    sky_errno_set(EPFNOSUPPORT, "Protocol family not supported");
#else
#ifdef WSAEPFNOSUPPORT
    sky_errno_set(WSAEPFNOSUPPORT, "Protocol family not supported");
#endif
#endif
#ifdef ENOPROTOOPT
    sky_errno_set(ENOPROTOOPT, "Protocol not available");
#else
#ifdef WSAENOPROTOOPT
    sky_errno_set(WSAENOPROTOOPT, "Protocol not available");
#endif
#endif
#ifdef EBUSY
    sky_errno_set(EBUSY, "Device or resource busy");
#endif
#ifdef EWOULDBLOCK
    sky_errno_set(EWOULDBLOCK, "Operation would block");
#else
#ifdef WSAEWOULDBLOCK
    sky_errno_set(WSAEWOULDBLOCK, "Operation would block");
#endif
#endif
#ifdef EBADFD
    sky_errno_set(EBADFD, "File descriptor in bad state");
#endif
#ifdef EDOTDOT
    sky_errno_set(EDOTDOT, "RFS specific error");
#endif
#ifdef EISCONN
    sky_errno_set(EISCONN, "Transport endpoint is already connected");
#else
#ifdef WSAEISCONN
    sky_errno_set(WSAEISCONN, "Transport endpoint is already connected");
#endif
#endif
#ifdef ENOANO
    sky_errno_set(ENOANO, "No anode");
#endif
#ifdef ESHUTDOWN
    sky_errno_set(ESHUTDOWN, "Cannot send after transport endpoint shutdown");
#else
#ifdef WSAESHUTDOWN
    sky_errno_set(WSAESHUTDOWN, "Cannot send after transport endpoint shutdown");
#endif
#endif
#ifdef ECHRNG
    sky_errno_set(ECHRNG, "Channel number out of range");
#endif
#ifdef ELIBBAD
    sky_errno_set(ELIBBAD, "Accessing a corrupted shared library");
#endif
#ifdef ENONET
    sky_errno_set(ENONET, "Machine is not on the network");
#endif
#ifdef EBADE
    sky_errno_set(EBADE, "Invalid exchange");
#endif
#ifdef EBADF
    sky_errno_set(EBADF, "Bad file number");
#else
#ifdef WSAEBADF
    sky_errno_set(WSAEBADF, "Bad file number");
#endif
#endif
#ifdef EMULTIHOP
    sky_errno_set(EMULTIHOP, "Multihop attempted");
#endif
#ifdef EIO
    sky_errno_set(EIO, "I/O error");
#endif
#ifdef EUNATCH
    sky_errno_set(EUNATCH, "Protocol driver not attached");
#endif
#ifdef EPROTOTYPE
    sky_errno_set(EPROTOTYPE, "Protocol wrong type for socket");
#else
#ifdef WSAEPROTOTYPE
    sky_errno_set(WSAEPROTOTYPE, "Protocol wrong type for socket");
#endif
#endif
#ifdef ENOSPC
    sky_errno_set(ENOSPC, "No space left on device");
#endif
#ifdef ENOEXEC
    sky_errno_set(ENOEXEC, "Exec format error");
#endif
#ifdef EALREADY
    sky_errno_set(EALREADY, "Operation already in progress");
#else
#ifdef WSAEALREADY
    sky_errno_set(WSAEALREADY, "Operation already in progress");
#endif
#endif
#ifdef ENETDOWN
    sky_errno_set(ENETDOWN, "Network is down");
#else
#ifdef WSAENETDOWN
    sky_errno_set(WSAENETDOWN, "Network is down");
#endif
#endif
#ifdef ENOTNAM
    sky_errno_set(ENOTNAM, "Not a XENIX named type file");
#endif
#ifdef EACCES
    sky_errno_set(EACCES, "Permission denied");
#else
#ifdef WSAEACCES
    sky_errno_set(WSAEACCES, "Permission denied");
#endif
#endif
#ifdef ELNRNG
    sky_errno_set(ELNRNG, "Link number out of range");
#endif
#ifdef EILSEQ
    sky_errno_set(EILSEQ, "Illegal byte sequence");
#endif
#ifdef ENOTDIR
    sky_errno_set(ENOTDIR, "Not a directory");
#endif
#ifdef ENOTUNIQ
    sky_errno_set(ENOTUNIQ, "Name not unique on network");
#endif
#ifdef EPERM
    sky_errno_set(EPERM, "Operation not permitted");
#endif
#ifdef EDOM
    sky_errno_set(EDOM, "Math argument out of domain of func");
#endif
#ifdef EXFULL
    sky_errno_set(EXFULL, "Exchange full");
#endif
#ifdef ECONNREFUSED
    sky_errno_set(ECONNREFUSED, "Connection refused");
#else
#ifdef WSAECONNREFUSED
    sky_errno_set(WSAECONNREFUSED, "Connection refused");
#endif
#endif
#ifdef EISDIR
    sky_errno_set(EISDIR, "Is a directory");
#endif
#ifdef EPROTONOSUPPORT
    sky_errno_set(EPROTONOSUPPORT, "Protocol not supported");
#else
#ifdef WSAEPROTONOSUPPORT
    sky_errno_set(WSAEPROTONOSUPPORT, "Protocol not supported");
#endif
#endif
#ifdef EROFS
    sky_errno_set(EROFS, "Read-only file system");
#endif
#ifdef EADDRNOTAVAIL
    sky_errno_set(EADDRNOTAVAIL, "Cannot assign requested address");
#else
#ifdef WSAEADDRNOTAVAIL
    sky_errno_set(WSAEADDRNOTAVAIL, "Cannot assign requested address");
#endif
#endif
#ifdef EIDRM
    sky_errno_set(EIDRM, "Identifier removed");
#endif
#ifdef ECOMM
    sky_errno_set(ECOMM, "Communication error on send");
#endif
#ifdef ESRMNT
    sky_errno_set(ESRMNT, "Srmount error");
#endif
#ifdef EREMOTEIO
    sky_errno_set(EREMOTEIO, "Remote I/O error");
#endif
#ifdef EL3RST
    sky_errno_set(EL3RST, "Level 3 reset");
#endif
#ifdef EBADMSG
    sky_errno_set(EBADMSG, "Not a data message");
#endif
#ifdef ENFILE
    sky_errno_set(ENFILE, "File table overflow");
#endif
#ifdef ELIBMAX
    sky_errno_set(ELIBMAX, "Attempting to link in too many shared libraries");
#endif
#ifdef ESPIPE
    sky_errno_set(ESPIPE, "Illegal seek");
#endif
#ifdef ENOLINK
    sky_errno_set(ENOLINK, "Link has been severed");
#endif
#ifdef ENETRESET
    sky_errno_set(ENETRESET, "Network dropped connection because of reset");
#else
#ifdef WSAENETRESET
    sky_errno_set(WSAENETRESET, "Network dropped connection because of reset");
#endif
#endif
#ifdef ETIMEDOUT
    sky_errno_set(ETIMEDOUT, "Connection timed out");
#else
#ifdef WSAETIMEDOUT
    sky_errno_set(WSAETIMEDOUT, "Connection timed out");
#endif
#endif
#ifdef ENOENT
    sky_errno_set(ENOENT, "No such file or directory");
#endif
#ifdef EEXIST
    sky_errno_set(EEXIST, "File exists");
#endif
#ifdef EDQUOT
    sky_errno_set(EDQUOT, "Quota exceeded");
#else
#ifdef WSAEDQUOT
    sky_errno_set(WSAEDQUOT, "Quota exceeded");
#endif
#endif
#ifdef ENOSTR
    sky_errno_set(ENOSTR, "Device not a stream");
#endif
#ifdef EBADSLT
    sky_errno_set(EBADSLT, "Invalid slot");
#endif
#ifdef EBADRQC
    sky_errno_set(EBADRQC, "Invalid request code");
#endif
#ifdef ELIBACC
    sky_errno_set(ELIBACC, "Can not access a needed shared library");
#endif
#ifdef EFAULT
    sky_errno_set(EFAULT, "Bad address");
#else
#ifdef WSAEFAULT
    sky_errno_set(WSAEFAULT, "Bad address");
#endif
#endif
#ifdef EFBIG
    sky_errno_set(EFBIG, "File too large");
#endif
#ifdef EDEADLK
    sky_errno_set(EDEADLK, "Resource deadlock would occur");
#endif
#ifdef ENOTCONN
    sky_errno_set(ENOTCONN, "Transport endpoint is not connected");
#else
#ifdef WSAENOTCONN
    sky_errno_set(WSAENOTCONN, "Transport endpoint is not connected");
#endif
#endif
#ifdef EDESTADDRREQ
    sky_errno_set(EDESTADDRREQ, "Destination address required");
#else
#ifdef WSAEDESTADDRREQ
    sky_errno_set(WSAEDESTADDRREQ, "Destination address required");
#endif
#endif
#ifdef ELIBSCN
    sky_errno_set(ELIBSCN, ".lib section in a.out corrupted");
#endif
#ifdef ENOLCK
    sky_errno_set(ENOLCK, "No record locks available");
#endif
#ifdef EISNAM
    sky_errno_set(EISNAM, "Is a named type file");
#endif
#ifdef ECONNABORTED
    sky_errno_set(ECONNABORTED, "Software caused connection abort");
#else
#ifdef WSAECONNABORTED
    sky_errno_set(WSAECONNABORTED, "Software caused connection abort");
#endif
#endif
#ifdef ENETUNREACH
    sky_errno_set(ENETUNREACH, "Network is unreachable");
#else
#ifdef WSAENETUNREACH
    sky_errno_set(WSAENETUNREACH, "Network is unreachable");
#endif
#endif
#ifdef ESTALE
    sky_errno_set(ESTALE, "Stale NFS file handle");
#else
#ifdef WSAESTALE
    sky_errno_set(WSAESTALE, "Stale NFS file handle");
#endif
#endif
#ifdef ENOSR
    sky_errno_set(ENOSR, "Out of streams resources");
#endif
#ifdef ENOMEM
    sky_errno_set(ENOMEM, "Out of memory");
#endif
#ifdef ENOTSOCK
    sky_errno_set(ENOTSOCK, "Socket operation on non-socket");
#else
#ifdef WSAENOTSOCK
    sky_errno_set(WSAENOTSOCK, "Socket operation on non-socket");
#endif
#endif
#ifdef ESTRPIPE
    sky_errno_set(ESTRPIPE, "Streams pipe error");
#endif
#ifdef EMLINK
    sky_errno_set(EMLINK, "Too many links");
#endif
#ifdef ERANGE
    sky_errno_set(ERANGE, "Math result not representable");
#endif
#ifdef ELIBEXEC
    sky_errno_set(ELIBEXEC, "Cannot exec a shared library directly");
#endif
#ifdef EL3HLT
    sky_errno_set(EL3HLT, "Level 3 halted");
#endif
#ifdef ECONNRESET
    sky_errno_set(ECONNRESET, "Connection reset by peer");
#else
#ifdef WSAECONNRESET
    sky_errno_set(WSAECONNRESET, "Connection reset by peer");
#endif
#endif
#ifdef EADDRINUSE
    sky_errno_set(EADDRINUSE, "Address already in use");
#else
#ifdef WSAEADDRINUSE
    sky_errno_set(WSAEADDRINUSE, "Address already in use");
#endif
#endif
#ifdef EOPNOTSUPP
    sky_errno_set(EOPNOTSUPP, "Operation not supported on transport endpoint");
#else
#ifdef WSAEOPNOTSUPP
    sky_errno_set(WSAEOPNOTSUPP, "Operation not supported on transport endpoint");
#endif
#endif
#ifdef EREMCHG
    sky_errno_set(EREMCHG, "Remote address changed");
#endif
#ifdef EAGAIN
    sky_errno_set(EAGAIN, "Try again");
#endif
#ifdef ENAMETOOLONG
    sky_errno_set(ENAMETOOLONG, "File name too long");
#else
#ifdef WSAENAMETOOLONG
    sky_errno_set(WSAENAMETOOLONG, "File name too long");
#endif
#endif
#ifdef ENOTTY
    sky_errno_set(ENOTTY, "Not a typewriter");
#endif
#ifdef ERESTART
    sky_errno_set(ERESTART, "Interrupted system call should be restarted");
#endif
#ifdef ESOCKTNOSUPPORT
    sky_errno_set(ESOCKTNOSUPPORT, "Socket type not supported");
#else
#ifdef WSAESOCKTNOSUPPORT
    sky_errno_set(WSAESOCKTNOSUPPORT, "Socket type not supported");
#endif
#endif
#ifdef ETIME
    sky_errno_set(ETIME, "Timer expired");
#endif
#ifdef EBFONT
    sky_errno_set(EBFONT, "Bad font file format");
#endif
#ifdef EDEADLOCK
    sky_errno_set(EDEADLOCK, "Error EDEADLOCK");
#endif
#ifdef ETOOMANYREFS
    sky_errno_set(ETOOMANYREFS, "Too many references: cannot splice");
#else
#ifdef WSAETOOMANYREFS
    sky_errno_set(WSAETOOMANYREFS, "Too many references: cannot splice");
#endif
#endif
#ifdef EMFILE
    sky_errno_set(EMFILE, "Too many open files");
#else
#ifdef WSAEMFILE
    sky_errno_set(WSAEMFILE, "Too many open files");
#endif
#endif
#ifdef ETXTBSY
    sky_errno_set(ETXTBSY, "Text file busy");
#endif
#ifdef EINPROGRESS
    sky_errno_set(EINPROGRESS, "Operation now in progress");
#else
#ifdef WSAEINPROGRESS
    sky_errno_set(WSAEINPROGRESS, "Operation now in progress");
#endif
#endif
#ifdef ENXIO
    sky_errno_set(ENXIO, "No such device or address");
#endif
#ifdef ENOPKG
    sky_errno_set(ENOPKG, "Package not installed");
#endif
#ifdef WSASY
    sky_errno_set(WSASY, "Error WSASY");
#endif
#ifdef WSAEHOSTDOWN
    sky_errno_set(WSAEHOSTDOWN, "Host is down");
#endif
#ifdef WSAENETDOWN
    sky_errno_set(WSAENETDOWN, "Network is down");
#endif
#ifdef WSAENOTSOCK
    sky_errno_set(WSAENOTSOCK, "Socket operation on non-socket");
#endif
#ifdef WSAEHOSTUNREACH
    sky_errno_set(WSAEHOSTUNREACH, "No route to host");
#endif
#ifdef WSAELOOP
    sky_errno_set(WSAELOOP, "Too many symbolic links encountered");
#endif
#ifdef WSAEMFILE
    sky_errno_set(WSAEMFILE, "Too many open files");
#endif
#ifdef WSAESTALE
    sky_errno_set(WSAESTALE, "Stale NFS file handle");
#endif
#ifdef WSAVERNOTSUPPORTED
    sky_errno_set(WSAVERNOTSUPPORTED, "Error WSAVERNOTSUPPORTED");
#endif
#ifdef WSAENETUNREACH
    sky_errno_set(WSAENETUNREACH, "Network is unreachable");
#endif
#ifdef WSAEPROCLIM
    sky_errno_set(WSAEPROCLIM, "Error WSAEPROCLIM");
#endif
#ifdef WSAEFAULT
    sky_errno_set(WSAEFAULT, "Bad address");
#endif
#ifdef WSANOTINITIALISED
    sky_errno_set(WSANOTINITIALISED, "Error WSANOTINITIALISED");
#endif
#ifdef WSAEUSERS
    sky_errno_set(WSAEUSERS, "Too many users");
#endif
#ifdef WSAMAKEASYNCREPL
    sky_errno_set(WSAMAKEASYNCREPL, "Error WSAMAKEASYNCREPL");
#endif
#ifdef WSAENOPROTOOPT
    sky_errno_set(WSAENOPROTOOPT, "Protocol not available");
#endif
#ifdef WSAECONNABORTED
    sky_errno_set(WSAECONNABORTED, "Software caused connection abort");
#endif
#ifdef WSAENAMETOOLONG
    sky_errno_set(WSAENAMETOOLONG, "File name too long");
#endif
#ifdef WSAENOTEMPTY
    sky_errno_set(WSAENOTEMPTY, "Directory not empty");
#endif
#ifdef WSAESHUTDOWN
    sky_errno_set(WSAESHUTDOWN, "Cannot send after transport endpoint shutdown");
#endif
#ifdef WSAEAFNOSUPPORT
    sky_errno_set(WSAEAFNOSUPPORT, "Address family not supported by protocol");
#endif
#ifdef WSAETOOMANYREFS
    sky_errno_set(WSAETOOMANYREFS, "Too many references: cannot splice");
#endif
#ifdef WSAEACCES
    sky_errno_set(WSAEACCES, "Permission denied");
#endif
#ifdef WSATR
    sky_errno_set(WSATR, "Error WSATR");
#endif
#ifdef WSABASEERR
    sky_errno_set(WSABASEERR, "Error WSABASEERR");
#endif
#ifdef WSADESCRIPTIO
    sky_errno_set(WSADESCRIPTIO, "Error WSADESCRIPTIO");
#endif
#ifdef WSAEMSGSIZE
    sky_errno_set(WSAEMSGSIZE, "Message too long");
#endif
#ifdef WSAEBADF
    sky_errno_set(WSAEBADF, "Bad file number");
#endif
#ifdef WSAECONNRESET
    sky_errno_set(WSAECONNRESET, "Connection reset by peer");
#endif
#ifdef WSAGETSELECTERRO
    sky_errno_set(WSAGETSELECTERRO, "Error WSAGETSELECTERRO");
#endif
#ifdef WSAETIMEDOUT
    sky_errno_set(WSAETIMEDOUT, "Connection timed out");
#endif
#ifdef WSAENOBUFS
    sky_errno_set(WSAENOBUFS, "No buffer space available");
#endif
#ifdef WSAEDISCON
    sky_errno_set(WSAEDISCON, "Error WSAEDISCON");
#endif
#ifdef WSAEINTR
    sky_errno_set(WSAEINTR, "Interrupted system call");
#endif
#ifdef WSAEPROTOTYPE
    sky_errno_set(WSAEPROTOTYPE, "Protocol wrong type for socket");
#endif
#ifdef WSAHOS
    sky_errno_set(WSAHOS, "Error WSAHOS");
#endif
#ifdef WSAEADDRINUSE
    sky_errno_set(WSAEADDRINUSE, "Address already in use");
#endif
#ifdef WSAEADDRNOTAVAIL
    sky_errno_set(WSAEADDRNOTAVAIL, "Cannot assign requested address");
#endif
#ifdef WSAEALREADY
    sky_errno_set(WSAEALREADY, "Operation already in progress");
#endif
#ifdef WSAEPROTONOSUPPORT
    sky_errno_set(WSAEPROTONOSUPPORT, "Protocol not supported");
#endif
#ifdef WSASYSNOTREADY
    sky_errno_set(WSASYSNOTREADY, "Error WSASYSNOTREADY");
#endif
#ifdef WSAEWOULDBLOCK
    sky_errno_set(WSAEWOULDBLOCK, "Operation would block");
#endif
#ifdef WSAEPFNOSUPPORT
    sky_errno_set(WSAEPFNOSUPPORT, "Protocol family not supported");
#endif
#ifdef WSAEOPNOTSUPP
    sky_errno_set(WSAEOPNOTSUPP, "Operation not supported on transport endpoint");
#endif
#ifdef WSAEISCONN
    sky_errno_set(WSAEISCONN, "Transport endpoint is already connected");
#endif
#ifdef WSAEDQUOT
    sky_errno_set(WSAEDQUOT, "Quota exceeded");
#endif
#ifdef WSAENOTCONN
    sky_errno_set(WSAENOTCONN, "Transport endpoint is not connected");
#endif
#ifdef WSAEREMOTE
    sky_errno_set(WSAEREMOTE, "Object is remote");
#endif
#ifdef WSAEINVAL
    sky_errno_set(WSAEINVAL, "Invalid argument");
#endif
#ifdef WSAEINPROGRESS
    sky_errno_set(WSAEINPROGRESS, "Operation now in progress");
#endif
#ifdef WSAGETSELECTEVEN
    sky_errno_set(WSAGETSELECTEVEN, "Error WSAGETSELECTEVEN");
#endif
#ifdef WSAESOCKTNOSUPPORT
    sky_errno_set(WSAESOCKTNOSUPPORT, "Socket type not supported");
#endif
#ifdef WSAGETASYNCERRO
    sky_errno_set(WSAGETASYNCERRO, "Error WSAGETASYNCERRO");
#endif
#ifdef WSAMAKESELECTREPL
    sky_errno_set(WSAMAKESELECTREPL, "Error WSAMAKESELECTREPL");
#endif
#ifdef WSAGETASYNCBUFLE
    sky_errno_set(WSAGETASYNCBUFLE, "Error WSAGETASYNCBUFLE");
#endif
#ifdef WSAEDESTADDRREQ
    sky_errno_set(WSAEDESTADDRREQ, "Destination address required");
#endif
#ifdef WSAECONNREFUSED
    sky_errno_set(WSAECONNREFUSED, "Connection refused");
#endif
#ifdef WSAENETRESET
    sky_errno_set(WSAENETRESET, "Network dropped connection because of reset");
#endif
#ifdef WSAN
    sky_errno_set(WSAN, "Error WSAN");
#endif
#ifdef ENOMEDIUM
    sky_errno_set(ENOMEDIUM, "No medium found");
#endif
#ifdef EMEDIUMTYPE
    sky_errno_set(EMEDIUMTYPE, "Wrong medium type");
#endif
#ifdef ECANCELED
    sky_errno_set(ECANCELED, "Operation Canceled");
#endif
#ifdef ENOKEY
    sky_errno_set(ENOKEY, "Required key not available");
#endif
#ifdef EKEYEXPIRED
    sky_errno_set(EKEYEXPIRED, "Key has expired");
#endif
#ifdef EKEYREVOKED
    sky_errno_set(EKEYREVOKED, "Key has been revoked");
#endif
#ifdef EKEYREJECTED
    sky_errno_set(EKEYREJECTED, "Key was rejected by service");
#endif
#ifdef EOWNERDEAD
    sky_errno_set(EOWNERDEAD, "Owner died");
#endif
#ifdef ENOTRECOVERABLE
    sky_errno_set(ENOTRECOVERABLE, "State not recoverable");
#endif
#ifdef ERFKILL
    sky_errno_set(ERFKILL, "Operation not possible due to RF-kill");
#endif

    /* Solaris-specific errnos */
#ifdef ECANCELED
    sky_errno_set(ECANCELED, "Operation canceled");
#endif
#ifdef ENOTSUP
    sky_errno_set(ENOTSUP, "Operation not supported");
#endif
#ifdef EOWNERDEAD
    sky_errno_set(EOWNERDEAD, "Process died with the lock");
#endif
#ifdef ENOTRECOVERABLE
    sky_errno_set(ENOTRECOVERABLE, "Lock is not recoverable");
#endif
#ifdef ELOCKUNMAPPED
    sky_errno_set(ELOCKUNMAPPED, "Locked lock was unmapped");
#endif
#ifdef ENOTACTIVE
    sky_errno_set(ENOTACTIVE, "Facility is not active");
#endif

    /* MacOSX specific errnos */
#ifdef EAUTH
    sky_errno_set(EAUTH, "Authentication error");
#endif
#ifdef EBADARCH
    sky_errno_set(EBADARCH, "Bad CPU type in executable");
#endif
#ifdef EBADEXEC
    sky_errno_set(EBADEXEC, "Bad executable (or shared library)");
#endif
#ifdef EBADMACHO
    sky_errno_set(EBADMACHO, "Malformed Mach-o file");
#endif
#ifdef EBADRPC
    sky_errno_set(EBADRPC, "RPC struct is bad");
#endif
#ifdef EDEVERR
    sky_errno_set(EDEVERR, "Device error");
#endif
#ifdef EFTYPE
    sky_errno_set(EFTYPE, "Inappropriate file type or format");
#endif
#ifdef ENEEDAUTH
    sky_errno_set(ENEEDAUTH, "Need authenticator");
#endif
#ifdef ENOATTR
    sky_errno_set(ENOATTR, "Attribute not found");
#endif
#ifdef ENOPOLICY
    sky_errno_set(ENOPOLICY, "Policy not found");
#endif
#ifdef EPROCLIM
    sky_errno_set(EPROCLIM, "Too many processes");
#endif
#ifdef EPROCUNAVAIL
    sky_errno_set(EPROCUNAVAIL, "Bad procedure for program");
#endif
#ifdef EPROGMISMATCH
    sky_errno_set(EPROGMISMATCH, "Program version wrong");
#endif
#ifdef EPROGUNAVAIL
    sky_errno_set(EPROGUNAVAIL, "RPC prog. not avail");
#endif
#ifdef EPWROFF
    sky_errno_set(EPWROFF, "Device power is off");
#endif
#ifdef ERPCMISMATCH
    sky_errno_set(ERPCMISMATCH, "RPC version wrong");
#endif
#ifdef ESHLIBVERS
    sky_errno_set(ESHLIBVERS, "Shared library version mismatch");
#endif
}
