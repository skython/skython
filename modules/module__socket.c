#include "../core/sky_private.h"
#include "../core/sky_socket_private.h"

#include <sys/types.h>
#include <sys/socket.h>

#if defined(HAVE_NETDB_H)
#   include <netdb.h>
#endif
#if defined(HAVE_ARPA_INET_H)
#   include <arpa/inet.h>
#endif
#if defined(HAVE_LINUX_NETLINK_H)
#   include <linux/netlink.h>
#endif
#if defined(HAVE_NET_IF_H)
#   include <net/if.h>
#endif
#if defined(HAVE_NETINET_TCP_H)
#   include <netinet/tcp.h>
#endif


SKY_MODULE_EXTERN const char *skython_module__socket_doc;

SKY_MODULE_EXTERN void
skython_module__socket_initialize(sky_module_t module);


static sky_type_t sky__socket_herror = NULL;

static SKY_NORETURN void 
sky__socket_raise_herror(int error)
{
    sky_object_t    value;

    value = sky_object_build("(is)", error, hstrerror(error));
    sky_error_raise_object(sky__socket_herror, value);
}


static int
sky__socket_dup(int integer)
{
    if ((integer = dup(integer)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return integer;
}

static const char sky__socket_dup_doc[] =
"dup(integer) -> integer\n\n"
"Duplicate an integer socket file descriptor.  This is like os.dup(), but for\n"
"sockets; on some platforms os.dup() won't work for socket file descriptors.";


static sky_list_t
sky__socket_getaddrinfo(
                sky_object_t    host,
                sky_object_t    port,
                int             family,
                int             type,
                int             proto,
                int             flags)
{
    sky_list_t  list;

    SKY_ASSET_BLOCK_BEGIN {
        int             error;
        char            *hostname, *servname;
        sky_buffer_t    buffer;
        struct addrinfo hints, *res;

        if (sky_object_isnull(host)) {
            hostname = NULL;
        }
        else {
            if (sky_object_isa(host, sky_string_type)) {
                host = sky_codec_encode(NULL,
                                        host,
                                        SKY_STRING_LITERAL("idna"),
                                        NULL,
                                        0);
            }
            if (!sky_buffer_check(host)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "getaddrinfo() argument 1 must be string or None");
            }
            sky_buffer_acquire(&buffer, host, SKY_BUFFER_FLAG_SIMPLE);
            hostname = sky_asset_strndup(buffer.buf,
                                         buffer.len,
                                         SKY_ASSET_CLEANUP_ALWAYS);
            sky_buffer_release(&buffer);
        }

        if (sky_object_isnull(port)) {
            servname = NULL;
        }
        else if (sky_object_isa(port, sky_integer_type)) {
            sky_format_asprintf(&servname, "%jd",
                                sky_integer_value(port,
                                                  INTMAX_MIN, INTMAX_MAX,
                                                  NULL));
            sky_asset_save(servname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }
        else if (sky_object_isa(port, sky_string_type)) {
            servname = sky_codec_encodetocstring(port, 0);
        }
        else if (!sky_buffer_check(port)) {
            sky_error_raise_string(sky_OSError,
                                   "Int or String expected");
        }
        else {
            sky_buffer_acquire(&buffer, port, SKY_BUFFER_FLAG_SIMPLE);
            servname = sky_asset_strndup(buffer.buf,
                                         buffer.len,
                                         SKY_ASSET_CLEANUP_ALWAYS);
            sky_buffer_release(&buffer);
        }

#if defined(__APPLE__) && defined(AI_NUMERICSERV)
        /* On OS X up to at least 10.8 getaddrinfo() crashes if AI_NUMERICSERV
         * is set and servname is NULL or "0". This workaround avoids a
         * segfault in libSystem.
         */
        if ((flags & AI_NUMERICSERV) &&
            (!servname || ('0' == servname[0] && '\0' == servname[1])))
        {
            servname = "00";
        }
#endif

        memset(&hints, 0, sizeof(hints));
        hints.ai_family = family;
        hints.ai_socktype = type;
        hints.ai_protocol = proto;
        hints.ai_flags = flags;
        if ((error = getaddrinfo(hostname, servname, &hints, &res))) {
            sky_socket_raise_gaierror(error);
        }
        sky_asset_save(res, (sky_free_t)freeaddrinfo, SKY_ASSET_CLEANUP_ALWAYS);

        list = sky_list_create(NULL);
        while (res) {
            sky_socket_address_t    address;

            sky_error_validate_debug(res->ai_addrlen <= sizeof(address));
            memcpy(&address, res->ai_addr, res->ai_addrlen);
            sky_list_append(
                    list,
                    sky_object_build(
                            "(iiisO)",
                            res->ai_family,
                            res->ai_socktype,
                            res->ai_protocol,
                            (res->ai_canonname ? res->ai_canonname : ""),
                            sky_socket_address_object(&address)));

            res = res->ai_next;
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky__socket_getaddrinfo_doc[] =
"getaddrinfo(host, port [, family, socktype, proto, flags])\n"
"    -> list of (family, socktype, proto, canonname, sockaddr)\n\n"
"Resolve host and port into addrinfo struct.";


static sky_object_t
sky__socket_getdefaulttimeout(void)
{
    double  timeout;

    timeout = sky_socket_getdefaulttimeout();
    if (timeout < 0.0) {
        return sky_None;
    }
    return sky_float_create(timeout);
}

static const char sky__socket_getdefaulttimeout_doc[] =
"getdefaulttimeout() -> timeout\n\n"
"Returns the default timeout in seconds (float) for new socket objects.\n"
"A value of None indicates that new socket objects have no timeout.\n"
"When the socket module is first imported, the default is None.";


/* Convert a struct hostent into a tuple. */
static sky_tuple_t
sky__socket_hostent(const struct hostent *he)
{
    int             error;
    char            host[NI_MAXHOST], **p;
    sky_list_t      addresses, aliases;
    sky_string_t    name;

    if (!he->h_name) {
        name = sky_string_empty;
    }
    else {
        name = sky_string_createfrombytes(he->h_name,
                                          strlen(he->h_name),
                                          NULL,
                                          NULL);
    }

    aliases = sky_list_create(NULL);
    if (he->h_aliases) {
        for (p = he->h_aliases; *p; ++p) {
            sky_list_append(
                    aliases,
                    sky_string_createfrombytes(*p, strlen(*p), NULL, NULL));
        }
    }

    addresses = sky_list_create(NULL);
    if (he->h_addr_list) {
        socklen_t           addrlen;
        struct sockaddr     *addr;
        struct sockaddr_in  sin;
        struct sockaddr_in6 sin6;

        switch (he->h_addrtype) {
            case AF_INET:
                addr = (struct sockaddr *)&sin;
                addrlen = sizeof(struct sockaddr_in);
                break;
            case AF_INET6:
                addr = (struct sockaddr *)&sin6;
                addrlen = sizeof(struct sockaddr_in6);
                break;
            default:
                sky_error_raise_string(sky_OSError,
                                       "unsupported address family");
        }
        memset(addr, 0, addrlen);
        addr->sa_family = he->h_addrtype;

        for (p = he->h_addr_list; *p; ++p) {
            switch (he->h_addrtype) {
                case AF_INET:
                    memcpy(&(sin.sin_addr), *p, he->h_length);
                    break;
                case AF_INET6:
                    memcpy(&(sin6.sin6_addr), *p, he->h_length);
                    break;
            }

            if ((error = getnameinfo(addr,
                                     he->h_length,
                                     host,
                                     sizeof(host),
                                     NULL,
                                     0,
                                     NI_NUMERICHOST)) != 0)
            {
                sky_socket_raise_gaierror(error);
            }

            sky_list_append(addresses,
                            sky_string_createfrombytes(host,
                                                       strlen(host),
                                                       NULL,
                                                       NULL));
        }
    }

    return sky_tuple_pack(3, name, aliases, addresses);
}


static sky_string_t
sky__socket_gethostbyaddr(sky_string_t host)
{
    socklen_t               addrlen;
    const void              *addr;
    sky_object_t            result;
    struct hostent          *he;
    sky_socket_address_t    sockaddr;

    sky_socket_address_setipaddr(AF_UNSPEC, host, &sockaddr);
    switch (sockaddr.sockaddr.sa_family) {
        case AF_INET:
            addr = &(sockaddr.sockaddr_in.sin_addr);
            addrlen = sizeof(sockaddr.sockaddr_in.sin_addr);
            break;
        case AF_INET6:
            addr = &(sockaddr.sockaddr_in6.sin6_addr);
            addrlen = sizeof(sockaddr.sockaddr_in6.sin6_addr);
            break;
        default:
            sky_error_raise_string(sky_OSError, "unsupported address family");
    }

    SKY_ASSET_BLOCK_BEGIN {
#if defined(HAVE_GETHOSTBYADDR_R)
        char            *buf, *newbuf;
        socklen_t       buflen;
        struct hostent  ret;

        buflen = 16384;
        buf = sky_asset_malloc(buflen, SKY_ASSET_CLEANUP_ALWAYS);
        for (;;) {
            int error, h_error;

            if (!(error = gethostbyaddr_r(addr,
                                          addrlen,
                                          sockaddr.sockaddr.sa_family,
                                          &ret,
                                          buf,
                                          buflen,
                                          &he,
                                          &h_error)))
            {
                break;
            }
            if (ERANGE != error) {
                sky__socket_raise_herror(h_error);
            }
            buflen *= 2;
            newbuf = sky_malloc(buflen);
            sky_asset_update(buf,
                             newbuf,
                             SKY_ASSET_UPDATE_FIRST_CURRENT);
            sky_free(buf);
            buf = newbuf;
        }
#else
        if (!(he = gethostbyaddr(addr, addrlen, sockaddr.sockaddr.sa_family))) {
            sky__socket_raise_herror(h_errno);
        }
#endif

        result = sky__socket_hostent(he);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__socket_gethostbyaddr_doc[] =
"gethostbyaddr(host) -> (name, aliaslist, addresslist)\n\n"
"Return the true host name, a list of aliases, and a list of IP address,\n"
"for a host.  The host argument is a string giving a hsot name or IP number.";


static sky_string_t
sky__socket_gethostbyname(sky_string_t name)
{
    int                     error;
    char                    host[NI_MAXHOST];
    socklen_t               addrlen;
    sky_socket_address_t    addr;

    sky_socket_address_sockaddr(AF_INET,
                                sky_tuple_pack(2, name, sky_integer_zero),
                                &addr,
                                &addrlen);

    if ((error = getnameinfo(&(addr.sockaddr),
                             addrlen,
                             host,
                             sizeof(host),
                             NULL,
                             0,
                             NI_NUMERICHOST)) != 0)
    {
        sky_socket_raise_gaierror(error);
    }

    return sky_string_createfrombytes(host, strlen(host), NULL, NULL);
}

static const char sky__socket_gethostbyname_doc[] =
"gethostbyname(host) -> address\n\n"
"Return the IP address (a string of the form '255.255.255.255') for a host.";


static sky_string_t
sky__socket_gethostbyname_ex(sky_string_t host)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
#if defined(HAVE_GETHOSTBYNAME_R)
        char            *buf, *newbuf;
        socklen_t       buflen;
        struct hostent  ret;
#endif

        char            *hostname;
        ssize_t         nbytes;
        struct hostent  *he;

        hostname = (char *)sky_codec_encodetocbytes(NULL,
                                                    host,
                                                    SKY_STRING_LITERAL("idna"),
                                                    NULL,
                                                    0,
                                                    &nbytes);
        hostname = sky_realloc(hostname, nbytes + 1);
        hostname[nbytes] = '\0';
        sky_asset_save(hostname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

#if defined(HAVE_GETHOSTBYNAME_R)
        buflen = 16384;
        buf = sky_asset_malloc(buflen, SKY_ASSET_CLEANUP_ALWAYS);
        for (;;) {
            int error, h_error;

            if (!(error = gethostbyname_r(hostname,
                                          &ret,
                                          buf,
                                          buflen,
                                          &he,
                                          &h_error)))
            {
                break;
            }
            if (ERANGE != error) {
                sky__socket_raise_herror(h_error);
            }
            buflen *= 2;
            newbuf = sky_malloc(buflen);
            sky_asset_update(buf,
                             newbuf,
                             SKY_ASSET_UPDATE_FIRST_CURRENT);
            sky_free(buf);
            buf = newbuf;
        }
#else
        if (!(he = gethostbyname(hostname))) {
            sky__socket_raise_herror(h_errno);
        }
#endif

        result = sky__socket_hostent(he);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__socket_gethostbyname_ex_doc[] =
"gethostbyname_ex(host) -> (name, aliaslist, addresslist)\n\n"
"Return the true host name, a list of aliases, and a list of IP addresses,\n"
"for a host.  The host argument is a string giving a host name or IP number.";


static sky_string_t
sky__socket_gethostname(void)
{
    char            *buffer, static_buffer[256];
    sky_string_t    result;

    if (!gethostname(static_buffer, sizeof(static_buffer))) {
        return sky_string_createfrombytes(static_buffer,
                                          strlen(static_buffer),
                                          NULL,
                                          NULL);
    }

    if (errno != ENAMETOOLONG) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(sizeof(static_buffer) * 2,
                                  SKY_ASSET_CLEANUP_ALWAYS);
        while (gethostname(buffer, sky_memsize(buffer))) {
            if (errno != ENAMETOOLONG) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            buffer = sky_asset_realloc(buffer,
                                       sky_memsize(buffer) * 2,
                                       SKY_ASSET_CLEANUP_ALWAYS,
                                       SKY_ASSET_UPDATE_FIRST_CURRENT);
        }

        result = sky_string_createfrombytes(buffer, strlen(buffer), NULL, NULL);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky__socket_gethostname_doc[] =
"gethostname() -> string\n\n"
"Return the current host name.";


static sky_parameter_list_t sky__socket_getnameinfo_sockaddr_parameters;

static sky_tuple_t
sky__socket_getnameinfo(sky_tuple_t sockaddr, int flags)
{
    int             port;
    void            **values;
    sky_tuple_t     tuple;
    sky_object_t    host;
    unsigned int    flowinfo, scope_id;

    values = sky_parameter_list_apply(
                    sky__socket_getnameinfo_sockaddr_parameters,
                    sockaddr, NULL,
                    SKY_STRING_LITERAL("sockaddr"));
    host = *(sky_string_t *)values[0];
    port = *(int *)values[1];
    flowinfo = *(unsigned int *)values[2];
    scope_id = *(unsigned int *)values[3];
    sky_free(values);

    if (flowinfo > 0xfffff) {
        sky_error_raise_string(sky_OverflowError,
                               "getsockaddrarg: flowinfo must be 0-1048575.");
    }

    SKY_ASSET_BLOCK_BEGIN {
        int                 error;
        char                host_buffer[NI_MAXHOST], serv_buffer[NI_MAXSERV];
        char                *hostname, *servname;
        sky_buffer_t        buffer;
        struct addrinfo     hints, *res;
        struct sockaddr_in6 *sin6;

        host = sky_codec_encode(NULL,
                                host,
                                SKY_STRING_LITERAL("idna"),
                                NULL,
                                0);
        sky_buffer_acquire(&buffer, host, SKY_BUFFER_FLAG_SIMPLE);
        hostname = sky_asset_strndup(buffer.buf,
                                     buffer.len,
                                     SKY_ASSET_CLEANUP_ALWAYS);
        sky_buffer_release(&buffer);

        sky_format_asprintf(&servname, "%d", port);
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = 0;
        hints.ai_flags = AI_NUMERICHOST;
        if ((error = getaddrinfo(hostname, servname, &hints, &res)) != 0) {
            sky_socket_raise_gaierror(error);
        }
        sky_asset_save(res, (sky_free_t)freeaddrinfo, SKY_ASSET_CLEANUP_ALWAYS);

        if (res->ai_next) {
            sky_error_raise_string(sky_OSError,
                                   "sockaddr resolved to multiple addresses");
        }

        switch (res->ai_family) {
            case AF_INET:
                if (sky_tuple_len(sockaddr) != 2) {
                    sky_error_raise_string(sky_OSError,
                                           "IPv4 sockaddr must be 2 tuple");
                }
                break;
            case AF_INET6:
                sin6 = (struct sockaddr_in6 *)res->ai_addr;
                sin6->sin6_flowinfo = htonl(flowinfo);
                sin6->sin6_scope_id = scope_id;
                break;
        }

        if ((error = getnameinfo(res->ai_addr,
                                 res->ai_addrlen,
                                 host_buffer,
                                 sizeof(host_buffer),
                                 serv_buffer,
                                 sizeof(serv_buffer),
                                 flags)) != 0)
        {
            sky_socket_raise_gaierror(error);
        }

        tuple = sky_object_build("(ss)", host_buffer, serv_buffer);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static const char sky__socket_getnameinfo_doc[] =
"getnameinfo(sockaddr, flags) --> (host, port)\n\n"
"Get host and port for a sockaddr.";


static long
sky__socket_getprotobyname(sky_string_t name)
{
    struct protoent *pe;

    SKY_ASSET_BLOCK_BEGIN {
        char    *cname;

        cname = sky_codec_encodetocstring(name, 0);
        sky_asset_save(cname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (!(pe = getprotobyname(cname))) {
            sky_error_raise_string(sky_OSError, "protocol not found");
        }
    } SKY_ASSET_BLOCK_END;

    return pe->p_proto;
}

static const char sky__socket_getprotobyname_doc[] =
"getprotobyname(name) -> integer\n\n"
"Return the protocol number for the named protocol.  (Rarely used.)";


static long
sky__socket_getservbyname(sky_string_t servicename, sky_string_t protocolname)
{
    struct servent  *se;

    SKY_ASSET_BLOCK_BEGIN {
        char    *name, *proto = NULL;

        name = sky_codec_encodetocstring(servicename, 0);
        sky_asset_save(name, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (protocolname) {
            proto = sky_codec_encodetocstring(protocolname, 0);
            sky_asset_save(proto, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        if (!(se = getservbyname(name, proto))) {
            sky_error_raise_string(sky_OSError, "service/proto not found");
        }
    } SKY_ASSET_BLOCK_END;

    return (long)ntohs(se->s_port);
}

static const char sky__socket_getservbyname_doc[] =
"getservbyname(servicename[, protocolname]) -> integer\n\n"
"Return a port number from a service name and protocol name.\n"
"The optional protocol name, if given, should be 'tcp' or 'udp',\n"
"otherwise any protocol will match.";


static sky_string_t
sky__socket_getservbyport(int port, sky_string_t protocolname)
{
    struct servent  *se;

    SKY_ASSET_BLOCK_BEGIN {
        char    *proto = NULL;

        if (protocolname) {
            proto = sky_codec_encodetocstring(protocolname, 0);
            sky_asset_save(proto, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (port < 0 || port > 0xffff) {
            sky_error_raise_string(sky_OverflowError,
                                   "getservbyport: port must be 0-65535.");
        }

        if (!(se = getservbyport(htons(port), proto))) {
            sky_error_raise_string(sky_OSError, "port/proto not found");
        }
    } SKY_ASSET_BLOCK_END;

    return sky_string_createfrombytes(se->s_name,
                                      strlen(se->s_name),
                                      NULL,
                                      NULL);
}

static const char sky__socket_getservbyport_doc[] =
"getservbyport(port[, protocolname]) -> string\n\n"
"Return the service name from a port number and protocol name.\n"
"The optional protocol name, if given, should be 'tcp' or 'udp',\n"
"otherwise any protocol will match.";


/* This wrapper is needed because htonl() may be a preprocessor macro.
 * The types used are to match CPython's idiosyncratic behavior.
 */
static unsigned long
sky__socket_htonl(unsigned long integer)
{
#if ULONG_MAX > UINT32_MAX
    if (integer > UINT32_MAX) {
        sky_error_raise_string(sky_OverflowError,
                               "int larger than 32 bits");
    }
#endif
    return htonl(integer);
}

static const char sky__socket_htonl_doc[] =
"htonl(integer) -> integer\n\n"
"Convert a 32-bit integer from host to network byte order.";


/* This wrapper is needed because htons() may be a preprocessor macro.
 * The types used are to match CPython's idiosyncratic behavior.
 */
static unsigned int
sky__socket_htons(int integer)
{
    if (integer < 0) {
        sky_error_raise_string(
                sky_OverflowError,
                "can't convert negative number to unsigned long");
    }
    return (unsigned int)htons((unsigned short)integer);
}

static const char sky__socket_htons_doc[] =
"htons(integer) -> integer\n\n"
"Convert a 16-bit integer from host to network byte order.";


static sky_string_t
sky__socket_if_indextoname(unsigned long if_index)
{
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        char    *name;

        name = sky_asset_malloc(IF_NAMESIZE + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        if (!if_indextoname(if_index, name)) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        bytes = sky_bytes_createwithbytes(name, strlen(name), sky_free);
    } SKY_ASSET_BLOCK_END;

    return sky_codec_decodefilename(bytes);
}

static const char sky__socket_if_indextoname_doc[] =
"if_indextoname(if_index)\n\n"
"Returns the interface name corresponding to the interface index if_index.";


static sky_list_t
sky__socket_if_nameindex(void)
{
    sky_list_t  list;

    SKY_ASSET_BLOCK_BEGIN {
        size_t              i;
        sky_string_t        if_name;
        sky_integer_t       if_index;
        struct if_nameindex *index;

        if (!(index = if_nameindex())) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        list = sky_list_create(NULL);
        for (i = 0;
             i <= SSIZE_MAX && index[i].if_index && index[i].if_name;
             ++i)
        {
            if_index = sky_integer_createfromunsigned(index[i].if_index);
            if_name = sky_string_createfromfilename(index[i].if_name,
                                                    strlen(index[i].if_name));
            sky_list_append(list, sky_tuple_pack(2, if_index, if_name));
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char sky__socket_if_nameindex_doc[] =
"if_nameindex()\n\n"
"Returns a list of network interface information (index, name) tuples.";


static unsigned long
sky__socket_if_nametoindex(sky_object_t if_name)
{
    unsigned long   if_index;

    if (!sky_buffer_check(if_name)) {
        if_name = sky_codec_encodefilename(if_name);
    }

    SKY_ASSET_BLOCK_BEGIN {
        char            *ifname;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, if_name, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        /* Make sure that the buffer bytes are NUL terminated. */
        ifname = sky_asset_strndup(buffer.buf,
                                   buffer.len,
                                   SKY_ASSET_CLEANUP_ALWAYS);
        if (!(if_index = if_nametoindex(ifname))) {
            sky_error_raise_string(sky_OSError,
                                   "no interface with this name");
        }
    } SKY_ASSET_BLOCK_END;

    return if_index;
}

static const char sky__socket_if_nametoindex_doc[] =
"if_nametoindex(if_name)\n\n"
"Returns the interface index corresponding to the interface name if_name.";


static sky_bytes_t
sky__socket_inet_aton(sky_string_t string)
{
    const char      *cstring;
    struct in_addr  addr;

    if (!(cstring = sky_string_cstring(string)) ||
        !inet_aton(cstring, &addr))
    {
        sky_error_raise_string(sky_OSError,
                               "illegal IP address string passed to inet_aton");
    }

    return sky_bytes_createfrombytes(&addr, sizeof(addr));
}

static const char sky__socket_inet_aton_doc[] =
"inet_aton(string) -> bytes giving packed 32-bit IP representation\n\n"
"Convert an IP address in string format (123.45.67.89) to the 32-bit packed\n"
"binary format used in low-level network functions.";


static sky_string_t
sky__socket_inet_ntoa(sky_object_t packed_ip)
{
    sky_string_t    string;

    SKY_ASSET_BLOCK_BEGIN {
        char            *address;
#if defined(HAVE_INET_NTOA_R)
        char            address_buffer[16];
#endif
        sky_buffer_t    buffer;
        struct in_addr  addr;

        sky_buffer_acquire(&buffer, packed_ip, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (buffer.len != sizeof(addr)) {
            sky_error_raise_string(sky_OSError,
                                   "packed IP wrong length for inet_ntoa");
        }
        memcpy(&addr, buffer.buf, sizeof(addr));

#if defined(HAVE_INET_NTOA_R)
        address = inet_ntoa_r(addr, buffer, sizeof(buffer));
#else
        address = inet_ntoa(addr);
#endif

        string = sky_string_createfrombytes(address,
                                            strlen(address),
                                            NULL,
                                            NULL);
    } SKY_ASSET_BLOCK_END;

    return string;
}

static const char sky__socket_inet_ntoa_doc[] =
"inet_ntoa(packed_ip) -> ip_address_string\n\n"
"Convert an IP address from 32-bit packed binary format to string format";


#if defined(HAVE_INET_NTOP)
static sky_string_t
sky__socket_inet_ntop(int af, sky_object_t packed_ip)
{
    sky_string_t    string;

    SKY_ASSET_BLOCK_BEGIN {
        char            dst[SKY_MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) + 1];
        ssize_t         expected_len;
        const char      *result;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, packed_ip, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        switch (af) {
            case AF_INET:
                expected_len = sizeof(struct in_addr);
                break;
            case AF_INET6:
                expected_len = sizeof(struct in6_addr);
                break;
            default:
                sky_error_raise_format(sky_ValueError,
                                       "unknown address family %d", af);
        }
        if (buffer.len != expected_len) {
            sky_error_raise_string(
                    sky_ValueError,
                    "invalid length of packed IP address string");
        }

        if (!(result = inet_ntop(af, buffer.buf, dst, sizeof(dst)))) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        string = sky_string_createfrombytes(result, strlen(result), NULL, NULL);
    } SKY_ASSET_BLOCK_END;

    return string;
}

static const char sky__socket_inet_ntop_doc[] =
"inet_ntop(af, packed_ip) -> string formatted IP address\n\n"
"Convert a packed IP address of the given family to string format.";
#endif


#if defined(HAVE_INET_PTON)
static sky_bytes_t
sky__socket_inet_pton(int af, sky_string_t ip)
{
    sky_bytes_t bytes;

    if (af != AF_INET && af != AF_INET6) {
        sky_error_raise_string(sky_OSError, "unknown address family");
    }

    SKY_ASSET_BLOCK_BEGIN {
        int     rc;
        char    dst[SKY_MAX(sizeof(struct in_addr), sizeof(struct in6_addr))];
        char    *src;

        src = sky_codec_encodetocstring(ip, 0);
        sky_asset_save(src, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if ((rc = inet_pton(af, src, dst)) <= 0) {
            if (!rc) {
                sky_error_raise_string(
                        sky_OSError,
                        "illegal IP address string passed to inet_pton");
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        switch (af) {
            case AF_INET:
                bytes = sky_bytes_createfrombytes(dst, sizeof(struct in_addr));
                break;
            case AF_INET6:
                bytes = sky_bytes_createfrombytes(dst, sizeof(struct in6_addr));
                break;
            default:
                sky_error_fatal("internal error");
        }
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char sky__socket_inet_pton_doc[] =
"inet_pton(af, ip) -> packed IP address string\n\n"
"Convert an IP address from string format to a packed string suitable\n"
"for use with low-level network functions.";
#endif


/* This wrapper is needed because ntohl() may be a preprocessor macro.
 * The types used are to match CPython's idiosyncratic behavior.
 */
static unsigned long
sky__socket_ntohl(unsigned long integer)
{
#if ULONG_MAX > UINT32_MAX
    if (integer > UINT32_MAX) {
        sky_error_raise_string(sky_OverflowError,
                               "int larger than 32 bits");
    }
#endif
    return ntohl(integer);
}

static const char sky__socket_ntohl_doc[] =
"ntohl(integer) -> integer\n\n"
"Convert a 32-bit integer from network to host byte order.";


/* This wrapper is needed because ntohs() may be a preprocessor macro.
 * The types used are to match CPython's idiosyncratic behavior.
 */
static unsigned int
sky__socket_ntohs(int integer)
{
    if (integer < 0) {
        sky_error_raise_string(
                sky_OverflowError,
                "can't convert negative number to unsigned long");
    }
    return (unsigned int)ntohs((unsigned short)integer);
}

static const char sky__socket_ntohs_doc[] =
"ntohs(integer) -> integer\n\n"
"Convert a 16-bit integer from network to host byte order.";


static void
sky__socket_setdefaulttimeout(sky_object_t timeout)
{
    double  value;

    if (sky_object_isnull(timeout)) {
        value = -1.0;
    }
    else if (!SKY_OBJECT_METHOD_SLOT(timeout, FLOAT)) {
        sky_error_raise_string(sky_TypeError, "a float is required");
    }
    else {
        value = sky_float_value(sky_number_float(timeout));
        if (value < 0.0) {
            sky_error_raise_string(sky_ValueError,
                                   "Timeout value out of range");
        }
    }
    sky_socket_setdefaulttimeout(value);
}

static const char sky__socket_setdefaulttimeout_doc[] =
"setdefaulttimeout(timeout)\n\n"
"Set the default timeout in seconds (float) for new socket objects.\n"
"A value of None indicates that new socket objects have no timeout.\n"
"When the socket module is first imported, the default is None.";


static int
sky__socket_sethostname(sky_object_t name)
{
    int             result;
    sky_buffer_t    buffer;

    if (!sky_buffer_check(name)) {
        name = sky_codec_encodefilename(name);
    }

    sky_buffer_acquire(&buffer, name, SKY_BUFFER_FLAG_SIMPLE);
    result = sethostname(buffer.buf, buffer.len);
    sky_buffer_release(&buffer);

    return result;
}

static const char sky__socket_sethostname_doc[] =
"sethostname(name)\n\n"
"Sets the hostname to name.";


static void
sky__socket_socketpair_cleanup(void *arg)
{
    int *socket_vector = arg;

    if (socket_vector[0] != -1) {
        close(socket_vector[0]);
        socket_vector[0] = -1;
    }
    if (socket_vector[1] != -1) {
        close(socket_vector[1]);
        socket_vector[1] = -1;
    }
}

static sky_tuple_t
sky__socket_socketpair(int family, int type, int proto)
{
    sky_tuple_t tuple;

    SKY_ASSET_BLOCK_BEGIN {
        int             socket_vector[2];
        sky_socket_t    sockets[2];

        if (socketpair(family, type, proto, socket_vector) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_asset_save(socket_vector,
                       sky__socket_socketpair_cleanup,
                       SKY_ASSET_CLEANUP_ON_ERROR);

        sockets[0] = sky_socket_createwithdescriptor(socket_vector[0],
                                                     family,
                                                     type,
                                                     proto);
        socket_vector[0] = -1;

        sockets[1] = sky_socket_createwithdescriptor(socket_vector[1],
                                                     family,
                                                     type,
                                                     proto);
        socket_vector[1] = -1;

        tuple = sky_tuple_pack(2, sockets[0], sockets[1]);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}

static const char sky__socket_socketpair_doc[] =
"socketpair([family[, type[, proto]]]) -> (socket object, socket object)\n\n"
"Create a pair of socket objects from the sockets returned by the platform\n"
"socketpair function.\n"
"The arguments are the same as for socket() except he default family is\n"
"AF_UNIX if defined on the platform; otherwise, the default is AF_INET.";


static const char sky__socket_CMSG_LEN_doc[] =
"CMSG_LEN(length) -> control message length\n\n"
"Return the total length, without trailing padding, of an ancillary\n"
"data item with associated data of the given length.  This value can\n"
"often be used as the buffer size for recvmsg() to receive a single\n"
"item of ancillary data, but RFC 3542 requires portable applications to\n"
"use CMSG_SPACE() and thus incklude space for padding, even when the\n"
"item will be the last in the buffer.  Raises OverflowError if length\n"
"is outside the permissible range of values.";


static const char sky__socket_CMSG_SPACE_doc[] =
"CMSG_SPACE(length) -> buffer size\n\n"
"Return the buffer size needed for recvmsg() to receive an ancillary\n"
"data item with associated data of the given length, along with any\n"
"trailing padding.  The buffer space needed to receive multiple items\n"
"is the sum of the CMSG_SPACE() values for their associated data\n"
"lengths.  Raises OverflowError if length is outside the permissible\n"
"range of values.";


const char *skython_module__socket_doc =
"Implementation module for socket operations.\n"
"\n"
"See the socket module for documentation.";

void
skython_module__socket_initialize(sky_module_t module)
{
    sky_parameter_list_t    parameters;

    parameters = sky_parameter_list_create();
    sky_parameter_list_add(parameters,
                           SKY_STRING_LITERAL("host"),
                           SKY_DATA_TYPE_OBJECT_STRING,
                           NULL);
    sky_parameter_list_add(parameters,
                           SKY_STRING_LITERAL("port"),
                           SKY_DATA_TYPE_INT,
                           NULL);
    sky_parameter_list_add(parameters,
                           SKY_STRING_LITERAL("flowinfo"),
                           SKY_DATA_TYPE_UINT,
                           sky_integer_zero);
    sky_parameter_list_add(parameters,
                           SKY_STRING_LITERAL("scope_id"),
                           SKY_DATA_TYPE_UINT,
                           sky_integer_zero);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__socket_getnameinfo_sockaddr_parameters),
            parameters,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "CMSG_LEN",
            sky_function_createbuiltin(
                    "CMSG_LEN",
                    sky__socket_CMSG_LEN_doc,
                    (sky_native_code_function_t)sky_socket_CMSG_LEN,
                    SKY_DATA_TYPE_SIZE_T,
                    "length", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "CMSG_SPACE",
            sky_function_createbuiltin(
                    "CMSG_SPACE",
                    sky__socket_CMSG_SPACE_doc,
                    (sky_native_code_function_t)sky_socket_CMSG_SPACE,
                    SKY_DATA_TYPE_SIZE_T,
                    "length", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "dup",
            sky_function_createbuiltin(
                    "dup",
                    sky__socket_dup_doc,
                    (sky_native_code_function_t)sky__socket_dup,
                    SKY_DATA_TYPE_INT,
                    "integer", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getaddrinfo",
            sky_function_createbuiltin(
                    "getaddrinfo",
                    sky__socket_getaddrinfo_doc,
                    (sky_native_code_function_t)sky__socket_getaddrinfo,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "host", SKY_DATA_TYPE_OBJECT, NULL,
                    "port", SKY_DATA_TYPE_OBJECT, NULL,
                    "family", SKY_DATA_TYPE_INT, sky_integer_create(AF_UNSPEC),
                    "type", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "proto", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "getdefaulttimeout",
            sky_function_createbuiltin(
                    "getdefaulttimeout",
                    sky__socket_getdefaulttimeout_doc,
                    (sky_native_code_function_t)sky__socket_getdefaulttimeout,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "gethostbyaddr",
            sky_function_createbuiltin(
                    "gethostbyaddr",
                    sky__socket_gethostbyaddr_doc,
                    (sky_native_code_function_t)sky__socket_gethostbyaddr,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "host", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "gethostbyname",
            sky_function_createbuiltin(
                    "gethostbyname",
                    sky__socket_gethostbyname_doc,
                    (sky_native_code_function_t)sky__socket_gethostbyname,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "host", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "gethostbyname_ex",
            sky_function_createbuiltin(
                    "gethostbyname_ex",
                    sky__socket_gethostbyname_ex_doc,
                    (sky_native_code_function_t)sky__socket_gethostbyname_ex,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "host", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "gethostname",
            sky_function_createbuiltin(
                    "gethostname",
                    sky__socket_gethostname_doc,
                    (sky_native_code_function_t)sky__socket_gethostname,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL));
    sky_module_setattr(
            module,
            "getnameinfo",
            sky_function_createbuiltin(
                    "getnameinfo",
                    sky__socket_getnameinfo_doc,
                    (sky_native_code_function_t)sky__socket_getnameinfo,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "sockaddr", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "flags", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getprotobyname",
            sky_function_createbuiltin(
                    "getprotobyname",
                    sky__socket_getprotobyname_doc,
                    (sky_native_code_function_t)sky__socket_getprotobyname,
                    SKY_DATA_TYPE_LONG,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "getservbyname",
            sky_function_createbuiltin(
                    "getservbyname",
                    sky__socket_getservbyname_doc,
                    (sky_native_code_function_t)sky__socket_getservbyname,
                    SKY_DATA_TYPE_LONG,
                    "servicename", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "protocolname", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "getservbyport",
            sky_function_createbuiltin(
                    "getservbyport",
                    sky__socket_getservbyport_doc,
                    (sky_native_code_function_t)sky__socket_getservbyport,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "port", SKY_DATA_TYPE_INT, NULL,
                    "protocolname", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "htonl",
            sky_function_createbuiltin(
                    "htonl",
                    sky__socket_htonl_doc,
                    (sky_native_code_function_t)sky__socket_htonl,
                    SKY_DATA_TYPE_ULONG,
                    "integer", SKY_DATA_TYPE_ULONG, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "htons",
            sky_function_createbuiltin(
                    "htons",
                    sky__socket_htons_doc,
                    (sky_native_code_function_t)sky__socket_htons,
                    SKY_DATA_TYPE_INT,
                    "integer", SKY_DATA_TYPE_UINT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "if_indextoname",
            sky_function_createbuiltin(
                    "if_indextoname",
                    sky__socket_if_indextoname_doc,
                    (sky_native_code_function_t)sky__socket_if_indextoname,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "if_index", SKY_DATA_TYPE_ULONG, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "if_nameindex",
            sky_function_createbuiltin(
                    "if_nameindex",
                    sky__socket_if_nameindex_doc,
                    (sky_native_code_function_t)sky__socket_if_nameindex,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    NULL));
    sky_module_setattr(
            module,
            "if_nametoindex",
            sky_function_createbuiltin(
                    "if_nametoindex",
                    sky__socket_if_nametoindex_doc,
                    (sky_native_code_function_t)sky__socket_if_nametoindex,
                    SKY_DATA_TYPE_ULONG,
                    "if_name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "inet_aton",
            sky_function_createbuiltin(
                    "inet_aton",
                    sky__socket_inet_aton_doc,
                    (sky_native_code_function_t)sky__socket_inet_aton,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "inet_ntoa",
            sky_function_createbuiltin(
                    "inet_ntoa",
                    sky__socket_inet_ntoa_doc,
                    (sky_native_code_function_t)sky__socket_inet_ntoa,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "packed_ip", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#if defined(HAVE_INET_NTOP)
    sky_module_setattr(
            module,
            "inet_ntop",
            sky_function_createbuiltin(
                    "inet_ntop",
                    sky__socket_inet_ntop_doc,
                    (sky_native_code_function_t)sky__socket_inet_ntop,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "af", SKY_DATA_TYPE_INT, NULL,
                    "packed_ip", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
#endif
#if defined(HAVE_INET_PTON)
    sky_module_setattr(
            module,
            "inet_pton",
            sky_function_createbuiltin(
                    "inet_pton",
                    sky__socket_inet_pton_doc,
                    (sky_native_code_function_t)sky__socket_inet_pton,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "af", SKY_DATA_TYPE_INT, NULL,
                    "ip", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
#endif
    sky_module_setattr(
            module,
            "ntohl",
            sky_function_createbuiltin(
                    "ntohl",
                    sky__socket_ntohl_doc,
                    (sky_native_code_function_t)sky__socket_ntohl,
                    SKY_DATA_TYPE_ULONG,
                    "integer", SKY_DATA_TYPE_ULONG, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "ntohs",
            sky_function_createbuiltin(
                    "ntohs",
                    sky__socket_ntohs_doc,
                    (sky_native_code_function_t)sky__socket_ntohs,
                    SKY_DATA_TYPE_INT,
                    "integer", SKY_DATA_TYPE_UINT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "setdefaulttimeout",
            sky_function_createbuiltin(
                    "setdefaulttimeout",
                    sky__socket_setdefaulttimeout_doc,
                    (sky_native_code_function_t)sky__socket_setdefaulttimeout,
                    SKY_DATA_TYPE_VOID,
                    "timeout", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "sethostname",
            sky_function_createbuiltin(
                    "sethostname",
                    sky__socket_sethostname_doc,
                    (sky_native_code_function_t)sky__socket_sethostname,
                    SKY_DATA_TYPE_SYSCALL_VOID,
                    "name", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "socketpair",
            sky_function_createbuiltin(
                    "socketpair",
                    sky__socket_socketpair_doc,
                    (sky_native_code_function_t)sky__socket_socketpair,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
#if defined(AF_UNIX)
                    "family", SKY_DATA_TYPE_INT, sky_integer_create(AF_UNIX),
#else
                    "family", SKY_DATA_TYPE_INT, sky_integer_create(AF_INET),
#endif
                    "type", SKY_DATA_TYPE_INT, sky_integer_create(SOCK_STREAM),
                    "proto", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));

    sky_module_setattr(module, "error", sky_OSError);
    sky_module_setattr(module, "gaierror", sky_socket_gaierror);
    sky_module_setattr(module, "timeout", sky_socket_timeout);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__socket_herror),
            sky_object_call(sky_type_type,
                            sky_object_build("(s(O){})",
                                             "herror",
                                             sky_OSError),
                            NULL),
            SKY_TRUE);
    sky_module_setattr(module, "herror", sky__socket_herror);

    sky_module_setattr(module, "SocketType", sky_socket_type);
    sky_module_setattr(module, "socket", sky_socket_type);

#if defined(AF_INET6)
    sky_module_setattr(module, "has_ipv6", sky_True);
#else
    sky_module_setattr(module, "has_ipv6", sky_False);
#endif

    /* TODO PySocket_CAPI_NAME */

#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n))
#define str_const(_n, _v) \
    sky_module_setattr(module, \
                       #_n, \
                       sky_string_createfrombytes(_v, strlen(_v), NULL, NULL))

#ifdef AF_UNSPEC
    int_const(AF_UNSPEC);
#endif
#ifdef AF_INET
    int_const(AF_INET);
#endif
#ifdef AF_INET6
    int_const(AF_INET6);
#endif
#ifdef AF_UNIX
    int_const(AF_UNIX);
#endif
#ifdef AF_AX25
    int_const(AF_AX25);
#endif
#ifdef AF_IPX
    int_const(AF_IPX);
#endif
#ifdef AF_APPLETALK
    int_const(AF_APPLETALK);
#endif
#ifdef AF_NETROM
    int_const(AF_NETROM);
#endif
#ifdef AF_BRIDGE
    int_const(AF_BRIDGE);
#endif
#ifdef AF_ATMPVC
    int_const(AF_ATMPVC);
#endif
#ifdef AF_AAL5
    int_const(AF_AAL5);
#endif
#ifdef AF_X25
    int_const(AF_X25);
#endif
#ifdef AF_ROSE
    int_const(AF_ROSE);
#endif
#ifdef AF_DECnet
    int_const(AF_DECnet);
#endif
#ifdef AF_NETBEUI
    int_const(AF_NETBEUI);
#endif
#ifdef AF_SECURITY
    int_const(AF_SECURITY);
#endif
#ifdef AF_KEY
    int_const(AF_KEY);
#endif
#ifdef AF_NETLINK
    int_const(AF_NETLINK);
    int_const(NETLINK_ROUTE);
#   ifdef NETLINK_SKIP
    int_const(NETLINK_SKIP);
#   endif
#   ifdef NETLINK_W1
    int_const(NETLINK_W1);
#   endif
    int_const(NETLINK_USERSOCK);
    int_const(NETLINK_FIREWALL);
#   ifdef NETLINK_TCPDIAG
    int_const(NETLINK_TCPDIAG);
#   endif
#   ifdef NETLINK_NFLOG
    int_const(NETLINK_NFLOG);
#   endif
#   ifdef NETLINK_XFRM
    int_const(NETLINK_XFRM);
#   endif
#   ifdef NETLINK_ARPD
    int_const(NETLINK_ARPD);
#   endif
#   ifdef NETLINK_ROUTE6
    int_const(NETLINK_ROUTE6);
#   endif
#   ifdef NETLINK_IP6_FW
    int_const(NETLINK_IP6_FW);
#   endif
#   ifdef NETLINK_DNRTMSG
    int_const(NETLINK_DNRTMSG);
#   endif
#   ifdef NETLINK_TAPBASE
    int_const(NETLINK_TAPBASE);
#   endif
#endif
#ifdef AF_ROUTE
    int_const(AF_ROUTE);
#endif
#ifdef AF_ASH
    int_const(AF_ASH);
#endif
#ifdef AF_ECONET
    int_const(AF_ECONET);
#endif
#ifdef AF_ATMSVC
    int_const(AF_ATMSVC);
#endif
#ifdef AF_SNA
    int_const(AF_SNA);
#endif
#ifdef AF_IRDA
    int_const(AF_IRDA);
#endif
#ifdef AF_PPPOX
    int_const(AF_PPPOX);
#endif
#ifdef AF_WANPIPE
    int_const(AF_WANPIPE);
#endif
#ifdef AF_LLC
    int_const(AF_LLC);
#endif

#if 0   /* TODO Bluetooth constants */
    /* Bluetooth */
    int_const(AF_BLUETOOTH);
    int_const(BTPROTO_L2CAP);
    int_const(BTPROTO_HCI);
    int_const(SOL_HCI);
#if !defined(__NetBSD__) && !defined(__DragonFly__)
    int_const(HCI_FILTER);
#endif
#if !defined(__FreeBSD__)
#   if !defined(__NetBSD__) && !defined(__DragonFly__)
    int_const(HCI_TIME_STAMP);
#   endif
    int_const(HCI_DATA_DIR);
    int_const(BTPROTO_SC0);
#endif
    int_const(BTPROTO_RFCOMM);
    str_const(BDADDR_ANY, "00:00:00:00:00:00");
    str_const(BDADDR_LOCAL, "00:00:00:FF:FF:FF");
#endif

#ifdef AF_CAN
    int_const(AF_CAN);
#endif
#ifdef PF_CAN
    int_const(PF_CAN);
#endif
#ifdef AF_RDS
    int_const(AF_RDS);
#endif
#ifdef PF_RDS
    int_const(PF_RDS);
#endif
#ifdef AF_SYSTEM
    int_const(AF_SYSTEM);
#endif
#ifdef PF_SYSTEM
    int_const(PF_SYSTEM);
#endif
#ifdef AF_PACKET
    int_const(AF_PACKET);
#endif
#ifdef PF_PACKET
    int_const(PF_PACKET);
#endif
#ifdef PACKET_HOST
    int_const(PACKET_HOST);
#endif
#ifdef PACKET_BROADCAST
    int_const(PACKET_BROADCAST);
#endif
#ifdef PACKET_MULTICAST
    int_const(PACKET_MULTICAST);
#endif
#ifdef PACKET_OTHERHOST
    int_const(PACKET_OTHERHOST);
#endif
#ifdef PACKET_OUTGOING
    int_const(PACKET_OUTGOING);
#endif
#ifdef PACKET_LOOPBACK
    int_const(PACKET_LOOPBACK);
#endif
#ifdef PACKET_FASTROUTE
    int_const(PACKET_FASTROUTE);
#endif

#if defined(HAVE_LINUX_TIPC_H)
    int_const(AF_TIPC);

    int_const(TIPC_ADDR_NAMESEQ);
    int_const(TIPC_ADDR_NAME);
    int_const(TIPC_ADDR_ID);
    int_const(TIPC_ZONE_SCOPE);
    int_const(TIPC_CLUSTER_SCOPE);
    int_const(TIPC_NODE_SCOPE);

    int_const(SOL_TIPC);
    int_const(TIPC_IMPORTANCE);
    int_const(TIPC_SRC_DROPPABLE);
    int_const(TIPC_DEST_DROPPABLE);
    int_const(TIPC_CONN_TIMEOUT);
    int_const(TIPC_LOW_IMPORTANCE);
    int_const(TIPC_MEDIUM_IMPORTANCE);
    int_const(TIPC_HIGH_IMPORTANCE);
    int_const(TIPC_CRITICAL_IMPORTANCE);

    int_const(TIPC_SUB_PORTS);
    int_const(TIPC_SUB_SERVICE);
#   ifdef TIPC_SUB_CANCEL
    int_const(TIPC_SUB_CANCEL);
#   endif
    int_const(TIPC_WAIT_FOREVER);
    int_const(TIPC_PUBLISHED);
    int_const(TIPC_WITHDRAWN);
    int_const(TIPC_SUBSCR_TIMEOUT);
    int_const(TIPC_CFG_SRV);
    int_const(TIPC_TOP_SRV);
#endif

    int_const(SOCK_STREAM);
    int_const(SOCK_DGRAM);
    int_const(SOCK_RAW);
    int_const(SOCK_SEQPACKET);
#ifdef SOCK_RDM
    int_const(SOCK_RDM);
#endif
#ifdef SOCK_CLOEXEC
    int_const(SOCK_CLOEXEC);
#endif
#ifdef SOCK_NONBLOCK
    int_const(SOCK_NONBLOCK);
#endif

#ifdef SO_DEBUG
    int_const(SO_DEBUG);
#endif
#ifdef SO_ACCEPTCONN
    int_const(SO_ACCEPTCONN);
#endif
#ifdef SO_REUSEADDR
    int_const(SO_REUSEADDR);
#endif
#ifdef SO_EXCLUSIVEADDRUSE
    int_const(SO_EXCLUSIVEADDRUSE);
#endif
#ifdef SO_KEEPALIVE
    int_const(SO_KEEPALIVE);
#endif
#ifdef SO_DONTROUTE
    int_const(SO_DONTROUTE);
#endif
#ifdef SO_BROADCAST
    int_const(SO_BROADCAST);
#endif
#ifdef SO_USELOOPBACK
    int_const(SO_USELOOPBACK);
#endif
#ifdef SO_LINGER
    int_const(SO_LINGER);
#endif
#ifdef SO_OOBINLINE
    int_const(SO_OOBINLINE);
#endif
#ifdef SO_REUSEPORT
    int_const(SO_REUSEPORT);
#endif
#ifdef SO_SNDBUF
    int_const(SO_SNDBUF);
#endif
#ifdef SO_RCVBUF
    int_const(SO_RCVBUF);
#endif
#ifdef SO_SNDLOWAT
    int_const(SO_SNDLOWAT);
#endif
#ifdef SO_RCVLOWAT
    int_const(SO_RCVLOWAT);
#endif
#ifdef SO_SNDTIMEO
    int_const(SO_SNDTIMEO);
#endif
#ifdef SO_RCVTIMEO
    int_const(SO_RCVTIMEO);
#endif
#ifdef SO_ERROR
    int_const(SO_ERROR);
#endif
#ifdef SO_TYPE
    int_const(SO_TYPE);
#endif
#ifdef SO_SETFIB
    int_const(SO_SETFIB);
#endif
#ifdef SO_PASSCRED
    int_const(SO_PASSCRED);
#endif
#ifdef SO_PEERCRED
    int_const(SO_PEERCRED);
#endif
#ifdef LOCAL_PEERCRED
    int_const(LOCAL_PEERCRED);
#endif
#ifdef SO_BINDTODEVICE
    int_const(SO_BINDTODEVICE);
#endif

#ifdef SOMAXCONN
    int_const(SOMAXCONN);
#else
    sky_module_setattr(module, "SOMAXCONN", sky_integer_create(5));
#endif

#ifdef SCM_RIGHTS
    int_const(SCM_RIGHTS);
#endif
#ifdef SCM_CREDENTIALS
    int_const(SCM_CREDENTIALS);
#endif
#ifdef SCM_CREDS
    int_const(SCM_CREDS);
#endif

#ifdef MSG_OOB
    int_const(MSG_OOB);
#endif
#ifdef MSG_PEEK
    int_const(MSG_PEEK);
#endif
#ifdef MSG_DONTROUTE
    int_const(MSG_DONTROUTE);
#endif
#ifdef MSG_DONTWAIT
    int_const(MSG_DONTWAIT);
#endif
#ifdef MSG_EOR
    int_const(MSG_EOR);
#endif
#ifdef MSG_TRUNC
    int_const(MSG_TRUNC);
#endif
#ifdef MSG_CTRUNC
    int_const(MSG_CTRUNC);
#endif
#ifdef MSG_WAITALL
    int_const(MSG_WAITALL);
#endif
#ifdef MSG_BTAG
    int_const(MSG_BTAG);
#endif
#ifdef MSG_ETAG
    int_const(MSG_ETAG);
#endif
#ifdef MSG_NOSIGNAL
    int_const(MSG_NOSIGNAL);
#endif
#ifdef MSG_NOTIFICATION
    int_const(MSG_NOTIFICATION);
#endif
#ifdef MSG_CMSG_CLOEXEC
    int_const(MSG_CMSG_CLOEXEC);
#endif
#ifdef MSG_ERRQUEUE
    int_const(MSG_ERRQUEUE);
#endif
#ifdef MSG_CONFIRM
    int_const(MSG_CONFIRM);
#endif
#ifdef MSG_MORE
    int_const(MSG_MORE);
#endif
#ifdef MSG_EOF
    int_const(MSG_EOF);
#endif
#ifdef MSG_BCAST
    int_const(MSG_BCAST);
#endif
#ifdef MSG_MCAST
    int_const(MSG_MCAST);
#endif

#ifdef SOL_SOCKET
    int_const(SOL_SOCKET);
#endif
#ifdef SOL_IP
    int_const(SOL_IP);
#else
    sky_module_setattr(module, "SOL_IP", sky_integer_zero);
#endif
#ifdef SOL_IPX
    int_const(SOL_IPX);
#endif
#ifdef SOL_AX25
    int_const(SOL_AX25);
#endif
#ifdef SOL_ATALK
    int_const(SOL_ATALK);
#endif
#ifdef SOL_NETROM
    int_const(SOL_NETROM);
#endif
#ifdef SOL_ROSE
    int_const(SOL_ROSE);
#endif
#ifdef SOL_TCP
    int_const(SOL_TCP);
#else
    sky_module_setattr(module, "SOL_TCP", sky_integer_create(6));
#endif
#ifdef SOL_UDP
    int_const(SOL_UDP);
#else
    sky_module_setattr(module, "SOL_UDP", sky_integer_create(17));
#endif
#ifdef SOL_CAN_BASE
    int_const(SOL_CAN_BASE);
#endif
#ifdef SOL_CAN_RAW
    int_const(SOL_CAN_RAW);
    int_const(CAN_RAW);
#endif
#if defined(HAVE_LINUX_CAN_H)
    int_const(CAN_EFF_FLAG);
    int_const(CAN_RTR_FLAG);
    int_const(CAN_ERR_FLAG);
    int_const(CAN_SFF_MASK);
    int_const(CAN_EFF_MASK);
    int_const(CAN_ERR_MASK);
#endif
#if defined(HAVE_LINUX_CAN_RAW_H)
    int_const(CAN_RAW_FILTER);
    int_const(CAN_RAW_ERR_FILTER);
    int_const(CAN_RAW_LOOPBACK);
    int_const(CAN_RAW_RECV_OWN_MSGS);
#endif
#ifdef SOL_RDS
    int_const(SOL_RDS);
#endif
#ifdef RDS_CANCEL_SENT_TO
    int_const(RDS_CANCEL_SENT_TO);
#endif
#ifdef RDS_GET_MR
    int_const(RDS_GET_MR);
#endif
#ifdef RDS_FREE_MR
    int_const(RDS_FREE_MR);
#endif
#ifdef RDS_RECVERR
    int_const(RDS_RECVERR);
#endif
#ifdef RDS_CONG_MONITOR
    int_const(RDS_CONG_MONITOR);
#endif
#ifdef RDS_GET_MR_FOR_DEST
    int_const(RDS_GET_MR_FOR_DEST);
#endif
#ifdef IPPROTO_IP
    int_const(IPPROTO_IP);
#else
    sky_module_setattr(module, "IPPROTO_IP", sky_integer_zero);
#endif
#ifdef IPPROTO_HOPOPTS
    int_const(IPPROTO_HOPOPTS);
#endif
#ifdef IPPROTO_ICMP
    int_const(IPPROTO_ICMP);
#else
    sky_module_setattr(module, "IPPROTO_ICMP", sky_integer_one);
#endif
#ifdef IPPROTO_IGMP
    int_const(IPPROTO_IGMP);
#endif
#ifdef IPPROTO_GGP
    int_const(IPPROTO_GGP);
#endif
#ifdef IPPROTO_IPV4
    int_const(IPPROTO_IPV4);
#endif
#ifdef IPPROTO_IPV6
    int_const(IPPROTO_IPV6);
#endif
#ifdef IPPROTO_IPIP
    int_const(IPPROTO_IPIP);
#endif
#ifdef IPPROTO_TCP
    int_const(IPPROTO_TCP);
#else
    sky_module_setattr(module, "IPPROTO_TCP", sky_integer_create(6));
#endif
#ifdef IPPROTO_EGP
    int_const(IPPROTO_EGP);
#endif
#ifdef IPPROTO_PUP
    int_const(IPPROTO_PUP);
#endif
#ifdef IPPROTO_UDP
    int_const(IPPROTO_UDP);
#else
    sky_module_setattr(module, "IPPROTO_UDP", sky_integer_create(17));
#endif
#ifdef IPPROTO_IDP
    int_const(IPPROTO_IDP);
#endif
#ifdef IPPROTO_HELLO
    int_const(IPPROTO_HELLO);
#endif
#ifdef IPPROTO_ND
    int_const(IPPROTO_ND);
#endif
#ifdef IPPROTO_TP
    int_const(IPPROTO_TP);
#endif
#ifdef IPPROTO_IPV6
    int_const(IPPROTO_IPV6);
#endif
#ifdef IPPROTO_ROUTING
    int_const(IPPROTO_ROUTING);
#endif
#ifdef IPPROTO_FRAGMENT
    int_const(IPPROTO_FRAGMENT);
#endif
#ifdef IPPROTO_RSVP
    int_const(IPPROTO_RSVP);
#endif
#ifdef IPPROTO_GRE
    int_const(IPPROTO_GRE);
#endif
#ifdef IPPROTO_ESP
    int_const(IPPROTO_ESP);
#endif
#ifdef IPPROTO_AH
    int_const(IPPROTO_AH);
#endif
#ifdef IPPROTO_MOBILE
    int_const(IPPROTO_MOBILE);
#endif
#ifdef IPPROTO_ICMPV6
    int_const(IPPROTO_ICMPV6);
#endif
#ifdef IPPROTO_NONE
    int_const(IPPROTO_NONE);
#endif
#ifdef IPPROTO_DSTOPTS
    int_const(IPPROTO_DSTOPTS);
#endif
#ifdef IPPROTO_XTP
    int_const(IPPROTO_XTP);
#endif
#ifdef IPPROTO_EON
    int_const(IPPROTO_EON);
#endif
#ifdef IPPROTO_PIM
    int_const(IPPROTO_PIM);
#endif
#ifdef IPPROTO_IPCOMP
    int_const(IPPROTO_IPCOMP);
#endif
#ifdef IPPROTO_VRRP
    int_const(IPPROTO_VRRP);
#endif
#ifdef IPPROTO_SCTP
    int_const(IPPROTO_SCTP);
#endif
#ifdef IPPROTO_BIP
    int_const(IPPROTO_BIP);
#endif
#ifdef IPPROTO_RAW
    int_const(IPPROTO_RAW);
#else
    sky_module_setattr(module, "IPPROTO_RAW", sky_integer_create(255));
#endif
#ifdef IPPROTO_MAX
    int_const(IPPROTO_MAX);
#endif

#ifdef SYSPROTO_CONTROL
    int_const(SYSPROTO_CONTROL);
#endif

#ifdef IPPORT_RESERVED
    int_const(IPPORT_RESERVED);
#else
    sky_module_setattr(module, "IPPORT_RESERVED", sky_integer_create(1024));
#endif
#ifdef IPPORT_USERRESERVED
    int_const(IPPORT_USERRESERVED);
#else
    sky_module_setattr(module, "IPPORT_USERRESERVED", sky_integer_create(5000));
#endif

#ifdef INADDR_ANY
    int_const(INADDR_ANY);
#else
    sky_module_setattr(module, "INADDR_ANY", sky_integer_zero);
#endif
#ifdef INADDR_BROADCAST
    int_const(INADDR_BROADCAST);
#else
    sky_module_setattr(module,
                       "INADDR_BROADCAST",
                       sky_integer_createfromunsigned(0xFFFFFFFF));
#endif
#ifdef INADDR_LOOPBACK
    int_const(INADDR_LOOPBACK);
#else
    sky_module_setattr(module,
                       "INADDR_LOOPBACK",
                       sky_integer_createfromunsigned(0x7F000001));
#endif
#ifdef INADDR_UNSPEC_GROUP
    int_const(INADDR_UNSPEC_GROUP);
#else
    sky_module_setattr(module,
                       "INADDR_UNSPEC_GROUP",
                       sky_integer_createfromunsigned(0xE0000000));
#endif
#ifdef INADDR_ALLHOSTS_GROUP
    int_const(INADDR_ALLHOSTS_GROUP);
#else
    sky_module_setattr(module,
                       "INADDR_ALLHOSTS_GROUP",
                       sky_integer_createfromunsigned(0xE0000001));
#endif
#ifdef INADDR_MAX_LOCAL_GROUP
    int_const(INADDR_MAX_LOCAL_GROUP);
#else
    sky_module_setattr(module,
                       "INADDR_MAX_LOCAL_GROUP",
                       sky_integer_createfromunsigned(0xE00000FF));
#endif
#ifdef INADDR_NONE
    int_const(INADDR_NONE);
#else
    sky_module_setattr(module,
                       "INADDR_NONE",
                       sky_integer_createfromunsigned(0xFFFFFFFF));
#endif

#ifdef IP_OPTIONS
    int_const(IP_OPTIONS);
#endif
#ifdef IP_HDRINCL
    int_const(IP_HDRINCL);
#endif
#ifdef IP_TOS
    int_const(IP_TOS);
#endif
#ifdef IP_TTL
    int_const(IP_TTL);
#endif
#ifdef IP_RECVOPTS
    int_const(IP_RECVOPTS);
#endif
#ifdef IP_RECVRETOPTS
    int_const(IP_RECVRETOPTS);
#endif
#ifdef IP_RECVDSTADDR
    int_const(IP_RECVDSTADDR);
#endif
#ifdef IP_RETOPTS
    int_const(IP_RETOPTS);
#endif
#ifdef IP_MULTICAST_IF
    int_const(IP_MULTICAST_IF);
#endif
#ifdef IP_MULTICAST_TTL
    int_const(IP_MULTICAST_TTL);
#endif
#ifdef IP_MULTICAST_LOOP
    int_const(IP_MULTICAST_LOOP);
#endif
#ifdef IP_ADD_MEMBERSHIP
    int_const(IP_ADD_MEMBERSHIP);
#endif
#ifdef IP_DROP_MEMBERSHIP
    int_const(IP_DROP_MEMBERSHIP);
#endif
#ifdef IP_DEFAULT_MULTICAST_TTL
    int_const(IP_DEFAULT_MULTICAST_TTL);
#endif
#ifdef IP_DEFAULT_MULTICAST_LOOP
    int_const(IP_DEFAULT_MULTICAST_LOOP);
#endif
#ifdef IP_MAX_MEMBERSHIPS
    int_const(IP_MAX_MEMBERSHIPS);
#endif
#ifdef IP_TRANSPARENT
    int_const(IP_TRANSPARENT);
#endif

#ifdef IPV6_JOIN_GROUP
    int_const(IPV6_JOIN_GROUP);
#endif
#ifdef IPV6_LEAVE_GROUP
    int_const(IPV6_LEAVE_GROUP);
#endif
#ifdef IPV6_MULTICAST_HOPS
    int_const(IPV6_MULTICAST_HOPS);
#endif
#ifdef IPV6_MULTICAST_IF
    int_const(IPV6_MULTICAST_IF);
#endif
#ifdef IPV6_MULTICAST_LOOP
    int_const(IPV6_MULTICAST_LOOP);
#endif
#ifdef IPV6_UNICAST_HOPS
    int_const(IPV6_UNICAST_HOPS);
#endif
#ifdef IPV6_V6ONLY
    int_const(IPV6_V6ONLY);
#endif
#ifdef IPV6_CHECKSUM
    int_const(IPV6_CHECKSUM);
#endif
#ifdef IPV6_DONTFRAG
    int_const(IPV6_DONTFRAG);
#endif
#ifdef IPV6_DSTOPTS
    int_const(IPV6_DSTOPTS);
#endif
#ifdef IPV6_HOPLIMIT
    int_const(IPV6_HOPLIMIT);
#endif
#ifdef IPV6_HOPOPTS
    int_const(IPV6_HOPOPTS);
#endif
#ifdef IPV6_NEXTHOP
    int_const(IPV6_NEXTHOP);
#endif
#ifdef IPV6_PATHMTU
    int_const(IPV6_PATHMTU);
#endif
#ifdef IPV6_PKTINFO
    int_const(IPV6_PKTINFO);
#endif
#ifdef IPV6_RECVDSTOPTS
    int_const(IPV6_RECVDSTOPTS);
#endif
#ifdef IPV6_RECVHOPLIMIT
    int_const(IPV6_RECVHOPLIMIT);
#endif
#ifdef IPV6_RECVHOPOPTS
    int_const(IPV6_RECVHOPOPTS);
#endif
#ifdef IPV6_RECVPKTINFO
    int_const(IPV6_RECVPKTINFO);
#endif
#ifdef IPV6_RECVRTHDR
    int_const(IPV6_RECVRTHDR);
#endif
#ifdef IPV6_RECVTCLASS
    int_const(IPV6_RECVTCLASS);
#endif
#ifdef IPV6_RTHDR
    int_const(IPV6_RTHDR);
#endif
#ifdef IPV6_RTHDRDSTOPTS
    int_const(IPV6_RTHDRDSTOPTS);
#endif
#ifdef IPV6_RTHDR_TYPE_0
    int_const(IPV6_RTHDR_TYPE_0);
#endif
#ifdef IPV6_RECVPATHMTU
    int_const(IPV6_RECVPATHMTU);
#endif
#ifdef IPV6_TCLASS
    int_const(IPV6_TCLASS);
#endif
#ifdef IPV6_USE_MIN_MTU
    int_const(IPV6_USE_MIN_MTU);
#endif

#ifdef TCP_NODELAY
    int_const(TCP_NODELAY);
#endif
#ifdef TCP_MAXSEG
    int_const(TCP_MAXSEG);
#endif
#ifdef TCP_CORK
    int_const(TCP_CORK);
#endif
#ifdef TCP_KEEPIDLE
    int_const(TCP_KEEPIDLE);
#endif
#ifdef TCP_KEEPINTVL
    int_const(TCP_KEEPINTVL);
#endif
#ifdef TCP_KEEPCNT
    int_const(TCP_KEEPCNT);
#endif
#ifdef TCP_SYNCNT
    int_const(TCP_SYNCNT);
#endif
#ifdef TCP_LINGER2
    int_const(TCP_LINGER2);
#endif
#ifdef TCP_DEFER_ACCEPT
    int_const(TCP_DEFER_ACCEPT);
#endif
#ifdef TCP_WINDOW_CLAMP
    int_const(TCP_WINDOW_CLAMP);
#endif
#ifdef TCP_INFO
    int_const(TCP_INFO);
#endif
#ifdef TCP_QUICKACK
    int_const(TCP_QUICKACK);
#endif

#ifdef IPX_TYPE
    int_const(IPX_TYPE);
#endif

#ifdef RDS_CMSG_RDMA_ARGS
    int_const(RDS_CMSG_RDMA_ARGS);
#endif
#ifdef RDS_CMSG_RDMA_DEST
    int_const(RDS_CMSG_RDMA_DEST);
#endif
#ifdef RDS_CMSG_RDMA_MAP
    int_const(RDS_CMSG_RDMA_MAP);
#endif
#ifdef RDS_CMSG_RDMA_STATUS
    int_const(RDS_CMSG_RDMA_STATUS);
#endif
#ifdef RDS_CMSG_RDMA_UPDATE
    int_const(RDS_CMSG_RDMA_UPDATE);
#endif
#ifdef RDS_RDMA_READWRITE
    int_const(RDS_RDMA_READWRITE);
#endif
#ifdef RDS_RDMA_FENCE
    int_const(RDS_RDMA_FENCE);
#endif
#ifdef RDS_RDMA_INVALIDATE
    int_const(RDS_RDMA_INVALIDATE);
#endif
#ifdef RDS_RDMA_USE_ONCE
    int_const(RDS_RDMA_USE_ONCE);
#endif
#ifdef RDS_RDMA_DONTWAIT
    int_const(RDS_RDMA_DONTWAIT);
#endif
#ifdef RDS_RDMA_NOTIFY_ME
    int_const(RDS_RDMA_NOTIFY_ME);
#endif
#ifdef RDS_RDMA_SILENT
    int_const(RDS_RDMA_SILENT);
#endif

#ifdef EAI_ADDRFAMILY
    int_const(EAI_ADDRFAMILY);
#endif
#ifdef EAI_AGAIN
    int_const(EAI_AGAIN);
#endif
#ifdef EAI_BADFLAGS
    int_const(EAI_BADFLAGS);
#endif
#ifdef EAI_FAIL
    int_const(EAI_FAIL);
#endif
#ifdef EAI_FAMILY
    int_const(EAI_FAMILY);
#endif
#ifdef EAI_MEMORY
    int_const(EAI_MEMORY);
#endif
#ifdef EAI_NODATA
    int_const(EAI_NODATA);
#endif
#ifdef EAI_NONAME
    int_const(EAI_NONAME);
#endif
#ifdef EAI_OVERFLOW
    int_const(EAI_OVERFLOW);
#endif
#ifdef EAI_SERVICE
    int_const(EAI_SERVICE);
#endif
#ifdef EAI_SOCKTYPE
    int_const(EAI_SOCKTYPE);
#endif
#ifdef EAI_SYSTEM
    int_const(EAI_SYSTEM);
#endif
#ifdef EAI_BADHINTS
    int_const(EAI_BADHINTS);
#endif
#ifdef EAI_PROTOCOL
    int_const(EAI_PROTOCOL);
#endif
#ifdef EAI_MAX
    int_const(EAI_MAX);
#endif
#ifdef AI_PASSIVE
    int_const(AI_PASSIVE);
#endif
#ifdef AI_CANONNAME
    int_const(AI_CANONNAME);
#endif
#ifdef AI_NUMERICHOST
    int_const(AI_NUMERICHOST);
#endif
#ifdef AI_NUMERICSERV
    int_const(AI_NUMERICSERV);
#endif
#ifdef AI_MASK
    int_const(AI_MASK);
#endif
#ifdef AI_ALL
    int_const(AI_ALL);
#endif
#ifdef AI_V4MAPPED_CFG
    int_const(AI_V4MAPPED_CFG);
#endif
#ifdef AI_ADDRCONFIG
    int_const(AI_ADDRCONFIG);
#endif
#ifdef AI_V4MAPPED
    int_const(AI_V4MAPPED);
#endif
#ifdef AI_DEFAULT
    int_const(AI_DEFAULT);
#endif
#ifdef NI_MAXHOST
    int_const(NI_MAXHOST);
#endif
#ifdef NI_MAXSERV
    int_const(NI_MAXSERV);
#endif
#ifdef NI_NOFQDN
    int_const(NI_NOFQDN);
#endif
#ifdef NI_NUMERICHOST
    int_const(NI_NUMERICHOST);
#endif
#ifdef NI_NAMEREQD
    int_const(NI_NAMEREQD);
#endif
#ifdef NI_NUMERICSERV
    int_const(NI_NUMERICSERV);
#endif
#ifdef NI_DGRAM
    int_const(NI_DGRAM);
#endif

#if defined(SHUT_RD)
    int_const(SHUT_RD);
#elif defined(SD_RECEIVE)
    sky_module_setattr(module, "SHUT_RD", sky_integer_create(SD_RECEIVE));
#else
    sky_module_setattr(module, "SHUT_RD", sky_integer_zero);
#endif
#if defined(SHUT_WR)
    int_const(SHUT_WR);
#elif defined(SD_SEND)
    sky_module_setattr(module, "SHUT_WR", sky_integer_create(SD_SEND));
#else
    sky_module_setattr(module, "SHUT_WR", sky_integer_one);
#endif
#if defined(SHUT_RDWR)
    int_const(SHUT_RDWR);
#elif defined(SD_BOTH)
    sky_module_setattr(module, "SHUT_RDWR", sky_integer_create(SD_BOTH));
#else
    sky_module_setattr(module, "SHUT_RDWR", sky_integer_create(2));
#endif

#ifdef SIO_RCVALL
    sky_module_setattr(module,
                       "SIO_RCVALL",
                       sky_integer_createfromunsigned(SIO_RCVALL));
    sky_module_setattr(module,
                       "SIO_KEEPALIVE_VALS",
                       sky_integer_createfromunsigned(SIO_KEEPALIVE_VALS));
    int_const(RCVALL_OFF);
    int_const(RCVALL_ON);
    int_const(RCVALL_SOCKETLEVELONLY);
#   ifdef RCVALL_IPLEVEL
    int_const(RCVALL_IPLEVEL);
#   endif
#   ifdef RCVALL_MAX
    int_const(RCVALL_MAX);
#   endif
#endif
}
