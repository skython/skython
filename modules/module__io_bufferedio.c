#include "../core/sky_private.h"

#include "module__io.h"


static const char sky__io__BufferedIOBase_detach_doc[] =
"Disconnect this buffer from its underlying raw stream and return it.\n"
"\n"
"After the raw stream has been detached, the buffer is in an unusable\n"
"state.\n";

static void
sky__io__BufferedIOBase_detach(SKY_UNUSED sky_object_t self)
{
    sky_error_raise_string(sky_UnsupportedOperation, "detach");
}


static const char sky__io__BufferedIOBase_read_doc[] =
"Read and return up to n bytes.\n"
"\n"
"If the argument is omitted, None, or negative, reads and\n"
"returns all data until EOF.\n"
"\n"
"If the argument is positive, and the underlying raw stream is\n"
"not 'interactive', multiple raw reads may be issued to satisfy\n"
"the byte count (unless EOF is reached first).  But for\n"
"interactive raw streams (as well as sockets and pipes), at most\n"
"one raw read will be issued, and a short result does not imply\n"
"that EOF is imminent.\n"
"\n"
"Returns an empty bytes object on EOF.\n"
"\n"
"Returns None if the underlying raw stream was open in non-blocking\n"
"mode and no data is available at the moment.\n";

static sky_object_t
sky__io__BufferedIOBase_read(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  ssize_t         n)
{
    sky_error_raise_string(sky_UnsupportedOperation, "read");
}


static const char sky__io__BufferedIOBase_read1_doc[] =
"Read and return up to n bytes, with at most one read() call\n"
"to the underlying raw stream. A short result does not imply\n"
"that EOF is imminent.\n"
"\n"
"Returns an empty bytes object on EOF.\n";

static sky_object_t
sky__io__BufferedIOBase_read1(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  ssize_t         n)
{
    sky_error_raise_string(sky_UnsupportedOperation, "read1");
}


static sky_object_t
sky__io__BufferedIOBase_readinto(sky_object_t self, sky_object_t b)
    /* _IOBase.read() is implemented using readinto().
     * _BufferedIOBase.readinto() is implemented using read().
     *
     * XXX  Why is this even here? CPython implements it, but it adds an extra
     *      unnecessary copy, completely defeating the purpose of readinto().
     *      In addition, _pyio.py does NOT implement this. Worse, CPython
     *      does not implement readinto() in BufferedReader, and neither does
     *      _pyio.py. The io code in CPython needs some TLC.
     */
{
    sky_buffer_t    buffer, bytes_buffer;
    sky_object_t    bytes, result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, b, SKY_BUFFER_FLAG_SIMPLE |
                                       SKY_BUFFER_FLAG_WRITABLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        bytes = sky_file_read(self, buffer.len);
        if (sky_None == bytes) {
            result = sky_None;
        }
        else {
            sky_buffer_acquire(&bytes_buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&bytes_buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);

            if (bytes_buffer.len > buffer.len) {
                sky_error_raise_format(
                        sky_ValueError,
                        "raw readinto() returned invalid length %zd "
                        "(should have been between 0 and %zd)",
                        bytes_buffer.len,
                        buffer.len);
            }
            memcpy(buffer.buf, bytes_buffer.buf, bytes_buffer.len);
            result = sky_integer_create(bytes_buffer.len);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io__BufferedIOBase_write_doc[] =
"Write the given buffer to the IO stream.\n"
"\n"
"Returns the number of bytes written, which is never less than\n"
"len(b).\n"
"\n"
"Raises BlockingIOError if the buffer is full and the\n"
"underlying raw stream cannot accept more data at the moment.\n";

static sky_object_t
sky__io__BufferedIOBase_write(
    SKY_UNUSED  sky_object_t    self,
    SKY_UNUSED  sky_object_t    b)
{
    sky_error_raise_string(sky_UnsupportedOperation, "write");
}


sky_type_t sky__io__BufferedIOBase_type = NULL;

sky_type_t
sky__io__BufferedIOBase_initialize(void)
{
    const char type_doc[] =
"Base class for buffered IO objects.\n"
"\n"
"The main difference with RawIOBase is that the read() method\n"
"supports omitting the size argument, and does not have a default\n"
"implementation that defers to readinto().\n"
"\n"
"In addition, read(), readinto() and write() may raise\n"
"BlockingIOError if the underlying raw stream is in non-blocking\n"
"mode and not ready; unlike their raw counterparts, they will never\n"
"return None.\n"
"\n"
"A typical implementation should not inherit from a RawIOBase\n"
"implementation, but wrap one.\n";

    sky_type_t  type;

    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("_BufferedIOBase"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__io__IOBase_type),
                    NULL);

    sky_type_setattr_builtin(type,
            "detach",
            sky_function_createbuiltin(
                    "_BufferedIOBase.detach",
                    sky__io__BufferedIOBase_detach_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOBase_detach,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "_BufferedIOBase.read",
                    sky__io__BufferedIOBase_read_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOBase_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "read1",
            sky_function_createbuiltin(
                    "_BufferedIOBase.read1",
                    sky__io__BufferedIOBase_read1_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOBase_read1,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "readinto",
            sky_function_createbuiltin(
                    "_BufferedIOBase.readinto",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOBase_readinto,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "_BufferedIOBase.write",
                    sky__io__BufferedIOBase_write_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOBase_write,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io__BufferedIOBase_type),
            type,
            SKY_TRUE);
    return sky__io__BufferedIOBase_type;
}


static inline void
sky__io__BufferedIOMixin__BlockingIOError(
                int             errno_code,
                const char *    msg,
                ssize_t         nwritten)
{
    sky_object_t    e;

    e = sky_object_call(sky_BlockingIOError,
                        sky_object_build("isiz", errno_code, msg, nwritten),
                        NULL);
    sky_error_raise_object(sky_object_type(e), e);
}


static inline void
sky__io__BufferedIOMixin__checkClosed(sky_object_t self)
{
    sky_object_callmethod(sky__io__BufferedIOMixin_raw(self),
                          SKY_STRING_LITERAL("_checkClosed"),
                          NULL,
                          NULL);
}


static inline void
sky__io__BufferedIOMixin__checkSeekable(sky_object_t self)
{
    if (!sky__io__BufferedIOMixin_data(self)->seekable) {
        sky_error_raise_string(sky_UnsupportedOperation,
                               "File or stream is not seekable.");
    }
}


static inline sky_object_t
sky__io__BufferedIOMixin__checkDetached(sky_object_t self)
{
    sky_object_t    raw = sky__io__BufferedIOMixin_raw(self);

    if (sky_object_isnull(raw)) {
        sky_error_raise_string(sky_ValueError,
                               "raw stream has been detached");
    }

    return raw;
}


static inline sky_object_t
sky__io__BufferedIOMixin__forward(
                sky_object_t    self,
                sky_string_t    method,
                sky_tuple_t     args,
                sky_dict_t      kws)
{
    sky_object_t    raw;

    raw = sky__io__BufferedIOMixin__checkDetached(self);
    return sky_object_callmethod(raw, method, args, kws);
}


static inline void
sky__io__BufferedIOMixin__lock(sky_object_t self)
{
    if (sky_object_gc_isthreaded() && sky_object_isglobal(self)) {
        sky__io__BufferedIOMixin_data_t *self_data;

        self_data = sky__io__BufferedIOMixin_data(self);
        sky_mutex_lock(self_data->mutex, SKY_TIME_INFINITE);
        sky_asset_save(self_data->mutex,
                       (sky_free_t)sky_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);
    }
}


static inline void
sky__io__BufferedIOMixin__markUsed(
                sky_object_t    self,
                ssize_t         start,
                ssize_t         length)
{
    ssize_t end = start + length;

    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (!iomixin_data->buffer_end) {
        iomixin_data->buffer_start = start;
        iomixin_data->buffer_end = end;
    }
    else {
        iomixin_data->buffer_start = SKY_MIN(iomixin_data->buffer_start, start);
        iomixin_data->buffer_end = SKY_MAX(iomixin_data->buffer_end, end);
    }
}


static inline void
sky__io__BufferedIOMixin__markDirty(
                sky_object_t    self,
                ssize_t         start,
                ssize_t         length)
{
    ssize_t end = start + length;

    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (!iomixin_data->dirty) {
        iomixin_data->dirty = (length > 0);
        iomixin_data->dirty_start = start;
        iomixin_data->dirty_end = end;
    }
    else {
        iomixin_data->dirty_start = SKY_MIN(iomixin_data->dirty_start, start);
        iomixin_data->dirty_end = SKY_MAX(iomixin_data->dirty_end, end);
    }
    sky__io__BufferedIOMixin__markUsed(self, start, length);
}


static inline ssize_t
sky__io__BufferedIOMixin__rawSeek(sky_object_t self, ssize_t pos, int whence)
{
    sky_object_t    raw = sky__io__BufferedIOMixin_raw(self);
    sky_type_t      raw_type = sky_object_type(raw);

    if (sky__io_FileIO_type == raw_type) {
        return sky__io_FileIO_seek_native(raw, pos, whence);
    }
    if (sky__io_BytesIO_type == raw_type) {
        return sky__io_BytesIO_seek_native(raw, pos, whence);
    }
    if (sky__io_StringIO_type == raw_type) {
        return sky__io_StringIO_seek_native(raw, pos, whence);
    }

    return sky_file_seek(raw, pos, whence);
}


static inline void
sky__io__BufferedIOMixin__sync(sky_object_t self, ssize_t pos)
{
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    iomixin_data->buffer_start = 0;
    iomixin_data->buffer_end = 0;
    iomixin_data->buffer_pos = pos % iomixin_data->buffer_size;
    iomixin_data->raw_pos =
            sky__io__BufferedIOMixin__rawSeek(
                    self,
                    -iomixin_data->buffer_pos,
                    1);
    if (iomixin_data->raw_pos % iomixin_data->buffer_size) {
        iomixin_data->stable = 0;
        sky_error_raise_string(
                sky_OSError,
                "raw seek refuses to cooperate with buffered seek");
    }
    iomixin_data->stable = 1;
}


static inline void
sky__io__BufferedIOMixin__checkStable(sky_object_t self)
{
    ssize_t                         pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (!iomixin_data->stable) {
        pos = sky__io__BufferedIOMixin__rawSeek(self, 0, 1);
        sky__io__BufferedIOMixin__sync(self, pos);
    }
}


static void
sky__io__BufferedIOMixin_close(sky_object_t self)
{
    sky_object_t    closed, raw;

    raw = sky__io__BufferedIOMixin_raw(self);
    if (!sky_object_isnull(raw)) {
        closed = sky_object_getattr(raw,
                                    SKY_STRING_LITERAL("closed"),
                                    sky_NotSpecified);
        if (!sky_object_bool(closed)) {
            sky_file_flush(self);
            sky_file_close(raw);
        }
    }
}


static sky_object_t
sky__io__BufferedIOMixin_closed(sky_object_t self)
{
    return sky_object_getattr(sky__io__BufferedIOMixin_raw(self),
                              SKY_STRING_LITERAL("closed"),
                              sky_NotSpecified);
}


static sky_object_t
sky__io__BufferedIOMixin_closed_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky__io__BufferedIOMixin_closed(self);
}


static void
sky__io__BufferedIOMixin_del(sky_object_t self)
{
    if (sky__io__BufferedIOMixin_data(self)->dirty) {
        sky_file_flush(self);
    }
}


static sky_object_t
sky__io__BufferedIOMixin_detach(sky_object_t self)
{
    sky_object_t                    raw;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    raw = iomixin_data->raw;

    if (sky_object_isnull(raw)) {
        sky_error_raise_string(sky_ValueError,
                               "raw stream already detached");
    }
    sky_file_flush(self);
    iomixin_data->raw = NULL;

    return raw;
}


static const char sky__io__BufferedIOMixin_fileno_doc[] =
"fileno() -> int. Returns the file descriptor for the underlying stream.";

static sky_object_t
sky__io__BufferedIOMixin_fileno(sky_object_t self)
{
    return sky__io__BufferedIOMixin__forward(self,
                                             SKY_STRING_LITERAL("fileno"),
                                             NULL,
                                             NULL);
}


static void
sky__io__BufferedIOMixin_flush(sky_object_t self)
{
    sky__io__BufferedIOMixin__forward(self,
                                      SKY_STRING_LITERAL("flush"),
                                      NULL,
                                      NULL);
}


static sky_object_t
sky__io__BufferedIOMixin_getstate(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "cannot serialize %#@ object",
                           sky_type_name(sky_object_type(self)));
}


static void
sky__io__BufferedIOMixin_init(
                sky_object_t    self,
                sky_object_t    raw,
                ssize_t         buffer_size)
{
    ssize_t                                     pos;
    sky_object_t                                value;
    sky__io__BufferedIOMixin_data_t * volatile  iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (iomixin_data->dirty) {
        sky_file_flush(self);
    }

    if (buffer_size <= 0) {
        sky_error_raise_string(sky_ValueError, "invalid buffer size");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);

        sky_object_gc_set(&(iomixin_data->raw), raw, self);

        if (iomixin_data->buffer && iomixin_data->buffer_size != buffer_size) {
            sky_free(iomixin_data->buffer);
            iomixin_data->buffer = NULL;
        }
        iomixin_data->buffer = sky_malloc(buffer_size);

        iomixin_data->raw_pos = 0;
        iomixin_data->buffer_pos = 0;
        iomixin_data->buffer_start = 0;
        iomixin_data->buffer_end = 0;
        iomixin_data->buffer_size = buffer_size;
        iomixin_data->seekable = 1;
        iomixin_data->dirty = 0;
        iomixin_data->stable = 1;

        /* Check to see if the raw file is closed. It shouldn't be an error if
         * it is, but it is kind of pointless.
         */
        if (!sky_object_bool(sky__io__BufferedIOMixin_closed(self))) {
            /* Find out where the raw file pointer currently is. It's possible
             * that the raw file does not support seeking, so handle that
             * condition. If it does, move the raw file pointer around to the
             * buffer_size boundary that is just before the raw file position
             * so that we can perform reads and writes on block boundaries
             * (assuming that the buffer_size is properly specified as a block
             * boundary for the raw file (if appropriate, of course; BytesIO
             * and StringIO don't have blocks, for example).
             */
            SKY_ERROR_TRY {
                if (sky_file_isatty(raw)) {
                    pos = sky__io__BufferedIOMixin__rawSeek(self, 0, 0);
                }
                else {
                    pos = sky__io__BufferedIOMixin__rawSeek(self, 0, 1);
                }
                sky__io__BufferedIOMixin__sync(self, pos);
            } SKY_ERROR_EXCEPT(sky_UnsupportedOperation) {
                iomixin_data->seekable = 0;
            } SKY_ERROR_EXCEPT(sky_OSError) {
                value = sky_object_getattr(sky_error_current()->value,
                                           SKY_STRING_LITERAL("errno"),
                                           NULL);
                if (!sky_object_isa(value, sky_integer_type) ||
                    sky_integer_value(value, INT_MIN, INT_MAX, NULL) != ESPIPE)
                {
                    sky_error_raise();
                }

                iomixin_data->seekable = 0;
            } SKY_ERROR_TRY_END;
        }
    } SKY_ASSET_BLOCK_END;
}


static const char sky__io__BufferedIOMixin_isatty_doc[] =
"isatty() -> bool. True if the underlying stream is \"interactive\".";

static sky_object_t
sky__io__BufferedIOMixin_isatty(sky_object_t self)
{
    return sky__io__BufferedIOMixin__forward(self,
                                             SKY_STRING_LITERAL("isatty"),
                                             NULL,
                                             NULL);
}


static sky_object_t
sky__io__BufferedIOMixin_mode_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_object_getattr(sky__io__BufferedIOMixin_raw(self),
                              SKY_STRING_LITERAL("mode"),
                              sky_NotSpecified);
}


static sky_object_t
sky__io__BufferedIOMixin_name_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_object_getattr(sky__io__BufferedIOMixin_raw(self),
                              SKY_STRING_LITERAL("name"),
                              sky_NotSpecified);
}


static const char sky__io__BufferedIOMixin_readable_doc[] =
"readable() -> bool. True if the underlying raw stream is open for reading.";

static sky_object_t
sky__io__BufferedIOMixin_readable(sky_object_t self)
{
    return sky__io__BufferedIOMixin__forward(self,
                                             SKY_STRING_LITERAL("readable"),
                                             NULL,
                                             NULL);
}


static sky_string_t
sky__io__BufferedIOMixin_repr(sky_object_t self)
{
    sky_object_t    name;
    sky_string_t    type_name;

    type_name = sky_type_name(sky_object_type(self));
    if (!(name = sky_object_getattr(self, SKY_STRING_LITERAL("name"), NULL))) {
        return sky_string_createfromformat("<_io.%@>", type_name);
    }
    return sky_string_createfromformat("<_io.%@ name=%#@>", type_name, name);
}


static ssize_t
sky__io__BufferedIOMixin_seek_unlocked(
                sky_object_t    self,
                ssize_t         pos,
                int             whence)
{
    ssize_t                         current_pos, new_pos, new_raw_pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    sky__io__BufferedIOMixin__checkClosed(self);
    sky__io__BufferedIOMixin__checkSeekable(self);

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (iomixin_data->stable && (0 == whence || 1 == whence)) {
        current_pos = iomixin_data->raw_pos + iomixin_data->buffer_pos;
        new_pos = (whence ? current_pos + pos : pos);
        if (new_pos < 0) {
            sky_error_raise_string(sky_OSError, "invalid seek position");
        }

        new_raw_pos = new_pos / iomixin_data->buffer_size;
        if (new_raw_pos == iomixin_data->raw_pos) {
            iomixin_data->buffer_pos = new_pos % iomixin_data->buffer_size;
            return new_pos;
        }

        /* We know where we are, so we can quickly maneuver to the desired
         * position, but it does require a seek. The seek can destabilize our
         * view of the world, so be careful to make sure it returns to us what
         * we're expecting. If it doesn't, fall back to an unstable condition,
         * which will likely require another seek.
         */
        if (iomixin_data->dirty) {
            sky_file_flush(self);
        }

        iomixin_data->raw_pos =
                sky__io__BufferedIOMixin__rawSeek(self, new_raw_pos, 0);
        if (iomixin_data->raw_pos != new_raw_pos) {
            iomixin_data->stable = 0;
        }
        else {
            iomixin_data->buffer_pos = new_pos % iomixin_data->buffer_size;
            iomixin_data->buffer_start = 0;
            iomixin_data->buffer_end = 0;
            return new_pos;
        }
    }
    else if (iomixin_data->dirty) {
        sky_file_flush(self);
    }

    /* We are unstable or we can't optimize the location (SEEK_END), so do a
     * raw seek with the requested parameters first. Then use that information
     * to move our raw position to where it should be.
     */
    pos = sky__io__BufferedIOMixin__rawSeek(self, pos, whence);
    sky__io__BufferedIOMixin__sync(self, pos);
    return iomixin_data->raw_pos + iomixin_data->buffer_pos;
}


static ssize_t
sky__io__BufferedIOMixin_seek(sky_object_t self, ssize_t pos, int whence)
{
    if (whence < 0 || whence > 2) {
        sky_error_raise_string(sky_ValueError, "invalid whence");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);

        pos = sky__io__BufferedIOMixin_seek_unlocked(self, pos, whence);
    } SKY_ASSET_BLOCK_END;

    return pos;
}


static const char sky__io__BufferedIOMixin_seekable_doc[] =
"seekable() -> bool. True if the underlying raw stream supports random access.";

static sky_object_t
sky__io__BufferedIOMixin_seekable(sky_object_t self)
{
    return sky__io__BufferedIOMixin__forward(self,
                                             SKY_STRING_LITERAL("seekable"),
                                             NULL,
                                             NULL);
}


static size_t
sky__io__BufferedIOMixin_sizeof(sky_object_t self)
{
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    return sky_memsize(self) + sky_memsize(iomixin_data->buffer);
}


static ssize_t
sky__io__BufferedIOMixin_tell(sky_object_t self)
{
    ssize_t                         pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkSeekable(self);
        sky__io__BufferedIOMixin__checkStable(self);

        iomixin_data = sky__io__BufferedIOMixin_data(self);
        pos = iomixin_data->raw_pos + iomixin_data->buffer_pos;
    } SKY_ASSET_BLOCK_END;

    return pos;
}


static void
sky__io__BufferedIOMixin_truncate(sky_object_t self, sky_object_t pos)
{
    sky_object_t    raw = sky__io__BufferedIOMixin_raw(self);

    sky_file_flush(self);
    if (sky_object_isnull(pos)) {
        pos = sky_object_callmethod(raw, SKY_STRING_LITERAL("tell"), NULL, NULL);
    }

    sky_object_callmethod(raw,
                          SKY_STRING_LITERAL("truncate"),
                          sky_tuple_pack(1, pos),
                          NULL);
}


static const char sky__io__BufferedIOMixin_writable_doc[] =
"writable() -> bool. True if the underlying raw stream is open for writing.";

static sky_object_t
sky__io__BufferedIOMixin_writable(sky_object_t self)
{
    return sky__io__BufferedIOMixin__forward(self,
                                             SKY_STRING_LITERAL("writable"),
                                             NULL,
                                             NULL);
}


static void
sky__io__BufferedIOMixin_instance_initialize(sky_object_t self, void *data)
{
    sky__io__BufferedIOMixin_data_t *self_data = data;

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->mutex)),
                      sky_mutex_create(SKY_FALSE, SKY_TRUE),
                      self);
}


static void
sky__io__BufferedIOMixin_instance_finalize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__io__BufferedIOMixin_data_t *self_data = data;

    sky_free(self_data->buffer);
}


static void
sky__io__BufferedIOMixin_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io__BufferedIOMixin_data_t *self_data = data;

    sky_object_visit(self_data->raw, visit_data);
    sky_object_visit(self_data->mutex, visit_data);
}


sky_type_t sky__io__BufferedIOMixin_type = NULL;

sky_type_t
sky__io__BufferedIOMixin_initialize(void)
{
    const char type_doc[] =
"A mixin implementation of BufferedIOBase with an underlying raw stream.";

    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__io__BufferedIOMixin_instance_initialize;
    template.finalize = sky__io__BufferedIOMixin_instance_finalize;
    template.visit = sky__io__BufferedIOMixin_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("_BufferedIOMixin"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    sizeof(sky__io__BufferedIOMixin_data_t),
                    0,
                    sky_tuple_pack(1, sky__io__BufferedIOBase_type),
                    &template);

    sky_type_setattr_member(type,
            "buffer_size",
            NULL,
            offsetof(sky__io__BufferedIOMixin_data_t, buffer_size),
            SKY_DATA_TYPE_SSIZE_T,
            SKY_MEMBER_FLAG_READONLY);
    sky_type_setattr_member(type,
            "raw",
            NULL,
            offsetof(sky__io__BufferedIOMixin_data_t, raw),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_READONLY);

    sky_type_setattr_getset(type,
            "closed",
            NULL,
            sky__io__BufferedIOMixin_closed_getter,
            NULL);
    sky_type_setattr_getset(type,
            "mode",
            NULL,
            sky__io__BufferedIOMixin_mode_getter,
            NULL);
    sky_type_setattr_getset(type,
            "name",
            NULL,
            sky__io__BufferedIOMixin_name_getter,
            NULL);

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.close",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "detach",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.detach",
                    sky__io__BufferedIOBase_detach_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_detach,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "fileno",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.fileno",
                    sky__io__BufferedIOMixin_fileno_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_fileno,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "flush",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.flush",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_flush,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "isatty",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.isatty",
                    sky__io__BufferedIOMixin_isatty_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_isatty,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.readable",
                    sky__io__BufferedIOMixin_readable_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_readable,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "seek",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.seek",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_seek,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "whence", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "seekable",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.seekable",
                    sky__io__BufferedIOMixin_seekable_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_seekable,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "tell",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.tell",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_tell,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.truncate",
                    NULL,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "_BufferedIOMixin.writable",
                    sky__io__BufferedIOMixin_writable_doc,
                    (sky_native_code_function_t)sky__io__BufferedIOMixin_writable,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io__BufferedIOMixin_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "raw", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer_size", SKY_DATA_TYPE_SSIZE_T, NULL,
            NULL);

    sky_type_setmethodslots(type,
            "__del__", sky__io__BufferedIOMixin_del,
            "__repr__", sky__io__BufferedIOMixin_repr,
            "__sizeof__", sky__io__BufferedIOMixin_sizeof,
            "__getstate__", sky__io__BufferedIOMixin_getstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io__BufferedIOMixin_type),
            type,
            SKY_TRUE);
    return sky__io__BufferedIOMixin_type;
}


static ssize_t
sky__io_BufferedReader__rawReadInto(
                sky_object_t    self,
                void *          bytes,
                ssize_t         nbytes)
{
    sky_object_t    raw = sky__io__BufferedIOMixin_raw(self);
    sky_type_t      raw_type = sky_object_type(raw);

    ssize_t                     nread;
    sky_buffer_t                *buffer;
    sky_object_t                value;
    sky_object_t                result;
    sky_memoryview_t volatile   view;

    if (sky__io_FileIO_type == raw_type) {
        do {
            nread = sky__io_FileIO_readinto_native(raw, bytes, nbytes);
        } while (-1 == nread);
        return nread;
    }
    if (sky__io_BytesIO_type == raw_type) {
        do {
            nread = sky__io_BytesIO_readinto_native(raw, bytes, nbytes);
        } while (-1 == nread);
        return nread;
    }
    if (sky__io_StringIO_type == raw_type) {
        do {
            nread = sky__io_StringIO_readinto_native(raw, bytes, nbytes);
        } while (-1 == nread);
        return nread;
    }

    SKY_ASSET_BLOCK_BEGIN {
        SKY_ASSET_BLOCK_BEGIN {
            buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                      SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_initialize(buffer,
                                  bytes,
                                  nbytes,
                                  SKY_FALSE,
                                  SKY_BUFFER_FLAG_CONTIG);
            view = sky_memoryview_createwithbuffer(buffer);
        } SKY_ASSET_BLOCK_END;
        sky_asset_save(view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        do {
            SKY_ERROR_TRY {
                result = sky__io__BufferedIOMixin__forward(
                                self,
                                SKY_STRING_LITERAL("readinto"),
                                sky_tuple_pack(1, view),
                                NULL);
            } SKY_ERROR_EXCEPT(sky_OSError) {
                value = sky_object_getattr(sky_error_current()->value,
                                           SKY_STRING_LITERAL("errno"),
                                           NULL);
                if (!sky_object_isa(value, sky_integer_type) ||
                    sky_integer_value(value, INT_MIN, INT_MAX, NULL) != EINTR)
                {
                    sky_error_raise();
                }
                result = NULL;
            } SKY_ERROR_TRY_END;
        } while (!result);
        if (sky_None == result) {
            nread = -1;
        }
        else {
            nread = sky_integer_value(result, SSIZE_MIN, SSIZE_MAX, NULL);
            if (nread < 0 || nread > nbytes) {
                sky_error_raise_format(
                        sky_ValueError,
                        "raw readinto() returned invalid length %zd "
                        "(should have been between 0 and %zd)",
                        nread,
                        nbytes);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return nread;
}


static inline sky_bool_t
sky__io_BufferedReader__primeNeeded(sky_object_t self)
    /* A prime is needed if the buffer position is outside the range of what
     * is in memory, either at the head or the tail. Remember here that start
     * is inclusive and end is exclusive, as in buffer[start:end].
     */
{
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (iomixin_data->buffer_pos < iomixin_data->buffer_start ||
        iomixin_data->buffer_pos > iomixin_data->buffer_end)
    {
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static ssize_t
sky__io_BufferedReader__primeBuffer(sky_object_t self)
    /* If the buffer is partial and the read pointer is before or after what's
     * actually in the buffer, seek back to raw_pos and fill the buffer. The
     * return is -2 for no prime needed, -1 for a non-blocking read that would
     * block, 0 for EOF, or the number of bytes read (which must be >= 0).
     */
{
    ssize_t                         nread, pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    if (sky__io_BufferedReader__primeNeeded(self)) {
        iomixin_data = sky__io__BufferedIOMixin_data(self);

        /* The stream must be seekable for this case to even be possible. */
        sky_error_validate_debug(iomixin_data->seekable);
        pos = sky__io__BufferedIOMixin__rawSeek(self, iomixin_data->raw_pos, 0);
        if (pos != iomixin_data->raw_pos) {
            iomixin_data->stable = 0;
            sky_error_raise_string(
                    sky_OSError,
                    "raw seek refuses to cooperate with buffered seek");
        }

        iomixin_data->buffer_end = 0;
        do {
            nread = sky__io_BufferedReader__rawReadInto(
                            self,
                            iomixin_data->buffer + iomixin_data->buffer_end,
                            iomixin_data->buffer_size - iomixin_data->buffer_end);
            if (nread <= 0) {   /* Read would block (-1) or EOF (0) */
                if (nread < 0) {
                    return -1;
                }
                return 0;
            }
            sky__io__BufferedIOMixin__markUsed(self,
                                               iomixin_data->buffer_end,
                                               nread);
        } while (sky__io_BufferedReader__primeNeeded(self));

        return iomixin_data->buffer_end;
    }

    return -2;
}


static ssize_t
sky__io_BufferedReader__readBuffer(
                sky_object_t    self,
                void *          bytes,
                ssize_t         nbytes)
{
    ssize_t                         navail, nread = 0;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    navail = iomixin_data->buffer_end - iomixin_data->buffer_pos;
    if (navail > 0 && nbytes > 0) {
        nread = SKY_MIN(navail, nbytes);
        memcpy(bytes, iomixin_data->buffer + iomixin_data->buffer_pos, nread);
        iomixin_data->buffer_pos += nread;
    }

    return nread;
}


static void
sky__io_BufferedReader__updateBuffer(
                sky_object_t    self,
                const void *    bytes,
                ssize_t         nbytes,
                ssize_t         pos)
{
    ssize_t                         offset;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    offset = pos + nbytes;
    if (offset >= iomixin_data->raw_pos + iomixin_data->buffer_size) {
        iomixin_data->raw_pos = (offset / iomixin_data->buffer_size) *
                                iomixin_data->buffer_size;
        iomixin_data->buffer_start = 0;
        iomixin_data->buffer_end = offset % iomixin_data->buffer_size;
        iomixin_data->buffer_pos = iomixin_data->buffer_end;
        memcpy(iomixin_data->buffer,
               bytes + nbytes - iomixin_data->buffer_end,
               iomixin_data->buffer_end);
    }
}


static sky_object_t
sky__io_BufferedReader_readall(sky_object_t self)
{
    char                            *memory;
    ssize_t                         nbytes;
    sky_list_t                      chunks;
    sky_bytes_t                     buffer_bytes, readall_bytes;
    sky_object_t                    chunk, readall, value;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);

    if ((nbytes = iomixin_data->buffer_end - iomixin_data->buffer_pos) <= 0) {
        buffer_bytes = sky_bytes_empty;
    }
    else {
        memory = sky_malloc(nbytes);
        buffer_bytes = sky_bytes_createwithbytes(memory, nbytes, sky_free);
        sky__io_BufferedReader__readBuffer(self, memory, nbytes);
    }

    if ((readall = sky_object_getattr(iomixin_data->raw,
                                      SKY_STRING_LITERAL("readall"),
                                      NULL)) != NULL)
    {
        readall_bytes = sky_object_call(readall, NULL, NULL);
        return sky_number_add(buffer_bytes, readall_bytes);
    }

    /* Since the raw stream does not implement readall(), we need to fake it
     * by repeatedly calling read() until EOF.
     */
    chunks = sky_list_create(NULL);
    if (sky_bytes_empty != buffer_bytes) {
        sky_list_append(chunks, buffer_bytes);
        for (;;) {
            SKY_ERROR_TRY {
                chunk = sky_file_read(iomixin_data->raw, -1);
            } SKY_ERROR_EXCEPT(sky_OSError) {
                value = sky_object_getattr(sky_error_current()->value,
                                           SKY_STRING_LITERAL("errno"),
                                           NULL);
                if (!sky_object_isa(value, sky_integer_type) ||
                    sky_integer_value(value, INT_MIN, INT_MAX, NULL) != EINTR)
                {
                    sky_error_raise();
                }
                chunk = NULL;
            } SKY_ERROR_TRY_END;
            if (!chunk) {   /* EINTR */
                continue;
            }
            if (sky_None == chunk) {
                if (!sky_list_len(chunks)) {
                    return sky_None;
                }
                break;
            }
            if (!sky_object_bool(chunk)) {
                if (!sky_list_len(chunks)) {
                    return sky_bytes_empty;
                }
                break;
            }
            sky_list_append(chunks, chunk);
        }
    }

    return sky_bytes_join(sky_bytes_empty, chunks);
}


static sky_object_t
sky__io_BufferedReader_read_unlocked(sky_object_t self, ssize_t n)
{
    char                            *end, *memory, *p;
    ssize_t                         nread, prime, start_pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    if (!n) {
        return sky_bytes_empty;
    }

    prime = sky__io_BufferedReader__primeBuffer(self);
    if (-1 == prime) {
        return sky_None;
    }
    if (!prime) {
        return sky_bytes_empty;
    }

    if (n < 0) {    /* readall */
        return sky__io_BufferedReader_readall(self);
    }

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    memory = sky_asset_malloc(n + 1, SKY_ASSET_CLEANUP_ON_ERROR);
    p = memory;
    end = p + n;

    nread = 0;
    while (p < end) {
        if (iomixin_data->buffer_end - iomixin_data->buffer_pos > 0) {
            nread = sky__io_BufferedReader__readBuffer(self, p, end - p);
            p += nread;
            continue;
        }
        start_pos = iomixin_data->raw_pos + iomixin_data->buffer_pos;

        iomixin_data->buffer_end = 0;
        iomixin_data->buffer_pos = 0;
        nread = sky__io_BufferedReader__rawReadInto(
                        self,
                        iomixin_data->buffer + iomixin_data->buffer_end,
                        iomixin_data->buffer_size - iomixin_data->buffer_end);
        if (nread < 0) {    /* Read would block (-1) */
            break;
        }
        sky__io__BufferedIOMixin__markUsed(self,
                                           iomixin_data->buffer_end,
                                           nread);
        iomixin_data->raw_pos = start_pos;
        if (!nread) {       /* EOF */
            break;
        }
    }

    if (nread < 0 && p == memory) {
        return sky_None;
    }

    *p = '\0';
    return sky_bytes_createwithbytes(memory, p - memory, sky_free);
}


static sky_object_t
sky__io_BufferedReader_read1_unlocked(sky_object_t self, ssize_t n)
{
    char                            *end, *memory, *p;
    ssize_t                         nread, pos, start_pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    if (n < 0) {
        sky_error_raise_string(sky_ValueError,
                               "number of bytes to read must be positive");
    }
    if (!n) {
        return sky_bytes_empty;
    }

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    start_pos = iomixin_data->raw_pos + iomixin_data->buffer_pos;
    memory = sky_asset_malloc(n + 1, SKY_ASSET_CLEANUP_ON_ERROR);
    p = memory;
    end = p + n;

    /* A prime is needed if the buffer is partial and the current position in
     * the buffer falls outside of the filled range of the buffer, either at
     * the start or the end. The normal primeBuffer() function will do a read
     * to fill the buffer as much as possible, but we want to perform only a
     * single read, ever, so we can't use the normal priming method. Instead,
     * seek to the raw position plus the buffer position and read directly into
     * allocated memory as much as possible. Update the buffer afterwards to
     * keep it in sync.
     */
    if (sky__io_BufferedReader__primeNeeded(self)) {
        /* The stream must be seekable for this case to even be possible. */
        sky_error_validate_debug(iomixin_data->seekable);
        pos = sky__io__BufferedIOMixin__rawSeek(self, start_pos, 0);
        if (pos != start_pos) {
            iomixin_data->stable = 0;
            sky_error_raise_string(
                    sky_OSError,
                    "raw seek refuses to cooperate with buffered seek");
        }
    }
    else {
        /* Get as much out of the buffer as is possible. */
        nread = sky__io_BufferedReader__readBuffer(self, p, end - p);
        if (nread == n) {
            memory[nread] = 0;
            return sky_bytes_createwithbytes(memory, nread, sky_free);
        }
        p += nread;
        n -= nread;
    }

    nread = sky__io_BufferedReader__rawReadInto(self, p, n);
    if (nread > 0) {
        p += nread;
    }
    else if (p == memory) {
        sky_free(memory);
        if (nread < 0) {
            return sky_None;
        }
        return sky_bytes_empty;
    }

    sky__io_BufferedReader__updateBuffer(self, memory, p - memory, start_pos);

    *p = 0;
    return sky_bytes_createwithbytes(memory, p - memory, sky_free);
}


static ssize_t
sky__io_BufferedReader_readinto_native_unlocked(
                sky_object_t    self,
                void *          bytes,
                ssize_t         nbytes)
{
    char                            *end, *p;
    ssize_t                         nread, ntoread, prime, start_pos;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);

    /* How much to read is nbytes. Read that much unless the underlying raw
     * stream reports EOF or that the read would block in non-blocking mode.
     */
    p = bytes;
    end = p + nbytes;

    /* Prime the buffer if necessary. */
    prime = sky__io_BufferedReader__primeBuffer(self);
    if (-1 == prime) {  /* Read would block */
        return -1;
    }
    if (!prime) {       /* EOF */
        return 0;
    }

    /* Get as much out of the buffer as is possible. */
    start_pos = iomixin_data->raw_pos + iomixin_data->buffer_pos;
    nread = sky__io_BufferedReader__readBuffer(self, p, end - p);
    if (nread == nbytes) {
        return nread;
    }
    p += nread;

    /* While there's data to be read that's larger than the buffer, read it
     * directly into the buffer.
     */
    ntoread = (end - p) / iomixin_data->buffer_size * iomixin_data->buffer_size;
    while (ntoread > 0) {
        nread = sky__io_BufferedReader__rawReadInto(self, p, ntoread);
        if (nread <= 0) {   /* Read would block (-1) or EOF (0) */
            sky__io_BufferedReader__updateBuffer(self,
                                                 bytes,
                                                 end - p,
                                                 start_pos);
            if (nread < 0 && !(p - (char *)bytes)) {
                return -1;
            }
            return p - (char *)bytes;
        }
        iomixin_data->buffer_end = 0;
        iomixin_data->raw_pos += nread;
        ntoread -= nread;
        p += nread;
    }

    /* Read the final chunk into the buffer and make a copy in the output
     * buffer too.
     */
    if (p < end) {
        iomixin_data->buffer_end = 0;
        iomixin_data->buffer_pos = 0;
        nread = sky__io_BufferedReader__rawReadInto(
                        self,
                        iomixin_data->buffer,
                        iomixin_data->buffer_size);
        if (nread <= 0) {   /* Read would block (-1) or EOF (0) */
            if (nread < 0 && !(p - (char *)bytes)) {
                return -1;
            }
            return p - (char *)bytes;
        }
        sky__io__BufferedIOMixin__markUsed(self, 0, nread);

        nread = sky__io_BufferedReader__readBuffer(self, p, end - p);
        p += nread;
    }

    /* Return the number of bytes actually read. */
    return p - (char *)bytes;
}


static sky_object_t
sky__io_BufferedReader_readinto_unlocked(sky_object_t self, sky_object_t b)
{
    ssize_t         nread;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, b, SKY_BUFFER_FLAG_SIMPLE |
                                       SKY_BUFFER_FLAG_WRITABLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        nread = sky__io_BufferedReader_readinto_native_unlocked(
                        self,
                        buffer.buf,
                        buffer.len);
    } SKY_ASSET_BLOCK_END;

    if (nread < 0) {
        return sky_None;
    }
    return sky_integer_create(nread);
}


static void
sky__io_BufferedReader_init(
                sky_object_t    self,
                sky_object_t    raw,
                ssize_t         buffer_size)
{
    if (!sky_file_readable(raw)) {
        sky_error_raise_string(sky_OSError,
                               "'raw' argument must be readable.");
    }

    sky__io__BufferedIOMixin_init(self, raw, buffer_size);
}


static const char sky__io_BufferedReader_peek_doc[] =
"Returns buffered bytes without advancing the position.\n"
"\n"
"The argument indicates a desired minimal number of bytes; we\n"
"do at most one raw read to satisfy it.  We never return more\n"
"than self.buffer_size.\n";

static sky_object_t
sky__io_BufferedReader_peek_unlocked(sky_object_t self)
{
    char                            *memory;
    ssize_t                         navail, nread;
    sky_bytes_t                     bytes;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (!(navail = iomixin_data->buffer_end - iomixin_data->buffer_pos)) {
        if (sky__io_BufferedReader__primeBuffer(self) == -2) {
            /* If there's no prime needed, the buffer is empty. Reset the
             * buffer and fill it.
             */
            iomixin_data->buffer_end = 0;
            iomixin_data->buffer_pos = 0;
            nread = sky__io_BufferedReader__rawReadInto(
                            self,
                            iomixin_data->buffer + iomixin_data->buffer_end,
                            iomixin_data->buffer_size - iomixin_data->buffer_end);
            if (nread <= 0) {   /* Read would block (-1) or EOF (0) */
                return sky_bytes_empty;
            }
            sky__io__BufferedIOMixin__markUsed(self,
                                               iomixin_data->buffer_end,
                                               nread);
        }
        if (!(navail = iomixin_data->buffer_end - iomixin_data->buffer_pos)) {
            return sky_bytes_empty;
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        memory = sky_asset_malloc(navail + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        memcpy(memory, iomixin_data->buffer + iomixin_data->buffer_pos, navail);
        memory[navail] = 0;

        bytes = sky_bytes_createwithbytes(memory, navail, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_object_t
sky__io_BufferedReader_peek(sky_object_t self, SKY_UNUSED ssize_t n)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        result = sky__io_BufferedReader_peek_unlocked(self);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io_BufferedReader_read_doc[] =
"Read n bytes.\n"
"\n"
"Returns exactly n bytes of data unless the underlying raw IO\n"
"stream reaches EOF or if the call would block in non-blocking\n"
"mode. If n is negative, read until EOF or until read() would\n"
"block.\n";

static sky_object_t
sky__io_BufferedReader_read(sky_object_t self, sky_object_t n)
{
    ssize_t         nbytes;
    sky_object_t    result;

    if (sky_object_isnull(n)) {
        nbytes = -1;
    }
    else {
        nbytes = sky_integer_value(n, SSIZE_MIN, SSIZE_MAX, NULL);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        result = sky__io_BufferedReader_read_unlocked(self, nbytes);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io_BufferedReader_read1_doc[] =
"Reads up to n bytes, with at most one read() system call.";

static sky_object_t
sky__io_BufferedReader_read1(sky_object_t self, ssize_t n)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        result = sky__io_BufferedReader_read1_unlocked(self, n);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky__io_BufferedReader_readinto_doc[] =
"Read up to len(b) bytes directly into b, where b is any object that\n"
"supports the writable buffer protocol. The number of bytes read will\n"
"be returned, or None if no data was available for a non-blocking raw\n"
"stream. A return of 0 indicates end of file.";

static sky_object_t
sky__io_BufferedReader_readinto(sky_object_t self, sky_object_t b)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        result = sky__io_BufferedReader_readinto_unlocked(self, b);
    } SKY_ASSET_BLOCK_END;

    return result;
}

ssize_t
sky__io_BufferedReader_readinto_native(
                sky_object_t    self,
                void *          bytes,
                ssize_t         nbytes)
{
    ssize_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        result = sky__io_BufferedReader_readinto_native_unlocked(self,
                                                                 bytes,
                                                                 nbytes);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_type_t sky__io_BufferedReader_type = NULL;

sky_type_t
sky__io_BufferedReader_initialize(void)
{
    const char type_doc[] =
"BufferedReader(raw[, buffer_size])\n"
"\n"
"A buffer for a readable, sequential BaseRawIO object.\n"
"\n"
"The constructor creates a BufferedReader for the given readable raw\n"
"stream and buffer_size. If buffer_size is omitted, DEFAULT_BUFFER_SIZE\n"
"is used.\n";

    sky_type_t  type;

    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("BufferedReader"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__io__BufferedIOMixin_type),
                    NULL);

    sky_type_setattr_builtin(type,
            "peek",
            sky_function_createbuiltin(
                    "BufferedReader.peek",
                    sky__io_BufferedReader_peek_doc,
                    (sky_native_code_function_t)sky__io_BufferedReader_peek,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "BufferedReader.read",
                    sky__io_BufferedReader_read_doc,
                    (sky_native_code_function_t)sky__io_BufferedReader_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "read1",
            sky_function_createbuiltin(
                    "BufferedReader.read1",
                    sky__io_BufferedReader_read1_doc,
                    (sky_native_code_function_t)sky__io_BufferedReader_read1,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "readall",
            sky_function_createbuiltin(
                    "BufferedReader.readall",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedReader_readall,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readinto",
            sky_function_createbuiltin(
                    "BufferedReader.readinto",
                    sky__io_BufferedReader_readinto_doc,
                    (sky_native_code_function_t)sky__io_BufferedReader_readinto,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_BufferedReader_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "raw", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer_size", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_BufferedReader_type),
            type,
            SKY_TRUE);
    return sky__io_BufferedReader_type;
}


static ssize_t
sky__io_BufferedWriter__rawWrite(
                sky_object_t    self,
                const void *    bytes,
                ssize_t         nbytes)
    /* Write the requested data by calling the raw object's write() method,
     * which may return less than the requested amount. This function does
     * NOT update any data in the _IOMixin data structure. The number of bytes
     * written (as reported by raw.write()) will be returned, or -1 if the
     * write could not complete without blocking.
     */
{
    sky_object_t    raw = sky__io__BufferedIOMixin_raw(self);
    sky_type_t      raw_type = sky_object_type(raw);

    ssize_t                     nwritten;
    sky_buffer_t                *buffer;
    sky_object_t                value;
    sky_memoryview_t volatile   view;
    sky_object_t                result;

    if (sky__io_FileIO_type == raw_type) {
        return sky__io_FileIO_write_native(raw, bytes, nbytes);
    }
    if (sky__io_BytesIO_type == raw_type) {
        return sky__io_BytesIO_write_native(raw, bytes, nbytes);
    }
    if (sky__io_StringIO_type == raw_type) {
        return sky__io_StringIO_write_native(raw, bytes, nbytes);
    }

    SKY_ASSET_BLOCK_BEGIN {
        SKY_ASSET_BLOCK_BEGIN {
            buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                      SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_initialize(buffer,
                                  (void *)bytes,
                                  nbytes,
                                  SKY_TRUE,
                                  SKY_BUFFER_FLAG_CONTIG_RO);
            view = sky_memoryview_createwithbuffer(buffer);
        } SKY_ASSET_BLOCK_END;
        sky_asset_save(view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        do {
            SKY_ERROR_TRY {
                result = sky__io__BufferedIOMixin__forward(
                                self,
                                SKY_STRING_LITERAL("write"),
                                sky_tuple_pack(1, view),
                                NULL);
            } SKY_ERROR_EXCEPT(sky_OSError) {
                value = sky_object_getattr(sky_error_current()->value,
                                           SKY_STRING_LITERAL("errno"),
                                           NULL);
                if (!sky_object_isa(value, sky_integer_type) ||
                    sky_integer_value(value, INT_MIN, INT_MAX, NULL) != EINTR)
                {
                    sky_error_raise();
                }
                result = NULL;
            } SKY_ERROR_TRY_END;
        } while (!result);

        if (sky_None == result) {
            /* The underlying raw stream is non-blocking, and the write
             * could not be completed without blocking.
             */
            nwritten = -1;
        }
        else if (result) {
            nwritten = sky_integer_value(result, SSIZE_MIN, SSIZE_MAX, NULL);
            if (nwritten < 0 || nwritten > nbytes) {
                sky_error_raise_format(
                        sky_ValueError,
                        "raw write() returned invalid length %zd "
                        "(should have been between 0 and %zd)",
                        nwritten,
                        nbytes);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return nwritten;
}


static void
sky__io_BufferedWriter_init(
                sky_object_t    self,
                sky_object_t    raw,
                ssize_t         buffer_size)
{
    if (!sky_file_writable(raw)) {
        sky_error_raise_string(sky_OSError,
                               "'raw' argument must be writable.");
    }

    sky__io__BufferedIOMixin_init(self, raw, buffer_size);
}


static void
sky__io_BufferedWriter_flush_unlocked(sky_object_t self)
{
    ssize_t                         n, nwritten;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    sky__io__BufferedIOMixin__checkClosed(self);

    iomixin_data = sky__io__BufferedIOMixin_data(self);
    if (!iomixin_data->dirty) {
        return;
    }
    sky_error_validate_debug(iomixin_data->buffer_end > 0);
    sky_error_validate_debug(iomixin_data->stable);

    if (iomixin_data->seekable) {
        sky__io__BufferedIOMixin__rawSeek(self,
                                          iomixin_data->raw_pos +
                                          iomixin_data->dirty_start,
                                          0);
    }

    nwritten = 0;
    while (iomixin_data->dirty_end > iomixin_data->dirty_start) {
        n = sky__io_BufferedWriter__rawWrite(
                    self,
                    iomixin_data->buffer + iomixin_data->dirty_start,
                    iomixin_data->dirty_end - iomixin_data->dirty_start);
        if (n < 0) {
            sky__io__BufferedIOMixin__BlockingIOError(
                    EAGAIN,
                    "write could not complete without blocking",
                    nwritten);
        }
        iomixin_data->dirty_start += n;
        nwritten += n;
    }
    iomixin_data->dirty = 0;
}


static void
sky__io_BufferedWriter_flush(sky_object_t self)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        sky__io_BufferedWriter_flush_unlocked(self);
    } SKY_ASSET_BLOCK_END;
}


static void
sky__io_BufferedWriter_truncate(sky_object_t self, sky_object_t pos)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky__io__BufferedIOMixin__lock(self);

        sky__io_BufferedWriter_flush_unlocked(self);
        sky__io__BufferedIOMixin__forward(self,
                                          SKY_STRING_LITERAL("truncate"),
                                          sky_tuple_pack(1, pos),
                                          NULL);

        sky__io__BufferedIOMixin__sync(
                self,
                sky__io__BufferedIOMixin__rawSeek(self, 0, 1));
    } SKY_ASSET_BLOCK_END;
}


static ssize_t
sky__io_BufferedWriter_write_unlocked(
                sky_object_t    self,
                const char *    bytes,
                ssize_t         nbytes)
{
    ssize_t                         length, nwritten = 0;
    sky__io__BufferedIOMixin_data_t *iomixin_data;

    sky__io__BufferedIOMixin__checkClosed(self);
    iomixin_data = sky__io__BufferedIOMixin_data(self);

    /* If the data to write fits entirely within the buffer, simply drop it in
     * and we're done.
     */
    if (iomixin_data->buffer_pos + nbytes <= iomixin_data->buffer_size) {
        memcpy(iomixin_data->buffer + iomixin_data->buffer_pos, bytes, nbytes);
        sky__io__BufferedIOMixin__markDirty(self,
                                            iomixin_data->buffer_pos,
                                            nbytes);
        iomixin_data->buffer_pos += nbytes;
        return nbytes;
    }

    /* Write as much data as will fit into the buffer and flush it. */
    if (iomixin_data->buffer_pos > 0) {
        length = iomixin_data->buffer_size - iomixin_data->buffer_pos;
        memcpy(iomixin_data->buffer + iomixin_data->buffer_pos, bytes, length);
        sky__io__BufferedIOMixin__markDirty(self,
                                            iomixin_data->buffer_pos,
                                            length);
        sky__io_BufferedWriter_flush_unlocked(self);
        bytes += length;
        nbytes -= length;
        nwritten += length;
        iomixin_data->raw_pos += iomixin_data->buffer_size;
    }

    /* For as long as there's enough data left to write that will fill a whole
     * buffer, directly flush that data.
     */
    if (nbytes >= iomixin_data->buffer_size) {
        length = (nbytes / iomixin_data->buffer_size) *
                 iomixin_data->buffer_size;
        sky__io_BufferedWriter__rawWrite(self, bytes, length);
        bytes += length;
        nbytes -= length;
        nwritten += length;
        iomixin_data->raw_pos += length;
    }

    iomixin_data->dirty = 0;
    iomixin_data->buffer_pos = 0;
    iomixin_data->buffer_start = iomixin_data->buffer_end = 0;

    /* If there's any data left, put it in the buffer and mark it dirty. */
    if (nbytes > 0) {
        memcpy(iomixin_data->buffer, bytes, nbytes);
        sky__io__BufferedIOMixin__markDirty(self, 0, nbytes);
        iomixin_data->buffer_pos += nbytes;
        nwritten += length;
    }

    return nwritten;
}


static ssize_t
sky__io_BufferedWriter_write(sky_object_t self, sky_object_t b)
{
    ssize_t         nwritten;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, b, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky__io__BufferedIOMixin__lock(self);
        sky__io__BufferedIOMixin__checkClosed(self);
        sky__io__BufferedIOMixin__checkDetached(self);
        sky__io__BufferedIOMixin__checkStable(self);

        nwritten = sky__io_BufferedWriter_write_unlocked(self,
                                                         buffer.buf,
                                                         buffer.len);
    } SKY_ASSET_BLOCK_END;

    return nwritten;
}


sky_type_t sky__io_BufferedWriter_type = NULL;

sky_type_t
sky__io_BufferedWriter_initialize(void)
{
    const char type_doc[] =
"A buffer for a writeable sequential RawIO object.\n"
"\n"
"The constructor creates a BufferedWriter for the given writeable raw\n"
"stream. If the buffer_size is not given, it defaults to\n"
"DEFAULT_BUFFER_SIZE.\n";

    sky_type_t  type;

    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("BufferedWriter"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(1, sky__io__BufferedIOMixin_type),
                    NULL);

    sky_type_setattr_builtin(type,
            "flush",
            sky_function_createbuiltin(
                    "BufferedWriter.flush",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedWriter_flush,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "BufferedWriter.truncate",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedWriter_truncate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "BufferedWriter.write",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedWriter_write,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_BufferedWriter_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "raw", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer_size", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_BufferedWriter_type),
            type,
            SKY_TRUE);
    return sky__io_BufferedWriter_type;
}


static void
sky__io_BufferedRWPair_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__io_BufferedRWPair_data_t   *rwpair_data = data;

    sky_object_visit(rwpair_data->reader, visit_data);
    sky_object_visit(rwpair_data->writer, visit_data);
}


static void
sky__io_BufferedRWPair_close(sky_object_t self)
{
    sky__io_BufferedRWPair_data_t   *rwpair_data;

    rwpair_data = sky__io_BufferedRWPair_data(self);
    sky_file_close(rwpair_data->writer);
    sky_file_close(rwpair_data->reader);
}


static sky_object_t
sky__io_BufferedRWPair_closed_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_object_t    writer = sky__io_BufferedRWPair_writer(self);

    return (sky_file_closed(writer) ? sky_True : sky_False);
}


static void
sky__io_BufferedRWPair_flush(sky_object_t self)
{
    sky_file_flush(sky__io_BufferedRWPair_writer(self));
}


static void
sky__io_BufferedRWPair_init(
                sky_object_t    self,
                sky_object_t    reader,
                sky_object_t    writer,
                ssize_t         buffer_size)
{
    sky_object_t                    pair_reader, pair_writer;
    sky__io_BufferedRWPair_data_t   *rwpair_data;

    if (!sky_file_readable(reader)) {
        sky_error_raise_string(sky_OSError,
                               "'reader' argument must be readable.");
    }

    if (!sky_file_writable(writer)) {
        sky_error_raise_string(sky_OSError,
                               "'writer' argument must be writable.");
    }

    pair_reader = sky_object_allocate(sky__io_BufferedReader_type);
    sky__io_BufferedReader_init(pair_reader, reader, buffer_size);

    pair_writer = sky_object_allocate(sky__io_BufferedWriter_type);
    sky__io_BufferedWriter_init(pair_writer, writer, buffer_size);

    rwpair_data = sky__io_BufferedRWPair_data(self);
    sky_object_gc_set(&(rwpair_data->reader), pair_reader, self);
    sky_object_gc_set(&(rwpair_data->writer), pair_writer, self);
}


static sky_object_t
sky__io_BufferedRWPair_isatty(sky_object_t self)
{
    sky__io_BufferedRWPair_data_t   *rwpair_data;

    rwpair_data = sky__io_BufferedRWPair_data(self);
    if (sky_file_isatty(rwpair_data->reader)) {
        return sky_True;
    }
    return (sky_file_isatty(rwpair_data->writer) ? sky_True : sky_False);
}


static sky_object_t
sky__io_BufferedRWPair_peek(sky_object_t self, sky_object_t n)
{
    return sky_object_callmethod(sky__io_BufferedRWPair_reader(self),
                                 SKY_STRING_LITERAL("peek"),
                                 sky_tuple_pack(1, n),
                                 NULL);
}


static sky_object_t
sky__io_BufferedRWPair_read(sky_object_t self, sky_object_t n)
{
    if (sky_object_isnull(n)) {
        n = sky_integer_create(-1);
    }
    return sky_object_callmethod(sky__io_BufferedRWPair_reader(self),
                                 SKY_STRING_LITERAL("read"),
                                 sky_tuple_pack(1, n),
                                 NULL);
}


static sky_object_t
sky__io_BufferedRWPair_read1(sky_object_t self, sky_object_t n)
{
    return sky_object_callmethod(sky__io_BufferedRWPair_reader(self),
                                 SKY_STRING_LITERAL("read1"),
                                 sky_tuple_pack(1, n),
                                 NULL);
}


static sky_object_t
sky__io_BufferedRWPair_readable(sky_object_t self)
{
    sky_object_t    reader = sky__io_BufferedRWPair_reader(self);

    return (sky_file_readable(reader) ? sky_True : sky_False);
}


static sky_object_t
sky__io_BufferedRWPair_readinto(sky_object_t self, sky_object_t b)
{
    return sky_object_callmethod(sky__io_BufferedRWPair_reader(self),
                                 SKY_STRING_LITERAL("readinto"),
                                 sky_tuple_pack(1, b),
                                 NULL);
}


static sky_object_t
sky__io_BufferedRWPair_writable(sky_object_t self)
{
    sky_object_t    writer = sky__io_BufferedRWPair_writer(self);

    return (sky_file_writable(writer) ? sky_True : sky_False);
}


static sky_object_t
sky__io_BufferedRWPair_write(sky_object_t self, sky_object_t b)
{
    sky_object_t    writer = sky__io_BufferedRWPair_writer(self);

    return sky_integer_create(sky_file_write(writer, b));
}


sky_type_t sky__io_BufferedRWPair_type = NULL;

sky_type_t
sky__io_BufferedRWPair_initialize(void)
{
    const char type_doc[] =
"A buffered reader and writer object together.\n"
"\n"
"A buffered reader object and buffered writer object put together to\n"
"form a sequential IO object that can read and write. This is typically\n"
"used with a socket or two-way pipe.\n"
"\n"
"reader and writer are RawIOBase objects that are readable and\n"
"writeable respectively. If the buffer_size is omitted it defaults to\n"
"DEFAULT_BUFFER_SIZE.\n";

    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky__io_BufferedRWPair_instance_visit;
    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("BufferedRWPair"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    sizeof(sky__io_BufferedRWPair_data_t),
                    0,
                    sky_tuple_pack(1, sky__io__BufferedIOBase_type),
                    &template);

    sky_type_setattr_getset(type,
            "closed",
            NULL,
            sky__io_BufferedRWPair_closed_getter,
            NULL);

    sky_type_setattr_builtin(type,
            "close",
            sky_function_createbuiltin(
                    "BufferedRWPair.close",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "flush",
            sky_function_createbuiltin(
                    "BufferedRWPair.flush",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_flush,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "isatty",
            sky_function_createbuiltin(
                    "BufferedRWPair.isatty",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_isatty,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "peek",
            sky_function_createbuiltin(
                    "BufferedRWPair.peek",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_peek,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "BufferedRWPair.read",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "read1",
            sky_function_createbuiltin(
                    "BufferedRWPair.read1",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_read1,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "readable",
            sky_function_createbuiltin(
                    "BufferedRWPair.readable",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_readable,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "readinto",
            sky_function_createbuiltin(
                    "BufferedRWPair.readinto",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_readinto,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "writable",
            sky_function_createbuiltin(
                    "BufferedRWPair.writable",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_writable,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(type,
            "write",
            sky_function_createbuiltin(
                    "BufferedRWPair.write",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRWPair_write,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_BufferedRWPair_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "reader", SKY_DATA_TYPE_OBJECT, NULL,
            "writer", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer_size", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SKY__IO_DEFAULT_BUFFER_SIZE),
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_BufferedRWPair_type),
            type,
            SKY_TRUE);
    return sky__io_BufferedRWPair_type;
}


static void
sky__io_BufferedRandom_init(sky_object_t    self,
                            sky_object_t    raw,
                            ssize_t         buffer_size)
{
    if (!sky_file_seekable(raw)) {
        sky_error_raise_string(sky_OSError,
                               "'raw' argument must be seekable.");
    }
    if (!sky_file_readable(raw)) {
        sky_error_raise_string(sky_OSError,
                               "'raw' argument must be readable.");
    }
    if (!sky_file_writable(raw)) {
        sky_error_raise_string(sky_OSError,
                               "'raw' argument must be writable.");
    }

    sky__io_BufferedReader_init(self, raw, buffer_size);
    sky__io_BufferedWriter_init(self, raw, buffer_size);
}


static sky_object_t
sky__io_BufferedRandom_peek(sky_object_t self, ssize_t n)
{
    return sky__io_BufferedReader_peek(self, n);
}


static sky_object_t
sky__io_BufferedRandom_read(sky_object_t self, sky_object_t n)
{
    sky_file_flush(self);
    return sky__io_BufferedReader_read(self, n);
}


static sky_object_t
sky__io_BufferedRandom_read1(sky_object_t self, ssize_t n)
{
    sky_file_flush(self);
    return sky__io_BufferedReader_read1(self, n);
}


static sky_object_t
sky__io_BufferedRandom_readinto(sky_object_t self, sky_object_t b)
{
    sky_file_flush(self);
    return sky__io_BufferedReader_readinto(self, b);
}


static void
sky__io_BufferedRandom_truncate(sky_object_t self, sky_object_t pos)
{
    if (sky_object_isnull(pos)) {
        pos = sky_object_callmethod(self,
                                    SKY_STRING_LITERAL("tell"),
                                    NULL,
                                    NULL);
    }
    sky__io_BufferedWriter_truncate(self, pos);
}


sky_type_t sky__io_BufferedRandom_type = NULL;

sky_type_t
sky__io_BufferedRandom_initialize(void)
{
    const char type_doc[] =
"A buffered interface to random access streams.\n"
"\n"
"The constructor creates a reader and writer for a seekable stream,\n"
"raw, given in the first argument. If the buffer_size is omitted it\n"
"defaults to DEFAULT_BUFFER_SIZE.\n";

    sky_type_t  type;

    type = sky_type_createbuiltin(
                    SKY_STRING_LITERAL("BufferedRandom"),
                    type_doc,
                    SKY_TYPE_CREATE_FLAG_HAS_DICT,
                    0,
                    0,
                    sky_tuple_pack(2, sky__io_BufferedWriter_type,
                                      sky__io_BufferedReader_type),
                    NULL);

    sky_type_setattr_builtin(type,
            "peek",
            sky_function_createbuiltin(
                    "BufferedRandom.peek",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRandom_peek,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(type,
            "read",
            sky_function_createbuiltin(
                    "BufferedRandom.read",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRandom_read,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(type,
            "read1",
            sky_function_createbuiltin(
                    "BufferedRandom.read1",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRandom_read1,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "n", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "readinto",
            sky_function_createbuiltin(
                    "BufferedRandom.readinto",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRandom_readinto,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "b", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(type,
            "truncate",
            sky_function_createbuiltin(
                    "BufferedRandom.truncate",
                    NULL,
                    (sky_native_code_function_t)sky__io_BufferedRandom_truncate,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "pos", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

    sky_type_setmethodslotwithparameters(
            type,
            "__init__",
            (sky_native_code_function_t)sky__io_BufferedRandom_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "raw", SKY_DATA_TYPE_OBJECT, NULL,
            "buffer_size", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SKY__IO_DEFAULT_BUFFER_SIZE),
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__io_BufferedRandom_type),
            type,
            SKY_TRUE);
    return sky__io_BufferedRandom_type;
}
