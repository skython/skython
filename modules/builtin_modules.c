#include "builtin_modules.h"

#include "../core/sky_module_importlib.h"

sky_builtin_module_t sky_builtin_modules[] = {
    { "builtins",       skython_module_builtins_initialize, NULL, 0 },
    { "sys",            skython_module_sys_initialize, NULL, 0 },

    { "_ast",           skython_module__ast_initialize, NULL, 0 },
    { "_codecs",        skython_module__codecs_initialize, NULL, 0 },
    { "_collections",   skython_module__collections_initialize, NULL, 0 },
    { "_functools",     skython_module__functools_initialize, NULL, 0 },
    { "_imp",           skython_module__imp_initialize, NULL, 0 },
    { "_io",            skython_module__io_initialize, NULL, 0 },
    { "_locale",        skython_module__locale_initialize, NULL, 0 },
    { "_sre",           skython_module__sre_initialize, NULL, 0 },
    { "_string",        skython_module__string_initialize, NULL, 0 },
    { "_thread",        skython_module__thread_initialize, NULL, 0 },
    { "_warnings",      skython_module__warnings_initialize, NULL, 0 },
    { "_weakref",       skython_module__weakref_initialize, NULL, 0 },

    { "errno",          skython_module_errno_initialize, NULL, 0 },
    { "faulthandler",   skython_module_faulthandler_initialize, NULL, 0 },
    { "gc",             skython_module_gc_initialize, NULL, 0 },
    { "itertools",      skython_module_itertools_initialize, NULL, 0 },
    { "marshal",        skython_module_marshal_initialize, NULL, 0 },
    { "operator",       skython_module_operator_initialize, NULL, 0 },
    { "posix",          skython_module_posix_initialize, NULL, 0 },
#if defined(HAVE_PWD_H)
    { "pwd",            skython_module_pwd_initialize, NULL, 0 },
#endif
#if defined(HAVE_READLINE_H)
    { "readline",       skython_module_readline_initialize, NULL, 0 },
#endif
    { "resource",       skython_module_resource_initialize, NULL, 0 },
    { "signal",         skython_module_signal_initialize, NULL, 0 },

    { "_frozen_importlib",  NULL,
                            sky_modules_builtin_frozen_importlib_bytes,
                            sky_modules_builtin_frozen_importlib_nbytes },

    { NULL, NULL, NULL, 0 },
};
