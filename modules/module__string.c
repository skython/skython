#include "../core/skython.h"


void
skython_module__string_initialize(sky_module_t module)
{
    static const char doc[] = "string helper module";

    sky_module_setattr(
            module,
            "__doc__",
            sky_string_createfrombytes(doc, sizeof(doc) - 1, NULL, NULL));

#if 0
    sky_module_setattr(
            module,
            "formatter_parser",
            sky_function_createbuiltin(
                    "formatter_parser",
                    "parse the argument as a format string",
                    (sky_native_code_function_t)sky__string_formatter_parser,
                    SKY_DATA_TYPE_OBJECT,
                    ...,
                    NULL));
    sky_module_setattr(
            module,
            "formatter_field_name_split",
            sky_function_createbuiltin(
                    "formatter_field_name_split",
                    "split the argument as a field name",
                    (sky_native_code_function_t)sky__string_formatter_field_name_split,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
#endif
}
