#include "../core/skython.h"

#include "sha2.h"


SKY_MODULE_EXTERN void
skython_module__sha256_initialize(sky_module_t module);


static sky_type_t sky__sha224_sha224_type;
static sky_type_t sky__sha256_sha256_type;

SKY_EXTERN_INLINE SHA224_CTX *
sky__sha224_sha224_data(sky_object_t object)
{
    return sky_object_data(object, sky__sha224_sha224_type);
}

SKY_EXTERN_INLINE SHA256_CTX *
sky__sha256_sha256_data(sky_object_t object)
{
    return sky_object_data(object, sky__sha256_sha256_type);
}


static void
sky__sha224_sha224_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    SHA224_CTX  *self_data = data;

    SHA224_Init(self_data);
}

static void
sky__sha256_sha256_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    SHA256_CTX  *self_data = data;

    SHA256_Init(self_data);
}


static sky_object_t
sky__sha224_sha224_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA224_BLOCK_LENGTH);
}

static sky_object_t
sky__sha256_sha256_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA256_BLOCK_LENGTH);
}

static sky_object_t
sky__sha224_sha224_copy(sky_object_t self)
{
    sky_object_t    new_sha224;

    new_sha224 = sky_object_allocate(sky__sha224_sha224_type);
    memcpy(sky__sha224_sha224_data(new_sha224),
           sky__sha224_sha224_data(self),
           sizeof(SHA224_CTX));

    return new_sha224;
}

static sky_object_t
sky__sha256_sha256_copy(sky_object_t self)
{
    sky_object_t    new_sha256;

    new_sha256 = sky_object_allocate(sky__sha256_sha256_type);
    memcpy(sky__sha256_sha256_data(new_sha256),
           sky__sha256_sha256_data(self),
           sizeof(SHA256_CTX));

    return new_sha256;
}

static sky_bytes_t
sky__sha224_sha224_digest(sky_object_t self)
{
    SHA224_CTX  *self_data = sky__sha224_sha224_data(self);

    uint8_t     *raw_bytes;
    SHA224_CTX  final_data;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(SHA224_CTX));

        raw_bytes = sky_asset_malloc(SHA224_DIGEST_LENGTH,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        SHA224_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SHA224_DIGEST_LENGTH,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_bytes_t
sky__sha256_sha256_digest(sky_object_t self)
{
    SHA256_CTX  *self_data = sky__sha256_sha256_data(self);

    uint8_t     *raw_bytes;
    SHA256_CTX  final_data;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(SHA256_CTX));

        raw_bytes = sky_asset_malloc(SHA256_DIGEST_LENGTH,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        SHA256_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SHA256_DIGEST_LENGTH,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_object_t
sky__sha224_sha224_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA224_DIGEST_LENGTH);
}

static sky_object_t
sky__sha256_sha256_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SHA256_DIGEST_LENGTH);
}

static sky_string_t
sky__sha224_sha224_hexdigest(sky_object_t self)
{
    SHA224_CTX  *self_data = sky__sha224_sha224_data(self);

    char        hexdigest[SHA224_DIGEST_LENGTH * 2], *p;
    size_t      i;
    uint8_t     digest[SHA224_DIGEST_LENGTH];
    const char  *hexdigits;
    SHA224_CTX  final_data;

    memcpy(&final_data, self_data, sizeof(SHA224_CTX));
    SHA224_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_string_t
sky__sha256_sha256_hexdigest(sky_object_t self)
{
    SHA256_CTX  *self_data = sky__sha256_sha256_data(self);

    char        hexdigest[SHA256_DIGEST_LENGTH * 2], *p;
    size_t      i;
    uint8_t     digest[SHA256_DIGEST_LENGTH];
    const char  *hexdigits;
    SHA256_CTX  final_data;

    memcpy(&final_data, self_data, sizeof(SHA256_CTX));
    SHA256_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_object_t
sky__sha224_sha224_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("SHA224");
}

static sky_object_t
sky__sha256_sha256_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("SHA256");
}

static void
sky__sha224_sha224_update(sky_object_t self, sky_object_t string)
{
    SHA224_CTX  *self_data = sky__sha224_sha224_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        SHA224_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}

static void
sky__sha256_sha256_update(sky_object_t self, sky_object_t string)
{
    SHA256_CTX  *self_data = sky__sha256_sha256_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        SHA256_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t
sky__sha224_sha224(sky_object_t string)
{
    sky_object_t    sha224;

    sha224 = sky_object_allocate(sky__sha224_sha224_type);
    if (!sky_object_isnull(string)) {
        sky__sha224_sha224_update(sha224, string);
    }

    return sha224;
}

static sky_object_t
sky__sha256_sha256(sky_object_t string)
{
    sky_object_t    sha256;

    sha256 = sky_object_allocate(sky__sha256_sha256_type);
    if (!sky_object_isnull(string)) {
        sky__sha256_sha256_update(sha256, string);
    }

    return sha256;
}


void
skython_module__sha256_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__sha224_sha224_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("sha224"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(SHA224_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__sha224_sha224_block_size_getter, NULL,
            "digest_size",  NULL, sky__sha224_sha224_digest_size_getter, NULL,
            "name",         NULL, sky__sha224_sha224_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "sha224.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__sha224_sha224_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "sha224.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__sha224_sha224_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "sha224.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__sha224_sha224_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "sha224.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__sha224_sha224_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__sha224_sha224_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "sha224",
            sky_function_createbuiltin(
                    "sha224",
                    "Return a new SHA224 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__sha224_sha224,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__sha256_sha256_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("sha256"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(SHA256_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__sha256_sha256_block_size_getter, NULL,
            "digest_size",  NULL, sky__sha256_sha256_digest_size_getter, NULL,
            "name",         NULL, sky__sha256_sha256_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "sha256.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__sha256_sha256_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "sha256.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__sha256_sha256_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "sha256.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__sha256_sha256_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "sha256.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__sha256_sha256_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__sha256_sha256_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "sha256",
            sky_function_createbuiltin(
                    "sha256",
                    "Return a new SHA256 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__sha256_sha256,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
}
