#include "../core/sky_private.h"

#include <float.h>


SKY_MODULE_EXTERN void
skython_module__testcapi_initialize(sky_module_t module);


static sky_object_t
sky__testcapi_make_exception_with_doc(
                sky_string_t    name,
                sky_object_t    doc,
                sky_object_t    base,
                sky_object_t    dict)
{
    sky_tuple_t     bases;
    sky_object_t    v;
    sky_string_t    module;

    v = sky_string_rsplit(sky_string_copy(name), SKY_STRING_LITERAL("."), 1);
    if (sky_object_len(v) != 2) {
        sky_error_raise_string(sky_SystemError, "name must be module.class");
    }
    module = sky_sequence_get(v, 0);
    name = sky_sequence_get(v, 1);

    if (!base) {
        base = sky_Exception;
    }
    if (sky_object_isa(base, sky_tuple_type)) {
        bases = base;
    }
    else {
        bases = sky_tuple_pack(1, base);
    }
    if (!dict) {
        dict = sky_dict_create();
    }
    sky_dict_setitem(dict, SKY_STRING_LITERAL("__module__"), module);
    if (doc) {
        sky_dict_setitem(dict, SKY_STRING_LITERAL("__doc__"), doc);
    }

    /* type(name, (base,), dict) */
    return sky_object_call(sky_type_type,
                           sky_object_build("(OOO)", name, bases, dict),
                           NULL);
}


static void
sky__testcapi_raise_exception(sky_object_t exc, ssize_t num_args)
{
    ssize_t         i;
    sky_tuple_t     exc_args;
    sky_object_t    *objects;

    if (num_args < 0) {
        sky_error_raise_string(sky_ValueError, "num_args must be >= 0");
    }
    else if (!num_args) {
        exc_args = sky_tuple_empty;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            objects = sky_asset_calloc(num_args, sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ALWAYS);
            for (i = 0; i < num_args; ++i) {
                objects[i] = sky_integer_create(i);
            }
            exc_args = sky_tuple_createwitharray(num_args, objects, sky_free);
        } SKY_ASSET_BLOCK_END;
    }

    sky_error_raise_object(exc, exc_args);
}


static void
sky__testcapi_raise_memoryerror(void)
{
    sky_error_raise_object(sky_MemoryError, sky_None);
}


void
skython_module__testcapi_initialize(sky_module_t module)
{
    sky_module_setattr(
            module,
            "make_exception_with_doc",
            sky_function_createbuiltin(
                    "make_exception_with_doc",
                    NULL,
                    (sky_native_code_function_t)sky__testcapi_make_exception_with_doc,
                    SKY_DATA_TYPE_OBJECT,
                    "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "doc", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "base", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "dict", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "raise_exception",
            sky_function_createbuiltin(
                    "raise_exception",
                    NULL,
                    (sky_native_code_function_t)sky__testcapi_raise_exception,
                    SKY_DATA_TYPE_VOID,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    "num_args", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
    sky_module_setattr(
            module,
            "raise_memoryerror",
            sky_function_createbuiltin(
                    "raise_memoryerror",
                    NULL,
                    (sky_native_code_function_t)sky__testcapi_raise_memoryerror,
                    SKY_DATA_TYPE_VOID,
                    NULL));

#define float_const(_n) \
    sky_module_setattr(module, #_n, sky_float_create(_n))
#define int_const(_n) \
    sky_module_setattr(module, #_n, sky_integer_create(_n))

    int_const(CHAR_MAX);
    int_const(CHAR_MIN);
    int_const(UCHAR_MAX);
    int_const(SHRT_MAX);
    int_const(SHRT_MIN);
    int_const(USHRT_MAX);
    int_const(INT_MAX);
    int_const(INT_MIN);
    int_const(UINT_MAX);
    int_const(LONG_MAX);
    int_const(LONG_MIN);
    int_const(ULONG_MAX);
    float_const(FLT_MAX);
    float_const(FLT_MIN);
    float_const(DBL_MAX);
    float_const(DBL_MIN);
    int_const(LLONG_MAX);
    int_const(LLONG_MIN);
    int_const(ULLONG_MAX);

    sky_module_setattr(module, "PY_SSIZE_T_MAX", sky_integer_create(SSIZE_MAX));
    sky_module_setattr(module, "PY_SSIZE_T_MIN", sky_integer_create(SSIZE_MIN));
}
