#include "../core/skython.h"

#include "md5.h"


SKY_MODULE_EXTERN void
skython_module__md5_initialize(sky_module_t module);


#define SKY__MD5_BLOCKSIZE  64
#define SKY__MD5_DIGESTSIZE 16


static sky_type_t sky__md5_md5_type;

SKY_EXTERN_INLINE MD5_CTX *
sky__md5_md5_data(sky_object_t object)
{
    return sky_object_data(object, sky__md5_md5_type);
}

static void
sky__md5_md5_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    MD5_CTX *self_data = data;

    MD5_Init(self_data);
}


static sky_object_t
sky__md5_md5_block_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SKY__MD5_BLOCKSIZE);
}

static sky_object_t
sky__md5_md5_copy(sky_object_t self)
{
    sky_object_t    new_md5;

    new_md5 = sky_object_allocate(sky__md5_md5_type);
    memcpy(sky__md5_md5_data(new_md5),
           sky__md5_md5_data(self),
           sizeof(MD5_CTX));

    return new_md5;
}

static sky_bytes_t
sky__md5_md5_digest(sky_object_t self)
{
    MD5_CTX *self_data = sky__md5_md5_data(self);

    MD5_CTX     final_data;
    uint8_t     *raw_bytes;
    sky_bytes_t bytes;

    SKY_ASSET_BLOCK_BEGIN {
        memcpy(&final_data, self_data, sizeof(MD5_CTX));

        raw_bytes = sky_asset_malloc(SKY__MD5_DIGESTSIZE,
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        MD5_Final(raw_bytes, &final_data);

        bytes = sky_bytes_createwithbytes(raw_bytes,
                                          SKY__MD5_DIGESTSIZE,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static sky_object_t
sky__md5_md5_digest_size_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return sky_integer_create(SKY__MD5_DIGESTSIZE);
}

static sky_string_t
sky__md5_md5_hexdigest(sky_object_t self)
{
    MD5_CTX *self_data = sky__md5_md5_data(self);

    char        hexdigest[SKY__MD5_DIGESTSIZE * 2], *p;
    size_t      i;
    MD5_CTX     final_data;
    uint8_t     digest[SKY__MD5_DIGESTSIZE];
    const char  *hexdigits;

    memcpy(&final_data, self_data, sizeof(MD5_CTX));
    MD5_Final(digest, &final_data);

    p = hexdigest;
    hexdigits = sky_ctype_lower_hexdigits;
    for (i = 0; i < sizeof(digest); ++i) {
        *p++ = hexdigits[(digest[i] >> 4) & 0xF];
        *p++ = hexdigits[(digest[i]     ) & 0xF];
    }

    return sky_string_createfrombytes(hexdigest, sizeof(hexdigest), NULL, NULL);
}

static sky_object_t
sky__md5_md5_name_getter(
        SKY_UNUSED  sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    return SKY_STRING_LITERAL("MD5");
}

static void
sky__md5_md5_update(sky_object_t self, sky_object_t string)
{
    MD5_CTX *self_data = sky__md5_md5_data(self);

    sky_buffer_t    buffer;

    if (sky_object_isa(string, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "Unicode-objects must be encoded before hashing");
    }
    if (!sky_buffer_check(string)) {
        sky_error_raise_string(
                sky_TypeError,
                "object supporting the buffer API required");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        MD5_Update(self_data, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t
sky__md5_md5(sky_object_t string)
{
    sky_object_t    md5;

    md5 = sky_object_allocate(sky__md5_md5_type);
    if (!sky_object_isnull(string)) {
        sky__md5_md5_update(md5, string);
    }

    return md5;
}


void
skython_module__md5_initialize(sky_module_t module)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__md5_md5_instance_initialize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("md5"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(MD5_CTX),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setgetsets(type,
            "block_size",   NULL, sky__md5_md5_block_size_getter, NULL,
            "digest_size",  NULL, sky__md5_md5_digest_size_getter, NULL,
            "name",         NULL, sky__md5_md5_name_getter, NULL,
            NULL);
    sky_type_setattr_builtin(
            type,
            "copy",
            sky_function_createbuiltin(
                    "md5.copy",
                    "Return a copy of the hash object.",
                    (sky_native_code_function_t)sky__md5_md5_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "digest",
            sky_function_createbuiltin(
                    "md5.digest",
                    "Return the digest value as a string of binary data.",
                    (sky_native_code_function_t)sky__md5_md5_digest,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "hexdigest",
            sky_function_createbuiltin(
                    "md5.hexdigest",
                    "Return the digest value as a string of hexadecimal digits.",
                    (sky_native_code_function_t)sky__md5_md5_hexdigest,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "update",
            sky_function_createbuiltin(
                    "md5.update",
                    "Update this hash object's state with the provided string.",
                    (sky_native_code_function_t)sky__md5_md5_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "string", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__md5_md5_type),
            type,
            SKY_TRUE);

    sky_module_setattr(
            module,
            "md5",
            sky_function_createbuiltin(
                    "md5",
                    "Return a new MD5 hash object; optionally initialized with a string.",
                    (sky_native_code_function_t)sky__md5_md5,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
}
