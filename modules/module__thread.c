#include "../core/sky_private.h"
#include "../core/sky_mutex_private.h"

#include <math.h>


#define SKY__THREAD_TIMEOUT_MAX INT64_MAX


/* TODO handle atfork_child() -> reset thread count to 0 */
static int32_t  sky__thread_count = 0;


static sky_type_t   sky__thread_lock_type;

static const char sky__thread_lock_type_doc[] =
"A lock object is a synchronization primitive.  To create a lock,\n"
"call the PyThread_allocate_lock() function.  Methods are:\n"
"\n"
"acquire() -- lock the lock, possibly blocking until it can be obtained\n"
"release() -- unlock of the lock\n"
"locked() -- test whether the lock is current locked\n"
"\n"
"A lock is not owned by the thread that locked it; another thread may\n"
"unlock it.  A thread attempting to lock a lock that it has already locked\n"
"will block until another thread unlocks it.  Deadlocks may ensue.";

typedef struct sky__thread_lock_data_s {
    sky_mutex_t                         mutex;
} sky__thread_lock_data_t;

SKY_STATIC_INLINE sky__thread_lock_data_t *
sky__thread_lock_data(sky_object_t object)
{
    return sky_object_data(object, sky__thread_lock_type);
}

SKY_STATIC_INLINE sky_mutex_t
sky__thread_lock_mutex(sky_object_t object)
{
    return sky__thread_lock_data(object)->mutex;
}

static void
sky__thread_lock_instance_initialize(sky_object_t self, void *data)
{
    sky__thread_lock_data_t *self_data = data;

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->mutex)),
                      sky_mutex_create(SKY_FALSE, SKY_FALSE),
                      self);
}

static void
sky__thread_lock_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__thread_lock_data_t *self_data = data;

    sky_object_visit(self_data->mutex, visit_data);
}


static sky_type_t   sky__thread_RLock_type;

typedef struct sky__thread_RLock_data_s {
    sky_mutex_t                         mutex;
} sky__thread_RLock_data_t;

SKY_STATIC_INLINE sky__thread_RLock_data_t *
sky__thread_RLock_data(sky_object_t object)
{
    return sky_object_data(object, sky__thread_RLock_type);
}

SKY_STATIC_INLINE sky_mutex_t
sky__thread_RLock_mutex(sky_object_t object)
{
    return sky__thread_RLock_data(object)->mutex;
}

static void
sky__thread_RLock_instance_initialize(
                sky_object_t    self,
                void *          data)
{
    sky__thread_RLock_data_t    *self_data = data;

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->mutex)),
                      sky_mutex_create(SKY_TRUE, SKY_FALSE),
                      self);
}

static void
sky__thread_RLock_instance_visit(
    SKY_UNUSED  sky_object_t                self,
                void *                      data,
                sky_object_visit_data_t *   visit_data)
{
    sky__thread_RLock_data_t    *self_data = data;

    sky_object_visit(self_data->mutex, visit_data);
}


static sky_type_t   sky__thread__local_type;

typedef struct sky__thread__local_data_s {
    sky_tlskey_t                        key;
} sky__thread__local_data_t;

SKY_STATIC_INLINE sky__thread__local_data_t *
sky__thread__local_data(sky_object_t object)
{
    return sky_object_data(object, sky__thread__local_type);
}

static void
sky__thread__local_data_cleanup(void *arg)
{
    sky_dict_t  *dict = arg;

    sky_object_gc_setroot(SKY_AS_OBJECTP(dict), NULL, SKY_FALSE);
    sky_free(dict);
}

static sky_dict_t
sky__thread__local_dict(sky_object_t self)
{
    sky_tlskey_t    key = sky__thread__local_data(self)->key;

    sky_dict_t  *dict;

    if (!(dict = sky_tls_get(key))) {
        SKY_ASSET_BLOCK_BEGIN {
            dict = sky_asset_calloc(1, sizeof(sky_dict_t),
                                    SKY_ASSET_CLEANUP_ON_ERROR);
            sky_tls_set(key, dict);
        } SKY_ASSET_BLOCK_END;
    }
    if (!*dict) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(dict),
                              sky_dict_create(),
                              SKY_FALSE);
    }
    return *dict;
}

static void
sky__thread__local_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky__thread__local_data_t   *self_data = data;

    sky_tls_free(self_data->key);
}

static void
sky__thread__local_instance_initialize(
    SKY_UNUSED  sky_object_t    self,
                void *          data)
{
    sky__thread__local_data_t   *self_data = data;

    self_data->key = sky_tls_alloc(sky__thread__local_data_cleanup);
}


static sky_bool_t
sky__thread_lock_acquire_lock(
                sky_object_t    self,
                sky_bool_t      blocking,
                double          timeout)
{
    sky_time_t  time_timeout;

    if (!blocking && timeout != -1) {
        sky_error_raise_string(
                sky_ValueError,
                "can't specify a timeout for a non-blocking call");
    }
    if (timeout < 0 && timeout != -1) {
        sky_error_raise_string(
                sky_ValueError,
                "timeout value must be strictly positive");
    }

    if (!blocking) {
        time_timeout = 0;
    }
    else if (timeout < 0) {
        time_timeout = SKY_TIME_INFINITE;
    }
    else {
        double  floatpart, intpart;

        if (timeout > (double)SKY__THREAD_TIMEOUT_MAX / 1000000) {
            sky_error_raise_string(sky_OverflowError,
                                   "timeout value is too large");
        }

        floatpart = modf(timeout, &intpart);
        time_timeout = ((uint64_t)intpart * UINT64_C(1000000000)) +
                       ((uint64_t)(floatpart * 1000000000.0));
    }

    return sky_mutex_lock(sky__thread_lock_mutex(self), time_timeout);
}

static const char sky__thread_lock_acquire_lock_doc[] =
"acquire([wait]) -> bool\n"
"(acquire_lock() is an obsolete synonym)\n\n"
"Lock the lock.  Without argument, this blocks if the lock is already\n"
"locked (even by the same thread), waiting for another thread to release\n"
"the lock, and return True once the lock is acquire.\n"
"With an argument, this will only block if the argument is true,\n"
"and the return value reflects whether the lock is acquired.\n"
"The blocking operation is interruptible.";


sky_bool_t
sky__thread_lock_locked(sky_object_t self)
{
    return sky_mutex_islocked(sky__thread_lock_mutex(self));
}

static const char sky__thread_lock_locked_lock_doc[] =
"locked() -> bool\n"
"(locked_lock() is an obsolete synonym)\n\n"
"Return whether the lock is in the locked state.";


void
sky__thread_lock_release(sky_object_t self)
{
    sky_mutex_unlock(sky__thread_lock_mutex(self));
}

static const char sky__thread_lock_release_lock_doc[] =
"release()\n"
"(release_lock() is an obsolete synonym)\n\n"
"Release the lock, allowing another thread that is blocked waiting for\n"
"the lock to acquire the lock.  The lock must be in the locked state,\n"
"but it needn't be locked by the same thread that unlocks it.";


static sky_object_t
sky__thread_lock_enter(sky_object_t self)
{
    if (sky__thread_lock_acquire_lock(self, SKY_TRUE, -1.0)) {
        return sky_True;
    }
    return sky_False;
}


static sky_bool_t
sky__thread_lock_exit(
                sky_object_t    self,
    SKY_UNUSED  sky_type_t      exc_type,
    SKY_UNUSED  sky_object_t    exc_value,
    SKY_UNUSED  sky_object_t    exc_traceback)
{
    sky__thread_lock_release(self);
    return SKY_FALSE;
}


static sky_bool_t
sky__thread_RLock_acquire(
                sky_object_t    self,
                sky_bool_t      blocking,
                double          timeout)
{
    sky_time_t  time_timeout;

    if (!blocking && timeout != -1) {
        sky_error_raise_string(
                sky_ValueError,
                "can't specify a timeout for a non-blocking call");
    }
    if (timeout < 0 && timeout != -1) {
        sky_error_raise_string(
                sky_ValueError,
                "timeout value must be strictly positive");
    }

    if (!blocking) {
        time_timeout = 0;
    }
    else if (timeout < 0) {
        time_timeout = SKY_TIME_INFINITE;
    }
    else {
        double  floatpart, intpart;

        if (timeout > (double)SKY__THREAD_TIMEOUT_MAX / 1000000) {
            sky_error_raise_string(sky_OverflowError,
                                   "timeout value is too large");
        }

        floatpart = modf(timeout, &intpart);
        time_timeout = ((uint64_t)intpart * UINT64_C(1000000000)) +
                       ((uint64_t)(floatpart * 1000000000.0));
    }

    return sky_mutex_lock(sky__thread_RLock_mutex(self), time_timeout);
}

static const char sky__thread_RLock_acquire_doc[] =
"acquire(blocking=True) -> bool\n\n"
"Lock the lock.  `blocking` indicates whether we should wait\n"
"for the lock to be available or not.  If `blocking` is False\n"
"and another thread holds the lock, the method will return False\n"
"immediately.  If `blocking` is True and another thread holds\n"
"the lock, the method will wait for the lock to be released,\n"
"take it and then return True.\n"
"(note: the blocking operation is interruptible.)\n"
"\n"
"In all other cases, the method will return True immediately.\n"
"Precisely, if the current thread already holds the lock, its\n"
"internal counter is simply incremented. If nobody holds the lock,\n"
"the lock is taken and its internal counter initialized to 1.";


static void
sky__thread_RLock__acquire_restore(sky_object_t self, sky_tuple_t state)
{
    sky_mutex_t mutex = sky__thread_RLock_mutex(self);

    sky_mutex_data_t    *mutex_data;

    if (!sky_mutex_lock(mutex, SKY_TIME_INFINITE)) {
        sky_error_raise_string(sky_RuntimeError, "couldn't acquire lock");
    }

    mutex_data = sky_mutex_data(mutex);
    mutex_data->count = sky_integer_value(sky_tuple_get(state, 0),
                                          INT32_MIN, INT32_MAX,
                                          NULL);
    mutex_data->owner = (sky_thread_t)sky_integer_value(sky_tuple_get(state, 1),
                                                        0, UINTPTR_MAX,
                                                        NULL);
}

static const char sky__thread_RLock__acquire_restore_doc[] =
"_acquire_restore(state) -> None\n\n"
"For internal use by `threading.Condition`.";


static sky_bool_t
sky__thread_RLock__is_owned(sky_object_t self)
{
    return sky_mutex_isowned(sky__thread_RLock_mutex(self));
}

static const char sky__thread_RLock__is_owned_doc[] =
"_is_owned() -> bool\n\n"
"For internal use by `threading.Condition`.";


static void
sky__thread_RLock_release(sky_object_t self)
{
    sky_mutex_unlock(sky__thread_RLock_mutex(self));
}

static const char sky__thread_RLock_release_doc[] =
"release()\n\n"
"Release the lock, allowing another thread that is blocked waiting for\n"
"the lock to acquire the lock.  The lock must be in the locked state,\n"
"and must be locked by the same thread that unlocks it; otherwise a\n"
"`RuntimeError` is raised.\n"
"\n"
"Do note that if the lock was acquire()d several times in a row by the\n"
"current thread, release() needs to be called as many times for the lock\n"
"to be available for other threads.";


static sky_tuple_t
sky__thread_RLock__release_save(sky_object_t self)
{
    sky_mutex_t mutex = sky__thread_RLock_mutex(self);

    sky_tuple_t         result;
    sky_mutex_data_t    *mutex_data;

    if (!sky_mutex_islocked(mutex)) {
        sky_error_raise_string(sky_RuntimeError,
                               "cannot release un-acquired lock");
    }
    /* This assumes that the calling thread is the owner of the mutex, which is
     * exactly what CPython does. It's not safe.
     */
    mutex_data = sky_mutex_data(mutex);
    result = sky_object_build("(i32uj)",
                              mutex_data->count,
                              (uintmax_t)mutex_data->owner);

    /* Trick the mutex code into fully unlocking the mutex.
     * This is not safe.
     */
    mutex_data->count = 1;
    mutex_data->owner = sky_thread_self();
    sky_mutex_unlock(mutex);

    return result;
}

static const char sky__thread_RLock__release_save_doc[] =
"_release_save() -> tuple\n\n"
"For internal use by `threading.Condition`.";


static sky_object_t
sky__thread_RLock_enter(sky_object_t self)
{
    if (sky__thread_RLock_acquire(self, SKY_TRUE, -1.0)) {
        return sky_True;
    }
    return sky_False;
}


static sky_bool_t
sky__thread_RLock_exit(
                sky_object_t    self,
    SKY_UNUSED  sky_type_t      exc_type,
    SKY_UNUSED  sky_object_t    exc_value,
    SKY_UNUSED  sky_object_t    exc_traceback)
{
   sky__thread_RLock_release(self);
   return SKY_FALSE;
}


static sky_object_t
sky__thread_RLock_new(
                sky_type_t  cls,
    SKY_UNUSED  sky_tuple_t args,
    SKY_UNUSED  sky_dict_t  kws)
{
    return sky_object_allocate(cls);
}


static sky_string_t
sky__thread_RLock_repr(sky_object_t self)
{
    sky_mutex_t         mutex = sky__thread_RLock_mutex(self);
    sky_mutex_data_t    *mutex_data = sky_mutex_data(mutex);

    return sky_string_createfromformat("<%@ owner=%ju count=%jd>",
                                       sky_type_name(sky_object_type(self)),
                                       (uintmax_t)mutex_data->owner,
                                       (intmax_t)mutex_data->count);
}


static void
sky__thread__local_delattr(sky_object_t self, sky_string_t name)
{
    if (sky_object_compare(name,
                           SKY_STRING_LITERAL("__dict__"),
                           SKY_COMPARE_OP_EQUAL))
    {
        sky_error_raise_format(
                sky_AttributeError,
                "%#@ object attribute %#@ is read-only",
                sky_type_name(self),
                name);
    }

    sky_base_object_delattrwithdict(self, name, sky__thread__local_dict(self));
}

static sky_object_t
sky__thread__local_getattribute(sky_object_t self, sky_string_t name)
{
    sky_dict_t  dict = sky__thread__local_dict(self);

    sky_object_t    value;

    if (sky_object_compare(name,
                           SKY_STRING_LITERAL("__dict__"),
                           SKY_COMPARE_OP_EQUAL))
    {
        return dict;
    }

    if (sky_object_type(self) == sky__thread__local_type &&
        (value = sky_dict_getitem(dict, name)) != NULL)
    {
        return value;
    }

    return sky_base_object_getattrwithdict(self, name, dict);
}

static sky_object_t
sky__thread__local_new(sky_type_t cls, sky_tuple_t args, sky_dict_t kws)
{
    if (sky__thread__local_type == cls &&
        (sky_object_bool(args) || sky_object_bool(kws)))
    {
        sky_error_raise_string(sky_TypeError,
                               "Initialization arguments are not supported");
    }

    return sky_object_allocate(cls);
}

static void
sky__thread__local_setattr(
                sky_object_t    self,
                sky_string_t    name,
                sky_object_t    value)
{
    if (sky_object_compare(name,
                           SKY_STRING_LITERAL("__dict__"),
                           SKY_COMPARE_OP_EQUAL))
    {
        sky_error_raise_format(
                sky_AttributeError,
                "%#@ object attribute %#@ is read-only",
                sky_type_name(self),
                name);
    }

    sky_base_object_setattrwithdict(self,
                                    name,
                                    value,
                                    sky__thread__local_dict(self));
}


static sky_object_t
sky__thread_allocate_lock(void)
{
    return sky_object_allocate(sky__thread_lock_type);
}

static const char sky__thread_allocate_lock_doc[] =
"allocate_lock() -> lock object\n"
"(allocate() is an obsolete synonym)\n\n"
"Create a new lock object.  See help(LockType) for information about locks.";


static int32_t
sky__thread__count(void)
{
    return sky__thread_count;
}

static const char sky__thread__count_doc[] =
"_count() -> integer\n\n"
"Return the number of currently running Python threads, excluding \n"
"the main thread. The returned number comprises all threads created\n"
"through `start_new_thread()` as well as `threading.Thread`, and not\n"
"yet finished.\n"
"\n"
"This function is meant for internal and specialized purposes only.\n"
"In most applications `threading.enumerate()` should be used instead.";


static SKY_NORETURN void
sky__thread_exit_thread(void)
{
    sky_error_raise_object(sky_SystemExit, sky_None);
}

static const char sky__thread_exit_thread_doc[] =
"exit()\n"
"(exit_thread() is an obsolete synonym)\n\n"
"This is synonymous to ``raise SystemExit''.  It will cause the current\n"
"thread to exit silently unless the exception is caught.";


static uintmax_t
sky__thread_get_ident(void)
{
    return (uintmax_t)sky_thread_self();
}

static const char sky__thread_get_ident_doc[] =
"get_ident() -> integer\n\n"
"Return a non-zero integer that uniquely identifies the current thread\n"
"amonst other threads that exist simultaneously.\n"
"This may be used to identify per-thread resources.\n"
"Even though on some platforms threads identities may appear to be\n"
"allocated consecutive numbers starting at 1, this behavior should not\n"
"be relied upon, and the number should be seen purely as a magic cookie.\n"
"A thread's identity may be reused for another thread after it exits.";


static void
sky__thread_interrupt_main(void)
{
    sky_signal_trigger(SIGINT);
}

static const char sky__thread_interrupt_main_doc[] =
"interrupt_main()\n\n"
"Raise a KeyboardInterrupt in the main thread.\n"
"A subthread can use this function to interrupt the main thread.";


static const char sky__thread_stack_size_doc[] =
"stack_size([size]) -> size\n\n"
"Return the thread stack size used when creating new threads.  The\n"
"optional size argument specifies the stack size (in bytes) to be used\n"
"for subsequently created threads, and must be 0 (use platform or\n"
"configured default) or a positive integer value of at least 32,768 (32k).\n"
"If changing the thread stack size is unsupported, a ThreadError\n"
"exception is raised.  If the specified size is invalid, a ValueError\n"
"exception is raised, and the stack size is unmodified.  32k bytes\n"
" currently the minimum supported stack size value to guarantee\n"
"sufficient stack space for the interpreter itself.\n"
"\n"
"Note that some platforms may have particular restrictions on values for\n"
"the stack size, such as requiring a minimum stack size larger than 32kB or\n"
"requiring allocation in multiples of the system memory page size\n"
"- platform documentation should be referred to for more information\n"
"(4kB pages are common; using multiples of 4096 for the stack size is\n"
"the suggested approach in the absence of more specific information).";


typedef struct sky__thread_data_s {
    sky_object_t                        function;
    sky_tuple_t                         args;
    sky_dict_t                          kwargs;
} sky__thread_data_t;

static void
sky__thread_data_cleanup(void *arg)
{
    sky__thread_data_t  *data = arg;

    sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->function)), NULL, SKY_FALSE);
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->args)), NULL, SKY_FALSE);
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->kwargs)), NULL, SKY_FALSE);
    sky_free(data);
}

static void *
sky__thread_main(void *arg)
{
    sky__thread_data_t  *data = arg;

    sky_atomic_increment32(&sky__thread_count);

    SKY_ERROR_TRY {
        sky_asset_save(data,
                       (sky_free_t)sky__thread_data_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_object_call(data->function, data->args, data->kwargs);
    } SKY_ERROR_EXCEPT(sky_SystemExit) {
        /* pass */
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_record_t  *record = sky_error_current();

        sky_module_t    sys;
        sky_object_t    sys_stderr;

        if ((sys = sky_module_import(SKY_STRING_LITERAL("sys"))) != NULL &&
            (sys_stderr = sky_object_getattr(sys,
                                             SKY_STRING_LITERAL("stderr"),
                                             NULL)) != NULL)
        {
            sky_string_t    message;

            message = sky_string_createfromformat(
                            "Unhandled exception in thread started by %#@\n",
                            data->function);
            sky_object_callmethod(sys_stderr,
                                  SKY_STRING_LITERAL("write"),
                                  sky_tuple_pack(1, message),
                                  NULL);
        }
        sky_error_display(record);
    } SKY_ERROR_TRY_END;

    sky_atomic_decrement32(&sky__thread_count);
    return NULL;
}

static uintptr_t
sky__thread_start_new_thread(
                sky_object_t    function,
                sky_tuple_t     args,
                sky_dict_t      kwargs)
{
    sky_thread_t        new_thread;
    sky__thread_data_t  *data;

    if (!sky_object_callable(function)) {
        sky_error_raise_string(sky_TypeError, "first arg must be callable");
    }

    SKY_ASSET_BLOCK_BEGIN {
        data = sky_asset_calloc(1, sizeof(sky__thread_data_t),
                                SKY_ASSET_CLEANUP_ON_ERROR);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->function)),
                              function,
                              SKY_TRUE);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->args)),
                              args,
                              SKY_TRUE);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(data->kwargs)),
                              kwargs,
                              SKY_TRUE);

        new_thread = sky_thread_create(sky__thread_main, data);
    } SKY_ASSET_BLOCK_END;

    sky_thread_detach(new_thread);
    return (uintptr_t)new_thread;
}

static const char sky__thread_start_new_thread_doc[] =
"start_new_thread(function, args[, kwargs])\n"
"(start_new() is an obsolete synonym)\n\n"
"Start a new thread and return its identifier.  The thread will call the\n"
"function with positional arguments from the tuple args and keyword arguments\n"
"taken from the optional dictionary kwargs.  The thread exits when the\n"
"function returns; the return value is ignored.  The thread will also exit\n"
"when the function raises an unhandled exception; a stack trace will be\n"
"printed unless the exception is SystemExit.\n";


void
skython_module__thread_initialize(sky_module_t module)
{
    static const char module_doc[] =
"This module provides primitive operations to write multi-threaded programs.\n"
"The 'threading' module provides a more convenient interface.";

    sky_string_t        doc;
    sky_type_t          type;
    sky_type_template_t template;

    doc = sky_string_createfrombytes(module_doc,
                                     sizeof(module_doc) - 1,
                                     NULL,
                                     NULL);
    sky_module_setattr(module, "__doc__", doc);

    sky__thread_count = 0;

    sky_module_setattr(module,
                       "TIMEOUT_MAX",
                       sky_float_create(SKY__THREAD_TIMEOUT_MAX / 1000000));

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__thread_lock_instance_initialize;
    template.visit = sky__thread_lock_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("lock"),
                                   sky__thread_lock_type_doc,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky__thread_lock_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__enter__", sky__thread_lock_enter,
            "__exit__", sky__thread_lock_exit,
            NULL);

    sky_type_setattr_builtin(
            type,
            "acquire",
            sky_function_createbuiltin(
                    "lock.acquire",
                    sky__thread_lock_acquire_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_acquire_lock,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "blocking", SKY_DATA_TYPE_BOOL, sky_True,
                    "timeout", SKY_DATA_TYPE_DOUBLE, sky_float_create(-1.0),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "acquire_lock",
            sky_function_createbuiltin(
                    "lock.acquire_lock",
                    sky__thread_lock_acquire_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_acquire_lock,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "blocking", SKY_DATA_TYPE_BOOL, sky_True,
                    "timeout", SKY_DATA_TYPE_DOUBLE, sky_float_create(-1.0),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "locked",
            sky_function_createbuiltin(
                    "lock.locked",
                    sky__thread_lock_locked_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_locked,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "locked_lock",
            sky_function_createbuiltin(
                    "lock.locked_lock",
                    sky__thread_lock_locked_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_locked,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "release",
            sky_function_createbuiltin(
                    "lock.release",
                    sky__thread_lock_release_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_release,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "release_lock",
            sky_function_createbuiltin(
                    "lock.release_lock",
                    sky__thread_lock_release_lock_doc,
                    (sky_native_code_function_t)sky__thread_lock_release,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__thread_lock_type),
            type,
            SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__thread_RLock_instance_initialize;
    template.visit = sky__thread_RLock_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("RLock"),
                                   NULL,
                                   0,
                                   sizeof(sky__thread_RLock_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmethodslots(type,
            "__new__", sky__thread_RLock_new,
            "__repr__", sky__thread_RLock_repr,
            "__enter__", sky__thread_RLock_enter,
            "__exit__", sky__thread_RLock_exit,
            NULL);

    sky_type_setattr_builtin(
            type,
            "acquire",
            sky_function_createbuiltin(
                    "RLock.acquire",
                    sky__thread_RLock_acquire_doc,
                    (sky_native_code_function_t)sky__thread_RLock_acquire,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "blocking", SKY_DATA_TYPE_BOOL, sky_True,
                    "timeout", SKY_DATA_TYPE_DOUBLE, sky_float_create(-1.0),
                    NULL));
    sky_type_setattr_builtin(
            type,
            "_acquire_restore",
            sky_function_createbuiltin(
                    "RLock._acquire_restore",
                    sky__thread_RLock__acquire_restore_doc,
                    (sky_native_code_function_t)sky__thread_RLock__acquire_restore,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    "state", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "_is_owned",
            sky_function_createbuiltin(
                    "RLock._is_owned",
                    sky__thread_RLock__is_owned_doc,
                    (sky_native_code_function_t)sky__thread_RLock__is_owned,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "release",
            sky_function_createbuiltin(
                    "RLock.release",
                    sky__thread_RLock_release_doc,
                    (sky_native_code_function_t)sky__thread_RLock_release,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));
    sky_type_setattr_builtin(
            type,
            "_release_save",
            sky_function_createbuiltin(
                    "RLock._release_save",
                    sky__thread_RLock__release_save_doc,
                    (sky_native_code_function_t)sky__thread_RLock__release_save,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__thread_RLock_type),
            type,
            SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.initialize = sky__thread__local_instance_initialize;
    template.finalize = sky__thread__local_instance_finalize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("_local"),
                                   "Thread-local data",
                                   0,
                                   sizeof(sky__thread__local_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmethodslots(type,
            "__new__", sky__thread__local_new,
            "__getattribute__", sky__thread__local_getattribute,
            "__setattr__", sky__thread__local_setattr,
            "__delattr__", sky__thread__local_delattr,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky__thread__local_type),
            type,
            SKY_TRUE);

    sky_module_setattr(module, "LockType", sky__thread_lock_type);
    sky_module_setattr(module, "RLock", sky__thread_RLock_type);
    sky_module_setattr(module, "error", sky_RuntimeError);
    sky_module_setattr(module, "_local", sky__thread__local_type);

    sky_module_setattr(
            module,
            "allocate",
            sky_function_createbuiltin(
                    "allocate",
                    sky__thread_allocate_lock_doc,
                    (sky_native_code_function_t)sky__thread_allocate_lock,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "allocate_lock",
            sky_function_createbuiltin(
                    "allocate_lock",
                    sky__thread_allocate_lock_doc,
                    (sky_native_code_function_t)sky__thread_allocate_lock,
                    SKY_DATA_TYPE_OBJECT,
                    NULL));
    sky_module_setattr(
            module,
            "_count",
            sky_function_createbuiltin(
                    "_count",
                    sky__thread__count_doc,
                    (sky_native_code_function_t)sky__thread__count,
                    SKY_DATA_TYPE_INT32,
                    NULL));
    sky_module_setattr(
            module,
            "exit",
            sky_function_createbuiltin(
                    "exit",
                    sky__thread_exit_thread_doc,
                    (sky_native_code_function_t)sky__thread_exit_thread,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "exit_thread",
            sky_function_createbuiltin(
                    "exit_thread",
                    sky__thread_exit_thread_doc,
                    (sky_native_code_function_t)sky__thread_exit_thread,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "get_ident",
            sky_function_createbuiltin(
                    "get_ident",
                    sky__thread_get_ident_doc,
                    (sky_native_code_function_t)sky__thread_get_ident,
                    SKY_DATA_TYPE_UINTMAX_T,
                    NULL));
    sky_module_setattr(
            module,
            "interrupt_main",
            sky_function_createbuiltin(
                    "interrupt_main",
                    sky__thread_interrupt_main_doc,
                    (sky_native_code_function_t)sky__thread_interrupt_main,
                    SKY_DATA_TYPE_VOID,
                    NULL));
    sky_module_setattr(
            module,
            "stack_size",
            sky_function_createbuiltin(
                    "stack_size",
                    sky__thread_stack_size_doc,
                    (sky_native_code_function_t)sky_thread_setstacksize,
                    SKY_DATA_TYPE_SSIZE_T,
                    "size", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_module_setattr(
            module,
            "start_new",
            sky_function_createbuiltin(
                    "start_new",
                    sky__thread_start_new_thread_doc,
                    (sky_native_code_function_t)sky__thread_start_new_thread,
                    SKY_DATA_TYPE_UINTPTR_T,
                    "function", SKY_DATA_TYPE_OBJECT, NULL,
                    "args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "kwargs", SKY_DATA_TYPE_OBJECT_DICT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(
            module,
            "start_new_thread",
            sky_function_createbuiltin(
                    "start_new_thread",
                    sky__thread_start_new_thread_doc,
                    (sky_native_code_function_t)sky__thread_start_new_thread,
                    SKY_DATA_TYPE_UINTPTR_T,
                    "function", SKY_DATA_TYPE_OBJECT, NULL,
                    "args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "kwargs", SKY_DATA_TYPE_OBJECT_DICT, sky_NotSpecified,
                    NULL));
}
