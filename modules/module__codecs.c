#include "skython.h"


/*  This module assumes that none of the built-in codecs have initializer or
 *  finalizer methods. If they do, they will not be called, because the CPython
 *  API does not provide for them.
 *
 *  At present, the only two built-in codecs that have initializers are the
 *  utf16-[bl]e and utf32-[bl]e codecs. They set flags according to the endian
 *  in use. Since this module does not pass flags, it does not matter that they
 *  do not get called.
 */


static void
sky__codecs_decode_bytes_callback(
                const void *    bytes,
                ssize_t         nbytes,
    SKY_UNUSED  size_t          width,
                void *          arg)
{
    sky_bytes_builder_appendbytes(arg, bytes, nbytes);
}

static void
sky__codecs_decode_string_callback(
                const void *    bytes,
                ssize_t         nbytes,
                size_t          width,
                void *          arg)
{
    sky_string_builder_appendcodepoints(arg, bytes, nbytes / width, width);
}


#define DECODER(_name, codec)                                           \
    static sky_tuple_t                                                  \
    sky__codecs_##_name(                                                \
                    sky_object_t    input,                              \
                    sky_string_t    errors,                             \
                    sky_bool_t      final)                              \
    {                                                                   \
        ssize_t             consumed;                                   \
        sky_object_t        builder, result;                            \
        sky_codec_output_t  output;                                     \
                                                                        \
        input = sky_codec_decode_prepareinput(codec, input);            \
        if ((codec->decoder_flags &                                     \
                    SKY_CODEC_DECODER_FLAG_RETURN_STRING_INPUT) &&      \
            sky_object_isa(input, sky_string_type))                     \
        {                                                               \
            return sky_tuple_pack(2, input, sky_object_len(input));     \
        }                                                               \
                                                                        \
        SKY_ASSET_BLOCK_BEGIN {                                         \
            sky_buffer_t    buffer;                                     \
                                                                        \
            sky_buffer_acquire(&buffer, input, SKY_BUFFER_FLAG_SIMPLE); \
            sky_asset_save(&buffer,                                     \
                           (sky_free_t)sky_buffer_release,              \
                           SKY_ASSET_CLEANUP_ALWAYS);                   \
                                                                        \
            if (codec->decoder_flags &                                  \
                        SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES)            \
            {                                                           \
                builder = sky_bytes_builder_create();                   \
                output = sky__codecs_decode_bytes_callback;             \
            }                                                           \
            else {                                                      \
                builder = sky_string_builder_create();                  \
                output = sky__codecs_decode_string_callback;            \
            }                                                           \
            consumed = codec->decoder_decode(                           \
                                NULL,                                   \
                                NULL,                                   \
                                errors,                                 \
                                output,                                 \
                                builder,                                \
                                NULL,                                   \
                                buffer.buf,                             \
                                buffer.len,                             \
                                final);                                 \
        } SKY_ASSET_BLOCK_END;                                          \
                                                                        \
        if (codec->decoder_flags &                                      \
                    SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES)                \
        {                                                               \
            result = sky_bytes_builder_finalize(builder);               \
        }                                                               \
        else {                                                          \
            result = sky_string_builder_finalize(builder);              \
        }                                                               \
        return sky_object_build("(Oiz)", result, consumed);             \
    }

#define ENCODER(_name, codec)                                           \
    static sky_tuple_t                                                  \
    sky__codecs_##_name(sky_object_t input, sky_string_t errors)        \
    {                                                                   \
        sky_object_t    output;                                         \
                                                                        \
        output = sky_codec_encode(codec, input, NULL, errors, 0);       \
        return sky_object_build("(Oiz)", output, sky_object_len(input));\
    }

DECODER(ascii_decode, sky_codec_ascii);
ENCODER(ascii_encode, sky_codec_ascii);
DECODER(escape_decode, sky_codec_escape);
ENCODER(escape_encode, sky_codec_escape);
DECODER(latin_1_decode, sky_codec_latin1);
ENCODER(latin_1_encode, sky_codec_latin1);
DECODER(raw_unicode_escape_decode, sky_codec_raw_unicode_escape);
ENCODER(raw_unicode_escape_encode, sky_codec_raw_unicode_escape);
DECODER(unicode_escape_decode, sky_codec_unicode_escape);
ENCODER(unicode_escape_encode, sky_codec_unicode_escape);
DECODER(unicode_internal_decode, sky_codec_unicode_internal);
ENCODER(unicode_internal_encode, sky_codec_unicode_internal);
DECODER(utf_16_decode, sky_codec_utf16);
DECODER(utf_16_be_decode, sky_codec_utf16_be);
DECODER(utf_16_le_decode, sky_codec_utf16_le);
ENCODER(utf_16_encode, sky_codec_utf16);
ENCODER(utf_16_be_encode, sky_codec_utf16_be);
ENCODER(utf_16_le_encode, sky_codec_utf16_le);
DECODER(utf_32_decode, sky_codec_utf32);
DECODER(utf_32_be_decode, sky_codec_utf32_be);
DECODER(utf_32_le_decode, sky_codec_utf32_le);
ENCODER(utf_32_encode, sky_codec_utf32);
ENCODER(utf_32_be_encode, sky_codec_utf32_be);
ENCODER(utf_32_le_encode, sky_codec_utf32_le);
DECODER(utf_7_decode, sky_codec_utf7);
ENCODER(utf_7_encode, sky_codec_utf7);
DECODER(utf_8_decode, sky_codec_utf8);
ENCODER(utf_8_encode, sky_codec_utf8);

#undef ENCODER
#undef DECODER


static sky_tuple_t
sky__codecs_charmap_decode(
                sky_object_t    input,
                sky_string_t    errors,
                sky_object_t    mapping)
{
    sky_string_t    string;

    string = sky_codec_charmap_decode(mapping, errors, NULL, input);
    return sky_object_build("(Oiz)", string, sky_object_len(input));
}


static sky_tuple_t
sky__codecs_charmap_encode(
                sky_object_t    input,
                sky_string_t    errors,
                sky_object_t    mapping)
{
    sky_bytes_t bytes;

    input = sky_string_createfromobject(input);
    bytes = sky_codec_charmap_encode(input, errors, mapping);
    return sky_object_build("(Oiz)", bytes, sky_bytes_len(bytes));
}


static sky_object_t
sky__codecs_decode(
                sky_object_t    input,
                sky_string_t    encoding,
                sky_string_t    errors)
{
    return sky_codec_decode(NULL, encoding, errors, NULL, input);
}


static sky_object_t
sky__codecs_encode(
                sky_object_t    input,
                sky_string_t    encoding,
                sky_string_t    errors)
{
    return sky_codec_encode(NULL, input, encoding, errors, 0);
}


static sky_tuple_t
sky__codecs_readbuffer_encode(
                sky_object_t    input,
    SKY_UNUSED  sky_string_t    errors)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        if (sky_object_isa(input, sky_string_type)) {
            input = sky_codec_encode(sky_codec_utf8, input, NULL, NULL, 0);
        }
        sky_buffer_acquirecbuffer(&buffer,
                                  input,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "readbuffer_encode() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_object_build("(y#iz)", buffer.buf, buffer.len, buffer.len);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_tuple_t
sky__codecs_utf_16_ex_decode(
                sky_object_t    input,
                sky_string_t    errors,
                int             byteorder,
                sky_bool_t      final)
{
    ssize_t                 consumed;
    uint32_t                flags;
    sky_buffer_t            buffer;
    sky_string_builder_t    builder;

    flags = 0;
    if (byteorder < 0) {
        flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
    }
    else if (byteorder > 0) {
        flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, input, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        consumed = sky_codec_utf16->decoder_decode(
                            NULL,
                            NULL,
                            errors,
                            sky__codecs_decode_string_callback,
                            builder,
                            &flags,
                            buffer.buf,
                            buffer.len,
                            final);
    } SKY_ASSET_BLOCK_END;

    if (flags & SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE) {
        byteorder = -1;
    }
    else if (flags & SKY_CODEC_DECODE_FLAG_ENDIAN_BIG) {
        byteorder = 1;
    }
    else {
        byteorder = 0;
    }

    return sky_object_build("(Oizi)",
                            sky_string_builder_finalize(builder),
                            consumed,
                            byteorder);
}


static sky_tuple_t
sky__codecs_utf_32_ex_decode(
                sky_object_t    input,
                sky_string_t    errors,
                int             byteorder,
                sky_bool_t      final)
{
    ssize_t                 consumed;
    uint32_t                flags;
    sky_buffer_t            buffer;
    sky_string_builder_t    builder;

    flags = 0;
    if (byteorder < 0) {
        flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
    }
    else if (byteorder > 0) {
        flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, input, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        consumed = sky_codec_utf32->decoder_decode(
                            NULL,
                            NULL,
                            errors,
                            sky__codecs_decode_string_callback,
                            builder,
                            &flags,
                            buffer.buf,
                            buffer.len,
                            final);
    } SKY_ASSET_BLOCK_END;

    if (flags & SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE) {
        byteorder = -1;
    }
    else if (flags & SKY_CODEC_DECODE_FLAG_ENDIAN_BIG) {
        byteorder = 1;
    }
    else {
        byteorder = 0;
    }

    return sky_object_build("(Oizi)",
                            sky_string_builder_finalize(builder),
                            consumed,
                            byteorder);
}


static const char sky__codecs_decode_doc[] =
"decode(obj, [encoding[,errors]]) -> object\n"
"\n"
"Decodes obj using the codec registered for encoding. encoding defaults\n"
"to the default encoding. errors may be given to set a different error\n"
"handling scheme. Default is 'strict' meaning that encoding errors raise\n"
"a ValueError. Other possible values are 'ignore' and 'replace'\n"
"as well as any other name registered with codecs.register_error that is\n"
"able to handle ValueErrors.";


static const char sky__codecs_encode_doc[] =
"encode(obj, [encoding[,errors]]) -> object\n"
"\n"
"Encodes obj using the codec registered for encoding. encoding defaults\n"
"to the default encoding. errors may be given to set a different error\n"
"handling scheme. Default is 'strict' meaning that encoding errors raise\n"
"a ValueError. Other possible values are 'ignore', 'replace' and\n"
"'xmlcharrefreplace' as well as any other name registered with\n"
"codecs.register_error that can handle ValueErrors.";


static const char sky__codecs_lookup_doc[] =
"lookup(encoding) -> CodecInfo\n"
"\n"
"Looks up a codec tuple in the Python codec registry and returns\n"
"a CodecInfo object.";


static const char sky__codecs_lookup_error_doc[] =
"lookup_error(errors) -> handler\n"
"\n"
"Return the error handler for the specified error handling name\n"
"or raise a LookupError, if no handler exists under this name.";


static const char sky__codecs_register_doc[] =
"register(search_function)\n"
"\n"
"Register a codec search function. Search functions are expected to take\n"
"one argument, the encoding name in all lower case letters, and return\n"
"a tuple of functions (encoder, decoder, stream_reader, stream_writer)\n"
"(or a CodecInfo object).";


static const char sky__codecs_register_error_doc[] =
"register_error(errors, handler)\n"
"\n"
"Register the specified error handler under the name\n"
"errors. handler must be a callable object, that\n"
"will be called with an exception instance containing\n"
"information about the location of the encoding/decoding\n"
"error and must return a (replacement, new position) tuple.";


void
skython_module__codecs_initialize(sky_module_t module)
{
    sky_module_setattr(module,
            "register",
            sky_function_createbuiltin(
                    "register",
                    sky__codecs_register_doc,
                    (sky_native_code_function_t)sky_codec_register,
                    SKY_DATA_TYPE_VOID,
                    "search_function", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "lookup",
            sky_function_createbuiltin(
                    "lookup",
                    sky__codecs_lookup_doc,
                    (sky_native_code_function_t)sky_codec_lookup,
                    SKY_DATA_TYPE_OBJECT,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "encode",
            sky_function_createbuiltin(
                    "encode",
                    sky__codecs_encode_doc,
                    (sky_native_code_function_t)sky__codecs_encode,
                    SKY_DATA_TYPE_OBJECT,
                    "obj", SKY_DATA_TYPE_OBJECT, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    NULL));
    sky_module_setattr(module,
            "decode",
            sky_function_createbuiltin(
                    "decode",
                    sky__codecs_decode_doc,
                    (sky_native_code_function_t)sky__codecs_decode,
                    SKY_DATA_TYPE_OBJECT,
                    "obj", SKY_DATA_TYPE_OBJECT, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    NULL));
    sky_module_setattr(module,
            "register_error",
            sky_function_createbuiltin(
                    "register_error",
                    sky__codecs_register_error_doc,
                    (sky_native_code_function_t)sky_codec_register_error,
                    SKY_DATA_TYPE_VOID,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "handler", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_module_setattr(module,
            "lookup_error",
            sky_function_createbuiltin(
                    "lookup_error",
                    sky__codecs_lookup_error_doc,
                    (sky_native_code_function_t)sky_codec_lookup_error,
                    SKY_DATA_TYPE_OBJECT,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));

    sky_module_setattr(module,
            "utf_16_ex_decode",
            sky_function_createbuiltin(
                    "utf_16_ex_decode",
                    NULL,
                    (sky_native_code_function_t)sky__codecs_utf_16_ex_decode,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "input", SKY_DATA_TYPE_OBJECT, NULL,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "byteorder", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "final", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));

    sky_module_setattr(module,
            "utf_32_ex_decode",
            sky_function_createbuiltin(
                    "utf_32_ex_decode",
                    NULL,
                    (sky_native_code_function_t)sky__codecs_utf_32_ex_decode,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "input", SKY_DATA_TYPE_OBJECT, NULL,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "byteorder", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "final", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));

    sky_module_setattr(module,
            "charmap_build",
            sky_function_createbuiltin(
                    "charmap_build",
                    NULL,
                    (sky_native_code_function_t)sky_codec_charmap_build,
                    SKY_DATA_TYPE_OBJECT,
                    "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_module_setattr(module,
            "charmap_decode",
            sky_function_createbuiltin(
                    "charmap_decode",
                    NULL,
                    (sky_native_code_function_t)sky__codecs_charmap_decode,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "input", SKY_DATA_TYPE_OBJECT, NULL,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "mapping", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_module_setattr(module,
            "charmap_encode",
            sky_function_createbuiltin(
                    "charmap_encode",
                    NULL,
                    (sky_native_code_function_t)sky__codecs_charmap_encode,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "input", SKY_DATA_TYPE_OBJECT, NULL,
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                    "mapping", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));

#define DECODER(_name, _final)                                          \
    sky_module_setattr(module,                                          \
        #_name,                                                         \
        sky_function_createbuiltin(                                     \
            #_name,                                                     \
            NULL,                                                       \
            (sky_native_code_function_t)sky__codecs_##_name,            \
            SKY_DATA_TYPE_OBJECT_TUPLE,                                 \
            "input", SKY_DATA_TYPE_OBJECT, NULL,                        \
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,    \
            "final", SKY_DATA_TYPE_BOOL, _final,                        \
            NULL))

#define ENCODER(_name)                                                  \
    sky_module_setattr(module,                                          \
        #_name,                                                         \
        sky_function_createbuiltin(                                     \
            #_name,                                                     \
            NULL,                                                       \
            (sky_native_code_function_t)sky__codecs_##_name,            \
            SKY_DATA_TYPE_OBJECT_TUPLE,                                 \
            "input", SKY_DATA_TYPE_OBJECT, NULL,                        \
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,    \
            NULL))

    DECODER(ascii_decode, sky_True);
    ENCODER(ascii_encode);
    DECODER(escape_decode, sky_True);
    ENCODER(escape_encode);
    DECODER(latin_1_decode, sky_True);
    ENCODER(latin_1_encode);
    DECODER(raw_unicode_escape_decode, sky_True);
    ENCODER(raw_unicode_escape_encode);
    ENCODER(readbuffer_encode);
    DECODER(unicode_escape_decode, sky_True);
    ENCODER(unicode_escape_encode);
    DECODER(unicode_internal_decode, sky_True);
    ENCODER(unicode_internal_encode);
    DECODER(utf_16_decode, sky_False);
    DECODER(utf_16_be_decode, sky_False);
    DECODER(utf_16_le_decode, sky_False);
    ENCODER(utf_16_encode);
    ENCODER(utf_16_be_encode);
    ENCODER(utf_16_le_encode);
    DECODER(utf_32_decode, sky_False);
    DECODER(utf_32_be_decode, sky_False);
    DECODER(utf_32_le_decode, sky_False);
    ENCODER(utf_32_encode);
    ENCODER(utf_32_be_encode);
    ENCODER(utf_32_le_encode);
    DECODER(utf_7_decode, sky_False);
    ENCODER(utf_7_encode);
    DECODER(utf_8_decode, sky_False);
    ENCODER(utf_8_encode);

#undef ENCODER
#undef DECODER
}
