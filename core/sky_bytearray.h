/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_bytearray Mutable Byte Array Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BYTEARRAY_H__
#define __SKYTHON_CORE_SKY_BYTEARRAY_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Combine two bytearray objects together, resulting in a new bytearray object.
  * The @a other object to be combined with @a self may be any object that
  * supports the buffer protocol, which of course includes bytearray objects.
  *
  * @param[in]  self    the first object to add.
  * @param[in]  other   the second object to add.
  * @return     a new bytearray object that is the concatenation of @a self and
  *             @a other.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_add(sky_bytearray_t self, sky_object_t other);


/** Append a byte to the end of a bytearray object.
  *
  * @param[in]  self    the bytearray object to which the byte is to be
  *                     appended.
  * @param[in]  x       the byte to append, which must be in the range 0 to
  *                     256.
  */
SKY_EXTERN void
sky_bytearray_append(sky_bytearray_t self, ssize_t x);


/** Return the byte at a specific index position in a bytearray object.
  *
  * @param[in]  self    the bytearray object to query.
  * @param[in]  index   the index position to query.
  * @return     the byte located at the requested index position.
  */
SKY_EXTERN uint8_t
sky_bytearray_byteat(sky_bytearray_t self, ssize_t index);


/** Capitalize a bytearray object as if it were an ASCII string.
  * A capitalized bytearray object is the same as a capitalized string, which
  * is the first character uppercased and all remaining characters lowercased.
  *
  * @param[in]  self    the bytearray object to capitalize.
  * @return     a new bytearray object that is @a self capitalized.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_capitalize(sky_bytearray_t self);


/** Center a bytearray object.
  *
  * @param[in]  self        the bytearray object to center.
  * @param[in]  width       the width in which to center @a self.
  * @param[in]  fillchar    the byte value to use to fill the leading and
  *                         trailing area after centering. This should be a
  *                         bytes or bytearray object of length 1.
  * @return     a bytearray object containing @a self centered.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_center(sky_bytearray_t self, ssize_t width, sky_object_t fillchar);


/** Delete all of the bytes in a bytearray object, leaving it empty.
  *
  * @param[in]  self        the bytearray object to be cleared.
  */
SKY_EXTERN void
sky_bytearray_clear(sky_bytearray_t self);


/** Compare a bytearray object with another object.
  * Any object supporting the buffer protocol may be compared with @a self.
  * Possible return values are @c sky_True, @c sky_False, and
  * @c sky_NotImplemented, which will be returned if @a other does not support
  * the buffer protocol.
  *
  * @param[in]  self        the bytearray object to compare with @a other.
  * @param[in]  other       the object to compare with @a self.
  * @param[in]  compare_op  the comparison operation to perform.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not support the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_compare(sky_bytearray_t   self,
                      sky_object_t      other,
                      sky_compare_op_t  compare_op);


/** Determine whether an arbitrary string of bytes is contained within a
  * bytearray object.
  *
  * @param[in]  self    the bytearray object to check.
  * @param[in]  item    the item to look for, which may be any object that
  *                     supports the buffer protocol.
  * @return     @c SKY_TRUE if @a item is contained within @a self, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_contains(sky_bytearray_t self, sky_object_t item);


/** Return a copy of a bytearray object, converting it to a true bytearray
  * object if it's an instance of a bytearray sub-type.
  *
  * @param[in]  self    the bytearray object to copy.
  * @return     a bytearray object instance that is a copy of @a self.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_copy(sky_bytearray_t self);


/** Count the number of times a string of bytes occurs in a bytearray object.
  *
  * @param[in]  self    the bytearray object in which the number of occurrences
  *                     of @a sub is to be counted.
  * @param[in]  sub     the string of bytearray to count occurrences of.
  * @param[in]  start   the starting offset (zero-based, inclusive).
  * @param[in]  end     the ending offset (zero-based, exclusive).
  * @return     the number of times @a sub occurs in @a self[@a start:@a end].
  */
SKY_EXTERN ssize_t
sky_bytearray_count(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end);


/** Create a new bytearray object instance from an array of bytes in memory.
  * The new bytearray object instance will make a copy of the byte array, which
  * the bytearray object instance will own and free when it is finalized.
  *
  * @param[in]  bytes       an array of bytes to be copied.
  * @param[in]  nbytes      the number of bytes to copy.
  * @return     the new bytearray object instance.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_createfrombytes(const void *bytes, size_t nbytes);


/** Create a new bytearray object instance with a reference to an array of bytes
  * in memory.
  * The new bytearray object instance will store a reference to the array of
  * bytes. The bytearray API expects that the content of the underlying array
  * will not change while the bytearray object exists.
  *
  * @param[in]  bytes       an array of bytes.
  * @param[in]  nbytes      the number of bytes in the array.
  * @param[in]  free        a function to call to release the @a bytes array
  *                         when the bytearray object instance is finalized or
  *                         resized. May be @c NULL.
  * @return     the new bytearray object instance.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_createwithbytes(void *        bytes,
                              size_t        nbytes,
                              sky_free_t    free);


/** Create a new bytearray object instance using a format string.
  * The array of bytes used is created via sky_format_asprintf(), and the
  * bytearray object instance will free the array when it is finalized.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ...         additional arguments as required by the format
  *                         string.
  * @return     the new bytearray object instance.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_createfromformat(const char *format, ...);


/** Create a new bytearray object instance using a format string.
  * The array of bytes used is created via sky_format_asprintf(), and the
  * bytearray object instance will free the array when it is finalized.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ap          additional arguments as required by the format
  *                         string.
  * @return     the new bytearray object instance.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_createfromformatv(const char *format, va_list ap);


/** Create a new bytearray object using an arbitrary object as its data source.
  * An object capable of being used to create a bytearray object either
  * implements the buffer protocol or is an iterable containing only integers
  * in the range of 0 to 255.
  *
  * @param[in]  object      the object to use as a data source.
  * @return     a new bytearray object instance created from @a object.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_createfromobject(sky_object_t object);


/** Decode a bytearray object using a specified encoding, resulting in a string.
  *
  * @param[in]  self        the bytearray object to be decoded.
  * @param[in]  encoding    the encoding to use, which may be @c NULL to use
  *                         the default (UTF-8).
  * @param[in]  errors      how to handle decoding errors, which may be @c NULL
  *                         to use the default ("strict").
  * @return     a new string object instance decoded from @a self.
  */
SKY_EXTERN sky_string_t
sky_bytearray_decode(sky_bytearray_t    self,
                     sky_string_t       encoding,
                     sky_string_t       errors);


/** Delete a portion of a bytearray object.
  * This is the implementation of __delitem__().
  *
  * @param[in]  self        the bytearray object to slice.
  * @param[in]  item        the slice of @a self to delete, which may be a
  *                         number object implementing __index__() or a slice
  *                         object.
  */
SKY_EXTERN void
sky_bytearray_delitem(sky_bytearray_t self, sky_object_t item);


/** Determine whether a bytearray object ends with a specified run of bytes.
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  suffix      the suffix for comparison, which must be either an
  *                         object supporting the buffer protocol or a tuple of
  *                         objects supporting the buffer protocol.
  * @param[in]  start       the starting offset of @a self to compare.
  * @param[in]  end         the ending offset of @a self to compare.
  * @return     @c SKY_TRUE if @a self[@a start:@a end] ends with @a suffix, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_endswith(sky_bytearray_t  self,
                       sky_object_t     suffix,
                       ssize_t          start,
                       ssize_t          end);


/** Compare a bytearray object with another object for equality.
  * This is the implementation of __eq__(self, other). It's simply implemented
  * as a call to sky_bytearray_compare(self, other, @c SKY_COMPARE_OP_EQUAL).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_eq(sky_bytearray_t self, sky_object_t other);


/** Expand tab characters to spaces.
  *
  * @param[in]  self        the bytearray object to expand.
  * @param[in]  tabsize     the tab size to use.
  * @return     a new bytearray object with tabs expanded to spaces.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_expandtabs(sky_bytearray_t self, ssize_t tabsize);


/** Extend a bytearray object from a sequence of integers.
  * The source data, @a x, may be any sequence of integers. If it is an object
  * that supports the buffer protocol, the buffer protocol will be used as the
  * source data; otherwise, iterating @a x must produce integers in the range
  * 0 to 255. The source data is appended to the end of @a self.
  *
  * @param[in]  self        the bytearray object to extend.
  * @param[in]  x           the source of the data to append to @a self.
  */
SKY_EXTERN void
sky_bytearray_extend(sky_bytearray_t self, sky_object_t x);


/** Find the starting offset of a run of bytes in a bytearray object.
  *
  * @param[in]  self        the bytearray object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytearray_find(sky_bytearray_t  self,
                   sky_object_t     sub,
                   ssize_t          start,
                   ssize_t          end);


/** Create a new bytearray object from a string of hex digits.
  *
  * @param[in]  cls         the type of object to instantiate, which must be
  *                         @c sky_bytearray_type or a sub-type.
  * @param[in]  string      the string of hex digits to convert, which may 
  *                         contain whitespace, but otherwise must contain an
  *                         even number of hex digits for conversion (e.g.,
  *                         fromhex('61 62 63') -> b'abc'
  * @return     a new object instance of type @a cls initialized from @a string.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_fromhex(sky_type_t cls, sky_string_t string);


/** Compare a bytearray object with another object for greater than or equality.
  * This is the implementation of __ge__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytearray_compare(@a self, @a other, @c SKY_COMPARE_OP_GREATER_EQUAL).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_ge(sky_bytearray_t self, sky_object_t other);


/** Return a portion of a bytearray object.
  * This is the implementation of __getitem__().
  *
  * @param[in]  self        the bytearray object to slice.
  * @param[in]  item        the slice of @a self to return, which may be a
  *                         number object implementing __index__(), or a slice
  *                         object.
  * @return     if @a item is a number, the return will be an integer object
  *             representing the byte at the requested offset; otherwise, a
  *             new bytearray object that is the requested slice will be
  *             returned.
  */
SKY_EXTERN sky_object_t
sky_bytearray_getitem(sky_bytearray_t self, sky_object_t item);


/** Compare a bytearray object with another object for greater than.
  * This is the implementation of __gt__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytearray_compare(@a self, @a other, @c SKY_COMPARE_OP_GREATER).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_gt(sky_bytearray_t self, sky_object_t other);


/** Concatenante a bytearray object in place with another object.
  * The @a other object to be combined with @a self may be any object that
  * supports the buffer protocol, which of course includes bytearray objects.
  *
  * @param[in]  self    the first object to add.
  * @param[in]  other   the second object to add.
  * @return     the returned object will always be @a self.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_iadd(sky_bytearray_t self, sky_object_t other);


/** Multiply a byteobject in place.
  * This is equivalent to sky_bytearray_mul(), except that the repeating bytes
  * are appended to @a self in place instead of creating a new bytearray object
  * with the repeated bytes.
  *
  * @param[in]  self        the bytearray object to repeat.
  * @param[in]  other       a number object implementing __index__() that is
  *                         the number of times to multiply @a self.
  * @return     the returned object will always be @a self.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_imul(sky_bytearray_t self, sky_object_t other);


/** Find the starting offset of a run of bytes in a bytearray object.
  * This function is the same as sky_bytearray_find(), except that
  * @c sky_IndexError will be raised if @a sub is not found instead of
  * returning -1.
  *
  * @param[in]  self        the bytearray object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self.
  */
SKY_EXTERN ssize_t
sky_bytearray_index(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end);


/** Insert a byte into a bytearray object.
  *
  * @param[in]  self        the bytearray object into which @a x is to be
  *                         inserted.
  * @param[in]  index       the index position at which to insert @a x.
  * @param[in]  x           the byte to insert into @a self.
  */
SKY_EXTERN void
sky_bytearray_insert(sky_bytearray_t self, ssize_t index, ssize_t x);


/** Determine whether a bytearray object contains only ASCII alpha-numeric
  * characters.
  *
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII alpha-numeric characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_isalnum(sky_bytearray_t self);


/** Determine whether a bytearray object contains only ASCII alphabetic
  * characters.
  *
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII alphabetic characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_isalpha(sky_bytearray_t self);


/** Determine whether a bytearray object contains only ASCII digit characters.
  *
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII digit characters, @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_isdigit(sky_bytearray_t self);


/** Determine whether a bytearray object contains only ASCII lowercase cased
  * characters.
  *
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are both ASCII cased and lowercase characters,
  *             @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_islower(sky_bytearray_t self);


/** Determine whether a bytearray object contains only ASCII whitepace
  * characters.
  * 
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII whitespace characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_isspace(sky_bytearray_t self);


/** Determine whether a bytearray object contains ASCII title cased character.
  * A run of bytes is title cased if it contains at least one cased character,
  * uppercase characters follow only non-cased characters, and all other cased
  * characters are lowercase. For example, b'23. Illuminated History' is title
  * cased.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_istitle(sky_bytearray_t self);


/** Determine whether a bytearray object contains only ASCII uppercase cased
  * characters.
  *
  * @param[in]  self        the bytearray object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are both ASCII cased and uppercase characters,
  *             @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_isupper(sky_bytearray_t self);


/** Combine a sequence of bytearray objects together into a single bytearray
  * object.
  * All of the objects yielded by iterating @a iterable must support the buffer
  * protocol. The bytearray object @a self is inserted in between each object
  * from @a iterable.
  *
  * @param[in]  self        the bytearray object to use as a separator between
  *                         each object yielded by iterating @a iterable.
  * @param[in]  iterable    an iterable object that must yield only objects
  *                         supporting the buffer protocol.
  * @return     a new bytearray object that is the concatenation of all objects
  *             from @a iterable with @a self inserted between each.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_join(sky_bytearray_t self, sky_object_t iterable);


/** Return an iterator for a bytearray object.
  * This is the implementation of __iter__().
  *
  * @param[in]  self        the bytearray object for which an iterator object
  *                         is to be returned.
  */
SKY_EXTERN sky_object_t
sky_bytearray_iter(sky_bytearray_t self);


/** Compare a bytearray object with another object for less than or equality.
  * This is the implementation of __le__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytearray_compare(@a self, @a other, @c SKY_COMPARE_OP_LESS_EQUAL).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_le(sky_bytearray_t self, sky_object_t other);


/** Return the number of bytes in a bytearray object.
  * This is the implementation of __len__().
  *
  * @param[in]  self        the bytearray object for which the number of bytes
  *                         it contains is to be returned.
  * @return     the number of bytes in @a self.
  */
SKY_EXTERN ssize_t
sky_bytearray_len(sky_bytearray_t self);


/** Left justify a bytearray object.
  *
  * @param[in]  self        the bytearray object to be left justified.
  * @param[in]  width       the (minimum) width of the resulting left justified
  *                         bytearray object.
  * @param[in]  fillchar    the character to use to fill the space between the
  *                         length of @a self and @a width. This should be a
  *                         bytes or bytearray object of length 1.
  * @return     a new bytearray object that is @a self left justified, or
  *             @a self itself if it is longer than @a width.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_ljust(sky_bytearray_t self, ssize_t width, sky_object_t fillchar);


/** Convert the case of all cased characters in a bytearray object to uppercase.
  * Bytes are treated as ASCII characters.
  *
  * @param[in]  self        the bytearray object for which the case of all cased
  *                         characters are to be converted to uppercase.
  * @return     a new bytearray object with the case of all cased characters
  *             converted to uppercase.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_lower(sky_bytearray_t self);


/** Strip leading characters from a bytearray object.
  *
  * @param[in]  self        the bytearray object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytearray object that is @a self stripped of leading
  *             whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_lstrip(sky_bytearray_t self, sky_object_t bytes);


/** Compare a bytearray object with another object for less than.
  * This is the implementation of __lt__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytearray_compare(@a self, @a other, @c SKY_COMPARE_OP_LESS).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_lt(sky_bytearray_t self, sky_object_t other);


/** Multiply a bytearray object.
  * This is the implementation of __mul__(), and is primarily a wrapper around
  * a call to sky_bytearray_repeat().
  *
  * @param[in]  self        the bytearray object to multiply.
  * @param[in]  other       a number object implementing __index__() that is
  *                         the number of times to multiply @a self.
  * @return     a new bytearray object that is @a self multiplied the number of
  *             times specified by @a other.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_mul(sky_bytearray_t self, sky_object_t other);


/** Compare a bytearray object with another object for inequality.
  * This is the implementation of __ne__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytearray_compare(@a self, @a other, @c SKY_COMPARE_OP_NOT_EQUAL).
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytearray_ne(sky_bytearray_t self, sky_object_t other);


/** Partition a bytearray object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with @a self and two empty bytearray objects.
  *
  * @param[in]  self        the bytearray object to partition.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             @a self and two empty bytearray objects.
  */
SKY_EXTERN sky_tuple_t
sky_bytearray_partition(sky_bytearray_t self, sky_object_t sep);


/** Remove a byte from a bytearray object and return it.
  *
  * @param[in]  self        the bytearray object from which a byte is to be
  *                         removed.
  * @param[in]  index       the index position from which the byte is to be
  *                         removed.
  * @return     the byte that was removed.
  */
SKY_EXTERN uint8_t
sky_bytearray_pop(sky_bytearray_t self, ssize_t index);


/** Remove the first occurrence of a particular byte value from a bytearray
  * object.
  *
  * @param[in]  self        the bytearray object from which a byte is to be
  *                         removed.
  * @param[in]  x           the byte that is to be removed.
  */
SKY_EXTERN void
sky_bytearray_remove(sky_bytearray_t self, ssize_t x);


/** Return a bytearray object repeated a specified number of times.
  * If the repeat count is less than or equal to zero, the result will be the
  * empty bytearray object. If the repeat count is one, the result will be the
  * original bytearray object.
  *
  * @param[in]  self        the bytearray object to be repeated.
  * @param[in]  count       the number of times to repeat @a self.
  * @return     a bytearray object that is @a self repeated @a count times.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_repeat(sky_bytearray_t self, ssize_t count);


/** Replace all non-overlapping occurrences of one byte sequence with another
  * one in a bytearray object.
  *
  * @param[in]  self        the bytearray object in which @a old_object is to
  *                         be replaced with @a new_object.
  * @param[in]  old_object  the byte sequence to replace, which may be any
  *                         object supporting the buffer protocol.
  * @param[in]  new_object  the byte sequence to replace @a old_object with,
  *                         which may be any object supporting the buffer
  *                         protocol.
  * @param[in]  count       if >= 0, the maximum number of occurrences of
  *                         @a old_object to replace; otherwise, there is no
  *                         limit.
  * @return     a new bytearray object with @a count occurrences of
  *             @a old_object in @a self replaced by @a new_object.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_replace(sky_bytearray_t   self,
                      sky_object_t      old_object,
                      sky_object_t      new_object,
                      ssize_t           count);


/** Return a string representation of a bytearray object.
  * This is the implementation of __repr__().
  *
  * @param[in]  self        the bytearray object for which a string
  *                         representation is to be returned.
  * @return     the string representation of @a self.
  */
SKY_EXTERN sky_string_t
sky_bytearray_repr(sky_bytearray_t self);


/** Reverse the order of the bytes in a bytearray object.
  * The bytes contained by the bytearray object are reversed in place.
  *
  * @param[in]  self        the bytearray object to reverse.
  */
SKY_EXTERN void
sky_bytearray_reverse(sky_bytearray_t self);


/** Find the starting offset of a run of bytes in a bytearray object.
  * This function is the same as sky_bytearray_find(), except that the search
  * is performed from the end of @a self.
  *
  * @param[in]  self        the bytearray object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytearray_rfind(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end);


/** Find the starting offset of a run of bytes in a bytearray object.
  * This function is the same as sky_bytearray_rfind(), except that
  * @c sky_IndexError will be raised if @a sub is not found instead of
  * returning -1.
  *
  * @param[in]  self        the bytearray object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytearray_rindex(sky_bytearray_t    self,
                     sky_object_t       sub,
                     ssize_t            start,
                     ssize_t            end);


/** Right justify a bytearray object.
  *
  * @param[in]  self        the bytearray object to be right justified.
  * @param[in]  width       the (minimum) width of the resulting right
  *                         justified bytearray object.
  * @param[in]  fillchar    the character to use to fill the space between the
  *                         length of @a self and @a width. This should be a
  *                         bytes or bytearray object of length 1.
  * @return     a new bytearray object that is @a self right justified, or
  *             @a self itself if it is longer than @a width.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_rjust(sky_bytearray_t self, ssize_t width, sky_object_t fillchar);


/** Partition a bytearray object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with two empty bytearray objects and @a self. This function works
  * the same as sky_bytearray_partition(), except that the search for @a sep is
  * done in reverse.
  *
  * @param[in]  self        the bytearray object to partition.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             two empty bytearray objects and @a self.
  */
SKY_EXTERN sky_tuple_t
sky_bytearray_rpartition(sky_bytearray_t self, sky_object_t sep);


/** Split a bytearray object by a separator.
  * This function is the same as sky_bytearray_split(), except that it performs
  * the splits in reverse. If the number of splits is less than @a maxsplit,
  * the results from the two functions are identical. If no separator is
  * specified, splits will be done by runs of whitespace.
  *
  * @param[in]  self        the bytearray object to split.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol. May also be @c NULL
  *                         (or @c sky_None) to perform splits by runs of
  *                         whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, b'123 456 789'.split()
  *             would yield [b'123', b'456', b'789'].
  */
SKY_EXTERN sky_list_t
sky_bytearray_rsplit(sky_bytearray_t self, sky_object_t sep, ssize_t maxsplit);


/** Strip trailing characters from a bytearray object.
  *
  * @param[in]  self        the bytearray object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytearray object that is @a self stripped of trailing
  *             whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_rstrip(sky_bytearray_t self, sky_object_t bytes);


/** Assign to a portion of a bytearray object.
  * This is the implementation of __setitem__().
  *
  * @param[in]  self        the bytearray object to assign to.
  * @param[in]  item        the slice of @a self to assign to, which may be a
  *                         number object implementing __index__(), or a slice
  *                         object.
  * @param[in]  value       the value to assign, which must be a single integer
  *                         in byte range (0 to 255, inclusive) if @a item is a
  *                         single index position; otherwise, it may be an
  *                         object supporting the buffer protocol, a sequence
  *                         of integers, or a single integer that will be used
  *                         as the number of 0 bytes to use for source data.
  */
SKY_EXTERN void
sky_bytearray_setitem(sky_bytearray_t   self,
                      sky_object_t      item,
                      sky_object_t      value);


/** Return the amount of memory a bytearray object is consuming.
  * This is the implementation of __sizeof__().
  *
  * @param[in]  self        the bytearray object for which the amount of memory
  *                         is returned.
  * @return     the amount of memory that @a self is consuming.
  */
SKY_EXTERN size_t
sky_bytearray_sizeof(sky_bytearray_t self);


/** Return a slice of a bytearray object.
  *
  * @param[in]  self        the bytearray object to slice.
  * @param[in]  start       the starting index of the slice (inclusive).
  * @param[in]  stop        the stopping index of the slice (exclusive).
  * @param[in]  step        the step of the slice.
  * @return     a new bytearray object that is equivalent to
  *             self[start:stop:step].
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_slice(sky_bytearray_t self,
                    ssize_t         start,
                    ssize_t         stop,
                    ssize_t         step);


/** Return a slice of a bytearray object as a bytes object.
  *
  * @param[in]  self        the bytearray object to slice.
  * @param[in]  start       the starting index of the slice (inclusive).
  * @param[in]  stop        the stopping index of the slice (exclusive).
  * @param[in]  step        the step of the slice.
  * @return     a new bytes object that is equivalent to
  *             bytes(self[start:stop:step]).
  */
SKY_EXTERN sky_bytes_t
sky_bytearray_slice_asbytes(sky_bytearray_t self,
                            ssize_t         start,
                            ssize_t         stop,
                            ssize_t         step);


/** Split a bytearray object by a separator.
  *
  * @param[in]  self        the bytearray object to split.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol. May also be @c NULL
  *                         (or @c sky_None) to perform splits by runs of
  *                         whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, b'123 456 789'.split()
  *             would yield [b'123', b'456', b'789'].
  */
SKY_EXTERN sky_list_t
sky_bytearray_split(sky_bytearray_t self, sky_object_t sep, ssize_t maxsplit);


/** Split a bytearray object by line endings.
  * Line endings are '\\r' (carriage return), '\\n' (newline) , or '\\r\\n'
  * (crlf).
  *
  * @param[in]  self        the bytearray object to split.
  * @param[in]  keepends    if @c SKY_TRUE, line endings will be included with
  *                         each line; otherwise, line endings will be stripped
  *                         from the results.
  * @return     a list object containing slices of @a self split by line
  *             endings.
  */
SKY_EXTERN sky_list_t
sky_bytearray_splitlines(sky_bytearray_t self, sky_bool_t keepends);


/** Determine whether a bytearray object starts with a specified run of bytes.
  *
  * @param[in]  self        the bytearray object to compare.
  * @param[in]  prefix      the prefix for comparison, which must be either an
  *                         object supporting the buffer protocol or a tuple of
  *                         objects supporting the buffer protocol.
  * @param[in]  start       the starting offset of @a self to compare.
  * @param[in]  end         the ending offset of @a self to compare.
  * @return     @c SKY_TRUE if @a self[@a start:@a end] startss with @a suffix,
  *             or @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytearray_startswith(sky_bytearray_t    self,
                         sky_object_t       prefix,
                         ssize_t            start,
                         ssize_t            end);


/** Strip leading and trailing trailing characters from a bytearray object.
  *
  * @param[in]  self        the bytearray object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytearray object that is @a self stripped of leading and
  *             trailing whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_strip(sky_bytearray_t self, sky_object_t bytes);


/** Swap the case of all cased characters in a bytearray object.
  * All uppercase characters are converted to lowercase and vice versa. Bytes
  * are treated as ASCII characters.
  *
  * @param[in]  self        the bytearray object for which the case of all
  *                         cased characters are to be swapped.
  * @return     a new bytearray object with the cases of all cased characters
  *             in @a self swapped.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_swapcase(sky_bytearray_t self);


/** Convert a bytearray object to be titlecased.
  * A titlecased string is one where a cased character following a non-cased
  * character is converted to uppercase, and all other cased characters are
  * converted to lowercase.
  *
  * @param[in]  self        the bytearray object to convert to titlecase.
  * @return     a new bytearray object that is @a self converted to titlecase.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_title(sky_bytearray_t self);


/** Translate a bytearray object using a translation table.
  * The function sky_bytearray_maketrans() can be used to create a translation
  * table suitable for use with this function. The translation table is
  * required, and can be any object supporting the buffer protocol. The table
  * must have 256 entries, one for each byte value. If @a deletechars is
  * neither @c NULL nor @c sky_None, it must be an object supporting the buffer
  * protocol, and any byte that it contains will be removed from the result.
  *
  * @param[in]  self        the bytearray object to translate.
  * @param[in]  table       the translation table to use, which must be any
  *                         object supporting the buffer protocol, and it must
  *                         be exactly 256 bytes.
  * @param[in]  deletechars an optional set of bytes to delete from @a self in
  *                         the resulting bytearray object.
  * @return     a new bytearray object that is @a self translated by @a table,
  *             and possibly having bytes removed if @a deletechars is supplied
  *             and not empty.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_translate(sky_bytearray_t self,
                        sky_object_t    table,
                        sky_object_t    deletechars);


/** Convert the case of all cased characters in a bytearray object to uppercase.
  * Bytes are treated as ASCII characters.
  *
  * @param[in]  self        the bytearray object for which the case of all
  *                         cased characters are to be converted to uppercase.
  * @return     a new bytearray object with the case of all cased characters
  *             converted to uppercase.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_upper(sky_bytearray_t self);


/** Right justify a string with zeros, preserving the sign, if present.
  * This is really just a special purpose version of sky_bytearray_rjust() using
  * using a fillchar of '0', and keeping any sign ('-' or '+' that may be
  * present at the start of the resulting bytearray object.
  *
  * @param[in]  self        the bytearray object to be zero filled.
  * @param[in]  width       the width to make the resulting bytearray object.
  * @return     a new bytearray object that is @a self padded with '0' bytes to
  *             make it as wide as @a width.
  */
SKY_EXTERN sky_bytearray_t
sky_bytearray_zfill(sky_bytearray_t self, ssize_t width);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BYTEARRAY_H__ */

/** @} **/
/** @} **/
