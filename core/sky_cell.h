/* @file
 * @brief
 * @defgroup sky_object Objects
 * @{
 * @defgroup sky_cell Cell Objects
 * @{
 */

#ifndef __SKYTHON_CORE_SKY_CELL_H__
#define __SKYTHON_CORE_SKY_CELL_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Return the contents of a cell. **/
SKY_EXTERN sky_object_t
sky_cell_contents(sky_cell_t cell);


/** Create a new cell object. **/
SKY_EXTERN sky_cell_t
sky_cell_create(sky_object_t object);


/** Set the contents of a cell. **/
SKY_EXTERN void
sky_cell_setcontents(sky_cell_t cell, sky_object_t object);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CELL_H__ */

/** @} **/
/** @} **/
