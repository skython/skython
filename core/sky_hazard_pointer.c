#include "sky_private.h"


/* Once allocated, hazard pointer structures are never deallocated, but instead
 * are re-used as needed. Never deallocating a hazard pointer structure is
 * very important in order for the implementation to function correctly without
 * the need for complicated synchronization.
 *
 * There is a global list of allocated hazard pointer structures that is used
 * at scan time to build a complete set of all protected pointers. The global
 * list is organized in blocks so that an additional pointer is not required in
 * the hazard pointer structure to maintain its position in the global list.
 * The global list is not used for anything other than scanning.
 *
 * Each thread maintains a private list of unused hazard pointers that starts
 * out empty. As a thread allocates hazard pointers and releases them, the
 * released hazard pointers will go into the thread's private free list so that
 * future allocations can hopefully avoid having to use expensive cas operations
 * to acquire a hazard pointer. When the thread dies, all of its available
 * hazard pointers will become orphaned for other threads to adopt as needed.
 *
 * Each thread maintains a private list of retired pointers that's added to by
 * each call to sky_hazard_pointer_retire(). When enough pointers have been
 * retired, an automatic scan will be triggered to clean up as many of the
 * thread's retired pointers as possible. A retired pointer can be cleaned up
 * when there are no hazard pointers protecting it.
 *
 * Finally, a count of how many hazard pointers are active is maintained for
 * each thread. If a thread terminates with an active hazard pointer count
 * other than 0, sky_error_fatal() will be called. This is a resource leak
 * that must be fixed. Threads should never terminate normally with active
 * hazards.
 */


static int32_t                          sky_hazard_pointer_count             = 0;
static volatile sky_bool_t              sky_hazard_pointer_scanning          = SKY_FALSE;
static sky_hazard_pointer_block_t *     sky_hazard_pointer_blocks            = NULL;
static sky_hazard_pointer_set_t *       sky_hazard_pointer_scan_set          = NULL;
static size_t                           sky_hazard_pointer_scan_set_capacity = 0;

static sky_hazard_pointer_private_t *   sky_hazard_pointer_allocate(void);
static void                             sky_hazard_pointer_retire_internal(sky_hazard_pointer_tlsdata_t *tlsdata, void *pointer, sky_free_t free);
static void                             sky_hazard_pointer_scan_internal(sky_hazard_pointer_tlsdata_t * volatile tlsdata);


static inline void
sky_hazard_pointer_retiree_block_become_head(
        sky_hazard_pointer_tlsdata_t *      tlsdata,
        sky_hazard_pointer_retiree_block_t *block)
{
    /* Don't move the block if it is already the head, or if its prev block has
     * slots available.
     */
    if (block != tlsdata->retiree_blocks_head &&
        block->prev->inuse_count == block->prev->avail_count)
    {
        *(block->next ? &(block->next->prev)
                      : &(tlsdata->retiree_blocks_tail)) = block->prev;
        block->prev->next = block->next;
        block->next = tlsdata->retiree_blocks_head;
        block->prev = NULL;
        tlsdata->retiree_blocks_head->prev = block;
        tlsdata->retiree_blocks_head = block;
    }
}


static inline void
sky_hazard_pointer_retiree_block_become_tail(
        sky_hazard_pointer_tlsdata_t *      tlsdata,
        sky_hazard_pointer_retiree_block_t *block)
{
    /* Don't move the block if it is already the tail, or if its next block is
     * also "full".
     */
    if (block != tlsdata->retiree_blocks_tail &&
        block->next->inuse_count != block->next->avail_count)
    {
        block->next->prev = block->prev;
        *(block->prev ? &(block->prev->next)
                      : &(tlsdata->retiree_blocks_head)) = block->next;
        block->next = NULL;
        block->prev = tlsdata->retiree_blocks_tail;
        tlsdata->retiree_blocks_tail->next = block;
        tlsdata->retiree_blocks_tail = block;
    }
}


static sky_hazard_pointer_retiree_t *
sky_hazard_pointer_retiree_acquire(sky_hazard_pointer_tlsdata_t *tlsdata)
{
    sky_hazard_pointer_retiree_t        *retiree;
    sky_hazard_pointer_retiree_block_t  *block;

    for (block = tlsdata->retiree_blocks_head;  block;  block = block->next) {
        if ((retiree = block->freelist) != NULL) {
            block->freelist = retiree->next;
            if (++block->inuse_count == block->avail_count) {
                sky_hazard_pointer_retiree_block_become_tail(tlsdata, block);
            }
            return retiree;
        }
        if (block->inuse_index < block->avail_count) {
            retiree = &(block->retirees[block->inuse_index++]);
            retiree->block = block;
            if (++block->inuse_count == block->avail_count) {
                sky_hazard_pointer_retiree_block_become_tail(tlsdata, block);
            }
            return retiree;
        }
    }

    block = sky_system_alloc(sky_system_pagesize());
    block->next = tlsdata->retiree_blocks_head;
    block->prev = NULL;
    block->avail_count = (sky_system_pagesize() -
                          sizeof(sky_hazard_pointer_retiree_block_t)) /
                         sizeof(sky_hazard_pointer_retiree_t);
    block->inuse_count = 1;
    block->inuse_index = 1;

    *(tlsdata->retiree_blocks_head ? &(tlsdata->retiree_blocks_head->prev)
                                   : &(tlsdata->retiree_blocks_tail)) = block;
    tlsdata->retiree_blocks_head = block;

    retiree = &(block->retirees[0]);
    retiree->block = block;
    return retiree;
}


static void
sky_hazard_pointer_retiree_release(sky_hazard_pointer_tlsdata_t *   tlsdata,
                                   sky_hazard_pointer_retiree_t *   retiree)
{
    sky_hazard_pointer_retiree_block_t  *block;

    block = retiree->block;
    retiree->next = block->freelist;
    block->freelist = retiree;

    if (!--block->inuse_count) {
        sky_hazard_pointer_retiree_block_become_head(tlsdata, block);
    }
}


static void **
sky_hazard_pointer_set_lookup(sky_hazard_pointer_set_t * restrict   set,
                              void * restrict                       pointer)
    /* The collision resolution algorithm is taken from Python 3.1.2. */
    /* (Specifically, see sky_set.c - this is a special-case implementation) */
{
    void    **table_item;
    size_t  perturb, table_index;

    table_index = perturb = (ssize_t)pointer >> SKY_CACHE_LINE_BITS;
    for (;;) {
        table_item = &(set->table[table_index & set->capacity_mask]);
        if (!*table_item || *table_item == pointer) {
            return table_item;
        }

        table_index = (table_index << 2) + table_index + perturb + 1;
        perturb >>= 5;
    }

    sky_error_fatal("internal error");
}


static inline void
sky_hazard_pointer_set_add(sky_hazard_pointer_set_t * restrict  set,
                           void * restrict                      pointer)
{
    *sky_hazard_pointer_set_lookup(set, pointer) = pointer;
}


static inline sky_bool_t
sky_hazard_pointer_set_contains(sky_hazard_pointer_set_t * restrict set,
                                void * restrict                     pointer)
{
    return (*sky_hazard_pointer_set_lookup(set, pointer) ? SKY_TRUE
                                                         : SKY_FALSE);
}


sky_hazard_pointer_t *
sky_hazard_pointer_acquire(void * volatile *address)
{
    sky_hazard_pointer_t            *hazard;
    sky_hazard_pointer_private_t    *hazard_private;
    sky_hazard_pointer_tlsdata_t    *tlsdata;

    tlsdata = &(sky_tlsdata_get()->hazard_pointer_tlsdata);
    if ((hazard_private = tlsdata->unused_hazard_pointers) != NULL) {
        tlsdata->unused_hazard_pointers = hazard_private->next;
    }
    else {
        hazard_private = sky_hazard_pointer_allocate();
    }
    ++tlsdata->active_hazard_count;

    hazard = (sky_hazard_pointer_t *)hazard_private;
    if (address != NULL) {
        sky_hazard_pointer_update(hazard, address);
    }

    return hazard;
}


sky_hazard_pointer_private_t *
sky_hazard_pointer_allocate(void)
{
    size_t                      nbytes;
    int32_t                     new_inuse_count, old_inuse_count;
    sky_hazard_pointer_block_t  *block, *new_block;

    for (;;) {
        block = sky_hazard_pointer_blocks;
        if (likely(block != NULL)) {
            old_inuse_count = block->inuse_count;
            if (old_inuse_count < block->avail_count) {
                new_inuse_count = old_inuse_count + 1;
                if (sky_atomic_cas32(old_inuse_count,
                                     new_inuse_count,
                                     &(block->inuse_count)))
                {
                    sky_atomic_increment32(&sky_hazard_pointer_count);
                    return &(block->hazard_pointers[old_inuse_count]);
                }
            }
        }

        /* Create a new block and try to add it to the head of the list of
         * blocks. If that fails, discard the new block and start all over.
         */
        nbytes = sky_system_pagesize();
        new_block = memset(sky_system_alloc(nbytes), 0x00, nbytes);
        new_block->next = block;
        new_block->avail_count =
                (int32_t)(nbytes - sizeof(sky_hazard_pointer_block_t)) /
                          sizeof(sky_hazard_pointer_private_t);
        new_block->inuse_count = 1;
        if (sky_atomic_cas(block,
                           new_block,
                           SKY_AS_VOIDP(&sky_hazard_pointer_blocks)))
        {
            sky_atomic_increment32(&sky_hazard_pointer_count);
            return &(new_block->hazard_pointers[0]);
        }
        sky_system_free(new_block, nbytes);
    }
}


void
sky_hazard_pointer_tlsdata_adopt(
        sky_hazard_pointer_tlsdata_t *          tlsdata,
        sky_hazard_pointer_tlsdata_orphan_t *   orphan_tlsdata)
{
    sky_hazard_pointer_private_t        *hazard_private;
    sky_hazard_pointer_retiree_t        *retiree;
    sky_hazard_pointer_retiree_block_t  *retiree_block;

    while ((hazard_private = orphan_tlsdata->unused_hazard_pointers) != NULL) {
        orphan_tlsdata->unused_hazard_pointers = hazard_private->next;
        hazard_private->next = tlsdata->unused_hazard_pointers;
        tlsdata->unused_hazard_pointers = hazard_private;
    }

    while ((retiree = orphan_tlsdata->retired_pointers) != NULL) {
        orphan_tlsdata->retired_pointers = retiree->next;
        retiree->next = tlsdata->retired_pointers;
        tlsdata->retired_pointers = retiree;
        ++tlsdata->retired_pointer_count;

        /* Check to see if the block has been adopted yet. If so, do nothing.
         * Otherwise, adopt it. This is a linear check, but the number of
         * orphaned retirees should generally be extremely low, and so the
         * number of blocks involved here should most often never be more than
         * one, which will always end up at the tail, so search backwards.
         */
        for (retiree_block = tlsdata->retiree_blocks_tail;
             retiree_block && retiree_block != retiree->block;
             retiree_block = retiree_block->prev);
        if (!retiree_block) {
            /* Nope, we haven't adopted this block yet. Adopt it now. */
            retiree_block = retiree->block;
            retiree_block->next = NULL;
            if (!(retiree_block->prev = tlsdata->retiree_blocks_tail)) {
                tlsdata->retiree_blocks_head = retiree_block;
            }
            else {
                tlsdata->retiree_blocks_tail->next = retiree_block;
            }
            tlsdata->retiree_blocks_tail = retiree_block;
        }
    }
}


void
sky_hazard_pointer_tlsdata_cleanup(
        sky_hazard_pointer_tlsdata_t *          tlsdata,
        sky_hazard_pointer_tlsdata_orphan_t *   orphan_tlsdata,
        void *                                  tlsdata_pointer,
        sky_free_t                              tlsdata_free)
{
    sky_hazard_pointer_retiree_block_t  *block, *next_block;

    /* Release all active hazards. Ideally there shouldn't be any. */
    sky_error_validate(!tlsdata->active_hazard_count);

    /* Force a retirement scan to minimize the number of retired pointers. */
    if (tlsdata->retired_pointer_count > 0) {
        sky_hazard_pointer_scan_internal(tlsdata);
    }

    /* Create a retire record for tlsdata_pointer, but do so without triggering
     * a scan. It will always be left behind as an orphan.
     */
    if (tlsdata_pointer && tlsdata_free) {
        sky_hazard_pointer_retire_internal(tlsdata,
                                           tlsdata_pointer,
                                           tlsdata_free);
    }

    /* Orphan unused hazard pointers and retired pointers. */
    if (!orphan_tlsdata->unused_hazard_pointers) {
        orphan_tlsdata->unused_hazard_pointers =
                tlsdata->unused_hazard_pointers;
    }
    else {
        sky_hazard_pointer_private_t    *tail;

        for (tail = orphan_tlsdata->unused_hazard_pointers;
             tail->next;
             tail = tail->next);
        tail->next = tlsdata->unused_hazard_pointers;
    }
    orphan_tlsdata->retired_pointers = tlsdata->retired_pointers;

    /* Free all retiree blocks that are empty. */
    for (block = tlsdata->retiree_blocks_head;  block;  block = next_block) {
        next_block = block->next;
        if (!block->inuse_count) {
            *(block->next ? &(block->next->prev)
                          : &(tlsdata->retiree_blocks_tail)) = block->prev;
            *(block->prev ? &(block->prev->next)
                          : &(tlsdata->retiree_blocks_head)) = block->next;
            sky_system_free(block, sky_system_pagesize());
        }
    }

    tlsdata->unused_hazard_pointers = NULL;
    tlsdata->retired_pointers = NULL;
    tlsdata->retired_pointer_count = 0;
}


void
sky_hazard_pointer_release(sky_hazard_pointer_t *hazard)
{
    sky_hazard_pointer_private_t    *hazard_private =
                                    (sky_hazard_pointer_private_t *)hazard;

    sky_hazard_pointer_tlsdata_t    *tlsdata;

    tlsdata = &(sky_tlsdata_get()->hazard_pointer_tlsdata);
    hazard_private->pointer = NULL;
    hazard_private->next    = tlsdata->unused_hazard_pointers;
    tlsdata->unused_hazard_pointers = hazard_private;
    --tlsdata->active_hazard_count;
}


static void
sky_hazard_pointer_retire_internal(sky_hazard_pointer_tlsdata_t *   tlsdata,
                                   void *                           pointer,
                                   sky_free_t                       free)
{
    sky_hazard_pointer_retiree_t    *retiree;

    retiree = sky_hazard_pointer_retiree_acquire(tlsdata);
    retiree->pointer = pointer;
    retiree->free    = free;
    retiree->next    = tlsdata->retired_pointers;
    tlsdata->retired_pointers = retiree;
}


void
sky_hazard_pointer_retire(void *pointer, sky_free_t free)
{
    sky_hazard_pointer_tlsdata_t    *tlsdata;

    if (likely(likely(pointer != NULL) && likely(free != NULL))) {
        tlsdata = &(sky_tlsdata_get()->hazard_pointer_tlsdata);
        sky_hazard_pointer_retire_internal(tlsdata, pointer, free);
        if (++tlsdata->retired_pointer_count >= (sky_hazard_pointer_count >> 1)) {
            sky_hazard_pointer_scan_internal(tlsdata);
        }
    }
}


void
sky_hazard_pointer_scan(void)
{
    sky_hazard_pointer_tlsdata_t    *tlsdata;

    tlsdata = &(sky_tlsdata_get()->hazard_pointer_tlsdata);
    sky_hazard_pointer_scan_internal(tlsdata);
}


static void
sky_hazard_pointer_scan_internal(
        sky_hazard_pointer_tlsdata_t * volatile tlsdata)
{
    void                            *pointer;
    size_t                          capacity, old_nbytes, set_nbytes;
    int32_t                         i;
    sky_hazard_pointer_set_t        *hazard_set;
    sky_hazard_pointer_block_t      *block;
    sky_hazard_pointer_retiree_t    *next_retiree, **prev_retiree, *retiree;
    sky_hazard_pointer_retiree_t    * volatile dead_retiree,
                                    * volatile dead_retirees;

    if (!sky_atomic_cas32(SKY_FALSE, SKY_TRUE, &sky_hazard_pointer_scanning)) {
        /* There is a scan in progress. Do not start another one! */
        return;
    }

    sky_tlsdata_adopt();

    /* Build a set of existing hazards. */
    capacity = sky_util_roundpow2(SKY_MAX(sky_hazard_pointer_count, 8) * 3 / 2);
    set_nbytes = sizeof(sky_hazard_pointer_set_t) + (capacity * sizeof(void *));
    if (capacity > sky_hazard_pointer_scan_set_capacity) {
        if (sky_hazard_pointer_scan_set) {
            old_nbytes = sizeof(sky_hazard_pointer_set_t) +
                         (sky_hazard_pointer_scan_set_capacity * sizeof(void *));
            sky_system_free(sky_hazard_pointer_scan_set, old_nbytes);
        }
        sky_hazard_pointer_scan_set_capacity = capacity;
        sky_hazard_pointer_scan_set = sky_system_alloc(set_nbytes);
    }
    hazard_set = memset(sky_hazard_pointer_scan_set, 0x00, set_nbytes);
    hazard_set->capacity_mask = capacity - 1;

    for (block = sky_hazard_pointer_blocks;  block;  block = block->next) {
        for (i = 0;  i < block->inuse_count;  i++) {
            if ((pointer = block->hazard_pointers[i].pointer) != NULL) {
                sky_hazard_pointer_set_add(hazard_set, pointer);
            }
        }
    }

    /* Scan through this thread's list of retirees, and clean up any that
     * do not have entries in hazard_set. Do not clean up right away,
     * because the clean up could potentially add new retirees, which would
     * break this iteration, and also possibly trigger a new scan. Instead,
     * add them to a dead list, and clean up the dead list later.
     */
    dead_retirees = NULL;
    prev_retiree  = &(tlsdata->retired_pointers);
    for (retiree = tlsdata->retired_pointers;  retiree;  retiree = next_retiree) {
        next_retiree = retiree->next;
        if (sky_hazard_pointer_set_contains(hazard_set, retiree->pointer)) {
            prev_retiree = &(retiree->next);
        }
        else {
            *prev_retiree = next_retiree;
            retiree->next = dead_retirees;
            dead_retirees = retiree;
            --tlsdata->retired_pointer_count;
        }
    }

    /* Clean up all of the dead retirees. */
    while ((dead_retiree = dead_retirees) != NULL) {
        dead_retiree->free(dead_retiree->pointer);
        dead_retirees = dead_retiree->next;
        sky_hazard_pointer_retiree_release(tlsdata, dead_retiree);
    }

    sky_hazard_pointer_scanning = SKY_FALSE;
}


void
sky_hazard_pointer_update(sky_hazard_pointer_t *hazard,
                          void * volatile *     address)
{
    void    *pointer;

    do {
        pointer = *address;
        hazard->pointer = pointer;
    } while (pointer != *address);
}
