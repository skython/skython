/** @file
  * @brief Asset Management
  * @defgroup sky_error Error Handling
  * @{
  * @defgroup sky_asset Asset Management
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ASSET_H__
#define __SKYTHON_CORE_SKY_ASSET_H__ 1


#include "sky_base.h"
#include "sky_error.h"


SKY_CDECLS_BEGIN


/** Begin a new asset management block.
  * A new asset management block will be started, meaning that assets may be
  * saved within the block to be cleaned up when the block ends. The block will
  * not be considered when looking for an error handler for an exception, but
  * the assets it's tracking will be cleaned up as the stack is unwound.
  */
#define SKY_ASSET_BLOCK_BEGIN                                               \
    do {                                                                    \
        sky_error_block_t   __asset_block;                                  \
                                                                            \
        sky_error_block_push(&__asset_block, SKY_ERROR_BLOCK_TYPE_ASSET);   \
        do {

/** End an asset management block.
  * The current asset management block started with @c SKY_ASSET_BLOCK_BEGIN
  * will be ended, cleaning up all assets saved within the block as appropriate.
  * Assets saved with @c SKY_ASSET_CLEANUP_ON_ERROR will not be cleaned up
  * unless the block is ending due to the stack being unwound on an exception.
  */
#define SKY_ASSET_BLOCK_END                                                 \
        } while (0);                                                        \
        sky_error_block_pop();                                              \
    } while (0)


/** Constants that determine how to clean up assets when an asset management
  * block ends.
  */
typedef enum sky_asset_cleanup_e {
    SKY_ASSET_CLEANUP_NEVER         =   0,  /**< never clean up the asset                       **/
    SKY_ASSET_CLEANUP_ALWAYS        =   1,  /**< always clean up the asset                      **/
    SKY_ASSET_CLEANUP_ON_ERROR      =   2,  /**< only clean up the asset if an error is raised. **/
    SKY_ASSET_CLEANUP_ON_SUCCESS    =   3,  /**< only clean up the asset if no error is raised. **/
} sky_asset_cleanup_t;

/** Constants that determine how to update asset pointers. **/
typedef enum sky_asset_update_e {
    SKY_ASSET_UPDATE_FIRST_CURRENT  =   1,  /**< update only the first instance found in the current block. **/
    SKY_ASSET_UPDATE_FIRST_DEEP     =   2,  /**< update only the first instance found in all blocks.        **/
    SKY_ASSET_UPDATE_ALL_CURRENT    =   3,  /**< update all instances found in the current block.           **/
    SKY_ASSET_UPDATE_ALL_DEEP       =   4,  /**< update all instances found in all blocks.                  **/
} sky_asset_update_t;


/** Save an asset to be cleaned up when the current block ends.
  * Assets can be saved for automatic cleanup when an error block ends. An
  * asset is an arbitrary pointer that may be @c NULL. Stored with each asset
  * is a function pointer to be called to clean up the asset if and when it's
  * appropriate.
  *
  * @param[in]  asset_pointer   an arbitrary pointer that is the asset to save.
  * @param[in]  asset_free      the function to call to clean up the asset.
  * @param[in]  asset_cleanup   may be @c SKY_ASSET_CLEANUP_NEVER to never clean
  *                             up the asset (this is a convenience, and basically
  *                             basically does nothing),
  *                             @c SKY_ASSET_CLEANUP_ALWAYS to
  *                             always clean up the asset,
  *                             @c SKY_ASSET_CLEANUP_ON_ERROR to only clean up
  *                             the asset when an exception is raised, or
  *                             @c SKY_ASSET_CLEANUP_ON_SUCCESS to only clean
  *                             up the asset if no exception is raised.
  */
SKY_EXTERN void
sky_asset_save(void *               asset_pointer,
               sky_free_t           asset_free,
               sky_asset_cleanup_t  asset_cleanup);

/** Update a saved asset's pointer.
  * Should an asset's pointer change while it's being tracked (e.g., due to
  * resizing an allocation via sky_realloc()), use sky_asset_update() to alter
  * the tracked pointer.
  *
  * @param[in] old_asset_pointer    the asset's tracked pointer value.
  * @param[in] new_asset_pointer    the pointer value to change the asset to.
  * @param[in] update_mode          how to perform the update.
  */
SKY_EXTERN void
sky_asset_update(void *             old_asset_pointer,
                 void *             new_asset_pointer,
                 sky_asset_update_t update_mode);


/** Conveniently combine calls to sky_calloc() and sky_asset_save(). **/
SKY_EXTERN void *
sky_asset_calloc(size_t count, size_t nbytes, sky_asset_cleanup_t cleanup);

/** Conveniently combine calls to sky_malloc() and sky_asset_save(). **/
SKY_EXTERN void *
sky_asset_malloc(size_t nbytes, sky_asset_cleanup_t cleanup);

/** Conveniently combine calls to sky_memalign() and sky_asset_save(). **/
SKY_EXTERN void *
sky_asset_memalign(size_t alignment, size_t nbytes, sky_asset_cleanup_t clean);

/** Conveniently combine calls to sky_memdup() and sky_asset_save(). **/
SKY_EXTERN void *
sky_asset_memdup(const void *bytes, size_t nbytes, sky_asset_cleanup_t cleanup);

/** Conveniently combine calls to sky_realloc() and sky_asset_save() or
  * sky_asset_update().
  */
SKY_EXTERN void *
sky_asset_realloc(void *                old_pointer,
                  size_t                new_nbytes,
                  sky_asset_cleanup_t   cleanup,
                  sky_asset_update_t    update);

/** Conveniently combine calls to sky_strdup() and sky_asset_save(). **/
SKY_EXTERN char *
sky_asset_strdup(const char *string, sky_asset_cleanup_t cleanup);

/** Conveniently combine calls to sky_strndup() and sky_asset_save(). **/
SKY_EXTERN char *
sky_asset_strndup(const char *          string,
                  size_t                string_length,
                  sky_asset_cleanup_t   cleanup);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ASSET_H__ */

/** @} **/
/** @} **/
