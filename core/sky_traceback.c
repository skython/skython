#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"
#include "sky_python_compiler.h"
#include "sky_string_private.h"


typedef struct sky_traceback_data_s {
    sky_traceback_t                     tb_next;
    sky_frame_t                         tb_frame;
    ssize_t                             tb_lasti;
    int                                 tb_lineno;
} sky_traceback_data_t;

SKY_EXTERN_INLINE sky_traceback_data_t *
sky_traceback_data(sky_object_t object)
{
    return sky_object_data(object, sky_traceback_type);
}

struct sky_traceback_s {
    sky_object_data_t                   object_data;
    sky_traceback_data_t                traceback_data;
};


static void
sky_traceback_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_traceback_data_t    *self_data = data;

    sky_object_visit(self_data->tb_frame, visit_data);
    sky_object_visit(self_data->tb_next, visit_data);
}


sky_traceback_t
sky_traceback_create(sky_traceback_t next_traceback, sky_frame_t frame)
{
    sky_frame_data_t    *frame_data = sky_frame_data(frame);

    sky_traceback_t         traceback;
    sky_traceback_data_t    *traceback_data;

    traceback = sky_object_allocate(sky_traceback_type);
    traceback_data = sky_traceback_data(traceback);
    sky_object_gc_set(SKY_AS_OBJECTP(&(traceback_data->tb_next)),
                      next_traceback,
                      traceback);
    sky_object_gc_set(SKY_AS_OBJECTP(&(traceback_data->tb_frame)),
                      frame,
                      traceback);
    traceback_data->tb_lasti = frame_data->f_lasti;
    if (!sky_object_isa(frame_data->f_code, sky_code_type)) {
        traceback_data->tb_lineno = 1;
    }
    else {
        traceback_data->tb_lineno = sky_code_lineno(frame_data->f_code,
                                                    frame_data->f_lasti);
    }

    return traceback;
}


sky_list_t
sky_traceback_dir(SKY_UNUSED sky_object_t self)
{
    return sky_object_build("[ssss]",
                            "tb_next", "tb_frame", "tb_lasti", "tb_lineno");
}


sky_traceback_t
sky_traceback_filter_importlib(sky_traceback_t traceback, sky_type_t exception)
{
    sky_bool_t              filter_all;
    sky_string_t            codename, filename;
    sky_code_data_t         *code_data;
    sky_traceback_t         prev_tb, *prev_tb_link, tb;
    sky_frame_data_t        *frame_data;
    sky_traceback_data_t *  tb_data;

    if (!exception ||
        (sky_interpreter_flags() & SKY_LIBRARY_INITIALIZE_FLAG_VERBOSE))
    {
        return traceback;
    }

    codename = SKY_STRING_LITERAL("_call_with_frames_removed");
    filename = SKY_STRING_LITERAL("<frozen importlib._bootstrap>");

    filter_all = sky_type_issubtype(exception, sky_ImportError);
    prev_tb = NULL;
    prev_tb_link = &traceback;

    for (tb = traceback; tb; tb = tb_data->tb_next) {
        tb_data = sky_traceback_data(tb);
        frame_data = sky_frame_data(tb_data->tb_frame);
        code_data = sky_code_data(frame_data->f_code);

        if (sky_object_compare(code_data->co_filename,
                               filename,
                               SKY_COMPARE_OP_NOT_EQUAL))
        {
            prev_tb = tb;
            prev_tb_link = &(tb_data->tb_next);
        }
        else if (filter_all ||
                 sky_object_compare(code_data->co_name,
                                    codename,
                                    SKY_COMPARE_OP_EQUAL))
        {
            if (prev_tb) {
                sky_object_gc_set(SKY_AS_OBJECTP(prev_tb_link),
                                  tb_data->tb_next,
                                  prev_tb);
            }
            else {
                *prev_tb_link = tb_data->tb_next;
                traceback = tb_data->tb_next;
            }
        }
    }

    return traceback;
}


void
sky_traceback_print(sky_traceback_t self, sky_object_t file)
{
    volatile ssize_t    depth = 0, limit = 1000;

    sky_string_t                    line;
    sky_code_data_t                 *code_data;
    sky_traceback_t volatile        tb;
    sky_frame_data_t                *frame_data;
    sky_traceback_data_t * volatile tb_data;

    sky_object_t            attr;
    sky_object_t volatile   write_method;

    if (sky_object_isnull(file)) {
        file = sky_object_getattr(sky_interpreter_module_sys(),
                                  SKY_STRING_LITERAL("stderr"),
                                  sky_NotSpecified);
    }
    write_method = sky_object_getattr(file,
                                      SKY_STRING_LITERAL("write"),
                                      sky_NotSpecified);

    attr = sky_object_getattr(sky_interpreter_module_sys(),
                              SKY_STRING_LITERAL("tracebacklimit"),
                              NULL);
    if (attr) {
        SKY_ERROR_TRY {
            limit = sky_integer_value(attr, 0, SSIZE_MAX, NULL);
        } SKY_ERROR_EXCEPT(sky_OverflowError) {
        } SKY_ERROR_TRY_END;
    }

    sky_object_call(write_method, 
                    sky_object_build("(s)",
                                     "Traceback (most recent call last):\n"),
                    NULL);

    for (tb = self; tb; tb = tb_data->tb_next) {
        tb_data = sky_traceback_data(tb);
        ++depth;
    }
    for (tb = self; depth > limit; tb = tb_data->tb_next) {
        tb_data = sky_traceback_data(tb);
        --depth;
    }
    while (tb) {
        tb_data = sky_traceback_data(tb);
        frame_data = sky_frame_data(tb_data->tb_frame);
        code_data = sky_code_data(frame_data->f_code);

        /* Ignore errors to prevent recursion */
        SKY_ERROR_TRY {
            line = sky_string_createfromformat(
                            "  File \"%@\", line %d, in %@\n",
                            code_data->co_filename,
                            tb_data->tb_lineno,
                            code_data->co_name);
            sky_object_call(write_method, sky_tuple_pack(1, line), NULL);
            line = sky_python_compiler_line(code_data->co_filename,
                                            tb_data->tb_lineno);
            if (sky_object_bool(line)) {
                line = sky_string_strip(line, NULL);
                line = sky_string_createfromformat("    %@\n", line);
                sky_object_call(write_method, sky_tuple_pack(1, line), NULL);
            }
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;

        tb = tb_data->tb_next;
    }
}


static inline void
sky_traceback_dump_puts(int fd, const char *string)
{
    write(fd, string, strlen(string));
}

static void
sky_traceback_dump_decimal(int fd, intmax_t value)
{
    char    buffer[24]; /* enough for a 64-bit value, signed or unsigned */
    size_t  nbytes;

    sky_error_validate_debug(sizeof(intmax_t) <= 8);
    if (!value) {
        sky_traceback_dump_puts(fd, "0");
        return;
    }

    nbytes = sky_util_itoa(value, buffer, sizeof(buffer));
    sky_error_validate_debug(nbytes <= sizeof(buffer));

    write(fd, buffer, nbytes);
}

static void
sky_traceback_dump_hexadecimal(int fd, uintmax_t value, size_t width)
{
    char        buffer[sizeof(value) * 2];
    size_t      length, pos;
    uintmax_t   tmp;

    if (width > sizeof(buffer)) {
        width = sizeof(buffer);
    }
    memset(buffer, '0', sizeof(buffer));
    if (!value) {
        write(fd, buffer, width);
        return;
    }

    for (length = 0, tmp = value; tmp; ++length, tmp >>= 4);
    for (pos = length, tmp = value; tmp; tmp >>= 4) {
        buffer[--pos] = sky_ctype_lower_hexdigits[(int)(tmp & 0xF)];
    }

    write(fd, buffer, SKY_MAX(length, width));
}

static void
sky_traceback_dump_string(int fd, sky_string_t string)
{
    char                buffer[10];
    sky_string_data_t   *string_data, tagged_string_data;

    string_data = sky_string_data_noctype(string, &tagged_string_data);
    if (string_data->highest_codepoint <= 0x7F) {
        write(fd, string_data->data.ascii, string_data->len);
        return;
    }

    buffer[0] = '\\';
    if (string_data->highest_codepoint < 0x100) {
        const uint8_t   *c, *end, *start;

        start = string_data->data.latin1;
        end = start + string_data->len;
        for (c = start; c < end; ++c) {
            if (*c <= 0x7F) {
                continue;
            }

            buffer[1] = 'x';
            buffer[2] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
            buffer[3] = sky_ctype_lower_hexdigits[*c & 0xF];
            write(fd, start, c - start);
            write(fd, buffer, 4);
            start = c + 1;
        }
        if (start > c) {
            write(fd, start, c - start);
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        const uint16_t  *c, *end;

        end = string_data->data.ucs2 + string_data->len;
        for (c = string_data->data.ucs2; c < end; ++c) {
            if (*c < 0x7F) {
                buffer[1] = (char)*c;
                write(fd, &(buffer[1]), 1);
            }
            else if (*c < 0x100) {
                buffer[1] = 'x';
                buffer[2] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
                buffer[3] = sky_ctype_lower_hexdigits[*c & 0xF];
                write(fd, buffer, 4);
            }
            else {
                buffer[1] = 'u';
                buffer[2] = sky_ctype_lower_hexdigits[(*c >> 12) & 0xF];
                buffer[3] = sky_ctype_lower_hexdigits[(*c >> 8) & 0xF];
                buffer[4] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
                buffer[5] = sky_ctype_lower_hexdigits[*c & 0xF];
                write(fd, buffer, 6);
            }
        }
    }
    else {
        const sky_unicode_char_t    *c, *end;

        end = string_data->data.ucs4 + string_data->len;
        for (c = string_data->data.ucs4; c < end; ++c) {
            if (*c < 0x7F) {
                buffer[1] = (char)*c;
                write(fd, &(buffer[1]), 1);
            }
            else if (*c < 0x100) {
                buffer[1] = 'x';
                buffer[2] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
                buffer[3] = sky_ctype_lower_hexdigits[*c & 0xF];
                write(fd, buffer, 4);
            }
            else if (*c < 0x10000) {
                buffer[1] = 'u';
                buffer[2] = sky_ctype_lower_hexdigits[(*c >> 12) & 0xF];
                buffer[3] = sky_ctype_lower_hexdigits[(*c >> 8) & 0xF];
                buffer[4] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
                buffer[5] = sky_ctype_lower_hexdigits[*c & 0xF];
                write(fd, buffer, 6);
            }
            else {
                buffer[1] = 'U';
                buffer[2] = sky_ctype_lower_hexdigits[(*c >> 28) & 0xF];
                buffer[3] = sky_ctype_lower_hexdigits[(*c >> 24) & 0xF];
                buffer[4] = sky_ctype_lower_hexdigits[(*c >> 20) & 0xF];
                buffer[5] = sky_ctype_lower_hexdigits[(*c >> 16) & 0xF];
                buffer[6] = sky_ctype_lower_hexdigits[(*c >> 12) & 0xF];
                buffer[7] = sky_ctype_lower_hexdigits[(*c >> 8) & 0xF];
                buffer[8] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
                buffer[9] = sky_ctype_lower_hexdigits[*c & 0xF];
                write(fd, buffer, 10);
            }
        }
    }
}

static void
sky_traceback_dump_frame(int fd, sky_frame_t frame)
{
    sky_frame_data_t    *frame_data = sky_frame_data(frame);

    sky_code_data_t *code_data;

    if (!sky_object_isa(frame_data->f_code, sky_code_type)) {
        return;
    }
    code_data = sky_code_data(frame_data->f_code);

    sky_traceback_dump_puts(fd, "  File ");
    if (sky_object_isa(code_data->co_filename, sky_string_type)) {
        sky_traceback_dump_string(fd, code_data->co_filename);
    }
    else {
        sky_traceback_dump_puts(fd, "???");
    }

    sky_traceback_dump_puts(fd, ", line ");
    sky_traceback_dump_decimal(fd, sky_code_lineno(frame_data->f_code,
                                                   frame_data->f_lasti));

    sky_traceback_dump_puts(fd, " in ");
    if (sky_object_isa(code_data->co_name, sky_string_type)) {
        sky_traceback_dump_string(fd, code_data->co_name);
    }
    else {
        sky_traceback_dump_puts(fd, "???");
    }

    sky_traceback_dump_puts(fd, "\n");
}

static void
sky_traceback_dump_thread(int fd, sky_tlsdata_t *tlsdata, sky_bool_t header)
{
    sky_frame_t         frame;
    sky_frame_data_t    *frame_data;

    if (header) {
        sky_traceback_dump_puts(fd, "Traceback (most recent call first):\n");
    }

    for (frame = tlsdata->current_frame; frame; frame = frame_data->f_back) {
        frame_data = sky_frame_data(frame);
        sky_traceback_dump_frame(fd, frame);
    }
}


void
sky_traceback_dump(int fd)
{
    sky_traceback_dump_thread(fd, sky_tlsdata_get(), SKY_TRUE);
}


void
sky_traceback_dump_threads(int fd)
{
    sky_tlsdata_t   *current_tlsdata = sky_tlsdata_get();

    int                     count;
    uint32_t                id;
    sky_tlsdata_t           *tlsdata;
    sky_hazard_pointer_t    *hazard;

    count = 0;
    for (id = 0; id < sky_tlsdata_highestid(); ++id) {
        if (!(hazard = sky_tlsdata_getid(id))) {
            continue;
        }
        if ((tlsdata = hazard->pointer) != NULL) {
            if (++count > 1) {
                write(fd, "\n", 1);
            }
            if (tlsdata == current_tlsdata) {
                sky_traceback_dump_puts(fd, "Current thread 0x");
            }
            else {
                sky_traceback_dump_puts(fd, "Thread 0x");
            }
            sky_traceback_dump_hexadecimal(fd,
                                           (uintmax_t)tlsdata->pthread_self,
                                           sizeof(uintmax_t) * 2);
            sky_traceback_dump_thread(fd, tlsdata, SKY_FALSE);
        }
        sky_hazard_pointer_release(hazard);
    }
}


SKY_TYPE_DEFINE_SIMPLE(traceback,
                       "traceback",
                       sizeof(sky_traceback_data_t),
                       NULL,
                       NULL,
                       sky_traceback_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_traceback_initialize_library(void)
{
    sky_type_initialize_builtin(sky_traceback_type, 0);

    sky_type_setmembers(sky_traceback_type,
            "tb_next",      NULL,
                            offsetof(sky_traceback_data_t, tb_next),
                            SKY_DATA_TYPE_OBJECT_INSTANCEOF,
                            SKY_MEMBER_FLAG_READONLY,
                            sky_traceback_type,
            "tb_frame",     NULL,
                            offsetof(sky_traceback_data_t, tb_frame),
                            SKY_DATA_TYPE_OBJECT_INSTANCEOF,
                            SKY_MEMBER_FLAG_READONLY,
                            sky_frame_type,
            "tb_lasti",     NULL,
                            offsetof(sky_traceback_data_t, tb_lasti),
                            SKY_DATA_TYPE_SSIZE_T,
                            SKY_MEMBER_FLAG_READONLY,
            "tb_lineno",    NULL,
                            offsetof(sky_traceback_data_t, tb_lineno),
                            SKY_DATA_TYPE_INT,
                            SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setmethodslots(sky_traceback_type,
            "__dir__", sky_traceback_dir,
            NULL);
}
