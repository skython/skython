#ifndef __SKYTHON_CORE_SKY_PARAMETER_LIST_PRIVATE_H__
#define __SKYTHON_CORE_SKY_PARAMETER_LIST_PRIVATE_H__ 1


SKY_CDECLS_BEGIN


typedef struct sky_parameter_list_item_s sky_parameter_list_item_t;
struct sky_parameter_list_item_s {
    sky_hashtable_item_t                base;
    sky_parameter_list_item_t *         next;
    ssize_t                             index;
    sky_string_t                        keyword;
    sky_object_t                        data_type_extra;
    sky_data_type_t                     data_type;
};


typedef struct sky_parameter_list_data_s {
    sky_hashtable_t                     keywords;
    sky_parameter_list_item_t *         first_keyword;
    sky_parameter_list_item_t *         last_keyword;
    sky_object_t                        defaults;
    sky_dict_t                          kwdefaults;

    ssize_t                             positional_count;
    ssize_t                             keyword_count;
    ssize_t                             args_index;
    ssize_t                             kws_index;
} sky_parameter_list_data_t;

SKY_EXTERN_INLINE sky_parameter_list_data_t *
sky_parameter_list_data(sky_object_t object)
{
    if (sky_parameter_list_type == sky_object_type(object)) {
        return (sky_parameter_list_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_parameter_list_type);
}

struct sky_parameter_list_s {
    sky_object_data_t                   object_data;
    sky_parameter_list_data_t           parameter_list_data;
};


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PARAMETER_LIST_PRIVATE_H__ */
