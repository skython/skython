#ifndef __SKYTHON_CORE_SKY_POINTER_SET_H__
#define __SKYTHON_CORE_SKY_POINTER_SET_H__ 1


#include "sky_base.h"
#include "sky_hashtable.h"


SKY_CDECLS_BEGIN


typedef struct sky_pointer_set_item_s {
    sky_hashtable_item_t                base;
    void *                              pointer;
} sky_pointer_set_item_t;

typedef struct sky_pointer_set_s sky_pointer_set_t;
struct sky_pointer_set_s {
    sky_hashtable_t                     table;
    sky_pointer_set_t *                 next;
};


extern sky_bool_t
sky_pointer_set_add(sky_pointer_set_t *set, void *pointer);

extern void
sky_pointer_set_cleanup(sky_pointer_set_t *set);

extern sky_pointer_set_t *
sky_pointer_set_create(void);

extern sky_bool_t
sky_pointer_set_delete(sky_pointer_set_t *set, void *pointer);

extern void
sky_pointer_set_destroy(sky_pointer_set_t *set);

extern void
sky_pointer_set_init(sky_pointer_set_t *set);

extern void
sky_pointer_set_insert(sky_pointer_set_t *      set,
                       sky_hashtable_item_t *   item,
                       uintptr_t                pointer_hash,
                       void *                   pointer);

extern void
sky_pointer_set_merge(sky_pointer_set_t *set, sky_pointer_set_t *other);

extern void *
sky_pointer_set_pop_pointer(sky_pointer_set_t *set);


SKY_EXTERN_INLINE uintptr_t
sky_pointer_set_key_hash(void *key)
{
    return (uintptr_t)key >> 4;
}


SKY_EXTERN_INLINE sky_bool_t
sky_pointer_set_contains(sky_pointer_set_t *set, void *pointer)
{
    return (sky_hashtable_lookup(&(set->table),
                                 sky_pointer_set_key_hash(pointer),
                                 pointer) ? SKY_TRUE : SKY_FALSE);
}

SKY_EXTERN_INLINE size_t
sky_pointer_set_length(sky_pointer_set_t *set)
{
    return (set ? sky_hashtable_length(&(set->table)) : 0);
}

SKY_EXTERN_INLINE sky_pointer_set_item_t *
sky_pointer_set_pop(sky_pointer_set_t *set)
{
    return (sky_pointer_set_item_t *)
           sky_hashtable_delete(&(set->table), 0, NULL);
}


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_POINTER_SET_H__ */
