#include "sky_private.h"


typedef struct sky_getset_data_s {
    sky_type_t                          type;
    sky_string_t                        qualname;
    sky_string_t                        name;
    sky_string_t                        doc;
    sky_getset_get_t                    get;
    sky_getset_set_t                    set;
} sky_getset_data_t;

SKY_EXTERN_INLINE sky_getset_data_t *
sky_getset_data(sky_object_t object)
{
    if (sky_getset_type == sky_object_type(object)) {
        return (sky_getset_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_getset_type);
}


static void
sky_getset_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_getset_data_t   *getset_data = data;

    sky_object_visit(getset_data->type, visit_data);
    sky_object_visit(getset_data->qualname, visit_data);
    sky_object_visit(getset_data->name, visit_data);
    sky_object_visit(getset_data->doc, visit_data);
}


void
sky_getset_delete(sky_object_t self, sky_object_t instance)
{
    sky_getset_data_t   *getset_data = sky_getset_data(self);

    if (!sky_object_isa(instance, getset_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                getset_data->name,
                sky_type_name(getset_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    if (!getset_data->set) {
        sky_error_raise_format(
                sky_AttributeError,
                "attribute %#@ of %#@ objects is read-only",
                getset_data->name,
                sky_type_name(getset_data->type));
    }

    getset_data->set(instance, NULL, getset_data->type);
}


static sky_object_t
sky_getset_qualname_getter(sky_object_t instance, SKY_UNUSED sky_type_t type)
{
    sky_getset_data_t   *getset_data = sky_getset_data(instance);

    if (!getset_data->qualname) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(getset_data->qualname)),
                sky_string_createfromformat(
                        "%@.%@",
                        sky_type_qualname(sky_object_type(instance)),
                        getset_data->name),
                instance);
    }

    return getset_data->qualname;
}


sky_object_t
sky_getset_get(           sky_object_t  self,
                          sky_object_t  instance,
               SKY_UNUSED sky_type_t    type)
{
    sky_getset_data_t   *getset_data = sky_getset_data(self);

    if (!instance) {
        return self;
    }
    if (!sky_object_isa(instance, getset_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                getset_data->name,
                sky_type_name(getset_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    if (!getset_data->get) {
        sky_error_raise_format(
                sky_AttributeError,
                "attribute %#@ of %#@ objects is not readable",
                getset_data->name,
                sky_type_name(getset_data->type));
    }

    return getset_data->get(instance, getset_data->type);
}


sky_string_t
sky_getset_repr(sky_object_t self)
{
    sky_getset_data_t   *getset_data = sky_getset_data(self);

    return sky_string_createfromformat("<attribute %#@ of %#@ objects>",
                                       getset_data->name,
                                       sky_type_name(getset_data->type));
}


void
sky_getset_set(sky_object_t self, sky_object_t instance, sky_object_t value)
{
    sky_getset_data_t   *getset_data = sky_getset_data(self);

    if (!sky_object_isa(instance, getset_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                getset_data->name,
                sky_type_name(getset_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    if (!getset_data->set) {
        sky_error_raise_format(
                sky_AttributeError,
                "attribute %#@ of %#@ objects is read-only",
                getset_data->name,
                sky_type_name(getset_data->type));
    }

    getset_data->set(instance, value, getset_data->type);
}


sky_getset_t
sky_getset_create(sky_type_t        type,
                  const char *      name,
                  const char *      doc,
                  sky_getset_get_t  get,
                  sky_getset_set_t  set)
{
    sky_getset_t        getset;
    sky_getset_data_t   *getset_data;

    getset = sky_object_allocate(sky_getset_type);
    getset_data = sky_getset_data(getset);
    getset_data->get = get;
    getset_data->set = set;

    sky_error_validate(sky_object_isa(type, sky_type_type));
    sky_object_gc_set(SKY_AS_OBJECTP(&(getset_data->type)), type, getset);

    sky_error_validate(NULL != name);
    sky_object_gc_set(
            SKY_AS_OBJECTP(&(getset_data->name)),
            sky_string_createfrombytes(name, strlen(name), NULL, NULL),
            getset);

    if (doc) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(getset_data->doc)),
                sky_string_createfrombytes(doc, strlen(doc), NULL, NULL),
                getset);
    }

    return getset;
}


SKY_TYPE_DEFINE_SIMPLE(getset,
                       "getset_descriptor",
                       sizeof(sky_getset_data_t),
                       NULL,
                       NULL,
                       sky_getset_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_getset_initialize_library(void)
{
    sky_type_setattr_builtin(sky_getset_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_getset_type,
            "__repr__", sky_getset_repr,
            "__get__", sky_getset_get,
            "__set__", sky_getset_set,
            "__delete__", sky_getset_delete,
            NULL);

    sky_type_setattr_getset(sky_getset_type,
                            "__qualname__",
                            NULL,
                            sky_getset_qualname_getter,
                            NULL);

    sky_type_setmembers(sky_getset_type,
            "__doc__",      NULL, offsetof(sky_getset_data_t, doc),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__name__",     NULL, offsetof(sky_getset_data_t, type),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__objclass__", NULL, offsetof(sky_getset_data_t, type),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);
}
