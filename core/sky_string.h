/** @file
  * @brief
  * Note that significant portions of the documention here are lifted directly
  * from Python documentation.
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_string String Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_STRING_H__
#define __SKYTHON_CORE_SKY_STRING_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_unicode.h"


SKY_CDECLS_BEGIN


/** A string object instance that is the empty string (i.e., ""). **/
SKY_EXTERN sky_string_t const sky_string_empty;


/** Create a new string by combining two strings together.
  *
  * @param[in]  string      the first string to combine.
  * @param[in]  other       the second string to combine.
  * @return     a new string object instance that is @a string and @a other
  *             combined together.
  */
SKY_EXTERN sky_string_t
sky_string_add(sky_string_t string, sky_string_t other);


/** Convert a string to capitalized form.
  * The capitalized form of a string is defined to be the first character of
  * the string converted to uppercase with all remaining cased characters
  * converted to lowercase.
  *
  * @param[in]  string      the string to be converted.
  * @return     a string object instacne that is @a string capitalized.
  */
SKY_EXTERN sky_string_t
sky_string_capitalize(sky_string_t string);


/** Convert a string into its case-folded form suitable for performing
  * case-insensitive comparisons.
  * Case-folded strings are useful for performing case-insensitive string
  * matching. Case-folding is similar to lowercasing but more aggressive
  * because it is intended to remove all case distinctions from a string.
  * The case-folding algorithm is described in section 3.13 of the Unicode
  * Standard.
  *
  * @param[in]  string      the string to be case-folded.
  * @return     a string object instance that is @a string case-folded.
  */
SKY_EXTERN sky_string_t
sky_string_casefold(sky_string_t string);


/** Center a string.
  *
  * @param[in]  self        the string to center.
  * @param[in]  width       the width in which to center @a self.
  * @param[in]  fillchar    the character to use to fill the leading and
  *                         trailing area after centering. This must be a
  *                         string of length 1.
  * @return     a string object containing @a self centered.
  */
SKY_EXTERN sky_string_t
sky_string_center(sky_string_t  self,
                  ssize_t       width,
                  sky_string_t  fillchar);


/** Return the code point at a specific index position in a string object
  *
  * @param[in]  string      the string object to query.
  * @param[in]  index       the index position to query.
  * @return     the code point located at the requested index position.
  */
SKY_EXTERN sky_unicode_char_t
sky_string_charat(sky_string_t string, ssize_t index);


/** Create a copy of a string instance.
  * Given that string objects are immutable, it would seem pointless for this
  * function to exist at all; however, the function is primarily useful for
  * creating an actual string instance from an object instance of a type that
  * is a subtype of the string type.
  *
  * @param[in]  string      the string to copy.
  * @return     a new string object instance that is a copy of @a string.
  */
SKY_EXTERN sky_string_t
sky_string_copy(sky_string_t string);


/** Count the number of occurrences of a string within another string.
  *
  * @param[in]  string      the string in which @a sub is to be counted.
  * @param[in]  sub         the substring to count.
  * @param[in]  start       the starting position in @a string to consider.
  * @param[in]  end         the ending position in @a string to consider.
  * @param[in]  maxcount    the maximum number of occurrences to count.
  * @return     the number of non-overlapping occurrences of @a sub in
  *             @a string less than or equal to @a maxcount.
  */
SKY_EXTERN ssize_t
sky_string_count(sky_string_t   string,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end,
                 ssize_t        maxcount);


/** Create a new string object instance from an array of bytes in memory.
  * The array of bytes will be decoded into Unicode using the encoding that's
  * specified. If no encoding is specified, the default encoding will be used.
  *
  * @param[in]  bytes       the array of bytes to be decoded.
  * @param[in]  nbytes      the number of bytes in the @a bytes array.
  * @param[in]  encoding    the encoding to use. If @c NULL, UTF-8 is the
  *                         default.
  * @param[in]  errors      how to handle errors (typically "strict", "ignore",
  *                         or "replace"). If @c NULL, "strict" is the default.
  * @return     the new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createfrombytes(const void * bytes,
                           size_t       nbytes,
                           sky_string_t encoding,
                           sky_string_t errors);


/** Create a new string object instance from a single Unicode codepoint.
  *
  * @param[in]  codepoint   the codepoint to use for the string.
  * @return     a string object instance containing the single codepoint.
  */
SKY_EXTERN sky_string_t
sky_string_createfromcodepoint(sky_unicode_char_t codepoint);


/** Create a new string object instance from a C-style string containing a
  * filename.
  * This function will use the appropriate encoding and error handler to create
  * a Unicode string object from a C-style string.
  *
  * @param[in]  bytes       the array of bytes containing the filename to be
  *                         encoded.
  * @param[in]  nbytes      the number of bytes in @a bytes to consider.
  * @return     a string object instance containing the filename encoded from
  *             @a bytes.
  */
SKY_EXTERN sky_string_t
sky_string_createfromfilename(const void *bytes, size_t nbytes);


/** Create a new string object instance using a format string.
  * The format string and optional arguments are passed through
  * sky_format_asprintf() to obtain an array of bytes suitable for decoding
  * into a Unicode string. The output from sky_format_asprintf() is always
  * assumed to be UTF-8 encoded, meaning that the format string and all of the
  * arguments must be UTF-8 encoded.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ...         additional arguments as required by the format
  *                         string
  * @return     the new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createfromformat(const char *format, ...);


/** Create a new string object instance using a format string.
  * The format string and optional arguments are passed through
  * sky_format_asprintf() to obtain an array of bytes suitable for decoding
  * into a Unicode string. The output from sky_format_asprintf() is always
  * assumed to be UTF-8 encoded, meaning that the format string and all of the
  * arguments must be UTF-8 encoded.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ap          additional arguments as required by the format
  *                         string
  * @return     the new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createfromformatv(const char *format, va_list ap);


/** Create a new string object instance from an array of bytes in memory, which
  * are assumed to be a string literal at source level.
  * The array of bytes will be decoded into Unicode using UTF-8 encoding. The
  * source bytes are assumed to be a string literal at source level. The
  * behavior is largely undefined if the string is not actually a source-level
  * string literal. The most likely result is a string object that will never
  * be garbage collected.
  *
  * @param[in]  bytes       the array of bytes to be decoded.
  * @param[in]  nbytes      the number of bytes in the @a bytes array.
  * @return     the new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createfromliteral(const void *bytes, size_t nbytes);


/** Create a new string object instance from a source-level string literal.
  * This is a convenience macro around sky_string_createfromliteral() that
  * has to be implemented as a macro because it uses sizeof() to get the size
  * of the string literal.
  *
  * @param[in]  literal     the source-level string literal.
  */
#define SKY_STRING_LITERAL(literal) \
        sky_string_createfromliteral(literal, sizeof(literal) - 1)


/** Create a new string object using an arbitrary object as its data source.
  * An object capable of being used to a create a string object is another
  * string object or an instance of a string subtype. In the former case, the
  * string is returned as-is. In the latter case, a new string object instance
  * is created with the data from the specified instance.
  *
  * @param[in]  object      the object to use as a data source.
  * @return     a new string object instance created from @a object.
  */
SKY_EXTERN sky_string_t
sky_string_createfromobject(sky_object_t object);


/** Create a new string object from an array of code points.
  *
  * @param[in]  bytes       the array of code points.
  * @param[in]  nbytes      the number of bytes present in @a bytes. This is
  *                         not the number of code points!
  * @param[in]  width       the width of the code points present in @a bytes,
  *                         which must be one of 1, 2, or 4.
  * @return     a new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createfromcodepoints(const void *bytes,
                                ssize_t     nbytes,
                                size_t      width);


/** Create a new string object using an array of code points.
  * Ownership of the array of code points is taken by the new string object.
  * The array of code points must be terminated by a 0 code point that is not
  * included in the code point count. While this function avoids a new memory
  * allocation and copy, a pass is made over the code points for internal
  * accounting and to validate the data. When the string object is cleaned up,
  * it will call @a free to free @a bytes.
  *
  * @param[in]  bytes       the array of code points.
  * @param[in]  nbytes      the number of bytes present in @a bytes. This is
  *                         not the number of code points!
  * @param[in]  width       the width of the code points present in @a bytes,
  *                         which must be one of 1, 2, or 4.
  * @param[in]  free        the function to call to free @a bytes when it is
  *                         no longer needed. May be @c NULL.
  * @return     a new string object instance.
  */
SKY_EXTERN sky_string_t
sky_string_createwithcodepoints(void *      bytes,
                                ssize_t     nbytes,
                                size_t      width,
                                sky_free_t  free);


/** Return the bytes for a string as a C-style string.
  * If the string contains code points wider than a single byte, the return
  * will be @c NULL; otherwise, the result will be a read-only pointer to the
  * code points making up the string. The pointer returned is likely a pointer
  * to the string object's internal representation, so it must not be modified,
  * and it should only be considered valid as long as the string itself is
  * valid. It should not be freed by the caller.
  *
  * @param[in]  string      the string for which the code points are to
  *                         be returned.
  * @return     a pointer to the code points making up the string, or
  *             @c NULL if the string contains wide code points.
  */
SKY_EXTERN const void *
sky_string_cstring(sky_string_t string);


/** Determine if a string ends with another string.
  *
  * @param[in]  string      the string to test.
  * @param[in]  suffix      the substring to look for, which may be a tuple of
  *                         strings to try.
  * @param[in]  start       the starting position in @a string to consider.
  * @param[in]  end         the ending position in @a string to consider.
  * @return     @c SKY_TRUE if @a string[@a start:@a end] ends with @a prefix,
  *             or @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_string_endswith(sky_string_t    string,
                    sky_object_t    suffix,
                    ssize_t         start,
                    ssize_t         end);


/** Compare two strings for equality.
  *
  * @param[in]  string_1    the first string to compare.
  * @param[in]  string_2    the second string to compare.
  * @return     @c sky_True if the two strings are equal, or @c sky_False if
  *             they are not. Note that the returned value is an object, not
  *             @c sky_bool.
  */
SKY_EXTERN sky_object_t
sky_string_eq(sky_string_t string_1, sky_string_t string_2);


/** Expand tab characters to spaces.
  *
  * @param[in]   self        the string to expand.
  * @param[in]   tabsize     the tab size to use.
  * @return      a new string object with tabs expanded to spaces.
  */
SKY_EXTERN sky_string_t
sky_string_expandtabs(sky_string_t self, ssize_t tabsize);


/** Return the lowest index position of a string within a string.
  *
  * @param[in]  string      the string to be searched.
  * @param[in]  sub         the sub-string for which to search.
  * @param[in]  start       the starting index position to consider.
  * @param[in]  end         the ending index position to consider.
  * @return     the lowest index position of @a sub in @a string, or -1 if
  *             @a sub is not found.
  */
SKY_EXTERN ssize_t
sky_string_find(sky_string_t    string,
                sky_string_t    sub,
                ssize_t         start,
                ssize_t         end);


/** Format a string using argument substitutions.
  * Substitutions are identified by braces per PEP 3101.
  *
  * @param[in]  self        the format string.
  * @param[in]  args        positional arguments for substitution.
  * @param[in]  kws         keyword arguments for substitution.
  * @return     @a self formatted using substitutions from @a args and @a kws.
  */
SKY_EXTERN sky_string_t
sky_string_format(sky_string_t self, sky_tuple_t args, sky_dict_t kws);


/** Format a string using substitutions from a mapping.
  * Substitutions are identified by braces per PEP 3101.
  *
  * @param[in]  self        the format string.
  * @param[in]  map         keyword arguments for substitution.
  * @return     @a self formatted using substitutions from @a kws.
  */
SKY_EXTERN sky_string_t
sky_string_format_map(sky_string_t self, sky_object_t map);


/** Return a hash value for a string.
  *
  * @param[in]  string      the string for which a hash value is to be
  *                         returned.
  * @return     the hash value for the string.
  */
SKY_EXTERN uintptr_t
sky_string_hash(sky_string_t string);


/** Return the lowest index position of a string within a string.
  *
  * @param[in]  string      the string to be searched.
  * @param[in]  sub         the sub-string for which to search.
  * @param[in]  start       the starting index position to consider.
  * @param[in]  end         the ending index position to consider.
  * @return     the lowest index position of @a sub in @a string. If @a sub
  *             is not found, @c sky_ValueError will be raised.
  */
SKY_EXTERN ssize_t
sky_string_index(sky_string_t   string,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end);


/** Enter a string into the internal string cache.
  *
  * @param[in]  string      the string to be cached.
  * @return     the matching string object instance from the string cache if it
  *             already exists, or @a string if it's newly added to the cache.
  */
SKY_EXTERN sky_string_t
sky_string_intern(sky_string_t string);


/** Determine if a string contains only alphanumeric characters.
  * Return @c SKY_TRUE if all characters in @a string are alphanumeric and
  * there is at least one character. A character is alphanumeric if one of
  * sky_unicode_isalpha(), sky_unicode_isdecimal(), sky_unicode_isdigit(), or
  * sky_unicode_isnumeric() returns @c SKY_TRUE.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are alphanumeric.
  */
SKY_EXTERN sky_bool_t
sky_string_isalnum(sky_string_t string);


/** Determine if a string contains only alphabetic characters.
  * Return @c SKY_TRUE if all characters in @a string are alphabetic and there
  * is at least one character. Alphabetic characters are those characters
  * defined in the Unicode character database as "Letter"; i.e., those with the
  * general category property being one of "Ll", "Lm", "Lo", "Lt", or "Lu".
  * Note that this is different from the "Alphabetic" property defined in the
  * Unicode standard.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are alphabetic.
  */
SKY_EXTERN sky_bool_t
sky_string_isalpha(sky_string_t string);


/** Determine if a string contains only ASCII characters.
  * Return @c SKY_TRUE if all characters in @a string are ASCII characters.
  * ASCII characters are Unicode codepoints in the range U+0000 through U+007F,
  * inclusive.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are ASCII.
  */
SKY_EXTERN sky_bool_t
sky_string_isascii(sky_string_t string);


/** Determine if a string contains only decimal characters.
  * Return @c SKY_TRUE if all characters in @a string are decimal characters
  * and there is at least one character. Decimal characters are those
  * characters defined in the Unicode character database with the general
  * category property being "Nd". This category includes digit characters and
  * all characters that can be used to form decimal-radix numbers, e.g. U+0660,
  * ARABIC-INDIC DIGIT ZERO.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are decimal.
  */
SKY_EXTERN sky_bool_t
sky_string_isdecimal(sky_string_t string);


/** Determine if a string contains only digit characters.
  * Return @c SKY_TRUE if all characters in @a string are digit characters and
  * there is at least one character. Digit characters are those characters
  * defined in the Unicode character database having a Numeric_Type property
  * value of Digit or Decimal.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are digits.
  */
SKY_EXTERN sky_bool_t
sky_string_isdigit(sky_string_t string);


/** Determine if a string is a valid language identifier.
  * Return @c SKY_TRUE if @a string is a valid identifier according to the
  * language definition (Python language specification section 2.3, Identifiers
  * and Keywords).
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if @a string is a valid language identifier.
  */
SKY_EXTERN sky_bool_t
sky_string_isidentifier(sky_string_t string);


/** Determine if all cased characters in a string are lowercase characters.
  * Return @c SKY_TRUE if all cased characters in @a string are lowercase and
  * there is at least one cased character. A cased character is defined in the
  * Unicode character database with the general category property being one of
  * "Ll", "Lt", or "Lu".
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all cased characters in @a string are lowercase.
  */
SKY_EXTERN sky_bool_t
sky_string_islower(sky_string_t string);


/** Determine if a string contains only numeric characters.
  * Return @c SKY_TRUE if all characters in @a string are numeric characters
  * and there is at least one character. Numeric characters are those
  * characters in the Unicode character database having a Numeric_Type property
  * value of Decimal, Digit, or Numeric. For example, U+2155, VULGAR FRACTION
  * ONE FIFTH, is a numeric character.
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are numeric.
  */
SKY_EXTERN sky_bool_t
sky_string_isnumeric(sky_string_t string);


/** Determine if a string contains only printable characters.
  * Return @c SKY_TRUE if all characters in @a string are printable characters
  * or if the string is empty. Non-printable characters are those characters
  * defined in the Unicode character database as "Other" or "Separator",
  * excepting the ASCII space (U+0020), which is considered printable. Note
  * that printable characters in this context are those which should not be
  * escaped by sky_string_repr() (the string's __repr__() method).
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string or printable.
  */
SKY_EXTERN sky_bool_t
sky_string_isprintable(sky_string_t string);


/** Determine if a string contains only whitespace characters.
  * Return @c SKY_TRUE if all characters in @a string or whitespace characters.
  * Whitespace characters are those characters in the Unicode character
  * database as "Other" or "Separator" and those with the bidirectional
  * property being one of "WS", "B", or "S".
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all characters in @a string are whitespace.
  */
SKY_EXTERN sky_bool_t
sky_string_isspace(sky_string_t string);


/** Determine if a string is titlecased.
  * Return @c SKY_TRUE if the string is a titlecased string and there is at
  * least one character. A string is titled cased if it has at least one cased
  * character. If there is more than a single titled cased character, lowercase
  * characters may only follow titlecase or uppercase characters, and titlecase
  * or uppercase characters may only follow non-cased characters. A cased
  * character is defined in the Unicode character database with the general
  * category property being one of "Ll", "Lt", or "Lu".
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if @a string is titlecased.
  */
SKY_EXTERN sky_bool_t
sky_string_istitle(sky_string_t string);


/** Determine if all cased characters in a string are uppercase characters.
  * Return @c SKY_TRUE if all cased characters in @a string are uppercase and
  * there is at least one cased character. A cased character is defined in the
  * Unicode character database with the general category property being one of
  * "Ll", "Lt", or "Lu".
  *
  * @param[in]  string      the string to test.
  * @return     @c SKY_TRUE if all cased characters in @a string are uppercase.
  */
SKY_EXTERN sky_bool_t
sky_string_isupper(sky_string_t string);


/** Create a new string by combining a sequence of strings together.
  * Return a string that is the concatenation of the strings in @a iterable,
  * which must contain only strings; otherwise, a @c sky_TypeError will be
  * raised. The separator between elements will be @a string.
  *
  * @param[in]  string      the string to use as a separator between elements.
  * @param[in]  iterable    an iterable object yielding only string objects to
  *                         be combined together as one string separated by
  *                         @a string.
  * @return     a string object instance that is the concatenation of all
  *             strings in @a iterable separated by @a string.
  */
SKY_EXTERN sky_string_t
sky_string_join(sky_string_t string, sky_object_t iterable);


/** Return the length of a string as a number of code points.
  *
  * @param[in]  string      the string for which the length is to be returned.
  * @return     the length of the string as a number of code points.
  */
SKY_EXTERN ssize_t
sky_string_len(sky_string_t string);


/** Left justify a string.
  *
  * @param[in]  self        the string to left justify.
  * @param[in]  width       the (minimum) width of the resulting left justified
  *                         string.
  * @param[in]  fillchar    the charactrer to use to fill the space between the
  *                         length of @a self and @a width. This must be a
  *                         string of length 1.
  * @return a new string object that is @a self left justified, or @a itself if
  *         it is longer than @a width.
  */
SKY_EXTERN sky_string_t
sky_string_ljust(sky_string_t   self,
                 ssize_t        width,
                 sky_string_t   fillchar);


/** Convert a string into its lowercase form.
  * All cased characters present in @a string are converted to lowercase. Any
  * non-cased characters are preserved as-is. The returned string may be the
  * same as @a string if there are no cased characters present, or if they are
  * all already lowercase.
  *
  * @param[in]  string      the string to be lowercased.
  * @return     a string object instance that is @a string lowercased.
  */
SKY_EXTERN sky_string_t
sky_string_lower(sky_string_t string);


/** Strip leading whitespace or characters from a string.
  *
  * @param[in]  string      the string to be stripped.
  * @param[in]  chars       the characters to strip, or whitespace if @c NULL
  *                         or @c sky_None.
  * @return     @a string stripped of leading whitespace or characters in
  *             @a chars.
  */
SKY_EXTERN sky_string_t
sky_string_lstrip(sky_string_t string, sky_object_t chars);


/** Make a translation table suitable for use with sky_string_translate().
  * If there is only one argument (@a y and @a z are either @c NULL or
  * @c sky_None), it must be a dictionary mapping Unicode ordinals (integers)
  * or characters to Unicode ordinals, strings, or None. Character keys will be
  * converted to ordinals.
  *
  * If there are two or more arguments, @a x and @a y must both be strings of
  * equal length. In the resulting dictionary, each character in @a x will be
  * mapped to the character in the same position in @a y. If @a z is supplied,
  * it must be a string, whose characters will be mapped to @c sky_None in the
  * result.
  */
SKY_EXTERN sky_dict_t
sky_string_maketrans(sky_object_t x, sky_object_t y, sky_string_t z);


/** Constants defining Unicode normalization forms. **/
typedef enum sky_string_normalize_form_e {
    SKY_STRING_NORMALIZE_FORM_NFC   =   1,
    SKY_STRING_NORMALIZE_FORM_NFD   =   2,
    SKY_STRING_NORMALIZE_FORM_NFKC  =   3,
    SKY_STRING_NORMALIZE_FORM_NFKD  =   4,
} sky_string_normalize_form_t;


/** Normalize a string according to one of the supported Unicode normalization
  * forms (NFC, NFD, NFKC, or NFKD).
  *
  * @param[in]  self    the string to normalize.
  * @param[in]  form    the normalization form to use, which must be one of
  *                     the @c sky_string_normalize_form_t constants.
  * @return     a new string instance normalized to the requested form.
  */
SKY_EXTERN sky_string_t
sky_string_normalize(sky_string_t self, sky_string_normalize_form_t form);


/** Fill in a @c sky_format_specification_t structure by parsing a standard
  * format specification string as defined by PEP 3101 (Advanced String
  * Formatting).
  *
  * @param[in]  self    the format specification string to be parsed.
  * @param[in]  spec    the format specification structure to be filled in
  *                     with the information from the parsed @a string.
  * @return     @c SKY_TRUE if a format specification other than the default
  *             was parsed; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_string_parseformatspecification(sky_string_t                self,
                                    sky_format_specification_t *spec);


/** Partition a string object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with @a self and two empty string objects.
  *
  * @param[in]  self        the string object to partition.
  * @param[in]  sep         the separator to use.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             @a self and two empty string objects.
  */
SKY_EXTERN sky_tuple_t
sky_string_partition(sky_string_t self, sky_string_t sep);


/** Repeat a string a variable number of times.
  * If the repeat count is less than or equal to 0, @c sky_string_empty will be
  * returned. If @a count is 1, the original string will be returned.
  *
  * @param[in]  string      the string to be repeated.
  * @param[in]  count       the number of times to repeat @a string.
  * @return     a new string object instance that is @a string repeated
  *             @a count times.
  */
SKY_EXTERN sky_string_t
sky_string_repeat(sky_string_t string, ssize_t count);


/** Replace all non-overlapping occurrences of one string with another one in a
  * string object.
  *
  * @param[in]  self        the string object in which @a old_string is to be
  *                         replaced with @a new_string.
  * @param[in]  old_string  the string to replace.
  * @param[in]  new_string  the string to replace @a old_string with.
  * @param[in]  count       if >= 0, the maximum number of occurrences of
  *                         @a old_string to replace; otherwise, there is no
  *                         limit.
  * @return     a new string object with @a count occurrences of @a old_string
  *             in @a self replaced by @a new_string.
  */
SKY_EXTERN sky_string_t
sky_string_replace(sky_string_t self,
                   sky_string_t old_string,
                   sky_string_t new_string,
                   ssize_t      count);


/** Return a parseable representation of a string object instance.
  * This is the implementation of str.__repr__().
  *
  * @param[in]  self        the string object instance for which a parseable
  *                         representation is to be returned.
  * @return     an string object instance that is a parseable representation of
  *             @a self.
  */
SKY_EXTERN sky_string_t
sky_string_repr(sky_string_t self);


/** Return the highest index position of a string within a string.
  *
  * @param[in]  string      the string to be searched.
  * @param[in]  sub         the sub-string for which to search.
  * @param[in]  start       the starting index position to consider.
  * @param[in]  end         the ending index position to consider.
  * @return     the highest index position of @a sub in @a string, or -1 if
  *             @a sub is not found.
  */
SKY_EXTERN ssize_t
sky_string_rfind(sky_string_t   string,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end);


/** Return the highest index position of a string within a string.
  *
  * @param[in]  string      the string to be searched.
  * @param[in]  sub         the sub-string for which to search.
  * @param[in]  start       the starting index position to consider.
  * @param[in]  end         the ending index position to consider.
  * @return     the highest index position of @a sub in @a string. If @a sub
  *             is not found, @c sky_ValueError will be raised.
  */
SKY_EXTERN ssize_t
sky_string_rindex(sky_string_t  string,
                  sky_string_t  sub,
                  ssize_t       start,
                  ssize_t       end);


/** Right justify a string.
  *
  * @param[in]  self        the string to right justify.
  * @param[in]  width       the (minimum) width of the resulting right justified
  *                         string.
  * @param[in]  fillchar    the charactrer to use to fill the space between the
  *                         length of @a self and @a width. This must be a
  *                         string of length 1.
  * @return a new string object that is @a self right justified, or @a itself if
  *         it is longer than @a width.
  */
SKY_EXTERN sky_string_t
sky_string_rjust(sky_string_t   self,
                 ssize_t        width,
                 sky_string_t   fillchar);


/** Partition a string object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with two empty string objects and @a self. This function works the
  * same as sky_string_partition(), except that the search for @a sep is done
  * in reverse.
  *
  * @param[in]  self        the string object to partition.
  * @param[in]  sep         the separator to use.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             two empty string objects and @a self.
  */
SKY_EXTERN sky_tuple_t
sky_string_rpartition(sky_string_t self, sky_string_t sep);


/** Split a string by a separator.
  * This function is the same as sky_string_split(), except that it performs
  * the splits in reverse. If the number of splits is less than @a maxsplit,
  * the results from the two functions are identical. If no separator is
  * specified, splits will be done by runs of whitespace.
  *
  * @param[in]  self        the string to split.
  * @param[in]  sep         the separator to use, which may be @c NULL or
  *                         @c sky_None to perform splits by runs of whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, '123 456 789'.rsplit()
  *             would yield ['123', '456', '789'].
  */
SKY_EXTERN sky_list_t
sky_string_rsplit(sky_string_t self, sky_string_t sep, ssize_t maxsplit);


/** Strip trailing whitespace or characters from a string.
  *
  * @param[in]  string      the string to be stripped.
  * @param[in]  chars       the characters to strip, or whitespace if @c NULL
  *                         or @c sky_None.
  * @return     @a string stripped of trailing whitespace or characters in
  *             @a chars.
  */
SKY_EXTERN sky_string_t
sky_string_rstrip(sky_string_t string, sky_object_t chars);


/** Return the amount of memory (in bytes) that a string is using.
  *
  * @param[in]  string      the string for which its memory usage is to be
  *                         returned.
  * @return     the amount of memory (in bytes) that @a string is using.
  */
SKY_EXTERN size_t
sky_string_sizeof(sky_string_t string);


/** Return a sub-section of a string.
  *
  * @param[in]  string      the string to slice.
  * @param[in]  start       the starting position for the slice (inclusive).
  * @param[in]  stop        the ending position for the slice (exclusive).
  * @param[in]  step        the step for the slice.
  * @return     a new string object instance that is the requested sub-section
  *             of @a string.
  */
SKY_EXTERN sky_string_t
sky_string_slice(sky_string_t   string,
                 ssize_t        start,
                 ssize_t        stop,
                 ssize_t        step);


/** Split a string by a separator.
  *
  * @param[in]  self        the string to split.
  * @param[in]  sep         the separator to use, which may be @c NULL or
  *                         @c sky_None to perform splits by runs of whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, '123 456 789'.split()
  *             would yield ['123', '456', '789'].
  */
SKY_EXTERN sky_list_t
sky_string_split(sky_string_t self, sky_string_t sep, ssize_t maxsplit);


/** Split a string by line endings.
  * Line endings are '\\r' (carriage return), '\\n' (newline), or '\\r\\n'
  * (crlf).
  *
  * @param[in]  self        the string to split.
  * @param[in]  keepends    if @c SKY_TRUE, line endings will be included with
  *                         each line; otherwise, line endings will be stripped
  *                         from the results.
  * @return     a list containing slices of @a self split by line endings.
  */
SKY_EXTERN sky_list_t
sky_string_splitlines(sky_string_t self, sky_bool_t keepends);


/** Determine if a string starts with another string.
  *
  * @param[in]  string      the string to test.
  * @param[in]  prefix      the substring to look for, which may be a tuple of
  *                         strings to try.
  * @param[in]  start       the starting position in @a string to consider.
  * @param[in]  end         the ending position in @a string to consider.
  * @return     @c SKY_TRUE if @a string[@a start:@a end] starts with
  *             @a prefix, or @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_string_startswith(sky_string_t  string,
                      sky_object_t  prefix,
                      ssize_t       start,
                      ssize_t       end);


/** Return a human readable representation of a string object instance.
  * This is the implementation of str.__str__().
  *
  * @param[in]  self        the string object instance for which a human
  *                         readable representation is to be returned.
  * @return     an string object instance that is a human readable
  *             representation of @a self.
  */
SKY_EXTERN sky_string_t
sky_string_str(sky_string_t self);


/** Strip leading and trailing whitespace or characters from a string.
  *
  * @param[in]  string      the string to be stripped.
  * @param[in]  chars       the characters to strip, or whitespace if @c NULL
  *                         or @c sky_None.
  * @return     @a string stripped of leading and trailing whitespace or
  *             characters in @a chars.
  */
SKY_EXTERN sky_string_t
sky_string_strip(sky_string_t string, sky_object_t chars);


/** Swap the case of all cased characters in a string.
  * All uppercase characters are converted to lowecase and vice versa. Any
  * titlecase characters are not affected.
  *
  * @param[in]  self        the string for which the case of all cased
  *                         characters are to be swapped.
  * @return     a new string object with the cases of all cased characters in
  *             @a self swapped.
  */
SKY_EXTERN sky_string_t
sky_string_swapcase(sky_string_t self);


/** Convert a string into its titlecase form.
  * The string is converted so that words start with an uppercase character and
  * the remaining cased characters are lowercase. The algorithm uses a simple
  * language-independent definition of a word as groups of consecutive letters.
  * The definition works in many contexts, but it means that apostrophes in
  * contractions and possessives form word boundaries, which may not be the
  * desired result.
  *
  * @param[in]  string      the string to be converted.
  * @return     a string that is @a converted to titlecase.
  */
SKY_EXTERN sky_string_t
sky_string_title(sky_string_t string);


/** Transform a string to make it suitable for numeric parsing.
  * This is here specifically for complex, float, and integer types to parse
  * strings into their respective types, but it may be otherwise useful. The
  * string is converted to a simple ASCII representation, if possible. Any
  * Unicode code points that are classified as whitespace are converted to
  * ASCII space (0x20). Similarly, any Unicode code points that have a numeric
  * value are converted to the ASCII code point that has the equivalent numeric
  * value, if possible. If a conversion is not possible, @c NULL will be
  * returned.
  *
  * @param[in]  string      the string to be transformed.
  * @return     a new string that is @a string transformed, @a string itself if
  *             no transformation was required, or @c NULL if transformation is
  *             not possible.
  */
SKY_EXTERN sky_string_t
sky_string_transform_numeric(sky_string_t string);


/** Translate a string using a translation table.
  * The translation table is typically created using sky_string_maketrans().
  * It must be a dictionary object that contains mappings of Unicode codepoints
  * expressed as ordinals (integers) to one of Unicode ordinals, strings, or
  * @c sky_None. Unmapped characters are left untouched. Characters mapped to
  * @c sky_None are deleted.
  *
  * @param[in]  string      the string to be translated.
  * @param[in]  table       the translation table to use.
  * @return     a new string that is @a string translated using @a table.
  */
SKY_EXTERN sky_string_t
sky_string_translate(sky_string_t string, sky_dict_t table);


/** Convert a string into its uppercase form.
  * All cased characters present in @a string are converted to uppercase. Any
  * non-cased characters are preserved as-is. The returned string may be the
  * same as @a string if there are no cased characters present, or if they are
  * all already uppercase.
  *
  * @param[in]  string      the string to be uppercased.
  * @return     a string object instance that is @a string uppercased.
  */
SKY_EXTERN sky_string_t
sky_string_upper(sky_string_t string);


/** Right justify a string with zeros, preserving the sign, if present.
  * This is really just a special purpose version of sky_string_rjust() using
  * a fillchar of '0', and keeping any sign ('-' or '+') that may be present at
  * the start of the resulting string.
  *
  * @param[in]  self        the string to be zero filled.
  * @param[in]  width       the width to make the resulting object.
  * @return     a new string object that is @a self padded with '0' characters
  *             to make it as wide as @a width.
  */
SKY_EXTERN sky_string_t
sky_string_zfill(sky_string_t self, ssize_t width);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_STRING_H__ */

/** @} **/
/** @} **/
