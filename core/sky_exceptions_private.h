#ifndef __SKYTHON_CORE_SKY_EXCEPTIONS_PRIVATE_H__
#define __SKYTHON_CORE_SKY_EXCEPTIONS_PRIVATE_H__ 1


SKY_CDECLS_BEGIN


typedef struct sky_BaseException_data_s {
    sky_object_t                        traceback;
    sky_object_t                        cause;
    sky_object_t                        context;
    sky_object_t                        args;
    sky_bool_t                          suppress_context;
} sky_BaseException_data_t;

SKY_EXTERN_INLINE sky_BaseException_data_t *
sky_BaseException_data(sky_object_t object)
{
    return sky_object_data(object, sky_BaseException);
}


typedef struct sky_ImportError_data_s {
    sky_object_t                        msg;
    sky_object_t                        name;
    sky_object_t                        path;
} sky_ImportError_data_t;

SKY_EXTERN_INLINE sky_ImportError_data_t *
sky_ImportError_data(sky_object_t object)
{
    return sky_object_data(object, sky_ImportError);
}


typedef struct sky_OSError_data_s {
    sky_object_t                        oserror;    /* errno */
    sky_object_t                        strerror;   /* strerror(oserror) */
    sky_object_t                        filename;
#if defined(_WIN32)
    sky_object_t                        winerror;   /* GetLastError() */
#endif
    ssize_t                             written;    /* only for BlockIOError */
} sky_OSError_data_t;

SKY_EXTERN_INLINE sky_OSError_data_t *
sky_OSError_data(sky_object_t object)
{
    return sky_object_data(object, sky_OSError);
}


typedef struct sky_StopIteration_data_s {
    sky_object_t                        value;
} sky_StopIteration_data_t;

SKY_EXTERN_INLINE sky_StopIteration_data_t *
sky_StopIteration_data(sky_object_t object)
{
    return sky_object_data(object, sky_StopIteration);
}


typedef struct sky_SyntaxError_data_s {
    sky_object_t                        msg;
    sky_object_t                        filename;
    sky_object_t                        lineno;
    sky_object_t                        offset;
    sky_object_t                        text;
    sky_object_t                        print_file_and_line;
} sky_SyntaxError_data_t;

SKY_EXTERN_INLINE sky_SyntaxError_data_t *
sky_SyntaxError_data(sky_object_t object)
{
    return sky_object_data(object, sky_SyntaxError);
}


typedef struct sky_SystemExit_data_s {
    sky_object_t                        code;
} sky_SystemExit_data_t;

SKY_EXTERN_INLINE sky_SystemExit_data_t *
sky_SystemExit_data(sky_object_t object)
{
    return sky_object_data(object, sky_SystemExit);
}


typedef struct sky_UnicodeError_data_s {
    sky_object_t                        encoding;
    sky_object_t                        object;
    ssize_t                             start;
    ssize_t                             end;
    sky_object_t                        reason;
} sky_UnicodeError_data_t;

SKY_EXTERN_INLINE sky_UnicodeError_data_t *
sky_UnicodeError_data(sky_object_t object)
{
    return sky_object_data(object, sky_UnicodeError);
}


/* This is basically the same as SKY_TYPE_DEFINE() except that the names of
 * the variables produced differ (specifically, there's no _type suffix).
 */
#define SKY_EXCEPTION_DEFINE(name, size, init, fini, visit, flags, doc) \
    SKY_ALIGNED(16)                                                     \
    struct sky_type_s sky_##name##_struct = {                           \
        SKY_OBJECT_DATA_INITIALIZER(&sky_type_type_struct),             \
        {                                                               \
            #name,                                                      \
            NULL,                                                       \
            0,                                                          \
            ((flags) | SKY_TYPE_FLAG_BUILTIN |                          \
                       SKY_TYPE_FLAG_HAS_DICT |                         \
                       SKY_TYPE_FLAG_STATIC),                           \
            sizeof(sky_object_data_t) + size,                           \
            sizeof(void *),                                             \
            size,                                                       \
            sizeof(void *),                                             \
            NULL,                                                       \
            NULL,                                                       \
            NULL,                                                       \
            NULL,                                                       \
            sky_type_allocate_calloc,                                   \
            sky_free,                                                   \
            init,                                                       \
            fini,                                                       \
            visit,                                                      \
            NULL,                                                       \
            NULL,                                                       \
            NULL,                                                       \
            NULL,                                                       \
            (char *)(doc),                                              \
            NULL,                                                       \
            SKY_SPINLOCK_INITIALIZER,                                   \
        },                                                              \
        NULL,                                                           \
    };                                                                  \
    sky_type_t const sky_##name = &sky_##name##_struct

#define SKY_EXCEPTION_DEFINE_TRIVIAL(name, doc)                         \
        SKY_EXCEPTION_DEFINE(name,                                      \
                             0,                                         \
                             NULL,                                      \
                             NULL,                                      \
                             NULL,                                      \
                             0,                                         \
                             doc)


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_EXCEPTIONS_PRIVATE_H__ */
