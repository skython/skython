#include "sky_private.h"
#include "sky_bytestring.h"
#include "sky_bytes_private.h"


typedef struct sky_bytearray_data_s {
    sky_bytestring_t *                  bytes;
    sky_bytestring_t                    local_bytes;
    int                                 buffer_count;
    sky_spinlock_t                      spinlock;
} sky_bytearray_data_t;

SKY_EXTERN_INLINE sky_bytearray_data_t *
sky_bytearray_data(sky_object_t object)
{
    if (sky_bytearray_type == sky_object_type(object)) {
        return (sky_bytearray_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_bytearray_type);
}

static inline sky_bool_t
sky_bytearray_islockable(sky_object_t object)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(object));
}

static inline sky_bytestring_t *
sky_bytearray_bytes(sky_object_t object)
{
    sky_bytearray_data_t    *bytearray_data = sky_bytearray_data(object);

    sky_hazard_pointer_t    *hazard;

    /* If the bytearray object isn't lockable, just return the bytes data.
     * Otherwise, use a hazard pointer, because it's possible that another
     * thread may be mutating it while this one is using it.
     */
    if (!sky_bytearray_islockable(object)) {
        return bytearray_data->bytes;
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(bytearray_data->bytes)));
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);

    return hazard->pointer;
}

static inline sky_bytearray_t
sky_bytearray_empty(void)
{
    sky_bytearray_t object;

    object = sky_object_allocate(sky_bytearray_type);
    sky_bytestring_initwithbytes(sky_bytearray_data(object)->bytes,
                                 "",
                                 0,
                                 NULL);
    return object;
}

static void
sky_bytearray_unlock(sky_object_t object)
{
    sky_spinlock_unlock(&(sky_bytearray_data(object)->spinlock));
}

static inline sky_bool_t
sky_bytearray_lock(sky_object_t object)
{
    sky_bool_t              result = SKY_FALSE;
    sky_bytearray_data_t    *bytearray_data = sky_bytearray_data(object);

    if (sky_bytearray_islockable(object)) {
        sky_spinlock_lock(&(bytearray_data->spinlock));
        sky_asset_save(object, sky_bytearray_unlock, SKY_ASSET_CLEANUP_ALWAYS);
        result = SKY_TRUE;
    }
    if (bytearray_data->buffer_count > 0) {
        sky_error_raise_string(
                sky_BufferError,
                "Existing exports of data: object cannot be modified");
    }

    return result;
}

static inline void
sky_bytearray_bytes_retire(sky_bytearray_data_t *   bytearray_data,
                           sky_bytestring_t *       bytes)
{
    if (bytes) {
        if (&(bytearray_data->local_bytes) == bytes) {
            sky_hazard_pointer_retire(bytes,
                                      (sky_free_t)sky_bytestring_cleanup);
        }
        else {
            sky_hazard_pointer_retire(bytes,
                                      (sky_free_t)sky_bytestring_destroy);
        }
    }
}

static sky_bytestring_t *
sky_bytearray_write_begin(sky_object_t object, ssize_t capacity)
{
    sky_bytearray_data_t    *bytearray_data = sky_bytearray_data(object);

    if (!sky_bytearray_lock(object)) {
        if (capacity) {
            sky_bytestring_resize(bytearray_data->bytes,
                                  bytearray_data->bytes->used + capacity);
        }
    }
    else {
        sky_bytestring_t    *new_bytes;

        if (SSIZE_MAX - bytearray_data->bytes->used <= capacity) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }

        new_bytes = sky_calloc(1, sizeof(sky_bytestring_t));
        sky_asset_save(new_bytes,
                       (sky_free_t)sky_bytestring_destroy,
                       SKY_ASSET_CLEANUP_ON_ERROR);
        sky_bytestring_initfrombytes(new_bytes,
                                     bytearray_data->bytes->bytes,
                                     bytearray_data->bytes->used,
                                     bytearray_data->bytes->used + capacity);
        return new_bytes;
    }
    return bytearray_data->bytes;
}

#define SKY_BYTEARRAY_WRITE_BEGIN(__o, __b, __c)                    \
        do {                                                        \
            sky_bytearray_t         __o_object = __o;               \
                                                                    \
            sky_bytearray_data_t    *__o_data;                      \
            sky_bytestring_t        *__o_bytes;                     \
                                                                    \
            __o_data = sky_bytearray_data(__o_object);              \
            SKY_ASSET_BLOCK_BEGIN {                                 \
                __b = sky_bytearray_write_begin(__o, __c);          \
                __o_bytes = __o_data->bytes;                        \
                do

#define SKY_BYTEARRAY_WRITE_END(__b)                                \
                while (0);                                          \
                __o_data->bytes = __b;                              \
            } SKY_ASSET_BLOCK_END;                                  \
            if (__o_bytes != __b) {                                 \
                sky_bytearray_bytes_retire(__o_data, __o_bytes);    \
            }                                                       \
        } while (0)

struct sky_bytearray_s {
    sky_object_data_t                   object_data;
    sky_bytearray_data_t                bytearray_data;
};


typedef struct sky_bytearray_iterator_data_s {
    sky_bytearray_t                     bytearray;
    ssize_t                             next_index;
} sky_bytearray_iterator_data_t;

SKY_EXTERN_INLINE sky_bytearray_iterator_data_t *
sky_bytearray_iterator_data(sky_object_t object)
{
    if (sky_bytearray_iterator_type == sky_object_type(object)) {
        return (sky_bytearray_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_bytearray_iterator_type);
}

struct sky_bytearray_iterator_s {
    sky_object_data_t                   object_data;
    sky_bytearray_iterator_data_t       iterator_data;
};


static void
sky_bytearray_instance_buffer_acquire(sky_object_t  self,
                                      void *        data,
                                      sky_buffer_t *buffer,
                                      unsigned int  flags)
{
    sky_bytearray_data_t    *bytearray_data = data;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_bytearray_islockable(self)) {
            sky_spinlock_lock(&(bytearray_data->spinlock));
            sky_asset_save(self, sky_bytearray_unlock, SKY_ASSET_CLEANUP_ALWAYS);
        }

        sky_buffer_initialize(buffer,
                              bytearray_data->bytes->bytes,
                              bytearray_data->bytes->used,
                              SKY_FALSE,
                              flags);
        ++bytearray_data->buffer_count;
    } SKY_ASSET_BLOCK_END;
}

static void
sky_bytearray_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_bytearray_data_t    *bytearray_data = data;

    sky_error_validate(0 == bytearray_data->buffer_count);
    sky_bytestring_cleanup(bytearray_data->bytes);
    if (bytearray_data->bytes != &(bytearray_data->local_bytes)) {
        sky_free(bytearray_data->bytes);
    }
    else {
        sky_bytestring_cleanup(&(bytearray_data->local_bytes));
    }
}

static void
sky_bytearray_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_bytearray_data_t    *bytearray_data = data;

    sky_spinlock_init(&(bytearray_data->spinlock));
    bytearray_data->bytes = &(bytearray_data->local_bytes);
}

static void
sky_bytearray_instance_buffer_release(           sky_object_t   self,
                                                 void *         data,
                                      SKY_UNUSED sky_buffer_t * buffer)
{
    sky_bytearray_data_t    *bytearray_data = data;

    if (!sky_bytearray_islockable(self)) {
        sky_error_validate(bytearray_data->buffer_count > 0);
        --bytearray_data->buffer_count;
    }
    else {
        sky_spinlock_lock(&(bytearray_data->spinlock));
        sky_error_validate(bytearray_data->buffer_count > 0);
        --bytearray_data->buffer_count;
        sky_spinlock_unlock(&(bytearray_data->spinlock));
    }
}

static void
sky_bytearray_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_bytearray_iterator_data_t   *iterator_data = data;

    sky_object_visit(iterator_data->bytearray, visit_data);
}


static inline void
sky_bytearray_bufferasbytes(sky_buffer_t *      buffer,
                            sky_object_t        object,
                            sky_bytestring_t *  bytes)
{
    if (sky_bytearray_type == sky_object_type(object)) {
        *bytes = *sky_bytearray_bytes(object);
    }
    else {
        sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_bytestring_initwithbytes(bytes, buffer->buf, buffer->len, NULL);
    }
}


static sky_bytearray_t
sky_bytearray_createfrombytestring(const sky_bytestring_t *bytestring)
{
    sky_bytearray_t     object;
    sky_bytestring_t    *bytes;

    if (!bytestring->used) {
        return sky_bytearray_empty();
    }

    object = sky_object_allocate(sky_bytearray_type);
    bytes = sky_bytearray_bytes(object);
    bytes->bytes = memcpy(sky_malloc(bytestring->used + 1),
                          bytestring->bytes,
                          bytestring->used);
    bytes->bytes[bytestring->used] = '\0';
    bytes->used = bytestring->used;
    bytes->size = sky_memsize(bytes->bytes);
    bytes->u.free = sky_free;

    return object;
}


static sky_bytearray_t
sky_bytearray_createwithbytestring(const sky_bytestring_t *bytestring)
{
    sky_bytearray_t     object;
    sky_bytestring_t    *bytes;

    if (!bytestring->used) {
        return sky_bytearray_empty();
    }

    object = sky_object_allocate(sky_bytearray_type);
    bytes = sky_bytearray_bytes(object);
    bytes->bytes = bytestring->bytes;
    bytes->used = bytestring->used;
    bytes->size = bytestring->size;
    bytes->u.free = bytestring->u.free;

    return object;
}


static void
sky_bytearray_parse_sub(sky_object_t        sub,
                        uint8_t *           byte,
                        sky_buffer_t *      sub_buffer,
                        sky_bytestring_t *  sub_bytes)
{
    volatile sky_bool_t buffer = SKY_FALSE;

    SKY_ERROR_TRY {
        *byte = sky_integer_value(sky_number_index(sub), 0, 255, NULL);
        sky_bytestring_initwithbytes(sub_bytes, byte, 1, NULL);
    }
    SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_raise_string(sky_ValueError,
                               "byte must be in range(0, 256)");
    }
    SKY_ERROR_EXCEPT_ANY {
        buffer = SKY_TRUE;
    } SKY_ERROR_TRY_END;

    if (buffer) {
        /* This must be outside of the TRY block because it may save assets. */
        sky_bytearray_bufferasbytes(sub_buffer, sub, sub_bytes);
    }
}


sky_bytearray_t
sky_bytearray_add(sky_bytearray_t self, sky_object_t other)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        ssize_t             result_len;
        sky_buffer_t        other_buffer;
        sky_bytestring_t    other_bytes, *result_bytes;

        sky_bytearray_bufferasbytes(&other_buffer, other, &other_bytes);

        if (SSIZE_MAX - self_bytes->used <= other_bytes.used) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }
        if (!(result_len = self_bytes->used + other_bytes.used)) {
            result = sky_bytearray_empty();
        }
        else {
            result = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_bytes(result);
            sky_bytestring_initfrombytes(result_bytes,
                                         self_bytes->bytes,
                                         self_bytes->used,
                                         result_len);
            sky_bytestring_concat(result_bytes, &other_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


size_t
sky_bytearray_alloc(sky_bytearray_t self)
{
    size_t  size;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!self_bytes->bytes) {
            size = 0;
        }
        else if (sky_free == self_bytes->u.free) {
            size = sky_memsize(self_bytes->bytes);
        }
        else {
            size = self_bytes->size;
        }
    } SKY_ASSET_BLOCK_END;

    return size;
}

static const char *sky_bytearray_alloc_doc =
"B.__alloc__() -> int\n\n"
"Return the number of bytes actually allocated.";


void
sky_bytearray_append(sky_bytearray_t self, ssize_t x)
{
    uint8_t             byte = (uint8_t)x;
    sky_bytestring_t    *bytes;

    if (x < 0 || x > 255) {
        sky_error_raise_string(sky_ValueError,
                               "byte must be in range(0, 256)");
    }

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 1) {
        sky_bytestring_appendbytes(bytes, &byte, 1);
    } SKY_BYTEARRAY_WRITE_END(bytes);
}

static const char *sky_bytearray_append_doc =
"B.append(int) -> None\n\n"
"Append a single item to the end of B.";


uint8_t
sky_bytearray_byteat(sky_bytearray_t self, ssize_t index)
{
    uint8_t byte;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (index < 0) {
            index += self_bytes->used;
        }
        if (index < 0 || index >= self_bytes->used) {
            sky_error_raise_format(sky_IndexError,
                                   "%#@ index out of range",
                                   sky_type_name(sky_object_type(self)));
        }

        byte = self_bytes->bytes[index];
    } SKY_ASSET_BLOCK_END;

    return byte;
}


sky_bytearray_t
sky_bytearray_capitalize(sky_bytearray_t self)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_capitalize(self_bytes, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_capitalize_doc =
"B.capitalize() -> copy of B\n\n"
"Return a copy of B with only its first character capitalized (ASCII)\n"
"and the rest lower-cased.";


sky_bytearray_t
sky_bytearray_center(sky_bytearray_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes;

        sky_bytearray_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_center(self_bytes,
                                   width,
                                   fillchar_bytes.bytes[0],
                                   &result_bytes))
        {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_center_doc =
"B.center(width[, fillchar]) -> copy of B\n\n"
"Return B centered in a string of length width.  Padding is\n"
"done using the specified fill character (default is a space).";


void
sky_bytearray_clear(sky_bytearray_t self)
{
    sky_bytestring_t        *old_bytes = NULL;
    sky_bytearray_data_t    *self_data = sky_bytearray_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_bytearray_lock(self)) {
            sky_bytestring_cleanup(self_data->bytes);
        }
        else {
            old_bytes = self_data->bytes;
            self_data->bytes = sky_calloc(1, sizeof(sky_bytestring_t));
        }
    } SKY_ASSET_BLOCK_END;

    sky_bytearray_bytes_retire(self_data, old_bytes);
}

static const char *sky_bytearray_clear_doc =
"B.clear() -> None\n\n"
"Remove all items from B.";


sky_object_t
sky_bytearray_compare(sky_bytearray_t   self,
                      sky_object_t      other,
                      sky_compare_op_t  compare_op)
{
    sky_object_t    result;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        other_buffer;
        sky_bytestring_t    other_bytes;

        if (!sky_buffer_check(other)) {
            if ((SKY_COMPARE_OP_EQUAL == compare_op ||
                 SKY_COMPARE_OP_NOT_EQUAL == compare_op) &&
                (sky_interpreter_flags() & (SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS |
                                            SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS)) &&
                sky_object_isa(other, sky_string_type))
            {
                sky_error_warn_string(
                        sky_BytesWarning,
                        "Comparison between bytearray and string");
            }
            result = sky_NotImplemented;
        }
        else {
            sky_bytearray_bufferasbytes(&other_buffer, other, &other_bytes);
            result = (sky_bytestring_compare(self_bytes,
                                             &other_bytes,
                                             compare_op) ? sky_True
                                                         : sky_False);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bool_t
sky_bytearray_contains(sky_bytearray_t self, sky_object_t item)
{
    ssize_t pos;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        uint8_t             byte;
        sky_buffer_t        item_buffer;
        sky_bytestring_t    item_bytes;

        if (!SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            sky_bytearray_bufferasbytes(&item_buffer, item, &item_bytes);
        }
        else {
            byte = sky_integer_value(sky_number_index(item),
                                     0, 255,
                                     sky_ValueError);
            sky_bytestring_initwithbytes(&item_bytes, &byte, 1, NULL);
        }

        pos = sky_bytestring_find(self_bytes, &item_bytes, 0, SSIZE_MAX);
    } SKY_ASSET_BLOCK_END;

    return (pos >= 0 ? SKY_TRUE : SKY_FALSE);
}


sky_bytearray_t
sky_bytearray_copy(sky_bytearray_t self)
{
    sky_bytearray_t copy;

    SKY_ASSET_BLOCK_BEGIN {
        copy = sky_bytearray_createfrombytestring(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return copy;
}

static const char *sky_bytearray_copy_doc =
"B.copy() -> bytearray\n\n"
"Return a copy of B.";


ssize_t
sky_bytearray_count(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end)
{
    ssize_t count;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        sky_bytearray_parse_sub(sub, &sub_byte, &sub_buffer, &sub_bytes);
        count = sky_bytestring_count(self_bytes, &sub_bytes, start, end, -1);
    } SKY_ASSET_BLOCK_END;

    return count;
}

static const char *sky_bytearray_count_doc =
"B.count(sub[, start[, end]]) -> int\n\n"
"Return the number of non-overlapping occurrences of substring sub in\n"
"string B[start:end].  Optional arguments start and end are interpreted\n"
"as in slice notation.";


sky_bytearray_t
sky_bytearray_createfrombytes(const void *bytes, size_t nbytes)
{
    sky_bytearray_t object;

    if (!nbytes) {
        return sky_bytearray_empty();
    }
    if (nbytes > SSIZE_MAX) {
        sky_error_raise_string(sky_ValueError, "source data too large");
    }

    object = sky_object_allocate(sky_bytearray_type);
    sky_bytestring_initfrombytes(sky_bytearray_data(object)->bytes,
                                 bytes,
                                 nbytes,
                                 0);

    return object;
}


sky_bytearray_t
sky_bytearray_createwithbytes(void *bytes, size_t nbytes, sky_free_t free)
{
    sky_bytearray_t object;

    if (!nbytes) {
        if (free) {
            free(bytes);
        }
        return sky_bytearray_empty();
    }
    if (nbytes > SSIZE_MAX) {
        sky_error_raise_string(sky_ValueError, "source data too large");
    }

    object = sky_object_allocate(sky_bytearray_type);
    sky_bytestring_initwithbytes(sky_bytearray_data(object)->bytes,
                                 bytes,
                                 nbytes,
                                 free);

    return object;
}


sky_bytearray_t
sky_bytearray_createfromformat(const char *format, ...)
{
    va_list         ap;
    sky_bytearray_t object;

    va_start(ap, format);
    object = sky_bytearray_createfromformatv(format, ap);
    va_end(ap);

    return object;
}


sky_bytearray_t
sky_bytearray_createfromformatv(const char *format, va_list ap)
{
    char            *bytes;
    size_t          nbytes;
    sky_bytearray_t object;

    if (!(nbytes = sky_format_asprintf(&bytes, format, ap))) {
        sky_free(bytes);
        object = sky_bytearray_empty();
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
            if (nbytes > SSIZE_MAX) {
                sky_error_raise_string(sky_ValueError, "source data too large");
            }

            object = sky_object_allocate(sky_bytearray_type);
            sky_bytestring_initwithbytes(sky_bytearray_data(object)->bytes,
                                         bytes,
                                         nbytes,
                                         sky_free);
        } SKY_ASSET_BLOCK_END;
    }

    return object;
}


sky_bytearray_t
sky_bytearray_createfromobject(sky_object_t object)
{
    sky_bytearray_t bytearray;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             value;
        uint8_t             byte;
        sky_buffer_t        object_buffer;
        sky_object_t        item, iterator;
        sky_bytestring_t    *bytes, object_bytes;

        if (sky_buffer_check(object)) {
            sky_bytearray_bufferasbytes(&object_buffer, object, &object_bytes);
            bytearray = sky_bytearray_createfrombytestring(&object_bytes);
        }
        else if (SKY_OBJECT_METHOD_SLOT(object, INDEX)) {
            object = sky_number_index(object);
            value = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
            if (value < 0) {
                sky_error_raise_string(sky_ValueError, "negative count");
            }

            bytearray = sky_object_allocate(sky_bytearray_type);
            bytes = sky_bytearray_data(bytearray)->bytes;
            sky_bytestring_initfrombytes(bytes, NULL, value, value);
            memset(bytes->bytes, 0x00, bytes->used);
        }
        else if (sky_object_isiterable(object)) {
            bytearray = sky_object_allocate(sky_bytearray_type);
            bytes = sky_bytearray_data(bytearray)->bytes;
            sky_bytestring_initfrombytes(bytes,
                                         NULL,
                                         0,
                                         sky_object_length_hint(object, 0));

            SKY_ERROR_TRY {
                if (sky_sequence_isiterable(object)) {
                    SKY_SEQUENCE_FOREACH(object, item) {
                        item = sky_number_index(item);
                        byte = sky_integer_value(item, 0, 255, NULL);
                        sky_bytestring_appendbytes(bytes, &byte, 1);
                    } SKY_SEQUENCE_FOREACH_END;
                }
                else {
                    iterator = sky_object_iter(object);
                    while ((item = sky_object_next(iterator, NULL)) != NULL) {
                        item = sky_number_index(item);
                        byte = sky_integer_value(item, 0, 255, NULL);
                        sky_bytestring_appendbytes(bytes, &byte, 1);
                    }
                }
            }
            SKY_ERROR_EXCEPT(sky_OverflowError) {
                sky_error_raise_string(sky_ValueError,
                                       "bytes must be in range(0, 256)");
            } SKY_ERROR_TRY_END;
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "cannot create %#@ object from %#@",
                                   sky_type_name(sky_bytearray_type),
                                   sky_type_name(sky_object_type(object)));
        }
    } SKY_ASSET_BLOCK_END;

    return bytearray;
}


sky_string_t
sky_bytearray_decode(sky_bytearray_t    self,
                     sky_string_t       encoding,
                     sky_string_t       errors)
{
    sky_string_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        result = sky_bytestring_decode(self_bytes, encoding, errors);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_decode_doc =
"B.decode(encoding='utf-8', errors='strict') -> str\n\n"
"Decode B using the codec registered for encoding. Default encoding\n"
"is 'utf-8'. errors may be given to set a different error\n"
"handling scheme.  Default is 'strict' meaning that encoding errors raise\n"
"a UnicodeDecodeError.  Other possible values are 'ignore' and 'replace'\n"
"as well as any other name registered with codecs.register_error that is\n"
"able to handle UnicodeDecodeErrors.";


void
sky_bytearray_delitem(sky_bytearray_t self, sky_object_t item)
{
    ssize_t             length, start, step, stop;
    sky_bytestring_t    *bytes;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        item = sky_number_index(item);
        start = sky_integer_value(item, SSIZE_MIN, SSIZE_MAX, NULL);

        SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
            if (start < 0) {
                start += bytes->used;
            }
            if (start < 0 || start >= bytes->used) {
                sky_error_raise_string(sky_IndexError, "index out of range");
            }
            sky_bytestring_delete(bytes, start, start + 1, 1);
        } SKY_BYTEARRAY_WRITE_END(bytes);
    }
    else if (sky_object_isa(item, sky_slice_type)) {
        SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
            length = sky_slice_range(item, bytes->used, &start, &stop, &step);
            if (length) {
                sky_bytestring_delete(bytes, start, stop, step);
            }
        } SKY_BYTEARRAY_WRITE_END(bytes);
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "%#@ indices must be integers",
                               sky_type_name(sky_object_type(self)));
    }
}


sky_bool_t
sky_bytearray_endswith(sky_bytearray_t  self,
                       sky_object_t     suffix,
                       ssize_t          start,
                       ssize_t          end)
{
    sky_bool_t  endswith = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        sub_buffer;
        sky_object_t        object;
        sky_bytestring_t    sub_bytes;

        if (!sky_object_isa(suffix, sky_tuple_type)) {
            if (!sky_buffer_check(suffix)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "endswith first arg must be bytes or a tuple of bytes, "
                        "not %#@",
                        sky_type_name(sky_object_type(suffix)));
            }
            sky_bytearray_bufferasbytes(&sub_buffer, suffix, &sub_bytes);
            endswith = sky_bytestring_endswith(self_bytes,
                                               &sub_bytes,
                                               start,
                                               end);
        }
        else {
            SKY_SEQUENCE_FOREACH(suffix, object) {
                sky_bytearray_bufferasbytes(&sub_buffer, object, &sub_bytes);
                endswith = sky_bytestring_endswith(self_bytes,
                                                   &sub_bytes,
                                                   start,
                                                   end);
                if (endswith) {
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
            } SKY_SEQUENCE_FOREACH_END;
        }
    } SKY_ASSET_BLOCK_END;

    return endswith;
}

static const char *sky_bytearray_endswith_doc =
"B.endswith(suffix[, start[, end]]) -> bool\n\n"
"Return True if B ends with the specified suffix, False otherwise.\n"
"With optional start, test B beginning at that position.\n"
"With optional end, stop comparing B at that position.\n"
"suffix can also be a tuple of bytes to try.";


sky_object_t
sky_bytearray_eq(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_bytearray_t
sky_bytearray_expandtabs(sky_bytearray_t self, ssize_t tabsize)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_expandtabs(self_bytes, tabsize, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_expandtabs_doc =
"B.expandtabs([tabsize]) -> copy of B\n\n"
"Return a copy of B where all tab characters are expanded using spaces.\n"
"If tabsize is not given, a tab size of 8 characters is assumed.";


void
sky_bytearray_extend(sky_bytearray_t self, sky_object_t x)
{
    uint8_t             byte;
    sky_object_t        item, iterator;
    sky_bytestring_t    *bytes;

    if (sky_buffer_check(x)) {
        sky_bytearray_iadd(self, x);
        return;
    }
    if (!sky_object_isiterable(x)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(x)));
    }

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, sky_object_length_hint(x, 0)) {
        volatile ssize_t    used = bytes->used;

        SKY_ERROR_TRY {
            if (sky_sequence_isiterable(x)) {
                SKY_SEQUENCE_FOREACH(x, item) {
                    item = sky_number_index(item);
                    byte = sky_integer_value(item, 0, 255, NULL);
                    sky_bytestring_appendbytes(bytes, &byte, 1);
                } SKY_SEQUENCE_FOREACH_END;
            }
            else {
                iterator = sky_object_iter(x);
                while ((item = sky_object_next(iterator, NULL)) != NULL) {
                    item = sky_number_index(item);
                    byte = sky_integer_value(item, 0, 255, NULL);
                    sky_bytestring_appendbytes(bytes, &byte, 1);
                }
            }
        }
        SKY_ERROR_EXCEPT(sky_OverflowError) {
            bytes->used = used;
            bytes->bytes[used] = 0;
            sky_error_raise_string(sky_ValueError,
                                   "bytes must be in range(0, 256)");
        } SKY_ERROR_TRY_END;
    } SKY_BYTEARRAY_WRITE_END(bytes);
}

static const char *sky_bytearray_extend_doc =
"B.extend(iterable_of_ints) -> None\n\n"
"Append all the elements from the iterator or sequence to the\n"
"end of B.";


ssize_t
sky_bytearray_find(sky_bytearray_t  self,
                   sky_object_t     sub,
                   ssize_t          start,
                   ssize_t          end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        sky_bytearray_parse_sub(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_find(self_bytes, &sub_bytes, start, end);
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytearray_find_doc =
"B.find(sub[, start[, end]]) -> int\n\n"
"Return the lowest index in B where substring sub is found,\n"
"such that sub is contained within B[start:end].  Optional\n"
"arguments start and end are interpreted as in slice notation.\n\n"
"Return -1 on failure.";


sky_bytearray_t
sky_bytearray_fromhex(sky_type_t cls, sky_string_t string)
{
    const char              *cstring;
    sky_bytearray_t         object;
    sky_bytestring_t        *old_bytes, *new_bytes;
    sky_bytearray_data_t    *bytearray_data;

    if (!sky_type_issubtype(cls, sky_bytearray_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ is not a sub-type of %#@",
                               sky_type_name(cls),
                               sky_type_name(sky_bytearray_type));
    }
    if (!(cstring = sky_string_cstring(string))) {
        sky_error_raise_string(sky_ValueError,
                               "invalid string for fromhex()");
    }

    if (sky_bytearray_type == cls) {
        object = sky_object_allocate(cls);
        bytearray_data = sky_bytearray_data(object);
        sky_bytestring_fromhex(bytearray_data->bytes,
                               cstring,
                               sky_string_len(string));
        return object;
    }

    object = sky_object_call(cls, NULL, NULL);
    if (!sky_object_isa(object, sky_bytearray_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ does not a produce a sub-type of %#@",
                               sky_type_name(cls),
                               sky_type_name(sky_bytearray_type));
    }
    old_bytes = NULL;

    SKY_ASSET_BLOCK_BEGIN {
        bytearray_data = sky_bytearray_data(object);
        new_bytes = sky_asset_calloc(1,
                                     sizeof(sky_bytestring_t),
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        sky_bytestring_fromhex(new_bytes,
                               cstring,
                               sky_string_len(string));

        if (!sky_bytearray_lock(object)) {
            sky_bytestring_cleanup(bytearray_data->bytes);
            *bytearray_data->bytes = *new_bytes;
            sky_free(new_bytes);
        }
        else {
            old_bytes = bytearray_data->bytes;
            bytearray_data->bytes = new_bytes;
        }

    } SKY_ASSET_BLOCK_END;

    sky_bytearray_bytes_retire(bytearray_data, old_bytes);
    return object;
}

const char *sky_bytearray_fromhex_doc =
"bytearray.fromhex(string) -> bytearray\n\n"
"Create a bytearray object from a string of hexadecimal numbers.\n"
"Spaces between two numbers are accepted.\n"
"Example: bytearray.fromhex('B9 01EF') -> b'\\xb9\\x01\\xef'.";


sky_object_t
sky_bytearray_ge(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_bytearray_getitem(sky_bytearray_t self, sky_object_t item)
{
    ssize_t             i, j, length, start, step, stop;
    sky_object_t        result_object;
    sky_bytestring_t    *result_bytes, *self_bytes;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        start = sky_integer_value(sky_number_index(item),
                                  SSIZE_MIN, SSIZE_MAX,
                                  sky_IndexError);
        return sky_integer_create(sky_bytearray_byteat(self, start));
    }

    if (!sky_object_isa(item, sky_slice_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ indices must be integers",
                               sky_type_name(sky_object_type(self)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        self_bytes = sky_bytearray_bytes(self);
        length = sky_slice_range(item, self_bytes->used, &start, &stop, &step);
        if (!length) {
            result_object = sky_bytearray_empty();
        }
        else if (length == self_bytes->used && 1 == step) {
            result_object = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result_object = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_data(result_object)->bytes;
            if (1 == step) {
                sky_bytestring_initfrombytes(result_bytes,
                                             &(self_bytes->bytes[start]),
                                             length,
                                             length);
            }
            else {
                sky_bytestring_initfrombytes(result_bytes,
                                             NULL,
                                             length,
                                             length);
                for (i = 0, j = start; i < length; ++i, j += step) {
                    result_bytes->bytes[i] = self_bytes->bytes[j];
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result_object;
}


sky_object_t
sky_bytearray_gt(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_GREATER);
}


sky_bytearray_t
sky_bytearray_iadd(sky_bytearray_t self, sky_object_t other)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t        other_buffer;
        sky_bytestring_t    *bytes, other_bytes;

        if (other == self) {
            SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, other_bytes.used) {
                sky_bytestring_concat(bytes, bytes);
            } SKY_BYTEARRAY_WRITE_END(bytes);
        }
        else {
            sky_bytearray_bufferasbytes(&other_buffer, other, &other_bytes);
            if (other_bytes.used) {
                SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, other_bytes.used) {
                    sky_bytestring_concat(bytes, &other_bytes);
                } SKY_BYTEARRAY_WRITE_END(bytes);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return self;
}


sky_bytearray_t
sky_bytearray_imul(sky_bytearray_t self, sky_object_t other)
{
    sky_bytestring_t        *old_bytes = NULL;
    sky_bytearray_data_t    *self_data = sky_bytearray_data(self);

    ssize_t             count;
    sky_bytestring_t    *new_bytes;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot multiply %#@ by non-int of type %#@",
                               sky_type_name(sky_object_type(self)),
                               sky_type_name(sky_object_type(other)));
    }
    other = sky_number_index(other);
    count = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);

    SKY_ASSET_BLOCK_BEGIN {
        if (count <= 0) {
            if (!sky_bytearray_lock(self)) {
                sky_bytestring_cleanup(self_data->bytes);
            }
            else {
                old_bytes = self_data->bytes;
                self_data->bytes = sky_calloc(1, sizeof(sky_bytestring_t));
            }
        }
        else if (count > 1) {
            if (!sky_bytearray_lock(self)) {
                sky_bytestring_repeat(self_data->bytes, count);
            }
            else {
                old_bytes = self_data->bytes;
                if (SSIZE_MAX / count <= old_bytes->used) {
                    sky_error_raise_string(sky_OverflowError, "result too big");
                }
                new_bytes = sky_calloc(1, sizeof(sky_bytestring_t));
                sky_asset_save(new_bytes,
                               (sky_free_t)sky_bytestring_destroy,
                               SKY_ASSET_CLEANUP_ON_ERROR);
                sky_bytestring_initfrombytes(new_bytes,
                                             old_bytes->bytes,
                                             old_bytes->used,
                                             old_bytes->used * count);
                sky_bytestring_repeat(new_bytes, count);
                self_data->bytes = new_bytes;
            }
        }
    } SKY_ASSET_BLOCK_END;

    sky_bytearray_bytes_retire(self_data, old_bytes);
    return self;
}


ssize_t
sky_bytearray_index(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        sky_bytearray_parse_sub(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_find(self_bytes, &sub_bytes, start, end);
        if (index < 0) {
            sky_error_raise_string(sky_ValueError, "substring not found");
        }
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytearray_index_doc =
"B.index(sub[, start[, end]]) ->int\n\n"
"Like B.find() but raise ValueError when the substring is not found.";


void
sky_bytearray_init(sky_bytearray_t  self,
                   sky_object_t     source,
                   sky_string_t     encoding,
                   sky_string_t     errors)
{
    sky_bytestring_t        source_bytes = SKY_BYTESTRING_INITIALIZER;
    sky_bytearray_data_t    *self_data = sky_bytearray_data(self);

    ssize_t             size;
    sky_bool_t          copy, lockable;
    sky_object_t        object;
    sky_buffer_t        source_buffer;
    sky_bytestring_t    *new_bytes, *old_bytes;

    if ((encoding || errors) && !sky_object_isa(source, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "encoding or errors specified for non-string source");
    }

    SKY_ASSET_BLOCK_BEGIN {
        copy = SKY_TRUE;
        old_bytes = NULL;

        if (sky_object_isa(source, sky_string_type)) {
            if (!encoding) {
                sky_error_raise_string(
                        sky_TypeError,
                        "encoding is required for string source");
            }
            source = sky_codec_encode(NULL, source, encoding, errors, 0);
            sky_bytearray_bufferasbytes(&source_buffer, source, &source_bytes);
        }
        else if (SKY_OBJECT_METHOD_SLOT(source, INDEX)) {
            size = sky_integer_value(sky_number_index(source),
                                     SSIZE_MIN,
                                     SSIZE_MAX,
                                     NULL);
            if (size < 0) {
                sky_error_raise_string(sky_ValueError, "negative size");
            }
            sky_bytestring_initfrombytes(&source_bytes, NULL, size, size);
            memset(source_bytes.bytes, 0, size);
            if (source_bytes.bytes != source_bytes.u.tiny_bytes) {
                copy = SKY_FALSE;
            }
        }
        else if ((object = sky_object_bytes(source)) != NULL) {
            sky_bytearray_bufferasbytes(&source_buffer, object, &source_bytes);
        }
        else {
            sky_error_raise_format(sky_TypeError, 
                                   "cannot create %#@ from %#@",
                                   sky_type_name(sky_bytearray_type),
                                   sky_type_name(sky_object_type(source)));
        }

        lockable = sky_bytearray_islockable(self);
        if (!lockable) {
            new_bytes = self_data->bytes;
            sky_bytestring_cleanup(self_data->bytes);
        }
        else {
            new_bytes = sky_asset_malloc(sizeof(sky_bytestring_t),
                                         SKY_ASSET_CLEANUP_ON_ERROR);
        }

        if (copy) {
            sky_bytestring_initfrombytes(new_bytes,
                                         source_bytes.bytes,
                                         source_bytes.used,
                                         0);
        }
        else {
            sky_bytestring_initwithbytes(new_bytes,
                                         source_bytes.bytes,
                                         source_bytes.used,
                                         sky_free);
        }

        if (lockable) {
            old_bytes = sky_atomic_exchange(new_bytes,
                                            SKY_AS_VOIDP(&(self_data->bytes)));
        }
    } SKY_ASSET_BLOCK_END;

    sky_bytearray_bytes_retire(self_data, old_bytes);
}


void
sky_bytearray_insert(sky_bytearray_t self, ssize_t index, ssize_t x)
{
    sky_bytestring_t    *bytes;

    if (x < 0 || x > 255) {
        sky_error_raise_string(sky_ValueError,
                               "byte must be in range(0, 256)");
    }

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 1) {
        uint8_t byte = (uint8_t)x;

        sky_bytestring_insert(bytes, index, &byte, 1);
    } SKY_BYTEARRAY_WRITE_END(bytes);
}

static const char *sky_bytearray_insert_doc =
"B.insert(index, int) -> None\n\n"
"Insert a single item into the bytearray before the given index.";


sky_bool_t
sky_bytearray_isalnum(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_isalnum(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_isalnum_doc =
"B.isalnum() -> bool\n\n"
"Return True if all characters in B are alphanumeric\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytearray_isalpha(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_isalpha(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_isalpha_doc =
"B.isalpha() -> bool\n\n"
"Return True if all characters in B are alphabetic\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytearray_isdigit(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_isdigit(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_isdigit_doc =
"B.isdigit() -> bool\n\n"
"Return True if all characters in B are digits\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytearray_islower(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_islower(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_islower_doc =
"B.islower() -> bool\n\n"
"Return True if all cased characters in B are lowercase and there is\n"
"at least one cased character in B, False otherwise.";


sky_bool_t
sky_bytearray_isspace(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_isspace(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_isspace_doc =
"B.isspace() -> bool\n\n"
"Return True if all characters in B are whitespace\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytearray_istitle(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_istitle(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_istitle_doc =
"B.istitle() -> bool\n\n"
"Return True if B is a titlecased string and there is at least one\n"
"character in B, i.e. uppercase characters may only follow uncased\n"
"characters and lowercase characters only cased ones. Return False\n"
"otherwise.";


sky_bool_t
sky_bytearray_isupper(sky_bytearray_t self)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        result = sky_bytestring_isupper(sky_bytearray_bytes(self));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_isupper_doc =
"B.isupper() -> bool\n\n"
"Return True if all cased characters in B are uppercase and there is\n"
"at least one cased character in B, False otherwise.";


sky_object_t
sky_bytearray_iter(sky_bytearray_t self)
{
    sky_bytearray_iterator_t        iterator;
    sky_bytearray_iterator_data_t   *iterator_data;

    iterator = sky_object_allocate(sky_bytearray_iterator_type);
    iterator_data = sky_bytearray_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->bytearray)),
                      self,
                      iterator);

    return iterator;
}


typedef struct sky_bytearray_join_item_s {
    sky_buffer_t                        buffer;
    sky_bytestring_t                    bytes;
} sky_bytearray_join_item_t;

typedef struct sky_bytearray_join_list_s {
    ssize_t                             len;
    ssize_t                             size;
    sky_bytearray_join_item_t           items[0];
} sky_bytearray_join_list_t;

static void
sky_bytearray_join_list_append(sky_bytearray_join_list_t    **list,
                               sky_object_t                 object)
{
    size_t                      new_size;
    sky_buffer_t                *buffer;
    sky_bytestring_t            *bytes;
    sky_bytearray_join_item_t   *item;

    if ((*list)->len >= (*list)->size) {
        new_size = sizeof(sky_bytearray_join_list_t) +
                   (((*list)->size + 1) * sizeof(sky_bytearray_join_item_t));
        (*list) = sky_asset_realloc((*list),
                                    new_size,
                                    SKY_ASSET_CLEANUP_ALWAYS,
                                    SKY_ASSET_UPDATE_ALL_DEEP);
        (*list)->size =
                (sky_memsize((*list)) - sizeof(sky_bytearray_join_list_t)) /
                sizeof(sky_bytearray_join_item_t);
    }
    item = &((*list)->items[(*list)->len]);
    buffer = &(item->buffer);
    bytes = &(item->bytes);

    /* Can't use sky_bytearray_bufferasbytes() here, because it'll store an
     * asset in the current asset block to cleanup. That asset block could be
     * the fast sequence iterator's asset block, which means it'll get cleaned
     * up before we're ready to use it. That's kind of the whole point as to
     * why we have the join list in the first place.
     */
    sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    bytes->bytes = buffer->buf;
    bytes->used = buffer->len;
    bytes->size = buffer->len;
    bytes->u.free = NULL;

    ++(*list)->len;
}

static void
sky_bytearray_join_list_cleanup(sky_bytearray_join_list_t *list)
{
    ssize_t i;

    for (i = 0; i < list->len; ++i) {
        sky_buffer_release(&(list->items[i].buffer));
    }
    sky_free(list);
}


sky_bytearray_t
sky_bytearray_join(sky_bytearray_t self, sky_object_t iterable)
{
    sky_bytearray_t result;

    if (!sky_object_isiterable(iterable)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(iterable)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        ssize_t                     i, size;
        sky_object_t                iterator, object;
        sky_bytestring_t            *result_bytes;
        sky_bytearray_join_list_t   *list;

        size = sizeof(sky_bytearray_join_list_t) +
               (sky_object_length_hint(iterable, 8) *
                sizeof(sky_bytearray_join_item_t));
        list = sky_malloc(size);
        list->len = 0;
        list->size = (sky_memsize(list) - sizeof(sky_bytearray_join_list_t)) /
                     sizeof(sky_bytearray_join_item_t);
        sky_asset_save(list,
                       (sky_free_t)sky_bytearray_join_list_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        size = 0;
        if (sky_sequence_isiterable(iterable)) {
            SKY_SEQUENCE_FOREACH(iterable, object) {
                sky_bytearray_join_list_append(&list, object);
                if (SSIZE_MAX - size < list->items[list->len - 1].bytes.used) {
                    sky_error_raise_string(sky_OverflowError, "result too big");
                }
                size += list->items[list->len - 1].bytes.used;
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iterator = sky_object_iter(iterable);
            while ((object = sky_object_next(iterator, NULL)) != NULL) {
                sky_bytearray_join_list_append(&list, object);
                if (SSIZE_MAX - size < list->items[list->len - 1].bytes.used) {
                    sky_error_raise_string(sky_OverflowError, "result too big");
                }
                size += list->items[list->len - 1].bytes.used;
            }
        }
        if (!list->len) {
            result = sky_bytearray_empty();
        }
        else {
            if ((SSIZE_MAX - size) / list->len < self_bytes->used) {
                sky_error_raise_string(sky_OverflowError, "result too big");
            }
            size += (list->len * self_bytes->used);

            result = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_bytes(result);
            sky_bytestring_initfrombytes(result_bytes, NULL, 0, size);
            sky_bytestring_concat(result_bytes, &(list->items[0].bytes));
            for (i = 1; i < list->len; ++i) {
                sky_bytestring_concat(result_bytes, self_bytes);
                sky_bytestring_concat(result_bytes, &(list->items[i].bytes));
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_join_doc =
"B.join(iterable_of_bytes) -> bytes\n\n"
"Concatenate any number of bytearray objects, with B in between each pair.\n"
"Example: b'.'.join([b'ab', b'pq', b'rs']) -> b'ab.pq.rs'.";


sky_object_t
sky_bytearray_le(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


ssize_t
sky_bytearray_len(sky_bytearray_t self)
{
    ssize_t len;

    SKY_ASSET_BLOCK_BEGIN {
        len = sky_bytearray_bytes(self)->used;
    } SKY_ASSET_BLOCK_END;

    return len;
}


sky_bytearray_t
sky_bytearray_ljust(sky_bytearray_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes;

        sky_bytearray_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_ljust(self_bytes,
                                  width,
                                  fillchar_bytes.bytes[0],
                                  &result_bytes))
        {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_ljust_doc =
"B.ljust(width[, fillchar]) -> copy of B\n\n"
"Return B left justified in a string of length width. Padding is\n"
"done using the specified fill character (default is a space).";


sky_bytearray_t
sky_bytearray_lower(sky_bytearray_t self)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_lower(self_bytes, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_lower_doc =
"B.lower() -> copy of B\n\n"
"Return a copy of B with all ASCII characters converted to lowercase.";


sky_bytearray_t
sky_bytearray_lstrip(sky_bytearray_t self, sky_object_t bytes)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_bool_t  stripped;

        if (!bytes || sky_None == bytes) {
            stripped = sky_bytestring_lstrip(self_bytes, NULL, &result_bytes);
        }
        else {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytearray_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_lstrip(self_bytes,
                                             &bytes_bytes,
                                             &result_bytes);
        }

        if (!stripped) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_lstrip_doc =
"B.lstrip([bytes]) -> bytes\n\n"
"Strip leading bytes contained in the argument.\n"
"If the argument is omitted, strip leading ASCII whitespace.";


sky_object_t
sky_bytearray_lt(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_bytes_t
sky_bytearray_maketrans(sky_object_t from, sky_object_t to)
{
    return sky_bytes_maketrans(from, to);
}

static const char *sky_bytearray_maketrans_doc =
"bytearray.maketrans(from, to) -> translation table\n\n"
"Return a translation table (a bytes object of length 256) suitable\n"
"for use in the bytes or bytearray translate method where each byte\n"
"in from is mapped to the byte at the same position in to.\n"
"The bytes objects from and to must be of the same length.";


sky_bytearray_t
sky_bytearray_mul(sky_bytearray_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot multiply %#@ by non-int of type %#@",
                               sky_type_name(sky_object_type(self)),
                               sky_type_name(sky_object_type(other)));
    }
    other = sky_number_index(other);
    count = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);

    return sky_bytearray_repeat(self, count);
}


sky_object_t
sky_bytearray_ne(sky_bytearray_t self, sky_object_t other)
{
    return sky_bytearray_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_tuple_t
sky_bytearray_partition(sky_bytearray_t self, sky_object_t sep)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        ssize_t             index;
        sky_buffer_t        sep_buffer;
        sky_bytestring_t    sep_bytes;

        sky_bytearray_bufferasbytes(&sep_buffer, sep, &sep_bytes);
        if (!sep_bytes.used) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        index = sky_bytestring_find(self_bytes, &sep_bytes, 0, SSIZE_MAX);
        if (index < 0) {
            result = sky_tuple_pack(3, sky_bytearray_copy(self),
                                       sky_bytearray_empty(),
                                       sky_bytearray_empty());
        }
        else {
            result = sky_object_build(
                            "(Y#OY#)",
                            self_bytes->bytes, index,
                            sep,
                            self_bytes->bytes + index + sep_bytes.used,
                            self_bytes->used - index - sep_bytes.used);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_partition_doc =
"B.partition(sep) -> (head, sep, tail)\n\n"
"Search for the separator sep in B, and return the part before it,\n"
"the separator itself, and the part after it.  If the separator is not\n"
"found, returns B and two empty bytes objects.";


uint8_t
sky_bytearray_pop(sky_bytearray_t self, ssize_t index)
{
    uint8_t             result;
    sky_bytestring_t    *bytes;

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
        if (!bytes->used) {
            sky_error_raise_string(sky_IndexError,
                                   "pop from empty bytearray");
        }
        result = sky_bytestring_pop(bytes, index);
    } SKY_BYTEARRAY_WRITE_END(bytes);

    return result;
}

static const char *sky_bytearray_pop_doc =
"B.pop([index]) -> int\n\n"
"Remove and return a single item from B. If no index\n"
"argument is given, will pop the last value.";


sky_bytearray_t
sky_bytearray_radd(sky_bytearray_t self, sky_object_t other)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        ssize_t             result_len;
        sky_buffer_t        other_buffer;
        sky_bytestring_t    other_bytes, *result_bytes;

        sky_bytearray_bufferasbytes(&other_buffer, other, &other_bytes);

        if (SSIZE_MAX - self_bytes->used <= other_bytes.used) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }
        if (!(result_len = self_bytes->used + other_bytes.used)) {
            result = sky_bytearray_empty();
        }
        else {
            result = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_bytes(result);
            sky_bytestring_initfrombytes(result_bytes,
                                         other_bytes.bytes,
                                         other_bytes.used,
                                         result_len);
            sky_bytestring_concat(result_bytes, self_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_bytearray_reduce_ex(sky_bytearray_t self, int proto)
{
    sky_object_t    dict, result;
    sky_string_t    latin1, str;

    dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), sky_None);
    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (proto < 3) {
            latin1 = SKY_STRING_LITERAL("latin-1");
            if (!self_bytes->used) {
                str = sky_string_empty;
            }
            else {
                str = sky_string_createfrombytes(self_bytes->bytes,
                                                 self_bytes->used,
                                                 latin1,
                                                 NULL);
            }
            result = sky_object_build("(O(OO)O)",
                                      sky_object_type(self),
                                      str,
                                      latin1,
                                      dict);
        }
        else {
            if (!self_bytes->used) {
                result = sky_object_build("(O()O)",
                                          sky_object_type(self),
                                          dict);
            }
            else {
                result = sky_object_build("(O(O)O)",
                                          sky_object_type(self),
                                          sky_bytes_createfromobject(self),
                                          dict);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_bytearray_reduce(sky_bytearray_t self)
{
    return sky_bytearray_reduce_ex(self, 2);
}


void
sky_bytearray_remove(sky_bytearray_t self, ssize_t x)
{
    ssize_t             index;
    sky_bytestring_t    *bytes;

    if (x < 0 || x > 255) {
        sky_error_raise_string(sky_ValueError,
                               "byte must be in range(0, 256)");
    }
    index = sky_bytearray_index(self, sky_integer_create(x), 0, SSIZE_MAX);

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
        sky_bytestring_pop(bytes, index);
    } SKY_BYTEARRAY_WRITE_END(bytes);
}

static const char *sky_bytearray_remove_doc =
"B.remove(int) -> None\n\n"
"Remove the first occurrence of a value in B.";


sky_bytearray_t
sky_bytearray_repeat(sky_bytearray_t self, ssize_t count)
{
    sky_bytearray_t result;

    if (count <= 0) {
        return sky_bytearray_empty();
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (1 == count) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            sky_bytestring_t    *result_bytes;

            if ((SSIZE_MAX / count) - 1 < self_bytes->used) {
                sky_error_raise_string(sky_OverflowError, "result too big");
            }

            result = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_data(result)->bytes;
            sky_bytestring_initfrombytes(result_bytes,
                                         self_bytes->bytes,
                                         self_bytes->used,
                                         self_bytes->used * count);
            sky_bytestring_repeat(result_bytes, count);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bytearray_t
sky_bytearray_replace(sky_bytearray_t   self,
                      sky_object_t      old_object,
                      sky_object_t      new_object,
                      ssize_t           count)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t        new_buffer, old_buffer;
        sky_bytestring_t    new_bytes, old_bytes, *result_bytes, *self_bytes;

        sky_bytearray_bufferasbytes(&old_buffer, old_object, &old_bytes);
        sky_bytearray_bufferasbytes(&new_buffer, new_object, &new_bytes);

        result = sky_object_allocate(sky_bytearray_type);
        result_bytes = sky_bytearray_bytes(result);
        if (!sky_bytestring_replace(sky_bytearray_bytes(self),
                                    &old_bytes,
                                    &new_bytes,
                                    count,
                                    result_bytes))
        {
            self_bytes = sky_bytearray_bytes(self);
            if (!self_bytes->used) {
                sky_bytestring_initwithbytes(result_bytes, "", 0, NULL);
            }
            else {
                sky_bytestring_initfrombytes(result_bytes,
                                             self_bytes->bytes,
                                             self_bytes->used,
                                             self_bytes->used);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_replace_doc =
"B.replace(old, new[, count]) -> bytearray\n\n"
"Return a copy of B with all occurrences of subsection\n"
"old replaced by new.  If the optional argument count is\n"
"given, only first count occurrences are replaced.";


sky_string_t
sky_bytearray_repr(sky_bytearray_t self)
{
    sky_string_t    repr;

    SKY_ASSET_BLOCK_BEGIN {
        repr = sky_bytestring_repr(sky_bytearray_bytes(self),
                                   "bytearray(b",
                                   ")");
    } SKY_ASSET_BLOCK_END;

    return repr;
}


void
sky_bytearray_reverse(sky_bytearray_t self)
{
    sky_bytestring_t    *bytes;

    SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
        if (bytes->used) {
            uint8_t *head = bytes->bytes,
                    *tail = bytes->bytes + bytes->used - 1;

            while (head < tail) {
                uint8_t byte = *head;
                *head++ = *tail;
                *tail-- = byte;
            }
        }
    } SKY_BYTEARRAY_WRITE_END(bytes);
}

static const char *sky_bytearray_reverse_doc =
"B.reverse() -> None\n\n"
"Reverse the order of the values in B in place.";


ssize_t
sky_bytearray_rfind(sky_bytearray_t self,
                    sky_object_t    sub,
                    ssize_t         start,
                    ssize_t         end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        sky_bytearray_parse_sub(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_rfind(sky_bytearray_bytes(self),
                                     &sub_bytes,
                                     start,
                                     end);
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytearray_rfind_doc =
"B.rfind(sub[, start[, end]]) -> int\n\n"
"Return the highest index in B where substring sub is found,\n"
"such that sub is contained within B[start:end].  Optional\n"
"arguments start and end are interpreted as in slice notation.\n\n"
"Return -1 on failure.";


ssize_t
sky_bytearray_rindex(sky_bytearray_t    self,
                     sky_object_t       sub,
                     ssize_t            start,
                     ssize_t            end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        sky_bytearray_parse_sub(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_rfind(sky_bytearray_bytes(self),
                                     &sub_bytes,
                                     start,
                                     end);
        if (index < 0) {
            sky_error_raise_string(sky_ValueError, "substring not found");
        }
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytearray_rindex_doc =
"B.rindex(sub[, start[, end]]) -> int\n\n"
"Like B.rfind() but raise ValueError when the substring is not found.";


sky_bytearray_t
sky_bytearray_rjust(sky_bytearray_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes;

        sky_bytearray_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_rjust(self_bytes,
                                  width,
                                  fillchar_bytes.bytes[0],
                                  &result_bytes))
        {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_rjust_doc =
"B.rjust(width[, fillchar]) -> copy of B\n\n"
"Return B right justified in a string of length width. Padding is\n"
"done using the specified fill character (default is a space).";


sky_tuple_t
sky_bytearray_rpartition(sky_bytearray_t self, sky_object_t sep)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        ssize_t             index;
        sky_buffer_t        sep_buffer;
        sky_bytestring_t    sep_bytes;

        sky_bytearray_bufferasbytes(&sep_buffer, sep, &sep_bytes);
        if (!sep_bytes.used) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        index = sky_bytestring_rfind(self_bytes, &sep_bytes, 0, SSIZE_MAX);
        if (index < 0) {
            result = sky_tuple_pack(3, sky_bytearray_empty(),
                                       sky_bytearray_empty(),
                                       sky_bytearray_copy(self));
        }
        else {
            result = sky_object_build(
                            "(Y#OY#)",
                            self_bytes->bytes, index,
                            sep,
                            self_bytes->bytes + index + sep_bytes.used,
                            self_bytes->used - index - sep_bytes.used);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_rpartition_doc =
"B.rpartition(sep) -> (head, sep, tail)\n\n"
"Search for the separator sep in B, starting at the end of B,\n"
"and return the part before it, the separator itself, and the\n"
"part after it.  If the separator is not found, returns two empty\n"
"bytes objects and B.";


sky_list_t
sky_bytearray_rsplit(sky_bytearray_t self, sky_object_t sep, ssize_t maxsplit)
{
    static sky_bytestring_createfrombytes_t createfrombytes =
            (sky_bytestring_createfrombytes_t)sky_bytearray_createfrombytes;

    sky_list_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (sky_object_isnull(sep)) {
            result = sky_bytestring_rsplit(self_bytes,
                                           NULL,
                                           maxsplit,
                                           createfrombytes);
        }
        else {
            sky_buffer_t        sep_buffer;
            sky_bytestring_t    sep_bytes;

            sky_bytearray_bufferasbytes(&sep_buffer, sep, &sep_bytes);
            result = sky_bytestring_rsplit(self_bytes,
                                           &sep_bytes,
                                           maxsplit,
                                           createfrombytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_rsplit_doc =
"B.rsplit(sep=None, maxsplit=-1) -> list of bytes\n\n"
"Return a list of the sections in B, using sep as the delimiter,\n"
"starting at the end of B and working to the front.\n"
"If sep is not given, B is split on ASCII whitespace characters\n"
"(space, tab, return, newline, formfeed, vertical tab).\n"
"If maxsplit is given, at most maxsplit splits are done.";


sky_bytearray_t
sky_bytearray_rstrip(sky_bytearray_t self, sky_object_t bytes)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_bool_t  stripped;

        if (!bytes || sky_None == bytes) {
            stripped = sky_bytestring_rstrip(self_bytes, NULL, &result_bytes);
        }
        else {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytearray_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_rstrip(self_bytes,
                                             &bytes_bytes,
                                             &result_bytes);
        }

        if (!stripped) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_rstrip_doc =
"B.rstrip([bytes]) -> bytes\n\n"
"Strip trailing bytes contained in the argument.\n"
"If the argument is omitted, strip trailing ASCII whitespace.";


void
sky_bytearray_setitem(sky_bytearray_t   self,
                      sky_object_t      item,
                      sky_object_t      value)
{
    ssize_t             i, j, length, start, step, stop;
    uint8_t             byte;
    sky_bytestring_t    *bytes, *value_bytes;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        item = sky_number_index(item);
        start = sky_integer_value(item, SSIZE_MIN, SSIZE_MAX, NULL);

        value = sky_number_index(value);
        SKY_ERROR_TRY {
            byte = sky_integer_value(value, 0, 255, NULL);
        }
        SKY_ERROR_EXCEPT(sky_OverflowError) {
            sky_error_raise_string(sky_ValueError,
                                   "byte must be in range(0, 256)");
        } SKY_ERROR_TRY_END;

        SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
            if (start < 0) {
                start += bytes->used;
            }
            if (start < 0 || start >= bytes->used) {
                sky_error_raise_string(sky_IndexError, "index out of range");
            }
            bytes->bytes[start] = byte;
        } SKY_BYTEARRAY_WRITE_END(bytes);
    }
    else if (sky_object_isa(item, sky_slice_type)) {
        if (value == self || !sky_object_isa(value, sky_bytearray_type)) {
            if (sky_number_check(value) ||
                sky_object_isa(value, sky_string_type))
            {
                sky_error_raise_string(
                        sky_TypeError,
                        "can assign only bytes, buffers, or iterables "
                        "of ints in range(0, 256)");
            }
            value = sky_bytearray_createfromobject(value);
        }

        SKY_BYTEARRAY_WRITE_BEGIN(self, bytes, 0) {
            value_bytes = sky_bytearray_bytes(value);

            length = sky_slice_range(item, bytes->used, &start, &stop, &step);
            if (!length) {
                stop = start;
            }

            if (1 == step) {
                sky_bytestring_resize(bytes,
                                      bytes->used - length + value_bytes->used);
                if (length > value_bytes->used) {
                    memmove(bytes->bytes + start + value_bytes->used,
                            bytes->bytes + stop,
                            bytes->used - stop);
                }
                bytes->used = bytes->used - length + value_bytes->used;
                if (length < value_bytes->used) {
                    memmove(bytes->bytes + start + value_bytes->used,
                            bytes->bytes + stop,
                            bytes->used - start - value_bytes->used);
                }
                if (value_bytes->used > 0) {
                    memcpy(bytes->bytes + start,
                           value_bytes->bytes,
                           value_bytes->used);
                }
                bytes->bytes[bytes->used] = '\0';
            }
            else if (length != value_bytes->used) {
                sky_error_raise_format(sky_ValueError,
                                       "attempt to assign bytes of size %zd "
                                       "to extended slice of size %zd",
                                       value_bytes->used,
                                       length);
            }
            else {
                for (i = start, j = 0; j < length; i += step, ++j) {
                    bytes->bytes[i] = value_bytes->bytes[j];
                }
            }
        } SKY_BYTEARRAY_WRITE_END(bytes);
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "%#@ indices must be integers",
                               sky_type_name(sky_object_type(self)));
    }
}


size_t
sky_bytearray_sizeof(sky_bytearray_t self)
{
    size_t  size;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!self_bytes->bytes) {
            size = sky_memsize(self);
        }
        else if (sky_free == self_bytes->u.free) {
            size = sky_memsize(self) + sky_memsize(self_bytes->bytes);
        }
        else {
            return sky_memsize(self) + self_bytes->size;
        }
    } SKY_ASSET_BLOCK_END;

    return size;
}


sky_bytearray_t
sky_bytearray_slice(sky_bytearray_t self,
                    ssize_t         start,
                    ssize_t         stop,
                    ssize_t         step)
{
    ssize_t             i, j, length;
    sky_bytearray_t     result_object;
    sky_bytestring_t    *result_bytes, *self_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        self_bytes = sky_bytearray_bytes(self);
        length = sky_sequence_indices(self_bytes->used, &start, &stop, &step);
        if (!length) {
            result_object = sky_bytearray_empty();
        }
        else if (length == self_bytes->used) {
            result_object = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result_object = sky_object_allocate(sky_bytearray_type);
            result_bytes = sky_bytearray_data(result_object)->bytes;
            if (1 == step) {
                sky_bytestring_initfrombytes(result_bytes,
                                             &(self_bytes->bytes[start]),
                                             length,
                                             length);
            }
            else {
                sky_bytestring_initfrombytes(result_bytes,
                                             NULL,
                                             length,
                                             length);
                for (i = 0, j = start; i < length; ++i, j += step) {
                    result_bytes->bytes[i] = self_bytes->bytes[j];
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result_object;
}


sky_bytes_t
sky_bytearray_slice_asbytes(sky_bytearray_t self,
                            ssize_t         start,
                            ssize_t         stop,
                            ssize_t         step)
{
    ssize_t             i, j, length;
    sky_bytes_t         result_object;
    sky_bytestring_t    *result_bytes, *self_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        self_bytes = sky_bytearray_bytes(self);
        length = sky_sequence_indices(self_bytes->used, &start, &stop, &step);
        if (!length) {
            result_object = sky_bytes_empty;
        }
        else if (1 == length) {
            result_object = SKY_BYTES_SINGLE(self_bytes->bytes[start]);
        }
        else if (length == self_bytes->used) {
            result_object = sky_bytes_createfrombytestring(self_bytes);
        }
        else {
            result_object = sky_object_allocate(sky_bytes_type);
            result_bytes = sky_bytes_bytes(result_object, NULL);
            if (1 == step) {
                sky_bytestring_initfrombytes(result_bytes,
                                             &(self_bytes->bytes[start]),
                                             length,
                                             length);
            }
            else {
                sky_bytestring_initfrombytes(result_bytes,
                                             NULL,
                                             length,
                                             length);
                for (i = 0, j = start; i < length; ++i, j += step) {
                    result_bytes->bytes[i] = self_bytes->bytes[j];
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result_object;
}


sky_list_t
sky_bytearray_split(sky_bytearray_t self, sky_object_t sep, ssize_t maxsplit)
{
    static sky_bytestring_createfrombytes_t createfrombytes =
            (sky_bytestring_createfrombytes_t)sky_bytearray_createfrombytes;

    sky_list_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (sky_object_isnull(sep)) {
            result = sky_bytestring_split(self_bytes,
                                          NULL,
                                          maxsplit,
                                          createfrombytes);
        }
        else {
            sky_buffer_t        sep_buffer;
            sky_bytestring_t    sep_bytes;

            sky_bytearray_bufferasbytes(&sep_buffer, sep, &sep_bytes);
            result = sky_bytestring_split(self_bytes,
                                          &sep_bytes,
                                          maxsplit,
                                          createfrombytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_split_doc =
"B.split(sep=None, maxsplit=-1) -> list of bytearray\n\n"
"Return a list of the sections in B, using sep as the delimiter.\n"
"If sep is not specified or is None, B is split on ASCII whitespace\n"
"characters (space, tab, return, newline, formfeed, vertical tab).\n"
"If maxsplit is given, at most maxsplit splits are done.";


sky_list_t
sky_bytearray_splitlines(sky_bytearray_t self, sky_bool_t keepends)
{
    sky_list_t  list;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        const uint8_t   *b, *endb, *ending, *startb;

        list = sky_list_create(NULL);
        if (self_bytes->used) {
            startb = self_bytes->bytes;
            endb = self_bytes->bytes + self_bytes->used;
            for (b = startb; b < endb; ++b) {
                while (b < endb && *b != '\n' && *b != '\r') {
                    ++b;
                }
                ending = b;
                if (b < endb) {
                    if (*b == '\r' && b + 1 < endb && *(b + 1) == '\n') {
                        ++b;
                    }
                }
                if (startb == self_bytes->bytes && ending == endb) {
                    sky_list_append(list, sky_bytearray_copy(self));
                }
                else if (keepends) {
                    sky_list_append(list,
                                    sky_bytearray_createfrombytes(
                                            startb,
                                            b - startb + 1));
                }
                else {
                    sky_list_append(list,
                                    sky_bytearray_createfrombytes(
                                            startb,
                                            ending - startb));
                }
                startb = b + 1;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return list;
}

static const char *sky_bytearray_splitlines_doc =
"B.splitlines([keepends]) -> list of lines\n\n"
"Return a list of the lines in B, breaking at line boundaries.\n"
"Line breaks are not included in the resulting list unless keepends\n"
"is given and true.";


sky_bool_t
sky_bytearray_startswith(sky_bytearray_t    self,
                         sky_object_t       prefix,
                         ssize_t            start,
                         ssize_t            end)
{
    sky_bool_t  startswith = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_buffer_t        sub_buffer;
        sky_object_t        object;
        sky_bytestring_t    sub_bytes;

        if (!sky_object_isa(prefix, sky_tuple_type)) {
            if (!sky_buffer_check(prefix)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "endswith first arg must be bytes or a tuple of bytes, "
                        "not %#@",
                        sky_type_name(sky_object_type(prefix)));
            }
            sky_bytearray_bufferasbytes(&sub_buffer, prefix, &sub_bytes);
            startswith = sky_bytestring_startswith(self_bytes,
                                                   &sub_bytes,
                                                   start,
                                                   end);
        }
        else {
            SKY_SEQUENCE_FOREACH(prefix, object) {
                sky_bytearray_bufferasbytes(&sub_buffer, object, &sub_bytes);
                startswith = sky_bytestring_startswith(self_bytes,
                                                       &sub_bytes,
                                                       start,
                                                       end);
                if (startswith) {
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
            } SKY_SEQUENCE_FOREACH_END;
        }
    } SKY_ASSET_BLOCK_END;

    return startswith;
}

static const char *sky_bytearray_startswith_doc =
"B.startswith(prefix[, start[, end]]) -> bool\n\n"
"Return True if B starts with the specified prefix, False otherwise.\n"
"With optional start, test B beginning at that position.\n"
"With optional end, stop comparing B at that position.\n"
"prefix can also be a tuple of bytes to try.";


sky_string_t
sky_bytearray_str(sky_bytearray_t self)
{
    if (sky_interpreter_flags() & (SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS |
                                   SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS))
    {
        sky_error_warn_string(sky_BytesWarning,
                              "str() on a bytearray instance");
    }
    return sky_bytearray_repr(self);
}


sky_bytearray_t
sky_bytearray_strip(sky_bytearray_t self, sky_object_t bytes)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_bool_t  stripped;

        if (!bytes || sky_None == bytes) {
            stripped = sky_bytestring_strip(self_bytes, NULL, &result_bytes);
        }
        else {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytearray_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_strip(self_bytes,
                                            &bytes_bytes,
                                            &result_bytes);
        }

        if (!stripped) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_strip_doc =
"B.strip([bytes]) -> bytearray\n\n"
"Strip leading and trailing bytes contained in the argument.\n"
"If the argument is omitted, strip leading and trailing ASCII whitespace.";


sky_bytearray_t
sky_bytearray_swapcase(sky_bytearray_t self)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_swapcase(self_bytes, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_swapcase_doc =
"B.swapcase() -> copy of B\n\n"
"Return a copy of B with uppercase ASCII characters converted\n"
"to lowercase ASCII and vice versa.";


sky_bytearray_t
sky_bytearray_title(sky_bytearray_t self)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_title(self_bytes, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_title_doc =
"B.title() -> copy of B\n\n"
"Return a titlecased version of B, i.e. ASCII words start with uppercase\n"
"characters, all remaining cased characters have lowercase.";


sky_bytearray_t
sky_bytearray_translate(sky_bytearray_t self,
                        sky_object_t    table,
                        sky_object_t    deletechars)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        sky_bool_t          translated;
        sky_buffer_t        delete_buffer, table_buffer;
        sky_bytestring_t    *delete_bytes, delete_bytestring,
                            *table_bytes, table_bytestring;

        if (sky_object_isnull(table)) {
            table_bytes = NULL;
        }
        else {
            table_bytes = &table_bytestring;
            sky_bytearray_bufferasbytes(&table_buffer, table, table_bytes);
            if (table_bytes->used != 256) {
                sky_error_raise_string(
                        sky_ValueError,
                        "translation table must be 256 characters long");
            }
        }
        if (sky_object_isnull(deletechars)) {
            delete_bytes = NULL;
        }
        else{
            delete_bytes = &delete_bytestring;
            sky_bytearray_bufferasbytes(&delete_buffer,
                                        deletechars,
                                        delete_bytes);
        }

        translated = sky_bytestring_translate(self_bytes,
                                              table_bytes,
                                              delete_bytes,
                                              &result_bytes);

        if (!translated) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_translate_doc =
"B.translate(table[, deletechars]) -> bytearray\n\n"
"Return a copy of B, where all characters occurring in the\n"
"optional argument deletechars are removed, and the remaining\n"
"characters have been mapped through the given translation\n"
"table, which must be a bytes object of length 256.";


sky_bytearray_t
sky_bytearray_upper(sky_bytearray_t self)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_upper(self_bytes, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_upper_doc =
"B.upper() -> copy of B\n\n"
"Return a copy of B with all ASCII characters converted to uppercase.";


sky_bytearray_t
sky_bytearray_zfill(sky_bytearray_t self, ssize_t width)
{
    sky_bytearray_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;
        sky_bytestring_t    *self_bytes = sky_bytearray_bytes(self);

        if (!sky_bytestring_zfill(self_bytes, width, &result_bytes)) {
            result = sky_bytearray_createfrombytestring(self_bytes);
        }
        else {
            result = sky_bytearray_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytearray_zfill_doc =
"B.zfill(width) -> copy of B\n\n"
"Pad a numeric string B with zeros on the left, to fill a field\n"
"of the specified width.  B is never truncated.";


void
sky_bytearray_iterator_init(sky_bytearray_iterator_t    self,
                            sky_object_t                iterable)
{
    sky_bytearray_iterator_data_t   *iterator_data =
                                    sky_bytearray_iterator_data(self);

    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->bytearray)),
                      iterable,
                      self);
}


sky_object_t
sky_bytearray_iterator_iter(sky_bytearray_iterator_t self)
{
    return self;
}


ssize_t
sky_bytearray_iterator_len(sky_bytearray_iterator_t self)
{
    sky_bytearray_iterator_data_t   *iterator_data =
                                    sky_bytearray_iterator_data(self);

    ssize_t len;

    if (!iterator_data->bytearray) {
        return 0;
    }

    len = sky_bytearray_len(iterator_data->bytearray) -
          iterator_data->next_index;
    return (len < 0 ? 0 : len);
}


sky_object_t
sky_bytearray_iterator_next(sky_bytearray_iterator_t self)
{
    sky_bytearray_iterator_data_t   *iterator_data =
                                    sky_bytearray_iterator_data(self);

    uint8_t byte;

    if (!iterator_data->bytearray) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    *bytes =
                            sky_bytearray_bytes(iterator_data->bytearray);

        if (iterator_data->next_index >= bytes->used) {
            iterator_data->bytearray = NULL;
            sky_error_raise_object(sky_StopIteration, sky_None);
        }

        byte = bytes->bytes[iterator_data->next_index++];
    } SKY_ASSET_BLOCK_END;

    return sky_integer_create(byte);
}


sky_object_t
sky_bytearray_iterator_reduce(sky_bytearray_iterator_t self)
{
    sky_bytearray_iterator_data_t   *iterator_data =
                                    sky_bytearray_iterator_data(self);

    if (iterator_data->bytearray) {
        return sky_object_build("(O(O)iz)",
                                sky_module_getbuiltin("iter"),
                                iterator_data->bytearray,
                                iterator_data->next_index);
    }
    return sky_object_build("(O(O))",
                            sky_module_getbuiltin("iter"),
                            sky_bytearray_empty());
}


void
sky_bytearray_iterator_setstate(sky_bytearray_iterator_t    self,
                                sky_object_t                state)
{
    sky_bytearray_iterator_data_t   *self_data =
                                    sky_bytearray_iterator_data(self);

    if (self_data->bytearray) {
        self_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
        if (self_data->next_index < 0) {
            self_data->next_index = 0;
        }
        else if (self_data->next_index > sky_object_len(self_data->bytearray)) {
            self_data->next_index = sky_object_len(self_data->bytearray);
        }
    }
}


static const char sky_bytearray_type_doc[] = 
"bytearray(iterable_of_ints) -> bytearray\n"
"bytearray(string, encoding[, errors]) -> bytearray\n"
"bytearray(bytes_or_buffer) -> mutable copy of bytes_or_buffer\n"
"bytearray(int) -> bytearray object of size given by the parameter initialized with null bytes\n"
"bytearray() -> empty bytearray object\n"
"\n"
"Construct a mutable array of bytes from:\n"
"  - an iterable yielding integers in range(256)\n"
"  - a text string encoded using the specified encoding\n"
"  - any object implementing the buffer API.\n"
"  - an integer";


SKY_TYPE_DEFINE(bytearray,
                "bytearray",
                sizeof(sky_bytearray_data_t),
                sky_bytearray_instance_initialize,
                sky_bytearray_instance_finalize,
                NULL,
                NULL,
                NULL,
                sky_bytearray_instance_buffer_acquire,
                sky_bytearray_instance_buffer_release,
                0,
                sky_bytearray_type_doc);


SKY_TYPE_DEFINE_SIMPLE(bytearray_iterator,
                       "bytearray_iterator",
                       sizeof(sky_bytearray_iterator_data_t),
                       NULL,
                       NULL,
                       sky_bytearray_iterator_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_bytearray_initialize_library(void)
{
    sky_integer_t   ssize_max;

    ssize_max = sky_integer_create(SSIZE_MAX);

    sky_type_setattr_builtin(
            sky_bytearray_type,
            "__alloc__",
            sky_function_createbuiltin(
                    "bytearray.__alloc__",
                    sky_bytearray_alloc_doc,
                    (sky_native_code_function_t)sky_bytearray_alloc,
                    SKY_DATA_TYPE_SIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "append",
            sky_function_createbuiltin(
                    "bytearray.append",
                    sky_bytearray_append_doc,
                    (sky_native_code_function_t)sky_bytearray_append,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "x", SKY_DATA_TYPE_INDEX, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "capitalize",
            sky_function_createbuiltin(
                    "bytearray.capitalize",
                    sky_bytearray_capitalize_doc,
                    (sky_native_code_function_t)sky_bytearray_capitalize,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "center",
            sky_function_createbuiltin(
                    "bytearray.center",
                    sky_bytearray_center_doc,
                    (sky_native_code_function_t)sky_bytearray_center,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "clear",
            sky_function_createbuiltin(
                    "bytearray.clear",
                    sky_bytearray_clear_doc,
                    (sky_native_code_function_t)sky_bytearray_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "copy",
            sky_function_createbuiltin(
                    "bytearray.copy",
                    sky_bytearray_copy_doc,
                    (sky_native_code_function_t)sky_bytearray_copy,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "count",
            sky_function_createbuiltin(
                    "bytearray.count",
                    sky_bytearray_count_doc,
                    (sky_native_code_function_t)sky_bytearray_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "decode",
            sky_function_createbuiltin(
                    "bytearray.decode",
                    sky_bytearray_decode_doc,
                    (sky_native_code_function_t)sky_bytearray_decode,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("utf-8"),
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("strict"),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "endswith",
            sky_function_createbuiltin(
                    "bytearray.endswith",
                    sky_bytearray_endswith_doc,
                    (sky_native_code_function_t)sky_bytearray_endswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "suffix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "expandtabs",
            sky_function_createbuiltin(
                    "bytearray.expandtabs",
                    sky_bytearray_expandtabs_doc,
                    (sky_native_code_function_t)sky_bytearray_expandtabs,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "tabsize", SKY_DATA_TYPE_INDEX, sky_integer_create(8),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "extend",
            sky_function_createbuiltin(
                    "bytearray.extend",
                    sky_bytearray_extend_doc,
                    (sky_native_code_function_t)sky_bytearray_extend,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "x", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "find",
            sky_function_createbuiltin(
                    "bytearray.find",
                    sky_bytearray_find_doc,
                    (sky_native_code_function_t)sky_bytearray_find,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "fromhex",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "bytearray.fromhex",
                            sky_bytearray_fromhex_doc,
                            (sky_native_code_function_t)sky_bytearray_fromhex,
                            SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "index",
            sky_function_createbuiltin(
                    "bytearray.index",
                    sky_bytearray_index_doc,
                    (sky_native_code_function_t)sky_bytearray_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "insert",
            sky_function_createbuiltin(
                    "bytearray.insert",
                    sky_bytearray_insert_doc,
                    (sky_native_code_function_t)sky_bytearray_insert,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "index", SKY_DATA_TYPE_INDEX, NULL,
                    "x", SKY_DATA_TYPE_INDEX, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "isalnum",
            sky_function_createbuiltin(
                    "bytearray.isalnum",
                    sky_bytearray_isalnum_doc,
                    (sky_native_code_function_t)sky_bytearray_isalnum,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "isalpha",
            sky_function_createbuiltin(
                    "bytearray.isalpha",
                    sky_bytearray_isalpha_doc,
                    (sky_native_code_function_t)sky_bytearray_isalpha,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "isdigit",
            sky_function_createbuiltin(
                    "bytearray.isdigit",
                    sky_bytearray_isdigit_doc,
                    (sky_native_code_function_t)sky_bytearray_isdigit,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "islower",
            sky_function_createbuiltin(
                    "bytearray.islower",
                    sky_bytearray_islower_doc,
                    (sky_native_code_function_t)sky_bytearray_islower,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "isspace",
            sky_function_createbuiltin(
                    "bytearray.isspace",
                    sky_bytearray_isspace_doc,
                    (sky_native_code_function_t)sky_bytearray_isspace,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "istitle",
            sky_function_createbuiltin(
                    "bytearray.istitle",
                    sky_bytearray_istitle_doc,
                    (sky_native_code_function_t)sky_bytearray_istitle,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "isupper",
            sky_function_createbuiltin(
                    "bytearray.isupper",
                    sky_bytearray_isupper_doc,
                    (sky_native_code_function_t)sky_bytearray_isupper,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "join",
            sky_function_createbuiltin(
                    "bytearray.join",
                    sky_bytearray_join_doc,
                    (sky_native_code_function_t)sky_bytearray_join,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "ljust",
            sky_function_createbuiltin(
                    "bytearray.ljust",
                    sky_bytearray_ljust_doc,
                    (sky_native_code_function_t)sky_bytearray_ljust,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "lower",
            sky_function_createbuiltin(
                    "bytearray.lower",
                    sky_bytearray_lower_doc,
                    (sky_native_code_function_t)sky_bytearray_lower,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "lstrip",
            sky_function_createbuiltin(
                    "bytearray.lstrip",
                    sky_bytearray_lstrip_doc,
                    (sky_native_code_function_t)sky_bytearray_lstrip,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "maketrans",
            sky_staticmethod_create(
                    sky_function_createbuiltin(
                            "bytearray.maketrans",
                            sky_bytearray_maketrans_doc,
                            (sky_native_code_function_t)sky_bytearray_maketrans,
                            SKY_DATA_TYPE_OBJECT_BYTES,
                            "from", SKY_DATA_TYPE_OBJECT, NULL,
                            "to", SKY_DATA_TYPE_OBJECT, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "partition",
            sky_function_createbuiltin(
                    "bytearray.partition",
                    sky_bytearray_partition_doc,
                    (sky_native_code_function_t)sky_bytearray_partition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "pop",
            sky_function_createbuiltin(
                    "bytearray.pop",
                    sky_bytearray_pop_doc,
                    (sky_native_code_function_t)sky_bytearray_pop,
                    SKY_DATA_TYPE_UINT8,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "index", SKY_DATA_TYPE_INDEX, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "remove",
            sky_function_createbuiltin(
                    "bytearray.remove",
                    sky_bytearray_remove_doc,
                    (sky_native_code_function_t)sky_bytearray_remove,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "x", SKY_DATA_TYPE_INDEX, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "replace",
            sky_function_createbuiltin(
                    "bytearray.replace",
                    sky_bytearray_replace_doc,
                    (sky_native_code_function_t)sky_bytearray_replace,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "old", SKY_DATA_TYPE_OBJECT, NULL,
                    "new", SKY_DATA_TYPE_OBJECT, NULL,
                    "count", SKY_DATA_TYPE_INDEX, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "reverse",
            sky_function_createbuiltin(
                    "bytearray.reverse",
                    sky_bytearray_reverse_doc,
                    (sky_native_code_function_t)sky_bytearray_reverse,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rfind",
            sky_function_createbuiltin(
                    "bytearray.rfind",
                    sky_bytearray_rfind_doc,
                    (sky_native_code_function_t)sky_bytearray_rfind,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rindex",
            sky_function_createbuiltin(
                    "bytearray.rindex",
                    sky_bytearray_rindex_doc,
                    (sky_native_code_function_t)sky_bytearray_rindex,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rjust",
            sky_function_createbuiltin(
                    "bytearray.rjust",
                    sky_bytearray_rjust_doc,
                    (sky_native_code_function_t)sky_bytearray_rjust,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rpartition",
            sky_function_createbuiltin(
                    "bytearray.rpartition",
                    sky_bytearray_rpartition_doc,
                    (sky_native_code_function_t)sky_bytearray_rpartition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rsplit",
            sky_function_createbuiltin(
                    "bytearray.rsplit",
                    sky_bytearray_rsplit_doc,
                    (sky_native_code_function_t)sky_bytearray_rsplit,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, sky_None,
                    "maxsplit", SKY_DATA_TYPE_INDEX, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "rstrip",
            sky_function_createbuiltin(
                    "bytearray.rstrip",
                    sky_bytearray_rstrip_doc,
                    (sky_native_code_function_t)sky_bytearray_rstrip,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "split",
            sky_function_createbuiltin(
                    "bytearray.split",
                    sky_bytearray_split_doc,
                    (sky_native_code_function_t)sky_bytearray_split,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, sky_None,
                    "maxsplit", SKY_DATA_TYPE_INDEX, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "splitlines",
            sky_function_createbuiltin(
                    "bytearray.splitlines",
                    sky_bytearray_splitlines_doc,
                    (sky_native_code_function_t)sky_bytearray_splitlines,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "keepends", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "startswith",
            sky_function_createbuiltin(
                    "bytearray.startswith",
                    sky_bytearray_startswith_doc,
                    (sky_native_code_function_t)sky_bytearray_startswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "prefix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "strip",
            sky_function_createbuiltin(
                    "bytearray.strip",
                    sky_bytearray_strip_doc,
                    (sky_native_code_function_t)sky_bytearray_strip,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "swapcase",
            sky_function_createbuiltin(
                    "bytearray.swapcase",
                    sky_bytearray_swapcase_doc,
                    (sky_native_code_function_t)sky_bytearray_swapcase,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "title",
            sky_function_createbuiltin(
                    "bytearray.title",
                    sky_bytearray_title_doc,
                    (sky_native_code_function_t)sky_bytearray_title,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "translate",
            sky_function_createbuiltin(
                    "bytearray.translate",
                    sky_bytearray_translate_doc,
                    (sky_native_code_function_t)sky_bytearray_translate,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "table", SKY_DATA_TYPE_OBJECT, NULL,
                    "deletechars", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "upper",
            sky_function_createbuiltin(
                    "bytearray.upper",
                    sky_bytearray_upper_doc,
                    (sky_native_code_function_t)sky_bytearray_upper,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytearray_type,
            "zfill",
            sky_function_createbuiltin(
                    "bytearray.zfill",
                    sky_bytearray_zfill_doc,
                    (sky_native_code_function_t)sky_bytearray_zfill,
                    SKY_DATA_TYPE_OBJECT_BYTEARRAY,
                    "self", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    NULL));

    sky_type_setattr_builtin(sky_bytearray_type, "__hash__", sky_None);
    sky_type_setmethodslotwithparameters(
            sky_bytearray_type,
            "__init__",
            (sky_native_code_function_t)sky_bytearray_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "source", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
            "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            NULL);
    sky_type_setmethodslots(sky_bytearray_type,
            "__repr__", sky_bytearray_repr,
            "__str__", sky_bytearray_str,
            "__lt__", sky_bytearray_lt,
            "__le__", sky_bytearray_le,
            "__eq__", sky_bytearray_eq,
            "__ne__", sky_bytearray_ne,
            "__gt__", sky_bytearray_gt,
            "__ge__", sky_bytearray_ge,
            "__sizeof__", sky_bytearray_sizeof,
            "__len__", sky_bytearray_len,
            "__getitem__", sky_bytearray_getitem,
            "__setitem__", sky_bytearray_setitem,
            "__delitem__", sky_bytearray_delitem,
            "__iter__", sky_bytearray_iter,
            "__contains__", sky_bytearray_contains,
            "__add__", sky_bytearray_add,
            "__mul__", sky_bytearray_mul,
            "__radd__", sky_bytearray_radd,
            "__rmul__", sky_bytearray_mul,
            "__iadd__", sky_bytearray_iadd,
            "__imul__", sky_bytearray_imul,
            "__reduce__", sky_bytearray_reduce,
            "__reduce_ex__", sky_bytearray_reduce_ex,
            NULL);


    sky_type_initialize_builtin(sky_bytearray_iterator_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_bytearray_iterator_type,
            "__init__",
            (sky_native_code_function_t)sky_bytearray_iterator_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT_BYTEARRAY, NULL,
            NULL);
    sky_type_setmethodslots(sky_bytearray_iterator_type,
            "__len__", sky_bytearray_iterator_len,
            "__iter__", sky_bytearray_iterator_iter,
            "__next__", sky_bytearray_iterator_next,
            "__reduce__", sky_bytearray_iterator_reduce,
            "__setstate__", sky_bytearray_iterator_setstate,
            NULL);
}
