/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_None The None Object
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NONE_H__
#define __SKYTHON_CORE_SKY_NONE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An object representing the None object. **/
SKY_EXTERN sky_None_t const sky_None;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NONE_H__ */

/** @} **/
/** @} **/
