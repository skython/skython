#include "sky_private.h"


typedef struct sky_member_data_s {
    sky_type_t                          type;
    sky_string_t                        qualname;
    sky_string_t                        name;
    sky_string_t                        doc;
    size_t                              offset;
    sky_data_type_t                     data_type;
    unsigned int                        flags;
    sky_object_t                        data_type_extra;
} sky_member_data_t;

SKY_EXTERN_INLINE sky_member_data_t *
sky_member_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_member_type) {
        return (sky_member_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_member_type);
}


static void
sky_member_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_member_data_t   *member_data = data;

    sky_object_visit(member_data->type, visit_data);
    sky_object_visit(member_data->qualname, visit_data);
    sky_object_visit(member_data->name, visit_data);
    sky_object_visit(member_data->doc, visit_data);
    sky_object_visit(member_data->data_type_extra, visit_data);
}


sky_member_t
sky_member_create(sky_type_t        type,
                  sky_string_t      name,
                  sky_string_t      doc,
                  size_t            offset,
                  sky_data_type_t   data_type,
                  unsigned int      flags,
                  ...)
{
    va_list             ap;
    sky_member_t        member;
    sky_member_data_t   *member_data;

    sky_error_validate(sky_object_isa(type, sky_type_type));
    sky_error_validate(NULL != name);

    member = sky_object_allocate(sky_member_type);
    member_data = sky_member_data(member);
    member_data->offset = offset;
    member_data->data_type = data_type;
    member_data->flags = flags;
    sky_object_gc_set(SKY_AS_OBJECTP(&(member_data->type)), type, member);
    sky_object_gc_set(SKY_AS_OBJECTP(&(member_data->name)), name, member);
    if (doc) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(member_data->doc)), doc, member);
    }

    if (sky_data_type_isextraneeded(member_data->data_type)) {
        va_start(ap, flags);
        sky_object_gc_set(SKY_AS_OBJECTP(&(member_data->data_type_extra)),
                          va_arg(ap, sky_object_t),
                          member);
        va_end(ap);
    }

    return member;
}


void
sky_member_delete(sky_object_t self, sky_object_t instance)
{
    sky_member_data_t   *member_data = sky_member_data(self);

    const char  *object_data;

    if (!sky_object_isa(instance, member_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                member_data->name,
                sky_type_name(member_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    if (member_data->flags & SKY_MEMBER_FLAG_READONLY) {
        sky_error_raise_format(
                sky_AttributeError,
                "%#@ object attribute %#@ is read-only",
                sky_type_name(sky_object_type(instance)),
                member_data->name);
    }

    /* Cannot delete anything other than SKY_DATA_TYPE_OBJECT members that
     * have SKY_MEMBER_FLAG_DELETEABLE set.
     */
    if (!(member_data->data_type & SKY_DATA_TYPE_OBJECT) ||
        !(member_data->flags & SKY_MEMBER_FLAG_DELETEABLE))
    {
        sky_error_raise_format(
                sky_TypeError,
                "%#@ object attribute %#@ cannot be deleted",
                sky_type_name(sky_object_type(instance)),
                member_data->name);
    }

    object_data = (const char *)sky_object_data(instance, member_data->type) +
                  member_data->offset;
    sky_object_gc_set(&(*(sky_object_t *)object_data), NULL, instance);
}


sky_object_t
sky_member_get(           sky_object_t  self,
                          sky_object_t  instance,
               SKY_UNUSED sky_type_t    type)
{
    sky_member_data_t   *member_data = sky_member_data(self);

    const char      *object_data;
    sky_object_t    value;

    if (!instance) {
        return self;
    }
    if (!sky_object_isa(instance, member_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                member_data->name,
                sky_type_name(member_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    object_data = (const char *)sky_object_data(instance, member_data->type) +
                  member_data->offset;
    value = sky_data_type_load((member_data->data_type |
                                SKY_DATA_TYPE_OBJECT_PRESERVE_NULL),
                               object_data, 
                               member_data->data_type_extra);

    if (!value) {
        if (member_data->flags & SKY_MEMBER_FLAG_NULL_EXCEPTION) {
            sky_error_raise_format(
                    sky_AttributeError,
                    "%#@ object has no attribute %#@",
                    sky_type_name(sky_object_type(instance)),
                    member_data->name);
        }
        value = sky_None;
    }

    return value;
}


static sky_object_t
sky_member_qualname_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_member_data_t   *member_data = sky_member_data(self);

    if (!member_data->qualname) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(member_data->qualname)),
                sky_string_createfromformat(
                        "%@.%@",
                        sky_type_qualname(sky_object_type(self)),
                        member_data->name),
                self);
    }

    return member_data->qualname;
}


sky_string_t
sky_member_repr(sky_object_t self)
{
    sky_member_data_t   *member_data = sky_member_data(self);

    return sky_string_createfromformat("<member %#@ of %#@ objects>",
                                       member_data->name,
                                       sky_type_name(member_data->type));
}


void
sky_member_set(sky_object_t self, sky_object_t instance, sky_object_t value)
{
    sky_member_data_t   *member_data = sky_member_data(self);

    const char  *object_data;

    if (!sky_object_isa(instance, member_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                member_data->name,
                sky_type_name(member_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    if ((member_data->flags & SKY_MEMBER_FLAG_READONLY) ||
        SKY_DATA_TYPE_STRING == member_data->data_type)
    {
        sky_error_raise_format(
                sky_AttributeError,
                "%#@ object attribute %#@ is read-only",
                sky_type_name(sky_object_type(instance)),
                member_data->name);
    }

    object_data = (const char *)sky_object_data(instance, member_data->type) +
                  member_data->offset;
    if (sky_None == value) {
        if (!(member_data->flags & SKY_MEMBER_FLAG_ALLOW_NONE)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "expected %#@; got %#@",
                    sky_data_type_name(member_data->data_type,
                                       member_data->data_type_extra),
                    sky_type_name(sky_object_type(value)));
        }
        if (member_data->flags & SKY_MEMBER_FLAG_NONE_IS_NULL) {
            *(sky_object_t *)object_data = NULL;
        }
        else {
            sky_data_type_store(member_data->data_type,
                                self,
                                value,
                                (void **)object_data,
                                member_data->data_type_extra);
        }
    }
    else {
        sky_data_type_store(member_data->data_type,
                            self,
                            value,
                            (void **)object_data,
                            member_data->data_type_extra);
    }
}


SKY_TYPE_DEFINE_SIMPLE(member,
                       "member",
                       sizeof(sky_member_data_t),
                       NULL,
                       NULL,
                       sky_member_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_member_initialize_library(void)
{
    sky_type_setattr_builtin(sky_member_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_member_type,
            "__repr__", sky_member_repr,
            "__get__", sky_member_get,
            "__set__", sky_member_set,
            "__delete__", sky_member_delete,
            NULL);

    sky_type_setattr_getset(sky_member_type,
                            "__qualname__",
                            NULL,
                            sky_member_qualname_getter,
                            NULL);

    sky_type_setmembers(sky_member_type,
            "__doc__",      NULL, offsetof(sky_member_data_t, doc),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__name__",     NULL, offsetof(sky_member_data_t, name),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__objclass__", NULL, offsetof(sky_member_data_t, type),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);
}
