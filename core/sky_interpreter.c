#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"

#include "sky_python_parser.h"
#include "sky_python_compiler.h"

#if defined(HAVE_DLFCN_H)
#   include <dlfcn.h>
#endif
#include <sys/stat.h>
#if defined(HAVE_SYS_PARAM_H)
#   include <sys/param.h>
#endif


typedef struct sky_interpreter_s {
    int                                 optlevel;
    unsigned int                        flags;
    int                                 dlopenflags;
    ssize_t                             recursionlimit;

    sky_object_t                        importlib;
    sky_dict_t                          modules;

    sky_module_t                        builtins;
    sky_module_t                        main;
    sky_module_t                        sys;

    int                                 argc;
    char **                             argv;
    const char *                        argv0;

    sky_string_t                        executable;
    sky_string_t                        exec_prefix;
    sky_list_t                          path;
    sky_string_t                        prefix;

    char **                             warn_options;
    ssize_t                             warn_options_count;
    char *                              warn_options_buffer;
    ssize_t                             warn_options_buffer_length;

    char **                             x_options;
    ssize_t                             x_options_count;
    char *                              x_options_buffer;
    ssize_t                             x_options_buffer_length;

    sky_interpreter_input_t             input_hook;
    sky_bool_t                          input_hook_readline;
} sky_interpreter_t;

static sky_interpreter_t                sky_interpreter_state;


int
sky_interpreter_argc(void)
{
    return sky_interpreter_state.argc;
}


char **
sky_interpreter_argv(void)
{
    return sky_interpreter_state.argv;
}


const char *
sky_interpreter_argv0(void)
{
    return sky_interpreter_state.argv0;
}


int
sky_interpreter_dlopenflags(void)
{
    return sky_interpreter_state.dlopenflags;
}


void
sky_interpreter_setdlopenflags(int value)
{
    sky_interpreter_state.dlopenflags = value;
}


sky_string_t
sky_interpreter_executable(void)
{
    return sky_interpreter_state.executable;
}


sky_string_t
sky_interpreter_exec_prefix(void)
{
    return sky_interpreter_state.exec_prefix;
}


int
sky_interpreter_optlevel(void)
{
    return sky_interpreter_state.optlevel;
}


void
sky_interpreter_setoptlevel(int value)
{
    sky_interpreter_state.optlevel = value;
}


sky_list_t
sky_interpreter_path(void)
{
    return sky_interpreter_state.path;
}


sky_string_t
sky_interpreter_prefix(void)
{
    return sky_interpreter_state.prefix;
}


ssize_t
sky_interpreter_recursionlimit(void)
{
    if (!sky_interpreter_state.recursionlimit) {
        return 1000;
    }
    return sky_interpreter_state.recursionlimit;
}


void
sky_interpreter_setrecursionlimit(ssize_t value)
{
    if (value <= 0) {
        sky_error_raise_string(sky_ValueError,
                               "recursion limit must be positive");
    }
    sky_interpreter_state.recursionlimit = value;
}


void
sky_interpreter_addwarnoption(const char *option)
{
    sky_interpreter_t   *state = &sky_interpreter_state;

    ssize_t option_length = strlen(option) + 1;
    ssize_t needed_length = option_length +
                            state->warn_options_buffer_length;
    ssize_t avail_length = sky_memsize(state->warn_options_buffer);

    ssize_t avail_count;

    if (!state->warn_options) {
        state->warn_options = sky_malloc(sizeof(char *) * 2);
        avail_count = (sky_memsize(state->warn_options) / sizeof(char *)) - 1;
    }
    else {
        avail_count = (sky_memsize(state->warn_options) / sizeof(char *)) - 1;
        if (state->warn_options_count == avail_count) {
            ++avail_count;
            state->warn_options =
                    sky_realloc(state->warn_options,
                                (avail_count + 1) * sizeof(char *));
        }
    }

    if (!state->warn_options_buffer || needed_length > avail_length) {
        state->warn_options_buffer = sky_realloc(state->warn_options_buffer,
                                                 needed_length);
    }

    state->warn_options[state->warn_options_count] =
            state->warn_options_buffer + state->warn_options_buffer_length;
    state->warn_options[state->warn_options_count + 1] = NULL;
    state->warn_options_buffer_length += option_length;

    memcpy(state->warn_options[state->warn_options_count],
           option,
           option_length);
    ++state->warn_options_count;
}

const char * const *
sky_interpreter_warnoptions(void)
{
    return (const char * const *)sky_interpreter_state.warn_options;
}


void
sky_interpreter_addxoption(const char *option)
{
    sky_interpreter_t   *state = &sky_interpreter_state;

    ssize_t option_length = strlen(option) + 1;
    ssize_t needed_length = option_length +
                            state->x_options_buffer_length;
    ssize_t avail_length = sky_memsize(state->x_options_buffer);

    ssize_t avail_count;

    if (!state->x_options) {
        state->x_options = sky_malloc(sizeof(char *) * 2);
        avail_count = (sky_memsize(state->x_options) / sizeof(char *)) - 1;
    }
    else {
        avail_count = (sky_memsize(state->x_options) / sizeof(char *)) - 1;
        if (state->x_options_count == avail_count) {
            ++avail_count;
            state->x_options =
                    sky_realloc(state->x_options,
                                (avail_count + 1) * sizeof(char *));
        }
    }

    if (!state->x_options_buffer || needed_length > avail_length) {
        state->x_options_buffer = sky_realloc(state->x_options_buffer,
                                              needed_length);
    }

    state->x_options[state->x_options_count] =
            state->x_options_buffer + state->x_options_buffer_length;
    state->x_options[state->x_options_count + 1] = NULL;
    state->x_options_buffer_length += option_length;

    memcpy(state->x_options[state->x_options_count],
           option,
           option_length);
    ++state->x_options_count;
}

const char * const *
sky_interpreter_xoptions(void)
{
    return (const char * const *)sky_interpreter_state.x_options;
}


sky_object_t
sky_interpreter_builtins(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    if (!tlsdata->current_frame) {
        return sky_object_dict(sky_interpreter_module_builtins());
    }

    return sky_frame_data(tlsdata->current_frame)->f_builtins;
}


sky_object_t
sky_interpreter_eval(sky_object_t   source,
                     sky_dict_t     globals,
                     sky_object_t   locals)
{
    sky_string_t    builtins;
    sky_code_data_t *code_data;

    if (sky_object_isa(source, sky_string_type) || sky_buffer_check(source)) {
        SKY_ASSET_BLOCK_BEGIN {
            sky_string_t        filename;
            sky_python_parser_t parser;

            filename = SKY_STRING_LITERAL("<string>");
            sky_python_parser_init(&parser,
                                   source,
                                   NULL,
                                   filename,
                                   NULL,
                                   NULL,
                                   0);
            sky_asset_save(&parser,
                           (sky_free_t)sky_python_parser_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);
            source = sky_python_parser_eval_input(&parser);
            source = sky_python_compiler_compile(source,
                                                 filename,
                                                 SKY_PYTHON_COMPILER_MODE_EVAL,
                                                 0,
                                                 -1);
        } SKY_ASSET_BLOCK_END;
    }
    if (!sky_object_isa(source, sky_code_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "eval() arg 1 must be a string, bytes or code object");
    }
    code_data = sky_code_data(source);

    if (sky_object_isnull(globals)) {
        globals = sky_interpreter_globals();
        if (sky_object_isnull(locals)) {
            locals = sky_interpreter_locals();
        }
    }
    else if (sky_object_isnull(locals)) {
        locals = globals;
    }

    builtins = SKY_STRING_LITERAL("__builtins__");
    if (!sky_dict_get(globals, builtins, NULL)) {
        sky_dict_setitem(globals, builtins, sky_interpreter_builtins());
    }

    if (sky_object_bool(code_data->co_freevars)) {
        sky_error_raise_string(
                sky_TypeError,
                "code object passed to eval() may not contain free variables");
    }

    return sky_code_execute(source, globals, locals, NULL, 0, NULL);
}


void
sky_interpreter_exec(sky_object_t   source,
                     sky_dict_t     globals,
                     sky_object_t   locals)
{
    sky_string_t    builtins;
    sky_code_data_t *code_data;

    if (sky_object_isa(source, sky_string_type) || sky_buffer_check(source)) {
        SKY_ASSET_BLOCK_BEGIN {
            sky_string_t        filename;
            sky_python_parser_t parser;

            filename = SKY_STRING_LITERAL("<string>");
            sky_python_parser_init(&parser,
                                   source,
                                   NULL,
                                   filename,
                                   NULL,
                                   NULL,
                                   0);
            sky_asset_save(&parser,
                           (sky_free_t)sky_python_parser_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);
            source = sky_python_parser_file_input(&parser);
            source = sky_python_compiler_compile(source,
                                                 filename,
                                                 SKY_PYTHON_COMPILER_MODE_FILE,
                                                 0,
                                                 -1);
        } SKY_ASSET_BLOCK_END;
    }
    if (!sky_object_isa(source, sky_code_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "exec() arg 1 must be a string, bytes or code object");
    }
    code_data = sky_code_data(source);

    if (sky_object_isnull(globals)) {
        globals = sky_interpreter_globals();
        if (sky_object_isnull(locals)) {
            locals = sky_interpreter_locals();
        }
    }
    else if (sky_object_isnull(locals)) {
        locals = globals;
    }

    builtins = SKY_STRING_LITERAL("__builtins__");
    if (!sky_dict_get(globals, builtins, NULL)) {
        sky_dict_setitem(globals, builtins, sky_interpreter_builtins());
    }

    if (sky_object_bool(code_data->co_freevars)) {
        sky_error_raise_string(
                sky_TypeError,
                "code object passed to exec() may not contain free variables");
    }

    sky_code_execute(source, globals, locals, NULL, 0, NULL);
}


unsigned int
sky_interpreter_flags(void)
{
    return sky_interpreter_state.flags;
}


sky_dict_t
sky_interpreter_globals(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    if (!tlsdata->current_frame) {
        if (!sky_interpreter_module_main()) {
            return NULL;
        }
        return sky_object_dict(sky_interpreter_module_main());
    }

    return sky_frame_data(tlsdata->current_frame)->f_globals;
}


sky_object_t
sky_interpreter_importlib(void)
{
    return sky_interpreter_state.importlib;
}


sky_object_t
sky_interpreter_locals(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    if (!tlsdata->current_frame) {
        return sky_object_dict(sky_interpreter_module_main());
    }
    return sky_frame_locals(tlsdata->current_frame);
}


sky_module_t
sky_interpreter_module_builtins(void)
{
    return sky_interpreter_state.builtins;
}


sky_module_t
sky_interpreter_module_main(void)
{
    return sky_interpreter_state.main;
}


sky_module_t
sky_interpreter_module_sys(void)
{
    return sky_interpreter_state.sys;
}


sky_dict_t
sky_interpreter_modules(void)
{
    return sky_interpreter_state.modules;
}


void
sky_interpreter_addmodule(sky_module_t module)
{
    sky_string_t    name;

    if (!sky_object_isa(module, sky_module_type)) {
        sky_error_raise_format(sky_TypeError,
                               "module must be %#@; got %#@",
                               sky_type_name(sky_module_type),
                               sky_type_name(sky_object_type(module)));
    }

    name = sky_object_getattr(module,
                              SKY_STRING_LITERAL("__name__"),
                              sky_NotSpecified);
    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "module __name__ must be %#@; got %#@",
                               sky_type_name(sky_string_type),
                               sky_type_name(sky_object_type(name)));
    }

    sky_dict_setitem(sky_interpreter_state.modules, name, module);
}


sky_object_t
sky_interpreter_gettrace(void)
{
    return sky_tlsdata_get()->trace_function;
}


void
sky_interpreter_settrace(sky_object_t function)
{
    if (sky_object_isnull(function)) {
        function = NULL;
    }
    else if (!sky_object_callable(function)) {
        sky_error_raise_string(sky_TypeError,
                               "trace function must be callable");
    }
    sky_object_gc_setroot(&(sky_tlsdata_get()->trace_function),
                          function,
                          SKY_FALSE);
}


sky_bool_t
sky_interpreter_interactive(unsigned int flags)
{
    sky_bool_t          eof;
    sky_code_t          code;
    sky_dict_t          dict;
    sky_module_t        main_module, sys;
    sky_object_t        ast, file;
    sky_string_t        encoding, filename, ps1, ps2;
    sky_python_parser_t parser;

    sys = sky_interpreter_module_sys();
    if (!(ps1 = sky_object_getattr(sys, SKY_STRING_LITERAL("ps1"), NULL))) {
        ps1 = SKY_STRING_LITERAL(">>> ");
        sky_module_setattr(sys, "ps1", ps1);
    }
    if (!(ps2 = sky_object_getattr(sys, SKY_STRING_LITERAL("ps2"), NULL))) {
        ps2 = SKY_STRING_LITERAL("... ");
        sky_module_setattr(sys, "ps2", ps2);
    }
    if (!(file = sky_object_getattr(sys, SKY_STRING_LITERAL("stdin"), NULL))) {
        file = sky_object_getattr(sys, SKY_STRING_LITERAL("__stdin__"), NULL);
        if (!file) {
            sky_error_fatal("sys module has no stdin");
        }
    }
    encoding = sky_object_getattr(file,
                                  SKY_STRING_LITERAL("encoding"),
                                  sky_NotSpecified);
    filename = sky_object_getattr(file, SKY_STRING_LITERAL("name"), NULL);
    if (!filename) {
        filename = SKY_STRING_LITERAL("<stdin>");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_python_parser_init(&parser,
                               file,
                               encoding,
                               filename,
                               ps1,
                               ps2,
                               flags);
        sky_asset_save(&parser,
                       (sky_free_t)sky_python_parser_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (!(ast = sky_python_parser_interactive_input(&parser))) {
            eof = SKY_TRUE;
        }
        else {
            eof = SKY_FALSE;
            code = sky_python_compiler_compile(
                            ast,
                            filename,
                            SKY_PYTHON_COMPILER_MODE_INTERACTIVE,
                            flags,
                            sky_interpreter_optlevel());
            main_module = sky_interpreter_module_main();
            dict = sky_object_dict(main_module);

            sky_code_execute(code, dict, dict, NULL, 0, NULL);
        }
    } SKY_ASSET_BLOCK_END;

    return eof;
}


void
sky_interpreter_interactive_loop(unsigned int flags)
{
    sky_bool_t  done;

    do {
        SKY_ERROR_TRY {
            done = sky_interpreter_interactive(flags);
        } SKY_ERROR_EXCEPT(sky_SystemExit) {
            sky_error_raise();
        } SKY_ERROR_EXCEPT_ANY {
            sky_error_display(sky_error_current());
            done = SKY_FALSE;
        } SKY_ERROR_TRY_END;
    } while (!done);
}


static sky_string_t
sky_interpreter_input_default(sky_string_t prompt)
{
    sky_module_t    sys;
    sky_object_t    sys_stdin, sys_stdout;

    sys = sky_module_import(SKY_STRING_LITERAL("sys"));

    if (!sky_object_isnull(prompt)) {
        sys_stdout = sky_object_getattr(sys,
                                        SKY_STRING_LITERAL("stdout"),
                                        sky_NotSpecified);

        sky_file_write(sys_stdout, prompt);
        SKY_ERROR_TRY {
            sky_file_flush(sys_stdout);
        } SKY_ERROR_EXCEPT_ANY {
            /* pass */
        } SKY_ERROR_TRY_END;
    }

    sys_stdin = sky_object_getattr(sys,
                                   SKY_STRING_LITERAL("stdin"),
                                   sky_NotSpecified);
    return sky_file_readline(sys_stdin, -1);
}


sky_interpreter_input_t
sky_interpreter_inputhook(void)
{
    return sky_interpreter_state.input_hook;
}


sky_interpreter_input_t
sky_interpreter_setinputhook(sky_interpreter_input_t hook)
{
    sky_interpreter_input_t old_hook;

    old_hook = sky_interpreter_state.input_hook;
    if (!hook) {
        hook = sky_interpreter_input_default;
    }
    sky_interpreter_state.input_hook = hook;

    return old_hook;
}


sky_string_t
sky_interpreter_input(sky_string_t prompt)
{
    ssize_t         len;
    sky_bool_t      use_hook;
    sky_module_t    sys;
    sky_object_t    sys_stderr, sys_stdin, sys_stdout;
    sky_string_t    input;

    if (!sky_interpreter_state.input_hook_readline) {
        sky_interpreter_state.input_hook_readline = SKY_TRUE;
        SKY_ERROR_TRY {
            sky_module_import(SKY_STRING_LITERAL("readline"));
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;
    }

    /* Make sure the stdin/stdout/stderr are all intact */
    sys = sky_module_import(SKY_STRING_LITERAL("sys"));
    sys_stdin = sky_object_getattr(sys, SKY_STRING_LITERAL("stdin"), NULL);
    if (sky_object_isnull(sys_stdin)) {
        sky_error_raise_string(sky_RuntimeError, "input(): lost sys.stdin");
    }
    sys_stdout = sky_object_getattr(sys, SKY_STRING_LITERAL("stdout"), NULL);
    if (sky_object_isnull(sys_stdout)) {
        sky_error_raise_string(sky_RuntimeError, "input(): lost sys.stdout");
    }
    sys_stderr = sky_object_getattr(sys, SKY_STRING_LITERAL("stderr"), NULL);
    if (sky_object_isnull(sys_stderr)) {
        sky_error_raise_string(sky_RuntimeError, "input(): lost sys.stderr");
    }

    /* Make sure that sys_stdin, sys_stdout, and sys_stderr all match C's
     * stdin, stdout, and stderr before calling the input hook. Additionally,
     * make sure that stdin is a tty. If any one of these fails, use the
     * default input hook.
     */
    use_hook = SKY_TRUE;
    SKY_ERROR_TRY {
        if (sky_file_fileno(sys_stdin) != fileno(stdin) ||
            sky_file_fileno(sys_stdout) != fileno(stdout) ||
            sky_file_fileno(sys_stderr) != fileno(stderr) ||
            !sky_file_isatty(sys_stdin))
        {
            use_hook = SKY_FALSE;
        }
    } SKY_ERROR_EXCEPT_ANY {
        use_hook = SKY_FALSE;
    } SKY_ERROR_TRY_END;

    SKY_ERROR_TRY {
        sky_file_flush(sys_stderr);
    } SKY_ERROR_EXCEPT_ANY {
        /* pass */
    } SKY_ERROR_TRY_END;

    if (prompt) {
        prompt = sky_object_str(prompt);
    }

    if (use_hook && sky_interpreter_state.input_hook) {
        input = sky_interpreter_state.input_hook(prompt);
    }
    else {
        input = sky_interpreter_input_default(prompt);
    }
    if (!sky_object_bool(input)) {
        sky_error_raise_object(sky_EOFError, sky_None);
    }

    /* Strip the line ending for return */
    len = sky_string_len(input);
    if (sky_string_endswith(input, SKY_STRING_LITERAL("\n"), 0, len)) {
        --len;
        if (sky_string_endswith(input, SKY_STRING_LITERAL("\r"), 0, len)) {
            --len;
        }
        input = sky_string_slice(input, 0, len, 1);
    }

    return input;
}


static sky_bool_t
sky_interpreter_isdir(sky_string_t pathname)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        const char  *encoded_pathname;
        sky_bytes_t bytes;
        struct stat st;

        bytes = sky_codec_encodefilename(pathname);
        encoded_pathname = (const char *)sky_bytes_cstring(bytes, SKY_TRUE);

        if (stat(encoded_pathname, &st) == -1) {
            result = SKY_FALSE;
        }
        else if (!S_ISDIR(st.st_mode)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_bool_t
sky_interpreter_isfile(sky_string_t filename)
{
    sky_bool_t  result;

    SKY_ASSET_BLOCK_BEGIN {
        const char  *encoded_filename;
        sky_bytes_t bytes;
        struct stat st;

        bytes = sky_codec_encodefilename(filename);
        encoded_filename = (const char *)sky_bytes_cstring(bytes, SKY_TRUE);

        if (stat(encoded_filename, &st) == -1) {
            result = SKY_FALSE;
        }
        else if (!S_ISREG(st.st_mode)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_bool_t
sky_interpreter_ismodule(sky_string_t filename)
{
    char    suffix;

    if (sky_interpreter_isfile(filename)) {
        return SKY_TRUE;
    }

    suffix = (sky_interpreter_optlevel() ? 'o' : 'c');
    filename = sky_string_createfromformat("%@%c", filename, suffix);
    return sky_interpreter_isfile(filename);
}


static sky_string_t
sky_interpreter_resolve_exec_prefix(sky_string_t    executable_path, 
                                    sky_list_t      home,
                                    sky_bool_t *    build)
{
    char            *line;
    FILE            *fp;
    sky_list_t      list;
    sky_string_t    filename, path, prefix, slash, string;

    *build = SKY_FALSE;

    /* If $PYTHONHOME is set, use it without verifying it. */
    if (sky_object_bool(home)) {
        prefix = (sky_list_len(home) > 1 ? sky_list_get(home, 1)
                                         : sky_list_get(home, 0));
        if (sky_object_bool(prefix)) {
            return sky_string_createfromformat("%@/lib/python%d.%d/lib-dynload",
                                               prefix,
                                               SKYTHON_PYTHON_VERSION_MAJOR,
                                               SKYTHON_PYTHON_VERSION_MINOR);
        }
        if (sky_list_len(home) > 1) {
            prefix = sky_list_get(home, 0);
            if (sky_object_bool(prefix)) {
                return sky_string_createfromformat("%@/lib/python%d.%d/lib-dynload",
                                                   prefix,
                                                   SKYTHON_PYTHON_VERSION_MAJOR,
                                                   SKYTHON_PYTHON_VERSION_MINOR);
            }
        }
    }

    /* Look for pybuilddir.txt in the executable_path. If it's there, open it
     * and read the first line. Use the content of that line as exec_prefix.
     * This file is written out during the build process by sysconfig.py.
     */
    filename = sky_string_createfromformat("%@/pybuilddir.txt", executable_path);

    SKY_ASSET_BLOCK_BEGIN {
        const char      *encoded_path;
        sky_bytes_t     bytes;

        bytes = sky_codec_encodefilename(filename);
        encoded_path = (const char *)sky_bytes_cstring(bytes, SKY_TRUE);

        prefix = NULL;
        if ((fp = fopen(encoded_path, "r")) != NULL) {
            SKY_ASSET_BLOCK_BEGIN {
                sky_asset_save(fp,
                               (sky_free_t)fclose,
                               SKY_ASSET_CLEANUP_ALWAYS);
                line = sky_asset_malloc(MAXPATHLEN + 2,
                                        SKY_ASSET_CLEANUP_ALWAYS);
                if (fgets(line, MAXPATHLEN + 2, fp) != NULL) {
                    if ('\n' == line[strlen(line) - 1]) {
                        line[strlen(line) - 1] = '\0';
                    }
                    string = sky_string_createfrombytes(line,
                                                        strlen(line),
                                                        NULL,
                                                        NULL);
                    if ('/' == line[0]) {
                        prefix = string;
                    }
                    else {
                        prefix = sky_string_createfromformat("%@/%@",
                                                             executable_path,
                                                             string);
                    }
                    *build = SKY_TRUE;
                }
            } SKY_ASSET_BLOCK_END;
        }
    } SKY_ASSET_BLOCK_END;
    if (prefix) {
        return prefix;
    }

    /* Search from executable_path back to root looking for lib-dynload. */
    slash = SKY_STRING_LITERAL("/");
    prefix = executable_path;
    while (sky_object_bool(prefix)) {
        path = sky_string_createfromformat("%@/lib/python/%d.%d/lib-dynload",
                                           prefix,
                                           SKYTHON_PYTHON_VERSION_MAJOR,
                                           SKYTHON_PYTHON_VERSION_MINOR);
        if (sky_interpreter_isdir(path)) {
            return path;
        }
        list = sky_string_rsplit(prefix, slash, 1);
        if (1 == sky_object_len(list)) {
            break;
        }
        prefix = sky_list_get(list, 0);
    }

    /* Last resort: try CMAKE_INSTALL_PREFIX */
    prefix = sky_string_createfromfilename(CMAKE_INSTALL_PREFIX,
                                           strlen(CMAKE_INSTALL_PREFIX));
    path = sky_string_createfromformat("%@/lib/python%d.%d/lib-dynload",
                                       prefix,
                                       SKYTHON_PYTHON_VERSION_MAJOR,
                                       SKYTHON_PYTHON_VERSION_MINOR);
    if (!sky_interpreter_isdir(path)) {
        return NULL;
    }

    return path;
}


static sky_string_t
sky_interpreter_resolve_executable(void)
{
    const char      *argv0;
    sky_string_t    executable;

    argv0 = sky_interpreter_state.argv0;
    if ('/' == *argv0) {
        return sky_string_createfromfilename(argv0, strlen(argv0));
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!strchr(argv0, '/')) {
            char        *fullname;
            size_t      argv0_len, fullname_len, fullname_size;
            const char  *c, *p, *path, *path_end;
            struct stat st;

            argv0_len = strlen(argv0);
            fullname = NULL;
            fullname_size = 0;
            path = getenv("PATH");
            path_end = path + strlen(path);
            executable = sky_string_empty;

            for (p = path; p < path_end; p = c + 1) {
                if (!(c = strchr(p, ':'))) {
                    c = path_end;
                }

                fullname_len = (c - p) + 1 + argv0_len;
                if (fullname_len + 1 > fullname_size) {
                    fullname = sky_asset_realloc(
                                        fullname,
                                        fullname_len + 1,
                                        SKY_ASSET_CLEANUP_ALWAYS,
                                        SKY_ASSET_UPDATE_FIRST_CURRENT);
                    fullname_size = sky_memsize(fullname);
                }
                memcpy(fullname, p, (c - p));
                fullname[c - p] = '/';
                memcpy(fullname + ((c - p) + 1), argv0, argv0_len + 1);
                if (stat(fullname, &st) != -1 && S_ISREG(st.st_mode)) {
                    executable = sky_string_createfromfilename(fullname,
                                                               fullname_len);
                    break;
                }
            }
        }
        else {
            char                    *cwd;
            sky_string_builder_t    builder;

            cwd = getcwd(NULL, 0);
            sky_asset_save(cwd, free, SKY_ASSET_CLEANUP_ALWAYS);

            builder = sky_string_builder_create();
            sky_string_builder_appendfilename(builder, cwd, strlen(cwd));
            sky_string_builder_appendcodepoint(builder, '/', 1);

            while ('.' == argv0[0] && '/' == argv0[1]) {
                argv0 += 2;
            }
            sky_string_builder_appendfilename(builder, argv0, strlen(argv0));

            executable = sky_string_builder_finalize(builder);
        }
    } SKY_ASSET_BLOCK_END;

    return executable;
}


static sky_string_t
sky_interpreter_resolve_path(sky_string_t path)
{
    ssize_t         len;
    sky_list_t      new_segments, old_segments;
    sky_string_t    dot, dotdot, segment, slash;

#if defined(HAVE_READLINK)
    SKY_ASSET_BLOCK_BEGIN {
        char            buf[MAXPATHLEN + 1], encoded_path[MAXPATHLEN + 1];
        ssize_t         len;
        sky_bytes_t     bytes;
        sky_buffer_t    buffer;

        bytes = sky_codec_encodefilename(path);
        sky_buffer_acquire(&buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if ((size_t)buffer.len >= sizeof(encoded_path)) {
            sky_error_raise_string(sky_OverflowError, "buffer too small");
        }
        memcpy(encoded_path, buffer.buf, buffer.len);
        encoded_path[buffer.len] = '\0';

        while ((len = readlink(encoded_path, buf, sizeof(buf) - 1)) != -1) {
            /* readlink() does not append a NUL character to buf. */
            buf[len] = '\0';
            if ('/' == *buf) {
                strcpy(encoded_path, buf);
            }
            else {
                if (strlen(buf) + strlen(encoded_path) + 1 > MAXPATHLEN) {
                    sky_error_raise_string(sky_OverflowError, "buffer too small");
                }
                strcat(encoded_path, "/");
                strcat(encoded_path, buf);
            }
        }

        path = sky_string_createfromfilename(encoded_path, strlen(encoded_path));
    } SKY_ASSET_BLOCK_END;
#endif

    /* Resolve relative segments in the path */
    dotdot = SKY_STRING_LITERAL("..");
    if (sky_object_contains(path, dotdot)) {
        dot = SKY_STRING_LITERAL(".");
        slash = SKY_STRING_LITERAL("/");

        len = 0;
        old_segments = sky_string_split(path, slash, -1);
        new_segments = sky_list_createwithcapacity(sky_object_len(old_segments));
        SKY_SEQUENCE_FOREACH(old_segments, segment) {
            if (sky_object_compare(segment, dot, SKY_COMPARE_OP_EQUAL)) {
                continue;
            }
            if (sky_object_compare(segment, dotdot, SKY_COMPARE_OP_EQUAL)) {
                sky_list_delete(new_segments, len - 1, len, 1);
                --len;
            }
            else {
                sky_list_append(new_segments, segment);
                ++len;
            }
        } SKY_SEQUENCE_FOREACH_END;

        path = sky_string_join(slash, new_segments);
    }

    return path;
}


static sky_string_t
sky_interpreter_resolve_prefix(sky_string_t executable_path,
                               sky_list_t   home,
                               sky_bool_t * build)
{
    sky_list_t      list;
    sky_string_t    path, prefix;

    *build = SKY_FALSE;

    /* If $PYTHONHOME is set, use it without verifying it. */
    if (sky_object_bool(home)) {
        prefix = sky_list_get(home, 0);
        if (sky_object_bool(prefix)) {
            return sky_string_createfromformat("%@/lib/python%d.%d/os.py",
                                               prefix,
                                               SKYTHON_PYTHON_VERSION_MAJOR,
                                               SKYTHON_PYTHON_VERSION_MINOR);
        }
    }

    /* Is the executable running from the build directory? */
    path = sky_string_createfromformat("%@/core/sky_base.h", executable_path);
    if (sky_interpreter_isfile(path)) {
        prefix = sky_string_createfromfilename(CMAKE_SOURCE_DIR,
                                               strlen(CMAKE_SOURCE_DIR));
        path = sky_string_createfromformat("%@/stdlib/os.py", prefix);
        if (sky_interpreter_ismodule(path)) {
            *build = SKY_TRUE;
            return path;
        }
    }

    /* Search from executable_path up to root. */
    prefix = executable_path;
    while (sky_object_bool(prefix)) {
        path = sky_string_createfromformat("%@/lib/python%d.%d/os.py",
                                           prefix,
                                           SKYTHON_PYTHON_VERSION_MAJOR,
                                           SKYTHON_PYTHON_VERSION_MINOR);
        if (sky_interpreter_ismodule(path)) {
            return path;
        }
        list = sky_string_rsplit(prefix, SKY_STRING_LITERAL("/"), 1);
        if (1 == sky_object_len(list)) {
            break;
        }
        prefix = sky_list_get(list, 0);
    }

    /* Last resort: try CMAKE_INSTALL_PREFIX */
    prefix = sky_string_createfromfilename(CMAKE_INSTALL_PREFIX,
                                           strlen(CMAKE_INSTALL_PREFIX));
    path = sky_string_createfromformat("%@/lib/python%d.%d/os.py",
                                       prefix,
                                       SKYTHON_PYTHON_VERSION_MAJOR,
                                       SKYTHON_PYTHON_VERSION_MINOR);
    if (!sky_interpreter_ismodule(path)) {
        return NULL;
    }

    return path;
}


static sky_string_t
sky_interpreter_virtual_home(sky_string_t executable_path)
{
    char            encoded_path[MAXPATHLEN + 1], *line, *p;
    FILE            *fp;
    ssize_t         len, size;
    sky_list_t      list;
    sky_bytes_t     bytes;
    sky_buffer_t    buffer;
    sky_string_t    filename, home, string;

    SKY_ASSET_BLOCK_BEGIN {
        filename = sky_string_createfromformat("%@/pyvenv.cfg", executable_path);
        bytes = sky_codec_encodefilename(filename);
        sky_buffer_acquire(&buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if ((size_t)buffer.len >= sizeof(encoded_path)) {
            sky_error_raise_string(sky_OverflowError, "buffer too small");
        }
        memcpy(encoded_path, buffer.buf, buffer.len);
        encoded_path[buffer.len] = '\0';

        if (!(fp = fopen(encoded_path, "r"))) {
            *strrchr(encoded_path, '/') = '\0';
            if ((p = strrchr(encoded_path, '/')) != NULL) {
                if (p - encoded_path + 11 > MAXPATHLEN) {
                    sky_error_raise_string(sky_OverflowError,
                                           "buffer too small");
                }
                strcpy(p, "/pyvenv.cfg");
                fp = fopen(encoded_path, "r");
            }
        }
    } SKY_ASSET_BLOCK_END;
    if (!fp) {
        return NULL;
    }
    home = NULL;

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(fp, (sky_free_t)fclose, SKY_ASSET_CLEANUP_ALWAYS);

        line = sky_asset_malloc(1024, SKY_ASSET_CLEANUP_ALWAYS);
        size = sky_memsize(line);
        len = 0;

        while ((fgets(line + len, size - len, fp)) != NULL) {
            len = strlen(line);
            if (line[len - 1] != '\n') {
                line = sky_asset_realloc(line,
                                         size * 2,
                                         SKY_ASSET_CLEANUP_ALWAYS,
                                         SKY_ASSET_UPDATE_FIRST_CURRENT);
                size = sky_memsize(line);
                continue;
            }

            if (*line != '#') {
                string = sky_string_createfrombytes(line, len, NULL, NULL);
                list = sky_string_split(string, NULL, 2);
                if (sky_object_len(list) == 3 &&
                    sky_object_compare(sky_list_get(list, 0),
                                       SKY_STRING_LITERAL("home"),
                                       SKY_COMPARE_OP_EQUAL) &&
                    sky_object_compare(sky_list_get(list, 1),
                                       SKY_STRING_LITERAL("="),
                                       SKY_COMPARE_OP_EQUAL))
                {
                    home = sky_list_get(list, 2);
                    break;
                }
            }

            len = 0;
        }
    } SKY_ASSET_BLOCK_END;

    return home;
}


static void
sky_interpreter_initialize_paths(void)
{
    sky_list_t  pythonhome = NULL;

    const char      *envar;
    sky_bool_t      build, exec_build;
    sky_list_t      list;
    sky_string_t    exec_prefix, executable_path, prefix, string;

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.path)),
            sky_list_create(NULL),
            SKY_TRUE);
    sky_list_append(sky_interpreter_state.path, sky_string_empty);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.executable)),
            sky_interpreter_resolve_executable(),
            SKY_TRUE);

    list = sky_string_rsplit(sky_interpreter_state.executable,
                             SKY_STRING_LITERAL("/"),
                             1);
    if (sky_list_len(list) == 1) {
        executable_path = SKY_STRING_LITERAL(".");
    }
    else {
        executable_path = sky_list_get(list, 0);
    }
    executable_path = sky_interpreter_resolve_path(executable_path);

    /* PEP 405 -- Python Virtual Environments
     *
     * Look for a pyvenv.cfg file in executable_path or the parent directory.
     * If it exists, open it and look for a 'home' variable within it. If there
     * is no 'home' variable, do nothing; otherwise, use the variable's value
     * as the new value for executable_path. If the new value is a link, don't
     * resolve it.
     */
    if ((string = sky_interpreter_virtual_home(executable_path)) != NULL) {
        executable_path = string;
    }

    /* If "PYTHONHOME" exists in the environment, use it for prefix and
     * exec_prefix without making any effort to ensure that it's correct.
     */
    if (!(sky_interpreter_state.flags &
          SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT) &&
        (envar = getenv("PYTHONHOME")) != NULL &&
        *envar)
    {
        /* PYTHONHOME=<prefix>[:<exec_prefix>] */
        string = sky_string_createfromfilename(envar, strlen(envar));
        pythonhome = sky_string_split(string, SKY_STRING_LITERAL(":"), -1);
    }

    prefix = sky_interpreter_resolve_prefix(executable_path,
                                            pythonhome,
                                            &build);
    if (!prefix &&
        !(sky_interpreter_state.flags &
          SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES))
    {
        sky_format_fprintf(
                stderr,
                "Could not find platform independent libraries <prefix>\n");
    }

    exec_prefix = sky_interpreter_resolve_exec_prefix(executable_path,
                                                      pythonhome,
                                                      &exec_build);
    if (!exec_prefix &&
        !(sky_interpreter_state.flags &
          SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES))
    {
        sky_format_fprintf(
                stderr,
                "Could not find platform dependent libraries <exec_prefix>\n");
    }

    if (!prefix &&
        !exec_prefix &&
        !(sky_interpreter_state.flags &
          SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES))
    {
        sky_format_fprintf(
                stderr,
                "Consider setting $PYTHONHOME to <prefix>[:<exec_prefix>]\n");
    }
    if (!prefix) {
        string = sky_string_createfromfilename(CMAKE_INSTALL_PREFIX,
                                               strlen(CMAKE_INSTALL_PREFIX));
        prefix = sky_string_createfromformat("%@/lib/python%d.%d",
                                             prefix,
                                             SKYTHON_PYTHON_VERSION_MAJOR,
                                             SKYTHON_PYTHON_VERSION_MINOR);
    }
    else {
        /* Strip off trailing "/os.py" */
        list = sky_string_rsplit(prefix, SKY_STRING_LITERAL("/"), 1);
        prefix = sky_list_get(list, 0);
    }
    if (!exec_prefix) {
        string = sky_string_createfromfilename(CMAKE_INSTALL_PREFIX,
                                               strlen(CMAKE_INSTALL_PREFIX));
        exec_prefix = sky_string_createfromformat("%@/lib/python%d.%d/lib-dynload",
                                                  prefix,
                                                  SKYTHON_PYTHON_VERSION_MAJOR,
                                                  SKYTHON_PYTHON_VERSION_MINOR);
    }

    if (!(sky_interpreter_state.flags &
          SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT) &&
        (envar = getenv("PYTHONPATH")) != NULL &&
        *envar)
    {
        string = sky_string_createfromfilename(envar, strlen(envar));
        sky_list_extend(sky_interpreter_state.path,
                        sky_string_split(string, SKY_STRING_LITERAL(":"), -1));
    }

    if (build) {
        list = sky_string_rsplit(executable_path, SKY_STRING_LITERAL("/"), 1);
    }
    else {
        list = sky_string_rsplit(prefix, SKY_STRING_LITERAL("/"), 1);
    }
    sky_list_append(sky_interpreter_state.path,
                    sky_string_createfromformat("%@/python%d%d.zip",
                                                sky_list_get(list, 0),
                                                SKYTHON_PYTHON_VERSION_MAJOR,
                                                SKYTHON_PYTHON_VERSION_MINOR));

    sky_list_append(sky_interpreter_state.path, prefix);
    sky_list_append(sky_interpreter_state.path,
                    sky_string_createfromformat("%@/plat-%s",
                                                prefix,
                                                SKYTHON_PLATFORM_NAME));
    sky_list_append(sky_interpreter_state.path, exec_prefix);

    if (!build) {
        list = sky_string_rsplit(prefix, SKY_STRING_LITERAL("/"), 2);
        prefix = sky_list_get(list, 0);
    }
    if (!exec_build) {
        list = sky_string_rsplit(exec_prefix, SKY_STRING_LITERAL("/"), 3);
        prefix = sky_list_get(list, 0);
    }

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.exec_prefix)),
            exec_prefix,
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.prefix)),
            prefix,
            SKY_TRUE);
}


static void
sky_interpreter_stdio_flush(void)
{
    sky_module_t    sys;
    sky_object_t    file;

    sys = sky_module_import(SKY_STRING_LITERAL("sys"));

    SKY_ERROR_TRY {
        file = sky_object_getattr(sys, SKY_STRING_LITERAL("stdout"), NULL);
        if (sky_object_bool(file) && !sky_file_closed(file)) {
            sky_file_flush(file);
        }
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_display(sky_error_current());
    } SKY_ERROR_TRY_END;

    SKY_ERROR_TRY {
        file = sky_object_getattr(sys, SKY_STRING_LITERAL("stderr"), NULL);
        if (sky_object_bool(file) && !sky_file_closed(file)) {
            sky_file_flush(file);
        }
    } SKY_ERROR_EXCEPT_ANY {
        /* pass. suppress the exception */
    } SKY_ERROR_TRY_END;
}


static sky_object_t
sky_interpreter_stdio_open(sky_module_t io,
                           int          fileno,
                           sky_bool_t   readonly,
                           sky_string_t name,
                           sky_string_t encoding,
                           sky_string_t errors)
{
    unsigned int    flags = sky_interpreter_flags();

    int             buffering;
    sky_bool_t      line_buffering;
    sky_tuple_t     args;
    sky_object_t    buffer, file, _io, raw, TextIOWrapper;
    sky_string_t    mode;

    if (-1 == fcntl(fileno, F_GETFL) && EBADF == errno) {
        return sky_None;
    }

    if ((flags & SKY_LIBRARY_INITIALIZE_FLAG_UNBUFFERED_STDIO) && !readonly) {
        buffering = 0;
    }
    else {
        buffering = -1;
    }
    if (readonly) {
        mode = SKY_STRING_LITERAL("rb");
    }
    else {
        mode = SKY_STRING_LITERAL("wb");
    }

    args = sky_object_build("(iOiOOOB)",
                            fileno,         /* file         */
                            mode,           /* mode         */
                            buffering,      /* buffering    */
                            sky_None,       /* encoding     */
                            sky_None,       /* errors       */
                            sky_None,       /* newline      */
                            SKY_FALSE);     /* closefd      */
    buffer = sky_object_callmethod(io,
                                   SKY_STRING_LITERAL("open"),
                                   args,
                                   NULL);

    if (!buffering) {
        raw = buffer;
    }
    else {
        raw = sky_object_getattr(buffer,
                                 SKY_STRING_LITERAL("raw"),
                                 sky_NotSpecified);
    }
    sky_object_setattr(raw, SKY_STRING_LITERAL("name"), name);

    _io = sky_module_import(SKY_STRING_LITERAL("_io"));
    TextIOWrapper = sky_object_getattr(_io,
                                       SKY_STRING_LITERAL("TextIOWrapper"),
                                       sky_NotSpecified);
    if ((flags & SKY_LIBRARY_INITIALIZE_FLAG_UNBUFFERED_STDIO) ||
        sky_file_isatty(raw))
    {
        line_buffering = SKY_TRUE;
    }
    else {
        line_buffering = SKY_FALSE;
    }
    file = sky_object_call(TextIOWrapper,
                           sky_object_build("(OOOOB)",
                                            buffer,
                                            encoding,
                                            errors,
#if defined(_WIN32)
                                            NULL,
#else
                                            SKY_STRING_LITERAL("\n"),
#endif
                                            line_buffering),
                           NULL);
    sky_object_setattr(file,
                       SKY_STRING_LITERAL("mode"),
                       (readonly ? SKY_STRING_LITERAL("r")
                                 : SKY_STRING_LITERAL("w")));

    return file;
}


static void
sky_interpreter_initialize_stdio(void)
{
    unsigned int    flags = sky_interpreter_flags();

    const char      *env;
    sky_module_t    io, sys;
    sky_object_t    file, parts;
    sky_string_t    encoding, errors, string;

    io = sky_module_import(SKY_STRING_LITERAL("io"));
    sys = sky_interpreter_module_sys();

    if ((flags & SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT) ||
        !(env = getenv("PYTHONIOENCODING")) ||
        !*env)
    {
        encoding = errors = NULL;
    }
    else {
        string = sky_string_createfrombytes(env, strlen(env), NULL, NULL);
        parts = sky_string_partition(string, SKY_STRING_LITERAL(":"));
        encoding = sky_sequence_get(parts, 0);
        if (!sky_object_bool(encoding)) {
            encoding = NULL;
        }
        errors = sky_sequence_get(parts, 2);
        if (!sky_object_bool(errors)) {
            errors = NULL;
        }
    }

    /* Create sys.stdin, sys.stdout, and sys.stderr */
    file = sky_interpreter_stdio_open(io,
                                      fileno(stdin),
                                      SKY_TRUE,
                                      SKY_STRING_LITERAL("<stdin>"),
                                      encoding,
                                      errors);
    sky_module_setattr(sys, "stdin", file);
    sky_module_setattr(sys, "__stdin__", file);

    file = sky_interpreter_stdio_open(io,
                                      fileno(stdout),
                                      SKY_FALSE,
                                      SKY_STRING_LITERAL("<stdout>"),
                                      encoding,
                                      errors);
    sky_module_setattr(sys, "stdout", file);
    sky_module_setattr(sys, "__stdout__", file);

    file = sky_interpreter_stdio_open(io,
                                      fileno(stderr),
                                      SKY_FALSE,
                                      SKY_STRING_LITERAL("<stderr>"),
                                      encoding,
                                      SKY_STRING_LITERAL("backslashreplace"));
    sky_module_setattr(sys, "stderr", file);
    sky_module_setattr(sys, "__stderr__", file);
}


static void
sky_interpreter_initialize_zipimport(void)
{
    sky_module_t    sys, zipimport;
    sky_object_t    path_hooks, zipimporter;

    sys = sky_module_import(SKY_STRING_LITERAL("sys"));
    path_hooks = sky_object_getattr(sys,
                                    SKY_STRING_LITERAL("path_hooks"),
                                    sky_NotSpecified);

    SKY_ERROR_TRY {
        zipimport = sky_module_import(SKY_STRING_LITERAL("zipimport"));
    } SKY_ERROR_EXCEPT(sky_ImportError) {
        zipimport = NULL;
    } SKY_ERROR_TRY_END;

    /* No zip import module is okay. */
    if (!zipimport) {
        return;
    }

    /* No zipimporter is okay too. */
    if (!(zipimporter = sky_object_getattr(zipimport,
                                           SKY_STRING_LITERAL("zipimporter"),
                                           NULL)))
    {
        return;
    }

    sky_list_insert(path_hooks, 0, zipimporter);
}


static void
sky_interpreter_finalize_library(void)
{
    unsigned int flags = sky_interpreter_state.flags;

    if (!(flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES)) {
        sky_module_t    threading;

        threading = sky_dict_get(sky_interpreter_state.modules,
                                 SKY_STRING_LITERAL("threading"),
                                 NULL);
        if (threading) {
            SKY_ERROR_TRY {
                sky_object_callmethod(threading,
                                      SKY_STRING_LITERAL("_shutdown"),
                                      NULL,
                                      NULL);
            } SKY_ERROR_EXCEPT_ANY {
                sky_error_display(sky_error_current());
            } SKY_ERROR_TRY_END;
        }
    }

    sky_free(sky_interpreter_state.warn_options);
    sky_free(sky_interpreter_state.warn_options_buffer);
    sky_free(sky_interpreter_state.x_options);
    sky_free(sky_interpreter_state.x_options_buffer);

    sky_interpreter_state.warn_options = NULL;
    sky_interpreter_state.warn_options_count = 0;
    sky_interpreter_state.warn_options_buffer = NULL;
    sky_interpreter_state.warn_options_buffer_length = 0;

    sky_interpreter_state.x_options = NULL;
    sky_interpreter_state.x_options_count = 0;
    sky_interpreter_state.x_options_buffer = NULL;
    sky_interpreter_state.x_options_buffer_length = 0;

    if (!(flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES)) {
        sky_interpreter_stdio_flush();
    }
}


void
sky_interpreter_initialize_library(const char * argv0,
                                   int          argc,
                                   char *       argv[],
                                   unsigned int flags)
{
    const char      *env;
    sky_module_t    _imp;
    sky_object_t    loader;

    sky_library_atfinalize(sky_interpreter_finalize_library);
    if (!(flags & SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT)) {
        if ((env = getenv("PYTHONDONTWRITEBYTECODE")) && *env) {
            flags |= SKY_LIBRARY_INITIALIZE_FLAG_DONT_WRITE_BYTECODE;
        }
        if ((env = getenv("PYTHONOPTIMIZE")) && *env) {
            flags |= SKY_LIBRARY_INITIALIZE_FLAG_OPTIMIZE_BYTECODE;
            if (atoi(env) > 0) {
                flags |= SKY_LIBRARY_INITIALIZE_FLAG_DISCARD_DOCSTRINGS;
            }
        }
        if ((env = getenv("PYTHONDEBUG")) && *env) {
            flags |= SKY_LIBRARY_INITIALIZE_FLAG_DEBUG;
        }
        if ((env = getenv("PYTHONUNBUFFERED")) && *env) {
            flags |= SKY_LIBRARY_INITIALIZE_FLAG_UNBUFFERED_STDIO;
        }
        if ((env = getenv("PYTHONVERBOSE")) && *env) {
            flags |= SKY_LIBRARY_INITIALIZE_FLAG_VERBOSE;
            if (atoi(env) > 0) {
                flags |= SKY_LIBRARY_INITIALIZE_FLAG_VERY_VERBOSE;
            }
        }
    }

    sky_interpreter_state.flags = flags;
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_OPTIMIZE_BYTECODE) {
        sky_interpreter_state.optlevel = 1;
    }
    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_DISCARD_DOCSTRINGS) {
        sky_interpreter_state.optlevel = 2;
    }
#if defined(HAVE_DLFCN_H)
    sky_interpreter_state.dlopenflags = RTLD_NOW;
#endif
    sky_interpreter_state.recursionlimit = 1000;

    sky_interpreter_state.argc = argc;
    sky_interpreter_state.argv = argv;
    sky_interpreter_state.argv0 = argv0;
    sky_interpreter_state.input_hook = sky_interpreter_input_default;
    sky_interpreter_state.input_hook_readline = SKY_FALSE;

    /* Initialize executable, exec_prefix, path, and prefix */
    sky_interpreter_initialize_paths();

    sky_error_warn_initialize_library(sky_interpreter_state.flags);

    sky_error_validate(NULL == sky_interpreter_state.modules);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_interpreter_state.modules),
            sky_dict_create(),
            SKY_TRUE);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.builtins)),
            sky_module_import_builtin(SKY_STRING_LITERAL("builtins")),
            SKY_TRUE);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&(sky_interpreter_state.sys)),
            sky_module_import_builtin(SKY_STRING_LITERAL("sys")),
            SKY_TRUE);

    if (!(flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES)) {
        /* initialize importlib */
        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&(sky_interpreter_state.importlib)),
                sky_module_import_frozen(
                        SKY_STRING_LITERAL("_frozen_importlib")),
                SKY_TRUE);

        /* create the _imp module that's needed by importlib */
        _imp = sky_module_import_builtin(SKY_STRING_LITERAL("_imp"));

        sky_object_callmethod(sky_interpreter_state.importlib,
                              SKY_STRING_LITERAL("_install"),
                              sky_tuple_pack(2, sky_interpreter_state.sys,
                                                _imp),
                              NULL);

        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&(sky_interpreter_state.main)),
                sky_module_create(SKY_STRING_LITERAL("__main__")),
                SKY_TRUE);
        sky_interpreter_addmodule(sky_interpreter_state.main);
        sky_module_setattr(sky_interpreter_state.main,
                           "__builtins__",
                           sky_interpreter_state.builtins);

        if (!sky_object_getattr(sky_interpreter_state.main,
                                SKY_STRING_LITERAL("__loader__"),
                                NULL))
        {
            loader = sky_object_getattr(sky_interpreter_state.importlib,
                                        SKY_STRING_LITERAL("BuiltinImporter"),
                                        NULL);
            if (!loader) {
                sky_error_fatal("Failed to retrieve BuiltinImporter");
            }
            sky_object_setattr(sky_interpreter_state.main,
                               SKY_STRING_LITERAL("__loader__"),
                               loader);
        }

        sky_interpreter_initialize_zipimport();

        sky_module_import(SKY_STRING_LITERAL("encodings"));

        sky_module_import(SKY_STRING_LITERAL("signal"));

        sky_interpreter_initialize_stdio();

        if (sky_interpreter_state.warn_options &&
            sky_interpreter_state.warn_options[0])
        {
            SKY_ERROR_TRY {
                sky_module_import(SKY_STRING_LITERAL("warnings"));
            } SKY_ERROR_EXCEPT_ANY {
                sky_format_fprintf(stderr,
                                   "'import warnings' failed; traceback:\n");
                sky_error_display(sky_error_current());
            } SKY_ERROR_TRY_END;
        }

        if (!(flags & SKY_LIBRARY_INITIALIZE_FLAG_NO_SITE)) {
            sky_module_import(SKY_STRING_LITERAL("site"));
        }
    }
}
