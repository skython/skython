#include "sky_private.h"


#if !defined(va_copy)
#   if defined(_WIN32)
#       define va_copy(d, s)            d = s
#   elif (defined(__GNUC__) || defined(__clang__)) && \
       (__STDC_VERSION < 199900 || defined(__STRICT_ANSI__))
#       define va_copy(d, s)            __builtin_va_copy(d, s)
#   endif
#endif

#define FMTLIMIT_ARGCOUNT               256


typedef enum sky_format_modifier_e {
    SKY_FORMAT_MODIFIER_NONE        =   0,
    SKY_FORMAT_MODIFIER_CHAR        =   1,
    SKY_FORMAT_MODIFIER_SHORT       =   2,
    SKY_FORMAT_MODIFIER_LONG        =   3,
    SKY_FORMAT_MODIFIER_LONG_LONG   =   4,
    SKY_FORMAT_MODIFIER_INTMAX_T    =   5,
    SKY_FORMAT_MODIFIER_SIZE_T      =   6,
    SKY_FORMAT_MODIFIER_PTRDIFF_T   =   7,
} sky_format_modifier_t;

typedef struct sky_format_arg_s
{
    sky_data_type_t                     type;
    union {
        intmax_t                        fa_int;
        uintmax_t                       fa_uint;
        void *                          fa_pointer;
        double                          fa_double;
    } fa;
} sky_format_arg_t;

typedef struct sky_format_context_s {
    const char *                        fmtstring;
    const char *                        endof_fmtstring;
    size_t                              nbytes_output;

    char *                              padding_buf;
    size_t                              padding_bufsz;
    char *                              formatting_buf;
    size_t                              formatting_buflen;
    size_t                              formatting_bufsz;

    sky_format_output_t                 output;
    void *                              output_arg;
    va_list                             ap;
    sky_format_arg_t                    arglist[FMTLIMIT_ARGCOUNT];

    int                                 current_ap_index;   /* current index into ap in absence of nn$ */
    int                                 arglist_count;      /* number of elements filled in arglist    */
    sky_bool_t                          fmtstring_scanned;  /* has the format string been scanned?     */
} sky_format_context_t;


static int          sky_format_arg_checkorset   (sky_format_context_t *context, int idx, sky_data_type_t type);
static double       sky_format_arg_getdouble    (sky_format_context_t *context, int idx, sky_data_type_t type);
static intmax_t     sky_format_arg_getint       (sky_format_context_t *context, int idx, sky_data_type_t type);
static void *       sky_format_arg_getpointer   (sky_format_context_t *context, int idx, sky_data_type_t type);
static uintmax_t    sky_format_arg_getuint      (sky_format_context_t *context, int idx, sky_data_type_t type);
static void         sky_format_arg_load         (sky_format_context_t *context, int idx);
static void         sky_format_context_init     (sky_format_context_t *context, const char *fmt, size_t fmtsz, va_list ap, sky_format_output_t output, void *output_arg);
static size_t       sky_format_output_int       (sky_format_context_t *context, sky_format_specification_t *spec, intmax_t value);
static size_t       sky_format_output_padding   (sky_format_context_t *context, sky_format_specification_t *spec, size_t length);
static void         sky_format_output_string    (sky_format_context_t *context, sky_format_specification_t *spec, sky_string_t string);
static size_t       sky_format_output_uint      (sky_format_context_t *context, sky_format_specification_t *spec, uintmax_t value);
static void         sky_format_output_wrapper   (const void *bytes, ssize_t nbytes, size_t width, void *arg);
static size_t       sky_format_parse            (sky_format_context_t *context);
static int          sky_format_parse_argnum     (sky_format_context_t *context, const char **start);
static sky_bool_t   sky_format_parse_optionalint(sky_format_context_t *context, const char **start, ssize_t *value);
static void         sky_format_scan             (sky_format_context_t *context);


static int
sky_format_arg_checkorset(sky_format_context_t *context, int idx, sky_data_type_t type)
{
    if (idx) {
        if (!context->fmtstring_scanned) {
            sky_format_scan(context);
        }
        --idx;
    }
    else {
        if (unlikely(context->current_ap_index == FMTLIMIT_ARGCOUNT)) {
            sky_error_raise_string(sky_ValueError, "too many arguments");
        }
        idx = context->current_ap_index++;
        if (idx == context->arglist_count) {
            context->arglist[idx].type = type;
            context->arglist_count++;
            sky_format_arg_load(context, idx);
        }
    }
    if (unlikely(idx >= context->arglist_count) ||
        unlikely(context->arglist[idx].type == SKY_DATA_TYPE_VOID) ||
        unlikely(context->arglist[idx].type != type))
    {
        sky_error_raise_string(sky_ValueError, "illegal argument specifier");
    }

    return idx;
}


static double
sky_format_arg_getdouble(sky_format_context_t *context, int idx, sky_data_type_t type)
{
    idx = sky_format_arg_checkorset(context, idx, type);
    return context->arglist[idx].fa.fa_double;
}


static intmax_t
sky_format_arg_getint(sky_format_context_t *context, int idx, sky_data_type_t type)
{
    idx = sky_format_arg_checkorset(context, idx, type);
    return context->arglist[idx].fa.fa_int;
}


static void *
sky_format_arg_getpointer(sky_format_context_t *context, int idx, sky_data_type_t type)
{
    idx = sky_format_arg_checkorset(context, idx, type);
    return context->arglist[idx].fa.fa_pointer;
}


static uintmax_t
sky_format_arg_getuint(sky_format_context_t *context, int idx, sky_data_type_t type)
{
    idx = sky_format_arg_checkorset(context, idx, type);
    return context->arglist[idx].fa.fa_uint;
}


static void
sky_format_arg_load(sky_format_context_t *context, int idx)
    /* Extract an argument from the variable argument list and place it into
     * the format context's argument list.
     */
{
    switch (context->arglist[idx].type) {
        case SKY_DATA_TYPE_CHAR:
            context->arglist[idx].fa.fa_int = (intmax_t)(char)va_arg(context->ap, int);
            break;
        case SKY_DATA_TYPE_UCHAR:
            context->arglist[idx].fa.fa_uint = (uintmax_t)(unsigned char)va_arg(context->ap, unsigned int);
            break;
        case SKY_DATA_TYPE_SHORT:
            context->arglist[idx].fa.fa_int = (intmax_t)(short)va_arg(context->ap, int);
            break;
        case SKY_DATA_TYPE_USHORT:
            context->arglist[idx].fa.fa_uint = (uintmax_t)(unsigned short)va_arg(context->ap, unsigned int);
            break;
        case SKY_DATA_TYPE_INT:
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, int);
            break;
        case SKY_DATA_TYPE_UINT:
            context->arglist[idx].fa.fa_uint = (uintmax_t)va_arg(context->ap, unsigned int);
            break;
        case SKY_DATA_TYPE_LONG:
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, long);
            break;
        case SKY_DATA_TYPE_ULONG:
            context->arglist[idx].fa.fa_uint = (uintmax_t)va_arg(context->ap, unsigned long);
            break;
        case SKY_DATA_TYPE_LONG_LONG:
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, long long);
            break;
        case SKY_DATA_TYPE_ULONG_LONG:
            context->arglist[idx].fa.fa_uint = (uintmax_t)va_arg(context->ap, unsigned long long);
            break;
        case SKY_DATA_TYPE_INTMAX_T:
            context->arglist[idx].fa.fa_int = va_arg(context->ap, intmax_t);
            break;
        case SKY_DATA_TYPE_UINTMAX_T:
            context->arglist[idx].fa.fa_uint = va_arg(context->ap, uintmax_t);
            break;
        case SKY_DATA_TYPE_SSIZE_T:
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, ssize_t);
            break;
        case SKY_DATA_TYPE_SIZE_T:
            context->arglist[idx].fa.fa_uint = (uintmax_t)va_arg(context->ap, size_t);
            break;
        case SKY_DATA_TYPE_PTRDIFF_T:
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, ptrdiff_t);
            break;
        case SKY_DATA_TYPE_UPTRDIFF_T:
            context->arglist[idx].fa.fa_uint = (uintmax_t)va_arg(context->ap, uptrdiff_t);
            break;
        case SKY_DATA_TYPE_VOID_PTR:
        case SKY_DATA_TYPE_STRING:
        case SKY_DATA_TYPE_OBJECT:
            context->arglist[idx].fa.fa_pointer = va_arg(context->ap, void *);
            break;
        case SKY_DATA_TYPE_DOUBLE:
            context->arglist[idx].fa.fa_double = va_arg(context->ap, double);
            break;
        default:
            /* Grr, we have no idea what type this argument is. Do we
             * raise an exception? Do we assume an int? Yet another
             * example of how broken printf is ...
             */
            context->arglist[idx].fa.fa_int = (intmax_t)va_arg(context->ap, int);
            break;
    }
}


static void
sky_format_context_init(sky_format_context_t *context, const char *fmt, size_t fmtsz, va_list ap,
                        sky_format_output_t output, void *output_arg)
{
    context->fmtstring         = fmt;
    context->endof_fmtstring   = fmt + fmtsz;
    context->padding_buf       = NULL;
    context->padding_bufsz     = 0;
    context->formatting_buf    = NULL;
    context->formatting_buflen = 0;
    context->formatting_bufsz  = 0;
    context->nbytes_output     = 0;
    context->output            = output;
    context->output_arg        = output_arg;
    context->current_ap_index  = 0;
    context->arglist_count     = 0;
    context->fmtstring_scanned = SKY_FALSE;

    va_copy(context->ap, ap);
    memset(context->arglist, 0, sizeof(sky_format_arg_t) * FMTLIMIT_ARGCOUNT);
}


#define sky_format_output_bytes(bytes, nbytes)                      \
    do {                                                            \
        context->output((bytes), (nbytes), 1, context->output_arg); \
        context->nbytes_output += (nbytes);                         \
    } while (0)


static size_t
sky_format_output_int(sky_format_context_t *        context,
                      sky_format_specification_t *  spec,
                      intmax_t                      value)
{
    char        *result;
    size_t      actual_length, length;
    intmax_t    tmp;
    uint32_t    sign;

    for (length = (value ? 0 : 1), tmp = value;  tmp;  length++, tmp /= 10);
    if ((ssize_t)length < spec->precision) {
        length += (spec->precision - length);
    }
    sign = sky_format_sign(spec);
    if (value < 0 ||
        SKY_FORMAT_FLAG_SIGN_PLUS == sign ||
        SKY_FORMAT_FLAG_SIGN_SPACE == sign)
    {
        ++length;
    }

    if (length + 1 > context->formatting_bufsz) {
        context->formatting_buf =
                sky_asset_realloc(context->formatting_buf,
                                  length + 1,
                                  SKY_ASSET_CLEANUP_ALWAYS,
                                  SKY_ASSET_UPDATE_ALL_CURRENT);
        context->formatting_bufsz = sky_memsize(context->formatting_buf);
    }

    actual_length = length;
    result = context->formatting_buf;
    result[length] = '\0';

    if (value) {
        for (tmp = value;  tmp;  tmp /= 10) {
            result[--length] = sky_ctype_digits[abs((int)(tmp % 10))];
        }
    }
    while (length) {
        result[--length] = '0';
    }
    if (value < 0) {
        result[0] = '-';
    }
    else if (SKY_FORMAT_FLAG_SIGN_PLUS == sign) {
        result[0] = '+';
    }
    else if (SKY_FORMAT_FLAG_SIGN_SPACE == sign) {
        result[0] = ' ';
    }

    return actual_length;
}

static size_t
sky_format_output_uint(sky_format_context_t *       context,
                       sky_format_specification_t * spec,
                       uintmax_t                    value)
{
    int         base;
    char        *result;
    size_t      actual_length, length, precision;
    uintmax_t   tmp;
    const char  *set;

    if (spec->type == 'o') {
        base = 8;
    }
    else if (spec->type == 'x' || spec->type == 'X') {
        base = 16;
    }
    else {
        base = 10;
    }
    set = (spec->type == 'x' ? sky_ctype_lower_hexdigits
                             : sky_ctype_upper_hexdigits);

    precision = spec->precision;
    for (length = (value ? 0 : 1), tmp = value;  tmp;  length++, tmp /= base);
    if ((spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) &&
        base == 8 && (ssize_t)length >= spec->precision && value > 0)
    {
        precision = length + 1;
    }
    if (length < precision) {
        length += (precision - length);
    }
    if ((spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) && base != 10) {
        length += 2;
    }

    if (length + 1 > context->formatting_bufsz) {
        context->formatting_buf =
                sky_asset_realloc(context->formatting_buf,
                                  length + 1,
                                  SKY_ASSET_CLEANUP_ALWAYS,
                                  SKY_ASSET_UPDATE_ALL_CURRENT);
        context->formatting_bufsz = sky_memsize(context->formatting_buf);
    }

    actual_length = length;
    result = context->formatting_buf;
    result[length] = '\0';

    if (value) {
        for (tmp = value;  tmp;  tmp /= base) {
            result[--length] = set[(tmp % base)];
        }
    }
    while (length) {
        result[--length] = '0';
    }

    if (spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
        if (spec->type == 'x') {
            result[1] = 'x';
        }
        else if (spec->type == 'X') {
            result[1] = 'X';
        }
        else if (spec->type == 'o') {
            result[1] = 'o';
        }
    }

    return actual_length;
}


static size_t
sky_format_output_padding(sky_format_context_t *        context,
                          sky_format_specification_t *  spec,
                          size_t                        length)
    /* Generate padding with spaces, but only if the flags indicate that it is
     * required (width specified). For efficiency, keep a padding buffer,
     * growing it as required. It will be cleaned up when the format context
     * is cleaned up.
     */
{
    size_t  nbytes_output = 0;

    sky_error_validate_debug(length < SSIZE_MAX);
    if ((spec->flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH) &&
        (ssize_t)length < spec->width)
    {
        if (context->padding_bufsz < spec->width - length) {
            context->padding_buf =
                    sky_asset_realloc(context->padding_buf,
                                      spec->width - length,
                                      SKY_ASSET_CLEANUP_ALWAYS,
                                      SKY_ASSET_UPDATE_ALL_CURRENT);
            sky_error_validate_debug(spec->fill < 0x100);
            context->padding_bufsz = sky_memsize(context->padding_buf);
        }
        memset(context->padding_buf, spec->fill, context->padding_bufsz);
        sky_format_output_bytes(context->padding_buf, spec->width - length);
    }

    return nbytes_output;
}


static void
sky_format_output_encoding(const void * bytes,
                           ssize_t      nbytes,
                SKY_UNUSED size_t       width,
                           void *       arg)
{
    sky_format_context_t    *context = arg;

    size_t  new_formatting_bufsz;

    new_formatting_bufsz = context->formatting_buflen + nbytes;
    if (new_formatting_bufsz > context->formatting_bufsz) {
        context->formatting_buf =
                sky_asset_realloc(context->formatting_buf,
                                  new_formatting_bufsz,
                                  SKY_ASSET_CLEANUP_ALWAYS,
                                  SKY_ASSET_UPDATE_ALL_CURRENT);
        context->formatting_bufsz = sky_memsize(context->formatting_buf);
    }

    memcpy(context->formatting_buf + context->formatting_buflen, bytes, nbytes);
    context->formatting_buflen += nbytes;
}


static void
sky_format_output_string(sky_format_context_t *         context,
                         sky_format_specification_t *   spec,
                         sky_string_t                   string)
{
    static sky_string_t blackslashreplace;

    size_t  length, nbytes_output;

    if (!blackslashreplace) {
        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&blackslashreplace),
                SKY_STRING_LITERAL("backslashreplace"),
                SKY_TRUE);
    }

    /* Fast path: if there's no justification to be done (i.e., neither leading
     * nor trailing padding), simply output the string through the output
     * callback. Otherwise, we have to output to a bytes buffer, because we
     * need the length.
     */
    if (!(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH)) {
        sky_codec_encodewithcallback(NULL,
                                     string,
                                     NULL,
                                     blackslashreplace,
                                     0,
                                     sky_format_output_wrapper,
                                     context);
        return;
    }

    /* If there's no precision specified (meaning the string should not be
     * truncated if it's too long) and the string is to be left justified,
     * the length isn't needed until after it's written out, so we can simply
     * pass it through here too.
     */
    if (!(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) &&
        sky_format_alignment(spec) == SKY_FORMAT_FLAG_ALIGN_LEFT)
    {
        nbytes_output = context->nbytes_output;
        sky_codec_encodewithcallback(NULL,
                                     string,
                                     NULL,
                                     blackslashreplace,
                                     0,
                                     sky_format_output_wrapper,
                                     context);
        sky_format_output_padding(context,
                                  spec,
                                  context->nbytes_output - nbytes_output);
        return;
    }

    /* Encode the string into a temporary buffer so that we know its length.
     * Do truncation, padding, etc. normally using this buffer.
     */
    context->formatting_buflen = 0;
    nbytes_output = context->nbytes_output;
    sky_codec_encodewithcallback(NULL,
                                 string,
                                 NULL,
                                 blackslashreplace,
                                 0,
                                 sky_format_output_encoding,
                                 context);
    length = context->nbytes_output - nbytes_output;

    sky_error_validate_debug(spec->precision >= 0);
    if ((spec->flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) &&
        spec->precision < (ssize_t)length)
    {
        length = spec->precision;
    }
    if (sky_format_alignment(spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
        sky_format_output_padding(context, spec, length);
    }
    sky_format_output_bytes(context->formatting_buf, length);
    if (sky_format_alignment(spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
        sky_format_output_padding(context, spec, length);
    }
}


static void
sky_format_output_wrapper(const void *  bytes,
                          ssize_t       nbytes,
                          size_t        width,
                          void *        arg)
{
    sky_format_context_t    *context = arg;

    context->output(bytes, nbytes, width, context->output_arg);
    context->nbytes_output += nbytes;
}


static size_t
sky_format_parse(sky_format_context_t *context)
{
    int                         argnum, done;
    char                        sky_c, *sky_s;
    double                      sky_double;
    size_t                      length;
    intmax_t                    sky_d;
    uintmax_t                   sky_u;
    const char                  *c, *start;
    sky_object_t                sky_o;
    sky_string_t                sky_us;
    sky_format_modifier_t       modifier;
    sky_format_specification_t  spec;

    context->nbytes_output = 0;
    for (c = start = context->fmtstring;  likely(c < context->endof_fmtstring);  start = ++c) {
        while (*c != '%') {
            if (unlikely(++c == context->endof_fmtstring)) {
                goto finished;
            }
        }
        if (c > start) {
            sky_format_output_bytes(start, c - start);
        }

        modifier = SKY_FORMAT_MODIFIER_NONE;
        spec.fill = ' ';
        spec.type = 0;
        spec.width = spec.precision = 0;
        spec.flags = SKY_FORMAT_FLAG_ALIGN_RIGHT | SKY_FORMAT_FLAG_SIGN_MINUS;

        /* advance 'c' to the character immediately following the percent, but
         * first save the pointer to the percent for possible later use,
         * particularly for a, A, e, E, f, F, g, and G. If we've got %%, just
         * dump out a single % and move along.
         */
        start = c++;
        if (*c == '%') {
            sky_format_output_bytes(c, 1);
            continue;
        }

        /* check for nn$ argument specification */
        argnum = sky_format_parse_argnum(context, &c);

        /* parse out any flags that may be present */
        for (done = 0;  c < context->endof_fmtstring && !done;  ++c) {
            switch (*c) {
                case '-':
                    spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                    spec.flags |= SKY_FORMAT_FLAG_ALIGN_LEFT |
                                  SKY_FORMAT_FLAG_SPECIFIED_ALIGN;
                    break;
                case '+':
                    spec.flags &= ~SKY_FORMAT_FLAG_SIGN_MASK;
                    spec.flags |= SKY_FORMAT_FLAG_SIGN_PLUS |
                                  SKY_FORMAT_FLAG_SPECIFIED_SIGN;
                    break;
                case ' ':
                    spec.flags &= ~SKY_FORMAT_FLAG_SIGN_MASK;
                    spec.flags |= SKY_FORMAT_FLAG_SIGN_SPACE |
                                  SKY_FORMAT_FLAG_SPECIFIED_SIGN;
                    break;
                case '#':
                    spec.flags |= SKY_FORMAT_FLAG_ALTERNATE_FORM;
                    break;
                case '0':
                    spec.fill = '0';    /* SKY_FORMAT_FLAG_ZERO_PAD */
                    spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_FILL;
                    break;
                default:
                    done = 1;
                    --c;
                    break;
            }
        }
        if (unlikely(c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }

        /* parse out the field width if present */
        if (sky_format_parse_optionalint(context, &c, &(spec.width))) {
            spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_WIDTH;
            if (spec.width < 0) {
                spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_ALIGN_LEFT;
                spec.width = -spec.width;
            }
        }

        /* parse out the field precision if present */
        if (*c == '.') {
            if (unlikely(++c >= context->endof_fmtstring)) {
                sky_error_raise_string(sky_ValueError,
                                       "unexpected end of format string");
            }
            spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_PRECISION;
            sky_format_parse_optionalint(context, &c, &(spec.precision));
            if (unlikely(spec.precision < 0)) {
                spec.precision = 0;
            }
        }

        if ('0' == spec.fill) {
            if ((spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) ||
                sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_RIGHT)
            {
                spec.fill = ' ';
            }
        }

        /* look for a length modifier */
        switch (*c) {
            case 'h':
                modifier = (*++c != 'h' ? SKY_FORMAT_MODIFIER_SHORT : (++c, SKY_FORMAT_MODIFIER_CHAR));
                break;
            case 'l':
                modifier = (*++c != 'l' ? SKY_FORMAT_MODIFIER_LONG : (++c, SKY_FORMAT_MODIFIER_LONG_LONG));
                break;
            case 'j':
                modifier = (++c, SKY_FORMAT_MODIFIER_INTMAX_T);
                break;
            case 'z':
                modifier = (++c, SKY_FORMAT_MODIFIER_SIZE_T);
                break;
            case 't':
                modifier = (++c, SKY_FORMAT_MODIFIER_PTRDIFF_T);
                break;
        }

        /* handle the conversion specifier */
        sky_d = sky_u = 0;  /* Keep GCC quiet when building optimized. */
        if (unlikely(c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }
        spec.type = *c;
        switch (*c) {
            case 'd': case 'i':
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
                    spec.precision = 1;
                }
                switch (modifier) {
                    case SKY_FORMAT_MODIFIER_CHAR:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_CHAR);
                        break;
                    case SKY_FORMAT_MODIFIER_SHORT:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_SHORT);
                        break;
                    case SKY_FORMAT_MODIFIER_LONG:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_LONG);
                        break;
                    case SKY_FORMAT_MODIFIER_LONG_LONG:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_LONG_LONG);
                        break;
                    case SKY_FORMAT_MODIFIER_INTMAX_T:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_INTMAX_T);
                        break;
                    case SKY_FORMAT_MODIFIER_SIZE_T:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_SSIZE_T);
                        break;
                    case SKY_FORMAT_MODIFIER_PTRDIFF_T:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_PTRDIFF_T);
                        break;
                    default:
                        sky_d = sky_format_arg_getint(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_INT);
                        break;
                }
                length = sky_format_output_int(context, &spec, sky_d);
                if (sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                sky_format_output_bytes(context->formatting_buf, length);
                if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                break;

            case 'u':
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                /* FALL THROUGH TO o, x, X case */
            case 'o': case 'x': case 'X':
                if (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_SIGN) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
                    spec.precision = 1;
                }
                switch (modifier) {
                    case SKY_FORMAT_MODIFIER_CHAR:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_UCHAR);
                        break;
                    case SKY_FORMAT_MODIFIER_SHORT:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_USHORT);
                        break;
                    case SKY_FORMAT_MODIFIER_LONG:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_ULONG);
                        break;
                    case SKY_FORMAT_MODIFIER_LONG_LONG:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_ULONG_LONG);
                        break;
                    case SKY_FORMAT_MODIFIER_INTMAX_T:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_UINTMAX_T);
                        break;
                    case SKY_FORMAT_MODIFIER_SIZE_T:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_SIZE_T);
                        break;
                    case SKY_FORMAT_MODIFIER_PTRDIFF_T:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_UPTRDIFF_T);
                        break;
                    default:
                        sky_u = sky_format_arg_getuint(context,
                                                       argnum,
                                                       SKY_DATA_TYPE_UINT);
                        break;
                }
                length = sky_format_output_uint(context, &spec, sky_u);
                if (sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                sky_format_output_bytes(context->formatting_buf, length);
                if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                break;

            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
                    spec.precision = 6;
                }
                /* 64 = fudge, because everybody likes it */
                length = spec.precision + spec.width + 64;
                if (context->formatting_bufsz < length + 1) {
                    context->formatting_buf =
                            sky_asset_realloc(context->formatting_buf,
                                              length + 1,
                                              SKY_ASSET_CLEANUP_ALWAYS,
                                              SKY_ASSET_UPDATE_ALL_CURRENT);
                    context->formatting_bufsz =
                            sky_memsize(context->formatting_buf);
                }
                sky_s = sky_strndup(start, c - start + 1);
                sky_asset_save(sky_s, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
                sky_double = sky_format_arg_getdouble(context,
                                                      argnum,
                                                      SKY_DATA_TYPE_DOUBLE);
#if defined(_WIN32)
                length = _snprintf_s(context->formatting_buf,
                                     context->formatting_bufsz,
                                     _TRUNCATE,
                                     sky_s,
                                     sky_double);
#else
                length = snprintf(context->formatting_buf,
                                  context->formatting_bufsz,
                                  sky_s,
                                  sky_double);
#endif
                sky_format_output_bytes(context->formatting_buf, length);
                break;

            case 'c':
                /* Since wide characters are not supported, raise an error for
                 * any kind of length modifier even though C99 allows 'l' for 'c'
                 */
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE) ||
                    spec.flags & (SKY_FORMAT_FLAG_SPECIFIED_SIGN |
                                  SKY_FORMAT_FLAG_SPECIFIED_FILL |
                                  SKY_FORMAT_FLAG_SPECIFIED_PRECISION))
                {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                sky_c = (char)sky_format_arg_getint(context,
                                                    argnum,
                                                    SKY_DATA_TYPE_CHAR);

                if (sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, 1);
                }
                sky_format_output_bytes(&sky_c, 1);
                if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, 1);
                }
                break;

            case 's':
                /* Since wide characters are not supported, raise an error for
                 * any kind of length modifier even though C99 allows 'l' for 's'
                 */
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                if (!(sky_s = sky_format_arg_getpointer(context, argnum, SKY_DATA_TYPE_STRING))) {
                    sky_s = "(null)";
                }
                length = strlen(sky_s);
                if ((spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) &&
                    (size_t)spec.precision < length)
                {
                    length = spec.precision;
                }
                if (sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                sky_format_output_bytes(sky_s, length);
                if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                break;

            case 'p':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE) ||
                    (spec.flags & (SKY_FORMAT_FLAG_ALTERNATE_FORM |
                                   SKY_FORMAT_FLAG_SPECIFIED_SIGN)))
                {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                spec.type = 'x';
                spec.flags |= SKY_FORMAT_FLAG_ALTERNATE_FORM;
                sky_u = (uintmax_t)(uintptr_t)
                        sky_format_arg_getpointer(context,
                                                  argnum,
                                                  SKY_DATA_TYPE_VOID_PTR);

                length = sky_format_output_uint(context, &spec, sky_u);
                if (sky_format_alignment(&spec) != SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                sky_format_output_bytes(context->formatting_buf, length);
                if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_LEFT) {
                    sky_format_output_padding(context, &spec, length);
                }
                break;

            case '@':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                if (!(sky_o = sky_format_arg_getpointer(context,
                                                        argnum,
                                                        SKY_DATA_TYPE_OBJECT)))
                {
                    sky_o = sky_None;
                }
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    sky_us = sky_object_repr(sky_o);
                }
                else {
                    sky_us = sky_object_str(sky_o);
                }
                sky_format_output_string(context, &spec, sky_us);
                break;

            default:
                sky_error_raise_string(sky_ValueError, "bad format string");
        }
    }

finished:
    if (c > start) {
        sky_format_output_bytes(start, c - start);
    }
    return context->nbytes_output;
}


static int
sky_format_parse_argnum(sky_format_context_t *context, const char **start)
    /* look for an argument index in the form of "nn$", where "nn" must be a
     * base 10 one-based index into the variable argument list. We will allow
     * any value from 1 to FMTLIMIT_ARGCOUNT.
     */
{
    int         argnum;
    const char  *c;

    if (!sky_ctype_isdigit(**start)) {
        return 0;
    }
    for (argnum = 0, c = *start;
         c < context->endof_fmtstring && sky_ctype_isdigit(*c);
         c++)
    {
        argnum = (argnum * 10) + (*c - '0');
    }
    if (c >= context->endof_fmtstring || *c != '$') {
        return 0;
    }
    if (unlikely(argnum < 1) || unlikely(argnum > FMTLIMIT_ARGCOUNT)) {
        sky_error_raise_string(sky_ValueError, "argument index out of range");
    }
    *start = c + 1;

    return argnum;
}


static sky_bool_t
sky_format_parse_optionalint(sky_format_context_t * context,
                             const char **          start,
                             ssize_t *              value)
{
    int         argnum;
    intmax_t    int_value;
    const char  *c;

    if (*(c = *start) == '*') {
        if (unlikely(++c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }
        argnum = sky_format_parse_argnum(context, &c);
        int_value = sky_format_arg_getint(context, argnum, SKY_DATA_TYPE_INT);
        *value = (size_t)int_value;
        *start = c;
        return SKY_TRUE;
    }
    if (sky_ctype_isdigit(*c)) {
        *value = 0;
        while (sky_ctype_isdigit(*c)) {
            *value = (*value * 10) + (*c - '0');
            if (unlikely(++c >= context->endof_fmtstring)) {
                sky_error_raise_string(sky_ValueError,
                                       "unexpected end of format string");
            }
        }
        *start = c;
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static void
sky_format_scan(sky_format_context_t *context)
{
    int                     arglist_count, argnums[3], current_arg, done, i;
    va_list                 ap;
    const char              *c;
    sky_data_type_t         type;
    sky_format_modifier_t   modifier;

    current_arg = 0;
    arglist_count = context->arglist_count;
    for (c = context->fmtstring;  c < context->endof_fmtstring;  ++c) {
        while (*c != '%' && c < context->endof_fmtstring) {
            ++c;
        }
        if (c == context->endof_fmtstring) {
            break;
        }
        if (unlikely(++c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }
        if (*c == '%') {
            continue;
        }

        argnums[2] = sky_format_parse_argnum(context, &c);

        for (done = 0;  !done && c < context->endof_fmtstring;  ++c) {
            switch (*c) {
                case '-': case '+': case ' ': case '#': case '0':
                    break;
                default:
                    done = 1;
                    --c;
                    break;
            }
        }
        if (unlikely(c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }

        if (*c == '*') {
            if (unlikely(++c >= context->endof_fmtstring)) {
                sky_error_raise_string(sky_ValueError,
                                       "unexpected end of format string");
            }
            argnums[0] = sky_format_parse_argnum(context, &c);
        }
        else {
            argnums[0] = -1;
            if (sky_ctype_isdigit(*c)) {
                while (sky_ctype_isdigit(*c)) {
                    if (unlikely(++c >= context->endof_fmtstring)) {
                        sky_error_raise_string(sky_ValueError,
                                               "unexpected end of format string");
                    }
                }
            }
        }

        if (*c == '.') {
            if (unlikely(++c >= context->endof_fmtstring)) {
                sky_error_raise_string(sky_ValueError,
                                       "unexpected end of format string");
            }
            if (*c == '*') {
                if (unlikely(++c >= context->endof_fmtstring)) {
                    sky_error_raise_string(sky_ValueError,
                                           "unexpected end of format string");
                }
                argnums[1] = sky_format_parse_argnum(context, &c);
            }
            else {
                argnums[1] = -1;
                if (sky_ctype_isdigit(*c)) {
                    while (sky_ctype_isdigit(*c)) {
                        if (unlikely(++c >= context->endof_fmtstring)) {
                            sky_error_raise_string(sky_ValueError,
                                                   "unexpected end of format string");
                        }
                    }
                }
            }
        }

        modifier = SKY_FORMAT_MODIFIER_NONE;
        switch (*c) {
        case 'h':
            modifier = (*++c != 'h' ? SKY_FORMAT_MODIFIER_SHORT
                                    : (++c, SKY_FORMAT_MODIFIER_CHAR));
            break;
        case 'l':
            modifier = (*++c != 'l' ? SKY_FORMAT_MODIFIER_LONG
                                    : (++c, SKY_FORMAT_MODIFIER_LONG_LONG));
            break;
        case 'j':
            modifier = (++c, SKY_FORMAT_MODIFIER_INTMAX_T);
            break;
        case 'z':
            modifier = (++c, SKY_FORMAT_MODIFIER_SIZE_T);
            break;
        case 't':
            modifier = (++c, SKY_FORMAT_MODIFIER_PTRDIFF_T);
            break;
        }
        if (unlikely(c >= context->endof_fmtstring)) {
            sky_error_raise_string(sky_ValueError,
                                   "unexpected end of format string");
        }

        type = SKY_DATA_TYPE_VOID;
        switch (*c) {
            case 'd': case 'i':
                switch (modifier) {
                    case SKY_FORMAT_MODIFIER_CHAR:
                        type = SKY_DATA_TYPE_CHAR;
                        break;
                    case SKY_FORMAT_MODIFIER_SHORT:
                        type = SKY_DATA_TYPE_SHORT;
                        break;
                    case SKY_FORMAT_MODIFIER_LONG:
                        type = SKY_DATA_TYPE_LONG;
                        break;
                    case SKY_FORMAT_MODIFIER_LONG_LONG:
                        type = SKY_DATA_TYPE_LONG_LONG;
                        break;
                    case SKY_FORMAT_MODIFIER_INTMAX_T:
                        type = SKY_DATA_TYPE_INTMAX_T;
                        break;
                    case SKY_FORMAT_MODIFIER_SIZE_T:
                        type = SKY_DATA_TYPE_SSIZE_T;
                        break;
                    case SKY_FORMAT_MODIFIER_PTRDIFF_T:
                        type = SKY_DATA_TYPE_PTRDIFF_T;
                        break;
                    default:
                        type = SKY_DATA_TYPE_INT;
                        break;
                }
                break;

            case 'o': case 'u': case 'x': case 'X':
                switch (modifier) {
                    case SKY_FORMAT_MODIFIER_CHAR:
                        type = SKY_DATA_TYPE_UCHAR;
                        break;
                    case SKY_FORMAT_MODIFIER_SHORT:
                        type = SKY_DATA_TYPE_USHORT;
                        break;
                    case SKY_FORMAT_MODIFIER_LONG:
                        type = SKY_DATA_TYPE_ULONG;
                        break;
                    case SKY_FORMAT_MODIFIER_LONG_LONG:
                        type = SKY_DATA_TYPE_ULONG_LONG;
                        break;
                    case SKY_FORMAT_MODIFIER_INTMAX_T:
                        type = SKY_DATA_TYPE_UINTMAX_T;
                        break;
                    case SKY_FORMAT_MODIFIER_SIZE_T:
                        type = SKY_DATA_TYPE_SIZE_T;
                        break;
                    case SKY_FORMAT_MODIFIER_PTRDIFF_T:
                        type = SKY_DATA_TYPE_UPTRDIFF_T;
                        break;
                    default:
                        type = SKY_DATA_TYPE_UINT;
                        break;
                }
                break;

            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                type = SKY_DATA_TYPE_DOUBLE;
                break;

            case 'c':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                type = SKY_DATA_TYPE_CHAR;
                break;

            case 's':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                type = SKY_DATA_TYPE_STRING;
                break;

            case 'p':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                type = SKY_DATA_TYPE_VOID_PTR;
                break;

            case '@':
                if (unlikely(modifier != SKY_FORMAT_MODIFIER_NONE)) {
                    sky_error_raise_string(sky_ValueError, "bad format string");
                }
                type = SKY_DATA_TYPE_OBJECT;
                break;

            default:
                sky_error_raise_string(sky_ValueError, "bad format string");
        }

        for (i = 0;  i < 3;  i++) {
            if (argnums[i] != -1) {
                if (argnums[i]) {
                    --argnums[i];
                }
                else {
                    if (unlikely(current_arg == FMTLIMIT_ARGCOUNT)) {
                        sky_error_raise_string(sky_ValueError,
                                               "too many arguments");
                    }
                    argnums[i] = current_arg++;
                }
                if (unlikely(argnums[i] >= arglist_count) ||
                    unlikely(context->arglist[argnums[i]].type == SKY_DATA_TYPE_VOID) ||
                    unlikely(context->arglist[argnums[i]].type != (i < 2 ? SKY_DATA_TYPE_INT : type)))
                {
                    sky_error_raise_string(sky_ValueError,
                                           "illegal argument specifier");
                }
                context->arglist[argnums[i]].type = (i < 2 ? SKY_DATA_TYPE_INT
                                                           : type);
                if (argnums[i] > arglist_count) {
                    arglist_count = argnums[i] + 1;
                }
            }
        }
    }

    va_copy(ap, context->ap);
    for (i = context->current_ap_index;  i < arglist_count;  i++) {
        sky_format_arg_load(context, i);
    }
    va_copy(context->ap, ap);

    context->arglist_count = arglist_count;
    context->fmtstring_scanned = SKY_TRUE;
}


size_t
sky_format_output(sky_format_output_t output, void *arg, const char *fmt, ...)
{
    size_t  nbytes_output = 0;
    va_list ap;

    va_start(ap, fmt);
    nbytes_output = sky_format_voutput(output, arg, fmt, ap);
    va_end(ap);

    return nbytes_output;
}


size_t
sky_format_voutput(sky_format_output_t  output,
                   void *               arg,
                   const char *         fmt,
                   va_list              ap)
{
    size_t                  nbytes_output;
    sky_format_context_t    context;

    sky_error_validate_debug(output != NULL);
    sky_error_validate_debug(fmt != NULL);

    SKY_ASSET_BLOCK_BEGIN {
        sky_format_context_init(&context, fmt, strlen(fmt), ap, output, arg);
        nbytes_output = sky_format_parse(&context);
    } SKY_ASSET_BLOCK_END;

    return nbytes_output;
}


typedef struct sky_format_asprintf_s {
    char *                              buffer;
    size_t                              buffer_size;
    size_t                              buffer_used;
} sky_format_asprintf_t;


static void
sky_format_asprintf_output(const void * bytes,
                           ssize_t      nbytes,
                SKY_UNUSED size_t       width,
                           void *       arg)
{
    sky_format_asprintf_t   *output_buffer = arg;

    size_t                  new_size;

    if (output_buffer->buffer_used + nbytes + 1 > output_buffer->buffer_size) {
        new_size = sky_util_roundpow2(output_buffer->buffer_used + nbytes + 1);
        output_buffer->buffer = sky_asset_realloc(output_buffer->buffer,
                                                  new_size,
                                                  SKY_ASSET_CLEANUP_ON_ERROR,
                                                  SKY_ASSET_UPDATE_ALL_CURRENT);
        output_buffer->buffer_size = sky_memsize(output_buffer->buffer);
    }

    memcpy(output_buffer->buffer + output_buffer->buffer_used, bytes, nbytes);
    output_buffer->buffer_used += nbytes;
    output_buffer->buffer[output_buffer->buffer_used] = '\0';
}


size_t
sky_format_asprintf(char **buffer, const char *fmt, ...)
{
    size_t  nbytes_output = 0;
    va_list ap;

    va_start(ap, fmt);
    nbytes_output = sky_format_vasprintf(buffer, fmt, ap);
    va_end(ap);

    return nbytes_output;
}


size_t
sky_format_vasprintf(char **buffer, const char *fmt, va_list ap)
{
    sky_format_asprintf_t   output_buffer = { NULL, 0, 0 };

    size_t                  nbytes_output;
    sky_format_context_t    context;

    sky_error_validate_debug(buffer != NULL);
    sky_error_validate_debug(fmt != NULL);

    SKY_ASSET_BLOCK_BEGIN {
        sky_format_context_init(&context, fmt, strlen(fmt), ap,
                                sky_format_asprintf_output, &output_buffer);
        nbytes_output = sky_format_parse(&context);
    } SKY_ASSET_BLOCK_END;

    if (!output_buffer.buffer) {
        output_buffer.buffer = sky_strdup("");
    }

    *buffer = output_buffer.buffer;
    return nbytes_output;
}


#if !defined(NDEBUG)
static void
sky_format_dprintf_output(const void *  bytes,
                          ssize_t       nbytes,
               SKY_UNUSED size_t        width,
               SKY_UNUSED void *        arg)
{
#if defined(_WIN32)
    char    buffer[256];
    size_t  nbytes_tooutput, nbytes_output;

    for (nbytes_output = 0;
         nbytes_output < nbytes;
         nbytes_output += nbytes_tooutput)
    {
        nbytes_tooutput = SKY_MIN(nbytes - nbytes_output, sizeof(buffer) - 1);
        CopyMemory(buffer, (const char *)bytes + nbytes_output, nbytes_tooutput);
        buffer[nbytes_tooutput] = '\0';
        OutputDebugStringA(buffer);
    }
#else
    if (nbytes > 0 && unlikely(fwrite(bytes, nbytes, 1, stderr) != 1)) {
        sky_error_raise_errno(sky_OSError, errno);
    }
#endif
}
#endif


size_t
sky_format_dprintf(SKY_UNUSED const char *fmt, ...)
{
#if !defined(NDEBUG)
    size_t  nbytes_output = 0;
    va_list ap;

    va_start(ap, fmt);
    nbytes_output = sky_format_vdprintf(fmt, ap);
    va_end(ap);

    return nbytes_output;
#else
    return 0;
#endif
}


#if !defined(NDEBUG)
static pthread_mutex_t sky_format_vdprintf_mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

size_t
sky_format_vdprintf(SKY_UNUSED const char *fmt, SKY_UNUSED va_list ap)
{
#if !defined(NDEBUG)
    size_t                  nbytes_output;
    sky_format_context_t    context;

    SKY_ASSET_BLOCK_BEGIN {
        pthread_mutex_lock(&sky_format_vdprintf_mutex);
        sky_asset_save(&sky_format_vdprintf_mutex,
                       (sky_free_t)pthread_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_format_context_init(&context, fmt, strlen(fmt), ap,
                                sky_format_dprintf_output, NULL);
        nbytes_output = sky_format_parse(&context);
    } SKY_ASSET_BLOCK_END;

    return nbytes_output;
#else
    return 0;
#endif
}


static void
sky_format_fprintf_output(const void *  bytes,
                          ssize_t       nbytes,
               SKY_UNUSED size_t        width,
                          void *        arg)
{
    if (nbytes > 0 && unlikely(fwrite(bytes, nbytes, 1, (FILE *)arg) != 1)) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}


size_t
sky_format_fprintf(FILE *stream, const char *fmt, ...)
{
    size_t  nbytes_output = 0;
    va_list ap;

    va_start(ap, fmt);
    nbytes_output = sky_format_vfprintf(stream, fmt, ap);
    va_end(ap);

    return nbytes_output;
}


static pthread_mutex_t  sky_format_vfprintf_mutex =
#if defined(PTHREAD_RECURSIVE_MUTEX_INITIALIZER)
                        PTHREAD_RECURSIVE_MUTEX_INITIALIZER;
#elif defined(PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP)
                        PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
#else
#   error implementation missing
#endif

size_t
sky_format_vfprintf(FILE *stream, const char *fmt, va_list ap)
{
    size_t                  nbytes_output;
    sky_format_context_t    context;

    SKY_ASSET_BLOCK_BEGIN {
        pthread_mutex_lock(&sky_format_vfprintf_mutex);
        sky_asset_save(&sky_format_vfprintf_mutex,
                       (sky_free_t)pthread_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_format_context_init(&context, fmt, strlen(fmt), ap,
                                sky_format_fprintf_output, stream);
        nbytes_output = sky_format_parse(&context);
    } SKY_ASSET_BLOCK_END;

    return nbytes_output;
}


typedef struct sky_format_sprintf_t {
    char *                              buffer;
    size_t                              buffer_size;
    size_t                              buffer_used;
    size_t                              buffer_needed;
} sky_format_sprintf_t;


static void
sky_format_sprintf_output(const void *  bytes,
                          ssize_t       nbytes,
               SKY_UNUSED size_t        width,
                          void *        arg)
{
    sky_format_sprintf_t    *output_buffer = arg;

    output_buffer->buffer_needed += nbytes;
    if (output_buffer->buffer_used + nbytes + 1 > output_buffer->buffer_size) {
        nbytes = (output_buffer->buffer_size - 1 - output_buffer->buffer_used);
    }

    if (!output_buffer->buffer) {
        output_buffer->buffer_used += nbytes;
    }
    else if (nbytes > 0) {
        memcpy(output_buffer->buffer + output_buffer->buffer_used, bytes, nbytes);
        output_buffer->buffer_used += nbytes;
        output_buffer->buffer[output_buffer->buffer_used] = '\0';
    }
}


size_t
sky_format_sprintf(char * restrict          buffer,
                   size_t                   buffer_length,
                   const char * restrict    fmt,
                   ...)
{
    size_t  nbytes_output = 0;
    va_list ap;

    va_start(ap, fmt);
    nbytes_output = sky_format_vsprintf(buffer, buffer_length, fmt, ap);
    va_end(ap);

    return nbytes_output;
}


size_t
sky_format_vsprintf(char * restrict         buffer,
                    size_t                  buffer_length,
                    const char * restrict   fmt,
                    va_list                 ap)
{
    sky_format_sprintf_t    output_buffer = { NULL, 0, 0, 0 };

    sky_format_context_t    context;

    /* buffer may be NULL to find out how much space is required */
    sky_error_validate_debug(fmt != NULL);

    output_buffer.buffer = buffer;
    output_buffer.buffer_size = buffer_length;
    output_buffer.buffer_needed = 1;

    SKY_ASSET_BLOCK_BEGIN {
        sky_format_context_init(&context, fmt, strlen(fmt), ap,
                                sky_format_sprintf_output, &output_buffer);
        sky_format_parse(&context);
    } SKY_ASSET_BLOCK_END;

    if (unlikely(output_buffer.buffer_needed == 1) && likely(buffer != NULL)) {
        *buffer = '\0';
    }

    /* Do not include the '\0' in the return count. */
    return output_buffer.buffer_needed - 1;
}


size_t
sky_format_number(sky_format_output_t   output,
                  void *                arg,
                  const char *          bytes,
                  size_t                nbytes,
                  ssize_t               minimum_width,
                  const char *          decimal_point,
                  const char *          thousands_sep,
                  const char *          grouping)
{
    static const char zeros[] = "0000000000000000";

#define output_zeros(_n)                                                    \
    do {                                                                    \
        ssize_t nz = (_n);                                                  \
                                                                            \
        if (nz > 0) {                                                       \
            if (nz <= (ssize_t)sizeof(zeros) - 1) {                         \
                output(zeros, nz, 1, arg);                                  \
                nbytes_out += nz;                                           \
            }                                                               \
            else {                                                          \
                while (nz > 0) {                                            \
                    n = SKY_MIN(nz, (ssize_t)sizeof(zeros) - 1);            \
                    output(zeros, n, 1, arg);                               \
                    nbytes_out += n;                                        \
                    nz -= n;                                                \
                }                                                           \
            }                                                               \
        }                                                                   \
    } while (0)

    size_t      decimal_point_len, n, nbytes_out, thousands_sep_len;
    ssize_t     nleft, nzeros;
    sky_bool_t  group;
    const char  *dot, *end, *g, *zg;

    nbytes_out = 0;
    group = ((thousands_sep && *thousands_sep &&
              grouping && *grouping && *grouping != CHAR_MAX) ? SKY_TRUE
                                                              : SKY_FALSE);
    end = bytes + nbytes;
    if (decimal_point) {
        decimal_point_len = strlen(decimal_point);
        for (dot = bytes; dot < end && *dot != '.'; ++dot);
        if (!group) {
            nleft = dot - bytes;
            output_zeros(minimum_width - nleft);
            if (1 == decimal_point_len && *decimal_point == '.') {
                output(bytes, nbytes, 1, arg);
                return nbytes_out + nbytes;
            }
            output(bytes, nleft, 1, arg);
            output(decimal_point, decimal_point_len, 1, arg);
            output(dot + 1, end - (dot + 1), 1, arg);

            return nbytes_out + nleft + decimal_point_len + (end - (dot + 1));
        }
    }
    else if (!group) {
        output_zeros(minimum_width - nbytes);
        output(bytes, nbytes, 1, arg);
        return nbytes_out + nbytes;
    }
    else {
        dot = end;
    }

    /* At this point, grouping is definitely going to be done. The grouping
     * information in 'grouping' is stored right to left, but we need to call
     * 'output' from left to right. Consider everything before the decimal
     * point ('dot' - which will always be <= 'end') for grouping.
     */
    thousands_sep_len = (thousands_sep[1] ? strlen(thousands_sep) : 1);
    nleft = dot - bytes;
    for (g = grouping; *g && CHAR_MAX != *g && nleft > *g; ++g) {
        nleft -= *g;
        minimum_width -= (*g + thousands_sep_len);
    }

    if (minimum_width > 0) {
        zg = g;
        if (CHAR_MAX == *zg) {
            nzeros = 0;
        }
        else if (!*zg) {
            nzeros = SKY_MIN(zg[-1], minimum_width) - nleft;
            minimum_width -= zg[-1];
        }
        else {
            nzeros = SKY_MIN(*zg, minimum_width) - nleft;
            minimum_width -= *zg;
            for (++zg; *zg && CHAR_MAX != *zg && minimum_width > *zg; ++zg) {
                minimum_width -= (*zg + thousands_sep_len);
            }
        }
        if (CHAR_MAX == *zg) {
            output_zeros(minimum_width);
        }
        else if (!*zg) {
            if (minimum_width > 0) {
                minimum_width -= thousands_sep_len;
                if (!minimum_width) {
                    output_zeros(1);
                }
                else if (!(minimum_width % zg[-1])) {
                    output_zeros(zg[-1]);
                }
                else {
                    output_zeros(minimum_width % zg[-1]);
                }
                output(thousands_sep, thousands_sep_len, 1, arg);
                nbytes_out += thousands_sep_len;
            }
            while (minimum_width > zg[-1]) {
                output_zeros(zg[-1]);
                output(thousands_sep, thousands_sep_len, 1, arg);
                nbytes_out += thousands_sep_len;
                minimum_width -= (zg[-1] + thousands_sep_len);
            }
        }
        else if (minimum_width > 0) {
            n = SKY_MIN(*zg, minimum_width);
            output_zeros(n);
            output(thousands_sep, thousands_sep_len, 1, arg);
            nbytes_out += thousands_sep_len;
        }
        while (--zg > g) {
            output(zeros, *g, 1, arg);
            output(thousands_sep, thousands_sep_len, 1, arg);
            nbytes_out += (*g + thousands_sep_len);
        }
        output_zeros(nzeros);
    }

    if (CHAR_MAX == *g) {
        output(bytes, nleft, 1, arg);
        nbytes_out += nleft;
    }
    else if (!*g) {
        if (!(nleft % g[-1])) {
            output(bytes, g[-1], 1, arg);
            nbytes_out += g[-1];
            bytes += g[-1];
            nleft -= g[-1];
        }
        else {
            output(bytes, (nleft % g[-1]), 1, arg);
            nbytes_out += (nleft % g[-1]);
            bytes += (nleft % g[-1]);
            nleft -= (nleft % g[-1]);
        }
        while (nleft >= g[-1]) {
            output(thousands_sep, thousands_sep_len, 1, arg);
            output(bytes, g[-1], 1, arg);
            nbytes_out += (thousands_sep_len + g[-1]);
            bytes += g[-1];
            nleft -= g[-1];
        }
        sky_error_validate_debug(0 == nleft);
    }
    else {
        output(bytes, nleft, 1, arg);
        nbytes_out += nleft;
        bytes += nleft;
    }
    while (--g >= grouping) {
        output(thousands_sep, thousands_sep_len, 1, arg);
        output(bytes, *g, 1, arg);
        nbytes_out += thousands_sep_len + *g;
        bytes += *g;
    }
    sky_error_validate_debug(bytes <= end);

    /* Dump out whatever is left. */
    if (dot < end) {
        sky_error_validate_debug(decimal_point != NULL);
        if (1 == decimal_point_len && *decimal_point == '.') {
            output(dot, end - dot, 1, arg);
            nbytes_out += (end - dot);
        }
        else {
            output(decimal_point, decimal_point_len, 1, arg);
            output(dot + 1, end - (dot + 1), 1, arg);
            nbytes_out += (decimal_point_len + (end - (dot + 1)));
        }
    }

    return nbytes_out;
}


void
sky_format_atfork_child(void)
{
    pthread_mutexattr_t attr;

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

#if !defined(NDEBUG)
    memset(&sky_format_vdprintf_mutex, 0, sizeof(sky_format_vdprintf_mutex));
    pthread_mutex_init(&sky_format_vdprintf_mutex, NULL);
#endif
    memset(&sky_format_vfprintf_mutex, 0, sizeof(sky_format_vfprintf_mutex));
    pthread_mutex_init(&sky_format_vfprintf_mutex, &attr);

    pthread_mutexattr_destroy(&attr);
}
