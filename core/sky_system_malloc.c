#include "sky_private.h"

#if defined(HAVE_MALLOC_H)
#   include <malloc.h>
#endif
#if defined(HAVE_MALLOC_MALLOC_H)
#   include <malloc/malloc.h>
#endif
#if defined(HAVE_MALLOC_NP_H)
#   include <malloc_np.h>
#endif


#if defined(_WIN32)
static HANDLE sky_system_malloc_processheap;
#endif


static void *sky_system_malloc_calloc   (size_t count, size_t nbytes);
static void *sky_system_malloc_malloc   (size_t nbytes);
static void *sky_system_malloc_memalign (size_t alignment, size_t nbytes);
static void *sky_system_malloc_realloc  (void *old_pointer, size_t new_nbytes);


static void *
sky_system_malloc_calloc(size_t count, size_t nbytes)
{
#if defined(__APPLE__) && (defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MEMALIGN))
    /* Apple's libc calloc() implementation guarantees 16-byte alignment. */
    return calloc(count, nbytes);
#elif defined(_WIN64)
    void    *pointer, *raw_pointer;

    /* HeapAlloc() guarantees 16-byte alignment on 64-bit platforms, but only
     * 8-byte alignment on 32-bit platforms.
     * See http://support.microsoft.com/kb/286470
     */
    raw_pointer = HeapAlloc(sky_system_malloc_processheap,
                            HEAP_ZERO_MEMORY,
                            (count * nbytes) + 16);
    if (unlikely(!raw_pointer)) {
        return NULL;
    }
    pointer = (void *)((char *)raw_pointer + 16);
    *(void **)((char *)pointer - sizeof(void *)) = raw_pointer;
    *(size_t *)((char *)pointer - sizeof(void *) - sizeof(size_t)) =
            count * nbytes;

    return memset(pointer, 0x00, count * nbytes);
#else
    void    *pointer;

    if (unlikely(!(pointer = sky_system_malloc_malloc(count * nbytes)))) {
        return NULL;
    }

    return memset(pointer, 0x00, count * nbytes);
#endif
}


static void
sky_system_malloc_free(void *pointer)
{
#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MEMALIGN)
    free(pointer);
#elif defined(_WIN32)
    HeapFree(sky_system_malloc_processheap, 0,
             *(void **)((char *)pointer - sizeof(void *)));
#else
    free(*(void **)((char *)pointer - sizeof(void *)));
#endif
}


static void *
sky_system_malloc_malloc(size_t nbytes)
{
#if defined(__APPLE__) && (defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MEMALIGN))
    /* Apple's libc malloc() implementation guarantees 16-byte alignment. */
    return malloc(nbytes);
#elif defined(_WIN64)
    /* HeapAlloc() guarantees 16-byte alignment on 64-bit platforms, but only
     * 8-byte alignment on 32-bit platforms.
     * See http://support.microsoft.com/kb/286470
     */
    void    *pointer, *raw_pointer;

    raw_pointer = HeapAlloc(sky_system_malloc_processheap, 0, nbytes + 16);
    if (unlikely(!raw_pointer)) {
        return NULL;
    }
    pointer = (void *)((char *)raw_pointer + 16);
    *(void **)((char *)pointer - sizeof(void *)) = raw_pointer;
    *(size_t *)((char *)pointer - sizeof(void *) - sizeof(size_t)) = nbytes;

    return pointer;
#elif defined(HAVE_POSIX_MEMALIGN)
    void    *pointer;

    if (unlikely(posix_memalign(&pointer, 16, nbytes) != 0)) {
        return NULL;
    }

    return pointer;
#elif defined(HAVE_MEMALIGN)
    return memalign(16, nbytes);
#else
    static const size_t extra_nbytes = sizeof(void *) + sizeof(size_t) + 15;

    void                *pointer, *raw_pointer;

    if (unlikely(SIZE_MAX - nbytes < extra_nbytes)) {
        return NULL;
    }
#if defined(_WIN32)
    raw_pointer = HeapAlloc(sky_system_malloc_processheap, 0, nbytes + extra_nbytes);
#else
    raw_pointer = malloc(nbytes + extra_nbytes);
#endif
    if (unlikely(!raw_pointer)) {
        return NULL;
    }
    pointer = (void *)(((uintptr_t)raw_pointer + extra_nbytes) & ~15);
    *(void **)((char *)pointer - sizeof(void *)) = raw_pointer;
    *(size_t *)((char *)pointer - sizeof(void *) - sizeof(size_t)) = nbytes;

    return pointer;
#endif
}


static void *
sky_system_malloc_memalign(size_t alignment, size_t nbytes)
{
#if defined(HAVE_POSIX_MEMALIGN)
    void    *pointer;

    if (unlikely(posix_memalign(&pointer, alignment, nbytes) != 0)) {
        return NULL;
    }

    return pointer;
#elif defined(HAVE_MEMALIGN)
    return memalign(alignment, nbytes);
#else
    void                *pointer, *raw_pointer;
    size_t              extra_nbytes;

    extra_nbytes = sizeof(void *) + sizeof(size_t) + (alignment - 1);
    if (unlikely(SIZE_MAX - nbytes < extra_nbytes)) {
        return NULL;
    }
#if defined(_WIN32)
    raw_pointer = HeapAlloc(sky_system_malloc_processheap, 0, nbytes + extra_nbytes);
#else
    raw_pointer = malloc(nbytes + extra_nbytes);
#endif
    if (unlikely(!raw_pointer)) {
        return NULL;
    }
    pointer = (void *)(((uintptr_t)raw_pointer + extra_nbytes) & ~(alignment - 1));
    *(void **)((char *)pointer - sizeof(void *)) = raw_pointer;
    *(size_t *)((char *)pointer - sizeof(void *) - sizeof(size_t)) = nbytes;

    return pointer;
#endif
}


static size_t
sky_system_malloc_memsize(const void *pointer)
{
#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MEMALIGN)
#   if defined(HAVE_MALLOC_SIZE)
    return malloc_size(pointer);
#   elif defined(HAVE_MALLOC_USABLE_SIZE)
    return malloc_usable_size((void *)pointer); /* strip the const, ugh */
#   else
#       error implementation missing
#   endif
#else
    void    *raw_pointer = *(void **)((char *)pointer - sizeof(void *));
    size_t  raw_size;

#   if defined(_WIN32)
    raw_size = HeapSize(sky_system_malloc_processheap, 0, raw_pointer);
#   elif defined(HAVE_MALLOC_SIZE)
    raw_size = malloc_size(raw_pointer);
#   elif defined(HAVE_MALLOC_USABLE_SIZE)
    raw_size = malloc_usable_size(raw_pointer);
#   else
#       error implementation missing
#   endif
    return raw_size - ((char *)pointer - (char *)raw_pointer);
#endif
}


static void *
sky_system_malloc_realloc(void *old_pointer, size_t new_nbytes)
{
#if defined(__APPLE__) && (defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MEMALIGN))
    /* Apple's libc realloc() implementation guarantees 16-byte alignment. */
    return realloc(old_pointer, new_nbytes);
#else
    void    *new_pointer;
    size_t  old_nbytes;

    old_nbytes = sky_system_malloc_memsize(old_pointer);
    if (old_nbytes >= new_nbytes) {
        return old_pointer;
    }

    if (unlikely(!(new_pointer = sky_system_malloc_malloc(new_nbytes)))) {
        return NULL;
    }

    memcpy(new_pointer, old_pointer, SKY_MIN(old_nbytes, new_nbytes));
    sky_system_malloc_free(old_pointer);
    return new_pointer;
#endif
}


void
sky_system_malloc_finalize(void)
{
}


sky_bool_t
sky_system_malloc_initialize(void)
{
#if defined(_WIN32)
    if (!sky_system_malloc_processheap) {
        sky_system_malloc_processheap = GetProcessHeap();
    }
#endif

    sky_malloc_sethooks(sky_system_malloc_malloc,
                        sky_system_malloc_free,
                        sky_system_malloc_realloc,
                        sky_system_malloc_calloc,
                        NULL,                           /* sky_memdup       */
                        NULL,                           /* sky_strdup       */
                        NULL,                           /* sky_strndup      */
                        sky_system_malloc_memalign,
                        sky_system_malloc_memsize,
                        NULL);                          /* sky_malloc_idle  */

    return SKY_TRUE;
}
