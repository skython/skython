/* This file is normally automatically generated by arithchk.c as part of the
 * standard build process for the gdtoa library; however, because Skython's
 * platform support is very limited it's easier to just define everything as
 * it's known to be for the supported platforms.
 */

#include "../sky_malloc.h"

#if defined(__x86_64__) || defined(_M_X64)
#   define IEEE_8087
#   define Arith_Kind_ASL 1
#   define Long int
#   define Intcast (int)(long)
#   define Double_Align
#   define X64_bit_pointers
#elif defined(__i386__) || defined(_M_IX86)
#   define IEEE_8087
#   define Arith_Kind_ASL 1
#   define Double_Align
#else
#   error implementation missing
#endif

#define NO_HEX_FP
#define No_Hex_NaN

#define MALLOC sky_malloc
#define FREE sky_free
#define Omit_Private_Memory

#define MULTIPLE_THREADS
#define ACQUIRE_DTOA_LOCK(n)
#define FREE_DTOA_LOCK(n)
