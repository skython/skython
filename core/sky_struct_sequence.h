/* @file
 * @brief
 * @defgroup sky_object Objects
 * @{
 * @defgroup sky_struct_sequence Structure Sequences
 * @{
 */

#ifndef __SKYTHON_CORE_SKY_STRUCT_SEQUENCE_H__
#define __SKYTHON_CORE_SKY_STRUCT_SEQUENCE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Flags controlling the behavior of a field. **/
typedef enum sky_struct_sequence_field_flag_e {
    SKY_STRUCT_SEQUENCE_FIELD_FLAG_NONE             =   0x0,
    SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY   =   0x1,
    SKY_STRUCT_SEQUENCE_FIELD_FLAG_SEQUENCE_ONLY    =   0x2,
} sky_struct_sequence_field_flag_t;


/** A structure that defines a field in a structure sequence. **/
typedef struct sky_struct_sequence_field_s {
    /** The name of the field, which may be @c NULL for an unnamed field. **/
    const char *                        name;
    /** The doc string for the field. **/
    const char *                        doc;
    /** Flags for the field (see @c sky_struct_sequence_field_flag_t). **/
    unsigned int                        flags;
} sky_struct_sequence_field_t;


/** Define a new structure sequence type.
  * A new type is created via that is a structure sequence with the specified
  * fields defined.
  *
  * @param[in]  name        the name of the structure sequence type.
  * @param[in]  doc         the doc string for the structure sequence type.
  * @param[in]  nfields     the number of fields in the structure sequence.
  * @param[in]  fields      the list of fields in the structure sequence.
  * @return     a new structure sequence type.
  */
SKY_EXTERN sky_type_t
sky_struct_sequence_define(const char *                 name,
                           const char *                 doc,
                           ssize_t                      nfields,
                           sky_struct_sequence_field_t *fields);


/** Instantiate a new structure sequence.
  *
  * @param[in]  type        the structure sequence type as returned by a
  *                         previous call to sky_struct_sequence_define().
  * @param[in]  nvalues     the number of values present in @a values.
  * @param[in]  values      the values to use for each of the defined fields.
  * @return     a new object instance of type @a type.
  */
SKY_EXTERN sky_object_t
sky_struct_sequence_create(sky_type_t       type,
                           ssize_t          nvalues,
                           sky_object_t *   values);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_STRUCT_SEQUENCE_H__ */

/** @} **/
/** @} **/
