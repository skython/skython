#include "sky_private.h"


/* This hash table algorithm is based on that presented by Shalev and Shavit,
 * 2006. Split-Ordered Lists: Lock-Free Extensible Hash Tables.
 * ACM, New York, Volume 53 Issue 3, May 2006.
 *
 * This hash table algorithm also uses list-based sets as described by Michael,
 * M. M. 2002a. High performance dynamic lock-free hash tables and list-based
 * sets. In Pro-ceedings of the 14th Annual ACM Symposium on Parallel
 * Algorithms and Architectures. ACM, New York, 73–82.
 *
 * This is a completely lock-free thread-safe hash table implementation.
 */


typedef struct sky_lockfree_hashtable_tlsdata_s {
    intmax_t                            snapshot_length;
    intmax_t                            snapshot_max_length;
    intmax_t                            snapshot_delta;

    intmax_t                            delta;

    sky_bool_t                          initialized;

    uint8_t padding[SKY_CACHE_LINE_PAD((sizeof(intmax_t) * 4) +
                                        sizeof(sky_bool_t))];
} sky_lockfree_hashtable_tlsdata_t;
#if !defined(_WIN32)
char sky_lockfree_hashtable_tlsdata_size_check[sizeof(sky_lockfree_hashtable_tlsdata_t) == SKY_CACHE_LINE_SIZE ? 0 : -1];
#endif


typedef struct sky_lockfree_hashtable_tlsdata_block_s
        sky_lockfree_hashtable_tlsdata_block_t;
struct sky_lockfree_hashtable_tlsdata_block_s {
    sky_lockfree_hashtable_tlsdata_block_t * volatile   next;

    volatile int32_t                    size;
    volatile int32_t                    inuse;

    SKY_CACHE_LINE_ALIGNED
    sky_lockfree_hashtable_tlsdata_t    slots[0];
};
#if !defined(_WIN32)
char sky_lockfree_hashtable_tlsdata_block_size_check[sizeof(sky_lockfree_hashtable_tlsdata_block_t) == SKY_CACHE_LINE_SIZE ? 0 : -1];
#endif


static sky_bool_t
sky_lockfree_hashtable_iterator_find(
        sky_lockfree_hashtable_iterator_t *         iterator,
        uintptr_t                                   key_hash,
        void *                                      key);

static sky_bool_t
sky_lockfree_hashtable_iterator_finditem(
        sky_lockfree_hashtable_iterator_t *         iterator,
        sky_lockfree_hashtable_item_t *             item);

static sky_lockfree_hashtable_item_t *
sky_lockfree_hashtable_iterator_first(
        sky_lockfree_hashtable_iterator_t *iterator);

static void
sky_lockfree_hashtable_iterator_retry(
        sky_lockfree_hashtable_iterator_t * iterator);

static sky_bool_t
sky_lockfree_hashtable_iterator_visit(
        sky_lockfree_hashtable_iterator_t * iterator);


static inline void
sky_lockfree_hashtable_increment_sizeof(sky_lockfree_hashtable_t *  table,
                                        size_t                      amount)
{
#if defined(SKY_ARCH_32BIT)
    sky_atomic_add32_old(amount,
                         SKY_AS_TYPE(&(table->internal_sizeof), int32_t *));
#elif defined(SKY_ARCH_64BIT)
    sky_atomic_add64_old(amount,
                         SKY_AS_TYPE(&(table->internal_sizeof), int64_t *));
#else
#   error implementation missing
#endif
}


static sky_lockfree_hashtable_tlsdata_block_t *
sky_lockfree_hashtable_tlsdata_block_alloc(sky_lockfree_hashtable_t *table)
{
    size_t                                  nslots;
    sky_lockfree_hashtable_tlsdata_block_t  *block;

    nslots = sky_util_roundpow2(SKY_MAX(sky_tlsdata_highestid(),
                                        sky_system_cpucount() + 1));
    block = sky_calloc(1, sizeof(sky_lockfree_hashtable_tlsdata_block_t) +
                          sizeof(sky_lockfree_hashtable_tlsdata_t) * nslots);
    block->size = (sky_memsize(block) -
                   sizeof(sky_lockfree_hashtable_tlsdata_block_t)) /
                  sizeof(sky_lockfree_hashtable_tlsdata_t);
    block->next = table->tlsdata;
    if (!sky_atomic_cas(block->next, block, SKY_AS_VOIDP(&(table->tlsdata)))) {
        sky_free(block);
    }
    else {
        sky_lockfree_hashtable_increment_sizeof(table, sky_memsize(block));
    }
    return table->tlsdata;
}


/* Ideally there should be some way of cleaning up old, unused tlsdata blocks.
 * However, it's complicated to do so, and any solution I can come up with at
 * this point in time would negatively impact the normal code flow. Given
 * Skython's architecture, the block size really shouldn't ever need to change,
 * so the cost of handling clean up greatly outweighs the benefit. We're simply
 * going to leave old, unused tlsdata blocks hanging around until the hashtable
 * itself is cleaned up.
 */
static sky_lockfree_hashtable_tlsdata_t *
sky_lockfree_hashtable_tlsdata_get(sky_lockfree_hashtable_t *table)
{
    int32_t                                 thread_id;
    sky_lockfree_hashtable_tlsdata_t        *old_tlsdata, *tlsdata;
    sky_lockfree_hashtable_tlsdata_block_t  * volatile block;

    if (!(block = table->tlsdata)) {
        block = sky_lockfree_hashtable_tlsdata_block_alloc(table);
    }

    thread_id = sky_thread_id();
    while (thread_id > block->size) {
        block = sky_lockfree_hashtable_tlsdata_block_alloc(table);
    }

    tlsdata = &(block->slots[thread_id - 1]);
    if (!tlsdata->initialized) {
        sky_atomic_increment32_old(&(block->inuse));
        tlsdata->initialized = SKY_TRUE;

        while ((block = block->next) != NULL) {
            if (thread_id > block->size) {
                continue;
            }
            old_tlsdata = &(block->slots[thread_id - 1]);
            if (!old_tlsdata->initialized) {
                continue;
            }

            *tlsdata = *old_tlsdata;
            sky_atomic_decrement32(&(block->inuse));
            break;
        }
    }

    /* Update this thread's snapshot if it doesn't what's been snapshotted
     * previously. We keep local copies of everything so that we know when
     * the global copies change. We'll also use the local copies for helping
     * to trigger a global update. Note that global updates are expensive, so
     * we want to avoid them when possible.
     */
    if (tlsdata->snapshot_length != table->global_length ||
        tlsdata->snapshot_max_length != table->global_max_length)
    {
        tlsdata->snapshot_length = table->global_length;
        tlsdata->snapshot_max_length = table->global_max_length;
        tlsdata->snapshot_delta = 0;
    }

    return tlsdata;
}


SKY_EXTERN_INLINE uintptr_t
sky_lockfree_hashtable_reverse(uintptr_t value)
{
#if defined(SKY_ARCH_32BIT)
    return sky_util_reverse32(value);
#elif defined(SKY_ARCH_64BIT)
    return sky_util_reverse64(value);
#else
#   error implementation missing
#endif
}


static void
sky_lockfree_hashtable_update_length(sky_lockfree_hashtable_t *table)
{
    int32_t                                 i;
    intmax_t                                length, max_length;
    sky_lockfree_hashtable_tlsdata_t        *tlsdata;
    sky_lockfree_hashtable_tlsdata_block_t  *block, *top_block;

    do {
        length = 0;
        if ((top_block = table->tlsdata) != NULL) {
            for (i = 0; i < top_block->size; ++i) {
                tlsdata = &(top_block->slots[i]);
                if (tlsdata->initialized) {
                    length += tlsdata->delta;
                }
                else {
                    for (block = top_block->next; block; block = block->next) {
                        if (i < block->size) {
                            tlsdata = &(top_block->slots[i]);
                            if (tlsdata->initialized) {
                                length += tlsdata->delta;
                                break;
                            }
                        }
                    }
                }
            }
        }
    } while (top_block != table->tlsdata);

    table->global_length = length;
    max_length = table->global_max_length;
    if (max_length < 1 << (SKY_HASHTABLE_SEGMENT_COUNT - 1) &&
        length >= max_length * 2 / 3)
    {
        /* These swaps may fail, and that's ok. It simply means that another
         * thread has increased max_length before us. It'll never get smaller.
         */
        if (sizeof(int32_t) == sizeof(intmax_t)) {
            sky_atomic_cas32((int32_t)max_length,
                             (int32_t)max_length * 2,
                             SKY_AS_TYPE(&(table->global_max_length), int32_t *));
        }
        else if (sizeof(int64_t) == sizeof(intmax_t)) {
            sky_atomic_cas64(max_length,
                             max_length * 2,
                             SKY_AS_TYPE(&(table->global_max_length), int64_t *));
        }
        else {
            sky_error_fatal("implementation missing");
        }
    }
}


void
sky_lockfree_hashtable_init(
        sky_lockfree_hashtable_t *              table,
        sky_lockfree_hashtable_item_compare_t   item_compare,
        sky_lockfree_hashtable_item_delete_t    item_delete,
        sky_free_t                              item_free)
{
    table->tlsdata = NULL;
    table->global_length = 0;
    table->global_max_length = 2;
    table->item_compare = item_compare;
    table->item_delete = item_delete;
    table->item_free = item_free;
    memset((void *)table->segments, 0, sizeof(table->segments));
    table->segments[0] =
            sky_calloc(1,
                       sizeof(sky_lockfree_hashtable_segment_t) +
                       (sizeof(sky_lockfree_hashtable_item_t *) * 2));
    table->segments[0]->items[0] =
            sky_calloc(1, sizeof(sky_lockfree_hashtable_item_t));
    table->internal_sizeof = sky_memsize(table->segments[0]) +
                             sky_memsize(table->segments[0]->items[0]);
}


sky_lockfree_hashtable_t *
sky_lockfree_hashtable_create(
        sky_lockfree_hashtable_item_compare_t   item_compare,
        sky_lockfree_hashtable_item_delete_t    item_delete,
        sky_free_t                              item_free)
{
    sky_lockfree_hashtable_t    *table;

    table = sky_calloc(1, sizeof(sky_lockfree_hashtable_t));
    sky_lockfree_hashtable_init(table, item_compare, item_delete, item_free);

    return table;
}


void
sky_lockfree_hashtable_cleanup(sky_lockfree_hashtable_t *table)
{
    size_t                                  i;
    sky_lockfree_hashtable_item_t           *item, *next_item;
    sky_lockfree_hashtable_tlsdata_block_t  *block, *next_block;

    next_item = table->segments[0]->items[0];
    while ((item = next_item) != NULL) {
        next_item = item->next;
        if (item->key_hash & 1) {
            table->item_free(item);
        }
        else {
            sky_free(item);
        }
    }
    for (i = 0; i < SKY_HASHTABLE_SEGMENT_COUNT && table->segments[i]; ++i) {
        sky_free(table->segments[i]);
    }

    next_block = table->tlsdata;
    while ((block = next_block) != NULL) {
        next_block = block->next;
        sky_free(block);
    }
}


void
sky_lockfree_hashtable_destroy(sky_lockfree_hashtable_t *table)
{
    sky_lockfree_hashtable_cleanup(table);
    sky_free(table);
}


static sky_bool_t
sky_lockfree_hashtable_find_dummy(
        sky_lockfree_hashtable_iterator_t * iterator,
        uintptr_t                           key_hash)
{
    sky_lockfree_hashtable_item_t   *item;

    for (;;) {                          /* try_again: */
        do {
            iterator->hazards[1]->pointer =
                    (void *)((uintptr_t)(*iterator->prev) & ~(uintptr_t)1);
        } while (*iterator->prev != iterator->hazards[1]->pointer);
        iterator->current = iterator->hazards[1]->pointer;

        for (;;) {
            if (!iterator->current) {
                return SKY_FALSE;
            }
            item = iterator->current->next;
            iterator->hazards[0]->pointer =
                    (void *)((uintptr_t)item & ~(uintptr_t)1);
            if (iterator->current->next != item) {
                break;                  /* goto try_again */
            }
            if (*iterator->prev != iterator->current) {
                break;                  /* goto try_again */
            }
            iterator->next = iterator->hazards[0]->pointer;

            if (!((uintptr_t)item & 1)) {
                sky_lockfree_hashtable_iterator_visit(iterator);
                if (iterator->current->key_hash == key_hash) {
                    return SKY_TRUE;
                }
                if (iterator->current->key_hash > key_hash) {
                    return SKY_FALSE;
                }
                iterator->prev = &(iterator->current->next);
                iterator->hazards[2]->pointer = iterator->current;
            }
            else {
                if (!sky_atomic_cas(iterator->current,
                                    iterator->next,
                                    (void **)iterator->prev))
                {
                    break;              /* goto try_again */
                }
                /* Dummy items never get deleted, so no key_hash check is
                 * necessary here. It's always a data item being deleted.
                 */
                sky_hazard_pointer_retire(iterator->current,
                                          iterator->table->item_free);
            }

            iterator->current = iterator->next;
            iterator->hazards[1]->pointer = iterator->next;
        }

        sky_lockfree_hashtable_iterator_retry(iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


static sky_bool_t
sky_lockfree_hashtable_insert_dummy(
        sky_lockfree_hashtable_iterator_t * iterator,
        sky_lockfree_hashtable_item_t *     item)
{
    for (;;) {
        if (sky_lockfree_hashtable_find_dummy(iterator, item->key_hash)) {
            return SKY_FALSE;
        }
        item->next = iterator->current;
        if (sky_atomic_cas(iterator->current, item, (void **)iterator->prev)) {
            return SKY_TRUE;
        }
        sky_lockfree_hashtable_iterator_reset(iterator);
    }
}


static sky_lockfree_hashtable_item_t * volatile *
sky_lockfree_hashtable_item(sky_lockfree_hashtable_t *table, uintptr_t key_hash)
{
    size_t                              bucket, parent_bucket;
    sky_lockfree_hashtable_item_t       *dummy,
                                        * volatile *item, * volatile *parent;
    sky_lockfree_hashtable_iterator_t   iterator;

    bucket = (size_t)key_hash & (table->global_max_length - 1);
    if (bucket <= 1) {
        item = &(table->segments[0]->items[bucket]);
    }
    else {
        size_t                              segmentno;
        sky_lockfree_hashtable_segment_t    *segment;

        segmentno = sky_util_flsl(bucket) - 1;
        if (unlikely(!(segment = table->segments[segmentno]))) {
            segment = sky_calloc(1,
                                 sizeof(sky_lockfree_hashtable_segment_t) +
                                 (((size_t)1 << segmentno) *
                                  sizeof(sky_lockfree_hashtable_item_t)));
            if (!sky_atomic_cas(NULL,
                                segment,
                                SKY_AS_VOIDP(&(table->segments[segmentno]))))
            {
                sky_free(segment);
                segment = table->segments[segmentno];
            }
            else {
                sky_lockfree_hashtable_increment_sizeof(table,
                                                        sky_memsize(segment));
            }
        }
        item = &(segment->items[bucket - ((size_t)1 << segmentno)]);
    }

    if (unlikely(!*item)) {
        parent_bucket =
                (bucket ? (bucket & (((1 << sky_util_flsl(bucket)) / 2) - 1))
                        : 0);
        parent = sky_lockfree_hashtable_item(table, parent_bucket);

        dummy = sky_calloc(1, sizeof(sky_lockfree_hashtable_item_t));
        dummy->key_hash = sky_lockfree_hashtable_reverse(bucket) & ~(uintptr_t)1;

        /* Insert the dummy or use the one inserted by another thread if we've
         * been beaten to the punch. The iterator cleanup is safe where it is
         * because the dummy node will never be deleted once it's inserted into
         * the table. The hazard keeping it safe isn't needed.
         */
        sky_lockfree_hashtable_iterator_init(&iterator, table, parent, SKY_FALSE);
        if (!sky_lockfree_hashtable_insert_dummy(&iterator, dummy)) {
            sky_free(dummy);
            dummy = iterator.current;
        }
        else {
            sky_lockfree_hashtable_increment_sizeof(table,
                                                    sky_memsize(dummy));
        }
        sky_lockfree_hashtable_iterator_cleanup(&iterator);

        *item = dummy;
    }

    return item;
}


static sky_bool_t
sky_lockfree_hashtable_delete_current(
        sky_lockfree_hashtable_iterator_t * iterator,
        uintptr_t                           key_hash,
        void *                              key)
{
    sky_lockfree_hashtable_t    *table = iterator->table;

    sky_lockfree_hashtable_tlsdata_t    *tlsdata;

    if (sky_atomic_cas(iterator->next,
                       (void *)((uintptr_t)iterator->next | 1),
                       SKY_AS_VOIDP(&(iterator->current->next))))
    {
        if (sky_atomic_cas(iterator->current,
                           iterator->next,
                           SKY_AS_VOIDP(iterator->prev)))
        {
            sky_hazard_pointer_retire(iterator->current, table->item_free);
        }
        else {
            /* The failure of the CAS in line B3 implies that another
             * thread must have removed the node cur^ from the list after
             * the success of the CAS in line B2 by the current thread.
             * In such a case, a new Find is invoked in order to guarantee
             * that the number of deleted nodes not yet removed never
             * exceeds the maximum number of concurrent threads operating
             * on the object.
             */
            sky_lockfree_hashtable_iterator_reset(iterator);
            if (key) {
                sky_lockfree_hashtable_iterator_find(iterator, key_hash, key);
            }
            else {
                sky_lockfree_hashtable_iterator_first(iterator);
            }
        }
        tlsdata = sky_lockfree_hashtable_tlsdata_get(table);
        --tlsdata->delta;
        --tlsdata->snapshot_delta;
        if (table->item_delete) {
            table->item_delete(table, iterator->current);
        }
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


sky_bool_t
sky_lockfree_hashtable_delete(sky_lockfree_hashtable_t *table,
                              uintptr_t                 key_hash,
                              void *                    key)
{
    sky_lockfree_hashtable_iterator_t           iterator;
    sky_lockfree_hashtable_item_t * volatile *  head;

    head = sky_lockfree_hashtable_item(table, key_hash);
    key_hash = sky_lockfree_hashtable_reverse(key_hash) | 1;
    sky_lockfree_hashtable_iterator_init(&iterator, table, head, SKY_FALSE);
    for (;;) {
        if (!sky_lockfree_hashtable_iterator_find(&iterator, key_hash, key)) {
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            return SKY_FALSE;
        }
        if (sky_lockfree_hashtable_delete_current(&iterator, key_hash, key)) {
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            return SKY_TRUE;
        }
        sky_lockfree_hashtable_iterator_reset(&iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


sky_bool_t
sky_lockfree_hashtable_deleteitem(sky_lockfree_hashtable_t *        table,
                                  sky_lockfree_hashtable_item_t *   item)
{
    sky_lockfree_hashtable_iterator_t   iterator;

    sky_lockfree_hashtable_iterator_init(&iterator, table, NULL, SKY_FALSE);
    for (;;) {
        if (!sky_lockfree_hashtable_iterator_finditem(&iterator, item)) {
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            return SKY_FALSE;
        }
        if (sky_lockfree_hashtable_delete_current(&iterator, 0, NULL)) {
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            return SKY_TRUE;
        }
        sky_lockfree_hashtable_iterator_reset(&iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


sky_hazard_pointer_t *
sky_lockfree_hashtable_insert(sky_lockfree_hashtable_t *        table,
                              sky_lockfree_hashtable_item_t *   item,
                              uintptr_t                         key_hash,
                              void *                            key)
{
    sky_hazard_pointer_t                        *hazard;
    sky_lockfree_hashtable_tlsdata_t            *tlsdata;
    sky_lockfree_hashtable_iterator_t           iterator;
    sky_lockfree_hashtable_item_t * volatile *  head;

    head = sky_lockfree_hashtable_item(table, key_hash);
    item->key_hash = sky_lockfree_hashtable_reverse(key_hash) | 1;
    sky_lockfree_hashtable_iterator_init(&iterator, table, head, SKY_FALSE);
    for (;;) {
        if (sky_lockfree_hashtable_iterator_find(&iterator,
                                                 item->key_hash,
                                                 key))
        {
            hazard = sky_hazard_pointer_acquire(NULL);
            hazard->pointer = iterator.current;
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            return hazard;
        }
        item->next = iterator.current;
        if (sky_atomic_cas(iterator.current, item, (void **)iterator.prev)) {
            sky_lockfree_hashtable_iterator_cleanup(&iterator);
            tlsdata = sky_lockfree_hashtable_tlsdata_get(table);
            ++tlsdata->delta;
            ++tlsdata->snapshot_delta;
            /* XXX: this should be better. if inserts are evenly distributed
             *      across 16 threads, it'll take 16 times too long to grow
             *      the table, for example.
             */
            if (tlsdata->snapshot_length + tlsdata->snapshot_delta >=
                tlsdata->snapshot_max_length * 2 / 3)
            {
                sky_lockfree_hashtable_update_length(table);
            }
            hazard = sky_hazard_pointer_acquire(NULL);
            hazard->pointer = item;
            return hazard;
        }
        sky_lockfree_hashtable_iterator_reset(&iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


size_t
sky_lockfree_hashtable_length(sky_lockfree_hashtable_t *table)
{
    sky_lockfree_hashtable_update_length(table);
    return table->global_length;
}


sky_hazard_pointer_t *
sky_lockfree_hashtable_lookup(sky_lockfree_hashtable_t *table,
                              uintptr_t                 key_hash,
                              void *                    key)
{
    sky_hazard_pointer_t                        *hazard;
    sky_lockfree_hashtable_iterator_t           iterator;
    sky_lockfree_hashtable_item_t * volatile    *head;

    head = sky_lockfree_hashtable_item(table, key_hash);
    key_hash = sky_lockfree_hashtable_reverse(key_hash) | 1;
    sky_lockfree_hashtable_iterator_init(&iterator, table, head, SKY_FALSE);
    if (!sky_lockfree_hashtable_iterator_find(&iterator, key_hash, key)) {
        hazard = NULL;
    }
    else {
        hazard = sky_hazard_pointer_acquire(NULL);
        hazard->pointer = iterator.current;
    }
    sky_lockfree_hashtable_iterator_cleanup(&iterator);

    return hazard;
}


sky_hazard_pointer_t *
sky_lockfree_hashtable_pop(sky_lockfree_hashtable_t *   table,
                           uintptr_t                    key_hash,
                           void *                       key)
{
    sky_hazard_pointer_t                        *hazard;
    sky_lockfree_hashtable_iterator_t           iterator;
    sky_lockfree_hashtable_item_t * volatile *  head, *item;

    if (!key) {
        head = &(table->segments[0]->items[0]);
        sky_lockfree_hashtable_iterator_init(&iterator, table, head, SKY_FALSE);
        for (;;) {
            if (!(item = sky_lockfree_hashtable_iterator_first(&iterator))) {
                hazard = NULL;
                break;
            }
            if (sky_lockfree_hashtable_delete_current(&iterator, 0, NULL)) {
                hazard = sky_hazard_pointer_acquire(NULL);
                hazard->pointer = item;
                break;
            }
            sky_lockfree_hashtable_iterator_reset(&iterator);
        }
    }
    else {
        head = sky_lockfree_hashtable_item(table, key_hash);
        key_hash = sky_lockfree_hashtable_reverse(key_hash) | 1;
        sky_lockfree_hashtable_iterator_init(&iterator, table, head, SKY_FALSE);
        for (;;) {
            if (!sky_lockfree_hashtable_iterator_find(&iterator, key_hash, key)) {
                hazard = NULL;
            }
            else {
                hazard = sky_hazard_pointer_acquire(NULL);
                hazard->pointer = iterator.current;
                if (sky_lockfree_hashtable_delete_current(&iterator, 0, NULL)) {
                    break;
                }
                sky_hazard_pointer_release(hazard);
            }
            sky_lockfree_hashtable_iterator_reset(&iterator);
        }
    }

    sky_lockfree_hashtable_iterator_cleanup(&iterator);
    return hazard;
}


void
sky_lockfree_hashtable_setcapacity(sky_lockfree_hashtable_t *   table,
                                   size_t                       capacity)
{
    intmax_t    max_length;

    capacity = SKY_MIN((size_t)sky_util_roundpow2(capacity),
                       (size_t)INT32_MAX + 1);
    while (capacity > (size_t)(max_length = table->global_max_length)) {
        if (sizeof(int32_t) == sizeof(intmax_t)) {
            sky_atomic_cas32((int32_t)max_length,
                             (int32_t)capacity,
                             SKY_AS_TYPE(&(table->global_max_length), int32_t *));
        }
        else if (sizeof(int64_t) == sizeof(intmax_t)) {
            sky_atomic_cas64(max_length,
                             capacity,
                             SKY_AS_TYPE(&(table->global_max_length), int64_t *));
        }
        else {
            sky_error_fatal("implementation missing");
        }
    }
}


void
sky_lockfree_hashtable_iterator_init(
        sky_lockfree_hashtable_iterator_t *         iterator,
        sky_lockfree_hashtable_t *                  table,
        sky_lockfree_hashtable_item_t * volatile *  head,
        sky_bool_t                                  safe)
{
    if (!head) {
        head = &(table->segments[0]->items[0]);
    }
    iterator->table = table;
    iterator->head = head;
    iterator->prev = head;
    iterator->marker = head;
    iterator->current = NULL;
    iterator->next = NULL;
    iterator->hazards[0] = sky_hazard_pointer_acquire(NULL);
    iterator->hazards[1] = sky_hazard_pointer_acquire(NULL);
    iterator->hazards[2] = sky_hazard_pointer_acquire(NULL);
    iterator->visited = NULL;
    iterator->visited_cursor = iterator->visited_size = iterator->visited_used = 0;
    iterator->retrying = SKY_FALSE;
    iterator->safe = safe;
}


void
sky_lockfree_hashtable_iterator_cleanup(
        sky_lockfree_hashtable_iterator_t * iterator)
{
    sky_hazard_pointer_release(iterator->hazards[0]);
    sky_hazard_pointer_release(iterator->hazards[1]);
    sky_hazard_pointer_release(iterator->hazards[2]);
    sky_free(iterator->visited);
}


void
sky_lockfree_hashtable_iterator_reset(
        sky_lockfree_hashtable_iterator_t * iterator)
{
    iterator->marker = iterator->prev = iterator->head;
    iterator->current = iterator->next = NULL;
    iterator->hazards[0]->pointer = NULL;
    iterator->hazards[1]->pointer = NULL;
    iterator->hazards[2]->pointer = NULL;
    iterator->visited_cursor = iterator->visited_used = 0;
    iterator->retrying = SKY_FALSE;
}


static void
sky_lockfree_hashtable_iterator_retry(
        sky_lockfree_hashtable_iterator_t * iterator)
{
    iterator->prev = iterator->marker;
    iterator->current = iterator->next = NULL;
    iterator->hazards[0]->pointer = NULL;
    iterator->hazards[1]->pointer = NULL;
    iterator->hazards[2]->pointer = NULL;
    iterator->visited_cursor = 0;
    iterator->retrying = SKY_TRUE;
}


static sky_bool_t
sky_lockfree_hashtable_iterator_visit(
        sky_lockfree_hashtable_iterator_t * iterator)
{
    size_t  i;

    if (!(iterator->current->key_hash & 1)) {
        sky_error_validate_debug(iterator->current == *iterator->prev);
        iterator->marker = &(iterator->current);
        iterator->visited_cursor = iterator->visited_used = 0;
        iterator->retrying = SKY_FALSE;
        return SKY_TRUE;
    }

    if (iterator->safe) {
        if (iterator->retrying) {
            for (i = iterator->visited_cursor; i < iterator->visited_used; ++i) {
                if (iterator->current == iterator->visited[i]) {
                    iterator->visited_cursor = i + 1;
                    return SKY_TRUE;
                }
            }
        }

        if (iterator->visited_used >= iterator->visited_size) {
            iterator->visited_size = (iterator->visited_size
                                   ?  iterator->visited_size * 2
                                   :  8);
            iterator->visited =
                    sky_realloc(iterator->visited,
                                iterator->visited_size *
                                sizeof(sky_lockfree_hashtable_item_t *));
        }
        iterator->visited[iterator->visited_used++] = iterator->current;
    }

    return SKY_FALSE;
}


sky_bool_t
sky_lockfree_hashtable_iterator_find(
        sky_lockfree_hashtable_iterator_t * iterator,
        uintptr_t                           key_hash,
        void *                              key)
{
    sky_lockfree_hashtable_item_t   *item;

    for (;;) {                          /* try_again: */
        do {
            iterator->hazards[1]->pointer =
                    (void *)((uintptr_t)(*iterator->prev) & ~(uintptr_t)1);
        } while (*iterator->prev != iterator->hazards[1]->pointer);
        iterator->current = iterator->hazards[1]->pointer;

        for (;;) {
            if (!iterator->current) {
                return SKY_FALSE;
            }
            item = iterator->current->next;
            iterator->hazards[0]->pointer =
                    (void *)((uintptr_t)item & ~(uintptr_t)1);
            if (iterator->current->next != item) {
                break;                  /* goto try_again */
            }
            if (*iterator->prev != iterator->current) {
                break;                  /* goto try_again */
            }
            iterator->next = iterator->hazards[0]->pointer;

            if (!((uintptr_t)item & 1)) {
                if (!sky_lockfree_hashtable_iterator_visit(iterator)) {
                    if (iterator->current->key_hash == key_hash &&
                        iterator->table->item_compare(iterator->current, key))
                    {
                        return SKY_TRUE;
                    }
                }
                if (iterator->current->key_hash > key_hash) {
                    return SKY_FALSE;
                }
                iterator->prev = &(iterator->current->next);
                iterator->hazards[2]->pointer = iterator->current;
            }
            else {
                if (!sky_atomic_cas(iterator->current,
                                    iterator->next,
                                    (void **)iterator->prev))
                {
                    break;              /* goto try_again */
                }
                /* Dummy items never get deleted, so no key_hash check is
                 * necessary here. It's always a data item being deleted.
                 */
                sky_hazard_pointer_retire(iterator->current,
                                          iterator->table->item_free);
            }

            iterator->current = iterator->next;
            iterator->hazards[1]->pointer = iterator->next;
        }

        sky_lockfree_hashtable_iterator_retry(iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


sky_bool_t
sky_lockfree_hashtable_iterator_finditem(
        sky_lockfree_hashtable_iterator_t * iterator,
        sky_lockfree_hashtable_item_t *     item)
{
    sky_lockfree_hashtable_item_t   *existing_item;

    for (;;) {                          /* try_again: */
        do {
            iterator->hazards[1]->pointer =
                    (void *)((uintptr_t)(*iterator->prev) & ~(uintptr_t)1);
        } while (*iterator->prev != iterator->hazards[1]->pointer);
        iterator->current = iterator->hazards[1]->pointer;

        for (;;) {
            if (!iterator->current) {
                return SKY_FALSE;
            }
            existing_item = iterator->current->next;
            iterator->hazards[0]->pointer =
                    (void *)((uintptr_t)existing_item & ~(uintptr_t)1);
            if (iterator->current->next != existing_item) {
                break;                  /* goto try_again */
            }
            if (*iterator->prev != iterator->current) {
                break;                  /* goto try_again */
            }
            iterator->next = iterator->hazards[0]->pointer;

            if (!((uintptr_t)existing_item & 1)) {
                if (!sky_lockfree_hashtable_iterator_visit(iterator)) {
                    if (iterator->current == item) {
                        return SKY_TRUE;
                    }
                }
                if (iterator->current->key_hash > item->key_hash) {
                    return SKY_FALSE;
                }
                iterator->prev = &(iterator->current->next);
                iterator->hazards[2]->pointer = iterator->current;
            }
            else {
                if (!sky_atomic_cas(iterator->current,
                                    iterator->next,
                                    (void **)iterator->prev))
                {
                    break;              /* goto try_again */
                }
                /* Dummy items never get deleted, so no key_hash check is
                 * necessary here. It's always a data item being deleted.
                 */
                sky_hazard_pointer_retire(iterator->current,
                                          iterator->table->item_free);
            }

            iterator->current = iterator->next;
            iterator->hazards[1]->pointer = iterator->next;
        }

        sky_lockfree_hashtable_iterator_retry(iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


static sky_lockfree_hashtable_item_t *
sky_lockfree_hashtable_iterator_first(
        sky_lockfree_hashtable_iterator_t *iterator)
{
    sky_lockfree_hashtable_item_t   *item;

    for (;;) {                          /* try_again: */
        if (!iterator->hazards[1]->pointer) {
            do {
                iterator->hazards[1]->pointer =
                        (void *)((uintptr_t)(*iterator->prev) & ~(uintptr_t)1);
            } while (*iterator->prev != iterator->hazards[1]->pointer);
            iterator->current = iterator->hazards[1]->pointer;
        }

        for (;;) {
            if (!iterator->current) {
                return NULL;
            }
            item = iterator->current->next;
            iterator->hazards[0]->pointer =
                    (void *)((uintptr_t)item & ~(uintptr_t)1);
            if (iterator->current->next != item) {
                break;                  /* goto try_again */
            }
            if (*iterator->prev != iterator->current) {
                break;                  /* goto try_again */
            }
            iterator->next = iterator->hazards[0]->pointer;

            if (!((uintptr_t)item & 1)) {
                sky_lockfree_hashtable_iterator_visit(iterator);
                if (iterator->current->key_hash & 1) {
                    return iterator->current;
                }
                iterator->prev = &(iterator->current->next);
                iterator->hazards[2]->pointer = iterator->current;
            }
            else {
                if (!sky_atomic_cas(iterator->current,
                                    iterator->next,
                                    (void **)iterator->prev))
                {
                    break;              /* goto try_again */
                }
                /* Dummy items never get deleted, so no key_hash check is
                 * necessary here. It's always a data item being deleted.
                 */
                sky_hazard_pointer_retire(iterator->current,
                                          iterator->table->item_free);
            }

            iterator->current = iterator->next;
            iterator->hazards[1]->pointer = iterator->next;
        }

        sky_lockfree_hashtable_iterator_retry(iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}


sky_lockfree_hashtable_item_t *
sky_lockfree_hashtable_iterator_next(
        sky_lockfree_hashtable_iterator_t *iterator)
{
    sky_lockfree_hashtable_item_t   *item;

    for (;;) {                          /* try_again: */
        if (!iterator->hazards[1]->pointer) {
            do {
                iterator->hazards[1]->pointer =
                        (void *)((uintptr_t)(*iterator->prev) & ~(uintptr_t)1);
            } while (*iterator->prev != iterator->hazards[1]->pointer);
            iterator->current = iterator->hazards[1]->pointer;
        }

        for (;;) {
            if (!iterator->current) {
                return NULL;
            }
            item = iterator->current->next;
            iterator->hazards[0]->pointer =
                    (void *)((uintptr_t)item & ~(uintptr_t)1);
            if (iterator->current->next != item) {
                break;                  /* goto try_again */
            }
            if (*iterator->prev != iterator->current) {
                break;                  /* goto try_again */
            }
            iterator->next = iterator->hazards[0]->pointer;

            if (!((uintptr_t)item & 1)) {
                if (!sky_lockfree_hashtable_iterator_visit(iterator)) {
                    if ((iterator->current->key_hash & 1)) {
                        iterator->prev = &(iterator->current->next);
                        iterator->hazards[2]->pointer = iterator->current;
                        iterator->current = iterator->next;
                        iterator->hazards[1]->pointer = iterator->next;
                        return iterator->hazards[2]->pointer;
                    }
                }

                iterator->prev = &(iterator->current->next);
                iterator->hazards[2]->pointer = iterator->current;
            }
            else {
                if (!sky_atomic_cas(iterator->current,
                                    iterator->next,
                                    (void **)iterator->prev))
                {
                    break;              /* goto try_again */
                }
                /* Dummy items never get deleted, so no key_hash check is
                 * necessary here. It's always a data item being deleted.
                 */
                sky_hazard_pointer_retire(iterator->current,
                                          iterator->table->item_free);
            }

            iterator->current = iterator->next;
            iterator->hazards[1]->pointer = iterator->next;
        }

        sky_lockfree_hashtable_iterator_retry(iterator);
    }

    sky_error_fatal("internal error - unreachable code reached");
}
