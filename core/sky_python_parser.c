#include "sky_private.h"
#include "sky_python_parser.h"


typedef struct sky_python_parser_arguments_s {
    sky_list_t                          args;
    sky_list_t                          keywords;
    sky_object_t                        starargs;
    sky_object_t                        kwargs;
} sky_python_parser_arguments_t;


typedef struct sky_python_parser_parameter_s {
    sky_string_t                        name;
    int                                 name_lineno;
    int                                 name_col_offset;

    sky_object_t                        annotation;
    sky_object_t                        default_value;
} sky_python_parser_parameter_t;


static sky_object_t
sky_python_parser_expression(sky_python_parser_t *        parser,
                             const sky_python_token_t *   start_token);

static sky_object_t
sky_python_parser_expression_nocond(sky_python_parser_t *       parser,
                                    const sky_python_token_t *  start_token);

static sky_list_t
sky_python_parser_target_list(sky_python_parser_t *         parser,
                              sky_python_ast_expr_context_t ctx);

static sky_object_t
sky_python_parser_target_list_as_expr(sky_python_parser_t *         parser,
                                      sky_python_ast_expr_context_t ctx);

static sky_object_t
sky_python_parser_or_test(sky_python_parser_t *     parser,
                          const sky_python_token_t *start_token);


static void SKY_NORETURN
sky_python_parser_raise_(sky_python_parser_t *  parser,
                         sky_type_t             type,
                         const char *           c_function,
                         const char *           c_filename,
                         int                    c_lineno,
                         int                    lineno,
                         int                    col_offset,
                         const char *           fmt,
                         ...)
{
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformatv(fmt, ap);
    va_end(ap);

    text = sky_python_lexer_line(&(parser->lexer), lineno);

    sky_error_raise_(type,
                     sky_object_build("(O(OiiO))",
                                      message,
                                      parser->lexer.filename,
                                      lineno,
                                      col_offset,
                                      text),
                     c_function,
                     c_filename,
                     c_lineno);
}

#define sky_python_parser_raise(parser, type, lineno, col_offset, fmt, ...) \
        sky_python_parser_raise_(parser, type,                              \
                                 __FUNCTION__, __FILE__, __LINE__,          \
                                 lineno, col_offset, fmt, ## __VA_ARGS__)


static void SKY_NORETURN
sky_python_parser_error_(sky_python_parser_t *      parser,
                         const sky_python_token_t * token,
                         const char *               c_function,
                         const char *               c_filename,
                         int                        c_lineno)
{
    const char      *message;
    sky_type_t      type;
    sky_string_t    text;

    type = sky_SyntaxError;
    if (SKY_PYTHON_TOKEN_ENDMARKER == token->name) {
        message = "unexpected EOF while parsing";
    }
    else if (SKY_PYTHON_TOKEN_INDENT == token->name) {
        type = sky_IndentationError;
        message = "unexpected indent";
    }
    else {
        message = "invalid syntax";
    }
    text = sky_python_lexer_line(&(parser->lexer), token->lineno);

    sky_error_raise_(type,
                     sky_object_build("(s(OiiO))",
                                      message,
                                      parser->lexer.filename,
                                      token->lineno,
                                      token->col_offset,
                                      text),
                     c_function,
                     c_filename,
                     c_lineno);
}

#define sky_python_parser_error(parser, token)                              \
        sky_python_parser_error_(parser, token,                             \
                                 __FUNCTION__, __FILE__, __LINE__)


static void SKY_NORETURN
sky_python_parser_ast_error_(sky_python_parser_t *  parser,
                             sky_object_t           node,
                             const char *           c_function,
                             const char *           c_filename,
                             int                    c_lineno,
                             const char *           fmt,
                             ...)
{
    int             col_offset, lineno;
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformatv(fmt, ap);
    va_end(ap);

    if (sky_object_isa(node, sky_python_ast_stmt_type)) {
        lineno = ((sky_python_ast_stmt_t)node)->lineno;
        col_offset = ((sky_python_ast_stmt_t)node)->col_offset;
        text = sky_python_lexer_line(&(parser->lexer), lineno);
    }
    else if (sky_object_isa(node, sky_python_ast_expr_type)) {
        lineno = ((sky_python_ast_expr_t)node)->lineno;
        col_offset = ((sky_python_ast_expr_t)node)->col_offset;
        text = sky_python_lexer_line(&(parser->lexer), lineno);
    }
    else {
        lineno = 1;
        col_offset = 0;
        text = NULL;
    }

    sky_error_raise_(sky_SyntaxError,
                     sky_object_build("(O(OiiO))",
                                      message,
                                      parser->lexer.filename,
                                      lineno,
                                      col_offset,
                                      text),
                     c_function,
                     c_filename,
                     c_lineno);
}

#define sky_python_parser_ast_error(parser, node, fmt, ...)                 \
        sky_python_parser_ast_error_(parser, node,                          \
                                     __FUNCTION__, __FILE__, __LINE__,      \
                                    fmt, ## __VA_ARGS__)


static inline void
sky_python_parser_list_append(sky_list_t list, sky_object_t item)
{
    if (sky_object_isa(item, sky_list_type)) {
        sky_list_extend(list, item);
    }
    else {
        sky_error_validate_debug(!sky_object_isa(item, sky_tuple_type));
        sky_list_append(list, item);
    }
}


static inline void
sky_python_parser_token_get(sky_python_parser_t *   parser,
                            sky_python_token_t *    token)
    /* Get the next token from the parser's lexer, possibly popping from the
     * token stack (used for token look-ahead to disambiguate grammar).
     * Call sky_python_parser_error() on ERROR.
     * Return NULL on ENDMARKER.
     */
{
    if (parser->token_sp > parser->token_stack) {
        *token = *--parser->token_sp;
    }
    else {
        sky_python_lexer_token(&(parser->lexer), token);
    }
    if (SKY_PYTHON_TOKEN_ERROR == token->name) {
        sky_python_parser_error(parser, token);
    }
}


static void
sky_python_parser_token_push(sky_python_parser_t *      parser,
                             const sky_python_token_t * token)
    /* Push a token onto the token stack. A copy of the token is made. */
{
    size_t  stack_size = sizeof(parser->token_stack) /
                         sizeof(parser->token_stack[0]);

    sky_error_validate(parser->token_sp < &(parser->token_stack[stack_size]));
    *parser->token_sp++ = *token;
}


static inline sky_python_token_t *
sky_python_parser_expect_token(sky_python_parser_t *    parser,
                               sky_python_token_name_t  name,
                               sky_python_token_t *     token)
    /* Get the next token from the parser's lexer, possibly popping from the
     * token stack (used for token look-ahead to disambiguate grammar).
     * Call sky_python_parser_error() if the token is not what is expected.
     */
{
    sky_python_parser_token_get(parser, token);
    if (token->name != name) {
        sky_python_parser_error(parser, token);
    }
    return token;
}


static sky_bool_t
sky_python_parser_is_keyword(sky_string_t string)
{
    const char  *s;

    if (!(s = sky_string_cstring(string))) {
        return SKY_FALSE;
    }

    /* Check for language keywords parsed as Name nodes. There are only three:
     * None, True, and False.
     */
    if (!strcmp(s, "None") || !strcmp(s, "True") || !strcmp(s, "False")) {
        return SKY_TRUE;
    }

    /* CPython also restricts assignment to '__debug__' */
    if (!strcmp(s, "__debug__")) {
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static void
sky_python_parser_set_context(sky_python_parser_t *         parser,
                              sky_object_t                  node,
                              sky_python_ast_expr_context_t ctx)
{
    const char      *action, *target;
    sky_object_t    elt;

    sky_error_validate_debug(sky_object_isa(node, sky_python_ast_expr_type));

    if (sky_object_isa(node, sky_python_ast_Attribute_type)) {
        sky_python_ast_Attribute_t  attr = (sky_python_ast_Attribute_t)node;

        attr->ctx = ctx;
        if (sky_python_ast_Store == ctx &&
            sky_python_parser_is_keyword(attr->attr))
        {
            sky_python_parser_ast_error(parser,
                                        node,
                                        "assignment to keyword");
        }
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Subscript_type)) {
        ((sky_python_ast_Subscript_t)node)->ctx = ctx;
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Starred_type)) {
        sky_python_ast_Starred_t    starred = (sky_python_ast_Starred_t)node;

        starred->ctx = ctx;
        sky_python_parser_set_context(parser, starred->value, ctx);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Name_type)) {
        sky_python_ast_Name_t   name = (sky_python_ast_Name_t)node;

        name->ctx = ctx;
        if (sky_python_ast_Store == ctx &&
            sky_python_parser_is_keyword(name->id))
        {
            sky_python_parser_ast_error(parser,
                                        node,
                                        "assignment to keyword");
        }
        return;
    }
    if (sky_object_isa(node, sky_python_ast_List_type)) {
        sky_python_ast_List_t   list = (sky_python_ast_List_t)node;

        list->ctx = ctx;
        SKY_SEQUENCE_FOREACH(list->elts, elt) {
            sky_python_parser_set_context(parser, elt, ctx);
        } SKY_SEQUENCE_FOREACH_END;

        return;
    }
    if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        sky_python_ast_Tuple_t  tuple = (sky_python_ast_Tuple_t)node;

        tuple->ctx = ctx;
        SKY_SEQUENCE_FOREACH(tuple->elts, elt) {
            sky_python_parser_set_context(parser, elt, ctx);
        } SKY_SEQUENCE_FOREACH_END;

        return;
    }

    /* Anything else is an error. Generate an appropriate error message
     * depending on the node type.
     */

    if (sky_object_isa(node, sky_python_ast_BoolOp_type) ||
        sky_object_isa(node, sky_python_ast_BinOp_type) ||
        sky_object_isa(node, sky_python_ast_UnaryOp_type))
    {
        target = "operator";
    }
    else if (sky_object_isa(node, sky_python_ast_Lambda_type)) {
        target = "lambda";
    }
    else if (sky_object_isa(node, sky_python_ast_IfExp_type)) {
        target = "conditional expression";
    }
    else if (sky_object_isa(node, sky_python_ast_Dict_type) ||
             sky_object_isa(node, sky_python_ast_Set_type) ||
             sky_object_isa(node, sky_python_ast_Num_type) ||
             sky_object_isa(node, sky_python_ast_Str_type) ||
             sky_object_isa(node, sky_python_ast_Bytes_type))
    {
        target = "literal";
    }
    else if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
        target = "list comprehension";
    }
    else if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
        target = "set comprehension";
    }
    else if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
        target = "dict comprehension";
    }
    else if (sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        target = "generator expression";
    }
    else if (sky_object_isa(node, sky_python_ast_Yield_type) ||
             sky_object_isa(node, sky_python_ast_YieldFrom_type))
    {
        target = "yield expression";
    }
    else if (sky_object_isa(node, sky_python_ast_Compare_type)) {
        target = "comparison";
    }
    else if (sky_object_isa(node, sky_python_ast_Call_type)) {
        target = "function call";
    }
    else if (sky_object_isa(node, sky_python_ast_Ellipsis_type)) {
        target = "Ellipsis";
    }
    else {
        sky_error_fatal("internal error");
    }

    if (sky_object_isa(ctx, sky_python_ast_Store_type)) {
        action = "assign to";
    }
    else if (sky_object_isa(ctx, sky_python_ast_Del_type)) {
        action = "delete";
    }
    else {
        sky_error_fatal("internal error");
    }

    sky_python_parser_ast_error(parser, node, "can't %s %s", action, target);
}


static sky_object_t
sky_python_parser_expression_list(sky_python_parser_t *     parser,
                                  const sky_python_token_t *start_token,
                                  sky_object_t              expression,
                                  const sky_type_t          sequence_type)
    /* expression_list ::= expression ("," expression)* [","]
     */
{
    sky_list_t          elts;
    sky_python_token_t  token;

    if (!expression) {
        if (!(expression = sky_python_parser_expression(parser, start_token))) {
            return NULL;
        }

        /* Try for a comma. If there isn't one, simply return expression;
         * otherwise, create elts and populate it. Return Tuple at the end.
         */
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            sky_python_parser_token_push(parser, &token);
            return expression;
        }
    }

    elts = sky_object_build("[O]", expression);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (!(expression = sky_python_parser_expression(parser, &token))) {
            break;
        }
        sky_list_append(elts, expression);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            break;
        }
    }
    sky_python_parser_token_push(parser, &token);

    if (!sequence_type || sky_python_ast_Tuple_type == sequence_type) {
        return sky_python_ast_Tuple_create(elts,
                                           sky_python_ast_Load,
                                           start_token->lineno,
                                           start_token->col_offset);
    }
    if (sky_python_ast_List_type == sequence_type) {
        return sky_python_ast_List_create(elts,
                                          sky_python_ast_Load,
                                          start_token->lineno,
                                          start_token->col_offset);
    }
    if (sky_python_ast_Set_type == sequence_type) {
        return sky_python_ast_Set_create(elts,
                                         start_token->lineno,
                                         start_token->col_offset);
    }
    sky_error_fatal("internal error");
}


static sky_list_t
sky_python_parser_generators(sky_python_parser_t *parser)
    /* comprehension ::= expression comp_for
     * comp_for      ::= "for" target_list "in" or_test [comp_iter]
     * comp_iter     ::= comp_for | comp_if
     * comp_if       ::= "if" expression_nocond [comp_iter]
     *
     * This function assumes that the caller has already consumed the "for"
     * token.
     */
{
    sky_list_t          generators, ifs;
    sky_object_t        iter, target;
    sky_python_token_t  token;

    generators = sky_list_create(NULL);
    for (;;) {
        target = sky_python_parser_target_list_as_expr(parser,
                                                       sky_python_ast_Store);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_IN, &token);

        sky_python_parser_token_get(parser, &token);
        if (!(iter = sky_python_parser_or_test(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        ifs = sky_list_create(NULL);
        for (;;) {
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_IF != token.name) {
                break;
            }
            sky_list_append(ifs,
                            sky_python_parser_expression_nocond(parser, NULL));
        }
        sky_python_parser_token_push(parser, &token);

        sky_list_append(generators,
                        sky_python_ast_comprehension_create(target, iter, ifs));

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_FOR != token.name) {
            break;
        }
    }
    sky_python_parser_token_push(parser, &token);

    return generators;
}


static void
sky_python_parser_keyword_arguments(sky_python_parser_t *           parser,
                                    sky_python_parser_arguments_t * arguments,
                                    sky_set_t                       keywords)
    /* keyword_arguments ::= keyword_item ("," keyword_item)*
     * keyword_item      ::= identifier "=" expression
     */
{
    sky_object_t        value;
    sky_string_t        arg;
    sky_python_token_t  dummy, token;

    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_NAME != token.name) {
            sky_python_parser_token_push(parser, &token);
            break;
        }
        arg = token.object;
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_EQUALS, &dummy);
        value = sky_python_parser_expression(parser, NULL);
        if (!keywords) {
            keywords = sky_set_create(NULL);
        }
        if (!sky_set_add(keywords, arg)) {
            sky_python_parser_raise(parser,
                                    sky_SyntaxError,
                                    token.lineno,
                                    token.col_offset,
                                    "keyword argument repeated");
        }
        sky_list_append(arguments->keywords,
                        sky_python_ast_keyword_create(arg, value));

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            sky_python_parser_token_push(parser, &token);
            break;
        }
    }
}

static void
sky_python_parser_argument_list(sky_python_parser_t *           parser,
                                sky_python_parser_arguments_t * arguments)
{
    sky_set_t           keywords;
    sky_list_t          generators;
    sky_object_t        expr;
    sky_python_token_t  token;

    keywords = NULL;
    arguments->args = sky_list_create(NULL);
    arguments->keywords = sky_list_create(NULL);
    arguments->starargs = arguments->kwargs = NULL;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_STAR != token.name &&
        SKY_PYTHON_TOKEN_STAR_STAR != token.name)
    {
        if (!(expr = sky_python_parser_expression(parser, &token))) {
            /* Not an error - could be an empty argument list. The caller is
             * responsible for handling any closing parenthesis that might be
             * required.
             */
            sky_python_parser_token_push(parser, &token);
            return;
        }

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_FOR == token.name) {
            generators = sky_python_parser_generators(parser);
            sky_list_append(
                    arguments->args,
                    sky_python_ast_GeneratorExp_create(
                            expr,
                            generators,
                            token.lineno,
                            token.col_offset));
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_COMMA != token.name) {
                sky_python_parser_token_push(parser, &token);
            }
            return;
        }

        /* Positional arguments. */
        if (SKY_PYTHON_TOKEN_COMMA == token.name) {
            sky_list_append(arguments->args, expr);
            for (;;) {
                sky_python_parser_token_get(parser, &token);
                if (SKY_PYTHON_TOKEN_STAR == token.name ||
                    SKY_PYTHON_TOKEN_STAR_STAR == token.name)
                {
                    break;
                }
                if (!(expr = sky_python_parser_expression(parser, &token))) {
                    break;
                }

                sky_python_parser_token_get(parser, &token);
                if (SKY_PYTHON_TOKEN_EQUALS == token.name) {
                    break;
                }
                sky_list_append(arguments->args, expr);
                if (SKY_PYTHON_TOKEN_COMMA != token.name) {
                    break;
                }
            }
        }
        else if (SKY_PYTHON_TOKEN_EQUALS != token.name) {
            /* Single positional argument */
            sky_list_append(arguments->args, expr);
            sky_python_parser_token_push(parser, &token);
            return;
        }

        /* Keyword arguments */
        if (SKY_PYTHON_TOKEN_EQUALS == token.name) {
            /* Make sure expr is an identifier. */
            if (!sky_object_isa(expr, sky_python_ast_Name_type)) {
                sky_python_parser_error(parser, &token);
            }
            if (!keywords) {
                keywords = sky_set_create(NULL);
            }
            if (!sky_set_add(keywords, ((sky_python_ast_Name_t)expr)->id)) {
                sky_python_parser_raise(parser,
                                        sky_SyntaxError,
                                        token.lineno,
                                        token.col_offset,
                                        "keyword argument repeated");
            }
            sky_list_append(
                    arguments->keywords,
                    sky_python_ast_keyword_create(
                            ((sky_python_ast_Name_t)expr)->id,
                            sky_python_parser_expression(parser, NULL)));

            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_COMMA == token.name) {
                sky_python_parser_keyword_arguments(parser,
                                                    arguments,
                                                    keywords);
                sky_python_parser_token_get(parser, &token);
            }
        }
    }

    if (SKY_PYTHON_TOKEN_STAR == token.name) {
        arguments->starargs = sky_python_parser_expression(parser, NULL);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA == token.name) {
            sky_python_parser_keyword_arguments(parser, arguments, keywords);
            sky_python_parser_token_get(parser, &token);
        }
    }

    if (SKY_PYTHON_TOKEN_STAR_STAR == token.name) {
        arguments->kwargs = sky_python_parser_expression(parser, NULL);
        sky_python_parser_token_get(parser, &token);
    }

    if (SKY_PYTHON_TOKEN_COMMA != token.name) {
        sky_python_parser_token_push(parser, &token);
    }
}


static sky_object_t
sky_python_parser_yield_expression(sky_python_parser_t *        parser,
                                   const sky_python_token_t *   start_token)
    /* yield_expression ::= "yield" [expression_list | "from" expression]
     */
{
    int col_offset = start_token->col_offset,
        lineno = start_token->lineno;

    sky_object_t        value;
    sky_python_token_t  token;

    if (SKY_PYTHON_TOKEN_YIELD != start_token->name) {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_FROM == token.name) {
        value = sky_python_parser_expression(parser, NULL);
        return sky_python_ast_YieldFrom_create(value, lineno, col_offset);
    }
    else if (!(value = sky_python_parser_expression_list(parser,
                                                         &token,
                                                         NULL,
                                                         NULL)))
    {
        sky_python_parser_token_push(parser, &token);
    }

    return sky_python_ast_Yield_create(value, lineno, col_offset);
}


static sky_object_t
sky_python_parser_atom_lbrace(sky_python_parser_t *     parser,
                              const sky_python_token_t *start_token)
    /* set_display        ::= "{" (expression_list | comprehension) "}"
     * dict_display       ::= "{" [key_datum_list | dict_comprehension] "}"
     * key_datum_list     ::= key_datum ("," key_datum)* [","]
     * key_datum          ::= expression ":" expression
     * dict_comprehension ::= expression ":" expression comp_for
     */
{
    sky_list_t          elts, generators, keys, values;
    sky_object_t        atom, expr, value;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_RBRACE == token.name) {
        keys = sky_list_create(NULL);
        values = sky_list_create(NULL);
        return sky_python_ast_Dict_create(keys,
                                          values,
                                          start_token->lineno,
                                          start_token->col_offset);
    }

    if (!(expr = sky_python_parser_expression(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_RBRACE == token.name) {
        elts = sky_object_build("[O]", expr);
        return sky_python_ast_Set_create(elts, 
                                         start_token->lineno,
                                         start_token->col_offset);
    }
    if (SKY_PYTHON_TOKEN_FOR == token.name) {
        generators = sky_python_parser_generators(parser);
        atom = sky_python_ast_SetComp_create(expr,
                                             generators,
                                             start_token->lineno,
                                             start_token->col_offset);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RBRACE, &token);
        return atom;
    }
    if (SKY_PYTHON_TOKEN_COMMA == token.name) {
        atom = sky_python_parser_expression_list(parser,
                                                 start_token,
                                                 expr,
                                                 sky_python_ast_Set_type);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RBRACE, &token);
        return atom;
    }
    if (SKY_PYTHON_TOKEN_COLON != token.name) {
        sky_python_parser_error(parser, &token);
    }
    value = sky_python_parser_expression(parser, NULL);
    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_FOR == token.name) {
        generators = sky_python_parser_generators(parser);
        atom = sky_python_ast_DictComp_create(expr,
                                              value,
                                              generators,
                                              start_token->lineno,
                                              start_token->col_offset);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RBRACE, &token);
        return atom;
    }

    keys = sky_object_build("[O]", expr);
    values = sky_object_build("[O]", value);

    if (SKY_PYTHON_TOKEN_COMMA == token.name) {
        for (;;) {
            sky_python_parser_token_get(parser, &token);
            if (!(expr = sky_python_parser_expression(parser, &token))) {
                break;
            }
            sky_list_append(keys, expr);

            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_COLON,
                                           &token);

            value = sky_python_parser_expression(parser, NULL);
            sky_list_append(values, value);

            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_COMMA != token.name) {
                break;
            }
        }
    }

    if (SKY_PYTHON_TOKEN_RBRACE != token.name) {
        sky_python_parser_error(parser, &token);
    }

    return sky_python_ast_Dict_create(keys,
                                      values,
                                      start_token->lineno,
                                      start_token->col_offset);
}


static sky_object_t
sky_python_parser_atom_lparen(sky_python_parser_t *     parser,
                              const sky_python_token_t *start_token)
    /* parenth_form         ::= "(" [expression_list] ")"
     * generator_expression ::= "(" expression comp_for ")"
     * yield_atom           ::= "(" yield_expression ")"
     */
{
    sky_list_t          generators;
    sky_object_t        atom, expr;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_RPAREN == token.name) {
        return sky_python_ast_Tuple_create(sky_list_create(NULL),
                                           sky_python_ast_Load,
                                           start_token->lineno,
                                           start_token->col_offset);
    }

    if ((atom = sky_python_parser_yield_expression(parser, &token)) != NULL) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RPAREN, &token);
        return atom;
    }

    if (!(expr = sky_python_parser_expression(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_RPAREN == token.name) {
        return expr;
    }

    if (SKY_PYTHON_TOKEN_COMMA == token.name) {
        atom = sky_python_parser_expression_list(parser,
                                                 start_token,
                                                 expr,
                                                 sky_python_ast_Tuple_type);
    }
    else if (SKY_PYTHON_TOKEN_FOR == token.name) {
        generators = sky_python_parser_generators(parser);
        atom = sky_python_ast_GeneratorExp_create(expr,
                                                  generators,
                                                  start_token->lineno,
                                                  start_token->col_offset);
    }
    else {
        sky_python_parser_error(parser, &token);
    }

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RPAREN, &token);
    return atom;
}


static sky_object_t
sky_python_parser_list_display(sky_python_parser_t *        parser,
                               const sky_python_token_t *   start_token)
    /* list_display ::= "[" [expression_list | comprehension] "]" */
{
    sky_list_t          generators;
    sky_object_t        atom, expr;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_RBRACKET == token.name) {
        return sky_python_ast_List_create(sky_list_create(NULL),
                                          sky_python_ast_Load,
                                          start_token->lineno,
                                          start_token->col_offset);
    }

    if (!(expr = sky_python_parser_expression(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_COMMA == token.name) {
        atom = sky_python_parser_expression_list(parser,
                                                 start_token,
                                                 expr,
                                                 sky_python_ast_List_type);
    }
    else if (SKY_PYTHON_TOKEN_FOR == token.name) {
        generators = sky_python_parser_generators(parser);
        atom = sky_python_ast_ListComp_create(expr,
                                              generators,
                                              start_token->lineno,
                                              start_token->col_offset);
    }
    else if (SKY_PYTHON_TOKEN_RBRACKET == token.name) {
        return sky_python_ast_List_create(sky_object_build("[O]", expr),
                                          sky_python_ast_Load,
                                          start_token->lineno,
                                          start_token->col_offset);
    }
    else {
        sky_python_parser_error(parser, &token);
    }

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RBRACKET, &token);
    return atom;
}


static sky_python_ast_Bytes_t
sky_python_parser_atom_bytes(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
{
    sky_bytearray_t     buffer;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_BYTES != token.name) {
        sky_python_parser_token_push(parser, &token);
        return sky_python_ast_Bytes_create(start_token->object,
                                           start_token->lineno,
                                           start_token->col_offset);
    }

    buffer = sky_bytearray_createfromobject(start_token->object);
    for (;;) {
        sky_bytearray_extend(buffer, token.object);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_BYTES != token.name) {
            break;
        }
    }
    sky_python_parser_token_push(parser, &token);

    return sky_python_ast_Bytes_create(sky_bytes_createfromobject(buffer),
                                       start_token->lineno,
                                       start_token->col_offset);
}


static sky_python_ast_Str_t
sky_python_parser_atom_string(sky_python_parser_t *     parser,
                              const sky_python_token_t *start_token)
{
    ssize_t                 len;
    sky_python_token_t      token;
    sky_string_builder_t    builder;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_STRING != token.name) {
        sky_python_parser_token_push(parser, &token);
        return sky_python_ast_Str_create(start_token->object,
                                         start_token->lineno,
                                         start_token->col_offset);
    }

    len = sky_object_len(start_token->object) + sky_object_len(token.object);
    builder = sky_string_builder_createwithcapacity(len);
    sky_string_builder_append(builder, start_token->object);
    for (;;) {
        sky_string_builder_append(builder, token.object);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_STRING != token.name) {
            break;
        }
    }
    sky_python_parser_token_push(parser, &token);

    return sky_python_ast_Str_create(sky_string_builder_finalize(builder),
                                     start_token->lineno,
                                     start_token->col_offset);
}


static sky_object_t
sky_python_parser_atom(sky_python_parser_t *        parser,
                       const sky_python_token_t *   start_token)
    /* atom      ::= identifier | literal | enclosure
     * literal   ::= stringliteral | bytesliteral | integer
     *             | floatnumber | imagnumber
     * enclosure ::= parenth_form | list_display | dict_display
     *             | set_display | generator_expression | yield_atom
     */
{
    sky_object_t    value;

    switch (start_token->name) {
        case SKY_PYTHON_TOKEN_NAME:
            return sky_python_ast_Name_create(start_token->object,
                                              sky_python_ast_Load,
                                              start_token->lineno,
                                              start_token->col_offset);

        case SKY_PYTHON_TOKEN_TRUE:
            return sky_python_ast_Name_create(SKY_STRING_LITERAL("True"),
                                              sky_python_ast_Load,
                                              start_token->lineno,
                                              start_token->col_offset);
        case SKY_PYTHON_TOKEN_FALSE:
            return sky_python_ast_Name_create(SKY_STRING_LITERAL("False"),
                                              sky_python_ast_Load,
                                              start_token->lineno,
                                              start_token->col_offset);
        case SKY_PYTHON_TOKEN_NONE:
            return sky_python_ast_Name_create(SKY_STRING_LITERAL("None"),
                                              sky_python_ast_Load,
                                              start_token->lineno,
                                              start_token->col_offset);

        case SKY_PYTHON_TOKEN_STRING:
            return sky_python_parser_atom_string(parser, start_token);
        case SKY_PYTHON_TOKEN_BYTES:
            return sky_python_parser_atom_bytes(parser, start_token);
        case SKY_PYTHON_TOKEN_INTEGER:
        case SKY_PYTHON_TOKEN_DECIMAL:
        case SKY_PYTHON_TOKEN_IMAGINARY:
            return sky_python_ast_Num_create(start_token->object,
                                             start_token->lineno,
                                             start_token->col_offset);
        case SKY_PYTHON_TOKEN_ELLIPSIS:
            return sky_python_ast_Ellipsis_create(start_token->lineno,
                                                  start_token->col_offset);

        case SKY_PYTHON_TOKEN_LPAREN:
            return sky_python_parser_atom_lparen(parser, start_token);
        case SKY_PYTHON_TOKEN_LBRACKET:
            return sky_python_parser_list_display(parser, start_token);
        case SKY_PYTHON_TOKEN_LBRACE:
            return sky_python_parser_atom_lbrace(parser, start_token);

        case SKY_PYTHON_TOKEN_STAR:
            value = sky_python_parser_expression(parser, NULL);
            return sky_python_ast_Starred_create(value,
                                                 sky_python_ast_Load,
                                                 start_token->lineno,
                                                 start_token->col_offset);

        default:
            /* not an atom */
            return NULL;
    }
}


static sky_object_t
sky_python_parser_slice_item(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
    /* slice_item   ::= expression | proper_slice
     * proper_slice ::= [expression] ":" [expression] [ ":" [expression] ]
     */
{
    sky_object_t        lower, step, upper;
    sky_python_token_t  token;

    if (SKY_PYTHON_TOKEN_COLON == start_token->name) {
        lower = NULL;
    }
    else {
        if (!(lower = sky_python_parser_expression(parser, start_token))) {
            return NULL;
        }
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COLON != token.name) {
            sky_python_parser_token_push(parser, &token);
            return lower;
        }
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_COLON == token.name) {
        /* There's no upper expression, but there may be a step expression. */
        upper = NULL;
    }
    else {
        /* If there's no upper expression here, nothing else follows. */
        if (!(upper = sky_python_parser_expression(parser, &token))) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_Slice_create(lower, NULL, NULL);
        }
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COLON != token.name) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_Slice_create(lower, upper, NULL);
        }
    }

    sky_python_parser_token_get(parser, &token);
    if (!(step = sky_python_parser_expression(parser, &token))) {
        sky_python_parser_token_push(parser, &token);
    }

    return sky_python_ast_Slice_create(lower, upper, step);
}


static sky_object_t
sky_python_parser_slice_list(sky_python_parser_t *parser)
    /* slice_list ::= slice_item ("," slice_item)* [","]
     */
{
    ssize_t             i;
    sky_bool_t          subscript;
    sky_list_t          slice_list;
    sky_object_t        slice_item, value;
    sky_python_token_t  start_token, token;

    sky_python_parser_token_get(parser, &start_token);
    if (!(slice_item = sky_python_parser_slice_item(parser, &start_token))) {
        sky_python_parser_error(parser, &start_token);
    }
    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_COMMA != token.name) {
        sky_python_parser_token_push(parser, &token);
        if (!sky_object_isa(slice_item, sky_python_ast_Slice_type)) {
            slice_item = sky_python_ast_Index_create(slice_item);
        }
        return slice_item;
    }

    subscript = !sky_object_isa(slice_item, sky_python_ast_Slice_type);
    slice_list = sky_object_build("[O]", slice_item);
    for (;;) {
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            break;
        }
        sky_python_parser_token_get(parser, &token);
        if (!(slice_item = sky_python_parser_slice_item(parser, &token))) {
            break;
        }
        sky_list_append(slice_list, slice_item);
        if (sky_object_isa(slice_item, sky_python_ast_Slice_type)) {
            subscript = SKY_FALSE;
        }
        sky_python_parser_token_get(parser, &token);
    }
    sky_python_parser_token_push(parser, &token);

    if (subscript) {
        value = sky_python_ast_Tuple_create(slice_list,
                                            sky_python_ast_Load,
                                            start_token.lineno,
                                            start_token.col_offset);
        return sky_python_ast_Index_create(value);
    }

    for (i = sky_list_len(slice_list) - 1; i >= 0; --i) {
        value = sky_list_get(slice_list, i);
        if (!sky_object_isa(value, sky_python_ast_Slice_type)) {
            sky_list_set(slice_list, i, sky_python_ast_Index_create(value));
        }
    }

    return sky_python_ast_ExtSlice_create(slice_list);
}


static sky_object_t
sky_python_parser_primary(sky_python_parser_t *     parser,
                          const sky_python_token_t *start_token)
    /* primary      ::= atom | attributeref | subscription | slicing | call
     * attributeref ::= primary "." identifier
     * subscription ::= primary "[" expression_list "]"
     * slicing      ::= primary "[" slice_list "]"
     * call         ::= primary "(" [argument_list [","] | comprehension] ")"
     */
{
    sky_object_t                    primary, slice;
    sky_python_token_t              token;
    sky_python_parser_arguments_t   arguments;

    if (!(primary = sky_python_parser_atom(parser, start_token))) {
        return NULL;
    }

    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_DOT == token.name) {
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_NAME,
                                           &token);
            primary = sky_python_ast_Attribute_create(primary,
                                                      token.object,
                                                      sky_python_ast_Load,
                                                      start_token->lineno,
                                                      start_token->col_offset);
            continue;
        }
        if (SKY_PYTHON_TOKEN_LPAREN == token.name) {
            sky_python_parser_argument_list(parser, &arguments);
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_RPAREN,
                                           &token);
            primary = sky_python_ast_Call_create(primary,
                                                 arguments.args,
                                                 arguments.keywords,
                                                 arguments.starargs,
                                                 arguments.kwargs,
                                                 start_token->lineno,
                                                 start_token->col_offset);
            continue;
        }
        if (SKY_PYTHON_TOKEN_LBRACKET == token.name) {
            slice = sky_python_parser_slice_list(parser);
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_RBRACKET,
                                           &token);
            primary = sky_python_ast_Subscript_create(primary,
                                                      slice,
                                                      sky_python_ast_Load,
                                                      start_token->lineno,
                                                      start_token->col_offset);
            continue;
        }
        break;
    }
    sky_python_parser_token_push(parser, &token);

    return primary;
}


static sky_object_t
sky_python_parser_dotted_name(sky_python_parser_t *parser)
    /* NAME ('.' NAME)*
     *
     * Parse all NAME and '.' tokens, returning either a Name or an Attribute
     * AST node. A Name will be returned for a single NAME token. An Attribute
     * will be returned for any dotted NAMEs as a tree.
     *
     * For example, "x.y.z" will return:
     * Attribute(value=Attribute(value=Name(id='x'), attr='y'), attr='z')
     */
{
    sky_object_t        name;
    sky_python_token_t  dot_token, token;

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
    name = sky_python_ast_Name_create(token.object,
                                      sky_python_ast_Load,
                                      token.lineno,
                                      token.col_offset);

    sky_python_parser_token_get(parser, &dot_token);
    if (SKY_PYTHON_TOKEN_DOT != dot_token.name) {
        sky_python_parser_token_push(parser, &dot_token);
        return name;
    }

    for (;;) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
        name = sky_python_ast_Attribute_create(name,
                                               token.object,
                                               sky_python_ast_Load,
                                               token.lineno,
                                               token.col_offset);

        sky_python_parser_token_get(parser, &dot_token);
        if (SKY_PYTHON_TOKEN_DOT != dot_token.name) {
            sky_python_parser_token_push(parser, &dot_token);
            break;
        }
    }

    return name;
}


static sky_string_t
sky_python_parser_dotted_name_string(sky_python_parser_t *parser)
    /* NAME ('.' NAME)*
     *
     * Parse and combine all NAME and '.' tokens together into a single string.
     */
{
    sky_python_token_t      dot_token, token;
    sky_string_builder_t    dotted_name;

    /* Expect a NAME token. If this is the only part of the name available
     * (i.e., there is no ('.' NAME)* portion following the first NAME),
     * simply return the first token's object.
     */
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
    sky_python_parser_token_get(parser, &dot_token);
    if (SKY_PYTHON_TOKEN_DOT != dot_token.name) {
        sky_python_parser_token_push(parser, &dot_token);
        return token.object;
    }

    dotted_name = sky_string_builder_create();
    sky_string_builder_append(dotted_name, token.object);

    for (;;) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
        sky_string_builder_appendcodepoint(dotted_name, '.', 1);
        sky_string_builder_append(dotted_name, token.object);

        sky_python_parser_token_get(parser, &dot_token);
        if (SKY_PYTHON_TOKEN_DOT != dot_token.name) {
            sky_python_parser_token_push(parser, &dot_token);
            break;
        }
    }

    return sky_string_builder_finalize(dotted_name);
}


static sky_object_t
sky_python_parser_target(sky_python_parser_t *      parser,
                         const sky_python_token_t * start_token)
    /* target      ::= identifier
     *               | "(" target_list ")"
     *               | "[" target_list "]"
     *               | attributeref
     *               | subscription
     *               | slicing
     *               | "*" target
     */
{
    sky_object_t        value;
    sky_python_token_t  token;

    if (SKY_PYTHON_TOKEN_STAR == start_token->name) {
        sky_python_parser_token_get(parser, &token);
        value = sky_python_parser_target(parser, &token);
        return sky_python_ast_Starred_create(value,
                                             sky_python_ast_Load,
                                             start_token->lineno,
                                             start_token->col_offset);
    }
    if (!(value = sky_python_parser_primary(parser, start_token))) {
        return NULL;
    }

    sky_python_parser_set_context(parser, value, sky_python_ast_Store);
    return value;
}


static sky_list_t
sky_python_parser_target_list(sky_python_parser_t *         parser,
                              sky_python_ast_expr_context_t ctx)
    /* target_list ::= target ("," target)* [","]
     */
{
    sky_list_t          target_list;
    sky_object_t        target;
    sky_python_token_t  token;

    target_list = sky_list_create(NULL);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (!(target = sky_python_parser_target(parser, &token))) {
            if (!sky_list_len(target_list)) {
                sky_python_parser_error(parser, &token);
            }
            break;
        }
        sky_python_parser_set_context(parser, target, ctx);
        sky_list_append(target_list, target);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            break;
        }
    }
    sky_python_parser_token_push(parser, &token);

    return target_list;
}


static sky_object_t
sky_python_parser_target_list_as_expr(sky_python_parser_t *         parser,
                                      sky_python_ast_expr_context_t ctx)
{
    sky_list_t              target_list;
    sky_python_ast_expr_t   expr;

    target_list = sky_python_parser_target_list(parser, ctx);
    expr = sky_list_get(target_list, 0);

    if (sky_list_len(target_list) == 1) {
        return expr;
    }
    return sky_python_ast_Tuple_create(target_list,
                                       ctx,
                                       expr->lineno,
                                       expr->col_offset);
}


static sky_bool_t
sky_python_parser_parameter(sky_python_parser_t *           parser,
                            sky_python_parser_parameter_t * parameter,
                            sky_bool_t                      annotate)
    /* parameter ::= identifier [":" expression] */
{
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_NAME != token.name) {
        sky_python_parser_token_push(parser, &token);
        return SKY_FALSE;
    }
    parameter->name = token.object;
    parameter->name_lineno = token.lineno;
    parameter->name_col_offset = token.col_offset;

    if (!annotate) {
        parameter->annotation = NULL;
    }
    else {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COLON == token.name) {
            parameter->annotation = sky_python_parser_expression(parser, NULL);
        }
        else {
            sky_python_parser_token_push(parser, &token);
            parameter->annotation = NULL;
        }
    }

    return SKY_TRUE;
}


static sky_bool_t
sky_python_parser_defparameter(sky_python_parser_t *            parser,
                               sky_python_parser_parameter_t *  parameter,
                               sky_bool_t                       annotate)
    /* defparameter ::= parameter ["=" expression]
     */
{
    sky_python_token_t  token;

    if (!sky_python_parser_parameter(parser, parameter, annotate)) {
        return SKY_FALSE;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_EQUALS == token.name) {
        parameter->default_value = sky_python_parser_expression(parser, NULL);
    }
    else {
        sky_python_parser_token_push(parser, &token);
        parameter->default_value = NULL;
    }

    return SKY_TRUE;
}


static sky_python_ast_arguments_t
sky_python_parser_parameter_list(sky_python_parser_t *  parser,
                                 sky_bool_t             annotate)
    /* parameter_list ::= (defparameter ",")*
     *                    (  "*" [parameter] ("," defparameter)*
     *                    [, "**" parameter]
     *                    | "**" parameter
     *                    | defparameter [","] )
     */
{
    int                             col_offset, lineno;
    sky_list_t                      args, defaults, kwonlyargs, kw_defaults;
    sky_object_t                    kwargannotation, varargannotation;
    sky_string_t                    kwarg, vararg;
    sky_python_token_t              token;
    sky_python_parser_parameter_t   parameter;

    args = sky_list_create(NULL);
    defaults = sky_list_create(NULL);
    kwonlyargs = sky_list_create(NULL);
    kw_defaults = sky_list_create(NULL);
    kwargannotation = varargannotation = NULL;
    kwarg = vararg = NULL;

    /* Start with positional arguments. Get as many as are there. */
    for (;;) {
        if (!sky_python_parser_defparameter(parser, &parameter, annotate)) {
            break;
        }

        sky_list_append(args,
                        sky_python_ast_arg_create(parameter.name,
                                                  parameter.annotation));
        if (parameter.default_value) {
            sky_list_append(defaults, parameter.default_value);
        }
        else if (!parameter.default_value && sky_list_len(defaults)) {
            /* Once a positional parameter with a default has been specified,
             * all positional parameters following it must also have defaults.
             */
            sky_python_parser_raise(
                    parser,
                    sky_SyntaxError,
                    parameter.name_lineno,
                    parameter.name_col_offset,
                    "non-default parameter follows default parameter");
        }

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            goto finished;
        }
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_STAR == token.name) {
        lineno = token.lineno;
        col_offset = token.col_offset;
        if (sky_python_parser_parameter(parser, &parameter, annotate)) {
            vararg = parameter.name;
            varargannotation = parameter.annotation;
        }
        for (;;) {
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_COMMA != token.name) {
                if (!vararg && !sky_list_len(kwonlyargs)) {
                    sky_python_parser_raise(
                            parser,
                            sky_SyntaxError,
                            lineno,
                            col_offset,
                            "named arguments must follow bare *");
                }
                goto finished;
            }

            if (!sky_python_parser_defparameter(parser, &parameter, annotate)) {
                break;
            }
            sky_list_append(kwonlyargs,
                            sky_python_ast_arg_create(parameter.name,
                                                      parameter.annotation));
            sky_list_append(kw_defaults,
                            (parameter.default_value ? parameter.default_value
                                                     : sky_None));
        }
        if (!vararg && !sky_list_len(kwonlyargs)) {
            sky_python_parser_raise(parser,
                                    sky_SyntaxError,
                                    lineno,
                                    col_offset,
                                    "named arguments must follow bare *");
        }

        sky_python_parser_token_get(parser, &token);
    }

    if (SKY_PYTHON_TOKEN_STAR_STAR == token.name) {
        if (!sky_python_parser_parameter(parser, &parameter, annotate)) {
            sky_python_parser_error(parser, &token);
        }
        kwarg = parameter.name;
        kwargannotation = parameter.annotation;

        sky_python_parser_token_get(parser, &token);
    }

finished:
    sky_python_parser_token_push(parser, &token);
    return sky_python_ast_arguments_create(args,
                                           vararg, varargannotation,
                                           kwonlyargs,
                                           kwarg, kwargannotation,
                                           defaults,
                                           kw_defaults);
}


static sky_object_t
sky_python_parser_unary_expr(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token);

static sky_object_t
sky_python_parser_power_expr(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
{
    sky_object_t        left, right;
    sky_python_token_t  token;

    if (!(left = sky_python_parser_primary(parser, start_token))) {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_STAR_STAR != token.name) {
        sky_python_parser_token_push(parser, &token);
        return left;
    }

    sky_python_parser_token_get(parser, &token);
    if (!(right = sky_python_parser_unary_expr(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    return sky_python_ast_BinOp_create(left,
                                       sky_python_ast_Pow,
                                       right,
                                       start_token->lineno,
                                       start_token->col_offset);
}


static sky_object_t
sky_python_parser_unary_expr(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
{
    sky_object_t                operand;
    sky_python_token_t          token;
    sky_python_ast_unaryop_t    op;

    if (SKY_PYTHON_TOKEN_MINUS == start_token->name) {
        op = sky_python_ast_USub;
    }
    else if (SKY_PYTHON_TOKEN_PLUS == start_token->name) {
        op = sky_python_ast_UAdd;
    }
    else if (SKY_PYTHON_TOKEN_TILDE == start_token->name) {
        op = sky_python_ast_Invert;
    }
    else {
        return sky_python_parser_power_expr(parser, start_token);
    }
    sky_python_parser_token_get(parser, &token);
    if (!(operand = sky_python_parser_unary_expr(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    return sky_python_ast_UnaryOp_create(op,
                                         operand,
                                         start_token->lineno,
                                         start_token->col_offset);
}


static sky_object_t
sky_python_parser_multiplicative_expr(sky_python_parser_t *     parser,
                                      const sky_python_token_t *start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_unary_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_STAR == op_token.name) {
            op = sky_python_ast_Mult;
        }
        else if (SKY_PYTHON_TOKEN_SLASH == op_token.name) {
            op = sky_python_ast_Div;
        }
        else if (SKY_PYTHON_TOKEN_SLASH_SLASH == op_token.name) {
            op = sky_python_ast_FloorDiv;
        }
        else if (SKY_PYTHON_TOKEN_PERCENT == op_token.name) {
            op = sky_python_ast_Mod;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_unary_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_object_t
sky_python_parser_additive_expr(sky_python_parser_t *       parser,
                                const sky_python_token_t *  start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_multiplicative_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_PLUS == op_token.name) {
            op = sky_python_ast_Add;
        }
        else if (SKY_PYTHON_TOKEN_MINUS == op_token.name) {
            op = sky_python_ast_Sub;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_multiplicative_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_object_t
sky_python_parser_shift_expr(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_additive_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_LESS_LESS == op_token.name) {
            op = sky_python_ast_LShift;
        }
        else if (SKY_PYTHON_TOKEN_GREATER_GREATER == op_token.name) {
            op = sky_python_ast_RShift;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_additive_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_object_t
sky_python_parser_and_expr(sky_python_parser_t *        parser,
                           const sky_python_token_t *   start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_shift_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_AMPERSAND == op_token.name) {
            op = sky_python_ast_BitAnd;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_shift_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_object_t
sky_python_parser_xor_expr(sky_python_parser_t *        parser,
                           const sky_python_token_t *   start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_and_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_CARET == op_token.name) {
            op = sky_python_ast_BitXor;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_and_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_object_t
sky_python_parser_or_expr(sky_python_parser_t *     parser,
                          const sky_python_token_t *start_token)
{
    int                         lineno, col_offset;
    sky_object_t                left, right;
    sky_python_token_t          op_token, token;
    sky_python_ast_operator_t   op;

    if (!(left = sky_python_parser_xor_expr(parser, start_token))) {
        return NULL;
    }

    lineno = start_token->lineno;
    col_offset = start_token->col_offset;

    for (;;) {
        sky_python_parser_token_get(parser, &op_token);
        if (SKY_PYTHON_TOKEN_PIPE == op_token.name) {
            op = sky_python_ast_BitOr;
        }
        else {
            sky_python_parser_token_push(parser, &op_token);
            return left;
        }

        sky_python_parser_token_get(parser, &token);
        if (!(right = sky_python_parser_xor_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }

        left = sky_python_ast_BinOp_create(left, op, right, lineno, col_offset);
        lineno = op_token.lineno;
        col_offset = op_token.col_offset;
    }
}


static sky_python_ast_cmpop_t
sky_python_parser_cmpop(sky_python_parser_t *       parser,
                        const sky_python_token_t *  start_token)
{
    sky_python_token_t  token;

    switch (start_token->name) {
        case SKY_PYTHON_TOKEN_EQUALS_EQUALS:
            return sky_python_ast_Eq;
        case SKY_PYTHON_TOKEN_BANG_EQUALS:
            return sky_python_ast_NotEq;
        case SKY_PYTHON_TOKEN_LESS:
            return sky_python_ast_Lt;
        case SKY_PYTHON_TOKEN_LESS_EQUALS:
            return sky_python_ast_LtE;
        case SKY_PYTHON_TOKEN_GREATER:
            return sky_python_ast_Gt;
        case SKY_PYTHON_TOKEN_GREATER_EQUALS:
            return sky_python_ast_GtE;
        case SKY_PYTHON_TOKEN_IS:
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_NOT == token.name) {
                return sky_python_ast_IsNot;
            }
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_Is;
        case SKY_PYTHON_TOKEN_IN:
            return sky_python_ast_In;
        case SKY_PYTHON_TOKEN_NOT:
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_IN == token.name) {
                return sky_python_ast_NotIn;
            }
            sky_python_parser_token_push(parser, &token);
            return NULL;
        default:
            return NULL;
    }
}

static sky_object_t
sky_python_parser_comparison(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
{
    sky_list_t              comparators, ops;
    sky_object_t            expr, left;
    sky_python_token_t      token;
    sky_python_ast_cmpop_t  op;

    if (!(left = sky_python_parser_or_expr(parser, start_token))) {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (!(op = sky_python_parser_cmpop(parser, &token))) {
        sky_python_parser_token_push(parser, &token);
        return left;
    }

    comparators = sky_list_create(NULL);
    ops = sky_list_create(NULL);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (!(expr = sky_python_parser_or_expr(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_list_append(comparators, expr);
        sky_list_append(ops, op);

        sky_python_parser_token_get(parser, &token);
        if (!(op = sky_python_parser_cmpop(parser, &token))) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_Compare_create(left,
                                                 ops,
                                                 comparators,
                                                 start_token->lineno,
                                                 start_token->col_offset);
        }
    }
}


static sky_object_t
sky_python_parser_not_test(sky_python_parser_t *        parser,
                           const sky_python_token_t *   start_token)
{
    sky_object_t        operand;
    sky_python_token_t  token;

    if (SKY_PYTHON_TOKEN_NOT != start_token->name) {
        return sky_python_parser_comparison(parser, start_token);
    }

    sky_python_parser_token_get(parser, &token);
    if (!(operand = sky_python_parser_not_test(parser, &token))) {
        sky_python_parser_error(parser, &token);
    }

    return sky_python_ast_UnaryOp_create(sky_python_ast_Not,
                                         operand,
                                         start_token->lineno,
                                         start_token->col_offset);
}


static sky_object_t
sky_python_parser_and_test(sky_python_parser_t *        parser,
                           const sky_python_token_t *   start_token)
{
    sky_list_t          values;
    sky_object_t        value;
    sky_python_token_t  token;

    if (!(value = sky_python_parser_not_test(parser, start_token))) {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_AND != token.name) {
        sky_python_parser_token_push(parser, &token);
        return value;
    }

    values = sky_object_build("[O]", value);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (!(value = sky_python_parser_not_test(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_list_append(values, value);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_AND != token.name) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_BoolOp_create(sky_python_ast_And,
                                                values,
                                                start_token->lineno,
                                                start_token->col_offset);
        }
    }
}


static sky_object_t
sky_python_parser_or_test(sky_python_parser_t *     parser,
                          const sky_python_token_t *start_token)
{
    sky_list_t          values;
    sky_object_t        value;
    sky_python_token_t  token;

    if (!(value = sky_python_parser_and_test(parser, start_token))) {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_OR != token.name) {
        sky_python_parser_token_push(parser, &token);
        return value;
    }

    values = sky_object_build("[O]", value);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (!(value = sky_python_parser_and_test(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_list_append(values, value);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_OR != token.name) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_BoolOp_create(sky_python_ast_Or,
                                                values,
                                                start_token->lineno,
                                                start_token->col_offset);
        }
    }
}


static sky_object_t
sky_python_parser_expression(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
    /* If start_token is NULL, a new token will be acquired to begin parsing
     * an expression. If it is not an expression starting token, an error will
     * be raised. The expression in this context is REQUIRED.
     *
     * If start_token is NOT NULL, it will be used to begin parsing an
     * expression. If it is not an expression starting token, the return value
     * will be NULL. The expression in this context is OPTIONAL.
     */
{
    sky_bool_t                  required;
    sky_object_t                body, orelse, test;
    sky_python_token_t          first_token, token;
    sky_python_ast_arguments_t  args;

    if (start_token) {
        required = SKY_FALSE;
        first_token = *start_token;
    }
    else {
        required = SKY_TRUE;
        sky_python_parser_token_get(parser, &first_token);
    }

    if (SKY_PYTHON_TOKEN_LAMBDA == first_token.name) {
        args = sky_python_parser_parameter_list(parser, SKY_FALSE);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        body = sky_python_parser_expression(parser, NULL);
        return sky_python_ast_Lambda_create(args,
                                            body,
                                            first_token.lineno,
                                            first_token.col_offset);
    }

    if ((body = sky_python_parser_or_test(parser, &first_token)) != NULL) {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_IF != token.name) {
            sky_python_parser_token_push(parser, &token);
            return body;
        }
        sky_python_parser_token_get(parser, &token);
        if (!(test = sky_python_parser_or_test(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_ELSE, &token);
        orelse = sky_python_parser_expression(parser, NULL);
        return sky_python_ast_IfExp_create(test,
                                           body,
                                           orelse,
                                           first_token.lineno,
                                           first_token.col_offset);
    }
    if (required) {
        sky_python_parser_error(parser, &first_token);
    }
    return NULL;
}


static sky_object_t
sky_python_parser_expression_nocond(sky_python_parser_t *       parser,
                                    const sky_python_token_t *  start_token)
    /* The REQUIRED vs. OPTIONAL behavior for this function is the same as
     * it is for sky_python_parser_expression().
     */
{
    sky_bool_t                  required;
    sky_object_t                body, expr;
    sky_python_token_t          first_token, token;
    sky_python_ast_arguments_t  args;

    if (start_token) {
        required = SKY_FALSE;
        first_token = *start_token;
    }
    else {
        required = SKY_TRUE;
        sky_python_parser_token_get(parser, &first_token);
    }

    if (SKY_PYTHON_TOKEN_LAMBDA == first_token.name) {
        args = sky_python_parser_parameter_list(parser, SKY_FALSE);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        body = sky_python_parser_expression_nocond(parser, NULL);
        return sky_python_ast_Lambda_create(args,
                                            body,
                                            first_token.lineno,
                                            first_token.col_offset);
    }

    if (!(expr = sky_python_parser_or_test(parser, &first_token)) && required) {
        sky_python_parser_error(parser, &first_token);
    }
    return expr;
}


static sky_object_t
sky_python_parser_augassign(const sky_python_token_t *token)
{
    switch (token->name) {
        case SKY_PYTHON_TOKEN_PLUS_EQUALS:
            return sky_python_ast_Add;
        case SKY_PYTHON_TOKEN_MINUS_EQUALS:
            return sky_python_ast_Sub;
        case SKY_PYTHON_TOKEN_STAR_EQUALS:
            return sky_python_ast_Mult;
        case SKY_PYTHON_TOKEN_SLASH_EQUALS:
            return sky_python_ast_Div;
        case SKY_PYTHON_TOKEN_PERCENT_EQUALS:
            return sky_python_ast_Mod;
        case SKY_PYTHON_TOKEN_STAR_STAR_EQUALS:
            return sky_python_ast_Pow;
        case SKY_PYTHON_TOKEN_LESS_LESS_EQUALS:
            return sky_python_ast_LShift;
        case SKY_PYTHON_TOKEN_GREATER_GREATER_EQUALS:
            return sky_python_ast_RShift;
        case SKY_PYTHON_TOKEN_PIPE_EQUALS:
            return sky_python_ast_BitOr;
        case SKY_PYTHON_TOKEN_CARET_EQUALS:
            return sky_python_ast_BitXor;
        case SKY_PYTHON_TOKEN_AMPERSAND_EQUALS:
            return sky_python_ast_BitAnd;
        case SKY_PYTHON_TOKEN_SLASH_SLASH_EQUALS:
            return sky_python_ast_FloorDiv;
        default:
            return NULL;
    }
}


static sky_object_t
sky_python_parser_expression_stmt(sky_python_parser_t *     parser,
                                  const sky_python_token_t *start_token)
    /* expression_stmt           ::= expression_list
     * assignment_stmt           ::= (target_list "=")+
     *                               (expression_list | yield_expression)
     * augmented_assignment_stmt ::= augtarget augop
     *                               (expression_list | yield_expression)
     * augtarget                 ::= identifier | attributeref | subscription
     *                               | slicing
     * augop                     ::= "+=" | "-=" | "*=" | "/=" | "//=" | "%="
     *                               | "**=" | ">>=" | "<<=" | "&=" | "^="
     *                               | "|="
     */
{
    sky_list_t          targets;
    sky_object_t        op, target, value;
    sky_python_token_t  token;

    if (!(value = sky_python_parser_expression_list(parser,
                                                    start_token,
                                                    NULL,
                                                    NULL)))
    {
        return NULL;
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_EQUALS != token.name) {
        if (!(op = sky_python_parser_augassign(&token))) {
            sky_python_parser_token_push(parser, &token);
            return sky_python_ast_Expr_create(value,
                                              start_token->lineno,
                                              start_token->col_offset);
        }

        target = value;
        sky_python_parser_set_context(parser,
                                      target,
                                      sky_python_ast_Store);
        if (sky_object_isa(target, sky_python_ast_List_type) ||
            sky_object_isa(target, sky_python_ast_Tuple_type) ||
            sky_object_isa(target, sky_python_ast_Starred_type))
        {
            sky_python_parser_ast_error(
                    parser,
                    target,
                    "illegal expression for augmented assignment");
        }

        sky_python_parser_token_get(parser, &token);
        if (!(value = sky_python_parser_expression_list(parser,
                                                        &token,
                                                        NULL,
                                                        NULL)))
        {
            if (!(value = sky_python_parser_yield_expression(parser,
                                                             &token)))
            {
                sky_python_parser_error(parser, &token);
            }
        }

        return sky_python_ast_AugAssign_create(target,
                                               op,
                                               value,
                                               start_token->lineno,
                                               start_token->col_offset);
    }

    targets = sky_list_create(NULL);
    for (;;) {
        target = value;
        sky_python_parser_set_context(parser,
                                      target,
                                      sky_python_ast_Store);
        sky_list_append(targets, target);

        sky_python_parser_token_get(parser, &token);
        if (!(value = sky_python_parser_expression_list(parser,
                                                        &token,
                                                        NULL,
                                                        NULL)))
        {
            if (!(value = sky_python_parser_yield_expression(parser,
                                                             &token)))
            {
                sky_python_parser_error(parser, &token);
            }
        }

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_EQUALS != token.name) {
            sky_python_parser_token_push(parser, &token);
            break;
        }

        if (sky_object_isa(value, sky_python_ast_Yield_type)) {
            sky_python_parser_raise(
                    parser,
                    sky_SyntaxError,
                    ((sky_python_ast_Yield_t)value)->lineno,
                    ((sky_python_ast_Yield_t)value)->col_offset,
                    "assignment to yield expression not possible");
        }
    }

    return sky_python_ast_Assign_create(targets,
                                        value,
                                        start_token->lineno,
                                        start_token->col_offset);
}


static sky_python_ast_Assert_t
sky_python_parser_assert_stmt(sky_python_parser_t *parser,
                              int                   lineno,
                              int                   col_offset)
    /* assert_stmt ::= "assert" expression ["," expression]
     *
     * The first token, "assert", is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_object_t        message, expression;
    sky_python_token_t  token;

    /* assert_stmt ::= "assert" expression ["," expression] */
    expression = sky_python_parser_expression(parser, NULL);
    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_COMMA != token.name) {
        sky_python_parser_token_push(parser, &token);
        message = NULL;
    }
    else {
        message = sky_python_parser_expression(parser, NULL);
    }

    return sky_python_ast_Assert_create(expression,
                                        message,
                                        lineno,
                                        col_offset);
}


static sky_python_ast_Delete_t
sky_python_parser_del_stmt(sky_python_parser_t *parser,
                           int                  lineno,
                           int                  col_offset)
    /* del_stmt ::= "del" target_list
     *
     * The first token, "del", is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t  target_list;

    target_list = sky_python_parser_target_list(parser, sky_python_ast_Del);
    return sky_python_ast_Delete_create(target_list, lineno, col_offset);
}


static sky_list_t
sky_python_parser_identifier_list(sky_python_parser_t *parser)
    /* identifier_list: identifier (',' identifier)* */
{
    sky_list_t          identifier_list;
    sky_python_token_t  token;

    identifier_list = sky_list_create(NULL);
    for (;;) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
        sky_list_append(identifier_list, token.object);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            sky_python_parser_token_push(parser, &token);
            break;
        }
    }

    return identifier_list;
}


static sky_python_ast_ImportFrom_t
sky_python_parser_from_stmt(sky_python_parser_t *   parser,
                            int                     lineno,
                            int                     col_offset)
    /* from_stmt       ::= "from" relative_module "import" identifier ["as" name]
     *                   | "from" relative_module "import"
     *                            identifier ["as" name]
     *                            ( "," identifier ["as" name] )* ","]
     *                   | "from" relative_module "import"
     *                            "(" identifier ["as" name]
     *                            ( "," identifier ["as" name] )* [","] ")"
     *                   | "from" module "import" "*"
     * module          ::= (identifier ".")* identifier
     * relative_module ::= "."* module | "."+
     * name            ::= identifier
     *
     * This rule is actually written as part of import_stmt in the language
     * specification; however, it makes more programmatic sense to separate the
     * parsing of "from" and "import".
     *
     * The first token, 'from', has already been consumed before
     * this function is called. The return value will never be NULL.
     */
{
    int                 level;
    sky_bool_t          parens;
    sky_list_t          names;
    sky_string_t        asname, module, name;
    sky_python_token_t  token;

    level = 0;
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_DOT == token.name) {
            ++level;
        }
        else if (SKY_PYTHON_TOKEN_ELLIPSIS == token.name) {
            level += 3;
        }
        else {
            break;
        }
    }

    if (SKY_PYTHON_TOKEN_IMPORT != token.name) {
        sky_python_parser_token_push(parser, &token);
        module = sky_python_parser_dotted_name_string(parser);
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_IMPORT, &token);
    }
    else if (!level) {
        sky_python_parser_error(parser, &token);
    }
    else {
        module = NULL;
    }

    names = sky_list_create(NULL);
    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_STAR == token.name) {
        name = SKY_STRING_LITERAL("*");
        sky_list_append(names, sky_python_ast_alias_create(name, NULL));
    }
    else {
        if (SKY_PYTHON_TOKEN_LPAREN != token.name) {
            parens = SKY_FALSE;
        }
        else {
            parens = SKY_TRUE;
            sky_python_parser_token_get(parser, &token);
        }
        if (SKY_PYTHON_TOKEN_NAME != token.name) {
            sky_python_parser_error(parser, &token);
        }

        for (;;) {
            name = token.object;
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_AS != token.name) {
                asname = NULL;
            }
            else {
                sky_python_parser_expect_token(parser,
                                               SKY_PYTHON_TOKEN_NAME,
                                               &token);
                asname = token.object;
                if (sky_python_parser_is_keyword(asname)) {
                    sky_python_parser_raise(parser,
                                            sky_SyntaxError,
                                            token.lineno,
                                            token.col_offset,
                                            "assignment to keyword");
                }
                sky_python_parser_token_get(parser, &token);
            }
            sky_list_append(names, sky_python_ast_alias_create(name, asname));

            if (SKY_PYTHON_TOKEN_COMMA != token.name) {
                break;
            }

            sky_python_parser_token_get(parser, &token);
            if (parens && SKY_PYTHON_TOKEN_RPAREN == token.name) {
                break;
            }
            if (SKY_PYTHON_TOKEN_NAME != token.name) {
                if (SKY_PYTHON_TOKEN_NEWLINE == token.name) {
                    sky_python_parser_raise(
                            parser,
                            sky_SyntaxError,
                            token.lineno,
                            token.col_offset,
                            "trailing comma not allowed without "
                            "surrounding parentheses");
                }
                sky_python_parser_error(parser, &token);
            }
        }

        if (!parens) {
            sky_python_parser_token_push(parser, &token);
        }
        else if (SKY_PYTHON_TOKEN_RPAREN != token.name) {
            sky_python_parser_error(parser, &token);
        }
    }

    return sky_python_ast_ImportFrom_create(module, names, level,
                                            lineno, col_offset);
}


static sky_python_ast_Import_t
sky_python_parser_import_stmt(sky_python_parser_t * parser,
                              int                   lineno,
                              int                   col_offset)
    /* import_stmt ::= "import" module ["as" name] ( "," module ["as" name] )*
     *
     * The first token, 'import', has already been consumed before
     * this function is called. The return value will never be NULL.
     */
{
    sky_list_t          names;
    sky_string_t        asname, name;
    sky_python_token_t  token;

    names = sky_list_create(NULL);
    for (;;) {
        name = sky_python_parser_dotted_name_string(parser);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_AS != token.name) {
            asname = NULL;
        }
        else {
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_NAME,
                                           &token);
            asname = token.object;
            if (sky_python_parser_is_keyword(asname)) {
                sky_python_parser_raise(parser,
                                        sky_SyntaxError,
                                        token.lineno,
                                        token.col_offset,
                                        "assignment to keyword");
            }
            sky_python_parser_token_get(parser, &token);
        }
        sky_list_append(names, sky_python_ast_alias_create(name, asname));

        if (SKY_PYTHON_TOKEN_COMMA != token.name) {
            sky_python_parser_token_push(parser, &token);
            break;
        }
    }

    return sky_python_ast_Import_create(names, lineno, col_offset);
}


static sky_python_ast_Raise_t
sky_python_parser_raise_stmt(sky_python_parser_t *  parser,
                             int                    lineno,
                             int                    col_offset)
    /* raise_stmt ::= "raise" [expression ["from" expression]]
     *
     * The first token, "raise", has already been consumed before
     * this function is called. The return value will never be NULL.
     */
{
    sky_object_t        cause, exc;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (!(exc = sky_python_parser_expression(parser, &token))) {
        sky_python_parser_token_push(parser, &token);
        cause = exc = NULL;
    }
    else {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_FROM != token.name) {
            sky_python_parser_token_push(parser, &token);
            cause = NULL;
        }
        else {
            cause = sky_python_parser_expression(parser, NULL);
        }
    }

    return sky_python_ast_Raise_create(exc, cause, lineno, col_offset);
}


static sky_python_ast_Return_t
sky_python_parser_return_stmt(sky_python_parser_t * parser,
                              int                   lineno,
                              int                   col_offset)
    /* return_stmt ::= "return" [expression_list]
     *
     * The first token, "return", has already been consumed before
     * this function is called. The return value will never be NULL.
     */
{
    sky_object_t        value;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (!(value = sky_python_parser_expression_list(parser,
                                                    &token,
                                                    NULL,
                                                    NULL)))
    {
        sky_python_parser_token_push(parser, &token);
    }
    return sky_python_ast_Return_create(value, lineno, col_offset);
}


static sky_object_t
sky_python_parser_yield_stmt(sky_python_parser_t *      parser,
                             const sky_python_token_t * start_token)
    /* yield_stmt ::= yield_expression */
{
    sky_object_t    value;

    value = sky_python_parser_yield_expression(parser, start_token);
    return sky_python_ast_Expr_create(value,
                                      start_token->lineno,
                                      start_token->col_offset);
}


static sky_object_t
sky_python_parser_simple_stmt(sky_python_parser_t *     parser,
                              const sky_python_token_t *start_token)
    /* simple_stmt ::= expression_stmt
     *                 | assert_stmt
     *                 | assignment_stmt
     *                 | augmented_assignment_stmt
     *                 | pass_stmt
     *                 | del_stmt
     *                 | return_stmt
     *                 | yield_stmt
     *                 | raise_stmt
     *                 | break_stmt
     *                 | continue_stmt
     *                 | import_stmt
     *                 | global_stmt
     *                 | nonlocal_stmt
     */
{
    int col_offset = start_token->col_offset,
        lineno = start_token->lineno;

    switch (start_token->name) {
        case SKY_PYTHON_TOKEN_BREAK:
            /* break_stmt ::= "break" */
            return sky_python_ast_Break_create(lineno, col_offset);
        case SKY_PYTHON_TOKEN_CONTINUE:
            /* continue_stmt ::= "continue" */
            return sky_python_ast_Continue_create(lineno, col_offset);
        case SKY_PYTHON_TOKEN_PASS:
            /* pass_stmt ::= "pass" */
            return sky_python_ast_Pass_create(lineno, col_offset);

        case SKY_PYTHON_TOKEN_GLOBAL:
            /* global_stmt ::= "global" identifier ("," identifier)* */
            return sky_python_ast_Global_create(
                            sky_python_parser_identifier_list(parser),
                            lineno,
                            col_offset);
        case SKY_PYTHON_TOKEN_NONLOCAL:
            /* nonlocal_stmt ::= "nonlocal" identifier ("," identifier)* */
            return sky_python_ast_Nonlocal_create(
                            sky_python_parser_identifier_list(parser),
                            lineno,
                            col_offset);

        /* "import" and "from" are expressed as a single rule in the
         * language specification grammar, but it makes more sense
         * progammatically break them into two separate handlers since there's
         * really very little shared between them syntactically.
         */
        case SKY_PYTHON_TOKEN_FROM:
            return sky_python_parser_from_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_IMPORT:
            return sky_python_parser_import_stmt(parser, lineno, col_offset);

        case SKY_PYTHON_TOKEN_ASSERT:
            return sky_python_parser_assert_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_DEL:
            return sky_python_parser_del_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_RAISE:
            return sky_python_parser_raise_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_RETURN:
            return sky_python_parser_return_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_YIELD:
            return sky_python_parser_yield_stmt(parser, start_token);

        default:
            break;
    }

    /* This handles expression_stmt, assignment_stmt, and
     * augmented_assignment_stmt. While these are expressed as three rules
     * in the language specification grammar, they're sufficiently ambiguous
     * that it's better to handle them all together as one.
     */
    return sky_python_parser_expression_stmt(parser, start_token);
}


static sky_list_t
sky_python_parser_stmt_list(sky_python_parser_t *       parser,
                            const sky_python_token_t *  start_token)
    /* stmt_list: simple_stmt (';' simple_stmt)* [';']
     */
{
    sky_list_t          stmt_list;
    sky_object_t        stmt;
    sky_python_token_t  token;

    if ((stmt = sky_python_parser_simple_stmt(parser, start_token)) != NULL) {
        stmt_list = sky_list_create(NULL);
        for (;;) {
            sky_list_append(stmt_list, stmt);
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_SEMICOLON != token.name) {
                break;
            }
            sky_python_parser_token_get(parser, &token);
            if (!(stmt = sky_python_parser_simple_stmt(parser, &token))) {
                break;
            }
        }
        sky_python_parser_token_push(parser, &token);
        return stmt_list;
    }

    return NULL;
}


static sky_object_t
sky_python_parser_statement(sky_python_parser_t *       parser,
                            const sky_python_token_t *  start_token);

static sky_list_t
sky_python_parser_suite(sky_python_parser_t *parser)
    /* suite ::= stmt_list NEWLINE | NEWLINE INDENT statement+ DEDENT
     *
     * The return value is never NULL.
     */
{
    sky_list_t          stmt_list, suite;
    sky_object_t        stmt;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_NEWLINE != token.name) {
        /* simple_stmt */
        if (!(stmt_list = sky_python_parser_stmt_list(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_python_parser_expect_token(parser,
                                       SKY_PYTHON_TOKEN_NEWLINE,
                                       &token);
        return stmt_list;
    }

    /* NEWLINE INDENT stmt+ DEDENT */
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_INDENT, &token);

    suite = sky_list_create(NULL);
    sky_python_parser_token_get(parser, &token);
    for (;;) {
        if (!(stmt = sky_python_parser_statement(parser, &token))) {
            sky_python_parser_error(parser, &token);
        }
        sky_python_parser_list_append(suite, stmt);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_DEDENT == token.name) {
            return suite;
        }
    }
}


static sky_python_ast_If_t
sky_python_parser_if_stmt(sky_python_parser_t * parser,
                          int                   lineno,
                          int                   col_offset)
    /* if_stmt ::= "if" expression ":" suite
     *             ( "elif" expression ":" suite )*
     *             ["else" ":" suite]
     *
     * NOTE: this function is also called recursively to handle the "elif"
     *       clauses. Only a single If AST node is ever returned. Any "elif"
     *       clauses are rewritten as "if" statements in the "orelse" clause.
     *
     * The first token, "if" or "elif", is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t          body, orelse;
    sky_object_t        expression;
    sky_python_token_t  token;

    expression = sky_python_parser_expression(parser, NULL);
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
    body = sky_python_parser_suite(parser);

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_ELIF == token.name) {
        orelse = sky_list_create(NULL);
        sky_list_append(orelse,
                        sky_python_parser_if_stmt(parser,
                                                  token.lineno,
                                                  token.col_offset));
    }
    else if (SKY_PYTHON_TOKEN_ELSE == token.name) {
        sky_python_parser_expect_token(parser,
                                       SKY_PYTHON_TOKEN_COLON,
                                       &token);
        orelse = sky_python_parser_suite(parser);
    }
    else {
        sky_python_parser_token_push(parser, &token);
        orelse = sky_list_create(NULL);
    }

    return sky_python_ast_If_create(expression,
                                    body,
                                    orelse,
                                    lineno,
                                    col_offset);
}


static sky_python_ast_While_t
sky_python_parser_while_stmt(sky_python_parser_t *  parser,
                             int                    lineno,
                             int                    col_offset)
    /* while_stmt ::= "while" expression ":" suite
     *                ["else" ":" suite]
     *
     * The first token, "while", is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t          body, orelse;
    sky_object_t        expression;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    expression = sky_python_parser_expression(parser, &token);
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
    body = sky_python_parser_suite(parser);

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_ELSE == token.name) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        orelse = sky_python_parser_suite(parser);
    }
    else {
        sky_python_parser_token_push(parser, &token);
        orelse = sky_list_create(NULL);
    }

    return sky_python_ast_While_create(expression,
                                       body,
                                       orelse,
                                       lineno,
                                       col_offset);
}


static sky_python_ast_For_t
sky_python_parser_for_stmt(sky_python_parser_t *parser,
                           int                  lineno,
                           int                  col_offset)
    /* for_stmt ::= "for" target_list "in" expression_list ":" suite
     *              ["else" ":" suite]
     *
     * The first token, 'for', is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t          body, orelse;
    sky_object_t        iter, target;
    sky_python_token_t  token;

    target = sky_python_parser_target_list_as_expr(parser,
                                                   sky_python_ast_Store);
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_IN, &token);
    sky_python_parser_token_get(parser, &token);
    if (!(iter = sky_python_parser_expression_list(parser,
                                                   &token,
                                                   NULL,
                                                   NULL)))
    {
        sky_python_parser_error(parser, &token);
    }
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
    body = sky_python_parser_suite(parser);

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_ELSE == token.name) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        orelse = sky_python_parser_suite(parser);
    }
    else {
        sky_python_parser_token_push(parser, &token);
        orelse = sky_list_create(NULL);
    }

    return sky_python_ast_For_create(target, iter, body, orelse,
                                     lineno, col_offset);
}


static sky_python_ast_Try_t
sky_python_parser_try_stmt(sky_python_parser_t *parser,
                           int                  lineno,
                           int                  col_offset)
    /* try_stmt  ::= try1_stmt | try2_stmt
     * try1_stmt ::= "try" ":" suite
     *               ("except" [expression ["as" target]] ":" suite)+
     *               ["else" ":" suite]
     *               ["finally" ":" suite]
     * try2_stmt ::= "try" ":" suite
     *               "finally" ":" suite
     *
     * The first token, "try", is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t                      body, finalbody, handlers, orelse;
    sky_object_t                    type;
    sky_string_t                    name;
    sky_python_token_t              token;
    sky_python_ast_ExceptHandler_t  handler;

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
    body = sky_python_parser_suite(parser);

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_FINALLY == token.name) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        finalbody = sky_python_parser_suite(parser);
        return sky_python_ast_Try_create(body,
                                         sky_list_create(NULL),
                                         sky_list_create(NULL),
                                         finalbody,
                                         lineno,
                                         col_offset);
    }
    if (SKY_PYTHON_TOKEN_EXCEPT != token.name) {
        sky_python_parser_error(parser, &token);
    }

    /* excepthandler list, which must have at least one handler */
    handler = NULL;
    finalbody = orelse = NULL;
    handlers = sky_list_create(NULL);
    for (;;) {
        int except_col_offset = token.col_offset,
            except_lineno = token.lineno;

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_COLON == token.name) {
            /* default exception handler - must be last */
            type = NULL;
            name = NULL;
        }
        else {
            if (!(type = sky_python_parser_expression(parser, &token))) {
                sky_python_parser_error(parser, &token);
            }
            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_AS != token.name) {
                name = NULL;
            }
            else {
                sky_python_parser_expect_token(parser,
                                               SKY_PYTHON_TOKEN_NAME,
                                               &token);
                name = token.object;
                if (sky_python_parser_is_keyword(name)) {
                    sky_python_parser_raise(parser,
                                            sky_SyntaxError,
                                            token.lineno,
                                            token.col_offset,
                                            "assignment to keyword");
                }
                sky_python_parser_token_get(parser, &token);
            }
            if (SKY_PYTHON_TOKEN_COLON != token.name) {
                sky_python_parser_error(parser, &token);
            }
        }
        handler = 
                sky_python_ast_ExceptHandler_create(
                        type,
                        name,
                        sky_python_parser_suite(parser),
                        except_lineno,
                        except_col_offset);
        if (handler && !handler->type) {
            sky_python_parser_raise(parser,
                                    sky_SyntaxError,
                                    token.lineno,
                                    token.col_offset,
                                    "default 'except:' must be last");
        }
        sky_list_append(handlers, handler);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_EXCEPT != token.name) {
            break;
        }
    }

    if (SKY_PYTHON_TOKEN_ELSE == token.name) {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        orelse = sky_python_parser_suite(parser);
        sky_python_parser_token_get(parser, &token);
    }

    if (SKY_PYTHON_TOKEN_FINALLY != token.name) {
        sky_python_parser_token_push(parser, &token);
    }
    else {
        sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_COLON, &token);
        finalbody = sky_python_parser_suite(parser);
    }

    return sky_python_ast_Try_create(
                    body,
                    handlers,
                    (orelse ? orelse : sky_list_create(NULL)),
                    (finalbody ? finalbody : sky_list_create(NULL)),
                    lineno,
                    col_offset);
}


static sky_python_ast_With_t
sky_python_parser_with_stmt(sky_python_parser_t *   parser,
                            int                     lineno,
                            int                     col_offset)
    /* with_stmt ::= "with" with_item ("," with_item)* ":" suite
     * with_item ::= expression ["as" target]
     *
     * The first token, 'with', is already consumed before
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t          body, items;
    sky_object_t        context_expr, optional_vars;
    sky_python_token_t  token;

    items = sky_list_create(NULL);
    for (;;) {
        context_expr = sky_python_parser_expression(parser, NULL);
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_AS != token.name) {
            optional_vars = NULL;
        }
        else {
            sky_python_parser_token_get(parser, &token);
            if (!(optional_vars = sky_python_parser_target(parser, &token))) {
                sky_python_parser_error(parser, &token);
            }
            sky_python_parser_set_context(parser,
                                          optional_vars,
                                          sky_python_ast_Store);
            sky_python_parser_token_get(parser, &token);
        }
        sky_list_append(items,
                        sky_python_ast_withitem_create(context_expr,
                                                       optional_vars));
        if (SKY_PYTHON_TOKEN_COMMA == token.name) {
            continue;
        }
        break;
    }
    if (SKY_PYTHON_TOKEN_COLON != token.name) {
        sky_python_parser_error(parser, &token);
    }
    body = sky_python_parser_suite(parser);

    return sky_python_ast_With_create(items, body, lineno, col_offset);
}


static sky_python_ast_FunctionDef_t
sky_python_parser_funcdef(sky_python_parser_t * parser,
                          sky_list_t            decorator_list,
                          int                   lineno,
                          int                   col_offset)
    /* funcdef ::= [decorators] "def" funcname "(" [parameter_list] ")"
     *             ["->" expression] ":" suite
     * 
     * Optional decorators are handled by the compound_stmt rule.
     *
     * The first token, 'def', is already consumed before this
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t                  body;
    sky_object_t                returns;
    sky_string_t                name;
    sky_python_token_t          token;
    sky_python_ast_arguments_t  args;

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
    name = token.object;
    if (sky_python_parser_is_keyword(name)) {
        sky_python_parser_raise(parser,
                                sky_SyntaxError,
                                token.lineno,
                                token.col_offset,
                                "assignment to keyword");
    }

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_LPAREN, &token);
    args = sky_python_parser_parameter_list(parser, SKY_TRUE);
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_RPAREN, &token);

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_MINUS_GREATER != token.name) {
        returns = NULL;
    }
    else {
        returns = sky_python_parser_expression(parser, NULL);
        sky_python_parser_token_get(parser, &token);
    }
    if (SKY_PYTHON_TOKEN_COLON != token.name) {
        sky_python_parser_error(parser, &token);
    }

    body = sky_python_parser_suite(parser);

    return sky_python_ast_FunctionDef_create(name,
                                             args,
                                             body,
                                             decorator_list,
                                             returns,
                                             lineno,
                                             col_offset);
}


static sky_python_ast_ClassDef_t
sky_python_parser_classdef(sky_python_parser_t *parser,
                           sky_list_t           decorator_list,
                           int                  lineno,
                           int                  col_offset)
    /* classdef    ::= [decorators] "class" classname [inheritance] ":" suite
     * inheritance ::= "(" [argument_list [","] | comprehension] ")"
     * classname   ::= identifier
     *
     * Optional decorators are handled by the compound_stmt rule.
     *
     * The first token, 'class', is already consumed before this
     * this function is called. The return value is never NULL.
     */
{
    sky_list_t                      body;
    sky_string_t                    name;
    sky_python_token_t              token;
    sky_python_parser_arguments_t   arguments;

    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NAME, &token);
    name = token.object;
    if (sky_python_parser_is_keyword(name)) {
        sky_python_parser_raise(parser,
                                sky_SyntaxError,
                                token.lineno,
                                token.col_offset,
                                "assignment to keyword");
    }

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_LPAREN != token.name) {
        if (SKY_PYTHON_TOKEN_COLON != token.name) {
            sky_python_parser_error(parser, &token);
        }
        arguments.args = sky_list_create(NULL);
        arguments.keywords = sky_list_create(NULL);
        arguments.starargs = arguments.kwargs = NULL;
    }
    else {
        sky_python_parser_argument_list(parser, &arguments);
        sky_python_parser_expect_token(parser,
                                       SKY_PYTHON_TOKEN_RPAREN,
                                       &token);
        sky_python_parser_expect_token(parser,
                                       SKY_PYTHON_TOKEN_COLON,
                                       &token);
    }
    body = sky_python_parser_suite(parser);

    return sky_python_ast_ClassDef_create(name,
                                          arguments.args,
                                          arguments.keywords,
                                          arguments.starargs,
                                          arguments.kwargs,
                                          body,
                                          decorator_list,
                                          lineno,
                                          col_offset);
}


static sky_object_t
sky_python_parser_decorators(sky_python_parser_t *parser)
    /* decorators  ::= decorator+
     * decorator   ::= "@" dotted_name ["(" argument_list [","]] ")"] NEWLINE
     * dotted_name ::= identifier ("." identifier)*
     *
     * Note that the language specification says "parameter_list" rather than
     * "argument_list", but that is an error.
     *
     * The first token, '@', is already consumed before this
     * this function is called. The return value is never NULL,
     * and will be either a FunctionDef or a ClassDef AST node.
     */
{
    sky_list_t          decorators;
    sky_object_t        dotted_name;
    sky_python_token_t  token;

    decorators = sky_list_create(NULL);
    for (;;) {
        dotted_name = sky_python_parser_dotted_name(parser);

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_LPAREN != token.name) {
            if (SKY_PYTHON_TOKEN_NEWLINE != token.name) {
                sky_python_parser_error(parser, &token);
            }
            sky_list_append(decorators, dotted_name);
        }
        else {
            int lineno = token.lineno,
                col_offset = token.col_offset;

            sky_python_parser_arguments_t   arguments;

            sky_python_parser_argument_list(parser, &arguments);
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_RPAREN,
                                           &token);
            sky_python_parser_expect_token(parser,
                                           SKY_PYTHON_TOKEN_NEWLINE,
                                           &token);

            sky_list_append(
                    decorators, 
                    sky_python_ast_Call_create(dotted_name,
                                               arguments.args,
                                               arguments.keywords,
                                               arguments.starargs,
                                               arguments.kwargs,
                                               lineno,
                                               col_offset));
        }

        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_AT != token.name) {
            break;
        }
    }

    if (SKY_PYTHON_TOKEN_DEF == token.name) {
        return sky_python_parser_funcdef(parser,
                                         decorators,
                                         token.lineno,
                                         token.col_offset);
    }
    if (SKY_PYTHON_TOKEN_CLASS == token.name) {
        return sky_python_parser_classdef(parser,
                                          decorators,
                                          token.lineno,
                                          token.col_offset);
    }
    sky_python_parser_error(parser, &token);
}


static sky_object_t
sky_python_parser_compound_stmt(sky_python_parser_t *       parser,
                                const sky_python_token_t *  start_token)
    /* compound_stmt ::= if_stmt
     *                   | while_stmt
     *                   | for_stmt
     *                   | try_stmt
     *                   | with_stmt
     *                   | funcdef
     *                   | classdef
     *
     * Note that optional decorators preceding funcdef or classdef are handled
     * here, even though the grammar is not specified that way. Since it's
     * impossible to know whether a function or a class definition follows the
     * decorators, parsing would possibly have to occur twice, and the token
     * push-back stack would have to be enormous. The implementation parses the
     * decorators once and only once, without any need for the token push-back
     * stack to be able to hold any more than a single token.
     */
{
    int col_offset = start_token->col_offset,
        lineno = start_token->lineno;

    switch (start_token->name) {
        case SKY_PYTHON_TOKEN_IF:
            return sky_python_parser_if_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_WHILE:
            return sky_python_parser_while_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_FOR:
            return sky_python_parser_for_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_TRY:
            return sky_python_parser_try_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_WITH:
            return sky_python_parser_with_stmt(parser, lineno, col_offset);
        case SKY_PYTHON_TOKEN_DEF:
            return sky_python_parser_funcdef(parser,
                                             sky_list_create(NULL),
                                             lineno,
                                             col_offset);
        case SKY_PYTHON_TOKEN_CLASS:
            return sky_python_parser_classdef(parser,
                                              sky_list_create(NULL),
                                              lineno,
                                              col_offset);
        case SKY_PYTHON_TOKEN_AT:
            return sky_python_parser_decorators(parser);
        default:
            /* not a compound statement */
            return NULL;
    }
}


static sky_object_t
sky_python_parser_statement(sky_python_parser_t *       parser,
                            const sky_python_token_t *  start_token)
    /* statement ::= stmt_list NEWLINE | compound_statement
     */
{
    sky_list_t          stmt_list;
    sky_object_t        stmt;
    sky_python_token_t  token;

    if ((stmt = sky_python_parser_compound_stmt(parser, start_token)) != NULL) {
        return stmt;
    }

    if (!(stmt_list = sky_python_parser_stmt_list(parser, start_token))) {
        return NULL;
    }
    sky_python_parser_expect_token(parser, SKY_PYTHON_TOKEN_NEWLINE, &token);

    return stmt_list;
}


sky_python_ast_Expression_t
sky_python_parser_eval_input(sky_python_parser_t *parser)
    /* eval_input ::= expression_list NEWLINE*
     */
{
    sky_object_t        body;
    sky_python_token_t  token;
    sky_unicode_char_t  cp;

    /* Skip over leading whitespace */
    do {
        cp = sky_python_lexer_getc(&(parser->lexer));
    } while (' ' == cp || '\t' == cp || '\014' == cp || '\n' == cp);
    sky_python_lexer_ungetc(&(parser->lexer));

    sky_python_parser_token_get(parser, &token);
    if (!(body = sky_python_parser_expression_list(parser,
                                                   &token,
                                                   NULL,
                                                   NULL)))
    {
        sky_python_parser_error(parser, &token);
    }
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if (SKY_PYTHON_TOKEN_ENDMARKER == token.name) {
            break;
        }
        if (SKY_PYTHON_TOKEN_NEWLINE == token.name) {
            continue;
        }
        sky_python_parser_error(parser, &token);
    }
    return sky_python_ast_Expression_create(body);
}


sky_python_ast_Module_t
sky_python_parser_file_input(sky_python_parser_t *parser)
    /* file_input ::= (NEWLINE | statement)*
     */
{
    sky_list_t          body;
    sky_object_t        stmt;
    sky_python_token_t  token;

    body = sky_list_create(NULL);
    for (;;) {
        sky_python_parser_token_get(parser, &token);
        if ((stmt = sky_python_parser_statement(parser, &token)) != NULL) {
            sky_python_parser_list_append(body, stmt);
            continue;
        }
        if (SKY_PYTHON_TOKEN_ENDMARKER == token.name) {
            break;
        }
        if (SKY_PYTHON_TOKEN_NEWLINE == token.name) {
            continue;
        }
        sky_python_parser_error(parser, &token);
    }
    if (SKY_PYTHON_TOKEN_ENDMARKER == token.name) {
        return sky_python_ast_Module_create(body);
    }
    sky_python_parser_error(parser, &token);
}


sky_python_ast_Interactive_t
sky_python_parser_interactive_input(sky_python_parser_t *parser)
    /* interactive_input ::= [stmt_list] NEWLINE | compound_stmt NEWLINE
     */
{
    sky_list_t          body, stmt_list;
    sky_object_t        stmt;
    sky_python_token_t  token;

    sky_python_parser_token_get(parser, &token);
    if (SKY_PYTHON_TOKEN_ENDMARKER == token.name) {
        return NULL;
    }

    body = sky_list_create(NULL);
    if (SKY_PYTHON_TOKEN_NEWLINE != token.name) {
        if ((stmt = sky_python_parser_compound_stmt(parser, &token)) != NULL) {
            sky_list_append(body, stmt);
        }
        else {
            stmt_list = sky_python_parser_stmt_list(parser, &token);
            if (stmt_list) {
                sky_list_extend(body, stmt_list);
            }
        }
        sky_python_parser_token_get(parser, &token);
        if (parser->lexer.flags & SKY_PYTHON_LEXER_FLAG_INTERACTIVE) {
            if (SKY_PYTHON_TOKEN_NEWLINE != token.name &&
                SKY_PYTHON_TOKEN_ENDMARKER != token.name)
            {
                sky_python_parser_error(parser, &token);
            }
        }
        else if (SKY_PYTHON_TOKEN_NEWLINE == token.name) {
            int lineno = token.lineno,
                col_offset = token.col_offset;

            sky_python_parser_token_get(parser, &token);
            if (SKY_PYTHON_TOKEN_ENDMARKER != token.name) {
                sky_python_parser_raise(
                        parser,
                        sky_SyntaxError,
                        lineno,
                        col_offset,
                        "multiple statements found while compiling a "
                        "single statement");
            }
        }
        else if (SKY_PYTHON_TOKEN_ENDMARKER != token.name) {
            sky_python_parser_error(parser, &token);
        }
    }
    return sky_python_ast_Interactive_create(body);
}


void
sky_python_parser_cleanup(sky_python_parser_t *parser)
{
    sky_python_lexer_cleanup(&(parser->lexer));
}


sky_python_parser_t *
sky_python_parser_create(sky_object_t   source,
                         sky_string_t   encoding,
                         sky_string_t   filename,
                         sky_string_t   ps1,
                         sky_string_t   ps2,
                         unsigned int   flags)
{
    sky_python_parser_t *parser;

    SKY_ASSET_BLOCK_BEGIN {
        parser = sky_calloc(1, sizeof(sky_python_parser_t));
        sky_asset_save(parser,
                       (sky_free_t)sky_python_parser_destroy,
                       SKY_ASSET_CLEANUP_ON_ERROR);
        sky_python_parser_init(parser,
                               source,
                               encoding,
                               filename,
                               ps1,
                               ps2,
                               flags);
    } SKY_ASSET_BLOCK_END;

    return parser;
}


void
sky_python_parser_destroy(sky_python_parser_t *parser)
{
    if (parser) {
        sky_python_parser_cleanup(parser);
        sky_free(parser);
    }
}


void
sky_python_parser_init(sky_python_parser_t *parser,
                       sky_object_t         source,
                       sky_string_t         encoding,
                       sky_string_t         filename,
                       sky_string_t         ps1,
                       sky_string_t         ps2,
                       unsigned int         flags)
{
    memset(parser, 0, sizeof(sky_python_parser_t));

    sky_python_lexer_init(&(parser->lexer),
                          source,
                          encoding,
                          filename,
                          ps1,
                          ps2,
                          flags);
    parser->token_sp = &(parser->token_stack[0]);
    parser->flags = flags;
}
