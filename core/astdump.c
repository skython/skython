#include "skython.h"

#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_python_parser.h"
#include "sky_python_compiler.h"

#include <sys/stat.h>


static void
dump_code(sky_code_t code)
{
    sky_object_t    object;
    sky_code_data_t *code_data;

    sky_code_dump(code);

    code_data = sky_code_data(code);
    SKY_SEQUENCE_FOREACH(code_data->co_consts, object) {
        if (sky_object_isa(object, sky_code_type)) {
            dump_code(object);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static void
close_fd(void *arg)
{
    close(*(int *)arg);
}

static sky_bytes_t
read_source(const char *filename)
{
    int         fd;
    char        *bytes;
    ssize_t     n, nread = 0;
    sky_bytes_t source;
    struct stat st;

    SKY_ASSET_BLOCK_BEGIN {
        if ((fd = open(filename, O_RDONLY)) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_asset_save(&fd, close_fd, SKY_ASSET_CLEANUP_ALWAYS);

        if (fstat(fd, &st) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        bytes = sky_asset_malloc(st.st_size + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        while (nread < st.st_size) {
            n = read(fd, bytes + nread, st.st_size - nread);
            if (n < 0) {
                if (EINTR == errno) {
                    continue;
                }
                sky_error_raise_errno(sky_OSError, errno);
            }
            nread += n;
        }

        bytes[st.st_size] = 0;
        source = sky_bytes_createwithbytes(bytes, st.st_size, sky_free);
    } SKY_ASSET_BLOCK_END;

    return source;
}


int
main(int argc, char *argv[])
{
    int                     i;
    sky_bool_t              annotate_fields, compile, include_attributes,
                            lex_only;
    sky_code_t              code;
    sky_object_t            source;
    sky_string_t            dump, filename;
    sky_python_token_t      token;
    sky_python_parser_t     parser;
    sky_python_ast_Module_t ast;

    sky_error_validate(sky_library_initialize(argv[0], argc, argv,
                                              SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES));

    annotate_fields = SKY_TRUE;
    include_attributes = SKY_FALSE;
    lex_only = SKY_FALSE;

    for (i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-a")) {
            annotate_fields = !annotate_fields;
            continue;
        }
        if (!strcmp(argv[i], "-i")) {
            include_attributes = !include_attributes;
            continue;
        }
        if (!strcmp(argv[i], "-c")) {
            compile = !compile;
            continue;
        }
        if (!strcmp(argv[i], "-l")) {
            lex_only = !lex_only;
            continue;
        }

        SKY_ASSET_BLOCK_BEGIN {
            if (!strcmp(argv[i], "-f")) {
                if (++i >= argc) {
                    sky_format_fprintf(
                            stderr,
                            "%s: missing required argument for '-f'",
                            argv[0]);
                }
                filename = sky_string_createfrombytes(argv[i], strlen(argv[i]),
                                                      NULL, NULL);
                source = read_source(argv[i]);
            }
            else {
                filename = SKY_STRING_LITERAL("<string>");
                source = sky_string_createfrombytes(argv[i],
                                                    strlen(argv[i]),
                                                    NULL,
                                                    NULL);
            }

            sky_python_parser_init(&parser,
                                   source,
                                   NULL,
                                   filename,
                                   NULL,
                                   NULL,
                                   0);
            sky_asset_save(&parser,
                           (sky_free_t)sky_python_parser_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);

            if (lex_only) {
                while (sky_python_lexer_token(&(parser.lexer), &token)) {
                    sky_format_fprintf(stdout,
                                       "%-9s : %4d col %4d : %@\n",
                                       sky_python_token_name(token.name),
                                       token.lineno,
                                       token.col_offset,
                                       token.object);
                }
            }
            else {
                ast = sky_python_parser_file_input(&parser);
                if (compile) {
                    code = sky_python_compiler_compile(
                                    ast,
                                    filename,
                                    SKY_PYTHON_COMPILER_MODE_FILE,
                                    0,
                                    0);
                    dump_code(code);
                }
                else {
                    dump = sky_python_ast_dump(ast,
                                               annotate_fields,
                                               include_attributes,
                                               SKY_TRUE);
                    sky_format_fprintf(stdout, "%@\n", dump);
                }
            }
        } SKY_ASSET_BLOCK_END;
    }

    sky_library_finalize();

    return 0;
}
