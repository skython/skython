/** @file
  * @defgroup sky_format Formatted Output
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FORMAT_H__
#define __SKYTHON_CORE_SKY_FORMAT_H__ 1


#include "sky_base.h"
#include "sky_unicode.h"


SKY_CDECLS_BEGIN


/** A set of constants that control the behavior of a format specification
  * rendering.
  */
typedef enum sky_format_flag_e {
    SKY_FORMAT_FLAG_ALIGN_LEFT              =   0x0001,
    SKY_FORMAT_FLAG_ALIGN_RIGHT             =   0x0002,
    SKY_FORMAT_FLAG_ALIGN_CENTER            =   0x0003,
    SKY_FORMAT_FLAG_ALIGN_PADDED            =   0x0004,
    SKY_FORMAT_FLAG_ALIGN_MASK              =   0x000F,

    SKY_FORMAT_FLAG_SIGN_MINUS              =   0x0010,
    SKY_FORMAT_FLAG_SIGN_PLUS               =   0x0020,
    SKY_FORMAT_FLAG_SIGN_SPACE              =   0x0030,
    SKY_FORMAT_FLAG_SIGN_MASK               =   0x00F0,

    SKY_FORMAT_FLAG_ALTERNATE_FORM          =   0x0100,
    SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS    =   0x0200,
    SKY_FORMAT_FLAG_ZERO_PAD                =   0x0400,

    SKY_FORMAT_FLAG_SPECIFIED_FILL          =   0x01000,
    SKY_FORMAT_FLAG_SPECIFIED_ALIGN         =   0x02000,
    SKY_FORMAT_FLAG_SPECIFIED_SIGN          =   0x04000,
    SKY_FORMAT_FLAG_SPECIFIED_WIDTH         =   0x08000,
    SKY_FORMAT_FLAG_SPECIFIED_PRECISION     =   0x10000,
} sky_format_flag_t;


/** A structure specifying how a string of data is to be formatted. **/
typedef struct sky_format_specification_s {
    /** The character to use for filling empty space when needed. **/
    sky_unicode_char_t                  fill;
    /** A character representing type-specific formatting information. **/
    sky_unicode_char_t                  type;
    /** The width to pad to if @c SKY_FORMAT_FLAG_SPECIFIED_WIDTH is set. **/
    ssize_t                             width;
    /** The precision to use if @c SKY_FORMAT_FLAG_SPECIFIED_PRECISION is set. **/
    ssize_t                             precision;
    /** A set of flags from @c sky_format_flag_t. **/
    uint32_t                            flags;
} sky_format_specification_t;


/** Return the alignment for a format specification. **/
SKY_EXTERN_INLINE uint32_t
sky_format_alignment(const sky_format_specification_t *spec)
{
    return (spec->flags & SKY_FORMAT_FLAG_ALIGN_MASK);
}

/** Return the sign for a format specificaiton. **/
SKY_EXTERN_INLINE uint32_t
sky_format_sign(const sky_format_specification_t *spec)
{
    return (spec->flags & SKY_FORMAT_FLAG_SIGN_MASK);
}


/** A function pointer to be called with produced output.
  * This function will be called as many times as is necessary to output
  * portions of the formatted output.
  *
  * @param[in]  bytes   a buffer of bytes to be output.
  * @param[in]  nbytes  the number of bytes contained in the bytes buffer.
  * @param[in]  width   the width of characters present in @a bytes.
  * @param[in]  arg     an arbitrary pointer passed through.
  */
typedef void
        (*sky_format_output_t)(const void * bytes,
                               ssize_t      nbytes,
                               size_t       width,
                               void *       arg);


/** Format output according to a format specification.
  * The format specification is parsed, and the output function is called as
  * necessary to produce the formatted output. The format specification is
  * similar to C99 printf-style formatting with the major difference being that
  * %n is not supported at all.
  *
  * @param[in]  output  the function to call to produce output.
  * @param[in]  arg     an arbitrary pointer to pass through to the output
  *                     function.
  * @param[in]  fmt     the format specification.
  * @return     the total number of bytes reproduced after formatting.
  */
SKY_EXTERN size_t
sky_format_output(sky_format_output_t output, void *arg, const char *fmt, ...);

/** Format output according to a format specification.
  * The format specification is parsed, and the output function is called as
  * necessary to produce the formatted output. The format specification is
  * similar to C99 printf-style formatting with the major difference being that
  * %n is not supported at all.
  *
  * @param[in]  output  the function to call to with produced output.
  * @param[in]  arg     an arbitrary pointer to pass through to the outputi
  *                     function.
  * @param[in]  fmt     the format specification.
  * @param[in]  ap      variable arguments from va_start().
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_voutput(sky_format_output_t  output,
                   void *               arg,
                   const char *         fmt,
                   va_list              ap);


/** Format output according to a format specification to a dynamically
  * allocated output buffer.
  * The output buffer is dynamically allocated as needed via sky_malloc(). It
  * is the caller's responsibility to free the buffer when it is no longer
  * needed via sky_free().
  *
  * @param[out] buffer  the dynamically allocated buffer that will contain
  *                     the formatted output.
  * @param[in]  fmt     the format specification.
  * @return     the total number of bytes produced after formatting, not
  *             including the nul-byte terminator.
  */
SKY_EXTERN size_t
sky_format_asprintf(char **buffer, const char *fmt, ...);

/** Format output according to a format specification to a dynamically
  * allocated output buffer.
  * The output buffer is dynamically allocated as needed via sky_malloc(). It
  * is the caller's responsibility to free the buffer when it is no longer
  * needed via sky_free().
  *
  * @param[out] buffer  the dynamically allocated buffer that will contain
  *                     the formatted output.
  * @param[in]  fmt     the format specification.
  * @param[in]  ap      variable arguments from va_start().
  * @return     the total number of bytes produced after formatting, not
  *             including the nul-byte terminator.
  */
SKY_EXTERN size_t
sky_format_vasprintf(char **buffer, const char *fmt, va_list ap);

/** Format output according to a format specification to the debugging output
  * destination.
  * On Windows, the debugging output destination is OutputDebugString(). For
  * all other platforms, the debugging output destination is @c stderr. This
  * function does nothing in a non-debug build.
  *
  * @param[in]  fmt     the format specification
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_dprintf(const char *fmt, ...);

/** Format output according to a format specification to the debugging output
  * destination.
  * On Windows, the debugging output destination is OutputDebugString(). For
  * all other platforms, the debugging output destination is @c stderr. This
  * function does nothing in a non-debug build.
  *
  * @param[in]  fmt     the format specification
  * @param[in]  ap      variable arguments from va_start().
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_vdprintf(const char *fmt, va_list ap);

/** Format output according to a format specification to a standard C file
  * stream.
  *
  * @param[in]  stream  the file stream to write output to.
  * @param[in]  fmt     the format specification.
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_fprintf(FILE *stream, const char *fmt, ...);

/** Format output according to a format specification to a standard C file
  * stream.
  *
  * @param[in]  stream  the file stream to write output to.
  * @param[in]  fmt     the format specification.
  * @param[in]  ap      variable arguments from va_start().
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_vfprintf(FILE *stream, const char *fmt, va_list ap);

/** Format output according to a format specification to a pre-allocated output
  * buffer.
  *
  * @param[in]  buffer          the buffer into with formatted output will be
  *                             placed.
  * @param[in]  buffer_length   the size of the output buffer, to include the
  *                             c-style string terminator (0 byte).
  * @param[in]  fmt             the format specification
  * @return     the total number of bytes produced after formatting, not
  *             including the nul-byte terminator.
  */
SKY_EXTERN size_t
sky_format_sprintf(char * restrict          buffer,
                   size_t                   buffer_length,
                   const char * restrict    fmt, ...);

/** Format output according to a format specification to a pre-allocated output
  * buffer.
  *
  * @param[in]  buffer          the buffer into with formatted output will be
  *                             placed.
  * @param[in]  buffer_length   the size of the output buffer, to include the
  *                             c-style string terminator (0 byte).
  * @param[in]  fmt             the format specification
  * @param[in]  ap              variable arguments from va_start().
  * @return     the total number of bytes produced after formatting, not
  *             including the nul-byte terminator.
  */
SKY_EXTERN size_t
sky_format_vsprintf(char * restrict         buffer,
                    size_t                  buffer_length,
                    const char * restrict   fmt,
                    va_list                 ap);


/** Format a string representation of a number using parameters for
  * non-monetary values.
  *
  * @param[in]  output  the function to call to produce output.
  * @param[in]  arg     an arbitrary pointer to pass through to the output
  *                     function.
  * @param[in]  bytes   the bytes to be formatted. Each character is assumed
  *                     to be a single byte wide.
  * @param[in]  nbytes  the number of bytes present in @a bytes.
  * @param[in]  minimum_width   the minimum width of the output. 0's will be
  *                             add to the start of the output (along with
  *                             thousands separators, if appropriate) to meet
  *                             this width requirement.
  * @param[in]  decimal_point   a string to use for decimal points. May be
  *                             @c NULL to indicate that no decimal point is
  *                             present in the string (integers).
  * @param[in]  thousands_sep   the separator between groups of digits before
  *                             the decimal point. May be @c NULL if there is
  *                             to be no separator inserted.
  * @param[in]  grouping        the sizes of the groups of digits. This is the
  *                             same as the @c grouping field in the structure
  *                             returned by localeconv().
  * @return     the total number of bytes produced after formatting.
  */
SKY_EXTERN size_t
sky_format_number(sky_format_output_t   output,
                  void *                arg,
                  const char *          bytes,
                  size_t                nbytes,
                  ssize_t               minimum_width,
                  const char *          decimal_point,
                  const char *          thousands_sep,
                  const char *          grouping);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FORMAT_H__ */

/** @} **/
