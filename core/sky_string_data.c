#include "sky_private.h"
#include "sky_string_private.h"


sky_string_data_t *
sky_string_data(sky_object_t string, sky_string_data_t *tagged_data)
{
    sky_unicode_char_t  cp;
#if defined(SKY_ARCH_64BIT)
    sky_unicode_char_t  cp2, max_cp;
#endif

    if (sky_object_istagged(string)) {
        switch (SKY_OBJECT_TAG(string)) {
            case 0x3:
                cp = ((uintptr_t)string >> 4) & 0x1FFFFF;
                tagged_data->hash = 0;
                tagged_data->len = 1;
                tagged_data->highest_codepoint = 0;
                tagged_data->any_ctype_flags = 0;
                tagged_data->all_ctype_flags = 0;
                tagged_data->flags = 0;
                tagged_data->u.free = NULL;
                tagged_data->data.ascii = tagged_data->u.small_data;
                if (cp < 0x100) {
                    tagged_data->data.latin1[0] = cp;
                    tagged_data->data.latin1[1] = 0;
                    sky_string_data_update_1(tagged_data, cp);
                }
                else if (cp < 0x10000) {
                    tagged_data->data.ucs2[0] = cp;
                    tagged_data->data.ucs2[1] = 0;
                    sky_string_data_update(tagged_data, cp);
                }
                else {
                    tagged_data->data.ucs4[0] = cp;
                    tagged_data->data.ucs4[1] = 0;
                    sky_string_data_update(tagged_data, cp);
                }
                return tagged_data;
#if defined(SKY_ARCH_64BIT)
            case 0x4:
                cp = ((uintptr_t)string >> 25) & 0x1FFFFF;
                cp2 = ((uintptr_t)string >> 4) & 0x1FFFFF;
                max_cp = SKY_MAX(cp, cp2);
                tagged_data->hash = 0;
                tagged_data->len = 2;
                tagged_data->highest_codepoint = 0;
                tagged_data->any_ctype_flags = 0;
                tagged_data->all_ctype_flags = 0;
                tagged_data->flags = 0;
                tagged_data->u.free = NULL;
                tagged_data->data.ascii = tagged_data->u.small_data;
                if (max_cp < 0x100) {
                    tagged_data->data.latin1[0] = cp;
                    tagged_data->data.latin1[1] = cp2;
                    tagged_data->data.latin1[2] = 0;
                    sky_string_data_update_1(tagged_data, cp);
                    sky_string_data_update_1(tagged_data, cp2);
                }
                else if (max_cp < 0x10000) {
                    tagged_data->data.ucs2[0] = cp;
                    tagged_data->data.ucs2[1] = cp2;
                    tagged_data->data.ucs2[2] = 0;
                    sky_string_data_update(tagged_data, cp);
                    sky_string_data_update(tagged_data, cp2);
                }
                else {
                    tagged_data->data.ucs4[0] = cp;
                    tagged_data->data.ucs4[1] = cp2;
                    tagged_data->data.ucs4[2] = 0;
                    sky_string_data_update(tagged_data, cp);
                    sky_string_data_update(tagged_data, cp2);
                }
                return tagged_data;
#endif
            case 0x7:
                if (sky_string_empty == string) {
                    tagged_data->hash = 0;
                    tagged_data->len = 0;
                    tagged_data->data.ascii = tagged_data->u.small_data;
                    tagged_data->highest_codepoint = 0;
                    tagged_data->any_ctype_flags = 0;
                    tagged_data->all_ctype_flags = 0;
                    tagged_data->flags = SKY_STRING_FLAG_CTYPE_FLAGS_SET;
                    tagged_data->u.free = NULL;
                    tagged_data->u.small_data[0] = 0;
                    return tagged_data;
                }
                break;
        }
    }
    return sky_object_data(string, sky_string_type);
}


sky_string_data_t *
sky_string_data_noctype(sky_object_t string, sky_string_data_t *tagged_data)
{
    sky_unicode_char_t  cp;
#if defined(SKY_ARCH_64BIT)
    sky_unicode_char_t  cp2, max_cp;
#endif

    if (sky_object_istagged(string)) {
        switch (SKY_OBJECT_TAG(string)) {
            case 0x3:
                cp = ((uintptr_t)string >> 4) & 0x1FFFFF;
                tagged_data->hash = 0;
                tagged_data->len = 1;
                tagged_data->highest_codepoint = cp;
                tagged_data->any_ctype_flags = 0x5A5A5A5A;
                tagged_data->all_ctype_flags = 0x5A5A5A5A;
                tagged_data->flags = SKY_STRING_FLAG_CTYPE_FLAGS_SET;
                tagged_data->data.ascii = tagged_data->u.small_data;
                if (cp < 0x100) {
                    tagged_data->data.latin1[0] = cp;
                    tagged_data->data.latin1[1] = 0;
                }
                else if (cp < 0x10000) {
                    tagged_data->data.ucs2[0] = cp;
                    tagged_data->data.ucs2[1] = 0;
                }
                else {
                    tagged_data->data.ucs4[0] = cp;
                    tagged_data->data.ucs4[1] = 0;
                }
                return tagged_data;
#if defined(SKY_ARCH_64BIT)
            case 0x4:
                cp = ((uintptr_t)string >> 25) & 0x1FFFFF;
                cp2 = ((uintptr_t)string >> 4) & 0x1FFFFF;
                max_cp = SKY_MAX(cp, cp2);
                tagged_data->hash = 0;
                tagged_data->len = 2;
                tagged_data->highest_codepoint = max_cp;
                tagged_data->any_ctype_flags = 0x5A5A5A5A;
                tagged_data->all_ctype_flags = 0x5A5A5A5A;
                tagged_data->flags = SKY_STRING_FLAG_CTYPE_FLAGS_SET;
                tagged_data->data.ascii = tagged_data->u.small_data;
                if (max_cp < 0x100) {
                    tagged_data->data.latin1[0] = cp;
                    tagged_data->data.latin1[1] = cp2;
                    tagged_data->data.latin1[2] = 0;
                }
                else if (max_cp < 0x10000) {
                    tagged_data->data.ucs2[0] = cp;
                    tagged_data->data.ucs2[1] = cp2;
                    tagged_data->data.ucs2[2] = 0;
                }
                else {
                    tagged_data->data.ucs4[0] = cp;
                    tagged_data->data.ucs4[1] = cp2;
                    tagged_data->data.ucs4[2] = 0;
                }
                return tagged_data;
#endif
            case 0x7:
                if (sky_string_empty == string) {
                    tagged_data->hash = 0;
                    tagged_data->len = 0;
                    tagged_data->data.ascii = tagged_data->u.small_data;
                    tagged_data->highest_codepoint = 0;
                    tagged_data->any_ctype_flags = 0;
                    tagged_data->all_ctype_flags = 0;
                    tagged_data->flags = SKY_STRING_FLAG_CTYPE_FLAGS_SET;
                    tagged_data->u.small_data[0] = 0;
                    return tagged_data;
                }
                break;
        }
    }
    return sky_object_data(string, sky_string_type);
}


void
sky_string_data_append_1to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    uint8_t             *outcp;
    const uint8_t       *endcp, *incp;
    sky_unicode_char_t  cp;

    sky_string_data_resize(string_data, string_data->len + nbytes, 1);
    outcp = string_data->data.latin1 + string_data->len;
    endcp = (const uint8_t *)bytes + nbytes;
    for (incp = bytes; incp < endcp; ++incp) {
        *outcp++ = (uint8_t)(cp = (sky_unicode_char_t)*incp);
        sky_string_data_update_1(string_data, cp);
    }
    string_data->len += nbytes;
    string_data->data.latin1[string_data->len] = 0;
}


void
sky_string_data_append_1to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    uint16_t            *outcp;
    const uint8_t       *endcp, *incp;
    sky_unicode_char_t  cp;

    sky_string_data_resize(string_data, string_data->len + nbytes, 2);
    outcp = string_data->data.ucs2 + string_data->len;
    endcp = (const uint8_t *)bytes + nbytes;
    for (incp = bytes; incp < endcp; ++incp) {
        *outcp++ = (uint16_t)(cp = (sky_unicode_char_t)*incp);
        sky_string_data_update_1(string_data, cp);
    }
    string_data->len += nbytes;
    string_data->data.ucs2[string_data->len] = 0;
}


void
sky_string_data_append_1to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    const uint8_t       *endcp, *incp;
    sky_unicode_char_t  cp, *outcp;

    sky_string_data_resize(string_data, string_data->len + nbytes, 4);
    outcp = string_data->data.ucs4 + string_data->len;
    endcp = (const uint8_t *)bytes + nbytes;
    for (incp = bytes; incp < endcp; ++incp) {
        *outcp++ = cp = (sky_unicode_char_t)*incp;
        sky_string_data_update_1(string_data, cp);
    }
    string_data->len += nbytes;
    string_data->data.ucs4[string_data->len] = 0;
}


void
sky_string_data_append_2to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    size_t              needed;
    uint8_t             *outcp;
    const uint16_t      *endcp, *incp;
    sky_unicode_char_t  cp;

    sky_error_validate_debug(!(nbytes % 2));
    needed = string_data->len + (nbytes / 2);
    sky_string_data_resize(string_data, needed, 1);
    outcp = string_data->data.latin1 + string_data->len;
    endcp = (const uint16_t *)bytes + (nbytes / 2);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        if (cp > 0xFF) {
            string_data->len += (incp - (const uint16_t *)bytes);
            sky_string_data_widen(string_data, needed, 1, 2);
            sky_string_data_append_2to2(string_data, incp, (endcp - incp) * 2);
            return;
        }
        *outcp++ = (uint8_t)cp;
        sky_string_data_update_1(string_data, cp);
    }
    string_data->len += (nbytes / 2);
    string_data->data.latin1[string_data->len] = 0;
}


void
sky_string_data_append_2to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    uint16_t            *outcp;
    const uint16_t      *endcp, *incp;
    sky_unicode_char_t  cp;

    sky_error_validate_debug(!(nbytes % 2));
    sky_string_data_resize(string_data, string_data->len + (nbytes / 2), 2);
    outcp = string_data->data.ucs2 + string_data->len;
    endcp = (const uint16_t *)bytes + (nbytes / 2);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        *outcp++ = (uint16_t)cp;
        sky_string_data_update(string_data, cp);
    }
    string_data->len += (nbytes / 2);
    string_data->data.ucs2[string_data->len] = 0;
}


void
sky_string_data_append_2to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    const uint16_t      *endcp, *incp;
    sky_unicode_char_t  cp, *outcp;

    sky_error_validate_debug(!(nbytes % 2));
    sky_string_data_resize(string_data, string_data->len + (nbytes / 2), 4);
    outcp = string_data->data.ucs4 + string_data->len;
    endcp = (const uint16_t *)bytes + (nbytes / 2);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        *outcp++ = cp;
        sky_string_data_update(string_data, cp);
    }
    string_data->len += (nbytes / 2);
    string_data->data.ucs4[string_data->len] = 0;
}


void
sky_string_data_append_4to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    size_t                      needed;
    uint8_t                     *outcp;
    sky_unicode_char_t          cp;
    const sky_unicode_char_t    *endcp, *incp;

    sky_error_validate_debug(!(nbytes % 4));
    needed = string_data->len + (nbytes / 4);
    sky_string_data_resize(string_data, needed, 1);
    outcp = string_data->data.latin1 + string_data->len;
    endcp = (const sky_unicode_char_t *)bytes + (nbytes / 4);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        if (cp > 0xFFFF) {
            string_data->len += (incp - (const sky_unicode_char_t *)bytes);
            sky_string_data_widen(string_data, needed, 1, 4);
            sky_string_data_append_4to4(string_data, incp, (endcp - incp) * 4);
            return;
        }
        if (cp > 0xFF) {
            string_data->len += (incp - (const sky_unicode_char_t *)bytes);
            sky_string_data_widen(string_data, needed, 1, 2);
            sky_string_data_append_4to2(string_data, incp, (endcp - incp) * 4);
            return;
        }
        *outcp++ = cp;
        sky_string_data_update_1(string_data, cp);
    }
    string_data->len += (nbytes / 4);
    string_data->data.latin1[string_data->len] = 0;
}


void
sky_string_data_append_4to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    size_t                      needed;
    uint16_t                    *outcp;
    sky_unicode_char_t          cp;
    const sky_unicode_char_t    *endcp, *incp;

    sky_error_validate_debug(!(nbytes % 4));
    needed = string_data->len + (nbytes / 4);
    sky_string_data_resize(string_data, needed, 2);
    outcp = string_data->data.ucs2 + string_data->len;
    endcp = (const sky_unicode_char_t *)bytes + (nbytes / 4);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        if (cp > 0xFFFF) {
            string_data->len += (incp - (const sky_unicode_char_t *)bytes);
            sky_string_data_widen(string_data, needed, 2, 4);
            sky_string_data_append_4to4(string_data, incp, (endcp - incp) * 4);
            return;
        }
        *outcp++ = cp;
        sky_string_data_update(string_data, cp);
    }
    string_data->len += (nbytes / 4);
    string_data->data.ucs2[string_data->len] = 0;
}


void
sky_string_data_append_4to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes)
{
    sky_unicode_char_t          cp, *outcp;
    const sky_unicode_char_t    *endcp, *incp;

    sky_error_validate_debug(!(nbytes % 4));
    sky_string_data_resize(string_data, string_data->len + (nbytes / 4), 4);
    outcp = string_data->data.ucs4 + string_data->len;
    endcp = (const sky_unicode_char_t *)bytes + (nbytes / 4);
    for (incp = bytes; incp < endcp; ++incp) {
        cp = (sky_unicode_char_t)*incp;
        sky_error_validate_debug(cp <= SKY_UNICODE_CHAR_MAX);
        *outcp++ = cp;
        sky_string_data_update(string_data, cp);
    }
    string_data->len += (nbytes / 4);
    string_data->data.ucs4[string_data->len] = 0;
}


void
sky_string_data_append(sky_string_data_t *  string_data,
                       const void *         bytes,
                       size_t               nbytes,
                       size_t               width)
{
    sky_error_validate_debug(string_data != NULL);
    sky_error_validate_debug(bytes != NULL);
    sky_error_validate_debug(1 == width || 2 == width || 4 == width);

    if (string_data->highest_codepoint < 0x100) {
        if (1 == width) {
            sky_string_data_append_1to1(string_data, bytes, nbytes);
        }
        else if (2 == width) {
            sky_string_data_append_2to1(string_data, bytes, nbytes);
        }
        else {
            sky_string_data_append_4to1(string_data, bytes, nbytes);
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        if (1 == width) {
            sky_string_data_append_1to2(string_data, bytes, nbytes);
        }
        else if (2 == width) {
            sky_string_data_append_2to2(string_data, bytes, nbytes);
        }
        else {
            sky_string_data_append_4to2(string_data, bytes, nbytes);
        }
    }
    else {
        if (1 == width) {
            sky_string_data_append_1to4(string_data, bytes, nbytes);
        }
        else if (2 == width) {
            sky_string_data_append_2to4(string_data, bytes, nbytes);
        }
        else {
            sky_string_data_append_4to4(string_data, bytes, nbytes);
        }
    }
}


void
sky_string_data_append_codepoint(sky_string_data_t *string_data,
                                 sky_unicode_char_t codepoint,
                                 ssize_t            count)
{
    size_t  codepoint_width, string_width;

    if (!count) {
        return;
    }
    string_width = sky_string_data_width(string_data);
    if (codepoint < 0x100) {
        codepoint_width = 1;
    }
    else if (codepoint < 0x10000) {
        codepoint_width = 2;
    }
    else {
        codepoint_width = 4;
    }
    if (codepoint_width <= string_width) {
        sky_string_data_resize(string_data, string_data->len + count, string_width);
    }
    else {
        sky_string_data_widen(string_data,
                              string_data->len + count,
                              string_width,
                              codepoint_width);
        string_width = codepoint_width;
    }

    if (1 == string_width) {
        sky_string_data_update_1(string_data, codepoint);
        string_data->data.latin1[string_data->len + count] = 0;
        memset(&(string_data->data.latin1[string_data->len]), codepoint, count);
        string_data->len += count;
    }
    else {
        sky_string_data_update(string_data, codepoint);
        if (2 == string_width) {
            string_data->data.ucs2[string_data->len + count] = 0;
            while (count-- > 0) {
                string_data->data.ucs2[string_data->len++] = codepoint;
            }
        }
        else {
            string_data->data.ucs4[string_data->len + count] = 0;
            while (count-- > 0) {
                string_data->data.ucs4[string_data->len++] = codepoint;
            }
        }
    }
}


void
sky_string_data_append_slice(sky_string_data_t *        string_data,
                             const sky_string_data_t *  other_data,
                             ssize_t                    length,
                             ssize_t                    start,
                             ssize_t                    step)
{
    size_t      width;
    ssize_t     i, j;
    const char  *bytes;

    if (length > 0) {
        bytes = other_data->data.ascii;
        width = sky_string_data_width(other_data);
        if (1 == step) {
            sky_string_data_append(string_data,
                                   (const char *)bytes + (start * width),
                                   length * width,
                                   width);
        }
        else {
            for (i = 0, j = start; i < length; ++i, j += step) {
                sky_string_data_append(string_data,
                                       &(((const char *)bytes)[j * width]),
                                       width,
                                       width);
            }
        }
    }
}


void
sky_string_data_clear(sky_string_data_t *string_data)
{
    string_data->hash = 0;
    string_data->len = 0;
    if (string_data->data.ascii) {
        *string_data->data.ascii = '\0';
    }
    string_data->highest_codepoint = 0;
    string_data->any_ctype_flags = 0;
    string_data->all_ctype_flags = 0;
    string_data->flags = 0;
}


void
sky_string_data_combine(sky_string_data_t *         string_data,
                        const sky_string_data_t *   other_data)
{
    size_t  other_width, string_width;

    /* Short out early if there's nothing to combine, not just for performance,
     * but also because 'appending' the empty string will end up zeroing out
     * string_data's all_ctype_flags field, which is wrong.
     */
    if (!other_data->len) {
        return;
    }
    if (SSIZE_MAX - string_data->len < other_data->len + 1) {
        sky_error_raise_string(sky_OverflowError, "string too long");
    }

    string_width = sky_string_data_width(string_data);
    other_width = sky_string_data_width(other_data);

    if (string_width == other_width) {
        sky_string_data_resize(string_data,
                               string_data->len + other_data->len,
                               string_width);
        memcpy(string_data->data.ascii + (string_data->len * string_width),
               other_data->data.ascii,
               other_data->len * other_width);
    }
    else if (string_width < other_width) {
        sky_string_data_widen(string_data,
                              string_data->len + other_data->len,
                              string_width,
                              other_width);
        string_width = other_width;
        memcpy(string_data->data.ascii + (string_data->len * string_width),
               other_data->data.ascii,
               other_data->len * other_width);
    }
    else {  /* string_width > other_width */
        sky_string_data_resize(string_data,
                               string_data->len + other_data->len,
                               string_width);
        if (2 == string_width) {
            uint16_t        *o = string_data->data.ucs2 + string_data->len;
            const uint8_t   *c = other_data->data.latin1,
                            *end = other_data->data.latin1 + other_data->len;

            while (c < end) {
                *o++ = (uint16_t)*c++;
            }
        }
        else {  /* 4 == string_width */
            sky_unicode_char_t  *o = string_data->data.ucs4 + string_data->len;

            if (1 == other_width) {
                const uint8_t   *c = other_data->data.latin1,
                                *end = other_data->data.latin1 + other_data->len;

                while (c < end) {
                    *o++ = (sky_unicode_char_t)*c++;
                }
            }
            else {
                const uint16_t  *c = other_data->data.ucs2,
                                *end = other_data->data.ucs2 + other_data->len;

                while (c < end) {
                    *o++ = (sky_unicode_char_t)*c++;
                }
            }
        }
    }

    string_data->len += other_data->len;
    if (other_data->highest_codepoint > string_data->highest_codepoint) {
        string_data->highest_codepoint = other_data->highest_codepoint;
    }
    if (!(string_data->flags & SKY_STRING_FLAG_CTYPE_FLAGS_SET)) {
        string_data->all_ctype_flags = other_data->all_ctype_flags;
        string_data->flags |= SKY_STRING_FLAG_CTYPE_FLAGS_SET;
    }
    else {
        string_data->all_ctype_flags &= other_data->all_ctype_flags;
    }
    string_data->any_ctype_flags |= other_data->any_ctype_flags;

    if (string_data->highest_codepoint < 0x100) {
        string_data->data.latin1[string_data->len] = 0;
    }
    else if (string_data->highest_codepoint < 0x10000) {
        string_data->data.ucs2[string_data->len] = 0;
    }
    else {
        string_data->data.ucs4[string_data->len] = 0;
    }
}


static int
sky_string_data_compare_1vs1(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    const void  *s = string_data->data.latin1,
                *o = other_data->data.latin1;

    int cmprc;

    if (string_data->len == other_data->len) {
        return memcmp(s, o, string_data->len);
    }
    if (string_data->len < other_data->len) {
        cmprc = memcmp(s, o, string_data->len);
        return (cmprc ? cmprc : -1);
    }
    cmprc = memcmp(s, o, other_data->len);
    return (cmprc ? cmprc : 1);
}

static int
sky_string_data_compare_1vs2(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    int                 defaultrc;
    const uint8_t       *end, *s;
    const uint16_t      *o;
    sky_unicode_char_t  cmprc;

    if (string_data->len == other_data->len) {
        defaultrc = 0;
        end = string_data->data.latin1 + string_data->len;
    }
    else if (string_data->len < other_data->len) {
        defaultrc = -1;
        end = string_data->data.latin1 + string_data->len;
    }
    else {
        defaultrc = 1;
        end = string_data->data.latin1 + other_data->len;
    }
    s = string_data->data.latin1;
    o = other_data->data.ucs2;
    while (s < end) {
        cmprc = ((sky_unicode_char_t)*s++ - (sky_unicode_char_t)*o++);
        if (cmprc) {
            return (cmprc < 0 ? -1 : 1);
        }
    }

    return defaultrc;
}

static int
sky_string_data_compare_1vs4(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    int                         defaultrc;
    const uint8_t               *end, *s;
    sky_unicode_char_t          cmprc;
    const sky_unicode_char_t    *o;

    if (string_data->len == other_data->len) {
        defaultrc = 0;
        end = string_data->data.latin1 + string_data->len;
    }
    else if (string_data->len < other_data->len) {
        defaultrc = -1;
        end = string_data->data.latin1 + string_data->len;
    }
    else {
        defaultrc = 1;
        end = string_data->data.latin1 + other_data->len;
    }
    s = string_data->data.latin1;
    o = other_data->data.ucs4;
    while (s < end) {
        cmprc = ((sky_unicode_char_t)*s++ - (sky_unicode_char_t)*o++);
        if (cmprc) {
            return (cmprc < 0 ? -1 : 1);
        }
    }

    return defaultrc;
}

static int
sky_string_data_compare_2vs2(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    int                 defaultrc;
    const uint16_t      *end, *o, *s;
    sky_unicode_char_t  cmprc;

    if (string_data->len == other_data->len) {
        defaultrc = 0;
        end = string_data->data.ucs2 + string_data->len;
    }
    else if (string_data->len < other_data->len) {
        defaultrc = -1;
        end = string_data->data.ucs2 + string_data->len;
    }
    else {
        defaultrc = 1;
        end = string_data->data.ucs2 + other_data->len;
    }
    s = string_data->data.ucs2;
    o = other_data->data.ucs2;
    while (s < end) {
        cmprc = ((sky_unicode_char_t)*s++ - (sky_unicode_char_t)*o++);
        if (cmprc) {
            return (cmprc < 0 ? -1 : 1);
        }
    }

    return defaultrc;
}

static int
sky_string_data_compare_2vs4(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    int                         defaultrc;
    const uint16_t              *end, *s;
    sky_unicode_char_t          cmprc;
    const sky_unicode_char_t    *o;

    if (string_data->len == other_data->len) {
        defaultrc = 0;
        end = string_data->data.ucs2 + string_data->len;
    }
    else if (string_data->len < other_data->len) {
        defaultrc = -1;
        end = string_data->data.ucs2 + string_data->len;
    }
    else {
        defaultrc = 1;
        end = string_data->data.ucs2 + other_data->len;
    }
    s = string_data->data.ucs2;
    o = other_data->data.ucs4;
    while (s < end) {
        cmprc = ((sky_unicode_char_t)*s++ - (sky_unicode_char_t)*o++);
        if (cmprc) {
            return (cmprc < 0 ? -1 : 1);
        }
    }

    return defaultrc;
}

static int
sky_string_data_compare_4vs4(const sky_string_data_t *  string_data,
                             const sky_string_data_t *  other_data)
{
    int                         defaultrc;
    sky_unicode_char_t          cmprc;
    const sky_unicode_char_t    *end, *o, *s;

    if (string_data->len == other_data->len) {
        defaultrc = 0;
        end = string_data->data.ucs4 + string_data->len;
    }
    else if (string_data->len < other_data->len) {
        defaultrc = -1;
        end = string_data->data.ucs4 + string_data->len;
    }
    else {
        defaultrc = 1;
        end = string_data->data.ucs4 + other_data->len;
    }
    s = string_data->data.ucs4;
    o = other_data->data.ucs4;
    while (s < end) {
        cmprc = ((sky_unicode_char_t)*s++ - (sky_unicode_char_t)*o++);
        if (cmprc) {
            return (cmprc < 0 ? -1 : 1);
        }
    }

    return defaultrc;
}

int
sky_string_data_compare(const sky_string_data_t *   string_data,
                        const sky_string_data_t *   other_data)
{
    if (!string_data->len) {
        return (other_data->len ? -1 : 0);
    }
    if (!other_data->len) {
        return 1;
    }
    if (string_data->highest_codepoint < 0x100) {
        if (other_data->highest_codepoint < 0x100) {
            return sky_string_data_compare_1vs1(string_data, other_data);
        }
        if (other_data->highest_codepoint < 0x10000) {
            return sky_string_data_compare_1vs2(string_data, other_data);
        }
        return sky_string_data_compare_1vs4(string_data, other_data);
    }
    if (string_data->highest_codepoint < 0x10000) {
        if (other_data->highest_codepoint < 0x100) {
            return -sky_string_data_compare_1vs2(other_data, string_data);
        }
        if (other_data->highest_codepoint < 0x10000) {
            return sky_string_data_compare_2vs2(string_data, other_data);
        }
        return sky_string_data_compare_2vs4(string_data, other_data);
    }
    if (other_data->highest_codepoint < 0x100) {
        return -sky_string_data_compare_1vs4(other_data, string_data);
    }
    if (other_data->highest_codepoint < 0x10000) {
        return -sky_string_data_compare_2vs4(other_data, string_data);
    }
    return sky_string_data_compare_4vs4(string_data, other_data);
}


void
sky_string_data_copy(sky_string_data_t *        target_data,
                     const sky_string_data_t *  source_data)
{
    size_t  nbytes;

    if (target_data->data.ascii) {
        sky_string_data_finalize(target_data);
        sky_string_data_clear(target_data);
    }

    target_data->hash = source_data->hash;
    target_data->len = source_data->len;
    target_data->highest_codepoint = source_data->highest_codepoint;
    target_data->any_ctype_flags = source_data->any_ctype_flags;
    target_data->all_ctype_flags = source_data->all_ctype_flags;
    target_data->flags = source_data->flags & (SKY_STRING_FLAG_HASHED |
                                               SKY_STRING_FLAG_CTYPE_FLAGS_SET);

    if (source_data->highest_codepoint < 0x100) {
        nbytes = source_data->len + 1;
    }
    else if (source_data->highest_codepoint < 0x10000) { 
        nbytes = (source_data->len + 1) * 2;
    }
    else {
        nbytes = (source_data->len + 1) * 4;
    }
    if (nbytes > sizeof(target_data->u.small_data)) {
        target_data->data.ascii = sky_memdup(source_data->data.ascii, nbytes);
        target_data->u.free = sky_free;
    }
    else {
        memcpy(target_data->u.small_data, source_data->data.ascii, nbytes);
        target_data->data.ascii = target_data->u.small_data;
    }
}


static void
sky_string_data_decode_callback(const void *bytes,
                                ssize_t     nbytes,
                                size_t      width,
                                void *      arg)
{
    sky_string_data_append(arg, bytes, nbytes, width);
}


void
sky_string_data_decode(sky_string_data_t *  string_data,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       const void *         bytes,
                       ssize_t              nbytes)
{
    uint32_t    flags = SKY_CODEC_DECODE_FLAG_REQUIRE_TEXT;

    sky_codec_decodewithcallback(NULL,
                                 encoding,
                                 errors,
                                 sky_string_data_decode_callback,
                                 string_data,
                                 &flags,
                                 bytes,
                                 nbytes);
}


void
sky_string_data_finalize(sky_string_data_t *string_data)
{
    if (string_data->u.small_data != string_data->data.ascii &&
        string_data->u.free)
    {
        string_data->u.free(string_data->data.ascii);
    }
    string_data->data.ascii = NULL;
    string_data->u.free = NULL;
}


void
sky_string_data_join(sky_string_data_t *        string_data,
                     const sky_string_data_t *  separator,
                     sky_string_data_t * const *strings,
                     ssize_t                    nstrings)
{
    size_t  final_width, width;
    ssize_t final_len, i;

    if (!nstrings) {
        return;
    }
    if (1 == nstrings) {
        sky_string_data_combine(string_data, strings[0]);
        return;
    }

    /* Figure out how long the final string will be to avoid multiple memory
     * reallocations. Also ensures that we don't overflow.
     */
    final_width = sky_string_data_width(string_data);
    if ((width = sky_string_data_width(separator)) > final_width) {
        final_width = width;
    }
    final_len = string_data->len + 1;   /* +1 for convenience '\0' byte */
    if (SSIZE_MAX - final_len < strings[0]->len) {
        sky_error_raise_string(sky_OverflowError, "string too long");
    }
    final_len += strings[0]->len;
    if ((width = sky_string_data_width(strings[0])) > final_width) {
        final_width = width;
    }
    for (i = 1; i < nstrings; ++i) {
        if (SSIZE_MAX - final_len < separator->len ||
            SSIZE_MAX - final_len - separator->len < strings[i]->len)
        {
            sky_error_raise_string(sky_OverflowError, "string too long");
        }
        final_len += (separator->len + strings[i]->len);
        if ((width = sky_string_data_width(strings[i])) > final_width) {
            final_width = width;
        }
    }

    if (final_width <= (width = sky_string_data_width(string_data))) {
        sky_string_data_resize(string_data, final_len - 1, width);
    }
    else {
        sky_string_data_widen(string_data,
                              final_len - 1,
                              width,
                              final_width);
    }

    /* Join all of the strings together with the separator in between each
     * string. Use sky_string_data_combine() to save on ctype lookups.
     */
    sky_string_data_combine(string_data, strings[0]);
    for (i = 1; i < nstrings; ++i) {
        sky_string_data_combine(string_data, separator);
        sky_string_data_combine(string_data, strings[i]);
    }
}


void
sky_string_data_resize(sky_string_data_t *  string_data,
                       size_t               needed_size,
                       size_t               width)
{
    void    *new_data;
    size_t  avail_size;

    /* In general, strings are immutable, and their sizes are at least somewhat
     * estimatable at the time of creation; therefore, it does not make sense
     * to attempt to allocate more memory than needed in an effort to reduce
     * reallocations. Simply allocate the requested size.
     */
    needed_size = (needed_size + 1) * width;
    if (!string_data->data.ascii) {
        string_data->data.ascii = string_data->u.small_data;
        avail_size = sizeof(string_data->u.small_data);
    }
    else if (string_data->u.small_data == string_data->data.ascii) {
        avail_size = sizeof(string_data->u.small_data);
    }
    else if (sky_free == string_data->u.free) {
        avail_size = sky_memsize(string_data->data.ascii);
    }
    else {
        avail_size = 0;
    }
    if (needed_size > avail_size) {
        /* Don't use realloc, because it'll likely mean more data is copied
         * than necessary. It may not seem like much, but the difference can
         * be significant.
         */
        new_data = sky_malloc(needed_size);
        if (1 == width) {
            memcpy(new_data, string_data->data.latin1, string_data->len);
        }
        else if (2 == width) {
            memcpy(new_data, string_data->data.ucs2, string_data->len * 2);
            if (string_data->highest_codepoint < 0x100) {
                string_data->highest_codepoint = 0x100;
            }
        }
        else {
            memcpy(new_data, string_data->data.ucs4, string_data->len * 4);
            if (string_data->highest_codepoint < 0x10000) {
                string_data->highest_codepoint = 0x10000;
            }
        }
        if (string_data->u.small_data != string_data->data.ascii &&
            string_data->u.free)
        {
            string_data->u.free(string_data->data.ascii);
        }
        string_data->data.ascii = new_data;
        string_data->u.free = sky_free;
    }
}


void
sky_string_data_widen(sky_string_data_t *   string_data,
                      size_t                needed,
                      size_t                old_width,
                      size_t                new_width)
{
    void    *new_data;
    size_t  avail_size, needed_size;

    sky_error_validate_debug(old_width < new_width);
    sky_error_validate_debug(1 == old_width || 2 == old_width);
    sky_error_validate_debug(2 == new_width || 4 == new_width);

    needed_size = (needed + 1) * new_width;
    if (!string_data->data.ascii) {
        string_data->data.ascii = string_data->u.small_data;
        avail_size = sizeof(string_data->u.small_data);
    }
    else if (string_data->u.small_data == string_data->data.ascii) {
        avail_size = sizeof(string_data->u.small_data);
    }
    else if (sky_free == string_data->u.free) {
        avail_size = sky_memsize(string_data->data.ascii);
    }
    else {
        avail_size = 0;
    }

    if (needed_size > avail_size) {
        new_data = sky_malloc(needed_size);

        if (1 == old_width) {
            uint8_t *end, *inc;

            inc = string_data->data.latin1;
            end = inc + string_data->len;
            if (2 == new_width) {
                uint16_t    *outc = new_data;

                while (inc < end) {
                    *outc++ = (uint16_t)*inc++;
                }
                string_data->highest_codepoint = 0x100;
            }
            else {
                sky_unicode_char_t  *outc = new_data;

                while (inc < end) {
                    *outc++ = (sky_unicode_char_t)*inc++;
                }
                string_data->highest_codepoint = 0x10000;
            }
        }
        else {
            uint16_t            *end, *inc;
            sky_unicode_char_t  *outc = new_data;

            inc = string_data->data.ucs2;
            end = inc + string_data->len;
            while (inc < end) {
                *outc++ = (sky_unicode_char_t)*inc++;
            }
            string_data->highest_codepoint = 0x10000;
        }

        if (string_data->u.small_data != string_data->data.ascii &&
            string_data->u.free)
        {
            string_data->u.free(string_data->data.ascii);
        }
        string_data->data.ascii = new_data;
        string_data->u.free = sky_free;
    }
    else {
        if (1 == old_width) {
            uint8_t *inc = &(string_data->data.latin1[string_data->len]);

            if (2 == new_width) {
                uint16_t    *outc = &(string_data->data.ucs2[string_data->len]);

                while (inc >= string_data->data.latin1) {
                    *outc-- = *inc--;
                }
                string_data->highest_codepoint = 0x100;
            }
            else {
                sky_unicode_char_t  *outc =
                                    &(string_data->data.ucs4[string_data->len]);

                while (inc >= string_data->data.latin1) {
                    *outc-- = *inc--;
                }
                string_data->highest_codepoint = 0x10000;
            }
        }
        else {
            uint16_t            *inc =
                                &(string_data->data.ucs2[string_data->len]);
            sky_unicode_char_t  *outc =
                                &(string_data->data.ucs4[string_data->len]);

            while (inc >= string_data->data.ucs2) {
                *outc-- = *inc--;
            }
            string_data->highest_codepoint = 0x10000;
        }
    }
}
