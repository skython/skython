#include "sky_private.h"


typedef struct sky_weakref_data_s {
    sky_object_t                        object;
    uintptr_t                           hash;
    sky_bool_t                          hashed;            
    sky_object_t                        callback;
} sky_weakref_data_t;

SKY_EXTERN_INLINE sky_weakref_data_t *
sky_weakref_data(sky_object_t object)
{
    if (sky_weakref_type == sky_object_type(object)) {
        return (sky_weakref_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_weakref_type);
}


void
sky_weakref_init(sky_weakref_t  self,
                 sky_object_t   object,
                 sky_object_t   callback);


static void
sky_weakref_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_weakref_data_t  *self_data = data;

    sky_object_visit(self_data->callback, visit_data);
    if (SKY_OBJECT_VISIT_REASON_GC_MARK != visit_data->reason) {
        sky_object_visit(self_data->object, visit_data);
    }
}


void
sky_weakref_callback(sky_object_t object)
{
    sky_weakref_data_t  *weakref_data = sky_weakref_data(object);

    weakref_data->object = NULL;
    if (weakref_data->callback) {
        sky_object_call(weakref_data->callback,
                        sky_tuple_pack(1, object),
                        NULL);
    }
}


sky_object_t
sky_weakref_call(sky_weakref_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_weakref_data_t  *self_data = sky_weakref_data(self);

    if (sky_object_bool(args) || sky_object_bool(kws)) {
        sky_error_raise_format(
                sky_TypeError,
                "__call__() takes at most 0 arguments (%zd given)",
                (args ? sky_object_len(args) : 0) +
                (kws ? sky_object_len(kws) : 0));
    }

    return self_data->object;
}


sky_weakref_t
sky_weakref_create(sky_object_t object, sky_object_t callback)
{
    sky_weakref_t   ref;

    ref = sky_object_allocate(sky_weakref_type);
    sky_weakref_init(ref, object, callback);

    return ref;
}


sky_object_t
sky_weakref_eq(sky_weakref_t self, sky_object_t other)
{
    sky_object_t    other_object, self_object;

    if (!sky_object_isa(other, sky_weakref_type)) {
        return sky_NotImplemented;
    }

    self_object = sky_weakref_data(self)->object;
    other_object = sky_weakref_data(other)->object;

    if (!self_object) {
        return (other_object ? sky_False : sky_True);
    }
    if (!other_object) {
        return sky_False;
    }
    return sky_object_eq(self_object, other_object);
}


uintptr_t
sky_weakref_hash(sky_weakref_t self)
{
    sky_weakref_data_t  *self_data = sky_weakref_data(self);

    sky_object_t    object;

    if (!self_data->hashed) {
        if (!(object = self_data->object)) {
            sky_error_raise_string(sky_TypeError, "weak object has gone away");
        }
        self_data->hash = sky_object_hash(object);
        self_data->hashed = SKY_TRUE;
    }

    return self_data->hash;
}


void
sky_weakref_init(sky_weakref_t  self,
                 sky_object_t   object,
                 sky_object_t   callback)
{
    sky_weakref_data_t  *self_data = sky_weakref_data(self);

    if (sky_object_istagged(object)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot create weak reference to %#@ object",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isnull(callback)) {
        callback = NULL;
    }

    sky_object_gc_set(&(self_data->callback), callback, self);
    sky_object_gc_setweakref(&(self_data->object),
                             object,
                             self,
                             sky_weakref_callback,
                             self);
}


sky_object_t
sky_weakref_ne(sky_weakref_t self, sky_object_t other)
{
    sky_object_t    other_object, self_object;

    if (!sky_object_isa(other, sky_weakref_type)) {
        return sky_NotImplemented;
    }

    self_object = sky_weakref_data(self)->object;
    other_object = sky_weakref_data(other)->object;

    if (!self_object) {
        return (other_object ? sky_True : sky_False);
    }
    if (!other_object) {
        return sky_True;
    }
    return sky_object_ne(self_object, other_object);
}


sky_object_t
sky_weakref_object(sky_weakref_t self)
{
    return sky_weakref_data(self)->object;
}


sky_string_t
sky_weakref_repr(sky_weakref_t self)
{
    sky_weakref_data_t  *self_data = sky_weakref_data(self);

    sky_object_t    object;
    sky_string_t    name, type_name;

    if (!(object = self_data->object)) {
        return sky_string_createfromformat("<weakref at %p; dead>", self);
    }

    name = sky_object_getattr(object, SKY_STRING_LITERAL("__name__"), NULL);
    type_name = sky_type_name(sky_object_type(object));

    if (!name) {
        return sky_string_createfromformat("<weakref at %p; to %#@ at %p>",
                                           self,
                                           type_name,
                                           object);
    }
    return sky_string_createfromformat("<weakref at %p; to %#@ at %p (%@)>",
                                       self,
                                       type_name,
                                       object,
                                       name);
}


SKY_TYPE_DEFINE_SIMPLE(weakref,
                       "weakref",
                       sizeof(sky_weakref_data_t),
                       NULL,
                       NULL,
                       sky_weakref_instance_visit,
                       0,
                       NULL);


void
sky_weakref_initialize_library(void)
{
    sky_type_initialize_builtin(sky_weakref_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_weakref_type,
            "__init__",
            (sky_native_code_function_t)sky_weakref_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "object", SKY_DATA_TYPE_OBJECT, NULL,
            "callback", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);
    sky_type_setmethodslots(sky_weakref_type,
            "__repr__", sky_weakref_repr,
            "__eq__", sky_weakref_eq,
            "__ne__", sky_weakref_ne,
            "__hash__", sky_weakref_hash,
            "__call__", sky_weakref_call,
            NULL);
}
