#ifndef __SKYTHON_CORE_SKY_PYTHON_TOKEN_H__
#define __SKYTHON_CORE_SKY_PYTHON_TOKEN_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


typedef enum sky_python_token_name_e {
    SKY_PYTHON_TOKEN_ERROR                  =   -1,
    SKY_PYTHON_TOKEN_ENDMARKER              =   0,

    SKY_PYTHON_TOKEN_INDENT                 =   1,
    SKY_PYTHON_TOKEN_DEDENT                 =   2,
    SKY_PYTHON_TOKEN_NEWLINE                =   3,
    SKY_PYTHON_TOKEN_SEMICOLON              =   4,      /* ;    */

    SKY_PYTHON_TOKEN_INTEGER                =   11,
    SKY_PYTHON_TOKEN_DECIMAL                =   12,
    SKY_PYTHON_TOKEN_IMAGINARY              =   13,
    SKY_PYTHON_TOKEN_STRING                 =   14,
    SKY_PYTHON_TOKEN_BYTES                  =   15,

    SKY_PYTHON_TOKEN_EQUALS                 =   101,    /* =    */
    SKY_PYTHON_TOKEN_EQUALS_EQUALS          =   102,    /* ==   */
    SKY_PYTHON_TOKEN_BANG_EQUALS            =   103,    /* !=   */

    SKY_PYTHON_TOKEN_LESS                   =   104,    /* <    */
    SKY_PYTHON_TOKEN_LESS_EQUALS            =   105,    /* <=   */
    SKY_PYTHON_TOKEN_LESS_LESS              =   106,    /* <<   */
    SKY_PYTHON_TOKEN_LESS_LESS_EQUALS       =   107,    /* <<=  */

    SKY_PYTHON_TOKEN_GREATER                =   108,    /* >    */
    SKY_PYTHON_TOKEN_GREATER_EQUALS         =   109,    /* >=   */
    SKY_PYTHON_TOKEN_GREATER_GREATER        =   110,    /* >>   */
    SKY_PYTHON_TOKEN_GREATER_GREATER_EQUALS =   111,    /* >>=  */

    SKY_PYTHON_TOKEN_PLUS                   =   112,    /* +    */
    SKY_PYTHON_TOKEN_PLUS_EQUALS            =   113,    /* +=   */

    SKY_PYTHON_TOKEN_MINUS                  =   114,    /* -    */
    SKY_PYTHON_TOKEN_MINUS_EQUALS           =   115,    /* -=   */
    SKY_PYTHON_TOKEN_MINUS_GREATER          =   116,    /* ->   */

    SKY_PYTHON_TOKEN_STAR                   =   117,    /* *    */
    SKY_PYTHON_TOKEN_STAR_EQUALS            =   118,    /* *=   */
    SKY_PYTHON_TOKEN_STAR_STAR              =   119,    /* **   */
    SKY_PYTHON_TOKEN_STAR_STAR_EQUALS       =   120,    /* **=  */

    SKY_PYTHON_TOKEN_SLASH                  =   121,    /* /    */
    SKY_PYTHON_TOKEN_SLASH_EQUALS           =   122,    /* /=   */
    SKY_PYTHON_TOKEN_SLASH_SLASH            =   123,    /* //   */
    SKY_PYTHON_TOKEN_SLASH_SLASH_EQUALS     =   124,    /* //=  */

    SKY_PYTHON_TOKEN_PERCENT                =   125,    /* %    */
    SKY_PYTHON_TOKEN_PERCENT_EQUALS         =   126,    /* %=   */

    SKY_PYTHON_TOKEN_AMPERSAND              =   127,    /* &    */
    SKY_PYTHON_TOKEN_AMPERSAND_EQUALS       =   128,    /* &=   */
    SKY_PYTHON_TOKEN_PIPE                   =   129,    /* |    */
    SKY_PYTHON_TOKEN_PIPE_EQUALS            =   130,    /* |=   */
    SKY_PYTHON_TOKEN_CARET                  =   131,    /* ^    */
    SKY_PYTHON_TOKEN_CARET_EQUALS           =   132,    /* ^=   */

    SKY_PYTHON_TOKEN_TILDE                  =   133,    /* ~    */

    SKY_PYTHON_TOKEN_LPAREN                 =   134,    /* (    */
    SKY_PYTHON_TOKEN_RPAREN                 =   135,    /* )    */
    SKY_PYTHON_TOKEN_LBRACKET               =   136,    /* [    */
    SKY_PYTHON_TOKEN_RBRACKET               =   137,    /* ]    */
    SKY_PYTHON_TOKEN_LBRACE                 =   138,    /* {    */
    SKY_PYTHON_TOKEN_RBRACE                 =   139,    /* }    */
    SKY_PYTHON_TOKEN_COMMA                  =   140,    /* ,    */
    SKY_PYTHON_TOKEN_COLON                  =   141,    /* :    */
    SKY_PYTHON_TOKEN_DOT                    =   142,    /* .    */
    SKY_PYTHON_TOKEN_ELLIPSIS               =   143,    /* ...  */
    SKY_PYTHON_TOKEN_AT                     =   144,    /* @    */

    SKY_PYTHON_TOKEN_NAME                   =   200,
    SKY_PYTHON_TOKEN_AND                    =   201,
    SKY_PYTHON_TOKEN_AS                     =   202,
    SKY_PYTHON_TOKEN_ASSERT                 =   203,
    SKY_PYTHON_TOKEN_BREAK                  =   204,
    SKY_PYTHON_TOKEN_CLASS                  =   205,
    SKY_PYTHON_TOKEN_CONTINUE               =   206,
    SKY_PYTHON_TOKEN_DEF                    =   207,
    SKY_PYTHON_TOKEN_DEL                    =   208,
    SKY_PYTHON_TOKEN_ELIF                   =   209,
    SKY_PYTHON_TOKEN_ELSE                   =   210,
    SKY_PYTHON_TOKEN_EXCEPT                 =   211,
    SKY_PYTHON_TOKEN_FALSE                  =   212,
    SKY_PYTHON_TOKEN_FINALLY                =   213,
    SKY_PYTHON_TOKEN_FOR                    =   214,
    SKY_PYTHON_TOKEN_FROM                   =   215,
    SKY_PYTHON_TOKEN_GLOBAL                 =   216,
    SKY_PYTHON_TOKEN_IF                     =   217,
    SKY_PYTHON_TOKEN_IMPORT                 =   218,
    SKY_PYTHON_TOKEN_IN                     =   219,
    SKY_PYTHON_TOKEN_IS                     =   220,
    SKY_PYTHON_TOKEN_LAMBDA                 =   221,
    SKY_PYTHON_TOKEN_NONE                   =   222,
    SKY_PYTHON_TOKEN_NONLOCAL               =   223,
    SKY_PYTHON_TOKEN_NOT                    =   224,
    SKY_PYTHON_TOKEN_OR                     =   225,
    SKY_PYTHON_TOKEN_PASS                   =   226,
    SKY_PYTHON_TOKEN_RAISE                  =   227,
    SKY_PYTHON_TOKEN_RETURN                 =   228,
    SKY_PYTHON_TOKEN_TRUE                   =   229,
    SKY_PYTHON_TOKEN_TRY                    =   230,
    SKY_PYTHON_TOKEN_WHILE                  =   231,
    SKY_PYTHON_TOKEN_WITH                   =   232,
    SKY_PYTHON_TOKEN_YIELD                  =   233,
} sky_python_token_name_t;


typedef struct sky_python_token_s {
    sky_python_token_name_t             name;
    sky_object_t                        object;
    int                                 lineno;
    int                                 col_offset;
} sky_python_token_t;


SKY_EXTERN const char *
sky_python_token_name(sky_python_token_name_t token);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PYTHON_TOKEN_H__ */
