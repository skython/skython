#include "sky_private.h"


typedef struct sky_callable_iterator_data_s {
    sky_object_t                        callable;
    sky_object_t                        sentinel;
} sky_callable_iterator_data_t;

SKY_EXTERN_INLINE sky_callable_iterator_data_t *
sky_callable_iterator_data(sky_object_t object)
{
    if (sky_callable_iterator_type == sky_object_type(object)) {
        return (sky_callable_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_callable_iterator_type);
}

struct sky_callable_iterator_s {
    sky_object_data_t                   object_data;
    sky_callable_iterator_t             iterator_data;
};


static void
sky_callable_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_callable_iterator_data_t    *iterator_data = data;

    sky_object_visit(iterator_data->callable, visit_data);
    sky_object_visit(iterator_data->sentinel, visit_data);
}


sky_callable_iterator_t
sky_callable_iterator_create(sky_object_t callable, sky_object_t sentinel)
{
    sky_callable_iterator_t         iterator;
    sky_callable_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_callable_iterator_type);
    iterator_data = sky_callable_iterator_data(iterator);

    sky_object_gc_set(&(iterator_data->callable), callable, iterator);
    sky_object_gc_set(&(iterator_data->sentinel), sentinel, iterator);

    return iterator;
}


sky_object_t
sky_callable_iterator_iter(sky_callable_iterator_t self)
{
    return self;
}


sky_object_t
sky_callable_iterator_next(sky_callable_iterator_t self)
{
    sky_callable_iterator_data_t * volatile self_data =
                                            sky_callable_iterator_data(self);

    sky_object_t    value;

    if (!self_data->callable || !self_data->sentinel) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }
    SKY_ERROR_TRY {
        value = sky_object_call(self_data->callable, NULL, NULL);
    }
    SKY_ERROR_EXCEPT(sky_StopIteration) {
        self_data->callable = NULL;
        self_data->sentinel = NULL;
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    if (sky_object_compare(value, self_data->sentinel, SKY_COMPARE_OP_EQUAL)) {
        self_data->callable = NULL;
        self_data->sentinel = NULL;
        sky_error_raise_object(sky_StopIteration, sky_None);
    }
    return value;
}


sky_object_t
sky_callable_iterator_reduce(sky_callable_iterator_t self)
{
    sky_callable_iterator_data_t    *self_data =
                                    sky_callable_iterator_data(self);

    if (self_data->callable && self_data->sentinel) {
        return sky_object_build("O(OO)",
                                sky_module_getbuiltin("iter"),
                                self_data->callable,
                                self_data->sentinel);
    }
    return sky_object_build("O(())", sky_module_getbuiltin("iter"));
}


SKY_TYPE_DEFINE_SIMPLE(callable_iterator,
                       "callable_iterator",
                       sizeof(sky_callable_iterator_data_t),
                       NULL,
                       NULL,
                       sky_callable_iterator_instance_visit,
                       0,
                       NULL);

void
sky_callable_iterator_initialize_library(void)
{
    sky_type_initialize_builtin(sky_callable_iterator_type, 0);

    sky_type_setattr_builtin(sky_callable_iterator_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_callable_iterator_type,
            "__iter__", sky_callable_iterator_iter,
            "__next__", sky_callable_iterator_next,
            "__reduce__", sky_callable_iterator_reduce,
            NULL);
}
