#include "sky_private.h"
#include "sky_list_private.h"


/* The code contained herein is ported directly from CPython 3.3.2.
 * Global names have been changed to conform to Skython's naming conventions.
 * (local variables, parameters, etc. remain unchanged).
 * Other code has been changed as necessary for porting purposes.
 * The algorithm remains unchanged. The file listsort.txt that is referenced
 * can be found in the CPython source distribution.
 */

/* Lots of code for an adaptive, stable, natural mergesort.  There are many
 * pieces to this algorithm; read listsort.txt for overviews and details.
 */

/* A sortslice contains a pointer to an array of keys and a pointer to
 * an array of corresponding values.  In other words, keys[i]
 * corresponds with values[i].  If values == NULL, then the keys are
 * also the values.
 *
 * Several convenience routines are provided here, so that keys and
 * values are always moved in sync.
 */

typedef struct sky_list_sort_slice_s {
    sky_object_t *                      keys;
    sky_object_t *                      values;
} sky_list_sort_slice_t;

static inline void
sky_list_sort_slice_copy(sky_list_sort_slice_t *s1,
                         ssize_t                i,
                         sky_list_sort_slice_t *s2,
                         ssize_t                j)
{
    s1->keys[i] = s2->keys[j];
    if (s1->values != NULL) {
        s1->values[i] = s2->values[j];
    }
}

static inline void
sky_list_sort_slice_copy_incr(sky_list_sort_slice_t *   dst,
                              sky_list_sort_slice_t *   src)
{
    *dst->keys++ = *src->keys++;
    if (dst->values != NULL) {
        *dst->values++ = *src->values++;
    }
}

static inline void
sky_list_sort_slice_copy_decr(sky_list_sort_slice_t *   dst,
                              sky_list_sort_slice_t *   src)
{
    *dst->keys-- = *src->keys--;
    if (dst->values != NULL) {
        *dst->values-- = *src->values--;
    }
}


static inline void
sky_list_sort_slice_memcpy(sky_list_sort_slice_t *  s1,
                           ssize_t                  i,
                           sky_list_sort_slice_t *  s2,
                           ssize_t                  j,
                           ssize_t                  n)
{
    memcpy(&s1->keys[i], &s2->keys[j], sizeof(sky_object_t) * n);
    if (s1->values != NULL) {
        memcpy(&s1->values[i], &s2->values[j], sizeof(sky_object_t) * n);
    }
}

static inline void
sky_list_sort_slice_memmove(sky_list_sort_slice_t * s1,
                            ssize_t                 i,
                            sky_list_sort_slice_t * s2,
                            ssize_t                 j,
                            ssize_t                 n)
{
    memmove(&s1->keys[i], &s2->keys[j], sizeof(sky_object_t) * n);
    if (s1->values != NULL) {
        memmove(&s1->values[i], &s2->values[j], sizeof(sky_object_t) * n);
    }
}

static inline void
sky_list_sort_slice_advance(sky_list_sort_slice_t *slice, ssize_t n)
{
    slice->keys += n;
    if (slice->values != NULL) {
        slice->values += n;
    }
}

/* Comparison function: sky_object_compare with SKY_COMPARE_OP_LESS.
 * Returns SKY_TRUE (1) if x < y, SKY_FALSE (0) if x >= y.
 */

#define ISLT(X, Y) (sky_object_compare(X, Y, SKY_COMPARE_OP_LESS))

/* binarysort is the best method for sorting small arrays: it does
   few compares, but can do data movement quadratic in the number of
   elements.
   [lo, hi) is a contiguous slice of a list, and is sorted via
   binary insertion.  This sort is stable.
   On entry, must have lo <= start <= hi, and that [lo, start) is already
   sorted (pass start == lo if you don't know!).
   Even in case of error, the output slice will be some permutation of
   the input (nothing is lost or duplicated).
*/
static void
sky_list_sort_binarysort(sky_list_sort_slice_t  lo,
                         sky_object_t *         hi,
                         sky_object_t *         start)
{
    register sky_object_t   *l, *p, *r;
    register sky_object_t   pivot;

    sky_error_validate_debug(lo.keys <= start && start <= hi);
    /* assert [lo, start) is sorted */
    if (lo.keys == start) {
        ++start;
    }
    for (; start < hi; ++start) {
        /* set l to where *start belongs */
        l = lo.keys;
        r = start;
        pivot = *r;
        /* Invariants:
         * pivot >= all in [lo, l).
         * pivot  < all in [r, start).
         * The second is vacuously true at the start.
         */
        sky_error_validate_debug(l < r);
        do {
            p = l + ((r - l) >> 1);
            if (ISLT(pivot, *p)) {
                r = p;
            }
            else {
                l = p+1;
            }
        } while (l < r);
        sky_error_validate_debug(l == r);
        /* The invariants still hold, so pivot >= all in [lo, l) and
           pivot < all in [l, start), so pivot belongs at l.  Note
           that if there are elements equal to pivot, l points to the
           first slot after them -- that's why this sort is stable.
           Slide over to make room.
           Caution: using memmove is much slower under MSVC 5;
           we're not usually moving many slots. */
        for (p = start; p > l; --p) {
            *p = *(p-1);
        }
        *l = pivot;
        if (lo.values != NULL) {
            ssize_t offset = lo.values - lo.keys;
            p = start + offset;
            pivot = *p;
            l += offset;
            for (p = start + offset; p > l; --p) {
                *p = *(p-1);
            }
            *l = pivot;
        }
    }
}

/*
Return the length of the run beginning at lo, in the slice [lo, hi).  lo < hi
is required on entry.  "A run" is the longest ascending sequence, with

    lo[0] <= lo[1] <= lo[2] <= ...

or the longest descending sequence, with

    lo[0] > lo[1] > lo[2] > ...

Boolean *descending is set to 0 in the former case, or to 1 in the latter.
For its intended use in a stable mergesort, the strictness of the defn of
"descending" is needed so that the caller can safely reverse a descending
sequence without violating stability (strict > ensures there are no equal
elements to get out of order).
*/
static ssize_t
sky_list_sort_count_run(sky_object_t *  lo,
                        sky_object_t *  hi,
                        sky_bool_t *    descending)
{
    ssize_t n;

    sky_error_validate_debug(lo < hi);
    *descending = SKY_FALSE;
    ++lo;
    if (lo == hi) {
        return 1;
    }

    n = 2;
    if (ISLT(*lo, *(lo-1))) {
        *descending = SKY_TRUE;
        for (lo = lo+1; lo < hi; ++lo, ++n) {
            if (!ISLT(*lo, *(lo-1))) {
                break;
            }
        }
    }
    else {
        for (lo = lo+1; lo < hi; ++lo, ++n) {
            if (ISLT(*lo, *(lo-1))) {
                break;
            }
        }
    }

    return n;
}

/*
Locate the proper position of key in a sorted vector; if the vector contains
an element equal to key, return the position immediately to the left of
the leftmost equal element.  [gallop_right() does the same except returns
the position to the right of the rightmost equal element (if any).]

"a" is a sorted vector with n elements, starting at a[0].  n must be > 0.

"hint" is an index at which to begin the search, 0 <= hint < n.  The closer
hint is to the final result, the faster this runs.

The return value is the int k in 0..n such that

    a[k-1] < key <= a[k]

pretending that *(a-1) is minus infinity and a[n] is plus infinity.  IOW,
key belongs at index k; or, IOW, the first k elements of a should precede
key, and the last n-k should follow key.

See listsort.txt for info on the method.
*/
static ssize_t
sky_list_sort_gallop_left(sky_object_t  key,
                          sky_object_t *a,
                          ssize_t       n,
                          ssize_t       hint)
{
    ssize_t ofs;
    ssize_t lastofs;
    ssize_t k;

    sky_error_validate_debug(key && a && n > 0 && hint >= 0 && hint < n);

    a += hint;
    lastofs = 0;
    ofs = 1;
    if (ISLT(*a, key)) {
        /* a[hint] < key -- gallop right, until
         * a[hint + lastofs] < key <= a[hint + ofs]
         */
        const ssize_t maxofs = n - hint;        /* &a[n-1] is highest */
        while (ofs < maxofs) {
            if (ISLT(a[ofs], key)) {
                lastofs = ofs;
                ofs = (ofs << 1) + 1;
                if (ofs <= 0) {                 /* int overflow */
                    ofs = maxofs;
                }
            }
            else {              /* key <= a[hint + ofs] */
                break;
            }
        }
        if (ofs > maxofs) {
            ofs = maxofs;
        }
        /* Translate back to offsets relative to &a[0]. */
        lastofs += hint;
        ofs += hint;
    }
    else {
        /* key <= a[hint] -- gallop left, until
         * a[hint - ofs] < key <= a[hint - lastofs]
         */
        const ssize_t maxofs = hint + 1;        /* &a[0] is lowest */
        while (ofs < maxofs) {
            if (ISLT(*(a-ofs), key)) {
                break;
            }
            /* key <= a[hint - ofs] */
            lastofs = ofs;
            ofs = (ofs << 1) + 1;
            if (ofs <= 0) {             /* int overflow */
                ofs = maxofs;
            }
        }
        if (ofs > maxofs) {
            ofs = maxofs;
        }
        /* Translate back to positive offsets relative to &a[0]. */
        k = lastofs;
        lastofs = hint - ofs;
        ofs = hint - k;
    }
    a -= hint;

    sky_error_validate_debug(-1 <= lastofs && lastofs < ofs && ofs <= n);
    /* Now a[lastofs] < key <= a[ofs], so key belongs somewhere to the
     * right of lastofs but no farther right than ofs.  Do a binary
     * search, with invariant a[lastofs-1] < key <= a[ofs].
     */
    ++lastofs;
    while (lastofs < ofs) {
        ssize_t m = lastofs + ((ofs - lastofs) >> 1);

        if (ISLT(a[m], key)) {
            lastofs = m+1;              /* a[m] < key */
        }
        else {
            ofs = m;                    /* key <= a[m] */
        }
    }
    sky_error_validate_debug(lastofs == ofs); /* so a[ofs-1] < key <= a[ofs] */
    return ofs;
}

/*
Exactly like gallop_left(), except that if key already exists in a[0:n],
finds the position immediately to the right of the rightmost equal value.

The return value is the int k in 0..n such that

    a[k-1] <= key < a[k]

The code duplication is massive, but this is enough different given that
we're sticking to "<" comparisons that it's much harder to follow if
written as one routine with yet another "left or right?" flag.
*/
static ssize_t
sky_list_sort_gallop_right(sky_object_t     key,
                           sky_object_t *   a,
                           ssize_t          n,
                           ssize_t          hint)
{
    ssize_t ofs;
    ssize_t lastofs;
    ssize_t k;

    sky_error_validate_debug(key && a && n > 0 && hint >= 0 && hint < n);

    a += hint;
    lastofs = 0;
    ofs = 1;
    if (ISLT(key, *a)) {
        /* key < a[hint] -- gallop left, until
         * a[hint - ofs] <= key < a[hint - lastofs]
         */
        const ssize_t maxofs = hint + 1;        /* &a[0] is lowest */
        while (ofs < maxofs) {
            if (ISLT(key, *(a-ofs))) {
                lastofs = ofs;
                ofs = (ofs << 1) + 1;
                if (ofs <= 0) {                 /* int overflow */
                    ofs = maxofs;
                }
            }
            else {              /* a[hint - ofs] <= key */
                break;
            }
        }
        if (ofs > maxofs) {
            ofs = maxofs;
        }
        /* Translate back to positive offsets relative to &a[0]. */
        k = lastofs;
        lastofs = hint - ofs;
        ofs = hint - k;
    }
    else {
        /* a[hint] <= key -- gallop right, until
         * a[hint + lastofs] <= key < a[hint + ofs]
        */
        const ssize_t maxofs = n - hint;        /* &a[n-1] is highest */
        while (ofs < maxofs) {
            if (ISLT(key, a[ofs])) {
                break;
            }
            /* a[hint + ofs] <= key */
            lastofs = ofs;
            ofs = (ofs << 1) + 1;
            if (ofs <= 0) {             /* int overflow */
                ofs = maxofs;
            }
        }
        if (ofs > maxofs) {
            ofs = maxofs;
        }
        /* Translate back to offsets relative to &a[0]. */
        lastofs += hint;
        ofs += hint;
    }
    a -= hint;

    sky_error_validate_debug(-1 <= lastofs && lastofs < ofs && ofs <= n);
    /* Now a[lastofs] <= key < a[ofs], so key belongs somewhere to the
     * right of lastofs but no farther right than ofs.  Do a binary
     * search, with invariant a[lastofs-1] <= key < a[ofs].
     */
    ++lastofs;
    while (lastofs < ofs) {
        ssize_t m = lastofs + ((ofs - lastofs) >> 1);

        if (ISLT(key, a[m])) {
            ofs = m;                    /* key < a[m] */
        }
        else {
            lastofs = m+1;              /* a[m] <= key */
        }
    }
    sky_error_validate_debug(lastofs == ofs); /* so a[ofs-1] <= key < a[ofs] */
    return ofs;
}

/* The maximum number of entries in a MergeState's pending-runs stack.
 * This is enough to sort arrays of size up to about
 *     32 * phi ** MAX_MERGE_PENDING
 * where phi ~= 1.618.  85 is ridiculouslylarge enough, good for an array
 * with 2**64 elements.
 */
#define MAX_MERGE_PENDING 85

/* When we get into galloping mode, we stay there until both runs win less
 * often than MIN_GALLOP consecutive times.  See listsort.txt for more info.
 */
#define MIN_GALLOP 7

/* Avoid malloc for small temp arrays. */
#define MERGESTATE_TEMP_SIZE 256

/* One MergeState exists on the stack per invocation of mergesort.  It's just
 * a convenient way to pass state around among the helper functions.
 */
struct s_slice {
    sky_list_sort_slice_t               base;
    ssize_t                             len;
};

typedef struct sky_list_sort_MergeState_s {
    /* This controls when we get *into* galloping mode.  It's initialized
     * to MIN_GALLOP.  merge_lo and merge_hi tend to nudge it higher for
     * random data, and lower for highly structured data.
     */
    ssize_t min_gallop;

    /* 'a' is temp storage to help with merges.  It contains room for
     * alloced entries.
     */
    sky_list_sort_slice_t a;        /* may point to temparray below */
    ssize_t alloced;

    /* A stack of n pending runs yet to be merged.  Run #i starts at
     * address base[i] and extends for len[i] elements.  It's always
     * true (so long as the indices are in bounds) that
     *
     *     pending[i].base + pending[i].len == pending[i+1].base
     *
     * so we could cut the storage for this, but it's a minor amount,
     * and keeping all the info explicit simplifies the code.
     */
    int n;
    struct s_slice pending[MAX_MERGE_PENDING];

    /* 'a' points to this when possible, rather than muck with malloc. */
    sky_object_t temparray[MERGESTATE_TEMP_SIZE];
} sky_list_sort_MergeState_t;

/* Conceptually a MergeState's constructor. */
static void
sky_list_sort_merge_init(sky_list_sort_MergeState_t *   ms,
                         ssize_t                        list_size,
                         sky_bool_t                     has_keyfunc)
{
    sky_error_validate_debug(ms != NULL);
    if (has_keyfunc) {
        /* The temporary space for merging will need at most half the list
         * size rounded up.  Use the minimum possible space so we can use the
         * rest of temparray for other things.  In particular, if there is
         * enough extra space, listsort() will use it to store the keys.
         */
        ms->alloced = (list_size + 1) / 2;

        /* ms->alloced describes how many keys will be stored at
           ms->temparray, but we also need to store the values.  Hence,
           ms->alloced is capped at half of MERGESTATE_TEMP_SIZE. */
        if (MERGESTATE_TEMP_SIZE / 2 < ms->alloced) {
            ms->alloced = MERGESTATE_TEMP_SIZE / 2;
        }
        ms->a.values = &ms->temparray[ms->alloced];
    }
    else {
        ms->alloced = MERGESTATE_TEMP_SIZE;
        ms->a.values = NULL;
    }
    ms->a.keys = ms->temparray;
    ms->n = 0;
    ms->min_gallop = MIN_GALLOP;
}

/* Free all the temp memory owned by the MergeState.  This must be called
 * when you're done with a MergeState, and may be called before then if
 * you want to free the temp memory early.
 */
static void
sky_list_sort_merge_freemem(sky_list_sort_MergeState_t *ms)
{
    sky_error_validate_debug(ms != NULL);
    if (ms->a.keys != ms->temparray) {
        sky_free(ms->a.keys);
    }
}

/* Ensure enough temp memory for 'need' array slots is available.
 */
static void
sky_list_sort_merge_getmem(sky_list_sort_MergeState_t *ms, ssize_t need)
{
    int multiplier;

    sky_error_validate_debug(ms != NULL);
    if (need <= ms->alloced) {
        return;
    }

    multiplier = (ms->a.values != NULL ? 2 : 1);

    /* Don't realloc!  That can cost cycles to copy the old data, but
     * we don't care what's in the block.
     */
    sky_list_sort_merge_freemem(ms);
    if ((size_t)need > SSIZE_MAX / sizeof(sky_object_t) / multiplier) {
        sky_error_raise_string(sky_MemoryError, "out of memory");
    }
    ms->a.keys = sky_malloc(multiplier * need * sizeof(sky_object_t));
    ms->alloced = need;
    if (ms->a.values != NULL) {
        ms->a.values = &ms->a.keys[need];
    }
}
#define SKY_LIST_SORT_MERGE_GETMEM(MS, NEED)        \
    do {                                            \
        if ((NEED) > (MS)->alloced) {               \
            sky_list_sort_merge_getmem(MS, NEED);   \
        }                                           \
    } while (0)

/* Merge the na elements starting at ssa with the nb elements starting at
 * ssb.keys = ssa.keys + na in a stable way, in-place.  na and nb must be > 0.
 * Must also have that ssa.keys[na-1] belongs at the end of the merge, and
 * should have na <= nb.  See listsort.txt for more info.
 */
static void
sky_list_sort_merge_lo(sky_list_sort_MergeState_t * ms,
                       sky_list_sort_slice_t        ssa,
                       ssize_t                      na,
                       sky_list_sort_slice_t        ssb,
                       ssize_t                      nb)
{
    ssize_t k;
    sky_list_sort_slice_t dest;
    ssize_t min_gallop;

    sky_error_validate_debug(ms && ssa.keys && ssb.keys && na > 0 && nb > 0);
    sky_error_validate_debug(ssa.keys + na == ssb.keys);
    SKY_LIST_SORT_MERGE_GETMEM(ms, na);
    sky_list_sort_slice_memcpy(&ms->a, 0, &ssa, 0, na);
    dest = ssa;
    ssa = ms->a;

    sky_list_sort_slice_copy_incr(&dest, &ssb);
    --nb;
    if (nb == 0) {
        goto Succeed;
    }
    if (na == 1) {
        goto CopyB;
    }

    min_gallop = ms->min_gallop;
    for (;;) {
        ssize_t acount = 0;             /* # of times A won in a row */
        ssize_t bcount = 0;             /* # of times B won in a row */

        /* Do the straightforward thing until (if ever) one run
         * appears to win consistently.
         */
        for (;;) {
            sky_error_validate_debug(na > 1 && nb > 0);
            k = ISLT(ssb.keys[0], ssa.keys[0]);
            if (k) {
                sky_list_sort_slice_copy_incr(&dest, &ssb);
                ++bcount;
                acount = 0;
                --nb;
                if (nb == 0) {
                    goto Succeed;
                }
                if (bcount >= min_gallop) {
                    break;
                }
            }
            else {
                sky_list_sort_slice_copy_incr(&dest, &ssa);
                ++acount;
                bcount = 0;
                --na;
                if (na == 1) {
                    goto CopyB;
                }
                if (acount >= min_gallop) {
                    break;
                }
            }
        }

        /* One run is winning so consistently that galloping may
         * be a huge win.  So try that, and continue galloping until
         * (if ever) neither run appears to be winning consistently
         * anymore.
         */
        ++min_gallop;
        do {
            sky_error_validate_debug(na > 1 && nb > 0);
            min_gallop -= min_gallop > 1;
            ms->min_gallop = min_gallop;
            k = sky_list_sort_gallop_right(ssb.keys[0], ssa.keys, na, 0);
            acount = k;
            if (k) {
                sky_list_sort_slice_memcpy(&dest, 0, &ssa, 0, k);
                sky_list_sort_slice_advance(&dest, k);
                sky_list_sort_slice_advance(&ssa, k);
                na -= k;
                if (na == 1) {
                    goto CopyB;
                }
                /* na==0 is impossible now if the comparison
                 * function is consistent, but we can't assume
                 * that it is.
                 */
                if (na == 0) {
                    goto Succeed;
                }
            }
            sky_list_sort_slice_copy_incr(&dest, &ssb);
            --nb;
            if (nb == 0) {
                goto Succeed;
            }

            k = sky_list_sort_gallop_left(ssa.keys[0], ssb.keys, nb, 0);
            bcount = k;
            if (k) {
                sky_list_sort_slice_memmove(&dest, 0, &ssb, 0, k);
                sky_list_sort_slice_advance(&dest, k);
                sky_list_sort_slice_advance(&ssb, k);
                nb -= k;
                if (nb == 0) {
                    goto Succeed;
                }
            }
            sky_list_sort_slice_copy_incr(&dest, &ssa);
            --na;
            if (na == 1) {
                goto CopyB;
            }
        } while (acount >= MIN_GALLOP || bcount >= MIN_GALLOP);
        ++min_gallop;           /* penalize it for leaving galloping mode */
        ms->min_gallop = min_gallop;
    }
Succeed:
    if (na) {
        sky_list_sort_slice_memcpy(&dest, 0, &ssa, 0, na);
    }
    return;
CopyB:
    sky_error_validate_debug(na == 1 && nb > 0);
    /* The last element of ssa belongs at the end of the merge. */
    sky_list_sort_slice_memmove(&dest, 0, &ssb, 0, nb);
    sky_list_sort_slice_copy(&dest, nb, &ssa, 0);
}

/* Merge the na elements starting at pa with the nb elements starting at
 * ssb.keys = ssa.keys + na in a stable way, in-place.  na and nb must be > 0.
 * Must also have that ssa.keys[na-1] belongs at the end of the merge, and
 * should have na >= nb.  See listsort.txt for more info.
 */
static void
sky_list_sort_merge_hi(sky_list_sort_MergeState_t * ms,
                       sky_list_sort_slice_t        ssa,
                       ssize_t                      na,
                       sky_list_sort_slice_t        ssb,
                       ssize_t                      nb)
{
    ssize_t k;
    sky_list_sort_slice_t dest, basea, baseb;
    ssize_t min_gallop;

    sky_error_validate_debug(ms && ssa.keys && ssb.keys && na > 0 && nb > 0);
    sky_error_validate_debug(ssa.keys + na == ssb.keys);
    SKY_LIST_SORT_MERGE_GETMEM(ms, nb);
    dest = ssb;
    sky_list_sort_slice_advance(&dest, nb-1);
    sky_list_sort_slice_memcpy(&ms->a, 0, &ssb, 0, nb);
    basea = ssa;
    baseb = ms->a;
    ssb.keys = ms->a.keys + nb - 1;
    if (ssb.values != NULL) {
        ssb.values = ms->a.values + nb - 1;
    }
    sky_list_sort_slice_advance(&ssa, na - 1);

    sky_list_sort_slice_copy_decr(&dest, &ssa);
    --na;
    if (na == 0) {
        goto Succeed;
    }
    if (nb == 1) {
        goto CopyA;
    }

    min_gallop = ms->min_gallop;
    for (;;) {
        ssize_t acount = 0;             /* # of times A won in a row */
        ssize_t bcount = 0;             /* # of times B won in a row */

        /* Do the straightforward thing until (if ever) one run
         * appears to win consistently.
         */
        for (;;) {
            sky_error_validate_debug(na > 0 && nb > 1);
            k = ISLT(ssb.keys[0], ssa.keys[0]);
            if (k) {
                sky_list_sort_slice_copy_decr(&dest, &ssa);
                ++acount;
                bcount = 0;
                --na;
                if (na == 0) {
                    goto Succeed;
                }
                if (acount >= min_gallop) {
                    break;
                }
            }
            else {
                sky_list_sort_slice_copy_decr(&dest, &ssb);
                ++bcount;
                acount = 0;
                --nb;
                if (nb == 1) {
                    goto CopyA;
                }
                if (bcount >= min_gallop) {
                    break;
                }
            }
        }

        /* One run is winning so consistently that galloping may
         * be a huge win.  So try that, and continue galloping until
         * (if ever) neither run appears to be winning consistently
         * anymore.
         */
        ++min_gallop;
        do {
            sky_error_validate_debug(na > 0 && nb > 1);
            min_gallop -= min_gallop > 1;
            ms->min_gallop = min_gallop;
            k = sky_list_sort_gallop_right(ssb.keys[0], basea.keys, na, na-1);
            k = na - k;
            acount = k;
            if (k) {
                sky_list_sort_slice_advance(&dest, -k);
                sky_list_sort_slice_advance(&ssa, -k);
                sky_list_sort_slice_memmove(&dest, 1, &ssa, 1, k);
                na -= k;
                if (na == 0) {
                    goto Succeed;
                }
            }
            sky_list_sort_slice_copy_decr(&dest, &ssb);
            --nb;
            if (nb == 1) {
                goto CopyA;
            }

            k = sky_list_sort_gallop_left(ssa.keys[0], baseb.keys, nb, nb-1);
            k = nb - k;
            bcount = k;
            if (k) {
                sky_list_sort_slice_advance(&dest, -k);
                sky_list_sort_slice_advance(&ssb, -k);
                sky_list_sort_slice_memcpy(&dest, 1, &ssb, 1, k);
                nb -= k;
                if (nb == 1) {
                    goto CopyA;
                }
                /* nb==0 is impossible now if the comparison
                 * function is consistent, but we can't assume
                 * that it is.
                 */
                if (nb == 0) {
                    goto Succeed;
                }
            }
            sky_list_sort_slice_copy_decr(&dest, &ssa);
            --na;
            if (na == 0) {
                goto Succeed;
            }
        } while (acount >= MIN_GALLOP || bcount >= MIN_GALLOP);
        ++min_gallop;           /* penalize it for leaving galloping mode */
        ms->min_gallop = min_gallop;
    }
Succeed:
    if (nb) {
        sky_list_sort_slice_memcpy(&dest, -(nb-1), &baseb, 0, nb);
    }
    return;
CopyA:
    sky_error_validate_debug(nb == 1 && na > 0);
    /* The first element of ssb belongs at the front of the merge. */
    sky_list_sort_slice_memmove(&dest, 1-na, &ssa, 1-na, na);
    sky_list_sort_slice_advance(&dest, -na);
    sky_list_sort_slice_advance(&ssa, -na);
    sky_list_sort_slice_copy(&dest, 0, &ssb, 0);
}

/* Merge the two runs at stack indices i and i+1.
 */
static void
sky_list_sort_merge_at(sky_list_sort_MergeState_t *ms, ssize_t i)
{
    sky_list_sort_slice_t ssa, ssb;
    ssize_t na, nb;
    ssize_t k;

    sky_error_validate_debug(ms != NULL);
    sky_error_validate_debug(ms->n >= 2);
    sky_error_validate_debug(i >= 0);
    sky_error_validate_debug(i == ms->n - 2 || i == ms->n - 3);

    ssa = ms->pending[i].base;
    na = ms->pending[i].len;
    ssb = ms->pending[i+1].base;
    nb = ms->pending[i+1].len;
    sky_error_validate_debug(na > 0 && nb > 0);
    sky_error_validate_debug(ssa.keys + na == ssb.keys);

    /* Record the length of the combined runs; if i is the 3rd-last
     * run now, also slide over the last run (which isn't involved
     * in this merge).  The current run i+1 goes away in any case.
     */
    ms->pending[i].len = na + nb;
    if (i == ms->n - 3) {
        ms->pending[i+1] = ms->pending[i+2];
    }
    --ms->n;

    /* Where does b start in a?  Elements in a before that can be
     * ignored (already in place).
     */
    k = sky_list_sort_gallop_right(*ssb.keys, ssa.keys, na, 0);
    sky_list_sort_slice_advance(&ssa, k);
    na -= k;
    if (na == 0) {
        return;
    }

    /* Where does a end in b?  Elements in b after that can be
     * ignored (already in place).
     */
    nb = sky_list_sort_gallop_left(ssa.keys[na-1], ssb.keys, nb, nb-1);
    if (nb <= 0) {
        return;
    }

    /* Merge what remains of the runs, using a temp array with
     * min(na, nb) elements.
     */
    if (na <= nb) {
        sky_list_sort_merge_lo(ms, ssa, na, ssb, nb);
    }
    else {
        sky_list_sort_merge_hi(ms, ssa, na, ssb, nb);
    }
}

/* Examine the stack of runs waiting to be merged, merging adjacent runs
 * until the stack invariants are re-established:
 *
 * 1. len[-3] > len[-2] + len[-1]
 * 2. len[-2] > len[-1]
 *
 * See listsort.txt for more info.
 */
static void
sky_list_sort_merge_collapse(sky_list_sort_MergeState_t *ms)
{
    struct s_slice *p = ms->pending;

    sky_error_validate_debug(ms);
    while (ms->n > 1) {
        ssize_t n = ms->n - 2;
        if (n > 0 && p[n-1].len <= p[n].len + p[n+1].len) {
            if (p[n-1].len < p[n+1].len) {
                --n;
            }
            sky_list_sort_merge_at(ms, n);
        }
        else if (p[n].len <= p[n+1].len) {
            sky_list_sort_merge_at(ms, n);
        }
        else {
            break;
        }
    }
}

/* Regardless of invariants, merge all runs on the stack until only one
 * remains.  This is used at the end of the mergesort.
 */
static void
sky_list_sort_merge_force_collapse(sky_list_sort_MergeState_t *ms)
{
    struct s_slice *p = ms->pending;

    sky_error_validate_debug(ms);
    while (ms->n > 1) {
        ssize_t n = ms->n - 2;
        if (n > 0 && p[n-1].len < p[n+1].len) {
            --n;
        }
        sky_list_sort_merge_at(ms, n);
    }
}

/* Compute a good value for the minimum run length; natural runs shorter
 * than this are boosted artificially via binary insertion.
 *
 * If n < 64, return n (it's too small to bother with fancy stuff).
 * Else if n is an exact power of 2, return 32.
 * Else return an int k, 32 <= k <= 64, such that n/k is close to, but
 * strictly less than, an exact power of 2.
 *
 * See listsort.txt for more info.
 */
static ssize_t
sky_list_sort_merge_compute_minrun(ssize_t n)
{
    ssize_t r = 0;              /* becomes 1 if any 1 bits are shifted off */

    sky_error_validate_debug(n >= 0);
    while (n >= 64) {
        r |= n & 1;
        n >>= 1;
    }
    return n + r;
}

/* Reverse a slice of a list in place, from lo up to (exclusive) hi. */
static void
sky_list_sort_reverse_slice(sky_object_t *lo, sky_object_t *hi)
{
    sky_error_validate_debug(lo && hi);

    --hi;
    while (lo < hi) {
        sky_object_t t = *lo;
        *lo = *hi;
        *hi = t;
        ++lo;
        --hi;
    }
}

static void
sky_list_sort_reverse_sort_slice(sky_list_sort_slice_t *s, ssize_t n)
{
    sky_list_sort_reverse_slice(s->keys, &(s->keys[n]));
    if (s->values != NULL) {
        sky_list_sort_reverse_slice(s->values, &(s->values[n]));
    }
}


typedef struct sky_list_sort_restore_s {
    sky_list_t                          list;
    sky_list_data_t *                   list_data;
    sky_list_table_t *                  saved_table;
} sky_list_sort_restore_t;

static void
sky_list_sort_restore(void *ptr)
{
    sky_list_sort_restore_t *restore = ptr;

    sky_object_gc_setarray(restore->saved_table->len,
                           restore->saved_table->objects,
                           restore->list);

    sky_list_data_settable(restore->list_data,
                           restore->saved_table,
                           sky_list_isshared(restore->list));
}

/* An adaptive, stable, natural mergesort.  See listsort.txt.
 * Returns sky_None on success, NULL on error.  Even in case of error, the
 * list will be some permutation of its input state (nothing is lost or
 * duplicated).
 */
void
sky_list_sort(sky_list_t self, sky_object_t key, sky_bool_t reverse)
{
    sky_list_data_t *self_data = sky_list_data(self);

    ssize_t                     i, minrun, nremaining;
    sky_object_t                *keys;
    sky_list_table_t            *empty_table, *table;
    sky_list_sort_slice_t       lo;
    sky_list_sort_restore_t     restore;
    sky_list_sort_MergeState_t  ms;

    if (sky_object_isnull(key)) {
        key = NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        /* The list is temporarily made empty, so that mutations performed
         * by comparison functions can't affect the slice of memory we're
         * sorting (allowing mutations during sorting is a core-dump
         * factory, since ob_item may change).
         */
        empty_table = sky_calloc(1, sizeof(sky_list_table_t));
        table = sky_atomic_exchange(empty_table,
                                    SKY_AS_VOIDP(&(self_data->table)));

        restore.list = self;
        restore.list_data = self_data;
        restore.saved_table = table;
        sky_asset_save(&restore,
                       sky_list_sort_restore,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (!key) {
            keys = NULL;
            lo.keys = table->objects;
            lo.values = NULL;
        }
        else {
            if (table->len < MERGESTATE_TEMP_SIZE/2) {
                /* Leverage stack space we allocated but won't otherwise use */
                keys = &(ms.temparray[table->len+1]);
            }
            else {
                keys = sky_asset_malloc(sizeof(sky_object_t) * table->len,
                                        SKY_ASSET_CLEANUP_ALWAYS);
            }

            for (i = 0; i < table->len ; i++) {
                keys[i] = sky_object_call(key,
                                          sky_tuple_pack(1, table->objects[i]),
                                          NULL);
            }

            lo.keys = keys;
            lo.values = table->objects;
        }

        sky_list_sort_merge_init(&ms, table->len, keys != NULL);
        sky_asset_save(&ms,
                       (sky_free_t)sky_list_sort_merge_freemem,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nremaining = table->len;
        if (nremaining < 2) {
            goto succeed;
        }

        /* Reverse sort stability achieved by initially reversing the list,
        applying a stable forward sort, then reversing the final result. */
        if (reverse) {
            if (keys) {
                sky_list_sort_reverse_slice(&keys[0], &keys[table->len]);
            }
            sky_list_sort_reverse_slice(&table->objects[0],
                                        &table->objects[table->len]);
        }

        /* March over the array once, left to right, finding natural runs,
         * and extending short natural runs to minrun elements.
         */
        minrun = sky_list_sort_merge_compute_minrun(nremaining);
        do {
            int descending;
            ssize_t n;

            /* Identify next run. */
            n = sky_list_sort_count_run(lo.keys,
                                        lo.keys + nremaining,
                                        &descending);
            if (descending) {
                sky_list_sort_reverse_sort_slice(&lo, n);
            }
            /* If short, extend to min(minrun, nremaining). */
            if (n < minrun) {
                const ssize_t force = (nremaining <= minrun ? nremaining
                                                            : minrun);
                sky_list_sort_binarysort(lo, lo.keys + force, lo.keys + n);
                n = force;
            }
            /* Push run onto pending-runs stack, and maybe merge. */
            sky_error_validate_debug(ms.n < MAX_MERGE_PENDING);
            ms.pending[ms.n].base = lo;
            ms.pending[ms.n].len = n;
            ++ms.n;
            sky_list_sort_merge_collapse(&ms);
            /* Advance to find next run. */
            sky_list_sort_slice_advance(&lo, n);
            nremaining -= n;
        } while (nremaining);

        sky_list_sort_merge_force_collapse(&ms);
        sky_error_validate_debug(ms.n == 1);
        sky_error_validate_debug(keys == NULL
               ? ms.pending[0].base.keys == table->objects
               : ms.pending[0].base.keys == &keys[0]);
        sky_error_validate_debug(ms.pending[0].len == table->len);
        lo = ms.pending[0].base;

succeed:
        if (reverse && table->len > 1) {
            sky_list_sort_reverse_slice(table->objects,
                                        table->objects + table->len);
        }
        if (self_data->table != empty_table || empty_table->len) {
            sky_error_raise_string(sky_ValueError,
                                   "list modified during sort");
        }
    } SKY_ASSET_BLOCK_END;
}
#undef ISLT
