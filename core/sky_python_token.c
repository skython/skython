#include "sky_private.h"
#include "sky_python_token.h"


const char *
sky_python_token_name(sky_python_token_name_t token)
{
    switch (token) {
        case SKY_PYTHON_TOKEN_ERROR:                    return "ERROR";
        case SKY_PYTHON_TOKEN_ENDMARKER:                return "ENDMARKER";

        case SKY_PYTHON_TOKEN_INDENT:                   return "INDENT";
        case SKY_PYTHON_TOKEN_DEDENT:                   return "DEDENT";
        case SKY_PYTHON_TOKEN_NEWLINE:                  return "NEWLINE";
        case SKY_PYTHON_TOKEN_SEMICOLON:                return ";";

        case SKY_PYTHON_TOKEN_INTEGER:                  return "INTEGER";
        case SKY_PYTHON_TOKEN_DECIMAL:                  return "DECIMAL";
        case SKY_PYTHON_TOKEN_IMAGINARY:                return "IMAGINARY";
        case SKY_PYTHON_TOKEN_STRING:                   return "STRING";
        case SKY_PYTHON_TOKEN_BYTES:                    return "BYTES";

        case SKY_PYTHON_TOKEN_EQUALS:                   return "=";
        case SKY_PYTHON_TOKEN_EQUALS_EQUALS:            return "==";
        case SKY_PYTHON_TOKEN_BANG_EQUALS:              return "!=";

        case SKY_PYTHON_TOKEN_LESS:                     return "<";
        case SKY_PYTHON_TOKEN_LESS_EQUALS:              return "<=";
        case SKY_PYTHON_TOKEN_LESS_LESS:                return "<<";
        case SKY_PYTHON_TOKEN_LESS_LESS_EQUALS:         return "<<=";

        case SKY_PYTHON_TOKEN_GREATER:                  return ">";
        case SKY_PYTHON_TOKEN_GREATER_EQUALS:           return ">=";
        case SKY_PYTHON_TOKEN_GREATER_GREATER:          return ">>";
        case SKY_PYTHON_TOKEN_GREATER_GREATER_EQUALS:   return ">>=";

        case SKY_PYTHON_TOKEN_PLUS:                     return "+";
        case SKY_PYTHON_TOKEN_PLUS_EQUALS:              return "+=";

        case SKY_PYTHON_TOKEN_MINUS:                    return "-";
        case SKY_PYTHON_TOKEN_MINUS_EQUALS:             return "-=";
        case SKY_PYTHON_TOKEN_MINUS_GREATER:            return "->";

        case SKY_PYTHON_TOKEN_STAR:                     return "*";
        case SKY_PYTHON_TOKEN_STAR_EQUALS:              return "*=";
        case SKY_PYTHON_TOKEN_STAR_STAR:                return "**";
        case SKY_PYTHON_TOKEN_STAR_STAR_EQUALS:         return "**=";

        case SKY_PYTHON_TOKEN_SLASH:                    return "/";
        case SKY_PYTHON_TOKEN_SLASH_EQUALS:             return "/=";
        case SKY_PYTHON_TOKEN_SLASH_SLASH:              return "//";
        case SKY_PYTHON_TOKEN_SLASH_SLASH_EQUALS:       return "//=";

        case SKY_PYTHON_TOKEN_PERCENT:                  return "%";
        case SKY_PYTHON_TOKEN_PERCENT_EQUALS:           return "%=";

        case SKY_PYTHON_TOKEN_AMPERSAND:                return "&";
        case SKY_PYTHON_TOKEN_AMPERSAND_EQUALS:         return "&=";
        case SKY_PYTHON_TOKEN_PIPE:                     return "|";
        case SKY_PYTHON_TOKEN_PIPE_EQUALS:              return "|=";
        case SKY_PYTHON_TOKEN_CARET:                    return "^";
        case SKY_PYTHON_TOKEN_CARET_EQUALS:             return "^=";

        case SKY_PYTHON_TOKEN_TILDE:                    return "~";

        case SKY_PYTHON_TOKEN_LPAREN:                   return "(";
        case SKY_PYTHON_TOKEN_RPAREN:                   return ")";
        case SKY_PYTHON_TOKEN_LBRACKET:                 return "[";
        case SKY_PYTHON_TOKEN_RBRACKET:                 return "]";
        case SKY_PYTHON_TOKEN_LBRACE:                   return "{";
        case SKY_PYTHON_TOKEN_RBRACE:                   return "}";
        case SKY_PYTHON_TOKEN_COMMA:                    return ",";
        case SKY_PYTHON_TOKEN_COLON:                    return ":";
        case SKY_PYTHON_TOKEN_DOT:                      return ".";
        case SKY_PYTHON_TOKEN_ELLIPSIS:                 return "...";
        case SKY_PYTHON_TOKEN_AT:                       return "@";

        case SKY_PYTHON_TOKEN_NAME:                     return "NAME";
        case SKY_PYTHON_TOKEN_AND:                      return "and";
        case SKY_PYTHON_TOKEN_AS:                       return "as";
        case SKY_PYTHON_TOKEN_ASSERT:                   return "assert";
        case SKY_PYTHON_TOKEN_BREAK:                    return "break";
        case SKY_PYTHON_TOKEN_CLASS:                    return "class";
        case SKY_PYTHON_TOKEN_CONTINUE:                 return "continue";
        case SKY_PYTHON_TOKEN_DEF:                      return "def";
        case SKY_PYTHON_TOKEN_DEL:                      return "del";
        case SKY_PYTHON_TOKEN_ELIF:                     return "elif";
        case SKY_PYTHON_TOKEN_ELSE:                     return "else";
        case SKY_PYTHON_TOKEN_EXCEPT:                   return "except";
        case SKY_PYTHON_TOKEN_FALSE:                    return "False";
        case SKY_PYTHON_TOKEN_FINALLY:                  return "finally";
        case SKY_PYTHON_TOKEN_FOR:                      return "for";
        case SKY_PYTHON_TOKEN_FROM:                     return "from";
        case SKY_PYTHON_TOKEN_GLOBAL:                   return "global";
        case SKY_PYTHON_TOKEN_IF:                       return "if";
        case SKY_PYTHON_TOKEN_IMPORT:                   return "import";
        case SKY_PYTHON_TOKEN_IN:                       return "in";
        case SKY_PYTHON_TOKEN_IS:                       return "is";
        case SKY_PYTHON_TOKEN_LAMBDA:                   return "lambda";
        case SKY_PYTHON_TOKEN_NONE:                     return "None";
        case SKY_PYTHON_TOKEN_NONLOCAL:                 return "nonlocal";
        case SKY_PYTHON_TOKEN_NOT:                      return "not";
        case SKY_PYTHON_TOKEN_OR:                       return "or";
        case SKY_PYTHON_TOKEN_PASS:                     return "pass";
        case SKY_PYTHON_TOKEN_RAISE:                    return "raise";
        case SKY_PYTHON_TOKEN_RETURN:                   return "return";
        case SKY_PYTHON_TOKEN_TRUE:                     return "True";
        case SKY_PYTHON_TOKEN_TRY:                      return "try";
        case SKY_PYTHON_TOKEN_WHILE:                    return "while";
        case SKY_PYTHON_TOKEN_WITH:                     return "with";
        case SKY_PYTHON_TOKEN_YIELD:                    return "yield";
    }

    return "<<<ERR>>>";
}
