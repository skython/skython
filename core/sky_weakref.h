/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_weakref Weak References
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_WEAKREF_H__
#define __SKYTHON_CORE_SKY_WEAKREF_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new weak reference object instance.
  *
  * @param[in]  object      the object to be weakly referenced.
  * @param[in]  callback    an optional callable object to be called when
  *                         @a object becomes unreachable.
  * @return     a new weak reference object instance.
  */
SKY_EXTERN sky_weakref_t
sky_weakref_create(sky_object_t object, sky_object_t callback);


/** Return the object to which a weak reference object refers.
  *
  * @param[in]  ref     the weak reference object instance.
  * @return     the object to which @a ref refers, or @c NULL if the object has
  *             become unreachable.
  */
SKY_EXTERN sky_object_t
sky_weakref_object(sky_weakref_t ref);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_WEAKREF_H__ */

/** @} **/
/** @} **/
