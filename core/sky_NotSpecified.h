/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_NotSpecified The NotSpecified Object
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NOTSPECIFIED_H__
#define __SKYTHON_CORE_SKY_NOTSPECIFIED_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An object representing the NotSpecified object. **/
SKY_EXTERN sky_NotSpecified_t const sky_NotSpecified;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NOTSPECIFIED_H__ */

/** @} **/
/** @} **/
