/** @file
  * @brief
  * @defgroup sky_function Functions
  * @{
  * @defgroup sky_method Instance methods
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_METHOD_H__
#define __SKYTHON_CORE_SKY_METHOD_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new instance method object, binding a callable object to an
  * object instance.
  *
  * @param[in]  function    the callable object to bind.
  * @param[in]  self        the object instance to bind.
  * @return     the new instance method object.
  */
SKY_EXTERN sky_method_t
sky_method_create(sky_object_t function, sky_object_t self);


/** Return the function object instance that a method object is wrapping.
  *
  * @param[in]  method      the method object to query.
  * @return     the function object that @a method is wrapping.
  */
SKY_EXTERN sky_object_t
sky_method_function(sky_method_t method);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_METHOD_H__ */

/** @} **/
/** @} **/
