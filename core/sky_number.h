/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_number Number Protocol
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NUMBER_H__
#define __SKYTHON_CORE_SKY_NUMBER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Determine whether or not an object is a numeric type.
  * A type is considered numeric if it implements one of either __int__() or
  * __float__().
  *
  * @param[in]  object      the object to test.
  * @return     @c SKY_TRUE if the object's type is numeric, or @c SKY_FALSE
  *             if it is not.
  */
SKY_EXTERN sky_bool_t
sky_number_check(sky_object_t object);


/** Coerce a numeric type to a complex object.
  * This is equivalent to calling the object's __complex__() method.
  *
  * @param[in]  object      the object to coerce.
  * @return     the result of coercing @a object to a complex object, which
  *             may be @a object itself if it is already a complex object.
  */
SKY_EXTERN sky_complex_t
sky_number_complex(sky_object_t object);


/** Coerce a numeric type to a floating point object.
  * This is equivalent to calling the object's __float__() method.
  *
  * @param[in]  object      the object to coerce.
  * @return     the result of coercing @a object to a floating point object,
  *             which may be @a object itself if it is already a floating
  *             point object.
  */
SKY_EXTERN sky_float_t
sky_number_float(sky_object_t object);


/** Coerce a numeric type to an integer object.
  * This is equivalent to calling the object's __int__() method.
  *
  * @param[in]  object      the object to coerce.
  * @return     the result of coercing @a object to an integer object, which
  *             may be @a object itself if it is already an integer object.
  */
SKY_EXTERN sky_integer_t
sky_number_integer(sky_object_t object);


/** Round a floating point value.
  * Returns a floating point value rounded to the specified number of digits
  * after the decimal point. This is equivalent to calling the object's
  * __round__() method.
  *
  * @param[in]  object      the object to round.
  * @param[in]  digits      the number of digits after the decimal point.
  * @return     the result of rounding @a object to have @a digits after the
  *             decimal point.
  */
SKY_EXTERN sky_object_t
sky_number_round(sky_object_t object, sky_object_t digits);


/** Convert an object to an integer suitable for use as an index value.
  * This function may effectively coerce an object to an integer, but it should
  * only be implemented for object types that make sense to be used for
  * indexing. This is equivalent to calling the object's __index__() method.
  * See PEP 357.
  *
  * @param[in]  object      the object to be used as an index value.
  * @return     a @c sky_integer_type object instance representing the index
  *             value equivalent to @a object.
  */
SKY_EXTERN sky_integer_t
sky_number_index(sky_object_t object);


/** Return the absolute value of a number.
  * This is equivalent to calling the object's __abs__() method.
  *
  * @param[in]  object      the number for which the absolute value is to be
  *                         returned.
  * @return     a number object instance that is the absolute value of
  *             @a object.
  */
SKY_EXTERN sky_object_t
sky_number_absolute(sky_object_t object);


/** Return the inverted value of a number.
  * This is equivalent to calling the object's __invert__() method, which is
  * used to implement the unary ~ operator.
  *
  * @param[in]  object      the number for which the inverted value is to be
  *                         returned.
  * @return     a number object instance that is the inverted value of
  *             @a object.
  */
SKY_EXTERN sky_object_t
sky_number_invert(sky_object_t object);


/** Return the negative value of a number.
  * This is equivalent to calling the object's __neg__() method, which is used
  * to implement the unary - operator.
  *
  * @param[in]  object      the number for which the negative value is to be
  *                         returned.
  * @return     a number object instance that is the negative value of
  *             @a object.
  */
SKY_EXTERN sky_object_t
sky_number_negative(sky_object_t object);


/** Return the positive value of a number.
  * This is equivalent to calling the object's __pos__() method, which is used
  * to implement the unary + operator.
  *
  * @param[in]  object      the number for which the positive value is to be
  *                         returned.
  * @return     a number object instance that is the negative value of
  *             @a object.
  */
SKY_EXTERN sky_object_t
sky_number_positive(sky_object_t object);


/** Return the sum of two numbers.
  * Performs a standard binary operation using the __add__() and __radd__()
  * methods, used to implement the binary + operation.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_add(sky_object_t self, sky_object_t other);


/** Return the sum of two numbers, performing the operation in place if
  * possible.
  * Performs a standard binary operation using the __iadd__() method. If the
  * __iadd__() method is not implemented, the operation falls back to using
  * sky_number_add().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_add(sky_object_t self, sky_object_t other);


/** Return the bitwise-and of two numbers.
  * Performs a standard binary operation using the __and__() and __rand__()
  * methods, used to implement the binary & operation.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_and(sky_object_t self, sky_object_t other);


/** Return the bitwise-and of two numbers, performing the operation in place
  * if possible.
  * Performs a standard binary operation using the __iand__() method. If the
  * __iand__() method is not implemented, the operation falls back to using
  * sky_number_and().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_and(sky_object_t self, sky_object_t other);


/** Return the quotient and remainder resulting from the division of two
  * numbers.
  * Performs a standard binary operation using the __divmod__() and
  * __rdivmod__() methods, used to implement the divmod() built-in. The
  * result should always be a tuple object having two items, which are the
  * floored quotient and the remainder, in that order.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_divmod(sky_object_t self, sky_object_t other);


/** Return the floored quotient of two numbers rounded towards -infinity.
  * Performs a standard binary operation using the __floordiv__() and
  * __rfloordiv__() methods, used to implement the // operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_floordivide(sky_object_t self, sky_object_t other);


/** Return the floored quotient of two numbers rounded towards -infinity,
  * performing the operation in place if possible.
  * Performs a standard binary operation using the __ifloordiv__() method.
  * If the __ifloordiv__() method is not implemented, the operation falls
  * back to using sky_number_floordiv().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_floordivide(sky_object_t self, sky_object_t other);


/** Return a number shifted left.
  * Performs a standard binary operation using the __lshift__() and
  * __rlshift__() methods, used to implement the << operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_lshift(sky_object_t self, sky_object_t other);


/** Return a number shift left, performing the operation in place if possible.
  * Performs a standard binary operation using the __ilshift__() method. If
  * the __ilshift__() method is not implemented, the operation falls back to
  * using sky_number_lshift().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_lshift(sky_object_t self, sky_object_t other);


/** Return the remainder resulting from the division of two numbers.
  * Performs a standard binary operation using the __mod__() and __rmod__()
  * methods, used to implement the % operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_modulo(sky_object_t self, sky_object_t other);


/** Return the remainder resulting from the division of two numbers, performing
  * the operation in place if possible.
  * Performs a standard binary operation using the __imod__() method. If the
  * __imod__() method is not implemented, the operation falls back to using
  * sky_number_mod().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_modulo(sky_object_t self, sky_object_t other);


/** Return the product of two numbers.
  * Performs a standard binary operation using the __mul__() and __rmul__()
  * methods, used to implement the * operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_multiply(sky_object_t self, sky_object_t other);


/** Return the product of two numbers, performing the operation in place if
  * possible.
  * Performs a standard binary operation using the __imul__() method. If the
  * __imul__() method is not implemented, the operation falls back to using
  * sky_number_multiply().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_multiply(sky_object_t self, sky_object_t other);


/** Return the bitwise inclusive-or of two numbers.
  * Performs a standard binary operation using the __or__() and __ror__()
  * methods, used to implement the | operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_or(sky_object_t self, sky_object_t other);


/** Return the bitwise inclusive-or of two numbers, performing the operation
  * in place if possible.
  * Performs a standard binary operation using the __ior__() method. If the
  * __ior__() method is not implemented, the operation falls back to using
  * sky_number_or().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_or(sky_object_t self, sky_object_t other);


/** Return the result of raising a number to the power of another.
  * Performs a ternary operation using the __pow__() and __rpow__() methods.
  * For calls to the __pow__() method, all three parameters are used, but any
  * calls to the __rpow__() method are binary. Used to implement the **
  * operator and pow() built-in function.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @param[in]  modulo      an optional third number to use as a modulo for
  *                         the operation. May be either @c NULL or @c sky_None
  *                         to indicate omission.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_pow(sky_object_t self, sky_object_t other, sky_object_t modulo);


/** Return the result of raising number to the power of another, performing the
  * operation in place if possible.
  * Performs a standard binary operation using the __ipow__() method. If the
  * __ipow__() method is not implemented, the operation falls back to using
  * sky_number_pow().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @param[in]  modulo      an optional third number to use as a modulo for
  *                         the operation. May be either @c NULL or @c sky_None
  *                         to indicate omission.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_pow(sky_object_t self, sky_object_t other, sky_object_t modulo);


/** Return a number shifted right.
  * Performs a standard binary operation using the __rshift__() and
  * __rrshift__() methods, used to implement the >> operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_rshift(sky_object_t self, sky_object_t other);


/** Return a number shifted right, performing the operation in place if
  * possible.
  * Performs a standard binary operation using the __irshift__() method. If the
  * __irshift__() method is not implemented, the operation falls back to using
  * sky_number_rshift().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_rshift(sky_object_t self, sky_object_t other);


/** Return the difference of two numbers.
  * Performs a standard binary operation using the __sub__() and __rsub__()
  * methods, used to implement the binary - operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_subtract(sky_object_t self, sky_object_t other);


/** Return the difference of two numbers, performing the operation in place if
  * possible.
  * Performs a standard binary operation using the __isub__() method. If the
  * __isub__() method is not implemented, the operation falls back to using
  * sky_number_subtract().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_subtract(sky_object_t self, sky_object_t other);


/** Return the quotient of two numbers.
  * Performs a standard binary operation using the __truediv__() and
  * __rtruediv__() methods, used to implement the / operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_truedivide(sky_object_t self, sky_object_t other);


/** Return the quotient of two numbers, performing the operation in place if
  * possible.
  * Performs a standard binary operation using the __itruediv__() method. If
  * the __itruediv__() method is not implemented, the operation falls back to
  * sky_number_truedivide().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_truedivide(sky_object_t self, sky_object_t other);


/** Return the bitwise exclusive-or of two numbers.
  * Performs a standard binary operation using the __xor__() and __rxor__()
  * methods, used to implement the ^ operator.
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_xor(sky_object_t self, sky_object_t other);


/** Return the bitwise exclusive-or of two numbers, performing the operation in
  * place if possible.
  * Performs a standard binary operation using the __ixor__() method. If the
  * __ixor__() method is not implemented, the operation falls back to using
  * sky_number_xor().
  *
  * @param[in]  self        the first number in the operation.
  * @param[in]  other       the second number in the operation.
  * @return     the result.
  */
SKY_EXTERN sky_object_t
sky_number_inplace_xor(sky_object_t self, sky_object_t other);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NUMBER_H__ */

/** @} **/
/** @} **/
