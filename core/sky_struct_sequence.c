#include "sky_private.h"
#include "sky_tuple_private.h"


static sky_type_t sky_struct_sequence_item_type = NULL;

typedef struct sky_struct_sequence_item_data_s {
    sky_type_t                          type;
    sky_string_t                        qualname;
    sky_string_t                        name;
    sky_string_t                        doc;
    ssize_t                             index;
} sky_struct_sequence_item_data_t;

SKY_EXTERN_INLINE sky_struct_sequence_item_data_t *
sky_struct_sequence_item_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_struct_sequence_item_type) {
        return (sky_struct_sequence_item_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_struct_sequence_item_type);
}


typedef struct sky_struct_sequence_item_s {
    sky_object_data_t                   object_data;
    sky_struct_sequence_item_data_t     item_data;
} *sky_struct_sequence_item_t;


static void
sky_struct_sequence_item_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_struct_sequence_item_data_t *self_data = data;

    sky_object_visit(self_data->type, visit_data);
    sky_object_visit(self_data->qualname, visit_data);
    sky_object_visit(self_data->name, visit_data);
    sky_object_visit(self_data->doc, visit_data);
}


sky_struct_sequence_item_t
sky_struct_sequence_item_create(sky_type_t      type,
                                const char *    name,
                                const char *    doc,
                                ssize_t         index)
{
    sky_struct_sequence_item_t      item;
    sky_struct_sequence_item_data_t *item_data;

    item = sky_object_allocate(sky_struct_sequence_item_type);
    item_data = sky_struct_sequence_item_data(item);
    item_data->index = index;

    sky_object_gc_set(
            SKY_AS_OBJECTP(&(item_data->type)),
            type,
            item);
    sky_object_gc_set(
            SKY_AS_OBJECTP(&(item_data->name)),
            sky_string_createfrombytes(name, strlen(name), NULL, NULL),
            item);
    if (doc) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(item_data->doc)),
                sky_string_createfrombytes(doc, strlen(doc), NULL, NULL),
                item);
    }

    return item;
}


void
sky_struct_sequence_item_delete(sky_object_t    self,
                                sky_object_t    instance)
{
    sky_struct_sequence_item_data_t *self_data =
                                    sky_struct_sequence_item_data(self);

    if (!sky_object_isa(instance, self_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                self_data->name,
                sky_type_name(self_data->type),
                sky_type_name(sky_object_type(instance)));
    }
    sky_error_raise_format(
            sky_TypeError,
            "%#@ object attribute %#@ is read-only",
            sky_type_name(sky_object_type(instance)),
            self_data->name);
}


sky_object_t
sky_struct_sequence_item_get(           sky_object_t   self,
                                        sky_object_t   instance,
                             SKY_UNUSED sky_type_t     type)
{
    sky_struct_sequence_item_data_t *self_data =
                                    sky_struct_sequence_item_data(self);

    if (!instance) {
        return self;
    }
    if (!sky_object_isa(instance, self_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                self_data->name,
                sky_type_name(self_data->type),
                sky_type_name(sky_object_type(instance)));
    }

    return sky_tuple_data(instance, NULL)->objects[self_data->index];
}


static sky_object_t
sky_struct_sequence_item_qualname_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_struct_sequence_item_data_t *self_data =
                                    sky_struct_sequence_item_data(self);

    if (!self_data->qualname) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(self_data->qualname)),
                sky_string_createfromformat(
                        "%@.%@",
                        sky_type_qualname(sky_object_type(self)),
                        self_data->name),
                self);
    }

    return self_data->qualname;
}


sky_string_t
sky_struct_sequence_item_repr(sky_object_t self)
{
    sky_struct_sequence_item_data_t *self_data =
                                    sky_struct_sequence_item_data(self);

    return sky_string_createfromformat("<member %#@ of %#@ objects>",
                                       self_data->name,
                                       sky_type_name(self_data->type));
}


void
sky_struct_sequence_item_set(           sky_object_t   self,
                                        sky_object_t   instance,
                             SKY_UNUSED sky_object_t   value)
{
    sky_struct_sequence_item_data_t *self_data =
                                    sky_struct_sequence_item_data(self);

    if (!sky_object_isa(instance, self_data->type)) {
        sky_error_raise_format(
                sky_TypeError,
                "descriptor %#@ for %#@ objects doesn't apply to %#@ object",
                self_data->name,
                sky_type_name(self_data->type),
                sky_type_name(sky_object_type(instance)));
    }
    sky_error_raise_format(
            sky_TypeError,
            "%#@ object attribute %#@ is read-only",
            sky_type_name(sky_object_type(instance)),
            self_data->name);
}


static sky_parameter_list_t sky_struct_sequence_new_parameters = NULL;

sky_object_t
sky_struct_sequence_new(sky_type_t cls, sky_object_t sequence, sky_dict_t dict)
{
    ssize_t                         i, n_fields, n_sequence_fields, nvalues;
    sky_object_t                    item, key, object, value;
    sky_tuple_data_t                *tuple_data;
    sky_struct_sequence_item_data_t *item_data;

    if (!sky_sequence_check(sequence)) {
        sky_error_raise_string(sky_TypeError,
                               "constructor requires a sequence");
    }
    nvalues = sky_object_len(sequence);

    object = sky_object_getattr(cls,
                                SKY_STRING_LITERAL("n_fields"),
                                sky_NotSpecified);
    n_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_fields < 0");
    }

    object = sky_object_getattr(cls,
                                SKY_STRING_LITERAL("n_sequence_fields"),
                                sky_NotSpecified);
    n_sequence_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_sequence_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields < 0");
    }
    if (n_sequence_fields > n_fields) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields > n_fields");
    }

    if (n_fields != n_sequence_fields) {
        if (nvalues > n_fields) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%@() takes an at most %zd-sequence (%zd-sequence given)",
                    sky_type_name(cls),
                    n_fields,
                    nvalues);
        }
        if (nvalues < n_sequence_fields) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%@() takes an at least %zd-sequence (%zd-sequence given)",
                    sky_type_name(cls),
                    n_sequence_fields,
                    nvalues);
        }
    }
    else if (nvalues != n_fields) {
        sky_error_raise_format(
                sky_TypeError,
                "%@() takes a %zd-sequence (%zd-sequence given)",
                sky_type_name(cls),
                n_fields,
                nvalues);
    }

    object = sky_object_allocate(cls);
    tuple_data = sky_tuple_data(object, NULL);
    tuple_data->len = n_sequence_fields;
    if (n_fields <= SKY_TUPLE_TINY_COUNT) {
        tuple_data->objects = tuple_data->u.tiny_objects;
    }
    else {
        tuple_data->objects = sky_calloc(n_fields, sizeof(sky_object_data_t));
    }

    for (i = 0; i < nvalues; ++i) {
        tuple_data->objects[i] = sky_tuple_get(sequence, i);
    }
    if (nvalues < n_fields) {
        if (!sky_object_bool(dict)) {
            for (i = nvalues; i < n_fields; ++i) {
                tuple_data->objects[i] = sky_None;
            }
        }
        else {
            SKY_SEQUENCE_FOREACH(sky_object_dict(object), key) {
                item = sky_dict_getitem(dict, key);
                if (!sky_object_isa(item, sky_struct_sequence_item_type)) {
                    continue;
                }
                item_data = sky_struct_sequence_item_data(item);
                if (item_data->index < nvalues) {
                    continue;
                }
                value = sky_dict_get(dict, item_data->name, sky_None);
                tuple_data->objects[item_data->index] = value;
            } SKY_SEQUENCE_FOREACH_END;
            for (i = nvalues; i < n_fields; ++i) {
                if (!tuple_data->objects[i]) {
                    tuple_data->objects[i] = sky_None;
                }
            }
        }
    }

    sky_object_gc_setarray(tuple_data->len, tuple_data->objects, object);
    return object;
}


sky_object_t
sky_struct_sequence_reduce(sky_object_t self)
{
    ssize_t                         n_fields, n_sequence_fields,
                                    n_unnamed_fields;
    sky_dict_t                      dict, kws;
    sky_tuple_t                     args;
    sky_object_t                    item, key, object;
    sky_tuple_data_t                *tuple_data;
    sky_struct_sequence_item_data_t *item_data;

    object = sky_object_getattr(self,
                                SKY_STRING_LITERAL("n_fields"),
                                sky_NotSpecified);
    n_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_fields < 0");
    }

    object = sky_object_getattr(self,
                                SKY_STRING_LITERAL("n_sequence_fields"),
                                sky_NotSpecified);
    n_sequence_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_sequence_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields < 0");
    }
    if (n_sequence_fields > n_fields) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields > n_fields");
    }

    object = sky_object_getattr(self,
                                SKY_STRING_LITERAL("n_unnamed_fields"),
                                sky_NotSpecified);
    n_unnamed_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_unnamed_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_unnamed_fields < 0");
    }
    if (n_unnamed_fields > n_fields) {
        sky_error_raise_string(sky_ValueError, "n_unnamed_fields > n_fields");
    }
    if (n_sequence_fields + n_unnamed_fields != n_fields) {
        sky_error_raise_string(
                sky_ValueError,
                "n_sequence_fields + n_unnamed_fields != n_fields");
    }

    args = self;
    kws = sky_dict_create();

    tuple_data = sky_tuple_data(self, NULL);
    if (tuple_data->len != n_sequence_fields) {
        sky_error_raise_string(sky_ValueError,
                               "len(self) != n_sequence_fields");
    }
    if (tuple_data->len < n_fields && n_unnamed_fields > 0) {
        dict = sky_object_dict(sky_object_type(self));
        SKY_SEQUENCE_FOREACH(dict, key) {
            item = sky_dict_getitem(dict, key);
            if (!sky_object_isa(item, sky_struct_sequence_item_type)) {
                continue;
            }
            item_data = sky_struct_sequence_item_data(item);
            if (item_data->index < n_sequence_fields) {
                continue;
            }
            sky_dict_setitem(kws,
                             item_data->name,
                             tuple_data->objects[item_data->index]);
        } SKY_SEQUENCE_FOREACH_END;
    }

    return sky_object_build("(O(OO))", sky_object_type(self), args, kws);
}


sky_string_t
sky_struct_sequence_repr(sky_object_t self)
{
    ssize_t                         i, n_sequence_fields;
    sky_dict_t                      dict;
    sky_object_t                    key, o;
    sky_string_t                    *names;
    sky_tuple_data_t                *tuple_data;
    sky_string_builder_t            builder;
    sky_struct_sequence_item_data_t *item_data;

    builder = sky_string_builder_create();
    sky_string_builder_append(builder, sky_type_name(sky_object_type(self)));
    sky_string_builder_appendcodepoint(builder, '(', 1);

    if (!sky_object_stack_push(self)) {
        SKY_ASSET_BLOCK_BEGIN {
            sky_asset_save(self,
                           sky_object_stack_pop,
                           SKY_ASSET_CLEANUP_ALWAYS);
            o = sky_object_getattr(self,
                                   SKY_STRING_LITERAL("n_sequence_fields"),
                                   sky_NotSpecified);
            n_sequence_fields = sky_integer_value(o,
                                                  SSIZE_MIN,
                                                  SSIZE_MAX,
                                                  NULL);
            if (n_sequence_fields < 0) {
                sky_error_raise_string(sky_ValueError,
                                       "n_sequence_fields < 0");
            }

            names = sky_asset_calloc(n_sequence_fields,
                                     sizeof(sky_string_type),
                                     SKY_ASSET_CLEANUP_ALWAYS);

            dict = sky_object_dict(sky_object_type(self));
            SKY_SEQUENCE_FOREACH(dict, key) {
                o = sky_dict_getitem(dict, key);
                if (!sky_object_isa(o, sky_struct_sequence_item_type)) {
                    continue;
                }
                item_data = sky_struct_sequence_item_data(o);
                if (item_data->index < n_sequence_fields) {
                    names[item_data->index] = item_data->name;
                }
            } SKY_SEQUENCE_FOREACH_END;

            tuple_data = sky_tuple_data(self, NULL);
            for (i = 0; i < n_sequence_fields; ++i) {
                if (names[i]) {
                    sky_string_builder_append(builder, names[i]);
                    sky_string_builder_appendcodepoint(builder, '=', 1);
                }
                sky_string_builder_append(
                        builder,
                        sky_object_repr(tuple_data->objects[i]));
                if (i + 1 < n_sequence_fields) {
                    sky_string_builder_appendcodepoints(builder, ", ", 2, 1);
                }
            }
        } SKY_ASSET_BLOCK_END;
    }

    sky_string_builder_appendcodepoint(builder, ')', 1);
    return sky_string_builder_finalize(builder);
}


sky_type_t
sky_struct_sequence_define(const char *                 qualname,
                           const char *                 doc,
                           ssize_t                      nfields,
                           sky_struct_sequence_field_t *fields)
{
    ssize_t                     i, nsequence_fields, nunnamed_fields;
    sky_type_t                  type;
    sky_string_t                qualname_string;
    sky_struct_sequence_item_t  item;
    sky_struct_sequence_field_t *field;

    qualname_string = sky_string_createfrombytes(qualname,
                                                 strlen(qualname),
                                                 NULL,
                                                 NULL);
    type = sky_type_createbuiltin(qualname_string,
                                   doc,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   0,
                                   0,
                                   sky_tuple_pack(1, sky_tuple_type),
                                   NULL);
    sky_type_setmethodslotwithparameterlist(
            type,
            "__new__",
            (sky_native_code_function_t)sky_struct_sequence_new,
            sky_struct_sequence_new_parameters);
    sky_type_setmethodslots(type,
            "__repr__", sky_struct_sequence_repr,
            "__reduce__", sky_struct_sequence_reduce,
            NULL);

    nsequence_fields = nunnamed_fields = 0;
    for (i = 0; i < nfields; ++i) {
        field = &(fields[i]);
        if (field->flags & SKY_STRUCT_SEQUENCE_FIELD_FLAG_SEQUENCE_ONLY) {
            ++nunnamed_fields;
            continue;
        }
        if (!(field->flags & SKY_STRUCT_SEQUENCE_FIELD_FLAG_ATTRIBUTE_ONLY)) {
            ++nsequence_fields;
        }
        item = sky_struct_sequence_item_create(type,
                                               field->name,
                                               field->doc,
                                               i);
        sky_type_setattr_builtin(type, field->name, item);
    }

    sky_type_setattr_builtin(type,
                             "n_fields", sky_integer_create(nfields));
    sky_type_setattr_builtin(type,
                             "n_sequence_fields",
                             sky_integer_create(nsequence_fields));
    sky_type_setattr_builtin(type,
                             "n_unnamed_fields",
                             sky_integer_create(nunnamed_fields));

    return type;
}


sky_object_t
sky_struct_sequence_create(sky_type_t       type,
                           ssize_t          nvalues,
                           sky_object_t *   values)
{
    ssize_t             i, n_fields, n_sequence_fields;
    sky_object_t        object, value;
    sky_tuple_data_t    *tuple_data;

    object = sky_object_getattr(type,
                                SKY_STRING_LITERAL("n_fields"),
                                sky_NotSpecified);
    n_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_fields < 0");
    }
    if (nvalues > n_fields) {
        sky_error_raise_string(sky_ValueError, "nvalues > n_fields");
    }

    object = sky_object_getattr(type,
                                SKY_STRING_LITERAL("n_sequence_fields"),
                                sky_NotSpecified);
    n_sequence_fields = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
    if (n_sequence_fields < 0) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields < 0");
    }
    if (n_sequence_fields > n_fields) {
        sky_error_raise_string(sky_ValueError, "n_sequence_fields > n_fields");
    }

    object = sky_object_allocate(type);
    tuple_data = sky_tuple_data(object, NULL);
    tuple_data->len = n_sequence_fields;
    if (n_fields <= SKY_TUPLE_TINY_COUNT) {
        tuple_data->objects = tuple_data->u.tiny_objects;
    }
    else {
        tuple_data->objects = sky_calloc(n_fields, sizeof(sky_object_data_t));
        tuple_data->u.free = sky_free;
    }

    for (i = 0; i < nvalues; ++i) {
        value = (values[i] ? values[i] : sky_None);
        sky_object_gc_set(&(tuple_data->objects[i]), value, object);
    }
    if (nvalues < n_fields) {
        for (i = nvalues; i < n_fields; ++i) {
            tuple_data->objects[i] = sky_None;
        }
    }

    return object;
}


void
sky_struct_sequence_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_error_validate_debug(NULL == sky_struct_sequence_new_parameters);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_struct_sequence_new_parameters),
            sky_parameter_list_createwithparameters(
                    "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    "sequence", SKY_DATA_TYPE_OBJECT, NULL,
                    "dict", SKY_DATA_TYPE_OBJECT_DICT, sky_NotSpecified,
                    NULL),
            SKY_TRUE);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_struct_sequence_item_instance_visit;

    type = sky_type_createbuiltin(SKY_STRING_LITERAL("struct_sequence_item"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_struct_sequence_item_data_t),
                                   0,
                                   NULL,
                                   &template);
    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__repr__", sky_struct_sequence_item_repr,
            "__get__", sky_struct_sequence_item_get,
            "__set__", sky_struct_sequence_item_set,
            "__delete__", sky_struct_sequence_item_delete,
            NULL);
    sky_type_setattr_getset(type,
                            "__qualname__",
                            NULL,
                            sky_struct_sequence_item_qualname_getter,
                            NULL);
    sky_type_setmembers(type,
            "__doc__",      NULL,
                            offsetof(sky_struct_sequence_item_data_t, doc),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__name__",     NULL,
                            offsetof(sky_struct_sequence_item_data_t, name),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__objclass__", NULL,
                            offsetof(sky_struct_sequence_item_data_t, type),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_struct_sequence_item_type),
            type,
            SKY_TRUE);
}
