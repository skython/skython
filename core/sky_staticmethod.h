/** @file
  * @brief
  * @defgroup sky_function Functions
  * @{
  * @defgroup sky_staticmethod Static methods
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_STATICMETHOD_H__
#define __SKYTHON_CORE_SKY_STATICMETHOD_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new static method object.
  *
  * @param[in]  function    the callable object to bind.
  * @return     the new static method object.
  */
SKY_EXTERN sky_staticmethod_t
sky_staticmethod_create(sky_object_t function);


/** Return the function object instance that a static method object is wrapping.
  *
  * @param[in]  method      the static method object to query.
  * @return     the function object that @a method is wrapping.
  */
SKY_EXTERN sky_object_t
sky_staticmethod_function(sky_staticmethod_t method);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_STATICMETHOD_H__ */

/** @} **/
/** @} **/
