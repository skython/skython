/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_base_object Base Object
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BASE_OBJECT_H__
#define __SKYTHON_CORE_SKY_BASE_OBJECT_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Implementation of the __dir__() method for an object. **/
SKY_EXTERN sky_object_t
sky_base_object_dir(sky_object_t self);


/** Implementation of the __format__() method for an object. **/
SKY_EXTERN sky_string_t
sky_base_object_format(sky_object_t self, sky_string_t format_spec);


/** Implementation of the __delattr__() method for an object. **/
SKY_EXTERN void
sky_base_object_delattr(sky_object_t self, sky_string_t name);

/** Implementation of __delattr__() with a specified dict. **/
SKY_EXTERN void
sky_base_object_delattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_dict_t      dict);

/** Implementation of the __getattribute__() method for an object. **/
SKY_EXTERN sky_object_t
sky_base_object_getattribute(sky_object_t self, sky_string_t name);

/** Implementation of __getattribute__() with a specified dict. **/
SKY_EXTERN sky_object_t
sky_base_object_getattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_dict_t      dict);


/** Implementation of the __hash__() method for an object. **/
SKY_EXTERN uintptr_t
sky_base_object_hash(sky_object_t self);


/** Implementation of the __init__() method for an object. **/
SKY_EXTERN void
sky_base_object_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws);


/** Implementation of the __new__() method for an object. **/
SKY_EXTERN sky_object_t
sky_base_object_new(sky_object_t cls, sky_object_t args, sky_object_t kws);


/** Implementation of the __repr__() method for an object. **/
SKY_EXTERN sky_string_t
sky_base_object_repr(sky_object_t self);


/** Implementation of the __setattr__() method for an object. **/
SKY_EXTERN void
sky_base_object_setattr(sky_object_t    self,
                        sky_string_t    name,
                        sky_object_t    value);

/** Implementation of __setattr__() with a specified dict. **/
SKY_EXTERN void
sky_base_object_setattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_object_t    value,
                                sky_dict_t      dict);


/** Implementation of the __sizeof__() method for an object. **/
SKY_EXTERN size_t
sky_base_object_sizeof(sky_object_t self);


/** Implementation of the __str__() method for an object. **/
SKY_EXTERN sky_string_t
sky_base_object_str(sky_object_t self);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BASE_OBJECT_H__ */

/** @} **/
/** @} **/
