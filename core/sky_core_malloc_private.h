#ifndef __SKYTHON_CORE_SKY_CORE_MALLOC_PRIVATE_H__
#define __SKYTHON_CORE_SKY_CORE_MALLOC_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_atomic_lifo.h"


SKY_CDECLS_BEGIN


/* XXX: The value for SKY_CORE_MALLOC_HUGE_PAGE_SIZE is architecture dependent.
 * It may be able to be determined at run-time on some platforms. For others,
 * it probably needs to be a compile-time constant. There's some investigative
 * work that needs to be done here. All I know at the time of this writing is
 * that FreeBSD uses superpages automatically if it thinks it should, Darwin
 * does not support them at all, Windows requires privilege to use large tlbs
 * (?), and Linux requires flaming hoops to be leapt through while soaked in
 * highly flammable liquid to maybe, possibly, if you're real gosh darn lucky
 * get a huge page. Huge pages are always 4MB unless PAE is in use, in which
 * case they're 2MB. No way to tell from here, but PAE is unlikely, so always
 * use 4MB pages.
 *
 * 2016-01-27: This was written several years ago. The state of things has most
 * probably gotten better!
 */
#define SKY_CORE_MALLOC_HUGE_PAGE_SIZE  (1024 * 1024 * 4)

/* SKY_CORE_MALLOC_SMALL_SIZE_MAX must be a multiple of 4096. */
#define SKY_CORE_MALLOC_SMALL_SIZE_MAX  32768

/* SKY_CORE_MALLOC_HEAP_SIZE must be a multiple of
 * SKY_CORE_MALLOC_SMALL_SIZE_MAX, and it must not be any greater than
 * SKY_CORE_MALLOC_HUGE_PAGE_SIZE.
 */
#define SKY_CORE_MALLOC_HEAP_SIZE       (SKY_CORE_MALLOC_SMALL_SIZE_MAX * 8)

/* SKY_CORE_MALLOC_SMALL_SIZE_COUNT is the number of slots available for small
 * allocations. The implementations of sky_core_malloc_slab_index() and
 * sky_core_malloc_slab_size() are tied directly to this value.
 */
#define SKY_CORE_MALLOC_SMALL_SIZE_COUNT     32


/* Only seven bits are available for use here, because these flags are combined
 * with pointer addresses that most often point to a slab structure, which is
 * typically 128-byte aligned (each slab occupies two cache lines, and this is
 * assuming a 64-bit cache line size).
 */
typedef enum sky_core_malloc_page_flag_e {
    /* This flag indicates that the page is the first page in an allocation
     * from sky_system_alloc(), meaning that it can be used as the base address
     * for a call to sky_system_free(). This flag must always be preserved.
     */
    SKY_CORE_MALLOC_PAGE_FLAG_FIRST     =   0x01,

    /* This flag indicates that the page is the start of a large allocation,
     * which also means that the upper bits are the size rather than a pointer
     * to a slab.
     */
    SKY_CORE_MALLOC_PAGE_FLAG_LARGE     =   0x10,

    /* This flag indicates that the page is the start of a huge allocation,
     * which also means that the upper bits are the size rather than a pointer
     * to a slab.
     */
    SKY_CORE_MALLOC_PAGE_FLAG_HUGE      =   0x20,

    /* The mask of bits that should typically be preserved when calling
     * sky_core_malloc_maskvalue().
     */
    SKY_CORE_MALLOC_PAGE_FLAG_PRESERVE  =   0x0F,

    /* The mask of bits available for use in this enum. */
    SKY_CORE_MALLOC_PAGE_FLAG_MASK      =   0x7F,
} sky_core_malloc_page_flag_t;


/* This structure describes a chunk of available memory. Initially allocated
 * from the operating system in sizes of sky_core_malloc_huge_page_size, it'll
 * later be broken up by sky_core_malloc_heap_size for memory given to small
 * arg large allocations. In the latter case, large allocations will have their
 * sizes rounded up to the nearest sky_core_malloc_heap_size.
 */
typedef struct sky_core_malloc_chunk_s sky_core_malloc_chunk_t;
struct sky_core_malloc_chunk_s {
    size_t                              chunk_size;
    sky_core_malloc_chunk_t *           next_chunk;
    sky_core_malloc_chunk_t *           prev_chunk;
};


/* Slabs are used for small allocations. */
typedef struct sky_core_malloc_slab_s sky_core_malloc_slab_t;


typedef struct sky_core_malloc_slab_local_s {
    uint32_t                            thread_id;
    uint32_t                            slab_index;
    int32_t                             allocation_count;
    int32_t                             available_count;
    void *                              pointer;
    void *                              first_unallocated;
    void *                              calloc_freelist;
    void *                              malloc_freelist;

    sky_core_malloc_slab_t *            next;
    sky_core_malloc_slab_t *            prev;
} sky_core_malloc_slab_local_t;


typedef union sky_core_malloc_slab_remote_u {
    struct {
        void * volatile                 freelist;
        uintptr_t volatile              aba_tag;
    } data;
#if defined(SKY_ARCH_32BIT)
    int64_t                             cas_value;
#elif defined(SKY_ARCH_64BIT)
    char                                cas_value[16];
#else
#   error implementation missing
#endif
} sky_core_malloc_slab_remote_t;


struct sky_core_malloc_slab_s {
    SKY_CACHE_LINE_ALIGNED
    sky_core_malloc_slab_remote_t       remote;

    SKY_CACHE_LINE_ALIGNED
    sky_core_malloc_slab_local_t        local;
};


typedef struct sky_core_malloc_slab_chunk_s sky_core_malloc_slab_chunk_t;
struct sky_core_malloc_slab_chunk_s {
    sky_core_malloc_slab_chunk_t *      next;
    sky_core_malloc_slab_chunk_t *      prev;
    sky_core_malloc_slab_t *            freelist;
    size_t                              avail_count;
    size_t                              free_count;
    size_t                              used_count;
};


typedef struct sky_core_malloc_slab_list_s {
    sky_core_malloc_slab_t *            head;
    sky_core_malloc_slab_t *            tail;
} sky_core_malloc_slab_list_t;


/* This structure is dynamically allocated directly from the system using
 * sky_system_alloc(). It must be dynamically allocated, because it may be
 * orphaned when the thread dies if it has a large_count, any free chunks that
 * cannot be released back to the operating system, or any small allocations in
 * slabs.
 */
typedef struct sky_core_malloc_tlsdata_s sky_core_malloc_tlsdata_t;
struct sky_core_malloc_tlsdata_s {
    sky_core_malloc_tlsdata_t *         next_orphan;
    sky_core_malloc_tlsdata_t *         next;

    sky_core_malloc_chunk_t *           free_chunks_head;
    sky_core_malloc_chunk_t *           free_chunks_tail;

    sky_core_malloc_slab_chunk_t *      slabs_head;
    sky_core_malloc_slab_chunk_t *      slabs_tail;

    size_t                              large_count;

    /* for convenience - this is the same as sky_tlsdata_get()->thread_id */
    uint32_t                            thread_id;

    sky_core_malloc_slab_list_t         slabs[SKY_CORE_MALLOC_SMALL_SIZE_COUNT];

    SKY_CACHE_LINE_ALIGNED
    sky_atomic_lifo_t                   large_freelist;
};


extern void
sky_core_malloc_tlsdata_adopt(sky_core_malloc_tlsdata_t *tlsdata,
                              sky_core_malloc_tlsdata_t *orphan_tlsdata);

extern void
sky_core_malloc_tlsdata_cleanup(sky_core_malloc_tlsdata_t *tlsdata,
                                sky_core_malloc_tlsdata_t **orphan_tlsdata);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CORE_MALLOC_PRIVATE_H__ */
