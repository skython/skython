#include "sky_private.h"


SKY_TYPE_DEFINE_SIMPLE(NotImplemented,
                       "NotImplementedType",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

sky_NotImplemented_t const sky_NotImplemented = SKY_OBJECT_TAGGED_NOTIMPLEMENTED;


sky_object_t
sky_NotImplemented_new(SKY_UNUSED sky_type_t type, sky_tuple_t args, sky_dict_t kws)
{
    if ((args && sky_object_len(args)) || (kws && sky_object_len(kws))) {
        sky_error_raise_string(sky_TypeError,
                               "NotImplementedType takes no arguments");
    }
    return sky_NotImplemented;
}


sky_string_t
sky_NotImplemented_repr(SKY_UNUSED sky_object_t self)
{
    return SKY_STRING_LITERAL("NotImplemented");
}


void
sky_NotImplemented_initialize_library(void)
{
    sky_type_setmethodslots(sky_NotImplemented_type,
            "__new__", sky_NotImplemented_new,
            "__repr__", sky_NotImplemented_repr,
            NULL);
}
