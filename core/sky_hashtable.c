#include "sky_private.h"


/* This hash table algorithm is based on that presented by Shalev and Shavit,
 * 2006. Split-Ordered Lists: Lock-Free Extensible Hash Tables.
 * ACM, New York, Volume 53 Issue 3, May 2006.
 *
 * The implementation found here is NOT thread-safe except for a secondary
 * thread iterating the backing list (sky_hashtable_visit() does this for
 * Skython's GC marking thread). The backing list is a simple singlely-linked
 * list.
 */


SKY_EXTERN_INLINE uintptr_t
sky_hashtable_reverse(uintptr_t value)
{
#if defined(SKY_ARCH_32BIT)
    return sky_util_reverse32(value);
#elif defined(SKY_ARCH_64BIT)
    return sky_util_reverse64(value);
#else
#   error implementation missing
#endif
}


void
sky_hashtable_init(sky_hashtable_t *            table,
                   sky_hashtable_item_compare_t item_compare)
{
    table->max_length = 2;
    table->item_compare = item_compare;
    memset(table->segments, 0, sizeof(table->segments));
    table->segments[0] = sky_calloc(1, sizeof(sky_hashtable_segment_t) +
                                       (sizeof(sky_hashtable_item_t *) * 2));
    table->segments[0]->items[0] = sky_calloc(1, sizeof(sky_hashtable_item_t));
    table->dummy_nbytes = sky_memsize(table->segments[0]) +
                          sky_memsize(table->segments[0]->items[0]);
}


sky_hashtable_t *
sky_hashtable_create(sky_hashtable_item_compare_t item_compare)
{
    sky_hashtable_t *table;

    table = sky_calloc(1, sizeof(sky_hashtable_t));
    sky_hashtable_init(table, item_compare);

    return table;
}


void
sky_hashtable_cleanup(sky_hashtable_t *table, sky_free_t item_free)
{
    size_t                  i;
    sky_hashtable_item_t    *item, *next_item;

    next_item = table->segments[0]->items[0];
    if (item_free) {
        while ((item = next_item) != NULL) {
            next_item = item->next;
            if (item->key_hash & 1) {
                item_free(item);
            }
            else {
                sky_free(item);
            }
        }
    }
    else {
        while ((item = next_item) != NULL) {
            next_item = item->next;
            if (!(item->key_hash & 1)) {
                sky_free(item);
            }
        }
    }
    for (i = 0; i < SKY_HASHTABLE_SEGMENT_COUNT; ++i) {
        sky_free(table->segments[i]);
    }
}

void
sky_hashtable_destroy(sky_hashtable_t *table, sky_free_t item_free)
{
    sky_hashtable_cleanup(table, item_free);
    sky_free(table);
}


void
sky_hashtable_setcapacity(sky_hashtable_t *table, ssize_t capacity)
{
    capacity = SKY_MIN((ssize_t)sky_util_roundpow2(capacity),
                       (ssize_t)INT32_MAX + 1);
    if (capacity > table->max_length) {
        table->max_length = capacity;
    }
}


void
sky_hashtable_visit(sky_hashtable_t *           table,
                    sky_hashtable_item_visit_t  item_visit,
                    sky_object_visit_data_t *   visit_data)
{
    sky_hashtable_item_t    *first_item, *item;
    sky_hazard_pointer_t    *hazard;

    first_item = table->segments[0]->items[0];
    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&first_item));
    while ((item = hazard->pointer) != NULL) {
        if (!((uintptr_t)item & 1)) {
            if (!(item->key_hash & 1)) {
                first_item = item;
            }
            else {
                item_visit(item, visit_data);
            }
        }
        else {
            item = first_item;
        }
        sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(item->next)));
    }

    sky_hazard_pointer_release(hazard);
}


static inline sky_hashtable_item_t **
sky_hashtable_item(sky_hashtable_t *table, uintptr_t key_hash)
{
    size_t                  bucket, parent_bucket, segment;
    sky_hashtable_item_t    *current, *dummy, **item, **parent;

    bucket = (size_t)key_hash & (table->max_length - 1);
    if (bucket <= 1) {
        segment = 0;
        item = &(table->segments[0]->items[bucket]);
    }
    else {
        segment = sky_util_flsl(bucket) - 1;
        if (unlikely(!table->segments[segment])) {
            table->segments[segment] =
                    sky_calloc((size_t)1 << segment,
                               sizeof(sky_hashtable_item_t *));
            table->dummy_nbytes += sky_memsize(table->segments[segment]);
        }
        item = &(table->segments[segment]->items[bucket - ((size_t)1 << segment)]);
    }

    if (unlikely(!*item)) {
        parent_bucket =
                (bucket ? (bucket & (((1 << sky_util_flsl(bucket)) / 2) - 1))
                        : 0);
        parent = sky_hashtable_item(table, parent_bucket);
        dummy = sky_calloc(1, sizeof(sky_hashtable_item_t));
        dummy->key_hash = sky_hashtable_reverse(bucket) & ~(uintptr_t)1;
        table->dummy_nbytes += sky_memsize(dummy);

        while ((current = *parent) != NULL) {
            if (current->key_hash > dummy->key_hash) {
                dummy->next = current;
                break;
            }
            parent = &(current->next);
        }
        *item = *parent = dummy;
    }

    return item;
}


sky_hashtable_item_t *
sky_hashtable_delete(sky_hashtable_t *table, uintptr_t key_hash, void *key)
{
    sky_bool_t              match;
    sky_hashtable_item_t    *item, **prev, *start;
    sky_hazard_pointer_t    *hazard;

    if (!key) {
        prev = &(table->segments[0]->items[0]->next);
        if (!(item = table->segments[0]->items[0]->next)) {
            return NULL;
        }
        while (!(item->key_hash & 1)) {
            prev = &(item->next);
            if (!(item = item->next)) {
                return NULL;
            }
        }
        *prev = item->next;
        item->next = (sky_hashtable_item_t *)((uintptr_t)item->next | 1);
        --table->length;
        return item;
    }

    start = *sky_hashtable_item(table, key_hash);
    key_hash = sky_hashtable_reverse(key_hash) | 1;
    match = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        prev = &(start->next);
        while ((item = *prev) != NULL) {
            if (item->key_hash == key_hash) {
                match = table->item_compare(item, key);
                if ((uintptr_t)item->next & 1) {
                    prev = &(start->next);
                    continue;
                }
                if (match) {
                    break;
                }
            }
            if (item->key_hash > key_hash) {
                break;
            }
            prev = &(item->next);
        }
    } SKY_ASSET_BLOCK_END;

    if (match) {
        *prev = item->next;
        item->next = (sky_hashtable_item_t *)((uintptr_t)item->next | 1);
        --table->length;
        return item;
    }

    return NULL;
}


sky_hashtable_item_t *
sky_hashtable_insert(sky_hashtable_t *      table,
                     sky_hashtable_item_t * new_item,
                     uintptr_t              key_hash,
                     void *                 key)
{
    sky_bool_t              match;
    sky_hashtable_item_t    *item, **prev, *start;
    sky_hazard_pointer_t    *hazard;

    start = *sky_hashtable_item(table, key_hash);
    key_hash = sky_hashtable_reverse(key_hash) | 1;
    match = SKY_FALSE;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        prev = &(start->next);
        while ((item = *prev) != NULL) {
            if (item->key_hash == key_hash) {
                sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&item));
                match = table->item_compare(item, key);
                if ((uintptr_t)item->next & 1) {
                    prev = &(start->next);
                    continue;
                }
                if (match) {
                    break;
                }
            }
            if (item->key_hash > key_hash) {
                break;
            }
            prev = &(item->next);
        }
    } SKY_ASSET_BLOCK_END;

    if (match) {
        return item;
    }

    if (table->length == SKY_HASHTABLE_MAX_LENGTH) {
        return NULL;
    }

    new_item->next = item;
    new_item->key_hash = key_hash;
    *prev = new_item;

    ++table->length;
    if (table->max_length < 1 << (SKY_HASHTABLE_SEGMENT_COUNT - 1) &&
        table->length > table->max_length * 2 / 3)
    {
        table->max_length *= 2;
    }
    return new_item;
}


sky_hashtable_item_t *
sky_hashtable_lookup(sky_hashtable_t *table, uintptr_t key_hash, void *key)
{
    sky_bool_t              match;
    sky_hashtable_item_t    *item, *start;
    sky_hazard_pointer_t    *hazard;

    item = start = *sky_hashtable_item(table, key_hash);
    key_hash = sky_hashtable_reverse(key_hash) | 1;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        while ((item = item->next) != NULL) {
            if (item->key_hash == key_hash) {
                sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&item));
                match = table->item_compare(item, key);
                if ((uintptr_t)item->next & 1) {
                    item = start;
                    continue;
                }
                if (match) {
                    break;
                }
            }
            if (item->key_hash > key_hash) {
                item = NULL;
                break;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return item;
}


void
sky_hashtable_iterator_init(sky_hashtable_iterator_t *  iterator,
                            sky_hashtable_t *           table)
{
    sky_hashtable_item_t    *item;

    item = table->segments[0]->items[0]->next;
    while (item && !(item->key_hash & 1)) {
        item = item->next;
    }

    iterator->table = table;
    iterator->next_item = item;
}


sky_hashtable_iterator_t *
sky_hashtable_iterator_create(sky_hashtable_t *table)
{
    sky_hashtable_iterator_t    *iterator;

    iterator = sky_malloc(sizeof(sky_hashtable_iterator_t));
    sky_hashtable_iterator_init(iterator, table);

    return iterator;
}


void
sky_hashtable_iterator_cleanup(SKY_UNUSED sky_hashtable_iterator_t *iterator)
{
}


void
sky_hashtable_iterator_destroy(sky_hashtable_iterator_t *iterator)
{
    if (likely(iterator)) {
        sky_hashtable_iterator_cleanup(iterator);
        sky_free(iterator);
    }
}


sky_hashtable_item_t *
sky_hashtable_iterator_next(sky_hashtable_iterator_t *iterator)
{
    sky_hashtable_item_t    *item;

    if ((item = iterator->next_item) != NULL) {
        do {
            iterator->next_item = iterator->next_item->next;
        } while (iterator->next_item &&
                 !(iterator->next_item->key_hash & 1));
    }

    return item;
}


#if !defined(NDEBUG)
#include "sky_format.h"

void
sky_hashtable_dump(sky_hashtable_t *table)
{
    int                     i;
    size_t                  bucket, n, size;
    uintptr_t               key_hash, previous_hash;
    sky_hashtable_item_t    *item;

    sky_format_fprintf(stderr, "Dumping hash table %p:\n", table);
    sky_format_fprintf(stderr, "    length:       %zu\n", table->length);
    sky_format_fprintf(stderr, "    max_length:   %zu\n", table->max_length);
    sky_format_fprintf(stderr, "    item_compare: %p\n", table->item_compare);
    for (i = 0; i < SKY_HASHTABLE_SEGMENT_COUNT; ++i) {
        size = (i ? 1 << i : 2);        /* this is the segment's size */
        if (table->segments[i]) {
            sky_format_fprintf(stderr, "    segments[%2d]: %p\n", i, table->segments[i]);
            for (n = 0; n < size; ++n) {
                bucket = (i ? n + size : n);
                if ((item = table->segments[i]->items[n]) != NULL) {
                    key_hash = sky_hashtable_reverse(item->key_hash);
                    if (key_hash != bucket) {
                        sky_format_fprintf(stderr, "        items[%6zu]: %p    !!! BAD KEY HASH 0x%016jX expected 0x%016X\n", n, item, key_hash, bucket);
                    }
                    else {
                        sky_format_fprintf(stderr, "        items[%6zu]: %p\n", n, item);
                    }
                }
            }
        }
    }
    sky_format_fprintf(stderr, "Linked list of items follows:\n");
    previous_hash = 0;
    for (item = table->segments[0]->items[0]; item; item = item->next) {
        key_hash = sky_hashtable_reverse(item->key_hash);
        if (item->key_hash < previous_hash ||
            (!(item->key_hash & 1) &&
             *sky_hashtable_item(table, key_hash) != item))
        {
            sky_format_fprintf(stderr, "    %p -> 0x%016jX  0x%016jX  !!! BAD DUMMY ITEM\n",
                               item, item->key_hash, key_hash);
        }
        else {
            sky_format_fprintf(stderr, "    %p -> 0x%016jX  0x%016jX\n",
                               item, item->key_hash, key_hash);
        }
        previous_hash = item->key_hash;
    }
}
#endif
