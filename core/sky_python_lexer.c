#include "sky_private.h"
#include "sky_python_lexer.h"
#include "sky_python_compiler.h"


#if SKY_ENDIAN == SKY_ENDIAN_BIG
#define SKY_PYTHON_LEXER_UCS2_ENCODING  SKY_STRING_LITERAL("utf-16be")
#define SKY_PYTHON_LEXER_UCS4_ENCODING  SKY_STRING_LITERAL("utf-32be")
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
#define SKY_PYTHON_LEXER_UCS2_ENCODING  SKY_STRING_LITERAL("utf-16le")
#define SKY_PYTHON_LEXER_UCS4_ENCODING  SKY_STRING_LITERAL("utf-32le")
#else
#   error implementation missing
#endif


/* Column offsets are tracked as 1-based offsets from the start of the line.
 * There's lexer->col_offset, which expands tabs as 8 columns per tab, and
 * there's lexer->tab_col_offset, which does not expand tabs (each tab counts
 * as a single column). Columns are counted by Unicode codepoint.
 *
 * For tokens, the col_offset is the 0-based non-tab-expanded column that marks
 * the start of the token. The exception is for NEWLINE, which reports a
 * 1-based non-tab-expanded column.
 *
 * For error reporting, SyntaxError includes an 'offset' member that is set
 * as lexer->tab_col_offset - 1 to make it 0-based. In the case of certain
 * errors (mostly having to do with indentation errors), the offset reported
 * is the offset of the end-of-line marker for the line. As for the NEWLINE
 * token, offsets pointing to a end-of-line marker are 1-based.
 *
 * This is to maintain compatibility with CPython (mostly so we can pass the
 * stdlib unit tests). None of this is documented anywhere, so I've had to
 * figure out what the matching behavior is. There is probably still
 * differences, but this at least explains why the odd col_offset adjustments
 * are being made in various places.
 */


static inline int
sky_python_lexer_is_newline(const uint8_t *b, const uint8_t *end, int width)
{
    uint16_t    *w, *wend;
    uint32_t    *d, *dend;

    switch (width) {
        case 1:
            return (*b == '\n' ||
                    (*b == '\r' && (b + 1 >= end || *(b + 1) != '\n')));
        case 2:
            w = (uint16_t *)b;
            wend = (uint16_t *)end;
            return (*w == '\n' ||
                    (*w == '\r' && (w + 1 >= wend || *(w + 1) != '\n')));
        case 4:
            d = (uint32_t *)b;
            dend = (uint32_t *)end;
            return (*d == '\n' ||
                    (*d == '\r' && (d + 1 >= dend || *(d + 1) != '\n')));
    }
    sky_error_fatal("internal error");
}


static sky_tuple_t
sky_python_lexer_error(sky_python_lexer_t *lexer, const char *fmt, ...)
{
    int             col_offset;
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformatv(fmt, ap);
    va_end(ap);

    /* This col_offset adjustment is to match CPython's behavior */
    col_offset = lexer->tab_col_offset - 1;
    text = sky_python_lexer_line(lexer, lexer->lineno);
    if (col_offset < sky_string_len(text) &&
        '\n' == sky_string_charat(text, col_offset))
    {
        ++col_offset;
    }

    return sky_object_build("(O(OiiO))",
                            message,
                            lexer->filename,
                            lexer->lineno,
                            col_offset,
                            text);
}


static sky_bool_t
sky_python_lexer_readline(sky_python_lexer_t *lexer)
{
    sky_object_t    line;
    sky_string_t    prompt;
    
    prompt = (lexer->lineno > 1 && lexer->ps2 ? lexer->ps2 : lexer->ps1);
    line = sky_interpreter_inputhook()(prompt);

    if (!sky_object_bool(line)) {  /* EOF */
        lexer->start = lexer->end = lexer->next_line = NULL;
        lexer->next.byte = NULL;
        return SKY_FALSE;
    }
    if (sky_object_isa(line, sky_string_type)) {
        /* Decode the string into a bytes object. */
        line = sky_codec_encode(lexer->codec, line, lexer->encoding, NULL, 0);
    }

    if (lexer->buffer.object) {
        sky_buffer_release(&(lexer->buffer));
        lexer->buffer.object = NULL;
    }

    sky_buffer_acquire(&(lexer->buffer), line, SKY_BUFFER_FLAG_SIMPLE);
    lexer->start = lexer->buffer.buf;
    lexer->end = lexer->start + lexer->buffer.len;
    lexer->next.byte = lexer->start;

    return SKY_TRUE;
}


static sky_unicode_char_t
sky_python_lexer_getc_ucs2(sky_python_lexer_t *lexer)
{
    sky_unicode_char_t  cp;

    if (lexer->next.byte >= lexer->end) {
        return 0;
    }
    if (!(cp = *lexer->next.ucs2++)) {
        sky_error_raise_string(sky_TypeError,
                               "source code cannot contain null bytes");
    }
    return cp;
}

static void
sky_python_lexer_ungetc_ucs2(sky_python_lexer_t *lexer)
{
    sky_error_validate_debug(lexer->next.byte > lexer->start);
    --lexer->next.ucs2;
}


static sky_unicode_char_t
sky_python_lexer_getc_ucs4(sky_python_lexer_t *lexer)
{
    sky_unicode_char_t  cp;

    if (lexer->next.byte >= lexer->end) {
        return 0;
    }
    if (!(cp = *lexer->next.ucs4++)) {
        sky_error_raise_string(sky_TypeError,
                               "source code cannot contain null bytes");
    }
    return cp;
}

static void
sky_python_lexer_ungetc_ucs4(sky_python_lexer_t *lexer)
{
    sky_error_validate_debug(lexer->next.byte > lexer->start);
    --lexer->next.ucs4;
}


/* The _sbcs getc/ungetc functions are used for ASCII-based single-byte
 * character set encodings, including UTF-8. If a character is > 0x7F, then
 * the buffer is decoded up to the next line ending; otherwise, the character
 * maps directly to UTF-32 without having to be specially decoded.
 */
static sky_unicode_char_t
sky_python_lexer_getc_sbcs(sky_python_lexer_t *lexer)
{
    ssize_t             nbytes;
    sky_unicode_char_t  cp;

    if (lexer->decode_buffer.len > 0) {
        if (lexer->next.offset >= lexer->decode_buffer.len) {
            return 0;
        }
        if (lexer->decode_buffer.highest_codepoint < 0x100) {
            cp = *(lexer->decode_buffer.data.latin1 + lexer->next.offset);
        }
        else if (lexer->decode_buffer.highest_codepoint < 0x10000) {
            cp = *(lexer->decode_buffer.data.ucs2 + lexer->next.offset);
        }
        else {
            cp = *(lexer->decode_buffer.data.ucs4 + lexer->next.offset);
        }
        ++lexer->next.offset;
        if ('\n' == cp) {
            sky_string_data_clear(&(lexer->decode_buffer));
            lexer->next.byte = lexer->next_line;
        }
    }
    else if (!lexer->start ||
             (lexer->next.byte >= lexer->end &&
              (!(lexer->flags & SKY_PYTHON_LEXER_FLAG_FILE_SOURCE) ||
               !sky_python_lexer_readline(lexer))))
    {
        return 0;
    }
    else if (lexer->next.byte < lexer->start) {
        ++lexer->next.byte;
        sky_error_validate_debug(lexer->next.byte == lexer->start);
        cp = '\n';
    }
    else {
        cp = *lexer->next.byte++;
        if (cp > 0x7F ||
            (lexer->next.byte < lexer->end && *lexer->next.byte > 0x7F))
        {
            /* Decode the current line into lexer->decode_buffer */
            for (lexer->next_line = lexer->next.byte;
                 lexer->next_line < lexer->end &&
                 !sky_python_lexer_is_newline(lexer->next_line, lexer->end, 1);
                 ++lexer->next_line);
            if (lexer->next_line < lexer->end) {
                if ('\r' == *lexer->next_line) {
                    ++lexer->next_line;
                }
                ++lexer->next_line;
            }
            nbytes = lexer->next_line - lexer->start;

            sky_string_data_decode(&(lexer->decode_buffer),
                                   lexer->encoding,
                                   NULL,    /* strict error handling */
                                   lexer->start,
                                   nbytes);
            lexer->next.offset = (lexer->next.byte - 1) - lexer->start;
            return sky_python_lexer_getc_sbcs(lexer);
        }
    }

    if (!cp) {
        sky_error_raise_string(sky_TypeError,
                               "source code cannot contain null bytes");
    }
    return cp;
}

static void
sky_python_lexer_ungetc_sbcs(sky_python_lexer_t *lexer)
{
    if (lexer->decode_buffer.len > 0) {
        sky_error_validate_debug(lexer->next.offset > 0);
        --lexer->next.offset;
    }
    else {
        /* It's okay to go back one character before the start of the current
         * line, because it's possible to back up over a NEWLINE (this happens
         * frequently). It's not okay to back up beyond a NEWLINE, however.
         */
        sky_error_validate_debug(lexer->next.byte >= lexer->start);
        --lexer->next.byte;
    }
}


sky_unicode_char_t
sky_python_lexer_getc(sky_python_lexer_t *lexer)
{
    sky_unicode_char_t  c;

    if (lexer->flags & SKY_PYTHON_LEXER_FLAG_NEWLINE) {
        lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_NEWLINE;
        ++lexer->lineno;
        lexer->col_offset = 0;
        lexer->tab_col_offset = 0;
        lexer->start = lexer->next.byte;
    }

    SKY_ERROR_TRY {
        if (lexer->flags & SKY_PYTHON_LEXER_FLAG_EOF) {
            c = 0;
        }
        else if (!(c = lexer->decoder_getc(lexer))) {
            lexer->flags |= SKY_PYTHON_LEXER_FLAG_EOF;
        }
        else if (c < 0x20) {
            /* Do more stuff if it's a control character */
            switch ((char)c) {
                case '\n':
                    lexer->flags |= SKY_PYTHON_LEXER_FLAG_NEWLINE;
                    break;
                case '\r':
                    lexer->flags |= SKY_PYTHON_LEXER_FLAG_NEWLINE;
                    if (!(c = lexer->decoder_getc(lexer))) {
                        lexer->flags |= SKY_PYTHON_LEXER_FLAG_EOF;
                    }
                    else if (c != '\n') {
                        lexer->decoder_ungetc(lexer);
                    }
                    c = '\n';
                    break;
                case '\t':
                    lexer->col_offset = ((lexer->col_offset / 8) + 1) * 8;
                    lexer->tab_col_offset = ((lexer->tab_col_offset / 1) + 1) * 1;
                    break;
                case '\014':
                    /* For compatibility with CPython. Other than to consider a
                     * formfeed whitespace, what to do with one is undefined.
                     * CPython always resets the indent to 0.
                     */
                    lexer->col_offset = 0;
                    lexer->tab_col_offset = 0;
                    break;
            }
        }
    } SKY_ERROR_EXCEPT(sky_UnicodeDecodeError) {
        sky_error_raise_object(
                sky_SyntaxError,
                sky_python_lexer_error(lexer, "invalid character"));
    } SKY_ERROR_TRY_END;

    ++lexer->col_offset;
    ++lexer->tab_col_offset;
    return c;
}

void
sky_python_lexer_ungetc(sky_python_lexer_t *lexer)
{
    if (!(lexer->flags & SKY_PYTHON_LEXER_FLAG_EOF)) {
        lexer->decoder_ungetc(lexer);
    }

    /* Decrement the column. This works correctly even for tabs. Well, sort
     * of. It may leave the column incorrect at the end of a token, but the
     * only interesting column is really the start of a token.
     */
    sky_error_validate_debug(lexer->col_offset > 0);
    if (lexer->flags & SKY_PYTHON_LEXER_FLAG_NEWLINE) {
        lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_NEWLINE;
    }
    --lexer->tab_col_offset;
    if (!--lexer->col_offset) {
        lexer->flags |= SKY_PYTHON_LEXER_FLAG_NEWLINE;
        --lexer->lineno;
    }
}


static void
sky_python_lexer_setencoding(sky_python_lexer_t *lexer, sky_string_t encoding)
{
    lexer->flags |= SKY_PYTHON_LEXER_FLAG_ENCODING_SET;
    lexer->codec = sky_codec_builtin(encoding);
    lexer->encoding = encoding;

    if (lexer->codec &&
        2 == (lexer->codec->encode_flags & SKY_CODEC_ENCODE_FLAG_WIDTH_MASK))
    {
        lexer->decoder_getc = sky_python_lexer_getc_ucs2;
        lexer->decoder_ungetc = sky_python_lexer_ungetc_ucs2;
    }
    else if (lexer->codec &&
             4 == (lexer->codec->encode_flags & SKY_CODEC_ENCODE_FLAG_WIDTH_MASK))
    {
        lexer->decoder_getc = sky_python_lexer_getc_ucs4;
        lexer->decoder_ungetc = sky_python_lexer_ungetc_ucs4;
    }
    else {
        if (!lexer->codec ||
            !(lexer->codec->encode_flags & SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII))
        {
            sky_error_raise_format(sky_SyntaxError,
                                   "encoding not supported: %@",
                                   encoding);
        }
        lexer->decoder_getc = sky_python_lexer_getc_sbcs;
        lexer->decoder_ungetc = sky_python_lexer_ungetc_sbcs;
    }
}


static sky_unicode_char_t
sky_python_lexer_escape(sky_python_lexer_t *lexer, sky_bool_t string)
{
    int                 i;
    uint8_t             byte;
    sky_unicode_char_t  c, cp;

    cp = sky_python_lexer_getc(lexer);
    switch (cp) {
        case '\\':  return '\\';
        case '\'':  return '\'';
        case '\"':  return '\"';

        case 'a':   return 7;   /* BEL  */
        case 'b':   return 8;   /* BS   */
        case 'f':   return 12;  /* FF   */
        case 'n':   return 10;  /* LF   */
        case 'r':   return 13;  /* CR   */
        case 't':   return 9;   /* TAB  */
        case 'v':   return 11;  /* VT   */

        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7':
            byte = (cp - '0');
            for (i = 0; i < 2; ++i) {
                cp = sky_python_lexer_getc(lexer);
                if (cp > 0x7F || !sky_ctype_isodigit((int)cp)) {
                    sky_python_lexer_ungetc(lexer);
                    return byte;
                }
                byte = (byte << 3) | (cp - '0');
            }
            return byte;

        case 'x':
            cp = sky_python_lexer_getc(lexer);
            if (cp > 0x7F || !sky_ctype_isxdigit((int)cp)) {
                sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid \\x escape"));
            }
            byte = sky_ctype_hexdigit((int)cp);

            cp = sky_python_lexer_getc(lexer);
            if (cp > 0x7F || !sky_ctype_isxdigit((int)cp)) {
                sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid \\x escape"));
            }
            return (byte << 4) | sky_ctype_hexdigit((int)cp);

        case 'u':
            if (string) {
                c = 0;
                for (i = 0; i < 4; ++i) {
                    cp = sky_python_lexer_getc(lexer);
                    if (cp > 0x7F || !sky_ctype_isxdigit((int)cp)) {
                        sky_error_raise_object(
                                sky_SyntaxError,
                                sky_python_lexer_error(lexer,
                                                       "invalid \\u escape"));
                    }
                    c = (c << 4) | sky_ctype_hexdigit((int)cp);
                }
                return c;
            }
            break;
        case 'U':
            if (string) {
                c = 0;
                for (i = 0; i < 8; ++i) {
                    cp = sky_python_lexer_getc(lexer);
                    if (cp > 0x7F || !sky_ctype_isxdigit((int)cp)) {
                        sky_error_raise_object(
                                sky_SyntaxError,
                                sky_python_lexer_error(lexer,
                                                       "invalid \\U escape"));
                    }
                    c = (c << 4) | sky_ctype_hexdigit((int)cp);
                }
                if (c > SKY_UNICODE_CHAR_MAX) {
                    sky_error_raise_object(
                            sky_SyntaxError,
                            sky_python_lexer_error(
                                    lexer,
                                    "invalid Unicode character"));
                }
                return c;
            }
            break;
        case 'N':
            if (string) {
                /* TODO "\N{name}" */
            }
            break;
    }

    sky_python_lexer_ungetc(lexer);
    return '\\';
}


typedef struct sky_python_lexer_buffer_s {
    uint8_t *                           bytes;
    size_t                              len;
    size_t                              size;
} sky_python_lexer_buffer_t;


static inline void
sky_python_lexer_buffer_append(sky_python_lexer_buffer_t *  buffer,
                               sky_unicode_char_t           cp)
{
    if (buffer->len + 1 == buffer->size) {
        buffer->bytes = sky_asset_realloc(buffer->bytes,
                                          buffer->size * 2,
                                          SKY_ASSET_CLEANUP_ON_ERROR,
                                          SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer->size = sky_memsize(buffer->bytes);
    }
    buffer->bytes[buffer->len++] = (uint8_t)cp;
}


static inline sky_unicode_char_t
sky_python_lexer_bytes_getc(sky_python_lexer_t *lexer)
{
    sky_unicode_char_t  cp;

    if (!(cp = sky_python_lexer_getc(lexer))) {
        sky_error_raise_object(
            sky_SyntaxError,
            sky_python_lexer_error(
                lexer,
                "EOF while scanning string literal"));
    }
    if (cp > 0x7F) {
        sky_error_raise_object(
            sky_SyntaxError,
            sky_python_lexer_error(
                lexer,
                "bytes can only contain ASCII literal characters."));
    }

    return cp;
}


static void
sky_python_lexer_bytes(sky_python_lexer_t * lexer,
                       sky_python_token_t * token,
                       sky_bool_t           raw)
{
    sky_bool_t                  longbytes;
    sky_unicode_char_t          cp, endcp;
    sky_python_lexer_buffer_t   buffer;

    token->name = SKY_PYTHON_TOKEN_BYTES;

    longbytes = SKY_FALSE;
    endcp = sky_python_lexer_getc(lexer);
    if (sky_python_lexer_getc(lexer) != endcp) {
        sky_python_lexer_ungetc(lexer);
    }
    else {
        /* May be longbytes or empty */
        if (sky_python_lexer_getc(lexer) == endcp) {
            longbytes = SKY_TRUE;
        }
        else {
            sky_python_lexer_ungetc(lexer);
            token->object = sky_bytes_empty;
            return;
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer.bytes = sky_asset_malloc(64, SKY_ASSET_CLEANUP_ON_ERROR);
        buffer.len = 0;
        buffer.size = sky_memsize(buffer.bytes);

        for (;;) {
            cp = sky_python_lexer_bytes_getc(lexer);

            if (cp == endcp) {
                if (!longbytes) {
                    break;
                }
                if (sky_python_lexer_getc(lexer) == endcp) {
                    if (sky_python_lexer_getc(lexer) == endcp) {
                        break;
                    }
                    sky_python_lexer_ungetc(lexer);
                }
                sky_python_lexer_ungetc(lexer);
            }

            if (!longbytes && cp == '\n') {
                sky_error_raise_object(
                    sky_SyntaxError,
                    sky_python_lexer_error(
                        lexer,
                        "EOL while scanning string literal"));
            }
            if (cp == '\\') {
                if (raw) {
                    sky_python_lexer_buffer_append(&buffer, cp);
                    cp = sky_python_lexer_bytes_getc(lexer);
                }
                else {
                    if (sky_python_lexer_getc(lexer) == '\n') {
                        continue;
                    }
                    sky_python_lexer_ungetc(lexer);
                    cp = sky_python_lexer_escape(lexer, SKY_FALSE);
                    sky_error_validate_debug(cp <= SKY_UNICODE_CHAR_MAX);
                }
            }

            sky_python_lexer_buffer_append(&buffer, cp);
        }

        buffer.bytes[buffer.len] = '\0';
        token->object = sky_bytes_createwithbytes(buffer.bytes,
                                                  buffer.len,
                                                  sky_free);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_python_lexer_comment(sky_python_lexer_t *lexer)
{
    char                encoding[64], *p;
    sky_unicode_char_t  cp;

    /* An encoding declaration must appear on a line of its own within the
     * first two physical lines of the input source.
     */

    if (lexer->lineno > 2 ||
        (lexer->flags & SKY_PYTHON_LEXER_FLAG_ENCODING_SET) ||
        (lexer->flags & SKY_PYTHON_COMPILER_FLAG_IGNORE_COOKIE))
    {
        /* Simply skip to the end of the line, paying no attention to the
         * content of the comment.
         */
        while ((cp = sky_python_lexer_getc(lexer)) != 0 && cp != '\n');
        return;
    }

    /* Skip to the end of the line, but look for an encoding declaration. */
    cp = sky_python_lexer_getc(lexer);
    while (cp && cp != '\n') {
        if ('c' == cp) {
            /* Look for "coding:" or "coding=" */
            if ((cp = sky_python_lexer_getc(lexer)) != 'o') {
                continue;
            }
            if ((cp = sky_python_lexer_getc(lexer)) != 'd') {
                continue;
            }
            if ((cp = sky_python_lexer_getc(lexer)) != 'i') {
                continue;
            }
            if ((cp = sky_python_lexer_getc(lexer)) != 'n') {
                continue;
            }
            if ((cp = sky_python_lexer_getc(lexer)) != 'g') {
                continue;
            }
            cp = sky_python_lexer_getc(lexer);
            if (cp != '=' && cp != ':') {
                continue;
            }
            while ((cp = sky_python_lexer_getc(lexer)) == ' ' || cp == '\t');
            if (!cp || cp == '\n') {
                break;
            }

            /* From this point forward to the next whitespace code point or
             * the end of the line, collect the code points to make up an
             * encoding name. The code points must fall in the ASCII range
             * (0x20..0x7F, inclusive); otherwise, the declaration is
             * discarded.
             */
            p = encoding;
            while (cp && cp <= 0x7F && cp != ' ' && cp != '\t' && cp != '\n') {
                if (p >= encoding + sizeof(encoding)) {
                    break;  /* encoding name too long. discard it. */
                }
                *p++ = cp;
                cp = sky_python_lexer_getc(lexer);
            }
            if (cp <= 0x7F) {
                sky_python_lexer_setencoding(
                        lexer, 
                        sky_string_createfrombytes(encoding,
                                                   p - encoding,
                                                   NULL,
                                                   NULL));
            }
            continue;
        }

        cp = sky_python_lexer_getc(lexer);
    }
}


static sky_python_token_name_t
sky_python_lexer_identifier_keyword(const char *word, size_t word_length)
{
    switch (word_length) {
        case 2: /* as, if, in, is, or */
            switch (word[0]) {
                case 'a':
                    if (word[1] == 's') {
                        return SKY_PYTHON_TOKEN_AS;
                    }
                    break;
                case 'i':
                    switch (word[1]) {
                        case 'f':
                            return SKY_PYTHON_TOKEN_IF;
                        case 'n':
                            return SKY_PYTHON_TOKEN_IN;
                        case 's':
                            return SKY_PYTHON_TOKEN_IS;
                    }
                    break;
                case 'o':
                    if (word[1] == 'r') {
                        return SKY_PYTHON_TOKEN_OR;
                    }
                    break;
            }
            break;

        case 3: /* and, def, del, for, not, try */
            switch (word[0]) {
                case 'a':
                    if (word[1] == 'n' && word[2] == 'd') {
                        return SKY_PYTHON_TOKEN_AND;
                    }
                    break;
                case 'd':
                    if (word[1] == 'e') {
                        if (word[2] == 'f') {
                            return SKY_PYTHON_TOKEN_DEF;
                        }
                        if (word[2] == 'l') {
                            return SKY_PYTHON_TOKEN_DEL;
                        }
                    }
                    break;
                case 'f':
                    if (word[1] == 'o' && word[2] == 'r') {
                        return SKY_PYTHON_TOKEN_FOR;
                    }
                    break;
                case 'n':
                    if (word[1] == 'o' && word[2] == 't') {
                        return SKY_PYTHON_TOKEN_NOT;
                    }
                    break;
                case 't':
                    if (word[1] == 'r' && word[2] == 'y') {
                        return SKY_PYTHON_TOKEN_TRY;
                    }
                    break;
            }
            break;

        case 4: /* elif, else, from, None, pass, True, with */
            switch (word[0]) {
                case 'e':
                    if (!memcmp(word, "elif", 4)) {
                        return SKY_PYTHON_TOKEN_ELIF;
                    }
                    if (!memcmp(word, "else", 4)) {
                        return SKY_PYTHON_TOKEN_ELSE;
                    }
                    break;
                case 'f':
                    if (!memcmp(word, "from", 4)) {
                        return SKY_PYTHON_TOKEN_FROM;
                    }
                    break;
                case 'N':
                    if (!memcmp(word, "None", 4)) {
                        return SKY_PYTHON_TOKEN_NONE;
                    }
                    break;
                case 'p':
                    if (!memcmp(word, "pass", 4)) {
                        return SKY_PYTHON_TOKEN_PASS;
                    }
                    break;
                case 'T':
                    if (!memcmp(word, "True", 4)) {
                        return SKY_PYTHON_TOKEN_TRUE;
                    }
                    break;
                case 'w':
                    if (!memcmp(word, "with", 4)) {
                        return SKY_PYTHON_TOKEN_WITH;
                    }
                    break;
            }
            break;

        case 5: /* break, class, False, raise, while, yield */
            switch (word[0]) {
                case 'b':
                    if (!memcmp(word, "break", 5)) {
                        return SKY_PYTHON_TOKEN_BREAK;
                    }
                    break;
                case 'c':
                    if (!memcmp(word, "class", 5)) {
                        return SKY_PYTHON_TOKEN_CLASS;
                    }
                    break;
                case 'F':
                    if (!memcmp(word, "False", 5)) {
                        return SKY_PYTHON_TOKEN_FALSE;
                    }
                    break;
                case 'r':
                    if (!memcmp(word, "raise", 5)) {
                        return SKY_PYTHON_TOKEN_RAISE;
                    }
                    break;
                case 'w':
                    if (!memcmp(word, "while", 5)) {
                        return SKY_PYTHON_TOKEN_WHILE;
                    }
                    break;
                case 'y':
                    if (!memcmp(word, "yield", 5)) {
                        return SKY_PYTHON_TOKEN_YIELD;
                    }
                    break;
            }
            break;

        case 6: /* assert, except, global, import, lambda, return */
            switch (word[0]) {
                case 'a':
                    if (!memcmp(word, "assert", 6)) {
                        return SKY_PYTHON_TOKEN_ASSERT;
                    }
                    break;
                case 'e':
                    if (!memcmp(word, "except", 6)) {
                        return SKY_PYTHON_TOKEN_EXCEPT;
                    }
                    break;
                case 'g':
                    if (!memcmp(word, "global", 6)) {
                        return SKY_PYTHON_TOKEN_GLOBAL;
                    }
                    break;
                case 'i':
                    if (!memcmp(word, "import", 6)) {
                        return SKY_PYTHON_TOKEN_IMPORT;
                    }
                    break;
                case 'l':
                    if (!memcmp(word, "lambda", 6)) {
                        return SKY_PYTHON_TOKEN_LAMBDA;
                    }
                    break;
                case 'r':
                    if (!memcmp(word, "return", 6)) {
                        return SKY_PYTHON_TOKEN_RETURN;
                    }
                    break;
            }
            break;

        case 7: /* finally */
            if (!memcmp(word, "finally", 7)) {
                return SKY_PYTHON_TOKEN_FINALLY;
            }
            break;

        case 8: /* continue, nonlocal */
            if (!memcmp(word, "continue", 8)) {
                return SKY_PYTHON_TOKEN_CONTINUE;
            }
            if (!memcmp(word, "nonlocal", 8)) {
                return SKY_PYTHON_TOKEN_NONLOCAL;
            }
            break;
    }

    return SKY_PYTHON_TOKEN_NAME;
}


static void
sky_python_lexer_identifier(sky_python_lexer_t *lexer,
                            sky_python_token_t *token)
    /* Parse an identifier, possibly matching it to a language keyword. We're
     * going to assume that most of the time we're dealing with an identifier
     * that contains nothing other than ASCII code points (alpha, numeric, and
     * underscore. If a non-ASCII code point is found, whatever has been
     * accumulated so far will be converted to Unicode, and the remainder of
     * the parse will proceed using the slower path to handle a non-ASCII
     * identifier.
     */
{
    sky_string_data_t   buffer;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        memset(&buffer, 0, sizeof(buffer));
        sky_asset_save(&buffer,
                       (sky_free_t)sky_string_data_finalize,
                       SKY_ASSET_CLEANUP_ALWAYS);

        cp = sky_python_lexer_getc(lexer);
        sky_error_validate_debug(sky_unicode_isxid_start(cp));
        sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));

        while ((cp = sky_python_lexer_getc(lexer)) != 0) {
            if (!sky_unicode_isxid_continue(cp)) {
                sky_python_lexer_ungetc(lexer);
                break;
            }
            sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
        }

        if (buffer.highest_codepoint < 0x7F) {
            /* All keywords are ASCII code points only, so only check keyword
             * mappings in ASCII mode. If the identifier is not a keyword,
             * normalization can be skipped, and a string can be created
             * directly using ASCII encoding.
             */
            token->name = sky_python_lexer_identifier_keyword(buffer.data.ascii,
                                                              buffer.len);
            if (SKY_PYTHON_TOKEN_NAME == token->name) {
                token->object = sky_string_createfromstringdata(&buffer);
                token->object = sky_string_intern(token->object);
            }
        }
        else {
            token->name = SKY_PYTHON_TOKEN_NAME;
            token->object =
                    sky_string_normalize(
                            sky_string_createfromstringdata(&buffer),
                            SKY_STRING_NORMALIZE_FORM_NFKC);
            token->object = sky_string_intern(token->object);
        }
    } SKY_ASSET_BLOCK_END;
}


static sky_python_token_name_t
sky_python_lexer_number_decimal(sky_python_lexer_t *lexer,
                                sky_string_data_t * buffer)
{
    sky_unicode_char_t  cp;

    if (buffer->data.ascii[buffer->len - 1] == '.') {
        /* If the '.' is the only character in the buffer, at least one digit
         * must follow it.
         */
        if (1 == buffer->len) {
            if (!(cp = sky_python_lexer_getc(lexer))) {
                sky_error_raise_object(
                    sky_SyntaxError,
                    sky_python_lexer_error(lexer, "invalid_syntax"));
            }
            if (cp > 0x7F || !sky_ctype_isdigit((int)cp)) {
                sky_error_raise_object(
                    sky_SyntaxError,
                    sky_python_lexer_error(lexer, "invalid_syntax"));
            }
            sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
        }

        for (;;) {
            if (!(cp = sky_python_lexer_getc(lexer))) {
                return SKY_PYTHON_TOKEN_DECIMAL;
            }
            if ('e' == cp || 'E' == cp) {
                sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
                break;
            }
            if ('j' == cp || 'J' == cp) {
                sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
                return SKY_PYTHON_TOKEN_IMAGINARY;
            }
            if (cp > 0x7F || !sky_ctype_isdigit((int)cp)) {
                sky_python_lexer_ungetc(lexer);
                return SKY_PYTHON_TOKEN_DECIMAL;
            }
            sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
        }
    }

    /* The last byte in the buffer must be either 'e' or 'E' at this point. */
    sky_error_validate_debug('e' == buffer->data.ascii[buffer->len - 1] ||
                             'E' == buffer->data.ascii[buffer->len - 1]);

    /* There may be an optional '+' or '-' immediately following the 'e'.
     * After that, there must be at least one digit.
     */

    cp = sky_python_lexer_getc(lexer);
    if ('+' == cp || '-' == cp) {
        sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
        cp = sky_python_lexer_getc(lexer);
    }
    if (cp > 0x7F || !sky_ctype_isdigit((int)cp)) {
        sky_error_raise_object(
            sky_SyntaxError,
            sky_python_lexer_error(lexer, "invalid token"));
    }
    sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));

    /* Accumulate digits. There may be a terminating 'j' or 'J' to turn the
     * number into an imaginary (complex) number.
     */
    while ((cp = sky_python_lexer_getc(lexer)) != 0) {
        if ('j' == cp || 'J' == cp) {
            sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
            return SKY_PYTHON_TOKEN_IMAGINARY;
        }
        if (cp > 0x7F || !sky_ctype_isdigit((int)cp)) {
            sky_python_lexer_ungetc(lexer);
            return SKY_PYTHON_TOKEN_DECIMAL;
        }
        sky_string_data_append(buffer, &cp, sizeof(cp), sizeof(cp));
    }

    return SKY_PYTHON_TOKEN_DECIMAL;
}


static void
sky_python_lexer_number(sky_python_lexer_t *lexer, sky_python_token_t *token)
{
    sky_bool_t          nonzero;
    sky_string_data_t   buffer;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        token->name = SKY_PYTHON_TOKEN_INTEGER;
        memset(&buffer, 0, sizeof(buffer));
        sky_asset_save(&buffer,
                       (sky_free_t)sky_string_data_finalize,
                       SKY_ASSET_CLEANUP_ALWAYS);
        cp = sky_python_lexer_getc(lexer);

        if ('0' == cp) {
            sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
            cp = sky_python_lexer_getc(lexer);
            if ('b' == cp || 'B' == cp) {
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                while ((cp = sky_python_lexer_getc(lexer)) != 0 &&
                       ('0' == cp || '1' == cp))
                {
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                }
                sky_python_lexer_ungetc(lexer);
                if (buffer.len <= 2) {
                    sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid token"));
                }
            }
            else if ('o' == cp || 'O' == cp) {
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                while ((cp = sky_python_lexer_getc(lexer)) != 0 &&
                       (cp <= 0x7F && sky_ctype_isodigit((int)cp)))
                {
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                }
                sky_python_lexer_ungetc(lexer);
                if (buffer.len <= 2) {
                    sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid token"));
                }
            }
            else if ('x' == cp || 'X' == cp) {
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                while ((cp = sky_python_lexer_getc(lexer)) != 0 &&
                       (cp <= 0x7F && sky_ctype_isxdigit((int)cp)))
                {
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                }
                sky_python_lexer_ungetc(lexer);
                if (buffer.len <= 2) {
                    sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid token"));
                }
            }
            else if ('.' == cp || 'e' == cp || 'E' == cp) {
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                token->name = sky_python_lexer_number_decimal(lexer, &buffer);
            }
            else if ('j' == cp || 'J' == cp) {
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                token->name = SKY_PYTHON_TOKEN_IMAGINARY;
            }
            else if (cp <= 0x7F && sky_ctype_isdigit(cp)) {
                nonzero = (cp != '0' ? SKY_TRUE : SKY_FALSE);
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                while ((cp = sky_python_lexer_getc(lexer)) != 0) {
                    if ('.' == cp || 'e' == cp || 'E' == cp) {
                        sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                        token->name = sky_python_lexer_number_decimal(lexer, &buffer);
                        break;
                    }
                    if ('j' == cp || 'J' == cp) {
                        sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                        token->name = SKY_PYTHON_TOKEN_IMAGINARY;
                        break;
                    }
                    if (cp > 0x7F || !sky_ctype_isdigit(cp)) {
                        sky_python_lexer_ungetc(lexer);
                        break;
                    }
                    if (cp != '0') {
                        nonzero = SKY_TRUE;
                    }
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                }
                if (SKY_PYTHON_TOKEN_INTEGER == token->name && nonzero) {
                    sky_error_raise_object(
                        sky_SyntaxError,
                        sky_python_lexer_error(lexer, "invalid token"));
                }
            }
            else {
                sky_python_lexer_ungetc(lexer);
            }
        }
        else if ('.' == cp) {
            sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
            token->name = sky_python_lexer_number_decimal(lexer, &buffer);
        }
        else if ('j' == cp || 'J' == cp) {
            sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
            token->name = SKY_PYTHON_TOKEN_IMAGINARY;
        }
        else {
            sky_error_validate_debug(cp <= 0x7F && sky_ctype_isdigit((int)cp));
            sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
            while ((cp = sky_python_lexer_getc(lexer)) != 0) {
                if ('.' == cp || 'e' == cp || 'E' == cp) {
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                    token->name = sky_python_lexer_number_decimal(lexer, &buffer);
                    break;
                }
                if ('j' == cp || 'J' == cp) {
                    sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
                    token->name = SKY_PYTHON_TOKEN_IMAGINARY;
                    break;
                }
                if (cp > 0x7F || !sky_ctype_isdigit((int)cp)) {
                    sky_python_lexer_ungetc(lexer);
                    break;
                }
                sky_string_data_append(&buffer, &cp, sizeof(cp), sizeof(cp));
            }
        }

        if (SKY_PYTHON_TOKEN_INTEGER == token->name) {
            token->object = sky_integer_createfromascii(buffer.data.ascii,
                                                        buffer.len,
                                                        0);
        }
        else if (SKY_PYTHON_TOKEN_DECIMAL == token->name) {
            token->object = sky_float_createfromascii(buffer.data.ascii,
                                                      buffer.len);
        }
        else if (SKY_PYTHON_TOKEN_IMAGINARY == token->name) {
            token->object = sky_complex_createfromascii(buffer.data.ascii,
                                                        buffer.len);
        }
        else {
            sky_error_fatal("internal error");
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_python_lexer_string(sky_python_lexer_t *lexer,
                        sky_python_token_t *token,
                        sky_bool_t          raw)
{
    sky_bool_t              longstring;
    sky_unicode_char_t      cp, endcp;
    sky_string_builder_t    builder;

    token->name = SKY_PYTHON_TOKEN_STRING;

    longstring = SKY_FALSE;
    endcp = sky_python_lexer_getc(lexer);
    if (sky_python_lexer_getc(lexer) != endcp) {
        sky_python_lexer_ungetc(lexer);
    }
    else {
        /* May be longstring or empty */
        if (sky_python_lexer_getc(lexer) == endcp) {
            longstring = SKY_TRUE;
        }
        else {
            sky_python_lexer_ungetc(lexer);
            token->object = sky_string_empty;
            return;
        }
    }

    builder = sky_string_builder_create();
    for (;;) {
        if (!(cp = sky_python_lexer_getc(lexer))) {
            sky_error_raise_object(
                sky_SyntaxError,
                sky_python_lexer_error(
                    lexer,
                    "EOF while scanning string literal"));
        }

        if (cp == endcp) {
            if (!longstring) {
                break;
            }
            if (sky_python_lexer_getc(lexer) == endcp) {
                if (sky_python_lexer_getc(lexer) == endcp) {
                    break;
                }
                sky_python_lexer_ungetc(lexer);
            }
            sky_python_lexer_ungetc(lexer);
        }

        if (!longstring && cp == '\n') {
            sky_error_raise_object(
                sky_SyntaxError,
                sky_python_lexer_error(
                    lexer,
                    "EOL while scanning string literal"));
        }
        if (cp == '\\') {
            if (raw) {
                sky_string_builder_appendcodepoint(builder, cp, 1);
                if (!(cp = sky_python_lexer_getc(lexer))) {
                    sky_error_raise_object(
                            sky_SyntaxError,
                            sky_python_lexer_error(
                                    lexer,
                                    "EOF while scanning string literal"));
                }
            }
            else {
                if (sky_python_lexer_getc(lexer) == '\n') {
                    continue;
                }
                sky_python_lexer_ungetc(lexer);
                cp = sky_python_lexer_escape(lexer, SKY_TRUE);
                sky_error_validate_debug(cp <= SKY_UNICODE_CHAR_MAX);
            }
        }
        sky_string_builder_appendcodepoint(builder, cp, 1);
    }

    token->object = sky_string_builder_finalize(builder);
}


sky_bool_t
sky_python_lexer_token(sky_python_lexer_t *lexer, sky_python_token_t *token)
{
    int                 col_offset, tab_col_offset;
    sky_unicode_char_t  cp;

    token->name = SKY_PYTHON_TOKEN_ERROR;
    token->object = NULL;
    token->lineno = token->col_offset = 0;

    if (lexer->pending_dedents) {
        --lexer->pending_dedents;
        token->lineno = lexer->lineno + 1;
        token->col_offset = lexer->tab_col_offset - 1;
        token->name = SKY_PYTHON_TOKEN_DEDENT;
        return SKY_TRUE;
    }

    if (lexer->flags & SKY_PYTHON_LEXER_FLAG_NEWLINE) {
        for (;;) {
            cp = sky_python_lexer_getc(lexer);
            if (cp == '#') {
                sky_python_lexer_comment(lexer);
            }
            else if (' ' != cp && '\t' != cp && '\014' != cp && '\n' != cp) {
                break;
            }
            else if ('\n' == cp &&
                     (lexer->flags & SKY_PYTHON_LEXER_FLAG_INTERACTIVE))
            {
                break;
            }
        }

        token->lineno = lexer->lineno;
        token->col_offset = lexer->tab_col_offset - 1;
        col_offset = lexer->col_offset - 1;
        tab_col_offset = lexer->tab_col_offset - 1;

        if (col_offset && (!cp || '\n' == cp)) {
            token->col_offset = 0;
            col_offset = 0;
            tab_col_offset = 0;
        }

        if (col_offset > *lexer->indent_stack_pointer) {
            sky_python_lexer_ungetc(lexer);
            if (tab_col_offset <= *lexer->tab_indent_stack_pointer) {
                /* Match CPython */
                while ((cp = sky_python_lexer_getc(lexer)) != '\0' &&
                        cp != '\n');
                sky_error_raise_object(
                    sky_TabError,
                    sky_python_lexer_error(
                        lexer,
                        "inconsistent use of tabs and spaces in indentation"));
            }
            *++lexer->indent_stack_pointer = col_offset;
            *++lexer->tab_indent_stack_pointer = tab_col_offset;
            token->name = SKY_PYTHON_TOKEN_INDENT;
            return SKY_TRUE;
        }
        if (col_offset < *lexer->indent_stack_pointer) {
            sky_python_lexer_ungetc(lexer);
            --lexer->indent_stack_pointer;
            --lexer->tab_indent_stack_pointer;
            while (col_offset < *lexer->indent_stack_pointer) {
                --lexer->indent_stack_pointer;
                --lexer->tab_indent_stack_pointer;
                ++lexer->pending_dedents;
            }
            if (col_offset != *lexer->indent_stack_pointer) {
                /* Match CPython */
                while ((cp = sky_python_lexer_getc(lexer)) != '\0' &&
                        cp != '\n');
                sky_error_raise_object(
                    sky_IndentationError,
                    sky_python_lexer_error(
                        lexer,
                        "unindent does not match any outer indentation level"));
            }
            if (tab_col_offset != *lexer->tab_indent_stack_pointer) {
                /* Match CPython */
                while ((cp = sky_python_lexer_getc(lexer)) != '\0' &&
                        cp != '\n');
                sky_error_raise_object(
                    sky_TabError,
                    sky_python_lexer_error(
                        lexer,
                        "inconsistent use of tabs and spaces in indentation"));
            }
            if (!cp &&
                (lexer->flags & SKY_PYTHON_COMPILER_FLAG_DONT_IMPLY_DEDENT))
            {
                lexer->pending_dedents = 0;
                token->name = SKY_PYTHON_TOKEN_ENDMARKER;
                return SKY_FALSE;
            }
            token->name = SKY_PYTHON_TOKEN_DEDENT;
            return SKY_TRUE;
        }
        if (tab_col_offset != *lexer->tab_indent_stack_pointer) {
            /* Match CPython */
            while ((cp = sky_python_lexer_getc(lexer)) != '\0' &&
                    cp != '\n');
            sky_error_raise_object(
                sky_TabError,
                sky_python_lexer_error(
                    lexer,
                    "inconsistent use of tabs and spaces in indentation"));
        }
    }
    else if (!(cp = sky_python_lexer_getc(lexer)) && lexer->col_offset > 0) {
        /* There's no more input, but since the last line doesn't end with a
         * NEWLINE, one must be fabricated so that any pending DEDENTs that
         * follow will parse correctly.
         */
        lexer->flags |= SKY_PYTHON_LEXER_FLAG_NEWLINE;
        cp = '\n';
    }

try_again:
    token->lineno = lexer->lineno;
    while (' ' == cp || '\t' == cp || '\014' == cp) {
        cp = sky_python_lexer_getc(lexer);
    }
    token->col_offset = lexer->tab_col_offset - 1;

    switch (cp) {
        case '\0':
            token->name = SKY_PYTHON_TOKEN_ENDMARKER;
            return SKY_FALSE;

        case '#':
            while ((cp = sky_python_lexer_getc(lexer)) != 0 && cp != '\n');
            goto newline;

        case '\\':
            if (sky_python_lexer_getc(lexer) == '\n') {
                cp = sky_python_lexer_getc(lexer);
                lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_NEWLINE;
                goto try_again;
            }
            sky_python_lexer_ungetc(lexer);
            break;

        case '\n':
        newline:
            if (lexer->nesting_level) {
                /* Implicit line join. Swallow the newline and try again for
                 * the next token.
                 */
                cp = sky_python_lexer_getc(lexer);
                goto try_again;
            }
            ++token->col_offset;
            token->name = SKY_PYTHON_TOKEN_NEWLINE;
            return SKY_TRUE;

        case ';':
            token->name = SKY_PYTHON_TOKEN_SEMICOLON;
            return SKY_TRUE;

        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            sky_python_lexer_ungetc(lexer);
            sky_python_lexer_number(lexer, token);
            return SKY_TRUE;

        case '=':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_EQUALS_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_EQUALS;
            return SKY_TRUE;

        case '!':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_BANG_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_ERROR;
            return SKY_TRUE;

        case '<':
            switch (sky_python_lexer_getc(lexer)) {
                case '<':
                    if ('=' == sky_python_lexer_getc(lexer)) {
                        token->name = SKY_PYTHON_TOKEN_LESS_LESS_EQUALS;
                        return SKY_TRUE;
                    }
                    sky_python_lexer_ungetc(lexer);
                    token->name = SKY_PYTHON_TOKEN_LESS_LESS;
                    return SKY_TRUE;
                case '=':
                    token->name = SKY_PYTHON_TOKEN_LESS_EQUALS;
                    return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_LESS;
            return SKY_TRUE;

        case '>':
            switch (sky_python_lexer_getc(lexer)) {
                case '>':
                    if ('=' == sky_python_lexer_getc(lexer)) {
                        token->name = SKY_PYTHON_TOKEN_GREATER_GREATER_EQUALS;
                        return SKY_TRUE;
                    }
                    sky_python_lexer_ungetc(lexer);
                    token->name = SKY_PYTHON_TOKEN_GREATER_GREATER;
                    return SKY_TRUE;
                case '=':
                    token->name = SKY_PYTHON_TOKEN_GREATER_EQUALS;
                    return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_GREATER;
            return SKY_TRUE;

        case '+':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_PLUS_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_PLUS;
            return SKY_TRUE;

        case '-':
            switch (sky_python_lexer_getc(lexer)) {
                case '=':
                    token->name = SKY_PYTHON_TOKEN_MINUS_EQUALS;
                    return SKY_TRUE;
                case '>':
                    token->name = SKY_PYTHON_TOKEN_MINUS_GREATER;
                    return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_MINUS;
            return SKY_TRUE;

        case '*':
            switch (sky_python_lexer_getc(lexer)) {
                case '*':
                    if ('=' == sky_python_lexer_getc(lexer)) {
                        token->name = SKY_PYTHON_TOKEN_STAR_STAR_EQUALS;
                        return SKY_TRUE;
                    }
                    sky_python_lexer_ungetc(lexer);
                    token->name = SKY_PYTHON_TOKEN_STAR_STAR;
                    return SKY_TRUE;
                case '=':
                    token->name = SKY_PYTHON_TOKEN_STAR_EQUALS;
                    return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_STAR;
            return SKY_TRUE;

        case '/':
            switch (sky_python_lexer_getc(lexer)) {
                case '/':
                    if ('=' == sky_python_lexer_getc(lexer)) {
                        token->name = SKY_PYTHON_TOKEN_SLASH_SLASH_EQUALS;
                        return SKY_TRUE;
                    }
                    sky_python_lexer_ungetc(lexer);
                    token->name = SKY_PYTHON_TOKEN_SLASH_SLASH;
                    return SKY_TRUE;
                case '=':
                    token->name = SKY_PYTHON_TOKEN_SLASH_EQUALS;
                    return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_SLASH;
            return SKY_TRUE;

        case '%':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_PERCENT_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_PERCENT;
            return SKY_TRUE;

        case '&':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_AMPERSAND_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_AMPERSAND;
            return SKY_TRUE;

        case '|':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_PIPE_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_PIPE;
            return SKY_TRUE;

        case '^':
            if ('=' == sky_python_lexer_getc(lexer)) {
                token->name = SKY_PYTHON_TOKEN_CARET_EQUALS;
                return SKY_TRUE;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_CARET;
            return SKY_TRUE;

        case '~':
            token->name = SKY_PYTHON_TOKEN_TILDE;
            return SKY_TRUE;
        case '(':
            ++lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_LPAREN;
            return SKY_TRUE;
        case ')':
            --lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_RPAREN;
            return SKY_TRUE;
        case '[':
            ++lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_LBRACKET;
            return SKY_TRUE;
        case ']':
            --lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_RBRACKET;
            return SKY_TRUE;
        case '{':
            ++lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_LBRACE;
            return SKY_TRUE;
        case '}':
            --lexer->nesting_level;
            token->name = SKY_PYTHON_TOKEN_RBRACE;
            return SKY_TRUE;
        case ',':
            token->name = SKY_PYTHON_TOKEN_COMMA;
            return SKY_TRUE;
        case ':':
            token->name = SKY_PYTHON_TOKEN_COLON;
            return SKY_TRUE;
        case '@':
            token->name = SKY_PYTHON_TOKEN_AT;
            return SKY_TRUE;

        case '.':
            switch (sky_python_lexer_getc(lexer)) {
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9':
                    sky_python_lexer_ungetc(lexer);
                    sky_python_lexer_ungetc(lexer);
                    sky_python_lexer_number(lexer, token);
                    return SKY_TRUE;
                case '.':
                    if ('.' == sky_python_lexer_getc(lexer)) {
                        token->name = SKY_PYTHON_TOKEN_ELLIPSIS;
                        return SKY_TRUE;
                    }
                    sky_python_lexer_ungetc(lexer);
                    break;
            }
            sky_python_lexer_ungetc(lexer);
            token->name = SKY_PYTHON_TOKEN_DOT;
            return SKY_TRUE;

        case '\'': case '\"':
            sky_python_lexer_ungetc(lexer);
            sky_python_lexer_string(lexer, token, SKY_FALSE);
            return SKY_TRUE;

        case 'b': case 'B':
            switch (sky_python_lexer_getc(lexer)) {
                case 'r': case 'R':
                    switch (sky_python_lexer_getc(lexer)) {
                        case '\'': case '\"':
                            sky_python_lexer_ungetc(lexer);
                            sky_python_lexer_bytes(lexer, token, SKY_TRUE);
                            return SKY_TRUE;
                        case 0:
                            lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_EOF;
                            break;
                        default:
                            sky_python_lexer_ungetc(lexer);
                            break;
                    }
                    sky_python_lexer_ungetc(lexer); /* 'r' or 'R' */
                    break;
                case '\'': case '\"':
                    sky_python_lexer_ungetc(lexer);
                    sky_python_lexer_bytes(lexer, token, SKY_FALSE);
                    return SKY_TRUE;
                case 0:
                    lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_EOF;
                    break;
                default:
                    sky_python_lexer_ungetc(lexer);
                    break;
            }
            sky_python_lexer_ungetc(lexer); /* 'b' or 'B' */
            sky_python_lexer_identifier(lexer, token);
            return SKY_TRUE;

        case 'r': case 'R':
            switch (sky_python_lexer_getc(lexer)) {
                case 'b': case 'B':
                    switch (sky_python_lexer_getc(lexer)) {
                        case '\'': case '\"':
                            sky_python_lexer_ungetc(lexer);
                            sky_python_lexer_bytes(lexer, token, SKY_TRUE);
                            return SKY_TRUE;
                        case 0:
                            lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_EOF;
                            break;
                        default:
                            sky_python_lexer_ungetc(lexer);
                            break;
                    }
                    sky_python_lexer_ungetc(lexer); /* 'b' or 'B' */
                    break;
                case '\'': case '\"':
                    sky_python_lexer_ungetc(lexer);
                    sky_python_lexer_string(lexer, token, SKY_TRUE);
                    return SKY_TRUE;
                case 0:
                    lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_EOF;
                    break;
                default:
                    sky_python_lexer_ungetc(lexer);
                    break;
            }
            sky_python_lexer_ungetc(lexer); /* 'r' or 'R' */
            sky_python_lexer_identifier(lexer, token);
            return SKY_TRUE;

        case 'u': case 'U':
            switch (sky_python_lexer_getc(lexer)) {
                case '\'': case '\"':
                    sky_python_lexer_ungetc(lexer);
                    sky_python_lexer_string(lexer, token, SKY_TRUE);
                    return SKY_TRUE;
                case 0:
                    lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_EOF;
                    break;
                default:
                    sky_python_lexer_ungetc(lexer);
                    break;
            }
            sky_python_lexer_ungetc(lexer); /* 'u' or 'U' */
            sky_python_lexer_identifier(lexer, token);
            return SKY_TRUE;
    }

    if (sky_unicode_isxid_start(cp)) {
        sky_python_lexer_ungetc(lexer);
        sky_python_lexer_identifier(lexer, token);
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


void
sky_python_lexer_cleanup(sky_python_lexer_t *lexer)
{
    sky_string_data_finalize(&(lexer->decode_buffer));
    if (lexer->buffer.object) {
        sky_buffer_release(&(lexer->buffer));
    }
}


sky_python_lexer_t *
sky_python_lexer_create(sky_object_t    source,
                        sky_string_t    encoding,
                        sky_string_t    filename,
                        sky_string_t    ps1,
                        sky_string_t    ps2,
                        unsigned int    flags)
{
    sky_python_lexer_t  *lexer;

    SKY_ASSET_BLOCK_BEGIN {
        lexer = sky_calloc(1, sizeof(sky_python_lexer_t));
        sky_asset_save(lexer,
                       (sky_free_t)sky_python_lexer_destroy,
                       SKY_ASSET_CLEANUP_ON_ERROR);
        sky_python_lexer_init(lexer,
                              source,
                              encoding,
                              filename,
                              ps1,
                              ps2,
                              flags);
    } SKY_ASSET_BLOCK_END;

    return lexer;
}


void
sky_python_lexer_destroy(sky_python_lexer_t *lexer)
{
    if (lexer) {
        sky_python_lexer_cleanup(lexer);
        sky_free(lexer);
    }
}


void
sky_python_lexer_init(sky_python_lexer_t *  lexer,
                      sky_object_t          source,
                      sky_string_t          encoding,
                      sky_string_t          filename,
                      sky_string_t          ps1,
                      sky_string_t          ps2,
                      unsigned int          flags)
{
    memset(lexer, 0, sizeof(sky_python_lexer_t));

    lexer->flags = flags;
    lexer->source = source;
    lexer->filename = filename;
    lexer->ps1 = ps1;
    lexer->ps2 = ps2;
    if (sky_object_isa(source, sky_string_type)) {
        sky_string_data_t   *source_data;
        
        source_data = sky_string_data(source, &(lexer->source_tagged_data));
        lexer->start = source_data->data.latin1;
        if (source_data->highest_codepoint < 0x100) {
            lexer->end = lexer->start + source_data->len;
            sky_python_lexer_setencoding(lexer,
                                         SKY_STRING_LITERAL("latin1"));
        }
        else if (source_data->highest_codepoint < 0x10000) {
            lexer->end = lexer->start + (source_data->len * 2);
            sky_python_lexer_setencoding(lexer,
                                         SKY_PYTHON_LEXER_UCS2_ENCODING);
        }
        else {
            lexer->end = lexer->start + (source_data->len * 4);
            sky_python_lexer_setencoding(lexer,
                                         SKY_PYTHON_LEXER_UCS4_ENCODING);
        }
    }
    else {
        if (sky_buffer_check(source)) {
            sky_buffer_acquire(&(lexer->buffer),
                               lexer->source,
                               SKY_BUFFER_FLAG_SIMPLE);
            lexer->start = lexer->buffer.buf;
            lexer->end = lexer->start + lexer->buffer.len;
        }
        else if (sky_object_hasattr(source, SKY_STRING_LITERAL("readable")) &&
                 (lexer->readline =
                        sky_object_getattr(source,
                                           SKY_STRING_LITERAL("readline"),
                                           NULL)) != NULL)
        {
            if (!sky_file_readable(source)) {
                sky_error_raise_string(sky_TypeError,
                                       "source must be readable.");
            }

            lexer->flags |= SKY_PYTHON_LEXER_FLAG_FILE_SOURCE;
            if (sky_file_isatty(source)) {
                lexer->flags |= SKY_PYTHON_LEXER_FLAG_INTERACTIVE;
            }

            sky_python_lexer_readline(lexer);
        }
        else {
            sky_error_raise_string(sky_TypeError,
                                   "source must be a file, be a string, "
                                   "or support the buffer protocol.");
        }

        if (encoding) {
            /* If an encoding is specified explicitly, use that encoding
             * and ignore any encoding markers in comments that may be
             * present.
             */
            sky_python_lexer_setencoding(lexer, encoding);
        }
        else if (flags & SKY_PYTHON_COMPILER_FLAG_SOURCE_IS_UTF8) {
            sky_python_lexer_setencoding(lexer,
                                         SKY_STRING_LITERAL("utf-8"));
        }
        else if (lexer->end - lexer->start >= 3 &&
                 lexer->start[0] == 0xEF &&
                 lexer->start[1] == 0xBB &&
                 lexer->start[2] == 0xBF)
        {
            /* There's a UTF-8 BOM present. Skip over it and force the encoding
             * to be UTF-8, ignoring any encoding markers in comments that may
             * be present.
             */
            lexer->start += 3;
            sky_python_lexer_setencoding(lexer,
                                         SKY_STRING_LITERAL("utf-8"));
        }
        else {
            /* Default to UTF-8 encoding, but honor any encoding markers in
             * comments. Note that in this case the encoding must be ASCII
             * based.
             */
            sky_python_lexer_setencoding(lexer,
                                         SKY_STRING_LITERAL("utf-8"));
            lexer->flags &= ~SKY_PYTHON_LEXER_FLAG_ENCODING_SET;
        }
    }

    lexer->flags |= SKY_PYTHON_LEXER_FLAG_NEWLINE;
    lexer->next.byte = lexer->start;
    *(lexer->indent_stack_pointer = lexer->indent_stack) = 0;
    *(lexer->tab_indent_stack_pointer = lexer->tab_indent_stack) = 0;
}


static sky_string_t
sky_python_lexer_line_lookup(const uint8_t *bytes,
                             size_t         nbytes,
                             size_t         width,
                             int            lineno,
                             sky_string_t   encoding)
{
    const uint8_t           *c, *end = bytes + nbytes, *line = bytes;
    sky_string_builder_t    builder;

    end = bytes + nbytes;
    for (line = bytes; --lineno > 0 && line < end; line += width) {
        while (line < end) {
            if (sky_python_lexer_is_newline(line, end, width)) {
                break;
            }
            line += width;
        }
    }
    if (line >= end) {
        return NULL;
    }

    for (c = line;
         c < end && !sky_python_lexer_is_newline(c, end, width);
         c += width);

    builder = sky_string_builder_createwithcapacity(c - line + 1);
    sky_string_builder_appendbytes(builder,
                                   line,
                                   c - line,
                                   encoding,
                                   SKY_STRING_LITERAL("replace"));
    sky_string_builder_appendcodepoint(builder, '\n', 1);
    return sky_string_builder_finalize(builder);
}


sky_string_t
sky_python_lexer_line(sky_python_lexer_t *lexer, int lineno)
{
    size_t          nbytes, width;
    sky_object_t    pos;
    sky_string_t    line;
    const uint8_t   *bytes;

    /* If the requested line is the current line, it's easily available in the
     * buffer, even if the input source is interactive.
     */
    if (lexer->lineno == lineno) {
        const uint8_t           *eof, *eol;
        sky_string_builder_t    builder;

        if (lexer->decode_buffer.len > 0) {
            const sky_string_data_t *s = &(lexer->decode_buffer);

            void *  codepoints = s->data.ascii;
            ssize_t ncodepoints = s->len;
            
            /* lexer->decode_buffer only ever contains a single line at a time.
             * The buffer does include the end of line marker, but that could
             * be any of '\r', '\r\n', or '\n'. Ideally, we want just '\n'. So,
             * look at the buffer tail to figure out what the line ending is
             * rather than simply call sky_string_createfromstringdata().
             */
#define adjust_ncodepoints(_F)                          \
    do {                                                \
        if (s->len >= 2 &&                              \
            '\r' == s->data._F[s->len - 2] &&           \
            '\n' == s->data._F[s->len - 1])             \
        {                                               \
            ncodepoints = s->len - 2;                   \
        }                                               \
        else if ('\r' == s->data._F[s->len - 1]) {      \
            ncodepoints = s->len - 1;                   \
        }                                               \
        else if ('\n' == s->data._F[s->len - 1]) {      \
            return sky_string_createfromstringdata(s);  \
        }                                               \
    } while (0)

            if (s->highest_codepoint < 0x100) {
                adjust_ncodepoints(latin1);
            }
            else if (s->highest_codepoint < 0x10000) {
                adjust_ncodepoints(ucs2);
            }
            else {
                adjust_ncodepoints(ucs4);
            }

#undef adjust_ncodepoints

            builder = sky_string_builder_createwithcapacity(ncodepoints + 1);
            sky_string_builder_appendcodepoints(builder,
                                                codepoints,
                                                ncodepoints,
                                                sky_string_data_width(s));
            sky_string_builder_appendcodepoint(builder, '\n', 1);
            return sky_string_builder_finalize(builder);
        }

        eof = lexer->end;
        eol = lexer->next.byte;
        if (eol > lexer->start && eol >= eof) {
            if (eol - 2 >= lexer->start &&
                sky_python_lexer_is_newline(eol - 2, eof, 1))
            {
                eol -= 2;
            }
            else if (eol - 1 >= lexer->start &&
                     sky_python_lexer_is_newline(eol - 1, eof, 1))
            {
                --eol;
            }
        }
        else {
            while (eol < eof && !sky_python_lexer_is_newline(eol, eof, 1)) {
                ++eol;
            }
        }

        builder = sky_string_builder_createwithcapacity(eol - lexer->start + 1);
        sky_string_builder_appendbytes(builder,
                                       lexer->start,
                                       eol - lexer->start,
                                       lexer->encoding,
                                       SKY_STRING_LITERAL("replace"));
        sky_string_builder_appendcodepoint(builder, '\n', 1);
        return sky_string_builder_finalize(builder);
    }

    /* If we have a memory mapped buffer as an input source, we can simply scan
     * for the line. The encoding could be single, double, quadruple, or
     * variable bytes (UTF-8). For double or quadruple byte code characters,
     * the endian is also an issue.
     */
    if (lexer->buffer.buf && lexer->buffer.len) {
        const sky_codec_t   *codec;

        codec = sky_codec_builtin(lexer->encoding);
        if (codec &&
            2 == (codec->encode_flags & SKY_CODEC_ENCODE_FLAG_WIDTH_MASK))
        {
            width = 2;
        }
        else if (codec &&
                 4 == (codec->encode_flags & SKY_CODEC_ENCODE_FLAG_WIDTH_MASK))
        {
            width = 4;
        }
        else {
            if (!codec ||
                !(codec->encode_flags & SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII))
            {
                return NULL;
            }
            width = 1;
        }
        bytes = lexer->buffer.buf;
        nbytes = lexer->buffer.len;
    }
    /* If the source is a string, there are only three possibilities: Latin-1,
     * UCS2 (native endian), or UCS4 (native endian).
     */
    else if (sky_object_isa(lexer->source, sky_string_type)) {
        sky_string_data_t   *source_data;
        
        source_data = sky_string_data(lexer->source,
                                      &(lexer->source_tagged_data));
        width = sky_string_data_width(source_data);
        bytes = source_data->data.latin1;
        nbytes = source_data->len * width;
    }
    else if (lexer->flags & SKY_PYTHON_LEXER_FLAG_INTERACTIVE) {
        /* There's no way to get the line. The input source is interactive,
         * so the original line has been consumed and is now irretrievable.
         */
        return NULL;
    }
    else if (lexer->readline && sky_file_seekable(lexer->source)) {
        /* Remember the current position and rewind to the beginning. */
        pos = sky_object_callmethod(lexer->source,
                                    SKY_STRING_LITERAL("tell"),
                                    NULL,
                                    NULL);
        sky_object_callmethod(lexer->source,
                              SKY_STRING_LITERAL("seek"),
                              sky_tuple_pack(1, sky_integer_zero),
                              NULL);

        SKY_ERROR_TRY {
            while (lineno-- > 0) {
                line = sky_object_call(lexer->readline, NULL, NULL);
                if (!sky_object_bool(line)) {
                    line = NULL;
                    break;
                }
            }
        } SKY_ERROR_EXCEPT_ANY {
            sky_object_callmethod(lexer->source,
                                  SKY_STRING_LITERAL("seek"),
                                  sky_tuple_pack(1, pos),
                                  NULL);
            sky_error_raise();
        } SKY_ERROR_TRY_END;

        if (line && !sky_object_isa(line, sky_string_type)) {
            line = sky_codec_decode(lexer->codec,
                                    lexer->encoding,
                                    SKY_STRING_LITERAL("replace"),
                                    NULL,
                                    line);
        }

        sky_object_callmethod(lexer->source,
                              SKY_STRING_LITERAL("seek"),
                              sky_tuple_pack(1, pos),
                              NULL);
        return line;
    }
    else {
        return NULL;
    }

    return sky_python_lexer_line_lookup(bytes,
                                        nbytes,
                                        width,
                                        lineno,
                                        lexer->encoding);
}


void
sky_python_lexer_setfilename(sky_python_lexer_t *lexer, sky_string_t filename)
{
    lexer->filename = filename;
}
