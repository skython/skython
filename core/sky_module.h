/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_module Modules
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MODULE_H__
#define __SKYTHON_CORE_SKY_MODULE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The signature of a function called when a module object is visited during
  * garbage collection.
  *
  * @param[in]  module      the module object being visited.
  * @param[in]  data        the module's data pointer.
  * @param[in]  visit_data  the visitation data.
  */
typedef void (*sky_module_visit_t)(sky_module_t             module,
                                   void *                   module_data,
                                   sky_object_visit_data_t *visit_data);


/** Create a new module object instance.
  *
  * @param[in]  name    the name of the module, which will be exposed as the
  *                     module's @c __name__ attribute.
  * @return     the new module object instance.
  */
SKY_EXTERN sky_module_t
sky_module_create(sky_string_t name);


/** Execute initialization code for a module.
  *
  * @param[in]  self    the module to initialize.
  * @param[in]  code    the code to run.
  * @param[in]  file    the filename to use for the @c __file__ attribute.
  * @param[in]  cached  the filename to use for the @c __cached__ attribute.
  */
SKY_EXTERN void
sky_module_initialize(sky_module_t  self,
                      sky_code_t    code,
                      sky_string_t  file,
                      sky_string_t  cached);


/** Return a named attribute from the builtins module.
  * This is a convenience function to import the builtins module and return an
  * an object attribute from it.
  *
  * @param[in]  name    the name of the attribute to return from the builtins
  *                     module.
  * @return     the value of the named attribute from the builtins module.
  */
SKY_EXTERN sky_object_t
sky_module_getbuiltin(const char *name);


/** Return the data pointer stored in a module object.
  *
  * @param[in]  self    the module for which the data pointer is to be returned.
  * @return     the data pointer stored in the module object @a self.
  */
SKY_EXTERN void *
sky_module_getdata(sky_module_t self);


/** Import a module.
  *
  * @param[in]  name    the name of the module to import.
  * @return     the module instance for the named module.
  */
SKY_EXTERN sky_module_t
sky_module_import(sky_string_t name);


/** Import a built-in module.
  * This is normally only used during bootstrapping the import machinery, but
  * there may be other reasons to use it. A new module object is returned for
  * the named built-in module, even if there is already a copy loaded.
  *
  * @param[in]  name    the name of the built-in module to import.
  * @return     the module instance for the named built-in module, or @c NULL
  *             if there is no such built-in module.
  */
SKY_EXTERN sky_module_t
sky_module_import_builtin(sky_string_t name);


/** Import a dynamic module.
  *
  * @param[in]  name        the name of the dynamic module to import.
  * @param[in]  pathname    the filesystem location of the dynamic module.
  * @return     the module instance for the dynamic module.
  */
SKY_EXTERN sky_module_t
sky_module_import_dynamic(sky_string_t  name,
                          sky_string_t  pathname);


/** Import a frozen module.
  * This is normally only used during bootstrapping the import machinery, but
  * there may be other frozen modules that need to be imported for other
  * reasons. Frozen modules are never re-initialized. Importing one always
  * returns an already loaded copy if there is one.
  *
  * @param[in]  name    the name of the frozen module to import.
  * @return     the module instance for the named frozen module, or @c NULL if
  *             there is no such frozen module.
  */
SKY_EXTERN sky_module_t
sky_module_import_frozen(sky_string_t name);


/** Return the importer for a module.
  *
  * @param[in]  name    the name of the module.
  * @return     the importer to use for importing the module.
  */
SKY_EXTERN sky_object_t
sky_module_importer(sky_string_t name);


/** Set an attribute for a module object.
  * This is little more than a wrapper around sky_object_setattr(). The
  * only difference between the two is that this function will also set the
  * @c __module__ attribute for functions, methods, and types. It does not
  * implement __setattr__() for module objects. It is intended to be used only
  * from C code in creating builtin and extension modules.
  *
  * @param[in]  self        the module for which the attribute is to be set.
  * @param[in]  name        the name of the attribute to set.
  * @param[in]  value       the value to set for the attribute.
  */
SKY_EXTERN void
sky_module_setattr(sky_module_t self,
                   const char * name,
                   sky_object_t value);


/** Set the data pointer for a module object.
  * Every module object is capable of storing an opaque data pointer that can
  * be used for any purpose. Initially the data pointer of a module object is
  * @c NULL. Setting the data pointer for a module object that already has a
  * data pointer set will simply replace the references stored; no action is
  * taken on the existing data pointer.
  *
  * @param[in]  self        the module for which the data pointer is to be set.
  * @param[in]  data        the data pointer to store in the module object.
  * @param[in]  visit       a pointer to a function to call when the module
  *                         object is visited for some garbage collection
  *                         related action.
  * @param[in]  destructor  a pointer to a function to call when the module
  *                         object is garbage collected.
  */
SKY_EXTERN void
sky_module_setdata(sky_module_t         self,
                   void *               data,
                   sky_module_visit_t   visit,
                   sky_free_t           destructor);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MODULE_H__ */

/** @} **/
/** @} **/
