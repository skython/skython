#ifndef __SKYTHON_CORE_SKY_FUNCTION_PRIVATE_H__
#define __SKYTHON_CORE_SKY_FUNCTION_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_function.h"


typedef enum sky_function_flag_e {
    SKY_FUNCTION_FLAG_BUILTIN       =   0x1,
    SKY_FUNCTION_FLAG_METHOD        =   0x2,
    SKY_FUNCTION_FLAG_DEPRECATED    =   0x4,
    SKY_FUNCTION_FLAG_FASTCALLABLE  =   0x8,
} sky_function_flag_t;

typedef struct sky_function_data_s {
    sky_string_t                        doc;            /* __doc__          */
    sky_string_t                        name;           /* __name__         */
    sky_string_t                        qualname;       /* __qualname__     */
    sky_string_t                        module;         /* __module__       */
    sky_object_t                        code;           /* __code__         */
    sky_dict_t                          globals;        /* __globals__      */
    sky_tuple_t                         closure;        /* __closure__      */
    sky_dict_t                          annotations;    /* __annotations__  */

    sky_parameter_list_t                parameters;

    sky_string_t                        deprecated_message;

    uint32_t                            flags;
} sky_function_data_t;

SKY_EXTERN_INLINE sky_function_data_t *
sky_function_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_function_type) {
        return (sky_function_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_function_type);
}

struct sky_function_s {
    sky_object_data_t                   object_data;
    sky_function_data_t                 function_data;
};


extern sky_bool_t
sky_function_fastcallable(sky_function_t self);

extern void
sky_function_setfastcallable(sky_function_t self, sky_bool_t flag);


#endif  /* __SKYTHON_CORE_SKY_FUNCTION_PRIVATE_H__ */
