#include "sky_private.h"


typedef struct sky_mappingproxy_data_s {
    sky_object_t                        mapping;
} sky_mappingproxy_data_t;

SKY_EXTERN_INLINE sky_mappingproxy_data_t *
sky_mappingproxy_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_mappingproxy_type) {
        return (sky_mappingproxy_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_mappingproxy_type);
}

SKY_EXTERN_INLINE sky_object_t
sky_mappingproxy_mapping(sky_object_t object)
{
    return sky_mappingproxy_data(object)->mapping;
}


struct sky_mappingproxy_s {
    sky_object_data_t                   object_data;
    sky_mappingproxy_data_t             proxy_data;
};


static void
sky_mappingproxy_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_mappingproxy_data_t *self_data = data;

    sky_object_visit(self_data->mapping, visit_data);
}


sky_bool_t
sky_mappingproxy_contains(sky_mappingproxy_t self, sky_object_t item)
{
    return sky_object_contains(sky_mappingproxy_mapping(self), item);
}


sky_object_t
sky_mappingproxy_copy(sky_mappingproxy_t self)
{
    return sky_object_callmethod(sky_mappingproxy_mapping(self),
                                 SKY_STRING_LITERAL("copy"),
                                 NULL,
                                 NULL);
}


sky_mappingproxy_t
sky_mappingproxy_create(sky_object_t mapping)
{
    sky_object_t            keys;
    sky_mappingproxy_t      self;
    sky_mappingproxy_data_t *self_data;

    if (!sky_object_isa(mapping, sky_dict_type)) {
        if (!SKY_OBJECT_METHOD_SLOT(mapping, GETITEM) ||
            !(keys = sky_object_getattr(mapping,
                                        SKY_STRING_LITERAL("keys"),
                                        NULL)) ||
            !sky_object_callable(keys))
        {
            sky_error_raise_format(sky_TypeError,
                                   "expected mapping; got %#@",
                                   sky_type_name(sky_object_type(mapping)));
        }
    }

    self = sky_object_allocate(sky_mappingproxy_type);
    self_data = sky_mappingproxy_data(self);
    sky_object_gc_set(&(self_data->mapping), mapping, self);

    return self;
}


sky_object_t
sky_mappingproxy_eq(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_eq(sky_mappingproxy_mapping(self), other);
}


sky_object_t
sky_mappingproxy_ge(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_ge(sky_mappingproxy_mapping(self), other);
}


sky_object_t
sky_mappingproxy_get(sky_mappingproxy_t self, sky_tuple_t args, sky_dict_t kws)
{
    return sky_object_callmethod(sky_mappingproxy_mapping(self),
                                 SKY_STRING_LITERAL("get"),
                                 args,
                                 kws);
}


sky_object_t
sky_mappingproxy_getitem(sky_mappingproxy_t self, sky_object_t item)
{
    return sky_object_getitem(sky_mappingproxy_mapping(self), item);
}


sky_object_t
sky_mappingproxy_gt(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_gt(sky_mappingproxy_mapping(self), other);
}


sky_object_t
sky_mappingproxy_items(sky_mappingproxy_t self)
{
    return sky_object_callmethod(sky_mappingproxy_mapping(self),
                                 SKY_STRING_LITERAL("items"),
                                 NULL,
                                 NULL);
}


sky_object_t
sky_mappingproxy_iter(sky_mappingproxy_t self)
{
    return sky_object_iter(sky_mappingproxy_mapping(self));
}


sky_object_t
sky_mappingproxy_keys(sky_mappingproxy_t self)
{
    return sky_object_callmethod(sky_mappingproxy_mapping(self),
                                 SKY_STRING_LITERAL("keys"),
                                 NULL,
                                 NULL);
}


sky_object_t
sky_mappingproxy_le(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_le(sky_mappingproxy_mapping(self), other);
}


ssize_t
sky_mappingproxy_len(sky_mappingproxy_t self)
{
    return sky_object_len(sky_mappingproxy_mapping(self));
}


sky_object_t
sky_mappingproxy_lt(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_lt(sky_mappingproxy_mapping(self), other);
}


sky_object_t
sky_mappingproxy_ne(sky_mappingproxy_t self, sky_object_t other)
{
    return sky_object_ne(sky_mappingproxy_mapping(self), other);
}


sky_object_t
sky_mappingproxy_new(sky_type_t cls, sky_object_t mapping)
{
    sky_mappingproxy_t      self;
    sky_mappingproxy_data_t *self_data;

    /* This is roughly equivalent to CPython's PyMapping_Check().
     * Skython has no separation between __getitem__ implemented for a mapping
     * and __getitem__ implemented for a sequence for built-in types as CPython
     * does; however, most of its built-in types implement both.
     * mappingproxy("foo") is valid in CPython, and it is here too, even though
     * it won't actually do anything useful. A better check might be
     * __getitem__ and keys.
     */
    if (!SKY_OBJECT_METHOD_SLOT(mapping, GETITEM) ||
        sky_object_isa(mapping, sky_list_type) ||
        sky_object_isa(mapping, sky_tuple_type))
    {
        sky_error_raise_format(
                sky_TypeError,
                "mappingproxy() argument must be a mapping, not %#@",
                sky_type_name(sky_object_type(mapping)));
    }

    self = sky_object_allocate(cls);
    self_data = sky_mappingproxy_data(self);
    sky_object_gc_set(&(self_data->mapping), mapping, self);

    return self;
}


sky_string_t
sky_mappingproxy_repr(sky_mappingproxy_t self)
{
    return sky_string_createfromformat("mappingproxy(%#@)",
                                       sky_mappingproxy_mapping(self));
}


sky_string_t
sky_mappingproxy_str(sky_mappingproxy_t self)
{
    return sky_object_str(sky_mappingproxy_mapping(self));
}


sky_object_t
sky_mappingproxy_values(sky_mappingproxy_t self)
{
    return sky_object_callmethod(sky_mappingproxy_mapping(self),
                                 SKY_STRING_LITERAL("values"),
                                 NULL,
                                 NULL);
}


SKY_TYPE_DEFINE_SIMPLE(mappingproxy,
                       "mappingproxy",
                       sizeof(sky_mappingproxy_data_t),
                       NULL,
                       NULL,
                       sky_mappingproxy_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);
                        

void
sky_mappingproxy_initialize_library(void)
{
    sky_type_setattr_builtin(
            sky_mappingproxy_type,
            "copy",
            sky_function_createbuiltin(
                    "mappingproxy.copy",
                    "D.copy() -> a shallow copy of D",
                    (sky_native_code_function_t)sky_mappingproxy_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mappingproxy_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_mappingproxy_type,
            "get",
            sky_function_createbuiltin(
                    "mappingproxy.get",
                    "D.get(k[, d]) -> D[k] if k in D, else d.",
                    (sky_native_code_function_t)sky_mappingproxy_get,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mappingproxy_type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_mappingproxy_type,
            "items",
            sky_function_createbuiltin(
                    "mappingproxy.items",
                    "D.items() -> list of D's (key, value) pairs, as 2-tuples",
                    (sky_native_code_function_t)sky_mappingproxy_items,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mappingproxy_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_mappingproxy_type,
            "keys",
            sky_function_createbuiltin(
                    "mappingproxy.keys",
                    "D.keys() -> list of D's keys",
                    (sky_native_code_function_t)sky_mappingproxy_keys,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mappingproxy_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_mappingproxy_type,
            "values",
            sky_function_createbuiltin(
                    "mappingproxy.values",
                    "D.values() -> list of D's values",
                    (sky_native_code_function_t)sky_mappingproxy_values,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mappingproxy_type,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_mappingproxy_type,
            "__new__",
            (sky_native_code_function_t)sky_mappingproxy_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "mapping", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_mappingproxy_type,
            "__lt__", sky_mappingproxy_lt,
            "__le__", sky_mappingproxy_le,
            "__eq__", sky_mappingproxy_eq,
            "__ne__", sky_mappingproxy_ne,
            "__gt__", sky_mappingproxy_gt,
            "__ge__", sky_mappingproxy_ge,
            "__repr__", sky_mappingproxy_repr,
            "__str__", sky_mappingproxy_str,
            "__len__", sky_mappingproxy_len,
            "__getitem__", sky_mappingproxy_getitem,
            "__iter__", sky_mappingproxy_iter,
            "__contains__", sky_mappingproxy_contains,
            NULL);
}
