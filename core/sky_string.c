#include "sky_private.h"
#include "sky_string_private.h"
#include "sky_codec_private.h"

#include "sky_unicode_private.h"
#include "sky_unicode_decomp.h"


/* Constants for Hangul decomposition (Section 3.12) */
static const sky_unicode_char_t LBase = 0x1100, SBase = 0xAC00,
                                TBase = 0x11A7, VBase = 0x1161;
static int LCount = 19, TCount = 28, VCount = 21;
static int NCount = 588;    /* VCount * TCount */
static int SCount = 11172;  /* LCount * NCount */


static sky_unicode_decomp_data_t sky_unicode_decomp_data_struct = {
    sky_unicode_decomp_data,
    sky_unicode_decomp_table,
    sky_unicode_decomp_shift,
    sky_unicode_decomp_index_1,
    sky_unicode_decomp_index_2,
    sky_unicode_compose_table,
    sky_unicode_compose_shift,
    sky_unicode_compose_index_1,
    sky_unicode_compose_index_2,
};
const sky_unicode_decomp_data_t * const sky_unicode_decomp_tables =
    &sky_unicode_decomp_data_struct;


static inline sky_unicode_decomp_t *
sky_string_decomp_lookup(const sky_unicode_decomp_data_t *  data,
                         sky_unicode_char_t                 c)
{
    int index;

    sky_error_validate_debug(c >= SKY_UNICODE_CHAR_MIN);
    sky_error_validate_debug(c <= SKY_UNICODE_CHAR_MAX);
    index = data->index_1[c >> data->shift] << data->shift;
    index = data->index_2[index + (c & ((1 << data->shift) - 1))];
    return &(data->table[index]);
}

static inline int64_t *
sky_string_compose_lookup(const sky_unicode_decomp_data_t * data,
                          sky_unicode_char_t                c)
{
    int index;

    sky_error_validate_debug(c >= SKY_UNICODE_CHAR_MIN);
    sky_error_validate_debug(c <= SKY_UNICODE_CHAR_MAX);
    index = data->compose_index_1[c >> data->compose_shift] << data->compose_shift;
    index = data->compose_index_2[index + (c & ((1 << data->compose_shift) - 1))];
    return &(data->compose_table[index]);
}

static inline sky_unicode_char_t
sky_string_composite(const sky_unicode_decomp_data_t *  data,
                     const sky_unicode_char_t           L,
                     const sky_unicode_char_t           C)
{
    int64_t *P;

    for (P = sky_string_compose_lookup(data, L); (*P & 0x1FFFFF) == L; ++P) {
        if (((*P >> 21) & 0x1FFFFF) == C) {
            return ((*P >> 42) & 0x1FFFFF);
        }
    }

    return -1;
}


static inline sky_string_t
sky_string_new_string(sky_string_t new_string, sky_string_t original_string)
{
    if (sky_object_istagged(new_string)) {
        return new_string;
    }
    if (!new_string) {
        return sky_string_copy(original_string);
    }
    if (new_string == original_string) {
        return sky_string_copy(original_string);
    }
    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


#define SKY_STRING_WIDTH 1
#include "templates/sky_string_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 2
#include "templates/sky_string_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 4
#include "templates/sky_string_t.c"
#undef SKY_STRING_WIDTH


sky_string_cache_t sky_string_cache_instance = NULL;


static void
sky_string_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_string_data_finalize(data);
}


static void
sky_string_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_string_iterator_data_t  *iterator_data = data;

    sky_object_visit(iterator_data->string, visit_data);
}


sky_string_t
sky_string_add(sky_string_t self, sky_string_t other)
{
    size_t              other_width, self_width;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data, *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (!sky_object_isa(self, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "Can't convert %#@ object to str implicitly",
                               sky_type_name(sky_object_type(self)));
    }
    if (!sky_object_isa(other, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "Can't convert %#@ object to str implicitly",
                               sky_type_name(sky_object_type(other)));
    }

    if (sky_string_empty == self) {
        return sky_string_copy(other);
    }
    if (sky_string_empty == other) {
        return sky_string_copy(self);
    }

    self_data = sky_string_data(self, &self_tagged_data);
    self_width = sky_string_data_width(self_data);
    other_data = sky_string_data(other, &other_tagged_data);
    other_width = sky_string_data_width(other_data);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data,
                           self_data->len + other_data->len,
                           SKY_MAX(self_width, other_width));
    sky_string_data_combine(new_string_data, self_data);
    sky_string_data_combine(new_string_data, other_data);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_string_t
sky_string_capitalize(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    if (!(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                        SKY_UNICODE_CTYPE_FLAG_TITLE |
                                        SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return sky_string_copy(self);
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_capitalize_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_capitalize_ucs2(self_data);
    }
    else {
        new_string = sky_string_capitalize_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_casefold(sky_string_t self)
{
    sky_string_t        new_string;
    unsigned int        flags;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    flags = self_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_MASK;
    if (!flags) {
        return sky_string_copy(self);
    }
    if (!(flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL)) {
        if (flags == SKY_UNICODE_CTYPE_FLAG_CASEFOLD_LOWER &&
            !(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_TITLE |
                                            SKY_UNICODE_CTYPE_FLAG_UPPER)))
        {
            return sky_string_copy(self);
        }
        if (flags == SKY_UNICODE_CTYPE_FLAG_CASEFOLD_UPPER &&
            !(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                            SKY_UNICODE_CTYPE_FLAG_TITLE)))
        {
            return sky_string_copy(self);
        }
        if (flags == SKY_UNICODE_CTYPE_FLAG_CASEFOLD_TITLE &&
            !(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                            SKY_UNICODE_CTYPE_FLAG_UPPER)))
        {
            return sky_string_copy(self);
        }
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_casefold_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_casefold_ucs2(self_data);
    }
    else {
        new_string = sky_string_casefold_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_center(sky_string_t self, ssize_t width, sky_string_t fillchar)
{
    size_t              fillchar_width, new_string_width, self_width;
    ssize_t             head, margin, tail;
    sky_string_t        new_string;
    sky_string_data_t   *fillchar_data, *new_string_data, *self_data,
                        tagged_fillchar_data, tagged_self_data;
    sky_unicode_char_t  fillchar_codepoint;

    self_data = sky_string_data(self, &tagged_self_data);
    fillchar_data = sky_string_data(fillchar, &tagged_fillchar_data);
    if (fillchar_data->len != 1) {
        sky_error_raise_string(
                sky_TypeError,
                "The fill character must be exactly one character long");
    }
    if (width <= self_data->len) {
        return sky_string_copy(self);
    }
    if (fillchar_data->highest_codepoint < 0x100) {
        fillchar_codepoint = fillchar_data->data.latin1[0];
        fillchar_width = 1;
    }
    else if (fillchar_data->highest_codepoint < 0x10000) {
        fillchar_codepoint = fillchar_data->data.ucs2[0];
        fillchar_width = 2;
    }
    else {
        fillchar_codepoint = fillchar_data->data.ucs4[0];
        fillchar_width = 4;
    }

    self_width = sky_string_data_width(self_data);
    new_string_width = SKY_MAX(fillchar_width, self_width);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, width, new_string_width);

    margin = width - self_data->len;
    head = (margin / 2) + (margin & width & 1);
    tail = margin - head;

    sky_string_data_append_codepoint(new_string_data, fillchar_codepoint, head);
    sky_string_data_combine(new_string_data, self_data);
    sky_string_data_append_codepoint(new_string_data, fillchar_codepoint, tail);

    return sky_string_new_string(new_string, self);
}


sky_unicode_char_t
sky_string_charat(sky_string_t self, ssize_t index)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data_noctype(self, &tagged_data);
    if (index < 0) {
        index += string_data->len;
    }
    if (index < 0 || index >= string_data->len) {
        sky_error_raise_string(sky_IndexError,
                               "string index out of range");
    }

    if (string_data->highest_codepoint < 0x100) {
        return string_data->data.latin1[index];
    }
    if (string_data->highest_codepoint < 0x10000) {
        return string_data->data.ucs2[index];
    }
    return string_data->data.ucs4[index];
}


sky_string_t
sky_string_clone(sky_string_t self, sky_type_t type)
{
    sky_string_t        clone;
    sky_string_data_t   *clone_data, *self_data, tagged_self_data;

    if (!type) {
        type = sky_string_type;
    }

    if (sky_object_istagged(self) && sky_string_type == type) {
        sky_error_validate_debug(sky_object_type(self) == sky_string_type);
        return self;
    }
    self_data = sky_string_data(self, &tagged_self_data);

    clone = sky_object_allocate(type);
    clone_data = sky_string_data(clone, NULL);
    sky_string_data_copy(clone_data, self_data);

    if (sky_string_type == type) {
        clone = sky_string_cache_string(sky_string_cache(), clone, SKY_FALSE);
    }
    return clone;
}


sky_bool_t
sky_string_contains(sky_string_t self, sky_string_t item)
{
    ssize_t             index;
    sky_string_data_t   *item_data, item_tagged_data,
                        *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    item_data = sky_string_data(item, &item_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        index = sky_string_search_latin1(self_data,
                                         item_data,
                                         0,
                                         self_data->len,
                                         1,
                                         0);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        index = sky_string_search_ucs2(self_data,
                                       item_data,
                                       0,
                                       self_data->len,
                                       1,
                                       0);
    }
    else {
        index = sky_string_search_ucs4(self_data,
                                       item_data,
                                       0,
                                       self_data->len,
                                       1,
                                       0);
    }

    return (index >= 0 ? SKY_TRUE : SKY_FALSE);
}


sky_string_t
sky_string_copy(sky_string_t self)
{
    if (sky_object_istagged(self)) {
        return self;
    }
    if (sky_object_type(self) == sky_string_type) {
        return self;
    }

    return sky_string_createfromstringdata(sky_string_data(self, NULL));
}


ssize_t
sky_string_count(sky_string_t   self,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end,
                 ssize_t        maxcount)
{
    sky_string_data_t   *self_data, self_tagged_data,
                        *sub_data, sub_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    sub_data = sky_string_data(sub, &sub_tagged_data);

    if (self_data->highest_codepoint < 0x100) {
        return sky_string_search_latin1(self_data,
                                        sub_data,
                                        start,
                                        end,
                                        0,
                                        maxcount);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_search_ucs2(self_data,
                                      sub_data,
                                      start,
                                      end,
                                      0,
                                      maxcount);
    }
    return sky_string_search_ucs4(self_data,
                                  sub_data,
                                  start,
                                  end,
                                  0,
                                  maxcount);
}


sky_string_t
sky_string_createfrombytes(const void * bytes,
                           size_t       nbytes,
                           sky_string_t encoding,
                           sky_string_t errors)
{
    sky_string_t        string;
    sky_string_data_t   *string_data;

    if (!encoding) {
        string = sky_string_cache_lookup(sky_string_cache(), bytes, nbytes);
        if (string) {
            return string;
        }
    }

    string = sky_object_allocate(sky_string_type);
    string_data = sky_string_data(string, NULL);
    sky_string_data_decode(string_data, encoding, errors, bytes, nbytes);

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


sky_string_t
sky_string_createfromcodepoint(sky_unicode_char_t codepoint)
{
    if (codepoint < SKY_UNICODE_CHAR_MIN || codepoint > SKY_UNICODE_CHAR_MAX) {
        sky_error_raise_format(sky_ValueError,
                               "codepoint not in range(%d)",
                               SKY_UNICODE_CHAR_MAX + 1);
    }
    return SKY_OBJECT_MAKETAG(0x3, codepoint);
}


sky_string_t
sky_string_createfromfilename(const void *bytes, size_t nbytes)
{
    sky_string_t        string;
    sky_string_data_t   *string_data;

    string = sky_object_allocate(sky_string_type);
    string_data = sky_string_data(string, NULL);
    sky_string_data_decode(string_data,
                           sky_codec_filesystemencoding_string,
                           sky_codec_filesystemerrors_string,
                           bytes,
                           nbytes);

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


static void
sky_string_createfromformat_output(const void * bytes,
                                   ssize_t      nbytes,
                        SKY_UNUSED size_t       width,
                                   void *       arg)
{
    sky_string_data_decode((sky_string_data_t *)arg, NULL, NULL, bytes, nbytes);
}


sky_string_t
sky_string_createfromformat(const char *format, ...)
{
    va_list         ap;
    sky_string_t    string;

    string = sky_object_allocate(sky_string_type);
    va_start(ap, format);
    sky_format_voutput(sky_string_createfromformat_output,
                       sky_string_data(string, NULL),
                       format,
                       ap);
    va_end(ap);

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


sky_string_t
sky_string_createfromformatv(const char *format, va_list ap)
{
    sky_string_t    string;

    string = sky_object_allocate(sky_string_type);
    sky_format_voutput(sky_string_createfromformat_output,
                       sky_string_data(string, NULL),
                       format,
                       ap);

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


sky_string_t
sky_string_createfromliteral(const void *bytes, size_t nbytes)
{
    sky_string_t        string;
    sky_string_data_t   *string_data;

    string = sky_string_cache_lookup(sky_string_cache(), bytes, nbytes);
    if (string) {
        return string;
    }

    string = sky_object_allocate(sky_string_type);
    string_data = sky_string_data(string, NULL);
    sky_string_data_decode(string_data, NULL, NULL, bytes, nbytes);

    return sky_string_cache_literal(sky_string_cache(), bytes, nbytes, string);
}


sky_string_t
sky_string_createfromobject(sky_object_t object)
{
    if (sky_object_type(object) == sky_string_type) {
        return object;
    }
    if (sky_object_isa(object, sky_string_type)) {
        return sky_string_copy(object);
    }
    sky_error_raise_format(sky_TypeError,
                           "Can't convert %#@ object to %@ implicitly",
                           sky_type_name(sky_object_type(object)),
                           sky_type_name(sky_string_type));
}


sky_string_t
sky_string_createfromcodepoints(const void *bytes,
                                ssize_t     nbytes,
                                size_t      width)
{
    sky_string_t        string;
    sky_string_data_t   *string_data;

    sky_error_validate_debug(1 == width || 2 == width || 4 == width);
    sky_error_validate_debug(!(nbytes % width));

    if (!nbytes) {
        return sky_string_empty;
    }
    if (1 == nbytes / width) {
        sky_unicode_char_t  cp;

        if (1 == width) {
            cp = ((uint8_t *)bytes)[0];
        }
        else if (2 == width) {
            cp = ((uint16_t *)bytes)[0];
        }
        else if (4 == width) {
            cp = ((sky_unicode_char_t *)bytes)[0];
        }
        else {
            sky_error_fatal("internal error");
        }
        return SKY_OBJECT_MAKETAG(0x3, cp);
    }
#if defined(SKY_ARCH_64BIT)
    if (2 == nbytes / width) {
        uintptr_t           extra;
        sky_unicode_char_t  cp, cp2;

        if (1 == width) {
            cp = ((uint8_t *)bytes)[0];
            cp2 = ((uint8_t *)bytes)[1];
        }
        else if (2 == width) {
            cp = ((uint16_t *)bytes)[0];
            cp2 = ((uint16_t *)bytes)[1];
        }
        else if (4 == width) {
            cp = ((sky_unicode_char_t *)bytes)[0];
            cp2 = ((sky_unicode_char_t *)bytes)[1];
        }
        else {
            sky_error_fatal("internal error");
        }
        extra = ((uintptr_t)cp << 21ull) | (uintptr_t)cp2;
        return SKY_OBJECT_MAKETAG(0x4, extra);
    }
#endif

    string = sky_object_allocate(sky_string_type);
    string_data = sky_string_data(string, NULL);
    sky_string_data_append(string_data, bytes, nbytes, width);

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


sky_string_t
sky_string_createfromstringdata(const sky_string_data_t *string_data)
{
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_copy(new_string_data, string_data);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_string_t
sky_string_createwithcodepoints(void *      bytes,
                                ssize_t     nbytes,
                                size_t      width,
                                sky_free_t  free)
{
    size_t              string_nbytes, string_width;
    ssize_t             i;
    sky_string_t        string;
    sky_string_data_t   *string_data;

    sky_error_validate_debug(1 == width || 2 == width || 4 == width);
    sky_error_validate_debug(!(nbytes % width));

    if (!nbytes) {
        if (free) {
            free(bytes);
        }
        return sky_string_empty;
    }
    if (1 == nbytes / width) {
        sky_unicode_char_t  cp;

        if (1 == width) {
            cp = ((uint8_t *)bytes)[0];
        }
        else if (2 == width) {
            cp = ((uint16_t *)bytes)[0];
        }
        else if (4 == width) {
            cp = ((sky_unicode_char_t *)bytes)[0];
        }
        else {
            sky_error_fatal("internal error");
        }
        if (free) {
            free(bytes);
        }
        return SKY_OBJECT_MAKETAG(0x3, cp);
    }
#if defined(SKY_ARCH_64BIT)
    if (2 == nbytes / width) {
        uintptr_t           extra;
        sky_unicode_char_t  cp, cp2;

        if (1 == width) {
            cp = ((uint8_t *)bytes)[0];
            cp2 = ((uint8_t *)bytes)[1];
        }
        else if (2 == width) {
            cp = ((uint16_t *)bytes)[0];
            cp2 = ((uint16_t *)bytes)[1];
        }
        else if (4 == width) {
            cp = ((sky_unicode_char_t *)bytes)[0];
            cp2 = ((sky_unicode_char_t *)bytes)[1];
        }
        else {
            sky_error_fatal("internal error");
        }
        extra = ((uintptr_t)cp << 21ull) | (uintptr_t)cp2;
        if (free) {
            free(bytes);
        }
        return SKY_OBJECT_MAKETAG(0x4, extra);
    }
#endif

    string = sky_object_allocate(sky_string_type);
    string_data = sky_string_data(string, NULL);

    string_data->len = nbytes / width;
    string_data->data.ascii = bytes;
    if (!free) {
        string_data->flags |= SKY_STRING_FLAG_NO_FREE;
    }
    string_data->u.free = free;

    if (1 == width) {
        const uint8_t   *c, *end;

        end = string_data->data.latin1 + string_data->len;
        for (c = string_data->data.latin1; c < end; ++c) {
            sky_string_data_update_1(string_data, *c);
        }
    }
    else if (2 == width) {
        const uint16_t  *c, *end;

        end = string_data->data.ucs2 + string_data->len;
        for (c = string_data->data.ucs2; c < end; ++c) {
            sky_string_data_update(string_data, *c);
        }
    }
    else if (4 == width) {
        const sky_unicode_char_t    *c, *end;

        end = string_data->data.ucs4 + string_data->len;
        for (c = string_data->data.ucs4; c < end; ++c) {
            if (*c > SKY_UNICODE_CHAR_MAX) {
                sky_error_raise_format(sky_ValueError,
                                       "codepoint %#x is invalid.",
                                       *c);
            }
            sky_string_data_update(string_data, *c);
        }
    }
    else {
        sky_error_fatal("internal error");
    }

    string_width = sky_string_data_width(string_data);
    if (string_width != width) {
        string_data->flags &= ~SKY_STRING_FLAG_NO_FREE;
        string_data->u.free = NULL;

        string_nbytes = (string_data->len + 1) * string_width;
        if (string_nbytes <= sizeof(string_data->u.small_data)) {
            string_data->data.ascii = string_data->u.small_data;
        }
        else {
            string_data->data.ascii = sky_malloc(string_nbytes);
        }

#define narrow(_t, _s)                                  \
        do {                                            \
            _s *source = bytes;                         \
            for (i = 0; i < string_data->len; ++i) {    \
                string_data->data._t[i] = source[i];    \
            }                                           \
        } while (0)

        if (string_data->highest_codepoint < 0x100) {
            if (2 == width) {
                narrow(latin1, uint16_t);
            }
            else {
                narrow(latin1, sky_unicode_char_t);
            }
        }
        else if (string_data->highest_codepoint < 0x10000) {
            narrow(ucs2, sky_unicode_char_t);
        }
        else {
            sky_error_fatal("internal error");
        }

#undef narrow

        if (free) {
            free(bytes);
        }
    }

    return sky_string_cache_string(sky_string_cache(), string, SKY_FALSE);
}


static const char *sky_string_1bytes[256] = {
    "\x00", "\x01", "\x02", "\x03", "\x04", "\x05", "\x06", "\x07",
    "\x08", "\x09", "\x0A", "\x0B", "\x0C", "\x0D", "\x0E", "\x0F",
    "\x10", "\x11", "\x12", "\x13", "\x14", "\x15", "\x16", "\x17",
    "\x18", "\x19", "\x1A", "\x1B", "\x1C", "\x1D", "\x1E", "\x1F",
    "\x20", "\x21", "\x22", "\x23", "\x24", "\x25", "\x26", "\x27",
    "\x28", "\x29", "\x2A", "\x2B", "\x2C", "\x2D", "\x2E", "\x2F",
    "\x30", "\x31", "\x32", "\x33", "\x34", "\x35", "\x36", "\x37",
    "\x38", "\x39", "\x3A", "\x3B", "\x3C", "\x3D", "\x3E", "\x3F",
    "\x40", "\x41", "\x42", "\x43", "\x44", "\x45", "\x46", "\x47",
    "\x48", "\x49", "\x4A", "\x4B", "\x4C", "\x4D", "\x4E", "\x4F",
    "\x50", "\x51", "\x52", "\x53", "\x54", "\x55", "\x56", "\x57",
    "\x58", "\x59", "\x5A", "\x5B", "\x5C", "\x5D", "\x5E", "\x5F",
    "\x60", "\x61", "\x62", "\x63", "\x64", "\x65", "\x66", "\x67",
    "\x68", "\x69", "\x6A", "\x6B", "\x6C", "\x6D", "\x6E", "\x6F",
    "\x70", "\x71", "\x72", "\x73", "\x74", "\x75", "\x76", "\x77",
    "\x78", "\x79", "\x7A", "\x7B", "\x7C", "\x7D", "\x7E", "\x7F",
    "\x80", "\x81", "\x82", "\x83", "\x84", "\x85", "\x86", "\x87",
    "\x88", "\x89", "\x8A", "\x8B", "\x8C", "\x8D", "\x8E", "\x8F",
    "\x90", "\x91", "\x92", "\x93", "\x94", "\x95", "\x96", "\x97",
    "\x98", "\x99", "\x9A", "\x9B", "\x9C", "\x9D", "\x9E", "\x9F",
    "\xA0", "\xA1", "\xA2", "\xA3", "\xA4", "\xA5", "\xA6", "\xA7",
    "\xA8", "\xA9", "\xAA", "\xAB", "\xAC", "\xAD", "\xAE", "\xAF",
    "\xB0", "\xB1", "\xB2", "\xB3", "\xB4", "\xB5", "\xB6", "\xB7",
    "\xB8", "\xB9", "\xBA", "\xBB", "\xBC", "\xBD", "\xBE", "\xBF",
    "\xC0", "\xC1", "\xC2", "\xC3", "\xC4", "\xC5", "\xC6", "\xC7",
    "\xC8", "\xC9", "\xCA", "\xCB", "\xCC", "\xCD", "\xCE", "\xCF",
    "\xD0", "\xD1", "\xD2", "\xD3", "\xD4", "\xD5", "\xD6", "\xD7",
    "\xD8", "\xD9", "\xDA", "\xDB", "\xDC", "\xDD", "\xDE", "\xDF",
    "\xE0", "\xE1", "\xE2", "\xE3", "\xE4", "\xE5", "\xE6", "\xE7",
    "\xE8", "\xE9", "\xEA", "\xEB", "\xEC", "\xED", "\xEE", "\xEF",
    "\xF0", "\xF1", "\xF2", "\xF3", "\xF4", "\xF5", "\xF6", "\xF7",
    "\xF8", "\xF9", "\xFA", "\xFB", "\xFC", "\xFD", "\xFE", "\xFF",
};

#if defined(SKY_ARCH_64BIT)
/* This will be 63 for a 64-byte cache line size. */
#define sky_string_2bytes_slotsize  (SKY_CACHE_LINE_SIZE / 3)

/* This will be 3121 for a 64-byte cache line size. */
#define sky_string_2bytes_slotcount                                     \
                ((256 * 256 * 3) + (sky_string_2bytes_slotsize - 1)) /  \
                sky_string_2bytes_slotsize

static const char *sky_string_2bytes[sky_string_2bytes_slotcount];
#endif

const void *
sky_string_cstring(sky_string_t self)
{
    sky_string_data_t   *self_data;

    if (sky_object_istagged(self)) {
        if (sky_string_empty == self) {
            return "";
        }
        if (SKY_OBJECT_TAG(self) == 0x3) {
            sky_unicode_char_t  cp = ((uintptr_t)self >> 4);

            if (cp >= 0x100) {
                return NULL;
            }
            return sky_string_1bytes[cp];
        }
#if defined(SKY_ARCH_64BIT)
        /* The table for this would be 196608 bytes, which is kind of
         * outrageous for such a seldom use. We'll build the table as needed,
         * but split it into cache-line sized chunks. If the table were to ever
         * be filled entirely, it would waste about 1K for a 64-byte cache line
         * size. Much better than waste 196608 bytes for a table that will not
         * likely ever be used (plus the initialization time).
         */
        if (SKY_OBJECT_TAG(self) == 0x4) {
            size_t              i, index, offset, slot;
            uint8_t             *chunk, *b;
            uint16_t            value;
            sky_unicode_char_t  cp = ((uintptr_t)self >> 25) & 0x1FFFFF,
                                cp2 = ((uintptr_t)self >> 4) & 0x1FFFFF;

            if (cp >= 0x100 || cp2 >= 0x100) {
                return NULL;
            }
            index = (cp << 8) | cp2;
            slot = (index * 3) / sky_string_2bytes_slotsize;
            offset = (index * 3) % sky_string_2bytes_slotsize;

            if (!sky_string_2bytes[slot]) {
                chunk = b = sky_malloc(SKY_CACHE_LINE_SIZE);
                value = (slot * sky_string_2bytes_slotsize) / 3;
                for (i = 0; i < sky_string_2bytes_slotsize; ++i, ++value) {
                    b[0] = (value >> 0x8) & 0xFF;
                    b[1] = value & 0xFF;
                    b[2] = 0;
                    b += 3;
                }
                if (!sky_atomic_cas(NULL,
                                    chunk,
                                    SKY_AS_VOIDP(&(sky_string_2bytes[slot]))))
                {
                    sky_free(chunk);
                }
            }
            return &(sky_string_2bytes[slot][offset]);
        }
#endif
        return NULL;
    }

    self_data = sky_string_data(self, NULL);
    if (self_data->highest_codepoint >= 0x100) {
        return NULL;
    }

    return self_data->data.ascii;
}


static sky_object_t
sky_string_encode(sky_string_t self, sky_string_t encoding, sky_string_t errors)
{
    sky_tuple_t tuple;

    tuple = sky_object_call(sky_codec_text_encoder(encoding),
                            sky_tuple_pack(2, self, errors),
                            NULL);

    return sky_tuple_get(tuple, 0);
}


sky_bool_t
sky_string_endswith(sky_string_t    self,
                    sky_object_t    suffix,
                    ssize_t         start,
                    ssize_t         end)
{
    sky_string_data_t   *self_data, self_tagged_data;
    
    self_data = sky_string_data_noctype(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_endswith_latin1(self_data, suffix, start, end);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_endswith_ucs2(self_data, suffix, start, end);
    }
    return sky_string_endswith_ucs4(self_data, suffix, start, end);
}


sky_object_t
sky_string_eq(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data(self, &self_tagged_data);
    other_data = sky_string_data(other, &other_tagged_data);

    if (self_data->len != other_data->len ||
        self_data->highest_codepoint != other_data->highest_codepoint ||
        self_data->any_ctype_flags != other_data->any_ctype_flags ||
        self_data->all_ctype_flags != other_data->all_ctype_flags)
    {
        return sky_False;
    }

    return (sky_string_data_compare(self_data, other_data) == 0 ? sky_True
                                                                : sky_False);
}


sky_string_t
sky_string_expandtabs(sky_string_t self, ssize_t tabsize)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, self_tagged_data;

    if (tabsize < 0) {
        sky_error_raise_string(sky_ValueError, "negative tab size");
    }

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    if (!self_data->len) {
        return sky_string_copy(self);
    }
    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_expandtabs_latin1(self, self_data, tabsize);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_expandtabs_ucs2(self, self_data, tabsize);
    }
    else {
        new_string = sky_string_expandtabs_ucs4(self, self_data, tabsize);
    }
    return sky_string_new_string(new_string, self);
}


ssize_t
sky_string_find(sky_string_t    self,
                sky_string_t    sub,
                ssize_t         start,
                ssize_t         end)
{
    sky_string_data_t   *self_data, self_tagged_data,
                        *sub_data, sub_tagged_data;
    
    self_data = sky_string_data(self, &self_tagged_data);
    sub_data = sky_string_data(sub, &sub_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_search_latin1(self_data, sub_data, start, end, 1, 0);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        return sky_string_search_ucs2(self_data, sub_data, start, end, 1, 0);
    }
    return sky_string_search_ucs4(self_data, sub_data, start, end, 1, 0);
}


sky_object_t
sky_string_ge(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    other_data = sky_string_data_noctype(other, &other_tagged_data);

    if (self_data->highest_codepoint < other_data->highest_codepoint) {
        return sky_False;
    }

    return (sky_string_data_compare(self_data, other_data) >= 0 ? sky_True
                                                                : sky_False);
}


sky_string_t
sky_string_getitem(sky_string_t self, sky_object_t item)
{
    ssize_t             length, start, step, stop;
    sky_string_t        new_string;
    sky_string_data_t   *self_data, self_tagged_data;

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        start = sky_integer_value(sky_number_index(item),
                                  SSIZE_MIN, SSIZE_MAX,
                                  sky_IndexError);
        if (start < 0) {
            start += self_data->len;
        }
        if (start < 0 || start >= self_data->len) {
            sky_error_raise_string(sky_IndexError,
                                   "string index out of range");
        }
        stop = start + 1;
        length = step = 1;
    }
    else if (sky_object_isa(item, sky_slice_type)) {
        length = sky_slice_range(item, self_data->len, &start, &stop, &step);
    }
    else {
        sky_error_raise_string(sky_TypeError,
                               "string indices must be integers");
    }

    if (length == self_data->len &&
        1 == step &&
        sky_string_type == sky_object_type(self))
    {
        return sky_string_copy(self);
    }

    new_string = sky_object_allocate(sky_string_type);
    sky_string_data_append_slice(sky_string_data(new_string, NULL),
                                 self_data,
                                 length,
                                 start,
                                 step);
    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_object_t
sky_string_getnewargs(sky_string_t self)
{
    return sky_object_build("(O)", sky_string_copy(self));
}


sky_object_t
sky_string_gt(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_False;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    other_data = sky_string_data_noctype(other, &other_tagged_data);

    return (sky_string_data_compare(self_data, other_data) > 0 ? sky_True
                                                               : sky_False);
}


uintptr_t
sky_string_hash(sky_string_t self)
{
    sky_string_data_t   *string_data, self_tagged_data;
    
    string_data = sky_string_data_noctype(self, &self_tagged_data);
    if (!(string_data->flags & SKY_STRING_FLAG_HASHED)) {
        size_t  width = sky_string_data_width(string_data);

        string_data->hash = sky_util_hashbytes(string_data->data.latin1,
                                               string_data->len * width,
                                               0);
        sky_atomic_or32_old(SKY_STRING_FLAG_HASHED, &(string_data->flags));
    }

    return string_data->hash;
}


ssize_t
sky_string_index(sky_string_t   self,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end)
{
    ssize_t             index;
    sky_string_data_t   *self_data, self_tagged_data,
                        *sub_data, sub_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    sub_data = sky_string_data(sub, &sub_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        index = sky_string_search_latin1(self_data, sub_data, start, end, 1, 0);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        index = sky_string_search_ucs2(self_data, sub_data, start, end, 1, 0);
    }
    else {
        index = sky_string_search_ucs4(self_data, sub_data, start, end, 1, 0);
    }

    if (index < 0) {
        sky_error_raise_string(sky_ValueError, "substring not found");
    }

    return index;
}


sky_string_t
sky_string_intern(sky_string_t self)
{
    return sky_string_cache_string(sky_string_cache(), self, SKY_TRUE);
}


sky_bool_t
sky_string_isalnum(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
        
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->len) {
        if (string_data->highest_codepoint < 0x100) {
            const uint8_t   *c, *end;

            end = string_data->data.latin1 + string_data->len;
            for (c = string_data->data.latin1; c < end; ++c) {
                if (!(sky_unicode_ctype_lookup_latin1(*c)->flags &
                            (SKY_UNICODE_CTYPE_FLAG_ALPHA |
                             SKY_UNICODE_CTYPE_FLAG_DECIMAL)))
                {
                    return SKY_FALSE;
                }
            }
        }
        else if (string_data->highest_codepoint < 0x10000) {
            const uint16_t  *c, *end;

            end = string_data->data.ucs2 + string_data->len;
            for (c = string_data->data.ucs2; c < end; ++c) {
                if (!sky_unicode_isalnum(*c)) {
                    return SKY_FALSE;
                }
            }
        }
        else {
            const sky_unicode_char_t    *c, *end;

            end = string_data->data.ucs4 + string_data->len;
            for (c = string_data->data.ucs4; c < end; ++c) {
                if (!sky_unicode_isalnum(*c)) {
                    return SKY_FALSE;
                }
            }
        }
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


sky_bool_t
sky_string_isalpha(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_ALPHA) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isascii(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data_noctype(self, &tagged_data);
    return (string_data->highest_codepoint <= 0x7F ? SKY_TRUE : SKY_FALSE);
}


sky_bool_t
sky_string_isdecimal(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_DECIMAL) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isdigit(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_DIGIT) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isidentifier(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);

    /* If there's no XID start, it's definitely NOT a valid identifier. */
    if (!(string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_XID_START)) {
        return SKY_FALSE;
    }

#define sky_string_isxid(F, T)                                      \
        do {                                                        \
            const T *c = string_data->data.F,                       \
                    *end = string_data->data.F + string_data->len;  \
                                                                    \
            if (!sky_unicode_isxid_start(*c)) {                     \
                return SKY_FALSE;                                   \
            }                                                       \
            while (++c < end) {                                     \
                if (!sky_unicode_isxid_continue(*c)) {              \
                    return SKY_FALSE;                               \
                }                                                   \
            }                                                       \
        } while (0)

    if (string_data->highest_codepoint < 0x100) {
        sky_string_isxid(latin1, uint8_t);
    }
    else if (string_data->highest_codepoint < 0x10000) {
        sky_string_isxid(ucs2, uint16_t);
    }
    else {
        sky_string_isxid(ucs4, sky_unicode_char_t);
    }

    return SKY_TRUE;
}


sky_bool_t
sky_string_islower(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if ((string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_LOWER) &&
        !(string_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_TITLE |
                                          SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isnumeric(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_NUMERIC) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isprintable(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (!string_data->len ||
        (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_PRINTABLE))
    {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_isspace(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if (string_data->all_ctype_flags & SKY_UNICODE_CTYPE_FLAG_SPACE) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_bool_t
sky_string_istitle(sky_string_t self)
{
    unsigned int        f;
    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data(self, &tagged_data);
    if (!string_data->len) {
        return SKY_FALSE;
    }
    if (1 == string_data->len) {
        if (string_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_TITLE |
                                            SKY_UNICODE_CTYPE_FLAG_UPPER))
        {
            return SKY_TRUE;
        }
        return SKY_FALSE;
    }

    /* If there are no cased characters at all, the result is SKY_FALSE. */
    if (!(string_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                          SKY_UNICODE_CTYPE_FLAG_TITLE |
                                          SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return SKY_FALSE;
    }
    /* If there are only lowercase characters, but no title/upper, the result
     * is SKY_FALSE.
     */
    if ((string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_LOWER) &&
        !(string_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_TITLE |
                                          SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return SKY_FALSE;
    }

    /* Scan the whole string. Lowercase characters may only follow title/upper
     * characters, and title/upper characters may not follow a cased character.
     */
#define sky_string_istitle_scan(F, T)                                   \
        do {                                                            \
            const T *c, *end = string_data->data.F + string_data->len;  \
                                                                        \
            for (c = string_data->data.F; c < end; ++c) {               \
                f = sky_unicode_ctype_lookup(*c)->flags;                \
                if (f & SKY_UNICODE_CTYPE_FLAG_LOWER) {                 \
                    return SKY_FALSE;                                   \
                }                                                       \
                if (!(f & (SKY_UNICODE_CTYPE_FLAG_TITLE |               \
                           SKY_UNICODE_CTYPE_FLAG_UPPER)))              \
                {                                                       \
                    continue;                                           \
                }                                                       \
                while (++c < end) {                                     \
                    f = sky_unicode_ctype_lookup(*c)->flags;            \
                    if (f & (SKY_UNICODE_CTYPE_FLAG_TITLE |             \
                             SKY_UNICODE_CTYPE_FLAG_UPPER))             \
                    {                                                   \
                        return SKY_FALSE;                               \
                    }                                                   \
                    if (!(f & SKY_UNICODE_CTYPE_FLAG_LOWER)) {          \
                        break;                                          \
                    }                                                   \
                }                                                       \
            }                                                           \
        } while (0)

    if (string_data->highest_codepoint < 0x100) {
        sky_string_istitle_scan(latin1, uint8_t);
    }
    else if (string_data->highest_codepoint < 0x10000) {
        sky_string_istitle_scan(ucs2, uint16_t);
    }
    else {
        sky_string_istitle_scan(ucs4, sky_unicode_char_t);
    }
    return SKY_TRUE;
}


sky_bool_t
sky_string_isupper(sky_string_t self)
{
    sky_string_data_t   *string_data, tagged_data;
    
    string_data = sky_string_data(self, &tagged_data);
    if ((string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_UPPER) &&
        !(string_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                          SKY_UNICODE_CTYPE_FLAG_TITLE)))
    {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_object_t
sky_string_iter(sky_string_t self)
{
    sky_string_iterator_t       iterator;
    sky_string_iterator_data_t  *iterator_data;

    iterator = sky_object_allocate(sky_string_iterator_type);
    iterator_data = sky_string_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->string)),
                      self,
                      iterator);

    return iterator;
}


sky_string_t
sky_string_join(sky_string_t self, sky_object_t iterable)
{
    sky_string_builder_t    builder;

    builder = sky_string_builder_create();
    sky_string_builder_appendjoined(builder, iterable, self);
    return sky_string_builder_finalize(builder);
}


sky_object_t
sky_string_le(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    other_data = sky_string_data_noctype(other, &other_tagged_data);

    if (self_data->highest_codepoint > other_data->highest_codepoint) {
        return sky_False;
    }

    return (sky_string_data_compare(self_data, other_data) <= 0 ? sky_True
                                                                : sky_False);
}


ssize_t
sky_string_len(sky_string_t self)
{
    if (sky_object_istagged(self)) {
        switch (SKY_OBJECT_TAG(self)) {
            case 3:
                return 1;
            case 4:
                return 2;
            case 7:
                if (sky_string_empty == self) {
                    return 0;
                }
                break;
        }
    }

    return sky_string_data(self, NULL)->len;
}


sky_string_t
sky_string_ljust(sky_string_t self, ssize_t width, sky_string_t fillchar)
{
    size_t              fillchar_width, new_string_width, self_width;
    ssize_t             fill;
    sky_string_t        new_string;
    sky_string_data_t   *fillchar_data, *new_string_data, *self_data,
                        tagged_fillchar_data, tagged_self_data;
    sky_unicode_char_t  fillchar_codepoint;

    self_data = sky_string_data(self, &tagged_self_data);
    fillchar_data = sky_string_data(fillchar, &tagged_fillchar_data);
    if (fillchar_data->len != 1) {
        sky_error_raise_string(
                sky_TypeError,
                "The fill character must be exactly one character long");
    }
    if (width <= self_data->len) {
        return sky_string_copy(self);
    }
    if (fillchar_data->highest_codepoint < 0x100) {
        fillchar_codepoint = fillchar_data->data.latin1[0];
        fillchar_width = 1;
    }
    else if (fillchar_data->highest_codepoint < 0x10000) {
        fillchar_codepoint = fillchar_data->data.ucs2[0];
        fillchar_width = 2;
    }
    else {
        fillchar_codepoint = fillchar_data->data.ucs4[0];
        fillchar_width = 4;
    }

    self_width = sky_string_data_width(self_data);
    new_string_width = SKY_MAX(fillchar_width, self_width);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, width, new_string_width);

    fill = width - self_data->len;
    sky_string_data_combine(new_string_data, self_data);
    sky_string_data_append_codepoint(new_string_data, fillchar_codepoint, fill);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_string_t
sky_string_lower(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    if (!(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_TITLE |
                                        SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return sky_string_copy(self);
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_lower_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_lower_ucs2(self_data);
    }
    else {
        new_string = sky_string_lower_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_lstrip(sky_string_t self, sky_object_t chars)
{
    sky_string_t        new_string;
    sky_string_data_t   *chars_data, chars_tagged_data,
                        *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    if (sky_object_isnull(chars)) {
        chars_data = NULL;
    }
    else {
        chars_data = sky_string_data(chars, &chars_tagged_data);
    }
    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_lstrip_latin1(self, self_data, chars_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_lstrip_ucs2(self, self_data, chars_data);
    }
    else {
        new_string = sky_string_lstrip_ucs4(self, self_data, chars_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_object_t
sky_string_lt(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_False;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    other_data = sky_string_data_noctype(other, &other_tagged_data);

    return (sky_string_data_compare(self_data, other_data) < 0 ? sky_True
                                                               : sky_False);
}


sky_dict_t
sky_string_maketrans(sky_object_t x, sky_object_t y, sky_string_t z)
{
    ssize_t         i, len;
    sky_dict_t      table;
    sky_object_t    key, value;

    table = sky_dict_create();

    if (sky_object_isnull(y)) {
        /* x must be a dictionary */
        if (sky_dict_type != sky_object_type(x)) {
            sky_error_raise_string(sky_TypeError,
                                   "the only argument must be a dict");
        }

        SKY_SEQUENCE_FOREACH(x, key) {
            if (sky_object_isa(key, sky_string_type)) {
                if (sky_object_len(key) != 1) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "string keys in translate table must be of length 1");
                }
                key = sky_integer_create(sky_string_charat(key, 0));
            }
            else if (!sky_object_isa(key, sky_integer_type)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "keys in translate table must be strings or integers");
            }
            value = sky_dict_getitem(x, key);
            sky_dict_setitem(table, key, value);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        /* x and y must be strings of equal length. */
        if (!sky_object_isa(x, sky_string_type) ||
            !sky_object_isa(y, sky_string_type) ||
            sky_object_len(x) != sky_object_len(y))
        {
            sky_error_raise_string(
                    sky_TypeError,
                    "the first two arguments must be strings of equal length");
        }

        len = sky_object_len(x);
        for (i = 0; i < len; ++i) {
            key = sky_integer_create(sky_string_charat(x, i));
            value = sky_integer_create(sky_string_charat(y, i));
            sky_dict_setitem(table, key, value);
        }

        if (!sky_object_isnull(z)) {
            /* z must be a string */
            len = sky_object_len(z);
            for (i = 0; i < len; ++i) {
                key = sky_integer_create(sky_string_charat(z, i));
                sky_dict_setitem(table, key, sky_None);
            }
        }
    }

    return table;
}


sky_string_t
sky_string_mul(sky_string_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "can't multiply str by non-int of type %#@",
                               sky_type_name(sky_object_type(other)));
    }
    other = sky_number_index(other);
    count = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);

    return sky_string_repeat(self, count);
}


sky_object_t
sky_string_ne(sky_string_t self, sky_string_t other)
{
    sky_string_data_t   *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (self == other) {
        return sky_False;
    }
    if (!sky_object_isa(self, sky_string_type) ||
        !sky_object_isa(other, sky_string_type))
    {
        return sky_NotImplemented;
    }

    self_data = sky_string_data(self, &self_tagged_data);
    other_data = sky_string_data(other, &other_tagged_data);

    if (self_data->len != other_data->len ||
        self_data->highest_codepoint != other_data->highest_codepoint ||
        self_data->any_ctype_flags != other_data->any_ctype_flags ||
        self_data->all_ctype_flags != other_data->all_ctype_flags)
    {
        return sky_True;
    }

    return (sky_string_data_compare(self_data, other_data) != 0 ? sky_True
                                                                : sky_False);
}


static sky_string_t
sky_string_new(sky_type_t   cls,
               sky_object_t object,
               sky_string_t encoding,
               sky_string_t errors)
{
    sky_string_t    string;

    if (sky_string_type != cls) {
        string = sky_string_new(sky_string_type, object, encoding, errors);
        return sky_string_clone(string, cls);
    }

    if (!object) {
        return sky_string_empty;
    }
    if (!encoding && !errors) {
        return sky_object_str(object);
    }
    return sky_codec_decode(NULL, encoding, errors, NULL, object);
}


sky_string_t
sky_string_normalizewithdata(sky_string_t                       self,
                             sky_string_normalize_form_t        form,
                             const sky_unicode_decomp_data_t *  data)
{
    sky_bool_t          compat;
    sky_string_t        string;
    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data_noctype(self, &tagged_data);
    switch (form) {
        case SKY_STRING_NORMALIZE_FORM_NFC:
            /* Latin-1 is already normalized in NFC. */
            if (string_data->highest_codepoint < 0x100) {
                return sky_string_copy(self);
            }
            compat = SKY_FALSE;
            break;
        case SKY_STRING_NORMALIZE_FORM_NFD:
            /* ASCII is already normalized in all forms. */
            if (string_data->highest_codepoint < 0x80) {
                return sky_string_copy(self);
            }
            compat = SKY_FALSE;
            break;
        case SKY_STRING_NORMALIZE_FORM_NFKC:
        case SKY_STRING_NORMALIZE_FORM_NFKD:
            /* ASCII is already normalized in all forms. */
            if (string_data->highest_codepoint < 0x80) {
                return sky_string_copy(self);
            }
            compat = SKY_TRUE;
            break;
        default:
            sky_error_raise_string(sky_ValueError,
                                   "unsupported normalization form");
    }

    if (string_data->highest_codepoint < 0x100) {
        string = sky_string_decompose_latin1(self, string_data, compat, data);
    }
    else if (string_data->highest_codepoint < 0x10000) {
        string = sky_string_decompose_ucs2(self, string_data, compat, data);
    }
    else {
        string = sky_string_decompose_ucs4(self, string_data, compat, data);
    }

    if (SKY_STRING_NORMALIZE_FORM_NFC == form ||
        SKY_STRING_NORMALIZE_FORM_NFKC == form)
    {
        string_data = sky_string_data_noctype(string, &tagged_data);
        if (string_data->highest_codepoint < 0x100) {
            string = sky_string_compose_latin1(string, string_data, compat, data);
        }
        else if (string_data->highest_codepoint < 0x10000) {
            string = sky_string_compose_ucs2(string, string_data, compat, data);
        }
        else {
            string = sky_string_compose_ucs4(string, string_data, compat, data);
        }
    }

    return sky_string_new_string(string, self);
}


sky_string_t
sky_string_normalize(sky_string_t self, sky_string_normalize_form_t form)
{
    return sky_string_normalizewithdata(self, form, sky_unicode_decomp_tables);
}


sky_tuple_t
sky_string_partition(sky_string_t self, sky_string_t sep)
{
    ssize_t             index;
    sky_string_t        after, before;
    sky_string_data_t   *self_data, self_tagged_data,
                        *sep_data, sep_tagged_data;

    if ((index = sky_string_find(self, sep, 0, SSIZE_MAX)) < 0) {
        before = sky_string_copy(self);
        after = sep = sky_string_empty;
    }
    else {
        self_data = sky_string_data_noctype(self, &self_tagged_data);
        sep_data = sky_string_data_noctype(sep, &sep_tagged_data);

        before = sky_string_slice(self, 0, index, 1);
        after = sky_string_slice(self,
                                 index + sep_data->len,
                                 self_data->len,
                                 1);
    }

    return sky_tuple_pack(3, before, sep, after);
}


sky_string_t
sky_string_radd(sky_string_t self, sky_string_t other)
{
    size_t              other_width, self_width;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data, *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    if (!sky_object_isa(self, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "Can't convert %#@ object to str implicitly",
                               sky_type_name(sky_object_type(self)));
    }
    if (!sky_object_isa(other, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "Can't convert %#@ object to str implicitly",
                               sky_type_name(sky_object_type(other)));
    }

    if (sky_string_empty == self) {
        return sky_string_copy(other);
    }
    if (sky_string_empty == other) {
        return sky_string_copy(self);
    }

    self_data = sky_string_data(self, &self_tagged_data);
    self_width = sky_string_data_width(self_data);
    other_data = sky_string_data(other, &other_tagged_data);
    other_width = sky_string_data_width(other_data);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data,
                           self_data->len + other_data->len,
                           SKY_MAX(self_width, other_width));
    sky_string_data_combine(new_string_data, other_data);
    sky_string_data_combine(new_string_data, self_data);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_string_t
sky_string_repeat(sky_string_t self, ssize_t count)
{
    size_t              width;
    ssize_t             i;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data, *string_data, tagged_data;

    if (count <= 0) {
        return sky_string_empty;
    }
    if (1 == count) {
        return sky_string_copy(self);
    }
    string_data = sky_string_data(self, &tagged_data);
    width = sky_string_data_width(string_data);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, (string_data->len * count), width);
    for (i = 0; i < count; ++i) {
        sky_string_data_combine(new_string_data, string_data);
    }

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_string_t
sky_string_replace(sky_string_t self,
                   sky_string_t old_string,
                   sky_string_t new_string,
                   ssize_t      count)
{
    sky_string_t        result;
    sky_string_data_t   *new_string_data, new_tagged_data,
                        *old_string_data, old_tagged_data,
                        *self_data, self_tagged_data;

    /* NOTE old_string == new_string is considered a pathological case, and so
     * is more expensive to test for than it is worth, except for a simple
     * pointer check.
     */
    if (!count || sky_string_empty == self || old_string == new_string) {
        return sky_string_copy(self);
    }
    if (count < 0) {
        count = SSIZE_MAX;
    }

    self_data = sky_string_data(self, &self_tagged_data);
    old_string_data = sky_string_data(old_string, &old_tagged_data);
    new_string_data = sky_string_data(new_string, &new_tagged_data);

    if (self_data->highest_codepoint < 0x100) {
        result = sky_string_replace_latin1(self,
                                           self_data,
                                           old_string_data,
                                           new_string_data,
                                           count);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        result = sky_string_replace_ucs2(self,
                                         self_data,
                                         old_string_data,
                                         new_string_data,
                                         count);
    }
    else {
        result = sky_string_replace_ucs4(self,
                                         self_data,
                                         old_string_data,
                                         new_string_data,
                                         count);
    }

    return sky_string_new_string(result, self);
}


sky_string_t
sky_string_repr(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data_noctype(self, &tagged_data);
    if (string_data->highest_codepoint < 0x100) {
        new_string = sky_string_repr_latin1(string_data);
    }
    else if (string_data->highest_codepoint < 0x10000) {
        new_string = sky_string_repr_ucs2(string_data);
    }
    else {
        new_string = sky_string_repr_ucs4(string_data);
    }
    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


ssize_t
sky_string_rfind(sky_string_t   self,
                 sky_string_t   sub,
                 ssize_t        start,
                 ssize_t        end)
{
    sky_string_data_t   *self_data, self_tagged_data,
                        *sub_data, sub_tagged_data;
    
    self_data = sky_string_data(self, &self_tagged_data);
    sub_data = sky_string_data(sub, &sub_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_search_latin1(self_data,
                                        sub_data,
                                        start,
                                        end,
                                        -1,
                                        0);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        return sky_string_search_ucs2(self_data,
                                      sub_data,
                                      start,
                                      end,
                                      -1,
                                      0);
    }
    return sky_string_search_ucs4(self_data,
                                  sub_data,
                                  start,
                                  end,
                                  -1,
                                  0);
}


ssize_t
sky_string_rindex(sky_string_t  self,
                  sky_string_t  sub,
                  ssize_t       start,
                  ssize_t       end)
{
    ssize_t             index;
    sky_string_data_t   *self_data, self_tagged_data,
                        *sub_data, sub_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    sub_data = sky_string_data(sub, &sub_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        index = sky_string_search_latin1(self_data,
                                         sub_data,
                                         start,
                                         end,
                                         -1,
                                         0);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        index = sky_string_search_ucs2(self_data,
                                       sub_data,
                                       start,
                                       end,
                                       -1,
                                       0);
    }
    else {
        index = sky_string_search_ucs4(self_data,
                                       sub_data,
                                       start,
                                       end,
                                       -1,
                                       0);
    }

    if (index < 0) {
        sky_error_raise_string(sky_ValueError, "substring not found");
    }

    return index;
}


sky_string_t
sky_string_rjust(sky_string_t self, ssize_t width, sky_string_t fillchar)
{
    size_t              fillchar_width, new_string_width, self_width;
    ssize_t             fill;
    sky_string_t        new_string;
    sky_string_data_t   *fillchar_data, *new_string_data, *self_data,
                        tagged_fillchar_data, tagged_self_data;
    sky_unicode_char_t  fillchar_codepoint;

    self_data = sky_string_data(self, &tagged_self_data);
    fillchar_data = sky_string_data(fillchar, &tagged_fillchar_data);
    if (fillchar_data->len != 1) {
        sky_error_raise_string(
                sky_TypeError,
                "The fill character must be exactly one character long");
    }
    if (width <= self_data->len) {
        return sky_string_copy(self);
    }
    if (fillchar_data->highest_codepoint < 0x100) {
        fillchar_codepoint = fillchar_data->data.latin1[0];
        fillchar_width = 1;
    }
    else if (fillchar_data->highest_codepoint < 0x10000) {
        fillchar_codepoint = fillchar_data->data.ucs2[0];
        fillchar_width = 2;
    }
    else {
        fillchar_codepoint = fillchar_data->data.ucs4[0];
        fillchar_width = 4;
    }

    self_width = sky_string_data_width(self_data);
    new_string_width = SKY_MAX(fillchar_width, self_width);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, width, new_string_width);

    fill = width - self_data->len;
    sky_string_data_append_codepoint(new_string_data, fillchar_codepoint, fill);
    sky_string_data_combine(new_string_data, self_data);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


static sky_object_t
sky_string_rmod(sky_string_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_string_type)) {
        return sky_NotImplemented;
    }
    return sky_string_printf(other, self);
}


sky_tuple_t
sky_string_rpartition(sky_string_t self, sky_string_t sep)
{
    ssize_t             index;
    sky_string_t        after, before;
    sky_string_data_t   *self_data, self_tagged_data,
                        *sep_data, sep_tagged_data;

    if ((index = sky_string_rfind(self, sep, 0, SSIZE_MAX)) < 0) {
        after = sky_string_copy(self);
        before = sep = sky_string_empty;
    }
    else {
        self_data = sky_string_data_noctype(self, &self_tagged_data);
        sep_data = sky_string_data_noctype(sep, &sep_tagged_data);

        before = sky_string_slice(self, 0, index, 1);
        after = sky_string_slice(self,
                                 index + sep_data->len,
                                 self_data->len,
                                 1);
    }

    return sky_tuple_pack(3, before, sep, after);
}


sky_list_t
sky_string_rsplit(sky_string_t self, sky_string_t sep, ssize_t maxsplit)
{
    sky_string_data_t   *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_rsplit_latin1(self, self_data, sep, maxsplit);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_rsplit_ucs2(self, self_data, sep, maxsplit);
    }
    return sky_string_rsplit_ucs4(self, self_data, sep, maxsplit);
}


sky_string_t
sky_string_rstrip(sky_string_t self, sky_object_t chars)
{
    sky_string_t        new_string;
    sky_string_data_t   *chars_data, chars_tagged_data,
                        *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    if (sky_object_isnull(chars)) {
        chars_data = NULL;
    }
    else {
        chars_data = sky_string_data(chars, &chars_tagged_data);
    }
    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_rstrip_latin1(self, self_data, chars_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_rstrip_ucs2(self, self_data, chars_data);
    }
    else {
        new_string = sky_string_rstrip_ucs4(self, self_data, chars_data);
    }
    return sky_string_new_string(new_string, self);
}


size_t
sky_string_sizeof(sky_string_t self)
{
    sky_string_data_t   *self_data;

    if (sky_object_istagged(self)) {
        return 0;
    }

    self_data = sky_string_data(self, NULL);
    if (self_data->data.ascii == self_data->u.small_data) {
        return sky_memsize(self);
    }
    return sky_memsize(self) + sky_memsize(self_data->data.ascii);
}


sky_string_t
sky_string_slice(sky_string_t self, ssize_t start, ssize_t stop, ssize_t step)
{
    ssize_t             length;
    sky_string_t        new_string;
    sky_string_data_t   *self_data, self_tagged_data;

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    length = sky_sequence_indices(self_data->len, &start, &stop, &step);
    if (length == self_data->len && sky_string_type == sky_object_type(self)) {
        return sky_string_copy(self);
    }

    new_string = sky_object_allocate(sky_string_type);
    sky_string_data_append_slice(sky_string_data(new_string, NULL),
                                 self_data,
                                 length,
                                 start,
                                 step);

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


sky_list_t
sky_string_split(sky_string_t self, sky_string_t sep, ssize_t maxsplit)
{
    sky_string_data_t   *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_split_latin1(self, self_data, sep, maxsplit);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_split_ucs2(self, self_data, sep, maxsplit);
    }
    return sky_string_split_ucs4(self, self_data, sep, maxsplit);
}


sky_list_t
sky_string_splitlines(sky_string_t self, sky_bool_t keepends)
{
    sky_string_data_t   *self_data, self_tagged_data;

    self_data = sky_string_data_noctype(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_splitlines_latin1(self, self_data, keepends);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_splitlines_ucs2(self, self_data, keepends);
    }
    return sky_string_splitlines_ucs4(self, self_data, keepends);
}


sky_bool_t
sky_string_startswith(sky_string_t    self,
                      sky_object_t    prefix,
                      ssize_t         start,
                      ssize_t         end)
{
    sky_string_data_t   *self_data, self_tagged_data;
    
    self_data = sky_string_data_noctype(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_startswith_latin1(self_data, prefix, start, end);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_startswith_ucs2(self_data, prefix, start, end);
    }
    return sky_string_startswith_ucs4(self_data, prefix, start, end);
}


sky_string_t
sky_string_str(sky_string_t self)
{
    return sky_string_copy(self);
}


sky_string_t
sky_string_strip(sky_string_t self, sky_object_t chars)
{
    sky_string_t        new_string;
    sky_string_data_t   *chars_data, chars_tagged_data,
                        *self_data, self_tagged_data;

    self_data = sky_string_data(self, &self_tagged_data);
    if (sky_object_isnull(chars)) {
        chars_data = NULL;
    }
    else {
        chars_data = sky_string_data(chars, &chars_tagged_data);
    }
    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_strip_latin1(self, self_data, chars_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_strip_ucs2(self, self_data, chars_data);
    }
    else {
        new_string = sky_string_strip_ucs4(self, self_data, chars_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_swapcase(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    if (!(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                        SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return sky_string_copy(self);
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_swapcase_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_swapcase_ucs2(self_data);
    }
    else {
        new_string = sky_string_swapcase_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_title(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    if (!(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                        SKY_UNICODE_CTYPE_FLAG_TITLE |
                                        SKY_UNICODE_CTYPE_FLAG_UPPER)))
    {
        return sky_string_copy(self);
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_title_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_title_ucs2(self_data);
    }
    else {
        new_string = sky_string_title_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_transform_numeric(sky_string_t self)
{
    sky_string_data_t       *self_data, tagged_self_data;
    sky_string_builder_t    builder;

    self_data = sky_string_data(self, &tagged_self_data);
    if (self_data->highest_codepoint < 0x80) {
        return sky_string_copy(self);
    }
    if (self_data->highest_codepoint < 0x100) {
        return NULL;
    }

    builder = sky_string_builder_createwithcapacity(self_data->len);
    if (self_data->highest_codepoint < 0x10000) {
        int64_t         value;
        const uint16_t  *c, *end, *start;

        start = self_data->data.ucs2;
        end = self_data->data.ucs2 + self_data->len;
        for (c = start; c < end; ++c) {
            if (*c < 0x80) {
                sky_string_builder_appendcodepoint(builder, *c, 1);
            }
            else if (sky_unicode_isspace(*c)) {
                sky_string_builder_appendcodepoint(builder, ' ', 1);
            }
            else if (sky_unicode_isintegral(*c) &&
                     (value = sky_unicode_integer(*c)) < 10)
            {
                sky_string_builder_appendcodepoint(builder, '0' + value, 1);
            }
            else {
                return NULL;    /* cannot transform */
            }
        }
    }
    else {
        int64_t                     value;
        const sky_unicode_char_t    *c, *end, *start;

        start = self_data->data.ucs4;
        end = self_data->data.ucs4 + self_data->len;
        for (c = start; c < end; ++c) {
            if (*c < 0x80) {
                sky_string_builder_appendcodepoint(builder, *c, 1);
            }
            else if (sky_unicode_isspace(*c)) {
                sky_string_builder_appendcodepoint(builder, ' ', 1);
            }
            else if (sky_unicode_isintegral(*c) &&
                     (value = sky_unicode_integer(*c)) < 10)
            {
                sky_string_builder_appendcodepoint(builder, '0' + value, 1);
            }
            else {
                return NULL;    /* cannot transform */
            }
        }
    }

    return sky_string_builder_finalize(builder);
}


sky_string_t
sky_string_translate(sky_string_t self, sky_dict_t table)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, self_tagged_data;

    if (!sky_object_bool(table)) {
        return sky_string_copy(self);
    }

    self_data = sky_string_data(self, &self_tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_translate_latin1(self, self_data, table);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_translate_ucs2(self, self_data, table);
    }
    else {
        new_string = sky_string_translate_ucs4(self, self_data, table);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_upper(sky_string_t self)
{
    sky_string_t        new_string;
    sky_string_data_t   *self_data, tagged_self_data;

    self_data = sky_string_data(self, &tagged_self_data);
    if (!(self_data->any_ctype_flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                                        SKY_UNICODE_CTYPE_FLAG_TITLE)))
    {
        return sky_string_copy(self);
    }

    if (self_data->highest_codepoint < 0x100) {
        new_string = sky_string_upper_latin1(self_data);
    }
    else if (self_data->highest_codepoint < 0x10000) {
        new_string = sky_string_upper_ucs2(self_data);
    }
    else {
        new_string = sky_string_upper_ucs4(self_data);
    }
    return sky_string_new_string(new_string, self);
}


sky_string_t
sky_string_zfill(sky_string_t self, ssize_t width)
{
    size_t              self_width;
    ssize_t             fill, i;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data, *self_data, tagged_self_data;
    sky_unicode_char_t  cp;

    self_data = sky_string_data(self, &tagged_self_data);
    if (width <= self_data->len) {
        return sky_string_copy(self);
    }

    fill = width - self_data->len;
    self_width = sky_string_data_width(self_data);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, width, self_width);

    new_string_data->len = fill;
    sky_string_data_update_1(new_string_data, '0');
    if (1 == self_width) {
        memset(new_string_data->data.latin1, '0', fill);
    }
    else if (2 == self_width) {
        for (i = 0; i < fill; new_string_data->data.ucs2[i++] = '0');
    }
    else {
        for (i = 0; i < fill; new_string_data->data.ucs4[i++] = '0');
    }
    sky_string_data_combine(new_string_data, self_data);

    if (1 == self_width) {
        cp = self_data->data.latin1[0];
        if ('+' == cp || '-' == cp) {
            new_string_data->data.latin1[0] = cp;
            new_string_data->data.latin1[fill] = '0';
        }
    }
    else if (2 == self_width) {
        cp = self_data->data.ucs2[0];
        if ('+' == cp || '-' == cp) {
            new_string_data->data.ucs2[0] = cp;
            new_string_data->data.ucs2[fill] = '0';
        }
    }
    else {
        cp = self_data->data.ucs4[0];
        if ('+' == cp || '-' == cp) {
            new_string_data->data.ucs4[0] = cp;
            new_string_data->data.ucs4[fill] = '0';
        }
    }

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


void
sky_string_iterator_init(sky_object_t self, sky_string_t string)
{
    sky_string_iterator_data_t  *iterator_data = sky_string_iterator_data(self);

    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->string)), string, self);
}


sky_object_t
sky_string_iterator_iter(sky_object_t self)
{
    return self;
}


ssize_t
sky_string_iterator_len(sky_object_t self)
{
    sky_string_iterator_data_t  *iterator_data = sky_string_iterator_data(self);

    return sky_object_len(iterator_data->string) - iterator_data->next_offset;
}


sky_object_t
sky_string_iterator_next(sky_object_t self)
{
    sky_string_iterator_data_t  *iterator_data = sky_string_iterator_data(self);

    sky_string_data_t   *string_data, tagged_data;
    sky_unicode_char_t  cp;

    string_data = sky_string_data(iterator_data->string, &tagged_data);
    if (iterator_data->next_offset >= string_data->len) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    if (string_data->highest_codepoint < 0x100) {
        cp = (sky_unicode_char_t)
             string_data->data.latin1[iterator_data->next_offset];
    }
    else if (string_data->highest_codepoint < 0x10000) {
        cp = (sky_unicode_char_t)
             string_data->data.ucs2[iterator_data->next_offset];
    }
    else {
        cp = string_data->data.ucs4[iterator_data->next_offset];
    }
    ++iterator_data->next_offset;

    return SKY_OBJECT_MAKETAG(0x3, cp);
}


sky_object_t
sky_string_iterator_reduce(sky_object_t self)
{
    sky_string_iterator_data_t  *iterator_data = sky_string_iterator_data(self);

    if (iterator_data->string) {
        return sky_object_build("(O(O)iz)",
                                sky_module_getbuiltin("iter"),
                                iterator_data->string,
                                iterator_data->next_offset);
    }
    return sky_object_build("(O(O))",
                            sky_module_getbuiltin("iter"),
                            sky_string_empty);
}


void
sky_string_iterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_string_iterator_data_t  *self_data = sky_string_iterator_data(self);

    if (self_data->string) {
        self_data->next_offset = sky_integer_value(sky_number_index(state),
                                                       SSIZE_MIN, SSIZE_MAX,
                                                       NULL);
        if (self_data->next_offset < 0) {
            self_data->next_offset = 0;
        }
        else if (self_data->next_offset > sky_object_len(self_data->string)) {
            self_data->next_offset = sky_object_len(self_data->string);
        }
    }
}


static const char sky_string_type_doc[] =
"str(string[, encoding[, errors]]) -> str\n"
"\n"
"Create a new string object from the given encoded string.\n"
"encoding defaults to the current default string encoding.\n"
"errors can be 'strict', 'replace', 'ignore' and defaults to 'strict'.";


SKY_TYPE_DEFINE_SIMPLE(string,
                       "str",
                       sizeof(sky_string_data_t),
                       NULL,
                       sky_string_instance_finalize,
                       NULL,
                       0,
                       sky_string_type_doc);


SKY_TYPE_DEFINE_SIMPLE(string_iterator,
                       "str_iterator",
                       sizeof(sky_string_iterator_data_t),
                       NULL,
                       NULL,
                       sky_string_iterator_instance_visit,
                       0,
                       NULL);


sky_string_t const sky_string_empty = SKY_OBJECT_TAGGED_STRING_EMPTY;


void
sky_string_initialize_library(void)
{
    sky_integer_t   ssize_max;

    sky_string_cache_initialize_library();
    ssize_max = sky_integer_create(SSIZE_MAX);

    sky_type_setattr_builtin(
            sky_string_type,
            "capitalize",
            sky_function_createbuiltin(
                    "str.capitalize",
                    "S.capitalize() -> str\n\n"
                    "Return a capitalized version of S, i.e. make the first character\n"
                    "have upper case and the rest lower case.",
                    (sky_native_code_function_t)sky_string_capitalize,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "casefold",
            sky_function_createbuiltin(
                    "str.casefold",
                    "S.casefold() -> str\n\n"
                    "Return a version of S suitable for caseless comparisons.",
                    (sky_native_code_function_t)sky_string_casefold,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "center",
            sky_function_createbuiltin(
                    "str.center",
                    "S.center(width[, fillchar]) -> str\n\n"
                    "Return S centered in a string of length width. Padding is\n"
                    "done using the specified fill character (default is a space)",
                    (sky_native_code_function_t)sky_string_center,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "width", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT_STRING, sky_string_createfromcodepoint(' '),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "count",
            sky_function_createbuiltin(
                    "str.count",
                    "S.count(sub[, start[, end]]) -> int\n\n"
                    "Return the number of non-overlapping occurrences of substring sub in\n"
                    "string S[start:end].  Optional arguments start and end are\n"
                    "interpreted as in slice notation.",
                    (sky_native_code_function_t)sky_string_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    "maxcount", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "encode",
            sky_function_createbuiltin(
                    "str.encode",
                    "S.encode(encoding='utf-8', errors='strict') -> bytes\n\n"
                    "Encode S using the codec registered for encoding. Default encoding\n"
                    "is 'utf-8'. errors may be given to set a different error\n"
                    "handling scheme. Default is 'strict' meaning that encoding errors raise\n"
                    "a UnicodeEncodeError. Other possible values are 'ignore', 'replace' and\n"
                    "'xmlcharrefreplace' as well as any other name registered with\n"
                    "codecs.register_error that can handle UnicodeEncodeErrors.",
                    (sky_native_code_function_t)sky_string_encode,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("utf-8"),
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("strict"),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "endswith",
            sky_function_createbuiltin(
                    "str.endswith",
                    "S.endswith(suffix[, start[, end]]) -> bool\n\n"
                    "Return True if S starts with the specified suffix, False otherwise.\n"
                    "With optional start, test S beginning at that position.\n"
                    "With optional end, stop comparing S at that position.\n"
                    "suffix can also be a tuple of strings to try.",
                    (sky_native_code_function_t)sky_string_endswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "suffix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "expandtabs",
            sky_function_createbuiltin(
                    "str.expandtabs",
                    "S.expandtabs([tabsize]) -> str\n\n"
                    "Return a copy of S where all tab characters are expanded using spaces.\n"
                    "If tabsize is not given, a tab size of 8 characters is assumed.",
                    (sky_native_code_function_t)sky_string_expandtabs,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "tabsize", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(8),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "find",
            sky_function_createbuiltin(
                    "str.find",
                    "S.find(sub[, start[, end]]) -> int\n\n"
                    "Return the lowest index in S where substring sub is found,\n"
                    "such that sub is contained within S[start:end]. Optional\n"
                    "arguments start and end are interpreted as in slice notation.\n\n"
                    "Return -1 on failure.",
                    (sky_native_code_function_t)sky_string_find,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "format",
            sky_function_createbuiltin(
                    "str.format",
                    "S.format(*args, **kwargs) -> str\n\n"
                    "Return a formatted version of S, using substitutions from args and kwargs.\n"
                    "The substitutions are identified by braces ('{' and '}').",
                    (sky_native_code_function_t)sky_string_format,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "format_map",
            sky_function_createbuiltin(
                    "str.format_map",
                    "S.format_map(mapping) -> str\n\n"
                    "Return a formatted version of S, using substitutions from mapping.\n"
                    "The substitutions are identified by braces ('{' and '}').",
                    (sky_native_code_function_t)sky_string_format_map,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "map", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setattr_builtin(
            sky_string_type,
            "index",
            sky_function_createbuiltin(
                    "str.index",
                    "S.index(sub[, start[, end]]) -> int\n\n"
                    "Like S.find() but raise Valueerror when the substring is not found.",
                    (sky_native_code_function_t)sky_string_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isalnum",
            sky_function_createbuiltin(
                    "str.isalnum",
                    "S.isalnum() -> bool\n\n"
                    "Return True if all characters in S are alphanumeric"
                    "and there is at least one character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_isalnum,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isalpha",
            sky_function_createbuiltin(
                    "str.isalpha",
                    "S.isalpha() -> bool\n\n"
                    "Return True if all characters in S are alphabetic\n"
                    "and there is at least one character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_isalpha,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isdecimal",
            sky_function_createbuiltin(
                    "str.isdecimal",
                    "S.isdecimal() -> bool\n\n"
                    "Return True if there are only decimal characters in S,\n"
                    "False otherwise",
                    (sky_native_code_function_t)sky_string_isdecimal,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isdigit",
            sky_function_createbuiltin(
                    "str.isdigit",
                    "S.isdigit() -> bool\n\n"
                    "Return True if all characters in S are digits\n"
                    "and there is at least one character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_isdigit,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isidentifier",
            sky_function_createbuiltin(
                    "str.isidentifier",
                    "S.isidentifier() -> bool\n\n"
                    "Return True if S is a valid identifier according\n"
                    "to the language definition",
                    (sky_native_code_function_t)sky_string_isidentifier,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "islower",
            sky_function_createbuiltin(
                    "str.islower",
                    "S.islower() -> bool\n\n"
                    "Return True if all cased characters in S are lowercase and there is\n"
                    "at least one cased character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_islower,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isnumeric",
            sky_function_createbuiltin(
                    "str.isnumeric",
                    "S.isnumeric() -> bool\n\n"
                    "Return True if there are only numeric characters in S,\n"
                    "False otherwise.",
                    (sky_native_code_function_t)sky_string_isnumeric,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isprintable",
            sky_function_createbuiltin(
                    "str.isprintable",
                    "S.isprintable() -> bool\n\n"
                    "Return True if all characters in S are considered\n"
                    "printable in repr() or S is empty, False otherwise.",
                    (sky_native_code_function_t)sky_string_isprintable,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isspace",
            sky_function_createbuiltin(
                    "str.isspace",
                    "S.isspace() -> bool\n\n"
                    "Return True if all characters in S are whitespace\n"
                    "and there is at least one character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_isspace,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "istitle",
            sky_function_createbuiltin(
                    "str.istitle",
                    "S.istitle() -> bool\n\n"
                    "Return True if S is a titlecased string and there is at least one\n"
                    "character in S, i.e. upper- and titlecase characters may only\n"
                    "follow uncased characters and lowercase characters only cased ones.\n"
                    "Return False otherwise.",
                    (sky_native_code_function_t)sky_string_istitle,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "isupper",
            sky_function_createbuiltin(
                    "str.isupper",
                    "S.isupper() -> bool\n\n"
                    "Return True if all cased characters in S are uppercase and there is\n"
                    "at least one cased character in S, False otherwise.",
                    (sky_native_code_function_t)sky_string_isupper,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "join",
            sky_function_createbuiltin(
                    "str.join",
                    "S.join(iterable) -> str\n\n"
                    "Return a string which is the concatenation of the strings in the\n"
                    "iterable. The separator between elements is S.",
                    (sky_native_code_function_t)sky_string_join,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "ljust",
            sky_function_createbuiltin(
                    "str.ljust",
                    "S.ljust(width[, fillchar]) -> str\n\n"
                    "Return S left-justified in a Unicode string of length width. Padding is\n"
                    "done using the specified fill character (default is a space).",
                    (sky_native_code_function_t)sky_string_ljust,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "width", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT_STRING, sky_string_createfromcodepoint(' '),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "lower",
            sky_function_createbuiltin(
                    "str.lower",
                    "S.lower() -> str\n\n"
                    "Return a copy of the string S converted to lowercase.",
                    (sky_native_code_function_t)sky_string_lower,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "lstrip",
            sky_function_createbuiltin(
                    "str.lstrip",
                    "S.lstrip([chars]) -> str\n\n"
                    "Return a copy of the string S with leading whitespace removed.\n"
                    "If chars is given and not None, remove characters in chars instead.",
                    (sky_native_code_function_t)sky_string_lstrip,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "chars", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "maketrans",
            sky_staticmethod_create(
                    sky_function_createbuiltin(
                            "str.maketrans",
                            "str.maketrans(x[, y[, z]]) -> dict (static method)\n\n"
                            "Return a translation table usable for str.translate().\n"
                            "If there is only one argument, it must be a dictionary mapping Unicode\n"
                            "ordinals (integers) or characters to Unicode ordinals, strings, or None.\n"
                            "Character keys will be then converted to ordinals.\n"
                            "If there are two arguments, they must be strings of equal length, and\n"
                            "in the resulting dictionary, each character in x will be mapped to the\n"
                            "character at the same position in y. If there is a third argument, it\n"
                            "must be a string, whose characters will be mapped to None in the result.",
                            (sky_native_code_function_t)sky_string_maketrans,
                            SKY_DATA_TYPE_OBJECT_DICT,
                            "x", SKY_DATA_TYPE_OBJECT, NULL,
                            "y", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                            "z", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
                            NULL)));
    sky_type_setattr_builtin(
            sky_string_type,
            "partition",
            sky_function_createbuiltin(
                    "str.partition",
                    "S.partition(sep) -> (head, sep, tail)\n\n"
                    "Search for the seaparator sep in S, and return the part before it,\n"
                    "the separator itself, and the part after it. If the separator is not\n"
                    "found, return S and two empty strings.",
                    (sky_native_code_function_t)sky_string_partition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "replace",
            sky_function_createbuiltin(
                    "str.replace",
                    "S.replace(old, new[, count]) -> str\n\n"
                    "Return a copy of S with all occurrences of substring\n"
                    "old replaced by new.  If the optional argument count is\n"
                    "given, only the first count occurrences are replaced.\n",
                    (sky_native_code_function_t)sky_string_replace,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "old", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "new", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "count", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(SSIZE_MAX),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rfind",
            sky_function_createbuiltin(
                    "str.rfind",
                    "S.rfind(sub[, start[, end]]) -> int\n\n"
                    "Return the highest index in S where substring sub is found,\n"
                    "such that sub is contained within S[start:end]. Optional\n"
                    "arguments start and end are interpreted as in slice notation.\n\n"
                    "Return -1 on failure.",
                    (sky_native_code_function_t)sky_string_rfind,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rindex",
            sky_function_createbuiltin(
                    "str.rindex",
                    "S.rindex(sub[, start[, end]]) -> int\n\n"
                    "Like S.rfind() but raise Valueerror when the substring is not found.",
                    (sky_native_code_function_t)sky_string_rindex,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rjust",
            sky_function_createbuiltin(
                    "str.rjust",
                    "S.rjust(width[, fillchar]) -> str\n\n"
                    "Return S right-justified in a Unicode string of length width. Padding is\n"
                    "done using the specified fill character (default is a space).",
                    (sky_native_code_function_t)sky_string_rjust,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "width", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT_STRING, sky_string_createfromcodepoint(' '),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rpartition",
            sky_function_createbuiltin(
                    "str.rpartition",
                    "S.rpartition(sep) -> (head, sep, tail)\n\n"
                    "Search for the seaparator sep in S, starting at the end of S, and return\n"
                    "the part before it, the separator itself, and the part after it. If the\n"
                    "separator is not found, return two empty strings and S.",
                    (sky_native_code_function_t)sky_string_rpartition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rsplit",
            sky_function_createbuiltin(
                    "str.rsplit",
                    "S.rsplit(sep=None, maxsplit=-1) -> list of strings\n\n"
                    "Return a list of the words in S, using sep as the\n"
                    "delimiter string, starting at the end of the string and\n"
                    "working to the front. If maxsplit is given, at most maxsplit\n"
                    "splits are done. If sep is not specified, any whitespace string\n"
                    "is a separator.",
                    (sky_native_code_function_t)sky_string_rsplit,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    "maxsplit", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "rstrip",
            sky_function_createbuiltin(
                    "str.rstrip",
                    "S.rstrip([chars]) -> str\n\n"
                    "Return a copy of the string S with trailing whitespace removed.\n"
                    "If chars is given and not None, remove characters in chars instead.",
                    (sky_native_code_function_t)sky_string_rstrip,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "chars", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "split",
            sky_function_createbuiltin(
                    "str.split",
                    "S.split(sep=None, maxsplit=-1) -> list of strings\n\n"
                    "Return a list of the words in S, using sep as the\n"
                    "delimiter string. If maxsplit is given, at most maxsplit\n"
                    "splits are done. If sep is not specified or is None, any\n"
                    "whitespace string is a separator and empty strings are\n"
                    "removed from the result.",
                    (sky_native_code_function_t)sky_string_split,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    "maxsplit", SKY_DATA_TYPE_SSIZE_T, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "splitlines",
            sky_function_createbuiltin(
                    "str.splitlines",
                    "S.splitlines([keepends]) -> list of strings\n\n"
                    "Return a list of the lines in S, breaking at line boundaries.\n"
                    "Line breaks are not included in the resulting list unless keepends\n"
                    "is given and true.",
                    (sky_native_code_function_t)sky_string_splitlines,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "keepends", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "startswith",
            sky_function_createbuiltin(
                    "str.startswith",
                    "S.startswith(prefix[, start[, end]]) -> bool\n\n"
                    "Return True if S starts with the specified prefix, False otherwise.\n"
                    "With optional start, test S beginning at that position.\n"
                    "With optional end, stop comparing S at that position.\n"
                    "prefix can also be a tuple of strings to try.",
                    (sky_native_code_function_t)sky_string_startswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "prefix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "strip",
            sky_function_createbuiltin(
                    "str.strip",
                    "S.strip([chars]) -> str\n\n"
                    "Return a copy of the string S with leading and trailing\n"
                    "whitespace removed.\n"
                    "If chars is given and not None, remove characters in chars instead.",
                    (sky_native_code_function_t)sky_string_strip,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "chars", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "swapcase",
            sky_function_createbuiltin(
                    "str.swapcase",
                    "S.swapcase() -> str\n\n"
                    "Return a copy of S with uppercase characters converted to lowercase\n"
                    "and vice versa.",
                    (sky_native_code_function_t)sky_string_swapcase,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "title",
            sky_function_createbuiltin(
                    "str.title",
                    "S.title() -> str\n\n"
                    "Return a titlecased version of S, i.e. words start with title case\n"
                    "characters, all remaining cased characters have lower case.",
                    (sky_native_code_function_t)sky_string_title,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "translate",
            sky_function_createbuiltin(
                    "str.translate",
                    "S.translate(table) -> str\n\n"
                    "Return a copy of the string S, where all character have been mapped\n"
                    "through the given translation table, which must be a mapping of\n"
                    "Unicode ordinals to Unicode ordinals, strings, or None.\n"
                    "Unmapped characters are left untouched. Characters mapped to None\n"
                    "are deleted.",
                    (sky_native_code_function_t)sky_string_translate,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "table", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "upper",
            sky_function_createbuiltin(
                    "str.upper",
                    "S.upper() -> str\n\n"
                    "Return a copy of the string S converted to uppercase.",
                    (sky_native_code_function_t)sky_string_upper,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_string_type,
            "zfill",
            sky_function_createbuiltin(
                    "str.zfill",
                    "S.zfill(width) -> str\n\n"
                    "Pad a numeric string S with zeros on the left, to fill a field\n"
                    "of the specified width. The string S is never truncated.",
                    (sky_native_code_function_t)sky_string_zfill,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "width", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_string_type,
            "__new__",
            (sky_native_code_function_t)sky_string_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "object", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_string_type,
            "__repr__", sky_string_repr,
            "__str__", sky_string_str,
            "__format__", sky_string___format__,
            "__lt__", sky_string_lt,
            "__le__", sky_string_le,
            "__eq__", sky_string_eq,
            "__ne__", sky_string_ne,
            "__gt__", sky_string_gt,
            "__ge__", sky_string_ge,
            "__hash__", sky_string_hash,
            "__sizeof__", sky_string_sizeof,
            "__len__", sky_string_len,
            "__getitem__", sky_string_getitem,
            "__iter__", sky_string_iter,
            "__contains__", sky_string_contains,
            "__add__", sky_string_add,
            "__mul__", sky_string_mul,
            "__mod__", sky_string_printf,
            "__radd__", sky_string_radd,
            "__rmul__", sky_string_mul,
            "__rmod__", sky_string_rmod,
            "__getnewargs__", sky_string_getnewargs,
            NULL);


    sky_type_initialize_builtin(sky_string_iterator_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_string_iterator_type,
            "__init__",
            (sky_native_code_function_t)sky_string_iterator_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
            NULL);

    sky_type_setmethodslots(sky_string_iterator_type,
            "__len__", sky_string_iterator_len,
            "__iter__", sky_string_iterator_iter,
            "__next__", sky_string_iterator_next,
            "__reduce__", sky_string_iterator_reduce,
            "__setstate__", sky_string_iterator_setstate,
            NULL);
}
