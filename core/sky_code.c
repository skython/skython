#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"

#include "sky_cell.h"
#include "sky_code.h"
#include "sky_generator.h"


static void
sky_code_instance_init(sky_code_t   self,
                       ssize_t      argcount,
                       ssize_t      kwonlyargcount,
                       ssize_t      nlocals,
                       ssize_t      stacksize,
                       unsigned int flags,
                       sky_bytes_t  codestring,
                       sky_tuple_t  constants,
                       sky_tuple_t  names,
                       sky_tuple_t  varnames,
                       sky_string_t filename,
                       sky_string_t name,
                       int          firstlineno,
                       sky_bytes_t  lnotab,
                       sky_tuple_t  freevars,
                       sky_tuple_t  cellvars)
{
    sky_code_data_t *self_data = sky_code_data(self);

    ssize_t         i, ncellvars;
    sky_string_t    cellvar_name;

    self_data->co_argcount = argcount;
    self_data->co_kwonlyargcount = kwonlyargcount;
    self_data->co_nlocals = nlocals;
    self_data->co_stacksize = stacksize;
    self_data->co_flags = flags;
    self_data->co_firstlineno = firstlineno;

    if (!freevars || sky_None == (sky_object_t)freevars) {
        freevars = sky_tuple_empty;
    }
    if (!cellvars || sky_None == (sky_object_t)cellvars) {
        cellvars = sky_tuple_empty;
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_code)),
                      codestring,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_consts)),
                      sky_tuple_create(constants),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_names)),
                      sky_tuple_create(names),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_varnames)),
                      sky_tuple_create(varnames),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_filename)),
                      filename,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_name)),
                      name,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_lnotab)),
                      lnotab,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_freevars)),
                      sky_tuple_create(freevars),
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->co_cellvars)),
                      sky_tuple_create(cellvars),
                      self);

    if ((ncellvars = sky_object_len(self_data->co_cellvars)) > 0) {
        self_data->cellvar_names = sky_malloc(ncellvars * sizeof(ssize_t));
        for (i = 0; i < ncellvars; ++i) {
            cellvar_name = sky_tuple_get(self_data->co_cellvars, i);
            self_data->cellvar_names[i] =
                    sky_tuple_find(self_data->co_varnames, cellvar_name);
        }
    }
}


static void
sky_code_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_code_data_t *self_data = data;

    sky_free(self_data->cellvar_names);
}


static void
sky_code_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_code_data_t *self_data = data;

    sky_object_visit(self_data->co_lnotab, visit_data);
    sky_object_visit(self_data->co_code, visit_data);
    sky_object_visit(self_data->co_consts, visit_data);
    sky_object_visit(self_data->co_names, visit_data);
    sky_object_visit(self_data->co_varnames, visit_data);
    sky_object_visit(self_data->co_freevars, visit_data);
    sky_object_visit(self_data->co_cellvars, visit_data);
    sky_object_visit(self_data->co_filename, visit_data);
    sky_object_visit(self_data->co_name, visit_data);
}


sky_code_t
sky_code_create(ssize_t         argcount,
                ssize_t         kwonlyargcount,
                ssize_t         nlocals,
                ssize_t         stacksize,
                unsigned int    flags,
                sky_bytes_t     codestring,
                sky_tuple_t     constants,
                sky_tuple_t     names,
                sky_tuple_t     varnames,
                sky_string_t    filename,
                sky_string_t    name,
                int             firstlineno,
                sky_bytes_t     lnotab,
                sky_tuple_t     freevars,
                sky_tuple_t     cellvars)
{
    sky_code_t  code;

    code = sky_object_allocate(sky_code_type);
    sky_code_instance_init(code,
                           argcount,
                           kwonlyargcount,
                           nlocals,
                           stacksize,
                           flags,
                           codestring,
                           constants,
                           names,
                           varnames,
                           filename,
                           name,
                           firstlineno,
                           lnotab,
                           (freevars ? freevars : sky_tuple_empty),
                           (cellvars ? cellvars : sky_tuple_empty));

#if !defined(NDEBUG)
    sky_code_validate(code);
#endif

    return code;
}


sky_string_t
sky_code_disassemble(sky_code_t self)
{
    const sky_code_data_t   *self_data = sky_code_data(self);

    sky_object_t            flags;
    sky_string_builder_t    builder;

    flags = sky_list_create(NULL);
    if (self_data->co_flags & SKY_CODE_FLAG_OPTIMIZED) {
        sky_list_append(flags, SKY_STRING_LITERAL("optimized"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_NEWLOCALS) {
        sky_list_append(flags, SKY_STRING_LITERAL("newlocals"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_VARARGS) {
        sky_list_append(flags, SKY_STRING_LITERAL("varargs"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_VARKEYWORDS) {
        sky_list_append(flags, SKY_STRING_LITERAL("varkeywords"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_NESTED) {
        sky_list_append(flags, SKY_STRING_LITERAL("nested"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_GENERATOR) {
        sky_list_append(flags, SKY_STRING_LITERAL("generator"));
    }
    if (self_data->co_flags & SKY_CODE_FLAG_NOFREE) {
        sky_list_append(flags, SKY_STRING_LITERAL("nofree"));
    }
    flags = sky_string_join(SKY_STRING_LITERAL(", "), flags);

    builder = sky_string_builder_create();
    sky_string_builder_appendformat(builder, "%@:\n", self_data->co_name);
    sky_string_builder_appendformat(builder, "    argcount:       %zd\n", self_data->co_argcount);
    sky_string_builder_appendformat(builder, "    kwonlyargcount: %zd\n", self_data->co_kwonlyargcount);
    sky_string_builder_appendformat(builder, "    nlocals:        %zd\n", self_data->co_nlocals);
    sky_string_builder_appendformat(builder, "    stacksize:      %zd\n", self_data->co_stacksize);
    sky_string_builder_appendformat(builder, "    flags:          %@\n", flags);
    sky_string_builder_appendformat(builder, "    firstlineno:    %d\n", self_data->co_firstlineno);
    sky_string_builder_appendcodepoint(builder, '\n', 1);
    sky_string_builder_appendformat(builder, "    consts:   %@\n", self_data->co_consts);
    sky_string_builder_appendformat(builder, "    names:    %@\n", self_data->co_names);
    sky_string_builder_appendformat(builder, "    varnames: %@\n", self_data->co_varnames);
    sky_string_builder_appendformat(builder, "    freevars: %@\n", self_data->co_freevars);
    sky_string_builder_appendformat(builder, "    cellvars: %@\n", self_data->co_cellvars);
    sky_string_builder_appendcodepoint(builder, '\n', 1);


#define DECODE_OPCODE_ARGUMENT(_v)                                          \
    code = sky_util_varint_decode(code, code_end - code, (_v))

#define DECODE_OPCODE_OFFSET(_v)                                            \
    do {                                                                    \
        *(_v) = (code[0] << 8) | code[1];                                   \
        code += 2;                                                          \
    } while (0)

    SKY_ASSET_BLOCK_BEGIN {
        int                 lineno, prev_lineno;
        size_t              argv[3];
        uint16_t            exit_offset, jump_offset, offset;
        ptrdiff_t           instr_ub;
        sky_buffer_t        code_buffer, lnotab_buffer;
        const uint8_t       *code, *code_end, *lnotab, *lnotab_end;
        sky_code_opcode_t   opcode;

        sky_buffer_acquire(&code_buffer,
                           self_data->co_code,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&code_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        code = code_buffer.buf;
        code_end = (const uint8_t *)code_buffer.buf + code_buffer.len;

        lineno = self_data->co_firstlineno;
        prev_lineno = 0;
        if (!sky_buffer_check(self_data->co_lnotab)) {
            instr_ub = code_buffer.len;
        }
        else {
            sky_buffer_acquire(&lnotab_buffer,
                               self_data->co_lnotab,
                               SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&lnotab_buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            lnotab = lnotab_buffer.buf;
            lnotab_end = (const uint8_t *)lnotab_buffer.buf + lnotab_buffer.len;
            instr_ub = lnotab[0];
        }

        while (code < code_end) {
            while (code - (uint8_t *)code_buffer.buf >= instr_ub) {
                lineno += lnotab[1];
                lnotab += 2;
                if (lnotab >= lnotab_end) {
                    instr_ub = code_buffer.len;
                }
                else {
                    instr_ub += lnotab[0];
                }
            }
            opcode = *code++;
            if (lineno != prev_lineno) {
                prev_lineno = lineno;
                sky_string_builder_appendformat(builder,
                                                "    %4d    [%4d] %-20s    ",
                                                lineno,
                                                (code - 1) - (const uint8_t *)code_buffer.buf,
                                                sky_code_opcodes[opcode].name);
            }
            else {
                sky_string_builder_appendformat(builder,
                                                "            [%4d] %-20s    ",
                                                (code - 1) - (const uint8_t *)code_buffer.buf,
                                                sky_code_opcodes[opcode].name);
            }

            switch (opcode) {
                case SKY_CODE_OPCODE_PUSH_CONST:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));     /* index */
                    sky_string_builder_appendformat(
                            builder,
                            "%zu [%#@]",
                            argv[0],
                            sky_tuple_get(self_data->co_consts, argv[0]));
                    break;
                case SKY_CODE_OPCODE_PUSH_LOCAL:
                case SKY_CODE_OPCODE_POP_LOCAL:
                case SKY_CODE_OPCODE_DELETE_LOCAL:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));     /* index */
                    sky_string_builder_appendformat(
                            builder,
                            "%zu [%@]",
                            argv[0],
                            sky_tuple_get(self_data->co_varnames, argv[0]));
                    break;
                case SKY_CODE_OPCODE_PUSH_GLOBAL:
                case SKY_CODE_OPCODE_POP_GLOBAL:
                case SKY_CODE_OPCODE_DELETE_GLOBAL:
                case SKY_CODE_OPCODE_PUSH_NAME:
                case SKY_CODE_OPCODE_POP_NAME:
                case SKY_CODE_OPCODE_DELETE_NAME:
                case SKY_CODE_OPCODE_PUSH_ATTRIBUTE:
                case SKY_CODE_OPCODE_POP_ATTRIBUTE:
                case SKY_CODE_OPCODE_DELETE_ATTRIBUTE:
                case SKY_CODE_OPCODE_IMPORT_NAME:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));     /* index */
                    sky_string_builder_appendformat(
                            builder,
                            "%zu [%@]",
                            argv[0],
                            sky_tuple_get(self_data->co_names, argv[0]));
                    break;
                case SKY_CODE_OPCODE_PUSH_FREE:
                case SKY_CODE_OPCODE_PUSH_CLOSURE_FREE:
                case SKY_CODE_OPCODE_POP_FREE:
                case SKY_CODE_OPCODE_DELETE_FREE:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));     /* index */
                    sky_string_builder_appendformat(
                            builder,
                            "%zu [%@]",
                            argv[0],
                            sky_tuple_get(self_data->co_freevars, argv[0]));
                    break;
                case SKY_CODE_OPCODE_PUSH_CELL:
                case SKY_CODE_OPCODE_PUSH_CLOSURE_CELL:
                case SKY_CODE_OPCODE_POP_CELL:
                case SKY_CODE_OPCODE_DELETE_CELL:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));     /* index */
                    sky_string_builder_appendformat(
                            builder,
                            "%zu [%@]",
                            argv[0],
                            sky_tuple_get(self_data->co_cellvars, argv[0]));
                    break;
                case SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT:
                case SKY_CODE_OPCODE_JUMP:
                case SKY_CODE_OPCODE_JUMP_IF_FALSE:
                case SKY_CODE_OPCODE_JUMP_IF_TRUE:
                case SKY_CODE_OPCODE_EXCEPT_ENTER:
                case SKY_CODE_OPCODE_FINALLY_ENTER:
                    DECODE_OPCODE_OFFSET(&offset);
                    sky_string_builder_appendformat(builder, "%hu", offset);
                    break;
                case SKY_CODE_OPCODE_WITH_ENTER:
                case SKY_CODE_OPCODE_TRY_EXCEPT:
                case SKY_CODE_OPCODE_TRY_FINALLY:
                    DECODE_OPCODE_OFFSET(&jump_offset);
                    DECODE_OPCODE_OFFSET(&exit_offset);
                    sky_string_builder_appendformat(builder,
                                                    "jump %hu, exit %hu",
                                                    jump_offset,
                                                    exit_offset);
                    break;
                case SKY_CODE_OPCODE_COMPARE:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    switch ((sky_compare_op_t)argv[0]) {
                        case SKY_COMPARE_OP_NOT_EQUAL:
                            sky_string_builder_appendformat(builder, "!=");
                            break;
                        case SKY_COMPARE_OP_EQUAL:
                            sky_string_builder_appendformat(builder, "==");
                            break;
                        case SKY_COMPARE_OP_GREATER:
                            sky_string_builder_appendformat(builder, ">");
                            break;
                        case SKY_COMPARE_OP_GREATER_EQUAL:
                            sky_string_builder_appendformat(builder, ">=");
                            break;
                        case SKY_COMPARE_OP_LESS:
                            sky_string_builder_appendformat(builder, "<");
                            break;
                        case SKY_COMPARE_OP_LESS_EQUAL:
                            sky_string_builder_appendformat(builder, "<=");
                            break;
                        case SKY_COMPARE_OP_IS:
                            sky_string_builder_appendformat(builder, "is");
                            break;
                        case SKY_COMPARE_OP_IS_NOT:
                            sky_string_builder_appendformat(builder, "is not");
                            break;
                        case SKY_COMPARE_OP_IN:
                            sky_string_builder_appendformat(builder, "in");
                            break;
                        case SKY_COMPARE_OP_NOT_IN:
                            sky_string_builder_appendformat(builder, "not in");
                            break;
                    }
                    break;
                case SKY_CODE_OPCODE_UNPACK_SEQUENCE:
                case SKY_CODE_OPCODE_BUILD_DICT:
                case SKY_CODE_OPCODE_BUILD_LIST:
                case SKY_CODE_OPCODE_BUILD_SET:
                case SKY_CODE_OPCODE_BUILD_TUPLE:
                case SKY_CODE_OPCODE_RAISE:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    sky_string_builder_appendformat(builder, "%zu", argv[0]);
                    break;
                case SKY_CODE_OPCODE_UNPACK_STARRED:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    DECODE_OPCODE_ARGUMENT(&(argv[1]));
                    sky_string_builder_appendformat(
                            builder,
                            "%zu before, %zu after",
                            argv[0],
                            argv[1]);
                    break;
                case SKY_CODE_OPCODE_BUILD_CLASS:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    DECODE_OPCODE_ARGUMENT(&(argv[1]));
                    DECODE_OPCODE_ARGUMENT(&(argv[2]));
                    sky_string_builder_appendformat(
                            builder,
                            "%zx, %zu bases, %zu keywords",
                            argv[0],
                            argv[1],
                            argv[2]);
                    break;
                case SKY_CODE_OPCODE_BUILD_FUNCTION:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    DECODE_OPCODE_ARGUMENT(&(argv[1]));
                    DECODE_OPCODE_ARGUMENT(&(argv[2]));
                    sky_string_builder_appendformat(
                            builder,
                            "%zx, %zu defaults, %zu kwdefaults",
                            argv[0],
                            argv[1],
                            argv[2]);
                    break;
                case SKY_CODE_OPCODE_CALL:
                case SKY_CODE_OPCODE_CALL_ARGS:
                case SKY_CODE_OPCODE_CALL_KWS:
                case SKY_CODE_OPCODE_CALL_ARGS_KWS:
                    DECODE_OPCODE_ARGUMENT(&(argv[0]));
                    DECODE_OPCODE_ARGUMENT(&(argv[1]));
                    sky_string_builder_appendformat(
                            builder,
                            "%zu positional, %zu keyword",
                            argv[0],
                            argv[1]);
                    break;

                case SKY_CODE_OPCODE_NOP:
                case SKY_CODE_OPCODE_PUSH_ITEM:
                case SKY_CODE_OPCODE_PUSH_ITERATOR:
                case SKY_CODE_OPCODE_PUSH_TOP:
                case SKY_CODE_OPCODE_POP:
                case SKY_CODE_OPCODE_POP_ITEM:
                case SKY_CODE_OPCODE_DELETE_ITEM:
                case SKY_CODE_OPCODE_BINARY_ADD:
                case SKY_CODE_OPCODE_BINARY_SUB:
                case SKY_CODE_OPCODE_BINARY_MUL:
                case SKY_CODE_OPCODE_BINARY_DIV:
                case SKY_CODE_OPCODE_BINARY_MOD:
                case SKY_CODE_OPCODE_BINARY_POW:
                case SKY_CODE_OPCODE_BINARY_LSHIFT:
                case SKY_CODE_OPCODE_BINARY_RSHIFT:
                case SKY_CODE_OPCODE_BINARY_OR:
                case SKY_CODE_OPCODE_BINARY_XOR:
                case SKY_CODE_OPCODE_BINARY_AND:
                case SKY_CODE_OPCODE_BINARY_FLOORDIV:
                case SKY_CODE_OPCODE_INPLACE_ADD:
                case SKY_CODE_OPCODE_INPLACE_SUB:
                case SKY_CODE_OPCODE_INPLACE_MUL:
                case SKY_CODE_OPCODE_INPLACE_DIV:
                case SKY_CODE_OPCODE_INPLACE_MOD:
                case SKY_CODE_OPCODE_INPLACE_POW:
                case SKY_CODE_OPCODE_INPLACE_LSHIFT:
                case SKY_CODE_OPCODE_INPLACE_RSHIFT:
                case SKY_CODE_OPCODE_INPLACE_OR:
                case SKY_CODE_OPCODE_INPLACE_XOR:
                case SKY_CODE_OPCODE_INPLACE_AND:
                case SKY_CODE_OPCODE_INPLACE_FLOORDIV:
                case SKY_CODE_OPCODE_UNARY_NEGATIVE:
                case SKY_CODE_OPCODE_UNARY_POSITIVE:
                case SKY_CODE_OPCODE_UNARY_INVERT:
                case SKY_CODE_OPCODE_UNARY_NOT:
                case SKY_CODE_OPCODE_DISPLAY:
                case SKY_CODE_OPCODE_COMPARE_EXCEPTION:
                case SKY_CODE_OPCODE_LIST_APPEND:
                case SKY_CODE_OPCODE_SET_ADD:
                case SKY_CODE_OPCODE_IMPORT_MODULE:
                case SKY_CODE_OPCODE_IMPORT_STAR:
                case SKY_CODE_OPCODE_BUILD_SLICE:
                case SKY_CODE_OPCODE_ROTATE_TWO:
                case SKY_CODE_OPCODE_ROTATE_THREE:
                case SKY_CODE_OPCODE_RETURN:
                case SKY_CODE_OPCODE_YIELD:
                case SKY_CODE_OPCODE_YIELD_FROM:
                case SKY_CODE_OPCODE_WITH_EXIT:
                case SKY_CODE_OPCODE_TRY_EXIT:
                case SKY_CODE_OPCODE_EXCEPT_EXIT:
                case SKY_CODE_OPCODE_FINALLY_EXIT:
                    /* nothing extra to print */
                    break;
            }
            sky_string_builder_appendcodepoint(builder, '\n', 1);
        }
    } SKY_ASSET_BLOCK_END;

#undef DECODE_OPCODE_OFFSET
#undef DECODE_OPCODE_ARGUMENT

    return sky_string_builder_finalize(builder);
}


void
sky_code_dump(sky_code_t code)
{
    sky_format_fprintf(stderr, "%@", sky_code_disassemble(code));
}


sky_object_t
sky_code_eq(sky_code_t self, sky_object_t other)
{
    const sky_code_data_t   *self_data = sky_code_data(self);

    sky_object_t            eq;
    const sky_code_data_t   *other_data;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(other, sky_code_type)) {
        return sky_NotImplemented;
    }
    other_data = sky_code_data(other);

    if (self_data->co_argcount != other_data->co_argcount ||
        self_data->co_kwonlyargcount != other_data->co_kwonlyargcount ||
        self_data->co_nlocals != other_data->co_nlocals ||
        self_data->co_flags != other_data->co_flags ||
        self_data->co_firstlineno != other_data->co_firstlineno)
    {
        return sky_False;
    }

    eq = sky_object_eq(self_data->co_name, other_data->co_name);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_code, other_data->co_code);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_consts, other_data->co_consts);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_names, other_data->co_names);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_varnames, other_data->co_varnames);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_freevars, other_data->co_freevars);
    if (eq != sky_True) {
        return eq;
    }
    eq = sky_object_eq(self_data->co_cellvars, other_data->co_cellvars);
    if (eq != sky_True) {
        return eq;
    }

    return sky_True;
}


sky_object_t
sky_code_execute(sky_code_t     code,
                 sky_dict_t     globals,
                 sky_dict_t     locals,
                 sky_tuple_t    closure,
                 ssize_t        argc,
                 sky_object_t * argv)
{
    sky_code_data_t *code_data = sky_code_data(code);

    ssize_t             expected_argc, i, ncellvars, nfreevars;
    sky_frame_t         frame;
    sky_object_t        value;
    sky_string_t        name;
    sky_frame_data_t    *frame_data;

    frame = sky_frame_create(code, globals, locals);
    frame_data = sky_frame_data(frame);

    expected_argc = code_data->co_argcount + code_data->co_kwonlyargcount;
    if (code_data->co_flags & SKY_CODE_FLAG_VARARGS) {
        ++expected_argc;
    }
    if (code_data->co_flags & SKY_CODE_FLAG_VARKEYWORDS) {
        ++expected_argc;
    }
    if (expected_argc != argc) {
        sky_error_raise_format(
                sky_TypeError,
                "argument count mismatch (expected %zd; got %zd)",
                expected_argc,
                argc);
    }

    if (argc) {
        if (frame_data->nlocals < 0) {
            for (i = 0; i < argc; ++i) {
                name = sky_tuple_get(code_data->co_varnames, i);
                sky_dict_setitem(frame_data->f_locals,
                                 name,
                                 *(sky_object_t *)argv[i]);
            }
        }
        else {
            for (i = 0; i < argc; ++i) {
                sky_object_gc_set(&(frame_data->locals[i]),
                                  *(sky_object_t *)argv[i],
                                  frame);
            }
        }
    }

    if ((ncellvars = sky_object_len(code_data->co_cellvars)) > 0) {
        for (i = 0; i < ncellvars; ++i) {
            if (-1 == code_data->cellvar_names[i]) {
                value = NULL;
            }
            else if (frame_data->nlocals < 0) {
                name = sky_tuple_get(code_data->co_varnames,
                                     code_data->cellvar_names[i]);
                value = sky_dict_get(frame_data->f_locals, name, NULL);
            }
            else {
                value = frame_data->locals[code_data->cellvar_names[i]];
            }
            sky_object_gc_set(&(frame_data->cellvars[i]),
                              sky_cell_create(value),
                              frame);
        }
    }
    if ((nfreevars = sky_object_len(code_data->co_freevars)) > 0) {
        if (!sky_object_isa(closure, sky_tuple_type) ||
            sky_object_len(closure) != nfreevars)
        {
            sky_error_raise_format(sky_ValueError,
                                   "bad closure for %zd free variable%s",
                                   nfreevars,
                                   (1 == nfreevars ? "" : "s"));
        }
        for (i = 0; i < nfreevars; ++i) {
            sky_object_gc_set(&(frame_data->freevars[i]),
                              sky_tuple_get(closure, i),
                              frame);
        }
    }

    if (code_data->co_flags & SKY_CODE_FLAG_GENERATOR) {
        sky_frame_evaluate_trace(frame,
                                 SKY_FRAME_EVALUATE_TRACE_CALL,
                                 sky_None);
        value = sky_generator_createwithframe(frame);
        sky_frame_evaluate_trace(frame,
                                 SKY_FRAME_EVALUATE_TRACE_RETURN,
                                 sky_None);
        return value;
    }

    return sky_frame_evaluate(frame, NULL, NULL);
}


uintptr_t
sky_code_hash(sky_code_t self)
{
    const sky_code_data_t   *self_data = sky_code_data(self);

    return self_data->co_argcount ^
           self_data->co_kwonlyargcount ^
           self_data->co_nlocals ^
           self_data->co_flags ^
           self_data->co_firstlineno ^
           sky_object_hash(self_data->co_name) ^
           sky_object_hash(self_data->co_code) ^
           sky_object_hash(self_data->co_consts) ^
           sky_object_hash(self_data->co_names) ^
           sky_object_hash(self_data->co_varnames) ^
           sky_object_hash(self_data->co_freevars) ^
           sky_object_hash(self_data->co_cellvars);
}


void
sky_code_init(sky_code_t    self,
              ssize_t       argcount,
              ssize_t       kwonlyargcount,
              ssize_t       nlocals,
              ssize_t       stacksize,
              unsigned int  flags,
              sky_bytes_t   codestring,
              sky_tuple_t   constants,
              sky_tuple_t   names,
              sky_tuple_t   varnames,
              sky_string_t  filename,
              sky_string_t  name,
              int           firstlineno,
              sky_bytes_t   lnotab,
              sky_tuple_t   freevars,
              sky_tuple_t   cellvars)
{
    sky_code_instance_init(self,
                           argcount,
                           kwonlyargcount,
                           nlocals,
                           stacksize,
                           flags,
                           codestring,
                           constants,
                           names,
                           varnames,
                           filename,
                           name,
                           firstlineno,
                           lnotab,
                           freevars,
                           cellvars);

    sky_code_validate(self);
}


int
sky_code_lineno(sky_code_t code, ssize_t address)
{
    sky_code_data_t *code_data = sky_code_data(code);

    int             lineno;
    sky_buffer_t    buffer;
    const uint8_t   *b, *end;

    sky_buffer_acquire(&buffer, code_data->co_lnotab, SKY_BUFFER_FLAG_SIMPLE);
    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        end = buffer.buf + buffer.len;
        b = buffer.buf;

        lineno = code_data->co_firstlineno;
        while (b < end) {
            if (address < b[0]) {
                break;
            }
            address -= b[0];
            lineno += b[1];
            b += 2;
        }
    } SKY_ASSET_BLOCK_END;

    return lineno;
}


sky_string_t
sky_code_repr(sky_code_t self)
{
    sky_code_data_t *self_data = sky_code_data(self);

    if (!self_data->co_filename) {
        return sky_string_createfromformat(
                        "<%@ object %@ at %p, file ???, line %d>",
                        sky_type_name(sky_code_type),
                        self_data->co_name,
                        self,
                        self_data->co_firstlineno);
    }

    return sky_string_createfromformat(
                    "<%@ object %@ at %p, file \"%@\", line %d>",
                    sky_type_name(sky_code_type),
                    self_data->co_name,
                    self,
                    self_data->co_filename,
                    self_data->co_firstlineno);
}


static void
sky_code_validate_const_tuple(sky_tuple_t tuple, const char *name)
{
    sky_object_t    object;

    if (sky_tuple_type != sky_object_type(tuple)) {
        sky_error_raise_format(sky_TypeError, "%s must be a tuple", name);
    }
    if (sky_object_stack_push(tuple)) {
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(tuple, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        SKY_SEQUENCE_FOREACH(tuple, object) {
            if (sky_object_isa(object, sky_tuple_type)) {
                sky_code_validate_const_tuple(object, name);
            }
            else if (sky_None != object &&
                     sky_Ellipsis != object &&
                     sky_True != object &&
                     sky_False != object &&
                     !sky_object_isa(object, sky_bytes_type) &&
                     !sky_object_isa(object, sky_code_type) &&
                     !sky_object_isa(object, sky_complex_type) &&
                     !sky_object_isa(object, sky_float_type) &&
                     !sky_object_isa(object, sky_integer_type) &&
                     !sky_object_isa(object, sky_slice_type) &&
                     !sky_object_isa(object, sky_string_type))
            {
                sky_error_raise_format(sky_ValueError,
                                       "%s is not constant",
                                       name);
            }
        } SKY_SEQUENCE_FOREACH_END;
    } SKY_ASSET_BLOCK_END;
}


static void
sky_code_validate_name_tuple(sky_tuple_t tuple, const char *name)
{
    sky_object_t    object;

    if (sky_tuple_type != sky_object_type(tuple)) {
        sky_error_raise_format(sky_TypeError, "%s must be a tuple", name);
    }

    SKY_SEQUENCE_FOREACH(tuple, object) {
        if (sky_string_type != sky_object_type(object)) {
            sky_error_raise_format(sky_TypeError,
                                   "%s must contain only strings; got %#@",
                                   name,
                                   sky_type_name(sky_object_type(object)));
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_code_validate_bytecode(sky_code_data_t  *   code_data,
                           const uint8_t *      bytecode,
                           ssize_t              bytecode_len)
{
    size_t              count, flags, index;
    ssize_t             exit_offset, jump_offset, offset;
    const uint8_t       *bytecode_end, *pc;
    sky_compare_op_t    compare_op;
    sky_code_opcode_t   jump_opcode, opcode;

#define DECODE_OPCODE_ARGUMENT(_v)                                          \
    do {                                                                    \
        if (!(pc = sky_util_varint_decode(pc, bytecode_end - pc, (_v)))) {  \
            sky_error_raise_string(sky_ValueError, "incomplete bytecode");  \
        }                                                                   \
    } while (0)

#define DECODE_OPCODE_OFFSET(_v)                                            \
    do {                                                                    \
        if (pc + 1 >= bytecode_end) {                                       \
            sky_error_raise_string(sky_ValueError, "incomplete bytecode");  \
        }                                                                   \
        *(_v) = (pc[0] << 8) | pc[1];                                       \
        pc += 2;                                                            \
        if (*(_v) < 0 || *(_v) > bytecode_len) {                            \
            goto invalid_offset;                                            \
        }                                                                   \
    } while (0)

    pc = bytecode;
    bytecode_end = bytecode + bytecode_len;
    while (pc < bytecode_end) {
        opcode = *pc++;
        if (!sky_code_opcodes[opcode].name) {
            sky_error_raise_format(sky_ValueError,
                                   "illegal opcode %#02X",
                                   opcode);
        }
        switch (opcode) {
            case SKY_CODE_OPCODE_NOP:
            case SKY_CODE_OPCODE_PUSH_ITEM:
            case SKY_CODE_OPCODE_PUSH_ITERATOR:
            case SKY_CODE_OPCODE_PUSH_TOP:
            case SKY_CODE_OPCODE_POP:
            case SKY_CODE_OPCODE_POP_ITEM:
            case SKY_CODE_OPCODE_DELETE_ITEM:
            case SKY_CODE_OPCODE_BINARY_ADD:
            case SKY_CODE_OPCODE_BINARY_SUB:
            case SKY_CODE_OPCODE_BINARY_MUL:
            case SKY_CODE_OPCODE_BINARY_DIV:
            case SKY_CODE_OPCODE_BINARY_MOD:
            case SKY_CODE_OPCODE_BINARY_POW:
            case SKY_CODE_OPCODE_BINARY_LSHIFT:
            case SKY_CODE_OPCODE_BINARY_RSHIFT:
            case SKY_CODE_OPCODE_BINARY_OR:
            case SKY_CODE_OPCODE_BINARY_XOR:
            case SKY_CODE_OPCODE_BINARY_AND:
            case SKY_CODE_OPCODE_BINARY_FLOORDIV:
            case SKY_CODE_OPCODE_INPLACE_ADD:
            case SKY_CODE_OPCODE_INPLACE_SUB:
            case SKY_CODE_OPCODE_INPLACE_MUL:
            case SKY_CODE_OPCODE_INPLACE_DIV:
            case SKY_CODE_OPCODE_INPLACE_MOD:
            case SKY_CODE_OPCODE_INPLACE_POW:
            case SKY_CODE_OPCODE_INPLACE_LSHIFT:
            case SKY_CODE_OPCODE_INPLACE_RSHIFT:
            case SKY_CODE_OPCODE_INPLACE_OR:
            case SKY_CODE_OPCODE_INPLACE_XOR:
            case SKY_CODE_OPCODE_INPLACE_AND:
            case SKY_CODE_OPCODE_INPLACE_FLOORDIV:
            case SKY_CODE_OPCODE_UNARY_NEGATIVE:
            case SKY_CODE_OPCODE_UNARY_POSITIVE:
            case SKY_CODE_OPCODE_UNARY_INVERT:
            case SKY_CODE_OPCODE_UNARY_NOT:
            case SKY_CODE_OPCODE_DISPLAY:
            case SKY_CODE_OPCODE_COMPARE_EXCEPTION:
            case SKY_CODE_OPCODE_LIST_APPEND:
            case SKY_CODE_OPCODE_SET_ADD:
            case SKY_CODE_OPCODE_IMPORT_MODULE:
            case SKY_CODE_OPCODE_IMPORT_STAR:
            case SKY_CODE_OPCODE_BUILD_SLICE:
            case SKY_CODE_OPCODE_ROTATE_TWO:
            case SKY_CODE_OPCODE_ROTATE_THREE:
            case SKY_CODE_OPCODE_RETURN:
            case SKY_CODE_OPCODE_YIELD:
            case SKY_CODE_OPCODE_YIELD_FROM:
            case SKY_CODE_OPCODE_WITH_EXIT:
            case SKY_CODE_OPCODE_TRY_EXIT:
            case SKY_CODE_OPCODE_EXCEPT_EXIT:
            case SKY_CODE_OPCODE_FINALLY_EXIT:
                break;

            case SKY_CODE_OPCODE_PUSH_CONST:
                DECODE_OPCODE_ARGUMENT(&index);
                if (index >= (size_t)sky_object_len(code_data->co_consts)) {
                    goto invalid_index;
                }
                break;
            case SKY_CODE_OPCODE_PUSH_LOCAL:
            case SKY_CODE_OPCODE_POP_LOCAL:
            case SKY_CODE_OPCODE_DELETE_LOCAL:
                DECODE_OPCODE_ARGUMENT(&index);
                if (index >= (size_t)sky_object_len(code_data->co_varnames)) {
                    goto invalid_index;
                }
                break;

            case SKY_CODE_OPCODE_PUSH_GLOBAL:
            case SKY_CODE_OPCODE_PUSH_NAME:
            case SKY_CODE_OPCODE_PUSH_ATTRIBUTE:
            case SKY_CODE_OPCODE_POP_GLOBAL:
            case SKY_CODE_OPCODE_POP_NAME:
            case SKY_CODE_OPCODE_POP_ATTRIBUTE:
            case SKY_CODE_OPCODE_DELETE_GLOBAL:
            case SKY_CODE_OPCODE_DELETE_NAME:
            case SKY_CODE_OPCODE_DELETE_ATTRIBUTE:
            case SKY_CODE_OPCODE_IMPORT_NAME:
                DECODE_OPCODE_ARGUMENT(&index);
                if (index >= (size_t)sky_object_len(code_data->co_names)) {
                    goto invalid_index;
                }
                break;

            case SKY_CODE_OPCODE_PUSH_FREE:
            case SKY_CODE_OPCODE_PUSH_CLOSURE_FREE:
            case SKY_CODE_OPCODE_POP_FREE:
            case SKY_CODE_OPCODE_DELETE_FREE:
                DECODE_OPCODE_ARGUMENT(&index);
                if (index >= (size_t)sky_object_len(code_data->co_freevars)) {
                    goto invalid_index;
                }
                break;

            case SKY_CODE_OPCODE_PUSH_CELL:
            case SKY_CODE_OPCODE_PUSH_CLOSURE_CELL:
            case SKY_CODE_OPCODE_POP_CELL:
            case SKY_CODE_OPCODE_DELETE_CELL:
                DECODE_OPCODE_ARGUMENT(&index);
                if (index >= (size_t)sky_object_len(code_data->co_cellvars)) {
                    goto invalid_index;
                }
                break;

            case SKY_CODE_OPCODE_COMPARE:
                DECODE_OPCODE_ARGUMENT(&index);
                compare_op = (sky_compare_op_t)index;
                switch (compare_op) {
                    case SKY_COMPARE_OP_EQUAL:
                    case SKY_COMPARE_OP_NOT_EQUAL:
                    case SKY_COMPARE_OP_LESS:
                    case SKY_COMPARE_OP_LESS_EQUAL:
                    case SKY_COMPARE_OP_GREATER:
                    case SKY_COMPARE_OP_GREATER_EQUAL:
                    case SKY_COMPARE_OP_IS:
                    case SKY_COMPARE_OP_IS_NOT:
                    case SKY_COMPARE_OP_IN:
                    case SKY_COMPARE_OP_NOT_IN:
                        break;
                    default:
                        sky_error_raise_format(sky_ValueError,
                                               "illegal operator for '%s'",
                                               sky_code_opcodes[opcode].name);
                }
                break;

            case SKY_CODE_OPCODE_UNPACK_SEQUENCE:
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_UNPACK_STARRED:
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_BUILD_CLASS:
                DECODE_OPCODE_ARGUMENT(&flags);
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_BUILD_DICT:
            case SKY_CODE_OPCODE_BUILD_LIST:
            case SKY_CODE_OPCODE_BUILD_SET:
            case SKY_CODE_OPCODE_BUILD_TUPLE:
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_BUILD_FUNCTION:
                DECODE_OPCODE_ARGUMENT(&flags);
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_CALL:
            case SKY_CODE_OPCODE_CALL_ARGS:
            case SKY_CODE_OPCODE_CALL_KWS:
            case SKY_CODE_OPCODE_CALL_ARGS_KWS:
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > SSIZE_MAX) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_RAISE:
                DECODE_OPCODE_ARGUMENT(&count);
                if (count > 2) {
                    goto invalid_count;
                }
                break;

            case SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT:
            case SKY_CODE_OPCODE_JUMP:
            case SKY_CODE_OPCODE_JUMP_IF_FALSE:
            case SKY_CODE_OPCODE_JUMP_IF_TRUE:
                DECODE_OPCODE_OFFSET(&offset);
                break;

            case SKY_CODE_OPCODE_WITH_ENTER:
                DECODE_OPCODE_OFFSET(&jump_offset);
                DECODE_OPCODE_OFFSET(&exit_offset);
                if (exit_offset < pc - bytecode) {
                    goto invalid_offset;
                }
                break;
            case SKY_CODE_OPCODE_TRY_EXCEPT:
                DECODE_OPCODE_OFFSET(&jump_offset);
                jump_opcode = *(bytecode + jump_offset);
                if (SKY_CODE_OPCODE_EXCEPT_ENTER != jump_opcode) {
                    goto invalid_offset;
                }
                DECODE_OPCODE_OFFSET(&exit_offset);
                if (exit_offset < pc - bytecode) {
                    goto invalid_offset;
                }
                break;
            case SKY_CODE_OPCODE_TRY_FINALLY:
                DECODE_OPCODE_OFFSET(&jump_offset);
                jump_opcode = *(bytecode + jump_offset);
                if (SKY_CODE_OPCODE_FINALLY_ENTER != jump_opcode) {
                    goto invalid_offset;
                }
                DECODE_OPCODE_OFFSET(&exit_offset);
                if (exit_offset < pc - bytecode) {
                    goto invalid_offset;
                }
                break;

            case SKY_CODE_OPCODE_EXCEPT_ENTER:
            case SKY_CODE_OPCODE_FINALLY_ENTER:
                DECODE_OPCODE_OFFSET(&exit_offset);
                if (exit_offset < pc - bytecode) {
                    goto invalid_offset;
                }
                break;
        }
    }
    return;

invalid_count:
    sky_error_raise_format(sky_ValueError,
                           "count for instruction '%s' is invalid.",
                           sky_code_opcodes[opcode].name);
invalid_index:
    sky_error_raise_format(sky_ValueError,
                           "index for instruction '%s' is invalid.",
                           sky_code_opcodes[opcode].name);
invalid_offset:
    sky_error_raise_format(sky_ValueError,
                           "jump offset for instruction '%s' is invalid.",
                           sky_code_opcodes[opcode].name);
}


static void
sky_code_validate_lnotab(const uint8_t *    lnotab,
                         ssize_t            lnotab_len,
                         ssize_t            bytecode_len)
{
    uintmax_t       address;
    const uint8_t   *b, *end;

    /* For validation, all that can really be done here is to make sure that
     * the address never runs past the end of available bytecode. It's a pretty
     * worthless check, all in all.
     */
    address = 0;
    for (b = lnotab, end = lnotab + lnotab_len; b < end; b += 2) {
        address += *b;
        if (address > SSIZE_MAX || (ssize_t)address > bytecode_len) {
            sky_error_raise_string(sky_ValueError,
                                   "line number table is invalid.");
        }
    }
}


void
sky_code_validate(sky_code_t self)
{
    sky_code_data_t *self_data = sky_code_data(self);

    ssize_t         nargs, nlocals;
    sky_object_t    object;

    if (self_data->co_argcount < 0) {
        sky_error_raise_string(sky_ValueError,
                               "argcount must not be negative");
    }
    if (self_data->co_kwonlyargcount < 0) {
        sky_error_raise_string(sky_ValueError,
                               "kwonlyargcount must not be negative");
    }
    if (self_data->co_nlocals < 0) {
        sky_error_raise_string(sky_ValueError,
                               "nlocals must not be negative");
    }

    /* Make sure tuples are actually tuples, and that they only contain valid
     * objects.
     */
    sky_code_validate_const_tuple(self_data->co_consts, "constants");
    sky_code_validate_name_tuple(self_data->co_names, "names");
    sky_code_validate_name_tuple(self_data->co_varnames, "varnames");
    sky_code_validate_name_tuple(self_data->co_freevars, "freevars");
    sky_code_validate_name_tuple(self_data->co_cellvars, "cellvars");

    nargs = self_data->co_argcount + self_data->co_kwonlyargcount;
    if (self_data->co_flags & SKY_CODE_FLAG_VARARGS) {
        ++nargs;
    }
    if (self_data->co_flags & SKY_CODE_FLAG_VARKEYWORDS) {
        ++nargs;
    }
    /* Counts are checked separately here to guard against integer overflow
     * in nargs.
     */
    if (self_data->co_argcount > 255 ||
        self_data->co_kwonlyargcount > 255 ||
        nargs > 257)
    {
        sky_error_raise_string(sky_SyntaxError,
                               "maximum number of arguments is 255");
    }
    if (nargs > sky_tuple_len(self_data->co_varnames)) {
        sky_error_raise_string(
                sky_ValueError,
                "len(varnames) must not be less than the total number of arguments");
    }

    /* Storing nlocals is somewhat redundant, but it does make it easier to
     * query the information elsewhere, I guess. Regardless, it's required by
     * the code object as documented, so at least make sure it's right.
     */
    nlocals = sky_tuple_len(self_data->co_varnames);
    if (nlocals != self_data->co_nlocals) {
        sky_error_raise_string(
                sky_ValueError,
                "length of varnames less cellvars must equal nlocals");
    }

    /* Elements in co_freevars must not be present in co_varnames */
    SKY_SEQUENCE_FOREACH(self_data->co_freevars, object) {
        if (sky_object_contains(self_data->co_varnames, object)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "names in freevars tuple must not appear in varnames");
        }
    } SKY_SEQUENCE_FOREACH_END;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    bytecode_buffer, lnotab_buffer;

        sky_buffer_acquire(&bytecode_buffer,
                           self_data->co_code,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&bytecode_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_code_validate_bytecode(self_data,
                                   bytecode_buffer.buf,
                                   bytecode_buffer.len);

        sky_buffer_acquire(&lnotab_buffer,
                           self_data->co_lnotab,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&lnotab_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_code_validate_lnotab(lnotab_buffer.buf,
                                 lnotab_buffer.len,
                                 bytecode_buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static const char sky_code_type_doc[] =
"code(argcount, kwonlyargcount, nlocals, stacksize, flags, codestring,\n"
"     constants, names, varnames, filename, name, firstlineno,\n"
"     lnotab[, freevars[, cellvars]])\n"
"\n"
"Create a code object.  Not for the faint of heart.";


SKY_TYPE_DEFINE_SIMPLE(code,
                       "code",
                       sizeof(sky_code_data_t),
                       NULL,
                       sky_code_instance_finalize,
                       sky_code_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       sky_code_type_doc);


void
sky_code_initialize_library(void)
{
    sky_type_initialize_builtin(sky_code_type, 0);

#define sky_code_data_offset(_f)    offsetof(sky_code_data_t, _f)
    sky_type_setmembers(sky_code_type,
            "co_argcount",       NULL, sky_code_data_offset(co_argcount),
                                 SKY_DATA_TYPE_SSIZE_T,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_kwonlyargcount", NULL, sky_code_data_offset(co_kwonlyargcount),
                                 SKY_DATA_TYPE_SSIZE_T,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_nlocals",        NULL, sky_code_data_offset(co_nlocals),
                                 SKY_DATA_TYPE_SSIZE_T,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_stacksize",      NULL, sky_code_data_offset(co_stacksize),
                                 SKY_DATA_TYPE_SSIZE_T,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_flags",          NULL, sky_code_data_offset(co_flags),
                                 SKY_DATA_TYPE_UINT,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_code",           NULL, sky_code_data_offset(co_code),
                                 SKY_DATA_TYPE_OBJECT_BYTES,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_consts",         NULL, sky_code_data_offset(co_consts),
                                 SKY_DATA_TYPE_OBJECT_TUPLE,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_names",          NULL, sky_code_data_offset(co_names),
                                 SKY_DATA_TYPE_OBJECT_TUPLE,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_varnames",       NULL, sky_code_data_offset(co_varnames),
                                 SKY_DATA_TYPE_OBJECT_TUPLE,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_freevars",       NULL, sky_code_data_offset(co_freevars),
                                 SKY_DATA_TYPE_OBJECT_TUPLE,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_cellvars",       NULL, sky_code_data_offset(co_cellvars),
                                 SKY_DATA_TYPE_OBJECT_TUPLE,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_filename",       NULL, sky_code_data_offset(co_filename),
                                 SKY_DATA_TYPE_OBJECT_STRING,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_name",           NULL, sky_code_data_offset(co_name),
                                 SKY_DATA_TYPE_OBJECT_STRING,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_firstlineno",    NULL, sky_code_data_offset(co_firstlineno),
                                 SKY_DATA_TYPE_INT,
                                 SKY_MEMBER_FLAG_READONLY,
            "co_lnotab",         NULL, sky_code_data_offset(co_lnotab),
                                 SKY_DATA_TYPE_OBJECT_BYTES,
                                 SKY_MEMBER_FLAG_READONLY,
            NULL);
#undef sky_code_data_offset

    sky_type_setattr_builtin(
            sky_code_type,
            "_disassemble",
            sky_function_createbuiltin(
                    "code._disassemble",
                    "Disassemble a code object into a string",
                    (sky_native_code_function_t)sky_code_disassemble,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_code_type,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_code_type,
            "__init__",
            (sky_native_code_function_t)sky_code_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "argcount", SKY_DATA_TYPE_SSIZE_T, NULL,
            "kwonlyargcount", SKY_DATA_TYPE_SSIZE_T, NULL,
            "nlocals", SKY_DATA_TYPE_SSIZE_T, NULL,
            "stacksize", SKY_DATA_TYPE_SSIZE_T, NULL,
            "flags", SKY_DATA_TYPE_UINT, NULL,
            "codestring", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
            "constants", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
            "names", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
            "varnames", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
            "filename", SKY_DATA_TYPE_OBJECT_STRING, NULL,
            "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
            "firstlineno", SKY_DATA_TYPE_INT, NULL,
            "lnotab", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
            "freevars", SKY_DATA_TYPE_OBJECT_TUPLE, sky_tuple_empty,
            "cellvars", SKY_DATA_TYPE_OBJECT_TUPLE, sky_tuple_empty,
            NULL);

    sky_type_setmethodslots(sky_code_type,
            "__repr__", sky_code_repr,
            "__eq__", sky_code_eq,
            "__hash__", sky_code_hash,
            NULL);
}


const sky_code_op_t sky_code_opcodes[256] = {
    { "nop",                0,  0,  0,  },

    { "push_const",         1,  0,  1,  },  /* index into co_consts         */
    { "push_local",         1,  0,  1,  },  /* index: range(co_nlocals)     */
    { "push_global",        1,  0,  1,  },  /* index into co_names          */
    { "push_name",          1,  0,  1,  },  /* index into co_names          */
    { "push_free",          1,  0,  1,  },  /* index into co_freevars       */
    { "push_cell",          1,  0,  1,  },  /* index into co_cellvars       */
    { "push_attribute",     1,  1,  1,  },  /* index into co_names          */
    { "push_item",          0,  2,  1,  },
    { "push_iterator",      0,  1,  1,  },
    { "push_iterator_next", 1,  0,  1,  },  /* jump offset on StopIteration */
    { "push_top",           0,  0,  1,  },
    { "push_closure_cell",  1,  0,  1,  },  /* index into co_cellvars       */
    { "push_closure_free",  1,  0,  1,  },  /* index into co_freevars       */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x0E              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x0F              */

    { "pop",                0,  1,  0,  },
    { "pop_local",          1,  1,  0,  },  /* index: range(co_nlocals)     */
    { "pop_global",         1,  1,  0,  },  /* index into co_names          */
    { "pop_name",           1,  1,  0,  },  /* index into co_names          */
    { "pop_free",           1,  1,  0,  },  /* index into co_freevars       */
    { "pop_cell",           1,  1,  0,  },  /* index into co_cellvars       */
    { "pop_attribute",      1,  2,  0,  },  /* index into co_names          */
    { "pop_item",           0,  3,  0,  },

    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x18              */
    { "delete_local",       1,  0,  0,  },  /* index: range(co_nlocals)     */
    { "delete_global",      1,  0,  0,  },  /* index into co_names          */
    { "delete_name",        1,  0,  0,  },  /* index into co_names          */
    { "delete_free",        1,  0,  0,  },  /* index into co_freevars       */
    { "delete_cell",        1,  0,  0,  },  /* index into co_cellvars       */
    { "delete_attribute",   1,  1,  0,  },  /* index into co_names          */
    { "delete_item",        0,  2,  0,  },

    { "binary_add",         0,  2,  1,  },
    { "binary_sub",         0,  2,  1,  },
    { "binary_mul",         0,  2,  1,  },
    { "binary_div",         0,  2,  1,  },
    { "binary_mod",         0,  2,  1,  },
    { "binary_pow",         0,  2,  1,  },
    { "binary_lshift",      0,  2,  1,  },
    { "binary_rshift",      0,  2,  1,  },
    { "binary_or",          0,  2,  1,  },
    { "binary_xor",         0,  2,  1,  },
    { "binary_and",         0,  2,  1,  },
    { "binary_floordiv",    0,  2,  1,  },
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x2C              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x2D              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x2E              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x2F              */

    { "inplace_add",        0,  2,  1,  },
    { "inplace_sub",        0,  2,  1,  },
    { "inplace_mul",        0,  2,  1,  },
    { "inplace_div",        0,  2,  1,  },
    { "inplace_mod",        0,  2,  1,  },
    { "inplace_pow",        0,  2,  1,  },
    { "inplace_lshift",     0,  2,  1,  },
    { "inplace_rshift",     0,  2,  1,  },
    { "inplace_or",         0,  2,  1,  },
    { "inplace_xor",        0,  2,  1,  },
    { "inplace_and",        0,  2,  1,  },
    { "inplace_floordiv",   0,  2,  1,  },
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x3C              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x3D              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x3E              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x3F              */

    { "unary_negative",     0,  1,  1,  },
    { "unary_positive",     0,  1,  1,  },
    { "unary_invert",       0,  1,  1,  },
    { "unary_not",          0,  1,  1,  },
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x44              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x45              */
    { "display",            0,  1,  0,  },
    { "compare_exception",  0,  2,  1,  },  /* index into co_names          */
    { "compare",            1,  2,  1,  },  /* sky_compare_op_t             */
    { "unpack_sequence",    1,  1, -1,  },  /* # elements on stack          */
    { "unpack_starred",     2,  1, -1,  },  /* # before, # after            */
    { "list_append",        0,  2,  0,  },
    { "set_add",            0,  2,  0,  },
    { "import_module",      0,  3,  1,  },
    { "import_star",        0,  0,  0,  },
    { "import_name",        1,  0,  1,  },

    { "build_class",        3, -1,  1,  },  /* flags, # bases, # keywords   */
    { "build_dict",         1, -1,  1,  },  /* # key/value pairs on stack   */
    { "build_function",     3, -1,  1,  },  /* flags, # defaults, # kwdefaults  */
    { "build_generator",    0,  0,  1,  },
    { "build_list",         1, -1,  1,  },  /* # elements on stack          */
    { "build_set",          1, -1,  1,  },  /* # elements on stack          */
    { "build_slice",        0,  3,  1,  },
    { "build_tuple",        1, -1,  1,  },  /* # elements on stack          */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x58              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x59              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x5A              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x5B              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x5C              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x5D              */
    { "rotate_two",         0,  3,  3,  },
    { "rotate_three",       0,  2,  2,  },

    { "call",               2, -1,  1,  },  /* # positional, # keyword      */
    { "call_args",          2, -1,  1,  },  /* # positional, # keyword      */
    { "call_kws",           2, -1,  1,  },  /* # positional, # keyword      */
    { "call_args_kws",      2, -1,  1,  },  /* # positional, # keyword      */
    { "return",             0,  0,  0,  },
    { "yield",              0,  1,  1,  },
    { "yield_from",         0,  2,  1,  },
    { "raise",              1, -1,  0,  },  /* # args to pop (exc, cause)   */
    { "jump",               1,  0,  0,  },  /* absolute offset              */
    { "jump_if_false",      1,  0,  0,  },  /* absolute offset              */
    { "jump_if_true",       1,  0,  0,  },  /* absolute offset              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x6B              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x6C              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x6D              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x6E              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x6F              */

    { "with_enter",         2,  1,  1,  },  /* offset to exit code          */
    { "with_exit",          0,  0,  0,  },
    { "try_except",         2,  0,  0,  },  /* offset to except code        */
    { "try_finally",        2,  0,  0,  },  /* offset to finally code       */
    { "try_exit",           0,  0,  0,  },
    { "except_enter",       1,  0,  1,  },
    { "except_exit",        0,  0,  0,  },
    { "finally_enter",      1,  0,  0,  },
    { "finally_exit",       0,  0,  0,  },
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x79              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7A              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7B              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7C              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7D              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7E              */
    { NULL,                 0,  0,  0,  },  /* UNASSIGNED 0x7F              */
};
