/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_bytes Immutable Byte Array Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BYTES_H__
#define __SKYTHON_CORE_SKY_BYTES_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** A bytes object instance that is empty (i.e., ''). **/
SKY_EXTERN sky_bytes_t const sky_bytes_empty;


/** Combine two bytes objects together, resulting in a new bytes object.
  * The @a other object to be combined with @a self may be any object that
  * supports the buffer protocol, which of course includes bytes objects.
  *
  * @param[in]  self    the first object to add.
  * @param[in]  other   the second object to add.
  * @return     a new bytes object that is the concatenation of @a self and
  *             @a other.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_add(sky_bytes_t self, sky_object_t other);


/** Return the byte at a specific index position in a bytes object.
  *
  * @param[in]  self    the bytes object to query.
  * @param[in]  index   the index position to query.
  * @return     the byte located at the requested index position.
  */
SKY_EXTERN uint8_t
sky_bytes_byteat(sky_bytes_t self, ssize_t index);


/** Capitalize a bytes object as if it were an ASCII string.
  * A capitalized bytes object is the same as a capitalized string, which is
  * the first character uppercased and all remaining characters lowercased.
  *
  * @param[in]  self    the bytes object to capitalize.
  * @return     a new bytes object that is @a self capitalized.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_capitalize(sky_bytes_t self);


/** Center a bytes object.
  *
  * @param[in]  self        the bytes object to center.
  * @param[in]  width       the width in which to center @a self.
  * @param[in]  fillchar    the byte value to use to fill the leading and
  *                         trailing area after centering. This should be
  *                         a bytes or bytearray object of length 1.
  * @return     a bytes object containing @a self centered.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_center(sky_bytes_t self, ssize_t width, sky_object_t fillchar);


/** Compare a bytes object with another object.
  * Any object supporting the buffer protocol may be compared with @a self.
  * Possible return values are @c sky_True, @c sky_False, and
  * @c sky_NotImplemented, which will be returned if @a other does not support
  * the buffer protocol.
  *
  * @param[in]  self        the bytes object to compare with @a other.
  * @param[in]  other       the object to compare with @a self.
  * @param[in]  compare_op  the comparison operation to perform.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not support the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_compare(sky_bytes_t       self,
                  sky_object_t      other,
                  sky_compare_op_t  compare_op);


/** Determine whether an arbitrary string of bytes is contained within a bytes
  * object.
  *
  * @param[in]  self    the bytes object to check.
  * @param[in]  item    the item to look for, which may be any object that
  *                     supports the buffer protocol.
  * @return     @c SKY_TRUE if @a item is contained within @a self, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytes_contains(sky_bytes_t self, sky_object_t item);


/** Return a copy of a bytes object, converting it to a true bytes object if
  * it's an instance of a bytes sub-type.
  *
  * @param[in]  self    the bytes object to copy.
  * @return     a bytes object instance that is a copy of @a bytes.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_copy(sky_bytes_t self);


/** Count the number of times a string of bytes occurs in a bytes object.
  *
  * @param[in]  self    the bytes object in which the number of occurrences of
  *                     @a sub is to be counted.
  * @param[in]  sub     the string of bytes to count occurrences of.
  * @param[in]  start   the starting offset (zero-based, inclusive).
  * @param[in]  end     the ending offset (zero-based, exclusive).
  * @return     the number of times @a sub occurs in @a self[@a start:@a end].
  */
SKY_EXTERN ssize_t
sky_bytes_count(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end);


/** Create a new bytes object instance from an array of bytes in memory.
  * The new bytes object instance will make a copy of the byte array, which
  * the bytes object instance will own and free when it is finalized.
  *
  * @param[in]  bytes       an array of bytes to be copied.
  * @param[in]  nbytes      the number of bytes to copy.
  * @return     the new bytes object instance.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_createfrombytes(const void *bytes, size_t nbytes);


/** Create a new bytes object instance with a reference to an array of bytes
  * in memory.
  * The new bytes object instance will store a reference the array of bytes.
  * The bytes API expects that the content of the underlying array will not
  * change while the bytes object exists.
  *
  * @param[in]  bytes       an array of bytes.
  * @param[in]  nbytes      the number of bytes in the array.
  * @param[in]  free        a function to call to release the @a bytes array
  *                         when the bytes object instance is finalized. May
  *                         be @c NULL.
  * @return     the new bytes object instance.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_createwithbytes(const void *bytes, size_t nbytes, sky_free_t free);


/** Create a new bytes object instance using a format string.
  * The array of bytes used is created via sky_format_asprintf(), and the
  * bytes object instance will free the array when it is finalized.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ...         additional arguments as required by the format
  *                         string.
  * @return     the new bytes object instance.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_createfromformat(const char *format, ...);


/** Create a new bytes object instance using a format string.
  * The array of bytes used is created via sky_format_asprintf(), and the
  * bytes object instance will free the array when it is finalized.
  *
  * @param[in]  format      the format string to use.
  * @param[in]  ap          additional arguments as required by the format
  *                         string.
  * @return     the new bytes object instance.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_createfromformatv(const char *format, va_list ap);


/** Create a new bytes object using an arbitrary object as its data source.
  * An object capable of being used to create a bytes object either implements
  * the buffer protocol or is an iterable containing only integers in the
  * range of 0 to 255.
  *
  * @param[in]  object      the object to use as a data source.
  * @return     a new bytes object instance created from @a object.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_createfromobject(sky_object_t object);


/** Return a pointer to the memory for a bytes object.
  * This function must be called from within an asset block, because it may
  * need to save assets. Do not free the memory returned; an asset may be set
  * up to do that already if it is necessary. If @a terminate is @c SKY_FALSE,
  * it is likely that the pointer returned will be the same as stored in the
  * bytes object; otherwise, it may be dynamically allocated. However the
  * returned pointer comes into being is an implementation detail. Do not
  * assume anything about it, and do not ever modify its contents. The lifetime
  * of the returned pointer shall only be valid as long as the bytes object is
  * valid and within the scope of the active asset block when the function is
  * called.
  *
  * @param[in]  self        the bytes object for which the C representation of
  *                         its bytes is to be returned.
  * @param[in]  terminate   @c SKY_TRUE if the returned pointer should be
  *                         @c NUL terminated; otherwise, @c SKY_FALSE to
  *                         return the data exactly as it is.
  * @return     a C pointer to the bytes for the object @a self.
  */
SKY_EXTERN const uint8_t *
sky_bytes_cstring(sky_bytes_t self, sky_bool_t terminate);


/** Decode a bytes object using a specified encoding, resulting in a string.
  *
  * @param[in]  self        the bytes object to be decoded.
  * @param[in]  encoding    the encoding to use, which may be @c NULL to use
  *                         the default (UTF-8).
  * @param[in]  errors      how to handle decoding errors, which may be @c NULL
  *                         to use the default ("strict").
  * @return     a new string object instance decoded from @a self.
  */
SKY_EXTERN sky_string_t
sky_bytes_decode(sky_bytes_t self, sky_string_t encoding, sky_string_t errors);


/** Determine whether a bytes object ends with a specified run of bytes.
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  suffix      the suffix for comparison, which must be either an
  *                         object supporting the buffer protocol or a tuple of
  *                         objects supporting the buffer protocol.
  * @param[in]  start       the starting offset of @a self to compare.
  * @param[in]  end         the ending offset of @a self to compare.
  * @return     @c SKY_TRUE if @a self[@a start:@a end] ends with @a suffix, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytes_endswith(sky_bytes_t  self,
                   sky_object_t suffix,
                   ssize_t      start,
                   ssize_t      end);


/** Compare a bytes object with another object for equality.
  * This is the implementation of __eq__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_EQUAL).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_eq(sky_bytes_t self, sky_object_t other);


/** Expand tab characters to spaces.
  *
  * @param[in]  self        the bytes object to expand.
  * @param[in]  tabsize     the tab size to use.
  * @return     a new bytes object with tabs expanded to spaces.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_expandtabs(sky_bytes_t self, ssize_t tabsize);


/** Find the starting offset of a run of bytes in a bytes object.
  *
  * @param[in]  self        the bytes object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytes_find(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end);


/** Create a new bytes object from a string of hex digits.
  *
  * @param[in]  cls         the type of object to instantiate, which must be
  *                         @c sky_bytes_type or a sub-type.
  * @param[in]  string      the string of hex digits to convert, which may 
  *                         contain whitespace, but otherwise must contain an
  *                         even number of hex digits for conversion (e.g.,
  *                         fromhex('61 62 63') -> b'abc'
  * @return     a new object instance of type @c initialized from @a string.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_fromhex(sky_type_t cls, sky_string_t string);


/** Compare a bytes object with another object for greater than or equality.
  * This is the implementation of __ge__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_GREATER_EQUAL).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_ge(sky_bytes_t self, sky_object_t other);


/** Return a portion of a bytes object.
  * This is the implementation of __getitem__().
  *
  * @param[in]  self        the bytes object to slice.
  * @param[in]  item        the slice of @a bytes to return, which may be a
  *                         number object implementing __index__(), or a slice
  *                         object.
  * @return     if @a item is a number, the return will be an integer object
  *             representing the byte at the requested offset; otherwise, a
  *             new bytes object that is the requested slice will be returned.
  */
SKY_EXTERN sky_object_t
sky_bytes_getitem(sky_bytes_t self, sky_object_t item);


/** Compare a bytes object with another object for greater than.
  * This is the implementation of __gt__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_GREATER).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_gt(sky_bytes_t self, sky_object_t other);


/** Return a hash value for a bytes object.
  * This is the implementation of __hash__().
  *
  * @param[in]  self        the bytes object for which a hash value is to be
  *                         returned.
  * @return     the hash value for @a self.
  */
SKY_EXTERN uintptr_t
sky_bytes_hash(sky_bytes_t self);


/** Find the starting offset of a run of bytes in a bytes object.
  * This function is the same as sky_bytes_find(), except that
  * @c sky_IndexError will be raised if @a sub is not found instead of
  * returning -1.
  *
  * @param[in]  self        the bytes object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self.
  */
SKY_EXTERN ssize_t
sky_bytes_index(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end);


/** Determine whether a bytes object contains only ASCII alpha-numeric
  * characters.
  *
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII alpha-numeric characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_isalnum(sky_bytes_t self);


/** Determine whether a bytes object contains only ASCII alphabetic characters.
  *
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII alphabetic characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_isalpha(sky_bytes_t self);


/** Determine whether a bytes object contains only ASCII digit characters.
  *
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII digit characters, @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_isdigit(sky_bytes_t self);


/** Determine whether a bytes object contains only ASCII lowercase cased
  * characters.
  *
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are both ASCII cased and lowercase characters,
  *             @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_islower(sky_bytes_t self);


/** Determine whether a bytes object contains only ASCII whitepace characters.
  * 
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are ASCII whitespace characters, @c SKY_FALSE
  *             otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_isspace(sky_bytes_t self);


/** Determine whether a bytes object contains ASCII title cased character.
  * A run of bytes is title cased if it contains at least one cased character,
  * uppercase characters follow only non-cased characters, and all other cased
  * characters are lowercase. For example, b'23. Illuminated History' is title
  * cased.
  */
SKY_EXTERN sky_bool_t
sky_bytes_istitle(sky_bytes_t self);


/** Determine whether a bytes object contains only ASCII uppercase cased
  * characters.
  *
  * @param[in]  self        the bytes object to check.
  * @return     @c SKY_TRUE if @a self contains at least one character and all
  *             characters are both ASCII cased and uppercase characters,
  *             @c SKY_FALSE otherwise.
  */
SKY_EXTERN sky_bool_t
sky_bytes_isupper(sky_bytes_t self);


/** Combine a sequence of bytes objects together into a single bytes object.
  * All of the objects yielded by iterating @a iterable must support the buffer
  * protocol. The bytes object @a self is inserted in between each object from
  * @a iterable.
  *
  * @param[in]  self        the bytes object to use as a separator between each
  *                         object yielded by iterating @a iterable.
  * @param[in]  iterable    an iterable object that must yield only objects
  *                         supporting the buffer protocol.
  * @return     a new bytes object that is the concatenation of all objects
  *             from @a iterable with @a self inserted between each.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_join(sky_bytes_t self, sky_object_t iterable);


/** Return an iterator for a bytes object.
  * This is the implementation of __iter__().
  *
  * @param[in]  self        the bytes object for which an iterator object is
  *                         to be returned.
  */
SKY_EXTERN sky_object_t
sky_bytes_iter(sky_bytes_t self);


/** Compare a bytes object with another object for less than or equality.
  * This is the implementation of __le__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_LESS_EQUAL).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_le(sky_bytes_t self, sky_object_t other);


/** Return the number of bytes in a bytes object.
  * This is the implementation of __len__().
  *
  * @param[in]  self        the bytes object for which the number of bytes it
  *                         contains is to be returned.
  * @return     the number of bytes in @a self.
  */
SKY_EXTERN ssize_t
sky_bytes_len(sky_bytes_t self);


/** Left justify a bytes object.
  *
  * @param[in]  self        the bytes object to be left justified.
  * @param[in]  width       the (minimum) width of the resulting left justified
  *                         bytes object.
  * @param[in]  fillchar    the character to use to fill the space between the
  *                         length of @a self and @a width. This should be a
  *                         bytes or bytearray object of length 1.
  * @return     a new bytes object that is @a self left justified, or @a self
  *             itself if it is longer than @a width.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_ljust(sky_bytes_t self, ssize_t width, sky_object_t fillchar);


/** Convert the case of all cased characters in a bytes object to uppercase.
  * Bytes are treated as ASCII characters.
  *
  * @param[in]  self        the bytes object for which the case of all cased
  *                         characters are to be converted to uppercase.
  * @return     a new bytes object with the case of all cased characters
  *             converted to uppercase.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_lower(sky_bytes_t self);


/** Strip leading characters from a bytes object.
  *
  * @param[in]  self        the bytes object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytes object that is @a self stripped of leading
  *             whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_lstrip(sky_bytes_t self, sky_object_t bytes);


/** Compare a bytes object with another object for less than.
  * This is the implementation of __lt__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_LESS).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_lt(sky_bytes_t self, sky_object_t other);


/** Make a translation table suitable for use with sky_bytes_translate().
  * The lengths of @a from and @a to must be the same. A new bytes object of
  * length 256 will be created with the bytes in @a from mapped one to one to
  * the bytes in @a to. If the same byte occurs in @a from more than once, the
  * last occurrence of it will be the one that's mapped.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_maketrans(sky_object_t from, sky_object_t to);


/** Multiply a bytes object.
  * This is the implementation of __mul__(), and is primarily a wrapper around
  * a call to sky_bytes_repeat().
  *
  * @param[in]  self        the bytes object to multiply.
  * @param[in]  other       a number object implementing __index__() that is
  *                         the number of times to multiply @a self.
  * @return     a new bytes object that is @a self multiplied the number of
  *             times specified by @a other.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_mul(sky_bytes_t self, sky_object_t other);


/** Compare a bytes object with another object for inequality.
  * This is the implementation of __ne__(self, other). It's simply implemented
  * as a call to
  * @ref sky_bytes_compare(@a self, @a other, @c SKY_COMPARE_OP_NOT_EQUAL).
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  other       the other object to compare with @a self, which may
  *                         be any object supporting the buffer protocol.
  * @return     @c sky_True, @c sky_False, or @c sky_NotImplemented if @a other
  *             does not implement the buffer protocol.
  */
SKY_EXTERN sky_object_t
sky_bytes_ne(sky_bytes_t self, sky_object_t other);


/** Partition a bytes object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with @a self and two empty bytes objects.
  *
  * @param[in]  self        the bytes object to partition.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             @a self and two empty bytes objects.
  */
SKY_EXTERN sky_tuple_t
sky_bytes_partition(sky_bytes_t self, sky_object_t sep);


/** Return a bytes object repeated a specified number of times.
  * If the repeat count is less than or equal to zero, the result will be the
  * empty bytes object. If the repeat count is one, the result will be the
  * original bytes object.
  *
  * @param[in]  self        the bytes object to be repeated.
  * @param[in]  count       the number of times to repeat @a bytes.
  * @return     a bytes object that is @a bytes repeated @a count times.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_repeat(sky_bytes_t self, ssize_t count);


/** Replace all non-overlapping occurrences of one byte sequence with another
  * one in a bytes object.
  *
  * @param[in]  self        the bytes object in which @a old_object is to be
  *                         replaced with @a new_object.
  * @param[in]  old_object  the byte sequence to replace, which may be any
  *                         object supporting the buffer protocol.
  * @param[in]  new_object  the byte sequence to replace @a old_object with,
  *                         which may be any object supporting the buffer
  *                         protocol.
  * @param[in]  count       if >= 0, the maximum number of occurrences of
  *                         @a old_object to replace; otherwise, there is no
  *                         limit.
  * @return     a new bytes object with @a count occurrences of @a old_object
  *             in @a self replaced by @a new_object.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_replace(sky_bytes_t   self,
                  sky_object_t  old_object,
                  sky_object_t  new_object,
                  ssize_t       count);


/** Return a string representation of a bytes object.
  * This is the implementation of __repr__().
  *
  * @param[in]  self        the bytes object for which a string representation
  *                         is to be returned.
  * @return     the string representation of @a self.
  */
SKY_EXTERN sky_string_t
sky_bytes_repr(sky_bytes_t self);


/** Find the starting offset of a run of bytes in a bytes object.
  * This function is the same as sky_bytes_find(), except that the search is
  * performed from the end of @a self.
  *
  * @param[in]  self        the bytes object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytes_rfind(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end);


/** Find the starting offset of a run of bytes in a bytes object.
  * This function is the same as sky_bytes_rfind(), except that
  * @c sky_IndexError will be raised if @a sub is not found instead of
  * returning -1.
  *
  * @param[in]  self        the bytes object to search.
  * @param[in]  sub         the run of bytes to search for, which must be
  *                         either an object supporting the buffer protocol or
  *                         a sequence of integers in range(0, 256).
  * @param[in]  start       the starting offset of @a self to search within.
  * @param[in]  end         the ending offset of @a self to search within.
  * @return     the offset where @a sub occurs in @a self[@a start:@a end] as
  *             an offset from the true start of @a self, or -1 if @a sub was
  *             not found.
  */
SKY_EXTERN ssize_t
sky_bytes_rindex(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end);


/** Right justify a bytes object.
  *
  * @param[in]  self        the bytes object to be right justified.
  * @param[in]  width       the (minimum) width of the resulting right
  *                         justified bytes object.
  * @param[in]  fillchar    the character to use to fill the space between the
  *                         length of @a self and @a width. This should be a
  *                         bytes or bytearray object of length 1.
  * @return     a new bytes object that is @a self right justified, or @a self
  *             itself if it is longer than @a width.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_rjust(sky_bytes_t self, ssize_t width, sky_object_t fillchar);


/** Partition a bytes object into three pieces by a separator.
  * If the specified separator, @a sep, is found in @a self, a 3-tuple is
  * returned with the slice before the separator, the separator itself, and
  * the slice after the separator. If the separator is not found, a 3-tuple is
  * returned with two empty bytes objects and @a self. This function works the
  * same as sky_bytes_partition(), except that the search for @a sep is done in
  * reverse.
  *
  * @param[in]  self        the bytes object to partition.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol.
  * @return     a 3-tuple containing three slices: the portion of @a self
  *             before @a sep, @a sep itself, and the portion of @a self after
  *             @a sep. If @a sep is not found, the 3-tuple is composed of
  *             two empty bytes objects and @a self.
  */
SKY_EXTERN sky_tuple_t
sky_bytes_rpartition(sky_bytes_t self, sky_object_t sep);


/** Split a bytes object by a separator.
  * This function is the same as sky_bytes_split(), except that it performs the
  * splits in reverse. If the number of splits is less than @a maxsplit, the
  * results from the two functions are identical. If no separator is specified,
  * splits will be done by runs of whitespace.
  *
  * @param[in]  self        the bytes object to split.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol. May also be @c NULL
  *                         (or @c sky_None) to perform splits by runs of
  *                         whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, b'123 456 789'.split()
  *             would yield [b'123', b'456', b'789'].
  */
SKY_EXTERN sky_list_t
sky_bytes_rsplit(sky_bytes_t self, sky_object_t sep, ssize_t maxsplit);


/** Strip trailing characters from a bytes object.
  *
  * @param[in]  self        the bytes object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytes object that is @a self stripped of trailing
  *             whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_rstrip(sky_bytes_t self, sky_object_t bytes);


/** Return the amount of memory a bytes object is consuming.
  * This is the implementation of __sizeof__().
  *
  * @param[in]  self        the bytes object for which the amount of memory is
  *                         returned.
  * @return     the amount of memory that @a self is consuming.
  */
SKY_EXTERN size_t
sky_bytes_sizeof(sky_bytes_t self);


/** Return a slice of a bytes object.
  *
  * @param[in]  self        the bytes object to slice.
  * @param[in]  start       the starting index of the slice (inclusive).
  * @param[in]  stop        the stopping index of the slice (exclusive).
  * @param[in]  step        the step of the slice.
  * @return     a new bytes object that is equivalent to self[start:stop:step].
  */
SKY_EXTERN sky_bytes_t
sky_bytes_slice(sky_bytes_t self,
                ssize_t     start,
                ssize_t     stop,
                ssize_t     step);


/** Split a bytes object by a separator.
  *
  * @param[in]  self        the bytes object to split.
  * @param[in]  sep         the separator to use, which may be any object that
  *                         supports the buffer protocol. May also be @c NULL
  *                         (or @c sky_None) to perform splits by runs of
  *                         whitespace.
  * @param[in]  maxsplit    the maximum number of splits to perform, with the
  *                         limit being unlimited if negative.
  * @return     a list of slices of @a self separated by @a sep, but @a sep is
  *             not included in the list. For example, b'123 456 789'.split()
  *             would yield [b'123', b'456', b'789'].
  */
SKY_EXTERN sky_list_t
sky_bytes_split(sky_bytes_t self, sky_object_t sep, ssize_t maxsplit);


/** Split a bytes object by line endings.
  * Line endings are '\\r' (carriage return), '\\n' (newline) , or '\\r\\n'
  * (crlf).
  *
  * @param[in]  self        the bytes object to split.
  * @param[in]  keepends    if @c SKY_TRUE, line endings will be included with
  *                         each line; otherwise, line endings will be stripped
  *                         from the results.
  * @return     a list object containing slices of @a self split by line
  *             endings.
  */
SKY_EXTERN sky_list_t
sky_bytes_splitlines(sky_bytes_t self, sky_bool_t keepends);


/** Determine whether a bytes object starts with a specified run of bytes.
  *
  * @param[in]  self        the bytes object to compare.
  * @param[in]  prefix      the prefix for comparison, which must be either an
  *                         object supporting the buffer protocol or a tuple of
  *                         objects supporting the buffer protocol.
  * @param[in]  start       the starting offset of @a self to compare.
  * @param[in]  end         the ending offset of @a self to compare.
  * @return     @c SKY_TRUE if @a self[@a start:@a end] startss with @a suffix,
  *             or @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_bytes_startswith(sky_bytes_t    self,
                     sky_object_t   prefix,
                     ssize_t        start,
                     ssize_t        end);


/** Strip leading and trailing trailing characters from a bytes object.
  *
  * @param[in]  self        the bytes object to strip.
  * @param[in]  bytes       the bytes to strip from @a self, which may be
  *                         @c NULL to strip whitespace.
  * @return     a new bytes object that is @a self stripped of leading and
  *             trailing whitespace or occurrences of @c bytes.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_strip(sky_bytes_t self, sky_object_t bytes);


/** Swap the case of all cased characters in a bytes object.
  * All uppercase characters are converted to lowercase and vice versa. Bytes
  * are treated as ASCII characters.
  *
  * @param[in]  self        the bytes object for which the case of all cased
  *                         characters are to be swapped.
  * @return     a new bytes object with the cases of all cased characters in
  *             @a self swapped.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_swapcase(sky_bytes_t self);


/** Convert a bytes object to be titlecased.
  * A titlecased string is one where a cased character following a non-cased
  * character is converted to uppercase, and all other cased characters are
  * converted to lowercase.
  *
  * @param[in]  self        the bytes object to convert to titlecase.
  * @return     a new bytes object that is @a self converted to titlecase.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_title(sky_bytes_t self);


/** Translate a bytes object using a translation table.
  * The function sky_bytes_maketrans() can be used to create a translation
  * table suitable for use with this function. The translation table is
  * required, and can be any object supporting the buffer protocol. The table
  * must have 256 entries, one for each byte value. If @a deletechars is
  * neither @c NULL nor @c sky_None, it must be an object supporting the buffer
  * protocol, and any byte that it contains will be removed from the result.
  *
  * @param[in]  self        the bytes object to translate.
  * @param[in]  table       the translation table to use, which must be any
  *                         object supporting the buffer protocol, and it must
  *                         be exactly 256 bytes.
  * @param[in]  deletechars an optional set of bytes to delete from @a self in
  *                         the resulting bytes object.
  * @return     a new bytes object that is @a self translated by @a table, and
  *             possible having bytes removed if @a deletechars is supplied and
  *             not empty.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_translate(sky_bytes_t     self,
                    sky_object_t    table,
                    sky_object_t    deletechars);


/** Convert the case of all cased characters in a bytes object to uppercase.
  * Bytes are treated as ASCII characters.
  *
  * @param[in]  self        the bytes object for which the case of all cased
  *                         characters are to be converted to uppercase.
  * @return     a new bytes object with the case of all cased characters
  *             converted to uppercase.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_upper(sky_bytes_t self);


/** Right justify a string with zeros, preserving the sign, if present.
  * This is really just a special purpose version of sky_bytes_rjust() using
  * a fillchar of '0', and keeping any sign ('-' or '+' that may be present at
  * the start of the resulting bytes object.
  *
  * @param[in]  self        the bytes object to be zero filled.
  * @param[in]  width       the width to make the resulting bytes object.
  * @return     a new bytes object that is @a self padded with '0' bytes to
  *             make it as wide as @a width.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_zfill(sky_bytes_t self, ssize_t width);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BYTES_H__ */

/** @} **/
/** @} **/
