#include "sky_private.h"
#include "sky_codec_private.h"
#include "sky_exceptions_private.h"
#include "sky_string_private.h"

#include <wchar.h>


static void
sky_codec_encode_exception(sky_codec_context_t *context,
                           ssize_t              start,
                           ssize_t              end,
                           const char *         reason)
{
    sky_tuple_t             args;
    sky_UnicodeError_data_t *error_data;

    if (!context->exception) {
        if (sky_object_isnull(context->encoding)) {
            sky_error_validate_debug(NULL != context->codec);
            context->encoding =
                    sky_string_createfrombytes(context->codec->name,
                                               strlen(context->codec->name),
                                               NULL,
                                               NULL);
        }
        args = sky_object_build("(OOuzuzs)",
                                context->encoding,
                                context->input,
                                start,
                                end,
                                reason);
        context->exception = sky_object_call(sky_UnicodeEncodeError,
                                             args,
                                             NULL);
        if (!sky_object_isa(context->exception, sky_UnicodeEncodeError)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%#@ expected; got %#@",
                    sky_type_name(sky_UnicodeEncodeError),
                    sky_type_name(sky_object_type(context->exception)));
        }
    }
    else {
        error_data = sky_UnicodeError_data(context->exception);
        error_data->start = start;
        error_data->end = end;
        sky_object_gc_set(
                &(error_data->reason),
                sky_string_createfrombytes(reason, strlen(reason), NULL, NULL),
                context->exception);
    }
}


static void
sky_codec_encode_error(sky_codec_context_t *context,
                       const void **        start,
                       const void *         end,
                       const char *         reason)
{
    ssize_t                     new_offset;
    sky_tuple_t                 tuple;
    sky_buffer_t                buffer;
    sky_object_t                code, error_handler, replacement;
    sky_codec_context_t         replacement_context;
    sky_codec_error_function_t  cfunction;

    sky_codec_encode_exception(
            context,
            ((const char *)*start - (const char *)context->bytes) / context->width,
            ((const char *)end - (const char *)context->bytes) / context->width,
            reason);

    error_handler = sky_codec_lookup_error(context->errors);
    if (sky_object_isa(error_handler, sky_function_type) &&
        (code = sky_function_code(error_handler)) != NULL &&
        sky_object_isa(code, sky_native_code_type))
    {
        cfunction = (sky_codec_error_function_t)sky_native_code_function(code);
        tuple = cfunction(context->exception);
    }
    else {
        tuple = sky_object_call(error_handler,
                                sky_tuple_pack(1, context->exception),
                                NULL);
    }
    if (!sky_object_isa(tuple, sky_tuple_type) || sky_tuple_len(tuple) != 2) {
        sky_error_raise_string(
                sky_TypeError,
                "encoding error handler must return (str/bytes, int) tuple");
    }
    if (!sky_object_isa(sky_tuple_get(tuple, 1), sky_integer_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "encoding error handler must return (str/bytes, int) tuple");
    }
    new_offset = sky_integer_value(sky_number_index(sky_tuple_get(tuple, 1)),
                                   SSIZE_MIN,
                                   SSIZE_MAX,
                                   NULL);

    replacement = sky_tuple_get(tuple, 0);
    if (sky_buffer_check(replacement)) {
        /* Pass the bytes data through to the encoder callback. */
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_acquire(&buffer, replacement, SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            context->callback(buffer.buf, buffer.len, 1, context->arg);
        } SKY_ASSET_BLOCK_END;
    }
    else if (sky_object_isa(replacement, sky_string_type)) {
        /* Pass the replacement string through to the encoder again, but make
         * sure that the errors are handled strictly.
         */
        memset(&replacement_context, 0, sizeof(replacement_context));
        replacement_context.input = replacement;
        replacement_context.encoding = context->encoding;
        replacement_context.mapping = context->mapping;
        replacement_context.callback = context->callback;
        replacement_context.arg = context->arg;
        replacement_context.flags = context->flags;
        sky_codec_encodewithcontext(&replacement_context);
    }
    else {
        sky_error_raise_string(
                sky_TypeError,
                "encoding error handler must return (str/bytes, int) tuple");
    }

    if (new_offset < 0) {
        new_offset = (context->nbytes / context->width) + new_offset;
    }
    if (new_offset < 0 ||
        new_offset > (ssize_t)(context->nbytes / context->width))
    {
        sky_error_raise_format(sky_IndexError,
                               "position %zd from error handler out of bounds",
                               new_offset);
    }

    *start = (const void *)((const char *)context->bytes +
                            (new_offset * context->width));
}


#define SKY_CODEC_ENCODE_WIDTH 1
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_BIG
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#define SKY_CODEC_ENCODE_ONCE
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_LITTLE
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#undef SKY_CODEC_ENCODE_WIDTH
#undef SKY_CODEC_ENCODE_ONCE

#define SKY_CODEC_ENCODE_WIDTH 2
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_BIG
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#define SKY_CODEC_ENCODE_ONCE
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_LITTLE
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#undef SKY_CODEC_ENCODE_WIDTH
#undef SKY_CODEC_ENCODE_ONCE

#define SKY_CODEC_ENCODE_WIDTH 4
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_BIG
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#define SKY_CODEC_ENCODE_ONCE
#define SKY_CODEC_ENCODE_ENDIAN SKY_ENDIAN_LITTLE
#include "templates/sky_codec_encode_t.c"
#undef SKY_CODEC_ENCODE_ENDIAN
#undef SKY_CODEC_ENCODE_WIDTH
#undef SKY_CODEC_ENCODE_ONCE


#define SKY_CODEC_ENCODE_FUNCTION(_name)                                    \
void                                                                        \
sky_codec_encode_##_name(sky_codec_context_t *context)                      \
{                                                                           \
    switch (context->width) {                                               \
        case 1:                                                             \
            sky_codec_encode_##_name##_latin1(context);                     \
            break;                                                          \
        case 2:                                                             \
            sky_codec_encode_##_name##_ucs2(context);                       \
            break;                                                          \
        case 4:                                                             \
            sky_codec_encode_##_name##_ucs4(context);                       \
            break;                                                          \
        default:                                                            \
            sky_error_fatal("internal error");                              \
    }                                                                       \
}

SKY_CODEC_ENCODE_FUNCTION(charmap);
SKY_CODEC_ENCODE_FUNCTION(locale);
SKY_CODEC_ENCODE_FUNCTION(unicode_escape);
SKY_CODEC_ENCODE_FUNCTION(utf7);

#undef SKY_CODEC_ENCODE_FUNCTION


void
sky_codec_encode_ascii(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            if (context->flags & SKY_CODEC_ENCODER_FLAG_ASCII) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  1,
                                  context->arg);
                break;
            }
            sky_codec_encode_ascii_latin1(context);
            break;
        case 2:
            sky_codec_encode_ascii_ucs2(context);
            break;
        case 4:
            sky_codec_encode_ascii_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_escape(sky_codec_context_t *context)
{
    const char * const hexdigits = sky_ctype_lower_hexdigits;

    char            escape[8], *outc;
    const uint8_t   *c, *end, *start;

    start = context->bytes;
    end = start + context->nbytes;
    escape[0] = '\\';
    outc = &(escape[1]);

    for (c = start; c < end; ++c) {
        if ('\'' == *c || '\\' == *c) {
            *outc++ = *c;
        }
        else if (*c < ' ' || *c >= 0x7F) {
            if ('\t' == *c) {
                *outc++ = 't';
            }
            else if ('\n' == *c) {
                *outc++ = 'n';
            }
            else if ('\r' == *c) {
                *outc++ = 'r';
            }
            else {
                *outc++ = 'x';
                *outc++ = hexdigits[(*c >> 4) & 0xF];
                *outc++ = hexdigits[(*c     ) & 0xF];
            }
        }
        else {
            continue;
        }

        if (c > start) {
            context->callback(start, c - start, 1, context->arg);
        }
        start = c + 1;
        context->callback(escape, outc - escape, 1, context->arg);
        outc = &(escape[1]);
    }
    if (c > start) {
        context->callback(start, c - start, 1, context->arg);
    }
}


void
sky_codec_encode_latin1(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            context->callback(context->bytes,
                              context->nbytes,
                              1,
                              context->arg);
            break;
        case 2:
            sky_codec_encode_latin1_ucs2(context);
            break;
        case 4:
            sky_codec_encode_latin1_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_raw_unicode_escape(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            context->callback(context->bytes,
                              context->nbytes,
                              1,
                              context->arg);
            break;
        case 2:
            sky_codec_encode_raw_unicode_escape_ucs2(context);
            break;
        case 4:
            sky_codec_encode_raw_unicode_escape_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_unicode_internal(sky_codec_context_t *context)
{
    sky_error_warn_string(sky_DeprecationWarning,
                          "unicode_internal codec has been deprecated");

    switch (context->width) {
        case 1:
            sky_codec_encode_unicode_internal_latin1(context);
            break;
        case 2:
            sky_codec_encode_unicode_internal_ucs2(context);
            break;
        case 4:
            sky_codec_encode_unicode_internal_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


static void
sky_codec_encode_unknown(sky_codec_context_t *context)
{
    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         argc;
        sky_tuple_t     args, tuple;
        sky_buffer_t    buffer;
        sky_object_t    encoder;

        encoder = sky_codec_encoder(context->encoding);
        argc = (sky_object_isnull(context->errors) ? 1 : 2);
        args = sky_tuple_pack(argc, context->input, context->errors);
        tuple = sky_object_call(encoder, args, NULL);
        if (!sky_object_isa(tuple, sky_tuple_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "encoder must return a tuple (object, integer)");
        }

        sky_buffer_acquire(&buffer,
                           sky_tuple_get(tuple, 0),
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        context->callback(buffer.buf, buffer.len, 1, context->arg);
    } SKY_ASSET_BLOCK_END;
}


void
sky_codec_encode_utf16_initialize(sky_codec_context_t *context)
{
    context->flags |= SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM;
}


void
sky_codec_encode_utf16_be(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            sky_codec_encode_utf16_be_latin1(context);
            break;
        case 2:
#if SKY_ENDIAN == SKY_ENDIAN_BIG
            if (!(context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM)) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  2,
                                  context->arg);
                break;
            }
#endif
            sky_codec_encode_utf16_be_ucs2(context);
            break;
        case 4:
            sky_codec_encode_utf16_be_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_utf16_le(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            sky_codec_encode_utf16_le_latin1(context);
            break;
        case 2:
#if SKY_ENDIAN == SKY_ENDIAN_LITTLE
            if (!(context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM)) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  2,
                                  context->arg);
                break;
            }
#endif
            sky_codec_encode_utf16_le_ucs2(context);
            break;
        case 4:
            sky_codec_encode_utf16_le_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_utf32_initialize(sky_codec_context_t *context)
{
    context->flags |= SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM;
}


void
sky_codec_encode_utf32_be(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            sky_codec_encode_utf32_be_latin1(context);
            break;
        case 2:
            sky_codec_encode_utf32_be_ucs2(context);
            break;
        case 4:
#if SKY_ENDIAN == SKY_ENDIAN_BIG
            if (!(context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM)) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  4,
                                  context->arg);
                break;
            }
#endif
            sky_codec_encode_utf32_be_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_utf32_le(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            sky_codec_encode_utf32_le_latin1(context);
            break;
        case 2:
            sky_codec_encode_utf32_le_ucs2(context);
            break;
        case 4:
#if SKY_ENDIAN == SKY_ENDIAN_LITTLE
            if (!(context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM)) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  4,
                                  context->arg);
                break;
            }
#endif
            sky_codec_encode_utf32_le_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


void
sky_codec_encode_utf8(sky_codec_context_t *context)
{
    switch (context->width) {
        case 1:
            if (context->flags & SKY_CODEC_ENCODER_FLAG_ASCII) {
                context->callback(context->bytes,
                                  context->nbytes,
                                  1,
                                  context->arg);
                break;
            }
            sky_codec_encode_utf8_latin1(context);
            break;
        case 2:
            sky_codec_encode_utf8_ucs2(context);
            break;
        case 4:
            sky_codec_encode_utf8_ucs4(context);
            break;
        default:
            sky_error_fatal("internal error");
    }
}


typedef struct sky_codec_encode_buffer_s {
    void *                              bytes;
    size_t                              nbytes_size;
    ssize_t                             nbytes_used;
    sky_bool_t                          resizable;
    sky_bool_t                          terminate;
} sky_codec_encode_buffer_t;

static void
sky_codec_encode_bytes(const void * bytes,
                       ssize_t      nbytes,
            SKY_UNUSED size_t       width,
                       void *       arg)
{
    sky_codec_encode_buffer_t   *buffer = arg;

    size_t  needed;

    needed = (size_t)buffer->nbytes_used + (size_t)nbytes;
    if (buffer->terminate) {
        ++needed;
    }
    if (buffer->resizable) {
        if (needed > buffer->nbytes_size) {
            buffer->bytes = sky_asset_realloc(buffer->bytes,
                                              needed,
                                              SKY_ASSET_CLEANUP_ON_ERROR,
                                              SKY_ASSET_UPDATE_FIRST_CURRENT);
        }
        buffer->nbytes_size = sky_memsize(buffer->bytes);
    }
    else if (needed > buffer->nbytes_size) {
        nbytes = needed - buffer->nbytes_size;
        memcpy((char *)buffer->bytes + buffer->nbytes_used, bytes, nbytes);
        buffer->nbytes_used += nbytes;
        sky_error_raise_string(sky_OverflowError, "buffer too small");
    }
    memcpy((char *)buffer->bytes + buffer->nbytes_used, bytes, nbytes);
    buffer->nbytes_used += nbytes;
    if (buffer->terminate) {
        ((char *)buffer->bytes)[buffer->nbytes_used] = 0;
    }
}


sky_object_t
sky_codec_encode_prepareinput(const sky_codec_t *codec, sky_object_t input)
{
    if (!codec) {
        return input;
    }

    if ((codec->encode_flags & SKY_CODEC_ENCODE_FLAG_DECODE_BUFFER_INPUT) &&
        !sky_object_isa(input, sky_string_type))
    {
        return sky_codec_decode(sky_codec_utf8, input, NULL, NULL, NULL);
    }

    if (codec->encode_flags & SKY_CODEC_ENCODE_FLAG_BUFFER_INPUT_REQUIRED) {
        if (!sky_buffer_check(input)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "input does not support the buffer protocol");
        }
    }
    else {
        input = sky_string_createfromobject(input);
    }

    return input;
}


void
sky_codec_encodewithcontext(sky_codec_context_t *context)
{
    const sky_codec_t   *codec = context->codec;

    sky_buffer_t        buffer;
    sky_string_data_t   *string_data, tagged_string_data;

    if (!context->bytes) {
        context->input = sky_codec_encode_prepareinput(context->codec,
                                                       context->input);
    }
    if (!codec) {
        sky_codec_encode_unknown(context);
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!context->bytes) {
            if (codec->encode_flags &
                    SKY_CODEC_ENCODE_FLAG_BUFFER_INPUT_REQUIRED)
            {
                sky_buffer_acquire(&buffer,
                                   context->input,
                                   SKY_BUFFER_FLAG_SIMPLE);
                sky_asset_save(&buffer,
                               (sky_free_t)sky_buffer_release,
                               SKY_ASSET_CLEANUP_ALWAYS);
                context->bytes = buffer.buf;
                context->nbytes = buffer.len;
                context->width = 1;
            }
            else {
                context->flags &= ~SKY_CODEC_ENCODER_FLAG_ASCII;
                context->input = sky_string_createfromobject(context->input);
                if (!sky_object_istagged(context->input)) {
                    string_data = sky_string_data_noctype(context->input, NULL);
                }
                else {
                    string_data = sky_string_data_noctype(context->input,
                                                          &tagged_string_data);
                }

                if (string_data->highest_codepoint < 0x80) {
                    context->flags |= SKY_CODEC_ENCODER_FLAG_ASCII;
                    context->bytes = string_data->data.ascii;
                    context->width = 1;
                }
                else if (string_data->highest_codepoint < 0x100) {
                    context->bytes = string_data->data.latin1;
                    context->width = 1;
                }
                else if (string_data->highest_codepoint < 0x10000) {
                    context->bytes = string_data->data.ucs2;
                    context->width = 2;
                }
                else {
                    context->bytes = string_data->data.ucs4;
                    context->width = 4;
                }
                context->nbytes = string_data->len * context->width;
            }
        }

        if (codec->encode_initialize) {
            codec->encode_initialize(context);
        }
        if (codec->encode_finalize) {
            sky_asset_save(context,
                           (sky_free_t)codec->encode_finalize,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (context->nbytes) {
            codec->encode_update(context);
        }
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_codec_encode(const sky_codec_t *codec,
                 sky_object_t       input,
                 sky_string_t       encoding,
                 sky_string_t       errors,
                 uint32_t           flags)
{
    sky_object_t                object;
    sky_codec_context_t         context;
    sky_codec_encode_buffer_t   buffer;

    memset(&context, 0, sizeof(context));
    context.codec = (codec ? codec : sky_codec_builtin(encoding));
    context.input = input;
    context.encoding = (codec ? NULL : encoding);
    context.errors = errors;
    context.callback = sky_codec_encode_bytes;
    context.arg = &buffer;
    context.flags = flags;

    buffer.bytes = NULL;
    buffer.nbytes_used = 0;
    buffer.nbytes_size = 0;
    buffer.resizable = SKY_TRUE;
    buffer.terminate = SKY_TRUE;

    sky_codec_encodewithcontext(&context);

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(buffer.bytes, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
        object = sky_bytes_createwithbytes(buffer.bytes,
                                           buffer.nbytes_used,
                                           sky_free);
    } SKY_ASSET_BLOCK_END;

    return object;
}


ssize_t
sky_codec_encodeintocbytes(const sky_codec_t *  codec,
                           sky_object_t         input,
                           sky_string_t         encoding,
                           sky_string_t         errors,
                           uint32_t             flags,
                           void *               bytes,
                           ssize_t              nbytes)
{
    sky_codec_context_t         context;
    sky_codec_encode_buffer_t   buffer;

    memset(&context, 0, sizeof(context));
    context.codec = (codec ? codec : sky_codec_builtin(encoding));
    context.input = input;
    context.encoding = (codec ? NULL : encoding);
    context.errors = errors;
    context.callback = sky_codec_encode_bytes;
    context.arg = &buffer;
    context.flags = flags;

    buffer.bytes = bytes;
    buffer.nbytes_used = 0;
    buffer.nbytes_size = nbytes;
    buffer.resizable = SKY_FALSE;
    buffer.terminate = SKY_FALSE;

    sky_codec_encodewithcontext(&context);
    return buffer.nbytes_used;
}


uint8_t *
sky_codec_encodetocbytes(const sky_codec_t *codec,
                         sky_object_t       input,
                         sky_string_t       encoding,
                         sky_string_t       errors,
                         uint32_t           flags,
                         ssize_t *          nbytes)
{
    sky_codec_context_t         context;
    sky_codec_encode_buffer_t   buffer;

    memset(&context, 0, sizeof(context));
    context.codec = (codec ? codec : sky_codec_builtin(encoding));
    context.input = input;
    context.encoding = (codec ? NULL : encoding);
    context.errors = errors;
    context.callback = sky_codec_encode_bytes;
    context.arg = &buffer;
    context.flags = flags;

    buffer.bytes = NULL;
    buffer.nbytes_used = 0;
    buffer.nbytes_size = 0;
    buffer.resizable = SKY_TRUE;
    buffer.terminate = SKY_FALSE;

    sky_codec_encodewithcontext(&context);
    *nbytes = buffer.nbytes_used;
    return buffer.bytes;
}


char *
sky_codec_encodetocstring(sky_object_t  input,
                          uint32_t      flags)
{
    sky_codec_context_t         context;
    sky_codec_encode_buffer_t   buffer;

    memset(&context, 0, sizeof(context));
    context.codec = sky_codec_utf8;
    context.input = input;
    context.callback = sky_codec_encode_bytes;
    context.arg = &buffer;
    context.flags = flags;

    buffer.bytes = NULL;
    buffer.nbytes_size = 0;
    buffer.nbytes_used = 0;
    buffer.resizable = SKY_TRUE;
    buffer.terminate = SKY_TRUE;

    sky_codec_encodewithcontext(&context);
    return buffer.bytes;
}


void
sky_codec_encodewithcallback(const sky_codec_t *codec,
                             sky_object_t       input,
                             sky_string_t       encoding,
                             sky_string_t       errors,
                             uint32_t           flags,
                             sky_codec_output_t callback,
                             void *             arg)
{
    sky_codec_context_t context;

    memset(&context, 0, sizeof(context));
    context.codec = (codec ? codec : sky_codec_builtin(encoding));
    context.input = input;
    context.encoding = (codec ? NULL : encoding);
    context.errors = errors;
    context.callback = callback;
    context.arg = arg;
    context.flags = flags;

    sky_codec_encodewithcontext(&context);
}


sky_bytes_t
sky_codec_charmap_encode(sky_object_t   input,
                         sky_string_t   errors,
                         sky_object_t   mapping)
{
    sky_bytes_t                 bytes;
    sky_codec_context_t         context;
    sky_codec_encode_buffer_t   buffer;

    memset(&context, 0, sizeof(context));
    context.codec = (mapping ? sky_codec_charmap : sky_codec_latin1);
    context.input = input;
    context.errors = errors;
    context.mapping = mapping;
    context.callback = sky_codec_encode_bytes;
    context.arg = &buffer;

    buffer.bytes = NULL;
    buffer.nbytes_size = 0;
    buffer.nbytes_used = 0;
    buffer.resizable = SKY_TRUE;
    buffer.terminate = SKY_TRUE;

    sky_codec_encodewithcontext(&context);

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(buffer.bytes, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
        bytes = sky_bytes_createwithbytes(buffer.bytes,
                                          buffer.nbytes_used,
                                          sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}


void
sky_codec_charmap_encodewithcallback(sky_object_t       input,
                                     sky_string_t       errors,
                                     sky_object_t       mapping,
                                     sky_codec_output_t callback,
                                     void *             arg)
{
    sky_codec_context_t context;

    memset(&context, 0, sizeof(context));
    context.codec = (mapping ? sky_codec_charmap : sky_codec_latin1);
    context.input = input;
    context.errors = errors;
    context.mapping = mapping;
    context.callback = callback;
    context.arg = arg;

    sky_codec_encodewithcontext(&context);
}


sky_bytes_t
sky_codec_encodefilename(sky_string_t input)
{
    return sky_codec_encode(sky_codec_filesystemencoding_codec,
                            input,
                            sky_codec_filesystemencoding_string,
                            sky_codec_filesystemerrors_string,
                            0);
}
