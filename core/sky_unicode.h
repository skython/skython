/** @file
  * @brief
  * @defgroup sky_unicode Unicode
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_UNICODE_H__
#define __SKYTHON_CORE_SKY_UNICODE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** The major component of the Unicode standard version to which the
  * Skython implementation conforms.
  */
#define SKY_UNICODE_VERSION_MAJOR       6
/** The minor component of the Unicode standard version to which the
  * Skython implementation conforms.
  */
#define SKY_UNICODE_VERSION_MINOR       1
/** The update component of the Unicode standard version to which the
  * Skython implementation conforms.
  */
#define SKY_UNICODE_VERSION_UPDATE      0
/** The Unicode standard version as a string. **/
#define SKY_UNICODE_VERSION_STRING      "6.1.0"


/** An integral type representing a single Unicode character or code point. **/
typedef int32_t sky_unicode_char_t;

/** The minimum legal value for a @c sky_unicode_char_t typed value. **/
#define SKY_UNICODE_CHAR_MIN            0

/** The maximum legal value for a @c sky_unicode_char_t typed value. **/
#define SKY_UNICODE_CHAR_MAX            0x10FFFF


/** Test a Unicode character to determine whether or not it's alphanumeric. **/
SKY_EXTERN sky_bool_t
sky_unicode_isalnum(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's alphabetic. **/
SKY_EXTERN sky_bool_t
sky_unicode_isalpha(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's a decimal
  * character as defined by the Unicode standard (Chapter 4).
  */
SKY_EXTERN sky_bool_t
sky_unicode_isdecimal(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's a digit character
  * as defined by the Unicode standard (Chapter 4).
  */
SKY_EXTERN sky_bool_t
sky_unicode_isdigit(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not its value is
  * fractional.
  */
SKY_EXTERN sky_bool_t
sky_unicode_isfractional(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not its value is
  * integral.
  */
SKY_EXTERN sky_bool_t
sky_unicode_isintegral(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's a linebreak. **/
SKY_EXTERN sky_bool_t
sky_unicode_islinebreak(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's lowercase. **/
SKY_EXTERN sky_bool_t
sky_unicode_islower(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's numeric. **/
SKY_EXTERN sky_bool_t
sky_unicode_isnumeric(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's printable. **/
SKY_EXTERN sky_bool_t
sky_unicode_isprint(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's whitespace. **/
SKY_EXTERN sky_bool_t
sky_unicode_isspace(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's titlecase. **/
SKY_EXTERN sky_bool_t
sky_unicode_istitle(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's uppercase. **/
SKY_EXTERN sky_bool_t
sky_unicode_isupper(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's an identifier continuer. **/
SKY_EXTERN sky_bool_t
sky_unicode_isxid_continue(sky_unicode_char_t c);

/** Test a Unicode character to determine whether or not it's an identifier starter. **/
SKY_EXTERN sky_bool_t
sky_unicode_isxid_start(sky_unicode_char_t c);

/** Return the fractional value of a Unicode character. **/
SKY_EXTERN double
sky_unicode_fraction(sky_unicode_char_t c);

/** Return the integral value of a Unicode character. **/
SKY_EXTERN int64_t
sky_unicode_integer(sky_unicode_char_t c);

/** Return the simple lowercase equivalent of a Unicode character. **/
SKY_EXTERN sky_unicode_char_t
sky_unicode_tolower(sky_unicode_char_t c);

/** Return the simple titlecase equivalent of a Unicode character. **/
SKY_EXTERN sky_unicode_char_t
sky_unicode_totitle(sky_unicode_char_t c);

/** Return the simple uppercase equivalent of a Unicode character. **/
SKY_EXTERN sky_unicode_char_t
sky_unicode_toupper(sky_unicode_char_t c);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_UNICODE_H__ */

/** @} **/
