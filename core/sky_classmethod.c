#include "sky_private.h"


typedef struct sky_classmethod_data_s {
    sky_object_t                        function;
} sky_classmethod_data_t;

SKY_EXTERN_INLINE sky_classmethod_data_t *
sky_classmethod_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_classmethod_type) {
        return (sky_classmethod_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_classmethod_type);
}


static void
sky_classmethod_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_classmethod_data_t  *method_data = data;

    sky_object_visit(method_data->function, visit_data);
}


static sky_object_t
sky_classmethod_isabstractmethod_getter(
                    sky_object_t    instance,
        SKY_UNUSED  sky_type_t      type)
{
    sky_object_t    function = sky_classmethod_data(instance)->function;

    sky_object_t    isabstract;

    isabstract = sky_object_getattr(function,
                                    SKY_STRING_LITERAL("__isabstractmethod__"),
                                    sky_False);
    return (sky_object_bool(isabstract) ? sky_True : sky_False);
}


sky_object_t
sky_classmethod_get(sky_object_t self, sky_object_t instance, sky_type_t owner)
{
    return sky_method_create(sky_classmethod_data(self)->function,
                             (owner ? owner : sky_object_type(instance)));
}


void
sky_classmethod_init(sky_object_t self, sky_object_t function)
{
    sky_classmethod_data_t  *method_data = sky_classmethod_data(self);

    sky_object_gc_set(&(method_data->function), function, self);
}


sky_object_t
sky_classmethod_function(sky_classmethod_t self)
{
    return sky_classmethod_data(self)->function;
}


sky_classmethod_t
sky_classmethod_create(sky_object_t function)
{
    sky_classmethod_t       classmethod;
    sky_classmethod_data_t  *method_data;

    classmethod = sky_object_allocate(sky_classmethod_type);
    method_data = sky_classmethod_data(classmethod);
    sky_object_gc_set(&(method_data->function), function, classmethod);

    return classmethod;
}


static const char sky_classmethod_type_doc[] =
"classmethod(function) -> method\n"
"\n"
"Convert a function to be a class method.\n"
"\n"
"A class method receives the class as implicit first argument,\n"
"just like an instance method receives the instance.\n"
"To declare a class method, use this idiom:\n"
"\n"
"  class C:\n"
"      def f(cls, arg1, arg2, ...): ...\n"
"      f = classmethod(f)\n"
"\n"
"It can be called either on the class (e.g. C.f()) or on an instance\n"
"(e.g. C().f()). The instanec is ignored except for its class.\n"
"If a class method is called for a derived class, the derived class\n"
"object is passed as the implied first argument.\n"
"\n"
"Class methods are different than C++ or Java static methods.\n"
"If you want those, see the staticmethod builtin.";


SKY_TYPE_DEFINE_SIMPLE(classmethod,
                       "classmethod",
                       sizeof(sky_classmethod_data_t),
                       NULL,
                       NULL,
                       sky_classmethod_instance_visit,
                       SKY_TYPE_FLAG_HAS_DICT,
                       sky_classmethod_type_doc);


void
sky_classmethod_initialize_library(void)
{
    sky_type_setattr_member(
            sky_classmethod_type,
            "__func__",
            NULL,
            offsetof(sky_classmethod_data_t, function),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_READONLY);
    sky_type_setattr_getset(
            sky_classmethod_type,
            "__isabstractmethod__",
            NULL,
            sky_classmethod_isabstractmethod_getter,
            NULL);

    sky_type_setmethodslotwithparameters(
            sky_classmethod_type,
            "__init__",
            (sky_native_code_function_t)sky_classmethod_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "function", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_classmethod_type,
            "__get__", sky_classmethod_get,
            NULL);
}
