#include "sky_private.h"


SKY_TYPE_DEFINE_SIMPLE(None,
                       "NoneType",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

sky_None_t const sky_None = SKY_OBJECT_TAGGED_NONE;


sky_bool_t
sky_None_bool(SKY_UNUSED sky_object_t self)
{
    return SKY_FALSE;
}


sky_object_t
sky_None_new(SKY_UNUSED sky_type_t type, sky_tuple_t args, sky_dict_t kws)
{
    if ((args && sky_object_len(args)) || (kws && sky_object_len(kws))) {
        sky_error_raise_string(sky_TypeError,
                               "NoneType takes no arguments");
    }
    return sky_None;
}


sky_string_t
sky_None_repr(SKY_UNUSED sky_object_t self)
{
    return SKY_STRING_LITERAL("None");
}


void
sky_None_initialize_library(void)
{
    sky_type_setmethodslots(sky_None_type,
            "__new__", sky_None_new,
            "__repr__", sky_None_repr,
            "__bool__", sky_None_bool,
            NULL);
}
