#include "sky_private.h"


typedef struct sky_cell_data_s {
    sky_object_t                        cell_contents;
} sky_cell_data_t;

SKY_EXTERN_INLINE sky_cell_data_t *
sky_cell_data(sky_object_t object)
{
    if (sky_cell_type == sky_object_type(object)) {
        return (sky_cell_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_cell_type);
}

struct sky_cell_s {
    sky_object_data_t                   object_data;
    sky_cell_data_t                     cell_data;
};


static void
sky_cell_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_cell_data_t *self_data = data;

    sky_object_visit(self_data->cell_contents, visit_data);
}


sky_object_t
sky_cell_compare(sky_cell_t         self,
                 sky_object_t       other,
                 sky_compare_op_t   compare_op)
{
    sky_bool_t      result;
    sky_cell_data_t *other_data, *self_data;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_cell_type)) {
        return sky_NotImplemented;
    }

    self_data = sky_cell_data(self);
    other_data = sky_cell_data(other);
    if (self_data->cell_contents && other_data->cell_contents) {
        return (sky_object_compare(self_data->cell_contents,
                                   other_data->cell_contents,
                                   compare_op) ? sky_True : sky_False);
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            result = (self_data->cell_contents == other_data->cell_contents);
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            result = (self_data->cell_contents != other_data->cell_contents);
            break;
        case SKY_COMPARE_OP_GREATER:
            result = (self_data->cell_contents > other_data->cell_contents);
            break;
        case SKY_COMPARE_OP_GREATER_EQUAL:
            result = (self_data->cell_contents >= other_data->cell_contents);
            break;
        case SKY_COMPARE_OP_LESS:
            result = (self_data->cell_contents < other_data->cell_contents);
            break;
        case SKY_COMPARE_OP_LESS_EQUAL:
            result = (self_data->cell_contents <= other_data->cell_contents);
            break;

        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            return sky_NotImplemented;
        case SKY_COMPARE_OP_IS:
            result = (self == other);
            break;
        case SKY_COMPARE_OP_IS_NOT:
            result = (self != other);
            break;
    }

    return (result ? sky_True : sky_False);
}


static sky_object_t
sky_cell_cell_contents_getter(sky_cell_t self, SKY_UNUSED sky_type_t type)
{
    sky_cell_data_t *self_data = sky_cell_data(self);

    if (!self_data->cell_contents) {
        sky_error_raise_string(sky_ValueError, "Cell is empty");
    }
    return self_data->cell_contents;
}


sky_object_t
sky_cell_contents(sky_cell_t self)
{
    return sky_cell_data(self)->cell_contents;
}


sky_cell_t
sky_cell_create(sky_object_t object)
{
    sky_cell_t      cell;
    sky_cell_data_t *cell_data;

    cell = sky_object_allocate(sky_cell_type);
    cell_data = sky_cell_data(cell);
    sky_object_gc_set(&(cell_data->cell_contents), object, cell);

    return cell;
}


sky_object_t
sky_cell_eq(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_object_t
sky_cell_ge(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_cell_gt(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_GREATER);
}


sky_object_t
sky_cell_le(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


sky_object_t
sky_cell_lt(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_cell_ne(sky_cell_t self, sky_object_t other)
{
    return sky_cell_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_string_t
sky_cell_repr(sky_cell_t self)
{
    sky_cell_data_t *self_data = sky_cell_data(self);

    if (!self_data->cell_contents) {
        return sky_string_createfromformat(
                        "<%@ at %p>",
                        sky_type_name(sky_object_type(self)),
                        self);
    }
    return sky_string_createfromformat(
                    "<%@ at %p: %@ object at %p>",
                    sky_type_name(sky_object_type(self)),
                    self,
                    sky_type_name(sky_object_type(self_data->cell_contents)),
                    self_data->cell_contents);
}


void
sky_cell_setcontents(sky_cell_t cell, sky_object_t object)
{
    sky_object_gc_set(&(sky_cell_data(cell)->cell_contents), object, cell);
}


SKY_TYPE_DEFINE_SIMPLE(cell,
                       "cell",
                       sizeof(sky_cell_data_t),
                       NULL,
                       NULL,
                       sky_cell_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_cell_initialize_library(void)
{
    sky_type_initialize_builtin(sky_cell_type, 0);

    sky_type_setattr_getset(sky_cell_type,
            "cell_contents",
            NULL,
            (sky_getset_get_t)sky_cell_cell_contents_getter,
            NULL);

    sky_type_setattr_builtin(sky_cell_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_cell_type,
            "__repr__", sky_cell_repr,
            "__lt__", sky_cell_lt,
            "__le__", sky_cell_le,
            "__eq__", sky_cell_eq,
            "__ne__", sky_cell_ne,
            "__gt__", sky_cell_gt,
            "__ge__", sky_cell_ge,
            NULL);
}
