#include "sky_private.h"


void
sky_asset_save(void *               asset_pointer,
               sky_free_t           asset_free,
               sky_asset_cleanup_t  asset_cleanup)
{
    sky_tlsdata_t       *tlsdata;
    sky_error_asset_t   *asset;

    sky_error_validate_debug(asset_free != NULL);
    sky_error_validate_debug(asset_cleanup == SKY_ASSET_CLEANUP_NEVER ||
                             asset_cleanup == SKY_ASSET_CLEANUP_ALWAYS ||
                             asset_cleanup == SKY_ASSET_CLEANUP_ON_ERROR ||
                             asset_cleanup == SKY_ASSET_CLEANUP_ON_SUCCESS);

    if (asset_cleanup != SKY_ASSET_CLEANUP_NEVER) {
        tlsdata = sky_tlsdata_get();
        sky_error_validate(tlsdata->error_blocks != NULL);

        if (!(asset = tlsdata->error_asset_freelist)) {
            asset = sky_malloc(sizeof(sky_error_asset_t));
        }
        else {
            tlsdata->error_asset_freelist = asset->next;
        }
        asset->asset_pointer = asset_pointer;
        asset->asset_free    = asset_free;
        asset->asset_cleanup = asset_cleanup;
        asset->next          = tlsdata->error_blocks->assets;
        tlsdata->error_blocks->assets = asset;
    }
}


void
sky_asset_update(void *             old_asset_pointer,
                 void *             new_asset_pointer,
                 sky_asset_update_t update_mode)
{
    sky_tlsdata_t       *tlsdata;
    sky_error_asset_t   *asset;
    sky_error_block_t   *block;

    tlsdata = sky_tlsdata_get();
    switch (update_mode) {
        case SKY_ASSET_UPDATE_FIRST_CURRENT:
            if ((block = tlsdata->error_blocks) != NULL) {
                for (asset = block->assets; asset; asset = asset->next) {
                    if (asset->asset_pointer == old_asset_pointer) {
                        asset->asset_pointer = new_asset_pointer;
                        return;
                    }
                }
            }
            return;
        case SKY_ASSET_UPDATE_FIRST_DEEP:
            for (block = tlsdata->error_blocks; block; block = block->next) {
                for (asset = block->assets; asset; asset = asset->next) {
                    if (asset->asset_pointer == old_asset_pointer) {
                        asset->asset_pointer = new_asset_pointer;
                        return;
                    }
                }
            }
            return;
        case SKY_ASSET_UPDATE_ALL_CURRENT:
            if ((block = tlsdata->error_blocks) != NULL) {
                for (asset = block->assets; asset; asset = asset->next) {
                    if (asset->asset_pointer == old_asset_pointer) {
                        asset->asset_pointer = new_asset_pointer;
                    }
                }
            }
            return;
        case SKY_ASSET_UPDATE_ALL_DEEP:
            for (block = tlsdata->error_blocks; block; block = block->next) {
                for (asset = block->assets; asset; asset = asset->next) {
                    if (asset->asset_pointer == old_asset_pointer) {
                        asset->asset_pointer = new_asset_pointer;
                    }
                }
            }
            return;
    }

    sky_error_fatal("illegal update_mode for sky_error_asset_update()");
}


void *
sky_asset_calloc(size_t count, size_t nbytes, sky_asset_cleanup_t cleanup)
{
    void    *pointer;

    pointer = sky_calloc(count, nbytes);
    sky_asset_save(pointer, sky_free, cleanup);

    return pointer;
}


void *
sky_asset_malloc(size_t nbytes, sky_asset_cleanup_t cleanup)
{
    void    *pointer;

    pointer = sky_malloc(nbytes);
    sky_asset_save(pointer, sky_free, cleanup);

    return pointer;
}


void *
sky_asset_memalign(size_t alignment, size_t nbytes, sky_asset_cleanup_t cleanup)
{
    void    *pointer;

    pointer = sky_memalign(alignment, nbytes);
    sky_asset_save(pointer, sky_free, cleanup);

    return pointer;
}


void *
sky_asset_memdup(const void *bytes, size_t nbytes, sky_asset_cleanup_t cleanup)
{
    void    *new_bytes;

    if (likely((new_bytes = sky_memdup(bytes, nbytes)) != NULL)) {
        sky_asset_save(new_bytes, sky_free, cleanup);
    }

    return new_bytes;
}


void *
sky_asset_realloc(void *old_pointer, size_t new_nbytes, sky_asset_cleanup_t cleanup, sky_asset_update_t update)
{
    void    *new_pointer;

    if (!old_pointer) {
        new_pointer = sky_malloc(new_nbytes);
        sky_asset_save(new_pointer, sky_free, cleanup);
    }
    else if ((new_pointer = sky_realloc(old_pointer, new_nbytes)) != NULL) {
        sky_asset_update(old_pointer, new_pointer, update);
    }

    return new_pointer;
}


char *
sky_asset_strdup(const char *string, sky_asset_cleanup_t cleanup)
{
    char    *new_string;

    if (likely((new_string = sky_strdup(string)) != NULL)) {
        sky_asset_save(new_string, sky_free, cleanup);
    }

    return new_string;
}


char *
sky_asset_strndup(const char *string, size_t string_length, sky_asset_cleanup_t cleanup)
{
    char    *new_string;

    if (likely((new_string = sky_strndup(string, string_length)) != NULL)) {
        sky_asset_save(new_string, sky_free, cleanup);
    }

    return new_string;
}
