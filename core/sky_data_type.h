/** @file
  * @brief
  * @defgroup sky_data_type Data Types
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_DATA_TYPE_H__
#define __SKYTHON_CORE_SKY_DATA_TYPE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Constants representing various primitive data types. **/
typedef enum sky_data_type_e {
    SKY_DATA_TYPE_VOID              =   0,

    SKY_DATA_TYPE_FLOAT             =   1,
    SKY_DATA_TYPE_DOUBLE            =   2,

    SKY_DATA_TYPE_INT               =   3,
    SKY_DATA_TYPE_UINT              =   4,
    SKY_DATA_TYPE_CHAR              =   5,
    SKY_DATA_TYPE_UCHAR             =   6,
    SKY_DATA_TYPE_SHORT             =   7,
    SKY_DATA_TYPE_USHORT            =   8,
    SKY_DATA_TYPE_LONG              =   9,
    SKY_DATA_TYPE_ULONG             =   10,
    SKY_DATA_TYPE_LONG_LONG         =   11,
    SKY_DATA_TYPE_ULONG_LONG        =   12,
    SKY_DATA_TYPE_INT8              =   13,
    SKY_DATA_TYPE_UINT8             =   14,
    SKY_DATA_TYPE_INT16             =   15,
    SKY_DATA_TYPE_UINT16            =   16,
    SKY_DATA_TYPE_INT32             =   17,
    SKY_DATA_TYPE_UINT32            =   18,
    SKY_DATA_TYPE_INT64             =   19,
    SKY_DATA_TYPE_UINT64            =   20,

    SKY_DATA_TYPE_BOOL              =   21,
    SKY_DATA_TYPE_SSIZE_T           =   22,
    SKY_DATA_TYPE_SIZE_T            =   23,
    SKY_DATA_TYPE_INTMAX_T          =   24,
    SKY_DATA_TYPE_UINTMAX_T         =   25,
    SKY_DATA_TYPE_PTRDIFF_T         =   26,
    SKY_DATA_TYPE_UPTRDIFF_T        =   27,
    SKY_DATA_TYPE_INTPTR_T          =   28,
    SKY_DATA_TYPE_UINTPTR_T         =   29,

    SKY_DATA_TYPE_VOID_PTR          =   30,
    SKY_DATA_TYPE_STRING            =   31,

    /* Always ssize_t, but use __index__() instead of __int__() for coercion */
    SKY_DATA_TYPE_INDEX             =   32,

    SKY_DATA_TYPE_GID_T             =   33,
    SKY_DATA_TYPE_UID_T             =   34,
    SKY_DATA_TYPE_PID_T             =   35,
    SKY_DATA_TYPE_MODE_T            =   36,
    SKY_DATA_TYPE_OFF_T             =   37,
    SKY_DATA_TYPE_DEV_T             =   38,
    SKY_DATA_TYPE_FD                =   39,
    SKY_DATA_TYPE_FD2               =   40,
    SKY_DATA_TYPE_CODEPOINT         =   41,

    /* These are meant to be used as return types for native code functions.
     * They cannot be stored.
     */
    SKY_DATA_TYPE_ERRNO             =   81,
    SKY_DATA_TYPE_SYSCALL_VOID      =   82,
    SKY_DATA_TYPE_SYSCALL_INT       =   83,
    SKY_DATA_TYPE_SYSCALL_BOOL      =   84,
    SKY_DATA_TYPE_SYSCALL_PID_T     =   85,
    SKY_DATA_TYPE_SYSCALL_OFF_T     =   86,

    SKY_DATA_TYPE_OBJECT            =   128,
    SKY_DATA_TYPE_OBJECT_BOOLEAN    =   129,
    SKY_DATA_TYPE_OBJECT_BYTEARRAY  =   130,
    SKY_DATA_TYPE_OBJECT_BYTES      =   131,
    SKY_DATA_TYPE_OBJECT_COMPLEX    =   132,
    SKY_DATA_TYPE_OBJECT_DICT       =   133,
    SKY_DATA_TYPE_OBJECT_FLOAT      =   134,
    SKY_DATA_TYPE_OBJECT_INTEGER    =   135,
    SKY_DATA_TYPE_OBJECT_LIST       =   136,
    SKY_DATA_TYPE_OBJECT_MODULE     =   137,
    SKY_DATA_TYPE_OBJECT_SET        =   138,
    SKY_DATA_TYPE_OBJECT_STRING     =   139,
    SKY_DATA_TYPE_OBJECT_TUPLE      =   140,
    SKY_DATA_TYPE_OBJECT_TYPE       =   141,
                                                
    SKY_DATA_TYPE_OBJECT_INSTANCEOF =   255,

    SKY_DATA_TYPE_MASK              =   0x0FF,

    /* Flags that can be OR'd into a SKY_DATA_TYPE listed above for more
     * options.
     */
    SKY_DATA_TYPE_OBJECT_OR_NONE            =   0x0100,
    SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED  =   0x0200,
    SKY_DATA_TYPE_OBJECT_PRESERVE_NULL      =   0x0400,
    SKY_DATA_TYPE_NONE_IS_EXTRA             =   0x0800,
    SKY_DATA_TYPE_INTEGER_CLAMP             =   0x1000,
    SKY_DATA_TYPE_CREATE_FROM_BUFFER        =   0x2000,
    SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME   =   0x4000,
    SKY_DATA_TYPE_ENCODE_STRING             =   0x8000,

    SKY_DATA_TYPE_OBJECT_BYTES_FSNAME       =
                    SKY_DATA_TYPE_OBJECT_BYTES |
                    SKY_DATA_TYPE_CREATE_FROM_BUFFER |
                    SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME,
} sky_data_type_t;


/** Check an object to determine whether it is valid for a data type.
  *
  * @param[in]  data_type   the data type to check.
  * @param[in]  object      the object to check.
  * @param[in]  extra       extra information that some data types may require
  *                         (e.g., @c SKY_DATA_TYPE_OBJECT_INSTANCEOF requires
  *                         a @c sky_type_t object).
  * @return     @c SKY_TRUE if @a object can satisfy @a data_type, or
  *             @c SKY_FALSE if it cannot.
  */
SKY_EXTERN sky_bool_t
sky_data_type_check(sky_data_type_t data_type,
                    sky_object_t    object,
                    sky_object_t    extra);


/** Determine whether extra information is needed for a data type.
  *
  * For check, load, name, and store operations there is an @c extra field
  * that is sometimes needed depending on the data type. This function will
  * return @c SKY_TRUE if the data type needs @c extra to be specified, or
  * @c SKY_FALSE if @c extra is ignored.
  *
  * @param[in]  data_type   the data type to check
  * @return     @c SKY_TRUE if @a data_type needs @c extra to be specified, or
  *             @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_data_type_isextraneeded(sky_data_type_t data_type);


/** Return the name for a data type.
  *
  * @param[in]  data_type   the data type for which a name is to be returned.
  * @param[in]  extra       extra information that some data types may require
  *                         (e.g., @c SKY_DATA_TYPE_OBJECT_INSTANCEOF requires
  *                         a @c sky_type_t object).
  * @return     a string name for @a data_type.
  */
SKY_EXTERN sky_string_t
sky_data_type_name(sky_data_type_t  data_type,
                   sky_object_t     extra);


/** Return an object instance that represents a data type with data loaded from
  * a memory location.
  *
  * @param[in]  data_type   the data type to load.
  * @param[in]  pointer     the memory location from which data is to be loaded.
  * @param[in]  extra       extra information that some data types may require
  *                         (e.g., @c SKY_DATA_TYPE_OBJECT_INSTANCEOF requires
  *                         a @c sky_type_t object).
  * @return     an object instance that represents @a data_type with data
  *             loaded from a memory location.
  */
SKY_EXTERN sky_object_t
sky_data_type_load(sky_data_type_t  data_type,
                   const void *     pointer,
                   sky_object_t     extra);


/** Store data represented by an object at a memory location using a format
  * string to specify the data type.
  * The format string should contain only a single format specification without
  * endian specification. For the 'p' and 's' formats, a count preceding them
  * is supported (e.g., '10s' means a 10 byte string). The supported format
  * characters are the same as the struct module.
  *
  * @param[in]  pointer     the memory location at which data is to be stored.
  * @param[in]  format      the format string representing the data type to be
  *                         stored.
  * @param[in]  value       the object instance to convert for storage.
  * @return     the number of bytes written starting at @a pointer.
  */
SKY_EXTERN size_t
sky_data_type_pack_native(void *        pointer,
                          const char *  format,
                          sky_object_t  value);


/** Store data represented by an object at a memory location.
  * If @a value cannot be used to represent the specified data type, a
  * @c sky_TypeError exception will be raised.
  *
  * @param[in]  data_type   the data type to store.
  * @param[in]  object      the object containing the memory location to which
  *                         @a value will be assigned.
  * @param[in]  value       the object instance to convert for storage.
  * @param[in]  pointer     the memory location at which data is to be stored.
  * @param[in]  extra       extra information that some data types may require
  *                         (e.g., @c SKY_DATA_TYPE_OBJECT_INSTANCEOF requires
  *                         a @c sky_type_t object).
  */
SKY_EXTERN void
sky_data_type_store(sky_data_type_t data_type,
                    sky_object_t    object,
                    sky_object_t    value,
                    void *          pointer,
                    sky_object_t    extra);


/** Return an object instance that represents a single data type with data
  * loaded from a memory location using a format string to specify the data
  * type.
  * The format string should contain only a single format specification without
  * an endian specification. For the 'p' and 's' formats, a count preceding
  * them is supported (e.g., '10s' means a 10 byte string). The supported format
  * characters are the same as the struct module.
  *
  * @param[in]  pointer     the memory location from which data is to be loaded.
  * @param[in]  format      the format string representing the data type to be
  *                         loaded.
  */
SKY_EXTERN sky_object_t
sky_data_type_unpack_native(const void *   pointer,
                            const char *   format);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_DATA_TYPE_H__ */

/** @} **/
