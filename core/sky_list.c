#include "sky_private.h"
#include "sky_list_private.h"


#define SKY_LIST_MINIMUM_CAPACITY       8


static sky_list_table_t sky_list_table_empty = { 0, 0, {} };


static sky_type_t sky_list_iterator_type = NULL;
static sky_type_t sky_list_reverseiterator_type = NULL;


typedef struct sky_list_iterator_data_s {
    sky_list_t                          list;
    ssize_t                             next_index;
} sky_list_iterator_data_t;

static inline sky_list_iterator_data_t *
sky_list_iterator_data(sky_object_t object)
{
    if (sky_object_isa(object, sky_list_iterator_type)) {
        return sky_object_data(object, sky_list_iterator_type);
    }
    if (sky_object_isa(object, sky_list_reverseiterator_type)) {
        return sky_object_data(object, sky_list_reverseiterator_type);
    }

    sky_error_raise_format(sky_TypeError,
                           "expected 'list_iterator'; got %#@",
                           sky_type_name(sky_object_type(object)));
}


struct sky_list_s {
    sky_object_data_t                   object_data;
    sky_list_data_t                     list_data;
};

typedef struct sky_list_iterator_s {
    sky_object_data_t                   object_data;
    sky_list_iterator_data_t            iterator_data;
} *sky_list_iterator_t, *sky_list_reverseiterator_t;


static inline sky_list_table_t *
sky_list_table(sky_list_t self, sky_list_data_t *self_data, sky_bool_t fast)
{
    sky_hazard_pointer_t    *hazard;

    if (fast && !sky_list_isshared(self)) {
        return (self_data->table ? self_data->table : &sky_list_table_empty);
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
    if (!hazard->pointer) {
        sky_hazard_pointer_release(hazard);
        return &sky_list_table_empty;
    }
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);
    return hazard->pointer;
}

static inline ssize_t
sky_list_index_normalize(ssize_t index, ssize_t len, const char *name)
{
    if (index < 0) {
        index += len;
        if (index < 0) {
            sky_error_raise_format(sky_IndexError,
                                   "%s index out of range",
                                   name);
        }
    }
    if (index >= len) {
        sky_error_raise_format(sky_IndexError,
                               "%s index out of range",
                               name);
    }
    return index;
}

static sky_bool_t
sky_list_table_compare(sky_list_table_t *   self_table,
                       sky_list_table_t *   other_table,
                       sky_compare_op_t     compare_op)
{
    ssize_t i, len, other_len, self_len;

    if ((self_len = self_table->len) != (other_len = other_table->len)) {
        if (SKY_COMPARE_OP_EQUAL == compare_op) {
            return SKY_FALSE;
        }
        if (SKY_COMPARE_OP_NOT_EQUAL == compare_op) {
            return SKY_TRUE;
        }
    }

    len = SKY_MIN(self_len, other_len);
    for (i = 0; i < len; ++i) {
        if (!sky_object_compare(self_table->objects[i],
                                other_table->objects[i],
                                SKY_COMPARE_OP_EQUAL))
        {
            switch (compare_op) {
                case SKY_COMPARE_OP_EQUAL:
                    return SKY_FALSE;
                case SKY_COMPARE_OP_NOT_EQUAL:
                    return SKY_TRUE;
                default:
                    return sky_object_compare(self_table->objects[i],
                                              other_table->objects[i],
                                              compare_op);
            }
        }
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            return (self_len == other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (self_len != other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_LESS:
            return (self_len < other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (self_len <= other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_GREATER:
            return (self_len > other_len ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (self_len >= other_len ? SKY_TRUE : SKY_FALSE);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }

    sky_error_fatal("internal error");
}

static sky_list_table_t *
sky_list_table_resize(sky_list_table_t *old_table,
                      ssize_t           capacity,
                      sky_bool_t        zero)
    /* NOTE: this does NOT free old_table! */
{
    ssize_t             i, new_capacity;
    sky_list_table_t    *new_table;

    i = (sizeof(sky_list_table_t) + sizeof(sky_object_t) - 1) /
        sizeof(sky_object_t);
    if (capacity < SKY_LIST_MINIMUM_CAPACITY) {
        capacity = SKY_LIST_MINIMUM_CAPACITY;
    }
    new_capacity = sky_util_roundpow2(capacity + i) - i;
    if (old_table && new_capacity == old_table->capacity) {
        return old_table;
    }
    new_table = sky_malloc(sizeof(sky_list_table_t) +
                           (new_capacity * sizeof(sky_object_t)));
    new_table->capacity = new_capacity;

    if (!old_table) {
        new_table->len = 0;
    }
    else if ((new_table->len = old_table->len) > 0) {
        memcpy(new_table->objects,
               old_table->objects,
               new_table->len * sizeof(sky_object_t));
    }

    /* Always zero the space between new_capacity and capacity, but only maybe
     * zero the difference between capacity and len.
     */
    i = (zero ? new_table->len : capacity);
    memset(&(new_table->objects[i]),
           0,
           (new_capacity - i) * sizeof(sky_object_t));

    return new_table;
}


static sky_list_table_t *
sky_list_table_create(sky_object_t sequence)
{
    ssize_t             capacity, len;
    sky_object_t        item, iter;
    sky_list_table_t    *new_table, *table;

    if (!sky_object_isiterable(sequence)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(sequence)));
    }

#define add_item(_item)                                         \
    do {                                                        \
        if (len >= capacity) {                                  \
            sky_error_validate_debug(len <= table->capacity);   \
            table->len = len;                                   \
            new_table = sky_list_table_resize(table,            \
                                              (capacity << 1),  \
                                              SKY_FALSE);       \
            sky_asset_update(table,                             \
                             new_table,                         \
                             SKY_ASSET_UPDATE_FIRST_CURRENT);   \
            sky_free(table);                                    \
            table = new_table;                                  \
            capacity = table->capacity;                         \
        }                                                       \
        SKY_ERROR_TRY {                                         \
            table->objects[len++] = (_item);                    \
        } SKY_ERROR_EXCEPT_ANY {                                \
            sky_error_validate_debug(len <= table->capacity);   \
            table->len = len;                                   \
            sky_error_raise();                                  \
        } SKY_ERROR_TRY_END;                                    \
    } while (0)

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table_resize(
                        NULL,
                        sky_object_length_hint(sequence,
                                               SKY_LIST_MINIMUM_CAPACITY),
                        SKY_FALSE);
        sky_asset_save(table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        len = 0;
        capacity = table->capacity;
        if (sky_sequence_isiterable(sequence)) {
            SKY_SEQUENCE_FOREACH(sequence, item) {
                add_item(item);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(sequence);
            while ((item = sky_object_next(iter, NULL)) != NULL) {
                add_item(item);
            }
        }
        sky_error_validate_debug(len <= table->capacity);
        table->len = len;
    } SKY_ASSET_BLOCK_END;

#undef add_item

    return table;
}

sky_list_table_t *
sky_list_data_settable(sky_list_data_t *    list_data,
                       sky_list_table_t *   new_table,
                       sky_bool_t           shared)
{
    sky_list_table_t    *old_table;

    if (!shared) {
        old_table = list_data->table;
        list_data->table = new_table;
    }
    else {
        old_table = sky_atomic_exchange(new_table,
                                        SKY_AS_VOIDP(&(list_data->table)));
    }
    if (old_table && old_table != new_table) {
        sky_error_validate_debug(&sky_list_table_empty != old_table);
        sky_hazard_pointer_retire(old_table, sky_free);
    }
    return new_table;
}

static sky_list_table_t *
sky_list_resize(sky_list_t list, ssize_t capacity, sky_bool_t zero)
{
    sky_list_data_t         *list_data = sky_list_data(list);

    sky_list_table_t        *new_table, *old_table;
    sky_hazard_pointer_t    *hazard;

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(list_data->table)));
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);

    old_table = hazard->pointer;
    new_table = sky_list_table_resize(old_table, capacity, zero);
    sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));
    sky_list_data_settable(list_data, new_table, sky_list_isshared(list));

    return new_table;
}

static sky_list_t
sky_list_createwithtype(sky_object_t sequence, sky_type_t type)
{
    sky_list_t          list;
    sky_list_table_t    *table;

    sky_error_validate_debug(sky_type_issubtype(type, sky_list_type));

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table_create(sequence);
        sky_asset_save(table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        list = sky_object_allocate(type);
        sky_object_gc_setarray(table->len, table->objects, list);
        sky_list_data(list)->table = table;
    } SKY_ASSET_BLOCK_END;

    return list;
}


static void
sky_list_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_free(((sky_list_data_t *)data)->table);
}

static void
sky_list_instance_iterate(
        SKY_UNUSED  sky_object_t                    self,
                    void *                          data,
                    sky_sequence_iterate_state_t *  state,
        SKY_UNUSED  sky_object_t *                  objects,
        SKY_UNUSED  ssize_t                         nobjects)
{
    sky_list_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    if (state->state) {
        state->objects = NULL;
        state->nobjects = 0;
    }
    else {
        sky_list_data_t *self_data = data;

        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        table = hazard->pointer;
        if (!(table = hazard->pointer) || !(state->state = table->len)) {
            sky_hazard_pointer_release(hazard);
            state->objects = NULL;
            state->nobjects = 0;
            return;
        }

        state->extra[0] = (uintptr_t)hazard;
        state->objects = table->objects;
        state->nobjects = state->state;
    }
}

static void
sky_list_instance_iterate_cleanup(
        SKY_UNUSED  sky_object_t                    self,
        SKY_UNUSED  void *                          data,
                    sky_sequence_iterate_state_t *  state)
{
    sky_hazard_pointer_t    *hazard;

    if ((hazard = (sky_hazard_pointer_t *)state->extra[0]) != NULL) {
        sky_hazard_pointer_release(hazard);
    }
}

static void
sky_list_instance_visit(
        sky_object_t                self,
        void *                      data,
        sky_object_visit_data_t *   visit_data)
{
    ssize_t             i;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        if ((table = sky_list_table(self, data, SKY_FALSE)) != NULL) {
            for (i = table->len - 1; i >= 0; --i) {
                sky_object_visit(table->objects[i], visit_data);
            }
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_list_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_list_iterator_data_t    *self_data = data;

    sky_object_visit(self_data->list, visit_data);
}


sky_object_t
sky_list_add(sky_list_t self, sky_object_t other)
{
    ssize_t             new_len, other_len, self_len;
    sky_list_t          new_list;
    sky_list_table_t    *new_table, *other_table, *self_table;

    if (!sky_object_isa(other, sky_list_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only concatenate list (not %#@) to list",
                sky_type_name(sky_object_type(other)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        self_table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        self_len = self_table->len;
        other_table = sky_list_table(other, sky_list_data(other), SKY_TRUE);
        other_len = other_table->len;

        if (SSIZE_MAX - self_len < other_len) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        new_len = self_len + other_len;
        new_table = sky_list_table_resize(NULL, new_len, SKY_FALSE);
        sky_asset_save(new_table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        sky_error_validate_debug(new_len <= new_table->capacity);
        new_table->len = new_len;
        memcpy(new_table->objects,
               self_table->objects,
               self_len * sizeof(sky_object_t));
        memcpy(&(new_table->objects[self_table->len]),
               other_table->objects,
               other_len * sizeof(sky_object_t));

        new_list = sky_object_allocate(sky_list_type);
        sky_object_gc_setarray(new_table->len, new_table->objects, new_list);
        sky_list_data(new_list)->table = new_table;
    } SKY_ASSET_BLOCK_END;

    return new_list;
}

void
sky_list_append(sky_list_t self, sky_object_t item)
{
    ssize_t             index;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);

        index = table->len;
        sky_error_validate_debug(index <= table->capacity);
        if (index >= table->capacity) {
            table = sky_list_resize(self, table->capacity << 1, SKY_FALSE);
        }
        sky_object_gc_set(&(table->objects[index]), item, self);
        sky_error_validate_debug(index + 1 <= table->capacity);
        table->len = index + 1;
    } SKY_ASSET_BLOCK_END;
}

void
sky_list_clear(sky_list_t self)
{
    sky_list_data_settable(sky_list_data(self),
                           sky_list_table_resize(NULL,
                                                 SKY_LIST_MINIMUM_CAPACITY,
                                                 SKY_TRUE),
                           sky_list_isshared(self));
}

sky_object_t
sky_list_compare(sky_list_t         self,
                 sky_object_t       other,
                 sky_compare_op_t   compare_op)
{
    sky_bool_t          result;
    sky_list_table_t    *other_table, *self_table;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_list_type)) {
        return sky_NotImplemented;
    }

    SKY_ASSET_BLOCK_BEGIN {
        self_table = sky_list_table(self, sky_list_data(self), SKY_FALSE);
        other_table = sky_list_table(other, sky_list_data(other), SKY_FALSE);
        result = sky_list_table_compare(self_table, other_table, compare_op);
    } SKY_ASSET_BLOCK_END;

    return (result ? sky_True : sky_False);
}

sky_bool_t
sky_list_contains(sky_list_t self, sky_object_t item)
{
    sky_bool_t      result;
    sky_object_t    object;

    result = SKY_FALSE;
    SKY_SEQUENCE_FOREACH(self, object) {
        if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
            result = SKY_TRUE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return result;
}

sky_list_t
sky_list_copy(sky_list_t self)
{
    return sky_list_createwithtype(self, sky_list_type);
}

ssize_t
sky_list_count(sky_list_t self, sky_object_t item)
{
    ssize_t         count;
    sky_object_t    object;

    count = 0;
    SKY_SEQUENCE_FOREACH(self, object) {
        if (sky_object_compare(object, item, SKY_COMPARE_OP_EQUAL)) {
            ++count;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return count;
}

sky_list_t
sky_list_create(sky_object_t sequence)
{
    sky_list_t  list;

    if (sequence) {
        list = sky_list_createwithtype(sequence, sky_list_type);
    }
    else {
        list = sky_object_allocate(sky_list_type);
        sky_list_data(list)->table =
                sky_list_table_resize(NULL,
                                      SKY_LIST_MINIMUM_CAPACITY,
                                      SKY_FALSE);
    }

    return list;
}

sky_list_t
sky_list_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t             i;
    sky_list_t          list;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table_resize(NULL, count, SKY_FALSE);
        sky_error_validate_debug(count <= table->capacity);
        table->len = count;
        for (i = 0; i < count; ++i) {
            table->objects[i] = (objects[i] ? objects[i] : sky_None);
        }

        list = sky_object_allocate(sky_list_type);
        sky_object_gc_setarray(table->len, table->objects, list);
        sky_list_data(list)->table = table;
    } SKY_ASSET_BLOCK_END;

    return list;
}

sky_list_t
sky_list_createwithcapacity(ssize_t capacity)
{
    sky_list_t  list;

    list = sky_object_allocate(sky_list_type);
    sky_list_data(list)->table =
            sky_list_table_resize(NULL, capacity, SKY_FALSE);

    return list;
}

void
sky_list_delete(sky_list_t self, ssize_t start, ssize_t stop, ssize_t step)
{
    ssize_t             i, j, k, slice_len, table_len;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        table_len = table->len;

        slice_len = sky_sequence_indices(table_len, &start, &stop, &step);
        if (step < 0) {
            stop = start + 1;
            start = stop + step * (slice_len - 1) - 1;
            step = -step;
        }

        j = start;
        if (step > 1) {
            for (i = start + 1; i < stop; i += step) {
                for (k = i; k < i + step - 1; ++k, ++j) {
                    sky_object_gc_set(&(table->objects[j]),
                                      table->objects[k],
                                      self);
                }
            }
        }
        for (i = stop; i < table_len; ++i, ++j) {
            sky_object_gc_set(&(table->objects[j]),
                              table->objects[i],
                              self);
        }

        sky_error_validate_debug(table_len - slice_len <= table->capacity);
        table->len = table_len - slice_len;
    } SKY_ASSET_BLOCK_END;
}

void
sky_list_delitem(sky_list_t self, sky_object_t item)
{
    sky_list_setitem(self, item, NULL);
}

sky_object_t
sky_list_eq(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_EQUAL);
}

void
sky_list_extend(sky_list_t self, sky_object_t sequence)
{
    sky_list_data_t *self_data = sky_list_data(self);

    ssize_t                 capacity, hint, len, start;
    sky_object_t            item, iter;
    sky_list_table_t        *new_table, *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        if (!(table = hazard->pointer)) {
            table = &sky_list_table_empty;
        }

        if (!sky_object_isiterable(sequence)) {
            sky_error_raise_format(sky_TypeError, 
                                   "expected sequence; got %#@",
                                   sky_type_name(sky_object_type(sequence)));
        }

        len = table->len;
        hint = sky_object_length_hint(sequence, SKY_LIST_MINIMUM_CAPACITY);
        if (len + hint > table->capacity) {
            new_table = sky_list_table_resize(table, len + hint, SKY_TRUE);
            sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));
            sky_error_validate_debug(hazard->pointer == new_table);
            table = sky_list_data_settable(self_data,
                                           new_table,
                                           sky_list_isshared(self));
        }
        capacity = table->capacity;
        sky_error_validate_debug(len <= capacity);

#define add_item(_item)                                                     \
    do {                                                                    \
        if (len >= capacity) {                                              \
            sky_error_validate_debug(len <= table->capacity);               \
            table->len = len;                                               \
            new_table = sky_list_table_resize(table,                        \
                                              capacity << 1,                \
                                              SKY_FALSE);                   \
            sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&new_table));    \
            sky_error_validate_debug(hazard->pointer == new_table);         \
            table = sky_list_data_settable(self_data,                       \
                                           new_table,                       \
                                           sky_list_isshared(self));        \
            capacity = table->capacity;                                     \
        }                                                                   \
        SKY_ERROR_TRY {                                                     \
            table->objects[len++] = _item;                                  \
        } SKY_ERROR_EXCEPT_ANY {                                            \
            sky_error_validate_debug(len <= table->capacity);               \
            table->len = len;                                               \
            sky_error_raise();                                              \
        } SKY_ERROR_TRY_END;                                                \
    } while (0)

        start = len;
        if (sky_sequence_isiterable(sequence)) {
            SKY_SEQUENCE_FOREACH(sequence, item) {
                add_item(item);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(sequence);
            while ((item = sky_object_next(iter, NULL)) != NULL) {
                add_item(item);
            }
        }
        sky_error_validate_debug(len <= table->capacity);
        table->len = len;

#undef add_item

        sky_object_gc_setarray(len - start, &(table->objects[start]), self);
    } SKY_ASSET_BLOCK_END;
}

ssize_t
sky_list_find(sky_list_t self, sky_object_t item, ssize_t start, ssize_t stop)
{
    ssize_t             i, result, step;
    sky_list_table_t    *table;

    result = -1;
    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_FALSE);

        step = 1;
        if (sky_sequence_indices(table->len, &start, &stop, &step)) {
            for (i = start; i < stop; ++i) {
                if (sky_object_compare(table->objects[i],
                                       item,
                                       SKY_COMPARE_OP_EQUAL))
                {
                    result = i;
                    break;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

sky_object_t
sky_list_ge(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}

sky_object_t
sky_list_get(sky_list_t self, ssize_t index)
{
    ssize_t             len;
    sky_object_t        item;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        len = table->len;

        index = sky_list_index_normalize(index, len, "list");
        item = table->objects[index];
    } SKY_ASSET_BLOCK_END;

    return item;
}

sky_object_t
sky_list_getitem(sky_list_t self, sky_object_t item)
{
    ssize_t             i, j, len, start, step, stop;
    sky_list_t          slice;
    sky_list_table_t    *self_table, *slice_table;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        return sky_list_get(self,
                            sky_integer_value_clamped(sky_number_index(item),
                                                      SSIZE_MIN, SSIZE_MAX));
    }

    if (!sky_object_isa(item, sky_slice_type)) {
        sky_error_raise_string(sky_TypeError,
                               "list indices must be integers");
    }

    SKY_ASSET_BLOCK_BEGIN {
        self_table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        len = sky_slice_range(item, self_table->len, &start, &stop, &step);

        slice_table = sky_list_table_resize(NULL, len, SKY_FALSE);
        sky_error_validate_debug(len <= slice_table->capacity);
        slice_table->len = len;
        for (i = start, j = 0; j < len; i += step, ++j) {
            slice_table->objects[j] = self_table->objects[i];
        }

        slice = sky_object_allocate(sky_list_type);
        sky_object_gc_setarray(slice_table->len, slice_table->objects, slice);
        sky_list_data(slice)->table = slice_table;
    } SKY_ASSET_BLOCK_END;

    return slice;
}

sky_object_t
sky_list_gt(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_GREATER);
}

sky_object_t
sky_list_iadd(sky_object_t self, sky_object_t other)
{
    sky_list_extend(self, other);
    return self;
}

sky_object_t
sky_list_imul(sky_object_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(
                sky_TypeError,
                "can't multiply sequence by non-int of type %#@",
                sky_type_name(sky_object_type(other)));
    }
    count = sky_integer_value(sky_number_index(other),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    
    return sky_list_repeat(self, count, SKY_TRUE);
}

ssize_t
sky_list_index(sky_list_t self, sky_object_t item, ssize_t start, ssize_t stop)
{
    ssize_t i;

    if ((i = sky_list_find(self, item, start, stop)) == -1) {
        sky_error_raise_format(sky_ValueError, "%#@ is not in list", item);
    }

    return i;
}

void
sky_list_init(sky_list_t self, sky_object_t sequence)
{
    sky_list_data_t *self_data = sky_list_data(self);

    sky_list_table_t    *table;

    if (sequence) {
        table = sky_list_table_create(sequence);
    }
    else {
        table = sky_list_table_resize(NULL,
                                      SKY_LIST_MINIMUM_CAPACITY,
                                      SKY_FALSE);
    }
    sky_list_data_settable(self_data, table, sky_list_isshared(self));
}

void
sky_list_insert(sky_list_t self, ssize_t index, sky_object_t item)
{
    ssize_t             i, len;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        len = table->len;

        if (index < 0) {
            index += len;
            if (index < 0) {
                index = 0;
            }
        }
        if (index > len) {
            index = len;
        }
        sky_error_validate_debug(index <= table->capacity);
        if (len + 1 >= table->capacity) {
            table = sky_list_resize(self, table->capacity << 1, SKY_FALSE);
        }

        if (!sky_list_isshared(self)) {
            memmove(&(table->objects[index + 1]),
                    &(table->objects[index]),
                    sizeof(sky_object_t) * (len - index));
        }
        else {
            for (i = len; i > index; --i) {
                table->objects[i] = table->objects[i - 1];
            }
        }
        table->objects[index] = item;
        sky_error_validate_debug(len + 1 <= table->capacity);
        table->len = ++len;
        sky_object_gc_setarray(len - index, &(table->objects[index]), self);
    } SKY_ASSET_BLOCK_END;
}

sky_object_t
sky_list_iter(sky_list_t self)
{
    sky_list_iterator_t         iterator;
    sky_list_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_list_iterator_type);
    iterator_data = sky_list_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->list)), self, iterator);

    return iterator;
}

sky_object_t
sky_list_le(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}

ssize_t
sky_list_len(sky_list_t self)
{
    ssize_t len;

    SKY_ASSET_BLOCK_BEGIN {
        len = sky_list_table(self, sky_list_data(self), SKY_TRUE)->len;
    } SKY_ASSET_BLOCK_END;

    return len;
}

sky_object_t
sky_list_lt(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_LESS);
}

sky_object_t
sky_list_mul(sky_object_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(
                sky_TypeError,
                "can't multiply sequence by non-int of type %#@",
                sky_type_name(sky_object_type(other)));
    }
    count = sky_integer_value(sky_number_index(other),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    
    return sky_list_repeat(self, count, SKY_FALSE);
}

sky_object_t
sky_list_ne(sky_list_t self, sky_object_t other)
{
    return sky_list_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}

sky_object_t
sky_list_pop(sky_list_t self, ssize_t index)
{
    ssize_t             i, len;
    sky_object_t        item;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        if (!(len = table->len)) {
            sky_error_raise_string(sky_IndexError, "pop from empty list");
        }

        index = sky_list_index_normalize(index, len, "pop");
        item = table->objects[index];
        sky_error_validate_debug(len - 1 <= table->capacity);
        table->len = len - 1;
        if (!sky_list_isshared(self)) {
            memmove(&(table->objects[index]),
                    &(table->objects[index + 1]),
                    sizeof(sky_object_t) * (len - 1 - index));
            sky_object_gc_setarray(len - 1 - index,
                                   &(table->objects[index]),
                                   self);
        }
        else {
            for (i = index + 1; i < len; ++i) {
                sky_object_gc_set(&(table->objects[i - 1]),
                                  table->objects[i],
                                  self);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return item;
}

void
sky_list_remove(sky_list_t self, sky_object_t item)
{
    ssize_t             i, index, len;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_FALSE);
        len = table->len;

        index = -1;
        for (i = 0; i < len; ++i) {
            if (sky_object_compare(table->objects[i],
                                   item,
                                   SKY_COMPARE_OP_EQUAL))
            {
                index = i;
                break;
            }
        }
        if (-1 == index) {
            sky_error_raise_string(sky_ValueError,
                                   "list.remove(x): x not in list");
        }

        sky_error_validate_debug(len - 1 <= table->capacity);
        table->len = len - 1;
        if (!sky_list_isshared(self)) {
            memmove(&(table->objects[index]),
                    &(table->objects[index + 1]),
                    sizeof(sky_object_t) * (len - 1 - index));
            sky_object_gc_setarray(len - 1 - index,
                                   &(table->objects[index]),
                                   self);
        }
        else {
            for (i = index + 1; i < len; ++i) {
                sky_object_gc_set(&(table->objects[i - 1]),
                                  table->objects[i],
                                  self);
            }
        }
    } SKY_ASSET_BLOCK_END;
}

sky_list_t
sky_list_repeat(sky_list_t self, ssize_t count, sky_bool_t inplace)
{
    ssize_t             i, source_len, target_len;
    sky_list_t          target_list;
    sky_list_table_t    *source_table, *target_table;

    if (count <= 0) {
        if (inplace) {
            sky_list_clear(self);
            return self;
        }
        return sky_list_create(NULL);
    }
    if (1 == count && inplace) {
        return self;
    }

    SKY_ASSET_BLOCK_BEGIN {
        source_table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        source_len = source_table->len;

        if (source_len > SSIZE_MAX / count) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        target_len = source_len * count;
        target_table = sky_list_table_resize(NULL, target_len, SKY_FALSE);
        sky_asset_save(target_table, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < count; ++i) {
            memcpy(&(target_table->objects[target_table->len]),
                   &(source_table->objects[0]),
                   sizeof(sky_object_t) * source_len);
            target_table->len += source_len;
        }

        if (inplace) {
            target_list = self;
        }
        else {
            target_list = sky_object_allocate(sky_list_type);
        }
        sky_object_gc_setarray(target_table->len,
                               target_table->objects,
                               target_list);
        sky_list_data_settable(sky_list_data(target_list),
                               target_table,
                               sky_list_isshared(target_list));
    } SKY_ASSET_BLOCK_END;

    return target_list;
}

sky_string_t
sky_list_repr(sky_list_t self)
{
    ssize_t                 i, len;
    sky_string_t            joiner, result;
    sky_list_table_t        *table;
    sky_string_builder_t    builder;

    if (sky_object_stack_push(self)) {
        return SKY_STRING_LITERAL("[...]");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        table = sky_list_table(self, sky_list_data(self), SKY_FALSE);
        if (!(len = table->len)) {
            result = SKY_STRING_LITERAL("[]");
        }
        else {
            builder = sky_string_builder_create();
            sky_string_builder_appendcodepoint(builder, '[', 1);

            sky_string_builder_append(builder,
                                      sky_object_repr(table->objects[0]));
            if (len > 1) {
                joiner = SKY_STRING_LITERAL(", ");
                for (i = 1; i < len; ++i) {
                    sky_string_builder_append(builder, joiner);
                    sky_string_builder_append(
                            builder,
                            sky_object_repr(table->objects[i]));
                }
            }

            sky_string_builder_appendcodepoint(builder, ']', 1);
            result = sky_string_builder_finalize(builder);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

void
sky_list_reverse(sky_list_t self)
{
    ssize_t             len;
    sky_object_t        *head, object, *tail;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        if ((len = table->len) > 1) {
            head = &(table->objects[0]);
            tail = &(table->objects[len - 1]);
            while (head < tail) {
                object = *head;
                *head++ = *tail;
                *tail-- = object;
            }
            sky_object_gc_setarray(len, table->objects, self);
        }
    } SKY_ASSET_BLOCK_END;
}

sky_object_t
sky_list_reversed(sky_list_t self)
{
    sky_list_iterator_t         iterator;
    sky_list_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_list_reverseiterator_type);
    iterator_data = sky_list_iterator_data(iterator);
    iterator_data->next_index = sky_list_len(self) - 1;
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->list)), self, iterator);

    return iterator;
}

void
sky_list_set(sky_list_t self, ssize_t index, sky_object_t item)
{
    ssize_t             len;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        len = table->len;

        index = sky_list_index_normalize(index, len, "list assignment");
        sky_object_gc_set(&(table->objects[index]), item, self);
    } SKY_ASSET_BLOCK_END;
}

static sky_list_table_t *
sky_list_setitem_slice(sky_list_t           self,
                       sky_list_table_t *   table,
                       ssize_t              len,
                       ssize_t              start,
                       ssize_t              stop,
                       sky_object_t         value)
{
    ssize_t         delta, i;
    sky_object_t    object;

    if (self == value) {
        /* Special case "self[start:stop] = self" */
        value = sky_list_createwithtype(value, sky_list_type);
    }
    if (value && !sky_sequence_isiterable(value)) {
        value = sky_list_create(value);
    }

    if (start < 0) {
        start = 0;
    }
    else if (start > len) {
        start = len;
    }
    if (stop < start) {
        stop = start;
    }
    else if (stop > len) {
        stop = len;
    }

    if (!value) {
        delta = -(stop - start);
    }
    else {
        delta = sky_object_len(value) - (stop - start);
    }
    if (!(len + delta)) {
        sky_list_clear(self);
        return table;
    }

    if (delta < 0) {
        /* Delete -delta items */
        for (i = 0; i < len - stop; ++i) {
            sky_object_gc_set(&(table->objects[stop + delta + i]),
                              table->objects[stop + i],
                              self);
        }
    }
    else if (delta > 0) {
        /* Insert delta items */
        table = sky_list_resize(self, len + delta, SKY_TRUE);
        for (i = len - stop - 1; i >= 0; --i) {
            sky_object_gc_set(&(table->objects[stop + delta + i]),
                              table->objects[stop + i],
                              self);
        }
    }
    sky_error_validate_debug(len + delta <= table->capacity);
    table->len = len + delta;

    if (value) {
        SKY_SEQUENCE_FOREACH(value, object) {
            sky_object_gc_set(&(table->objects[start]), object, self);
            ++start;
        } SKY_SEQUENCE_FOREACH_END;
    }

    return table;
}

void
sky_list_setitem(sky_list_t self, sky_object_t item, sky_object_t value)
{
    size_t              j, limit, x;
    ssize_t             i, index, len, start, step, stop, table_len;
    sky_object_t        object;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_FALSE);
        table_len = table->len;

        if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            index = sky_integer_value_clamped(sky_number_index(item),
                                              SSIZE_MIN, SSIZE_MAX);
            index = sky_list_index_normalize(index, table_len, "list assignment");
            if (value) {
                sky_object_gc_set(&(table->objects[index]), value, self);
            }
            else {
                sky_error_validate_debug(table_len - 1 <= table->capacity);
                table->len = table_len - 1;
                if (!sky_list_isshared(self)) {
                    memmove(&(table->objects[index]),
                            &(table->objects[index + 1]),
                            sizeof(sky_object_t) * (table_len - 1 - index));
                    sky_object_gc_setarray(table_len - 1 - index,
                                           &(table->objects[index]),
                                           self);
                }
                else {
                    for (i = index + 1; i < table_len; ++i) {
                        sky_object_gc_set(&(table->objects[i - 1]),
                                          table->objects[i],
                                          self);
                    }
                }
            }
        }
        else if (sky_object_isa(item, sky_slice_type)) {
            len = sky_slice_range(item, table_len, &start, &stop, &step);
            if (1 == step) {
                table = sky_list_setitem_slice(self,
                                               table,
                                               table_len,
                                               start,
                                               stop,
                                               value);
            }
            else {
                if ((step < 0 && start < stop) || (step > 0 && start > stop)) {
                    stop = start;
                }
                if (!value) {   /* __delitem__ */
                    if (len > 0) {
                        if (step < 0) {
                            stop = start + 1;
                            start = stop + (step * (len - 1) - 1);
                            step = -step;
                        }

                        for (j = start, i = 0; j < (size_t)stop; j += step, ++i) {
                            limit = step - 1;
                            if (j + step >= (size_t)table_len) {
                                limit = table_len - j - 1;
                            }
                            for (x = 0; x < limit; ++x) {
                                sky_object_gc_set(&(table->objects[j - i + x]),
                                                  table->objects[j + 1 + x],
                                                  self);
                            }
                        }
                        j = start + ((size_t)len * step);
                        if (j < (size_t)table_len) {
                            for (x = 0; x < table_len - j; ++x) {
                                sky_object_gc_set(&(table->objects[j - len + x]),
                                                  table->objects[j + x],
                                                  self);
                            }
                        }

                        sky_error_validate_debug(table_len - len <= table->capacity);
                        table->len = table_len - len;
                    }
                }
                else {          /* __setitem__ */
                    /* protect against a[::-1] = a */
                    if (self == value || !sky_sequence_isiterable(value)) {
                        value = sky_list_create(value);
                    }
                    if (sky_object_len(value) != len) {
                        sky_error_raise_format(
                                sky_ValueError,
                                "attempt to assign sequence of size %zd "
                                "to extended slice of size %zd",
                                sky_object_len(value),
                                len);
                    }
                    if (len > 0) {
                        i = start;
                        SKY_SEQUENCE_FOREACH(value, object) {
                            sky_error_validate_debug(i < table_len);
                            sky_object_gc_set(&(table->objects[i]), object, self);
                            i += step;
                        } SKY_SEQUENCE_FOREACH_END;
                    }
                }
            }
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "list indices must be integers, not %@",
                                   sky_type_name(sky_object_type(item)));
        }
    } SKY_ASSET_BLOCK_END;
}

size_t
sky_list_sizeof(sky_list_t self)
{
    size_t              result;
    sky_list_table_t    *table;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        result = sky_memsize(self);
        if (table != &sky_list_table_empty) {
            result += sky_memsize(table);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

sky_list_t
sky_list_slice(sky_list_t self, ssize_t start, ssize_t stop, ssize_t step)
{
    ssize_t             i, j, len;
    sky_list_t          slice;
    sky_list_table_t    *self_table, *slice_table;

    SKY_ASSET_BLOCK_BEGIN {
        self_table = sky_list_table(self, sky_list_data(self), SKY_TRUE);
        len = sky_sequence_indices(self_table->len, &start, &stop, &step);

        slice_table = sky_list_table_resize(NULL, len, SKY_FALSE);
        sky_error_validate_debug(len <= slice_table->capacity);
        slice_table->len = len;
        for (i = start, j = 0; i < len; i += step, ++j) {
            slice_table->objects[j] = self_table->objects[i];
        }

        slice = sky_object_allocate(sky_list_type);
        sky_object_gc_setarray(slice_table->len, slice_table->objects, slice);
        sky_list_data(slice)->table = slice_table;
    } SKY_ASSET_BLOCK_END;

    return slice;
}


static sky_object_t
sky_list_iterator_iter(sky_object_t self)
{
    return self;
}

static sky_object_t
sky_list_iterator_length_hint(sky_object_t self)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    ssize_t             result;
    sky_list_t          list;
    sky_list_table_t    *table;

    result = 0;
    SKY_ASSET_BLOCK_BEGIN {
        if ((list = self_data->list) != NULL) {
            table = sky_list_table(list, sky_list_data(list), SKY_TRUE);
            result = table->len - self_data->next_index;
        }
    } SKY_ASSET_BLOCK_END;

    return (result > 0 ? sky_integer_create(result) : sky_integer_zero);
}

static sky_object_t
sky_list_iterator_next(sky_object_t self)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    ssize_t             next_index;
    sky_list_t          list;
    sky_object_t        result;
    sky_list_table_t    *table;

    if (!(list = self_data->list)) {
        return NULL;
    }

    result = NULL;
    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(list, sky_list_data(list), SKY_TRUE);
        if ((next_index = self_data->next_index) < table->len) {
            result = table->objects[next_index];
            self_data->next_index = next_index + 1;
        }
    } SKY_ASSET_BLOCK_END;

    if (!result) {
        self_data->list = NULL;
    }
    return result;
}

static sky_object_t
sky_list_iterator_reduce(sky_object_t self)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    sky_list_t      list;
    sky_object_t    iter;

    if (!(list = self_data->list)) {
        return sky_object_build("(O([]))", sky_module_getbuiltin("iter"));
    }

    if (sky_object_isa(self, sky_list_iterator_type)) {
        iter = sky_module_getbuiltin("iter");
    }
    else {
        iter = sky_module_getbuiltin("reversed");
    }

    return sky_object_build("(O(O)iz)", iter, list, self_data->next_index);
}

static void
sky_list_iterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    if (self_data->list) {
        self_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
        if (self_data->next_index < 0) {
            self_data->next_index = 0;
        }
        else if (self_data->next_index > sky_object_len(self_data->list)) {
            self_data->next_index = sky_object_len(self_data->list);
        }
    }
}


static sky_object_t
sky_list_reverseiterator_length_hint(sky_object_t self)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    ssize_t             len, result;
    sky_list_t          list;
    sky_list_table_t    *table;

    result = 0;
    SKY_ASSET_BLOCK_BEGIN {
        if ((list = self_data->list) != NULL) {
            table = sky_list_table(self_data->list,
                                   sky_list_data(self_data->list),
                                   SKY_TRUE);
            result = self_data->next_index + 1;
            if (result > (len = table->len)) {
                result = len;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return (result > 0 ? sky_integer_create(result) : sky_integer_zero);
}

static sky_object_t
sky_list_reverseiterator_next(sky_object_t self)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    ssize_t             len, next_index;
    sky_list_t          list;
    sky_object_t        result;
    sky_list_table_t    *table;

    if (!(list = self_data->list)) {
        return NULL;
    }

    result = NULL;
    SKY_ASSET_BLOCK_BEGIN {
        table = sky_list_table(list, sky_list_data(list), SKY_TRUE);
        len = table->len;

        if ((next_index = self_data->next_index) >= 0 && next_index < len) {
            result = table->objects[next_index];
            self_data->next_index = next_index - 1;
        }
        else {
            self_data->next_index = -1;
        }
    } SKY_ASSET_BLOCK_END;

    if (!result) {
        self_data->list = NULL;
    }
    return result;
}

static void
sky_list_reverseiterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_list_iterator_data_t    *self_data = sky_list_iterator_data(self);

    ssize_t             index, len;
    sky_list_table_t    *table;

    index = sky_integer_value(sky_number_index(state),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    if (self_data->list) {
        if (index < -1) {
            index = -1;
        }
        else {
            SKY_ASSET_BLOCK_BEGIN {
                table = sky_list_table(self_data->list,
                                       sky_list_data(self_data->list),
                                       SKY_TRUE);
                if (index > (len = table->len) - 1) {
                    index = len - 1;
                }
            } SKY_ASSET_BLOCK_END;
        }
        self_data->next_index = index;
    }
}


static const char sky_list_type_doc[] =
"list() -> new empty list\n"
"list(iterable) -> new list initialized from an iterable's items";


SKY_TYPE_DEFINE_ITERABLE(list,
                         "list",
                         sizeof(sky_list_data_t),
                         NULL,
                         sky_list_instance_finalize,
                         sky_list_instance_visit,
                         sky_list_instance_iterate,
                         sky_list_instance_iterate_cleanup,
                         0,
                         sky_list_type_doc);


void
sky_list_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_type_setattr_builtin(
            sky_list_type,
            "append",
            sky_function_createbuiltin(
                    "list.append",
                    "L.append(object) -> None -- append object to end",
                    (sky_native_code_function_t)sky_list_append,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "clear",
            sky_function_createbuiltin(
                    "list.clear",
                    "L.clear() -> None -- remove all items from L",
                    (sky_native_code_function_t)sky_list_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "copy",
            sky_function_createbuiltin(
                    "list.copy",
                    "L.copy() -> list -- a shallow copy of L",
                    (sky_native_code_function_t)sky_list_copy,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "count",
            sky_function_createbuiltin(
                    "list.count",
                    "L.count(value) -> integer -- return number of occurrences of value",
                    (sky_native_code_function_t)sky_list_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "extend",
            sky_function_createbuiltin(
                    "list.extend",
                    "L.extend(iterable) -> None -- extend list by appending elements from the iterable",
                    (sky_native_code_function_t)sky_list_extend,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "index",
            sky_function_createbuiltin(
                    "list.index",
                    "L.index(value, [start, [stop]]) -> integer -- return first index of value.\n"
                    "Raises ValueError if the value is not present.",
                    (sky_native_code_function_t)sky_list_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero,
                    "stop", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_create(SSIZE_MAX),
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "insert",
            sky_function_createbuiltin(
                    "list.insert",
                    "L.insert(index, object) -- insert object before index",
                    (sky_native_code_function_t)sky_list_insert,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "index", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, NULL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "pop",
            sky_function_createbuiltin(
                    "list.pop",
                    "L.pop([index]) -> item -- remove and return item at index (default last).\n"
                    "Raises IndexError if list is empty or index is out of range.",
                    (sky_native_code_function_t)sky_list_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "index", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "remove",
            sky_function_createbuiltin(
                    "list.remove",
                    "L.remove(value) -> None -- remove first occurence of value.\n"
                    "Raises ValueError if the value is not present.",
                    (sky_native_code_function_t)sky_list_remove,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "reverse",
            sky_function_createbuiltin(
                    "list.reverse",
                    "L.reverse() -- reverse *IN PLACE*",
                    (sky_native_code_function_t)sky_list_reverse,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_list_type,
            "sort",
            sky_function_createbuiltin(
                    "list.sort",
                    "L.sort(key=None, reverse=False) -> None -- stable sort *IN PLACE*",
                    (sky_native_code_function_t)sky_list_sort,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_LIST, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, sky_None,
                    "reverse", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));

    sky_type_setattr_builtin(sky_list_type, "__hash__", sky_None);
    sky_type_setmethodslotwithparameters(
            sky_list_type,
            "__init__",
            (sky_native_code_function_t)sky_list_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "sequence", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);
    sky_type_setmethodslots(sky_list_type,
            "__repr__", sky_list_repr,
            "__lt__", sky_list_lt,
            "__le__", sky_list_le,
            "__eq__", sky_list_eq,
            "__ne__", sky_list_ne,
            "__gt__", sky_list_gt,
            "__ge__", sky_list_ge,
            "__sizeof__", sky_list_sizeof,
            "__len__", sky_list_len,
            "__getitem__", sky_list_getitem,
            "__setitem__", sky_list_setitem,
            "__delitem__", sky_list_delitem,
            "__iter__", sky_list_iter,
            "__reversed__", sky_list_reversed,
            "__contains__", sky_list_contains,
            "__add__", sky_list_add,
            "__mul__", sky_list_mul,
            "__rmul__", sky_list_mul,
            "__iadd__", sky_list_iadd,
            "__imul__", sky_list_imul,
            NULL);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_list_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("list_iterator"),
                                   NULL,
                                   0,
                                   sizeof(sky_list_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmethodslots(type,
            "__length_hint__", sky_list_iterator_length_hint,
            "__iter__", sky_list_iterator_iter,
            "__next__", sky_list_iterator_next,
            "__reduce__", sky_list_iterator_reduce,
            "__setstate__", sky_list_iterator_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_list_iterator_type),
            type,
            SKY_TRUE);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_list_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("list_reverseiterator"),
                                   NULL,
                                   0,
                                   sizeof(sky_list_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmethodslots(type,
            "__length_hint__", sky_list_reverseiterator_length_hint,
            "__iter__", sky_list_iterator_iter,
            "__next__", sky_list_reverseiterator_next,
            "__reduce__", sky_list_iterator_reduce,
            "__setstate__", sky_list_reverseiterator_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_list_reverseiterator_type),
            type,
            SKY_TRUE);
}
