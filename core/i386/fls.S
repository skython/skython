#include "../sky_asm.h"


.text
.align 2, 0x90


.globl SKY_ASM_MANGLE(fls)
SKY_ASM_TYPE(fls, @function)
SKY_ASM_MANGLE(fls):
    movl    $(-1), %edx
    bsrl    4(%esp), %eax
    cmovel  %edx, %eax
    incl    %eax
    ret
SKY_ASM_SIZE(fls)
