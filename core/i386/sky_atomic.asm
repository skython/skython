.686
.xmm
.model flat, C

.code


sky_atomic_add32 PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 ecx, [esp + 8]
    mov                 edx, eax
    lock xadd           [ecx], edx
    add                 eax, edx
    ret
sky_atomic_add32 ENDP

sky_atomic_add32_old PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 ecx, [esp + 8]
    lock xadd           [ecx], eax
    ret
sky_atomic_add32_old ENDP

sky_atomic_decrement32 PROC EXPORT
    mov                 eax, -1
    mov                 ecx, [esp + 4]
    lock xadd           [ecx], eax
    dec                 eax
    ret
sky_atomic_decrement32 ENDP

sky_atomic_decrement32_old PROC EXPORT
    mov                 eax, -1
    mov                 ecx, [esp + 4]
    lock xadd           [ecx], eax
    ret
sky_atomic_decrement32_old ENDP

sky_atomic_increment32 PROC EXPORT
    mov                 eax, 1
    mov                 ecx, [esp + 4]
    lock xadd           [ecx], eax
    inc                 eax
    ret
sky_atomic_increment32 ENDP

sky_atomic_increment32_old PROC EXPORT
    mov                 eax, 1
    mov                 ecx, [esp + 4]
    lock xadd           [ecx], eax
    ret
sky_atomic_increment32_old ENDP


sky_atomic_add64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, [esp + 12]
    adc                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    add                 eax, [esp + 12]
    adc                 edx, [esp + 16]
    pop                 edi
    pop                 ebx
    ret
sky_atomic_add64 ENDP

sky_atomic_add64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, [esp + 12]
    adc                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_add64_old ENDP

sky_atomic_decrement64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 12]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, -1
    adc                 ecx, -1
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    add                 eax, -1
    adc                 edx, -1
    pop                 edi
    pop                 ebx
    ret
sky_atomic_decrement64 ENDP

sky_atomic_decrement64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 12]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, -1
    adc                 ecx, -1
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_decrement64_old ENDP

sky_atomic_increment64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 12]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, 1
    adc                 ecx, 0
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    add                 eax, 1
    adc                 edx, 0
    pop                 edi
    pop                 ebx
    ret
sky_atomic_increment64 ENDP

sky_atomic_increment64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 12]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    add                 ebx, 1
    adc                 ecx, 0
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_increment64_old ENDP


sky_atomic_cas PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 edx, [esp + 8]
    mov                 ecx, [esp + 12]
    lock cmpxchg        [ecx], edx
    sete                al
    movzx               eax, al
    ret
sky_atomic_cas ENDP

sky_atomic_cas32 PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 edx, [esp + 8]
    mov                 ecx, [esp + 12]
    lock cmpxchg        [ecx], edx
    sete                al
    movzx               eax, al
    ret
sky_atomic_cas32 ENDP

sky_atomic_cas64 PROC EXPORT
    push                ebx
    push                edi
    mov                 eax, [esp + 12]
    mov                 edx, [esp + 16]
    mov                 ebx, [esp + 20]
    mov                 ecx, [esp + 24]
    mov                 edi, [esp + 28]
    lock cmpxchg8b      qword ptr [edi]
    sete                al
    movzx               eax, al
    pop                 edi
    pop                 ebx
    ret
sky_atomic_cas64 ENDP


sky_atomic_exchange PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 ecx, [esp + 8]
    lock xchg           eax, [ecx]
    ret
sky_atomic_exchange ENDP

sky_atomic_exchange32 PROC EXPORT
    mov                 eax, [esp + 4]
    mov                 ecx, [esp + 8]
    lock xchg           eax, [ecx]
    ret
sky_atomic_exchange32 ENDP

sky_atomic_exchange64 PROC EXPORT
    push                ebx
    push                edi
    mov                 ebx, [esp + 12]
    mov                 ecx, [esp + 16]
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_exchange64 ENDP


sky_atomic_and32 PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    and                 edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_and32 ENDP

sky_atomic_and32_old PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    and                 edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    ret
sky_atomic_and32_old ENDP

sky_atomic_or32 PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    or                  edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_or32 ENDP

sky_atomic_or32_old PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    or                  edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    ret
sky_atomic_or32_old ENDP

sky_atomic_xor32 PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    xor                 edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_xor32 ENDP

sky_atomic_xor32_old PROC EXPORT
    mov                 ecx, [esp + 8]
    mov                 eax, [ecx]
retry:
    mov                 edx, eax
    xor                 edx, [esp + 4]
    lock cmpxchg        [ecx], edx
    jnz                 retry
    ret
sky_atomic_xor32_old ENDP


sky_atomic_and64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    and                 ebx, [esp + 12]
    and                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    mov                 eax, ebx
    mov                 edx, ecx
    pop                 edi
    pop                 ebx
    ret
sky_atomic_and64 ENDP

sky_atomic_and64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    and                 ebx, [esp + 12]
    and                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_and64_old ENDP

sky_atomic_or64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    or                  ebx, [esp + 12]
    or                  ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    mov                 eax, ebx
    mov                 edx, ecx
    pop                 edi
    pop                 ebx
    ret
sky_atomic_or64 ENDP

sky_atomic_or64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    or                  ebx, [esp + 12]
    or                  ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_or64_old ENDP

sky_atomic_xor64 PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    xor                 ebx, [esp + 12]
    xor                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    mov                 eax, ebx
    mov                 edx, ecx
    pop                 edi
    pop                 ebx
    ret
sky_atomic_xor64 ENDP

sky_atomic_xor64_old PROC EXPORT
    push                ebx
    push                edi
    mov                 edi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    mov                 ebx, eax
    mov                 ecx, edx
    xor                 ebx, [esp + 12]
    xor                 ecx, [esp + 16]
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 edi
    pop                 ebx
    ret
sky_atomic_xor64_old ENDP


sky_atomic_testandclear PROC EXPORT
    mov                 ecx, [esp + 4]
    mov                 edx, [esp + 8]
    btr                 [edx], ecx
    setc                al
    movzx               eax, al
    ret
sky_atomic_testandclear ENDP

sky_atomic_testandset PROC EXPORT
    mov                 ecx, [esp + 4]
    mov                 edx, [esp + 8]
    bts                 [edx], ecx
    setc                al
    movzx               eax, al
    ret
sky_atomic_testandset ENDP



sky_atomic_lfence PROC EXPORT
    lfence
    ret
sky_atomic_lfence ENDP


sky_atomic_mfence PROC EXPORT
    mfence
    ret
sky_atomic_mfence ENDP


sky_atomic_sfence PROC EXPORT
    sfence
    ret
sky_atomic_sfence ENDP


sky_atomic_pause PROC EXPORT
    pause
    ret
sky_atomic_pause ENDP


END
