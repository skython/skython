.686
.xmm
.model flat, C

.code


sky_atomic_lifo_init PROC EXPORT
    mov                 ecx, [esp + 4]
    mov                 dword ptr [ecx], 0
    mov                 dword ptr [ecx + 4], 0
    mov                 edx, [esp + 8]
    mov                 [ecx + 8], edx
    ret
sky_atomic_lifo_init ENDP


sky_atomic_lifo_push PROC EXPORT
    push                ebx
    push                edi
    push                esi
    mov                 edi, [esp + 16]
    mov                 esi, [esp + 20]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
    mov                 ebx, esi
    add                 esi, [edi + 8]
retry:
    mov                 [esi], eax
    mov                 ecx, edx
    inc                 ecx
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    pop                 esi
    pop                 edi
    pop                 ebx
    ret
sky_atomic_lifo_push ENDP


sky_atomic_lifo_pop PROC EXPORT
    push                ebx
    push                edi
    push                esi
    mov                 edi, [esp + 16]
    mov                 eax, [edi]
    mov                 edx, [edi + 4]
retry:
    test                eax, eax
    jz                  exit
    mov                 esi, eax
    add                 esi, [edi + 8]
    mov                 ebx, [esi]
    mov                 ecx, edx
    inc                 ecx
    lock cmpxchg8b      qword ptr [edi]
    jnz                 retry
    mov                 esi, eax
    add                 esi, [edi + 8]
    mov                 dword ptr [esi], 0
exit:
    pop                 esi
    pop                 edi
    pop                 ebx
    ret
sky_atomic_lifo_pop ENDP


END
