/** @file
  * @brief
  * @defgroup sky_exceptions Exceptions
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_EXCEPTIONS_H__
#define __SKYTHON_CORE_SKY_EXCEPTIONS_H__ 1


#include "sky_base.h"
#include "sky_object_types.h"


SKY_CDECLS_BEGIN


/** Declare the C type and Skython type descriptor to represent an exception.
  *
  * @param[in]  name        the base name of the object as it is used in code
  *                         (e.g., "cond")
  * @param[in]  fullname    the descriptive name of the object as it is used in
  *                         English (e.g., "condition")
  */
#define SKY_EXCEPTION_DECLARE(name, fullname)                           \
    /** The C type representing a fullname exception type. **/          \
    typedef struct sky_##name##_s *sky_##name##_t;                      \
    /** The Skython type object instance for a fullname exception. **/  \
    SKY_EXTERN sky_type_t const sky_##name


/* Base classes */
SKY_EXCEPTION_DECLARE(BaseException, BaseException);
SKY_EXCEPTION_DECLARE(Exception, Exception);
SKY_EXCEPTION_DECLARE(ArithmeticError, ArithmeticError);
SKY_EXCEPTION_DECLARE(BufferError, BufferError);
SKY_EXCEPTION_DECLARE(LookupError, LookupError);

/* Concrete exceptions */
SKY_EXCEPTION_DECLARE(AssertionError, AssertionError);
SKY_EXCEPTION_DECLARE(AttributeError, AttributeError);
SKY_EXCEPTION_DECLARE(EOFError, EOFError);
SKY_EXCEPTION_DECLARE(FloatingPointError, FloatingPointError);
SKY_EXCEPTION_DECLARE(GeneratorExit, GeneratorExit);
SKY_EXCEPTION_DECLARE(ImportError, ImportError);
SKY_EXCEPTION_DECLARE(IndexError, IndexError);
SKY_EXCEPTION_DECLARE(KeyError, KeyError);
SKY_EXCEPTION_DECLARE(KeyboardInterrupt, KeyboardInterrupt);
SKY_EXCEPTION_DECLARE(MemoryError, MemoryError);
SKY_EXCEPTION_DECLARE(NameError, NameError);
SKY_EXCEPTION_DECLARE(NotImplementedError, NotImplementedError);
SKY_EXCEPTION_DECLARE(OSError, OSError);
SKY_EXCEPTION_DECLARE(OverflowError, OverflowError);
SKY_EXCEPTION_DECLARE(ReferenceError, ReferenceError);
SKY_EXCEPTION_DECLARE(RuntimeError, RuntimeError);
SKY_EXCEPTION_DECLARE(StopIteration, StopIteration);
SKY_EXCEPTION_DECLARE(SyntaxError, SyntaxError);
SKY_EXCEPTION_DECLARE(IndentationError, IndentationError);
SKY_EXCEPTION_DECLARE(TabError, TabError);
SKY_EXCEPTION_DECLARE(SystemError, SystemError);
SKY_EXCEPTION_DECLARE(SystemExit, SystemExit);
SKY_EXCEPTION_DECLARE(TypeError, TypeError);
SKY_EXCEPTION_DECLARE(UnboundLocalError, UnboundLocalError);
SKY_EXCEPTION_DECLARE(UnicodeError, UnicodeError);
SKY_EXCEPTION_DECLARE(UnicodeEncodeError, UnicodeEncodeError);
SKY_EXCEPTION_DECLARE(UnicodeDecodeError, UnicodeDecodeError);
SKY_EXCEPTION_DECLARE(UnicodeTranslateError, UnicodeTranslateError);
SKY_EXCEPTION_DECLARE(ValueError, ValueError);
SKY_EXCEPTION_DECLARE(ZeroDivisionError, ZeroDivisionError);

/* OS exceptions */
SKY_EXCEPTION_DECLARE(BlockingIOError, BlockingIOError);
SKY_EXCEPTION_DECLARE(ChildProcessError, ChildProcessError);
SKY_EXCEPTION_DECLARE(ConnectionError, ConnectionError);
SKY_EXCEPTION_DECLARE(BrokenPipeError, BrokenPipeError);
SKY_EXCEPTION_DECLARE(ConnectionAbortedError, ConnectionAbortedError);
SKY_EXCEPTION_DECLARE(ConnectionRefusedError, ConnectionRefusedError);
SKY_EXCEPTION_DECLARE(ConnectionResetError, ConnectionResetError);
SKY_EXCEPTION_DECLARE(FileExistsError, FileExistsError);
SKY_EXCEPTION_DECLARE(FileNotFoundError, FileNotFoundError);
SKY_EXCEPTION_DECLARE(InterruptedError, InterruptedError);
SKY_EXCEPTION_DECLARE(IsADirectoryError, IsADirectoryError);
SKY_EXCEPTION_DECLARE(NotADirectoryError, NotADirectoryError);
SKY_EXCEPTION_DECLARE(PermissionError, PermissionError);
SKY_EXCEPTION_DECLARE(ProcessLookupError, ProcessLookupError);
SKY_EXCEPTION_DECLARE(TimeoutError, TimeoutError);

/* Warnings */
SKY_EXCEPTION_DECLARE(Warning, Warning);
SKY_EXCEPTION_DECLARE(UserWarning, UserWarning);
SKY_EXCEPTION_DECLARE(DeprecationWarning, DeprecationWarning);
SKY_EXCEPTION_DECLARE(PendingDeprecationWarning, PendingDeprecationWarning);
SKY_EXCEPTION_DECLARE(SyntaxWarning, SyntaxWarning);
SKY_EXCEPTION_DECLARE(RuntimeWarning, RuntimeWarning);
SKY_EXCEPTION_DECLARE(FutureWarning, FutureWarning);
SKY_EXCEPTION_DECLARE(ImportWarning, ImportWarning);
SKY_EXCEPTION_DECLARE(UnicodeWarning, UnicodeWarning);
SKY_EXCEPTION_DECLARE(BytesWarning, BytesWarning);
SKY_EXCEPTION_DECLARE(ResourceWarning, ResourceWarning);


/** Return the cause for an exception (exception chaining).
  *
  * @param[in]  exception   the exception for which the cause is to be returned.
  * @return     the cause for the exception.
  */
SKY_EXTERN sky_object_t
sky_BaseException_cause(sky_object_t exception);


/** Set the cause for an exception (exception chaining).
  *
  * @param[in]  exception   the exception for which the cause is to be set.
  * @param[in]  cause       the cause to set.
  */
SKY_EXTERN void
sky_BaseException_setcause(sky_object_t exception, sky_object_t cause);


/** Return the context for an exception.
  *
  * @param[in]  exception   the exception for which the context is to be
  *                         returned.
  * @return     the context for the exception.
  */
SKY_EXTERN sky_object_t
sky_BaseException_context(sky_object_t exception);


/** Set the context for an exception.
  *
  * @param[in]  exception   the exception for which the context is to be set.
  * @param[in]  context     the context to set.
  */
SKY_EXTERN void
sky_BaseException_setcontext(sky_object_t exception, sky_object_t context);


/** Return the traceback for an exception.
  *
  * @param[in]  exception   the exception for which the traceback is to be
  *                         returned.
  * @return     the traceback for the exception.
  */
SKY_EXTERN sky_traceback_t
sky_BaseException_traceback(sky_object_t exception);


/** Set the traceback for an exception.
  *
  * @param[in]  exception   the exception for which the traceback is to be set.
  * @param[in]  traceback   the traceback to set.
  */
SKY_EXTERN void
sky_BaseException_settraceback(sky_object_t     exception,
                               sky_traceback_t  traceback);


/** Return whether the context of an exception should be suppressed when the
  * exception is displayed.
  *
  * @param[in]  exception   the exception for which the suppression of context
  *                         state should be returned.
  */
SKY_EXTERN sky_bool_t
sky_BaseException_suppress_context(sky_object_t exception);


/** Return the errno code for an OSError exception.
  *
  * @param[in]  exception   the exception for which the errno is to be
  *                         returned.
  * @return     the errno.
  */
SKY_EXTERN int
sky_OSError_errno(sky_object_t exception);


/** Return the encoding for a UnicodeError exception.
  *
  * @param[in]  exception   the exception for which the encoding is to be
  *                         returned.
  * @return     the encoding.
  */
SKY_EXTERN sky_string_t
sky_UnicodeError_encoding(sky_object_t exception);


/** Return the ending position for a UnicodeError exception.
  *
  * @param[in]  exception   the exception for which the ending position is
  *                         to be returned.
  * @return     the ending position.
  */
SKY_EXTERN ssize_t
sky_UnicodeError_end(sky_object_t exception);


/** Return the input object for a UnicodeError exception.
  *
  * @param[in]  exception   the exception for which the input object is to be
  *                         returned.
  * @return     the input object.
  */
SKY_EXTERN sky_object_t
sky_UnicodeError_object(sky_object_t exception);


/** Return the starting position for a UnicodeError exception.
  *
  * @param[in]  exception   the exception for which the starting position is
  *                         to be returned.
  * @return     the starting position.
  */
SKY_EXTERN ssize_t
sky_UnicodeError_start(sky_object_t exception);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_EXCEPTIONS_H__ */

/** @} **/
