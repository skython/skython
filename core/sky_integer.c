#include "sky_private.h"
#include "sky_integer_private.h"

#include <float.h>  /* for DBL_MAX_EXP */
#include <math.h>   /* for ldexp() */

#include <locale.h>
#if defined(HAVE_XLOCALE_H)
#   include <xlocale.h>
#endif


#if INT_MAX == INT64_MAX
#   define INT_BITS 64
#elif INT_MAX == INT32_MAX
#   define INT_BITS 32
#else
#   error implementation missing
#endif

#if LONG_MAX == INT64_MAX
#   define LONG_BITS 64
#elif LONG_MAX == INT32_MAX
#   define LONG_BITS 32
#else
#   error implementation missing
#endif

#if INTMAX_MAX == INT64_MAX
#   define INTMAX_BITS 64
#elif INTMAX_MAX == INT32_MAX
#   define INTMAX_BITS 32
#else
#   error implementation missing
#endif


sky_integer_data_t *
sky_integer_data(sky_object_t object, sky_integer_data_t *tagged_data)
{
    switch (SKY_OBJECT_TAG(object)) {
        case 0:
            if (sky_object_type(object) == sky_integer_type) {
                return (sky_integer_data_t *)
                       ((char *)object + sizeof(sky_object_data_t));
            }
            break;
        case 1:
            tagged_data->mpz[0]._mp_alloc = 1;
            tagged_data->mpz[0]._mp_size = 1;
            tagged_data->mpz[0]._mp_d = &(tagged_data->value);
            tagged_data->mpz[0]._mp_d[0] = ((uintptr_t)object >> 4);
            if (!tagged_data->value) {
                tagged_data->mpz[0]._mp_size = 0;
            }
            return tagged_data;
        case 2:
            tagged_data->mpz[0]._mp_alloc = 1;
            tagged_data->mpz[0]._mp_size = -1;
            tagged_data->mpz[0]._mp_d = &(tagged_data->value);
            tagged_data->mpz[0]._mp_d[0] = ((uintptr_t)object >> 4);
            sky_error_validate_debug(tagged_data->value != 0);
            return tagged_data;
        case 7:
            switch (((uintptr_t)object >> 12) & 0xF) {
                case 0:
                    tagged_data->mpz[0]._mp_alloc = 1;
                    tagged_data->mpz[0]._mp_d = &(tagged_data->value);
                    switch (((uintptr_t)object >> 8) & 0xF) {
                        case 4:
                            tagged_data->mpz[0]._mp_size = 0;
                            tagged_data->value = 0;
                            return tagged_data;
                        case 5:
                            tagged_data->mpz[0]._mp_size = 1;
                            tagged_data->value = 1;
                            return tagged_data;
                    }
                    break;
                case 2:
                    tagged_data->mpz[0]._mp_alloc = 1;
                    tagged_data->mpz[0]._mp_d = &(tagged_data->value);
                    switch (((uintptr_t)object >> 8) & 0xF) {
                        case 0: /* UINT32_MAX */
                            tagged_data->mpz[0]._mp_size = 1;
                            tagged_data->value = 0xFFFFFFFFul;
                            return tagged_data;
                        case 1: /* INT32_MIN */
                            tagged_data->mpz[0]._mp_size = -1;
                            tagged_data->value = 0x80000000ul;
                            return tagged_data;
                        case 2: /* INT32_MAX */
                            tagged_data->mpz[0]._mp_size = 1;
                            tagged_data->value = 0x7FFFFFFFul;
                            return tagged_data;
                        case 3: /* UINT64_MAX */
                            tagged_data->mpz[0]._mp_size = 1;
                            tagged_data->value = 0xFFFFFFFFFFFFFFFFull;
                            return tagged_data;
                        case 4: /* INT64_MAX */
                            tagged_data->mpz[0]._mp_size = -1;
                            tagged_data->value = 0x8000000000000000ull;
                            return tagged_data;
                        case 5: /* INT64_MIN */
                            tagged_data->mpz[0]._mp_size = 1;
                            tagged_data->value = 0x7FFFFFFFFFFFFFFFull;
                            return tagged_data;
                    }
                    break;
            }
            break;
    }

    /* This is going to raise an error. */
    return sky_object_data(object, sky_integer_type);
}


static sky_integer_t
sky_integer_cached_signed(intmax_t value)
{
    if (value >= 0) {
        if ((uintmax_t)value <= (UINTPTR_MAX >> 4)) {
            return SKY_OBJECT_MAKETAG(0x1, value);
        }
        if (UINT32_MAX == (uintmax_t)value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x200);
        }
        if (INT32_MAX == (uintmax_t)value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x220);
        }
        if (UINT64_MAX == (uintmax_t)value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x230);
        }
        if (INT64_MAX == (uintmax_t)value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x250);
        }
    }
    else {
        if ((uintmax_t)-value <= (UINTPTR_MAX >> 4)) {
            return SKY_OBJECT_MAKETAG(0x2, -value);
        }
        if (INT32_MIN == value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x210);
        }
        if (INT64_MIN == value) {
            return SKY_OBJECT_MAKETAG(0x7, 0x240);
        }
    }

    return NULL;
}


static sky_integer_t
sky_integer_cached_unsigned(uintmax_t value)
{
    if (value <= (UINTPTR_MAX >> 4)) {
        return SKY_OBJECT_MAKETAG(0x1, value);
    }
    if (UINT32_MAX == (uintmax_t)value) {
        return SKY_OBJECT_MAKETAG(0x7, 0x200);
    }
    if (INT32_MAX == (uintmax_t)value) {
        return SKY_OBJECT_MAKETAG(0x7, 0x220);
    }
    if (UINT64_MAX == (uintmax_t)value) {
        return SKY_OBJECT_MAKETAG(0x7, 0x230);
    }
    if (INT64_MAX == (uintmax_t)value) {
        return SKY_OBJECT_MAKETAG(0x7, 0x250);
    }
    return NULL;
}


static sky_integer_t
sky_integer_cached_mpz(mpz_ptr mpz)
{
    if (!mpz->_mp_size) {
        return sky_integer_zero;
    }
    if (1 == mpz->_mp_size) {
        return sky_integer_cached_unsigned(mpz->_mp_d[0]);
    }
    else if (-1 == mpz->_mp_size &&
             (uintmax_t)mpz->_mp_d[0] < ((uintmax_t)1 << (GMP_NUMB_BITS - 1)) - 1)
    {
        return sky_integer_cached_signed(-mpz->_mp_d[0]);
    }
    return NULL;
}


static sky_integer_t
sky_integer_cached(sky_integer_t integer)
{
    sky_integer_t       cached_integer;
    sky_integer_data_t  *integer_data;

    /* This will be TRUE for any valid tagged integer, making the NULL in the
     * call to sky_integer_data() SAFE.
     */
    if (sky_integer_type == sky_object_type(integer)) {
        return integer;
    }
    integer_data = sky_integer_data(integer, NULL);
    cached_integer = sky_integer_cached_mpz(integer_data->mpz);
    return (cached_integer ? cached_integer : integer);
}


static sky_integer_t
sky_integer_clone(sky_integer_t self, sky_type_t type, sky_bool_t immutable)
{
    sky_integer_t       clone;
    sky_integer_data_t  *clone_data, *self_data, tagged_self_data;

    if (!type) {
        type = sky_integer_type;
    }

    if (immutable && sky_object_istagged(self) && sky_integer_type == type) {
        if (sky_object_type(self) == sky_boolean_type) {
            sky_error_validate_debug(sky_True == (sky_object_t)self ||
                                     sky_False == (sky_object_t)self);
            return (sky_True == (sky_object_t)self ? sky_integer_one
                                                   : sky_integer_zero);
        }
        sky_error_validate_debug(sky_object_type(self) == sky_integer_type);
        return self;
    }
    self_data = sky_integer_data(self, &tagged_self_data);

    clone = sky_object_allocate(type);
    clone_data = sky_integer_data(clone, NULL);
    clone_data->mpz[0] = self_data->mpz[0];
    clone_data->value = self_data->value;
    if (immutable && self_data->mpz[0]._mp_d == &(self_data->value)) {
        clone_data->mpz[0]._mp_d = &(clone_data->value);
    }
    else {
        clone_data->mpz[0]._mp_d =
            sky_memdup(self_data->mpz[0]._mp_d,
                       clone_data->mpz[0]._mp_alloc * sizeof(mp_limb_t));
    }

    return clone;
}


static size_t
sky_integer_data_nbits(sky_integer_data_t *integer_data)
{
    return mpz_sizeinbase(integer_data->mpz, 2);
}


static int
sky_integer_bits_in_limb(mp_limb_t limb)
{
    static const int table[32] = {
        0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
    };

    int bits;

    for (bits = 0; limb >= 32; bits += 6, limb >>= 6);
    return bits + table[limb];
}


static void
sky_integer_data_normalize(sky_integer_data_t *integer_data)
{
    int size = sky_integer_data_size(integer_data);

    while (size > 0 && !integer_data->mpz[0]._mp_d[size - 1]) {
        --size;
    }
    if (integer_data->mpz[0]._mp_size < 0) {
        size = -size;
    }
    integer_data->mpz[0]._mp_size = size;
    if (1 == integer_data->mpz[0]._mp_size &&
        integer_data->mpz[0]._mp_d != &(integer_data->value))
    {
        integer_data->value = integer_data->mpz[0]._mp_d[0];
        sky_free(integer_data->mpz[0]._mp_d);
        integer_data->mpz[0]._mp_d = &(integer_data->value);
    }
}


sky_integer_t
sky_integer_createwithmpz(sky_type_t cls, mpz_ptr mpz)
{
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    if (sky_integer_type == cls) {
        if ((integer = sky_integer_cached_mpz(mpz)) != NULL) {
            mpz_clear(mpz);
            return integer;
        }
        integer = sky_object_allocate(cls);
    }
    else {
        integer = sky_object_call(cls, NULL, NULL);
        if (!sky_object_isa(integer, sky_integer_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%#@ does not produce a sub-type of %#@",
                    sky_type_name(cls),
                    sky_type_name(sky_integer_type));
        }
    }
    integer_data = sky_integer_data(integer, NULL);
    integer_data->mpz[0] = mpz[0];
    sky_integer_data_normalize(integer_data);

    return integer;
}


static void
sky_integer_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_integer_data_t  *integer_data = data;

    if (integer_data->mpz[0]._mp_d != &(integer_data->value)) {
        sky_free(integer_data->mpz[0]._mp_d);
    }
    integer_data->mpz[0]._mp_d = NULL;
}


sky_object_t
sky_integer_abs(sky_integer_t self)
{
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    if (SKY_OBJECT_TAG(self) == 1) {
        return self;
    }
    if (SKY_OBJECT_TAG(self) == 2) {
        return SKY_OBJECT_MAKETAG(1, ((uintptr_t)self >> 4));
    }

    if (!sky_integer_isnegative(self)) {
        integer = sky_integer_copy(self);
    }
    else {
        integer = sky_integer_clone(self, NULL, SKY_FALSE);
        integer_data = sky_integer_data(integer, NULL);
        mpz_abs(integer_data->mpz, integer_data->mpz);
    }

    return sky_integer_cached(integer);
}


#define SKY_INTEGER_BINARY_OP_CHECK(s, o)                   \
        do {                                                \
            if (!sky_object_isa((s), sky_integer_type) ||   \
                !sky_object_isa((o), sky_integer_type))     \
            {                                               \
                return sky_NotImplemented;                  \
            }                                               \
        } while (0)


sky_object_t
sky_integer_add(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_add(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_object_t
sky_integer_and(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (self == other) {
        return self;
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_and(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


ssize_t
sky_integer_bit_length(sky_integer_t self)
{
    size_t              nbits;
    sky_integer_data_t  *self_data, tagged_data;

    self_data = sky_integer_data(self, &tagged_data);
    if ((nbits = sky_integer_data_nbits(self_data)) > SSIZE_MAX) {
        return -1;
    }
    return (ssize_t)nbits;
}


sky_bool_t
sky_integer_bool(sky_integer_t self)
{
    return !sky_integer_iszero(self);
}


sky_object_t
sky_integer_compare(sky_object_t        object_1,
                    sky_object_t        object_2,
                    sky_compare_op_t    compare_op)
{
    int                 cmprc;
    uintptr_t           value_1, value_2;
    sky_integer_data_t  tagged_data_1, tagged_data_2;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (object_1 == object_2) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (object_1 == object_2) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (SKY_OBJECT_TAG(object_1) == 1 && SKY_OBJECT_TAG(object_2) == 1) {
        value_1 = (uintptr_t)object_1 >> 4;
        value_2 = (uintptr_t)object_2 >> 4;
        switch (compare_op) {
            case SKY_COMPARE_OP_LESS:
                return (value_1 < value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_LESS_EQUAL:
                return (value_1 <= value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_EQUAL:
                return (value_1 == value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_NOT_EQUAL:
                return (value_1 != value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER:
                return (value_1 > value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER_EQUAL:
                return (value_1 >= value_2 ? sky_True : sky_False);

            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
        }
    }
    else if (SKY_OBJECT_TAG(object_1) == 1 && SKY_OBJECT_TAG(object_2) == 2) {
        sky_error_validate_debug(((uintptr_t)object_2 >> 4) > 0);
        switch (compare_op) {
            case SKY_COMPARE_OP_LESS:
            case SKY_COMPARE_OP_LESS_EQUAL:
            case SKY_COMPARE_OP_EQUAL:
                return sky_False;
            case SKY_COMPARE_OP_NOT_EQUAL:
            case SKY_COMPARE_OP_GREATER:
            case SKY_COMPARE_OP_GREATER_EQUAL:
                return sky_True;

            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
        }
    }
    else if (SKY_OBJECT_TAG(object_1) == 2 && SKY_OBJECT_TAG(object_2) == 1) {
        sky_error_validate_debug(((uintptr_t)object_1 >> 4) > 0);
        switch (compare_op) {
            case SKY_COMPARE_OP_LESS:
            case SKY_COMPARE_OP_LESS_EQUAL:
            case SKY_COMPARE_OP_NOT_EQUAL:
                return sky_True;
            case SKY_COMPARE_OP_EQUAL:
            case SKY_COMPARE_OP_GREATER:
            case SKY_COMPARE_OP_GREATER_EQUAL:
                return sky_False;

            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
        }
    }
    else if (SKY_OBJECT_TAG(object_1) == 2 && SKY_OBJECT_TAG(object_2) == 2) {
        sky_error_validate_debug(((uintptr_t)object_1 >> 4) > 0);
        sky_error_validate_debug(((uintptr_t)object_2 >> 4) > 0);
        value_1 = (uintptr_t)object_1 >> 4;
        value_2 = (uintptr_t)object_2 >> 4;
        switch (compare_op) {
            case SKY_COMPARE_OP_LESS:
                return (value_1 > value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_LESS_EQUAL:
                return (value_1 >= value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_EQUAL:
                return (value_1 == value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_NOT_EQUAL:
                return (value_1 != value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER:
                return (value_1 < value_2 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER_EQUAL:
                return (value_1 <= value_2 ? sky_True : sky_False);

            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
        }
    }
    else {
        if (!sky_object_isa(object_1, sky_integer_type) ||
            !sky_object_isa(object_2, sky_integer_type))
        {
            return sky_NotImplemented;
        }

        cmprc = mpz_cmp(sky_integer_data(object_1, &tagged_data_1)->mpz,
                        sky_integer_data(object_2, &tagged_data_2)->mpz);
        switch (compare_op) {
            case SKY_COMPARE_OP_LESS:
                return (cmprc < 0 ? sky_True : sky_False);
            case SKY_COMPARE_OP_LESS_EQUAL:
                return (cmprc <= 0 ? sky_True : sky_False);
            case SKY_COMPARE_OP_EQUAL:
                return (cmprc == 0 ? sky_True : sky_False);
            case SKY_COMPARE_OP_NOT_EQUAL:
                return (cmprc != 0 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER:
                return (cmprc > 0 ? sky_True : sky_False);
            case SKY_COMPARE_OP_GREATER_EQUAL:
                return (cmprc >= 0 ? sky_True : sky_False);

            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
        }
    }

    sky_error_fatal("internal error");
}


sky_complex_t
sky_integer_complex(sky_integer_t self)
{
    return sky_complex_create(sky_integer_value_double(self), 0.0);
}


sky_integer_t
sky_integer_copy(sky_integer_t self)
{
    if (sky_integer_type == sky_object_type(self)) {
        return self;
    }
    return sky_integer_clone(self, NULL, SKY_TRUE);
}


sky_integer_t
sky_integer_create(intmax_t value)
{
    int                 size;
    uintmax_t           abs_value;
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    if ((integer = sky_integer_cached_signed(value)) != NULL) {
        return integer;
    }
    
    integer = sky_object_allocate(sky_integer_type);
    integer_data = sky_integer_data(integer, NULL);

    if (value < 0) {
        abs_value = (uintmax_t)(-1 - value) + 1;
        size = -1;
    }
    else {
        abs_value = value;
        size = 1;
    }

    if (!(abs_value & ~(uintmax_t)GMP_NUMB_MASK)) {
        integer_data->mpz[0]._mp_alloc = 1;
        integer_data->mpz[0]._mp_size = size;
        integer_data->mpz[0]._mp_d = &(integer_data->value);
        integer_data->value = (mp_limb_t)abs_value;
    }
#if INTMAX_BITS > GMP_NUMB_BITS
    else {
        int i;

        size *= ((sizeof(abs_value) * 8) / GMP_NUMB_BITS);
        integer_data->mpz[0]._mp_d = sky_calloc(abs(size), sizeof(mp_limb_t));
        integer_data->mpz[0]._mp_alloc =
                sky_memsize(integer_data->mpz[0]._mp_d) / sizeof(mp_limb_t);
        integer_data->mpz[0]._mp_size = size;
        for (i = 0; abs_value; ++i) {
            integer_data->mpz[0]._mp_d[i] = abs_value & GMP_NUMB_MASK;
            abs_value >>= GMP_NUMB_BITS;
        }
        sky_integer_data_normalize(integer_data);
    }
#endif

    return integer;
}


sky_integer_t
sky_integer_createfromascii(const char *bytes, size_t nbytes, int base)
{
    static const uint8_t values[256] = {
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
         0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 37, 37, 37, 37, 37, 37, 
        37, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 
        25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 37, 37, 37, 37, 
        37, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 
        25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
        37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 37, 
    };

    sky_integer_t   integer;

    SKY_ASSET_BLOCK_BEGIN {
        int         sign;
        char        c;
        mpz_t       mpz;
        uint8_t     *v, value_buffer[32], *value_str;
        const char  *end, *s, *str;

        /* Skip leading whitespace. */
        end = bytes + nbytes;
        for (str = bytes; str < end && sky_ctype_isspace(*str); ++str);
        if (str >= end) {
            sky_error_raise_format(sky_ValueError,
                                   "invalid literal for int() with base %d",
                                   (base ? base : 10));
        }

        /* Handle optional leading sign. */
        if (*str == '-') {
            sign = -1;
            ++str;
        }
        else {
            sign = 1;
            if (*str == '+') {
                ++str;
            }
        }

        /* If base is 0, figure out the base to use from the source. */
        if (!base) {
            c = (str < end ? *str : 0);
            if (c != '0') {
                base = 10;
            }
            else {
                c = (str + 1 < end ? str[1] : 0);
                if (c == 'x' || c == 'X') {
                    base = 16;
                }
                else if (c == 'o' || c == 'O') {
                    base = 8;
                }
                else if (c == 'b' || c == 'B') {
                    base = 2;
                }
                else {
                    /* C-style octal literals are not valid, but 0 is valid. */
                    while (str < end && *str == '0') {
                        ++str;
                    }
                    mpz[0]._mp_alloc = 0;
                    mpz[0]._mp_size = 0;
                    mpz[0]._mp_d = NULL;
                    goto finished;
                }
            }
        }

        /* Handle (optional if base originally != 0) acceptable base prefixes. */
        if (str + 1 < end && str[0] == '0' &&
            ((base == 16 && (str[1] == 'x' || str[1] == 'X')) ||
             (base ==  8 && (str[1] == 'o' || str[1] == 'O')) ||
             (base ==  2 && (str[1] == 'b' || str[1] == 'B'))))
        {
            str += 2;
        }

        /* Scan the input string to make sure it contains only valid characters,
         * because mpn_set_str() does no validation of its own. After this loop
         * s - str will be the length of the string to pass mpn_set_str().
         */
        if (end - str <= (ptrdiff_t)sizeof(value_buffer)) {
            v = value_str = value_buffer;
        }
        else {
            v = value_str = sky_asset_malloc(end - str, SKY_ASSET_CLEANUP_ALWAYS);
        }
        for (s = str; s < end && (*v++ = values[(uint8_t)*s]) < base; ++s);

        mpz[0]._mp_alloc =
                2 + (mp_size_t)((s - str) /
                                (GMP_NUMB_BITS *
                                 (0.69314718055994530942 / log((double)base))));
        mpz[0]._mp_d = sky_malloc(mpz[0]._mp_alloc * sizeof(mp_limb_t));
        sky_asset_save(mpz, (sky_free_t)mpz_clear, SKY_ASSET_CLEANUP_ON_ERROR);
        mpz[0]._mp_size = mpn_set_str(mpz[0]._mp_d, value_str, s - str, base);
        str = s;

    finished:
        /* Skip trailing whitespace. */
        while (str < end && sky_ctype_isspace(*str)) {
            ++str;
        }
        if (str < end) {
            sky_error_raise_format(sky_ValueError,
                                   "invalid literal for int() with base %d",
                                   (base ? base : 10));
        }

        if (sign < 0) {
            mpz[0]._mp_size = -mpz[0]._mp_size;
        }
        integer = sky_integer_createwithmpz(sky_integer_type, mpz);
    } SKY_ASSET_BLOCK_END;

    return integer;
}


sky_integer_t
sky_integer_createfrombytes(const void *bytes,
                            ssize_t     nbytes,
                            sky_bool_t  little_endian,
                            sky_bool_t  is_signed)
{
    int     endian;
    mpz_t   mpz;

    endian = (little_endian ? -1 : 1);

    mpz_init(mpz);
    mpz_import(mpz, nbytes, endian, 1, endian, 0, bytes);

    if (is_signed && mpz_tstbit(mpz, (nbytes * 8) - 1)) {
        mpz_t   shiftz;

        mpz_init_set_ui(shiftz, 1);
        mpz_mul_2exp(shiftz, shiftz, nbytes * 8);
        mpz[0]._mp_size = -mpz[0]._mp_size;
        mpz_add(mpz, mpz, shiftz);
        mpz[0]._mp_size = -mpz[0]._mp_size;
        mpz_clear(shiftz);
    }

    /* Create the integer object instance to return. This call will take
     * ownership of mpz, either giving it to the instance or cleaning it up
     * immediately in the case where mpz represents a cached integer.
     */
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_integer_t
sky_integer_createfromdouble(double value)
{
    mpz_t   mpz;

    if (isinf(value)) {
        sky_error_raise_string(sky_OverflowError,
                               "cannot convert float infinity to integer");
    }
    if (isnan(value)) {
        sky_error_raise_string(sky_OverflowError,
                               "cannot convert float NaN to integer");
    }

    mpz_init_set_d(mpz, value);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_integer_t
sky_integer_createfromstring(sky_object_t source, int base)
{
    sky_integer_t   result;

    if (base && (base < 2 || base > 36)) {
        sky_error_raise_string(sky_ValueError,
                               "int() base must be >= 2 and <= 36");
    }

    SKY_ASSET_BLOCK_BEGIN {
        const char  *c, *end;

        if (sky_object_isa(source, sky_string_type)) {
            if (!(source = sky_string_transform_numeric(source))) {
                sky_error_raise_format(sky_ValueError,
                                       "invalid literal for int() with base %d",
                                       (base ? base : 10));
            }

            c = sky_string_cstring(source);
            end = c + sky_object_len(source);
        }
        else {
            sky_buffer_t    buffer;

            sky_buffer_acquire(&buffer, source, SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);

            c = buffer.buf;
            end = buffer.buf + buffer.len;
        }
        result = sky_integer_createfromascii(c, end - c, base);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_integer_t
sky_integer_createfromunsigned(uintmax_t value)
{
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    if ((integer = sky_integer_cached_unsigned(value)) != NULL) {
        return integer;
    }
    
    integer = sky_object_allocate(sky_integer_type);
    integer_data = sky_integer_data(integer, NULL);

    if (!(value & ~(uintmax_t)GMP_NUMB_MASK)) {
        integer_data->mpz[0]._mp_alloc = 1;
        integer_data->mpz[0]._mp_size = 1;
        integer_data->mpz[0]._mp_d = &(integer_data->value);
        integer_data->value = (mp_limb_t)value;
    }
#if INTMAX_BITS > GMP_NUMB_BITS
    else {
        int i, size;

        size = ((sizeof(value) * 8) / GMP_NUMB_BITS);
        integer_data->mpz[0]._mp_d = sky_calloc(abs(size), sizeof(mp_limb_t));
        integer_data->mpz[0]._mp_alloc =
                sky_memsize(integer_data->mpz[0]._mp_d) / sizeof(mp_limb_t);
        integer_data->mpz[0]._mp_size = size;
        for (i = 0; value; ++i) {
            integer_data->mpz[0]._mp_d[i] = value & GMP_NUMB_MASK;
            value >>= GMP_NUMB_BITS;
        }
        sky_integer_data_normalize(integer_data);
    }
#endif

    return integer;
}


static sky_object_t
sky_integer_denominator_getter(SKY_UNUSED sky_object_t  self,
                               SKY_UNUSED sky_type_t    type)
{
    return sky_integer_one;
}


sky_object_t
sky_integer_divmod(sky_integer_t self, sky_object_t other)
{
    mpz_t               quotient_mpz, remainder_mpz;
    sky_integer_t       quotient, remainder;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    if (sky_integer_data_iszero(other_data)) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "integer division or modulo by zero");
    }

    mpz_init(quotient_mpz);
    mpz_init(remainder_mpz);
    mpz_fdiv_qr(quotient_mpz, remainder_mpz, self_data->mpz, other_data->mpz);

    quotient = sky_integer_createwithmpz(sky_integer_type, quotient_mpz);
    remainder = sky_integer_createwithmpz(sky_integer_type, remainder_mpz);
    return sky_tuple_pack(2, quotient, remainder);
}


sky_object_t
sky_integer_divmod_near(sky_integer_t self, sky_object_t other)
{
    mpz_t               doubled_mpz, quotient_mpz, remainder_mpz;
    sky_integer_t       quotient, remainder;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (sky_integer_iszero(other)) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "integer division or modulo by zero");
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(quotient_mpz);
    mpz_init(remainder_mpz);
    mpz_fdiv_qr(quotient_mpz, remainder_mpz, self_data->mpz, other_data->mpz);

    mpz_init(doubled_mpz);
    mpz_mul_ui(doubled_mpz, remainder_mpz, 2);
    if (mpz_cmp(doubled_mpz, remainder_mpz) > 0 ||
        (mpz_even_p(quotient_mpz) && !mpz_cmp(doubled_mpz, remainder_mpz)))
    {
        mpz_add_ui(quotient_mpz, quotient_mpz, 1);
        mpz_sub(remainder_mpz, remainder_mpz, other_data->mpz);
    }

    quotient = sky_integer_createwithmpz(sky_integer_type, quotient_mpz);
    remainder = sky_integer_createwithmpz(sky_integer_type, remainder_mpz);
    return sky_tuple_pack(2, quotient, remainder);
}


sky_object_t
sky_integer_eq(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


void
sky_integer_export(sky_integer_t    self,
                   uint8_t *        bytes,
                   size_t           nbytes,
                   sky_bool_t       little_endian,
                   sky_bool_t       is_signed)
{
    int                 fill;
    mpz_t               negative_mpz;
    size_t              count, written;
    mpz_ptr             mpz;
    sky_integer_data_t  *self_data, self_tagged_data;

    if (sky_integer_iszero(self)) {
        memset(bytes, 0, nbytes);
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        self_data = sky_integer_data(self, &self_tagged_data);
        count = mpz_sizeinbase(self_data->mpz, 2);
        if (!sky_integer_data_isnegative(self_data)) {
            fill = 0;
            mpz = self_data->mpz;
            if (is_signed && nbytes * 8 == count) {
                ++count;
            }
            count = (count + 7) / 8;
        }
        else if (!is_signed) {
            sky_error_raise_string(sky_OverflowError,
                                   "can't convert negative int to unsigned");
        }
        else {
            mpz_t   abs_mpz;

            fill = 0xff;
            count = (count + 7) / 8;
            mpz_init_set_ui(negative_mpz, 1);
            mpz_mul_2exp(negative_mpz, negative_mpz, count * 8);

            mpz_init_set(abs_mpz, self_data->mpz);
            mpz_abs(abs_mpz, abs_mpz);

            mpz_sub(negative_mpz, negative_mpz, abs_mpz);
            mpz_clear(abs_mpz);

            mpz = negative_mpz;
            sky_asset_save(negative_mpz,
                           (sky_free_t)mpz_clear,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }

        if (count > nbytes) {
            sky_error_raise_string(sky_OverflowError, "int too big to convert");
        }

        if (little_endian) {
            mpz_export(bytes, &written, -1, count, -1, 0, mpz);
            count *= written;
            memset(bytes + count, fill, nbytes - count);
        }
        else {
            mpz_export(bytes + (nbytes - count), &written, 1, count, 1, 0, mpz);
            count *= written;
            memset(bytes, fill, nbytes - count);
        }
    } SKY_ASSET_BLOCK_END;
}


sky_float_t
sky_integer_float(sky_integer_t self)
{
    return sky_float_create(sky_integer_value_double(self));
}


sky_object_t
sky_integer_floordiv(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (sky_integer_iszero(other)) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "integer division or modulo by zero");
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_fdiv_q(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


typedef struct sky_integer_buffer_s {
    char *                              bytes;
    ssize_t                             len;
    ssize_t                             size;
} sky_integer_buffer_t;

static void
sky_integer_format_output(const void *  bytes,
                          ssize_t       nbytes,
               SKY_UNUSED size_t        width,
                          void *        arg)
{
    sky_integer_buffer_t    *buffer = arg;

    if (buffer->len + nbytes > buffer->size) {
        buffer->bytes = sky_asset_realloc(buffer->bytes,
                                          buffer->len + nbytes,
                                          SKY_ASSET_CLEANUP_ALWAYS,
                                          SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer->size = sky_memsize(buffer->bytes);
    }
    memcpy(buffer->bytes + buffer->len, bytes, nbytes);
    buffer->len += nbytes;
}

sky_string_t
sky_integer_format(sky_integer_t self, sky_string_t format_spec)
{
    sky_integer_data_t  *self_data, self_tagged_data;

    int                         base;
    char                        *bytes, static_bytes[32];
    size_t                      grouped_nbytes, width;
    ssize_t                     length, lpad, minimum_width, nbytes, padding,
                                rpad, spad;
    const char                  *grouping, *thousands_sep;
    sky_string_t                prefix;
    struct lconv                *lc;
    sky_unicode_char_t          codepoint, sign;
    sky_integer_buffer_t        buffer;
    sky_string_builder_t        builder;
    sky_format_specification_t  spec;

    if (!sky_string_parseformatspecification(format_spec, &spec)) {
        return sky_object_str(self);
    }

    if (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) {
        sky_error_raise_string(
                sky_ValueError,
                "Precision not allowed in integer format specifier");
    }

    base = 10;
    sign = 0;
    prefix = NULL;

    if (!spec.type) {
        spec.type = 'd';
    }
    else {
        if (spec.type < 0x20 || spec.type >= 0x7F) {
            sky_error_raise_format(
                    sky_ValueError,
                    "Unknown format code '\\x%x' for object of type %#@",
                    (unsigned int)spec.type,
                    sky_type_name(sky_object_type(self)));
        }
        switch ((char)spec.type) {
            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
            case '%':
                return sky_object_format(sky_number_float(self), format_spec);

            case 'b':
                base = 2;
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    prefix = SKY_STRING_LITERAL("0b");
                }
                break;
            case 'c':
                /* Accept padded alignment, but treat it as left alignment. */
                if (sky_format_alignment(&spec) ==
                            SKY_FORMAT_FLAG_ALIGN_PADDED)
                {
                    spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                    spec.flags |= SKY_FORMAT_FLAG_ALIGN_LEFT;
                }
                if (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_SIGN) {
                    sky_error_raise_string(
                            sky_ValueError,
                            "Sign not allowed with integer format specifier 'c'");
                }
                break;
            case 'd':
            case 'n':
                base = 10;
                break;
            case 'o':
                base = 8;
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    prefix = SKY_STRING_LITERAL("0o");
                }
                break;
            case 'x':
                base = 16;
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    prefix = SKY_STRING_LITERAL("0x");
                }
                break;
            case 'X':
                base = -16;
                if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                    prefix = SKY_STRING_LITERAL("0X");
                }
                break;
            default:
                sky_error_raise_format(
                        sky_ValueError,
                        "Unknown format code '%c' for object of type %#@",
                        (char)spec.type,
                        sky_type_name(sky_object_type(self)));
                break;
        }
    }

    /* ',' can only be specified with '' or 'd', which forces thousands
     * separators using commas and groups of 3 digits. For 'n', the use of
     * thousands separators depends on the current locale settings.
     */
    if (spec.type != 'd' &&
        (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS))
    {
        sky_error_raise_format(sky_ValueError,
                               "Cannot specify ',' with '%c'",
                               (char)spec.type);
    }

    SKY_ASSET_BLOCK_BEGIN {
        if ('c' == spec.type) {
            /* Check for value overflow -- range(SKY_UNICODE_CHAR_MAX) */
            codepoint = sky_integer_value(self, INT_MIN, INT_MAX, NULL);
            if (codepoint < SKY_UNICODE_CHAR_MIN ||
                codepoint > SKY_UNICODE_CHAR_MAX)
            {
                sky_error_raise_format(sky_OverflowError,
                                       "%%c arg not in range(%#x)",
                                       (int)SKY_UNICODE_CHAR_MAX + 1);
            }
            bytes = (char *)&codepoint;
            nbytes = sizeof(codepoint);
            width = sizeof(codepoint);
            length = 1;
        }
        else {
            /* Avoid a malloc if possible, which is the common case */
            self_data = sky_integer_data(self, &self_tagged_data);
            nbytes = mpz_sizeinbase(self_data->mpz, base) + 2;
            if (nbytes > (ssize_t)sizeof(static_bytes)) {
                bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
            }
            else {
                bytes = static_bytes;
            }
            bytes = mpz_get_str(bytes, base, self_data->mpz);

            if (sky_integer_data_isnegative(self_data)) {
                ++bytes;
                sign = '-';
            }
            nbytes = strlen(bytes);
            width = 1;

            length = 0;
            if (sign) {
                ++length;
            }
            else if (sky_format_sign(&spec) != SKY_FORMAT_FLAG_SIGN_MINUS) {
                ++length;
                if (sky_format_sign(&spec) == SKY_FORMAT_FLAG_SIGN_PLUS) {
                    sign = '+';
                }
                else {
                    sign = ' ';
                }
            }
            if (prefix) {
                length += sky_object_len(prefix);
            }

            /* Maybe add comma separators in. Use a comma for '' or 'd', but
             * look up the current locale data for 'n', which may or may not
             * have a thousands separator defined for it.
             */
            grouping = thousands_sep = NULL;
            if ('n' == spec.type) {
#if defined(HAVE_LOCALECONV_L)
                lc = localeconv_l(uselocale(NULL));
#else
                lc = localeconv();
#endif
                thousands_sep = lc->thousands_sep;
                grouping = lc->grouping;
            }
            else if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
                thousands_sep = ",";
                grouping = "\x03";
            }
            if (thousands_sep && grouping) {
                if ((spec.flags & SKY_FORMAT_FLAG_ZERO_PAD) &&
                    sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED)
                {
                    minimum_width = spec.width - length;
                }
                else {
                    minimum_width = 0;
                }

                grouped_nbytes = nbytes + (strlen(thousands_sep) * nbytes);
                buffer.bytes = sky_asset_malloc(grouped_nbytes,
                                                SKY_ASSET_CLEANUP_ALWAYS);
                buffer.len = 0;
                buffer.size = sky_memsize(buffer.bytes);
                nbytes = sky_format_number(sky_integer_format_output,
                                           &buffer,
                                           bytes,
                                           nbytes,
                                           minimum_width,
                                           NULL,
                                           thousands_sep,
                                           grouping);
                sky_error_validate_debug(nbytes == buffer.len);
                bytes = buffer.bytes;
            }
            length += nbytes;
        }

        lpad = rpad = spad = 0;
        padding = spec.width - length;
        if (padding && (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH)) {
            if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_ALIGN)) {
                spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_ALIGN_RIGHT;
            }
            switch (sky_format_alignment(&spec)) {
                case SKY_FORMAT_FLAG_ALIGN_LEFT:
                    rpad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                    lpad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_PADDED:
                    spad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_CENTER:
                    lpad = padding / 2;
                    rpad = padding - lpad;
                    break;
            }
        }

        builder = sky_string_builder_createwithcapacity(length + padding);
        if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED) {
            if (sign) {
                sky_string_builder_appendcodepoint(builder, sign, 1);
            }
            if (prefix) {
                sky_string_builder_append(builder, prefix);
            }
            if (spad > 0) {
                sky_string_builder_appendcodepoint(builder, spec.fill, spad);
            }
        }
        else {
            if (lpad > 0) {
                sky_string_builder_appendcodepoint(builder, spec.fill, lpad);
            }
            if (sign) {
                sky_string_builder_appendcodepoint(builder, sign, 1);
            }
            if (prefix) {
                sky_string_builder_append(builder, prefix);
            }
        }
        sky_string_builder_appendcodepoints(builder,
                                            bytes,
                                            nbytes / width,
                                            width);
        if (rpad > 0) {
            sky_string_builder_appendcodepoint(builder, spec.fill, rpad);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


double
sky_integer_frexp(sky_integer_t self, ssize_t *exp)
{
    sky_integer_data_t  *self_data, self_tagged_data;

    self_data = sky_integer_data(self, &self_tagged_data);
    return mpz_get_d_2exp(exp, self_data->mpz);
}


sky_integer_t
sky_integer_from_bytes(sky_type_t   cls,
                       sky_object_t bytes,
                       sky_string_t byteorder,
                       sky_bool_t   is_signed)
{
    int             endian;
    mpz_t           mpz;
    const char      *cstring;
    sky_buffer_t    buffer;
    sky_integer_t   result;

    if (!sky_type_issubtype(cls, sky_integer_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ is not a sub-type of %#@",
                               sky_type_name(cls),
                               sky_type_name(sky_integer_type));
    }

    if (!(cstring = sky_string_cstring(byteorder))) {
        sky_error_raise_string(sky_ValueError,
                               "byteorder must be either 'little' or 'big'");
    }
    if (!strcmp(cstring, "little")) {
        endian = -1;
    }
    else if (!strcmp(cstring, "big")) {
        endian = 1;
    }
    else {
        sky_error_raise_string(sky_ValueError,
                               "byteorder must be either 'little' or 'big'");
    }

    /* Make sure 'bytes' either supports the buffer protocol or is an iterable
     * that produces only integers in the range(0, 256). Basically create a
     * bytes object from the object using sky_object_bytes(). If the object is
     * already a bytes object, it'll simply be returned as-is.
     */
    bytes = sky_object_bytes(bytes);

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_acquire(&buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        mpz_init(mpz);
        mpz_import(mpz, buffer.len, endian, 1, endian, 0, buffer.buf);

        if (is_signed && mpz_tstbit(mpz, (buffer.len * 8) - 1)) {
            mpz_t   shiftz;

            mpz_init_set_ui(shiftz, 1);
            mpz_mul_2exp(shiftz, shiftz, buffer.len * 8);
            mpz[0]._mp_size = -mpz[0]._mp_size;
            mpz_add(mpz, mpz, shiftz);
            mpz[0]._mp_size = -mpz[0]._mp_size;
            mpz_clear(shiftz);
        }

        /* Create the integer object instance to return. This call will take
         * ownership of mpz, either giving it to the instance or cleaning it up
         * immediately in the case where mpz represents a cached integer.
         */
        result = sky_integer_createwithmpz(cls, mpz);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_integer_from_bytes_doc =
"int.from_bytes(bytes, byteorder, *, signed=False) -> int\n\n"
"Return the integer represneted by the given array of bytes.\n"
"\n"
"The bytes argument must either support the buffer protocol or be an\n"
"iterable object producing bytes. Bytes and bytearray are examples of\n"
"built-in objects that support the buffer protocol.\n"
"\n"
"The byteorder argument determines the byte order used to represent the\n"
"integer. If byteorder is 'big', the most significant byte is at the\n"
"beginning of the byte array. If byteorder is 'little', the most\n"
"significant byte is at the end of the byte array. To request the native\n"
"byte order of the host system, use `sys.byteorder' as the byte order value.\n"
"\n"
"The signed keyword-only argument indicates whether two's complement is\n"
"used to represent the integer.";


sky_object_t
sky_integer_ge(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_integer_getnewargs(sky_integer_t integer)
{
    return sky_object_build("(O)", sky_integer_copy(integer));
}


sky_object_t
sky_integer_gt(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_GREATER);
}


uintptr_t
sky_integer_hash(sky_integer_t integer)
{
    sky_integer_data_t  tagged_data;

    return (uintptr_t)sky_integer_data(integer, &tagged_data)->mpz[0]._mp_d[0];
}


static sky_object_t
sky_integer_imag_getter(SKY_UNUSED sky_object_t self,
                        SKY_UNUSED sky_type_t   type)
{
    return sky_integer_zero;
}


sky_integer_t
sky_integer_index(sky_integer_t self)
{
    return sky_integer_copy(self);
}


sky_integer_t
sky_integer_int(sky_integer_t self)
{
    return sky_integer_copy(self);
}


sky_object_t
sky_integer_invert(sky_integer_t self)
{
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    integer = sky_integer_clone(self, NULL, SKY_FALSE);
    integer_data = sky_integer_data(integer, NULL);
    mpz_com(integer_data->mpz, integer_data->mpz);

    return sky_integer_cached(integer);
}


sky_bool_t
sky_integer_iseven(sky_integer_t self)
{
    sky_integer_data_t  *integer_data;

    switch (SKY_OBJECT_TAG(self)) {
        case 1: case 2:
            return (((uintptr_t)self >> 4) & 1 ? SKY_FALSE : SKY_TRUE);
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4:
                                return SKY_TRUE;
                            case 5:
                                return SKY_FALSE;
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0: case 2:
                            case 3: case 5:
                                return SKY_FALSE;
                            case 1: case 4:
                                return SKY_TRUE;
                        }
                        break;
                }
            }
            break;
    }

    integer_data = sky_integer_data(self, NULL);
    return (mpz_even_p(integer_data->mpz) ? SKY_TRUE : SKY_FALSE);
}


sky_bool_t
sky_integer_isnegative(sky_integer_t self)
{
    switch (SKY_OBJECT_TAG(self)) {
        case 1:
            return SKY_FALSE;
        case 2:
            sky_error_validate_debug(((uintptr_t)self >> 4) > 0);
            return SKY_TRUE;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4: case 5:
                                return SKY_FALSE;
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0: case 2:
                            case 3: case 5:
                                return SKY_FALSE;
                            case 1: case 4:
                                return SKY_TRUE;
                        }
                        break;
                }
            }
            break;
    }

    return sky_integer_data_isnegative(sky_integer_data(self, NULL));
}


sky_bool_t
sky_integer_iszero(sky_integer_t self)
{
    switch (SKY_OBJECT_TAG(self)) {
        case 1:
            return (!((uintptr_t)self >> 4) ? SKY_TRUE : SKY_FALSE);
        case 2:
            sky_error_validate_debug(((uintptr_t)self >> 4) > 0);
            return SKY_FALSE;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4:
                                return SKY_TRUE;
                            case 5:
                                return SKY_FALSE;
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0: case 1: case 2:
                            case 3: case 4: case 5:
                                return SKY_FALSE;
                        }
                        break;
                }
            }
            break;
    }

    return sky_integer_data_iszero(sky_integer_data(self, NULL));
}


sky_object_t
sky_integer_le(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


sky_object_t
sky_integer_lshift(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    ssize_t             nbits_result, nbits_shift;
    sky_integer_data_t  *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);

    nbits_shift = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);
    if (nbits_shift <= 0) {
        if (!nbits_shift) {
            return self;
        }
        sky_error_raise_string(sky_ValueError, "negative shift count");
    }

    self_data = sky_integer_data(self, &self_tagged_data);
    nbits_result = sky_integer_data_nbits(self_data) + nbits_shift;

    mpz_init2(mpz, nbits_result);
    mpz_mul_2exp(mpz, self_data->mpz, nbits_shift);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_object_t
sky_integer_lt(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_integer_mod(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (sky_integer_iszero(other)) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "integer division or modulo by zero");
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    /* Don't use mpz_mod(), because it does not consider sign. The correct
     * answers to match with CPython are given by mpz_fdiv_r().
     */

    mpz_init(mpz);
    mpz_fdiv_r(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_object_t
sky_integer_mul(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_mul(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_object_t
sky_integer_ne(sky_integer_t self, sky_object_t other)
{
    return sky_integer_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_object_t
sky_integer_neg(sky_integer_t self)
{
    sky_integer_t       integer;
    sky_integer_data_t  *integer_data;

    if (sky_object_istagged(self)) {
        switch (SKY_OBJECT_TAG(self)) {
            case 0x1:
                if (!((uintptr_t)self >> 4)) {
                    return self;
                }
                return SKY_OBJECT_MAKETAG(0x2, ((uintptr_t)self >> 4));
            case 0x2:
                return SKY_OBJECT_MAKETAG(0x1, ((uintptr_t)self >> 4));
            case 0x7:
                if (!((uintptr_t)self & 0xF0)) {
                    switch (((uintptr_t)self >> 8) & 0x3F) {
                        case 0x04:  /* sky_False */
                            return SKY_OBJECT_MAKETAG(0x1, 0);
                        case 0x05:  /* sky_True */
                            return SKY_OBJECT_MAKETAG(0x2, 1);
                    }
                }
                break;
        }
    }

    integer = sky_integer_clone(self, NULL, SKY_FALSE);
    integer_data = sky_integer_data(integer, NULL);
    mpz_neg(integer_data->mpz, integer_data->mpz);

    return sky_integer_cached(integer);
}


sky_object_t
sky_integer_new(sky_type_t cls, sky_object_t x, sky_object_t base)
{
    int             base_value;
    sky_integer_t   integer;

    if (sky_integer_type != cls) {
        integer = sky_integer_new(sky_integer_type, x, base);
        return sky_integer_clone(integer, cls, SKY_TRUE);
    }

    if (sky_object_isnull(x)) {
        return sky_integer_zero;
    }
    if (sky_object_isnull(base)) {
        return sky_number_integer(x);
    }

    SKY_ERROR_TRY {
        base_value = sky_integer_value(sky_number_integer(base),
                                       INT_MIN, INT_MAX,
                                       NULL);
    }
    SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_raise_object(sky_ValueError,
                               "int() args 2 must be >= 2 and <= 36");
    } SKY_ERROR_TRY_END;

    return sky_integer_createfromstring(x, base_value);
}


static sky_object_t
sky_integer_numerator_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_integer_copy(self);
}


sky_object_t
sky_integer_or(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (self == other) {
        return self;
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_ior(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_integer_t
sky_integer_pos(sky_integer_t self)
{
    return sky_integer_copy(self);
}


sky_object_t
sky_integer_pow(sky_integer_t self, sky_object_t other, sky_object_t modulo)
{
    mpz_t               mpz;
    sky_integer_data_t  *modulo_data, modulo_tagged_data,
                        *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (!sky_object_isnull(modulo) &&
        !sky_object_isa(modulo, sky_integer_type))
    {
        return sky_NotImplemented;
    }

    if (sky_integer_isnegative(self)) {
        if (!sky_object_isnull(modulo)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "pow() 2nd argument cannot be negative "
                    "when 3rd argument specified");
        }
    }

    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);
    if (sky_object_isnull(modulo)) {
        modulo_data = NULL;
    }
    else if (sky_integer_iszero(modulo)) {
        sky_error_raise_string(sky_ValueError,
                               "pow() 3rd argument cannot be 0");
    }
    else {
        modulo_data = sky_integer_data(modulo, &modulo_tagged_data);
        if (1 == modulo_data->mpz[0]._mp_size && 1 == modulo_data->value) {
            return sky_integer_zero;
        }
    }

    if (sky_integer_data_iszero(other_data)) {
        return sky_integer_one;
    }
    if (sky_integer_data_iszero(self_data)) {
        return sky_integer_zero;
    }

    mpz_init(mpz);

    if (modulo_data) {
        mpz_powm(mpz, self_data->mpz, other_data->mpz, modulo_data->mpz);
    }
    else {
        int         i;
        mp_limb_t   j;

        /* GMP does not provide a function to compute pow() with no modulo. */
        /* Left-to-right binary exponentiation (HAC Algorithm 14.79) */
        /* http://www.cacr.math.uwaterloo.ca/hac/about/chap14.pdf    */
        mpz_set_ui(mpz, 1);
        for (i = abs(self_data->mpz[0]._mp_size) - 1; i >= 0; --i) {
            for (j = (mp_limb_t)1 << (GMP_NUMB_BITS - 1); j; j >>= 1) {
                mpz_mul(mpz, mpz, mpz);
                if (other_data->mpz[0]._mp_d[i] & j) {
                    mpz_mul(mpz, mpz, self_data->mpz);
                }
            }
        }
    }

    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


static sky_object_t
sky_integer_real_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_integer_copy(self);
}


sky_string_t
sky_integer_repr(sky_integer_t self)
{
    char                    *str;
    size_t                  nbytes;
    sky_integer_data_t      *integer_data, tagged_data;
    sky_string_builder_t    builder;

    SKY_ASSET_BLOCK_BEGIN {
        integer_data = sky_integer_data(self, &tagged_data);
        nbytes = mpz_sizeinbase(integer_data->mpz, 10);
        str = sky_asset_malloc(nbytes + 2, SKY_ASSET_CLEANUP_ALWAYS);
        mpz_get_str(str, 10, integer_data->mpz);
        nbytes = strlen(str);

        builder = sky_string_builder_createwithcapacity(nbytes);
        sky_string_builder_appendcodepoints(builder, str, nbytes, 1);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


sky_object_t
sky_integer_round(sky_integer_t self, sky_object_t ndigits)
{
    sky_tuple_t         divmod_near;
    sky_integer_t       ten;
    sky_integer_data_t  tagged_data;

    if (sky_object_isnull(ndigits)) {
        return sky_integer_copy(self);
    }

    /* To round an integer m to the nearest 10**n (n positive), we make use of
     * the divmod_near operation, defined by:
     *
     *   divmod_near(a, b) = (q, r)
     *
     * where q is the nearest integer to the quotient a / b (the nearest even
     * integer in the case of a tie) and r == a - q * b. Hence, q * b = a - r
     * is the nearest multiple of b to a, preferring even multiples in the
     * case of a tie.
     *
     * So the nearest multiple of 10**n to m is:
     *
     *   m - divmod_near(m, 10**n)[1]
     */
    ndigits = sky_number_index(ndigits);
    if (sky_integer_data(ndigits, &tagged_data)->mpz[0]._mp_size >= 0) {
        return sky_integer_copy(self);
    }

    /* result = self - divmod_near(self, 10 ** -ndigits)[1] */
    ten = sky_integer_create(10);
    ndigits = sky_integer_neg(ndigits);
    divmod_near = sky_integer_divmod_near(self,
                                          sky_integer_pow(ten, ndigits, NULL));
    return sky_integer_sub(self, sky_tuple_get(divmod_near, 1));
}


sky_object_t
sky_integer_rshift(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    ssize_t             nbits_shift;
    sky_integer_data_t  *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);

    nbits_shift = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);
    if (nbits_shift <= 0) {
        if (!nbits_shift) {
            return self;
        }
        sky_error_raise_string(sky_ValueError, "negative shift count");
    }
    self_data = sky_integer_data(self, &self_tagged_data);

    mpz_init(mpz);
    mpz_fdiv_q_2exp(mpz, self_data->mpz, nbits_shift);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


int
sky_integer_sign(sky_integer_t self)
{
    switch (SKY_OBJECT_TAG(self)) {
        case 1:
            return (!((uintptr_t)self >> 4) ? 0 : 1);
        case 2:
            sky_error_validate_debug(((uintptr_t)self >> 4) > 0);
            return -1;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4:
                                return 0;
                            case 5:
                                return 1;
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0: case 2:
                            case 3: case 5:
                                return 1;
                            case 1: case 4:
                                return -1;
                        }
                        break;
                }
            }
            break;
    }

    return mpz_sgn(sky_integer_data(self, NULL)->mpz);
}


size_t
sky_integer_sizeof(sky_integer_t self)
{
    sky_integer_data_t  *integer_data;

    switch (SKY_OBJECT_TAG(self)) {
        case 1: case 2:
            return 0;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4: case 5:
                                return 0;
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0: case 1: case 2:
                            case 3: case 4: case 5:
                                return 0;
                        }
                        break;
                }
            }
            break;
    }

    integer_data = sky_integer_data(self, NULL);
    if (integer_data->mpz[0]._mp_d != &(integer_data->value)) {
        return sky_memsize(self) + sky_memsize(integer_data->mpz[0]._mp_d);
    }
    return sky_memsize(self);
}


size_t
sky_integer_sizeinbase(sky_integer_t self, int base)
{
    sky_integer_data_t  *self_data, tagged_data;

    self_data = sky_integer_data(self, &tagged_data);
    return mpz_sizeinbase(self_data->mpz, base);
}


sky_object_t
sky_integer_sub(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_sub(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


sky_integer_t
sky_integer_sum(sky_integer_t start, sky_object_t sequence)
{
    mpz_t               mpz;
    sky_object_t        element, iter;
    sky_integer_t       result;
    sky_integer_data_t  *integer_data, tagged_data;

    SKY_ASSET_BLOCK_BEGIN {
        integer_data = sky_integer_data(start, &tagged_data);
        mpz_init_set(mpz, integer_data->mpz);
        sky_asset_save(mpz, (sky_free_t)mpz_clear, SKY_ASSET_CLEANUP_ON_ERROR);

        if (sky_sequence_isiterable(sequence)) {
            SKY_SEQUENCE_FOREACH(sequence, element) {
                integer_data = sky_integer_data(element, &tagged_data);
                mpz_add(mpz, mpz, integer_data->mpz);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(sequence);
            while ((element = sky_object_next(iter, NULL)) != NULL) {
                integer_data = sky_integer_data(element, &tagged_data);
                mpz_add(mpz, mpz, integer_data->mpz);
            }
        }
        result = sky_integer_createwithmpz(sky_integer_type, mpz);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bytes_t
sky_integer_to_bytes(sky_integer_t  self,
                     ssize_t        length,
                     sky_string_t   byteorder,
                     sky_bool_t     is_signed)
{
    uint8_t     *buffer;
    const char  *cstring;
    sky_bool_t  little_endian;
    sky_bytes_t bytes;

    if (length < 0) {
        sky_error_raise_string(sky_ValueError,
                               "length argument must be non-negative");
    }
    if (!(cstring = sky_string_cstring(byteorder))) {
        sky_error_raise_string(sky_ValueError,
                               "byteorder must be either 'little' or 'big'");
    }
    if (!strcmp(cstring, "little")) {
        little_endian = SKY_TRUE;
    }
    else if (!strcmp(cstring, "big")) {
        little_endian = SKY_FALSE;
    }
    else {
        sky_error_raise_string(sky_ValueError,
                               "byteorder must be either 'little' or 'big'");
    }

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(length + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        buffer[length] = '\0';

        sky_integer_export(self, buffer, length, little_endian, is_signed);
        bytes = sky_bytes_createwithbytes(buffer, length, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}

static const char *sky_integer_to_bytes_doc =
"int.to_bytes(length, byteorder, *, signed=False) -> bytes\n\n"
"Return an array of bytes representing an integer.\n\n"
"The integer is represented using length bytes. An OverflowError is\n"
"raised if the integer is not representable with the given number of\n"
"bytes.\n"
"\n"
"The byteorder argument determines the byte order used to represent the\n"
"integer. If byteorder is 'big', the most significant byte is at the\n"
"beginning of the byte array. If byteorder is 'little', the most\n"
"significant byte is at the end of the byte array. To request the native\n"
"byte order of the host system, use `sys.byteorder' as the byte order value.\n"
"\n"
"The signed keyword-only argument determines whether two's complement is\n"
"used to represent the integer. If signed is False and a negative integer\n"
"is given, an OverflowError is raised.";


/* Method in a nutshell:
 *
 *   0. reduce to case a, b > 0; filter out obvious underflow/overflow
 *   1. choose a suitable integer 'shift'
 *   2. use integer arithmetic to compute x = floor(2**-shift*a/b)
 *   3. adjust x for correct rounding
 *   4. convert x to a double dx with the same value
 *   5. return ldexp(dx, shift).
 *
 * In more detail:
 *
 * 0. For any a, a/0 raises ZeroDivisionError; for nonzero b, 0/b
 * returns either 0.0 or -0.0, depending on the sign of b.  For a and
 * b both nonzero, ignore signs of a and b, and add the sign back in
 * at the end.  Now write a_bits and b_bits for the bit lengths of a
 * and b respectively (that is, a_bits = 1 + floor(log_2(a)); likewise
 * for b).  Then
 *
 *    2**(a_bits - b_bits - 1) < a/b < 2**(a_bits - b_bits + 1).
 *
 * So if a_bits - b_bits > DBL_MAX_EXP then a/b > 2**DBL_MAX_EXP and
 * so overflows.  Similarly, if a_bits - b_bits < DBL_MIN_EXP -
 * DBL_MANT_DIG - 1 then a/b underflows to 0.  With these cases out of
 * the way, we can assume that
 *
 *    DBL_MIN_EXP - DBL_MANT_DIG - 1 <= a_bits - b_bits <= DBL_MAX_EXP.
 *
 * 1. The integer 'shift' is chosen so that x has the right number of
 * bits for a double, plus two or three extra bits that will be used
 * in the rounding decisions.  Writing a_bits and b_bits for the
 * number of significant bits in a and b respectively, a
 * straightforward formula for shift is:
 *
 *    shift = a_bits - b_bits - DBL_MANT_DIG - 2
 *
 * This is fine in the usual case, but if a/b is smaller than the
 * smallest normal float then it can lead to double rounding on an
 * IEEE 754 platform, giving incorrectly rounded results.  So we
 * adjust the formula slightly.  The actual formula used is:
 *
 *    shift = MAX(a_bits - b_bits, DBL_MIN_EXP) - DBL_MANT_DIG - 2
 *
 * 2. The quantity x is computed by first shifting a (left -shift bits
 * if shift <= 0, right shift bits if shift > 0) and then dividing by
 * b.  For both the shift and the division, we keep track of whether
 * the result is inexact, in a flag 'inexact'; this information is
 * needed at the rounding stage.
 *
 * With the choice of shift above, together with our assumption that
 * a_bits - b_bits >= DBL_MIN_EXP - DBL_MANT_DIG - 1, it follows
 * that x >= 1.
 *
 * 3. Now x * 2**shift <= a/b < (x+1) * 2**shift.  We want to replace
 * this with an exactly representable float of the form
 *
 *    round(x/2**extra_bits) * 2**(extra_bits+shift).
 *
 * For float representability, we need x/2**extra_bits <
 * 2**DBL_MANT_DIG and extra_bits + shift >= DBL_MIN_EXP -
 * DBL_MANT_DIG.  This translates to the condition:
 *
 *    extra_bits >= MAX(x_bits, DBL_MIN_EXP - shift) - DBL_MANT_DIG
 *
 * To round, we just modify the bottom digit of x in-place; this can
 * end up giving a digit with value > PyLONG_MASK, but that's not a
 * problem since digits can hold values up to 2*PyLONG_MASK+1.
 *
 * With the original choices for shift above, extra_bits will always
 * be 2 or 3.  Then rounding under the round-half-to-even rule, we
 * round up iff the most significant of the extra bits is 1, and
 * either: (a) the computation of x in step 2 had an inexact result,
 * or (b) at least one other of the extra bits is 1, or (c) the least
 * significant bit of x (above those to be rounded) is 1.
 *
 * 4. Conversion to a double is straightforward; all floating-point
 * operations involved in the conversion are exact, so there's no
 * danger of rounding errors.
 *
 * 5. Use ldexp(x, shift) to compute x*2**shift, the final result.
 * The result will always be exactly representable as a double, except
 * in the case that it overflows.  To avoid dependence on the exact
 * behaviour of ldexp on overflow, we check for overflow before
 * applying ldexp.  The result of ldexp is adjusted for sign before
 * returning.
 */

#define SKY_INTEGER_MANT_DIG_DIGITS (DBL_MANT_DIG / GMP_NUMB_BITS)
#define SKY_INTEGER_MANT_DIG_BITS   (DBL_MANT_DIG % GMP_NUMB_BITS)

sky_object_t
sky_integer_truediv(sky_integer_t self, sky_object_t other)
{
    int                 extra_bits, other_size, self_size, shift, x_bits,
                        x_size;
    mpz_t               remainder, x;
    double              dx, result;
    ssize_t             diff;
    mp_limb_t           low, mask;
    sky_bool_t          inexact, negate;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (sky_integer_iszero(other)) {
        sky_error_raise_string(sky_ZeroDivisionError, "division by zero");
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    negate = sky_integer_data_isnegative(self_data) ^
             sky_integer_data_isnegative(other_data);
    if (sky_integer_data_iszero(self_data)) {
        goto underflow_or_zero;
    }

    self_size = sky_integer_data_size(self_data);
    other_size = sky_integer_data_size(other_data);

    /* Fast path for two small integers (exactly representable in a double).
     * Relies on floating-point division being correctly rounded; results may
     * be subject to double rounding on x86 machines that operate with the x87
     * FPU set to 64-bit precision.
     */
    if ((self_size <= SKY_INTEGER_MANT_DIG_DIGITS ||
         (self_size == SKY_INTEGER_MANT_DIG_DIGITS + 1 &&
          self_data->mpz[0]._mp_d[SKY_INTEGER_MANT_DIG_DIGITS] >>
                SKY_INTEGER_MANT_DIG_BITS == 0)) &&
        (other_size <= SKY_INTEGER_MANT_DIG_DIGITS ||
         (other_size == SKY_INTEGER_MANT_DIG_DIGITS + 1 &&
          other_data->mpz[0]._mp_d[SKY_INTEGER_MANT_DIG_DIGITS] >>
                SKY_INTEGER_MANT_DIG_BITS == 0)))
    {
        result = mpz_get_d(self_data->mpz) / mpz_get_d(other_data->mpz);
        return sky_float_create(result);
    }

    /* Catch obvious cases of underflow and overflow. */
    diff = self_size - other_size;
    if (diff > SSIZE_MAX / GMP_NUMB_BITS - 1) {
        goto overflow;
    }
    if (diff < 1 - SSIZE_MAX / GMP_NUMB_BITS) {
        goto underflow_or_zero;
    }
    diff = diff * GMP_NUMB_BITS +
           sky_integer_bits_in_limb(self_data->mpz[0]._mp_d[self_size - 1]) -
           sky_integer_bits_in_limb(other_data->mpz[0]._mp_d[other_size - 1]);
    if (diff > DBL_MAX_EXP) {
        goto overflow;
    }
    if (diff < DBL_MIN_EXP - DBL_MANT_DIG - 1) {
        goto underflow_or_zero;
    }

    /* Choose value for shift; see comments for step 1 above. */
    shift = SKY_MAX(diff, DBL_MIN_EXP) - DBL_MANT_DIG - 2;
    inexact = SKY_FALSE;

    if (shift <= 0) {
        /* x = abs(self * 2 ** -shift) */
        mpz_init2(x, sky_integer_data_nbits(self_data) + -shift);
        mpz_mul_2exp(x, self_data->mpz, -shift);
    }
    else {
        /* x = self >> shift */
        mpz_init2(x, sky_integer_data_nbits(self_data) - shift);
        mpz_fdiv_q_2exp(x, self_data->mpz, shift);

        /* Set inexact to SKY_TRUE if any of the bits shifted off are set. */
        if (mpz_scan1(self_data->mpz, 0) < (mp_bitcnt_t)shift) {
            inexact = SKY_TRUE;
        }
    }

    /* x //= other. If the remainder is non-zero, set inexact. We own the only
     * reference to x, so it's safe to modify it in-place.
     */
    mpz_init(remainder);
    mpz_tdiv_qr(x, remainder, x, other_data->mpz);
    if (remainder->_mp_size) {
        inexact = SKY_TRUE;
    }
    mpz_clear(remainder);

    x_size = abs(x[0]._mp_size);
    x_bits = (x_size - 1) * GMP_NUMB_BITS +
             sky_integer_bits_in_limb(x[0]._mp_d[x_size - 1]);

    /* The number of extra bits that have to be rounded away. */
    extra_bits = SKY_MAX(x_bits, DBL_MIN_EXP - shift) - DBL_MANT_DIG;
    sky_error_validate_debug(2 == extra_bits || 3 == extra_bits);

    /* Round by directly modifying the low limb of x. */
    mask = (mp_limb_t)1 << (extra_bits - 1);
    low = x[0]._mp_d[0] | (inexact ? 1 : 0);
    if ((low & mask) && (low & (3 * mask - 1))) {
        low += mask;
    }
    x[0]._mp_d[0] = low & ~(mask - 1);

    /* Convert x to a double dx; the conversion is exact. */
    dx = mpz_get_d(x);
    mpz_clear(x);

    /* Check whether ldexp result will overflow a double. */
    if (shift + x_bits >= DBL_MAX_EXP &&
        (shift + x_bits > DBL_MAX_EXP || dx == ldexp(1.0, x_bits)))
    {
        goto overflow;
    }
    result = ldexp(dx, shift);
    if (negate) {
        result = -result;
    }

    return sky_float_create(result);

overflow:
    sky_error_raise_string(sky_OverflowError,
                           "integer division result too large for a float");

underflow_or_zero:
    return sky_float_create(negate ? -0.0 : 0.0);
}


intmax_t
sky_integer_value(sky_integer_t self,
                  intmax_t      min,
                  uintmax_t     max,
                  sky_type_t    exception)
{
    int                 size;
    uintmax_t           abs_value;
    sky_integer_data_t  *integer_data;

    switch (SKY_OBJECT_TAG(self)) {
        case 1:
            abs_value = (uintptr_t)self >> 4;
            size = (abs_value ? 1 : 0);
            break;
        case 2:
            abs_value = (uintptr_t)self >> 4;
            size = (abs_value ? -1 : 0);
            break;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4:
                                abs_value = 0;
                                size = 0;
                                break;
                            case 5:
                                abs_value = 1;
                                size = 1;
                                break;
                            default:
                                /* This is an error. Call sky_object_data() to
                                 * force it to be raised.
                                 */
                                integer_data = sky_object_data(self,
                                                               sky_integer_type);
                                sky_error_fatal("not reachable");
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0:
                                abs_value = 0xFFFFFFFFul;
                                size = 1;
                                break;
                            case 1:
                                abs_value = 0x80000000ul;
                                size = -1;
                                break;
                            case 2:
                                abs_value = 0x7FFFFFFFul;
                                size = 1;
                                break;
                            case 3:
                                abs_value = 0xFFFFFFFFFFFFFFFFull;
                                size = 1;
                                break;
                            case 4:
                                abs_value = 0x8000000000000000ull;
                                size = -1;
                                break;
                            case 5:
                                abs_value = 0x7FFFFFFFFFFFFFFFull;
                                size = 1;
                                break;
                            default:
                                /* This is an error. Call sky_object_data() to
                                 * force it to be raised.
                                 */
                                integer_data = sky_object_data(self,
                                                               sky_integer_type);
                                sky_error_fatal("not reachable");
                        }
                        break;
                    default:
                        /* This is an error. Call sky_object_data() to
                         * force it to be raised.
                         */
                        integer_data = sky_object_data(self, sky_integer_type);
                        sky_error_fatal("not reachable");
                }
                break;
            }
            /* This is an error. Call sky_object_data() to
             * force it to be raised.
             */
            integer_data = sky_object_data(self, sky_integer_type);
            sky_error_fatal("not reachable");

        default:
            integer_data = sky_integer_data(self, NULL);
            size = integer_data->mpz[0]._mp_size;
            if (!size) {
                abs_value = 0;
            }
            else if (abs(size) == 1) {
                abs_value = integer_data->mpz[0]._mp_d[0];
            }
#if INTMAX_BITS > GMP_NUMB_BITS
            else { ... }
#else
            else {
                goto overflow;
            }
#endif
    }

    if (size < 0) {
        if (abs_value > (uintmax_t)(-1 - INTMAX_MIN) + 1) {
            goto overflow;
        }
        if (-(intmax_t)abs_value < min) {
            goto overflow;
        }
    }
    else {
        if (abs_value > max) {
            goto overflow;
        }
        if ((min > 0 && abs_value < (uintmax_t)min)) {
            goto overflow;
        }
    }

    return (size < 0 ? -(intmax_t)abs_value : (intmax_t)abs_value);

overflow:
    sky_error_raise_string(
            (exception ? exception : sky_OverflowError),
            "integer value too large to convert to C integer");
}


intmax_t
sky_integer_value_clamped(sky_integer_t self, intmax_t min, uintmax_t max)
{
    int                 size;
    uintmax_t           abs_value;
    sky_integer_data_t  *integer_data;
    
    switch (SKY_OBJECT_TAG(self)) {
        case 1:
            abs_value = (uintptr_t)self >> 4;
            size = (abs_value ? 1 : 0);
            break;
        case 2:
            abs_value = (uintptr_t)self >> 4;
            size = (abs_value ? -1 : 0);
            break;
        case 7:
            if (!(((uintptr_t)self >> 4) & 0xF)) {
                switch (((uintptr_t)self >> 12) & 0xF) {
                    case 0:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 4:
                                abs_value = 0;
                                size = 0;
                                break;
                            case 5:
                                abs_value = 1;
                                size = 1;
                                break;
                            default:
                                /* This is an error. Call sky_object_data() to
                                 * force it to be raised.
                                 */
                                integer_data = sky_object_data(self,
                                                               sky_integer_type);
                                sky_error_fatal("not reachable");
                        }
                        break;
                    case 2:
                        switch (((uintptr_t)self >> 8) & 0xF) {
                            case 0:
                                abs_value = 0xFFFFFFFFul;
                                size = 1;
                                break;
                            case 1:
                                abs_value = 0x80000000ul;
                                size = -1;
                                break;
                            case 2:
                                abs_value = 0x7FFFFFFFul;
                                size = 1;
                                break;
                            case 3:
                                abs_value = 0xFFFFFFFFFFFFFFFFull;
                                size = 1;
                                break;
                            case 4:
                                abs_value = 0x8000000000000000ull;
                                size = -1;
                                break;
                            case 5:
                                abs_value = 0x7FFFFFFFFFFFFFFFull;
                                size = 1;
                                break;
                            default:
                                /* This is an error. Call sky_object_data() to
                                 * force it to be raised.
                                 */
                                integer_data = sky_object_data(self,
                                                               sky_integer_type);
                                sky_error_fatal("not reachable");
                        }
                        break;
                    default:
                        /* This is an error. Call sky_object_data() to
                         * force it to be raised.
                         */
                        integer_data = sky_object_data(self, sky_integer_type);
                        sky_error_fatal("not reachable");
                }
                break;
            }
            /* This is an error. Call sky_object_data() to
             * force it to be raised.
             */
            integer_data = sky_object_data(self, sky_integer_type);
            sky_error_fatal("not reachable");

        default:
            integer_data = sky_integer_data(self, NULL);
            size = integer_data->mpz[0]._mp_size;
            if (!size) {
                abs_value = 0;
            }
            else if (abs(size) == 1) {
                abs_value = integer_data->mpz[0]._mp_d[0];
            }
#if INTMAX_BITS > GMP_NUMB_BITS
            else { ... }
#else
            else {
                return (size < 0 ? min : (intmax_t)max);
            }
#endif
    }

    if (size < 0) {
        if (abs_value > (uintmax_t)(-1 - INTMAX_MIN) + 1) {
            return min;
        }
        if (-(intmax_t)abs_value < min) {
            return min;
        }
    }
    else {
        if (abs_value > max) {
            return max;
        }
        if (min > 0 && abs_value < (uintmax_t)min) {
            return min;
        }
    }

    return (size < 0 ? -(intmax_t)abs_value : (intmax_t)abs_value);
}


double
sky_integer_value_double(sky_integer_t self)
{
    long                exponent;
    double              value;
    sky_integer_data_t  *self_data, tagged_data;

    self_data = sky_integer_data(self, &tagged_data);
    value = mpz_get_d_2exp(&exponent, self_data->mpz);
    if (exponent > DBL_MAX_EXP) {
        sky_error_raise_string(sky_OverflowError,
                               "long int too large to convert to float");
    }

    return ldexp(value, exponent);
}


sky_object_t
sky_integer_xor(sky_integer_t self, sky_object_t other)
{
    mpz_t               mpz;
    sky_integer_data_t  *other_data, other_tagged_data,
                        *self_data, self_tagged_data;

    SKY_INTEGER_BINARY_OP_CHECK(self, other);
    if (self == other) {
        return sky_integer_zero;
    }
    self_data = sky_integer_data(self, &self_tagged_data);
    other_data = sky_integer_data(other, &other_tagged_data);

    mpz_init(mpz);
    mpz_xor(mpz, self_data->mpz, other_data->mpz);
    return sky_integer_createwithmpz(sky_integer_type, mpz);
}


#define SKY_INTEGER_REVERSE_BINARY(op)                              \
        sky_object_t                                                \
        sky_integer_r##op(sky_integer_t self, sky_object_t other)   \
        {                                                           \
            if (!sky_object_isa(other, sky_integer_type)) {         \
                return sky_NotImplemented;                          \
            }                                                       \
            return sky_integer_##op(other, self);                   \
        }

#define SKY_INTEGER_REVERSE_TERNARY(op)                             \
        sky_object_t                                                \
        sky_integer_r##op(sky_integer_t self, sky_object_t other)   \
        {                                                           \
            if (!sky_object_isa(other, sky_integer_type)) {         \
                return sky_NotImplemented;                          \
            }                                                       \
            return sky_integer_##op(other, self, sky_None);         \
        }

SKY_INTEGER_REVERSE_BINARY(add)
SKY_INTEGER_REVERSE_BINARY(and)
SKY_INTEGER_REVERSE_BINARY(divmod)
SKY_INTEGER_REVERSE_BINARY(floordiv)
SKY_INTEGER_REVERSE_BINARY(lshift)
SKY_INTEGER_REVERSE_BINARY(mod)
SKY_INTEGER_REVERSE_BINARY(mul)
SKY_INTEGER_REVERSE_BINARY(or)
SKY_INTEGER_REVERSE_TERNARY(pow)
SKY_INTEGER_REVERSE_BINARY(rshift)
SKY_INTEGER_REVERSE_BINARY(sub)
SKY_INTEGER_REVERSE_BINARY(truediv)
SKY_INTEGER_REVERSE_BINARY(xor)


static const char sky_integer_type_doc[] =
"int(x[, base]) -> integer\n"
"\n"
"Convert a string or number to an integer, if possible. A floating\n"
"point argument will be truncated towards zero (this does not include a\n"
"string representation of a floating point number!) When converting a\n"
"string, use the optional base. It is an error to supply a base when\n"
"converting a non-string.";


SKY_TYPE_DEFINE_SIMPLE(integer,
                       "int",
                       sizeof(sky_integer_data_t),
                       NULL,
                       sky_integer_instance_finalize,
                       NULL,
                       0,
                       sky_integer_type_doc);

sky_integer_t const sky_integer_one = SKY_OBJECT_MAKETAG(1, 1);
sky_integer_t const sky_integer_zero = SKY_OBJECT_MAKETAG(1, 0);


void
sky_integer_initialize_library(void)
{
    sky_type_setattr_getset(
            sky_integer_type,
            "real",
            "the real part of a complex number",
            sky_integer_real_getter,
            NULL);
    sky_type_setattr_getset(
            sky_integer_type,
            "imag",
            "the imaginary part of a complex number",
            sky_integer_imag_getter,
            NULL);
    sky_type_setattr_getset(
            sky_integer_type,
            "numerator",
            "the numerator of a rational number in lowest terms",
            sky_integer_numerator_getter,
            NULL);
    sky_type_setattr_getset(
            sky_integer_type,
            "denominator",
            "the denominator of a rational number in lowest terms",
            sky_integer_denominator_getter,
            NULL);

    sky_type_setattr_builtin(
            sky_integer_type,
            "__ceil__",
            sky_function_createbuiltin(
                    "int.__ceil__",
                    "Ceiling of an Integral returns itself.",
                    (sky_native_code_function_t)sky_integer_copy,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_integer_type,
            "__floor__",
            sky_function_createbuiltin(
                    "int.__floor__",
                    "Flooring an Integral returns itself.",
                    (sky_native_code_function_t)sky_integer_copy,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_integer_type,
            "__trunc__",
            sky_function_createbuiltin(
                    "int.__trunc__",
                    "Truncating in Integral returns itself.",
                    (sky_native_code_function_t)sky_integer_copy,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_integer_type,
            "bit_length",
            sky_function_createbuiltin(
                    "int.bit_length",
                    "int.bit_length() -> int\n\n"
                    "Number of bits necessary to represent self in binary.\n"
                    ">>> bin(37)\n"
                    "'0b100101'\n"
                    ">>> (37).bit_length()\n"
                    "6",
                    (sky_native_code_function_t)sky_integer_bit_length,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_integer_type,
            "conjugate",
            sky_function_createbuiltin(
                    "int.conjugate",
                    "Returns self, the complex conjugate of any int.",
                    (sky_native_code_function_t)sky_integer_copy,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    NULL));

    sky_type_setattr_builtin(
            sky_integer_type,
            "from_bytes",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "int.from_bytes",
                            sky_integer_from_bytes_doc,
                            (sky_native_code_function_t)sky_integer_from_bytes,
                            SKY_DATA_TYPE_OBJECT_INTEGER,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "bytes", SKY_DATA_TYPE_OBJECT, NULL,
                            "byteorder", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                            "signed", SKY_DATA_TYPE_BOOL, sky_False,
                            NULL)));
    sky_type_setattr_builtin(
            sky_integer_type,
            "to_bytes",
            sky_function_createbuiltin(
                    "int.to_bytes",
                    sky_integer_to_bytes_doc,
                    (sky_native_code_function_t)sky_integer_to_bytes,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INTEGER, NULL,
                    "length", SKY_DATA_TYPE_INDEX, NULL,
                    "byteorder", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "*", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "signed", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_integer_type,
            "__new__",
            (sky_native_code_function_t)sky_integer_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "x", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "base", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_integer_type,
            "__repr__", sky_integer_repr,
            "__format__", sky_integer_format,
            "__lt__", sky_integer_lt,
            "__le__", sky_integer_le,
            "__eq__", sky_integer_eq,
            "__ne__", sky_integer_ne,
            "__gt__", sky_integer_gt,
            "__ge__", sky_integer_ge,
            "__hash__", sky_integer_hash,
            "__bool__", sky_integer_bool,
            "__sizeof__", sky_integer_sizeof,

            "__add__", sky_integer_add,
            "__sub__", sky_integer_sub,
            "__mul__", sky_integer_mul,
            "__truediv__", sky_integer_truediv,
            "__floordiv__", sky_integer_floordiv,
            "__mod__", sky_integer_mod,
            "__divmod__", sky_integer_divmod,
            "__pow__", sky_integer_pow,
            "__lshift__", sky_integer_lshift,
            "__rshift__", sky_integer_rshift,
            "__and__", sky_integer_and,
            "__xor__", sky_integer_xor,
            "__or__", sky_integer_or,

            "__radd__", sky_integer_radd,
            "__rsub__", sky_integer_rsub,
            "__rmul__", sky_integer_rmul,
            "__rtruediv__", sky_integer_rtruediv,
            "__rfloordiv__", sky_integer_rfloordiv,
            "__rmod__", sky_integer_rmod,
            "__rdivmod__", sky_integer_rdivmod,
            "__rpow__", sky_integer_rpow,
            "__rlshift__", sky_integer_rlshift,
            "__rrshift__", sky_integer_rrshift,
            "__rand__", sky_integer_rand,
            "__rxor__", sky_integer_rxor,
            "__ror__", sky_integer_ror,

            "__neg__", sky_integer_neg,
            "__pos__", sky_integer_pos,
            "__abs__", sky_integer_abs,
            "__invert__", sky_integer_invert,
            "__complex__", sky_integer_complex,
            "__int__", sky_integer_int,
            "__float__", sky_integer_float,
            "__round__", sky_integer_round,
            "__index__", sky_integer_index,

            "__getnewargs__", sky_integer_getnewargs,
            NULL);
}
