#include "sky_private.h"


void
sky_once_init(sky_once_t *once)
{
    *once = 0;
}


void
sky_once_run(volatile sky_once_t *  once,
             sky_once_initialize_t  function,
             void *                 arg)
{
    while (*once != 2) {
        if (!*once && sky_atomic_cas32(0, 1, once)) {
            SKY_ERROR_TRY {
                function(arg);
                *once = 2;
            }
            SKY_ERROR_EXCEPT_ANY {
                *once = 0;
                sky_error_raise();
            } SKY_ERROR_TRY_END;
        }
        else {
            while (1 == *once) {
                sky_atomic_pause();
                sky_system_yield();
            }
        }
    }
}


void
sky_once_run_unsafe(volatile sky_once_t *   once,
                    sky_once_initialize_t   function,
                    void *                  arg)
{
    while (*once != 2) {
        if (!*once && sky_atomic_cas32(0, 1, once)) {
            function(arg);
            *once = 2;
        }
        else {
            while (1 == *once) {
                sky_atomic_pause();
                sky_system_yield();
            }
        }
    }
}
