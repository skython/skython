/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_parameter_list Parameter Lists
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_PARAMETER_LIST_H__
#define __SKYTHON_CORE_SKY_PARAMETER_LIST_H__ 1


#include "sky_base.h"
#include "sky_data_type.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new parameter list object instance.
  * The created parameter list object instance will be created without any
  * parameters. Use sky_parameter_list_add() to add parameters.
  *
  * @return     a new parameter list object instance.
  */
SKY_EXTERN sky_parameter_list_t
sky_parameter_list_create(void);


/** Create a new parameter list object instance with parameters.
  * The newly created object instance will be created with the parameters as
  * specified in the argument list when called. For each parameter, three
  * or more arguments are required: the keyword for the parameter, the data
  * type for the argument, the default value for the argument, and optionally
  * the extra data required by the data type. If the data type does not require
  * extra information, the extra data should not be specified. The first
  * keyword is not specified as part of the variable argument list, but is
  * instead a required argument. The list of parameters is terminated by a
  * @c NULL keyword.
  *
  * @param[in]  keyword     the keyword for the first parameter.
  * @param[in]  ...         the variable argument list of parameter information
  *                         as described in the description section above.
  *
  * @return     a new parameter list object instance populated with parameters.
  */
SKY_EXTERN sky_parameter_list_t
sky_parameter_list_createwithparameters(const char *keyword, ...);


/** Create a new parameter list object instance with parameters.
  * The newly created object instance will be created with the parameters as
  * specified in the argument list when called. For each parameter, three
  * or more arguments are required: the keyword for the parameter, the data
  * type for the argument, the default value for the argument, and optionally
  * the extra data required by the data type. If the data type does not require
  * extra information, the extra data should not be specified. The first
  * keyword is not specified as part of the variable argument list, but is
  * instead a required argument. The list of parameters is terminated by a
  * @c NULL keyword.
  *
  * @param[in]  keyword     the keyword for the first parameter.
  * @param[in]  ap          the variable argument list of parameter information
  *                         as described in the description section above.
  *
  * @return     a new parameter list object instance populated with parameters.
  */
SKY_EXTERN sky_parameter_list_t
sky_parameter_list_createwithparametersv(const char *keyword, va_list ap);


/** Add a parameter to a parameter list.
  * Parameters are defined in the following order, all of which are completely
  * optional:
  *
  * @li required position parameters
  * @li positional parameters with default values
  * @li *args
  * @li required keyword-only parameters (PEP 3102)
  * @li keyword parameters with default values
  * @li **kws
  *
  * There is no defined limit to the number of parameters that a function may
  * define; however, the implementation limits the number of parameters to
  * @c SSIZE_MAX.
  *
  * @param[in]  list        the parameter list object to which the parameter is
  *                         to be added.
  * @param[in]  keyword     the keyword to assign to the parameter, which must
  *                         be unique in the parameter list.
  * @param[in]  data_type   the data type to expect when arguments are applied
  *                         to the parameter list.
  * @param[in]  value       the default value to assign to the parameter if an
  *                         argument is not supplied when arguments are applied
  *                         to the parameter list. May be @c NULL if there is
  *                         to be no default.
  * @param[in]  ...         if @a data_type is SKY_DATA_TYPE_OBJECT_INSTANCEOF,
  *                         an extra argument should be added that is the type
  *                         of the object instance to accept (@c sky_type_t).
  */
SKY_EXTERN void
sky_parameter_list_add(sky_parameter_list_t list,
                       sky_string_t         keyword,
                       sky_data_type_t      data_type,
                       sky_object_t         value,
                       ...);


/** Return a tuple containing the defaults assigned to positional parameters.
  * If the parameter list contains no positional parameters having defaults,
  * @c sky_tuple_empty will be returned.
  *
  * @param[in]  list        the parameter list for which the tuple of defaults
  *                         is to be returned.
  * @return     a tuple containing the default values assigned to positional
  *             parameters.
  */
SKY_EXTERN sky_tuple_t
sky_parameter_list_defaults(sky_parameter_list_t list);


/** Set the defaults for positional parameters.
  * Any existing defaults for positional parameters will be completely replaced
  * by the new defaults, which may be @c NULL to remove all default values for
  * positional parameters. If the tuple contains more items than there are
  * positional parameters in the list, the values at the tail of the list of
  * defaults will be used rather than those at the head of the list.
  *
  * @param[in]  list        the parameter list for which the default values are
  *                         to be set.
  * @param[in]  defaults    the default values to set, or @c NULL if all
  *                         defaults are to be removed, making all positional
  *                         parameters required.
  */
SKY_EXTERN void
sky_parameter_list_setdefaults(sky_parameter_list_t list,
                               sky_tuple_t          defaults);


/** Return a dictionary containing the defaults assigned to keyword parameters.
  * If the parameter list contains no keyword parameters have defaults, an
  * dict object will be returned.
  *
  * @param[in]  list        the parameter list for which the dict of keyword
  *                         defaults is to be returned.
  * @return     a dict containing the default values assigned to keyword
  *             parameters.
  */
SKY_EXTERN sky_dict_t
sky_parameter_list_kwdefaults(sky_parameter_list_t list);


/** Set the default for keyword parameters.
  * Any existing default for keyword parameters will be completely replaced by
  * the new defaults, which may be @c NULL to remove all default values for
  * keyword parameters.
  *
  * @param[in]  list        the parameter list for which the default keyword
  *                         values are to be set.
  * @param[in]  kwdefaults  the default values to set, or @c NULL if all
  *                         defaults are to be removed, making all keyword
  *                         parameters required.
  */
SKY_EXTERN void
sky_parameter_list_setkwdefaults(sky_parameter_list_t list,
                                 sky_dict_t           kwdefaults);


/** Apply arguments to a parameter list, returning a list of arguments ordered
  * as they are in the parameter list.
  * Positional and keyword arguments are applied to the parameters in the
  * supplied parameter list according to the algorithm described in PEP 3102.
  * Data types are checked (@c sky_TypeError will be raised when an argument
  * type does not match a parameter type) and defaults are applied. The return
  * is a list of the arguments ordered as they're specified by the parameter
  * list, converted to the appropriate data type. The list should be freed
  * using sky_free() when it is no longer needed.
  *
  * @param[in]  list        the parameter list to which arguments are to be
  *                         applied.
  * @param[in]  args        positional arguments to be applied.
  * @param[in]  kws         keyword arguments to be applied.
  * @param[in]  name        if an error is raised, use this name in the error
  *                         text.
  * @return     a list of the arguments ordered as they're specified by the
  *             parameter list, converted to the appropriate data type.
  */
SKY_EXTERN void **
sky_parameter_list_apply(sky_parameter_list_t       list,
                         sky_tuple_t                args,
                         sky_dict_t                 kws,
                         sky_string_t               name);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PARAMETER_LIST_H__ */

/** @} **/
/** @} **/
