#include "sky_private.h"


void
sky_type_dict_clear(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item deletion",
                           sky_type_name(sky_object_type(self)));
}


void
sky_type_dict_delitem(sky_object_t self, SKY_UNUSED sky_object_t key)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item deletion",
                           sky_type_name(sky_object_type(self)));
}


sky_object_t
sky_type_dict_pop(           sky_object_t   self,
                  SKY_UNUSED sky_object_t   key,
                  SKY_UNUSED sky_object_t   value)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item deletion",
                           sky_type_name(sky_object_type(self)));
}


sky_object_t
sky_type_dict_popitem(sky_object_t self)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item deletion",
                           sky_type_name(sky_object_type(self)));
}


sky_object_t
sky_type_dict_setdefault(           sky_object_t    self,
                         SKY_UNUSED sky_object_t    key,
                         SKY_UNUSED sky_object_t    value)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item assignment",
                           sky_type_name(sky_object_type(self)));
}


void
sky_type_dict_setitem(           sky_object_t   self,
                      SKY_UNUSED sky_object_t   key,
                      SKY_UNUSED sky_object_t   value)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item assignment",
                           sky_type_name(sky_object_type(self)));
}


void
sky_type_dict_update(           sky_object_t    self,
                     SKY_UNUSED sky_tuple_t     args,
                     SKY_UNUSED sky_dict_t      kws)
{
    sky_error_raise_format(sky_TypeError,
                           "%#@ object does not support item assignment",
                           sky_type_name(sky_object_type(self)));
}


SKY_TYPE_DEFINE_SIMPLE(type_dict,
                       "type_dict",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       0,
                       NULL);

void
sky_type_dict_initialize_library(void)
{
    sky_type_setattr_builtin(
            sky_type_dict_type,
            "clear",
            sky_function_createbuiltin(
                    "type_dict.clear",
                    NULL,
                    (sky_native_code_function_t)sky_type_dict_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_type_dict_type,
            "pop",
            sky_function_createbuiltin(
                    "type_dict.pop",
                    NULL,
                    (sky_native_code_function_t)sky_type_dict_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            sky_type_dict_type,
            "popitem",
            sky_function_createbuiltin(
                    "type_dict.popitem",
                    NULL,
                    (sky_native_code_function_t)sky_type_dict_popitem,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_type_dict_type,
            "setdefault",
            sky_function_createbuiltin(
                    "type_dict.setdefault",
                    NULL,
                    (sky_native_code_function_t)sky_type_dict_setdefault,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_type_dict_type,
            "update",
            sky_function_createbuiltin(
                    "type_dict.update",
                    NULL,
                    (sky_native_code_function_t)sky_type_dict_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));

    sky_type_setmethodslots(sky_type_dict_type,
            "__setitem__", sky_type_dict_setitem,
            "__delitem__", sky_type_dict_delitem,
            NULL);
}
