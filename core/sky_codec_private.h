#ifndef __SKYTHON_CORE_SKY_CODEC_PRIVATE_H__
#define __SKYTHON_CORE_SKY_CODEC_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_object_private.h"


SKY_CDECLS_BEGIN


extern sky_type_t sky_codec_charmap_type;


typedef struct sky_codec_charmap_data_s {
    int32_t                             count2;
    int32_t                             count3;
    uint8_t                             level1[32];
    uint8_t *                           level23;
} sky_codec_charmap_data_t;

SKY_EXTERN_INLINE sky_codec_charmap_data_t *
sky_codec_charmap_data(sky_object_t object)
{
    return sky_object_data(object, sky_codec_charmap_type);
}

typedef struct sky_codec_charmap_s {
    sky_object_data_t                   object_data;
    sky_codec_charmap_data_t            charmap_data;
} *sky_codec_charmap_t;


extern const sky_codec_t *sky_codec_filesystemencoding_codec;
extern sky_string_t sky_codec_filesystemencoding_string;
extern sky_string_t sky_codec_filesystemerrors_string;


#define SKY_CODEC_ENCODER_FLAG_ASCII    0x80000000


extern void
sky_codec_encodewithcontext(sky_codec_context_t *context);


extern ssize_t
sky_codec_decode_ascii(void *               state,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       uint32_t *           flags,
                       const void *         bytes,
                       ssize_t              nbytes,
                       sky_bool_t           final);

extern ssize_t
sky_codec_decode_charmap(void *             state,
                         sky_string_t       encoding,
                         sky_string_t       errors,
                         sky_codec_output_t output,
                         void *             output_arg,
                         uint32_t *         flags,
                         const void *       bytes,
                         ssize_t            nbytes,
                         sky_bool_t         final);

extern ssize_t
sky_codec_decode_escape(void *              state,
                        sky_string_t        encoding,
                        sky_string_t        errors,
                        sky_codec_output_t  output,
                        void *              output_arg,
                        uint32_t *          flags,
                        const void *        bytes,
                        ssize_t             nbytes,
                        sky_bool_t          final);

extern ssize_t
sky_codec_decode_latin1(void *              state,
                        sky_string_t        encoding,
                        sky_string_t        errors,
                        sky_codec_output_t  output,
                        void *              output_arg,
                        uint32_t *          flags,
                        const void *        bytes,
                        ssize_t             nbytes,
                        sky_bool_t          final);

extern ssize_t
sky_codec_decode_locale(void *              state,
                        sky_string_t        encoding,
                        sky_string_t        errors,
                        sky_codec_output_t  output,
                        void *              output_arg,
                        uint32_t *          flags,
                        const void *        bytes,
                        ssize_t             nbytes,
                        sky_bool_t          final);

extern ssize_t
sky_codec_decode_raw_unicode_escape(void *              state,
                                    sky_string_t        encoding,
                                    sky_string_t        errors,
                                    sky_codec_output_t  output,
                                    void *              output_arg,
                                    uint32_t *          flags,
                                    const void *        bytes,
                                    ssize_t             nbytes,
                                    sky_bool_t          final);

extern ssize_t
sky_codec_decode_unicode_escape(void *              state,
                                sky_string_t        encoding,
                                sky_string_t        errors,
                                sky_codec_output_t  output,
                                void *              output_arg,
                                uint32_t *          flags,
                                const void *        bytes,
                                ssize_t             nbytes,
                                sky_bool_t          final);

extern ssize_t
sky_codec_decode_unicode_internal(void *                state,
                                  sky_string_t          encoding,
                                  sky_string_t          errors,
                                  sky_codec_output_t    output,
                                  void *                output_arg,
                                  uint32_t *            flags,
                                  const void *          bytes,
                                  ssize_t               nbytes,
                                  sky_bool_t            final);

extern ssize_t
sky_codec_decode_utf16(void *               state,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       uint32_t *           flags,
                       const void *         bytes,
                       ssize_t              nbytes,
                       sky_bool_t           final);

extern void *
sky_codec_decode_utf16_be_initialize(sky_string_t       encoding,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags);

extern ssize_t
sky_codec_decode_utf16_be(void *                state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          uint32_t *            flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final);

extern void *
sky_codec_decode_utf16_le_initialize(sky_string_t       encoding,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags);

extern ssize_t
sky_codec_decode_utf16_le(void *                state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          uint32_t *            flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final);

extern ssize_t
sky_codec_decode_utf32(void *               state,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       uint32_t *           flags,
                       const void *         bytes,
                       ssize_t              nbytes,
                       sky_bool_t           final);

extern void *
sky_codec_decode_utf32_be_initialize(sky_string_t       encoding,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags);

extern ssize_t
sky_codec_decode_utf32_be(void *                state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          uint32_t *            flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final);

extern void *
sky_codec_decode_utf32_le_initialize(sky_string_t       encoding,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags);

extern ssize_t
sky_codec_decode_utf32_le(void *                state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          uint32_t *            flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final);

extern ssize_t
sky_codec_decode_utf7(void *                state,
                      sky_string_t          encoding,
                      sky_string_t          errors,
                      sky_codec_output_t    output,
                      void *                output_arg,
                      uint32_t *            flags,
                      const void *          bytes,
                      ssize_t               nbytes,
                      sky_bool_t            final);

extern ssize_t
sky_codec_decode_utf8(void *                state,
                      sky_string_t          encoding,
                      sky_string_t          errors,
                      sky_codec_output_t    output,
                      void *                output_arg,
                      uint32_t *            flags,
                      const void *          bytes,
                      ssize_t               nbytes,
                      sky_bool_t            final);


extern void sky_codec_encode_ascii(sky_codec_context_t *context);
extern void sky_codec_encode_charmap(sky_codec_context_t *context);
extern void sky_codec_encode_escape(sky_codec_context_t *context);
extern void sky_codec_encode_latin1(sky_codec_context_t *context);
extern void sky_codec_encode_locale(sky_codec_context_t *context);
extern void sky_codec_encode_raw_unicode_escape(sky_codec_context_t *context);
extern void sky_codec_encode_unicode_escape(sky_codec_context_t *context);
extern void sky_codec_encode_unicode_internal(sky_codec_context_t *context);
extern void sky_codec_encode_utf16_initialize(sky_codec_context_t *context);
extern void sky_codec_encode_utf16_be(sky_codec_context_t *context);
extern void sky_codec_encode_utf16_le(sky_codec_context_t *context);
extern void sky_codec_encode_utf32_initialize(sky_codec_context_t *context);
extern void sky_codec_encode_utf32_be(sky_codec_context_t *context);
extern void sky_codec_encode_utf32_le(sky_codec_context_t *context);
extern void sky_codec_encode_utf7(sky_codec_context_t *context);
extern void sky_codec_encode_utf8(sky_codec_context_t *context);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CODEC_PRIVATE_H__ */
