/** @file
  * @brief Generic lock-free, dynamically sized hash table
  * This hash table implementation is a low level hash table that is intended
  * to be used where Skython objects cannot otherwise be used (i.e., where
  * sky_lockfree_dict would normally suffice). This implementation is
  * thread-safe and lock-free.
  *
  * @defgroup sky_lockfree_hashtable Generic Lock-Free hash Table
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_LOCKFREE_HASHTABLE_H__
#define __SKYTHON_CORE_SKY_LOCKFREE_HASHTABLE_H__ 1


#include "sky_base.h"
#include "sky_hashtable.h"
#include "sky_hazard_pointer.h"


SKY_CDECLS_BEGIN


/** A structure representing an item stored in the hash table.
  * This structure should be present at the head of a larger structure that
  * includes the data to be stored in the hash table. Normally this might
  * include a copy or reference to the actual key, as well as additional data
  * that is associated with the key.
  *
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private and
  * should only be accessed via the proper functions.
  */
typedef struct sky_lockfree_hashtable_item_s sky_lockfree_hashtable_item_t;
struct sky_lockfree_hashtable_item_s {
    /** @cond **/
    sky_lockfree_hashtable_item_t * volatile    next;
    uintptr_t                                   key_hash;
    /** @endcond **/
};

/** @cond **/
typedef struct sky_lockfree_hashtable_segment_s sky_lockfree_hashtable_segment_t;
struct sky_lockfree_hashtable_segment_s {
    sky_lockfree_hashtable_item_t * volatile    items[0];
};
/** @endcond **/


typedef struct sky_lockfree_hashtable_s sky_lockfree_hashtable_t;


/** A structure representing a lock-free hashtable iterator.
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private and
  * should only be accessed via the proper functions.
  */
typedef struct sky_lockfree_hashtable_iterator_s {
    /** @cond **/
    sky_lockfree_hashtable_t *                  table;
    sky_lockfree_hashtable_item_t * volatile *  head;
    sky_lockfree_hashtable_item_t * volatile *  prev;
    sky_lockfree_hashtable_item_t * volatile *  marker;
    sky_lockfree_hashtable_item_t *             current;
    sky_lockfree_hashtable_item_t *             next;
    sky_hazard_pointer_t *                      hazards[3];

    sky_lockfree_hashtable_item_t **            visited;
    size_t                                      visited_cursor;
    size_t                                      visited_size;
    size_t                                      visited_used;
    sky_bool_t                                  retrying;
    sky_bool_t                                  safe;
    /** @endcond **/
} sky_lockfree_hashtable_iterator_t;

/** The signature of a function to be called to compare an existing hash table
  * item with a key.
  * This function will only ever be called for an item that has the same hash
  * value as the key.
  *
  * @param[in]  item    the existing hash table item to be compared.
  * @param[in]  key     the key to be compared.
  * @return     @c SKY_TRUE if the @a key matches the @a item, or
  *             @c SKY_FALSE if it does not.
  */
typedef sky_bool_t
(*sky_lockfree_hashtable_item_compare_t)(sky_lockfree_hashtable_item_t * item,
                                         void *                          key);

/** The signature of a function to be called when an item is deleted from a
  * hash table.
  *
  * @param[in]  table   the hash table from which the item is being deleted.
  * @param[in]  item    the item being deleted.
  */
typedef void
(*sky_lockfree_hashtable_item_delete_t)(sky_lockfree_hashtable_t *      table,
                                        sky_lockfree_hashtable_item_t * item);

/** A structure representing a lock-free hash table.
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private and
  * should only be accessed via the proper functions.
  */
struct sky_lockfree_hashtable_s {
    /** @cond **/
    void * volatile                             tlsdata;

    volatile intmax_t                           global_length;
    volatile intmax_t                           global_max_length;

    volatile size_t                             internal_sizeof;

    sky_lockfree_hashtable_item_compare_t       item_compare;
    sky_lockfree_hashtable_item_delete_t        item_delete;
    sky_free_t                                  item_free;

    sky_lockfree_hashtable_segment_t * volatile segments[SKY_HASHTABLE_SEGMENT_COUNT];
    /** @endcond **/
};


/** Initialize a hash table for use.
  * Initializes a hash table for use. Sufficient memory for the table
  * itself must be allocated prior to calling this function, and the memory
  * must be initialized with zero bytes. Use sky_lockfree_hashtable_cleanup()
  * when the table is no longer needed.
  *
  * @param[in]  table           the table to initialize.
  * @param[in]  item_compare    the function to call to compare an existing
  *                             item with a key. Must not be @c NULL.
  * @param[in]  item_delete     the function to call when an item is being
  *                             deleted from the table. May be @c NULL.
  * @param[in]  item_free       the function to call to free the memory for
  *                             an item.
  */
SKY_EXTERN void
sky_lockfree_hashtable_init(sky_lockfree_hashtable_t *              table,
                            sky_lockfree_hashtable_item_compare_t   item_compare,
                            sky_lockfree_hashtable_item_delete_t    item_delete,
                            sky_free_t                              item_free);

/** Allocate memory for a hash table, and initialize it for use.
  * Use sky_lockfree_hashtable_destroy() when the table is no longer needed.
  *
  * @param[in]  item_compare    the function to call to compare an existing
  *                             item with a key. Must not be @c NULL.
  * @param[in]  item_delete     the function to call when an item is being
  *                             deleted from the table. May be @c NULL.
  * @param[in]  item_free       the function to call to free the memory for
  *                             an item.
  * @return     a new hash table ready for use.
  */
SKY_EXTERN sky_lockfree_hashtable_t *
sky_lockfree_hashtable_create(sky_lockfree_hashtable_item_compare_t item_compare,
                              sky_lockfree_hashtable_item_delete_t  item_delete,
                              sky_free_t                            item_free);

/** Clean up a previously initialized hash table when it is no longer needed.
  * The memory for the hash table itself is not released in any way. This
  * function should be used for tables initialized via
  * sky_lockfree_hashtable_init().
  *
  * @param[in]  table       the table to clean up.
  */
SKY_EXTERN void
sky_lockfree_hashtable_cleanup(sky_lockfree_hashtable_t *table);

/** Clean up a previously initialized hash table and deallocate its memory.
  * This functino should be used for tables initialized via
  * sky_lockfree_hashtable_create().
  *
  * @param[in]  table       the table to destroy.
  */
SKY_EXTERN void
sky_lockfree_hashtable_destroy(sky_lockfree_hashtable_t *table);


/** Return the number of items present in the hash table.
  *
  * @param[in]  table       the table for which the number of items it contains
  *                         is to be returned.
  * @return     the number of items contained by @a table.
  */
SKY_EXTERN size_t
sky_lockfree_hashtable_length(sky_lockfree_hashtable_t *table);


/** Return the number of bytes being used internally by a hash table.
  * The value returned does not include memory allocated to the hash table
  * itself if it was allocated via sky_hashtable_create(). It includes only
  * internal memory that is not accessible externally. The value returned can
  * be considered overhead. It does not include memory allocated for items
  * added to the table via sky_hashtable_insert().
  *
  * @param[in]  table       the table for which the amount of memory being
  *                         used internally is to be returned.
  * @return     the amount of memory being used internally.
  */
SKY_EXTERN_INLINE size_t
sky_lockfree_hashtable_sizeof(sky_lockfree_hashtable_t *table)
{
    return table->internal_sizeof;
}


/** Set internal capacity for the a hash table.
  * The internal capacity for a table can only ever be increased. This is
  * useful primarily when a hash table is first created if it is known that it
  * is going to be a very large table.
  *
  * @param[in]  table       the table on which to set the capacity.
  * @param[in]  capacity    the capacity to set, which will be rounded up to
  *                         the next closest power of 2.
  */
SKY_EXTERN void
sky_lockfree_hashtable_setcapacity(sky_lockfree_hashtable_t *   table,
                                   size_t                       capacity);


/** Delete an item from a hash table.
  * The @a key_hash and @a key parameters will be used to find the item in the
  * hash table. If the hash value is not found in the table at all, the return
  * will be @c SKY_FALSE. If the hash value is found, the table's item
  * comparison function will be called with @a key for each item having the
  * same hash value. If none of the keys match, the return will be
  * @c SKY_FALSE; otherwise, the matching item will be deleted, and the return
  * will be @c SKY_TRUE.
  *
  * @param[in]  table       the table from which the item is to be deleted.
  * @param[in]  key_hash    the hash value for the key to be deleted.
  * @param[in]  key         the key to be deleted.
  * @return     @c SKY_TRUE if the key was found in the table and an item was
  *             deleted, or @c SKY_FALSE if the key was not found.
  */
SKY_EXTERN sky_bool_t
sky_lockfree_hashtable_delete(sky_lockfree_hashtable_t *table,
                              uintptr_t                 key_hash,
                              void *                    key);

/** Delete an item from a hash table via pointer identity.
  * The @a item parameter will be used to find the item in the hash table. The
  * normal key lookup process is bypassed, instead looking for the item by its
  * pointer address. If the item is not found in the table at all, the return
  * will be @c SKY_FALSE. If the item is found, item will be removed from the
  * table, and the return will be @c SKY_TRUE.
  *
  * @param[in]  table
  * @param[in]  item
  * @return     @c SKY_TRUE if the item was found in the table and was removed,
  *             or @c SKY_FALSE if the item was not found.
  */
SKY_EXTERN sky_bool_t
sky_lockfree_hashtable_deleteitem(sky_lockfree_hashtable_t *        table,
                                  sky_lockfree_hashtable_item_t *   item);

/** Insert a new item into a hash table.
  * The caller is responsible for pre-allocating the new item to be inserted
  * into the hash table. The @a key_hash and @a key parameters will be used
  * to find the appropriate location in the hash table for the item. If an
  * existing item is found with a matching hash value and key, the new item
  * will not be inserted, and the return will be the existing item; otherwise,
  * the new item will be inserted, and the return will be that item. A copy of
  * the item is not made, and @a new_item becomes property of the table when
  * it is inserted; however, if it is not inserted, it is the responsibility
  * of the caller to release its memory.
  *
  * @param[in]  table       the table into which @a new_item is to be inserted.
  * @param[in]  new_item    the new item to insert into the @a table.
  * @param[in]  key_hash    the hash value of the key.
  * @param[in]  key         the key.
  * @return     the item in the hashtable, which will be @a new_item if the new
  *             item was inserted; otherwise, it will be the existing item in
  *             the table.
  */
SKY_EXTERN sky_hazard_pointer_t *
sky_lockfree_hashtable_insert(sky_lockfree_hashtable_t *        table,
                              sky_lockfree_hashtable_item_t *   new_item,
                              uintptr_t                         key_hash,
                              void *                            key);

/** Find an item in a hash table.
  * The @a key_hash and @a key parameters will be used to find the item in the
  * hash table. If the hash value is not found in the table at all, the return
  * will be @c NULL. If the hash value is found, the table's item comparison
  * function will be called with @a key for each item having the same hash
  * value. If none of the keys match, the return will be @c NULL; otherwise,
  * the matching item will be returned.
  *
  * If the item is found, it will be returned wrapped in a hazard pointer,
  * because it could be deleted by another thread before it's even returned.
  * The caller is responsible for releasing the hazard pointer when it's no
  * longer needed via sky_hazard_pointer_release().
  *
  * @param[in]  table       the table to search.
  * @param[in]  key_hash    the hash value of the key to be found.
  * @param[in]  key         the key to be found.
  * @return     the item (wrapped in a hazard pointer) if its key is found, or
  *             @c NULL if an item does not exist for the key.
  */
SKY_EXTERN sky_hazard_pointer_t *
sky_lockfree_hashtable_lookup(sky_lockfree_hashtable_t *    table,
                              uintptr_t                     key_hash,
                              void *                        key);

/** Delete an item from the hash table and return it.
  * The returned item is wrapped in a hazard pointer that the caller is
  * responsible for releasing via sky_hazard_pointer_release() when it is no
  * longer needed. If @a key is not @c NULL, the item for that key will be
  * returned and removed from the hash table; otherwise, an arbitrary item will
  * be chosen. The return will be @c NULL if the table is empty or the
  * requested @a key is not found in the table.
  *
  * @param[in]  table       the table from which an item is to be popped.
  * @param[in]  key_hash    the hash value of the key to be popped; ignored if
  *                         @a key is @c NULL.
  * @param[in]  key         the key to be popped, which may be @c NULL for an
  *                         arbitrary key to be chosen.
  * @return     an item from the hash table wrapped in a hazard pointer, or
  *             @c NULL if the table is empty or @a key was not found.
  */
SKY_EXTERN sky_hazard_pointer_t *
sky_lockfree_hashtable_pop(sky_lockfree_hashtable_t *   table,
                           uintptr_t                    key_hash,
                           void *                       key);


/** Initialize an iterator structure for iterating a lock-free hash table.
  * Memory for the iterator should be pre-allocated, but does not need to be
  * filled with zero bytes. Due to the thread-safe, lock-free nature of the
  * hash table, iterating it safely is tricky business. Items may be inserted
  * or deleted by other threads while the table is being iterated. To help
  * guard against the same item being returned more than once during iteration,
  * @a safe can be specified as @c SKY_TRUE. This type of iteration is slower,
  * because it must consult a list of previously returned items, but the same
  * item will never be returned twice. There is still no guarantee that items
  * will not be missed, however. When the iterator is no longer needed, it
  * should be cleaned up via sky_lockfree_hashtable_iterator_cleanup().
  *
  * @param[in]  iterator    the iterator to initialize.
  * @param[in]  table       the table to iterate.
  * @param[in]  head        the starting point for iteration. This should
  *                         normally be @c NULL. It's provided as an argument
  *                         for internal use.
  * @param[in]  safe        @c SKY_TRUE if guards against the same item being
  *                         returned more than once should be used.
  */
SKY_EXTERN void
sky_lockfree_hashtable_iterator_init(
        sky_lockfree_hashtable_iterator_t *         iterator,
        sky_lockfree_hashtable_t *                  table,
        sky_lockfree_hashtable_item_t * volatile *  head,
        sky_bool_t                                  safe);

/** Clean up a previous initialized iterator.
  *
  * @param[in]  iterator    the iterator to clean up.
  */
SKY_EXTERN void
sky_lockfree_hashtable_iterator_cleanup(
        sky_lockfree_hashtable_iterator_t *         iterator);

/** Reset an iterator to re-start at the beginning of the table.
  *
  * @param[in]  iterator    the iterator to reset.
  */
SKY_EXTERN void
sky_lockfree_hashtable_iterator_reset(
        sky_lockfree_hashtable_iterator_t *         iterator);

/** Return the next item from an iterator.
  * The item returned here is not guarded with a hazard pointer, at least not
  * visibily. There is an internal hazard pointer protecting the item, at
  * least until another iterator function is called for the same iterator.
  *
  * @param[in]  iterator    the iterator from which the next item is to be
  *                         returned.
  * @return     the next item in the iteration, or @c NULL if there are no more
  *             items.
  */
SKY_EXTERN sky_lockfree_hashtable_item_t *
sky_lockfree_hashtable_iterator_next(
        sky_lockfree_hashtable_iterator_t *         iterator);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_LOCKFREE_HASHTABLE_H__ */

/** @} **/
