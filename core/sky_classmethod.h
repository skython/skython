/** @file
  * @brief
  * @defgroup sky_function Functions
  * @{
  * @defgroup sky_classmethod Class methods
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_CLASSMETHOD_H__
#define __SKYTHON_CORE_SKY_CLASSMETHOD_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new class method object, binding a callable object to an object
  * instance's type.
  *
  * @param[in]  function    the callable object to bind.
  * @return     the new class method object.
  */
SKY_EXTERN sky_classmethod_t
sky_classmethod_create(sky_object_t function);


/** Return the function object instance that a class method object is wrapping.
  *
  * @param[in]  method      the class method object to query.
  * @return     the function object that @a method is wrapping.
  */
SKY_EXTERN sky_object_t
sky_classmethod_function(sky_classmethod_t method);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CLASSMETHOD_H__ */

/** @} **/
/** @} **/
