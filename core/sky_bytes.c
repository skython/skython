#include "sky_private.h"
#include "sky_bytestring.h"
#include "sky_bytes_private.h"


typedef struct sky_bytes_iterator_data_s {
    sky_bytes_t                         bytes;
    ssize_t                             next_index;
} sky_bytes_iterator_data_t;

SKY_EXTERN_INLINE sky_bytes_iterator_data_t *
sky_bytes_iterator_data(sky_object_t object)
{
    if (sky_bytes_iterator_type == sky_object_type(object)) {
        return (sky_bytes_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_bytes_iterator_type);
}


struct sky_bytes_iterator_s {
    sky_object_data_t                   object_data;
    sky_bytes_iterator_data_t           iterator_data;
};


sky_bytestring_t *
sky_bytes_bytes(sky_object_t object, sky_bytestring_t *tagged_bytes)
{
    if (SKY_OBJECT_TAG(object) == 7) {
        tagged_bytes->bytes = tagged_bytes->u.tiny_bytes;
        tagged_bytes->size = sizeof(tagged_bytes->u.tiny_bytes);
        switch (((uintptr_t)object >> 4) & 0xF) {
            case 0: /* 0x0607 == sky_bytes_empty */
                if (SKY_OBJECT_TAGGED_BYTES_EMPTY == object) {
                    tagged_bytes->used = 0;
                    tagged_bytes->bytes[0] = 0;
                    return tagged_bytes;
                }
                break;
            case 1: /* 0xaa17 == single byte bytes */
                tagged_bytes->used = 1;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 8) & 0xFF;
                tagged_bytes->bytes[1] = 0;
                return tagged_bytes;
            case 2: /* 0xaabb27 == double byte bytes */
                tagged_bytes->used = 2;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[2] = 0;
                return tagged_bytes;
            case 3: /* 0xaabbcc37 == triple byte bytes */
                tagged_bytes->used = 3;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 24) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[2] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[3] = 0;
                return tagged_bytes;
#if defined(SKY_ARCH_64BIT)
            case 4: /* 0xaabbccdd47 == quadruple byte bytes */
                tagged_bytes->used = 4;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 32) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >> 24) & 0xFF;
                tagged_bytes->bytes[2] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[3] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[4] = 0;
                return tagged_bytes;
            case 5: /* 0xaabbccddee57 == quintuple byte bytes */
                tagged_bytes->used = 5;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 40) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >> 32) & 0xFF;
                tagged_bytes->bytes[2] = ((uintptr_t)object >> 24) & 0xFF;
                tagged_bytes->bytes[3] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[4] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[5] = 0;
                return tagged_bytes;
            case 6: /* 0xaabbccddeeff67 == sextuple byte bytes */
                tagged_bytes->used = 6;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 48) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >> 40) & 0xFF;
                tagged_bytes->bytes[2] = ((uintptr_t)object >> 32) & 0xFF;
                tagged_bytes->bytes[3] = ((uintptr_t)object >> 24) & 0xFF;
                tagged_bytes->bytes[4] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[5] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[6] = 0;
                return tagged_bytes;
            case 7: /* 0xaabbccddeeffgg77 == septuple byte bytes */
                tagged_bytes->used = 7;
                tagged_bytes->bytes[0] = ((uintptr_t)object >> 56) & 0xFF;
                tagged_bytes->bytes[1] = ((uintptr_t)object >> 48) & 0xFF;
                tagged_bytes->bytes[2] = ((uintptr_t)object >> 40) & 0xFF;
                tagged_bytes->bytes[3] = ((uintptr_t)object >> 32) & 0xFF;
                tagged_bytes->bytes[4] = ((uintptr_t)object >> 24) & 0xFF;
                tagged_bytes->bytes[5] = ((uintptr_t)object >> 16) & 0xFF;
                tagged_bytes->bytes[6] = ((uintptr_t)object >>  8) & 0xFF;
                tagged_bytes->bytes[7] = 0;
                return tagged_bytes;
#endif
        }
    }

    return &(sky_bytes_data(object)->bytes);
}

static void
sky_bytes_instance_buffer_acquire(sky_object_t  self,
                                  void *        data,
                                  sky_buffer_t *buffer,
                                  unsigned int  flags)
{
    sky_bytestring_t    *bytes, *tagged_bytes;

    if (!sky_object_istagged(self)) {
        sky_bytes_data_t    *self_data = data;

        tagged_bytes = NULL;
        bytes = &(self_data->bytes);
    }
    else {
        tagged_bytes = sky_malloc(sizeof(sky_bytestring_t));
        bytes = sky_bytes_bytes(self, tagged_bytes);
    }

    sky_buffer_initialize(buffer, bytes->bytes, bytes->used, SKY_TRUE, flags);
    buffer->internal = tagged_bytes;
}

static void
sky_bytes_instance_buffer_release(SKY_UNUSED sky_object_t   object,
                                  SKY_UNUSED void *         data,
                                             sky_buffer_t * buffer)
{
    sky_free(buffer->internal);
    buffer->internal = NULL;
}

static void
sky_bytes_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_bytestring_cleanup(&(((sky_bytes_data_t *)data)->bytes));
}

static void
sky_bytes_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_bytes_iterator_data_t   *iterator_data = data;

    sky_object_visit(iterator_data->bytes, visit_data);
}


static inline void
sky_bytes_bufferasbytes(sky_buffer_t *      buffer,
                        sky_object_t        object,
                        sky_bytestring_t *  bytes)
{
    sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    sky_asset_save(buffer,
                   (sky_free_t)sky_buffer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);

    bytes->bytes = buffer->buf;
    bytes->used = buffer->len;
    bytes->size = buffer->len;
    bytes->u.free = NULL;
}


static sky_bytes_t
sky_bytes_clone(sky_bytes_t self, sky_type_t type)
{
    sky_bytes_t         clone;
    sky_bytestring_t    *clone_bytes, *self_bytes, self_tagged_bytes;

    if (sky_bytes_type == type && sky_object_istagged(self)) {
        return self;
    }

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);

    clone = sky_object_allocate(type);
    clone_bytes = sky_bytes_bytes(clone, NULL);
    if (self_bytes->used + 1 <= (ssize_t)sizeof(clone_bytes->u.tiny_bytes)) {
        clone_bytes->bytes = clone_bytes->u.tiny_bytes;
        clone_bytes->size = sizeof(clone_bytes->u.tiny_bytes);
        memcpy(clone_bytes->bytes, self_bytes->bytes, self_bytes->used + 1);
    }
    else {
        clone_bytes->bytes = sky_memdup(self_bytes->bytes, self_bytes->used);
        clone_bytes->size = sky_memsize(clone_bytes->bytes);
        clone_bytes->u.free = sky_free;
    }
    clone_bytes->used = self_bytes->used;

    return clone;
}


sky_bytes_t
sky_bytes_createfrombytestring(const sky_bytestring_t *bytestring)
{
    sky_bytes_t         object;
    sky_bytestring_t    *bytes;

    if (!bytestring->used) {
        return sky_bytes_empty;
    }
    if (1 == bytestring->used) {
        return SKY_BYTES_SINGLE(bytestring->bytes[0]);
    }

    object = sky_object_allocate(sky_bytes_type);
    bytes = sky_bytes_bytes(object, NULL);
    if (bytestring->used < (ssize_t)sizeof(bytes->u.tiny_bytes)) {
        bytes->bytes = bytes->u.tiny_bytes;
        memcpy(bytes->bytes, bytestring->bytes, bytestring->size);
        bytes->size = bytestring->size;
    }
    else {
        bytes->bytes = memcpy(sky_malloc(bytestring->used + 1),
                              bytestring->bytes,
                              bytestring->used);
        bytes->size = sky_memsize(bytes->bytes);
        bytes->u.free = sky_free;
    }
    bytes->used = bytestring->used;
    bytes->bytes[bytes->used] = '\0';

    return object;
}


sky_bytes_t
sky_bytes_createwithbytestring(sky_bytestring_t *bytestring)
{
    sky_bytes_t         object;
    sky_bytestring_t    *bytes;

    if (!bytestring->used) {
        sky_bytestring_cleanup(bytestring);
        return sky_bytes_empty;
    }
    if (1 == bytestring->used) {
        object = SKY_BYTES_SINGLE(bytestring->bytes[0]);
        sky_bytestring_cleanup(bytestring);
        return object;
    }

    object = sky_object_allocate(sky_bytes_type);
    bytes = sky_bytes_bytes(object, NULL);
    if (bytestring->bytes == bytestring->u.tiny_bytes) {
        bytes->bytes = bytes->u.tiny_bytes;
        memcpy(bytes->bytes, bytestring->bytes, bytestring->size);
    }
    else {
        bytes->bytes = bytestring->bytes;
        bytes->u.free = bytestring->u.free;
    }
    bytes->size = bytestring->size;
    bytes->used = bytestring->used;

    return object;
}


static void
sky_bytes_parse_sub_integer(sky_object_t        sub,
                            uint8_t *           byte,
                            sky_buffer_t *      sub_buffer,
                            sky_bytestring_t *  sub_bytes)
{
    volatile sky_bool_t buffer = SKY_FALSE;

    SKY_ERROR_TRY {
        *byte = sky_integer_value(sky_number_index(sub), 0, 255, NULL);
        sky_bytestring_initwithbytes(sub_bytes, byte, 1, NULL);
    }
    SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_raise_string(sky_ValueError,
                               "byte must be in range(0, 256)");
    }
    SKY_ERROR_EXCEPT_ANY {
        buffer = SKY_TRUE;
    } SKY_ERROR_TRY_END;

    if (buffer) {
        /* This must outside of the TRY block because it may save assets. */
        sky_bytes_bufferasbytes(sub_buffer, sub, sub_bytes);
    }
}


sky_bytes_t
sky_bytes_add(sky_bytes_t self, sky_object_t other)
{
    ssize_t             result_len;
    sky_buffer_t        other_buffer;
    sky_object_t        result;
    sky_bytestring_t    other_bytes, *result_bytes,
                        *self_bytes, self_tagged_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&other_buffer, other, &other_bytes);

        if (SSIZE_MAX - self_bytes->used <= other_bytes.used) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }
        if (!(result_len = self_bytes->used + other_bytes.used)) {
            result = sky_bytes_empty;
        }
        else if (1 == result_len) {
            if (self_bytes->used) {
                result = SKY_BYTES_SINGLE(self_bytes->bytes[0]);
            }
            else {
                result = SKY_BYTES_SINGLE(other_bytes.bytes[0]);
            }
        }
        else {
            result = sky_object_allocate(sky_bytes_type);
            result_bytes = sky_bytes_bytes(result, NULL);
            sky_bytestring_initfrombytes(result_bytes,
                                         self_bytes->bytes,
                                         self_bytes->used,
                                         result_len);
            sky_bytestring_concat(result_bytes, &other_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


uint8_t
sky_bytes_byteat(sky_bytes_t self, ssize_t index)
{
    sky_bytestring_t    *self_bytes, self_tagged_bytes;
    
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (index < 0) {
        index += self_bytes->used;
    }
    if (index < 0 || index >= self_bytes->used) {
        sky_error_raise_format(sky_IndexError,
                               "%#@ index out of range",
                               sky_type_name(sky_object_type(self)));
    }

    return self_bytes->bytes[index];
}


sky_bytes_t
sky_bytes_capitalize(sky_bytes_t self)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_capitalize(self_bytes, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_capitalize_doc =
"B.capitalize() -> copy of B\n\n"
"Return a copy of B with only its first character capitalized (ASCII)\n"
"and the rest lower-cased.";


sky_bytes_t
sky_bytes_center(sky_bytes_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytes_t         result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes, *self_bytes, self_tagged_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_center(self_bytes,
                                   width,
                                   fillchar_bytes.bytes[0],
                                   &result_bytes))
        {
            result = sky_bytes_copy(self);
        }
        else {
            result = sky_bytes_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_center_doc =
"B.center(width[, fillchar]) -> copy of B\n\n"
"Return B centered in a string of length width.  Padding is\n"
"done using the specified fill character (default is a space).";


sky_object_t
sky_bytes_compare(sky_bytes_t       self,
                  sky_object_t      other,
                  sky_compare_op_t  compare_op)
{
    sky_bytestring_t    other_tagged_bytes, self_tagged_bytes;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_bytes_type)) {
        if ((SKY_COMPARE_OP_EQUAL == compare_op ||
             SKY_COMPARE_OP_NOT_EQUAL == compare_op) &&
            (sky_interpreter_flags() & (SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS |
                                        SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS)) &&
            sky_object_isa(other, sky_string_type))
        {
            sky_error_warn_string(sky_BytesWarning,
                                  "Comparison between bytes and string");
        }
        return sky_NotImplemented;
    }

    return (sky_bytestring_compare(sky_bytes_bytes(self, &self_tagged_bytes),
                                   sky_bytes_bytes(other, &other_tagged_bytes),
                                   compare_op) ? sky_True : sky_False);
}


sky_bool_t
sky_bytes_contains(sky_bytes_t self, sky_object_t item)
{
    ssize_t             pos;
    uint8_t             byte;
    sky_buffer_t        item_buffer;
    sky_bytestring_t    item_bytes, *self_bytes, self_tagged_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        if (!SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            sky_bytes_bufferasbytes(&item_buffer, item, &item_bytes);
        }
        else {
            byte = sky_integer_value(sky_number_index(item),
                                     0, 255,
                                     sky_ValueError);
            sky_bytestring_initwithbytes(&item_bytes, &byte, 1, NULL);
        }

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        pos = sky_bytestring_find(self_bytes, &item_bytes, 0, SSIZE_MAX);
    } SKY_ASSET_BLOCK_END;

    return (pos >= 0 ? SKY_TRUE : SKY_FALSE);
}


sky_bytes_t
sky_bytes_copy(sky_bytes_t self)
{
    if (sky_bytes_type == sky_object_type(self)) {
        return self;
    }
    return sky_bytes_clone(self, sky_bytes_type);
}


ssize_t
sky_bytes_count(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end)
{
    ssize_t             count;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    sub_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_parse_sub_integer(sub, &sub_byte, &sub_buffer, &sub_bytes);
        count = sky_bytestring_count(self_bytes, &sub_bytes, start, end, -1);
    } SKY_ASSET_BLOCK_END;

    return count;
}

static const char *sky_bytes_count_doc =
"B.count(sub[, start[, end]]) -> int\n\n"
"Return the number of non-overlapping occurrences of substring sub in\n"
"string B[start:end].  Optional arguments start and end are interpreted\n"
"as in slice notation.";


sky_bytes_t
sky_bytes_createfrombytes(const void *bytes, size_t nbytes)
{
    sky_bytes_t bytes_object;

    if (!nbytes) {
        return sky_bytes_empty;
    }
    else if (1 == nbytes) {
        return SKY_BYTES_SINGLE(*(uint8_t *)bytes);
    }
    if (nbytes > SSIZE_MAX) {
        sky_error_raise_string(sky_ValueError, "source data too large");
    }

    bytes_object = sky_object_allocate(sky_bytes_type);
    sky_bytestring_initfrombytes(sky_bytes_bytes(bytes_object, NULL),
                                 bytes,
                                 nbytes,
                                 0);

    return bytes_object;
}


sky_bytes_t
sky_bytes_createwithbytes(const void *bytes, size_t nbytes, sky_free_t free)
{
    sky_bytes_t bytes_object;

    if (!nbytes) {
        if (free) {
            free((void *)bytes);
        }
        return sky_bytes_empty;
    }
    else if (1 == nbytes) {
        bytes_object = SKY_BYTES_SINGLE(*(uint8_t *)bytes);
        if (free) {
            free((void *)bytes);
        }
        return bytes_object;
    }
    if (nbytes > SSIZE_MAX) {
        sky_error_raise_string(sky_ValueError, "source data too large");
    }

    bytes_object = sky_object_allocate(sky_bytes_type);
    sky_bytestring_initwithbytes(sky_bytes_bytes(bytes_object, NULL),
                                 (void *)bytes,
                                 nbytes,
                                 free);

    return bytes_object;
}


sky_bytes_t
sky_bytes_createfromformat(const char *format, ...)
{
    va_list     ap;
    sky_bytes_t bytes_object;

    va_start(ap, format);
    bytes_object = sky_bytes_createfromformatv(format, ap);
    va_end(ap);

    return bytes_object;
}


sky_bytes_t
sky_bytes_createfromformatv(const char *format, va_list ap)
{
    char        *bytes;
    size_t      nbytes;
    sky_bytes_t bytes_object;

    nbytes = sky_format_vasprintf(&bytes, format, ap);
    if (!nbytes) {
        bytes_object = sky_bytes_empty;
        sky_free(bytes);
    }
    else if (1 == nbytes) {
        bytes_object = SKY_BYTES_SINGLE(*(uint8_t *)bytes);
        sky_free(bytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ON_ERROR);
            if (nbytes > SSIZE_MAX) {
                sky_error_raise_string(sky_ValueError, "source data too large");
            }

            bytes_object = sky_object_allocate(sky_bytes_type);
            sky_bytestring_initwithbytes(sky_bytes_bytes(bytes_object, NULL),
                                         bytes,
                                         nbytes,
                                         sky_free);
        } SKY_ASSET_BLOCK_END;
    }

    return bytes_object;
}


sky_bytes_t
sky_bytes_createfromobject(sky_object_t object)
{
    ssize_t             value;
    uint8_t             byte;
    sky_bytes_t         bytes_object;
    sky_buffer_t        buffer;
    sky_object_t        item, iterator;
    sky_bytestring_t    *bytes;

    if (sky_bytes_type == sky_object_type(object)) {
        return object;
    }

    if (sky_buffer_check(object)) {
        bytes_object = sky_object_allocate(sky_bytes_type);
        bytes = sky_bytes_bytes(bytes_object, NULL);

        sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_bytestring_initfrombytes(bytes, buffer.buf, buffer.len, 0);
        sky_buffer_release(&buffer);

        return bytes_object;
    }

    if (SKY_OBJECT_METHOD_SLOT(object, INDEX)) {
        object = sky_number_index(object);
        value = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
        if (value < 0) {
            sky_error_raise_string(sky_ValueError, "negative count");
        }

        if (!value) {
            return sky_bytes_empty;
        }
        if (1 == value) {
            return (sky_bytes_t)0x17;
        }

        bytes_object = sky_object_allocate(sky_bytes_type);
        bytes = sky_bytes_bytes(bytes_object, NULL);
        sky_bytestring_initfrombytes(bytes, NULL, value, value);
        memset(bytes->bytes, 0x00, bytes->used);
        return bytes_object;
    }

    if (sky_object_isiterable(object)) {
        bytes_object = sky_object_allocate(sky_bytes_type);
        bytes = sky_bytes_bytes(bytes_object, NULL);
        sky_bytestring_initfrombytes(bytes,
                                     NULL,
                                     0,
                                     sky_object_length_hint(object, 0));

        SKY_ERROR_TRY {
            if (sky_sequence_isiterable(object)) {
                SKY_SEQUENCE_FOREACH(object, item) {
                    item = sky_number_index(item);
                    byte = sky_integer_value(item, 0, 255, NULL);
                    sky_bytestring_appendbytes(bytes, &byte, 1);
                } SKY_SEQUENCE_FOREACH_END;
            }
            else {
                iterator = sky_object_iter(object);
                while ((item = sky_object_next(iterator, NULL)) != NULL) {
                    item = sky_number_index(item);
                    byte = sky_integer_value(item, 0, 255, NULL);
                    sky_bytestring_appendbytes(bytes, &byte, 1);
                }
            }
        }
        SKY_ERROR_EXCEPT(sky_OverflowError) {
            sky_error_raise_string(sky_ValueError,
                                   "bytes must be in range(0, 256)");
        } SKY_ERROR_TRY_END;

        return bytes_object;
    }

    sky_error_raise_format(sky_TypeError,
                           "cannot create %#@ object from %#@",
                           sky_type_name(sky_bytes_type),
                           sky_type_name(sky_object_type(object)));
}


const uint8_t *
sky_bytes_cstring(sky_bytes_t self, sky_bool_t terminate)
{
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    /* Tagged objects always return a NUL terminated copy. */
    if (sky_object_istagged(self)) {
        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        return (const uint8_t *)
               sky_asset_strndup((const char *)self_bytes->bytes,
                                 self_bytes->used,
                                 SKY_ASSET_CLEANUP_ALWAYS);
    }

    self_bytes = sky_bytes_bytes(self, NULL);
    if (!terminate) {
        return self_bytes->bytes;
    }
    if (self_bytes->size > self_bytes->used &&
        !self_bytes->bytes[self_bytes->used])
    {
        return self_bytes->bytes;
    }
    return (const uint8_t *)
           sky_asset_strndup((const char *)self_bytes->bytes,
                             self_bytes->used,
                             SKY_ASSET_CLEANUP_ALWAYS);
}


sky_string_t
sky_bytes_decode(sky_bytes_t self, sky_string_t encoding, sky_string_t errors)
{
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    if (sky_bytes_empty == self) {
        return sky_string_empty;
    }
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    return sky_bytestring_decode(self_bytes, encoding, errors);
}

static const char *sky_bytes_decode_doc =
"B.decode(encoding='utf-8', errors='strict') -> str\n\n"
"Decode B using the codec registered for encoding. Default encoding\n"
"is 'utf-8'. errors may be given to set a different error\n"
"handling scheme.  Default is 'strict' meaning that encoding errors raise\n"
"a UnicodeDecodeError.  Other possible values are 'ignore' and 'replace'\n"
"as well as any other name registered with codecs.register_error that is\n"
"able to handle UnicodeDecodeErrors.";


sky_bool_t
sky_bytes_endswith(sky_bytes_t  self,
                   sky_object_t suffix,
                   ssize_t      start,
                   ssize_t      end)
{
    sky_bool_t  endswith = SKY_FALSE;

    sky_buffer_t        sub_buffer;
    sky_object_t        object;
    sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_object_isa(suffix, sky_tuple_type)) {
        if (!sky_buffer_check(suffix)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "endswith first arg must be bytes or a tuple of bytes, "
                    "not %#@",
                    sky_type_name(sky_object_type(suffix)));
        }
        SKY_ASSET_BLOCK_BEGIN {
            sky_bytes_bufferasbytes(&sub_buffer, suffix, &sub_bytes);
            endswith = sky_bytestring_endswith(self_bytes,
                                               &sub_bytes,
                                               start,
                                               end);
        } SKY_ASSET_BLOCK_END;
    }
    else {
        SKY_SEQUENCE_FOREACH(suffix, object) {
            SKY_ASSET_BLOCK_BEGIN {
                sky_bytes_bufferasbytes(&sub_buffer, object, &sub_bytes);
                endswith = sky_bytestring_endswith(self_bytes,
                                                   &sub_bytes,
                                                   start,
                                                   end);
            } SKY_ASSET_BLOCK_END;
            if (endswith) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }

    return endswith;
}

static const char *sky_bytes_endswith_doc =
"B.endswith(suffix[, start[, end]]) -> bool\n\n"
"Return True if B ends with the specified suffix, False otherwise.\n"
"With optional start, test B beginning at that position.\n"
"With optioanl end, stop comparing B at that position.\n"
"suffix can also be a tuple of bytes to try.";


sky_object_t
sky_bytes_eq(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_bytes_t
sky_bytes_expandtabs(sky_bytes_t self, ssize_t tabsize)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;
        
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_expandtabs(self_bytes, tabsize, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_expandtabs_doc =
"B.expandtabs([tabsize]) -> copy of B\n\n"
"Return a copy of B where all tab characters are expanded using spaces.\n"
"If tabsize is not given, a tab size of 8 characters is assumed.";


ssize_t
sky_bytes_find(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_parse_sub_integer(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_find(self_bytes, &sub_bytes, start, end);
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytes_find_doc =
"B.find(sub[, start[, end]]) -> int\n\n"
"Return the lowest index in B where substring sub is found,\n"
"such that sub is contained within B[start:end].  Optional\n"
"arguments start and end are interpreted as in slice notation.\n\n"
"Return -1 on failure.";


sky_bytes_t
sky_bytes_fromhex(sky_type_t cls, sky_string_t string)
{
    const char          *cstring;
    sky_bytes_t         bytes_object;
    sky_bytestring_t    *bytes;

    if (!sky_type_issubtype(cls, sky_bytes_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ is not a sub-type of %#@",
                               sky_type_name(cls),
                               sky_type_name(sky_bytes_type));
    }
    if (!(cstring = sky_string_cstring(string))) {
        sky_error_raise_string(sky_ValueError,
                               "invalid string for fromhex()");
    }

    if (sky_bytes_type == cls) {
        bytes_object = sky_object_allocate(cls);
    }
    else {
        bytes_object = sky_object_call(cls, NULL, NULL);
        if (!sky_object_isa(bytes_object, sky_bytes_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ does not a produce a sub-type of %#@",
                                   sky_type_name(cls),
                                   sky_type_name(sky_bytes_type));
        }
    }

    bytes = sky_bytes_bytes(bytes_object, NULL);
    sky_bytestring_fromhex(bytes, cstring, sky_string_len(string));
    if (sky_bytes_type == sky_object_type(bytes_object)) {
        if (!bytes->used) {
            return sky_bytes_empty;
        }
        if (1 == bytes->used) {
            return SKY_BYTES_SINGLE(bytes->bytes[0]);
        }
    }

    return bytes_object;
}

const char *sky_bytes_fromhex_doc =
"bytes.fromhex(string) -> bytes\n\n"
"Create a bytes object from a string of hexadecimal numbers.\n"
"Spaces between two numbers are accepted.\n"
"Example: bytes.fromhex('B9 01EF') -> b'\\xb9\\x01\\xef'.";


sky_object_t
sky_bytes_ge(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_bytes_getitem(sky_bytes_t self, sky_object_t item)
{
    ssize_t             i, j, length, start, step, stop;
    sky_object_t        result_object;
    sky_bytestring_t    *result_bytes, *self_bytes, self_tagged_bytes;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        start = sky_integer_value(sky_number_index(item),
                                  SSIZE_MIN, SSIZE_MAX,
                                  sky_IndexError);
        return sky_integer_create(sky_bytes_byteat(self, start));
    }

    if (!sky_object_isa(item, sky_slice_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ indices must be integers",
                               sky_type_name(sky_object_type(self)));
    }

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    length = sky_slice_range(item, self_bytes->used, &start, &stop, &step);
    if (!length) {
        return sky_bytes_empty;
    }
    if (1 == length) {
        return SKY_BYTES_SINGLE(self_bytes->bytes[start]);
    }
    if (length == self_bytes->used && 1 == step) {
        return self;
    }

    result_object = sky_object_allocate(sky_bytes_type);
    result_bytes = sky_bytes_bytes(result_object, NULL);
    if (1 == step) {
        sky_bytestring_initfrombytes(result_bytes,
                                     &(self_bytes->bytes[start]),
                                     length,
                                     length);
    }
    else {
        sky_bytestring_initfrombytes(result_bytes, NULL, length, length);
        for (i = 0, j = start; i < length; ++i, j += step) {
            result_bytes->bytes[i] = self_bytes->bytes[j];
        }
    }

    return result_object;
}


sky_object_t
sky_bytes_getnewargs(sky_bytes_t self)
{
    sky_bytestring_t    *self_bytes, self_tagged_bytes;
                        
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    return sky_object_build("(y#)", self_bytes->bytes, self_bytes->used);
}


sky_object_t
sky_bytes_gt(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_GREATER);
}


uintptr_t
sky_bytes_hash(sky_bytes_t self)
{
    sky_bytes_data_t    *bytes_data;
    sky_bytestring_t    *bytes, tagged_bytes;

    if (sky_object_istagged(self)) {
        if (sky_bytes_empty == self) {
            return 0;
        }
        bytes = sky_bytes_bytes(self, &tagged_bytes);
        return sky_util_hashbytes(bytes->bytes, bytes->used, 0);
    }

    bytes_data = sky_bytes_data(self);
    if (!bytes_data->hashed) {
        bytes_data->hash = sky_util_hashbytes(bytes_data->bytes.bytes,
                                              bytes_data->bytes.used,
                                              0);
        sky_atomic_cas32(SKY_FALSE, SKY_TRUE, &(bytes_data->hashed));
    }

    return bytes_data->hash;
}


ssize_t
sky_bytes_index(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_parse_sub_integer(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_find(self_bytes, &sub_bytes, start, end);
        if (index < 0) {
            sky_error_raise_string(sky_ValueError, "substring not found");
        }
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytes_index_doc =
"B.index(sub[, start[, end]]) ->int\n\n"
"Like B.find() but raise ValueError when the substring is not found.";


sky_bool_t
sky_bytes_isalnum(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_isalnum(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_isalnum_doc =
"B.isalnum() -> bool\n\n"
"Return True if all characters in B are alphanumeric\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytes_isalpha(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_isalpha(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_isalpha_doc =
"B.isalpha() -> bool\n\n"
"Return True if all characters in B are alphabetic\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytes_isdigit(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_isdigit(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_isdigit_doc =
"B.isdigit() -> bool\n\n"
"Return True if all characters in B are digits\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytes_islower(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_islower(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_islower_doc =
"B.islower() -> bool\n\n"
"Return True if all cased characters in B are lowercase and there is\n"
"at least one cased character in B, False otherwise.";


sky_bool_t
sky_bytes_isspace(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_isspace(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_isspace_doc =
"B.isspace() -> bool\n\n"
"Return True if all characters in B are whitespace\n"
"and there is at least one character in B, False otherwise.";


sky_bool_t
sky_bytes_istitle(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_istitle(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_istitle_doc =
"B.istitle() -> bool\n\n"
"Return True if B is a titlecased string and there is at least one\n"
"character in B, i.e. uppercase characters may only follow uncased\n"
"characters and lowercase characters only cased ones. Return False\n"
"otherwise.";


sky_bool_t
sky_bytes_isupper(sky_bytes_t self)
{
    sky_bytestring_t    tagged_bytes;

    return sky_bytestring_isupper(sky_bytes_bytes(self, &tagged_bytes));
}

static const char *sky_bytes_isupper_doc =
"B.isupper() -> bool\n\n"
"Return True if all cased characters in B are uppercase and there is\n"
"at least one cased character in B, False otherwise.";


typedef struct sky_bytes_join_item_s {
    sky_buffer_t                        buffer;
    sky_bytestring_t                    bytes;
} sky_bytes_join_item_t;

typedef struct sky_bytes_join_list_s {
    ssize_t                             len;
    ssize_t                             size;
    sky_bytes_join_item_t               items[0];
} sky_bytes_join_list_t;

static void
sky_bytes_join_list_append(sky_bytes_join_list_t **list, sky_object_t object)
{
    size_t                  new_size;
    sky_buffer_t            *buffer;
    sky_bytestring_t        *bytes;
    sky_bytes_join_item_t   *item;

    if ((*list)->len >= (*list)->size) {
        new_size = sizeof(sky_bytes_join_list_t) +
                   (((*list)->size + 1) * sizeof(sky_bytes_join_item_t));
        (*list) = sky_asset_realloc((*list),
                                    new_size,
                                    SKY_ASSET_CLEANUP_ALWAYS,
                                    SKY_ASSET_UPDATE_ALL_DEEP);
        (*list)->size = (sky_memsize((*list)) - sizeof(sky_bytes_join_list_t)) /
                     sizeof(sky_bytes_join_item_t);
    }
    item = &((*list)->items[(*list)->len]);
    buffer = &(item->buffer);
    bytes = &(item->bytes);

    /* Can't use sky_bytes_bufferasbytes() here, because it'll store an asset
     * in the current asset block to cleanup. That asset block could be the
     * fast sequence iterator's asset block, which means it'll get cleaned up
     * before we're ready to use it. That's kind of the whole point as to why
     * we have the join list in the first place.
     */
    sky_buffer_acquire(buffer, object, SKY_BUFFER_FLAG_SIMPLE);
    bytes->bytes = buffer->buf;
    bytes->used = buffer->len;
    bytes->size = buffer->len;
    bytes->u.free = NULL;

    ++(*list)->len;
}

static void
sky_bytes_join_list_cleanup(sky_bytes_join_list_t *list)
{
    ssize_t i;

    for (i = 0; i < list->len; ++i) {
        sky_buffer_release(&(list->items[i].buffer));
    }
    sky_free(list);
}


sky_bytes_t
sky_bytes_join(sky_bytes_t self, sky_object_t iterable)
{
    ssize_t                 i, size;
    sky_bytes_t             result;
    sky_object_t            iterator, object;
    sky_bytestring_t        *result_bytes, *self_bytes, self_tagged_bytes;
    sky_bytes_join_list_t   *list;

    if (!sky_object_isiterable(iterable)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(iterable)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        size = sizeof(sky_bytes_join_list_t) +
               (sky_object_length_hint(iterable, 8) *
                sizeof(sky_bytes_join_item_t));
        list = sky_malloc(size);
        list->len = 0;
        list->size = (sky_memsize(list) - sizeof(sky_bytes_join_list_t)) /
                     sizeof(sky_bytes_join_item_t);
        sky_asset_save(list,
                       (sky_free_t)sky_bytes_join_list_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        size = 0;
        if (sky_sequence_isiterable(iterable)) {
            SKY_SEQUENCE_FOREACH(iterable, object) {
                sky_bytes_join_list_append(&list, object);
                if (SSIZE_MAX - size < list->items[list->len - 1].bytes.used) {
                    sky_error_raise_string(sky_OverflowError, "result too big");
                }
                size += list->items[list->len - 1].bytes.used;
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iterator = sky_object_iter(iterable);
            while ((object = sky_object_next(iterator, NULL)) != NULL) {
                sky_bytes_join_list_append(&list, object);
                if (SSIZE_MAX - size < list->items[list->len - 1].bytes.used) {
                    sky_error_raise_string(sky_OverflowError, "result too big");
                }
                size += list->items[list->len - 1].bytes.used;
            }
        }
        if (!list->len) {
            result = sky_bytes_empty;
        }
        else {
            self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
            if ((SSIZE_MAX - size) / list->len < self_bytes->used) {
                sky_error_raise_string(sky_OverflowError, "result too big");
            }
            size += (list->len * self_bytes->used);

            result = sky_object_allocate(sky_bytes_type);
            result_bytes = sky_bytes_bytes(result, NULL);
            sky_bytestring_initfrombytes(result_bytes, NULL, 0, size);
            sky_bytestring_concat(result_bytes, &(list->items[0].bytes));
            for (i = 1; i < list->len; ++i) {
                sky_bytestring_concat(result_bytes, self_bytes);
                sky_bytestring_concat(result_bytes, &(list->items[i].bytes));
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_join_doc =
"B.join(iterable_of_bytes) -> bytes\n\n"
"Concatenate any number of bytes objects, with B in between each pair.\n"
"Example: b'.'.join([b'ab', b'pq', b'rs']) -> b'ab.pq.rs'.";


sky_object_t
sky_bytes_iter(sky_bytes_t self)
{
    sky_bytes_iterator_t        iterator;
    sky_bytes_iterator_data_t   *iterator_data;

    iterator = sky_object_allocate(sky_bytes_iterator_type);
    iterator_data = sky_bytes_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->bytes)), self, iterator);

    return iterator;
}


sky_object_t
sky_bytes_le(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


ssize_t
sky_bytes_len(sky_bytes_t self)
{
    if (sky_object_istagged(self)) {
        if (sky_bytes_empty == self) {
            return 0;
        }
        switch ((uintptr_t)self & 0xFF) {
            case 0x17:
                return 1;
            case 0x27:
                return 2;
            case 0x37:
                return 3;
#if defined(SKY_ARCH_64BIT)
            case 0x47:
                return 4;
            case 0x57:
                return 5;
            case 0x67:
                return 6;
            case 0x77:
                return 7;
#endif
        }
        /* Fall through -- sky_bytes_bytes() will raise an exception for a
         * bad tagged object.
         */
    }

    return sky_bytes_bytes(self, NULL)->used;
}


sky_bytes_t
sky_bytes_ljust(sky_bytes_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes, *self_bytes, self_tagged_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_ljust(self_bytes,
                                  width,
                                  fillchar_bytes.bytes[0],
                                  &result_bytes))
        {
            result = sky_bytes_copy(self);
        }
        else {
            result = sky_bytes_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_ljust_doc =
"B.ljust(width[, fillchar]) -> copy of B\n\n"
"Return B left justified in a string of length width. Padding is\n"
"done using the specified fill character (default is a space).";


sky_bytes_t
sky_bytes_lower(sky_bytes_t self)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_lower(self_bytes, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_lower_doc =
"B.lower() -> copy of B\n\n"
"Return a copy of B with all ASCII characters converted to lowercase.";


sky_bytes_t
sky_bytes_lstrip(sky_bytes_t self, sky_object_t bytes)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bool_t          stripped;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (sky_object_isnull(bytes)) {
        stripped = sky_bytestring_lstrip(self_bytes, NULL, &result_bytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytes_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_lstrip(self_bytes,
                                             &bytes_bytes,
                                             &result_bytes);
        } SKY_ASSET_BLOCK_END;
    }

    if (!stripped) {
        return sky_bytes_copy(self);
    }
    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_lstrip_doc =
"B.lstrip([bytes]) -> bytes\n\n"
"Strip leading bytes contained in the argument.\n"
"If the argument is omitted, strip leading ASCII whitespace.";


sky_object_t
sky_bytes_lt(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_bytes_t
sky_bytes_maketrans(sky_object_t from, sky_object_t to)
{
    sky_bytes_t         result;
    sky_bytestring_t    *result_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t        from_buffer, to_buffer;
        sky_bytestring_t    from_bytes, to_bytes;

        sky_bytes_bufferasbytes(&from_buffer, from, &from_bytes);
        sky_bytes_bufferasbytes(&to_buffer, to, &to_bytes);

        if (from_bytes.used != to_bytes.used) {
            sky_error_raise_string(sky_ValueError,
                                   "maketrans arguments must have same length");
        }

        result = sky_object_allocate(sky_bytes_type);
        result_bytes = sky_bytes_bytes(result, NULL);
        sky_bytestring_initfrombytes(result_bytes, NULL, 0, 256);
        sky_bytestring_maketrans(&from_bytes, &to_bytes, result_bytes);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_maketrans_doc =
"bytes.maketrans(from, to) -> translation table\n\n"
"Return a translation table (a bytes object of length 256) suitable\n"
"for use in the bytes or bytearray translate method where each byte\n"
"in from is mapped to the byte at the same position in to.\n"
"The bytes objects from and to must be of the same length.";


sky_bytes_t
sky_bytes_mul(sky_bytes_t self, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot multiply %#@ by non-int of type %#@",
                               sky_type_name(sky_object_type(self)),
                               sky_type_name(sky_object_type(other)));
    }
    other = sky_number_index(other);
    count = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);

    return sky_bytes_repeat(self, count);
}


sky_object_t
sky_bytes_ne(sky_bytes_t self, sky_object_t other)
{
    return sky_bytes_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_bytes_t
sky_bytes_new(sky_type_t    cls,
              sky_object_t  source,
              sky_string_t  encoding,
              sky_string_t  errors)
{
    sky_bytes_t bytes_object;

    if (sky_bytes_type != cls) {
        if (!sky_type_issubtype(cls, sky_bytes_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "%@.__new__(%@): %@ is not a subtype of %@",
                                   sky_type_name(sky_bytes_type),
                                   sky_type_name(cls),
                                   sky_type_name(cls),
                                   sky_type_name(sky_bytes_type));
        }
        bytes_object = sky_bytes_new(sky_bytes_type, source, encoding, errors);
        return sky_bytes_clone(bytes_object, cls);
    }

    if ((encoding || errors) && !sky_object_isa(source, sky_string_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "encoding or errors specified for non-string source");
    }

    if (!source) {
        return sky_bytes_empty;
    }
    if (sky_object_isa(source, sky_string_type)) {
        if (!encoding) {
            sky_error_raise_string(sky_TypeError,
                                   "encoding is required for string source");
        }
        return sky_codec_encode(NULL, source, encoding, errors, 0);
    }

    if (!(bytes_object = sky_object_bytes(source))) {
        sky_error_raise_format(sky_TypeError, 
                               "cannot create %#@ from %#@",
                               sky_type_name(cls),
                               sky_type_name(sky_object_type(source)));
    }

    return bytes_object;
}


sky_tuple_t
sky_bytes_partition(sky_bytes_t self, sky_object_t sep)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             index;
        sky_buffer_t        sep_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sep_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&sep_buffer, sep, &sep_bytes);
        if (!sep_bytes.used) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        index = sky_bytestring_find(self_bytes, &sep_bytes, 0, SSIZE_MAX);
        if (index < 0) {
            result = sky_tuple_pack(3, sky_bytes_copy(self),
                                       sky_bytes_empty,
                                       sky_bytes_empty);
        }
        else {
            result = sky_object_build(
                            "(y#Oy#)",
                            self_bytes->bytes, index,
                            sep,
                            self_bytes->bytes + index + sep_bytes.used,
                            self_bytes->used - index - sep_bytes.used);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_partition_doc =
"B.partition(sep) -> (head, sep, tail)\n\n"
"Search for the separator sep in B, and return the part before it,\n"
"the separator itself, and the part after it.  If the separator is not\n"
"found, returns B and two empty bytes objects.";


sky_bytes_t
sky_bytes_radd(sky_bytes_t self, sky_object_t other)
{
    ssize_t             result_len;
    sky_buffer_t        other_buffer;
    sky_object_t        result;
    sky_bytestring_t    other_bytes, *result_bytes,
                        *self_bytes, self_tagged_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&other_buffer, other, &other_bytes);

        if (SSIZE_MAX - self_bytes->used <= other_bytes.used) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }
        if (!(result_len = self_bytes->used + other_bytes.used)) {
            result = sky_bytes_empty;
        }
        else if (1 == result_len) {
            if (self_bytes->used) {
                result = SKY_BYTES_SINGLE(self_bytes->bytes[0]);
            }
            else {
                result = SKY_BYTES_SINGLE(other_bytes.bytes[0]);
            }
        }
        else {
            result = sky_object_allocate(sky_bytes_type);
            result_bytes = sky_bytes_bytes(result, NULL);
            sky_bytestring_initfrombytes(result_bytes,
                                         other_bytes.bytes,
                                         other_bytes.used,
                                         result_len);
            sky_bytestring_concat(result_bytes, self_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bytes_t
sky_bytes_repeat(sky_bytes_t self, ssize_t count)
{
    sky_object_t        result;
    sky_bytestring_t    *result_bytes, *self_bytes, self_tagged_bytes;

    if (count <= 0) {
        return sky_bytes_empty;
    }
    if (1 == count) {
        return self;
    }
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if ((SSIZE_MAX / count) - 1 < self_bytes->used) {
        sky_error_raise_string(sky_OverflowError, "result too big");
    }

    result = sky_object_allocate(sky_bytes_type);
    result_bytes = sky_bytes_bytes(result, NULL);
    sky_bytestring_initfrombytes(result_bytes,
                                 self_bytes->bytes,
                                 self_bytes->used,
                                 self_bytes->used * count);
    sky_bytestring_repeat(result_bytes, count);

    return result;
}


sky_bytes_t
sky_bytes_replace(sky_bytes_t   self,
                  sky_object_t  old_object,
                  sky_object_t  new_object,
                  ssize_t       count)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t        new_buffer, old_buffer;
        sky_bytestring_t    new_bytes, old_bytes,
                            *self_bytes, self_tagged_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&old_buffer, old_object, &old_bytes);
        sky_bytes_bufferasbytes(&new_buffer, new_object, &new_bytes);

        result = sky_object_allocate(sky_bytes_type);
        if (!sky_bytestring_replace(self_bytes,
                                    &old_bytes,
                                    &new_bytes,
                                    count,
                                    sky_bytes_bytes(result, NULL)))
        {
            result = sky_bytes_copy(self);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_replace_doc =
"B.replace(old, new[, count]) -> bytes\n\n"
"Return a copy of B with all occurrences of subsection\n"
"old replaced by new.  If the optional argument count is\n"
"given, only first count occurrences are replaced.";


sky_string_t
sky_bytes_repr(sky_bytes_t self)
{
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    return sky_bytestring_repr(self_bytes, "b", NULL);
}


ssize_t
sky_bytes_rfind(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_parse_sub_integer(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_rfind(self_bytes, &sub_bytes, start, end);
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytes_rfind_doc =
"B.rfind(sub[, start[, end]]) -> int\n\n"
"Return the highest index in B where substring sub is found,\n"
"such that sub is contained within B[start:end].  Optional\n"
"arguments start and end are interpreted as in slice notation.\n\n"
"Return -1 on failure.";


ssize_t
sky_bytes_rindex(sky_bytes_t self, sky_object_t sub, ssize_t start, ssize_t end)
{
    ssize_t index;

    SKY_ASSET_BLOCK_BEGIN {
        uint8_t             sub_byte;
        sky_buffer_t        sub_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_parse_sub_integer(sub, &sub_byte, &sub_buffer, &sub_bytes);
        index = sky_bytestring_rfind(self_bytes, &sub_bytes, start, end);
        if (index < 0) {
            sky_error_raise_string(sky_ValueError, "substring not found");
        }
    } SKY_ASSET_BLOCK_END;

    return index;
}

static const char *sky_bytes_rindex_doc =
"B.rindex(sub[, start[, end]]) -> int\n\n"
"Like B.rfind() but raise ValueError when the substring is not found.";


sky_bytes_t
sky_bytes_rjust(sky_bytes_t self, ssize_t width, sky_object_t fillchar)
{
    sky_bytes_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

        sky_buffer_t        fillchar_buffer;
        sky_bytestring_t    fillchar_bytes, *self_bytes, self_tagged_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&fillchar_buffer, fillchar, &fillchar_bytes);
        if (fillchar_bytes.used != 1) {
            sky_error_raise_format(sky_TypeError,
                                   "must be a byte string of length 1, not %#@",
                                   sky_type_name(sky_object_type(fillchar)));
        }

        if (!sky_bytestring_rjust(self_bytes,
                                  width,
                                  fillchar_bytes.bytes[0],
                                  &result_bytes))
        {
            result = sky_bytes_copy(self);
        }
        else {
            result = sky_bytes_createwithbytestring(&result_bytes);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_rjust_doc =
"B.rjust(width[, fillchar]) -> copy of B\n\n"
"Return B right justified in a string of length width. Padding is\n"
"done using the specified fill character (default is a space).";


sky_tuple_t
sky_bytes_rpartition(sky_bytes_t self, sky_object_t sep)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             index;
        sky_buffer_t        sep_buffer;
        sky_bytestring_t    *self_bytes, self_tagged_bytes, sep_bytes;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        sky_bytes_bufferasbytes(&sep_buffer, sep, &sep_bytes);
        if (!sep_bytes.used) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        index = sky_bytestring_rfind(self_bytes, &sep_bytes, 0, SSIZE_MAX);
        if (index < 0) {
            result = sky_tuple_pack(3, sky_bytes_empty,
                                       sky_bytes_empty,
                                       sky_bytes_copy(self));
        }
        else {
            result = sky_object_build(
                            "(y#Oy#)",
                            self_bytes->bytes, index,
                            sep,
                            self_bytes->bytes + index + sep_bytes.used,
                            self_bytes->used - index - sep_bytes.used);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char *sky_bytes_rpartition_doc =
"B.rpartition(sep) -> (head, sep, tail)\n\n"
"Search for the separator sep in B, starting at the end of B,\n"
"and return the part before it, the separator itself,a nd the\n"
"part after it.  If the separator is not found, returns two empty\n"
"bytes objects and B.";


sky_list_t
sky_bytes_rsplit(sky_bytes_t self, sky_object_t sep, ssize_t maxsplit)
{
    static sky_bytestring_createfrombytes_t createfrombytes =
            (sky_bytestring_createfrombytes_t)sky_bytes_createfrombytes;

    sky_list_t          result;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (sky_object_isnull(sep)) {
        result = sky_bytestring_rsplit(self_bytes,
                                       NULL,
                                       maxsplit,
                                       createfrombytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t        sep_buffer;
            sky_bytestring_t    sep_bytes;

            sky_bytes_bufferasbytes(&sep_buffer, sep, &sep_bytes);
            result = sky_bytestring_rsplit(self_bytes,
                                           &sep_bytes,
                                           maxsplit,
                                           createfrombytes);
        } SKY_ASSET_BLOCK_END;
    }

    return result;
}

static const char *sky_bytes_rsplit_doc =
"B.rsplit(sep=None, maxsplit=-1) -> list of bytes\n\n"
"Return a list of the sections in B, using sep as the delimiter,\n"
"starting at the end of B and working to the front.\n"
"If sep is not given, B is split on ASCII whitespace characters\n"
"(space, tab, return, newline, formfeed, vertical tab).\n"
"If maxsplit is given, at most maxsplit splits are done.";


sky_bytes_t
sky_bytes_rstrip(sky_bytes_t self, sky_object_t bytes)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bool_t          stripped;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (sky_object_isnull(bytes)) {
        stripped = sky_bytestring_rstrip(self_bytes, NULL, &result_bytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytes_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_rstrip(self_bytes,
                                             &bytes_bytes,
                                             &result_bytes);
        } SKY_ASSET_BLOCK_END;
    }

    if (!stripped) {
        return sky_bytes_copy(self);
    }
    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_rstrip_doc =
"B.rstrip([bytes]) -> bytes\n\n"
"Strip trailing bytes contained in the argument.\n"
"If the argument is omitted, strip trailing ASCII whitespace.";

size_t
sky_bytes_sizeof(sky_bytes_t self)
{
    sky_bytestring_t    *self_bytes;

    if (sky_object_istagged(self)) {
        return 0;
    }

    self_bytes = sky_bytes_bytes(self, NULL);
    if (self_bytes->bytes) {
        if (sky_free == self_bytes->u.free) {
            return sky_memsize(self) + sky_memsize(self_bytes->bytes);
        }
        return sky_memsize(self) + self_bytes->size;
    }
    return sky_memsize(self);
}


sky_bytes_t
sky_bytes_slice(sky_bytes_t self,
                ssize_t     start,
                ssize_t     stop,
                ssize_t     step)
{
    ssize_t             i, j, length;
    sky_bytes_t         result_object;
    sky_bytestring_t    *result_bytes, *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    length = sky_sequence_indices(self_bytes->used, &start, &stop, &step);
    if (!length) {
        return sky_bytes_empty;
    }
    if (1 == length) {
        return SKY_BYTES_SINGLE(self_bytes->bytes[start]);
    }
    if (length == self_bytes->used) {
        return self;
    }

    result_object = sky_object_allocate(sky_bytes_type);
    result_bytes = sky_bytes_bytes(result_object, NULL);
    if (1 == step) {
        sky_bytestring_initfrombytes(result_bytes,
                                     &(self_bytes->bytes[start]),
                                     length,
                                     length);
    }
    else {
        sky_bytestring_initfrombytes(result_bytes, NULL, length, length);
        for (i = 0, j = start; i < length; ++i, j += step) {
            result_bytes->bytes[i] = self_bytes->bytes[j];
        }
    }

    return result_object;
}


sky_list_t
sky_bytes_split(sky_bytes_t self, sky_object_t sep, ssize_t maxsplit)
{
    static sky_bytestring_createfrombytes_t createfrombytes =
            (sky_bytestring_createfrombytes_t)sky_bytes_createfrombytes;

    sky_list_t          result;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (sky_object_isnull(sep)) {
        result = sky_bytestring_split(self_bytes,
                                      NULL,
                                      maxsplit,
                                      createfrombytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t        sep_buffer;
            sky_bytestring_t    sep_bytes;

            sky_bytes_bufferasbytes(&sep_buffer, sep, &sep_bytes);
            result = sky_bytestring_split(self_bytes,
                                          &sep_bytes,
                                          maxsplit,
                                          createfrombytes);
        } SKY_ASSET_BLOCK_END;
    }

    return result;
}

static const char *sky_bytes_split_doc =
"B.split(sep=None, maxsplit=-1) -> list of bytes\n\n"
"Return a list of the sections in B, using sep as the delimiter.\n"
"If sep is not specified or is None, B is split on ASCII whitespace\n"
"characters (space, tab, return, newline, formfeed, vertical tab).\n"
"If maxsplit is given, at most maxsplit splits are done.";


sky_list_t
sky_bytes_splitlines(sky_bytes_t self, sky_bool_t keepends)
{
    sky_list_t          list;
    const uint8_t       *b, *endb, *ending, *startb;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    list = sky_list_create(NULL);
    if (!self_bytes->used) {
        return list;
    }

    startb = self_bytes->bytes;
    endb = self_bytes->bytes + self_bytes->used;
    for (b = startb; b < endb; ++b) {
        while (b < endb && *b != '\n' && *b != '\r') {
            ++b;
        }
        ending = b;
        if (b < endb) {
            if (*b == '\r' && b + 1 < endb && *(b + 1) == '\n') {
                ++b;
            }
        }
        if (startb == self_bytes->bytes && ending == endb) {
            sky_list_append(list, sky_bytes_copy(self));
        }
        else if (keepends) {
            sky_list_append(list,
                            sky_bytes_createfrombytes(startb, b - startb + 1));
        }
        else {
            sky_list_append(list,
                            sky_bytes_createfrombytes(startb, ending - startb));
        }
        startb = b + 1;
    }

    return list;
}

static const char *sky_bytes_splitlines_doc =
"B.splitlines([keepends]) -> list of lines\n\n"
"Return a list of the lines in B, breaking at line boundaries.\n"
"Line breaks are not included in the resulting list unless keepends\n"
"is given and true.";


sky_bool_t
sky_bytes_startswith(sky_bytes_t    self,
                     sky_object_t   prefix,
                     ssize_t        start,
                     ssize_t        end)
{
    sky_bool_t  startswith = SKY_FALSE;

    sky_buffer_t        sub_buffer;
    sky_object_t        object;
    sky_bytestring_t    *self_bytes, self_tagged_bytes, sub_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_object_isa(prefix, sky_tuple_type)) {
        if (!sky_buffer_check(prefix)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "startswith first arg must be bytes or a tuple of bytes, "
                    "not %#@",
                    sky_type_name(sky_object_type(prefix)));
        }
        SKY_ASSET_BLOCK_BEGIN {
            sky_bytes_bufferasbytes(&sub_buffer, prefix, &sub_bytes);
            startswith = sky_bytestring_startswith(self_bytes,
                                                   &sub_bytes,
                                                   start,
                                                   end);
        } SKY_ASSET_BLOCK_END;
    }
    else {
        SKY_SEQUENCE_FOREACH(prefix, object) {
            SKY_ASSET_BLOCK_BEGIN {
                sky_bytes_bufferasbytes(&sub_buffer, object, &sub_bytes);
                startswith = sky_bytestring_startswith(self_bytes,
                                                       &sub_bytes,
                                                       start,
                                                       end);
            } SKY_ASSET_BLOCK_END;
            if (startswith) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }

    return startswith;
}

static const char *sky_bytes_startswith_doc =
"B.startswith(prefix[, start[, end]]) -> bool\n\n"
"Return True if B starts with the specified prefix, False otherwise.\n"
"With optional start, test B beginning at that position.\n"
"With optional end, stop comparing B at that position.\n"
"prefix can also be a tuple of bytes to try.";


sky_string_t
sky_bytes_str(sky_bytes_t self)
{
    if (sky_interpreter_flags() & (SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS |
                                   SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS))
    {
        sky_error_warn_string(sky_BytesWarning, "str() on a bytes instance");
    }
    return sky_bytes_repr(self);
}


sky_bytes_t
sky_bytes_strip(sky_bytes_t self, sky_object_t bytes)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bool_t          stripped;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;
    
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (sky_object_isnull(bytes)) {
        stripped = sky_bytestring_strip(self_bytes, NULL, &result_bytes);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t        bytes_buffer;
            sky_bytestring_t    bytes_bytes;

            sky_bytes_bufferasbytes(&bytes_buffer, bytes, &bytes_bytes);
            stripped = sky_bytestring_strip(self_bytes,
                                            &bytes_bytes,
                                            &result_bytes);
        } SKY_ASSET_BLOCK_END;
    }

    if (!stripped) {
        return sky_bytes_copy(self);
    }
    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_strip_doc =
"B.strip([bytes]) -> bytes\n\n"
"Strip leading and trailing bytes contained in the argument.\n"
"If the argument is omitted, strip leading and trailing ASCII whitespace.";


sky_bytes_t
sky_bytes_swapcase(sky_bytes_t self)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;
    
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_swapcase(self_bytes, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_swapcase_doc =
"B.swapcase() -> copy of B\n\n"
"Return a copy of B with uppercase ASCII characters converted\n"
"to lowercase ASCII and vice versa.";


sky_bytes_t
sky_bytes_title(sky_bytes_t self)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;
    
    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_title(self_bytes, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_title_doc =
"B.title() -> copy of B\n\n"
"Return a titlecased version of B, i.e. ASCII words start with uppercase\n"
"characters, all remaining cased characters have lowercase.";


sky_bytes_t
sky_bytes_translate(sky_bytes_t     self,
                    sky_object_t    table,
                    sky_object_t    deletechars)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bool_t          translated;
    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t        delete_buffer, table_buffer;
        sky_bytestring_t    *delete_bytes, delete_bytestring,
                            *table_bytes, table_bytestring;

        self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
        if (sky_object_isnull(table)) {
            table_bytes = NULL;
        }
        else {
            table_bytes = &table_bytestring;
            sky_bytes_bufferasbytes(&table_buffer, table, table_bytes);
            if (table_bytes->used != 256) {
                sky_error_raise_string(
                        sky_ValueError,
                        "translation table must be 256 characters long");
            }
        }
        if (sky_object_isnull(deletechars)) {
            delete_bytes = NULL;
        }
        else{
            delete_bytes = &delete_bytestring;
            sky_bytes_bufferasbytes(&delete_buffer, deletechars, delete_bytes);
        }

        translated = sky_bytestring_translate(self_bytes,
                                              table_bytes,
                                              delete_bytes,
                                              &result_bytes);
    } SKY_ASSET_BLOCK_END;

    if (!translated) {
        return sky_bytes_copy(self);
    }
    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_translate_doc =
"B.translate(table[, deletechars]) -> bytes\n\n"
"Return a copy of B, where all characters occurring in the\n"
"optional argument deletechars are removed, and the remaining\n"
"characters have been mapped through the given translation\n"
"table, which must be a bytes object of length 256.";


sky_bytes_t
sky_bytes_upper(sky_bytes_t self)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_upper(self_bytes, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_upper_doc =
"B.upper() -> copy of B\n\n"
"Return a copy of B with all ASCII characters converted to uppercase.";


sky_bytes_t
sky_bytes_zfill(sky_bytes_t self, ssize_t width)
{
    sky_bytestring_t    result_bytes = SKY_BYTESTRING_INITIALIZER;

    sky_bytestring_t    *self_bytes, self_tagged_bytes;

    self_bytes = sky_bytes_bytes(self, &self_tagged_bytes);
    if (!sky_bytestring_zfill(self_bytes, width, &result_bytes)) {
        return sky_bytes_copy(self);
    }

    return sky_bytes_createwithbytestring(&result_bytes);
}

static const char *sky_bytes_zfill_doc =
"B.zfill(width) -> copy of B\n\n"
"Pad a numeric string B with zeros on the left, to fill a field\n"
"of the specified width.  B is never truncated.";


void
sky_bytes_iterator_init(sky_bytes_iterator_t self, sky_object_t iterable)
{
    sky_bytes_iterator_data_t   *iterator_data = sky_bytes_iterator_data(self);

    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->bytes)), iterable, self);
}


sky_object_t
sky_bytes_iterator_iter(sky_bytes_iterator_t self)
{
    return self;
}


ssize_t
sky_bytes_iterator_len(sky_bytes_iterator_t self)
{
    sky_bytes_iterator_data_t   *iterator_data = sky_bytes_iterator_data(self);

    ssize_t len;

    if (!iterator_data->bytes) {
        return 0;
    }

    len = sky_bytes_len(iterator_data->bytes) - iterator_data->next_index;
    return (len < 0 ? 0 : len);
}


sky_object_t
sky_bytes_iterator_next(sky_bytes_iterator_t self)
{
    sky_bytes_iterator_data_t   *iterator_data = sky_bytes_iterator_data(self);

    uint8_t             byte;
    sky_bytestring_t    *bytes, tagged_bytes;

    if (!iterator_data->bytes) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    bytes = sky_bytes_bytes(iterator_data->bytes, &tagged_bytes);
    if (iterator_data->next_index >= bytes->used) {
        iterator_data->bytes = NULL;
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    byte = bytes->bytes[iterator_data->next_index++];
    return sky_integer_create(byte);
}


sky_object_t
sky_bytes_iterator_reduce(sky_bytes_iterator_t self)
{
    sky_bytes_iterator_data_t   *iterator_data = sky_bytes_iterator_data(self);

    if (iterator_data->bytes) {
        return sky_object_build("(O(O)iz)",
                                sky_module_getbuiltin("iter"),
                                iterator_data->bytes,
                                iterator_data->next_index);
    }
    return sky_object_build("(O(O))",
                            sky_module_getbuiltin("iter"),
                            sky_bytes_empty);
}


void
sky_bytes_iterator_setstate(sky_bytes_iterator_t self, sky_object_t state)
{
    sky_bytes_iterator_data_t   *self_data = sky_bytes_iterator_data(self);

    if (self_data->bytes) {
        self_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
        if (self_data->next_index < 0) {
            self_data->next_index = 0;
        }
        else if (self_data->next_index > sky_object_len(self_data->bytes)) {
            self_data->next_index = sky_object_len(self_data->bytes);
        }
    }
}


static const char sky_bytes_type_doc[] =
"bytes(iterable_of_ints) -> bytes\n"
"bytes(string, encoding[, errors]) -> bytes\n"
"bytes(bytes_or_buffer) -> immutable copy of bytes_or_buffer\n"
"bytes(int) -> bytes object of size given by the parameter initialized with null bytes\n"
"bytes() -> empty bytes object\n"
"\n"
"Construct an immutable array of bytes from:\n"
"  - an iterable yielding integers in range(256)\n"
"  - a text string encoded using the specified encoding\n"
"  - any object implementing the buffer API.\n"
"  - an integer";


SKY_TYPE_DEFINE(bytes,
                "bytes",
                sizeof(sky_bytes_data_t),
                NULL,
                sky_bytes_instance_finalize,
                NULL,
                NULL,
                NULL,
                sky_bytes_instance_buffer_acquire,
                sky_bytes_instance_buffer_release,
                0,
                sky_bytes_type_doc);


SKY_TYPE_DEFINE_SIMPLE(bytes_iterator,
                       "bytes_iterator",
                       sizeof(sky_bytes_iterator_data_t),
                       NULL,
                       NULL,
                       sky_bytes_iterator_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

sky_bytes_t const sky_bytes_empty = SKY_OBJECT_TAGGED_BYTES_EMPTY;


void
sky_bytes_initialize_library(void)
{
    sky_integer_t   ssize_max;

    ssize_max = sky_integer_create(SSIZE_MAX);

    sky_type_setattr_builtin(
            sky_bytes_type,
            "capitalize",
            sky_function_createbuiltin(
                    "bytes.capitalize",
                    sky_bytes_capitalize_doc,
                    (sky_native_code_function_t)sky_bytes_capitalize,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "center",
            sky_function_createbuiltin(
                    "bytes.center",
                    sky_bytes_center_doc,
                    (sky_native_code_function_t)sky_bytes_center,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "count",
            sky_function_createbuiltin(
                    "bytes.count",
                    sky_bytes_count_doc,
                    (sky_native_code_function_t)sky_bytes_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "decode",
            sky_function_createbuiltin(
                    "bytes.decode",
                    sky_bytes_decode_doc,
                    (sky_native_code_function_t)sky_bytes_decode,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "encoding", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("utf-8"),
                    "errors", SKY_DATA_TYPE_OBJECT_STRING, SKY_STRING_LITERAL("strict"),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "endswith",
            sky_function_createbuiltin(
                    "bytes.endswith",
                    sky_bytes_endswith_doc,
                    (sky_native_code_function_t)sky_bytes_endswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "suffix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "expandtabs",
            sky_function_createbuiltin(
                    "bytes.expandtabs",
                    sky_bytes_expandtabs_doc,
                    (sky_native_code_function_t)sky_bytes_expandtabs,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "tabsize", SKY_DATA_TYPE_INDEX, sky_integer_create(8),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "find",
            sky_function_createbuiltin(
                    "bytes.find",
                    sky_bytes_find_doc,
                    (sky_native_code_function_t)sky_bytes_find,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "fromhex",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "bytes.fromhex",
                            sky_bytes_fromhex_doc,
                            (sky_native_code_function_t)sky_bytes_fromhex,
                            SKY_DATA_TYPE_OBJECT_BYTES,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "index",
            sky_function_createbuiltin(
                    "bytes.index",
                    sky_bytes_index_doc,
                    (sky_native_code_function_t)sky_bytes_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "isalnum",
            sky_function_createbuiltin(
                    "bytes.isalnum",
                    sky_bytes_isalnum_doc,
                    (sky_native_code_function_t)sky_bytes_isalnum,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "isalpha",
            sky_function_createbuiltin(
                    "bytes.isalpha",
                    sky_bytes_isalpha_doc,
                    (sky_native_code_function_t)sky_bytes_isalpha,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "isdigit",
            sky_function_createbuiltin(
                    "bytes.isdigit",
                    sky_bytes_isdigit_doc,
                    (sky_native_code_function_t)sky_bytes_isdigit,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "islower",
            sky_function_createbuiltin(
                    "bytes.islower",
                    sky_bytes_islower_doc,
                    (sky_native_code_function_t)sky_bytes_islower,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "isspace",
            sky_function_createbuiltin(
                    "bytes.isspace",
                    sky_bytes_isspace_doc,
                    (sky_native_code_function_t)sky_bytes_isspace,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "istitle",
            sky_function_createbuiltin(
                    "bytes.istitle",
                    sky_bytes_istitle_doc,
                    (sky_native_code_function_t)sky_bytes_istitle,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "isupper",
            sky_function_createbuiltin(
                    "bytes.isupper",
                    sky_bytes_isupper_doc,
                    (sky_native_code_function_t)sky_bytes_isupper,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "join",
            sky_function_createbuiltin(
                    "bytes.join",
                    sky_bytes_join_doc,
                    (sky_native_code_function_t)sky_bytes_join,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "ljust",
            sky_function_createbuiltin(
                    "bytes.ljust",
                    sky_bytes_ljust_doc,
                    (sky_native_code_function_t)sky_bytes_ljust,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "lower",
            sky_function_createbuiltin(
                    "bytes.lower",
                    sky_bytes_lower_doc,
                    (sky_native_code_function_t)sky_bytes_lower,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "lstrip",
            sky_function_createbuiltin(
                    "bytes.lstrip",
                    sky_bytes_lstrip_doc,
                    (sky_native_code_function_t)sky_bytes_lstrip,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "maketrans",
            sky_staticmethod_create(
                    sky_function_createbuiltin(
                            "bytes.maketrans",
                            sky_bytes_maketrans_doc,
                            (sky_native_code_function_t)sky_bytes_maketrans,
                            SKY_DATA_TYPE_OBJECT,
                            "from", SKY_DATA_TYPE_OBJECT, NULL,
                            "to", SKY_DATA_TYPE_OBJECT, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "partition",
            sky_function_createbuiltin(
                    "bytes.partition",
                    sky_bytes_partition_doc,
                    (sky_native_code_function_t)sky_bytes_partition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "replace",
            sky_function_createbuiltin(
                    "bytes.replace",
                    sky_bytes_replace_doc,
                    (sky_native_code_function_t)sky_bytes_replace,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "old", SKY_DATA_TYPE_OBJECT, NULL,
                    "new", SKY_DATA_TYPE_OBJECT, NULL,
                    "count", SKY_DATA_TYPE_INDEX, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rfind",
            sky_function_createbuiltin(
                    "bytes.rfind",
                    sky_bytes_rfind_doc,
                    (sky_native_code_function_t)sky_bytes_rfind,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rindex",
            sky_function_createbuiltin(
                    "bytes.rindex",
                    sky_bytes_rindex_doc,
                    (sky_native_code_function_t)sky_bytes_rindex,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sub", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rjust",
            sky_function_createbuiltin(
                    "bytes.rjust",
                    sky_bytes_rjust_doc,
                    (sky_native_code_function_t)sky_bytes_rjust,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    "fillchar", SKY_DATA_TYPE_OBJECT, sky_bytes_createfrombytes(" ", 1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rpartition",
            sky_function_createbuiltin(
                    "bytes.rpartition",
                    sky_bytes_rpartition_doc,
                    (sky_native_code_function_t)sky_bytes_rpartition,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rsplit",
            sky_function_createbuiltin(
                    "bytes.rsplit",
                    sky_bytes_rsplit_doc,
                    (sky_native_code_function_t)sky_bytes_rsplit,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, sky_None,
                    "maxsplit", SKY_DATA_TYPE_INDEX, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "rstrip",
            sky_function_createbuiltin(
                    "bytes.rstrip",
                    sky_bytes_rstrip_doc,
                    (sky_native_code_function_t)sky_bytes_rstrip,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "split",
            sky_function_createbuiltin(
                    "bytes.split",
                    sky_bytes_split_doc,
                    (sky_native_code_function_t)sky_bytes_split,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "sep", SKY_DATA_TYPE_OBJECT, sky_None,
                    "maxsplit", SKY_DATA_TYPE_INDEX, sky_integer_create(-1),
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "splitlines",
            sky_function_createbuiltin(
                    "bytes.splitlines",
                    sky_bytes_splitlines_doc,
                    (sky_native_code_function_t)sky_bytes_splitlines,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "keepends", SKY_DATA_TYPE_BOOL, sky_False,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "startswith",
            sky_function_createbuiltin(
                    "bytes.startswith",
                    sky_bytes_startswith_doc,
                    (sky_native_code_function_t)sky_bytes_startswith,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "prefix", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero, sky_integer_zero,
                    "end", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_NONE_IS_EXTRA | SKY_DATA_TYPE_INTEGER_CLAMP, ssize_max, ssize_max,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "strip",
            sky_function_createbuiltin(
                    "bytes.strip",
                    sky_bytes_strip_doc,
                    (sky_native_code_function_t)sky_bytes_strip,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "bytes", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "swapcase",
            sky_function_createbuiltin(
                    "bytes.swapcase",
                    sky_bytes_swapcase_doc,
                    (sky_native_code_function_t)sky_bytes_swapcase,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "title",
            sky_function_createbuiltin(
                    "bytes.title",
                    sky_bytes_title_doc,
                    (sky_native_code_function_t)sky_bytes_title,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "translate",
            sky_function_createbuiltin(
                    "bytes.translate",
                    sky_bytes_translate_doc,
                    (sky_native_code_function_t)sky_bytes_translate,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "table", SKY_DATA_TYPE_OBJECT, NULL,
                    "deletechars", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "upper",
            sky_function_createbuiltin(
                    "bytes.upper",
                    sky_bytes_upper_doc,
                    (sky_native_code_function_t)sky_bytes_upper,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_bytes_type,
            "zfill",
            sky_function_createbuiltin(
                    "bytes.zfill",
                    sky_bytes_zfill_doc,
                    (sky_native_code_function_t)sky_bytes_zfill,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_BYTES, NULL,
                    "width", SKY_DATA_TYPE_INDEX, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_bytes_type,
            "__new__",
            (sky_native_code_function_t)sky_bytes_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "source", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "encoding", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            "errors", SKY_DATA_TYPE_OBJECT_STRING, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_bytes_type,
            "__repr__", sky_bytes_repr,
            "__str__", sky_bytes_str,
            "__lt__", sky_bytes_lt,
            "__le__", sky_bytes_le,
            "__eq__", sky_bytes_eq,
            "__ne__", sky_bytes_ne,
            "__gt__", sky_bytes_gt,
            "__ge__", sky_bytes_ge,
            "__hash__", sky_bytes_hash,
            "__sizeof__", sky_bytes_sizeof,
            "__len__", sky_bytes_len,
            "__getitem__", sky_bytes_getitem,
            "__iter__", sky_bytes_iter,
            "__contains__", sky_bytes_contains,
            "__add__", sky_bytes_add,
            "__mul__", sky_bytes_mul,
            "__radd__", sky_bytes_radd,
            "__rmul__", sky_bytes_mul,
            "__getnewargs__", sky_bytes_getnewargs,
            NULL);


    sky_type_initialize_builtin(sky_bytes_iterator_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_bytes_iterator_type,
            "__init__",
            (sky_native_code_function_t)sky_bytes_iterator_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_bytes_iterator_type,
            "__len__", sky_bytes_iterator_len,
            "__iter__", sky_bytes_iterator_iter,
            "__next__", sky_bytes_iterator_next,
            "__reduce__", sky_bytes_iterator_reduce,
            "__setstate__", sky_bytes_iterator_setstate,
            NULL);
}
