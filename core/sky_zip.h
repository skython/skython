/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_zip Zip Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ZIP_H__
#define __SKYTHON_CORE_SKY_ZIP_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new zip object instance.
  *
  * @param[in]  iterable    an iterable object.
  * @param[in]  ...         a variable length list of iterable objects, the
  *                         last of which must be @c NULL.
  * @return     a new zip object instance.
  */
SKY_EXTERN sky_zip_t
sky_zip_create(sky_object_t iterable, ...);


/** Create a new zip object instance.
  *
  * @param[in]  iterable    an iterable object.
  * @param[in]  ap          a variable length list of iterable objects, the
  *                         last of which must be @c NULL.
  * @return     a new zip object instance.
  */
SKY_EXTERN sky_zip_t
sky_zip_createv(sky_object_t iterable, va_list ap);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ZIP_H__ */

/** @} **/
/** @} **/
