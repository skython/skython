/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_boolean Boolean Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BOOLEAN_H__
#define __SKYTHON_CORE_SKY_BOOLEAN_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An object representing the Boolean truth value that is false. **/
SKY_EXTERN sky_boolean_t const sky_False;

/** An object representing the Boolean truth value that is true. **/
SKY_EXTERN sky_boolean_t const sky_True;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BOOLEAN_H__ */

/** @} **/
/** @} **/
