#include "sky_private.h"


typedef struct sky_signal_descriptor_s {
    volatile sky_bool_t                 pending;
    sky_signal_handler_t                handler;
    sky_object_t                        object;

    struct sigaction                    oact;
} sky_signal_descriptor_t;

/* Use NSIG + 1, because signals are 1-based. Descriptor 0 will be wasted. */
static sky_signal_descriptor_t  sky_signal_signals[NSIG + 1];

static int          sky_signal_wakeup_fd;
static sky_bool_t   sky_signal_pending;

#if defined(SIGPIPE)
static struct sigaction sky_signal_action_SIGPIPE;
#endif
#if defined(SIGXFZ)
static struct sigaction sky_signal_action_SIGXFZ;
#endif
#if defined(SIGXFSZ)
static struct sigaction sky_signal_action_SIGXFSZ;
#endif


static inline void
sky_signal__checkMainThread(const char *function)
{
    if (!sky_thread_ismain()) {
        sky_error_raise_format(sky_ValueError,
                               "%s only works in main thread",
                               function);
    }
}

static void
sky_signal__checkValidSignal(int signal)
{
    if (signal < 1 || signal >= NSIG) {
        sky_error_raise_string(sky_ValueError, "signal number out of range");
    }
}

static void
sky_signal__handleSignal(int signal)
{
    int saved_errno;

    saved_errno = errno;

    if (sky_thread_ismain()) {
        sky_signal_trigger(signal);
    }

    errno = saved_errno;
}


static inline sky_bool_t
sky_signal_isunmaskable(int signal)
{
    return (SIGKILL == signal || SIGSTOP == signal);
}


sky_signal_handler_t
sky_signal_handler(int signal, sky_object_t *object)
{
    sky_signal__checkValidSignal(signal);
    if (object) {
        *object = sky_signal_signals[signal].object;
    }
    return sky_signal_signals[signal].handler;
}


void
sky_signal_check_pending(void)
{
    int i;

    if (!sky_signal_pending || !sky_thread_ismain()) {
        return;
    }

    sky_signal_pending = SKY_FALSE;
    for (i = 1; i < NSIG; ++i) {
        if (sky_signal_signals[i].pending) {
            sky_signal_signals[i].pending = SKY_FALSE;
            sky_signal_signals[i].handler(i, sky_signal_signals[i].object);
        }
    }
}


void
sky_signal_sethandler(int                   signal,
                      sky_signal_handler_t  handler,
                      sky_object_t          object)
{
    struct sigaction    act;

    sky_signal__checkMainThread("signal");
    sky_signal__checkValidSignal(signal);

    sigemptyset(&(act.sa_mask));
    if (SIG_IGN == (void *)handler || SIG_DFL == (void *)handler) {
        act.sa_handler = (void *)handler;
    }
    else {
        act.sa_handler = sky_signal__handleSignal;
    }
    act.sa_flags = 0;
    if (sigaction(signal, &act, NULL) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    sky_signal_signals[signal].pending = SKY_FALSE;
    sky_signal_signals[signal].handler = handler;
    sky_object_gc_setlibraryroot(&(sky_signal_signals[signal].object),
                                 object,
                                 SKY_TRUE);

}


void
sky_signal_setwakeupfd(int fd)
{
    sky_signal__checkMainThread("set_wakeup_fd");
    sky_signal_wakeup_fd = fd;
}


int
sky_signal_wakeupfd(void)
{
    return sky_signal_wakeup_fd;
}


void
sky_signal_atfork_child(void)
{
    int i;

    if (!sky_signal_pending) {
        return;
    }
    sky_signal_pending = SKY_FALSE;
    for (i = 1; i < NSIG; ++i) {
        sky_signal_signals[i].pending = SKY_FALSE;
    }
}

void
sky_signal_restore_defaults(void)
{
    int i;

    for (i = 1; i < NSIG; ++i) {
        if (sky_signal_isunmaskable(i)) {
            continue;
        }
        sigaction(i, &(sky_signal_signals[i].oact), NULL);
    }

#if defined(SIGPIPE)
    sigaction(SIGPIPE, &sky_signal_action_SIGPIPE, NULL);
#endif
#if defined(SIGXFZ)
    sigaction(SIGXFZ, &sky_signal_action_SIGXFZ, NULL);
#endif
#if defined(SIGXFSZ)
    sigaction(SIGXFSZ, &sky_signal_action_SIGXFSZ, NULL);
#endif
}

void
sky_signal_trigger(int signal)
{
    uint8_t signal_asbyte;

    if (sky_signal_wakeup_fd >= 0) {
        signal_asbyte = signal;
        write(sky_signal_wakeup_fd, &signal_asbyte, sizeof(signal_asbyte));
    }
    sky_signal_signals[signal].pending = SKY_TRUE;
    if (!sky_signal_pending) {
        sky_signal_pending = SKY_TRUE;
    }
}

void
sky_signal_initialize_library(SKY_UNUSED unsigned int flags)
{
    int                 i;
    struct sigaction    action;

    sigemptyset(&(action.sa_mask));
    action.sa_handler = SIG_IGN;
    action.sa_flags = 0;

#if defined(SIGPIPE)
    sigaction(SIGPIPE, &action, &sky_signal_action_SIGPIPE);
#endif
#if defined(SIGXFZ)
    sigaction(SIGXFZ, &action, &sky_signal_action_SIGXFZ);
#endif
#if defined(SIGXFSZ)
    sigaction(SIGXFSZ, &action, &sky_signal_action_SIGXFSZ);
#endif


    sky_signal_wakeup_fd = -1;
    sky_signal_pending = SKY_FALSE;

    for (i = 1; i < NSIG; ++i) {
        sky_signal_signals[i].pending = SKY_FALSE;
        sky_signal_signals[i].handler = NULL;
        sky_signal_signals[i].object = NULL;
        if (sky_signal_isunmaskable(i)) {
            continue;
        }

        if (sigaction(i, NULL, &(sky_signal_signals[i].oact)) == -1) {
            sigemptyset(&(sky_signal_signals[i].oact.sa_mask));
            sky_signal_signals[i].oact.sa_handler = SIG_DFL;
            sky_signal_signals[i].oact.sa_flags = 0;
        }

        if (SIG_DFL == sky_signal_signals[i].oact.sa_handler) {
            sky_signal_signals[i].handler = (sky_signal_handler_t)SIG_DFL;
        }
        else if (SIG_IGN == sky_signal_signals[i].oact.sa_handler) {
            sky_signal_signals[i].handler = (sky_signal_handler_t)SIG_IGN;
        }
    }

    sky_library_atfinalize(sky_signal_restore_defaults);
}
