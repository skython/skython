#include "sky_private.h"
#include "sky_codec_private.h"
#include "sky_string_private.h"


sky_type_t sky_codec_charmap_type = NULL;


static void
sky_codec_charmap_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_codec_charmap_data_t    *self_data = data;

    sky_free(self_data->level23);
}


static size_t
sky_codec_charmap_size(sky_object_t self)
{
    sky_codec_charmap_data_t    *self_data = sky_codec_charmap_data(self);

    return sky_type_data(sky_object_type(self))->instance_size +
           (16 * self_data->count2) +
           (128 * self_data->count3);
}


static size_t
sky_codec_charmap_sizeof(sky_object_t self)
{
    sky_codec_charmap_data_t    *self_data = sky_codec_charmap_data(self);

    return sky_memsize(self) + sky_memsize(self_data->level23);
}


static sky_object_t
sky_codec_charmap_build_dict(sky_string_data_t *string_data)
{
    ssize_t     i, length;
    sky_dict_t  dict;

    dict = sky_dict_create();
    length = SKY_MIN(string_data->len, 256);
    if (string_data->highest_codepoint < 0x100) {
        for (i = 0; i < length; ++i) {
            sky_dict_setitem(dict,
                             sky_integer_create(string_data->data.latin1[i]),
                             sky_integer_create(i));
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        for (i = 0; i < length; ++i) {
            sky_dict_setitem(dict,
                             sky_integer_create(string_data->data.ucs2[i]),
                             sky_integer_create(i));
        }
    }
    else {
        for (i = 0; i < length; ++i) {
            sky_dict_setitem(dict,
                             sky_integer_create(string_data->data.ucs4[i]),
                             sky_integer_create(i));
        }
    }

    return dict;
}


sky_object_t
sky_codec_charmap_build(sky_string_t string)
{
    int                         count2, count3;
    ssize_t                     i, length;
    uint8_t                     level1[32], level2[512],
                                *mlevel1, *mlevel2, *mlevel3;
    sky_string_data_t           *string_data, tagged_data;
    sky_unicode_char_t          cp;
    sky_codec_charmap_t         charmap;
    sky_codec_charmap_data_t    *charmap_data;

    string_data = sky_string_data(string, &tagged_data);
    if (!string_data->len ||
        string_data->highest_codepoint > 0xFFFF ||
        (string_data->highest_codepoint < 0x100 && !string_data->data.latin1[0]) ||
        (string_data->highest_codepoint < 0x10000 && !string_data->data.ucs2[0]))
    {
        return sky_codec_charmap_build_dict(string_data);
    }

    length = SKY_MIN(string_data->len, 256);
    memset(level1, 0xFF, sizeof(level1));
    memset(level2, 0xFF, sizeof(level2));
    count2 = count3 = 0;

    for (i = 1; i < length; ++i) {
        int l1, l2;

        if (string_data->highest_codepoint < 0x100) {
            cp = (sky_unicode_char_t)string_data->data.latin1[i];
        }
        else {
            cp = (sky_unicode_char_t)string_data->data.ucs2[i];
        }
        if (!cp) {
            return sky_codec_charmap_build_dict(string_data);
        }
        if (0xFFFE == cp) { /* unmapped character */
            continue;
        }
        l1 = cp >> 11;
        l2 = cp >> 7;
        if (0xFF == level1[l1]) {
            level1[l1] = count2++;
        }
        if (0xFF == level2[l2]) {
            level2[l2] = count3++;
        }
    }
    if (count2 >= 0xFF || count3 >= 0xFF) {
        return sky_codec_charmap_build_dict(string_data);
    }

    charmap = sky_object_allocate(sky_codec_charmap_type);
    charmap_data = sky_codec_charmap_data(charmap);
    charmap_data->count2 = count2;
    charmap_data->count3 = count3;
    charmap_data->level23 = sky_calloc(1, (16 * count2) + (128 * count3));

    mlevel1 = charmap_data->level1;
    mlevel2 = charmap_data->level23;
    mlevel3 = charmap_data->level23 + (16 * count2);

    sky_error_validate_debug(sizeof(charmap_data->level1) == sizeof(level1));
    memcpy(mlevel1, level1, sizeof(level1));
    memset(mlevel2, 0xFF, 16 * count2);

    count3 = 0;
    for (i = 1; i < length; ++i) {
        int i2, i3, o1, o2, o3;

        if (string_data->highest_codepoint < 0x100) {
            cp = (sky_unicode_char_t)string_data->data.latin1[i];
        }
        else {
            cp = (sky_unicode_char_t)string_data->data.ucs2[i];
        }
        if (0xFFFE == cp) {
            continue;
        }

        o1 = cp >> 1;
        o2 = (cp >> 7) & 0xF;
        i2 = 16 * mlevel1[o1] + o2;
        if (0xFF == mlevel2[i2]) {
            mlevel2[i2] = count3++;
        }
        o3 = cp & 0x7F;
        i3 = 128 * mlevel2[i2] + o3;
        mlevel3[i3] = i;
    }

    return charmap;
}


void
sky_codec_charmap_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.finalize = sky_codec_charmap_instance_finalize;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("EncodingMap"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_codec_charmap_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type,
            "size",
            sky_function_createbuiltin(
                    "EncodingMap.size",
                    NULL,
                    (sky_native_code_function_t)sky_codec_charmap_size,
                    SKY_DATA_TYPE_SIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                    NULL));

    sky_type_setmethodslots(type,
            "__sizeof__", sky_codec_charmap_sizeof,
            NULL);

    sky_error_validate(NULL == sky_codec_charmap_type);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky_codec_charmap_type),
                                 type,
                                 SKY_TRUE);
}
