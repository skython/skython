#ifndef __SKYTHON_CORE_SKY_THREAD_PRIVATE_H__
#define __SKYTHON_CORE_SKY_THREAD_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_time.h"
#include "sky_thread.h"

#include <pthread.h>
#include <sys/select.h>


SKY_CDECLS_BEGIN


typedef enum sky_thread_wait_state_e {
    SKY_THREAD_WAIT_STATE_RUNNING       =   0,
    SKY_THREAD_WAIT_STATE_COND          =   1,
    SKY_THREAD_WAIT_STATE_SELECT        =   2,
} sky_thread_wait_state_t;

struct sky_thread_wait_s {
    pthread_mutex_t                     mutex;
    pthread_cond_t                      cond;
    int                                 fds[2];
    sky_thread_wait_state_t             state;
    sky_bool_t                          atfork_check;
    sky_bool_t                          gc_needed;
};

extern void
sky_thread_wait_init(sky_thread_wait_t *wait);

extern void
sky_thread_wait_cleanup(sky_thread_wait_t *wait);


typedef struct sky_thread_waitlist_s {
    pthread_mutex_t                     mutex;
    ssize_t                             capacity;
    ssize_t                             len;
    sky_thread_wait_t **                waiters;
} sky_thread_waitlist_t;

#define SKY_THREAD_WAITLIST_INITIALIZER \
        { PTHREAD_MUTEX_INITIALIZER, 0, 0, NULL }

extern void
sky_thread_waitlist_init(sky_thread_waitlist_t *list);

extern void
sky_thread_waitlist_cleanup(sky_thread_waitlist_t *list);

extern void
sky_thread_waitlist_dequeue(sky_thread_waitlist_t * list,
                            sky_thread_wait_t *     wait);

extern void
sky_thread_waitlist_enqueue(sky_thread_waitlist_t * list,
                            sky_thread_wait_t *     wait);

extern void
sky_thread_waitlist_reset(sky_thread_waitlist_t *   list,
                          sky_thread_wait_t *       wait);

extern void
sky_thread_waitlist_signal(sky_thread_waitlist_t *list);

extern void
sky_thread_waitlist_broadcast(sky_thread_waitlist_t *list);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_THREAD_PRIVATE_H__ */
