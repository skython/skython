/** @file
  * @brief Memory allocation using libc malloc.
  * @defgroup memory_management Memory Management
  * @{
  * @defgroup sky_malloc Memory Allocation
  * @{
  * @defgroup sky_system_malloc Libc Malloc-Based Memory Allocation
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SYSTEM_MALLOC_H__
#define __SKYTHON_CORE_SKY_SYSTEM_MALLOC_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** Initialize the system/libc malloc implementation for use.
  * The initialization process finishes with a call to sky_malloc_sethooks() to
  * set the sky_malloc hooks to use the system/libc malloc implementation. The
  * implementation is a fairly light wrapper around malloc()/free() on most
  * platforms. On Windows, it is a wrapper around HeapAlloc()/HeapFree().
  *
  * @note   Since sky_system_malloc_initialize() calls sky_malloc_sethooks(),
  *         it should be called before sky_library_initialize().
  *
  * @return     @c SKY_TRUE if initialize succeeded, or
  *             @c SKY_FALSE if it failed.
  */
SKY_EXTERN sky_bool_t
sky_system_malloc_initialize(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SYSTEM_MALLOC_H__ */

/** @} **/
/** @} **/
/** @} **/
