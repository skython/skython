/** @file
  * @brief
  * @defgroup containers Containers
  * @{
  * @defgroup sky_set Key Sets
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SET_H__
#define __SKYTHON_CORE_SKY_SET_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new set object instance.
  * If @a object is @c NULL, a new empty set will be created; otherwise,
  * @a object must be iterable, and a new set will be created by iterating it.
  * Even if @a object is already a set, a new set will be created. Any object
  * that implements __iter__() can be converted into a set.
  *
  * @param[in]  object  the object to be converted into a set.
  * @return     the new set object instance.
  */
SKY_EXTERN sky_set_t
sky_set_create(sky_object_t object);


/** Create a new set object instance from an array of objects.
  *
  * @param[in]  count   the number of objects present in the @a objects list.
  * @param[in]  objects the objects to be used to populate the set. Any
  *                     @c NULL pointers will be replaced with @c sky_None
  *                     objects.
  * @return     the new set object instance.
  */
SKY_EXTERN sky_set_t
sky_set_createfromarray(ssize_t count, sky_object_t *objects);


/** Create a new empty set object instance.
  *
  * @param[in]  capacity    the number of objects that the set will initially
  *                         be able to store without the need to grow. If
  *                         specified as 0, a default initial capacity will be
  *                         used.
  * @return     the new set object instance.
  */
SKY_EXTERN sky_set_t
sky_set_createwithcapacity(size_t capacity);


/** Add a key to a set if it does not already exist.
  *
  * @param[in]  self    the set into which the key is to be stored.
  * @param[in]  key     the key to store.
  * @return     @c SKY_TRUE if the key was added, or @c SKY_FALSE if the key
  *             already existed in the set.
  */
SKY_EXTERN sky_bool_t
sky_set_add(sky_set_t self, sky_object_t key);


/** Remove all keys from a set.
  * Removes all keys from a set, leaving it empty.
  *
  * @param[in]  self    the set to be cleared.
  */
SKY_EXTERN void
sky_set_clear(sky_set_t self);


/** Determine whether a key already exists in a set.
  * 
  * @param[in]  self    the set to query.
  * @param[in]  key     the key to be queried.
  * @return     @c SKY_TRUE if the key exists in the set, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_set_contains(sky_set_t self, sky_object_t key);


/** Create a shallow copy of a set.
  *
  * @param[in]  self    the set to copy.
  * @return     a new set that is a shallow copy of @a set.
  */
SKY_EXTERN sky_set_t
sky_set_copy(sky_set_t self);


/** Delete a key from a set.
  *
  * @param[in]  self    the set from which the key is to be deleted.
  * @param[in]  key     the key to be deleted.
  * @return     @c SKY_TRUE if the key was removed, or @c SKY_FALSE if it did
  *             not exist in the set.
  */
SKY_EXTERN sky_bool_t
sky_set_delete(sky_set_t self, sky_object_t key);


/** Return the difference of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a tuple of sets to subtract from @a self.
  * @return     a new set that is @a self with all elements from the sets in
  *             @a args removed.
  */
SKY_EXTERN sky_set_t
sky_set_difference(sky_set_t self, sky_tuple_t args);


/** Compute the difference of two or more sets in place.
  * This is functionally equivalent to sky_set_difference(), except that the
  * set @a self is modified in place rather than creating a new set.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a tuple of sets to subtract from @a self.
  */
SKY_EXTERN void
sky_set_difference_update(sky_set_t self, sky_tuple_t args);


/** Return the intersection of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a tuple of sets to intersect with @a self.
  * @return     a new set that contains only the items from @a self and the
  *             sets in @a args that are in all of the sets.
  */
SKY_EXTERN sky_set_t
sky_set_intersection(sky_set_t self, sky_tuple_t args);


/** Compute the intersection of two or more sets in place.
  * This is functionally equivalent to sky_set_intersection(), except that the
  * set @a self is modified in place rather than creating a new set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   a sequence to intersect with @a self.
  */
SKY_EXTERN void
sky_set_intersection_update(sky_set_t self, sky_object_t other);


/** Determine if a two sets are disjoint.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a self and @a other do not intersect.
  */
SKY_EXTERN sky_bool_t
sky_set_isdisjoint(sky_set_t self, sky_object_t other);


/** Determine if a set is a subset of another set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a other is a subset of @a self.
  */
SKY_EXTERN sky_bool_t
sky_set_issubset(sky_set_t self, sky_object_t other);


/** Determine if a set is a superset of another set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a other is a superset of @a self.
  */
SKY_EXTERN sky_bool_t
sky_set_issuperset(sky_set_t self, sky_object_t other);


/** Return the number of keys in a set.
  *
  * @param[in]  self    the set for which the number of keys it contains is to
  *                     be returned.
  * @return     the number of keys contained in @a set.
  */
SKY_EXTERN ssize_t
sky_set_len(sky_set_t self);


/** Remove a key from a set and return it.
  *
  * @param[in]  self    the set from which the key is to be removed.
  * @return     the key that was removed, or @c NULL if the set is empty.
  */
SKY_EXTERN sky_object_t
sky_set_pop(sky_set_t self);


/** Return the symmetric difference of two sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     a new set that is the symmetric difference of @a set and
  *             @a other.
  */
SKY_EXTERN sky_set_t
sky_set_symmetric_difference(sky_set_t self, sky_object_t other);


/** Compute the symmetric difference of two sets in place.
  * This is functionally equivalent to sky_set_symmetric_difference(), except
  * that the set @a self is modified in place rather than creating a new set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  */
SKY_EXTERN void
sky_set_symmetric_difference_update(sky_set_t self, sky_object_t other);


/** Return the union of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a tuple of sets to combine with @a self.
  * @return     a new set that contains all of the elements from @a self and
  *             the sets in the list of sets, @a args.
  */
SKY_EXTERN sky_set_t
sky_set_union(sky_set_t self, sky_tuple_t args);


/** Update a set with all elements from another sequence.
  *
  * @param[in]  self    the set to update.
  * @param[in]  args    a tuple of sequences to update @a set with.
  */
SKY_EXTERN void
sky_set_update(sky_set_t self, sky_tuple_t args);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SET_H__ */

/** @} **/
/** @} **/
