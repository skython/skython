/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_getset Get/Set Descriptor Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_GETSET_H__
#define __SKYTHON_CORE_SKY_GETSET_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The signature of a function to call when the descriptor's __get__() method
  * is called.
  *
  * @param[in]  instance    the instance for which the get action is taking
  *                         place.
  * @param[in]  type        the type to which the descriptor is bound.
  * @return     the object instance to be returned as the value for the
  *             instance invoking the descriptor.
  */
typedef sky_object_t
        (*sky_getset_get_t)(sky_object_t instance, sky_type_t type);


/** The signature of a function to call when the descriptor's __set__() or
  * __delete__() method is called.
  *
  * @param[in]  instance    the instance for which the set or delete action
  *                         is taking place.
  * @param[in]  value       the value being set, or @c NULL if the action is
  *                         a delete rather than a set.
  * @param[in]  type        the type to which the descriptor is bound.
  */
typedef void
        (*sky_getset_set_t)(sky_object_t    instance,
                            sky_object_t    value,
                            sky_type_t      type);


/** Create a new getset descriptor object instance.
  *
  * @param[in]  type    the type for which the descriptor will apply.
  *                     Must not be @c NULL.
  * @param[in]  name    the name of the attribute for which the descriptor
  *                     will apply. Must not be @c NULL.
  * @param[in]  doc     the doc string for the descriptor. May be @c NULL.
  * @param[in]  get     the function to call when the __get__() method is
  *                     called. May be @c NULL.
  * @param[in]  set     the function to call when the __set__() or
  *                     __delete__() method is called. May be @c NULL.
  * @return     the new getset descriptor object instance.
  */
SKY_EXTERN sky_getset_t
sky_getset_create(sky_type_t        type,
                  const char *      name,
                  const char *      doc,
                  sky_getset_get_t  get,
                  sky_getset_set_t  set);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_GETSET_H__ */

/** @} **/
/** @} **/
