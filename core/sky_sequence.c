#include "sky_private.h"


sky_bool_t
sky_sequence_all(sky_object_t iterable)
{
    sky_bool_t      result;
    sky_object_t    element, iter;

    result = SKY_TRUE;
    if (sky_sequence_isiterable(iterable)) {
        SKY_SEQUENCE_FOREACH(iterable, element) {
            if (!sky_object_bool(element)) {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(iterable);
        while ((element = sky_object_next(iter, NULL)) != NULL) {
            if (!sky_object_bool(element)) {
                result = SKY_FALSE;
                break;
            }
        }
    }

    return result;
}


sky_bool_t
sky_sequence_any(sky_object_t iterable)
{
    sky_bool_t      result;
    sky_object_t    element, iter;

    result = SKY_FALSE;
    if (sky_sequence_isiterable(iterable)) {
        SKY_SEQUENCE_FOREACH(iterable, element) {
            if (sky_object_bool(element)) {
                result = SKY_TRUE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(iterable);
        while ((element = sky_object_next(iter, NULL)) != NULL) {
            if (sky_object_bool(element)) {
                result = SKY_TRUE;
                break;
            }
        }
    }

    return result;
}


sky_bool_t
sky_sequence_check(sky_object_t object)
{
    if (SKY_OBJECT_METHOD_SLOT(object, GETITEM) &&
        !sky_object_isa(object, sky_dict_type))
    {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


sky_object_t
sky_sequence_concatenate(sky_object_t sequence, sky_object_t other)
{
    sky_object_t    result;

    if (sky_sequence_check(sequence) && sky_sequence_check(other)) {
        result = sky_number_add(sequence, other);
        if (sky_NotImplemented != result) {
            return result;
        }
    }
    sky_error_raise_format(sky_TypeError,
                           "%#@ object can't be concatenated",
                           sky_type_name(sky_object_type(sequence)));
}


ssize_t
sky_sequence_count(sky_object_t sequence, sky_object_t object)
{
    ssize_t         count;
    sky_object_t    item, iter;

    count = 0;
    iter = sky_object_iter(sequence);
    while ((item = sky_object_next(iter, NULL)) != NULL) {
        if (sky_object_compare(item, object, SKY_COMPARE_OP_EQUAL)) {
            if (count++ == SSIZE_MAX) {
                sky_error_raise_string(sky_OverflowError,
                                       "count exceeds C integer size");
            }
        }
    }

    return count;
}


sky_object_t
sky_sequence_get(sky_object_t sequence, ssize_t index)
{
    if (sky_object_type(sequence) == sky_list_type) {
        return sky_list_get(sequence, index);
    }
    if (sky_object_type(sequence) == sky_tuple_type) {
        return sky_tuple_get(sequence, index);
    }
    return sky_object_getitem(sequence, sky_integer_create(index));
}


ssize_t
sky_sequence_index(sky_object_t sequence, sky_object_t object)
{
    ssize_t         index;
    sky_object_t    item, iter;

    index = 0;
    iter = sky_object_iter(sequence);
    while ((item = sky_object_next(iter, NULL)) != NULL) {
        if (sky_object_compare(item, object, SKY_COMPARE_OP_EQUAL)) {
            return index;
        }
        if (index++ == SSIZE_MAX) {
            sky_error_raise_string(sky_OverflowError,
                                   "index exceeds C integer size");
        }
    }

    /* XXX  Shouldn't this be an IndexError? CPython raises a ValueError */
    sky_error_raise_string(sky_ValueError,
                           "sequence.index(x): x not in sequence");
}


ssize_t
sky_sequence_indices(ssize_t    length,
                     ssize_t *  start,
                     ssize_t *  stop,
                     ssize_t *  step)
{
    if (!*step) {
        sky_error_raise_string(sky_ValueError,
                               "slice step cannot be zero");
    }
    if (*step < -SSIZE_MAX) {
        *step = -SSIZE_MAX;
    }

    if (*start < 0) {
        *start += length;
        if (*start < 0) {
            *start = (*step < 0 ? -1 : 0);
        }
    }
    if (*start >= length) {
        *start = (*step < 0 ? length - 1 : length);
    }

    if (*stop < 0) {
        *stop += length;
        if (*stop < 0) {
            *stop = (*step < 0 ? -1 : 0);
        }
    }
    if (*stop >= length) {
        *stop = (*step < 0 ? length - 1 : length);
    }

    if ((*step < 0 && *stop >= *start) || (*step > 0 && *start >= *stop)) {
        return 0;
    }
    if (*step < 0) {
        return ((*stop - *start + 1) / *step) + 1;
    }
    return ((*stop - *start - 1) / *step) + 1;
}


sky_object_t
sky_sequence_inplace_concatenate(sky_object_t sequence, sky_object_t other)
{
    sky_object_t    result;

    if (sky_sequence_check(sequence) && sky_sequence_check(other)) {
        result = sky_number_inplace_add(sequence, other);
        if (sky_NotImplemented != result) {
            return result;
        }
    }
    sky_error_raise_format(sky_TypeError,
                           "%#@ object can't be concatenated",
                           sky_type_name(sky_object_type(sequence)));
}


sky_bool_t
sky_sequence_isiterable(sky_object_t sequence)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(sequence));

    return (type_data->iterate ? SKY_TRUE : SKY_FALSE);
}


void
sky_sequence_iterate(sky_object_t                   sequence,
                     sky_sequence_iterate_state_t * state,
                     sky_object_t *                 objects,
                     ssize_t                        nobjects)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(sequence));

    void            *data;
    sky_type_list_t *offsets;

    if (!type_data->iterate) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ is not iterable (internal fast iteration)",
                               sky_type_name(sky_object_type(sequence)));
    }

    if (sky_object_istagged(sequence)) {
        data = NULL;
    }
    else {
        offsets = type_data->data_offsets;
        data = (void *)((char *)sequence + offsets->items[0].offset);
    }

    type_data->iterate(sequence, data, state, objects, nobjects);
}


void
sky_sequence_iterate_cleanup(sky_sequence_iterate_state_t *state)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(state->sequence));

    if (type_data->iterate_cleanup) {
        void            *data;
        sky_type_list_t *offsets;

        if (sky_object_istagged(state->sequence)) {
            data = NULL;
        }
        else {
            offsets = type_data->data_offsets;
            data = (void *)((char *)state->sequence + offsets->items[0].offset);
        }

        type_data->iterate_cleanup(state->sequence, data, state);
    }
}


static sky_object_t
sky_sequence_min_max(sky_object_t       sequence,
                     sky_object_t       key,
                     sky_compare_op_t   op)
{
    sky_object_t    element, iter, result_element, result_value, value;

    if (key && !sky_object_callable(key)) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ object is not callable",
                               sky_type_name(sky_object_type(key)));
    }

    result_value = NULL;
    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, element) {
            if (key) {
                value = sky_object_call(key, sky_tuple_pack(1, element), NULL);
            }
            else {
                value = element;
            }
            if (!result_value || sky_object_compare(value, result_value, op)) {
                result_element = element;
                result_value = value;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(sequence);
        while ((element = sky_object_next(iter, NULL)) != NULL) {
            if (key) {
                value = sky_object_call(key, sky_tuple_pack(1, element), NULL);
            }
            else {
                value = element;
            }
            if (!result_value || sky_object_compare(value, result_value, op)) {
                result_element = element;
                result_value = value;
            }
        }
    }

    if (!result_value) {
        sky_error_raise_string(sky_ValueError, "empty sequence");
    }

    return result_element;
}


sky_object_t
sky_sequence_max(sky_object_t sequence, sky_object_t key)
{
    return sky_sequence_min_max(sequence, key, SKY_COMPARE_OP_GREATER);
}


sky_object_t
sky_sequence_min(sky_object_t sequence, sky_object_t key)
{
    return sky_sequence_min_max(sequence, key, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_sequence_sum(sky_object_t sequence, sky_object_t start)
{
    sky_bool_t      fallback;
    sky_object_t    element, iter, result;

    if (sky_object_isnull(start)) {
        start = sky_integer_zero;
    }
    else {
        /* start cannot be strings - use ''.join() instead. */
        if (sky_object_isa(start, sky_string_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "cannot sum strings [use ''.join(sequence) instead]");
        }
        if (sky_object_isa(start, sky_bytes_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "cannot sum bytes [use b''.join(sequence) instead]");
        }
        if (sky_object_isa(start, sky_bytearray_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "cannot sum bytearrays [use bytearray(b'').join(sequence) instead]");
        }
    }

    if (!start || sky_integer_type == sky_object_type(start)) {
        SKY_ERROR_TRY {
            result = sky_integer_sum(start, sequence);
        } SKY_ERROR_EXCEPT(sky_TypeError) {
            result = NULL;
        } SKY_ERROR_TRY_END;

        if (result) {
            return result;
        }
    }

    if (sky_float_type == sky_object_type(start)) {
        double  value;

        value = sky_float_value(start);
        if (sky_sequence_isiterable(sequence)) {
            SKY_SEQUENCE_FOREACH(sequence, element) {
                if (sky_float_type != sky_object_type(element)) {
                    fallback = SKY_TRUE;
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
                value += sky_float_value(element);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(sequence);
            while ((element = sky_object_next(iter, NULL)) != NULL) {
                if (sky_float_type != sky_object_type(element)) {
                    fallback = SKY_TRUE;
                    break;
                }
                value += sky_float_value(element);
            }
        }
        if (!fallback) {
            return sky_float_create(value);
        }
    }

    result = start;
    if (sky_sequence_isiterable(sequence)) {
        SKY_SEQUENCE_FOREACH(sequence, element) {
            result = sky_number_add(result, element);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(sequence);
        while ((element = sky_object_next(iter, NULL)) != NULL) {
            result = sky_number_add(result, element);
        }
    }

    return result;
}
