#ifndef __SKYTHON_CORE_SKY_ATFORK_H__
#define __SKYTHON_CORE_SKY_ATFORK_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/* The signature of a function called to wake up a thread to perform atfork
 * handling.
 */
typedef void (*sky_atfork_wakeup_t)(void *arg);


typedef enum sky_atfork_state_e {
    SKY_ATFORK_STATE_IDLE = 0,
    SKY_ATFORK_STATE_PREPARING,
    SKY_ATFORK_STATE_RESUMING
} sky_atfork_state_t;

/* Per-thread information required for atfork handling. */
typedef struct sky_atfork_tlsdata_s {
    sky_atfork_wakeup_t volatile        wakeup_function;
    void * volatile                     wakeup_arg;
    volatile sky_atfork_state_t         state;
} sky_atfork_tlsdata_t;

/* Clean up a thread's atfork handling data. Called automatically by the
 * tlsdata code when a registered thread exits.
 */
extern void
sky_atfork_tlsdata_cleanup(sky_atfork_tlsdata_t *tlsdata);


/* A Skython "owned" thread calls this to let the atfork machinery know that it
 * is starting, and that a process fork should wait for it.
 */
extern void
sky_atfork_register(sky_atfork_wakeup_t function, void *arg);

/* All Skython "owned" threads must periodically call this function. It is
 * used to allow Skython to suspend and resume all of its threads when the
 * process is forking.
 */
extern void
sky_atfork_check(void);


/* Initializes the atfork handling infrastructure. Called during library
 * initialization.
 */
extern void
sky_atfork_initialize(void);


extern void sky_atfork_prepare(void);
extern void sky_atfork_parent(void);
extern void sky_atfork_child(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ATFORK_H__ */
