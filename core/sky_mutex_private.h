#ifndef __SKYTHON_CORE_SKY_MUTEX_PRIVATE_H__
#define __SKYTHON_CORE_SKY_MUTEX_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_thread_private.h"


SKY_CDECLS_BEGIN


typedef struct sky_mutex_data_s {
    sky_thread_t                        owner;
    int32_t                             count;
    uint32_t                            reentrant   : 1;
    uint32_t                            strict      : 1;
    sky_thread_waitlist_t               waitlist;
} sky_mutex_data_t;

SKY_STATIC_INLINE sky_mutex_data_t *
sky_mutex_data(sky_object_t object)
{
    return sky_object_data(object, sky_mutex_type);
}


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MUTEX_PRIVATE_H__ */
