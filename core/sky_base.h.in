/** @file
  * @brief Base definitions and types.
  * @defgroup sky_base Base Functions, Macros, and Types
  * @{
  */

#ifndef __SKYTHON_SKY_BASE_H__
#define __SKYTHON_SKY_BASE_H__ 1


#include <sys/types.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#if defined(_WIN32)
#   include <winsock2.h>
#   include <ws2tcpip.h>
#   include <mswsock.h>
#   include <windows.h>
#else
#   include <unistd.h>
#   include <sys/socket.h>
#endif

#include "sky_stdint.h"

#include <time.h>


/** Major version number of Skython as an integer **/
#define SKYTHON_VERSION_MAJOR               @SKYTHON_VERSION_MAJOR@
/** Minor version number of Skython as an integer **/
#define SKYTHON_VERSION_MINOR               @SKYTHON_VERSION_MINOR@
/** Micro version number of Skython as an integer **/
#define SKYTHON_VERSION_MICRO               @SKYTHON_VERSION_MICRO@
/** Serail version number of Skython as an integer **/
#define SKYTHON_VERSION_SERIAL              @SKYTHON_VERSION_SERIAL@
/** Build version number of Skython as an integer **/
#define SKYTHON_VERSION_BUILD               "@SKYTHON_VERSION_BUILD@"
/** Skython version release level as a string **/
#define SKYTHON_VERSION_RELEASELEVEL        "@SKYTHON_VERSION_RELEASELEVEL@"
/** Full version of Skython as a string **/
#if SKYTHON_VERSION_SERIAL > 0
#   define SKYTHON_VERSION_STRING           "@SKYTHON_VERSION_MAJOR@.@SKYTHON_VERSION_MINOR@.@SKYTHON_VERSION_MICRO@@SKYTHON_VERSION_RELEASELEVEL@@SKYTHON_VERSION_SERIAL@"
#else
#   define SKYTHON_VERSION_STRING           "@SKYTHON_VERSION_MAJOR@.@SKYTHON_VERSION_MINOR@.@SKYTHON_VERSION_MICRO@"
#endif


/** ABI version as an integer **/
#define SKYTHON_ABI_VERSION                 @SKYTHON_ABI_VERSION@

/** API version as an integer **/
#define SKYTHON_API_VERSION                 @SKYTHON_API_VERSION@


/** The major Python language version to which Skython conforms as an integer. **/
#define SKYTHON_PYTHON_VERSION_MAJOR        @SKYTHON_PYTHON_VERSION_MAJOR@
/** The minor Python language version to which Skython conforms as an integer. **/
#define SKYTHON_PYTHON_VERSION_MINOR        @SKYTHON_PYTHON_VERSION_MINOR@
/** The micro Python language version to which Skython conforms as an integer. **/
#define SKYTHON_PYTHON_VERSION_MICRO        @SKYTHON_PYTHON_VERSION_MICRO@
/** The releaselevel Python language version to which Skython conforms as a string. **/
#define SKYTHON_PYTHON_VERSION_RELEASELEVEL "@SKYTHON_PYTHON_VERSION_RELEASELEVEL@"
/** The serial Python language version to which Skython conforms as an integer. **/
#define SKYTHON_PYTHON_VERSION_SERIAL       @SKYTHON_PYTHON_VERSION_SERIAL@
/** The Python language version as a string literal. **/
#if SKYTHON_PYTHON_VERSION_SERIAL > 0
#   define SKYTHON_PYTHON_VERSION_STRING    "@SKYTHON_PYTHON_VERSION_MAJOR@.@SKYTHON_PYTHON_VERSION_MINOR@.@SKYTHON_PYTHON_VERSION_MICRO@@SKYTHON_PYTHON_VERSION_RELEASELEVEL@@SKYTHON_PYTHON_VERSION_SERIAL@"
#else
#   define SKYTHON_PYTHON_VERSION_STRING    "@SKYTHON_PYTHON_VERSION_MAJOR@.@SKYTHON_PYTHON_VERSION_MINOR@.@SKYTHON_PYTHON_VERSION_MICRO@"
#endif


#if defined(__i386__) || defined(_M_IX86)
/** Defined to 1 if the current build architecture is 32-bits.  **/
#   define  SKY_ARCH_32BIT              1
#endif
#if defined(__x86_64__) || defined(_M_X64)
/** Defined to 1 if the current build architecture is 64-bits.  **/
#   define  SKY_ARCH_64BIT              1
#endif
#if !(defined(SKY_ARCH_32BIT) || defined(SKY_ARCH_64BIT))
#   error   unsupported architecture
#endif


/** @cond **/
#cmakedefine HAVE_ALIGNED_ATTRIBUTE
#cmakedefine HAVE_NORETURN_ATTRIBUTE
#cmakedefine HAVE_RETURNS_TWICE_ATTRIBUTE
#cmakedefine HAVE_UNUSED_ATTRIBUTE
#cmakedefine HAVE_VISIBILITY_ATTRIBUTE


#if !defined(__STDC_VERSION__) || __STDC_VERSION__ < 199901
#   if _MSC_VER >= 1400 || __GNUC__ >= 3 || defined(__clang__)
#       define  restrict    __restrict
#   else
#       define  restrict
#   endif
#   if !defined(__cplusplus)
#       define  inline  __inline
#   endif
#endif


#if __NO_INLINE__
#   define SKY_EXTERN_INLINE static inline
#   define SKY_STATIC_INLINE static inline
#elif __GNUC_GNU_INLINE__
#   define SKY_EXTERN_INLINE extern inline
#   define SKY_STATIC_INLINE static inline
#else
#   define SKY_EXTERN_INLINE inline
#   define SKY_STATIC_INLINE static inline
#endif


#if defined(HAVE_VISIBILITY_ATTRIBUTE)
#   define  SKY_EXTERN          extern __attribute__ ((visibility("default")))
#   define  SKY_MODULE_EXTERN   extern __attribute__ ((visibility("default")))
#elif defined(_WIN32)
#   if defined(sky_core_EXPORTS)
#       define  SKY_EXTERN  extern __declspec(dllexport)
#   else
#       define  SKY_EXTERN  extern __declspec(dllimport)
#   endif
#   if defined(sky_module_EXPORTS)
#       define  SKY_MODULE_EXTERN   extern __declspec(dllexport)
#   else
#       define  SKY_MODULE_EXTERN   extern __declspec(dllimport)
#   endif
#else
#   define  SKY_EXTERN          extern
#   define  SKY_MODULE_EXTERN   extern
#endif
/** @endcond **/


/** @def likely(expr)
  * Evaluate an expression with a hint to the compiler that it is expected to
  * evaluate true.
  *
  * @def unlikely(expr)
  * Evaluate an expression with a hint to the compiler that it is expected to
  * evaluate false.
  */
#if defined(__GNUC__) || defined(__clang__)
#   define  likely(expr)    __builtin_expect(!!(expr), 1)
#   define  unlikely(expr)  __builtin_expect(!!(expr), 0)
#else
#   define  likely(expr)    (expr)
#   define  unlikely(expr)  (expr)
#endif


/** @def SKY_ALIGNED(x)
  * Used to force a specific byte alignment for a type or variable.
  */
#if defined(HAVE_ALIGNED_ATTRIBUTE)
#   define  SKY_ALIGNED(x)  __attribute__ ((aligned(x)))
#elif defined(_MSC_VER)
#   define  SKY_ALIGNED(x)  __declspec(align(x))
#else
#   define  SKY_ALIGNED(x)
#endif


/** @def SKY_NORETURN
  * Used to mark a function that never returns.
  */
#if defined(HAVE_NORETURN_ATTRIBUTE)
#   define SKY_NORETURN     __attribute__ ((noreturn))
#elif defined(_MSC_VER)
#   define SKY_NORETURN     __declspec(noreturn)
#else
#   define SKY_NORETURN
#endif


/** @def SKY_UNUSED
  * Used to mark a variable or function that is unused.
  */
#if defined(HAVE_UNUSED_ATTRIBUTE)
#   define SKY_UNUSED       __attribute__ ((unused))
#else
#   define SKY_UNUSED
#endif


/** @def SKY_RETURNS_TWICE
  * Used to mark a function that returns twice. Examples of such functions in
  * the standard C runtime would be @c setjmp() and @c fork().
  */
#if defined(HAVE_RETURNS_TWICE_ATTRIBUTE)
#   define SKY_RETURNS_TWICE    __attribute__ ((returns_twice))
#else
#   define SKY_RETURNS_TWICE
#endif


/** @cond **/
#include <setjmp.h>
#cmakedefine HAVE_LIBUNWIND_H
#if defined(__APPLE__)
#   if defined(HAVE_LIBUNWIND_H)
SKY_EXTERN int  SKY_RETURNS_TWICE
sky_setjmp(jmp_buf env);

SKY_EXTERN void SKY_NORETURN
sky_longjmp(jmp_buf env, int val);
#   else
#       define sky_longjmp(env, val)    _longjmp(env, val)
#       define sky_setjmp(env)          _setjmp(env)
#   endif
#else
#   define sky_longjmp(env, val)    longjmp(env, val)
#   define sky_setjmp(env)          setjmp(env)
#endif
/** @endcond **/


/** @def SKY_CDECLS_BEGIN
  * Used to mark the beginning of a block of declarations and/or definitions
  * that should have C linkage (i.e., their symbol names should not be mangled).
  * @def SKY_CDECLS_END
  * Used to mark the end of a block of declarations and/or definitions that
  * should have C linkage (i.e., their symbol names should not be manged).
  */
#if defined(__cplusplus)
#   define  SKY_CDECLS_BEGIN    extern "C" {
#   define  SKY_CDECLS_END      }
#else
#   define  SKY_CDECLS_BEGIN
#   define  SKY_CDECLS_END
#endif


/** @cond **/
#if !defined(SIZE_MAX)
#   define SIZE_MAX     UINTPTR_MAX
#endif
#if !defined(SSIZE_MAX)
#   define SSIZE_MAX    INTPTR_MAX
#   define SSIZE_MIN    INTPTR_MIN
#endif


/* uptrdiff_t is not a real type, but it's here for parity since we support
 * a modifier for ptrdiff_t.
 */
#if defined(SKY_ARCH_32BIT)
typedef uint32_t uptrdiff_t;
#   define UPTRDIFF_MAX UINT32_MAX
#elif defined(SKY_ARCH_64BIT)
typedef uint64_t uptrdiff_t;
#   define UPTRDIFF_MAX UINT64_MAX
#else
#   error implementation missing
#endif


#if defined(_MSC_VER)
#   if defined(_WIN64)
typedef int64_t ssize_t;
#   else
typedef int32_t ssize_t;
#   endif

struct iovec {
    void *                              iov_base;
    size_t                              iov_len;
};

struct msghdr {
    void *                              msg_name;
    socklen_t                           msg_namelen;
    struct iovec *                      msg_iov;
    int                                 msg_iovlen;
    void *                              msg_control;
    socklen_t                           msg_controllen;
    int                                 msg_flags;
};

struct timespec {
    time_t                              tv_sec;
    int64_t                             tv_nsec;
};
#endif
/** @endcond **/


/** Determine the greater of two integral expressions.
  * @warning the expression that evaluates greater will be evaluated twice.
  */
#define SKY_MAX(a, b)   ((a) >= (b) ? (a) : (b))

/** Determine the lesser of two integral expressions.
  * @warning the expression that evaluates lesser will be evaluated twice.
  */
#define SKY_MIN(a, b)   ((b) >= (a) ? (a) : (b))


/** @def SKY_CACHE_LINE_BITS
  * The size of a CPU cache line in bits.
  * @def SKY_CACHE_LINE_SIZE
  * The size of a CPU cache line in bytes.
  */
#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
    /* SKY_CACHE_LINE_SIZE cannot be defined in terms of SKY_CACHE_LINE_BITS
     * (e.g., 1 << SKY_CACHE_LINE_BITS), because on Windows
     * __declspec(align(xx)) requires xx to be a constant. It cannot be an
     * expression.
     */
#   define SKY_CACHE_LINE_BITS          6
#   define SKY_CACHE_LINE_SIZE          64
#else
#   error hardware cache-line size information unknown for this architecture
#endif


/** The mask for a cache line. **/
#define SKY_CACHE_LINE_MASK             (SKY_CACHE_LINE_SIZE - 1)

/** The amount of padding necessary to round x up to the size of a cache line. **/
#define SKY_CACHE_LINE_PAD(x)           (((x) & SKY_CACHE_LINE_MASK) ? \
        (SKY_CACHE_LINE_SIZE - ((x) & SKY_CACHE_LINE_MASK)) : 0)

/** SKY_ALIGNED(SKY_CACHE_LINE_SIZE) **/
#define SKY_CACHE_LINE_ALIGNED          SKY_ALIGNED(SKY_CACHE_LINE_SIZE)


/** Safely cast one type to another type. **/
#if defined(__GNUC__) || defined(__clang__)
#   define SKY_AS_TYPE(_p, _t)          (((union { __typeof__(_p) ap; _t tp; })(_p)).tp)
#else
#   define SKY_AS_TYPE(_p, _t)          (_t)(_p)
#endif

/** Safely cast a pointer of one type to void *. **/
#define SKY_AS_VOIDP(_p)                SKY_AS_TYPE(_p, void *)

/** Safely cast a pointer of one type to sky_object_t *. **/
#define SKY_AS_OBJECTP(_p)              SKY_AS_TYPE(_p, sky_object_t *)


/** Boolean value constants for true and false. **/
enum sky_bool_e {
    SKY_FALSE = 0,                      /**< false (0) **/
    SKY_TRUE  = 1,                      /**< true (1)  **/
};
typedef int32_t sky_bool_t;             /**< 32-bit signed representation of a Boolean value **/


/** Comparison operations. **/
typedef enum sky_compare_op_e {
    SKY_COMPARE_OP_NOT_EQUAL        =   0x0,
    SKY_COMPARE_OP_EQUAL            =   0x1,
    SKY_COMPARE_OP_GREATER          =   0x2,
    SKY_COMPARE_OP_LESS             =   0x4,

    SKY_COMPARE_OP_GREATER_EQUAL    =   SKY_COMPARE_OP_EQUAL | SKY_COMPARE_OP_GREATER,
    SKY_COMPARE_OP_LESS_EQUAL       =   SKY_COMPARE_OP_EQUAL | SKY_COMPARE_OP_LESS,

    SKY_COMPARE_OP_IS               =   0x10,
    SKY_COMPARE_OP_IS_NOT           =   0x20,
    SKY_COMPARE_OP_IN               =   0x40,
    SKY_COMPARE_OP_NOT_IN           =   0x80,
} sky_compare_op_t;


/** Compare two strings case-insensitively. **/
SKY_EXTERN_INLINE int
sky_strcasecmp(const char *s1, const char *s2)
{
#if defined(_WIN32)
    return _stricmp(s1, s2);
#else
    return strcasecmp(s1, s2);
#endif
}


/** Compare two strings case-insensitively. **/
SKY_EXTERN_INLINE int
sky_strncasecmp(const char *s1, const char *s2, size_t n)
{
#if defined(_WIN32)
    return _strnicmp(s1, s2, n);
#else
    return strncasecmp(s1, s2, n);
#endif
}


/** @cond **/
/* sky_system.h needs these */
#cmakedefine HAVE_PTY_H
#cmakedefine HAVE_UTIL_H
#cmakedefine HAVE_FORK1
/** @endcond **/


#endif  /* __SKYTHON_SKY_BASE_H__ */

/** @} **/
