/** @file
  * @brief
  * @defgroup containers Containers
  * @{
  * @defgroup sky_frozenset Immutable Key Sets
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FROZENSET_H__
#define __SKYTHON_CORE_SKY_FROZENSET_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** A frozen set object instance that contains nothing. **/
SKY_EXTERN sky_frozenset_t const sky_frozenset_empty;


/** Create a new frozen set object instance from an array of objects.
  *
  * @param[in]  count   the number of objects present in the @a objects list.
  * @param[in]  objects the objects to be used to populate the set. Any
  *                     @c NULL pointers will be replaced with @c sky_None
  *                     objects.
  * @return     the new frozen set object instance.
  */
SKY_EXTERN sky_frozenset_t
sky_frozenset_createfromarray(ssize_t count, sky_object_t *objects);


/** Determine whether a key already exists in a set.
  * 
  * @param[in]  set     the set to query.
  * @param[in]  key     the key to be queried.
  * @return     @c SKY_TRUE if the key exists in the set, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_frozenset_contains(sky_frozenset_t set, sky_object_t key);


/** Return the difference of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a list of sets to subtract from @a self.
  * @return     a new set that is @a self with all elements from the sets in
  *             @a args removed.
  */
SKY_EXTERN sky_frozenset_t
sky_frozenset_difference(sky_frozenset_t self, sky_object_t args);


/** Return the intersection of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a list of sets to intersect with @a self.
  * @return     a new set that contains only the items from @a self and the
  *             sets in @a args that are in all of the sets.
  */
SKY_EXTERN sky_frozenset_t
sky_frozenset_intersection(sky_frozenset_t self, sky_object_t args);


/** Determine if two sets are disjoint.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a self and @a other do not intersect.
  */
SKY_EXTERN sky_bool_t
sky_frozenset_isdisjoint(sky_frozenset_t self, sky_object_t other);


/** Determine if a set is a subset of another set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a other is a subset of @a self.
  */
SKY_EXTERN sky_bool_t
sky_frozenset_issubset(sky_frozenset_t self, sky_object_t other);


/** Determine if a set is a superset of another set.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     @c SKY_TRUE if @a other is a superset of @a self.
  */
SKY_EXTERN sky_bool_t
sky_frozenset_issuperset(sky_frozenset_t self, sky_object_t other);


/** Return the number of keys in a set.
  *
  * @param[in]  set     the set for which the number of keys it contains is to
  *                     be returned.
  * @return     the number of keys contained in @a set.
  */
SKY_EXTERN ssize_t
sky_frozenset_len(sky_frozenset_t set);


/** Return the symmetric difference of two sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  other   the second set.
  * @return     a new set that contains only those items from @a self or
  *             @a other that exist in exactly one of the two sets.
  */
SKY_EXTERN sky_frozenset_t
sky_frozenset_symmetric_difference(sky_frozenset_t self, sky_object_t other);


/** Return the union of two or more sets.
  *
  * @param[in]  self    the first set.
  * @param[in]  args    a list of sets to combine with @a self.
  * @return     a new set that contains all of the elements from @a self and
  *             the sets in the list of sets, @a args.
  */
SKY_EXTERN sky_frozenset_t
sky_frozenset_union(sky_frozenset_t self, sky_object_t args);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FROZENSET_H__ */

/** @} **/
/** @} **/
