/** @file
  * @brief Top-level public header.
  * This header file is the top-level public header. It can be included without
  * having to directly include anything else from the Skython package.
  * In addition, it contains the declarations for the library initialization
  * and finalization functions, which must be used in order to safely use any
  * of the Skython library.
  *
  * @defgroup skython Library Initialization and Finalization
  * @{
  */

#ifndef __SKYTHON_CORE_SKYTHON_H__
#define __SKYTHON_CORE_SKYTHON_H__ 1


#include "sky_base.h"
#include "sky_data_type.h"
#include "sky_endian.h"
#include "sky_error.h"
#include "sky_asset.h"

#include "sky_atomic.h"
#include "sky_atomic_lifo.h"
#include "sky_once.h"
#include "sky_spinlock.h"

#include "sky_hazard_pointer.h"
#include "sky_malloc.h"
#include "sky_system_malloc.h"
#include "sky_tls.h"

#include "sky_ctype.h"
#include "sky_format.h"
#include "sky_hashtable.h"
#include "sky_lockfree_hashtable.h"
#include "sky_system.h"
#include "sky_time.h"
#include "sky_unicode.h"
#include "sky_util.h"

#include "sky_interpreter.h"

#include "sky_object.h"
#include "sky_base_object.h"
#include "sky_type.h"

#include "sky_buffer.h"
#include "sky_number.h"
#include "sky_sequence.h"

#include "sky_classmethod.h"
#include "sky_descriptor.h"
#include "sky_getset.h"
#include "sky_member.h"
#include "sky_method.h"
#include "sky_property.h"
#include "sky_staticmethod.h"

#include "sky_capsule.h"
#include "sky_mappingproxy.h"
#include "sky_native_code.h"
#include "sky_parameter_list.h"

#include "sky_exceptions.h"
#include "sky_signal.h"

#include "sky_Ellipsis.h"
#include "sky_None.h"
#include "sky_NotImplemented.h"
#include "sky_NotSpecified.h"

#include "sky_boolean.h"
#include "sky_bytearray.h"
#include "sky_bytes.h"
#include "sky_bytes_builder.h"
#include "sky_complex.h"
#include "sky_dict.h"
#include "sky_float.h"
#include "sky_frozenset.h"
#include "sky_integer.h"
#include "sky_list.h"
#include "sky_module.h"
#include "sky_set.h"
#include "sky_string.h"
#include "sky_string_builder.h"
#include "sky_tuple.h"

#include "sky_callable_iterator.h"
#include "sky_codec.h"
#include "sky_enumerate.h"
#include "sky_file.h"
#include "sky_filter.h"
#include "sky_function.h"
#include "sky_map.h"
#include "sky_memoryview.h"
#include "sky_mutex.h"
#include "sky_namespace.h"
#include "sky_range.h"
#include "sky_slice.h"
#include "sky_socket.h"
#include "sky_struct_sequence.h"
#include "sky_thread.h"
#include "sky_weakproxy.h"
#include "sky_weakref.h"
#include "sky_zip.h"


SKY_CDECLS_BEGIN


/** Constants to control library initialization behavior. **/
typedef enum sky_library_initialize_flag_e {
    /** Do not bootstrap the import machinery, effectively disabling module
      * importing.
      */
    SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES          =   (1 << 0),

    /** Do not write bytecode files on import. **/
    SKY_LIBRARY_INITIALIZE_FLAG_DONT_WRITE_BYTECODE =   (1 << 1),

    /** Do not add user site directory to sys.path. **/
    SKY_LIBRARY_INITIALIZE_FLAG_NO_USER_SITE        =   (1 << 2),

    /** Disable the import of the site module. **/
    SKY_LIBRARY_INITIALIZE_FLAG_NO_SITE             =   (1 << 3),

    /** Ignore environment variables that modify the behavior of Skython. **/
    SKY_LIBRARY_INITIALIZE_FLAG_IGNORE_ENVIRONMENT  =   (1 << 4),

    /** Enable verbose output. **/
    SKY_LIBRARY_INITIALIZE_FLAG_VERBOSE             =   (1 << 5),

    /** Enable very verbose output. **/
    SKY_LIBRARY_INITIALIZE_FLAG_VERY_VERBOSE        =   (1 << 6),

    /** Disable the display of version and copyright messages in interactive
      * mode.
      */
    SKY_LIBRARY_INITIALIZE_FLAG_QUIET               =   (1 << 7),

    /** Force the binary I/O layers of stdin, stdout, and stderr to be
      * unbuffered.
      */
    SKY_LIBRARY_INITIALIZE_FLAG_UNBUFFERED_STDIO    =   (1 << 8),

    /** Enable bytecode optimizations. **/
    SKY_LIBRARY_INITIALIZE_FLAG_OPTIMIZE_BYTECODE   =   (1 << 9),

    /** Discard docstrings. This will also enable bytecode optimization. **/
    SKY_LIBRARY_INITIALIZE_FLAG_DISCARD_DOCSTRINGS  =   (1 << 10),

    /** Enable debug mode. **/
    SKY_LIBRARY_INITIALIZE_FLAG_DEBUG               =   (1 << 11),

    /** Enable warnings for str(bytes) or str(bytearray). **/
    SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS      =   (1 << 12),

    /** Enable errors for str(bytes) or str(bytearray). **/
    SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS        =   (1 << 13),
} sky_library_initialize_flag_t;


/** Initialize the Skython library for use.
  * Initialization of the library may be done more than once, but for each call
  * to sky_library_initialize(), there must be a corresponding call to
  * sky_library_finalize(). If library initialization fails, @c SKY_FALSE will
  * be returned. The Skython library must be initialized before any of its
  * services may be used.
  *
  * @note   If memory allocation hooks are to be set via sky_malloc_sethooks(),
  *         they should be set before calling sky_library_initialize() for the
  *         first time.
  *
  * @warning    For the sake of efficiency there are no checks anywhere to
  *             verify that the library is initialized. In many cases, any of
  *             the library's services that require initialization to have been
  *             done will likely raise an unrelated exception or crash.
  *
  * @param[in]  argv0   the original command-line argv[0] as passed to main()
  *                     by the operating system. Used to figure out the value
  *                     of sys.executable.
  * @param[in]  argc    command-line argument count.
  * @param[in]  argv    command-line arguments.
  * @param[in]  flags   flags to control initialization behavior.
  */
SKY_EXTERN sky_bool_t
sky_library_initialize(const char * argv0,
                       int          argc,
                       char *       argv[],
                       unsigned int flags);


/** Finalize the Skython library.
  * Finalization of the Skython library complements initialization of the
  * library. For each initialization call, a matching finalization call must be
  * made. No actual finalization work will occur until the last matching
  * finalization call is made. Once actual finalization has been done, it is
  * safe to call sky_library_initialize() again.
  */
SKY_EXTERN void
sky_library_finalize(void);


/** The signature of a function to be called during library finalization. **/
typedef void (*sky_library_finalize_t)(void);


/** Register a function to be called when library finalization is performed.
  * Registered finalization functions are called in the reverse order that they
  * are registered. The registration for the function is forgotten once the
  * function is called. That is, if sky_library_initialize() is called again
  * after sky_library_finalize() has run, any finalization function to be
  * called the next time sky_library_finalize() runs must be registered again.
  *
  * @param[in]  function    the function to be called.
  */
SKY_EXTERN void
sky_library_atfinalize(sky_library_finalize_t function);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKYTHON_H__ */

/** @} **/
