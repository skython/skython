#include "sky_private.h"


sky_bool_t
sky_number_check(sky_object_t object)
{
    return ((SKY_OBJECT_METHOD_SLOT(object, COMPLEX) ||
             SKY_OBJECT_METHOD_SLOT(object, INT) ||
             SKY_OBJECT_METHOD_SLOT(object, FLOAT)) ? SKY_TRUE : SKY_FALSE);
}

typedef sky_object_t
        (*sky_number_binary_op_t)(sky_object_t, sky_object_t);

static sky_object_t
sky_number_binary_call(sky_object_t method,
                       sky_object_t self,
                       sky_object_t other)
{
    sky_object_t            result;
    sky_number_binary_op_t  cfunction;

    if (!method) {
        return sky_NotImplemented;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_binary_op_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (self, other));
    }
    else {
        method = sky_descriptor_get(method, self, sky_object_type(self));
        result = sky_object_call(method, sky_tuple_pack(1, other), NULL);
    }
    return result;
}

static sky_object_t
sky_number_binary_op(sky_object_t   self,
                     sky_object_t   other,
                     const char *   op_name,
                     sky_object_t   method,
                     sky_object_t   rmethod)
{
    sky_object_t    result;

    /* If type(self) == type(other):
     * 1. call method(self, other)
     *
     * If issubtype(type(other), type(self)):
     * 1. call rmethod(other, self)
     * 2. call method(self, other)
     *
     * If type(self) != type(other)
     * 1. call method(self, other)
     * 2. call rmethod(other, self)
     */

    if (sky_object_type(self) == sky_object_type(other)) {
        result = sky_number_binary_call(method, self, other);
    }
    else if (sky_type_issubtype(sky_object_type(other),
                                sky_object_type(self)))
    {
        result = sky_number_binary_call(rmethod, other, self);
        if (result != sky_NotImplemented) {
            return result;
        }
        result = sky_number_binary_call(method, self, other);
    }
    else {
        result = sky_number_binary_call(method, self, other);
        if (result != sky_NotImplemented) {
            return result;
        }
        result = sky_number_binary_call(rmethod, other, self);
    }

    if (sky_NotImplemented != result) {
        return result;
    }
    sky_error_raise_format(sky_TypeError,
            "unsupported operand type(s) for %s: %#@ and %#@",
            op_name,
            sky_type_name(sky_object_type(self)),
            sky_type_name(sky_object_type(other)));
}

static sky_object_t
sky_number_inplace_binary_op(sky_object_t   self,
                             sky_object_t   other,
                             const char *   op_name,
                             sky_object_t   imethod,
                             sky_object_t   method,
                             sky_object_t   rmethod)
{
    sky_object_t    result;

    result = sky_number_binary_call(imethod, self, other);
    if (sky_NotImplemented != result) {
        return result;
    }

    return sky_number_binary_op(self, other, op_name, method, rmethod);
}


typedef sky_object_t
        (*sky_number_ternary_op_t)(sky_object_t, sky_object_t, sky_object_t);

static sky_object_t
sky_number_ternary_call(sky_object_t method,
                        sky_object_t self,
                        sky_object_t other,
                        sky_object_t third)
{
    sky_tuple_t             args;
    sky_object_t            result;
    sky_number_ternary_op_t cfunction;

    if (!method) {
        return sky_NotImplemented;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_ternary_op_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (self, other, third));
    }
    else {
        method = sky_descriptor_get(method, self, sky_object_type(self));
        if (sky_object_isnull(third)) {
            args = sky_tuple_pack(1, other);
        }
        else {
            args = sky_tuple_pack(2, other, third);
        }
        result = sky_object_call(method, args, NULL);
    }
    return result;
}

static sky_object_t
sky_number_ternary_op(sky_object_t  self,
                      sky_object_t  other,
                      sky_object_t  third,
                      const char *  op_name,
                      sky_object_t  method,
                      sky_object_t  rmethod)
{
    sky_object_t    result;

    /* If type(self) == type(other):
     * 1. call method(self, other, third)
     *
     * If issubtype(type(other), type(self)):
     * 1. call rmethod(other, self)
     * 2. call method(self, other, third)
     *
     * If type(self) != type(other)
     * 1. call method(self, other, third)
     * 2. call rmethod(other, self)
     */

    if (sky_object_type(self) == sky_object_type(other)) {
        result = sky_number_ternary_call(method, self, other, third);
    }
    else if (sky_type_issubtype(sky_object_type(other),
                                sky_object_type(self)))
    {
        if (rmethod) {
            result = sky_number_binary_call(rmethod, other, self);
            if (result != sky_NotImplemented) {
                return result;
            }
        }
        result = sky_number_ternary_call(method, self, other, third);
    }
    else {
        result = sky_number_ternary_call(method, self, other, third);
        if (result != sky_NotImplemented) {
            return result;
        }
        if (rmethod) {
            result = sky_number_binary_call(rmethod, other, self);
        }
    }

    if (sky_NotImplemented != result) {
        return result;
    }
    if (rmethod || sky_object_isnull(third)) {
        sky_error_raise_format(sky_TypeError,
                "unsupported operand type(s) for %s: %#@ and %#@",
                op_name,
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_object_type(other)));
    }
    else {
        sky_error_raise_format(sky_TypeError,
                "unsupported operand type(s) for %s: %#@, %#@, %#@",
                op_name,
                sky_type_name(sky_object_type(self)),
                sky_type_name(sky_object_type(other)),
                sky_type_name(sky_object_type(third)));
    }
}

static sky_object_t
sky_number_inplace_ternary_op(sky_object_t  self,
                              sky_object_t  other,
                              sky_object_t  third,
                              const char *  op_name,
                              sky_object_t  imethod,
                              sky_object_t  method,
                              sky_object_t  rmethod)
{
    sky_object_t    result;

    result = sky_number_ternary_call(imethod, self, other, third);
    if (sky_NotImplemented != result) {
        return result;
    }

    return sky_number_ternary_op(self, other, third, op_name, method, rmethod);
}


typedef sky_object_t
        (*sky_number_absolute_t)(sky_object_t);

sky_object_t
sky_number_absolute(sky_object_t object)
{
    sky_object_t            method, result;
    sky_number_absolute_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, ABS))) {
        sky_error_raise_format(sky_TypeError,
                               "bad operand type for abs(): %#@",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_absolute_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


sky_object_t
sky_number_add(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "+",
                                SKY_OBJECT_METHOD_SLOT(self, ADD),
                                SKY_OBJECT_METHOD_SLOT(other, RADD));
}


sky_object_t
sky_number_inplace_add(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "+=",
                                        SKY_OBJECT_METHOD_SLOT(self, IADD),
                                        SKY_OBJECT_METHOD_SLOT(self, ADD),
                                        SKY_OBJECT_METHOD_SLOT(other, RADD));
}


sky_object_t
sky_number_and(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "&",
                                SKY_OBJECT_METHOD_SLOT(self, AND),
                                SKY_OBJECT_METHOD_SLOT(other, RAND));
}


sky_object_t
sky_number_inplace_and(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "&=",
                                        SKY_OBJECT_METHOD_SLOT(self, IAND),
                                        SKY_OBJECT_METHOD_SLOT(self, AND),
                                        SKY_OBJECT_METHOD_SLOT(other, RAND));
}


typedef sky_complex_t
        (*sky_number_complex_t)(sky_object_t);

sky_complex_t
sky_number_complex(sky_object_t object)
{
    sky_object_t            method, result;
    sky_number_complex_t    cfunction;

    if (sky_complex_type == sky_object_type(object)) {
        return object;
    }

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, COMPLEX))) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ cannot be coerced to a complex object",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_complex_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_complex_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__complex__ must return a complex object");
    }
    return result;
}


sky_object_t
sky_number_divmod(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "divmod()",
                                SKY_OBJECT_METHOD_SLOT(self, DIVMOD),
                                SKY_OBJECT_METHOD_SLOT(other, RDIVMOD));
}


typedef sky_float_t
        (*sky_number_float_t)(sky_object_t);

sky_float_t
sky_number_float(sky_object_t object)
{
    sky_object_t        method, result;
    sky_number_float_t  cfunction;

    if (sky_float_type == sky_object_type(object)) {
        return object;
    }

    if ((method = SKY_OBJECT_METHOD_SLOT(object, FLOAT)) != NULL) {
        if (sky_object_isa(method, sky_native_code_type)) {
            cfunction = (sky_number_float_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
        }
        else {
            method = sky_descriptor_get(method, object, sky_object_type(object));
            result = sky_object_call(method, NULL, NULL);
        }
        if (!sky_object_isa(result, sky_float_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "__float__ must return a float object");
        }
        return result;
    }

    /* A float sub-type with no __float__() method. */
    if (sky_object_isa(object, sky_float_type)) {
        return sky_float_copy(object);
    }

    /* Bytes and strings cannot have a __float__() method, because then they'd
     * be considered numbers.
     */
    if (sky_buffer_check(object) || sky_object_isa(object, sky_string_type)) {
        return sky_float_createfromstring(object);
    }

    sky_error_raise_format(sky_TypeError,
                           "%#@ cannot be coerced to a float object",
                           sky_type_name(sky_object_type(object)));
}


sky_object_t
sky_number_floordivide(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "//",
                                SKY_OBJECT_METHOD_SLOT(self, FLOORDIV),
                                SKY_OBJECT_METHOD_SLOT(other, RFLOORDIV));
}


sky_object_t
sky_number_inplace_floordivide(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "//=",
                                        SKY_OBJECT_METHOD_SLOT(self, IFLOORDIV),
                                        SKY_OBJECT_METHOD_SLOT(self, FLOORDIV),
                                        SKY_OBJECT_METHOD_SLOT(other, RFLOORDIV));
}


typedef sky_integer_t
        (*sky_number_index_t)(sky_object_t);

sky_integer_t
sky_number_index(sky_object_t object)
{
    sky_object_t        method, result;
    sky_number_index_t  cfunction;

    if (sky_integer_type == sky_object_type(object)) {
        return object;
    }

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, INDEX))) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ object cannot be interpreted as an integer",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_index_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_integer_type)) {
        sky_error_raise_format(sky_TypeError,
                               "__index__ returned non-int (type %@)",
                               sky_type_name(sky_object_type(result)));
    }
    return result;
}


typedef sky_integer_t
        (*sky_number_integer_t)(sky_object_t);

sky_integer_t
sky_number_integer(sky_object_t object)
{
    sky_object_t            method, result;
    sky_number_integer_t    cfunction;

    if (sky_integer_type == sky_object_type(object)) {
        return object;
    }

    if ((method = SKY_OBJECT_METHOD_SLOT(object, INT)) != NULL) {
        if (sky_object_isa(method, sky_native_code_type)) {
            cfunction = (sky_number_integer_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
        }
        else {
            method = sky_descriptor_get(method, object, sky_object_type(object));
            result = sky_object_call(method, NULL, NULL);
        }
        if (!sky_object_isa(result, sky_integer_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "__int__ returned non-int (type %@)",
                                   sky_type_name(sky_object_type(result)));
        }
        return result;
    }

    /* An int sub-type with no __int__() method. */
    if (sky_object_isa(object, sky_integer_type)) {
        return sky_integer_copy(object);
    }

    /* Try __trunc__() */
    if ((method = sky_object_getattr(object,
                                     SKY_STRING_LITERAL("__trunc__"),
                                     NULL)) != NULL)
    {
        result = sky_object_call(method, NULL, NULL);
        if (sky_object_isa(result, sky_integer_type)) {
            return result;
        }
        if (SKY_OBJECT_METHOD_SLOT(result, INT)) {
            return sky_number_integer(result);
        }
        sky_error_raise_format(
                sky_TypeError,
                "__trunc__ returned non-Integral (type %@)",
                sky_type_name(sky_object_type(result)));
    }

    /* Bytes and strings cannot have an __int__() method, because then they'd
     * be considered numbers.
     */
    if (sky_buffer_check(object) || sky_object_isa(object, sky_string_type)) {
        return sky_integer_createfromstring(object, 10);
    }

    sky_error_raise_format(sky_TypeError,
                           "%#@ cannot be coerced to an integer object",
                           sky_type_name(sky_object_type(object)));
}


typedef sky_object_t
        (*sky_number_invert_t)(sky_object_t);

sky_object_t
sky_number_invert(sky_object_t object)
{
    sky_object_t        method, result;
    sky_number_invert_t cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, INVERT))) {
        sky_error_raise_format(sky_TypeError,
                               "bad operand for unary ~: %#@",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_invert_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


sky_object_t
sky_number_lshift(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "<<",
                                SKY_OBJECT_METHOD_SLOT(self, LSHIFT),
                                SKY_OBJECT_METHOD_SLOT(other, RLSHIFT));
}


sky_object_t
sky_number_inplace_lshift(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "<<=",
                                        SKY_OBJECT_METHOD_SLOT(self, ILSHIFT),
                                        SKY_OBJECT_METHOD_SLOT(self, LSHIFT),
                                        SKY_OBJECT_METHOD_SLOT(other, RLSHIFT));
}


sky_object_t
sky_number_modulo(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "%",
                                SKY_OBJECT_METHOD_SLOT(self, MOD),
                                SKY_OBJECT_METHOD_SLOT(other, RMOD));
}


sky_object_t
sky_number_inplace_modulo(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "%=",
                                        SKY_OBJECT_METHOD_SLOT(self, IMOD),
                                        SKY_OBJECT_METHOD_SLOT(self, MOD),
                                        SKY_OBJECT_METHOD_SLOT(other, RMOD));
}


sky_object_t
sky_number_multiply(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "*",
                                SKY_OBJECT_METHOD_SLOT(self, MUL),
                                SKY_OBJECT_METHOD_SLOT(other, RMUL));
}


sky_object_t
sky_number_inplace_multiply(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "*=",
                                        SKY_OBJECT_METHOD_SLOT(self, IMUL),
                                        SKY_OBJECT_METHOD_SLOT(self, MUL),
                                        SKY_OBJECT_METHOD_SLOT(other, RMUL));
}


typedef sky_object_t
        (*sky_number_negative_t)(sky_object_t);

sky_object_t
sky_number_negative(sky_object_t object)
{
    sky_object_t            method, result;
    sky_number_negative_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, NEG))) {
        sky_error_raise_format(sky_TypeError,
                               "bad operand type for unary -: %#@",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_negative_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


sky_object_t
sky_number_or(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "|",
                                SKY_OBJECT_METHOD_SLOT(self, OR),
                                SKY_OBJECT_METHOD_SLOT(other, ROR));
}


sky_object_t
sky_number_inplace_or(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "|=",
                                        SKY_OBJECT_METHOD_SLOT(self, IOR),
                                        SKY_OBJECT_METHOD_SLOT(self, OR),
                                        SKY_OBJECT_METHOD_SLOT(other, ROR));
}


typedef sky_object_t
        (*sky_number_positive_t)(sky_object_t);

sky_object_t
sky_number_positive(sky_object_t object)
{
    sky_object_t            method, result;
    sky_number_positive_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, POS))) {
        sky_error_raise_format(sky_TypeError,
                               "bad operand type for unary +: %#@",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_positive_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


sky_object_t
sky_number_pow(sky_object_t self, sky_object_t other, sky_object_t modulo)
{
    if (!sky_object_isnull(modulo)) {
        return sky_number_ternary_op(self, other, modulo, "** or pow()",
                                     SKY_OBJECT_METHOD_SLOT(self, POW),
                                     NULL);
    }
    return sky_number_ternary_op(self, other, modulo, "**",
                                 SKY_OBJECT_METHOD_SLOT(self, POW),
                                 SKY_OBJECT_METHOD_SLOT(other, RPOW));
}


sky_object_t
sky_number_inplace_pow(sky_object_t self,
                       sky_object_t other,
                       sky_object_t modulo)
{
    if (!sky_object_isnull(modulo)) {
        return
            sky_number_inplace_ternary_op(self, other, modulo, "** or pow()",
                                          SKY_OBJECT_METHOD_SLOT(self, IPOW),
                                          SKY_OBJECT_METHOD_SLOT(self, POW),
                                          NULL);
    }
    return sky_number_inplace_ternary_op(self, other, modulo, "**=",
                                         SKY_OBJECT_METHOD_SLOT(self, IPOW),
                                         SKY_OBJECT_METHOD_SLOT(self, POW),
                                         SKY_OBJECT_METHOD_SLOT(other, RPOW));
}


typedef sky_object_t
        (*sky_number_round_t)(sky_object_t, sky_object_t);

sky_object_t
sky_number_round(sky_object_t object, sky_object_t digits)
{
    sky_object_t        args, method, result;
    sky_number_round_t  cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, ROUND))) {
        sky_error_raise_format(sky_TypeError,
                               "type %#@ doesn't define __round__ method",
                               sky_type_name(sky_object_type(object)));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_number_round_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object, digits));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = (digits ? sky_tuple_pack(1, digits) : NULL);
        result = sky_object_call(method, args, NULL);
    }
    return result;
}


sky_object_t
sky_number_rshift(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, ">>",
                                SKY_OBJECT_METHOD_SLOT(self, RSHIFT),
                                SKY_OBJECT_METHOD_SLOT(other, RRSHIFT));
}


sky_object_t
sky_number_inplace_rshift(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, ">>=",
                                        SKY_OBJECT_METHOD_SLOT(self, IRSHIFT),
                                        SKY_OBJECT_METHOD_SLOT(self, RSHIFT),
                                        SKY_OBJECT_METHOD_SLOT(other, RRSHIFT));
}


sky_object_t
sky_number_subtract(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "-",
                                SKY_OBJECT_METHOD_SLOT(self, SUB),
                                SKY_OBJECT_METHOD_SLOT(other, RSUB));
}


sky_object_t
sky_number_inplace_subtract(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "-=",
                                        SKY_OBJECT_METHOD_SLOT(self, ISUB),
                                        SKY_OBJECT_METHOD_SLOT(self, SUB),
                                        SKY_OBJECT_METHOD_SLOT(other, RSUB));
}


sky_object_t
sky_number_truedivide(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "/",
                                SKY_OBJECT_METHOD_SLOT(self, TRUEDIV),
                                SKY_OBJECT_METHOD_SLOT(other, RTRUEDIV));
}


sky_object_t
sky_number_inplace_truedivide(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "/=",
                                        SKY_OBJECT_METHOD_SLOT(self, ITRUEDIV),
                                        SKY_OBJECT_METHOD_SLOT(self, TRUEDIV),
                                        SKY_OBJECT_METHOD_SLOT(other, RTRUEDIV));
}


sky_object_t
sky_number_xor(sky_object_t self, sky_object_t other)
{
    return sky_number_binary_op(self, other, "^",
                                SKY_OBJECT_METHOD_SLOT(self, XOR),
                                SKY_OBJECT_METHOD_SLOT(other, RXOR));
}


sky_object_t
sky_number_inplace_xor(sky_object_t self, sky_object_t other)
{
    return sky_number_inplace_binary_op(self, other, "^=",
                                        SKY_OBJECT_METHOD_SLOT(self, IXOR),
                                        SKY_OBJECT_METHOD_SLOT(self, XOR),
                                        SKY_OBJECT_METHOD_SLOT(other, RXOR));
}
