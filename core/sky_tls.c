#include "sky_tls_private.h"
#include "sky_private.h"


static int32_t          sky_tls_key_count = 0;
static sky_free_t       sky_tls_key_cleanups[255];

static sky_free_t const sky_tls_key_tombstone = (sky_free_t)(~(uintptr_t)0);


void
sky_tls_cleanup_tlsdata(sky_tls_tlsdata_t *tlsdata)
{
    void            *pointer;
    int32_t         i;

    for (i = 0;  i < sky_tls_key_count;  i++) {
        if ((pointer = tlsdata->tlskey_data[i]) != NULL) {
            tlsdata->tlskey_data[i] = NULL;
            if (sky_tls_key_cleanups[i] &&
                sky_tls_key_cleanups[i] != sky_tls_key_tombstone)
            {
                sky_tls_key_cleanups[i](tlsdata->tlskey_data[i]);
            }
        }
    }
}


sky_tlskey_t
sky_tls_alloc(sky_free_t cleanup)
{
    int32_t new_sky_tls_key_count, old_sky_tls_key_count;

    do {
        if ((old_sky_tls_key_count = sky_tls_key_count) >= 255) {
            int32_t tlskey;

            for (tlskey = 1;  tlskey <= sky_tls_key_count;  ++tlskey) {
                if (sky_tls_key_cleanups[tlskey] == sky_tls_key_tombstone &&
                    sky_atomic_cas(
                            sky_tls_key_tombstone,
                            cleanup,
                            SKY_AS_VOIDP(&(sky_tls_key_cleanups[tlskey]))))
                {
                    return tlskey;
                }
            }
            return 0;
        }
        new_sky_tls_key_count = old_sky_tls_key_count + 1;
    } while (!sky_atomic_cas32(old_sky_tls_key_count,
                               new_sky_tls_key_count,
                               &sky_tls_key_count));

    sky_tls_key_cleanups[new_sky_tls_key_count] = cleanup;
    return new_sky_tls_key_count;
}


void
sky_tls_free(sky_tlskey_t tlskey)
{
    uint32_t                id;
    sky_tlsdata_t           *tlsdata;
    sky_hazard_pointer_t    *hazard;

    sky_error_validate_debug(tlskey > 0 && tlskey <= sky_tls_key_count);
    sky_error_validate_debug(sky_tls_key_cleanups[tlskey] != sky_tls_key_tombstone);

    /* Before stubbing out the cleanup function pointer with a tombstone to
     * mark the key as free, clear out all of the existing data pointers for
     * each thread. This must be done now; otherwise, if the key is later
     * re-allocated, sky_tls_get() may return stale data pointers. This is
     * somewhat expensive to do, but it shouldn't matter, because tls key
     * deallocations should be extremely rare.
     */
    for (id = 1;  id < sky_tlsdata_highestid();  ++id) {
        if ((hazard = sky_tlsdata_getid(id)) != NULL &&
            (tlsdata = hazard->pointer) != NULL &&
            (tlsdata->flags & SKY_TLSDATA_FLAG_ALIVE))
        {
            tlsdata->tls_tlsdata.tlskey_data[tlskey - 1] = NULL;
        }
    }

    sky_tls_key_cleanups[tlskey] = sky_tls_key_tombstone;
}


void *
sky_tls_get(sky_tlskey_t tlskey)
{
    sky_tls_tlsdata_t   *tlsdata = &(sky_tlsdata_get()->tls_tlsdata);

    sky_error_validate_debug(tlskey > 0 && tlskey <= sky_tls_key_count);
    return tlsdata->tlskey_data[tlskey - 1];
}


sky_bool_t
sky_tls_set(sky_tlskey_t tlskey, void *pointer)
{
    sky_tls_tlsdata_t   *tlsdata = &(sky_tlsdata_get()->tls_tlsdata);

    sky_error_validate(tlskey > 0 && tlskey <= sky_tls_key_count);
    tlsdata->tlskey_data[tlskey - 1] = pointer;

    return SKY_TRUE;
}
