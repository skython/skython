#ifndef __SKYTHON_CORE_SKY_BYTES_PRIVATE_H__
#define __SKYTHON_CORE_SKY_BYTES_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_bytestring.h"


SKY_CDECLS_BEGIN


#define SKY_BYTES_SINGLE(_b) \
        (sky_object_t)((uintptr_t)0x17 | ((uintptr_t)(_b) << 8))


typedef struct sky_bytes_data_s {
    sky_bytestring_t                    bytes;
    uintptr_t                           hash;
    sky_bool_t                          hashed;
} sky_bytes_data_t;

SKY_EXTERN_INLINE sky_bytes_data_t *
sky_bytes_data(sky_object_t object)
{
    if (sky_bytes_type == sky_object_type(object)) {
        return (sky_bytes_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_bytes_type);
}

struct sky_bytes_s {
    sky_object_data_t                   object_data;
    sky_bytes_data_t                    bytes_data;
};


extern sky_bytestring_t *
sky_bytes_bytes(sky_object_t object, sky_bytestring_t *tagged_bytes);

extern sky_bytes_t
sky_bytes_createfrombytestring(const sky_bytestring_t *bytestring);

extern sky_bytes_t
sky_bytes_createwithbytestring(sky_bytestring_t *bytestring);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BYTES_PRIVATE_H__ */
