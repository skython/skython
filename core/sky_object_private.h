#ifndef __SKYTHON_CORE_SKY_OBJECT_PRIVATE_H__
#define __SKYTHON_CORE_SKY_OBJECT_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_hashtable.h"
#include "sky_pointer_set.h"


SKY_CDECLS_BEGIN


extern struct sky_type_s sky_base_object_type_struct;


typedef struct sky_object_reference_s sky_object_reference_t;
struct sky_object_reference_s {
    sky_object_reference_t *            next;
    sky_object_t *                      ref;
    sky_object_weakref_callback_t       callback;
    sky_object_t                        arg;
};


typedef struct sky_object_data_s {
    /* WARNING !! THIS MUST BE THE FIRST FIELD IN THIS STRUCT.
     * A pointer to the object's type, which may never be NULL.
     * For efficiency and data privacy reasons, various bits of code in a
     * variety of locations assume that the type is at offset 0 of an object
     * pointer's memory layout. This is assumed mostly for inlines in public
     * headers, but it can also be a convenient assumption elsewhere.
     */
    union {
        sky_type_t                      type;
        sky_free_t                      deallocate;
    } u;

    /* For garbage collection, gc_link is used to link the object to the next
     * object in whatever list to which it currently belongs.
     */
    sky_object_t                        gc_link;

    /* For garbage collection, gc_epoch is the current collection epoch. */
    uint32_t                            gc_epoch;

    /* Global means that the object is visible to more than just the thread
     * that created it. Once an object becomes global, it can never go back.
     */
    uint32_t                            global      : 1;
    /* Concrete means that the object has lasting references. Importantly,
     * it means that the object is linked for gc into a different list than the
     * one that's for ephemerals, which is the default when an object is first
     * instantiated. All ephemeral objects may be collected for a thread in any
     * call to sky_object_gc(). All global objects must also be concrete.
     */
    uint32_t                            concrete    : 1;
    /* Dead means that the object is dead and to be finalized and swept. It's
     * always set when __del__() is running, but resurrection could clear it.
     * It's used primarily to detect resurrection.
     */
    uint32_t                            dead        : 1;
    /* Finalized means that the object's finalizers and __del__() method have
     * been run, and that it is ready for deallocation. It is not possible to
     * resurrect the object at this point. Any attempt to do so shouldn't be
     * possible, but will result in a fatal error if it is caught. When this
     * flag is set, the 'type' field is no longer valid as a type. Instead, the
     * 'deallocate' field is the deallocation function from its original type.
     * This is done because the type and object could be swept in the same
     * pass, and the type could get deallocated before the object.
     */
    uint32_t                            finalized   : 1;
    /** Deleted means that the object's __del__() method has been run. This is
      * used primarily in the case of object resurrection to prevent the method
      * from being run again. Per PEP 442, the __del__() method should only be
      * run once.
      */
    uint32_t                            deleted     : 1;
    /** Finalizing means that the object's finalizer is running. In a release
      * build, this does nothing, but it is used in a debug build to try to
      * catch object resurrection from a finalizer.
      */
    uint32_t                            finalizing  : 1;
    /** Set by sky_object_gc_setlibraryroot() for an object that's been marked
      * as a library root in order to speed up object finalization passes
      * during GC. Once set for an object, it is never cleared.
      */
    uint32_t                            libraryroot : 1;

    /* References, generally weak. When the object is finalized, all of the
     * references in this list will be cleared.
     */
    sky_object_reference_t *            refs;

    /* On a 64-bit platform, the above totals to 32 bytes, which leaves 32
     * bytes available to keep from overflowing into the next cache-line, which
     * is 64 bytes on Intel x86, and probably other architectures as well, but
     * definitely never less than 64 bytes on any architecture that we care
     * about.
     */
} sky_object_data_t;


#define SKY_OBJECT_DATA(object)             ((sky_object_data_t *)(object))

#define SKY_OBJECT_METHOD_SLOT(_o, _name)   \
        sky_type_method_slot(sky_object_type(_o), SKY_TYPE_METHOD_SLOT_##_name)

#define SKY_OBJECT_DATA_INITIALIZER(_type)  { { _type }, NULL, 2,   \
                                              0, 0, 0, 0, 0, 0, 0,  \
                                              NULL }

#define SKY_OBJECT_INITIALIZE(_object, _type)                       \
    do {                                                            \
        sky_object_data_t   *__odata = SKY_OBJECT_DATA(_object);    \
                                                                    \
        __odata->u.type = _type;                                    \
        __odata->gc_link = NULL;                                    \
        __odata->gc_epoch = 2;                                      \
        __odata->global = 0;                                        \
        __odata->concrete = 0;                                      \
        __odata->dead = 0;                                          \
        __odata->finalized = 0;                                     \
        __odata->deleted = 0;                                       \
        __odata->finalizing = 0;                                    \
        __odata->libraryroot = 0;                                   \
        __odata->refs = NULL;                                       \
    } while (0)


SKY_EXTERN void sky_object_call_enter(void);
SKY_EXTERN void sky_object_call_leave(void *pointer);


#define SKY_OBJECT_CALL_NATIVE(_retval, _cfunction, _params)        \
    SKY_ASSET_BLOCK_BEGIN {                                         \
        sky_object_call_enter();                                    \
        _retval = _cfunction _params;                               \
    } SKY_ASSET_BLOCK_END

#define SKY_OBJECT_CALL_NATIVE_VOID(_cfunction, _params)            \
    SKY_ASSET_BLOCK_BEGIN {                                         \
        sky_object_call_enter();                                    \
        _cfunction _params;                                         \
    } SKY_ASSET_BLOCK_END


SKY_EXTERN_INLINE sky_bool_t
sky_object_istagged(sky_object_t object)
{
    return (((uintptr_t)object & 0xF) ? SKY_TRUE : SKY_FALSE);
}

SKY_EXTERN_INLINE sky_bool_t
sky_object_istaggedtuple(sky_object_t object)
{
    return (((uintptr_t)object & 0x8) ? SKY_TRUE : SKY_FALSE);
}

SKY_EXTERN_INLINE sky_object_t
sky_object_taggedtupleobject(sky_object_t object)
{
    return (sky_object_t)((uintptr_t)object & ~(uintptr_t)0x8);
}


#define SKY_OBJECT_MAKETAG(tag, extra)                          \
        (sky_object_t)(((uintptr_t)extra << 4) | ((tag) & 0xF))
#define SKY_OBJECT_TAG(object)                                  \
        ((uintptr_t)object & 0x7)


#define SKY_OBJECT_TAGGED_NONE                  SKY_OBJECT_MAKETAG(7, 0x000)
#define SKY_OBJECT_TAGGED_ELLIPSIS              SKY_OBJECT_MAKETAG(7, 0x010)
#define SKY_OBJECT_TAGGED_NOTIMPLEMENTED        SKY_OBJECT_MAKETAG(7, 0x020)
#define SKY_OBJECT_TAGGED_NOTSPECIFIED          SKY_OBJECT_MAKETAG(7, 0x030)
#define SKY_OBJECT_TAGGED_FALSE                 SKY_OBJECT_MAKETAG(7, 0x040)
#define SKY_OBJECT_TAGGED_TRUE                  SKY_OBJECT_MAKETAG(7, 0x050)
#define SKY_OBJECT_TAGGED_BYTES_EMPTY           SKY_OBJECT_MAKETAG(7, 0x060)
#define SKY_OBJECT_TAGGED_STRING_EMPTY          SKY_OBJECT_MAKETAG(7, 0x070)
#define SKY_OBJECT_TAGGED_TUPLE_EMPTY           SKY_OBJECT_MAKETAG(7, 0x080)
#define SKY_OBJECT_TAGGED_FROZENSET_EMPTY       SKY_OBJECT_MAKETAG(7, 0x090)

#define SKY_OBJECT_TAGGED_FLOAT_NAN             SKY_OBJECT_MAKETAG(7, 0x100)
#define SKY_OBJECT_TAGGED_FLOAT_INF             SKY_OBJECT_MAKETAG(7, 0x110)
#define SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_INF    SKY_OBJECT_MAKETAG(7, 0x120)
#define SKY_OBJECT_TAGGED_FLOAT_ONE             SKY_OBJECT_MAKETAG(7, 0x130)
#define SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ONE    SKY_OBJECT_MAKETAG(7, 0x140)
#define SKY_OBJECT_TAGGED_FLOAT_ZERO            SKY_OBJECT_MAKETAG(7, 0x150)
#define SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ZERO   SKY_OBJECT_MAKETAG(7, 0x160)

#define SKY_OBJECT_TAGGED_UINT32_MAX            SKY_OBJECT_MAKETAG(7, 0x200)
#define SKY_OBJECT_TAGGED_INT32_MIN             SKY_OBJECT_MAKETAG(7, 0x210)
#define SKY_OBJECT_TAGGED_INT32_MAX             SKY_OBJECT_MAKETAG(7, 0x220)
#define SKY_OBJECT_TAGGED_UINT64_MAX            SKY_OBJECT_MAKETAG(7, 0x230)
#define SKY_OBJECT_TAGGED_INT64_MIN             SKY_OBJECT_MAKETAG(7, 0x240)
#define SKY_OBJECT_TAGGED_INT64_MAX             SKY_OBJECT_MAKETAG(7, 0x250)


typedef struct sky_object_atfork_item_s sky_object_atfork_item_t;
struct sky_object_atfork_item_s {
    sky_hashtable_item_t                base;
    sky_object_t                        object;
    sky_object_atfork_prepare_t         prepare;
    sky_object_atfork_parent_t          parent;
    sky_object_atfork_child_t           child;
    sky_object_atfork_item_t *          next;
    sky_object_atfork_item_t *          prev;
};

typedef struct sky_object_atfork_table_s {
    sky_hashtable_t                     base;
    sky_object_atfork_item_t *          first;
    sky_object_atfork_item_t *          last;
} sky_object_atfork_table_t;


typedef struct sky_object_map_s sky_object_map_t;
struct sky_object_map_s {
    sky_hashtable_t                     table;
    sky_object_map_t *                  next;
};

typedef struct sky_object_map_item_s {
    sky_hashtable_item_t                base;
    sky_object_t                        object;         /* WEAK */
    intmax_t                            counter;
} sky_object_map_item_t;


typedef enum sky_object_gc_mode_e {
    /* No garbage collection cycle is in progress. */
    SKY_OBJECT_GC_MODE_IDLE         =   0,

    /* A new garbage collection cycle has been requested, but it hasn't been
     * started yet.
     */
    SKY_OBJECT_GC_MODE_REQUESTED    =   1,

    /* A new garbage collection cycle is starting. All mutator threads are
     * required to check-in before the the cycle can actually run.
     */
    SKY_OBJECT_GC_MODE_STARTING     =   2,

    /* A garbage collection cycle is in progress. Marking is in progress by the
     * marker thread. Mutator thread write barriers are building store sets.
     */
    SKY_OBJECT_GC_MODE_MARKING      =   3,

    /* A garbage collection cycle is in progress. Marking has completed. All
     * mutator threads must check-in and deliver their store sets to the marker
     * thread before sweeping can begin.
     */
    SKY_OBJECT_GC_MODE_MARK_ENDING  =   4,

    /* A garbage collection cycle is in progress. Mutator threads are sweeping
     * while the marker thread acts to synchronize the sweep phases among the
     * threads.
     */
    SKY_OBJECT_GC_MODE_SWEEP_1      =   5,
    SKY_OBJECT_GC_MODE_SWEEP_2      =   6,
    SKY_OBJECT_GC_MODE_SWEEP_3      =   7,
    SKY_OBJECT_GC_MODE_SWEEP_4      =   8,
    SKY_OBJECT_GC_MODE_SWEEP_5      =   9,
    SKY_OBJECT_GC_MODE_SWEEP_6      =   10,
} sky_object_gc_mode_t;


typedef struct sky_object_list_s {
    sky_object_t                        head;
    sky_object_t                        tail;
} sky_object_list_t;

static inline void
sky_object_list_empty(sky_object_list_t *list)
{
    list->head = list->tail = NULL;
}

static inline void
sky_object_list_prepend(sky_object_list_t *list, sky_object_t object)
{
    if (!(SKY_OBJECT_DATA(object)->gc_link = list->head)) {
        list->tail = object;
    }
    list->head = object;
}

static inline void
sky_object_list_extend(sky_object_list_t *list, sky_object_list_t *other)
{
    if (other->head) {
        if (!list->head) {
            list->head = other->head;
            list->tail = other->tail;
        }
        else {
            SKY_OBJECT_DATA(list->tail)->gc_link = other->head;
            list->tail = other->tail;
        }
        other->head = other->tail = NULL;
    }
}


/* Keep this structured so that the most frequently used fields are stored
 * at the head of the structure in an effort to keep them in the primary cache
 * line of the outer sky_tlsdata_t structure. This structure begins at offset
 * 24, meaning that the first 40 bytes of it will reside in the main cache
 * line.
 */
typedef struct sky_object_tlsdata_s {
    uint32_t                            gc_epoch        : 29;
    /* Indicates that this thread is the marker thread. */
    uint32_t                            gc_marker       : 1;
    /* Indicates that this thread has registered itself with the garbage
     * collector.
     */
    uint32_t                            gc_registered   : 1;
    /* Indicates that the thread believes a garbage collection cycle is in
     * progress. Once a mutator sets this flag for itself, it means it has
     * adjusted the wait count to let the marker thread know it's ackknowledged
     * the cycle start request. This flag is only set during the marking phase
     * of a garbage collection cycle.
     */
    uint32_t                            gc_running      : 1;

    uint32_t                            gc_alloc_count;

#if defined(SKY_EPHEMERAL_OBJECTS)
    /* Ephemeral object tracking */
    sky_object_list_t                   gc_ephemeral_objects;
#endif

    /* Concrete object tracking */
    sky_object_list_t                   gc_concrete_objects;
    sky_object_list_t                   gc_concrete_sweep;
    sky_pointer_set_t *                 gc_concrete_weakref_set;
    sky_object_list_t                   gc_concrete_dying;
    sky_object_list_t                   gc_concrete_del;
    sky_object_list_t                   gc_concrete_finalize;
    sky_object_list_t                   gc_concrete_deallocate;

    sky_object_map_t *                  gc_roots;
    sky_pointer_set_t *                 gc_stores;

    sky_object_gc_wakeup_t volatile     gc_wakeup_function;
    void * volatile                     gc_wakeup_arg;

    sky_object_atfork_table_t *         atfork_table;

    sky_object_gc_mode_t                gc_mode;
    uint32_t                            gc_sweep_5_done : 1;
    uint32_t                            gc_active       : 1;

    /* used by sky_object_stack_push() and sky_object_stack_pop(), which
     * are intended to be used to guard against recursion when generating
     * literal and string representations for objects.
     */
    sky_list_t                          object_stack;
} sky_object_tlsdata_t;


typedef struct sky_object_tlsdata_orphan_s sky_object_tlsdata_orphan_t;
struct sky_object_tlsdata_orphan_s {
    sky_object_list_t                   gc_concrete_objects;
    sky_object_list_t                   gc_concrete_sweep;
    sky_pointer_set_t *                 gc_concrete_weakref_set;
    sky_object_list_t                   gc_concrete_dying;
    sky_object_list_t                   gc_concrete_del;
    sky_object_list_t                   gc_concrete_finalize;
    sky_object_list_t                   gc_concrete_deallocate;

    sky_object_atfork_table_t *         atfork_table;

    sky_object_gc_mode_t                gc_mode;
};


extern void sky_object_atfork_child(sky_object_tlsdata_t *tlsdata);
extern void sky_object_atfork_parent(sky_object_tlsdata_t *tlsdata);
extern void sky_object_atfork_prepare(sky_object_tlsdata_t *tlsdata);

extern void
sky_object_tlsdata_adopt(sky_object_tlsdata_t *tlsdata,
                         sky_object_tlsdata_orphan_t *orphan_tlsdata);

extern void
sky_object_tlsdata_cleanup(sky_object_tlsdata_t *tlsdata,
                           sky_object_tlsdata_orphan_t *orphan_tlsdata,
                           sky_bool_t final);

extern void sky_object_del(sky_object_t object);
extern void sky_object_finalize(sky_object_t object);
extern sky_bool_t sky_object_hasrefs(sky_object_t object);


extern void sky_object_gc_finalize_library(void);
extern void sky_object_gc_initialize_library(void);

extern void
sky_object_gc_finalize_thread(sky_object_tlsdata_t *        tlsdata,
                              sky_object_tlsdata_orphan_t * orphan_tlsdata,
                              sky_bool_t                    final);

extern void
sky_object_gc_initialize_thread(sky_object_tlsdata_t *tlsdata);

extern void
sky_object_gc_mark(sky_object_t object, uint32_t mark);

extern void
sky_object_gc_notenew(sky_object_tlsdata_t *tlsdata, sky_object_t object);

extern void
sky_object_gc_wakeup(void);

extern void
sky_object_gc_atfork_child(sky_object_tlsdata_t *tlsdata);


/* The object map interface is used for tracking root object references for
 * the garbage collector. It is a special case implementation of sky_dict.
 */

extern void sky_object_map_combine(sky_object_map_t * restrict map,
                                   sky_object_map_t * restrict other_map);
extern sky_object_map_t *sky_object_map_create(void);
extern sky_bool_t sky_object_map_contains(sky_object_map_t *map,
                                          sky_object_t object);
extern void sky_object_map_destroy(sky_object_map_t *map);
extern void sky_object_map_mark(sky_object_map_t *map, uint32_t mark);
extern void sky_object_map_update(sky_object_map_t *map,
                                  sky_object_t object,
                                  intmax_t delta);


SKY_EXTERN_INLINE size_t
sky_object_map_length(sky_object_map_t *map)
{
    return (map ? sky_hashtable_length(&(map->table)) : 0);
}


SKY_EXTERN_INLINE void
sky_object_map_insert(sky_object_map_t *map, sky_object_t object)
{
    sky_object_map_update(map, object, 1);
}

SKY_EXTERN_INLINE void
sky_object_map_remove(sky_object_map_t *map,
                      sky_object_t object)
{
    sky_object_map_update(map, object, -1);
}


/* Defined in sky_weakproxy.c and sky_weakref.c, these are the callback
 * functions that are used to clear weakrefs. They're needed by the _weakref
 * module to find weakproxy and weakref objects.
 */
extern void sky_weakproxy_callback(sky_object_t);
extern void sky_weakref_callback(sky_object_t);

/* Type structs for built-in static types that need early initialization. */
extern struct sky_type_s sky_base_object_type_struct;
extern struct sky_type_s sky_boolean_type_struct;
extern struct sky_type_s sky_bytearray_type_struct;
extern struct sky_type_s sky_bytes_type_struct;
extern struct sky_type_s sky_bytes_builder_type_struct;
extern struct sky_type_s sky_capsule_type_struct;
extern struct sky_type_s sky_classmethod_type_struct;
extern struct sky_type_s sky_complex_type_struct;
extern struct sky_type_s sky_dict_type_struct;
extern struct sky_type_s sky_float_type_struct;
extern struct sky_type_s sky_function_type_struct;
extern struct sky_type_s sky_getset_type_struct;
extern struct sky_type_s sky_integer_type_struct;
extern struct sky_type_s sky_list_type_struct;
extern struct sky_type_s sky_mappingproxy_type_struct;
extern struct sky_type_s sky_member_type_struct;
extern struct sky_type_s sky_method_type_struct;
extern struct sky_type_s sky_native_code_type_struct;
extern struct sky_type_s sky_parameter_list_type_struct;
extern struct sky_type_s sky_set_type_struct;
extern struct sky_type_s sky_staticmethod_type_struct;
extern struct sky_type_s sky_string_type_struct;
extern struct sky_type_s sky_string_builder_type_struct;
extern struct sky_type_s sky_string_cache_type_struct;
extern struct sky_type_s sky_tuple_type_struct;
extern struct sky_type_s sky_type_type_struct;
extern struct sky_type_s sky_type_dict_type_struct;

extern struct sky_type_s sky_Ellipsis_type_struct;
extern struct sky_type_s sky_None_type_struct;
extern struct sky_type_s sky_NotImplemented_type_struct;
extern struct sky_type_s sky_NotSpecified_type_struct;


/* Library initialization and finalization for various built-in types. */
extern void sky_base_object_initialize_library(void);
extern void sky_boolean_initialize_library(void);
extern void sky_bytearray_initialize_library(void);
extern void sky_bytes_initialize_library(void);
extern void sky_bytes_builder_initialize_library(void);
extern void sky_callable_iterator_initialize_library(void);
extern void sky_capsule_initialize_library(void);
extern void sky_cell_initialize_library(void);
extern void sky_classmethod_initialize_library(void);
extern void sky_code_initialize_library(void);
extern void sky_complex_initialize_library(void);
extern void sky_deque_initialize_library(void);
extern void sky_dict_initialize_library(void);
extern void sky_Ellipsis_initialize_library(void);
extern void sky_enumerate_initialize_library(void);
extern void sky_exceptions_initialize_library(void);
extern void sky_filter_initialize_library(void);
extern void sky_float_initialize_library(void);
extern void sky_frame_initialize_library(void);
extern void sky_frozenset_initialize_library(void);
extern void sky_function_initialize_library(void);
extern void sky_generator_initialize_library(void);
extern void sky_getset_initialize_library(void);
extern void sky_integer_initialize_library(void);
extern void sky_iterator_initialize_library(void);
extern void sky_list_initialize_library(void);
extern void sky_map_initialize_library(void);
extern void sky_mappingproxy_initialize_library(void);
extern void sky_member_initialize_library(void);
extern void sky_memoryview_initialize_library(void);
extern void sky_module_initialize_library(void);
extern void sky_method_initialize_library(void);
extern void sky_mutex_initialize_library(void);
extern void sky_namespace_initialize_library(void);
extern void sky_native_code_initialize_library(void);
extern void sky_None_initialize_library(void);
extern void sky_NotImplemented_initialize_library(void);
extern void sky_NotSpecified_initialize_library(void);
extern void sky_parameter_list_initialize_library(void);
extern void sky_property_initialize_library(void);
extern void sky_range_initialize_library(void);
extern void sky_reversed_initialize_library(void);
extern void sky_set_initialize_library(void);
extern void sky_slice_initialize_library(void);
extern void sky_socket_initialize_library(void);
extern void sky_staticmethod_initialize_library(void);
extern void sky_string_initialize_library(void);
extern void sky_string_builder_initialize_library(void);
extern void sky_struct_sequence_initialize_library(void);
extern void sky_super_initialize_library(void);
extern void sky_thread_initialize_library(void);
extern void sky_traceback_initialize_library(void);
extern void sky_tuple_initialize_library(void);
extern void sky_type_dict_initialize_library(void);
extern void sky_weakproxy_initialize_library(void);
extern void sky_weakref_initialize_library(void);
extern void sky_zip_initialize_library(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_OBJECT_PRIVATE_H__ */
