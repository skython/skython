#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"

#include "sky_cell.h"


typedef struct sky_super_data_s {
    sky_object_t                        self;
    sky_type_t                          self_class;
    sky_type_t                          thisclass;
} sky_super_data_t;

SKY_EXTERN_INLINE sky_super_data_t *
sky_super_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_super_type) {
        return (sky_super_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_super_type);
}

struct sky_super_s {
    sky_object_data_t                   object_data;
    sky_super_data_t                    super_data;
};


static void
sky_super_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_super_data_t    *self_data = data;

    sky_object_visit(self_data->self, visit_data);
    sky_object_visit(self_data->self_class, visit_data);
    sky_object_visit(self_data->thisclass, visit_data);
}


static sky_type_t
sky_super_check(sky_type_t type, sky_object_t object)
    /* Check that a super() call makes sense. Return a type object.
     *
     * obj can be a class, or an instance of one:
     * - If it is a class, it must be a subclass of 'type'.
     *   This case is used for class methods; the return value is 'obj'.
     * - If it is an instance, it must be an instance of 'type'.
     *   This is the normal case; the return value is obj.__class__.
     *
     * But... when obj is an instance, we want to allow for the case where
     * sky_object_type(obj) is not a subclass of type, but obj.__class__ is!
     * This will allow using super() with a proxy for obj.
     */
{
    sky_object_t    attr;

    if (sky_object_isa(object, sky_type_type) &&
        sky_type_issubtype(object, type))
    {
        return object;
    }

    if (sky_type_issubtype(sky_object_type(object), type)) {
        return sky_object_type(object);
    }

    attr = sky_object_getattr(object,
                              SKY_STRING_LITERAL("__class__"),
                              NULL);
    if (attr &&
        sky_object_isa(attr, sky_type_type) &&
        attr != sky_object_type(object) &&
        sky_type_issubtype(attr, type))
    {
        return attr;
    }

    sky_error_raise_string(
            sky_TypeError,
            "super(type, obj): obj must be an instance or subtype of type");
}


sky_object_t
sky_super_get(           sky_super_t   self,
                         sky_object_t  instance,
              SKY_UNUSED sky_type_t    owner)
{
    sky_super_data_t    *self_data = sky_super_data(self);

    sky_type_t          self_class;
    sky_super_t         new_super;
    sky_super_data_t    *new_super_data;

    if (sky_object_isnull(instance) || sky_object_isnull(self_data->self)) {
        return self;
    }
    if (sky_object_type(self) != sky_super_type) {
        return sky_object_call(self,
                               sky_tuple_pack(2, self_data->thisclass,
                                                 instance),
                               NULL);
    }

    self_class = sky_super_check(self_data->thisclass, instance);

    new_super = sky_object_allocate(sky_super_type);
    new_super_data = sky_super_data(new_super);
    sky_object_gc_set(&(new_super_data->self), instance, new_super);
    sky_object_gc_set(SKY_AS_OBJECTP(&(new_super_data->self_class)),
                      self_class,
                      new_super);
    sky_object_gc_set(SKY_AS_OBJECTP(&(new_super_data->thisclass)),
                      sky_super_type,
                      new_super);

    return new_super;
}


sky_object_t
sky_super_getattribute(sky_super_t self, sky_string_t name)
{
    sky_super_data_t    *self_data = sky_super_data(self);

    ssize_t         i;
    sky_dict_t      dict;
    sky_object_t    instance, object;
    sky_type_data_t *type_data;
    sky_type_list_t *mro;

    if (!self_data->self_class ||
        sky_object_compare(name,
                           SKY_STRING_LITERAL("__class__"),
                           SKY_COMPARE_OP_EQUAL))
    {
        return sky_base_object_getattribute(self, name);
    }
    type_data = sky_type_data(self_data->self_class);

restart:
    mro = type_data->mro;
    for (i = 0; i < mro->len; ++i) {
        if (self_data->thisclass == mro->items[i].type) {
            break;
        }
    }

    while (++i < mro->len) {
        if (!sky_object_isa(mro->items[i].type, sky_type_type)) {
            continue;
        }
        dict = sky_object_dict(mro->items[i].type);

        SKY_ERROR_TRY {
            object = sky_object_getitem(dict, name);
        } SKY_ERROR_EXCEPT(sky_KeyError) {
            object = NULL;
        } SKY_ERROR_TRY_END;

        if (object) {
            instance = (self_data->self == self_data->self_class
                                         ? NULL
                                         : self_data->self);
            return sky_descriptor_get(object, instance, self_data->self_class);
        }

        /* The MRO can be changed during the call to sky_object_getitem(). */
        if (sky_type_data(self_data->self_class)->mro != mro) {
            goto restart;
        }
    }

    return sky_base_object_getattribute(self, name);
}


void
sky_super_init(sky_super_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_super_data_t    *self_data = sky_super_data(self);

    ssize_t             n;
    sky_type_t          object_type, type;
    sky_object_t        object;
    sky_tlsdata_t       *tlsdata;
    sky_code_data_t     *code_data;
    sky_frame_data_t    *frame_data;

    if (sky_object_bool(kws)) {
        sky_error_raise_string(sky_TypeError,
                               "super does not take keyword arguments");
    }
    if (!sky_object_bool(args)) {
        /* super() - use __class__ and the first local variable. */

        tlsdata = sky_tlsdata_get();
        frame_data = sky_frame_data(tlsdata->current_frame);
        code_data = sky_code_data(frame_data->f_code);

        if (!code_data) {
            sky_error_raise_string(sky_RuntimeError, "super(): no code object");
        }
        if (!code_data->co_argcount) {
            sky_error_raise_string(sky_RuntimeError, "super(): no arguments");
        }
        if (frame_data->locals) {
            object = frame_data->locals[0];
        }
        else {
            object = sky_object_getitem(
                            frame_data->locals,
                            sky_tuple_get(code_data->co_varnames, 0));
        }
        if (!object) {
            sky_error_raise_string(sky_RuntimeError, "super(): arg[0] deleted");
        }

        if ((n = sky_tuple_find(code_data->co_freevars,
                                SKY_STRING_LITERAL("__class__"))) == -1)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "super(): __class__ cell not found");
        }
        if (!sky_object_isa(frame_data->freevars[n], sky_cell_type)) {
            sky_error_raise_string(sky_RuntimeError,
                                   "super(): bad __class__ cell");
        }
        if (!(type = sky_cell_contents(frame_data->freevars[n]))) {
            sky_error_raise_string(sky_RuntimeError,
                                   "super(): empty __class__ cell");
        }
        if (!sky_object_isa(type, sky_type_type)) {
            sky_error_raise_format(sky_RuntimeError,
                                   "super(): __class__ is not a type (%@)",
                                   sky_type_name(type));
        }
    }
    else {
        type = sky_tuple_get(args, 0);
        if (!sky_object_isa(type, sky_type_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "super(): first argument must be a type");
        }
        if (sky_tuple_len(args) == 1) {
            object = NULL;
            object_type = NULL;
        }
        else {
            object = sky_tuple_get(args, 1);
            if (sky_tuple_len(args) > 2) {
                sky_error_raise_format(
                        sky_TypeError,
                        "super() takes at most 2 arguments (%zd given)",
                        sky_tuple_len(args));
            }
        }
    }

    if (sky_object_isnull(object)) {
        object = NULL;
    }
    else {
        object_type = sky_super_check(type, object);
    }

    sky_object_gc_set(&(self_data->self), object, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->self_class)),
                      object_type,
                      self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->thisclass)),
                      type,
                      self);
}


sky_string_t
sky_super_repr(sky_super_t self)
{
    sky_super_data_t    *self_data = sky_super_data(self);

    sky_string_t    name;

    name = (self_data->thisclass ? sky_type_name(self_data->thisclass)
                                 : SKY_STRING_LITERAL("NULL"));
    if (!self_data->self_class) {
        return sky_string_createfromformat("<super: <class %#@>>", name);
    }
    return sky_string_createfromformat("<super: <class %#@>, <%@ object>>",
                                       name,
                                       sky_type_name(self_data->self_class));
}


static const char sky_super_type_doc[] =
"super() -> same as super(__class__, <first argument>)\n"
"super(type) -> unbound super object\n"
"super(type, obj) -> bound super object; requires isinstance(obj, type)\n"
"super(type, type2) -> bound super object; requires issubclass(type2, type)\n"
"Typical use to call a cooperative superclass method:\n"
"class C(B):\n"
"    def meth(self, arg):\n"
"        super().meth(arg)\n"
"This works for class methods too:\n"
"class C(B):\n"
"    @classmethod\n"
"    def cmeth(cls, arg):\n"
"        super().cmeth(arg)\n";

SKY_TYPE_DEFINE_SIMPLE(super,
                       "super",
                       sizeof(sky_super_data_t),
                       NULL,
                       NULL,
                       sky_super_instance_visit,
                       0,
                       sky_super_type_doc);

void
sky_super_initialize_library(void)
{
    sky_type_initialize_builtin(sky_super_type, 0);
    sky_type_setmethodslots(sky_super_type,
            "__init__", sky_super_init,
            "__repr__", sky_super_repr,
            "__getattribute__", sky_super_getattribute,
            "__get__", sky_super_get,
            NULL);
    sky_type_setmembers(sky_super_type,
            "__self__",
                    "the instance invoking super(); may be None",
                    offsetof(sky_super_data_t, self),
                    SKY_DATA_TYPE_OBJECT,
                    SKY_MEMBER_FLAG_READONLY,
            "__self_class__",
                    "the type of the instance invoking super(); may be None",
                    offsetof(sky_super_data_t, self_class),
                    SKY_DATA_TYPE_OBJECT_TYPE,
                    SKY_MEMBER_FLAG_READONLY,
            "__thisclass__",
                    "the class invoking super()",
                    offsetof(sky_super_data_t, thisclass),
                    SKY_DATA_TYPE_OBJECT_TYPE,
                    SKY_MEMBER_FLAG_READONLY,
            NULL);
}
