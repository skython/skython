#include "../config.h"
#if !defined(__APPLE__) || \
    (!defined(__i386__) && !defined(__x86_64__)) || \
    !defined(HAVE_LIBUNWIND_H)
#   error this source file cannot be compiled in this configuration
#endif

/* libunwind - a platform-independent unwind library
   Copyright (C) 2003-2004 Hewlett-Packard Co
        Contributed by David Mosberger-Tang <davidm@hpl.hp.com>

This file is part of libunwind.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  */

#include "sky_error.h"

#include <libunwind.h>
#include <setjmp.h>

#define JB_RP           0
#define JB_SP           1
#if defined(__i386__)
#   define SKY_REG_AX   UNW_X86_EAX
#   define SKY_REG_DX   UNW_X86_EDX
#elif defined(__x86_64__)
#   define SKY_REG_AX   UNW_X86_64_RAX
#   define SKY_REG_DX   UNW_X86_64_RDX
#else
#   error implementation missing
#endif


void
sky_longjmp(jmp_buf env, int val)
{
    extern int  _UI_longjmp_cont;

    unw_word_t      sp, *wp;
    unw_cursor_t    c;
    unw_context_t   uc;

    /* Ignore the return value of unw_getcontext(), because Apple's
     * implementation of it returns garbage, at least for i386.  The first
     * instruction is "pushl %eax", and the last instruction before ret is
     * "popl %eax".  Judging by the lldb libunwind source that came from
     * Apple, this has been fixed at some point, but at least with OS X 10.6.5
     * it's still a problem.
     */
    unw_getcontext(&uc);
    if (unw_init_local(&c, &uc) < 0) {
        sky_error_fatal("internal error");
    }

    wp = (unw_word_t *)env;
    do {
        if (unw_get_reg(&c, UNW_REG_SP, &sp) < 0) {
            sky_error_fatal("internal error");
        }
        if (sp != wp[JB_SP]) {
            continue;
        }

#if 0   /* From the original source, this does nothing except on IA64, which
         * we are not supporting here; this code is Apple i386/x86_64 specific.
         */
        if (!bsp_match(&c, wp)) {
            continue;
        }
#endif

        /* found the right frame: */

        if (unw_set_reg(&c, SKY_REG_AX, wp[JB_RP]) < 0 ||
            unw_set_reg(&c, SKY_REG_DX, val) < 0 ||
            unw_set_reg(&c, UNW_REG_IP, (unw_word_t)(uintptr_t)&_UI_longjmp_cont))
        {
            sky_error_fatal("internal error");
        }

        unw_resume(&c);
        sky_error_fatal("internal error");
    } while (unw_step(&c) >= 0);

    sky_error_fatal("internal_error");
}
