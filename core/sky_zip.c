#include "sky_private.h"


typedef struct sky_zip_data_s {
    ssize_t                             len;
    sky_object_t *                      iterators;
} sky_zip_data_t;

SKY_EXTERN_INLINE sky_zip_data_t *
sky_zip_data(sky_object_t object)
{
    return sky_object_data(object, sky_zip_type);
}

struct sky_zip_s {
    sky_object_data_t                   object_data;
    sky_zip_data_t                      zip_data;
};


static void
sky_zip_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_zip_data_t  *self_data = data;

    sky_free(self_data->iterators);
}


static void
sky_zip_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_zip_data_t  *self_data = data;

    ssize_t i;

    for (i = 0; i < self_data->len; ++i) {
        sky_object_visit(self_data->iterators[i], visit_data);
    }
}


sky_object_t
sky_zip_new(sky_type_t cls, sky_tuple_t args, sky_dict_t kws)
{
    ssize_t         i, len;
    sky_object_t    *iterators, object, self;
    sky_zip_data_t  *self_data;

    if (sky_zip_type == cls && sky_object_bool(kws)) {
        sky_error_raise_string(sky_TypeError,
                               "zip() does not take keyword arguments");
    }

    self = sky_object_allocate(cls);
    self_data = sky_zip_data(self);

    if (sky_object_bool(args)) {
        SKY_ASSET_BLOCK_BEGIN {
            len = sky_tuple_len(args);
            iterators = sky_asset_calloc(len, sizeof(sky_object_t),
                                         SKY_ASSET_CLEANUP_ON_ERROR);

            i = 0;
            SKY_SEQUENCE_FOREACH(args, object) {
                SKY_ERROR_TRY {
                    sky_object_gc_set(&(iterators[i]),
                                      sky_object_iter(object),
                                      self);
                } SKY_ERROR_EXCEPT(sky_TypeError) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "zip argument #%zd must support iteration",
                            i + 1);
                } SKY_ERROR_TRY_END;
                ++i;
            } SKY_SEQUENCE_FOREACH_END;
        } SKY_ASSET_BLOCK_END;

        self_data->iterators = iterators;
        self_data->len = len;
    }

    return self;
}


sky_object_t
sky_zip_iter(sky_zip_t self)
{
    return self;
}


sky_object_t
sky_zip_next(sky_zip_t self)
{
    sky_zip_data_t  *self_data = sky_zip_data(self);

    ssize_t         i, len;
    sky_object_t    *objects, result;

    if (!(len = self_data->len)) {
        return NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_malloc(len * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < len; ++i) {
            objects[i] = sky_object_next(self_data->iterators[i], NULL);
            if (!objects[i]) {
                self_data->len = 0;
                sky_error_raise_object(sky_StopIteration, sky_None);
            }
        }

        result = sky_tuple_createwitharray(len, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_zip_reduce(sky_zip_t self)
{
    sky_zip_data_t  *self_data = sky_zip_data(self);

    ssize_t     len;
    sky_tuple_t iterators;

    if (!(len = self_data->len)) {
        iterators = sky_tuple_empty;
    }
    else {
        iterators = sky_tuple_createfromarray(len, self_data->iterators);
    }

    return sky_object_build("(OO)", sky_object_type(self), iterators);
}


sky_zip_t
sky_zip_create(sky_object_t iterable, ...)
{
    va_list     ap;
    sky_zip_t   zip;

    va_start(ap, iterable);
    zip = sky_zip_createv(iterable, ap);
    va_end(ap);

    return zip;
}


sky_zip_t
sky_zip_createv(sky_object_t iterable, va_list ap)
{
    sky_list_t      list;
    sky_tuple_t     tuple;
    sky_object_t    object;

    list = sky_list_create(NULL);
    sky_list_append(list, iterable);
    while ((object = va_arg(ap, sky_object_t)) != NULL) {
        sky_list_append(list, object);
    }
    tuple = sky_tuple_create(list);

    return sky_zip_new(sky_zip_type, tuple, NULL);
}


static const char sky_zip_type_doc[] =
"zip(iter1 [,iter2 [...]]) --> zip object\n"
"\n"
"Return a zip object whose .__next__() method returns a tuple where\n"
"the i-th element comes from the i-th iterable argument.  The .__next__()\n"
"method continues until the shortest iterable in the argument sequence\n"
"is exhausted and then it raises StopIteration.";


SKY_TYPE_DEFINE_SIMPLE(zip,
                       "zip",
                       sizeof(sky_zip_data_t),
                       NULL,
                       sky_zip_instance_finalize,
                       sky_zip_instance_visit,
                       0,
                       sky_zip_type_doc);


void
sky_zip_initialize_library(void)
{
    sky_type_initialize_builtin(sky_zip_type, 0);
    sky_type_setmethodslots(sky_zip_type,
            "__new__", sky_zip_new,
            "__iter__", sky_zip_iter,
            "__next__", sky_zip_next,
            "__reduce__", sky_zip_reduce,
            NULL);
}
