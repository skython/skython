/** @file
  * @brief Atomic LIFO
  * @defgroup sky_atomic Atomic Operations
  * @{
  * @defgroup sky_atomic_lifo Atomic LIFO
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ATOMIC_LIFO_H__
#define __SKYTHON_CORE_SKY_ATOMIC_LIFO_H__


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** An atomic LIFO.
  * @warning    This structure must be 16-byte aligned. If it is not properly
  *             aligned a hardware fault will occur.
  */
typedef SKY_ALIGNED(16) struct sky_atomic_lifo_s {
    /** pointer to the item at the head of the LIFO. */
    void * volatile                     pointer;

    /** tag used to prevent A-B-A problems. **/
    volatile intptr_t                   aba_tag;

    /** offset at which the link pointer resides within items pushed onto the
      * LIFO.
      */
    size_t                              link_offset;
} sky_atomic_lifo_t;


/** Constant expression to be used to statically initialize an atomic LIFO.
  *
  * @param[in]  offset      the offset at which the link pointer resides within
  *                         items pushed onto the LIFO. The argument must be a
  *                         constant expression.
  */
#define SKY_ATOMIC_LIFO_INITIALIZER(offset) { NULL, 0, (offset) }


/** Initialize an atomic LIFO for use.
  * Before an atomic LIFO may be used, it must be initialized. Initialization
  * may be done either statically using @c SKY_ATOMIC_LIFO_INITIALIZER(), or by
  * calling sky_atomic_lifo_init(). Initialization cannot fail.
  *
  * @param[in]  lifo        the LIFO to initialize.
  * @param[in]  link_offset the offset at which the link pointer resides within
  *                         items pushed onto the LIFO.
  */
SKY_EXTERN void
sky_atomic_lifo_init(sky_atomic_lifo_t *lifo, size_t link_offset);

/** Push an item onto an atomic LIFO.
  *
  * @param[in]  lifo        the LIFO onto which the item is to be pushed.
  * @param[in]  item        the item to push onto the LIFO.
  */
SKY_EXTERN void
sky_atomic_lifo_push(sky_atomic_lifo_t * restrict lifo, void * restrict item);

/** Pop an item from an atomic LIFO.
  *
  * @param[in]  lifo        the LIFO from which an item is to be popped.
  * @return     the item popped from the LIFO, or @c NULL if the LIFO is empty.
  */
SKY_EXTERN void *
sky_atomic_lifo_pop(sky_atomic_lifo_t *lifo);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ATOMIC_LIFO_H__ */

/** @} **/
/** @} **/
