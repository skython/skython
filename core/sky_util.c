#include "sky_private.h"

#include <math.h>

#include <locale.h>
#if defined(HAVE_XLOCALE_H)
#   include <xlocale.h>
#endif

#if defined(HAVE_STRINGS_H)
#   include <strings.h>
#endif
#if defined(HAVE_DIRENT_H)
#   include <dirent.h>
#endif

#if defined(_WIN32)
#   include <intrin.h>
#   include <shlobj.h>
#else
#   include <sys/stat.h>
#   if !defined(HAVE_FFS)
extern int ffs(unsigned int);
#   endif
#   if !defined(HAVE_FFSL)
extern int ffsl(unsigned long);
#   endif
#   if !defined(HAVE_FLS)
extern int fls(unsigned int);
#   endif
#   if !defined(HAVE_FLSL)
extern int flsl(unsigned long);
#   endif
#endif


uint32_t
sky_util_crc32(const void *bytes, size_t nbytes, uint32_t seed)
{
    static const uint32_t table[256] = {
        0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F,
        0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
        0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2,
        0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
        0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
        0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
        0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C,
        0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
        0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423,
        0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
        0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x01DB7106,
        0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
        0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D,
        0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
        0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
        0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
        0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7,
        0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
        0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA,
        0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
        0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81,
        0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
        0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84,
        0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
        0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
        0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
        0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E,
        0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
        0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55,
        0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
        0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28,
        0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
        0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F,
        0x72076785, 0x05005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
        0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
        0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
        0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69,
        0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
        0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC,
        0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
        0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693,
        0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
        0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
    };

    uint32_t        crc32;
    const uint8_t   *b, *end;

    crc32 = seed ^ 0xFFFFFFFF;
    for (end = (b = bytes) + nbytes;  b < end;  ++b) {
        crc32 = (crc32 >> 8) ^ table[(crc32 ^ *b) & 0xFF];
    }

    return crc32 ^ 0xFFFFFFFF;
}


int
sky_util_ffs(unsigned int value)
{
#if defined(_WIN32)
    unsigned long   Index;

    return (_BitScanForward(&Index, value) ? Index + 1 : 0);
#else
    return ffs(value);
#endif
}


int
sky_util_ffsl(size_t value)
{
#if defined(_WIN32)
    unsigned long   Index;

#   if !defined(_WIN64)
    return (_BitScanForward(&Index, value) ? Index + 1 : 0);
#   else
    return (_BitScanForward64(&Index, value) ? Index + 1 : 0);
#   endif
#else
    return ffsl(value);
#endif
}


int
sky_util_fls(unsigned int value)
{
#if defined(_WIN32)
    unsigned long   Index;

    return (_BitScanReverse(&Index, value) ? Index + 1 : 0);
#else
    return fls(value);
#endif
}


int
sky_util_flsl(size_t value)
{
#if defined(_WIN32)
    unsigned long   Index;

#   if !defined(_WIN64)
    return (_BitScanReverse(&Index, value) ? Index + 1 : 0);
#   else
    return (_BitScanReverse64(&Index, value) ? Index + 1 : 0);
#   endif
#else
    return flsl(value);
#endif
}


/* The hash algorithm employed by the various hashing functions here is
 * MurmurHash by Austin Appleby.
 * http://sites.google.com/site/murmurhash
 */


/* Intel processors allow unaligned memory access, but it'll slow things down
 * a bit, and the reads will not be atomic. Is it faster to let the processor
 * do the unaligned reads, or is it faster to handle it in software?
 */
#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
#   define sky_util_hash_extract32(data)    *(uint32_t *)data
#   define sky_util_hash_extract64(data)    *(uint64_t *)data
#else
#   error implementation missing
#endif


uint32_t
sky_util_hashbytes32(const void *bytes, size_t nbytes, uint32_t seed)
{
    const int       r = 24;
    const uint32_t  m = 0x5bd1e995;

    const uint8_t   *data;
    uint32_t        h, k;

    h = seed ^ (uint32_t)nbytes;
    data = (const uint8_t *)bytes;

    while (nbytes >= 4) {
        k = sky_endian_htol32(*(uint32_t *)data);

        k *= m;
        k ^= k >> r;
        k *= m;

        h *= m;
        h ^= k;

        data += 4;
        nbytes -= 4;
    }

    switch (nbytes) {
        case 3:
            h ^= (uint32_t)data[2] << 16;
        case 2:
            h ^= (uint32_t)data[1] << 8;
        case 1:
            h ^= (uint32_t)data[0];
            h *= m;
    }

    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;

    return h;
}


uint32_t
sky_util_hashbytes32_nocase(const void *bytes, size_t nbytes, uint32_t seed)
{
    const int       r = 24;
    const uint32_t  m = 0x5bd1e995;

    const uint8_t   *data;
    uint32_t        h, k;

    h = seed ^ (uint32_t)nbytes;
    data = (const uint8_t *)bytes;

    while (nbytes >= 4) {
        k  = (uint32_t)sky_ctype_tolower(data[0]);
        k |= (uint32_t)sky_ctype_tolower(data[1]) << 8;
        k |= (uint32_t)sky_ctype_tolower(data[2]) << 16;
        k |= (uint32_t)sky_ctype_tolower(data[3]) << 24;

        k *= m;
        k ^= k >> r;
        k *= m;

        h *= m;
        h ^= k;

        data += 4;
        nbytes -= 4;
    }

    switch (nbytes) {
        case 3:
            h ^= (uint32_t)sky_ctype_tolower(data[2]) << 16;
        case 2:
            h ^= (uint32_t)sky_ctype_tolower(data[1]) << 8;
        case 1:
            h ^= (uint32_t)sky_ctype_tolower(data[0]);
            h *= m;
    }

    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;

    return h;
}


uint64_t
sky_util_hashbytes64(const void *bytes, size_t nbytes, uint64_t seed)
{
    const int       r = 47;
    const uint64_t  m = 0xc6a4a7935bd1e995ull;

    const uint8_t   *data;
    uint64_t        h, k;

    h = seed ^ ((uint64_t)nbytes * m);
    data = (const uint8_t *)bytes;

    while (nbytes >= 8) {
        k = sky_endian_htol64(*(uint64_t *)data);

        k *= m;
        k ^= k >> r;
        k *= m;

        h ^= k;
        h *= m;

        data += 8;
        nbytes -= 8;
    }

    switch (nbytes) {
        case 7:
            h ^= (uint64_t)data[6] << 48;
        case 6:
            h ^= (uint64_t)data[5] << 40;
        case 5:
            h ^= (uint64_t)data[4] << 32;
        case 4:
            h ^= (uint64_t)data[3] << 24;
        case 3:
            h ^= (uint64_t)data[2] << 16;
        case 2:
            h ^= (uint64_t)data[1] << 8;
        case 1:
            h ^= (uint64_t)data[0];
            h *= m;
    }

    h ^= h >> r;
    h *= m;
    h ^= h >> r;

    return h;
}


uint64_t
sky_util_hashbytes64_nocase(const void *bytes, size_t nbytes, uint64_t seed)
{
    const int       r = 47;
    const uint64_t  m = 0xc6a4a7935bd1e995ull;

    const uint8_t   *data;
    uint64_t        h, k;

    h = seed ^ ((uint64_t)nbytes * m);
    data = (const uint8_t *)bytes;

    while (nbytes >= 8) {
        k  = (uint64_t)sky_ctype_tolower(data[0]);
        k |= (uint64_t)sky_ctype_tolower(data[1]) << 8;
        k |= (uint64_t)sky_ctype_tolower(data[2]) << 16;
        k |= (uint64_t)sky_ctype_tolower(data[3]) << 24;
        k |= (uint64_t)sky_ctype_tolower(data[4]) << 32;
        k |= (uint64_t)sky_ctype_tolower(data[5]) << 40;
        k |= (uint64_t)sky_ctype_tolower(data[6]) << 48;
        k |= (uint64_t)sky_ctype_tolower(data[7]) << 56;

        k *= m;
        k ^= k >> r;
        k *= m;

        h ^= k;
        h *= m;

        data += 8;
        nbytes -= 8;
    }

    switch (nbytes) {
        case 7:
            h ^= (uint64_t)sky_ctype_tolower(data[6]) << 48;
        case 6:
            h ^= (uint64_t)sky_ctype_tolower(data[5]) << 40;
        case 5:
            h ^= (uint64_t)sky_ctype_tolower(data[4]) << 32;
        case 4:
            h ^= (uint64_t)sky_ctype_tolower(data[3]) << 24;
        case 3:
            h ^= (uint64_t)sky_ctype_tolower(data[2]) << 16;
        case 2:
            h ^= (uint64_t)sky_ctype_tolower(data[1]) << 8;
        case 1:
            h ^= (uint64_t)sky_ctype_tolower(data[0]);
            h *= m;
    }

    h ^= h >> r;
    h *= m;
    h ^= h >> r;

    return h;
}


/* The hashing algorithm for double values is lifted from CPython 3.3.
 * see Objects/object.c :: _Py_HashDouble().
 */


/* For numeric types, the hash of a number x is based on the reduction
 * of x modulo the prime P = 2**_PyHASH_BITS - 1.  It's designed so that
 * hash(x) == hash(y) whenever x and y are numerically equal, even if
 * x and y have different types.
 *
 * A quick summary of the hashing strategy:
 *
 * (1) First define the 'reduction of x modulo P' for any rational
 * number x; this is a standard extension of the usual notion of
 * reduction modulo P for integers.  If x == p/q (written in lowest
 * terms), the reduction is interpreted as the reduction of p times
 * the inverse of the reduction of q, all modulo P; if q is exactly
 * divisible by P then define the reduction to be infinity.  So we've
 * got a well-defined map
 *
 *    reduce : { rational numbers } -> { 0, 1, 2, ..., P-1, infinity }.
 *
 * (2) Now for a rational number x, define hash(x) by:
 *
 *    reduce(x)   if x >= 0
 *    -reduce(-x) if x < 0
 *
 * If the result of the reduction is infinity (this is impossible for
 * integers, floats and Decimals) then use the predefined hash value
 * _PyHASH_INF for x >= 0, or -_PyHASH_INF for x < 0, instead.
 * _PyHASH_INF, -_PyHASH_INF and _PyHASH_NAN are also used for the
 * hashes of float and Decimal infinities and nans.
 *
 * A selling point for the above strategy is that it makes it possible
 * to compute hashes of decimal and binary floating-point numbers
 * efficiently, even if the exponent of the binary or decimal number
 * is large.  The key point is that
 *
 *    reduce(x * y) == reduce(x) * reduce(y) (modulo _PyHASH_MODULUS)
 *
 * provided that {reduce(x), reduce(y)} != {0, infinity}.  The reduction of a
 * binary or decimal float is never infinity, since the denominator is a power
 * of 2 (for binary) or a divisor of a power of 10 (for decimal).  So we have,
 * for nonnegative x,
 *
 *    reduce(x * 2**e) == reduce(x) * reduce(2**e) % _PyHASH_MODULUS
 *
 *    reduce(x * 10**e) == reduce(x) * reduce(10**e) % _PyHASH_MODULUS
 *
 * and reduce(10**e) can be computed efficiently by the usual modular
 * exponentiation algorithm.  For reduce(2**e) it's even better: since
 * P is of the form 2**n-1, reduce(2**e) is 2**(e mod n), and multiplication
 * by 2**(e mod n) modulo 2**n-1 just amounts to a rotation of bits.
 */

#if defined(SKY_ARCH_32BIT)
#   define SKY_UTIL_HASH_BITS (uintptr_t)31
#elif defined(SKY_ARCH_64BIT)
#   define SKY_UTIL_HASH_BITS (uintptr_t)61
#endif
#define SKY_UTIL_HASH_MODULUS (((uintptr_t)1 << SKY_UTIL_HASH_BITS) - 1)

uintptr_t
sky_util_hashdouble(double value)
{
    int         exp, sign;
    double      magnitude;
    uintptr_t   x, y;

    if (!isfinite(value)) {
        if (isinf(value)) {
            return (value > 0 ? 314159 : -314159);
        }
        return 0;
    }

    sign = 1;
    if ((magnitude = frexp(value, &exp)) < 0) {
        sign = -1;
        magnitude = -magnitude;
    }

    /* process 28 bits at a time; this should work well both for binary
     * and hexadecimal floating point.
     */
    x = 0;
    while (magnitude) {
        x = ((x << 28) & SKY_UTIL_HASH_MODULUS) |
            (x >> (SKY_UTIL_HASH_BITS - 28));
        magnitude *= 268435456.0;   /* 2**28 */
        exp -= 28;
        y = (uintptr_t)magnitude;   /* pull out integer part */
        magnitude -= y;
        x += y;
        if (x >= SKY_UTIL_HASH_MODULUS) {
            x -= SKY_UTIL_HASH_MODULUS;
        }
    }

    /* adjust for the exponent; first reduce it modulo SKY_UTIL_HASH_BITS */
    exp = (exp >= 0 ? exp % SKY_UTIL_HASH_BITS
                    : SKY_UTIL_HASH_BITS - 1 - ((-1 - exp) % SKY_UTIL_HASH_BITS));
    x = ((x << exp) & SKY_UTIL_HASH_MODULUS) | x >> (SKY_UTIL_HASH_BITS - exp);

    return x * sign;
}


size_t
sky_util_roundpow2(size_t value)
{
    return (value < 1 ? 0 : 1 << (size_t)sky_util_flsl(value - 1));
}


uint32_t
sky_util_random32(void)
{
    uint32_t    random_value;

    sky_util_randombytes(&random_value, sizeof(random_value));

    return random_value;
}


uint64_t
sky_util_random64(void)
{
    uint64_t    random_value;

    sky_util_randombytes(&random_value, sizeof(random_value));

    return random_value;
}


static sky_once_t   sky_util_random_once     = SKY_ONCE_INITIALIZER;
#if defined(_WIN32)
static HCRYPTPROV   sky_util_random_provider = 0;
#else
static int          sky_util_random_fd       = -1;
#endif


static void
sky_util_random_once_runner(SKY_UNUSED void *unused)
{
#if defined(_WIN32)
    if (unlikely(!CryptAcquireContext(&sky_util_random_provider,
                                      NULL,
                                      NULL,
                                      PROV_RSA_FULL,
                                      CRYPT_SILENT | CRYPT_VERIFYCONTEXT)))
    {
        sky_error_raise_win32(GetLastError());
    }
#else
    if (-1 == sky_util_random_fd) {
        sky_util_random_fd = open("/dev/urandom", O_RDONLY);
        if (unlikely(-1 == sky_util_random_fd)) {
            sky_error_raise_errno(sky_OSError, errno);
        }
    }
#endif
}


void
sky_util_randombytes(void *bytes, size_t nbytes)
{
#if !defined(_WIN32)
    ssize_t         nbytes_read;
#endif

    sky_error_validate_debug(bytes != NULL);
    sky_error_validate_debug(nbytes > 0 && nbytes <= INT32_MAX);

    sky_once_run(&sky_util_random_once, sky_util_random_once_runner, NULL);

#if defined(_WIN32)
    if (unlikely(!CryptGenRandom(sky_util_random_provider,
                                 (DWORD)nbytes,
                                 bytes)))
    {
        sky_error_raise_win32(GetLastError());
    }
#else
    while (nbytes > 0) {
        nbytes_read = read(sky_util_random_fd, bytes, nbytes);
        if (unlikely(-1 == nbytes_read)) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        bytes   = (char *)bytes + nbytes_read;
        nbytes -= nbytes_read;
    }
#endif
}


/* This QuickSort implementation is from http://www.efgh.com/software/qsort.htm
 * Written by Philip J. Erdelsky. It is in the public domain.
 */
static void
sky_util_quicksort_impl(char *  left,
                        char *  right,
                        size_t  size,
                        int     (*compare)(const void *, const void *, void *),
                        void *  arg)
{
    char    *p = left, *q = right, *t = left, x;
    size_t  i;

    for (;;) {
        while (p < right && (*compare)(p, t, arg) < 0) {
            p += size;
        }
        while (q > left && (*compare)(q, t, arg) > 0) {
            q -= size;
        }
        if (p > q) {
            break;
        }
        if (p < q) {
            for (i = 0; i < size; i++) {
                x = p[i];
                p[i] = q[i];
                q[i] = x;
            }
            if (t == p) {
                t = q;
            }
            else if (t == q) {
                t = p;
            }
        }
        p += size;
        q -= size;
    }
    if (left < q) {
        sky_util_quicksort_impl(left, q, size, compare, arg);
    }
    if (p < right) {
        sky_util_quicksort_impl(p, right, size, compare, arg);
    }
}


void
sky_util_quicksort(void *   base,
                   size_t   nel,
                   size_t   width,
                   int      (*compare)(const void *, const void *, void *),
                   void *   arg)
{
    if (nel > 1) {
        sky_util_quicksort_impl((char *)base,
                                (char *)base + ((nel - 1) * width),
                                width,
                                compare,
                                arg);
    }
}


static void
sky_util_listdir_close(void *pointer)
{
#if defined(_WIN32)
    FindClose((HANDLE)pointer);
#else
    closedir((DIR *)pointer);
#endif
}

static void
sky_util_listdir_update(char **old_files, size_t old_avail,
                        char **new_files, size_t new_avail, size_t nfiles)
{
    size_t  delta, i, nbytes;

    delta = new_avail - old_avail;
    nbytes = (char *)old_files + old_avail - old_files[nfiles - 1];
    memcpy((char *)new_files + new_avail - nbytes,
           old_files[nfiles - 1],
           nbytes);
    for (i = 0;  i < nfiles;  ++i) {
        new_files[i] = (char *)new_files +
                       (old_files[i] - (char *)old_files) + delta;
    }
}

char **
sky_util_listdir(const char *dirname)
{
    char    **files, *last_name, *name, **new_files;
    size_t  name_length, new_avail, nfiles, total_avail, total_length;

    files = NULL;
    nfiles = 0;
    total_avail = 0;
    total_length = sizeof(char *);

    SKY_ASSET_BLOCK_BEGIN {
#if defined(_WIN32)
        char            *dirname2;
        HANDLE          hFind;
        size_t          dirname_length;
        WIN32_FIND_DATA findFileData;

        dirname_length = strlen(dirname);
        dirname2 = sky_asset_malloc(dirname_length + 3,
                                    SKY_ASSET_CLEANUP_ALWAYS);
        memcpy(dirname2, dirname, dirname_length);
        dirname2[dirname_length    ] = '\\';
        dirname2[dirname_length + 1] = '*';
        dirname2[dirname_length + 2] = '\0';
        hFind = FindFirstFile(dirname, &findFileData);
        if (INVALID_HANDLE_VALUE == hFind) {
            if (ERROR_FILE_NOT_FOUND == GetLastError()) {
                /* directory exists, but is empty */
                goto nomorefiles;
            }
            sky_error_raise_win32(GetLastError());
        }
        sky_asset_save(hFind,
                       sky_util_listdir_close,
                       SKY_ASSET_CLEANUP_ALWAYS);
        do {
            name = findFileData.cFileName;
            name_length = strlen(findFileData.cFileName);
#else
        DIR             *dirp;
        struct dirent   *dirent;

        if (!(dirp = opendir(dirname))) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_asset_save(dirp, sky_util_listdir_close, SKY_ASSET_CLEANUP_ALWAYS);
        while ((dirent = readdir(dirp)) != NULL) {
            name = dirent->d_name;
            name_length = strlen(dirent->d_name);
#endif

            total_length += (sizeof(char *) + name_length + 1);
            if (total_length > total_avail) {
                new_avail = sky_util_roundpow2(total_length);
                new_files = sky_malloc(new_avail);
                if (!files) {
                    last_name = (char *)new_files + new_avail;
                    sky_asset_save(new_files,
                                   sky_free,
                                   SKY_ASSET_CLEANUP_ON_ERROR);
                }
                else {
                    sky_util_listdir_update(files, total_avail,
                                            new_files, new_avail, nfiles);
                    last_name = new_files[nfiles - 1];
                    sky_asset_update(files,
                                     new_files,
                                     SKY_ASSET_UPDATE_FIRST_CURRENT);
                    sky_free(files);
                }
                files = new_files;
                total_avail = new_avail;
            }
            files[nfiles] = memcpy(last_name - (name_length + 1),
                                   name,
                                   name_length);
            files[nfiles][name_length] = '\0';
            last_name = files[nfiles];
            ++nfiles;
#if defined(_WIN32)
        } while (FindNextFile(hFind, &findFileData));
        if (ERROR_NO_MORE_FILES != GetLastError()) {
            sky_error_raise_win32(GetLastError());
        }
nomorefiles:
        do {} while (0);
#else
        }
#endif
    } SKY_ASSET_BLOCK_END;

    files[nfiles] = NULL;
    return files;
}


void
sky_util_mkdir(const char *path, unsigned int mode, sky_bool_t intermediate)
{
#if defined(_WIN32)
    DWORD   error;

    if (!intermediate) {
        if (!CreateDirectory(path, NULL)) {
            error = GetLastError();
        }
    }
    else {
        error = SHCreateDirectoryEx(NULL, path, NULL);
    }
    if (ERROR_SUCCESS != error) {
        sky_error_raise_win32(error);
    }
#else
    char            *p, *path_buffer;
    size_t          path_length;
    const char      *end, *c;

    if (mkdir(path, mode) != -1) {
        return;
    }
    if (!intermediate || ENOENT != errno) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    /* Create the intermediate directories. */
    SKY_ASSET_BLOCK_BEGIN {
        if (!(end = strrchr(path, '/')) || !(path_length = end - path)) {
            sky_error_raise_errno(sky_OSError, ENOENT);
        }
        path_buffer = p = sky_malloc(path_length + 1);

        /* Skip leading '/', because we'll assume that the root exists. */
        for (c = path;  c < end && '/' == *c;  ++c) {
            *p++ = *c;
        }

        while (c < end) {
            while (c < end && '/' != *c) {
                *p++ = *c;
                ++c;
            }

            *p = '\0';
            if (mkdir(path_buffer, 0777) == -1 && EEXIST != errno) {
                sky_error_raise_errno(sky_OSError, errno);
            }

            while (c < end && '/' == *c) {
                *p++ = *c;
                ++c;
            }
        }
    } SKY_ASSET_BLOCK_END;

    /* Try again to create the full path. Any failure gets raised. */
    if (mkdir(path, mode) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
#endif
}


size_t
sky_util_itoa(intmax_t value, char *buffer, size_t buffer_size)
{
    size_t      length, pos;
    intmax_t    tmp;

    if (!value) {
        if (buffer_size) {
            buffer[0] = '0';
        }
        return 1;
    }

    for (length = 0, tmp = value; tmp; ++length, tmp /= 10);
    if (value < 0) {
        ++length;
    }

    if (length <= buffer_size) {
        for (pos = length, tmp = value; tmp; tmp /= 10) {
            buffer[--pos] = sky_ctype_digits[abs((int)(tmp % 10))];
        }
        if (value < 0) {
            buffer[0] = '-';
        }
    }

    return length;
}


size_t
sky_util_utoa(uintmax_t value, char *buffer, size_t buffer_size)
{
    size_t      length, pos;
    uintmax_t   tmp;

    if (!value) {
        if (buffer_size) {
            buffer[0] = '0';
        }
        return 1;
    }

    for (length = 0, tmp = value; tmp; ++length, tmp /= 10);

    if (length <= buffer_size) {
        for (pos = length, tmp = value; tmp; tmp /= 10) {
            buffer[--pos] = sky_ctype_digits[(int)(tmp % 10)];
        }
    }

    return length;
}


/* This is basically PyOS_double_to_string() taken straight from CPython 3.3 */
char *
sky_util_dtoa(double value, char type, int precision, unsigned int flags)
{
    extern char *dtoa(double, int, int, int *, int *, char **);
    extern void freedtoa(void *);

    static char *lower_strings[] = { "inf", "nan", "e" };
    static char *upper_strings[] = { "INF", "NAN", "E" };

    int             decpt_as_int, mode, sign;
    char            *buf, *decimal_point, *digits, *digits_end, *p, **strings;
    size_t          bufsize, decimal_point_len;
    ssize_t         decpt, digits_len, exp, vdigits_end, vdigits_start;
    sky_bool_t      use_exp;
    unsigned int    new_control_word, old_control_word, out_control_word;

    strings = lower_strings;

    switch (type) {
        case 'E':
            strings = upper_strings;
            type = 'e';
        case 'e':
            mode = 2;
            ++precision;
            break;

        case 'F':
            strings = upper_strings;
            type = 'f';
        case 'f':
            mode = 3;
            break;

        case 'G':
            strings = upper_strings;
            type = 'g';
        case 'g':
            mode = 2;
            if (!precision) {
                precision = 1;
            }
            break;

        case 'r':
            mode = 0;
            break;

        default:
            sky_error_fatal("internal error");
    }

    sky_system_controlfpu(0, 0, &old_control_word, NULL);
    new_control_word = (old_control_word & ~(_MCW_PC | _MCW_RC)) |
                       (_PC_53 | _RC_NEAR);
    if (new_control_word != old_control_word) {
        sky_system_controlfpu(new_control_word,
                              _MCW_PC | _MCW_RC,
                              &out_control_word,
                              NULL);
    }
    digits = dtoa(value, mode, precision, &decpt_as_int, &sign, &digits_end);
    if (new_control_word != old_control_word) {
        sky_system_controlfpu(old_control_word,
                              _MCW_PC | _MCW_RC,
                              &out_control_word,
                              NULL);
    }
    sky_error_validate(digits != NULL);
    digits_len = digits_end - digits;

    if (digits_len && !sky_ctype_isdigit(*digits)) {
        if (*digits == 'n' || *digits == 'N') {
            sign = 0;
        }
        buf = p = sky_malloc(5);
        if (sign) {
            *p++ = '-';
        }
        else if (flags & SKY_UTIL_DTOA_ALWAYS_SIGN) {
            *p++ = '+';
        }
        if (*digits == 'i' || *digits == 'I') {
            *p++ = strings[0][0];
            *p++ = strings[0][1];
            *p++ = strings[0][2];
        }
        else if (*digits == 'n' || *digits == 'N') {
            *p++ = strings[1][0];
            *p++ = strings[1][1];
            *p++ = strings[1][2];
        }
        else {
            sky_error_fatal("internal error");
        }
        *p++ = '\0';
        freedtoa(digits);
        return buf;
    }

    /* We got digits back, format them. We may need to pad 'digits' either
     * on the left or right (or both) with extra zeros, so in general the
     * resulting string has the form
     *
     *   [<sign>]<zeros><digits><zeros>[<exponent>]
     *
     * where either of the <zeros> pieces could be empty, and there's a
     * decimal point that could appear either in <digits> or in the leading
     * or trailing <zeros>.
     *
     * Imagine an infinite 'virtual' string vdigits, consisting of the string
     * 'digits' (starting at index 0) padded on both the left and right with
     * infinite strings of zeros. We want to output a slice
     *
     *   vdigits[vdigits_start : vdigits_end]
     *
     * of this virtual string. Thus if vdigits_start < 0 then we'll end up
     * producing some leading zeros; if vdigits_end > digits_len there will
     * be trailing zeros in the output. The next section of code determines
     * whther to use an exponent or not, figures out the position 'decpt' of
     * the decimal point, and computes 'vdigits_start' and 'vdigits_end'.
     */
    use_exp = SKY_FALSE;
    decpt = (ssize_t)decpt_as_int;
    vdigits_end = digits_len;
    switch (type) {
        case 'e':
            use_exp = SKY_TRUE;
            vdigits_end = precision;
            break;
        case 'f':
            vdigits_end = decpt + precision;
            break;
        case 'g':
            if (decpt <= -4 ||
                decpt > precision - ((flags & SKY_UTIL_DTOA_ADD_DOT_0) ? 1 : 0))
            {
                use_exp = SKY_TRUE;
            }
            if (flags & SKY_UTIL_DTOA_ALTERNATE_FORM) {
                vdigits_end = precision;
            }
            break;
        case 'r':
            /* convert to exponential format at 1e16. We used to convert at
             * 1e17, but that gives odd-looking results for some values when
             * a 16-digit 'shortest' repr is padded with bogus zeros. For
             * example, repr(2e16+8) would give 20000000000000010.0; the true
             * value is 20000000000000008.0.
             */
            if (decpt <= -4 || decpt > 16) {
                use_exp = SKY_TRUE;
            }
            break;
    }

    /* if using an exponent, reset decimal point position to 1 and adjust
     * exponent accordingly.
     */
    if (use_exp) {
        exp = decpt - 1;
        decpt = 1;
    }
    /* ensure vdigits_start < decpt <= vdigits_end, or
     * vdigits_start < decpt < vdigits_end if ADD_DOT_0 and no exponent.
     */
    vdigits_start = (decpt <= 0 ? decpt - 1 : 0);
    if (!use_exp && (flags & SKY_UTIL_DTOA_ADD_DOT_0)) {
        vdigits_end = (vdigits_end > decpt ? vdigits_end : decpt + 1);
    }
    else {
        vdigits_end = (vdigits_end > decpt ? vdigits_end : decpt);
    }
    sky_error_validate_debug(vdigits_start <= 0 &&
                             0 <= digits_len &&
                             digits_len <= vdigits_end);
    sky_error_validate_debug(vdigits_start <= decpt && decpt <= vdigits_end);

    /* Compute an upper bound on how much memory we need. This might be a few
     * bytes too long, but no big deal.
     */
    decimal_point = ".";
    if (flags & SKY_UTIL_DTOA_LOCALE_DECIMAL) {
        struct lconv    *lc;

#if defined(HAVE_LOCALECONV_L)
        lc = localeconv_l(uselocale(NULL));
#else
        lc = localeconv();
#endif
        if (lc->decimal_point) {
            decimal_point = lc->decimal_point;
        }
    }
    decimal_point_len = strlen(decimal_point);
    bufsize = (vdigits_end - vdigits_start) +
              (use_exp ? 5 : 0) +
              decimal_point_len + 2;
    buf = p = sky_malloc(bufsize);

    if (sign == 1) {
        *p++ = '-';
    }
    else if (flags & SKY_UTIL_DTOA_ALWAYS_SIGN) {
        *p++ = '+';
    }

    /* Note that exactly one of the three 'if' conditions is true,
     * so we include exactly one decimal point.
     */

#define append_decimal_point()                                  \
        do {                                                    \
            if (1 == decimal_point_len) {                       \
                *p++ = *decimal_point;                          \
            }                                                   \
            else {                                              \
                memcpy(p, decimal_point, decimal_point_len);    \
                p += decimal_point_len;                         \
            }                                                   \
        } while (0)

    /* Zero padding on left of digit string */
    if (decpt <= 0) {
        memset(p, '0', decpt - vdigits_start);
        p += decpt - vdigits_start;
        append_decimal_point();
        memset(p, '0', 0 - decpt);
        p += 0 - decpt;
    }
    else {
        memset(p, '0', 0 - vdigits_start);
        p += 0 - vdigits_start;
    }

    /* Digits, with included decimal point */
    if (0 < decpt && decpt <= digits_len) {
        memcpy(p, digits, decpt - 0);
        p += decpt - 0;
        append_decimal_point();
        memcpy(p, digits + decpt, digits_len - decpt);
        p += digits_len - decpt;
    }
    else {
        memcpy(p, digits, digits_len);
        p += digits_len;
    }

    /* Zeros on the right */
    if (digits_len < decpt) {
        memset(p, '0', decpt - digits_len);
        p += decpt - digits_len;
        append_decimal_point();
        memset(p, '0', vdigits_end - decpt);
        p += vdigits_end - decpt;
    }
    else {
        memset(p, '0', vdigits_end - digits_len);
        p += vdigits_end - digits_len;
    }

#undef append_decimal_point

    /* Delete a trailing decimal point unless using alternative formatting. */
    if (!(flags & SKY_UTIL_DTOA_ALTERNATE_FORM)) {
        if (1 == decimal_point_len) {
            if (p[-1] == *decimal_point) {
                --p;
            }
        }
        else if (!memcmp(p - decimal_point_len,
                         decimal_point,
                         decimal_point_len))
        {
            p -= decimal_point_len;
        }
    }

    /* Add the exponent - CPython uses sprintf(p, "%+.02d", exp) */
    if (use_exp) {
        sky_error_validate_debug(exp < 1000);
        *p++ = strings[2][0];
        if (exp >= 0) {
            *p++ = '+';
        }
        else {
            *p++ = '-';
            exp = -exp;
        }
        if (exp < 100) {
            *p++ = '0' + (exp / 10);
            *p++ = '0' + (exp % 10);
        }
        else {
            *p++ = '0' + (exp / 100);
            *p++ = '0' + ((exp % 100) / 10);
            *p++ = '0' + (exp % 10);
        }
    }

    *p++ = '\0';
    freedtoa(digits);
    return buf;
}


double
sky_util_strtod(const char *s, size_t slen, char **endptr)
{
    /* Use gdtoa_strtod() instead of the system strtod(), because the former
     * does not care about locale, whereas the latter probably does. It's also
     * better for consistency across platforms, ensuring the same results on
     * the same architecture, regardless of operating system or libc version.
     */
    extern double gdtoa_strtod(const char *, size_t, char **);

    return gdtoa_strtod(s, slen, endptr);
}


/* The following code for float/double encoding/decoding is taken straight from
 * CPython 3.3.5.
 */

uint8_t *
sky_util_float_encode(float value, uint8_t *bytes, size_t nbytes, int endian)
{
    int     incr = 1;
    uint8_t *p = bytes;

    if (!bytes || nbytes < 4) {
        return NULL;
    }
    if (!endian) {
        endian = SKY_ENDIAN;
    }
    else if (endian != SKY_ENDIAN_BIG && endian != SKY_ENDIAN_LITTLE) {
        return NULL;
    }
    if (SKY_FLOAT_FORMAT_UNKNOWN == sky_float_format_float) {
        int             e;
        double          f;
        unsigned int    fbits;
        unsigned char   sign = 0;

        if (SKY_ENDIAN_LITTLE == endian) {
            p += 3;
            incr = -1;
        }
        if (value < 0) {
            sign = 1;
            value = -value;
        }

        f = frexp(value, &e);

        /* Normalize f to be in the range [1.0, 2.0] */
        if (0.5 <= f && f < 1.0) {
            f *= 2.0;
            --e;
        }
        else if (0.0 == f) {
            e = 0;
        }
        else {
            /* frexp() result out of range */
            return NULL;
        }

        if (e >= 128) {
            /* Overflow */
            return NULL;
        }
        if (e < -126) {
            /* Gradual underflow */
            f = ldexp(f, 126 + e);
            e = 0;
        }
        else if (!(0 == e && 0.0 == f)) {
            e += 127;
            f -= 1.0;   /* Get rid of leading 1 */
        }

        f *= 8388608.0; /* 2**23 */
        fbits = (unsigned int)(f + 0.5); /* Round */
        sky_error_validate_debug(fbits <= 8388608);
        if (fbits >> 23) {
            /* The carry propagated out of a string of 23 1 bits. */
            fbits = 0;
            if (++e >= 255) {
                /* Overflow */
                return NULL;
            }
        }

        /* First byte */
        *p = (sign << 7) | (e >> 1);
        p += incr;

        /* Second byte */
        *p = (char)(((e & 1) << 7) | (fbits >> 16));
        p += incr;

        /* Third byte */
        *p = (fbits >> 8) & 0xFF;
        p += incr;

        /* Fourth byte */
        *p = fbits & 0xFF;
    }
    else {
        int         i;
        const char  *s = (char *)&value;

        if ((SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN == sky_float_format_float &&
             SKY_ENDIAN_BIG != endian) ||
            (SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN == sky_float_format_float &&
             SKY_ENDIAN_LITTLE != endian))
        {
            p += 3;
            incr = -1;
        }
        for (i = 0; i < 4; ++i) {
            *p = *s++;
            p += incr;
        }
    }

    return bytes + 4;
}


const uint8_t *
sky_util_float_decode(const uint8_t *   bytes,
                      size_t            nbytes,
                      int               endian,
                      float *           value)
{
    int             incr = 1;
    const uint8_t   *p = bytes;

    if (!bytes || nbytes < 4) {
        return NULL;
    }
    if (!endian) {
        endian = SKY_ENDIAN;
    }
    else if (SKY_ENDIAN_BIG != endian && SKY_ENDIAN_LITTLE != endian) {
        return NULL;
    }
    if (SKY_FLOAT_FORMAT_UNKNOWN == sky_float_format_float) {
        int             e;
        double          x;
        unsigned int    f;
        unsigned char   sign;

        if (SKY_ENDIAN_LITTLE == endian) {
            p += 3;
            incr = -1;
        }

        /* First byte */
        sign = (*p >> 7) & 1;
        e = (*p & 0x7F) << 1;
        p += incr;

        /* Second byte */
        e |= (*p >> 7) & 1;
        f = (*p & 0x7F) << 16;
        p += incr;

        if (255 == e) {
            /* can't unpack IEEE 754 special value on non-IEEE platform */
            return NULL;
        }

        /* Third byte */
        f |= *p << 8;
        p += incr;

        /* Fourth byte */
        f |= *p;

        x = (double)f / 8388608.0;

        /* XXX This sadly ignores Inf/NaN issues */
        if (!e) {
            e = -126;
        }
        else {
            x += 1.0;
            e -= 127;
        }
        x = ldexp(x, e);

        if (sign) {
            x = -x;
        }
        *value = (float)x;
    }
    else {
        int     i;
        char    *s = (char *)value;

        if ((SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN == sky_float_format_float &&
             SKY_ENDIAN_BIG != endian) ||
            (SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN == sky_float_format_float &&
             SKY_ENDIAN_LITTLE != endian))
        {
            p += 3;
            incr = -1;
        }
        for (i = 0; i < 4; ++i) {
            *s++ = *p;
            p += incr;
        }
    }

    return bytes + 4;
}


uint8_t *
sky_util_double_encode(double value, uint8_t *bytes, size_t nbytes, int endian)
{
    int     incr = 1;
    uint8_t *p = bytes;

    if (!bytes || nbytes < 8) {
        return NULL;
    }
    if (!endian) {
        endian = SKY_ENDIAN;
    }
    else if (endian != SKY_ENDIAN_BIG && endian != SKY_ENDIAN_LITTLE) {
        return NULL;
    }
    if (SKY_FLOAT_FORMAT_UNKNOWN == sky_float_format_double) {
        int             e;
        double          f;
        unsigned int    fhi, flo;
        unsigned char   sign = 0;

        if (SKY_ENDIAN_LITTLE == endian) {
            p += 7;
            incr = -1;
        }

        if (value < 0) {
            sign = 1;
            value = -value;
        }

        f = frexp(value, &e);

        /* Normalize f to be in the range [1.0, 2.0] */
        if (0.5 <= f && f < 1.0) {
            f *= 2.0;
            --e;
        }
        else if (0.0 == f) {
            e = 0;
        }
        else {
            /* frexp() result out of range */
            return NULL;
        }

        if (e >= 1024) {
            /* Overflow */
            return NULL;
        }
        if (e < -1022) {
            /* Gradual underflow */
            f = ldexp(f, 1022 + e);
            e = 0;
        }
        else if (!(0 == e && 0.0 == f)) {
            e += 1023;
            f = -1.0;   /* Get rid of leading 1 */
        }

        /* fhi receives the high 28 bits; flo the low 24 bits (== 52 bits) */
        f *= 268435456.0;   /* 2**28 */
        fhi = (unsigned int)f;  /* Truncate */
        sky_error_validate_debug(fhi < 268435456);

        f -= (double)fhi;
        f *= 16777216.0;     /* 2**24 */
        flo = (unsigned int)(f + 0.5);  /* Round */
        sky_error_validate_debug(flo <= 16777216);
        if (flo >> 24) {
            /* The carry propagated out of a string of 24 1 bits. */
            flo = 0;
            ++fhi;
            if (fhi >> 28) {
                /* And it also propagated out of the next 28 bits. */
                fhi = 0;
                ++e;
                if (e >= 2047) {
                    /* Overflow */
                    return NULL;
                }
            }
        }

        /* First byte */
        *p = (sign << 7) | (e >> 4);
        p += incr;

        /* Second byte */
        *p = (unsigned char)(((e & 0xF) << 4) | (fhi >> 24));
        p += incr;

        /* Third byte */
        *p = (fhi >> 16) & 0xFF;
        p += incr;

        /* Fourth byte */
        *p = (fhi >> 8) & 0xFF;
        p += incr;

        /* Fifth byte */
        *p = fhi & 0xFF;
        p += incr;

        /* Sixth byte */
        *p = (flo >> 16) & 0xFF;
        p += incr;

        /* Seventh byte */
        *p = (flo >> 8) & 0xFF;
        p += incr;

        /* Eighth byte */
        *p = flo & 0xFF;
    }
    else {
        int         i, incr = 1;
        uint8_t     *p = bytes;
        const char  *s = (char *)&value;

        if ((SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN == sky_float_format_double &&
             SKY_ENDIAN_BIG != endian) ||
            (SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN == sky_float_format_double &&
             SKY_ENDIAN_LITTLE != endian))
        {
            p += 7;
            incr = -1;
        }
        for (i = 0; i < 8; ++i) {
            *p = *s++;
            p += incr;
        }
    }

    return bytes + 8;
}


const uint8_t *
sky_util_double_decode(const uint8_t *   bytes,
                       size_t            nbytes,
                       int               endian,
                       double *          value)
{
    int             incr = 1;
    const uint8_t   *p = bytes;

    if (!bytes || nbytes < 8) {
        return NULL;
    }
    if (!endian) {
        endian = SKY_ENDIAN;
    }
    else if (SKY_ENDIAN_BIG != endian && SKY_ENDIAN_LITTLE != endian) {
        return NULL;
    }
    if (SKY_FLOAT_FORMAT_UNKNOWN == sky_float_format_double) {
        int             e;
        double          x;
        unsigned int    fhi, flo;
        unsigned char   sign;

        if (SKY_ENDIAN_LITTLE == endian) {
            p += 7;
            incr = -1;
        }

        /* First byte */
        sign = (*p >> 7) & 1;
        e = (*p & 0x7F) << 4;
        p += incr;

        /* Second byte */
        e |= (*p >> 4) & 0xF;
        fhi = (*p & 0xF) << 24;
        p += incr;

        if (2047 == e) {
            /* can't unpack IEEE 754 special value on non-IEEE platform */
           return NULL;
        }

        /* Third byte */
        fhi |= *p << 16;
        p += incr;

        /* Fourth byte */
        fhi |= *p << 8;
        p += incr;

        /* Fifth byte */
        fhi |= *p;
        p += incr;

        /* Sixth byte */
        flo = *p << 16;
        p += incr;

        /* Seventh byte */
        flo |= *p << 8;
        p += incr;

        /* Eighth byte */
        flo |= *p;

        x = (double)fhi + (double)flo / 16777216.0; /* 2**24 */
        x /= 268435456.0;   /* 2**28 */

        if (!e) {
            e = -1022;
        }
        else {
            x += 1.0;
            e -= 1023;
        }
        x = ldexp(*value, e);
        if (sign) {
            x = -x;
        }
        *value = x;
    }
    else {
        int     i;
        char    *s = (char *)value;

        if ((SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN == sky_float_format_double &&
             SKY_ENDIAN_BIG != endian) ||
            (SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN == sky_float_format_double &&
             SKY_ENDIAN_LITTLE != endian))
        {
            p += 7;
            incr = -1;
        }
        for (i = 0; i < 8; ++i) {
            *s++ = *p;
            p += incr;
        }
    }

    return bytes + 8;
}


uint8_t *
sky_util_varint_encode(uintmax_t value, uint8_t *bytes, size_t nbytes)
{
    const uint8_t   *end;

    for (end = bytes + nbytes; bytes < end; ++bytes) {
        *bytes = value & 0x7F;
        value >>= 7;
        if (!value) {
            return bytes + 1;
        }
        *bytes |= 0x80;
    }

    return NULL;
}


const uint8_t *
sky_util_varint_decode(const uint8_t *bytes, size_t nbytes, uintmax_t *value)
{
    uintmax_t   shift;
    uint8_t     byte;

    for (shift = *value = 0; nbytes > 0; --nbytes) {
        byte = *bytes++;
        if (!(byte & 0x80)) {
            *value |= ((ssize_t)byte << shift);
            return bytes;
        }
        *value |= ((size_t)(byte & 0x7F) << shift);
        shift += 7;
        if (shift > (sizeof(size_t) * 8) - 7) {
            break;
        }
    }

    return NULL;
}


size_t
sky_util_varint_size(size_t value)
{
    return (value ? (sky_util_flsl(value) + 6) / 7 : 1);
}
