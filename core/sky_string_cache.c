#include "sky_private.h"
#include "sky_string_private.h"


typedef struct sky_string_cache_intern_item_s {
    sky_lockfree_hashtable_item_t       base;
    sky_string_t                        string; /* weak reference */
    uint32_t                            epoch;
} sky_string_cache_intern_item_t;


static sky_bool_t
sky_string_cache_intern_item_compare(sky_lockfree_hashtable_item_t *item,
                                     void *                         key)
{
    sky_string_cache_intern_item_t  *cache_item =
                                    (sky_string_cache_intern_item_t *)item;

    sky_string_t    string;

    if (!cache_item->epoch || !(string = cache_item->string)) {
        return SKY_FALSE;
    }
    return sky_object_compare(key, cache_item->string, SKY_COMPARE_OP_EQUAL);
}


typedef struct sky_string_cache_literal_item_s {
    sky_lockfree_hashtable_item_t       base;
    const char *                        bytes;
    size_t                              nbytes;
    sky_string_t                        string;
} sky_string_cache_literal_item_t;

typedef struct sky_string_cache_literal_key_s {
    const char *                        bytes;
    size_t                              nbytes;
} sky_string_cache_literal_key_t;


static sky_bool_t
sky_string_cache_literal_item_compare(sky_lockfree_hashtable_item_t *   item,
                                      void *                            key)
{
    sky_string_cache_literal_key_t  *key_struct = key;
    sky_string_cache_literal_item_t *cache_item = 
                                    (sky_string_cache_literal_item_t *)item;

    return (key_struct->bytes == cache_item->bytes &&
            key_struct->nbytes == cache_item->nbytes);
}


SKY_EXTERN_INLINE uintptr_t
sky_string_cache_literal_hash(const char *bytes)
{
#if defined(SKY_ARCH_32BIT)
    return (uintptr_t)bytes >> 2;
#elif defined(SKY_ARCH_64BIT)
    return (uintptr_t)bytes >> 3;
#else
#   error implementation missing
#endif
}


static void
sky_string_cache_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_string_cache_data_t *cache_data = data;

    sky_lockfree_hashtable_cleanup(&(cache_data->intern_table));
    sky_lockfree_hashtable_cleanup(&(cache_data->literal_table));
}


static void
sky_string_cache_instance_initialize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_string_cache_data_t *cache_data = data;

    sky_lockfree_hashtable_init(&(cache_data->intern_table),
                                sky_string_cache_intern_item_compare,
                                NULL,
                                sky_free);
    sky_lockfree_hashtable_init(&(cache_data->literal_table),
                                sky_string_cache_literal_item_compare,
                                NULL,
                                sky_free);
}


static void
sky_string_cache_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_string_cache_data_t *cache_data = data;

    sky_string_cache_intern_item_t      *iitem;
    sky_string_cache_literal_item_t     *litem;
    sky_lockfree_hashtable_iterator_t   iterator;

    if (SKY_OBJECT_VISIT_REASON_GC_MARK != visit_data->reason) {
        sky_lockfree_hashtable_iterator_init(&iterator,
                                             &(cache_data->intern_table),
                                             NULL,
                                             SKY_FALSE);
        while ((iitem = (sky_string_cache_intern_item_t *)
                        sky_lockfree_hashtable_iterator_next(&iterator)) != NULL)
        {
            sky_object_visit(iitem->string, visit_data);
        }
        sky_lockfree_hashtable_iterator_cleanup(&iterator);
    }

    sky_lockfree_hashtable_iterator_init(&iterator,
                                         &(cache_data->literal_table),
                                         NULL,
                                         SKY_FALSE);
    while ((litem = (sky_string_cache_literal_item_t *)
                    sky_lockfree_hashtable_iterator_next(&iterator)) != NULL)
    {
        sky_object_visit(litem->string, visit_data);
    }
    sky_lockfree_hashtable_iterator_cleanup(&iterator);
}


typedef struct sky_string_cache_intern_weakref_s {
    sky_string_cache_t                  cache;
    sky_string_cache_intern_item_t *    item;
} sky_string_cache_intern_weakref_t;

static void
sky_string_cache_intern_weakref_visit(sky_capsule_t             capsule,
                                      sky_object_visit_data_t * visit_data)
{
    sky_string_cache_intern_weakref_t   *ref;
    
    ref = sky_capsule_pointer(capsule, NULL);
    sky_object_visit(ref->cache, visit_data);
}


static void
sky_string_cache_intern_weakref_callback(sky_object_t arg)
{
    uint32_t                            item_epoch;
    sky_string_cache_data_t             *cache_data;
    sky_string_cache_intern_weakref_t   *ref;

    ref = sky_capsule_pointer(arg, NULL);
    cache_data = sky_string_cache_data(ref->cache);

    item_epoch = ref->item->epoch;
    if (item_epoch < sky_tlsdata_get()->object_tlsdata.gc_epoch &&
        sky_atomic_cas32(item_epoch, 0, (int32_t *)&(ref->item->epoch)))
    {
        ref->item->string = NULL;
        sky_lockfree_hashtable_deleteitem(&(cache_data->intern_table),
                                          &(ref->item->base));
    }
}


static sky_string_t
sky_string_cache_intern_add(sky_string_cache_t cache, sky_string_t string)
{
    sky_string_cache_data_t *cache_data = sky_string_cache_data(cache);

    uint32_t                            epoch, item_epoch;
    uintptr_t                           hash;
    sky_capsule_t                       capsule;
    sky_hazard_pointer_t                *hazard;
    sky_string_cache_intern_item_t      *item, *new_item;
    sky_string_cache_intern_weakref_t   *ref;

    hash = sky_object_hash(string);
    new_item = sky_calloc(1, sizeof(sky_string_cache_intern_item_t));
    ref = sky_calloc(1, sizeof(sky_string_cache_intern_weakref_t));
    sky_object_gc_set(SKY_AS_OBJECTP(&(ref->cache)), cache, cache);
    ref->item = new_item;
    capsule = sky_capsule_create(ref,
                                 NULL,
                                 sky_free,
                                 sky_string_cache_intern_weakref_visit);
    sky_object_gc_setweakref(SKY_AS_OBJECTP(&(new_item->string)),
                             string,
                             cache,
                             sky_string_cache_intern_weakref_callback,
                             capsule);
    new_item->epoch = epoch = sky_tlsdata_get()->object_tlsdata.gc_epoch;

    for (;;) {
        hazard = sky_lockfree_hashtable_insert(&(cache_data->intern_table),
                                               &(new_item->base),
                                               hash,
                                               string);
        if (hazard->pointer == new_item) {
            sky_hazard_pointer_release(hazard);
            return string;
        }
        item = hazard->pointer;
        if (!(item_epoch = item->epoch)) {
            /* The item is being deleted by a weakref callback. Try the insert
             * again.
             */
            sky_hazard_pointer_release(hazard);
            continue;
        }
        /* The only time that item->epoch can go down to 0 is when the weak
         * reference is being cleared, but that can only happen if the item
         * epoch is < the gc epoch, so this check here is safe.
         */
        if (item_epoch >= epoch) {
            break;
        }
        if (!sky_object_gc_isthreaded()) {
            item->epoch = epoch;
            break;
        }
        if (sky_atomic_cas32(item_epoch, epoch, (int32_t *)&(item->epoch))) {
            break;
        }
        sky_hazard_pointer_release(hazard);
    }

    /* Free the item that was not inserted. Make sure to clear the weak ref
     * first so that there's no dangling pointer left behind!
     */
    sky_object_gc_setweakref(SKY_AS_OBJECTP(&(new_item->string)),
                             NULL,
                             cache,
                             NULL,
                             NULL);
    sky_free(new_item);

    string = item->string;
    sky_hazard_pointer_release(hazard);
    return string;
}


static sky_string_t
sky_string_cache_intern_lookup(sky_string_cache_t cache, sky_string_t string)
{
    sky_string_cache_data_t *cache_data = sky_string_cache_data(cache);

    uint32_t                        epoch, item_epoch;
    sky_string_t                    cached_string;
    sky_hazard_pointer_t            *hazard;
    sky_string_cache_intern_item_t  *item;

    hazard = sky_lockfree_hashtable_lookup(&(cache_data->intern_table),
                                           sky_object_hash(string),
                                           string);
    if (!hazard) {
        return NULL;
    }

    cached_string = NULL;
    if ((item = hazard->pointer) != NULL && item->epoch) {
        epoch = sky_tlsdata_get()->object_tlsdata.gc_epoch;
        while ((item_epoch = item->epoch) != 0) {
            /* The only time that item->epoch can go down to 0 is when the weak
             * reference is being cleared, but that can only happen if the item
             * epoch is < the gc epoch, so this check here is safe.
             */
            if (item_epoch >= epoch) {
                cached_string = item->string;
                break;
            }
            if (!sky_object_gc_isthreaded()) {
                item->epoch = epoch;
                cached_string = item->string;
                break;
            }
            if (sky_atomic_cas32(item_epoch,
                                 epoch,
                                 (int32_t *)&(item->epoch)))
            {
                cached_string = item->string;
                break;
            }
        }
    }
    sky_hazard_pointer_release(hazard);
    return cached_string;
}


sky_string_t
sky_string_cache_literal(sky_string_cache_t cache,
                         const char *       bytes,
                         size_t             nbytes,
                         sky_string_t       string)
{
    sky_string_cache_data_t *cache_data  = sky_string_cache_data(cache);

    uintptr_t                       key_hash;
    sky_string_data_t               *string_data, tagged_data;
    sky_hazard_pointer_t            *item_hazard;
    sky_string_cache_literal_key_t  key_struct;
    sky_string_cache_literal_item_t *cache_item;

    if (sky_object_istagged(string)) {
        return string;
    }
    string_data = sky_string_data(string, &tagged_data);

    /* Do nothing if the string has already been entered into the cache. */
    if (string_data->flags & SKY_STRING_FLAG_CACHED_LITERAL) {
        return string;
    }

    key_struct.bytes = bytes;
    key_struct.nbytes = nbytes;

    cache_item = NULL;
    key_hash = sky_string_cache_literal_hash(bytes);
    for (;;) {
        item_hazard =
                sky_lockfree_hashtable_lookup(&(cache_data->literal_table),
                                              key_hash,
                                              &key_struct);
        if (item_hazard) {
            sky_free(cache_item);   /* most likely NULL, but might not be */
            cache_item = item_hazard->pointer;
            string = cache_item->string;
            sky_hazard_pointer_release(item_hazard);
            return string;
        }

        if (!cache_item) {
            cache_item = sky_calloc(1, sizeof(sky_string_cache_literal_item_t));
            cache_item->bytes = bytes;
            cache_item->nbytes = nbytes;
            sky_object_gc_set(SKY_AS_OBJECTP(&(cache_item->string)),
                              string,
                              cache);
        }

        item_hazard =
                sky_lockfree_hashtable_insert(&(cache_data->literal_table),
                                              &(cache_item->base),
                                              key_hash,
                                              &key_struct);
        if (cache_item != item_hazard->pointer) {
            sky_free(cache_item);
            cache_item = item_hazard->pointer;
        }
        else if (!(string_data->flags & SKY_STRING_FLAG_CACHED_LITERAL)) {
            sky_atomic_or32_old(SKY_STRING_FLAG_CACHED_LITERAL,
                                &(string_data->flags));
        }
        string = cache_item->string;
        sky_hazard_pointer_release(item_hazard);
        return string;
    }

    sky_error_fatal("internal error - unreachable code reached");
}


sky_string_t
sky_string_cache_lookup(sky_string_cache_t  cache,
                        const char *        bytes,
                        size_t              nbytes)
{
    sky_string_cache_data_t *cache_data = sky_string_cache_data(cache);

    size_t                          key_hash;
    sky_string_t                    string;
    sky_hazard_pointer_t            *item_hazard;
    sky_string_cache_literal_key_t  key_struct;
    sky_string_cache_literal_item_t *item;

    /* 1. Empty string is quickest and easiest to check for. */
    if (!bytes || !nbytes) {
        return sky_string_empty;
    }

    /* 2. Single characters. For undecoded UTF-8, we can only check bytes in
     *    ASCII range.
     */
    if (1 == nbytes && ((uint8_t *)bytes)[0] < 0x80) {
        sky_unicode_char_t  cp = ((uint8_t *)bytes)[0];

        return SKY_OBJECT_MAKETAG(0x3, cp);
    }

#if defined(SKY_ARCH_64BIT)
    /* 3. Double characters. Only ASCII range bytes, same as singles. */
    if (2 == nbytes && ((uint8_t *)bytes)[0] < 0x80 &&
                       ((uint8_t *)bytes)[1] < 0x80)
    {
        uintptr_t           extra;
        sky_unicode_char_t  cp = ((uint8_t *)bytes)[0],
                            cp2 = ((uint8_t *)bytes)[1];

        extra = ((uintptr_t)cp << 21ull) | (uintptr_t)cp2;
        return SKY_OBJECT_MAKETAG(0x4, extra);
    }
#endif

    /* 4. String literal look ups are the worst. */
    key_hash = sky_string_cache_literal_hash(bytes);
    key_struct.bytes  = bytes;
    key_struct.nbytes = nbytes;
    item_hazard = sky_lockfree_hashtable_lookup(&(cache_data->literal_table),
                                                key_hash,
                                                &key_struct);
    if (item_hazard) {
        if ((item = item_hazard->pointer) != NULL) {
            string = item->string;
            sky_hazard_pointer_release(item_hazard);
            return string;
        }
        sky_hazard_pointer_release(item_hazard);
    }

    return NULL;
}


sky_string_t
sky_string_cache_string(sky_string_cache_t  cache,
                        sky_string_t        string,
                        sky_bool_t          enter)
{
    sky_string_t        cached_string;
    sky_string_data_t   *string_data, tagged_data;

    /* 1. Tagged objects return themselves. */
    if (sky_object_istagged(string)) {
        return string;
    }
    string_data = sky_string_data(string, &tagged_data);

    /* 2. The empty string. */
    if (!string_data->len) {
        return sky_string_empty;
    }

    /* 3. Single characters. */
    if (1 == string_data->len) {
        sky_unicode_char_t  cp;

        if (string_data->highest_codepoint < 0x100) {
            cp = string_data->data.latin1[0];
        }
        else if (string_data->highest_codepoint < 0x10000) {
            cp = string_data->data.ucs2[0];
        }
        else {
            cp = string_data->data.ucs4[0];
        }
        return SKY_OBJECT_MAKETAG(0x3, cp);
    }

#if defined(SKY_ARCH_64BIT)
    /* 4. Double characters. */
    if (2 == string_data->len) {
        uintptr_t           extra;
        sky_unicode_char_t  cp, cp2;

        if (string_data->highest_codepoint < 0x100) {
            cp = string_data->data.latin1[0];
            cp2 = string_data->data.latin1[1];
        }
        else if (string_data->highest_codepoint < 0x10000) {
            cp = string_data->data.ucs2[0];
            cp2 = string_data->data.ucs2[1];
        }
        else {
            cp = string_data->data.ucs4[0];
            cp2 = string_data->data.ucs4[1];
        }
        extra = ((uintptr_t)cp << 21ull) | (uintptr_t)cp2;
        return SKY_OBJECT_MAKETAG(0x4, extra);
    }
#endif

    /* 5. Intern table lookup */
    cached_string = sky_string_cache_intern_lookup(cache, string);
    if (cached_string) {
        return cached_string;
    }
    if (enter) {
        return sky_string_cache_intern_add(cache, string);
    }

    return string;
}


SKY_TYPE_DEFINE_SIMPLE(string_cache,
                       "string_cache",
                       sizeof(sky_string_cache_data_t),
                       sky_string_cache_instance_initialize,
                       sky_string_cache_instance_finalize,
                       sky_string_cache_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_string_cache_initialize_library(void)
{
}
