/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_map Map Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MAP_H__
#define __SKYTHON_CORE_SKY_MAP_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new map object instance.
  *
  * @param[in]  function    the function to call for each iteration.
  * @param[in]  iterable    an iterable object.
  * @param[in]  ...         a variable length list of iterable objects, the
  *                         last of which must be @c NULL.
  * @return     a new map object instance.
  */
SKY_EXTERN sky_map_t
sky_map_create(sky_object_t function, sky_object_t iterable, ...);


/** Create a new map object instance.
  *
  * @param[in]  function    the function to call for each iteration.
  * @param[in]  iterable    an iterable object.
  * @param[in]  ap          a variable length list of iterable objects, the
  *                         last of which must be @c NULL.
  * @return     a new map object instance.
  */
SKY_EXTERN sky_map_t
sky_map_createv(sky_object_t function, sky_object_t iterable, va_list ap);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MAP_H__ */

/** @} **/
/** @} **/
