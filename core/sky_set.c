#include "sky_private.h"


/* These numbers assume using sky_core_malloc for memory management.
 * They also assume a cache-line size of 64 bytes. For both 32-bit and 64-bit
 * they'll end up working out to 256 bytes, leaving no waste except for the
 * cache-line padding, which cannot be avoided. Note that the numbers must also
 * be powers of 2.
 */


#if defined(SKY_ARCH_32BIT) && SKY_CACHE_LINE_SIZE == 64
#   define SKY_SET_MINIMUM_CAPACITY     16
#   define SKY_SET_TINY_CAPACITY        16
#elif defined(SKY_ARCH_64BIT) && SKY_CACHE_LINE_SIZE == 64
#   define SKY_SET_MINIMUM_CAPACITY     8
#   define SKY_SET_TINY_CAPACITY        8
#else
#   error implementation missing
#endif


static sky_type_t sky_set_iterator_type = NULL;


typedef struct sky_set_item_s {
    uintptr_t                           key_hash;
    sky_object_t                        key;
} sky_set_item_t;


typedef struct sky_set_table_s {
    ssize_t                             len;
    ssize_t                             filled;
    size_t                              mask;
    volatile uintptr_t                  revision;

    sky_set_item_t *                    table;

    /* Don't use SKY_CACHE_LINE_ALIGN here, because it'll screw up the
     * alignment in struct sky_set_s down below. Manage alignment manually.
     */
    char                                alignment[SKY_CACHE_LINE_SIZE -
                                                  (sizeof(ssize_t) * 2) -
                                                  sizeof(size_t) -
                                                  sizeof(uintptr_t) -
                                                  sizeof(sky_set_item_t *)];
    sky_spinlock_t                      spinlock;
} sky_set_table_t;


typedef struct sky_set_data_s {
    sky_set_table_t * volatile          table;

    /* Don't use SKY_CACHE_LINE_ALIGN here, because the type code is actually
     * placing this struct immediately after the sky_object_data_t struct in
     * the allocated object, but there's no way to tell the compiler this.
     * Manage alignment manually.
     */
    char                                alignment[SKY_CACHE_LINE_SIZE -
                                                  sizeof(sky_object_data_t) -
                                                  sizeof(sky_set_table_t *)];

    sky_set_item_t                      tiny_items[SKY_SET_TINY_CAPACITY];

    sky_set_table_t                     tiny_table;
} sky_set_data_t;

SKY_EXTERN_INLINE sky_set_data_t *
sky_set_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_set_type) {
        return (sky_set_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_set_type);
}


/* A table pointer is stored to check for changes during iteration, but it is
 * not otherwise used.
 */
typedef struct sky_set_iterator_data_s {
    sky_set_t                           set;
    sky_set_table_t *                   table;
    uintptr_t                           revision;
    size_t                              index;
    ssize_t                             len;
} sky_set_iterator_data_t;

SKY_EXTERN_INLINE sky_set_iterator_data_t *
sky_set_iterator_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_set_iterator_type) {
        return (sky_set_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_set_iterator_type);
}


struct sky_set_s {
    sky_object_data_t                   object_data;
    sky_set_data_t                      set_data;
};

typedef struct sky_set_iterator_s {
    sky_object_data_t                   object_data;
    sky_set_iterator_data_t             iterator_data;
} *sky_set_iterator_t;


static inline sky_bool_t
sky_set_isshared(sky_set_t self)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(self));
}


static inline sky_bool_t
sky_set_item_isalive(sky_set_item_t *item)
{
    return (item->key && sky_NotSpecified != item->key);
}


#define SKY_SET_FOREACH(__data, __table, _item)                             \
    do {                                                                    \
        sky_set_data_t  *_data = __data;                                    \
        sky_set_table_t *_table = __table;                                  \
                                                                            \
        size_t      _index;                                                 \
        ssize_t     _count = _table->len;                                   \
        uintptr_t   _revision = _table->revision;                           \
        sky_bool_t  _break = SKY_FALSE;                                     \
                                                                            \
        for (_index = 0; _count > 0; --_count) {                            \
            do {                                                            \
                _item = &(_table->table[_index++]);                         \
            } while (!sky_set_item_isalive(_item));                         \
            do {

#define SKY_SET_FOREACH_BREAK                                               \
                _break = SKY_TRUE;                                          \
                break

#define SKY_SET_FOREACH_END                                                 \
            } while (0);                                                    \
            if (_break) {                                                   \
                break;                                                      \
            }                                                               \
            if (_data->table != _table || _table->revision != _revision) {  \
                sky_error_raise_string(sky_RuntimeError,                    \
                                       "set modified during iteration");    \
            }                                                               \
        }                                                                   \
    } while (0)


#define SKY_SET_FOREACH_SHARED(__data, __table, _item)                      \
    do {                                                                    \
        sky_set_data_t  *_data = __data;                                    \
        sky_set_table_t *_table = __table;                                  \
                                                                            \
        size_t      _index, _mask = _table->mask;                           \
        ssize_t     _count = _table->len;                                   \
        uintptr_t   _revision = _table->revision;                           \
        sky_bool_t  _break = SKY_FALSE;                                     \
                                                                            \
        for (_index = 0; _count > 0; --_count) {                            \
            while (_index <= _mask) {                                       \
                _item = &(_table->table[_index++]);                         \
                if (_revision != _table->revision) {                        \
                    sky_error_raise_string(sky_RuntimeError,                \
                                           "set modified during iteration");\
                }                                                           \
                if (sky_set_item_isalive(_item)) {                          \
                    break;                                                  \
                }                                                           \
            }                                                               \
            do {

#define SKY_SET_FOREACH_SHARED_BREAK                                        \
                _break = SKY_TRUE;                                          \
                break

#define SKY_SET_FOREACH_SHARED_END                                          \
            } while (0);                                                    \
            if (_break) {                                                   \
                break;                                                      \
            }                                                               \
            if (_data->table != _table || _table->revision != _revision) {  \
                sky_error_raise_string(sky_RuntimeError,                    \
                                       "set modified during iteration");    \
            }                                                               \
        }                                                                   \
    } while (0)


#define PERTURB_SHIFT 5

static sky_set_item_t *
sky_set_data_lookup(sky_set_data_t *        set_data,
                    sky_hazard_pointer_t *  hazard,
                    uintptr_t               key_hash,
                    sky_object_t            key)
{
    size_t          i, mask, perturb;
    uintptr_t       revision;
    sky_bool_t      match;
    sky_set_item_t  *item, *tombstone;
    sky_set_table_t *table;

retry:
    sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(set_data->table)));
    table = hazard->pointer;
    revision = table->revision;
    mask = table->mask;

    i = (size_t)key_hash & mask;
    item = &(table->table[i]);
    if (!item->key || item->key == key) {
        return item;
    }
    if (sky_NotSpecified == item->key) {
        tombstone = item;
    }
    else {
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (table != set_data->table || revision != table->revision) {
                goto retry;
            }
            if (match) {
                return item;
            }
        }
        tombstone = NULL;
    }

    for (perturb = key_hash; ; perturb >>= PERTURB_SHIFT) {
        i = (i << 2) + i + perturb + 1;
        item = &(table->table[i & mask]);
        if (!item->key) {
            return (tombstone ? tombstone : item);
        }
        if (item->key == key) {
            return item;
        }
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (table != set_data->table || revision != table->revision) {
                goto retry;
            }
            if (match) {
                return item;
            }
        }
        if (sky_NotSpecified == item->key && !tombstone) {
            tombstone = item;
        }
    }

    sky_error_fatal("not reached");
}


static sky_set_table_t *
sky_set_table_create(size_t capacity)
{
    sky_set_table_t *table;

    capacity = sky_util_roundpow2(capacity);
    if (capacity < SKY_SET_MINIMUM_CAPACITY) {
        capacity = SKY_SET_MINIMUM_CAPACITY;
    }

    table = sky_calloc(1, sizeof(sky_set_table_t));
    table->mask = capacity - 1;
    table->table = sky_calloc(capacity, sizeof(sky_set_item_t));
    sky_spinlock_init(&(table->spinlock));

    return table;
}


static void
sky_set_table_destroy(sky_set_table_t *table)
{
    sky_free(table->table);
    sky_free(table);
}


static sky_set_table_t *
sky_set_data_table(sky_set_data_t *set_data)
{
    sky_hazard_pointer_t    *hazard;

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(set_data->table)));
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);
    return hazard->pointer;
}


static void
sky_set_data_settable(sky_set_data_t *  set_data,
                      sky_set_table_t * new_table,
                      sky_bool_t        shared)
{
    sky_set_table_t *old_table;

    if (!shared) {
        old_table = set_data->table;
        set_data->table = new_table;
    }
    else {
        old_table = sky_atomic_exchange(new_table,
                                        SKY_AS_VOIDP(&(set_data->table)));
    }
    if (old_table && &(set_data->tiny_table) != old_table) {
        sky_hazard_pointer_retire(old_table,
                                  (sky_free_t)sky_set_table_destroy);
    }
}


static sky_set_table_t *
sky_set_table_copy(sky_set_table_t *table, sky_set_t set, size_t capacity)
{
    size_t          i, mask, perturb;
    ssize_t         count, index, new_index;
    sky_set_item_t  *item, *new_item;
    sky_set_table_t *new_table;

    new_table = sky_set_table_create(capacity);
    sky_error_validate_debug(new_table->mask + 1 > (size_t)table->len * 3 / 2);
    new_table->len = table->len;
    new_table->filled = new_table->len;

    mask = new_table->mask;
    for (count = table->len, index = 0; count > 0; --count) {
        do {
            item = &(table->table[index++]);
        } while (!sky_set_item_isalive(item));

        i = new_index = (size_t)item->key_hash & mask;
        for (perturb = item->key_hash; ; perturb >>= PERTURB_SHIFT) {
            new_item = &(new_table->table[new_index]);
            if (!new_item->key) {
                break;
            }
            i = (i << 2) + i + perturb + 1;
            new_index = i & mask;
        }

        sky_object_gc_set(&(new_item->key), item->key, set);
        new_item->key_hash = item->key_hash;
    }

    return new_table;
}


static sky_bool_t
sky_set_table_insert_clean(sky_set_table_t **   table,
                           sky_set_t            set,
                           uintptr_t            key_hash,
                           sky_object_t         key)
{
    size_t          i, index, mask, new_capacity, perturb;
    uintptr_t       revision;
    sky_bool_t      match;
    sky_set_data_t  *set_data;
    sky_set_item_t  *item, *items;
    sky_set_table_t *new_table;

retry:
    revision = (*table)->revision;
    mask = (*table)->mask;
    items = (*table)->table;
    i = index = (size_t)key_hash & mask;
    for (perturb = key_hash; ; perturb >>= PERTURB_SHIFT) {
        item = &(items[index]);
        if (!item->key) {
            break;
        }
        if (item->key == key) {
            return SKY_FALSE;
        }
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (revision != (*table)->revision) {
                goto retry;
            }
            if (match) {
                return SKY_FALSE;
            }
        }
        i = (i << 2) + i + perturb + 1;
        index = i & mask;
    }

    sky_object_gc_set(&(item->key), key, set);
    item->key_hash = key_hash;
    ++(*table)->len;
    if ((size_t)++(*table)->filled > (mask + 1) * 2 / 3) {
        new_capacity = (mask + 1) * (mask + 1 >= 0x10000 ? 2 : 4);
        new_table = sky_set_table_copy(*table, set, new_capacity);
        set_data = sky_set_data(set);
        if (set_data->table == *table) {
            sky_set_data_settable(set_data, new_table, sky_set_isshared(set));
        }
        *table = new_table;
    }
    return SKY_TRUE;
}


static void
sky_set_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_set_data_t  *set_data = data;

    set_data->table = &(set_data->tiny_table);
    set_data->table->mask = SKY_SET_TINY_CAPACITY - 1;
    set_data->table->table = set_data->tiny_items;

    sky_spinlock_init(&(set_data->table->spinlock));
}


static void
sky_set_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_set_data_t  *set_data = data;

    if (&(set_data->tiny_table) != set_data->table) {
        sky_set_table_destroy(set_data->table);
    }
    set_data->table = NULL;
}


static void
sky_set_instance_iterate(sky_object_t                   self,
                         void *                         data,
                         sky_sequence_iterate_state_t * state,
                         sky_object_t *                 objects,
                         ssize_t                        nobjects)
{
    sky_set_data_t  *self_data = data;

    uintptr_t               revision;
    sky_set_item_t          *item;
    sky_set_table_t         *table;
    sky_hazard_pointer_t    *hazard;

    state->objects = objects;
    state->nobjects = 0;

    if (!state->state) {
        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        table = (sky_set_table_t *)hazard->pointer;
        if (!table->len) {
            sky_hazard_pointer_release(hazard);
            return;
        }
        revision = table->revision;

        state->extra[0] = (uintptr_t)hazard;
        state->extra[1] = (uintptr_t)table;
        state->extra[2] = revision;
        state->extra[3] = 0;
    }
    else {
        table = (sky_set_table_t *)state->extra[1];
        revision = state->extra[2];
    }

    if (self_data->table != table || revision != table->revision) {
        sky_error_raise_string(sky_RuntimeError,
                               "set modified during iteration");
    }
    if (state->state >= table->len) {
        return;
    }

    if (!sky_set_isshared(self)) {
        while (state->nobjects < nobjects) {
            do {
                item = &(table->table[state->extra[3]++]);
            } while (!sky_set_item_isalive(item));
            state->objects[state->nobjects++] = item->key;
            if (++state->state >= table->len) {
                return;
            }
        }
    }
    else {
        while (state->nobjects < nobjects) {
            while (state->extra[3] <= table->mask) {
                item = &(table->table[state->extra[3]++]);
                if (self_data->table != table || revision != table->revision) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "set modified during iteration");
                }
                if (sky_set_item_isalive(item)) {
                    break;
                }
            }
            state->objects[state->nobjects] = item->key;
            if (sky_NotSpecified == state->objects[state->nobjects]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "set modified during iteration");
            }
            ++state->nobjects;
            if (++state->state >= table->len) {
                return;
            }
        }
    }
}


static void
sky_set_instance_iterate_cleanup(
        SKY_UNUSED  sky_object_t                    self,
        SKY_UNUSED  void *                          data,
                    sky_sequence_iterate_state_t *  state)
{
    sky_hazard_pointer_t    *hazard;

    if ((hazard = (sky_hazard_pointer_t *)state->extra[0]) != NULL) {
        sky_hazard_pointer_release(hazard);
    }
}


static void
sky_set_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_set_data_t  *self_data = data;

    size_t                  index;
    ssize_t                 count;
    uintptr_t               revision;
    sky_set_item_t          *item;
    sky_set_table_t         *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(self_data->table)));
        table = hazard->pointer;
        revision = table->revision;

        for (count = table->len, index = 0; count > 0; --count) {
            while (index <= table->mask) {
                item = &(table->table[index++]);
                if (table != self_data->table ||
                    revision != table->revision)
                {
                    goto retry;
                }
                if (sky_set_item_isalive(item)) {
                    break;
                }
            }
            sky_object_visit(item->key, visit_data);
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_set_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_set_iterator_data_t *self_data = data;

    sky_object_visit(self_data->set, visit_data);
}


static void
sky_set_setcapacity(sky_set_t self, size_t capacity)
    /* Assumes that self_data->spinlock is locked (if necessary).
     * Must NOT call any external code at all.
     */
{
    sky_set_data_t  *self_data = sky_set_data(self);

    sky_set_table_t *new_table, *old_table;

    old_table = self_data->table;
    capacity = sky_util_roundpow2(capacity);
    if (capacity < SKY_SET_MINIMUM_CAPACITY) {
        capacity = SKY_SET_MINIMUM_CAPACITY;
    }
    if (capacity - 1 > SSIZE_MAX) {
        sky_error_raise_string(sky_OverflowError, "set too big");
    }
    if (old_table->filled == old_table->len &&
        old_table->mask == capacity - 1)
    {
        return;
    }

    new_table = sky_set_table_copy(old_table, self, capacity);
    sky_set_data_settable(self_data, new_table, sky_set_isshared(self));
}


static sky_object_t
sky_set_createwithtype(sky_object_t object, sky_type_t type)
{
    ssize_t         len;
    sky_set_t       set;
    uintptr_t       key_hash;
    sky_object_t    iter, key;
    sky_set_table_t *table;

    sky_error_validate_debug(sky_type_issubtype(type, sky_set_type));

    if (!sky_object_isiterable(object)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_sequence_isiterable(object)) {
        if (!(len = sky_object_len(object))) {
            return sky_set_create(NULL);
        }
        set = sky_object_allocate(type);
        sky_set_setcapacity(set, len);
        table = sky_set_data(set)->table;

        SKY_SEQUENCE_FOREACH(object, key) {
            key_hash = sky_object_hash(key);
            sky_set_table_insert_clean(&table, set, key_hash, key);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        len = sky_object_length_hint(object, 8);
        set = sky_object_allocate(type);
        sky_set_setcapacity(set, len * 3 / 2);
        table = sky_set_data(set)->table;

        iter = sky_object_iter(object);
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            key_hash = sky_object_hash(key);
            sky_set_table_insert_clean(&table, set, key_hash, key);
        }
    }

    return set;
}


sky_bool_t
sky_set_add(sky_set_t self, sky_object_t key)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    size_t                  new_capacity;
    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_set_item_t          *item;
    sky_set_table_t         *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_set_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        new_capacity = 0;
        if (sky_set_item_isalive(item)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (sky_object_type(key) == sky_string_type) {
                key = sky_string_intern(key);
            }
            if (sky_set_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (sky_set_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            if (!item->key) {
                ++table->filled;
                if ((size_t)table->filled + 1 > (table->mask + 1) * 2 / 3) {
                    new_capacity = (table->mask + 1) *
                                   (table->mask >= 0x10000 ? 2 : 4);
                }
            }
            sky_object_gc_set(&(item->key), key, self);
            item->key_hash = key_hash;
            ++table->len;
            ++table->revision;
            if (new_capacity) {
                sky_set_setcapacity(self, new_capacity);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_set_and(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_set_intersection(self, sky_tuple_pack(1, other));
}


void
sky_set_clear(sky_set_t self)
{
    sky_set_data_settable(sky_set_data(self),
                          sky_set_table_create(SKY_SET_MINIMUM_CAPACITY),
                          sky_set_isshared(self));
}


static sky_object_t
sky_set_compare(sky_set_t          self,
                sky_object_t       other,
                sky_compare_op_t   compare_op)
{
    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_set_type)) {
        return sky_NotImplemented;
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            if (sky_object_len(self) != sky_object_len(other)) {
                return sky_False;
            }
            return (sky_set_issubset(self, other) ? sky_True : sky_False);
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (sky_object_len(self) == sky_object_len(other)) {
                return sky_True;
            }
            return (sky_set_issubset(self, other) ? sky_False : sky_True);
        case SKY_COMPARE_OP_LESS:
            if (sky_object_len(self) >= sky_object_len(other)) {
                return sky_False;
            }
            return (sky_set_issubset(self, other) ? sky_True : sky_False);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (sky_set_issubset(self, other) ? sky_True : sky_False);
        case SKY_COMPARE_OP_GREATER:
            if (sky_object_len(self) <= sky_object_len(other)) {
                return sky_False;
            }
            return (sky_set_issubset(other, self) ? sky_True : sky_False);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (sky_set_issubset(other, self) ? sky_True : sky_False);

        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
            break;
    }

    sky_error_fatal("internal error");
}


sky_bool_t
sky_set_contains(sky_set_t self, sky_object_t key)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    sky_bool_t              result;
    sky_set_item_t          *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        item = sky_set_data_lookup(self_data,
                                   hazard,
                                   sky_object_hash(key),
                                   key);
        result = sky_set_item_isalive(item);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_set_t
sky_set_copy(sky_set_t self)
{
    return sky_set_createwithtype(self, sky_set_type);
}


sky_set_t
sky_set_create(sky_object_t object)
{
    sky_set_t   set;

    if (object) {
        set = sky_set_createwithtype(object, sky_set_type);
    }
    else {
        set = sky_object_allocate(sky_set_type);
        sky_set_setcapacity(set, SKY_SET_MINIMUM_CAPACITY);
    }

    return set;
}


sky_set_t
sky_set_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t         i;
    sky_set_t       set;
    uintptr_t       key_hash;
    sky_object_t    key;
    sky_set_table_t *table;

    set = sky_object_allocate(sky_set_type);
    sky_set_setcapacity(set, (size_t)count * 3 / 2);

    table = sky_set_data(set)->table;
    for (i = 0; i < count; ++i) {
        if (!(key = objects[i])) {
            key = sky_None;
        }
        else if (sky_object_type(key) == sky_string_type) {
            key = sky_string_intern(key);
        }

        key_hash = sky_object_hash(key);
        sky_set_table_insert_clean(&table, set, key_hash, key);
    }

    return set;
}


sky_set_t
sky_set_createwithcapacity(size_t capacity)
{
    sky_set_t   set;

    set = sky_object_allocate(sky_set_type);
    sky_set_setcapacity(set, capacity * 3 / 2);

    return set;
}


sky_bool_t
sky_set_delete(sky_set_t self, sky_object_t key)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_set_item_t          *item;
    sky_set_table_t         *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_set_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        if (!sky_set_item_isalive(item)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (sky_set_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (!sky_set_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }
            sky_object_gc_set(&(item->key), sky_NotSpecified, self);
            item->key_hash = 0;
            --table->len;
            ++table->revision;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_set_t
sky_set_difference(sky_set_t self, sky_tuple_t args)
{
    uintptr_t       key_hash;
    sky_set_t       new_set;
    sky_object_t    key, other;
    sky_set_table_t *table;

    switch (sky_tuple_len(args)) {
        case 0:
            return sky_set_copy(self);
        case 1:
            other = sky_tuple_get(args, 0);
            if (!sky_object_isa(other, sky_set_type) &&
                !sky_object_isa(other, sky_frozenset_type))
            {
                other = sky_set_createwithtype(other, sky_set_type);
            }
            break;
        default:
            other = sky_set_create(NULL);
            sky_set_update(other, args);
            break;
    }

    new_set = sky_object_allocate(sky_set_type);
    sky_set_setcapacity(new_set, sky_set_len(self) * 3 / 2);

    table = sky_set_data(new_set)->table;
    SKY_SEQUENCE_FOREACH(self, key) {
        if (!sky_object_contains(other, key)) {
            key_hash = sky_object_hash(key);
            sky_set_table_insert_clean(&table, new_set, key_hash, key);
        }
    } SKY_SEQUENCE_FOREACH_END;

    return new_set;
}


void
sky_set_difference_update(sky_set_t self, sky_tuple_t args)
{
    sky_object_t    iter, object, other;

    SKY_SEQUENCE_FOREACH(args, other) {
        if (sky_sequence_isiterable(other)) {
            SKY_SEQUENCE_FOREACH(other, object) {
                sky_set_delete(self, object);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(other);
            while ((object = sky_object_next(iter, NULL)) != NULL) {
                sky_set_delete(self, object);
            }
        }
    } SKY_SEQUENCE_FOREACH_END;
}


void
sky_set_discard(sky_set_t set, sky_object_t object)
{
    sky_set_delete(set, object);
}


sky_object_t
sky_set_eq(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_object_t
sky_set_ge(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_set_get(sky_set_t self, sky_object_t key)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    sky_object_t            result;
    sky_set_item_t          *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        item = sky_set_data_lookup(self_data,
                                   hazard,
                                   sky_object_hash(key),
                                   key);
        result = (sky_set_item_isalive(item) ? sky_True : NULL);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_set_gt(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_GREATER);
}


void
sky_set_init(sky_set_t self, sky_tuple_t args, sky_dict_t kws)
{
    if (sky_object_bool(kws)) {
        sky_error_raise_string(sky_TypeError,
                               "set() does not take keyword arguments");
    }
    if (!sky_object_bool(args)) {
        sky_set_clear(self);
        return;
    }
    if (sky_object_len(args) != 1) {
        sky_error_raise_format(sky_TypeError,
                               "set expected at most 1 arguments, got %zd\n",
                               sky_object_len(args));
    }

    sky_set_clear(self);
    sky_set_update(self, args);
}


sky_set_t
sky_set_intersection(sky_set_t self, sky_tuple_t args)
{
    uintptr_t       key_hash;
    sky_set_t       new_set;
    sky_object_t    key, other;
    sky_set_table_t *table;

    switch (sky_object_len(args)) {
        case 0:
            return sky_set_create(NULL);
        case 1:
            other = sky_tuple_get(args, 0);
            if (!sky_object_isa(other, sky_set_type) &&
                !sky_object_isa(other, sky_frozenset_type))
            {
                other = sky_set_createwithtype(other, sky_set_type);
            }
            break;
        default:
            other = sky_set_create(NULL);
            sky_set_update(other, args);
            break;
    }

    new_set = sky_object_allocate(sky_set_type);
    sky_set_setcapacity(new_set, sky_set_len(self) * 3 / 2);

    table = sky_set_data(new_set)->table;
    SKY_SEQUENCE_FOREACH(self, key) {
        if (sky_object_contains(other, key)) {
            key_hash = sky_object_hash(key);
            sky_set_table_insert_clean(&table, new_set, key_hash, key);
        }
    } SKY_SEQUENCE_FOREACH_END;

    return new_set;
}


void
sky_set_intersection_update(sky_set_t self, sky_object_t other)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    uintptr_t       key_hash;
    sky_object_t    key;
    sky_set_table_t *table;

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        other = sky_set_createwithtype(other, sky_set_type);
    }

    table = sky_set_table_create(sky_set_len(self) * 3 / 2);
    SKY_SEQUENCE_FOREACH(self, key) {
        if (sky_object_contains(other, key)) {
            key_hash = sky_object_hash(key);
            sky_set_table_insert_clean(&table, self, key_hash, key);
        }
    } SKY_SEQUENCE_FOREACH_END;

    sky_set_data_settable(self_data, table, sky_set_isshared(self));
}


sky_bool_t
sky_set_isdisjoint(sky_set_t self, sky_object_t other)
{
    sky_bool_t      result;
    sky_object_t    iter, key;

    if (self == other) {
        return (sky_set_len(self) ? SKY_FALSE : SKY_TRUE);
    }

    result = SKY_TRUE;

    if ((sky_object_isa(other, sky_set_type) ||
         sky_object_isa(other, sky_frozenset_type)) &&
        sky_object_len(other) > sky_set_len(self))
    {
        key = other;
        other = self;
        self = key;
    }

    if (sky_sequence_isiterable(other)) {
        SKY_SEQUENCE_FOREACH(other, key) {
            if (sky_object_contains(self, key)) {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(other);
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            if (sky_object_contains(self, key)) {
                result = SKY_FALSE;
                break;
            }
        }
    }

    return result;
}


sky_bool_t
sky_set_issubset(sky_set_t self, sky_object_t other)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    sky_bool_t      result;
    sky_set_item_t  *item;
    sky_set_table_t *table;

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        other = sky_set_createwithtype(other, sky_set_type);
    }

    SKY_ASSET_BLOCK_BEGIN {
        table = (sky_set_isshared(self) ? self_data->table
                                        : sky_set_data_table(self_data));

        if (table->len > sky_object_len(other)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (!sky_set_isshared(self)) {
                SKY_SET_FOREACH(self_data, table, item) {
                    if (!sky_object_contains(other, item->key)) {
                        result = SKY_FALSE;
                        SKY_SET_FOREACH_BREAK;
                    }
                } SKY_SET_FOREACH_END;
            }
            else {
                SKY_SET_FOREACH_SHARED(self_data, table, item) {
                    if (!sky_object_contains(other, item->key)) {
                        result = SKY_FALSE;
                        SKY_SET_FOREACH_SHARED_BREAK;
                    }
                } SKY_SET_FOREACH_SHARED_END;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bool_t
sky_set_issuperset(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type)) {
        other = sky_set_createwithtype(other, sky_set_type);
    }
    return sky_set_issubset(other, self);
}


sky_object_t
sky_set_iand(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    sky_set_intersection_update(self, other);
    return self;
}


sky_object_t
sky_set_ior(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    sky_set_update(self, sky_tuple_pack(1, other));
    return self;
}


sky_object_t
sky_set_isub(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    sky_set_difference_update(self, sky_tuple_pack(1, other));
    return self;
}


sky_object_t
sky_set_ixor(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    sky_set_symmetric_difference_update(self, other);
    return self;
}


sky_object_t
sky_set_iter(sky_set_t self)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    sky_set_table_t         *table;
    sky_set_iterator_t      iterator;
    sky_set_iterator_data_t *iterator_data;

    iterator = sky_object_allocate(sky_set_iterator_type);
    iterator_data = sky_set_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->set)), self, iterator);

    if (!sky_set_isshared(self)) {
        table = self_data->table;

        iterator_data->table = self_data->table;
        iterator_data->revision = table->revision;
        iterator_data->len = table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_set_data_table(self_data);

            iterator_data->table = table;
            iterator_data->revision = table->revision;
            iterator_data->len = table->len;
        } SKY_ASSET_BLOCK_END;
    }

    return iterator;
}


sky_object_t
sky_set_le(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


ssize_t
sky_set_len(sky_set_t self)
{
    ssize_t len;

    if (!sky_set_isshared(self)) {
        len = sky_set_data(self)->table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            len = sky_set_data_table(sky_set_data(self))->len;
        } SKY_ASSET_BLOCK_END;
    }

    return len;
}


sky_object_t
sky_set_lt(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_set_ne(sky_set_t self, sky_object_t other)
{
    return sky_set_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_object_t
sky_set_or(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_set_union(self, sky_tuple_pack(1, other));
}


sky_object_t
sky_set_pop(sky_set_t self)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    size_t          index, mask;
    uintptr_t       revision;
    sky_object_t    key;
    sky_set_item_t  *item;
    sky_set_table_t *table;

    key = NULL;
    if (!sky_set_isshared(self)) {
        table = self_data->table;
        if (table->len) {
            index = 0;
            do {
                item = &(table->table[index++]);
            } while (!sky_set_item_isalive(item));

            key = item->key;
            sky_object_gc_set(&(item->key), sky_NotSpecified, self);
            item->key_hash = 0;
            --table->len;
            ++table->revision;
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_set_data_table(self_data);
            if (table->len) {
                revision = table->revision;
                mask = table->mask;
                for (index = 0; index <= mask; ++index) {
                    item = &(table->table[index]);
                    if (table != self_data->table ||
                        revision != table->revision)
                    {
                        sky_error_raise_string(
                                sky_RuntimeError,
                                "set modified during iteration");
                    }
                    if (sky_set_item_isalive(item)) {
                        sky_spinlock_lock(&(table->spinlock));
                        if (!sky_set_item_isalive(item)) {
                            sky_spinlock_unlock(&(table->spinlock));
                            sky_error_raise_string(
                                    sky_RuntimeError,
                                    "set modified during iteration");
                        }
                        key = item->key;
                        sky_object_gc_set(&(item->key), sky_NotSpecified, self);
                        item->key_hash = 0;
                        --table->len;
                        ++table->revision;
                        sky_spinlock_unlock(&(table->spinlock));
                        break;
                    }
                }
            }
        } SKY_ASSET_BLOCK_END;
    }

    if (!key) {
        sky_error_raise_string(sky_KeyError, "pop from an empty set");
    }
    return key;
}


sky_object_t
sky_set_reduce(sky_set_t self)
{
    sky_tuple_t     args;
    sky_object_t    dict;

    args = sky_tuple_pack(1, sky_list_create(self));
    dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), sky_None);

    return sky_tuple_pack(3, sky_object_type(self), args, dict);
}


void
sky_set_remove(sky_set_t self, sky_object_t object)
{
    if (!sky_set_delete(self, object)) {
        sky_error_raise_object(sky_KeyError, object);
    }
}


sky_bool_t
sky_set_replace(sky_set_t set, sky_object_t key)
{
    return sky_set_contains(set, key);
}


sky_string_t
sky_set_repr(sky_object_t self)
{
    sky_type_t  type = sky_object_type(self);

    ssize_t                 count, length;
    sky_object_t            item;
    sky_string_t            joiner;
    sky_set_table_t         *table;
    sky_string_builder_t    builder;

    if (sky_object_stack_push(self)) {
        return sky_string_createfromformat("%@(...)", sky_type_name(type));
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        table = sky_set_data_table(sky_set_data(self));
        builder = sky_string_builder_create();

        if (!(length = table->len)) {
            sky_string_builder_append(builder, sky_type_name(type));
            sky_string_builder_appendbytes(builder, "()", 2, NULL, NULL);
        }
        else {
            if (sky_object_type(self) != sky_set_type) {
                sky_string_builder_append(builder, sky_type_name(type));
                sky_string_builder_appendcodepoint(builder, '(', 1);
            }
            sky_string_builder_appendcodepoint(builder, '{', 1);

            count = 0;
            joiner = SKY_STRING_LITERAL(", ");

            /* SKY_SEQUENCE_FOREACH will guard against sky_NotSpecified getting
             * returned.
             */
            SKY_SEQUENCE_FOREACH(self, item) {
                sky_string_builder_append(builder, sky_object_repr(item));
                if (++count < length) {
                    sky_string_builder_append(builder, joiner);
                }
            } SKY_SEQUENCE_FOREACH_END;

            sky_string_builder_appendcodepoint(builder, '}', 1);
            if (sky_set_type != type) {
                sky_string_builder_appendcodepoint(builder, ')', 1);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


sky_object_t
sky_set_rsub(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_set_difference(other, sky_tuple_pack(1, self));
}


static size_t
sky_set_sizeof(sky_set_t self)
{
    sky_set_data_t  *self_data = sky_set_data(self);

    size_t          size;
    sky_set_table_t *table;
    
    size = sky_memsize(self);
    if (!sky_set_isshared(self)) {
        table = self_data->table;
        if (&(self_data->tiny_table) != table) {
            size += sky_memsize(table) + sky_memsize(table->table);
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_set_data_table(self_data);
            if (&(self_data->tiny_table) != table) {
                size += sky_memsize(table) + sky_memsize(table->table);
            }
        } SKY_ASSET_BLOCK_END;
    }

    return size;
}


sky_object_t
sky_set_sub(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_set_difference(self, sky_tuple_pack(1, other));
}


sky_set_t
sky_set_symmetric_difference(sky_set_t self, sky_object_t other)
{
    other = sky_set_createwithtype(other, sky_set_type);
    sky_set_symmetric_difference_update(other, self);

    return other;
}


void
sky_set_symmetric_difference_update(sky_set_t self, sky_object_t other)
{
    sky_object_t    key;

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        other = sky_set_createwithtype(other, sky_set_type);
    }

    SKY_SEQUENCE_FOREACH(other, key) {
        if (!sky_set_delete(self, key)) {
            sky_set_add(self, key);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


sky_set_t
sky_set_union(sky_set_t self, sky_tuple_t args)
{
    sky_set_t   new_set;

    new_set = sky_set_copy(self);
    sky_set_update(new_set, args);

    return new_set;
}


void
sky_set_union_update(sky_set_t self, sky_object_t other)
{
    sky_object_t    iter, object;

    if (sky_sequence_isiterable(other)) {
        SKY_SEQUENCE_FOREACH(other, object) {
            sky_set_add(self, object);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(other);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            sky_set_add(self, object);
        }
    }
}


void
sky_set_update(sky_set_t self, sky_tuple_t args)
{
    sky_object_t    source;

    SKY_SEQUENCE_FOREACH(args, source) {
        sky_set_union_update(self, source);
    } SKY_SEQUENCE_FOREACH_END;
}


sky_object_t
sky_set_xor(sky_set_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_set_symmetric_difference(self, sky_tuple_pack(1, other));
}


static sky_object_t
sky_set_iterator_iter(sky_object_t self)
{
    return self;
}


static ssize_t
sky_set_iterator_len(sky_object_t self)
{
    return sky_set_iterator_data(self)->len;
}


static sky_object_t
sky_set_iterator_next(sky_object_t self)
{
    sky_set_iterator_data_t *self_data = sky_set_iterator_data(self);

    sky_set_t       set;
    sky_object_t    key;
    sky_set_data_t  *set_data;
    sky_set_item_t  *item;
    sky_set_table_t *table;
    
    if (!(set = self_data->set)) {
        return NULL;
    }

    set_data = sky_set_data(set);
    if (!sky_set_isshared(set)) {
        table = set_data->table;
        if (table != self_data->table ||
            table->revision != self_data->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "set modified during iteration");
        }
        if (!self_data->len) {
            self_data->set = NULL;
            return NULL;
        }

        do {
            item = &(table->table[self_data->index++]);
        } while (!sky_set_item_isalive(item));

        --self_data->len;
        return item->key;
    }

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_set_data_table(set_data);
        if (table != self_data->table ||
            table->revision != self_data->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "set modified during iteration");
        }
        if (!self_data->len) {
            self_data->set = NULL;
            key = NULL;
        }
        else {
            while (self_data->index <= table->mask) {
                item = &(table->table[self_data->index++]);
                if (table != self_data->table ||
                    table->revision != self_data->revision)
                {
                    sky_error_raise_string(sky_RuntimeError,
                                           "set modified during iteration");
                }
                /* inline sky_set_item_isalive(item) to guard against it
                 * changing and possibly returning sky_NotSpecified to the
                 * caller.
                 */
                key = item->key;
                if (key && sky_NotSpecified != key) {
                    --self_data->len;
                    break;
                }
            }
        }
    } SKY_ASSET_BLOCK_END;

    return key;
}


static sky_object_t
sky_set_iterator_reduce(sky_object_t self)
{
    sky_set_iterator_data_t *self_data = sky_set_iterator_data(self);

    size_t          index;
    ssize_t         len;
    sky_set_t       set;
    sky_list_t      list;
    sky_object_t    key;
    sky_set_data_t  *set_data;
    sky_set_item_t  *item;
    sky_set_table_t *table;

    if (!(set = self_data->set)) {
        return sky_object_build("(O([]))", sky_module_getbuiltin("iter"));
    }

    /* Iterate into a temporary list, preserving original iterator state. */
    len = self_data->len;
    index = self_data->index;
    set_data = sky_set_data(set);
    list = sky_list_createwithcapacity(len);

    if (!sky_set_isshared(self)) {
        table = set_data->table;
        if (table != self_data->table ||
            table->revision != self_data->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "set modified during iteration");
        }

        while (len > 0) {
            do {
                item = &(table->table[index++]);
            } while (!sky_set_item_isalive(item));

            --len;
            sky_list_append(list, item->key);
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_set_data_table(set_data);
            while (len > 0) {
                while (index <= table->mask) {
                    item = &(table->table[index++]);
                    if (table != self_data->table ||
                        table->revision != self_data->revision)
                    {
                        sky_error_raise_string(sky_RuntimeError,
                                               "set modified during iteration");
                    }
                    /* inline sky_set_item_isalive(item) to guard against it
                     * chagning and possibly returning sky_NotSpecified to the
                     * caller.
                     */
                    key = item->key;
                    if (key && sky_NotSpecified != key) {
                        break;
                    }
                }

                --len;
                sky_list_append(list, key);
            }
        } SKY_ASSET_BLOCK_END;
    }

    return sky_object_build("(O(O))", sky_module_getbuiltin("iter"), list);
}


static const char sky_set_type_doc[] =
"set() -> new empty set object\n"
"set(iterable) -> new set object\n"
"\n"
"Built an unordered collection of unique elements.";


SKY_TYPE_DEFINE_ITERABLE(set,
                         "set",
                         sizeof(sky_set_data_t),
                         sky_set_instance_initialize,
                         sky_set_instance_finalize,
                         sky_set_instance_visit,
                         sky_set_instance_iterate,
                         sky_set_instance_iterate_cleanup,
                         0,
                         sky_set_type_doc);


void
sky_set_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_type_setattr_builtin(
            sky_set_type,
            "add",
            sky_function_createbuiltin(
                    "set.add",
                    "Add an element to a set.\n\n"
                    "This has no effect if the element is already present.",
                    (sky_native_code_function_t)sky_set_add,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "clear",
            sky_function_createbuiltin(
                    "set.clear",
                    "Remove all elements from this set.",
                    (sky_native_code_function_t)sky_set_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "copy",
            sky_function_createbuiltin(
                    "set.copy",
                    "Return a shallow copy of a set.",
                    (sky_native_code_function_t)sky_set_copy,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "difference",
            sky_function_createbuiltin(
                    "set.difference",
                    "Return the difference of two or more sets as a new set.\n\n"
                    "(i.e. all elements that are in this set but not the others.)",
                    (sky_native_code_function_t)sky_set_difference,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "difference_update",
            sky_function_createbuiltin(
                    "set.difference_update",
                    "Remove all elements of another set from this set.",
                    (sky_native_code_function_t)sky_set_difference_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "discard",
            sky_function_createbuiltin(
                    "set.remove",
                    "Remove an element from a set if it is a member.\n\n"
                    "If the element is not a member, do nothing.",
                    (sky_native_code_function_t)sky_set_discard,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "intersection",
            sky_function_createbuiltin(
                    "set.intersection",
                    "Return the intersection of two sets as a new set.\n\n"
                    "(i.e. all elements that are in both sets.)",
                    (sky_native_code_function_t)sky_set_intersection,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "intersection_update",
            sky_function_createbuiltin(
                    "set.intersection_update",
                    "Update a set with the intersection of itself and another.",
                    (sky_native_code_function_t)sky_set_intersection_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "isdisjoint",
            sky_function_createbuiltin(
                    "set.isdisjoint",
                    "Return True if two sets have a null intersection.",
                    (sky_native_code_function_t)sky_set_isdisjoint,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "issubset",
            sky_function_createbuiltin(
                    "set.issubset",
                    "Report whether another set contains this set.",
                    (sky_native_code_function_t)sky_set_issubset,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "issuperset",
            sky_function_createbuiltin(
                    "set.issuperset",
                    "Report whether this set contains another set.",
                    (sky_native_code_function_t)sky_set_issuperset,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "pop",
            sky_function_createbuiltin(
                    "set.pop",
                    "Remove and return an arbitrary set element."
                    "Raises KeyError if the set is empty.",
                    (sky_native_code_function_t)sky_set_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "remove",
            sky_function_createbuiltin(
                    "set.remove",
                    "Remove an element from a set; it must be a member.\n\n"
                    "If the element is not a member, raise a KeyError.",
                    (sky_native_code_function_t)sky_set_remove,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "object", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "symmetric_difference",
            sky_function_createbuiltin(
                    "set.symmetric_difference",
                    "Return the symmetric difference of two sets as a new set.\n\n"
                    "(i.e. all elements that are in exactly one of the sets.)",
                    (sky_native_code_function_t)sky_set_symmetric_difference,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "symmetric_difference_update",
            sky_function_createbuiltin(
                    "set.symmetric_difference_update",
                    "Update a set with the symmetric difference of itself and another.",
                    (sky_native_code_function_t)sky_set_symmetric_difference_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "union",
            sky_function_createbuiltin(
                    "set.union",
                    "Return the union of sets as a new set.\n\n"
                    "(i.e. all elements that are in either set.)",
                    (sky_native_code_function_t)sky_set_union,
                    SKY_DATA_TYPE_OBJECT_SET,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_set_type,
            "update",
            sky_function_createbuiltin(
                    "set.update",
                    "Update a set with the union of itself and others.",
                    (sky_native_code_function_t)sky_set_update,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_SET, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));

    sky_type_setattr_builtin(sky_set_type, "__hash__", sky_None);
    sky_type_setmethodslots(sky_set_type,
            "__init__", sky_set_init,
            "__repr__", sky_set_repr,
            "__lt__", sky_set_lt,
            "__le__", sky_set_le,
            "__eq__", sky_set_eq,
            "__ne__", sky_set_ne,
            "__gt__", sky_set_gt,
            "__ge__", sky_set_ge,
            "__sizeof__", sky_set_sizeof,
            "__len__", sky_set_len,
            "__iter__", sky_set_iter,
            "__contains__", sky_set_contains,
            "__sub__", sky_set_sub,
            "__and__", sky_set_and,
            "__xor__", sky_set_xor,
            "__or__", sky_set_or,
            "__rsub__", sky_set_rsub,
            "__rand__", sky_set_and,
            "__rxor__", sky_set_xor,
            "__ror__", sky_set_or,
            "__isub__", sky_set_isub,
            "__iand__", sky_set_iand,
            "__ixor__", sky_set_ixor,
            "__ior__", sky_set_ior,
            "__reduce__", sky_set_reduce,
            NULL);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_set_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("set_iterator"),
                                   NULL,
                                   0,
                                   sizeof(sky_set_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__len__", sky_set_iterator_len,
            "__iter__", sky_set_iterator_iter,
            "__next__", sky_set_iterator_next,
            "__reduce__", sky_set_iterator_reduce,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_set_iterator_type),
            type,
            SKY_TRUE);
}
