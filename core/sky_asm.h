#ifndef __SKYTHON_CORE_SKY_ASM_H__
#define __SKYTHON_CORE_SKY_ASM_H__ 1


#if defined(__APPLE__)
#   define  SKY_ASM_MANGLE(__symbol)            _##__symbol
#   define  SKY_ASM_TYPE(__symbol, __type)
#   define  SKY_ASM_SIZE(__symbol)
#elif defined(_WIN32)
#else
#   define  SKY_ASM_MANGLE(__symbol)            __symbol
#   define  SKY_ASM_TYPE(__symbol, __type)      .type __symbol, __type
#   define  SKY_ASM_SIZE(__symbol)              .size __symbol, .-__symbol
#endif


#endif  /* __SKYTHON_CORE_SKY_ASM_H__ */
