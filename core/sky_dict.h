/** @file
  * @brief
  * @defgroup containers Containers
  * @{
  * @defgroup sky_dict Key/Value Dictionaries
  * @{
  */

#ifndef __SKYTHON_SKY_DICT_H__
#define __SKYTHON_SKY_DICT_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new dict object instance.
  *
  * @return     the new dict object instance.
  */
SKY_EXTERN sky_dict_t
sky_dict_create(void);


/** Create a new dict object instance from an array of key/value pairs.
  * The array of key value pairs is expected to be ordered key, value, ...,
  * key, value. The size of the array must be even. The resulting length of
  * the dict will be half the size of the array.
  *
  * @param[in]  count   the number of objects present in the @a objects list.
  * @param[in]  objects the objects to be used as key/value pairs. Any @c NULL
  *                     pointers will be replaced with @c sky_None objects.
  * @return     the new dict object instance.
  */
SKY_EXTERN sky_dict_t
sky_dict_createfromarray(ssize_t count, sky_object_t *objects);


/** Create a new dict object instance.
  *
  * @param[in]  capacity    the number of items that the dict will initially
  *                         be able to store without the need to grow. If
  *                         specified as 0, a default initial capacity will be
  *                         used.
  * @return     the new dict object instance.
  */
SKY_EXTERN sky_dict_t
sky_dict_createwithcapacity(size_t capacity);


/** Add a key/value pair to a dict if the key does not already exist.
  *
  * @param[in]  dict    the dict into which the key/value pair is to be added.
  * @param[in]  key     the key to be be added.
  * @param[in]  value   the value to be added.
  * @return     @c SKY_TRUE if the key/value pair was added to the dict, or
  *             @c SKY_FALSE if the key already exists in the dict.
  */
SKY_EXTERN sky_bool_t
sky_dict_add(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Remove all key/value pairs from a dict.
  * Removes all key/value pairs from a dict, leaving it empty. The dict itself
  * is not shrunk.
  *
  * @param[in]  dict    the dict to be cleared.
  */
SKY_EXTERN void
sky_dict_clear(sky_dict_t dict);


/** Determine if a key exists in a dict.
  *
  * @param[in]  dict    the dict to be queried.
  * @param[in]  key     the key to query.
  * @return     @c SKY_TRUE if @a key exists in @a dict, or @c SKY_FALSE if
  *             it does not.
  */
SKY_EXTERN sky_bool_t
sky_dict_contains(sky_dict_t dict, sky_object_t key);


/** Make a shallow copy of a dict.
  *
  * @param[in]  dict    the dict to be copied.
  */
SKY_EXTERN sky_dict_t
sky_dict_copy(sky_dict_t dict);


/** Delete a key/value pair from a dict.
  *
  * @param[in]  dict    the dict from which the key is to be deleted.
  * @param[in]  key     the key to be deleted.
  * @return     @c SKY_TRUE if the key was removed, or @c SKY_FALSE if it did
  *             not exist in the dict.
  */
SKY_EXTERN sky_bool_t
sky_dict_delete(sky_dict_t dict, sky_object_t key);


/** Delete a key/value pair from a dict.
  * If the key does not exist in the dict, @c sky_KeyError will be raised.
  *
  * @param[in]  dict    the dict form which the key is to be deleted.
  * @param[in]  key     the key to be deleted.
  */
SKY_EXTERN void
sky_dict_delitem(sky_dict_t dict, sky_object_t key);


/** Create a new dict from a sequence of keys.
  * A new object of type @a cls (which must be @c sky_dict_type or a sub-type
  * of it) is created and populated with the keys from @a seq with the value
  * for each key is set to @a value.
  *
  * @param[in]  cls     the type of dict to create.
  * @param[in]  seq     the sequence from which keys are to be obtained.
  * @param[in]  value   the value to use for each key in the new dict.
  * @return     the new dict object instance.
  */
SKY_EXTERN sky_object_t
sky_dict_fromkeys(sky_type_t cls, sky_object_t seq, sky_object_t value);


/** Return the value associated with a key in a dict.
  *
  * @param[in]  dict    the dict to query.
  * @param[in]  key     the key to be queried.
  * @param[in]  value   the default value to return if @a key is not in @a dict.
  * @return     the value associated with the key, or @a value if the key does
  *             not exist in @a dict.
  */
SKY_EXTERN sky_object_t
sky_dict_get(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Return the value associated with a key in a dict.
  * If @a key does not exist in @a dict, a @c sky_KeyError exception will be
  * raised.
  *
  * @param[in]  dict    the dict to query.
  * @param[in]  key     the key to be queried.
  * @return     the value associated with the key.
  */
SKY_EXTERN sky_object_t
sky_dict_getitem(sky_dict_t dict, sky_object_t key);


/** Initialize a dict object instance.
  * This is the implementation of __init__().
  *
  * @param[in]  dict    the dict to initialize.
  * @param[in]  args    positional arguments.
  * @param[in]  kws     keyword arguments.
  */
SKY_EXTERN void
sky_dict_init(sky_dict_t dict, sky_tuple_t args, sky_dict_t kws);


/** Return a set-like object that provides a view of a dict's items.
  *
  * @param[in]  dict    the dict to be viewed.
  * @return     a new set-like object providing a view of @a dict's items.
  */
SKY_EXTERN sky_object_t
sky_dict_items(sky_dict_t dict);


/** Return a set-like object that provides a view of a dict's keys.
  *
  * @param[in]  dict    the dict to be viewed.
  * @return     a new set-like object providing a view of @a dict's keys.
  */
SKY_EXTERN sky_object_t
sky_dict_keys(sky_dict_t dict);


/** Return the number of key/value pairs stored in a dict.
  *
  * @param[in]  dict    the dict to query.
  * @return     the number of key/value pairs stored in @a dict.
  */
SKY_EXTERN ssize_t
sky_dict_len(sky_dict_t dict);


/** Merge a dictionary or sequence of key/value pairs into an existing
  * dictionary.
  * If @a other is a dictionary type, it will be merged with @a dict, possibly
  * overwriting existing keys. If @a other is a sequence type, it must have an
  * even number of items, and the key/value pairs merged into @a dict will be
  * taken from alternating index positions. For example, ["red", 1, "blue", 2]
  * will merge two key/value pairs: {"red": 1, "blue": 2}.
  *
  * @param[in]  dict        the dict into which @a other will be merged.
  * @param[in]  other       the dict or sequence to merge into @a dict.
  * @param[in]  overwrite   @c SKY_TRUE if keys from @a other should over-write
  *                         existing keys in @a dict.
  */
SKY_EXTERN void
sky_dict_merge(sky_dict_t dict, sky_object_t other, sky_bool_t overwrite);


/** Remove a key/value pair from a dict and return the value.
  *
  * @param[in]  dict    the dict on which to operate.
  * @param[in]  key     the key to remove from the dict.
  * @param[in]  value   the value to return if @a key is not in @a dict. If
  *                     @c NULL or @c sky_NotSpecified, @c sky_KeyError will
  *                     be raised if @a key is not in @a dict.
  * @return     the value associated with the key removed from @a dict, or
  *             @a value if @a key wasn't in @a dict.
  */
SKY_EXTERN sky_object_t
sky_dict_pop(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Remove an arbitrary key/value pair from a dict and return it.
  * If @a dict is empty, @c sky_KeyError will be raised; otherwise, a tuple
  * will be returned containing (key, value) for the key/value pair that was
  * removed from @a dict.
  *
  * @param[in]  dict    the dict from which an arbitrary key/value pair is to
  *                     be removed and returned.
  */
SKY_EXTERN sky_tuple_t
sky_dict_popitem(sky_dict_t dict);


/** Replace a key/value pair in dict, but only if the key already exists.
  *
  * @param[in]  dict    the dict into which the key/value pair is to be stored.
  * @param[in]  key     the key to be stored.
  * @param[in]  value   the value to be stored.
  * @return     @c SKY_TRUE if the value for the key was replaced, or
  *             @c SKY_FALSE if the key did not already exist in the dict.
  */
SKY_EXTERN sky_bool_t
sky_dict_replace(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Return a parseable representation of a dict as a string.
  *
  * @param[in]  dict    the dict to represent.
  * @return     a new string object that is a parseable representation of
  *             @a dict.
  */
SKY_EXTERN sky_string_t
sky_dict_repr(sky_dict_t dict);


/** Return the value associated with a key in a dict, setting it to a default
  * value if it's not present.
  *
  * @param[in]  dict    the dict to query.
  * @param[in]  key     the key to be queried.
  * @param[in]  value   the value to set if @a key is not in @a dict. Must not
  *                     be @a NULL.
  */
SKY_EXTERN sky_object_t
sky_dict_setdefault(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Set a key/value pair in a dict, adding it if the key does not already exist.
  *
  * @param[in]  dict    the dict into which the key/value pair is to be stored.
  * @param[in]  key     the key to store.
  * @param[in]  value   the value to store.
  * @return     @c SKY_TRUE if the key was added, or @c SKY_FALSE if the key
  *             already existed in the dict.
  */
SKY_EXTERN sky_bool_t
sky_dict_setitem(sky_dict_t dict, sky_object_t key, sky_object_t value);


/** Update the contents of a dict with key/value pairs from other objects.
  * If @a args is not @c NULL, it must be a @c tuple containing a single object
  * that is either another dictionary object instance or an iterable of
  * key/value pairs (as tuples or other iterables of length two). If @a kws is
  * not @c NULL, its content will be merged into @a dict. In all cases, keys
  * pre-existing in @a dict will be over-written by keys found in either
  * @a args or @a kws.
  *
  * @param[in]  dict    the dict to be updated.
  * @param[in]  args    an optional tuple containing a single object that will
  *                     provide key/value pairs to update @a dict.
  * @param[in]  kws     an optional dict to be merged into @a dict.
  */
SKY_EXTERN void
sky_dict_update(sky_dict_t dict, sky_tuple_t args, sky_dict_t kws);


/** Return an object that provides a view of a dict's values.
  *
  * @param[in]  dict    the dict to be viewed.
  * @return     a new object instance providing a view of @a dict's values.
  */
SKY_EXTERN sky_object_t
sky_dict_values(sky_dict_t dict);


SKY_CDECLS_END


#endif  /* __SKYTHON_SKY_DICT_H__ */

/** @} **/
/** @} **/
