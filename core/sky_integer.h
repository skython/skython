/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_integer Integer Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_INTEGER_H__
#define __SKYTHON_CORE_SKY_INTEGER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An integer object instance that is the number 1. **/
SKY_EXTERN sky_integer_t const sky_integer_one;

/** An integer object instance that is the number 0. **/
SKY_EXTERN sky_integer_t const sky_integer_zero;


/** Return the absolute value of an integer.
  * This is equivalent to the expression abs(integer).
  *
  * @param[in]  integer     the integer for which the absolute value is to be
  *                         returned.
  * @return     the absolute value of @a integer, which may be @a integer
  *             itself if it is not negative.
  */
SKY_EXTERN sky_object_t
sky_integer_abs(sky_integer_t integer);


/** Return the sum of two integers.
  * This is equivalent to the expression integer + other.
  *
  * @param[in]  integer     the first integer to be added.
  * @param[in]  other       the second integer to be added.
  * @return     the sum of @a integer and @a other, or @c sky_NotImplemented
  *             if either object is not an integer object.
  */
SKY_EXTERN sky_object_t
sky_integer_add(sky_integer_t integer, sky_object_t other);


/** Return the bitwise-and of two integers.
  * This is equivalent to the expression integer & other.
  *
  * @param[in]  integer     the first integer to be bitwise-anded.
  * @param[in]  other       the second integer to be bitwise-anded.
  * @return     the bitwise-and of @a integer and @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_and(sky_integer_t integer, sky_object_t other);


/** Return the number of bits required to represent an integer.
  *
  * @param[in]  self        the integer for which the number of bits required
  *                         to represent it are to be returned.
  * @return     the number of bits required to represent @a self.
  */
SKY_EXTERN ssize_t
sky_integer_bit_length(sky_integer_t self);


/** Make a copy of an integer object, converting it to a pure integer object if
  * it is an instance of a subtype.
  *
  * @param[in]  integer     the integer object to copy.
  * @return     an integer object instance that is a copy of @a integer.
  */
SKY_EXTERN sky_integer_t
sky_integer_copy(sky_integer_t integer);


/** Create a new integer object instance from a signed value. **/
SKY_EXTERN sky_integer_t
sky_integer_create(intmax_t value);


/** Create a new integer object from an array of ASCII characters. **/
SKY_EXTERN sky_integer_t
sky_integer_createfromascii(const char *bytes, size_t nbytes, int base);


/** Create a new integer object from an array of binary bytes.
  *
  * @param[in]  bytes           the binary bytes to use.
  * @param[in]  nbytes          the number of bytes present in @a bytes.
  * @param[in]  little_endian   @c SKY_TRUE if the bytes are arranged in little
  *                             endian order, or @c SKY_FALSE if they are
  *                             arranged in big endian order.
  * @param[in]  is_signed       @c SKY_TRUE if the bits are signed using two's
  *                             complement, or @c SKY_FALSE if the data is
  *                             unsigned.
  * @return     the new object instance created from @a bytes.
  */
SKY_EXTERN sky_integer_t
sky_integer_createfrombytes(const void *bytes,
                            ssize_t     nbytes,
                            sky_bool_t  little_endian,
                            sky_bool_t  is_signed);


/** Create an integer from a double value. **/
SKY_EXTERN sky_integer_t
sky_integer_createfromdouble(double value);


/** Create an integer from a string.
  *
  * @param[in]  source      the string to parse, which may be a string object
  *                         or any object supporting the buffer protocol.
  * @param[in]  base        the base to parse, which must be from 2 to 36, or
  *                         it may be 0 to figure out the base to use from the
  *                         source (e.g., leading "0x" will parse as base 16).
  * @return     a new integer object instance.
  */
SKY_EXTERN sky_integer_t
sky_integer_createfromstring(sky_object_t source, int base);


/** Create a new integer object instance from an unsigned value. **/
SKY_EXTERN sky_integer_t
sky_integer_createfromunsigned(uintmax_t value);


/** Return the quotient and the remainder resulting from the division of two
  * integers.
  *
  * @param[in]  integer     the first integer to be divided.
  * @param[in]  other       the second integer to be divided.
  * @return     a tuple containing the quotient of @a integer divided by
  *             @a other and the remainder of @a integer divided by @a other,
  *             or @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_divmod(sky_integer_t integer, sky_object_t other);


/** Implementation of the __eq__() method for integer objects. **/
SKY_EXTERN sky_object_t
sky_integer_eq(sky_integer_t integer, sky_object_t other);


/** Export the internal representation of an integer to a binary buffer.
  *
  * @param[in]  self        the integer to be exported.
  * @param[in]  bytes       the buffer into which the binary representation of
  *                         @a self will be written.
  * @param[in]  nbytes      the size of @a bytes, which must be enough space
  *                         to hold the binary representation of @a self. Any
  *                         extra bytes will be padded with 0 bytes.
  * @param[in]  little_endian   @c SKY_TRUE if the binary representation should
  *                             be written in little endian, or @c SKY_FALSE if
  *                             it should be written in big endian.
  * @param[in]  is_signed   @c SKY_TRUE to write negative values for @a self as
  *                         two's complement; otherwise, raise an exception if
  *                         @a self is negative.
  */
SKY_EXTERN void
sky_integer_export(sky_integer_t    self,
                   uint8_t *        bytes,
                   size_t           nbytes,
                   sky_bool_t       little_endian,
                   sky_bool_t       is_signed);


/** Return the floored quotient of an integer divided by another integer.
  * The result will always be a whole integer rounded towards minus infinity.
  * This is equivalent to the expression integer // other.
  *
  * @param[in]  integer     the first integer to be divded.
  * @param[in]  other       the second integer to be divided.
  * @return     the result of dividing @a integer by @a other, rounded towards
  *             minus infinity, or @c sky_NotImplemented if either object is
  *             not an integer object.
  */
SKY_EXTERN sky_object_t
sky_integer_floordiv(sky_integer_t integer, sky_object_t other);


/** Convert an integer into a float and exponent, truncating if necessary.
  * Returns @a integer in the form d * (2 ** exp), where d is the range
  * 0.5 <= abs(d) < 1 and exp >= 0.
  *
  * @param[in]  integer     the integer to convert.
  * @param[out] exp         the exponent from the conversion.
  * @return     the floating point value from the conversion.
  */
SKY_EXTERN double
sky_integer_frexp(sky_integer_t integer, ssize_t *exp);


/** Create an integer object instance from binary bytes.
  *
  * @param[in]  cls         the type to create, which must be either
  *                         @c sky_integer_type or a sub-type of it.
  * @param[in]  bytes       the binary bytes to use.
  * @param[in]  byteorder   either "big" or "little" to indicate big endian or
  *                         little endian.
  * @param[in]  is_signed   @c SKY_TRUE if the bits are signed using two's
  *                         complement, or @c SKY_FALSE if the data is
  *                         unsigned.
  * @return     the new object instance created from @a bytes.
  */
SKY_EXTERN sky_integer_t
sky_integer_from_bytes(sky_type_t   cls,
                       sky_object_t bytes,
                       sky_string_t byteorder,
                       sky_bool_t   is_signed);


/** Implementation of the __hash__() method for integer objects. **/
SKY_EXTERN uintptr_t
sky_integer_hash(sky_integer_t integer);


/** Invert the bits of an integer.
  * This is equivalent to the expression ~integer.
  *
  * @param[in]  integer     the integer to be inverted.
  * @return     an integer object with the bits of @a inverted.
  */
SKY_EXTERN sky_object_t
sky_integer_invert(sky_integer_t integer);


/** Determine if an integer is even.
  *
  * @param[in]  self        the integer to test.
  * @return     @c SKY_TRUE if @a self is even; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_integer_iseven(sky_integer_t self);


/** Determine if an integer is negative.
  *
  * @param[in]  self        the integer to test.
  * @return     @c SKY_TRUE if @a self is negative; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_integer_isnegative(sky_integer_t self);


/** Determine if an integer is zero.
  *
  * @param[in]  self        the integer to test.
  * @return     @c SKY_TRUE if @a self is zero; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_integer_iszero(sky_integer_t self);


/** Shift the bits of an integer left.
  * This is equivalent to the expression integer << other.
  *
  * @param[in]  integer     the integer to be shifted left.
  * @param[in]  other       an integer that is the number of bits to shift
  *                         @a integer. Must be >= 0.
  * @return     @a integer shifted left @a other bits, or @c sky_NotImplemented
  *             if either object is not an integer object.
  */
SKY_EXTERN sky_object_t
sky_integer_lshift(sky_integer_t integer, sky_object_t other);


/** Return the remainder resulting from the division of two integers.
  * This is equivalent to the expression integer % other.
  *
  * @param[in]  integer     the first integer to be divided.
  * @param[in]  other       the second integer to be divided.
  * @return     the remainder resulting from dividing @a integer by @a other,
  *             or @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_mod(sky_integer_t integer, sky_object_t other);


/** Return the product of two integers.
  * This is equivalent to the expression integer * other.
  *
  * @param[in]  integer     the first integer to be multiplied.
  * @param[in]  other       the second integer to be multiplied.
  * @return     the product of multiplying @a integer and @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_mul(sky_integer_t integer, sky_object_t other);


/** Return the negation of an integer.
  * This is equivalent to the expression -integer.
  *
  * @param[in]  integer     the integer to be negated.
  * @return     @a integer negated.
  */
SKY_EXTERN sky_object_t
sky_integer_neg(sky_integer_t integer);


/** Return the bitwise-or of two integers.
  * This is equivalent to the expression integer | other.
  *
  * @param[in]  integer     the first integer to be bitwise-ored.
  * @param[in]  other       the second integer to be bitwise-ored.
  * @return     the result of bitwise-oring @a integer and @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_or(sky_integer_t integer, sky_object_t other);


/** Round an integer.
  * This is the implementation of __round__().
  *
  * @param[in]  self    the integer to be rounded.
  * @param[in]  ndigits the number of digits to round to. May be @c NULL;
  *                     otherwise, any object implementing the __index__()
  *                     method.
  * @return     @a self rounded.
  */
SKY_EXTERN sky_object_t
sky_integer_round(sky_integer_t self, sky_object_t ndigits);


/** Shift the bits of an integer right.
  * This is equivalent to the expression integer >> other.
  *
  * @param[in]  integer     the integer to be shifted right.
  * @param[in]  other       an integer that is the number of bits to shift
  *                         @a integer. Must be >= 0.
  * @return     @a integer shifted right @a other bits, or @c sky_NotImplemented
  *             if either object is not an integer object.
  */
SKY_EXTERN sky_object_t
sky_integer_rshift(sky_integer_t integer, sky_object_t other);


/** Return the sign of an integer.
  *
  * @param[in]  self        the integer for which the sign is to be returned.
  * @return     one of -1, 0, or 1 to indicate negative, zero, or positive,
  *             respectively.
  */
SKY_EXTERN int
sky_integer_sign(sky_integer_t self);


/** Return the number of bits required to represent an integer in a given base.
  *
  * @param[in]  self        the integer.
  * @param[in]  base        the base.
  * @return     the number of bits required to represent the integer @a self
  *             in base @a base.
  */
SKY_EXTERN size_t
sky_integer_sizeinbase(sky_integer_t self, int base);


/** Return the difference of two integers.
  * This is equivalent to the expression integer - other.
  *
  * @param[in]  integer     the first integer to be subtracted.
  * @param[in]  other       the second integer to be subtracted.
  * @return     the difference of @a integer and @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_sub(sky_integer_t integer, sky_object_t other);


/** Compute the sum of a sequence of integers.
  *
  * @param[in]  start       the starting value.
  * @param[in]  sequence    the sequence of integers to sum.
  * @return     the sum of the integers in @a sequence, starting with the value
  *             @a start.
  */
SKY_EXTERN sky_integer_t
sky_integer_sum(sky_integer_t start, sky_object_t sequence);


/** Return the quotient of an integer divided by another integer.
  * The result will always be a floating-point number resulting from the true
  * division of the two integers. This is equivalent to the expression
  * integer / other.
  *
  * @param[in]  self     the first integer to be divded.
  * @param[in]  other       the second integer to be divided.
  * @return     the result of dividing @a self by @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_truediv(sky_integer_t self, sky_object_t other);


/** Return the value of an integer object instance as a primary type.
  * If the integer value is outside the range of @a min to @a max, the
  * exception type specified by @a exception will be raised.
  *
  * @param[in]  integer     the integer object.
  * @param[in]  min         the minimum acceptable value to return.
  * @param[in]  max         the maximum acceptable value to return.
  * @param[in]  exception   the exception type to raise. May be @c NULL, in
  *                         which case @c sky_OverflowError will be raised.
  * @return     the value of the integer object.
  */
SKY_EXTERN intmax_t
sky_integer_value(sky_integer_t integer,
                  intmax_t      min,
                  uintmax_t     max,
                  sky_type_t    exception);


/** Return the value of an integer object instance as a primary type clamped
  * to a specified range.
  * If the integer value is outside the range of @a min to @a max, the value
  * returned will be clamped to either @a min or @a max as appropriate.
  */
SKY_EXTERN intmax_t
sky_integer_value_clamped(sky_integer_t integer, intmax_t min, uintmax_t max);


/** Return the value of an integer object instance as a double floating point
  * value.
  * If the integer is too big to represent as a @c double, @c sky_OverflowError
  * will be raised.
  *
  * @param[in]  self        the integer for which its value is to be returned
  *                         as a @c double.
  * @return     the value of @a self represented as a @c double.
  */
SKY_EXTERN double
sky_integer_value_double(sky_integer_t self);


/** Return the bitwise-xor of two integers.
  * This is equivalent to the expression integer ^ other.
  *
  * @param[in]  integer     the first integer to be bitwise-xored.
  * @param[in]  other       the second integer to be bitwise-xored.
  * @return     the result of bitwise-xoring @a integer and @a other, or
  *             @c sky_NotImplemented if either object is not an integer
  *             object.
  */
SKY_EXTERN sky_object_t
sky_integer_xor(sky_integer_t integer, sky_object_t other);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_INTEGER_H__ */

/** @} **/
/** @} **/
