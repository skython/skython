#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"
#include "sky_function_private.h"


/* Parameters are defined for functions in the following order, all of which
 * are completely optional:
 *
 *  1.  required positional parameters
 *  2.  positional parameters with default values (__defaults__)
 *  3.  *args
 *  4.  required keyword only parameters
 *  5.  keyword parameters with default values (__kwdefaults__)
 *  5.  **kws
 *
 * There is no defined limit to the number of parameters that a function may
 * define; however, the implementation limits the number of parameters to
 * SSIZE_MAX (signed, 32-bit or 64-bit, depending on architecture).
 *
 * Most of the special fields are writable. Only the __globals__ and
 * __closure__ fields are read-only.
 *
 * The __code__ field is typically either sky_native_code_t for a function
 * that is implemented in C or sky_byte_code_t for a function that implements
 * byte-code. Either way, the __code__ object must be callable.
 */


static void
sky_function_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_function_data_t *function_data = data;

    sky_object_visit(function_data->doc, visit_data);
    sky_object_visit(function_data->name, visit_data);
    sky_object_visit(function_data->qualname, visit_data);
    sky_object_visit(function_data->module, visit_data);
    sky_object_visit(function_data->code, visit_data);
    sky_object_visit(function_data->globals, visit_data);
    sky_object_visit(function_data->closure, visit_data);
    sky_object_visit(function_data->annotations, visit_data);
    sky_object_visit(function_data->parameters, visit_data);
    sky_object_visit(function_data->deprecated_message, visit_data);
}


SKY_EXTERN sky_function_t
sky_function_create(sky_string_t    qualname,
                    sky_code_t      code,
                    sky_dict_t      globals,
                    sky_tuple_t     closure,
                    sky_tuple_t     defaults,
                    sky_dict_t      kwdefaults)
{
    ssize_t             argi, i;
    sky_object_t        object;
    sky_string_t        dot, keyword;
    sky_function_t      function;
    sky_code_data_t     *code_data;
    sky_function_data_t *function_data;

    function = sky_object_allocate(sky_function_type);
    function_data = sky_function_data(function);
    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->qualname)),
                      qualname,
                      function);
    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->code)),
                      code,
                      function);
    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->globals)),
                      globals,
                      function);
    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->closure)),
                      closure,
                      function);

    dot = SKY_STRING_LITERAL(".");
    if ((i = sky_string_rfind(qualname, dot, 0, SSIZE_MAX)) == -1) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->name)),
                          qualname,
                          function);
    }
    else {
        sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->name)),
                          sky_string_slice(qualname, i + 1, SSIZE_MAX, 1),
                          function);
        function_data->flags |= SKY_FUNCTION_FLAG_METHOD;
    }

    /* If the first constant in the code's constants table is a string, make
     * it the function's doc string.
     */
    code_data = sky_code_data(function_data->code);
    if (sky_tuple_len(code_data->co_consts)) {
        object = sky_tuple_get(code_data->co_consts, 0);
        if (sky_object_isa(object, sky_string_type)) {
            sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->doc)),
                              object,
                              function);
        }
    }

    /* Use the globals __name__ entry as the function's module. */
    object = sky_dict_get(globals, SKY_STRING_LITERAL("__name__"), NULL);
    if (object) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->module)),
                          object,
                          function);
    }

    /* Build the parameter list for the function. The data types are always
     * SKY_DATA_TYPE_OBJECT, except for *args and **kws arguments if they're
     * present. The parameter names can be found in the co_varnames list of
     * the code object, and whether *args and **kws arguments are present is
     * indicated by flags.
     */
    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->parameters)),
                      sky_parameter_list_create(),
                      function);

    argi = 0;
    if (code_data->co_argcount) {
        /* Positional parameters (without defaults) */
        for (i = 0; i < code_data->co_argcount; ++i) {
            keyword = sky_tuple_get(code_data->co_varnames, argi++);
            sky_parameter_list_add(function_data->parameters,
                                   keyword,
                                   SKY_DATA_TYPE_OBJECT,
                                   NULL);
        }
    }
    if (code_data->co_flags & SKY_CODE_FLAG_VARARGS) {
        keyword = sky_string_createfromformat(
                        "*%@",
                        sky_tuple_get(code_data->co_varnames, argi++));
        sky_parameter_list_add(function_data->parameters,
                               keyword,
                               SKY_DATA_TYPE_OBJECT_TUPLE,
                               NULL);
    }
    else if (code_data->co_kwonlyargcount) {
        sky_parameter_list_add(function_data->parameters,
                               SKY_STRING_LITERAL("*"),
                               SKY_DATA_TYPE_OBJECT_TUPLE,
                               NULL);
    }

    if (code_data->co_kwonlyargcount) {
        for (i = 0; i < code_data->co_kwonlyargcount; ++i) {
            keyword = sky_tuple_get(code_data->co_varnames, argi++);
            sky_parameter_list_add(function_data->parameters,
                                   keyword,
                                   SKY_DATA_TYPE_OBJECT,
                                   NULL);
        }
    }
    if (code_data->co_flags & SKY_CODE_FLAG_VARKEYWORDS) {
        keyword = sky_string_createfromformat(
                        "**%@",
                        sky_tuple_get(code_data->co_varnames, argi++));
        sky_parameter_list_add(function_data->parameters,
                               keyword,
                               SKY_DATA_TYPE_OBJECT_DICT,
                               NULL);
    }

    if (sky_object_bool(defaults)) {
        sky_parameter_list_setdefaults(function_data->parameters,
                                       defaults);
    }
    if (sky_object_bool(kwdefaults)) {
        sky_parameter_list_setkwdefaults(function_data->parameters,
                                         kwdefaults);
    }

    return function;
}


sky_function_t
sky_function_createbuiltin(const char *                  qualname,
                           const char *                 doc,
                           sky_native_code_function_t   native_function,
                           sky_data_type_t              return_type,
                           ...)
{
    va_list                 ap;
    const char              *keyword;
    sky_object_t            return_type_extra;
    sky_parameter_list_t    parameters;

    va_start(ap, return_type);

    return_type_extra = (sky_data_type_isextraneeded(return_type)
                         ? va_arg(ap, sky_object_t)
                         : NULL);

    keyword = va_arg(ap, const char *);
    parameters = sky_parameter_list_createwithparametersv(keyword, ap);

    va_end(ap);

    return sky_function_createbuiltinwithparameterlist(qualname,
                                                       doc,
                                                       native_function,
                                                       return_type,
                                                       return_type_extra,
                                                       parameters);
}


sky_function_t
sky_function_createbuiltinwithparameterlist(
        const char *                qualname,
        const char *                doc,
        sky_native_code_function_t  native_function,
        sky_data_type_t             return_type,
        sky_object_t                return_type_extra,
        sky_parameter_list_t        parameters)
{
    size_t              qualname_length;
    const char          *dot;
    sky_dict_t          globals;
    sky_object_t        object;
    sky_function_t      function;
    sky_function_data_t *function_data;

    sky_error_validate_debug(qualname != NULL);
    sky_error_validate_debug(native_function != NULL);
    sky_error_validate_debug(parameters != NULL);
    
    function = sky_object_allocate(sky_function_type);
    function_data = sky_function_data(function);
    function_data->flags = SKY_FUNCTION_FLAG_BUILTIN;

    if ((globals = sky_interpreter_globals()) != NULL) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->globals)),
                          globals,
                          function);
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->parameters)),
                      parameters,
                      function);

    sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->code)),
                      sky_native_code_create(native_function,
                                             return_type,
                                             function_data->parameters,
                                             return_type_extra),
                      function);

    qualname_length = strlen(qualname);
    sky_object_gc_set(
            SKY_AS_OBJECTP(&(function_data->qualname)),
            sky_string_createfrombytes(qualname, qualname_length, NULL, NULL),
            function);
    /* If there is a '.' present in qualname, set the name to be everything
     * after it; otherwise, set it to the same as qualname.
     */
    if ((dot = strrchr(qualname, '.')) != NULL) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(function_data->name)),
                sky_string_createfrombytes(
                        dot + 1,
                        (qualname_length - (dot - qualname)),
                        NULL,
                        NULL),
                function);
        function_data->flags |= SKY_FUNCTION_FLAG_METHOD;
    }
    else {
        sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->name)),
                          function_data->qualname,
                          function);
    }

    if (doc) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(function_data->doc)),
                sky_string_createfrombytes(doc, strlen(doc), NULL, NULL),
                function);
    }

    /* Use the globals __name__ entry as the function's module. */
    if (globals) {
        if ((object = sky_dict_get(sky_interpreter_globals(),
                                   SKY_STRING_LITERAL("__name__"),
                                   NULL)) != NULL)
        {
            sky_object_gc_set(SKY_AS_OBJECTP(&(function_data->module)),
                              object,
                              function);
        }
    }

    return function;
}


static sky_object_t
sky_function_annotations_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_function_data_t *self_data = sky_function_data(self);

    if (!self_data->annotations) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->annotations)),
                          sky_dict_create(),
                          self);
    }

    return self_data->annotations;
}


static void
sky_function_annotations_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_function_data_t *self_data = sky_function_data(self);

    if (value && !sky_object_isa(value, sky_dict_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__annotations__ must be set to a dict object");
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->annotations)), value, self);
}


sky_object_t
sky_function_call(sky_function_t function, sky_tuple_t args, sky_dict_t kws)
{
    sky_function_data_t *function_data = sky_function_data(function);

    void            **argv;
    ssize_t         argc;
    sky_object_t    return_value;

    if (sky_tuple_empty == args) {
        args = NULL;
    }
    if (args && !sky_object_isa(args, sky_tuple_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected 'tuple' for *args; got %#@",
                               sky_type_name(sky_object_type(args)));
    }
    if (kws && !sky_object_isa(kws, sky_dict_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected 'dict' for **kws; got %#@",
                               sky_type_name(sky_object_type(kws)));
    }

    if (function_data->flags & SKY_FUNCTION_FLAG_DEPRECATED) {
        ssize_t expected, provided;

        expected = sky_object_len(function_data->parameters);
        provided = (args ? sky_object_len(args) : 0) +
                   (kws ? sky_object_len(kws) : 0);

        /* Ignore 'self' for methods; it is implicit and always provided. */
        if (function_data->flags & SKY_FUNCTION_FLAG_METHOD) {
            --expected;
            --provided;
        }

        /* Do not warn if arguments are provided when no arguments are expected.
         * This matches the handling of CPython's METH_NOARGS method flag, which
         * results in the native code function never being called if arguments
         * are provided.
         */
        if (expected || !provided) {
            sky_error_warn(function_data->deprecated_message,
                           sky_DeprecationWarning,
                           1);
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        argv = sky_parameter_list_apply(function_data->parameters,
                                        args, kws,
                                        function_data->name);
        sky_asset_save(argv, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (sky_object_isa(function_data->code, sky_code_type)) {
            argc = sky_object_len(function_data->parameters);
            return_value = sky_code_execute(function_data->code,
                                            function_data->globals,
                                            NULL,
                                            function_data->closure,
                                            argc,
                                            argv);
        }
        else if (sky_object_isa(function_data->code, sky_native_code_type)) {
            return_value = sky_native_code_execute(function_data->code,
                                                   argv,
                                                   function_data->globals,
                                                   NULL);
        }
        else {
            sky_error_raise_format(
                    sky_TypeError,
                    "bad type for function code (expected %#@ or %#@; got %#@)",
                    sky_type_name(sky_code_type),
                    sky_type_name(sky_native_code_type),
                    sky_type_name(sky_object_type(function_data->code)));
        }
    } SKY_ASSET_BLOCK_END;

    return return_value;
}


sky_object_t
sky_function_code(sky_function_t self)
{
    return sky_function_data(self)->code;
}


static sky_object_t
sky_function_code_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_function_code(self);
}


static void
sky_function_code_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_function_data_t *self_data = sky_function_data(self);

    ssize_t nclosure, nfreevars;

    if (!sky_object_isa(value, sky_code_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__code__ must be set to a code object");
    }
    if (self_data->flags & SKY_FUNCTION_FLAG_BUILTIN) {
        sky_error_raise_string(sky_TypeError,
                               "cannot set __code__ for built-in functions");
    }

    nfreevars = sky_object_len(sky_code_data(value)->co_freevars);
    nclosure = (self_data->closure ? sky_object_len(self_data->closure) : 0);
    if (nfreevars != nclosure) {
        sky_error_raise_format(
                sky_TypeError,
                "%@() requires a code object with %zd free vars, not %zd",
                self_data->name,
                nclosure,
                nfreevars);
    }

    sky_object_gc_set(&(self_data->code), value, self);
}


static sky_object_t
sky_function_defaults_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_function_data_t *function_data = sky_function_data(self);

    return sky_parameter_list_defaults(function_data->parameters);
}


static void
sky_function_defaults_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_function_data_t *function_data = sky_function_data(self);

    sky_parameter_list_setdefaults(function_data->parameters, value);
}


sky_bool_t
sky_function_fastcallable(sky_function_t self)
{
    sky_function_data_t *self_data = sky_function_data(self);

    return (self_data->flags & SKY_FUNCTION_FLAG_FASTCALLABLE ? SKY_TRUE
                                                              : SKY_FALSE);
}


sky_object_t
sky_function_get(
                    sky_object_t    self,
                    sky_object_t    instance,
        SKY_UNUSED  sky_type_t      owner)
{
    sky_function_data_t *self_data = sky_function_data(self);

    if (sky_object_isnull(instance)) {
        return self;
    }
    if ((self_data->flags & SKY_FUNCTION_FLAG_BUILTIN) &&
        !(self_data->flags & SKY_FUNCTION_FLAG_METHOD))
    {
        return self;
    }

    return sky_method_create(self, instance);
}


static sky_object_t
sky_function_kwdefaults_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_function_data_t *function_data = sky_function_data(self);

    return sky_parameter_list_kwdefaults(function_data->parameters);
}


static void
sky_function_kwdefaults_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_function_data_t *function_data = sky_function_data(self);

    sky_parameter_list_setkwdefaults(function_data->parameters, value);
}


sky_function_t
sky_function_new(
        SKY_UNUSED  sky_type_t      cls,
                    sky_code_t      code,
                    sky_dict_t      globals,
                    sky_string_t    name,
                    sky_tuple_t     argdefs,
                    sky_tuple_t     closure)
{
    ssize_t         nfreevars;
    sky_object_t    object;
    sky_code_data_t *code_data;

    code_data = sky_code_data(code);
    if (!sky_object_isa(name, sky_string_type)) {   /* sky_None is legal */
        name = code_data->co_name;
    }

    /* If the code has free variables, a closure is required. */
    if (code_data->co_freevars) {
        nfreevars = sky_object_len(code_data->co_freevars);
        if (nfreevars) {
            if (!sky_object_isa(closure, sky_tuple_type)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "closure must be a tuple (code has free variables)");
            }
            if (nfreevars != sky_object_len(closure)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "closure tuple length must be exactly %zd (got %zd)",
                        nfreevars,
                        sky_object_len(closure));
            }
            /* make sure each element in closure is proper */
            SKY_SEQUENCE_FOREACH(closure, object) {
                if (!sky_object_isa(object, sky_cell_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "closure elements must be cell objects (got %#@)",
                            sky_type_name(sky_object_type(object)));
                }
            } SKY_SEQUENCE_FOREACH_END;
        }
    }

    return sky_function_create(name, code, globals, closure, argdefs, NULL);
}


sky_string_t
sky_function_repr(sky_function_t self)
{
    sky_function_data_t *self_data = sky_function_data(self);

    sky_object_t    parts;

    if (self_data->flags & SKY_FUNCTION_FLAG_BUILTIN) {
        if (self_data->flags & SKY_FUNCTION_FLAG_METHOD) {
            parts = sky_string_rpartition(self_data->qualname,
                                          SKY_STRING_LITERAL("."));
            return sky_string_createfromformat("<method %#@ of %#@ objects>",
                                               sky_sequence_get(parts, 2),
                                               sky_sequence_get(parts, 0));
        }
        return sky_string_createfromformat("<built-in function %@>",
                                           self_data->name);
    }

    return sky_string_createfromformat("<function %@ at %p>",
                                       sky_function_data(self)->qualname,
                                       self);
}


void
sky_function_setdeprecated(sky_function_t function, const char *message)
{
    sky_function_data_t *function_data = sky_function_data(function);

    function_data->flags |= SKY_FUNCTION_FLAG_DEPRECATED;
    if (message) {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(function_data->deprecated_message)),
                sky_string_createfrombytes(message, strlen(message), NULL, NULL),
                function);
    }
    else {
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(function_data->deprecated_message)),
                sky_string_createfromformat("%@() is deprecated",
                                            function_data->name),
                function);
    }
}


void
sky_function_setfastcallable(sky_function_t self, sky_bool_t flag)
{
    sky_function_data_t *self_data = sky_function_data(self);

    if (flag) {
        self_data->flags |= SKY_FUNCTION_FLAG_FASTCALLABLE;
    }
    else {
        self_data->flags &= ~SKY_FUNCTION_FLAG_FASTCALLABLE;
    }
}


static const char sky_function_type_doc[] =
"function(code, globals[, name[, argdefs[, closure]]])\n"
"\n"
"Create a function object from a code object and a dictionary.\n"
"The optional name string overrides the name from the code object.\n"
"The optional argdefs tuple specifies the default argument values.\n"
"The optional closure tuple supplies the bindings for free variables.";


SKY_TYPE_DEFINE_SIMPLE(function,
                       "function",
                       sizeof(sky_function_data_t),
                       NULL,
                       NULL,
                       sky_function_instance_visit,
                       SKY_TYPE_FLAG_HAS_DICT | SKY_TYPE_FLAG_FINAL,
                       sky_function_type_doc);


void
sky_function_initialize_library(void)
{
    sky_type_setmembers(sky_function_type,
            "__closure__",
                    NULL,
                    offsetof(sky_function_data_t, closure),
                    SKY_DATA_TYPE_OBJECT_TUPLE, SKY_MEMBER_FLAG_READONLY,
            "__doc__",
                    NULL,
                    offsetof(sky_function_data_t, doc),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_ALLOW_NONE,
            "__globals__",
                    NULL,
                    offsetof(sky_function_data_t, globals),
                    SKY_DATA_TYPE_OBJECT_DICT, SKY_MEMBER_FLAG_READONLY,
            "__module__",
                    NULL,
                    offsetof(sky_function_data_t, module),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_ALLOW_NONE,
            "__name__",
                    NULL,
                    offsetof(sky_function_data_t, name),
                    SKY_DATA_TYPE_OBJECT_STRING, 0,
            "__qualname__",
                    NULL,
                    offsetof(sky_function_data_t, qualname),
                    SKY_DATA_TYPE_OBJECT_STRING, 0,
            NULL);

    sky_type_setattr_getset(
            sky_function_type,
            "__annotations__",
            NULL,
            sky_function_annotations_getter,
            sky_function_annotations_setter);
    sky_type_setattr_getset(
            sky_function_type,
            "__code__",
            NULL,
            sky_function_code_getter,
            sky_function_code_setter);
    sky_type_setattr_getset(
            sky_function_type,
            "__defaults__",
            NULL,
            sky_function_defaults_getter,
            sky_function_defaults_setter);
    sky_type_setattr_getset(
            sky_function_type,
            "__kwdefaults__",
            NULL,
            sky_function_kwdefaults_getter,
            sky_function_kwdefaults_setter);

    sky_type_setmethodslotwithparameters(
            sky_function_type,
            "__new__",
            (sky_native_code_function_t)sky_function_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "code", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_code_type,
            "globals", SKY_DATA_TYPE_OBJECT_DICT, NULL,
            "name", SKY_DATA_TYPE_OBJECT_STRING, sky_None,
            "argdefs", SKY_DATA_TYPE_OBJECT_TUPLE, sky_tuple_empty,
            "closure", SKY_DATA_TYPE_OBJECT_TUPLE, sky_tuple_empty,
            NULL);

    sky_type_setmethodslots(sky_function_type,
            "__repr__", sky_function_repr,
            "__get__", sky_function_get,
            "__call__", sky_function_call,
            NULL);
}
