#ifndef __SKYTHON_CORE_SKY_PYTHON_COMPILER_H__
#define __SKYTHON_CORE_SKY_PYTHON_COMPILER_H__ 1


#include "sky_base.h"
#include "sky_hashtable.h"
#include "sky_object.h"

#include "sky_code.h"
#include "sky_python_symtable.h"


SKY_CDECLS_BEGIN


typedef enum sky_python_compiler_flag_e {
    SKY_PYTHON_COMPILER_FLAG_SOURCE_IS_UTF8     =   0x0100,
    SKY_PYTHON_COMPILER_FLAG_DONT_IMPLY_DEDENT  =   0x0200,
    SKY_PYTHON_COMPILER_FLAG_ONLY_AST           =   0x0400,
    SKY_PYTHON_COMPILER_FLAG_IGNORE_COOKIE      =   0x0800,
} sky_python_compiler_flag_t;


typedef struct sky_python_compiler_table_item_s
        sky_python_compiler_table_item_t;

struct sky_python_compiler_table_item_s {
    sky_hashtable_item_t                base;
    sky_object_t                        key;
    ssize_t                             index;
    sky_python_compiler_table_item_t *  next;
};

typedef struct sky_python_compiler_table_s {
    sky_hashtable_t                     base;
    sky_python_compiler_table_item_t *  items_head;
    sky_python_compiler_table_item_t *  items_tail;
    sky_bool_t                          readonly;
} sky_python_compiler_table_t;


typedef struct sky_python_compiler_block_s sky_python_compiler_block_t;

typedef struct sky_python_compiler_op_s {
    sky_code_opcode_t                   opcode;
    int                                 lineno;
    ssize_t                             offset;
    union {
        size_t                          argv[3];
        struct {
            sky_python_compiler_block_t *   target;
            uint16_t                        offset;
        } jump;
        struct {
            sky_python_compiler_block_t *   jump_block;
            uint16_t                        jump_offset;
            sky_python_compiler_block_t *   exit_block;
            ssize_t                         exit_op_index;
            uint16_t                        exit_offset;
            sky_python_compiler_block_t *   orelse_block;
            ssize_t                         guard_id;
        } enter;
        struct {
            sky_python_compiler_block_t *   enter_block;
            ssize_t                         enter_op_index;
        } exit;
    } u;
} sky_python_compiler_op_t;


/* This is a basic block. Basic blocks contain sets of instructions and are
 * used to build a control flow graph. A basic block consists of a leader (the
 * first instruction in the block, defined below) and all instructions up to
 * but not including the next leader or the end of the program (Dragon Book,
 * 8.4.1).
 *
 * 1. The first instruction is a leader.
 * 2. Any instruction that is the target of a conditional or unconditional jump
 *    is a leader.
 * 3. Any instruction that immediately follows a conditional or unconditional
 *    jump is a leader.
 *
 * In addition, guard_enter opcodes (WITH_ENTER, TRY_EXCEPT, TRY_FINALLY) shall
 * end a block, making the first instruction they guard a leader. This is done
 * simply for the sake of block layout during assembly.
 */

struct sky_python_compiler_block_s {
    sky_python_compiler_block_t *       alloc_next;

    sky_python_compiler_op_t *          ops;
    ssize_t                             ops_len;
    ssize_t                             ops_size;
    ssize_t                             bytecode_size;

    /* If this block flows into another one (the last op is either not a jump
     * or it's not an unconditional jump), this is the one.
     */
    sky_python_compiler_block_t *       flow_next;

    ssize_t                             layout_index;
    ssize_t                             stack_entry;
    ssize_t                             stack_delta;
    ssize_t                             stack_nadir;
    ssize_t                             guard_id;

    int                                 lineno;
    int                                 col_offset;

    unsigned int                        analyzed        : 1;
    unsigned int                        dce_visited     : 1;
    unsigned int                        counted         : 1;
    unsigned int                        reachable       : 1;
};


typedef struct sky_python_compiler_loop_s sky_python_compiler_loop_t;
struct sky_python_compiler_loop_s {
    sky_python_compiler_loop_t *        parent;
    sky_python_compiler_block_t *       break_block;
    sky_python_compiler_block_t *       continue_block;
    ssize_t                             pop_stack;
};


typedef struct sky_python_compiler_code_s sky_python_compiler_code_t;
struct sky_python_compiler_code_s {
    sky_python_compiler_code_t *        parent;
    sky_string_t                        name;

    sky_python_compiler_table_t         constants;
    sky_python_compiler_table_t         names;
    sky_python_compiler_table_t         varnames;
    sky_python_compiler_table_t         freevars;
    sky_python_compiler_table_t         cellvars;

    ssize_t                             argcount;
    ssize_t                             kwonlyargcount;
    ssize_t                             nlocals;
    ssize_t                             stacksize;

    unsigned int                        flags;
    int                                 firstlineno;

    sky_python_symtable_block_data_t *  symtable;

    ssize_t                             nblocks;
    sky_python_compiler_block_t *       blocks;
    sky_python_compiler_block_t *       entry_block;
    sky_python_compiler_block_t *       current_block;
    sky_python_compiler_block_t **      block_layout;

    ssize_t                             bytecode_size;
    sky_bytes_t                         bytecode;
    ssize_t                             lnotab_size;
    sky_bytes_t                         lnotab;

    sky_python_compiler_loop_t *        current_loop;
    ssize_t                             guard_id;
    ssize_t                             guard_id_counter;

#if !defined(NDEBUG)
    sky_tuple_t                         t_constants;
    sky_tuple_t                         t_names;
    sky_tuple_t                         t_varnames;
    sky_tuple_t                         t_freevars;
    sky_tuple_t                         t_cellvars;
#endif
};


typedef enum sky_python_compiler_mode_e {
    SKY_PYTHON_COMPILER_MODE_FILE           =   1,
    SKY_PYTHON_COMPILER_MODE_EVAL           =   2,
    SKY_PYTHON_COMPILER_MODE_INTERACTIVE    =   3,
} sky_python_compiler_mode_t;


typedef struct sky_python_compiler_s {
    sky_string_t                        filename;

    sky_dict_t                          symtable;

    sky_python_compiler_code_t *        code;
    sky_string_t                        class_name;

    unsigned int                        flags;
    int                                 optlevel;
    sky_python_compiler_mode_t          mode;
} sky_python_compiler_t;


/* Source must be AST: one of Expression, Interactive, or Module */
SKY_EXTERN sky_code_t
sky_python_compiler_compile(sky_object_t                ast,
                            sky_string_t                filename,
                            sky_python_compiler_mode_t  mode,
                            unsigned int                flags,
                            int                         optlevel);

SKY_EXTERN sky_string_t
sky_python_compiler_line(sky_string_t filename, int lineno);

SKY_EXTERN sky_string_t
sky_python_compiler_mangle_name(sky_string_t    class_name,
                                sky_string_t    name);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PYTHON_COMPILER_H__ */
