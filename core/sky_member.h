/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_member Member Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MEMBER_H__
#define __SKYTHON_CORE_SKY_MEMBER_H__ 1


#include "sky_base.h"
#include "sky_data_type.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Constants describing flags that may be associated with a member descriptor.
  */
typedef enum sky_member_flag_e {
    /** If this is set, the member may neither be deleted nor set. **/
    SKY_MEMBER_FLAG_READONLY        =   (1 << 0),
    /** If this is set, a @c NULL value will cause a @c sky_AttributeError to
      * be raised instead of a @c sky_None returned for a get operation.
      */
    SKY_MEMBER_FLAG_NULL_EXCEPTION  =   (1 << 1),
    /** If this is set, the member can be set to @c NULL. This only makes sense
      * when used with the @c SKY_DATA_TYPE_OBJECT family of data types.
      */
    SKY_MEMBER_FLAG_DELETEABLE      =   (1 << 2),
    /** If this is set, the member can be set to @c sky_None in addition to any
      * more specific type requirement. This only makes sense when used with
      * the @c SKY_DATA_TYPE_OBJECT family of data types.
      */
    SKY_MEMBER_FLAG_ALLOW_NONE      =   (1 << 3),
    /** If this is set, setting the member to @c sky_None sets the pointer in
      * memory to @c NULL. This only makes sense when used with the
      * @c SKY_DATA_TYPE_OBJECT fmaily of data types.
      */
    SKY_MEMBER_FLAG_NONE_IS_NULL    =   (1 << 4),
} sky_member_flag_t;


/** Create a new bound member descriptor object.
  *
  * @param[in]  type        the type to bind the descriptor to.
  * @param[in]  name        the name of the descriptor.
  * @param[in]  doc         the doc string for the descriptor. May be @c NULL.
  * @param[in]  offset      the offset into the bound type's object instance
  *                         data where the member value can be found.
  * @param[in]  data_type   the data type of the value (see @c sky_data_type_t).
  * @param[in]  flags       flags.
  * @param[in]  ...         if @a data_type is
  *                         @c SKY_DATA_TYPE_OBJECT_INSTANCEOF, the type that
  *                         the object must be an instance of is to be supplied
  *                         here.
  * @return     the member descriptor object bound to @a type.
  */
SKY_EXTERN sky_member_t
sky_member_create(sky_type_t        type,
                  sky_string_t      name,
                  sky_string_t      doc,
                  size_t            offset,
                  sky_data_type_t   data_type,
                  unsigned int      flags,
                  ...);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MEMBER_H__ */

/** @} **/
/** @} **/
