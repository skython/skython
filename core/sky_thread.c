#include "sky_private.h"
#include "sky_thread_private.h"

#if defined(HAVE_SYS_TIME_H)
#   include <sys/time.h>
#endif


#ifndef FD_COPY
#   define FD_COPY(_s, _d) (*(_d) = *(_s))
#endif

#define SKY_THREAD_MIN_STACK_SIZE       0x8000


static ssize_t      sky_thread_stacksize_value;
static sky_thread_t sky_thread_main_object;


typedef struct sky_thread_data_s {
    pthread_t                           thread;
} sky_thread_data_t;

SKY_EXTERN_INLINE sky_thread_data_t *
sky_thread_data(sky_object_t object)
{
    return sky_object_data(object, sky_thread_type);
}

static void
sky_thread_instance_atfork_child(sky_object_t self)
{
    sky_thread_data_t   *data = sky_thread_data(self);

    if (sky_thread_self() == self) {
        data->thread = pthread_self();
        sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky_thread_main_object),
                                     self,
                                     SKY_TRUE);
    }
    else {
        /* pthread_t is implementation dependent, and so it may be anything.
         * the safest way to invalidate it is to use memset().
         */
        memset(&(data->thread), 0x00, sizeof(pthread_t));
    }
}

static void
sky_thread_instance_finalize(sky_object_t self, SKY_UNUSED void *data)
{
    sky_object_atfork_unregister(self);
}

static void
sky_thread_instance_initialize(sky_object_t self, SKY_UNUSED void *data)
{
    sky_object_atfork_register(self,
                               NULL,
                               NULL,
                               sky_thread_instance_atfork_child);
}


typedef struct sky_thread_entry_s {
    sky_thread_function_t               start_routine;
    void *                              arg;
    sky_thread_t                        thread_object;
    volatile sky_bool_t                 ready;
    sky_thread_wait_t *                 wait;
} sky_thread_entry_t;


static void *
sky_thread_entry(void *arg)
{
    sky_tlsdata_t           *tlsdata      = sky_tlsdata_get();
    sky_thread_entry_t      *thread_entry = arg;
    sky_thread_function_t   start_routine = thread_entry->start_routine;
    void                    *start_arg    = thread_entry->arg;
    sky_thread_t            thread        = thread_entry->thread_object;
    sky_thread_data_t       *thread_data  = sky_thread_data(thread);

    int old_state;

    thread_data->thread = pthread_self();
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_state);

    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&(tlsdata->thread_self)),
                                 thread_entry->thread_object,
                                 SKY_TRUE);

    sky_atfork_register(NULL, NULL);
    sky_object_gc_register(NULL, NULL);

    thread_entry->ready = SKY_TRUE;
    sky_thread_wait_signal(thread_entry->wait);

    return start_routine(start_arg);
}


static sky_bool_t
sky_thread_ready(sky_object_t arg)
{
    return ((sky_thread_entry_t *)sky_capsule_pointer(arg, NULL))->ready;
}

sky_thread_t
sky_thread_create(sky_thread_function_t start_routine, void *arg)
{
    int                 rc;
#if !defined(_WIN32)
    sigset_t            oset, set;
#endif
    pthread_t           tid;
    sky_thread_t        thread;
    pthread_attr_t      attr;
    sky_thread_entry_t  thread_entry;

    thread = sky_object_allocate(sky_thread_type);

    thread_entry.start_routine = start_routine;
    thread_entry.arg           = arg;
    thread_entry.thread_object = thread;
    thread_entry.ready         = SKY_FALSE;
    thread_entry.wait          = sky_thread_wait();

#if !defined(_WIN32)
    sigfillset(&set);
    sigdelset(&set, SIGILL);
    sigdelset(&set, SIGABRT);
    sigdelset(&set, SIGFPE);
#   if defined(SIGBUS)
    sigdelset(&set, SIGBUS);
#   endif
    sigdelset(&set, SIGSEGV);
#   if defined(SIGSYS)
    sigdelset(&set, SIGSYS);
#   endif

    pthread_sigmask(SIG_BLOCK, &set, &oset);
#endif

    pthread_attr_init(&attr);
    if (sky_thread_stacksize_value > 0) {
        pthread_attr_setstacksize(&attr, sky_thread_stacksize_value);
    }

    rc = pthread_create(&tid, &attr, sky_thread_entry, &thread_entry);
#if !defined(_WIN32)
    pthread_sigmask(SIG_SETMASK, &oset, NULL);
#endif
    if (!rc) {
        if (sky_thread_wait_condition(
                    thread_entry.wait,
                    sky_thread_ready,
                    sky_capsule_create(&thread_entry, NULL, NULL, NULL),
                    SKY_TIME_INFINITE) == -1)
        {
            rc = errno;
        }
    }

    pthread_attr_destroy(&attr);

    if (rc != 0) {
        sky_error_raise_errno(sky_OSError, rc);
    }

    return thread;
}


void
sky_thread_detach(sky_thread_t thread)
{
    sky_thread_data_t   *thread_data = sky_thread_data(thread);

    int rc;

    if ((rc = pthread_detach(thread_data->thread)) != 0) {
        sky_error_raise_errno(sky_OSError, rc);
    }
}


void
sky_thread_exit(void *exit_value)
{
    pthread_exit(exit_value);
}


sky_bool_t
sky_thread_ismain(void)
{
    sky_thread_data_t   *thread_data = sky_thread_data(sky_thread_main_object);

    return (pthread_equal(pthread_self(), thread_data->thread) ? SKY_TRUE
                                                               : SKY_FALSE);
}


void *
sky_thread_join(sky_thread_t thread)
{
    sky_thread_data_t   *data = sky_thread_data(thread);

    int                 rc;
    void                *exit_value;

    if (unlikely((rc = pthread_join(data->thread, &exit_value)) != 0)) {
        sky_error_raise_errno(sky_OSError, rc);
    }

    return exit_value;
}


sky_thread_t
sky_thread_self(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    if (!tlsdata->thread_self) {
        sky_thread_t    thread = sky_object_allocate(sky_thread_type);

        sky_thread_data(thread)->thread = pthread_self();
        sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&(tlsdata->thread_self)),
                                     thread,
                                     SKY_TRUE);
    }

    return tlsdata->thread_self;
}


uint32_t
sky_thread_id(void)
{
    return sky_tlsdata_get()->thread_id;
}


uint32_t
sky_thread_highestid(void)
{
    return sky_tlsdata_highestid();
}


sky_thread_t
sky_thread_main(void)
{
    return sky_thread_main_object;
}


sky_string_t
sky_thread_repr(sky_thread_t self)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    return sky_string_createfromformat("ASCII",
                                       "<%@ instance at %p (thread id %u)>",
                                       sky_type_name(sky_object_type(self)),
                                       self,
                                       tlsdata->thread_id);
}


ssize_t
sky_thread_setstacksize(ssize_t size)
{
    ssize_t old_size = sky_thread_stacksize_value;

    if (size < 0) {
        sky_error_raise_string(sky_ValueError,
                               "size must be 0 or a positive value");
    }
    if (size && size < SKY_THREAD_MIN_STACK_SIZE) {
        sky_error_raise_format(sky_ValueError,
                               "size not valid: %zd bytes",
                               size);
    }
    sky_thread_stacksize_value = size;

    return old_size;
}


ssize_t
sky_thread_stacksize(void)
{
    return sky_thread_stacksize_value;
}


sky_thread_wait_t *
sky_thread_wait(void)
{
    return &(sky_tlsdata_get()->wait);
}


void
sky_thread_wait_cleanup(sky_thread_wait_t *wait)
{
    if (wait->fds[1] != -1) {
        close(wait->fds[1]);
        wait->fds[1] = -1;
    }
    if (wait->fds[0] != -1) {
        close(wait->fds[0]);
        wait->fds[0] = -1;
    }
    pthread_cond_destroy(&(wait->cond));
    pthread_mutex_destroy(&(wait->mutex));
}


static void
sky_thread_wait_signal_locked(sky_thread_wait_t *wait)
{
    switch (wait->state) {
        case SKY_THREAD_WAIT_STATE_RUNNING:
            break;
        case SKY_THREAD_WAIT_STATE_COND:
            pthread_cond_signal(&(wait->cond));
            break;
        case SKY_THREAD_WAIT_STATE_SELECT:
            for (;;) {
                int junk = 0;
                if (write(wait->fds[1], &junk, sizeof(junk)) == -1 &&
                    EINTR == errno)
                {
                    continue;
                }
                break;
            }
            break;
    }
}

static void
sky_thread_wait_atfork_wakeup(void *arg)
{
    sky_thread_wait_t   *wait = arg;

    pthread_mutex_lock(&(wait->mutex));
    wait->atfork_check = SKY_TRUE;
    sky_thread_wait_signal_locked(wait);
    pthread_mutex_unlock(&(wait->mutex));
}

static void
sky_thread_wait_gc_wakeup(void *arg)
{
    sky_thread_wait_t   *wait = arg;

    pthread_mutex_lock(&(wait->mutex));
    wait->gc_needed = SKY_TRUE;
    sky_thread_wait_signal_locked(wait);
    pthread_mutex_unlock(&(wait->mutex));
}


typedef struct sky_thread_wait_cleanup_s {
    sky_thread_wait_t *                 wait;
    sky_object_t                        condition_arg;

    sky_atfork_wakeup_t                 atfork_wakeup_function;
    void *                              atfork_wakeup_arg;

    sky_object_gc_wakeup_t              gc_wakeup_function;
    void *                              gc_wakeup_arg;

    sky_thread_wait_state_t             state;
} sky_thread_wait_cleanup_t;

static void
sky_thread_wait_cleanup_asset(void *arg)
{
    sky_thread_wait_cleanup_t   *cleanup = arg;

    sky_object_gc_register(cleanup->gc_wakeup_function, cleanup->gc_wakeup_arg);
    sky_object_gc_setroot(&(cleanup->condition_arg), NULL, SKY_FALSE);
    sky_atfork_register(cleanup->atfork_wakeup_function,
                        cleanup->atfork_wakeup_arg);
    cleanup->wait->state = cleanup->state;
}


int
sky_thread_wait_condition(sky_thread_wait_t *           wait,
                          sky_thread_wait_condition_t   condition,
                          sky_object_t                  condition_arg,
                          sky_time_t                    timeout)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    int             result;
    sky_time_t      now;
    struct timespec abstime;

    if (!wait) {
        wait = &(tlsdata->wait);
    }

    if (SKY_TIME_INFINITE != timeout) {
        now = sky_time_current();
        if (SKY_TIME_INFINITE - now < timeout) {
            timeout = SKY_TIME_INFINITE;
        }
        else {
            struct timeval  tv;

            gettimeofday(&tv, NULL);
            now = sky_time_fromtimeval(&tv);
            timeout += now;
            abstime = sky_time_totimespec(timeout);
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_thread_wait_cleanup_t   cleanup;

        pthread_mutex_lock(&(wait->mutex));
        sky_asset_save(&(wait->mutex),
                       (sky_free_t)pthread_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        memset(&cleanup, 0, sizeof(cleanup));
        cleanup.wait = wait;
        sky_object_gc_setroot(&(cleanup.condition_arg), condition_arg, SKY_FALSE);
        cleanup.gc_wakeup_function = tlsdata->object_tlsdata.gc_wakeup_function;
        cleanup.gc_wakeup_arg = tlsdata->object_tlsdata.gc_wakeup_arg;
        cleanup.atfork_wakeup_function = tlsdata->atfork_tlsdata.wakeup_function;
        cleanup.atfork_wakeup_arg = tlsdata->atfork_tlsdata.wakeup_arg;
        cleanup.state = wait->state;
        sky_asset_save(&cleanup,
                       sky_thread_wait_cleanup_asset,
                       SKY_ASSET_CLEANUP_ALWAYS);

        wait->state = SKY_THREAD_WAIT_STATE_COND;
        wait->atfork_check = SKY_TRUE;
        wait->gc_needed = SKY_TRUE;
        sky_object_gc_register(sky_thread_wait_gc_wakeup, wait);
        sky_atfork_register(sky_thread_wait_atfork_wakeup, wait);

        for (;;) {
            int rc;

            if (condition && condition(condition_arg)) {
                result = 1;
                break;
            }

            if (wait->atfork_check || wait->gc_needed) {
                sky_bool_t  atfork_check;

                atfork_check = wait->atfork_check;
                wait->atfork_check = SKY_FALSE;

                SKY_ASSET_BLOCK_BEGIN {
                    pthread_mutex_unlock(&(wait->mutex));
                    sky_asset_save(&(wait->mutex),
                                   (sky_free_t)pthread_mutex_lock,
                                   SKY_ASSET_CLEANUP_ALWAYS);

                    if (atfork_check) {
                        sky_atfork_check();
                    }
                    if (wait->gc_needed) {
                        wait->gc_needed = sky_object_gc();
                    }
                } SKY_ASSET_BLOCK_END;

                continue;
            }

            if (SKY_TIME_INFINITE == timeout) {
                rc = pthread_cond_wait(&(wait->cond), &(wait->mutex));
            }
            else {
                rc = pthread_cond_timedwait(&(wait->cond),
                                            &(wait->mutex),
                                            &abstime);
            }
            if (rc) {
                if (ETIMEDOUT == rc) {
                    result = 0;
                }
                else {
                    errno = rc;
                    result = -1;
                }
                break;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
sky_thread_wait_init(sky_thread_wait_t *wait)
{
    pthread_mutex_init(&(wait->mutex), NULL);
    pthread_cond_init(&(wait->cond), NULL);
    wait->fds[0] = wait->fds[1] = -1;
    wait->state = SKY_THREAD_WAIT_STATE_RUNNING;
}


static int
sky_thread_wait_select_internal(sky_thread_wait_t * wait,
                                int                 nfds,
                                fd_set *            readfds,
                                fd_set *            writefds,
                                fd_set *            errorfds,
                                struct timeval *    timeout)
{
    int result;

    pthread_mutex_unlock(&(wait->mutex));
    result = select(nfds, readfds, writefds, errorfds, timeout);
    pthread_mutex_lock(&(wait->mutex));

    if (result > 0) {
        if (FD_ISSET(wait->fds[0], readfds)) {
            char    buffer[PIPE_BUF];
            ssize_t n;

            do {
                if ((n = read(wait->fds[0], buffer, sizeof(buffer))) == -1) {
                    if (EINTR == errno) {
                        continue;
                    }
                    return -1;
                }
            } while (sizeof(buffer) == n);
            --result;
        }
    }

    return result;
}

int
sky_thread_wait_select(sky_thread_wait_t *  wait,
                       int                  nfds,
                       fd_set *             readfds,
                       fd_set *             writefds,
                       fd_set *             errorfds,
                       sky_time_t           timeout)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    int         result;
    fd_set      errorfds_in, errorfds_out, readfds_in, readfds_out,
                writefds_in, writefds_out;
    sky_time_t  now;

    if (!wait) {
        wait = &(tlsdata->wait);
    }

    if (SKY_TIME_INFINITE != timeout) {
        now = sky_time_current();
        if (SKY_TIME_INFINITE - now < timeout) {
            timeout = SKY_TIME_INFINITE;
        }
        else {
            timeout += now;
        }
    }

    if (wait->fds[0] == -1) {
        if (pipe(wait->fds) == -1) {
            return -1;
        }
        fcntl(wait->fds[0], F_SETFL, fcntl(wait->fds[0], F_GETFL) | O_NONBLOCK);
        fcntl(wait->fds[1], F_SETFL, fcntl(wait->fds[1], F_GETFL) | O_NONBLOCK);
    }

    if (readfds) {
        FD_COPY(readfds, &readfds_in);
    }
    else {
        FD_ZERO(&readfds_in);
    }
    FD_SET(wait->fds[0], &readfds_in);
    readfds = &readfds_out;
    if (nfds <= wait->fds[0]) {
        nfds = wait->fds[0] + 1;
    }

    if (writefds) {
        FD_COPY(writefds, &writefds_in);
        writefds = &writefds_out;
    }
    if (errorfds) {
        FD_COPY(errorfds, &errorfds_in);
        errorfds = &errorfds_out;
    }

    SKY_ASSET_BLOCK_BEGIN {
        struct timeval              *tv, tv_struct;
        sky_thread_wait_cleanup_t   cleanup;

        pthread_mutex_lock(&(wait->mutex));
        sky_asset_save(&(wait->mutex),
                       (sky_free_t)pthread_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        memset(&cleanup, 0, sizeof(cleanup));
        cleanup.wait = wait;
        cleanup.gc_wakeup_function = tlsdata->object_tlsdata.gc_wakeup_function;
        cleanup.gc_wakeup_arg = tlsdata->object_tlsdata.gc_wakeup_arg;
        cleanup.atfork_wakeup_function = tlsdata->atfork_tlsdata.wakeup_function;
        cleanup.atfork_wakeup_arg = tlsdata->atfork_tlsdata.wakeup_arg;
        cleanup.state = wait->state;
        sky_asset_save(&cleanup,
                       sky_thread_wait_cleanup_asset,
                       SKY_ASSET_CLEANUP_ALWAYS);

        wait->state = SKY_THREAD_WAIT_STATE_SELECT;
        wait->atfork_check = SKY_TRUE;
        wait->gc_needed = SKY_TRUE;
        sky_object_gc_register(sky_thread_wait_gc_wakeup, wait);
        sky_atfork_register(sky_thread_wait_atfork_wakeup, wait);

        tv = (SKY_TIME_INFINITE == timeout ? NULL : &tv_struct);

        for (;;) {
            sky_time_t      now;
            struct timeval  zero_timeout = { 0, 0 };

            if (SKY_TIME_INFINITE != timeout && sky_time_current() >= timeout) {
                result = 0;
                break;
            }

            FD_COPY(&readfds_in, readfds);
            if (writefds) {
                FD_COPY(&writefds_in, writefds);
            }
            if (errorfds) {
                FD_COPY(&errorfds_in, errorfds);
            }
            result = sky_thread_wait_select_internal(wait,
                                                     nfds,
                                                     readfds,
                                                     writefds,
                                                     errorfds,
                                                     &zero_timeout);
            if (result) {
                break;
            }

            if (wait->atfork_check || wait->gc_needed) {
                sky_bool_t  atfork_check;

                atfork_check = wait->atfork_check;
                wait->atfork_check = SKY_FALSE;

                SKY_ASSET_BLOCK_BEGIN {
                    pthread_mutex_unlock(&(wait->mutex));
                    sky_asset_save(&(wait->mutex),
                                   (sky_free_t)pthread_mutex_lock,
                                   SKY_ASSET_CLEANUP_ALWAYS);

                    if (atfork_check) {
                        sky_atfork_check();
                    }
                    if (wait->gc_needed) {
                        wait->gc_needed = sky_object_gc();
                    }
                } SKY_ASSET_BLOCK_END;

                continue;
            }

            if (SKY_TIME_INFINITE != timeout) {
                now = sky_time_current();
                if (now >= timeout) {
                    result = 0;
                    break;
                }
                tv->tv_sec = (timeout - now) / 1000000000;
                tv->tv_usec = ((timeout - now) % 1000000000) / 1000;
            }

            FD_COPY(&readfds_in, readfds);
            if (writefds) {
                FD_COPY(&writefds_in, writefds);
            }
            if (errorfds) {
                FD_COPY(&errorfds_in, errorfds);
            }
            result = sky_thread_wait_select_internal(wait,
                                                     nfds,
                                                     readfds,
                                                     writefds,
                                                     errorfds,
                                                     tv);
            if (result) {
                break;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

void
sky_thread_wait_signal(sky_thread_wait_t *wait)
{
    SKY_ASSET_BLOCK_BEGIN {
        pthread_mutex_lock(&(wait->mutex));
        sky_asset_save(&(wait->mutex),
                       (sky_free_t)pthread_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_thread_wait_signal_locked(wait);
    } SKY_ASSET_BLOCK_END;
}


void
sky_thread_waitlist_broadcast(sky_thread_waitlist_t *list)
{
    ssize_t i;

    if (list->len) {
        pthread_mutex_lock(&(list->mutex));
        for (i = 0; i < list->len; ++i) {
            sky_thread_wait_signal(list->waiters[i]);
        }
        pthread_mutex_unlock(&(list->mutex));
    }
}

void
sky_thread_waitlist_cleanup(sky_thread_waitlist_t *list)
{
    sky_free(list->waiters);
    pthread_mutex_destroy(&(list->mutex));
}

void
sky_thread_waitlist_dequeue(sky_thread_waitlist_t * list,
                            sky_thread_wait_t *     wait)
{
    ssize_t i;

    if (list->len) {
        pthread_mutex_lock(&(list->mutex));
        for (i = 0; i < list->len; ++i) {
            if (list->waiters[i] == wait) {
                memmove(&(list->waiters[i]),
                        &(list->waiters[i + 1]),
                        (list->len - (i + 1)) * sizeof(sky_thread_wait_t *));
                --list->len;
                pthread_mutex_unlock(&(list->mutex));
                return;
            }
        }
        pthread_mutex_unlock(&(list->mutex));
    }
    sky_error_raise_errno(sky_OSError, ESRCH);
}

void
sky_thread_waitlist_enqueue(sky_thread_waitlist_t * list,
                            sky_thread_wait_t *     wait)
{
    pthread_mutex_lock(&(list->mutex));
    if (list->len == list->capacity) {
        list->capacity = (list->capacity ? list->capacity * 2
                                         : (ssize_t)
                                           (SKY_CACHE_LINE_SIZE /
                                            sizeof(sky_thread_wait_t *)));

        list->waiters = sky_realloc(list->waiters,
                                    (list->capacity *
                                     sizeof(sky_thread_wait_t *)));
    }
    list->waiters[list->len++] = wait;
    pthread_mutex_unlock(&(list->mutex));
}

void
sky_thread_waitlist_init(sky_thread_waitlist_t *list)
{
    int rc;

    memset(list, 0, sizeof(sky_thread_waitlist_t));
    if ((rc = pthread_mutex_init(&(list->mutex), NULL)) != 0) {
        sky_error_raise_errno(sky_OSError, rc);
    }
}

void
sky_thread_waitlist_reset(sky_thread_waitlist_t *   list,
                          sky_thread_wait_t *       wait)
{
    ssize_t i, len;

    /* Re-initialize the list's wait mutex. See the comments in sky_atfork.c
     * for re-initialization of sky_atfork_mutex for details.
     */
    memset(&(list->mutex), 0, sizeof(list->mutex));
    pthread_mutex_init(&(list->mutex), NULL);

    if ((len = list->len) > 0) {
        list->len = 0;
        for (i = 0; i < len; ++i) {
            if (list->waiters[i] == wait) {
                list->waiters[0] = wait;
                list->len = 1;
                break;
            }
        }
    }
}

void
sky_thread_waitlist_signal(sky_thread_waitlist_t *list)
{
    if (list->len) {
        pthread_mutex_lock(&(list->mutex));
        if (list->len) {
            sky_thread_wait_signal(list->waiters[0]);
        }
        pthread_mutex_unlock(&(list->mutex));
    }
}


SKY_TYPE_DEFINE_SIMPLE(thread,
                       "thread",
                       sizeof(sky_thread_data_t),
                       sky_thread_instance_initialize,
                       sky_thread_instance_finalize,
                       NULL,
                       SKY_TYPE_FLAG_HAS_DICT,
                       NULL);

void
sky_thread_initialize_library(void)
{
    sky_thread_stacksize_value = 0;

    sky_type_initialize_builtin(sky_thread_type, 0);

    sky_type_setmethodslots(sky_thread_type,
            "__repr__", sky_thread_repr,
            NULL);

    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky_thread_main_object),
                                 sky_thread_self(),
                                 SKY_TRUE);
}
