/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_complex Complex Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_COMPLEX_H__
#define __SKYTHON_CORE_SKY_COMPLEX_H__ 1


SKY_CDECLS_BEGIN


/** Return the absolute value of a complex number.
  * This is the implementation of __abs__().
  *
  * @param[in]  self    the complex number for which the absolute value is to
  *                     be returned.
  * @return     the absolute value of @a self.
  */
SKY_EXTERN sky_float_t
sky_complex_abs(sky_complex_t self);


/** Add two complex numbers.
  * This is the implementation of __add__().
  *
  * @param[in]  self    the addend.
  * @param[in]  other   the augend. May be any object that implements the
  *                     __complex__() method to return a complex number.
  * @return     a new complex number that is the sum of @a self and @a other,
  *             or @c sky_NotImplemented if @a other cannot be converted to a
  *             complex number.
  */
SKY_EXTERN sky_object_t
sky_complex_add(sky_complex_t self, sky_object_t other);


/** Return the complex conjugate of a complex number.
  *
  * @param[in]  self    the complex number to conjugate.
  * @return     the complex conjugate of @a self.
  */
SKY_EXTERN sky_complex_t
sky_complex_conjugate(sky_complex_t self);


/** Make a copy of a complex object.
  * 
  * @param[in]  self    the complex object to copy.
  * @return     a copy of @a self, which may be @a self if its type is
  *             @c sky_complex_type rather than a sub-type.
  */
SKY_EXTERN sky_complex_t
sky_complex_copy(sky_complex_t self);


/** Create a new complex object instance.
  *
  * @param[in]  real    the real part of the complex number.
  * @param[in]  imag    the imaginary part of the complex number.
  * @return     the new complex object instance.
  */
SKY_EXTERN sky_complex_t
sky_complex_create(double real, double imag);


/** Create a new complex object instance from an array of ASCII characters.
  *
  * @param[in]  bytes   the array of bytes to parse.
  * @param[in]  nbytes  the number of bytes present in @a bytes.
  * @return     the new complex object instance.
  */
SKY_EXTERN sky_complex_t
sky_complex_createfromascii(const char *bytes, size_t nbytes);


/** Create a new complex object instance by parsing a string.
  *
  * @param[in]  source  the string to parse, which may be a string object
  *                     or any object supporting the buffer protocol.
  * @return     the new complex object instance.
  */
SKY_EXTERN sky_complex_t
sky_complex_createfromstring(sky_object_t source);


/** Return the imaginary portion of a complex number.
  *
  * @param[in]  self    the complex number for which the imaginary portion is
  *                     to be returned.
  * @return     the imaginary portion of @a self.
  */
SKY_EXTERN double
sky_complex_imag(sky_complex_t self);


/** Multiply two complex numbers.
  * This is the implementation of __mul__().
  *
  * @param[in]  self    the multicand.
  * @param[in]  other   the multiplier. May be any object that implements the
  *                     __complex__() method to return a complex number.
  * @return     a new complex number that is the product of @a self and
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a complex number.
  */
SKY_EXTERN sky_object_t
sky_complex_mul(sky_complex_t self, sky_object_t other);


/** Negate a complex number.
  * This is the implementation of __neg__().
  *
  * @param[in]  self    the complex number to be negated.
  * @return     the negative of @a self.
  */
SKY_EXTERN sky_complex_t
sky_complex_neg(sky_complex_t self);


/** Raise a complex number to the power of another.
  * This is the implementation of __pow__().
  *
  * @param[in]  self    the base.
  * @param[in]  other   the exponent. May be any object that implements the
  *                     __complex__() method to reutrn a complex number.
  * @param[in]  third   not supported; must be @c NULL or @c sky_None.
  * @return     a new complex number that is @a self raised to the power of
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a complex number.
  */
SKY_EXTERN sky_object_t
sky_complex_pow(sky_complex_t self, sky_object_t other, sky_object_t third);


/** Return the real portion of a complex number.
  *
  * @param[in]  self    the complex number for which the real portion is to be
  *                     returned.
  * @return     the real portion of @a self.
  */
SKY_EXTERN double
sky_complex_real(sky_complex_t self);


/** Return a string representation of a complex number.
  * This is the implementation of __repr__().
  *
  * @param[in]  self    the complex number for which the string representation
  *                     is to be returned.
  * @return     the string representation of @a self, which may be passed back
  *             to sky_complex_createfromstring() to create a new complex
  *             object that is equal to @a self.
  */
SKY_EXTERN sky_string_t
sky_complex_repr(sky_complex_t self);


/** Subtract two complex numbers.
  * This is the implementation of __sub__().
  *
  * @param[in]  self    the minuend.
  * @param[in]  other   the subtrahend. May be any object that implements the
  *                     __complex__() method to return a complex number.
  * @return     a new complex number that is the difference of @a self and
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a complex number.
  */
SKY_EXTERN sky_object_t
sky_complex_sub(sky_complex_t self, sky_object_t other);


/** Divide two complex numbers.
  * This is the implementation of __truediv__().
  *
  * @param[in]  self    the dividend.
  * @param[in]  other   the divisor. May be any object that implements the
  *                     __complex__() method to return a complex number.
  * @return     a new complex number that is the quotient of @a self and
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a complex number.
  */
SKY_EXTERN sky_object_t
sky_complex_truediv(sky_complex_t self, sky_object_t other);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_COMPLEX_H__ */

/** @} **/
/** @} **/
