/** @file
  * @brief Miscellaneous utilities
  * @defgroup sky_util Miscellaneous Utilities
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_UTIL_H__
#define __SKYTHON_CORE_SKY_UTIL_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** Compute a check value for a buffer of bytes using CRC-32.
  *
  * @param[in]  bytes   the array of bytes
  * @param[in]  nbytes  the number of bytes in @a bytes.
  * @param[in]  seed    the starting CRC-32 value.
  * @return     the CRC-32 check value.
  */
SKY_EXTERN uint32_t
sky_util_crc32(const void *bytes, size_t nbytes, uint32_t seed);


/** Find the first bit set in an unsigned int value.
  * @return If the value is 0, 0 will be returned; otherwise, the return value
  *         will be the one-based index of the least significant bit set in the
  *         value.
  */
SKY_EXTERN int
sky_util_ffs(unsigned int value);

/** Find the first bit set in an unsigned long value.
  * @return If the value is 0, 0 will be returned; otherwise, the return value
  *         will be the one-based index of the least significant bit set in the
  *         value.
  *
  * @note   This function is defined using size_t, which is normally defined
  *         to be unsigned long. The problem is that on 64-bit Windows,
  *         unsigned long is still 32 bits, but size_t is 64-bits. It is
  *         therefore safer to use size_t instead of unsigned long.
  */
SKY_EXTERN int
sky_util_ffsl(size_t value);

/** Find the last bit set in an unsigned int value.
  * @return If the value is 0, 0 will be returned; otherwise, the return value
  *         will be the one-based index of the most significant bit set in the
  *         value.
  */
SKY_EXTERN int
sky_util_fls(unsigned int value);

/** Find the last bit set in an unsigned long value.
  * @return If the value is 0, 0 will be returned; otherwise, the return value
  *         will be the one-based index of the most significant bit set in the
  *         value.
  *
  * @note   This function is defined using size_t, which is normally defined
  *         to be unsigned long. The problem is that on 64-bit Windows,
  *         unsigned long is still 32 bits, but size_t is 64-bits. It is
  *         therefore safer to use size_t instead of unsigned long.
  */
SKY_EXTERN int
sky_util_flsl(size_t value);


/** Compute a 32-bit hash value for a buffer of bytes. **/
SKY_EXTERN uint32_t
sky_util_hashbytes32(const void *bytes, size_t nbytes, uint32_t seed);

/** Compute a 32-bit hash value for a buffer of bytes case-insensitively for
  * 7-bit ASCII values.
  * Byte values in the ranges 0x41-0x5A and 0x61-0x7A will be considered
  * equivalent.
  */
SKY_EXTERN uint32_t
sky_util_hashbytes32_nocase(const void *bytes, size_t nbytes, uint32_t seed);

/** Compute a 64-bit hash value for a buffer of bytes. **/
SKY_EXTERN uint64_t
sky_util_hashbytes64(const void *bytes, size_t nbytes, uint64_t seed);

/** Compute a 64-bit hash value for a buffer of bytes case-insensitively for
  * 7-bit ASCII values.
  * Byte values in the ranges 0x41-0x5A and 0x61-0x7A will be considered
  * equivalent.
  */
SKY_EXTERN uint64_t
sky_util_hashbytes64_nocase(const void *bytes, size_t nbytes, uint64_t seed);


/** Compute a 32-bit or 64-bit hash value for a buffer of bytes. **/
SKY_EXTERN_INLINE uintptr_t
sky_util_hashbytes(const void *bytes, size_t nbytes, uintptr_t seed)
{
#if defined(SKY_ARCH_32BIT)
    return sky_util_hashbytes32(bytes, nbytes, seed);
#elif defined(SKY_ARCH_64BIT)
    return sky_util_hashbytes64(bytes, nbytes, seed);
#else
#   error implementation missing
#endif
}

/** Compute a 32-bit or 64-bit hash value for a buffer of bytes
  * case-insensitively for 7-bit ASCII values.
  * Byte values in the ranges 0x41-0x5A and 0x61-0x7A will be considered
  * equivalent.
  */
SKY_EXTERN_INLINE uintptr_t
sky_util_hashbytes_nocase(const void *bytes, size_t nbytes, uintptr_t seed)
{
#if defined(SKY_ARCH_32BIT)
    return sky_util_hashbytes32_nocase(bytes, nbytes, seed);
#elif defined(SKY_ARCH_64BIT)
    return sky_util_hashbytes64_nocase(bytes, nbytes, seed);
#else
#   error implementation missing
#endif
}


/** Compute a 32-bit or 64-bit hash value for a double value.
  *
  * @param[in]  value   the double value for which a hash value is to be
  *                     computed.
  * @return     the computed hash value for @a value.
  */
SKY_EXTERN uintptr_t
sky_util_hashdouble(double value);


/** Round a value up to the nearest power of 2. **/
SKY_EXTERN size_t
sky_util_roundpow2(size_t value);


/** Perform a bit reversal on an 8-bit value. **/
SKY_EXTERN uint8_t
sky_util_reverse8(uint8_t value);

/** Perform a bit reversal on a 16-bit value. **/
SKY_EXTERN uint16_t
sky_util_reverse16(uint16_t value);

/** Perform a bit reversal on a 32-bit value. **/
SKY_EXTERN uint32_t
sky_util_reverse32(uint32_t value);

/** Perform a bit reversal on a 64-bit value. **/
SKY_EXTERN uint64_t
sky_util_reverse64(uint64_t value);


/** Return a pseudorandom 32-bit value. **/
SKY_EXTERN uint32_t
sky_util_random32(void);

/** Return a pseudorandom 64-bit value. **/
SKY_EXTERN uint64_t
sky_util_random64(void);


/** Return an arbitrary number of random bytes.
  * On systems that have it, the random bytes come from /dev/urandom. On
  * Windows, the random bytes come from @c CryptGenRandom().
  *
  * @param[out] bytes       the buffer into which the random bytes are to be
  *                         placed. May not be NULL, and must be at least as
  *                         big as @a nbytes.
  * @param[in]  nbytes      the number of random bytes to place into @a bytes.
  */
SKY_EXTERN void
sky_util_randombytes(void *bytes, size_t nbytes);


/** Perform an inplace sort on an array using quicksort.
  *
  * @param[in]  base        the array to sort.
  * @param[in]  nel         the number of elements in the array to sort.
  * @param[in]  width       the size in bytes of each element in the array.
  * @param[in]  compare     a function to be called to compare two items in the
  *                         array.
  * @param[in]  arg         an arbitrary pointer to be passed to the comparison
  *                         function.
  */
SKY_EXTERN void
sky_util_quicksort(void *   base,
                   size_t   nel,
                   size_t   width,
                   int      (*compare)(const void *, const void *, void *),
                   void *   arg);


/** Return a list of files in a directory.
  *
  * @param[in]  dirname     the directory to list.
  * @return     a @c NULL-terminated array of files contained by the specified
  *             directory. The memory for this array is dynamically allocated
  *             and must be freed by the caller via sky_free().
  */
SKY_EXTERN char **
sky_util_listdir(const char *dirname);


/** Make a directory file, optionally creating intermediate directories as
  * required.
  *
  * @param[in]  path            the directory file to create.
  * @param[in]  mode            the mode to use for creating the directry file.
  * @param[in]  intermediate    @c SKY_TRUE to create intermediate directories
  *                             as required. Intermediate directories are
  *                             created with mode 0777.
  */
SKY_EXTERN void
sky_util_mkdir(const char *path, unsigned int mode, sky_bool_t intermediate);


/** Convert a signed integer to an ASCII base 10 string representation.
  * This function does not write a nul-byte into the output buffer.
  *
  * @param[in]  value           the value to be converted.
  * @param[in]  buffer          the buffer into which the conversion is to be
  *                             written.
  * @param[in]  buffer_size     the size of @a buffer in bytes.
  * @return     the number of bytes written into the buffer, or the number of
  *             bytes that would be required if @a buffer_size is too small.
  */
SKY_EXTERN size_t
sky_util_itoa(intmax_t value, char *buffer, size_t buffer_size);

/** Convert an unsigned integer to an ASCII base 10 string representation.
  * This function does not write a nul-byte into the output buffer.
  *
  * @param[in]  value           the value to be converted.
  * @param[in]  buffer          the buffer into which the conversion is to be
  *                             written.
  * @param[in]  buffer_size     the size of @a buffer in bytes.
  * @return     the number of bytes written into the buffer, or the number of
  *             bytes that would be required if @a buffer_size is too small.
  */
SKY_EXTERN size_t
sky_util_utoa(uintmax_t value, char *buffer, size_t buffer_size);


/** Constants to be used with sky_util_dota()'s flags parameter. **/
typedef enum sky_util_dtoa_e {
    SKY_UTIL_DTOA_ADD_DOT_0         =   0x1,
    SKY_UTIL_DTOA_ALTERNATE_FORM    =   0x2,
    SKY_UTIL_DTOA_ALWAYS_SIGN       =   0x4,
    SKY_UTIL_DTOA_LOCALE_DECIMAL    =   0x8,
} sky_util_dtoa_t;

/** Convert a double floating-point value to an ASCII string representation.
  * This function will allocate the space needed to perform the conversion to
  * a string. The caller is responsible for releasing the memory when it's no
  * longer needed via sky_free().
  *
  * @param[in]   value      the value to be converted.
  * @param[in]   type       the type of formatting to use, which must be one of
  *                         'e', 'E', 'f', 'F', 'g', 'G', or 'r'.
  * @param[in]   precision  the precision to use, which has different meanings
  *                         depending on the value of @a type, which correspond
  *                         to ecvt(), fcvt(), and gcvt() standard functions.
  * @param[in]   flags      flags to control the conversion, combined via
  *                         bitwise-or from the @c sky_util_dtoa_t constants.
  * @return      the string representation of @a value, which must always be
  *              released by calling via sky_free() when it's no longer needed.
  */
SKY_EXTERN char *
sky_util_dtoa(double value, char type, int precision, unsigned int flags);


/** Parse a floating point value from an array of ASCII characters.
  * This implementation of the standard C function strtod() does not consider
  * the active locale for the thread or process.
  *
  * @param[in]  s           the ASCII string to parse.
  * @param[in]  slen        the length of the ASCII string to parse.
  * @param[out] endptr      the last character + 1 consumed during parsing.
  *                         May be @c NULL if the information is not wanted.
  * @return     the value parsed from @a s.
  */
SKY_EXTERN double
sky_util_strtod(const char *s, size_t slen, char **endptr);


/** Encode a single precision floating point number into an encoding suitable
  * for transport.
  * On platforms that appear to use IEEE-754 formats, this function works by
  * copying bits. On other platforms, the format is identical to the IEEE-754
  * single precision format, although the encoding of INFs and NaNs (if such
  * things exist on the platform) isn't handled correctly. On non-IEEE
  * platforms with more precision, or larger dynamic range, than 754 supports,
  * not all values can be encoded. What happens in such cases is partly
  * accidental. This function will always write exactly 4 bytes.
  *
  * @param[in]  value       the value to encode.
  * @param[in]  bytes       the array of bytes into which the encoded bytes are
  *                         to be written.
  * @param[in]  nbytes      the number of bytes available in @a bytes.
  * @param[in]  endian      the endian to use, which should be either
  *                         @c SKY_ENDIAN_BIG or @c SKY_ENDIAN_LITTLE. A value
  *                         of 0 will use the host's endian.
  * @return     if the value is encoded successfully, the next byte available
  *             for use in @a bytes; otherwise, @c NULL if there is not enough
  *             room to encode @a value.
  */
SKY_EXTERN uint8_t *
sky_util_float_encode(float value, uint8_t *bytes, size_t nbytes, int endian);


/** Decode a single precision floating point number from an encoding suitable
  * for transport.
  * On platforms that appear to use IEEE-754 formats, this function works by
  * copying bits. On other platforms, the format is identical to the IEEE-754
  * single precision format, although the encoding of INFs and NaNs (if such
  * things exist on the platform) isn't handled correctly. On non-IEEE
  * platforms with less precision, or smaller dynamic range, than 754 supports,
  * not all values can be decoded. What happens in such cases is partly
  * accidental.
  *
  * @param[in]  bytes       the array of bytes containing the encoded value to
  *                         be decoded.
  * @param[in]  nbytes      the number of bytes available to consider for
  *                         decoding in @a bytes, which may be more than is
  *                         needed to decode a value.
  * @param[in]  endian      the endian to use, which should be either
  *                         @c SKY_ENDIAN_BIG or @c SKY_ENDIAN_LITTLE. A value
  *                         of 0 will use the host's endian.
  * @param[out] value       the decoded value.
  * @return     if the value is decoded successfully, the next byte after the
  *             last byte used for decoding; otherwise, @c NULL if there is not
  *             a valid encoded float present. If the decoded data overflows
  *             @a value, there is no indication passed back to the caller.
  */
SKY_EXTERN const uint8_t *
sky_util_float_decode(const uint8_t *   bytes,
                      size_t            nbytes,
                      int               endian,
                      float *           value);


/** Encode a double precision floating point number into an encoding suitable
  * for transport.
  * On platforms that appear to use IEEE-754 formats, this function works by
  * copying bits. On other platforms, the format is identical to the IEEE-754
  * double precision format, although the encoding of INFs and NaNs (if such
  * things exist on the platform) isn't handled correctly. On non-IEEE
  * platforms with more precision, or larger dynamic range, than 754 supports,
  * not all values can be encoded. What happens in such cases is partly
  * accidental. This function will always write exactly 8 bytes.
  *
  * @param[in]  value       the value to encode.
  * @param[in]  bytes       the array of bytes into which the encoded bytes are
  *                         to be written.
  * @param[in]  nbytes      the number of bytes available in @a bytes.
  * @param[in]  endian      the endian to use, which should be either
  *                         @c SKY_ENDIAN_BIG or @c SKY_ENDIAN_LITTLE. A value
  *                         of 0 will use the host's endian.
  * @return     if the value is encoded successfully, the next byte available
  *             for use in @a bytes; otherwise, @c NULL if there is not enough
  *             room to encode @a value (it may be partially encoded).
  */
SKY_EXTERN uint8_t *
sky_util_double_encode(double value, uint8_t *bytes, size_t nbytes, int endian);


/** Decode a double precision floating point number from an encoding suitable
  * for transport.
  * On platforms that appear to use IEEE-754 formats, this function works by
  * copying bits. On other platforms, the format is identical to the IEEE-754
  * double precision format, although the encoding of INFs and NaNs (if such
  * things exist on the platform) isn't handled correctly. On non-IEEE
  * platforms with less precision, or smaller dynamic range, than 754 supports,
  * not all values can be decoded. What happens in such cases is partly
  * accidental.
  *
  * @param[in]  bytes       the array of bytes containing the encoded value to
  *                         be decoded.
  * @param[in]  nbytes      the number of bytes available to consider for
  *                         decoding in @a bytes, which may be more than is
  *                         needed to decode a value.
  * @param[in]  endian      the endian to use, which should be either
  *                         @c SKY_ENDIAN_BIG or @c SKY_ENDIAN_LITTLE. A value
  *                         of 0 will use the host's endian.
  * @param[out] value       the decoded value.
  * @return     if the value is decoded successfully, the next byte after the
  *             last byte used for decoding; otherwise, @c NULL if there is not
  *             a valid encoded float present. If the decoded data overflows
  *             @a value, there is no indication passed back to the caller.
  */
SKY_EXTERN const uint8_t *
sky_util_double_decode(const uint8_t *  bytes,
                       size_t           nbytes,
                       int              endian,
                       double *         value);


/** Encode an integer into a variable length encoding suitable for transport.
  * The encoding used is simple: seven bits are encoded per byte, with the high
  * bit set if more bits follow. The bits are encoded from right to left, low
  * to high. Note that negative values cannot be encoded.
  *
  * @param[in]  value       the value to be encoded.
  * @param[in]  bytes       the array of bytes into which the encoded bytes are
  *                         to be written.
  * @param[in]  nbytes      the number of bytes available in @a bytes.
  * @return     if the value is encoded successfully, the next byte available
  *             for use in @a bytes; otherwise, @c NULL if there is not enough
  *             room to encode @a value (it may be partially encoded).
  */
SKY_EXTERN uint8_t *
sky_util_varint_encode(uintmax_t value, uint8_t *bytes, size_t nbytes);


/** Decode an integer from a variable length encoding.
  *
  * @param[in]  bytes       the array of bytes containing the encoded value to
  *                         be decoded.
  * @param[in]  nbytes      the number of bytes available to consider for
  *                         decoding in @a bytes, which may be more than is
  *                         needed to decode a value.
  * @param[out] value       the decoded value.
  * @return     if the value is decoded successfully, the next byte after the
  *             last byte used for decoding; otherwise, @c NULL if there is not
  *             a valid encoded integer present. If the decoded data overflows
  *             @a value, there is no indication passed back to the caller.
  */
SKY_EXTERN const uint8_t *
sky_util_varint_decode(const uint8_t *bytes, size_t nbytes, uintmax_t *value);


/** Return the number of bytes needed to encoding a value in a variable length
  * encoding.
  *
  * @param[in]  value   the value to be encoded.
  * @return     the number of bytes needed to encode @a value.
  */
SKY_EXTERN size_t
sky_util_varint_size(size_t value);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_UTIL_H__ */

/** @} **/
