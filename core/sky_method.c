#include "sky_private.h"


typedef struct sky_method_data_s {
    sky_object_t                        callable;
    sky_object_t                        self;
} sky_method_data_t;

SKY_EXTERN_INLINE sky_method_data_t *
sky_method_data(sky_object_t object)
{
    if (sky_method_type == sky_object_type(object)) {
        return (sky_method_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_method_type);
}


static void
sky_method_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_method_data_t   *method_data = data;

    sky_object_visit(method_data->callable, visit_data);
    sky_object_visit(method_data->self, visit_data);
}


static sky_object_t
sky_method_doc_getter(sky_object_t instance, SKY_UNUSED sky_type_t type)
{
    return sky_object_getattr(sky_method_data(instance)->callable,
                              SKY_STRING_LITERAL("__doc__"),
                              sky_NotSpecified);
}


sky_object_t
sky_method_call(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_method_data_t   *method_data = sky_method_data(self);

    sky_tuple_t bound_args;

    if (!args) {
        bound_args = sky_tuple_pack(1, method_data->self);
    }
    else {
        bound_args = sky_tuple_insert(args, 0, method_data->self);
    }

    return sky_object_call(method_data->callable, bound_args, kws);
}


sky_object_t
sky_method_eq(sky_object_t self, sky_object_t other)
{
    sky_object_t        result;
    sky_method_data_t   *self_data, *other_data;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(other, sky_method_type)) {
        return sky_NotImplemented;
    }

    self_data = sky_method_data(self);
    other_data = sky_method_data(other);
    result = sky_object_eq(self_data->callable, other_data->callable);
    if (sky_True != result) {
        return result;
    }

    return sky_object_eq(self_data->self, other_data->self);
}


sky_object_t
sky_method_function(sky_method_t self)
{
    return sky_method_data(self)->callable;
}


sky_object_t
sky_method_getattribute(sky_object_t self, sky_object_t name)
{
    sky_object_t    descriptor;

    if ((descriptor = sky_type_lookup(sky_object_type(self), name)) != NULL) {
        return sky_descriptor_get(descriptor, self, sky_object_type(self));
    }

    return sky_object_getattr(sky_method_data(self)->callable,
                              name,
                              sky_NotSpecified);
}


uintptr_t
sky_method_hash(sky_object_t self)
{
    sky_method_data_t   *method_data = sky_method_data(self);

    return sky_object_hash(method_data->callable) ^ 
           sky_object_hash(method_data->self);
}


void
sky_method_init(sky_object_t self, sky_object_t function, sky_object_t instance)
{
    sky_method_data_t   *method_data = sky_method_data(self);

    if (!SKY_OBJECT_METHOD_SLOT(function, CALL)) {
        sky_error_raise_string(sky_TypeError,
                               "first argument must be callable");
    }
    if (!instance || sky_None == instance) {
        sky_error_raise_string(sky_TypeError,
                               "instance must not be None");
    }

    sky_object_gc_set(&(method_data->callable), function, self);
    sky_object_gc_set(&(method_data->self), instance, self);
}


sky_string_t
sky_method_repr(sky_object_t self)
{
    sky_method_data_t   *method_data = sky_method_data(self);

    sky_object_t    name;

    name = sky_object_getattr(method_data->callable,
                              SKY_STRING_LITERAL("__name__"),
                              SKY_STRING_LITERAL("?"));
    return sky_string_createfromformat(
                "<bound method %@.%@ of %#@>",
                sky_type_name(sky_object_type(method_data->self)),
                name,
                method_data->self);
}


sky_method_t
sky_method_create(sky_object_t function, sky_object_t instance)
{
    sky_method_t        method;
    sky_method_data_t   *method_data;

    if (!SKY_OBJECT_METHOD_SLOT(function, CALL)) {
        sky_error_raise_string(sky_TypeError,
                               "first argument must be callable");
    }
    if (!instance || sky_None == instance) {
        sky_error_raise_string(sky_TypeError,
                               "instance must not be None");
    }

    method = sky_object_allocate(sky_method_type);
    method_data = sky_method_data(method);
    sky_object_gc_set(&(method_data->callable), function, method);
    sky_object_gc_set(&(method_data->self), instance, method);

    return method;
}


SKY_TYPE_DEFINE_SIMPLE(method,
                       "method",
                       sizeof(sky_method_data_t),
                       NULL,
                       NULL,
                       sky_method_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_method_initialize_library(void)
{
    sky_type_setattr_member(
            sky_method_type,
            "__func__",
            "the function (or other callable) implementing a method",
            offsetof(sky_method_data_t, callable),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_READONLY);
    sky_type_setattr_member(
            sky_method_type,
            "__self__",
            "the instance to which a method is bound",
            offsetof(sky_method_data_t, self),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_READONLY);

    /* Forward __doc__ to the method's callable's __doc__ */
    sky_type_setattr_getset(
            sky_method_type,
            "__doc__",
            NULL,
            sky_method_doc_getter,
            NULL);

    sky_type_setmethodslotwithparameters(
            sky_method_type,
            "__init__",
            (sky_native_code_function_t)sky_method_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "function", SKY_DATA_TYPE_OBJECT, NULL,
            "instance", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_method_type,
            "__repr__", sky_method_repr,
            "__eq__", sky_method_eq,
            "__hash__", sky_method_hash,
            "__getattribute__", sky_method_getattribute,
            "__call__", sky_method_call,
            NULL);
}
