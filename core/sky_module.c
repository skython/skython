#include "sky_private.h"
#include "sky_frame_private.h"
#include "sky_module_private.h"

#include "sky_code.h"
#include "sky_marshal.h"

#include "../modules/builtin_modules.h"

#if defined(HAVE_DLFCN_H)
#   include <dlfcn.h>
#endif
#if defined(HAVE_SYS_PARAM_H)
#   include <sys/param.h>
#endif


static sky_mutex_t sky_module_import_lock = NULL;


static sky_dict_t   sky_module_dynamic_modules = NULL;
static sky_mutex_t  sky_module_dynamic_modules_mutex = NULL;


typedef struct sky_module_data_s {
#if defined(HAVE_DLFCN_H)
    void *                              dlhandle;
#endif

    void *                              data;
    sky_module_visit_t                  visit;
    sky_free_t                          destructor;
} sky_module_data_t;

SKY_EXTERN_INLINE sky_module_data_t *
sky_module_data(sky_object_t object)
{
    return sky_object_data(object, sky_module_type);
}

struct sky_module_s {
    sky_object_data_t                   object_data;
    sky_module_data_t                   module_data;
};


static void
sky_module_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_module_data_t   *module_data = data;

    if (module_data->destructor) {
        module_data->destructor(module_data->data);
    }
}

static void
sky_module_instance_visit(sky_object_t              object,
                          void *                    data,
                          sky_object_visit_data_t * visit_data)
{
    sky_module_data_t   *module_data = data;

    if (module_data->visit) {
        module_data->visit(object, module_data->data, visit_data);
    }
}


#if 0
/* This is commented out rather than deleted to leave context for this comment.
 *
 * Do not unload dynamically loaded modules. Ever. Why? Objects may exist that
 * refer back to code in the unloaded module. For example, if a module defines
 * a type and instances of that type exist, any supporting code for that type
 * might get unloaded if no references to the module exist while instances of
 * the type still exist. There's no easy way to know when it's safe to unload
 * the module.
 */
static void
sky_module_instance_finalize(SKY_UNUSED sky_object_t self, void *instance_data)
{
    sky_module_data_t   *self_data = instance_data;

#if defined(HAVE_DLFCN_H)
    if (self_data->dlhandle) {
        dlclose(self_data->dlhandle);
        self_data->dlhandle = NULL;
    }
#endif
}
#endif


static void
sky_module_setup(sky_module_t   module,
                 sky_string_t   name,
                 sky_object_t   doc,
                 sky_object_t   package)
{
    sky_object_setattr(module,
                       SKY_STRING_LITERAL("__name__"),
                       name);
    sky_object_setattr(module,
                       SKY_STRING_LITERAL("__doc__"),
                       (doc ? doc : sky_None));
    sky_object_setattr(module,
                       SKY_STRING_LITERAL("__package__"),
                       (package ? package : sky_None));
}

sky_module_t
sky_module_create(sky_string_t name)
{
    sky_module_t    module;

    module = sky_object_allocate(sky_module_type);
    sky_module_setup(module, name, NULL, NULL);

    return module;
}


sky_list_t
sky_module_dir(sky_module_t self)
{
    sky_dict_t  dict;

    dict = sky_object_getattr(self,
                              SKY_STRING_LITERAL("__dict__"),
                              sky_NotSpecified);
    if (sky_object_isa(dict, sky_dict_type)) {
        return sky_dict_keys(dict);
    }

    sky_error_raise_format(sky_TypeError,
                           "%@.__dict__ is not a dictionary",
                           sky_object_getattr(self,
                                              SKY_STRING_LITERAL("__name__"),
                                              sky_NotSpecified));
}


sky_object_t
sky_module_getbuiltin(const char *name)
{
    sky_string_t    string;

    string = sky_string_createfrombytes(name, strlen(name), NULL, NULL);
    return sky_object_getattr(sky_interpreter_module_builtins(),
                              string,
                              sky_NotSpecified);
}


void *
sky_module_getdata(sky_module_t self)
{
    return sky_module_data(self)->data;
}


sky_module_t
sky_module_import(sky_string_t name)
{
    sky_object_t    args, builtins, __import__;

    if (!(builtins = sky_dict_get(sky_interpreter_globals(),
                                  SKY_STRING_LITERAL("__builtins__"),
                                  NULL)))
    {
        builtins = sky_interpreter_builtins();
    }
    if (sky_object_isa(builtins, sky_module_type)) {
        builtins = sky_object_dict(builtins);
    }
    __import__ = sky_object_getitem(builtins,
                                    SKY_STRING_LITERAL("__import__"));

    args = sky_tuple_pack(5, name,                      /* name */
                             sky_None,                  /* globals */
                             sky_None,                  /* locals */
                             sky_tuple_empty,           /* fromlist */
                             sky_integer_zero);         /* level */

    return sky_object_call(__import__, args, NULL);
}


static void
sky_module_initialize_cmodule_cleanup(sky_frame_t frame)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                          frame,
                          SKY_FALSE);
}


static void
sky_module_initialize_cmodule(sky_module_t module,
                              void (*initialize)(sky_module_t module))
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    void                    *arg, **argv;
    sky_dict_t              globals;
    sky_frame_t             frame;
    sky_native_code_t       code;
    sky_parameter_list_t    parameters;

    parameters = sky_parameter_list_create();
    sky_parameter_list_add(parameters,
                           SKY_STRING_LITERAL("self"),
                           SKY_DATA_TYPE_OBJECT_MODULE,
                           NULL);

    code = sky_native_code_create((sky_native_code_function_t)initialize,
                                  SKY_DATA_TYPE_VOID,
                                  parameters);

    SKY_ASSET_BLOCK_BEGIN {
        globals = sky_object_dict(module);
        frame = sky_frame_create(code, globals, globals);
        sky_asset_save(tlsdata->current_frame,
                       (sky_free_t)sky_module_initialize_cmodule_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                              frame,
                              SKY_FALSE);

        argv = &arg;
        argv[0] = &module;
        sky_native_code_execute(code, argv, globals, globals);
    } SKY_ASSET_BLOCK_END;
}


sky_module_t
sky_module_import_builtin(sky_string_t name)
{
    const char              *cstring;
    sky_module_t            module;
    sky_builtin_module_t    *builtin;

    if (!(cstring = sky_string_cstring(name))) {
        return NULL;
    }
    for (builtin = sky_builtin_modules; builtin->name; ++builtin) {
        if (!builtin->frozen_bytes && !strcmp(builtin->name, cstring)) {
            break;
        }
    }
    if (!builtin->name) {
        return NULL;
    }

    module = sky_module_create(name);
    if (builtin->initialize) {
        sky_module_initialize_cmodule(module, builtin->initialize);
    }
    sky_interpreter_addmodule(module);
    return module;
}


sky_module_t
sky_module_import_dynamic(sky_string_t  name,
                          sky_string_t  pathname)
{
    void (*initialize)(sky_module_t module);

    char                *doc_cstring, **docstr_bytes, *init_cstring;
    ssize_t             len;
    sky_tuple_t         tuple;
    sky_module_t        module;
    sky_string_t        doc, symbol;
    sky_module_data_t   *module_data;

    module = sky_dict_get(sky_module_dynamic_modules, pathname, NULL);
    if (module) {
        return module;
    }

    SKY_ASSET_BLOCK_BEGIN {
        tuple = sky_string_rpartition(sky_string_copy(name),
                                      SKY_STRING_LITERAL("."));
        name = sky_tuple_get(tuple, 2);

        symbol = sky_string_createfromformat("skython_module_%@_doc",
                                             name);
        doc_cstring =
                (char *)sky_codec_encodetocbytes(sky_codec_ascii,
                                                 symbol,
                                                 NULL,
                                                 NULL,
                                                 0,
                                                 &len);
        doc_cstring = sky_realloc(doc_cstring, len + 1);
        doc_cstring[len] = '\0';
        sky_asset_save(doc_cstring, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        symbol = sky_string_createfromformat("skython_module_%@_initialize",
                                             name);
        init_cstring =
                (char *)sky_codec_encodetocbytes(sky_codec_ascii,
                                                 symbol,
                                                 NULL,
                                                 NULL,
                                                 0,
                                                 &len);
        init_cstring = sky_realloc(init_cstring, len + 1);
        init_cstring[len] = '\0';
        sky_asset_save(init_cstring, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        module = sky_object_allocate(sky_module_type);
        module_data = sky_module_data(module);
        sky_module_setattr(module, "__file__", pathname);

#if defined(HAVE_DLFCN_H)
        SKY_ASSET_BLOCK_BEGIN {
            int     dlopenflags = sky_interpreter_dlopenflags();

            void        *handle;
            sky_bytes_t bytes;
            const char  *encoded_path, *message;

            bytes = sky_codec_encodefilename(pathname);
            encoded_path = (const char *)sky_bytes_cstring(bytes, SKY_TRUE);

            if (!(handle = dlopen(encoded_path, dlopenflags))) {
                if (!(message = dlerror())) {
                    message = "dlopen() error";
                }
                sky_error_raise_string(sky_ImportError, message);
            }
            sky_asset_save(handle,
                           (sky_free_t)dlclose,
                           SKY_ASSET_CLEANUP_ON_ERROR);

            if (!(initialize = dlsym(handle, init_cstring))) {
                if (!(message = dlerror())) {
                    message = "dlsym() error (symbol not found?)";
                }
                sky_error_raise_string(sky_ImportError, message);
            }

            if (!(docstr_bytes = dlsym(handle, doc_cstring))) {
                doc = NULL;
            }
            else {
                doc = sky_string_createfrombytes(*docstr_bytes,
                                                 strlen(*docstr_bytes),
                                                 NULL,
                                                 NULL);
            }

            module_data->dlhandle = handle;
        } SKY_ASSET_BLOCK_END;
#else
#   error not implemented
#endif

        sky_module_setup(module, name, doc, NULL);
        sky_module_initialize_cmodule(module, initialize);
        sky_interpreter_addmodule(module);
    } SKY_ASSET_BLOCK_END;

    SKY_ASSET_BLOCK_BEGIN {
        sky_mutex_lock(sky_module_dynamic_modules_mutex, SKY_TIME_INFINITE);
        sky_asset_save(sky_module_dynamic_modules_mutex,
                       (sky_free_t)sky_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_dict_setitem(sky_module_dynamic_modules, pathname, module);
    } SKY_ASSET_BLOCK_END;
    return module;
}


sky_module_t
sky_module_import_frozen(sky_string_t name)
{
    ssize_t                 nbytes;
    const char              *cstring;
    sky_code_t              code;
    sky_dict_t              modules;
    sky_bytes_t             bytes;
    sky_module_t            module;
    sky_builtin_module_t    *builtin;

    modules = sky_interpreter_modules();
    if ((module = sky_dict_get(modules, name, NULL)) != NULL) {
        return module;
    }

    if (!(cstring = sky_string_cstring(name))) {
        return NULL;
    }
    for (builtin = sky_builtin_modules; builtin->name; ++builtin) {
        if (builtin->frozen_bytes && !strcmp(builtin->name, cstring)) {
            break;
        }
    }
    if (!builtin->name) {
        return NULL;
    }

    if ((nbytes = builtin->frozen_nbytes) < 0) {
        nbytes = -nbytes;
    }
    bytes = sky_bytes_createwithbytes(builtin->frozen_bytes, nbytes, NULL);
    code = sky_marshal_loads(bytes);

    module = sky_module_create(name);
    sky_module_initialize(module, code, SKY_STRING_LITERAL("<frozen>"), NULL);
    if (builtin->initialize) {
        sky_module_initialize_cmodule(module, builtin->initialize);
    }
    sky_interpreter_addmodule(module);

    return module;
}


sky_bool_t
sky_module_import_lock_acquire(void)
{
    return sky_mutex_lock(sky_module_import_lock, SKY_TIME_INFINITE);
}


sky_bool_t
sky_module_import_lock_held(void)
{
    return sky_mutex_islocked(sky_module_import_lock);
}


sky_bool_t
sky_module_import_lock_release(void)
{
    sky_mutex_unlock(sky_module_import_lock);
    return SKY_TRUE;
}


sky_object_t
sky_module_importer(sky_string_t name)
{
    sky_dict_t      path_importer_cache;
    sky_list_t      path_hooks;
    sky_module_t    sys;
    sky_object_t    hook, importer;

    sys = sky_module_import(SKY_STRING_LITERAL("sys"));
    if (!(path_importer_cache =
                sky_object_getattr(sys,
                                   SKY_STRING_LITERAL("path_importer_cache"),
                                   NULL)))
    {
        return NULL;
    }
    if ((importer = sky_dict_get(path_importer_cache, name, NULL)) != NULL) {
        return importer;
    }

    if (!(path_hooks =
                sky_object_getattr(sys,
                                   SKY_STRING_LITERAL("path_hooks"),
                                   NULL)))
    {
        return NULL;
    }

    /* Set this to prevent recursion. */
    sky_dict_setitem(path_importer_cache, name, sky_None);

    SKY_SEQUENCE_FOREACH(path_hooks, hook) {
        SKY_ERROR_TRY {
            importer = sky_object_call(hook, sky_tuple_pack(1, name), NULL);
        } SKY_ERROR_EXCEPT(sky_ImportError) {
            importer = NULL;
        } SKY_ERROR_TRY_END;
        if (importer) {
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    if (!importer) {
        return sky_None;
    }
    sky_dict_setitem(path_importer_cache, name, importer);
    return importer;
}


void
sky_module_init(sky_object_t self, sky_string_t name, sky_object_t doc)
{
    sky_module_setup(self, name, doc, NULL);
}


void
sky_module_initialize(sky_module_t  self,
                      sky_code_t    code,
                      sky_string_t  file,
                      sky_string_t  cached)
{
    sky_dict_t  dict;

    if (!sky_object_getattr(self, SKY_STRING_LITERAL("__builtins__"), NULL)) {
        sky_module_setattr(self, "__builtins__", sky_interpreter_builtins());
    }

    if (sky_object_isnull(file)) {
        file = sky_object_getattr(code,
                                  SKY_STRING_LITERAL("co_filename"),
                                  sky_NotSpecified);
    }
    if (sky_object_isnull(cached)) {
        cached = (sky_object_t)sky_None;
    }
    sky_module_setattr(self, "__file__", file);
    sky_module_setattr(self, "__cached__", cached);

    dict = sky_object_dict(self);
    sky_code_execute(code, dict, dict, NULL, 0, NULL);
}


sky_string_t
sky_module_repr(sky_module_t self)
{
    sky_object_t        loader;
    sky_string_t        file, name;
    sky_dict_t volatile dict;

    dict = sky_object_dict(self);
    loader = sky_dict_get(dict, SKY_STRING_LITERAL("__loader__"), NULL);

    if (loader) {
        sky_object_t    repr;

        SKY_ERROR_TRY {
            repr = sky_object_callmethod(loader,
                                         SKY_STRING_LITERAL("module_repr"),
                                         sky_tuple_pack(1, self),
                                         NULL);
        }
        SKY_ERROR_EXCEPT_ANY {
            repr = NULL;
        } SKY_ERROR_TRY_END;

        if (repr) {
            return repr;
        }
    }

    name = sky_dict_get(dict, SKY_STRING_LITERAL("__name__"), NULL);
    if (!name || !sky_object_isa(name, sky_string_type)) {
        name = SKY_STRING_LITERAL("?");
    }

    file = sky_dict_get(dict, SKY_STRING_LITERAL("__file__"), NULL);
    if (file && !sky_object_isa(file, sky_string_type)) {
        file = NULL;
    }

    if (!file) {
        if (!loader) {
            return sky_string_createfromformat("<module %#@>", name);
        }
        return sky_string_createfromformat("<module %#@ (%#@)>", name, loader);
    }
    return sky_string_createfromformat("<module %#@ from %#@>", name, file);
}


void
sky_module_setattr(sky_module_t self,
                   const char * name,
                   sky_object_t value)
{
    sky_string_t    name_object;

    name_object = sky_string_createfrombytes(name, strlen(name), NULL, NULL);
    sky_object_setattr(self, name_object, value);
}


void
sky_module_setdata(sky_module_t         self,
                   void *               data,
                   sky_module_visit_t   visit,
                   sky_free_t           destructor)
{
    sky_module_data_t   *self_data = sky_module_data(self);

    self_data->data = data;
    self_data->visit = visit;
    self_data->destructor = destructor;
}


static const char sky_module_type_doc[] =
"module(name[, doc])\n"
"\n"
"Create a module object.\n"
"The name must be a string; the optional doc argument can have any type.";


SKY_TYPE_DEFINE_SIMPLE(module,
                       "module",
                       sizeof(sky_module_data_t),
                       NULL,
                       sky_module_instance_finalize,
                       sky_module_instance_visit,
                       SKY_TYPE_FLAG_HAS_DICT | SKY_TYPE_FLAG_DICT_READONLY,
                       sky_module_type_doc);


void
sky_module_initialize_library(void)
{
    sky_type_initialize_builtin(sky_module_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_module_type,
            "__init__",
            (sky_native_code_function_t)sky_module_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
            "doc", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);

    sky_type_setmethodslots(sky_module_type,
            "__repr__", sky_module_repr,
            "__dir__", sky_module_dir,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_module_import_lock),
            sky_mutex_create(SKY_TRUE, SKY_TRUE),
            SKY_TRUE);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_module_dynamic_modules),
            sky_dict_create(),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_module_dynamic_modules_mutex),
            sky_mutex_create(SKY_FALSE, SKY_TRUE),
            SKY_TRUE);
}
