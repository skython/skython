/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_slice Slice Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SLICE_H__
#define __SKYTHON_CORE_SKY_SLICE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new slice object instance.
  * Any of the three parameters may be @c NULL or @c sky_None. They should
  * otherwise be objects that implement the __index__() special method.
  *
  * @param[in]  start   the starting value for the slice.
  * @param[in]  stop    the stopping value for the slice.
  * @param[in]  step    the stepping value for the slice.
  * @return     the new slice object instance.
  */
SKY_EXTERN sky_slice_t
sky_slice_create(sky_object_t start, sky_object_t stop, sky_object_t step);


/** Compute the usable start, stop, and step indices from a slice object.
  * The returned indices are computed assuming a range having a length of
  * @a length.
  *
  * @param[in]  slice   the slice object.
  * @param[in]  length  the length of the range to slice.
  * @param[out] start   the starting index of the computed slice (inclusive).
  * @param[out] stop    the stopping index of the computed slice (exclusive).
  * @param[out] step    the stepping value of the computed slice.
  * @return     the length of the resulting slice.
  */
SKY_EXTERN ssize_t
sky_slice_range(sky_slice_t slice,
                ssize_t     length,
                ssize_t *   start,
                ssize_t *   stop,
                ssize_t *   step);


/** Return the starting value for a slice. **/
SKY_EXTERN sky_object_t
sky_slice_start(sky_slice_t slice);

/** Return the stopping value for a slice. **/
SKY_EXTERN sky_object_t
sky_slice_stop(sky_slice_t slice);

/** Return the stepping value for a slice. **/
SKY_EXTERN sky_object_t
sky_slice_step(sky_slice_t slice);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SLICE_H__ */

/** @} **/
/** @} **/
