/** @file
  * @brief
  * Bytes builder objects are subtypes of bytes objects that are mutable.
  * They're intended to help build complex bytes objects while minimizing the
  * amount of copying that using immutable bytes objects to achieve the same
  * end would require. At this time, they are intended to be used from C code
  * only.
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_bytes Immutable Byte Array Objects
  * @{
  * @defgroup sky_bytes_builder Bytes Builder Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BYTES_BUILDER_H__
#define __SKYTHON_CORE_SKY_BYTES_BUILDER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new, empty bytes builder object.
  *
  * @return     a new bytes builder object instance that contains no data.
  */
SKY_EXTERN sky_bytes_builder_t
sky_bytes_builder_create(void);

/** Create a new, empty bytes builder object with space pre-allocated.
  *
  * @param[in]  capacity    the amount of space to pre-allocate.
  * @return     a new bytes builder object instance that contains no data.
  */
SKY_EXTERN sky_bytes_builder_t
sky_bytes_builder_createwithcapacity(ssize_t capacity);

/** Append bytes to the bytes builder object.
  * 
  * @param[in]  builder     the bytes builder object to append to.
  * @param[in]  bytes       the raw bytes to append to @a builder.
  * @param[in]  nbytes      the number of bytes to append from @a bytes.
  */
SKY_EXTERN void
sky_bytes_builder_appendbytes(sky_bytes_builder_t   builder,
                              const void *          bytes,
                              ssize_t               nbytes);

/** Append a formatted string to the end of a bytes builder.
  *
  * @param[in]  builder     the bytes builder to which the formatted data is
  *                         to be written.
  * @param[in]  format      the format string to use.
  * @param[in]  ...         additional arguments as required by the format
  *                         string.
  */
SKY_EXTERN void
sky_bytes_builder_appendformat(sky_bytes_builder_t  builder,
                               const char *         format,
                               ...);

/** Append a formatted string to the end of a bytes builder.
  *
  * @param[in]  builder     the bytes builder to which the formatted data is
  *                         to be written.
  * @param[in]  format      the format string to use.
  * @param[in]  ap          additional arguments as required by the format
  *                         string.
  */
SKY_EXTERN void
sky_bytes_builder_appendformatv(sky_bytes_builder_t builder,
                                const char *        format,
                                va_list             ap);

/** Create a copy of a bytes builder object.
  *
  * @param[in]  builder     the bytes builder to copy.
  * @return     a new bytes builder object instance that is a copy of
  *             @a builder.
  */
SKY_EXTERN sky_bytes_builder_t
sky_bytes_builder_copy(sky_bytes_builder_t builder);

/** Finalize a bytes builder, converting it into an immutable bytes object.
  * The bytes builder is converted into a bytes object, rather than creating
  * a new bytes object using the data from the bytes builder. Once the bytes
  * builder is finalized, it can never be mutated again.
  *
  * @param[in]  builder     the bytes builder object to finalize.
  * @return     a bytes object that may or may not be the same as @a builder.
  *             Always use the returned object instead of continuing to use
  *             @a builder cast to @c sky_bytes_t.
  */
SKY_EXTERN sky_bytes_t
sky_bytes_builder_finalize(sky_bytes_builder_t builder);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BYTES_BUILDER_H__ */

/** @} **/
/** @} **/
/** @} **/
