/** @file
  * @brief C-Friendly access to the io module for files.
  * There isn't actually a specific file object that is exposed through this
  * API. Instead, this API provides C-friendly access to the io module, along
  * with some extra convenience functions.
  *
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_file File Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FILE_H__
#define __SKYTHON_CORE_SKY_FILE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Close an open file.
  * No error is raised if the file is already closed.
  *
  * @param[in]  file        the file to close.
  */
SKY_EXTERN void
sky_file_close(sky_object_t file);


/** Determine whether a file is open or closed.
  *
  * @param[in]  file        the file to test.
  * @return     @c SKY_TRUE if the file is closed, or @c SKY_FALSE if it is
  *             open.
  */
SKY_EXTERN sky_bool_t
sky_file_closed(sky_object_t file);


/** Query the encoding to use for an open file descriptor.
  * This is the implementation of os.device_encoding().
  *
  * @param[in]  fileno      the file descriptor to query.
  * @return     the encoding to use for the file descriptor, or @c sky_None if
  *             the encoding is not known.
  */
SKY_EXTERN sky_string_t
sky_file_device_encoding(int fileno);


/** Query the file descriptor for an open file.
  * @c sky_ValueError is raised if the file is closed. Implementations should
  * raise @c sky_UnsupportedOperation if there is no file descriptor (e.g.,
  * @c io.BytesIO).
  *
  * @param[in]  file        the file to query.
  * @return     the file descriptor for the file. The result will never be
  *             negative (@c sky_ValueError is raised in this case).
  */
SKY_EXTERN int
sky_file_fileno(sky_object_t file);


/** Flush an open file.
  * @c sky_ValueError is raised if the file is closed.
  *
  * @param[in]  file        the file to flush.
  */
SKY_EXTERN void
sky_file_flush(sky_object_t file);


/** Determine if a file is associated with an interactive terminal.
  *
  * @param[in]  file        the file to test.
  * @return     @c SKY_TRUE if the file is associated with an interactive
  *             terminal, or @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_file_isatty(sky_object_t file);


/** Open a file by filename.
  * This is simply a wrapper to call the function open() from the @c builtins
  * module.
  *
  * @param[in]  filename    the name of the file to open.
  * @param[in]  mode        the mode to open the file in. If @c NULL, the
  *                         default is "r".
  * @param[in]  buffering   the buffering to use, which is -1 for the default,
  *                         0 for no buffering at all, 1 for line buffering, or
  *                         any value > 1 to specify the size of the buffer to
  *                         use in bytes.
  * @param[in]  encoding    the encoding to use with the file. Only makes sense
  *                         if the file is not open in binary mode. The default
  *                         is "utf-8".
  * @param[in]  errors      the error handler to use with the file's encoding.
  *                         Only makes sense if the file is not open in binary
  *                         mode. The default is "strict".
  * @param[in]  newline     the character sequence to recognize as a newline.
  *                         This should be one of "\r", "\r\n", or "\n". The
  *                         default is "\n".
  * @param[in]  closefd     whether the underlying file descriptor should be
  *                         closed when the file object is finalized.
  * @param[in]  opener      a callable object to use as the opener for the
  *                         file.
  */
SKY_EXTERN sky_object_t
sky_file_open(sky_string_t  filename,
              sky_string_t  mode,       /* default: "r" */
              ssize_t       buffering,  /* default: -1 */
              sky_string_t  encoding,   /* default: sky_None */
              sky_string_t  errors,     /* default: sky_None */
              sky_string_t  newline,    /* default: sky_None */
              sky_bool_t    closefd,    /* default: SKY_TRUE */
              sky_object_t  opener);    /* default: sky_None */


/** Read data from an open file.
  *
  * @param[in]  file        the file to read from.
  * @param[in]  n           the maximum number of bytes to read. A value of -1
  *                         is effectively the same as sky_file_readall().
  * @return     the data read from the file, typically a bytes object in binary
  *             mode, or a string object in text mode. If the read would have
  *             blocked on a file opened in non-blocking mode, the return will
  *             be @c sky_None. An empty bytes or string object indicates that
  *             end of file has been reached.
  */
SKY_EXTERN sky_object_t
sky_file_read(sky_object_t file, ssize_t n);


/** Determine if a file can be used for reading.
  *
  * @param[in]  file        the file to test.
  * @return     @c SKY_TRUE if the file can be used for reading, or
  *             @c SKY_FALSE if it is write only.
  */
SKY_EXTERN sky_bool_t
sky_file_readable(sky_object_t file);


/** Read all of the available data from a file.
  *
  * @param[in]  file        the file to read from.
  * @return     the data read from the file, typically returned as a bytes
  *             object in binary mode, or a string object in text mode. If
  *             the read would have blocked on a file in non-blocking mode,
  *             the return will be @c sky_None.
  */
SKY_EXTERN sky_object_t
sky_file_readall(sky_object_t file);


/** Read data from the file directly into a buffer.
  * The buffer to read into, @a b, must support the buffer protocol for
  * writing. The amount of data that is read from the file is determined by
  * the size of @a b.
  *
  * @param[in]  file        the file to read from.
  * @param[in]  b           the buffer object to read into, which is typically
  *                         either a @c memoryview or a @c bytearray object.
  * @return     the number of bytes read from @a file into @a b. A return of
  *             -1 means the read would have blocked on a non-blocking file. A
  *             return of 0 means that end of file has been reached.
  */
SKY_EXTERN ssize_t
sky_file_readinto(sky_object_t file, sky_object_t b);


/** Read a line from a file.
  *
  * @param[in]  file        the file to read from.
  * @param[in]  limit       a value greater than 0 indicates the maximum
  *                         number of bytes that should be read.
  * @return     the line read from @a file. If a full line is read, the line
  *             ending character will be included in the result. An empty
  *             bytes or string indicates that end of file has been reached,
  *             and @c sky_None indicates that the read would have blocked on
  *             a file opened in non-blocking mode.
  */
SKY_EXTERN sky_object_t
sky_file_readline(sky_object_t file, ssize_t limit);


/** Read as many lines as are available from a file.
  * This function simply reads as many lines as are available from a file via
  * sky_file_readline(), and returns them all in a list.
  *
  * @param[in]  file        the file to read from.
  * @param[in]  hint        the approximate number of lines that are expected
  *                         to be read. This is not a limit to the number of
  *                         lines that may be read, but is used as a hint to
  *                         initially size the list that is to be returned.
  */
SKY_EXTERN sky_list_t
sky_file_readlines(sky_object_t file, ssize_t hint);


/** Move the current file position of a file.
  * @c sky_ValueError will be raised if the file is not seekable.
  *
  * @param[in]  file        the file to seek.
  * @param[in]  pos         the position to seek to. How it is interpreted
  *                         depends on @a whence.
  * @param[in]  whence      from where to seek (or, how to interpret @a pos).
  *                         A value of 0 means to seek from the beginning of
  *                         the file (@a pos is effectively absolute). A value
  *                         of 1 means to seek from the current position, which
  *                         means @a pos can be either a positive or a negative
  *                         offset. A value of 2 means to seek from the end of
  *                         the file, which means that @a pos must be negative.
  */
SKY_EXTERN ssize_t
sky_file_seek(sky_object_t file, ssize_t pos, int whence);


/** Determine if a file is capable of random access (seeking).
  *
  * @param[in]  file        the file to test.
  * @return     @c SKY_TRUE if the file is seekable, or @c SKY_FALSE if it can
  *             only be access sequentially.
  */
SKY_EXTERN sky_bool_t
sky_file_seekable(sky_object_t file);


/** Return the current offset in a file.
  *
  * @param[in]  file        the file to query.
  * @return     the current offset in @a file.
  */
SKY_EXTERN ssize_t
sky_file_tell(sky_object_t file);


/** Truncate a file.
  *
  * @param[in]  file        the file to truncate.
  * @param[in]  pos         the offset at which to truncate the file, which is
  *                         ultimately the size of the resulting file.
  */
SKY_EXTERN void
sky_file_truncate(sky_object_t file, ssize_t pos);


/** Determine if a file can be used for writing.
  *
  * @param[in]  file        the file to query.
  * @return     @c SKY_TRUE if the file can be used for writing, or
  *             @c SKY_FALSE if the file is read-only.
  */
SKY_EXTERN sky_bool_t
sky_file_writable(sky_object_t file);


/** Write data to a file.
  *
  * @param[in]  file        the file to write to.
  * @param[in]  b           the data to write to the file, which should
  *                         typically be an object that supports the buffer
  *                         protocol, unless the file is open in text mode, in
  *                         which case it should be a string.
  * @return     the number of bytes written succesfully. The return will be -1
  *             if the file is open in non-blocking mode and the write would
  *             have blocked.
  */
SKY_EXTERN ssize_t
sky_file_write(sky_object_t file, sky_object_t b);


/** Write a sequence of lines to a file.
  * The write does not return until it is complete or an error occurs. Do not
  * use this function with non-blocking files, because it will likely go into
  * a spin loop when a write operation would block.
  *
  * @param[in]  file        the file to write to.
  * @param[in]  lines       the lines to write, which should be a sequence of
  *                         the appropriate object type for writing, the same
  *                         as would be expected for sky_file_write().
  */
SKY_EXTERN void
sky_file_writelines(sky_object_t file, sky_object_t lines);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FILE_H__ */

/** @} **/
/** @} **/
