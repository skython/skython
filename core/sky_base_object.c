#include "sky_private.h"


static void
sky_base_object_instance_finalize(sky_object_t object, void *data)
{
    sky_object_data_t   *object_data = data;
    sky_type_data_t     *type_data = sky_type_data(object_data->u.type);

    sky_object_reference_t  *ref;

    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        *(sky_dict_t *)((char *)object + type_data->dict_offset) = NULL;
    }
    while ((ref = object_data->refs) != NULL) {
        object_data->refs = ref->next;
        sky_free(ref);
    }

}


static void
sky_base_object_instance_visit(sky_object_t             object,
                               void *                   data,
                               sky_object_visit_data_t *visit_data)
{
    sky_object_data_t   *object_data = data;
    sky_type_data_t     *type_data = sky_type_data(object_data->u.type);

    sky_dict_t              dict;
    sky_object_reference_t  *ref;

    sky_object_visit(object_data->u.type, visit_data);
    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        dict = *(sky_dict_t *)((char *)object + type_data->dict_offset);
        sky_object_visit(dict, visit_data);
    }
    for (ref = object_data->refs; ref; ref = ref->next) {
        sky_object_visit(ref->arg, visit_data);
    }
}


sky_object_t
sky_base_object_class_getter(sky_object_t instance, SKY_UNUSED sky_type_t type)
{
    return sky_object_type(instance);
}


static sky_bool_t
sky_base_object_builtintypesmatch(sky_type_data_t * type_data_1,
                                  sky_type_data_t * type_data_2)
{
    ssize_t         i, j;
    sky_type_data_t *mro_type_data;

    /* No fast iteration here, because the last item always has to be skipped,
     * which complicates the iteration. Also, this particular path should be
     * so extremely rare that the optimization is really worthless.
     */
    sky_error_validate_debug(type_data_1->mro->len ==
                             type_data_1->data_offsets->len);
    sky_error_validate_debug(type_data_2->mro->len ==
                             type_data_2->data_offsets->len);
    for (i = 0; i < type_data_1->mro->len - 1; ++i) {
        if (!type_data_1->data_offsets->items[i].offset) {
            continue;
        }
        mro_type_data = sky_type_data(type_data_1->mro->items[i].type);
        if (!(mro_type_data->flags & SKY_TYPE_FLAG_BUILTIN)) {
            continue;
        }

        for (j = 0; j < type_data_2->mro->len - 1; ++j) {
            if (type_data_2->mro->items[j].type !=
                type_data_1->mro->items[i].type)
            {
                continue;
            }
            if (type_data_2->data_offsets->items[j].offset ==
                type_data_1->data_offsets->items[i].offset)
            {
                break;
            }
        }
        if (j >= type_data_2->mro->len) {
            return SKY_FALSE;
        }
    }

    return SKY_TRUE;
}


static void
sky_base_object_class_setter(
                sky_object_t    instance,
                sky_object_t    new_type,
    SKY_UNUSED  sky_type_t      old_type)
{
    sky_object_t        new_slots, old_slots;
    sky_string_t        name;
    sky_type_data_t     *new_type_data, *old_type_data;
    sky_object_data_t   *object_data;

    if (new_type == sky_object_type(instance)) {
        return;
    }
    if (!new_type) {
        sky_error_raise_string(sky_TypeError,
                               "can't delete __class__ attribute");
    }
    if (!sky_object_isa(new_type, sky_type_type)) {
        sky_error_raise_format(sky_TypeError,
                               "__class__ must be set to a class, not %#@ object",
                               sky_type_name(sky_object_type(new_type)));
    }

    if (sky_object_istagged(instance)) {
        sky_error_raise_string(sky_TypeError,
                               "can't assign __class__ for built-in types");
    }

    object_data = SKY_OBJECT_DATA(instance);
    new_type_data = sky_type_data(new_type);
    old_type_data = sky_type_data(object_data->u.type);

    if (old_type_data->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_string(sky_TypeError,
                               "can't assign __class__ for built-in types");
    }
    if (new_type_data->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_string(sky_TypeError,
                               "can't assign __class__ with built-in types");
    }
    if (old_type_data->instance_size != new_type_data->instance_size ||
        old_type_data->instance_alignment != new_type_data->instance_alignment ||
        /* If one type has __dict__, the other must as well. */
        (old_type_data->flags & SKY_TYPE_FLAG_HAS_DICT) !=
        (new_type_data->flags & SKY_TYPE_FLAG_HAS_DICT) ||
        /* If old_type has any built-in types in its MRO (not counting the base
         * object), the new_type must have matching built-in types with the
         * same data offsets. The reverse must also be true.
         */
        !sky_base_object_builtintypesmatch(old_type_data, new_type_data) ||
        !sky_base_object_builtintypesmatch(new_type_data, old_type_data))
    {
        sky_error_raise_string(sky_TypeError,
                               "incompatibile types for __class__ assignment");
    }

    /* Both old_type and new_type either have dicts or they don't. If they
     * don't, check to make sure that they have matching slots.
     */
    if (!(old_type_data->flags & SKY_TYPE_FLAG_HAS_DICT)) {
        name = SKY_STRING_LITERAL("__slots__");
        old_slots = sky_object_getattr(sky_object_type(instance),
                                       name,
                                       sky_NotSpecified);
        new_slots = sky_object_getattr(new_type,
                                       name,
                                       sky_NotSpecified);
        if (!sky_object_compare(old_slots, new_slots, SKY_COMPARE_OP_EQUAL)) {
            sky_error_raise_string(sky_TypeError,
                                   "incompatible types for __class__ assignment");
        }
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(object_data->u.type)),
                      new_type,
                      instance);
}


sky_object_t
sky_base_object_dir(sky_object_t self)
{
    sky_dict_t      dict;
    sky_object_t    type;

    dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), NULL);
    if (!dict) {
        dict = sky_dict_create();
    }
    else {
        dict = sky_dict_copy(dict);
    }

    type = sky_object_getattr(self, SKY_STRING_LITERAL("__class__"), NULL);
    if (type) {
        sky_type_merge_class_dict(type, dict);
    }

    return sky_dict_keys(dict);
}


sky_object_t
sky_base_object_eq(sky_object_t self, sky_object_t other)
{
    /* Return NotImplemented instead of False, so if two objects are compared,
     * both get a chance at the comparison. See [CPython] issue #1393.
     */
    return (self == other ? (sky_object_t)sky_True
                          : (sky_object_t)sky_NotImplemented);
}


sky_string_t
sky_base_object_format(sky_object_t self, sky_string_t format_spec)
{
    sky_string_t    string;

    string = sky_object_str(self);
    if (sky_string_len(format_spec) > 0) {
        sky_error_warn_string(
                sky_DeprecationWarning,
                "object.__format__ with a non-empty format string is deprecated");
    }
    return sky_object_format(string, format_spec);
}


sky_object_t
sky_base_object_ge(SKY_UNUSED sky_object_t self, SKY_UNUSED sky_object_t other)
{
    return sky_NotImplemented;
}


void
sky_base_object_delattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_dict_t      dict)
{
    sky_type_t      type;
    sky_object_t    descriptor;
    sky_type_data_t *type_data;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    type = sky_object_type(self);
    descriptor = sky_type_lookup(type, name);
    if (descriptor && sky_descriptor_isdata(descriptor)) {
        sky_descriptor_delete(descriptor, self);
        return;
    }

    if (!dict) {
        type_data = sky_type_data(type);
        if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
            dict = *(sky_dict_t *)((char *)self + type_data->dict_offset);
            if (!dict) {
                sky_error_raise_object(sky_AttributeError, name);
            }
        }
    }
    if (dict) {
        if (sky_dict_delete(dict, name)) {
            return;
        }
        sky_error_raise_object(sky_AttributeError, name);
    }

    if (descriptor) {
        sky_descriptor_delete(descriptor, self);
        return;
    }

    sky_error_raise_format(sky_AttributeError,
                           "%#@ object has no attribute %#@",
                           sky_type_name(type),
                           name);
}

void
sky_base_object_delattr(sky_object_t self, sky_string_t name)
{
    sky_base_object_delattrwithdict(self, name, NULL);
}


sky_object_t
sky_base_object_getattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_dict_t      dict)
{
    sky_type_t      type;
    sky_object_t    descriptor, object;
    sky_type_data_t *type_data;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    type = sky_object_type(self);
    descriptor = sky_type_lookup(type, name);
    if (descriptor && sky_descriptor_isdata(descriptor)) {
        return sky_descriptor_get(descriptor, self, type);
    }

    if (!dict) {
        type_data = sky_type_data(type);
        if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
            dict = *(sky_dict_t *)((char *)self + type_data->dict_offset);
        }
    }
    if (dict && (object = sky_dict_get(dict, name, NULL)) != NULL) {
        return object;
    }

    if (descriptor) {
        return sky_descriptor_get(descriptor, self, type);
    }

    sky_error_raise_format(sky_AttributeError,
                           "%#@ object has no attribute %#@",
                           sky_type_name(type), name);
}

sky_object_t
sky_base_object_getattribute(sky_object_t self, sky_string_t name)
{
    return sky_base_object_getattrwithdict(self, name, NULL);
}


sky_object_t
sky_base_object_gt(SKY_UNUSED sky_object_t self, SKY_UNUSED sky_object_t other)
{
    return sky_NotImplemented;
}


uintptr_t
sky_base_object_hash(sky_object_t self)
{
    return (uintptr_t)self >> (uintptr_t)3;
}


void
sky_base_object_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    /* If either args or kws is not empty, raise TypeError if this function is
     * not the object's actual implementation, but __new__ is.
     * That is, we're here due to super().__init__(), but object.__new__() is
     * the object's new implementation.
     */
    if (sky_object_bool(args) || sky_object_bool(kws)) {
        sky_type_t      cls;
        sky_object_t    base_init, base_new, cls_init, cls_new;

        base_init = SKY_TYPE_METHOD_SLOT(sky_base_object_type, INIT);
        base_new = SKY_TYPE_METHOD_SLOT(sky_base_object_type, NEW);
        cls = sky_object_type(self);
        cls_init = SKY_TYPE_METHOD_SLOT(cls, INIT);
        cls_new = SKY_TYPE_METHOD_SLOT(cls, NEW);

        if (cls_new == base_new || cls_init != base_init) {
            sky_error_raise_string(sky_TypeError,
                                   "object.__init__() takes no parameters");
        }
    }
}


sky_object_t
sky_base_object_le(SKY_UNUSED sky_object_t self, SKY_UNUSED sky_object_t other)
{
    return sky_NotImplemented;
}


sky_object_t
sky_base_object_lt(SKY_UNUSED sky_object_t self, SKY_UNUSED sky_object_t other)
{
    return sky_NotImplemented;
}


sky_object_t
sky_base_object_ne(sky_object_t self, sky_object_t other)
{
    /* By default, != returns the opposite of ==, unless the latter returns
     * NotImplemented.
     */
    return (sky_object_compare(self, other, SKY_COMPARE_OP_EQUAL) ? sky_False
                                                                  : sky_True);
}


sky_object_t
sky_base_object_new(sky_object_t cls, sky_object_t args, sky_object_t kws)
{
    /* If either args or kws is not empty, raise TypeError if this function is
     * not the object's actual implementation, but __init__ is.
     * That is, we're here due to super().__new__(), but object.__init__() is
     * the object's new implementation.
     */
    if (sky_object_bool(args) || sky_object_bool(kws)) {
        sky_object_t    base_init, base_new, cls_init, cls_new;

        base_init = SKY_TYPE_METHOD_SLOT(sky_base_object_type, INIT);
        base_new = SKY_TYPE_METHOD_SLOT(sky_base_object_type, NEW);
        cls_init = SKY_TYPE_METHOD_SLOT(cls, INIT);
        cls_new = SKY_TYPE_METHOD_SLOT(cls, NEW);

        if (cls_init == base_init || cls_new != base_new) {
            sky_error_raise_string(sky_TypeError,
                                   "object() takes no parameters");
        }
    }

    return sky_object_allocate(cls);
}


sky_object_t
sky_base_object_reduce_impl(sky_object_t self, int proto)
{
    sky_dict_t      slots;
    sky_type_t      cls;
    sky_tuple_t     args;
    sky_module_t    copyreg;
    sky_object_t    dictitems, items, listitems, names, state, value;
    sky_string_t    name;

    copyreg = sky_module_import(SKY_STRING_LITERAL("copyreg"));
    if (proto < 2) {
        return sky_object_callmethod(copyreg,
                                     SKY_STRING_LITERAL("_reduce_ex"),
                                     sky_object_build("(Oi)", self, proto),
                                     NULL);
    }

    cls = sky_object_type(self);
    args = sky_object_getnewargs(self);
    if (!(state = sky_object_getstate(self))) {
        if (!(state = sky_object_dict(self))) {
            state = sky_None;
        }

        if (!(names = sky_object_getattr(cls,
                                         SKY_STRING_LITERAL("__slotnames__"),
                                         NULL)))
        {
            names = sky_object_callmethod(copyreg,
                                          SKY_STRING_LITERAL("_slotnames"),
                                          sky_tuple_pack(1, cls),
                                          NULL);
            if (sky_object_isnull(names)) {
                names = NULL;
            }
            else if (!sky_sequence_check(names)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "copyreg._slotnames didn't return a list or None");
            }
        }
        if (names && sky_object_len(names) > 0) {
            slots = sky_dict_createwithcapacity(sky_object_len(names));
            SKY_SEQUENCE_FOREACH(names, name) {
                if ((value = sky_object_getattr(self, name, NULL)) != NULL) {
                    sky_dict_setitem(slots, name, value);
                }
            } SKY_SEQUENCE_FOREACH_END;
            if (sky_dict_len(slots)) {
                state = sky_tuple_pack(2, state, slots);
            }
        }
    }

    dictitems = listitems = sky_None;
    if (sky_object_isa(self, sky_list_type)) {
        listitems = sky_object_iter(self);
    }
    if (sky_object_isa(self, sky_dict_type)) {
        items = sky_object_callmethod(self,
                                      SKY_STRING_LITERAL("items"),
                                      NULL,
                                      NULL);
        dictitems = sky_object_iter(items);
    }

    return sky_tuple_pack(5, sky_object_getattr(copyreg,
                                                SKY_STRING_LITERAL("__newobj__"),
                                                sky_NotSpecified),
                             sky_tuple_insert(args, 0, cls),
                             state,
                             listitems,
                             dictitems);
}


sky_object_t
sky_base_object_reduce(sky_object_t self)
{
    return sky_base_object_reduce_impl(self, 0);
}


sky_object_t
sky_base_object_reduce_ex(sky_object_t self, int proto)
{
    typedef sky_object_t (*reduce_t)(sky_object_t);

    reduce_t        cfunction;
    sky_object_t    objreduce, reduce, result;

    if ((reduce = SKY_OBJECT_METHOD_SLOT(self, REDUCE)) != NULL) {
        objreduce = SKY_TYPE_METHOD_SLOT(sky_base_object_type, REDUCE);
        if (reduce != objreduce) {
            /* call reduce and return the result */
            if (sky_object_isa(reduce, sky_native_code_type)) {
                cfunction = (reduce_t)sky_native_code_function(reduce);
                SKY_OBJECT_CALL_NATIVE(result, cfunction, (self));
                return result;
            }
            else {
                reduce = sky_descriptor_get(reduce, self, sky_object_type(self));
                return sky_object_call(reduce, NULL, NULL);
            }
        }
    }

    return sky_base_object_reduce_impl(self, proto);
}


sky_string_t
sky_base_object_repr(sky_object_t self)
{
    sky_type_t  type = sky_object_type(self);

    sky_string_t    module;

    module = sky_type_module(type);
    if (sky_string_eq(module, SKY_STRING_LITERAL("builtins")) == sky_True) {
        return sky_string_createfromformat("<%@ object at %p>",
                                           sky_type_name(type),
                                           self);
    }

    return sky_string_createfromformat("<%@.%@ object at %p>",
                                       module,
                                       sky_type_qualname(type),
                                       self);
}


void
sky_base_object_setattrwithdict(sky_object_t    self,
                                sky_string_t    name,
                                sky_object_t    value,
                                sky_dict_t      dict)
{
    sky_type_t      type;
    sky_object_t    descriptor;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    type = sky_object_type(self);
    descriptor = sky_type_lookup(type, name);
    if (descriptor && sky_descriptor_isdata(descriptor)) {
        sky_descriptor_set(descriptor, self, value);
        return;
    }

    if (!dict) {
        dict = sky_object_dict(self);
    }
    if (dict) {
        sky_dict_setitem(dict, name, value);
    }
    else if (descriptor) {
        sky_descriptor_set(descriptor, self, value);
    }
    else {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ has no attribute %#@",
                               sky_type_name(type), name);
    }
}

void
sky_base_object_setattr(sky_object_t    self,
                        sky_string_t    name,
                        sky_object_t    value)
{
    sky_base_object_setattrwithdict(self, name, value, NULL);
}


sky_string_t
sky_base_object_str(sky_object_t self)
{
    return sky_object_repr(self);
}


size_t
sky_base_object_sizeof(sky_object_t self)
{
    if (sky_object_istagged(self)) {
        return 0;
    }
    return sky_memsize(self);
}


sky_object_t
sky_base_object_subclasshook(SKY_UNUSED sky_type_t     cls,
                             SKY_UNUSED sky_object_t   subclass)
{
    return sky_NotImplemented;
}


static SKY_ALIGNED(16)
sky_type_list_t sky_base_object_bases = { 0, {} };

static SKY_ALIGNED(16) union {
    struct {
        size_t      len;
        sky_type_t  items[1];
    }                                   static_list;
    sky_type_list_t                     type_list;
} sky_base_object_mro = {
    { 1, { &sky_base_object_type_struct } },
};

static SKY_ALIGNED(16) union {
    struct {
        size_t      len;
        size_t      items[1];
    }                                   static_list;
    sky_type_list_t                     type_list;
} sky_base_object_data_offsets = {
    { 1, { 0 } },
};

static const char sky_base_object_type_doc[] =
"The most base type";

SKY_ALIGNED(16)
struct sky_type_s sky_base_object_type_struct = {
    SKY_OBJECT_DATA_INITIALIZER(&sky_type_type_struct),
    {
        "object",                                   /* name                 */
        NULL,                                       /* method_slots         */
        0,                                          /* dict_offset          */
        (SKY_TYPE_FLAG_BUILTIN |                    /* flags                */
         SKY_TYPE_FLAG_STATIC),
        sizeof(sky_object_data_t),                  /* instance_size        */
        sizeof(void *),                             /* instance_alignment   */
        sizeof(sky_object_data_t),                  /* type_size            */
        sizeof(void *),                             /* instance_alignment   */
        &sky_base_object_bases,                     /* bases                */
        &(sky_base_object_mro.type_list),           /* mro                  */
        &(sky_base_object_data_offsets.type_list),  /* data_offsets         */
        NULL,                                       /* subclasses           */
        sky_type_allocate_calloc,                   /* allocate             */
        sky_free,                                   /* deallocate           */
        NULL,                                       /* initialize           */
        sky_base_object_instance_finalize,          /* finalize             */
        sky_base_object_instance_visit,             /* visit                */
        NULL,                                       /* iterate              */
        NULL,                                       /* iterate_cleanup      */
        NULL,                                       /* buffer_acquire       */
        NULL,                                       /* buffer_release       */
        (char *)sky_base_object_type_doc,           /* doc                  */
        NULL,                                       /* qualname             */
        SKY_SPINLOCK_INITIALIZER,                   /* spinlock             */
    },
    NULL,
};
sky_type_t const sky_base_object_type = &sky_base_object_type_struct;


void
sky_base_object_initialize_library(void)
{
    sky_type_setmethodslots(sky_base_object_type,
            "__new__", sky_base_object_new,
            "__init__", sky_base_object_init,
            "__repr__", sky_base_object_repr,
            "__str__", sky_base_object_str,
            "__format__", sky_base_object_format,
            "__lt__", sky_base_object_lt,
            "__le__", sky_base_object_le,
            "__eq__", sky_base_object_eq,
            "__ne__", sky_base_object_ne,
            "__gt__", sky_base_object_gt,
            "__ge__", sky_base_object_ge,
            "__hash__", sky_base_object_hash,
            "__sizeof__", sky_base_object_sizeof,
            "__getattribute__", sky_base_object_getattribute,
            "__setattr__", sky_base_object_setattr,
            "__delattr__", sky_base_object_delattr,
            "__dir__", sky_base_object_dir,
            "__subclasshook__", sky_base_object_subclasshook,
            "__reduce__", sky_base_object_reduce,
            "__reduce_ex__", sky_base_object_reduce_ex,
            NULL);

    sky_type_setattr_getset(sky_base_object_type,
                            "__class__",
                            "the object's class",
                            sky_base_object_class_getter,
                            sky_base_object_class_setter);
}
