#ifndef __SKYTHON_CORE_SKY_ERROR_PRIVATE_H__
#define __SKYTHON_CORE_SKY_ERROR_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_asset.h"
#include "sky_error.h"
#include "sky_malloc.h"


SKY_CDECLS_BEGIN


struct sky_error_asset_s {
    sky_error_asset_t *                 next;
    void *                              asset_pointer;
    sky_free_t                          asset_free;
    sky_asset_cleanup_t                 asset_cleanup;
};


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ERROR_PRIVATE_H__ */
