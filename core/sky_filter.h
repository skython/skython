/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_filter Filter Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FILTER_H__
#define __SKYTHON_CORE_SKY_FILTER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new filter object instance.
  *
  * @param[in]  function    the function to call for each iteration.
  * @param[in]  iterable    an iterable object.
  * @return     a new filter object instance.
  */
SKY_EXTERN sky_filter_t
sky_filter_create(sky_object_t function, sky_object_t iterable);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FILTER_H__ */

/** @} **/
/** @} **/
