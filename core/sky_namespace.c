#include "sky_private.h"


void
sky_namespace_init(sky_namespace_t self, sky_tuple_t args, sky_dict_t kws)
{
    if (sky_object_bool(args)) {
        sky_error_raise_string(sky_TypeError,
                               "no positional arguments expected");
    }

    sky_dict_update(sky_object_dict(self), NULL, kws);
}


sky_string_t
sky_namespace_repr(sky_namespace_t self)
{
    sky_bool_t              first;
    sky_dict_t              dict;
    sky_list_t              keys;
    sky_object_t            key, value;
    sky_string_t            joiner;
    sky_string_builder_t    builder;

    builder = sky_string_builder_create();
    sky_string_builder_append(builder, sky_type_name(sky_object_type(self)));
    sky_string_builder_appendcodepoint(builder, '(', 1);

    if (sky_object_stack_push(self)) {
        sky_string_builder_append(builder, SKY_STRING_LITERAL("..."));
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_asset_save(self,
                           sky_object_stack_pop,
                           SKY_ASSET_CLEANUP_ALWAYS);

            first = SKY_TRUE;
            dict = sky_object_dict(self);
            keys = sky_list_create(dict);
            sky_list_sort(keys, NULL, SKY_FALSE);
            joiner = SKY_STRING_LITERAL(", ");
            SKY_SEQUENCE_FOREACH(keys, key) {
                if ((value = sky_dict_get(dict, key, NULL)) != NULL &&
                    sky_object_isa(key, sky_string_type))
                {
                    if (!first) {
                        sky_string_builder_append(builder, joiner);
                    }
                    first = SKY_FALSE;
                    sky_string_builder_append(builder, key);
                    sky_string_builder_appendcodepoint(builder, '=', 1);
                    sky_string_builder_append(builder, sky_object_repr(value));
                }
            } SKY_SEQUENCE_FOREACH_END;
        } SKY_ASSET_BLOCK_END;
    }

    sky_string_builder_appendcodepoint(builder, ')', 1);
    return sky_string_builder_finalize(builder);
}


sky_namespace_t
sky_namespace_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t         i;
    sky_dict_t      dict;
    sky_object_t    namespace;

    sky_error_validate_debug(count >= 0 && !(count & 1));

    namespace = sky_object_allocate(sky_namespace_type);
    dict = sky_object_dict(namespace);

    for (i = 0; i < count; i += 2) {
        sky_dict_setitem(dict, objects[i], objects[i + 1]);
    }

    return namespace;
}


sky_namespace_t
sky_namespace_createfromdict(sky_dict_t dict)
{
    sky_object_t    namespace;

    namespace = sky_object_allocate(sky_namespace_type);
    sky_dict_update(sky_object_dict(namespace), NULL, dict);

    return namespace;
}


SKY_TYPE_DEFINE_SIMPLE(namespace,
                       "namespace",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_HAS_DICT,
                       "A simple attribute-based namespace.\n\n"
                       "namespace(**kwargs)");


void
sky_namespace_initialize_library(void)
{
    sky_type_initialize_builtin(sky_namespace_type, 0);
    sky_type_setmethodslots(sky_namespace_type,
            "__init__", sky_namespace_init,
            "__repr__", sky_namespace_repr,
            NULL);
}
