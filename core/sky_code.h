#ifndef __SKYTHON_CORE_SKY_CODE_H__
#define __SKYTHON_CORE_SKY_CODE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Constants defining behavioral attributes of code objects. **/
typedef enum sky_code_flag_e {
    SKY_CODE_FLAG_OPTIMIZED         =   0x0001,
    SKY_CODE_FLAG_NEWLOCALS         =   0x0002,
    SKY_CODE_FLAG_VARARGS           =   0x0004,
    SKY_CODE_FLAG_VARKEYWORDS       =   0x0008,
    SKY_CODE_FLAG_NESTED            =   0x0010,
    SKY_CODE_FLAG_GENERATOR         =   0x0020,
    SKY_CODE_FLAG_NOFREE            =   0x0040,

    SKY_CODE_FLAG_FUTURE_DIVISION           =   0x02000,
    SKY_CODE_FLAG_FUTURE_ABSOLUTE_IMPORT    =   0x04000,
    SKY_CODE_FLAG_FUTURE_WITH_STATEMENT     =   0x08000,
    SKY_CODE_FLAG_FUTURE_PRINT_FUNCTION     =   0x10000,
    SKY_CODE_FLAG_FUTURE_UNICODE_LITERALS   =   0x20000,
    SKY_CODE_FLAG_FUTURE_BARRY_AS_BDFL      =   0x40000,

    SKY_CODE_FLAG_FUTURE_MASK   =   (SKY_CODE_FLAG_FUTURE_DIVISION |
                                     SKY_CODE_FLAG_FUTURE_ABSOLUTE_IMPORT |
                                     SKY_CODE_FLAG_FUTURE_WITH_STATEMENT |
                                     SKY_CODE_FLAG_FUTURE_PRINT_FUNCTION |
                                     SKY_CODE_FLAG_FUTURE_UNICODE_LITERALS |
                                     SKY_CODE_FLAG_FUTURE_BARRY_AS_BDFL),
    SKY_CODE_FLAG_OBSOLETE_MASK =   SKY_CODE_FLAG_NESTED
} sky_code_flag_t;


/** Create a new code object instance.
  * 
  * @param[in]  argcount        the number of positional arguments, including
  *                             those that have default values, but not *args.
  * @param[in]  kwonlyargcount  the number of keyword arguments, including
  *                             those that have default values, but not **kws.
  * @param[in]  nlocals         the number of local variables used in the code,
  *                             including arguments.
  * @param[in]  stacksize       the maximum number of stack slots required to
  *                             execute the code.
  * @param[in]  flags           behavioral attributes. See @c sky_code_flag_t.
  * @param[in]  codestring      executable bytecode.
  * @param[in]  constants       constant values used in the code.
  * @param[in]  names           all names used in the code, not including any
  *                             names that also appear in @a varnames,
  *                             @a freevars, or @a cellvars.
  * @param[in]  varnames        local variable names, including parameters and
  *                             cell variables.
  * @param[in]  filename        source filename.
  * @param[in]  name            name (function name, class name, etc).
  * @param[in]  firstlineno     first source line number for the code.
  * @param[in]  lnotab          source line number table.
  * @param[in]  freevars        names of all free variables used.
  * @param[in]  cellvars        names of local variables that are cell
  *                             variables.
  * @return
  */
SKY_EXTERN sky_code_t
sky_code_create(ssize_t         argcount,
                ssize_t         kwonlyargcount,
                ssize_t         nlocals,
                ssize_t         stacksize,
                unsigned int    flags,
                sky_bytes_t     codestring,
                sky_tuple_t     constants,
                sky_tuple_t     names,
                sky_tuple_t     varnames,
                sky_string_t    filename,
                sky_string_t    name,
                int             firstlineno,
                sky_bytes_t     lnotab,
                sky_tuple_t     freevars,
                sky_tuple_t     cellvars);


/** Disassemble a code object.
  *
  * @param[in]  code        the code object to disassemble.
  * @return     a string object instance containing the disassembly.
  */
SKY_EXTERN sky_string_t
sky_code_disassemble(sky_code_t code);


/** Dump a code object to stderr.
  * This is a convenience wrapper around sky_code_disassemble() that will write
  * the output from sky_code_disassemble() directly to @c stderr.
  *
  * @param[in]  code        the code object to dump.
  */
SKY_EXTERN void
sky_code_dump(sky_code_t code);


/** Execute a code object.
  * 
  * @param[in]  code        the code object to execute.
  * @param[in]  globals     the globals to use for execution.
  * @param[in]  locals      the locals to use for execution.
  * @param[in]  closure     the closure to use for free variables.
  * @param[in]  argc        the number of values present in @a argv.
  * @param[in]  argv        values to be used as arguments, to be applied in
  *                         the order: positional, *args, keyword-only, **kws.
  * @return     the value returned by the executed code, which shall never be
  *             @c NULL.
  */
SKY_EXTERN sky_object_t
sky_code_execute(sky_code_t     code,
                 sky_dict_t     globals,
                 sky_dict_t     locals,
                 sky_tuple_t    closure,
                 ssize_t        argc,
                 sky_object_t * argv);


/** Return the source line number for a code address.
  *
  * @param[in]  code        the code object.
  * @param[in]  address     the address (code offset) for which the source line
  *                         number is to be returned.
  * @return     the line number that corresponds to @a address.
  */
SKY_EXTERN int
sky_code_lineno(sky_code_t code, ssize_t address);


/** Validate a code object for correctness.
  * An appropriate exception will be raised in the event that an error in the
  * correctness of @a code is found. In most cases, this will be a
  * @c sky_ValueError.
  *
  * @param[in]  code        the code object to validate.
  */
SKY_EXTERN void
sky_code_validate(sky_code_t code);


/** Operation codes.
  * All opcodes are a single byte.
  */
typedef enum sky_code_opcode_e {
    SKY_CODE_OPCODE_NOP                     =   0x00,

    SKY_CODE_OPCODE_PUSH_CONST              =   0x01,
    SKY_CODE_OPCODE_PUSH_LOCAL              =   0x02,
    SKY_CODE_OPCODE_PUSH_GLOBAL             =   0x03,
    SKY_CODE_OPCODE_PUSH_NAME               =   0x04,
    SKY_CODE_OPCODE_PUSH_FREE               =   0x05,
    SKY_CODE_OPCODE_PUSH_CELL               =   0x06,
    SKY_CODE_OPCODE_PUSH_ATTRIBUTE          =   0x07,
    SKY_CODE_OPCODE_PUSH_ITEM               =   0x08,
    SKY_CODE_OPCODE_PUSH_ITERATOR           =   0x09,
    SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT      =   0x0A,
    SKY_CODE_OPCODE_PUSH_TOP                =   0x0B,
    SKY_CODE_OPCODE_PUSH_CLOSURE_CELL       =   0x0C,
    SKY_CODE_OPCODE_PUSH_CLOSURE_FREE       =   0x0D,
    /* unassigned                           =   0x0E,   */
    /* unassigned                           =   0x0F,   */

    SKY_CODE_OPCODE_POP                     =   0x10,
    SKY_CODE_OPCODE_POP_LOCAL               =   0x11,
    SKY_CODE_OPCODE_POP_GLOBAL              =   0x12,
    SKY_CODE_OPCODE_POP_NAME                =   0x13,
    SKY_CODE_OPCODE_POP_FREE                =   0x14,
    SKY_CODE_OPCODE_POP_CELL                =   0x15,
    SKY_CODE_OPCODE_POP_ATTRIBUTE           =   0x16,
    SKY_CODE_OPCODE_POP_ITEM                =   0x17,

    /* unassigned                           =   0x18,   */
    SKY_CODE_OPCODE_DELETE_LOCAL            =   0x19,
    SKY_CODE_OPCODE_DELETE_GLOBAL           =   0x1A,
    SKY_CODE_OPCODE_DELETE_NAME             =   0x1B,
    SKY_CODE_OPCODE_DELETE_FREE             =   0x1C,
    SKY_CODE_OPCODE_DELETE_CELL             =   0x1D,
    SKY_CODE_OPCODE_DELETE_ATTRIBUTE        =   0x1E,
    SKY_CODE_OPCODE_DELETE_ITEM             =   0x1F,

    SKY_CODE_OPCODE_BINARY_ADD              =   0x20,
    SKY_CODE_OPCODE_BINARY_SUB              =   0x21,
    SKY_CODE_OPCODE_BINARY_MUL              =   0x22,
    SKY_CODE_OPCODE_BINARY_DIV              =   0x23,
    SKY_CODE_OPCODE_BINARY_MOD              =   0x24,
    SKY_CODE_OPCODE_BINARY_POW              =   0x25,
    SKY_CODE_OPCODE_BINARY_LSHIFT           =   0x26,
    SKY_CODE_OPCODE_BINARY_RSHIFT           =   0x27,
    SKY_CODE_OPCODE_BINARY_OR               =   0x28,
    SKY_CODE_OPCODE_BINARY_XOR              =   0x29,
    SKY_CODE_OPCODE_BINARY_AND              =   0x2A,
    SKY_CODE_OPCODE_BINARY_FLOORDIV         =   0x2B,
    /* unassigned                           =   0x2C,   */
    /* unassigned                           =   0x2D,   */
    /* unassigned                           =   0x2E,   */
    /* unassigned                           =   0x2F,   */

    SKY_CODE_OPCODE_INPLACE_ADD             =   0x30,
    SKY_CODE_OPCODE_INPLACE_SUB             =   0x31,
    SKY_CODE_OPCODE_INPLACE_MUL             =   0x32,
    SKY_CODE_OPCODE_INPLACE_DIV             =   0x33,
    SKY_CODE_OPCODE_INPLACE_MOD             =   0x34,
    SKY_CODE_OPCODE_INPLACE_POW             =   0x35,
    SKY_CODE_OPCODE_INPLACE_LSHIFT          =   0x36,
    SKY_CODE_OPCODE_INPLACE_RSHIFT          =   0x37,
    SKY_CODE_OPCODE_INPLACE_OR              =   0x38,
    SKY_CODE_OPCODE_INPLACE_XOR             =   0x39,
    SKY_CODE_OPCODE_INPLACE_AND             =   0x3A,
    SKY_CODE_OPCODE_INPLACE_FLOORDIV        =   0x3B,
    /* unassigned                           =   0x3C,   */
    /* unassigned                           =   0x3D,   */
    /* unassigned                           =   0x3E,   */
    /* unassigned                           =   0x3F,   */

    SKY_CODE_OPCODE_UNARY_NEGATIVE          =   0x40,
    SKY_CODE_OPCODE_UNARY_POSITIVE          =   0x41,
    SKY_CODE_OPCODE_UNARY_INVERT            =   0x42,
    SKY_CODE_OPCODE_UNARY_NOT               =   0x43,
    /* unassigned                           =   0x44,   */
    /* unassigned                           =   0x45,   */
    SKY_CODE_OPCODE_DISPLAY                 =   0x46,
    SKY_CODE_OPCODE_COMPARE_EXCEPTION       =   0x47,
    SKY_CODE_OPCODE_COMPARE                 =   0x48,
    SKY_CODE_OPCODE_UNPACK_SEQUENCE         =   0x49,
    SKY_CODE_OPCODE_UNPACK_STARRED          =   0x4A,
    SKY_CODE_OPCODE_LIST_APPEND             =   0x4B,
    SKY_CODE_OPCODE_SET_ADD                 =   0x4C,
    SKY_CODE_OPCODE_IMPORT_MODULE           =   0x4D,
    SKY_CODE_OPCODE_IMPORT_STAR             =   0x4E,
    SKY_CODE_OPCODE_IMPORT_NAME             =   0x4F,

    SKY_CODE_OPCODE_BUILD_CLASS             =   0x50,
    SKY_CODE_OPCODE_BUILD_DICT              =   0x51,
    SKY_CODE_OPCODE_BUILD_FUNCTION          =   0x52,
    /* unassigned                           =   0x53,   */
    SKY_CODE_OPCODE_BUILD_LIST              =   0x54,
    SKY_CODE_OPCODE_BUILD_SET               =   0x55,
    SKY_CODE_OPCODE_BUILD_SLICE             =   0x56,
    SKY_CODE_OPCODE_BUILD_TUPLE             =   0x57,
    /* unassigned                           =   0x58,   */
    /* unassigned                           =   0x59,   */
    /* unassigned                           =   0x5A,   */
    /* unassigned                           =   0x5B,   */
    /* unassigned                           =   0x5C,   */
    /* unassigned                           =   0x5D,   */
    SKY_CODE_OPCODE_ROTATE_TWO              =   0x5E,
    SKY_CODE_OPCODE_ROTATE_THREE            =   0x5F,

    SKY_CODE_OPCODE_CALL                    =   0x60,
    SKY_CODE_OPCODE_CALL_ARGS               =   0x61,
    SKY_CODE_OPCODE_CALL_KWS                =   0x62,
    SKY_CODE_OPCODE_CALL_ARGS_KWS           =   0x63,
    SKY_CODE_OPCODE_RETURN                  =   0x64,
    SKY_CODE_OPCODE_YIELD                   =   0x65,
    SKY_CODE_OPCODE_YIELD_FROM              =   0x66,
    SKY_CODE_OPCODE_RAISE                   =   0x67,
    SKY_CODE_OPCODE_JUMP                    =   0x68,
    SKY_CODE_OPCODE_JUMP_IF_FALSE           =   0x69,
    SKY_CODE_OPCODE_JUMP_IF_TRUE            =   0x6A,
    /* unassigned                           =   0x6B,   */
    /* unassigned                           =   0x6C,   */
    /* unassigned                           =   0x6D,   */
    /* unassigned                           =   0x6E,   */
    /* unassigned                           =   0x6F,   */

    SKY_CODE_OPCODE_WITH_ENTER              =   0x70,
    SKY_CODE_OPCODE_WITH_EXIT               =   0x71,
    SKY_CODE_OPCODE_TRY_EXCEPT              =   0x72,
    SKY_CODE_OPCODE_TRY_FINALLY             =   0x73,
    SKY_CODE_OPCODE_TRY_EXIT                =   0x74,
    SKY_CODE_OPCODE_EXCEPT_ENTER            =   0x75,
    SKY_CODE_OPCODE_EXCEPT_EXIT             =   0x76,
    SKY_CODE_OPCODE_FINALLY_ENTER           =   0x77,
    SKY_CODE_OPCODE_FINALLY_EXIT            =   0x78,
    /* unassigned                           =   0x79,   */
    /* unassigned                           =   0x7A,   */
    /* unassigned                           =   0x7B,   */
    /* unassigned                           =   0x7C,   */
    /* unassigned                           =   0x7D,   */
    /* unassigned                           =   0x7E,   */
    /* unassigned                           =   0x7F,   */
} sky_code_opcode_t;


SKY_EXTERN_INLINE int
sky_code_opcode_isconditional_jump(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT == opcode ||
            SKY_CODE_OPCODE_JUMP_IF_FALSE == opcode ||
            SKY_CODE_OPCODE_JUMP_IF_TRUE == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_isunconditional_jump(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_JUMP == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_isjump(sky_code_opcode_t opcode)
{
    return sky_code_opcode_isconditional_jump(opcode) ||
           sky_code_opcode_isunconditional_jump(opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_isexit(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_RAISE == opcode ||
            SKY_CODE_OPCODE_RETURN == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_isguard_enter(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_WITH_ENTER == opcode ||
            SKY_CODE_OPCODE_TRY_EXCEPT == opcode ||
            SKY_CODE_OPCODE_TRY_FINALLY == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_isguard_exit(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_WITH_EXIT == opcode ||
            SKY_CODE_OPCODE_TRY_EXIT == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_ishandler_enter(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_EXCEPT_ENTER == opcode ||
            SKY_CODE_OPCODE_FINALLY_ENTER == opcode);
}

SKY_EXTERN_INLINE int
sky_code_opcode_ishandler_exit(sky_code_opcode_t opcode)
{
    return (SKY_CODE_OPCODE_EXCEPT_EXIT == opcode ||
            SKY_CODE_OPCODE_FINALLY_EXIT == opcode);
}


/** Flags used with @c SKY_CODE_OPCODE_BUILD_CLASS **/
typedef enum sky_code_build_class_flag_e {
    SKY_CODE_BUILD_CLASS_FLAG_ARGS      =   0x1,
    SKY_CODE_BUILD_CLASS_FLAG_KWS       =   0x2,
    SKY_CODE_BUILD_CLASS_FLAG_CLOSURE   =   0x4,
} sky_code_build_class_flag_t;


/** Flags used with @c SKY_CODE_OPCODE_BUILD_FUNCTION **/
typedef enum sky_code_build_function_flag_e {
    SKY_CODE_BUILD_FUNCTION_FLAG_ANNOTATIONS    =   0x1,
    SKY_CODE_BUILD_FUNCTION_FLAG_CLOSURE        =   0x2,
} sky_code_build_function_flag_t;


/** Defining characteristics of an opcode. **/
typedef struct sky_code_op_s {
    const char *                        name;   /**< name of the opcode     */
    int                                 argc;   /**< # encoded arguments    */
    int                                 popc;   /**< # stack values popped  */
    int                                 pushc;  /**< # stack values pushed  */
} sky_code_op_t;


SKY_EXTERN const sky_code_op_t sky_code_opcodes[256];


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CODE_H__ */
