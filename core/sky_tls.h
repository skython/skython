/** @file
  * @brief Thread local storage
  * @defgroup threading Threading Support
  * @{
  * @defgroup sky_tls Thread Local Storage
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_TLS_H__
#define __SKYTHON_CORE_SKY_TLS_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** A thread local storage key. **/
typedef int32_t sky_tlskey_t;


/** Allocate a new thread local storage key.
  * A tls key must be allocated before sky_tls_get() or sky_tls_set() may be
  * used. Each tls key that is allocated has a per-thread data pointer and a
  * cleanup function associated with it. When a thread that has set a non-NULL
  * pointer for a tls key exits, the cleanup function will be called with the
  * pointer stored for that key. The order in which cleanup functions are
  * called is undefined. That is, cleanup functions are not necessarily called
  * in an order related to the order in which tls keys are allocated.
  *
  * @note   There is a limit of 255 tls keys per process.
  *
  * @param[in]  cleanup a pointer to the function to be called to clean up any
  *                     non-NULL pointer stored for the tls key when a thread
  *                     exits.
  *
  * @return     the allocated tls key, or 0 if a new tls key could not be
  *             allocated.
  */
SKY_EXTERN sky_tlskey_t
sky_tls_alloc(sky_free_t cleanup);

/** Deallocate a thread local storage key.
  * When a tls key is deallocated, the cleanup function registered when it was
  * allocated is not called for any threads that are still using the key. It
  * is the responsibility of the application to do any necessary clean up on
  * its own.
  *
  * @param[in]  tlskey  the tls key to deallocate.
  */
SKY_EXTERN void
sky_tls_free(sky_tlskey_t tlskey);


/** Get the pointer stored for a thread local storage key for the calling thread.
  *
  * @param[in]  tlskey  the tls key for which the data pointer for the calling
  *                     thread is to be returned.
  *
  * @return     the data pointer stored for the calling thread for the specified
  *             tls key. The return may be @c NULL if no data pointer has been
  *             stored by the calling thread for the specified tls key, or if
  *             the specified tls key is not valid.
  */
SKY_EXTERN void *
sky_tls_get(sky_tlskey_t tlskey);

/** Set the pointer for a thread local storage key for the calling thread.
  * If a data pointer is already stored, the cleanup function will not be called
  * for the old pointer.
  *
  * @param[in]  tlskey  the tls key for which the data pointer for the calling
  *                     thread is to be stored.
  * @param[in]  pointer the data pointer to store for the specified tls key.
  *                     May be @c NULL.
  *
  * @return     @c SKY_TRUE if the pointer was stored successfully, or
  *             @c SKY_FALSE if the specified tls key is not valid.
  */
SKY_EXTERN sky_bool_t
sky_tls_set(sky_tlskey_t tlskey, void *pointer);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TLS_H__ */

/** @} **/
/** @} **/
