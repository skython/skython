/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_traceback Tracebacks
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_TRACEBACK_H__
#define __SKYTHON_CORE_SKY_TRACEBACK_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new traceback object instance.
  *
  * @param[in]  next_traceback  the traceback to be chained with the new
  *                             traceback.
  * @param[in]  frame           the current frame of execution for the
  *                             traceback.
  * @return     the new traceback object instance.
  */
SKY_EXTERN sky_traceback_t
sky_traceback_create(sky_traceback_t next_traceback, sky_frame_t frame);


/** Dump the Python traceback for the calling thread to a file.
  *
  * This traceback dump is designed to work without allocating any memory or
  * touching the Python stack itself. No source code is included in the output
  * as it normally would be, and all output is ASCII (using backslashescape to
  * handle non-ASCII characters in strings).
  *
  * @param[in]  fd      the file descriptor to write to.
  */
SKY_EXTERN void
sky_traceback_dump(int fd);


/** Dump the Python traceback for each thread to a file.
  *
  * For each thread that has a Python call stack, that stack is dumped to the
  * specified file descriptor. The traceback dump is designed to work without
  * allocating any memory or touching the Python stack itself. This is somewhat
  * dangerous if the threads are currently running, because frame objects are
  * normally considered thread-local, and this function treats them as global.
  * There are no synchronization safeguards.
  *
  * @param[in]  fd      the file descriptor to write to.
  */
SKY_EXTERN void
sky_traceback_dump_threads(int fd);


/** Filter out importlib frames from a traceback.
  *
  * @param[in]  traceback       the traceback to be filtered.
  * @param[in]  exception       the exception type causing the traceback.
  * @return     the new traceback with importlib frames removed.
  */
SKY_EXTERN sky_traceback_t
sky_traceback_filter_importlib(sky_traceback_t traceback, sky_type_t exception);


/** Print a traceback out to a file-like object.
  *
  * @param[in]  self        the traceback to print.
  * @param[in]  file        the file-like object to print to.
  */
SKY_EXTERN void
sky_traceback_print(sky_traceback_t self, sky_object_t file);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TRACEBACK_H__ */

/** @} **/
/** @} **/
