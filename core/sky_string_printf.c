#include "sky_private.h"
#include "sky_string_private.h"
#include "sky_integer_private.h"


static void
sky_string_printf_number(sky_string_data_t *            new_string_data,
                         char *                         bytes,
                         ssize_t                        nbytes,
                         const char *                   prefix,
                         ssize_t                        prefix_len,
                         sky_unicode_char_t             sign,
                         sky_format_specification_t *   spec)
{
    ssize_t             left, len, right;
    sky_unicode_char_t  fill;

    len = nbytes + prefix_len;
    if (sign) {
        ++len;
    }
    else if (sky_format_sign(spec) != SKY_FORMAT_FLAG_SIGN_MINUS) {
        ++len;
        if (sky_format_sign(spec) == SKY_FORMAT_FLAG_SIGN_PLUS) {
            sign = '+';
        }
        else {
            sign = ' ';
        }
    }

    left = right = 0;
    if ((spec->flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH) && len < spec->width) {
        switch (sky_format_alignment(spec)) {
            case SKY_FORMAT_FLAG_ALIGN_LEFT:
            case SKY_FORMAT_FLAG_ALIGN_PADDED:
                break;
            case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                left = spec->width - len;
                break;
            case SKY_FORMAT_FLAG_ALIGN_CENTER:
                left = (spec->width - len) / 2;
                break;
        }
        right = spec->width - (len + left);
    }

    if (sky_format_alignment(spec) == SKY_FORMAT_FLAG_ALIGN_PADDED) {
        if (sign) {
            sky_string_data_append_codepoint(new_string_data, sign, 1);
        }
        if (left > 0) {
            fill = ((spec->flags & SKY_FORMAT_FLAG_ZERO_PAD) ? '0' : ' ');
            sky_string_data_append_codepoint(new_string_data, fill, left);
        }
    }
    else {
        if (left > 0) {
            fill = ((spec->flags & SKY_FORMAT_FLAG_ZERO_PAD) ? '0' : ' ');
            sky_string_data_append_codepoint(new_string_data, fill, left);
        }
        if (sign) {
            sky_string_data_append_codepoint(new_string_data, sign, 1);
        }
    }
    if (prefix && prefix_len) {
        sky_string_data_append(new_string_data, prefix, prefix_len, 1);
    }
    sky_string_data_append(new_string_data, bytes, nbytes, 1);
    if (right > 0) {
        sky_string_data_append_codepoint(new_string_data, spec->fill, right);
    }
}


static void
sky_string_printf_format_float(sky_string_data_t *          new_string_data,
                               sky_object_t                 value,
                               sky_format_specification_t * spec)
{
    char                *bytes;
    unsigned int        flags;
    sky_unicode_char_t  sign;

    sign = '\0';
    flags = 0;
    if (spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
        flags |= SKY_UTIL_DTOA_ALTERNATE_FORM;
    }
    if (!(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
        spec->precision = 6;
    }

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_util_dtoa(sky_float_value(value),
                              spec->type,
                              spec->precision,
                              flags);
        sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if ('-' == *bytes) {
            ++bytes;
            sign = '-';
        }

        sky_string_printf_number(new_string_data,
                                 bytes,
                                 strlen(bytes),
                                 NULL,
                                 0,
                                 sign,
                                 spec);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_string_printf_format_integer(sky_string_data_t *            new_string_data,
                                 sky_object_t                   value,
                                 sky_format_specification_t *   spec)
{
    int                 base;
    char                *bytes, static_bytes[32];
    size_t              nbytes;
    ssize_t             prefix_len;
    const char          *prefix;
    sky_integer_data_t  *integer_data, tagged_integer_data;
    sky_unicode_char_t  sign;

    sign = '\0';
    prefix = NULL;
    prefix_len = 0;

    switch (spec->type) {
        case 'd':
            base = 10;
            break;
        case 'o':
            base = 8;
            if (spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                prefix = "0o";
                prefix_len = 2;
            }
            break;
        case 'x':
            base = 16;
            if (spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                prefix = "0x";
                prefix_len = 2;
            }
            break;
        case 'X':
            base = -16;
            if (spec->flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
                prefix = "0X";
                prefix_len = 2;
            }
            break;
    }

    SKY_ASSET_BLOCK_BEGIN {
        integer_data = sky_integer_data(value, &tagged_integer_data);
        nbytes = mpz_sizeinbase(integer_data->mpz, base) + 2;
        if (nbytes > sizeof(static_bytes)) {
            bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
        }
        else {
            bytes = static_bytes;
        }
        bytes = mpz_get_str(bytes, base, integer_data->mpz);

        if (sky_integer_data_isnegative(integer_data)) {
            ++bytes;
            sign = '-';
        }

        sky_string_printf_number(new_string_data,
                                 bytes,
                                 strlen(bytes),
                                 prefix,
                                 prefix_len,
                                 sign,
                                 spec);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_string_printf_format_string(sky_string_data_t *         new_string_data,
                                sky_object_t                value,
                                sky_format_specification_t *spec)
{
    ssize_t             left, len, right;
    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data(value, &tagged_data);

    /* If neither width nor precision were specified, alignment has no effect,
     * so just output value as-is.
     */
    if (!(spec->flags & (SKY_FORMAT_FLAG_SPECIFIED_WIDTH |
                         SKY_FORMAT_FLAG_SPECIFIED_PRECISION)))
    {
        sky_string_data_combine(new_string_data, string_data);
        return;
    }

    /* If precision was specified, limit the number of characters from the
     * string to the specified precision. If width was not specified and the
     * precision is greater than the length of the string, just output value
     * as-is.
     */
    len = string_data->len;
    if (spec->flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) {
        if (len > spec->precision) {
            len = spec->precision;
        }
        else if (!(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH)) {
            sky_string_data_combine(new_string_data, string_data);
            return;
        }
    }

    left = right = 0;
    if ((spec->flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH) && spec->width > len) {
        switch (sky_format_alignment(spec)) {
            case SKY_FORMAT_FLAG_ALIGN_LEFT:
                break;
            case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                left = spec->width - len;
                break;
            case SKY_FORMAT_FLAG_ALIGN_CENTER:
                left = (spec->width - len) / 2;
                break;
            case SKY_FORMAT_FLAG_ALIGN_PADDED:
                sky_error_fatal("unreachable");
        }
        right = spec->width - (left + len);
    }
    if (!left && !right && len >= string_data->len) {
        sky_string_data_combine(new_string_data, string_data);
        return;
    }

    if (left > 0) {
        sky_string_data_append_codepoint(new_string_data, spec->fill, left);
    }
    if (len >= string_data->len) {
        sky_string_data_combine(new_string_data, string_data);
    }
    else {
        sky_string_data_append_slice(new_string_data, string_data, len, 0, 1);
    }
    if (right > 0) {
        sky_string_data_append_codepoint(new_string_data, spec->fill, right);
    }
}


#define SKY_STRING_WIDTH 1
#include "templates/sky_string_printf_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 2
#include "templates/sky_string_printf_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 4
#include "templates/sky_string_printf_t.c"
#undef SKY_STRING_WIDTH


sky_string_t
sky_string_printf(sky_string_t format, sky_object_t values)
{
    sky_string_data_t   *format_data, tagged_data;

    format_data = sky_string_data(format, &tagged_data);
    if (format_data->highest_codepoint < 0x100) {
        return sky_string_printf_latin1(format, format_data, values);
    }
    if (format_data->highest_codepoint < 0x10000) {
        return sky_string_printf_ucs2(format, format_data, values);
    }
    return sky_string_printf_ucs4(format, format_data, values);
}
