/** @file
  * @brief Thread objects
  * @defgroup threading Threading Support
  * @{
  * @defgroup sky_thread Thread Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_THREAD_H__
#define __SKYTHON_CORE_SKY_THREAD_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** A function pointer defining a thread's entry point. **/
typedef void *
(*sky_thread_function_t)(void *arg);


/** Create a new thread object.
  * Creating a new thread object creates a new thread within the process.
  * When the new thread is created, it begins execution at the specified
  * start routine. The new thread will be started with all signals blocked.
  *
  * @param[in]  start_routine   the entrypoint for the thread.
  * @param[in]  arg             an arbitrary pointer argument to pass to the
  *                             thread's start routine.
  * @return     the new thread object.
  */
SKY_EXTERN sky_thread_t
sky_thread_create(sky_thread_function_t start_routine, void *arg);


/** Detach a thread.
  *
  * @param[in]  thread          the thread to detach.
  */
SKY_EXTERN void
sky_thread_detach(sky_thread_t thread);


/** Terminate the current thread.
  *
  * @param[in]  exit_value      the exit value to be returned for the thread
  *                             when sky_thread_join() is called.
  */
SKY_EXTERN SKY_NORETURN void
sky_thread_exit(void *exit_value);


/** Determine whether the calling thread is the main thread.
  * This is roughly equivalent to sky_thread_self() == sky_thread_main(). The
  * difference is that sky_thread_self() is not used, which avoids the possible
  * allocation of a thread object, making this function signal safe.
  *
  * @return     @c SKY_TRUE if the calling thread is the main thread, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_thread_ismain(void);


/** Wait for a thread to exit.
  * This function will not return until the specified thread exits. If the
  * thread has already exited, it may only be joined once. Joining the thread
  * again has undefined consequences.
  *
  * @param[in]  thread          the thread to wait for.
  * @return     the @c exit_value returned from the thread's start routine, or
  *             the value passed to sky_thread_exit() if the thread terminates
  *             that way.
  */
SKY_EXTERN void *
sky_thread_join(sky_thread_t thread);


/** Return the main thread object.
  * The main thread is the thread that called sky_library_initialize(). After a
  * fork, in the child process the main thread will be the thread that executed
  * the fork (which is the only remaining thread on most operating systems).
  *
  * @return     the thread object representing the main thread.
  */
SKY_EXTERN sky_thread_t
sky_thread_main(void);


/** Return the Skython object for the current thread.
  *
  * @return     the thread object for the current thread.
  */
SKY_EXTERN sky_thread_t
sky_thread_self(void);


/** Return the integer identifier for the current thread.
  * Skython assigns every thread created an integer identifier that is unique
  * at the time the identifier is assigned. In other words, the same identifier
  * may be re-used when a thread terminates and another is created. The
  * identifiers are normally assigned consecutively starting at 1. Holes are
  * re-used before the highest thread identifier is incremented.
  *
  * @return     the thread identifier assigned to the calling thread.
  */
SKY_EXTERN uint32_t
sky_thread_id(void);


/** Return the highest identifier assigned to a thread.
  *
  * @return     the highest identifier that's been assigned.
  */
SKY_EXTERN uint32_t
sky_thread_highestid(void);


/** Set the stack size to be used when creating new threads.
  *
  * @param[in]  size    the size to use, which must be positive. A value of 0
  *                     means to use the operating system default.
  * @return     the stack size that was previously set.
  */
SKY_EXTERN ssize_t
sky_thread_setstacksize(ssize_t size);


/** Return the stack size to be used when creating new threads.
  *
  * @return     the stack size to be used, which may be 0 to use the operating
  *             system default.
  */
SKY_EXTERN ssize_t
sky_thread_stacksize(void);


/** A structure containing data for a thread's wait state. **/
typedef struct sky_thread_wait_s sky_thread_wait_t;


/** Return the calling thread's wait state information. **/
SKY_EXTERN sky_thread_wait_t *
sky_thread_wait(void);


/** The signature of a function to be called to test whether a wait condition
  * has been met.
  *
  * @param[in]  arg     the arbitrary object argument supplied with the condition
  *                     function pointer.
  * @return     @c SKY_TRUE if the condition has been met (thus, terminating the
  *             wait), or @c SKY_FALSE if the condition has not been met.
  */
typedef sky_bool_t
        (*sky_thread_wait_condition_t)(sky_object_t arg);


/** Wait for a condition to be met.
  * Waits for a condition to be met as determined by calling @a condition with
  * @a condition_arg as a parameter. When the condition has been met, the wait
  * will be terminated. If a timeout is specified, the wait will be terminated
  * when the timeout elapses if the condition has not yet been met. While
  * waiting, other tasks such as garbage collection may take place. A wait
  * state does not guarantee that the waiting thread will not use any CPU time.
  *
  * @param[in]  wait            thread wait state. May be @c NULL to use the
  *                             calling thread's wait state.
  * @param[in]  condition       the function to call to test whether the wait
  *                             should be terminated or not. May be @c NULL, in
  *                             which case the wait will continue until the
  *                             timeout elapses.
  * @param[in]  condition_arg   an arbitrary object to be passed to @a condition
  *                             when it is called. May be @c NULL.
  * @param[in]  timeout         the timeout relative to the current time (in
  *                             units of nanoseconds). May be
  *                             @c SKY_TIME_INFINITE to wait without a timeout.
  * @return     -1 if there was an error (@c errno will be set), 0 if the wait
  *             timed out, or > 0 if the condition was met.
  */
SKY_EXTERN int
sky_thread_wait_condition(sky_thread_wait_t *           wait,
                          sky_thread_wait_condition_t   condition,
                          sky_object_t                  condition_arg,
                          sky_time_t                    timeout);


/** Wait for file descriptors to be ready for processing.
  * Waits for file descriptors to be ready for processing. This is essentially
  * a wrapper around the select() system call, but it will perform other
  * processing while it waits, if needed (e.g., if the thread needs to check in
  * for garbage collection, it will). As with sky_thread_timedwait(), the wait
  * state does not guarantee that the waiting thread will not use any CPU time.
  *
  * @param[in]  wait        thread wait state. May be @c NULL to use the calling
  *                         thread's wait state.
  * @param[in]  nfds        the highest fd in @a readfds, @a writefds, and
  *                         @a errorfds plus one. Usually @c FD_SETSIZE.
  * @param[in]  readfds     the set of file descriptors to wait for reading.
  * @param[in]  writefds    the set of file descriptors to wait for writing.
  * @param[in]  errorfds    the set of file descriptors to wait for errors.
  * @param[in]  timeout     the timeout relative to the current time (in units
  *                         of nanoseconds). May be @c SKY_TIME_INFINITE to
  *                         wait without a timeout.
  * @return     -1 if there was an error (@c errno will be set), 0 if the wait
  *             timed out, or the number of descriptors that are ready.
  */
SKY_EXTERN int
sky_thread_wait_select(sky_thread_wait_t *  wait,
                       int                  nfds,
                       fd_set *             readfds,
                       fd_set *             writefds,
                       fd_set *             errorfds,
                       sky_time_t           timeout);


/** Signal a waiting thread.
  *
  * @param[in]  wait    the thread wait state to signal. Must not be @c NULL.
  */
SKY_EXTERN void
sky_thread_wait_signal(sky_thread_wait_t *wait);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_THREAD_H__ */

/** @} **/
/** @} **/
