#include "sky_private.h"


typedef struct sky_reversed_data_s {
    sky_object_t                        object;
    ssize_t                             next_index;
} sky_reversed_data_t;

SKY_EXTERN_INLINE sky_reversed_data_t *
sky_reversed_data(sky_object_t object)
{
    if (sky_reversed_type == sky_object_type(object)) {
        return (sky_reversed_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_reversed_type);
}

struct sky_reversed_s {
    sky_object_data_t                   object_data;
    sky_reversed_data_t                 reversed_data;
};


static void
sky_reversed_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_reversed_data_t *reversed_data = data;

    sky_object_visit(reversed_data->object, visit_data);
}


sky_object_t
sky_reversed_iter(sky_object_t self)
{
    return self;
}


ssize_t
sky_reversed_len(sky_object_t self)
{
    sky_reversed_data_t *reversed_data = sky_reversed_data(self);

    if (!reversed_data->object) {
        return 0;
    }
    return sky_object_len(reversed_data->object) - reversed_data->next_index;
}


sky_object_t
sky_reversed_new(sky_object_t cls, sky_object_t object)
{
    sky_object_t        self;
    sky_reversed_data_t *reversed_data;

    if (SKY_OBJECT_METHOD_SLOT(object, REVERSED)) {
        return sky_object_reversed(object);
    }

    self = sky_object_allocate(cls);
    reversed_data = sky_reversed_data(self);
    sky_object_gc_set(&(reversed_data->object), object, self);
    reversed_data->next_index = sky_object_len(reversed_data->object) - 1;

    return self;
}


sky_object_t
sky_reversed_next(sky_object_t self)
{
    sky_reversed_data_t *reversed_data = sky_reversed_data(self);

    sky_object_t    object;

    if (!reversed_data->object) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }
    if (reversed_data->next_index < 0) {
        reversed_data->object = NULL;
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    object = sky_object_getitem(reversed_data->object,
                                sky_integer_create(reversed_data->next_index));
    --reversed_data->next_index;

    return object;
}


sky_object_t
sky_reversed_reduce(sky_object_t self)
{
    sky_reversed_data_t *reversed_data = sky_reversed_data(self);

    if (!reversed_data->object) {
        return sky_object_build("(O(()))", sky_object_type(self));
    }
    return sky_object_build("(O(O)iz)",
                            sky_object_type(self),
                            reversed_data->object,
                            reversed_data->next_index);
}


void
sky_reversed_setstate(sky_object_t self, sky_object_t state)
{
    sky_reversed_data_t *reversed_data = sky_reversed_data(self);

    ssize_t len;

    reversed_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
    if (reversed_data->next_index < -1) {
        reversed_data->next_index = -1;
    }
    len = sky_object_len(reversed_data->object);
    if (reversed_data->next_index > len - 1) {
        reversed_data->next_index = len - 1;
    }
}


static const char sky_reversed_type_doc[] =
"reversed(sequence) -> reverse iterator over values of the sequence\n"
"\n"
"Return a reverse iterator";


SKY_TYPE_DEFINE_SIMPLE(reversed,
                       "reversed",
                       sizeof(sky_reversed_data_t),
                       NULL,
                       NULL,
                       sky_reversed_instance_visit,
                       0,
                       sky_reversed_type_doc);


void
sky_reversed_initialize_library(void)
{
    sky_type_initialize_builtin(sky_reversed_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_reversed_type,
            "__new__",
            (sky_native_code_function_t)sky_reversed_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "sequence", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_reversed_type,
            "__len__", sky_reversed_len,
            "__iter__", sky_reversed_iter,
            "__next__", sky_reversed_next,
            "__reduce__", sky_reversed_reduce,
            "__setstate__", sky_reversed_setstate,
            NULL);
}
