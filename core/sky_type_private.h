#ifndef __SKYTHON_CORE_SKY_TYPE_PRIVATE_H__
#define __SKYTHON_CORE_SKY_TYPE_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_error.h"
#include "sky_object_private.h"
#include "sky_type.h"


SKY_CDECLS_BEGIN


typedef enum sky_type_flag_e {
    /* Set this if the type is built-in (i.e., written in C). */
    SKY_TYPE_FLAG_BUILTIN           =   0x01,

    /* Set this if the type has a __dict__ attribute. */
    SKY_TYPE_FLAG_HAS_DICT          =   0x02,

    /* Set this if the type's __dict__ attribute is read-only. */
    SKY_TYPE_FLAG_DICT_READONLY     =   0x04,

    /* Set this if the type may not be used as a base type. */
    SKY_TYPE_FLAG_FINAL             =   0x08,

    /* This gets set if __abstractmethods__ gets set. */
    SKY_TYPE_FLAG_ABSTRACT          =   0x10,

    /* This will be set if SKY_TYPE_DEFINE() is used to define the type. */
    SKY_TYPE_FLAG_STATIC            =   0x20,
} sky_type_flag_t;


typedef enum sky_type_method_slot_e {
    SKY_TYPE_METHOD_SLOT_NEW            =   0,
    SKY_TYPE_METHOD_SLOT_INIT           =   1,
    SKY_TYPE_METHOD_SLOT_DEL            =   2,
    SKY_TYPE_METHOD_SLOT_REPR           =   3,
    SKY_TYPE_METHOD_SLOT_STR            =   4,
    SKY_TYPE_METHOD_SLOT_BYTES          =   5,
    SKY_TYPE_METHOD_SLOT_FORMAT         =   6,
    SKY_TYPE_METHOD_SLOT_LT             =   7,
    SKY_TYPE_METHOD_SLOT_LE             =   8,
    SKY_TYPE_METHOD_SLOT_EQ             =   9,
    SKY_TYPE_METHOD_SLOT_NE             =   10,
    SKY_TYPE_METHOD_SLOT_GT             =   11,
    SKY_TYPE_METHOD_SLOT_GE             =   12,
    SKY_TYPE_METHOD_SLOT_HASH           =   13,
    SKY_TYPE_METHOD_SLOT_BOOL           =   14,
    SKY_TYPE_METHOD_SLOT_SIZEOF         =   15,
    SKY_TYPE_METHOD_SLOT_GETATTR        =   16,
    SKY_TYPE_METHOD_SLOT_GETATTRIBUTE   =   17,
    SKY_TYPE_METHOD_SLOT_SETATTR        =   18,
    SKY_TYPE_METHOD_SLOT_DELATTR        =   19,
    SKY_TYPE_METHOD_SLOT_DIR            =   20,
    SKY_TYPE_METHOD_SLOT_GET            =   21,
    SKY_TYPE_METHOD_SLOT_SET            =   22,
    SKY_TYPE_METHOD_SLOT_DELETE         =   23,
    SKY_TYPE_METHOD_SLOT_CALL           =   24,
    SKY_TYPE_METHOD_SLOT_INSTANCECHECK  =   25,
    SKY_TYPE_METHOD_SLOT_SUBCLASSCHECK  =   26,
    SKY_TYPE_METHOD_SLOT_SUBCLASSHOOK   =   27,
    SKY_TYPE_METHOD_SLOT_ENTER          =   28,
    SKY_TYPE_METHOD_SLOT_EXIT           =   29,

    SKY_TYPE_METHOD_SLOT_LEN            =   30,
    SKY_TYPE_METHOD_SLOT_LENGTH_HINT    =   31,
    SKY_TYPE_METHOD_SLOT_GETITEM        =   32,
    SKY_TYPE_METHOD_SLOT_SETITEM        =   33,
    SKY_TYPE_METHOD_SLOT_DELITEM        =   34,
    SKY_TYPE_METHOD_SLOT_ITER           =   35,
    SKY_TYPE_METHOD_SLOT_NEXT           =   36,
    SKY_TYPE_METHOD_SLOT_REVERSED       =   37,
    SKY_TYPE_METHOD_SLOT_CONTAINS       =   38,

    SKY_TYPE_METHOD_SLOT_ADD            =   39,
    SKY_TYPE_METHOD_SLOT_SUB            =   40,
    SKY_TYPE_METHOD_SLOT_MUL            =   41,
    SKY_TYPE_METHOD_SLOT_TRUEDIV        =   42,
    SKY_TYPE_METHOD_SLOT_FLOORDIV       =   43,
    SKY_TYPE_METHOD_SLOT_MOD            =   44,
    SKY_TYPE_METHOD_SLOT_DIVMOD         =   45,
    SKY_TYPE_METHOD_SLOT_POW            =   46,
    SKY_TYPE_METHOD_SLOT_LSHIFT         =   47,
    SKY_TYPE_METHOD_SLOT_RSHIFT         =   48,
    SKY_TYPE_METHOD_SLOT_AND            =   49,
    SKY_TYPE_METHOD_SLOT_XOR            =   50,
    SKY_TYPE_METHOD_SLOT_OR             =   51,

    SKY_TYPE_METHOD_SLOT_RADD           =   52,
    SKY_TYPE_METHOD_SLOT_RSUB           =   53,
    SKY_TYPE_METHOD_SLOT_RMUL           =   54,
    SKY_TYPE_METHOD_SLOT_RTRUEDIV       =   55,
    SKY_TYPE_METHOD_SLOT_RFLOORDIV      =   56,
    SKY_TYPE_METHOD_SLOT_RMOD           =   57,
    SKY_TYPE_METHOD_SLOT_RDIVMOD        =   58,
    SKY_TYPE_METHOD_SLOT_RPOW           =   59,
    SKY_TYPE_METHOD_SLOT_RLSHIFT        =   60,
    SKY_TYPE_METHOD_SLOT_RRSHIFT        =   61,
    SKY_TYPE_METHOD_SLOT_RAND           =   62,
    SKY_TYPE_METHOD_SLOT_RXOR           =   63,
    SKY_TYPE_METHOD_SLOT_ROR            =   64,

    SKY_TYPE_METHOD_SLOT_IADD           =   65,
    SKY_TYPE_METHOD_SLOT_ISUB           =   66,
    SKY_TYPE_METHOD_SLOT_IMUL           =   67,
    SKY_TYPE_METHOD_SLOT_ITRUEDIV       =   68,
    SKY_TYPE_METHOD_SLOT_IFLOORDIV      =   69,
    SKY_TYPE_METHOD_SLOT_IMOD           =   70,
    SKY_TYPE_METHOD_SLOT_IPOW           =   71,
    SKY_TYPE_METHOD_SLOT_ILSHIFT        =   72,
    SKY_TYPE_METHOD_SLOT_IRSHIFT        =   73,
    SKY_TYPE_METHOD_SLOT_IAND           =   74,
    SKY_TYPE_METHOD_SLOT_IXOR           =   75,
    SKY_TYPE_METHOD_SLOT_IOR            =   76,

    SKY_TYPE_METHOD_SLOT_NEG            =   77,
    SKY_TYPE_METHOD_SLOT_POS            =   78,
    SKY_TYPE_METHOD_SLOT_ABS            =   79,
    SKY_TYPE_METHOD_SLOT_INVERT         =   80,
    SKY_TYPE_METHOD_SLOT_COMPLEX        =   81,
    SKY_TYPE_METHOD_SLOT_INT            =   82,
    SKY_TYPE_METHOD_SLOT_FLOAT          =   83,
    SKY_TYPE_METHOD_SLOT_ROUND          =   84,
    SKY_TYPE_METHOD_SLOT_INDEX          =   85,

    SKY_TYPE_METHOD_SLOT_REDUCE         =   86,
    SKY_TYPE_METHOD_SLOT_REDUCE_EX      =   87,
    SKY_TYPE_METHOD_SLOT_GETNEWARGS     =   88,
    SKY_TYPE_METHOD_SLOT_GETSTATE       =   89,
    SKY_TYPE_METHOD_SLOT_SETSTATE       =   90,

    SKY_TYPE_METHOD_SLOT_COUNT          =   91
} sky_type_method_slot_t;


typedef struct sky_type_subclass_list_s sky_type_subclass_list_t;
struct sky_type_subclass_list_s {
    sky_type_subclass_list_t *          next;
    ssize_t                             len;
    sky_type_t                          types[0];
};


typedef union sky_type_list_item_s {
    sky_type_t                          type;
    size_t                              offset;
} sky_type_list_item_t;

typedef struct sky_type_list_s {
    ssize_t                             len;
    sky_type_list_item_t                items[0];
} sky_type_list_t;


typedef struct sky_type_data_s {
    char *                              name;
    sky_object_t *                      method_slots;
    size_t                              dict_offset;
    uint32_t                            flags;

    size_t                              instance_size;
    size_t                              instance_alignment;
    size_t                              type_size;
    size_t                              type_alignment;

    sky_type_list_t *                   bases;
    sky_type_list_t *                   mro;
    sky_type_list_t *                   data_offsets;
    sky_type_subclass_list_t *          subclasses;

    sky_memalign_t                      allocate;
    sky_free_t                          deallocate;
    sky_type_initialize_t               initialize;
    sky_type_finalize_t                 finalize;
    sky_object_visit_t                  visit;
    sky_sequence_iterate_t              iterate;
    sky_sequence_iterate_cleanup_t      iterate_cleanup;
    sky_buffer_acquire_t                buffer_acquire;
    sky_buffer_release_t                buffer_release;

    char *                              doc;
    sky_string_t                        qualname;

    sky_spinlock_t                      spinlock;
} sky_type_data_t;


static inline sky_type_data_t *
sky_type_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_type_type) {
        return (sky_type_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_type_type);
}


/* To be used for static construction of built-in types. */
struct sky_type_s {
    sky_object_data_t                   object_data;
    sky_type_data_t                     type_data;
    sky_dict_t                          dict;
};

extern struct sky_type_s sky_type_type_struct;

#define SKY_TYPE_DEFINE(cname, sname, size,                     \
                        initialize, finalize, visit,            \
                        iterate, iterate_cleanup,               \
                        buffer_acquire, buffer_release,         \
                        flags, doc)                             \
        SKY_ALIGNED(16)                                         \
        struct sky_type_s sky_##cname##_type_struct = {         \
            SKY_OBJECT_DATA_INITIALIZER(&sky_type_type_struct), \
            {                                                   \
                sname,                                          \
                NULL,                                           \
                0,                                              \
                ((flags) | SKY_TYPE_FLAG_BUILTIN |              \
                           SKY_TYPE_FLAG_STATIC),               \
                sizeof(sky_object_data_t) + size,               \
                sizeof(void *),                                 \
                size,                                           \
                sizeof(void *),                                 \
                NULL,                                           \
                NULL,                                           \
                NULL,                                           \
                NULL,                                           \
                sky_type_allocate_calloc,                       \
                sky_free,                                       \
                initialize,                                     \
                finalize,                                       \
                visit,                                          \
                iterate,                                        \
                iterate_cleanup,                                \
                buffer_acquire,                                 \
                buffer_release,                                 \
                (char *)(doc),                                  \
                NULL,                                           \
                SKY_SPINLOCK_INITIALIZER,                       \
            },                                                  \
            NULL,                                               \
        };                                                      \
        sky_type_t const sky_##cname##_type =                   \
                         &sky_##cname##_type_struct

#define SKY_TYPE_DEFINE_ITERABLE(cname, sname, size,            \
                                 initialize, finalize, visit,   \
                                 iterate, iterate_cleanup,      \
                                 flags, doc)                    \
        SKY_TYPE_DEFINE(cname, sname, size,                     \
                        initialize, finalize, visit,            \
                        iterate, iterate_cleanup,               \
                        NULL, NULL,                             \
                        flags, doc)

#define SKY_TYPE_DEFINE_SIMPLE(cname, sname, size,              \
                               initialize, finalize, visit,     \
                               flags, doc)                      \
        SKY_TYPE_DEFINE(cname, sname, size,                     \
                        initialize, finalize, visit,            \
                        NULL, NULL, NULL, NULL,                 \
                        flags, doc)


#define SKY_TYPE_METHOD_SLOT(_o, _name) \
        sky_type_method_slot(_o, SKY_TYPE_METHOD_SLOT_##_name)


/* Default object instance allocation functions, normally chosen by default
 * during type object initialization. The calloc version uses sky_calloc(),
 * and is chosen if alignment is <= 16. The memalign version uses sky_memalign()
 * and memset(), and is chosen in all other instances.
 */
extern void *sky_type_allocate_calloc(size_t alignment, size_t size);
extern void *sky_type_allocate_memalign(size_t alignment, size_t size);

/* Exposed for sky_base_object.c :: sky_base_object_dir() */
extern void sky_type_merge_class_dict(sky_type_t type, sky_dict_t dict);

/* These functions are used during early object initialization. They should
 * never be used by anything outside of sky_object.c's library initialization.
 */
extern void
sky_type_initialize_static(sky_type_t type, ssize_t nbases, ...);

extern void
sky_type_complete_static(sky_type_t type);

/* Perform initialization of a statically defined type. */
extern void
sky_type_initialize_builtin(sky_type_t type, ssize_t nbases, ...);

/* Return the object stored for a method slot. */
SKY_EXTERN sky_object_t
sky_type_method_slot(sky_type_t type, sky_type_method_slot_t slot);

extern void sky_type_initialize_library(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TYPE_PRIVATE_H__ */
