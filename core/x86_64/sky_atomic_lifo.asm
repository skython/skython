.code


sky_atomic_lifo_init PROC EXPORT
    mov                 qword ptr [rcx], 0
    mov                 qword ptr [rcx + 8], 0
    mov                 [rcx + 16], rdx
    ret
sky_atomic_lifo_init ENDP


sky_atomic_lifo_push PROC EXPORT
    push                rbx
    mov                 r8, rcx
    mov                 r9, rdx
    mov                 rax, [r8]
    mov                 rdx, [r8 + 8]
    mov                 rbx, r9
    add                 r9, [r8 + 16]
retry:
    mov                 [r9], rax
    mov                 rcx, rdx
    inc                 rcx
    lock cmpxchg16b     [r8]
    jnz                 retry
    pop                 rbx
    ret
sky_atomic_lifo_push ENDP


sky_atomic_lifo_pop PROC EXPORT
    push                rbx
    mov                 r8, rcx
    mov                 rax, [r8]
    mov                 rdx, [r8 + 8]
retry:
    test                rax, rax
    jz                  exit
    mov                 r9, rax
    add                 r9, [r8 + 16]
    mov                 rbx, [r9]
    mov                 rcx, rdx
    inc                 rcx
    lock cmpxchg16b     [r8]
    jnz                 retry
    mov                 r9, rax
    add                 r9, [r8 + 16]
    mov                 qword ptr [r9], 0
exit:
    pop                 rbx
    ret
sky_atomic_lifo_pop ENDP


END
