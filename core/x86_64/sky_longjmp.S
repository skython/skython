#include "../sky_asm.h"


.text
.align 2, 0x90


.globl SKY_ASM_MANGLE(_UI_longjmp_cont)
SKY_ASM_TYPE(_UI_longjmp_cont, @function)
SKY_ASM_MANGLE(_UI_longjmp_cont):
    /*; IP saved in RAX */
    pushq   %rax                        /*; push target IP as return address */
    movq    %rdx, %rax                  /*; set up return-value */
    ret
SKY_ASM_SIZE(_UI_longjmp_cont)


.globl SKY_ASM_MANGLE(sky_setjmp)
SKY_ASM_TYPE(sky_setjmp, @function)
SKY_ASM_MANGLE(sky_setjmp):
    movq    (%rsp), %rax
    movq    %rax, 0(%rdi)       /*; save the return address */
    movq    %rsp, %rax
    addq    $8, %rax            /*; account for the return address */
    movq    %rax, 8(%rdi)       /*; save the frame pointer  */
    movq    $0, %rax            /*; return 0 */
    ret
SKY_ASM_SIZE(sky_setjmp)
