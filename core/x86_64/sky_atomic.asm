.code


sky_atomic_add32 PROC EXPORT
    mov                 eax, ecx
    lock xadd           [rdx], ecx
    add                 eax, ecx
    ret
sky_atomic_add32 ENDP

sky_atomic_add32_old PROC EXPORT
    lock xadd           [rdx], ecx
    mov                 eax, ecx
    ret
sky_atomic_add32_old ENDP

sky_atomic_decrement32 PROC EXPORT
    mov                 eax, -1
    lock xadd           [rcx], eax
    dec                 eax
    ret
sky_atomic_decrement32 ENDP

sky_atomic_decrement32_old PROC EXPORT
    mov                 eax, -1
    lock xadd           [rcx], eax
    ret
sky_atomic_decrement32_old ENDP

sky_atomic_increment32 PROC EXPORT
    mov                 eax, 1
    lock xadd           [rcx], eax
    inc                 eax
    ret
sky_atomic_increment32 ENDP

sky_atomic_increment32_old PROC EXPORT
    mov                 eax, 1
    lock xadd           [rcx], eax
    ret
sky_atomic_increment32_old ENDP


sky_atomic_add64 PROC EXPORT
    mov                 rax, rcx
    lock xadd           [rdx], rcx
    add                 rax, rcx
    ret
sky_atomic_add64 ENDP

sky_atomic_add64_old PROC EXPORT
    lock xadd           [rdx], rcx
    mov                 rax, rcx
    ret
sky_atomic_add64_old ENDP

sky_atomic_decrement64 PROC EXPORT
    mov                 rax, -1
    lock xadd           [rcx], rax
    dec                 rax
    ret
sky_atomic_decrement64 ENDP

sky_atomic_decrement64_old PROC EXPORT
    mov                 rax, -1
    lock xadd           [rcx], rax
    ret
sky_atomic_decrement64_old ENDP

sky_atomic_increment64 PROC EXPORT
    mov                 rax, 1
    lock xadd           [rcx], rax
    inc                 rax
    ret
sky_atomic_increment64 ENDP

sky_atomic_increment64_old PROC EXPORT
    mov                 rax, 1
    lock xadd           [rcx], rax
    ret
sky_atomic_increment64_old ENDP


sky_atomic_cas PROC EXPORT
    mov                 rax, rcx
    lock cmpxchg        [r8], rdx
    sete                al
    movzx               rax, al
    ret
sky_atomic_cas ENDP

sky_atomic_cas32 PROC EXPORT
    mov                 eax, ecx
    lock cmpxchg        [r8], edx
    sete                al
    movzx               rax, al
    ret
sky_atomic_cas32 ENDP

sky_atomic_cas64 PROC EXPORT
    mov                 rax, rcx
    lock cmpxchg        [r8], rdx
    sete                al
    movzx               rax, al
    ret
sky_atomic_cas64 ENDP

sky_atomic_cas128 PROC EXPORT
    push                rbx
    mov                 r9, rcx
    mov                 rbx, [rdx]
    mov                 rcx, [rdx + 8]
    mov                 rax, [r9]
    mov                 rdx, [r9 + 8]
    lock cmpxchg16b     [r8]
    sete                al
    movzx               rax, al
    pop                 rbx
    ret
sky_atomic_cas128 ENDP


sky_atomic_exchange PROC EXPORT
    mov                 rax, rcx
    lock xchg           rax, [rdx]
    ret
sky_atomic_exchange ENDP


sky_atomic_exchange32 PROC EXPORT
    mov                 eax, ecx
    lock xchg           eax, [rdx]
    ret
sky_atomic_exchange32 ENDP


sky_atomic_exchange64 PROC EXPORT
    mov                 rax, rcx
    lock xchg           rax, [rdx]
    ret
sky_atomic_exchange64 ENDP


sky_atomic_and32 PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    and                 edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_and32 ENDP

sky_atomic_and32_old PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    and                 edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    ret
sky_atomic_and32_old ENDP

sky_atomic_or32 PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    or                  edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_or32 ENDP

sky_atomic_or32_old PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    or                  edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    ret
sky_atomic_or32_old ENDP

sky_atomic_xor32 PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    xor                 edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    mov                 eax, edx
    ret
sky_atomic_xor32 ENDP

sky_atomic_xor32_old PROC EXPORT
    mov                 r8, rdx
    mov                 eax, [r8]
retry:
    mov                 edx, eax
    xor                 edx, ecx
    lock cmpxchg        [r8], edx
    jnz                 retry
    ret
sky_atomic_xor32_old ENDP


sky_atomic_and64 PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    and                 rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    mov                 rax, rdx
    ret
sky_atomic_and64 ENDP

sky_atomic_and64_old PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    and                 rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    ret
sky_atomic_and64_old ENDP

sky_atomic_or64 PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    or                  rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    mov                 rax, rdx
    ret
sky_atomic_or64 ENDP

sky_atomic_or64_old PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    or                  rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    ret
sky_atomic_or64_old ENDP

sky_atomic_xor64 PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    xor                 rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    mov                 rax, rdx
    ret
sky_atomic_xor64 ENDP

sky_atomic_xor64_old PROC EXPORT
    mov                 r8, rdx
    mov                 rax, [r8]
retry:
    mov                 rdx, rax
    xor                 rdx, rcx
    lock cmpxchg        [r8], rdx
    jnz                 retry
    ret
sky_atomic_xor64_old ENDP


sky_atomic_testandclear PROC EXPORT
    btr                 [rdx], ecx
    setc                al
    movzx               rax, al
    ret
sky_atomic_testandclear ENDP

sky_atomic_testandset PROC EXPORT
    bts                 [rdx], ecx
    setc                al
    movzx               rax, al
    ret
sky_atomic_testandset ENDP



sky_atomic_lfence PROC EXPORT
    lfence
    ret
sky_atomic_lfence ENDP


sky_atomic_mfence PROC EXPORT
    mfence
    ret
sky_atomic_mfence ENDP


sky_atomic_sfence PROC EXPORT
    sfence
    ret
sky_atomic_sfence ENDP


sky_atomic_pause PROC EXPORT
    pause
    ret
sky_atomic_pause ENDP


END
