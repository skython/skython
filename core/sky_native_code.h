/** @file
  * @brief
  * @defgroup sky_native_code Native Code
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NATIVE_CODE_H__
#define __SKYTHON_CORE_SKY_NATIVE_CODE_H__ 1


#include "sky_base.h"
#include "sky_data_type.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** This is a stub type for a function pointer.
  * While this function pointer type is declared to return nothing and take no
  * arguments, the function pointer as called will be called as defined when
  * creating the code object.
  */
typedef void (*sky_native_code_function_t)(void);


/** Create a new native code object instance.
  * A native code object instance is simply a wrapper around a function that is
  * written in C. Internally, @c libffi is used to actually call the function.
  * A native code object instance is callable; however, it is not typically
  * called directly. Instead, a function object instance will normally set up
  * and execute the call.
  *
  * @param[in]  function    the function that is to be called when the native
  *                         code object instance is called.
  * @param[in]  return_type the type of the function's return value.
  * @param[in]  parameters  a parameter list, used to obtain type information
  *                         for the arguments passed when called.
  * @param[in]  ...         if @a return_type requires an extra object, the
  *                         extra is specified at the end of the argument list.
  * @return     the new native code object instance.
  */
SKY_EXTERN sky_native_code_t
sky_native_code_create(sky_native_code_function_t   function,
                       sky_data_type_t              return_type,
                       sky_parameter_list_t         parameters,
                       ...);


/** Execute a native code object directly.
  * Performs a direct call of a native code object using the supplied values.
  * This is the interface that should be used to call a native code object
  * directly from C. No processing of the values is done; they are passed to
  * the function to be called directly via ffi_call().
  */
SKY_EXTERN sky_object_t
sky_native_code_execute(sky_native_code_t   code,
                        void **             avalues,
                        sky_dict_t          globals,
                        sky_object_t        locals);


/** Return the function pointer wrapped by a native code object.
  *
  * @param[in]  code    the native code object instance to query.
  * @return     the function pointer for the C function that the native code
  *             object wraps.
  */
SKY_EXTERN sky_native_code_function_t
sky_native_code_function(sky_native_code_t code);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NATIVE_CODE_H__ */

/** @} **/
