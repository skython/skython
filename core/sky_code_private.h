#ifndef __SKYTHON_CORE_SKY_CODE_PRIVATE_H__
#define __SKYTHON_CORE_SKY_CODE_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object_private.h"
#include "sky_code.h"


SKY_CDECLS_BEGIN


typedef struct sky_code_data_s {
    ssize_t                             co_argcount;
    ssize_t                             co_kwonlyargcount;
    ssize_t                             co_nlocals;
    ssize_t                             co_stacksize;
    unsigned int                        co_flags;

    int                                 co_firstlineno;
    sky_bytes_t                         co_lnotab;

    sky_bytes_t                         co_code;

    sky_tuple_t                         co_consts;
    sky_tuple_t                         co_names;
    sky_tuple_t                         co_varnames;
    sky_tuple_t                         co_freevars;
    sky_tuple_t                         co_cellvars;

    sky_string_t                        co_filename;
    sky_string_t                        co_name;

    /* This is a mapping by index of co_cellvars into co_varnames. It's created
     * when the code object is created so that it's only ever done once instead
     * of every time the code is executed.
     */
    ssize_t *                           cellvar_names;
} sky_code_data_t;

SKY_EXTERN_INLINE sky_code_data_t *
sky_code_data(sky_object_t object)
{
    if (sky_code_type == sky_object_type(object)) {
        return (sky_code_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_code_type);
}


struct sky_code_s {
    sky_object_data_t                   object_data;
    sky_code_data_t                     code_data;
};


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CODE_PRIVATE_H__ */
