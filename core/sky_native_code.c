#include "sky_private.h"
#include "sky_parameter_list_private.h"

#if defined(HAVE_DLFCN_H)
#   include <dlfcn.h>
#endif
#include <ffi.h>


typedef struct sky_native_code_data_s {
    ffi_cif                             cif;
    sky_native_code_function_t          function;
    sky_data_type_t                     return_type;
    sky_object_t                        return_type_extra;
    ffi_type *                          ffi_rtype;
    ffi_type **                         ffi_atypes;
    unsigned int                        natypes;
} sky_native_code_data_t;

SKY_EXTERN_INLINE sky_native_code_data_t *
sky_native_code_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_native_code_type) {
        return (sky_native_code_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_native_code_type);
}

struct sky_native_code_s {
    sky_object_data_t                   object_data;
    sky_native_code_data_t              native_code_data;
};


static void
sky_native_code_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_native_code_data_t  *code_data = data;

    sky_free(code_data->ffi_atypes);
    code_data->ffi_atypes = NULL;
}


static void
sky_native_code_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_native_code_data_t  *self_data = data;

    sky_object_visit(self_data->return_type_extra, visit_data);
}


static sky_once_t   sky_native_code_once = SKY_ONCE_INITIALIZER;
static ffi_type *   sky_native_code_ffi_types[256];
static size_t       sky_native_code_max_width;


static void
sky_native_code_once_runner(SKY_UNUSED void *unused)
{
    int         i;
    ffi_type    *type;

#define determine_signed_ffi_type(_t)                       \
        switch (sizeof(_t)) {                               \
            case 1: type = &ffi_type_sint8; break;          \
            case 2: type = &ffi_type_sint16; break;         \
            case 4: type = &ffi_type_sint32; break;         \
            case 8: type = &ffi_type_sint64; break;         \
            default: sky_error_fatal("not implemented");    \
        }

#define determine_unsigned_ffi_type(_t)                     \
        switch (sizeof(_t)) {                               \
            case 1: type = &ffi_type_uint8; break;          \
            case 2: type = &ffi_type_uint16; break;         \
            case 4: type = &ffi_type_uint32; break;         \
            case 8: type = &ffi_type_uint64; break;         \
            default: sky_error_fatal("not implemented");    \
        }

    determine_unsigned_ffi_type(gid_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_GID_T] = type;
    determine_unsigned_ffi_type(uid_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_UID_T] = type;
    determine_signed_ffi_type(pid_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_PID_T] = type;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SYSCALL_PID_T] = type;
    determine_unsigned_ffi_type(mode_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_MODE_T] = type;
    determine_signed_ffi_type(off_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_OFF_T] = type;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SYSCALL_OFF_T] = type;
    determine_signed_ffi_type(dev_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_DEV_T] = type;
    sky_native_code_ffi_types[SKY_DATA_TYPE_FD] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_FD2] = &ffi_type_sint;
    determine_signed_ffi_type(sky_unicode_char_t);
    sky_native_code_ffi_types[SKY_DATA_TYPE_CODEPOINT] = type;

#undef determine_unsigned_ffi_type
#undef determine_signed_ffi_type


    sky_native_code_ffi_types[SKY_DATA_TYPE_VOID] = &ffi_type_void;
    sky_native_code_ffi_types[SKY_DATA_TYPE_FLOAT] = &ffi_type_float;
    sky_native_code_ffi_types[SKY_DATA_TYPE_DOUBLE] = &ffi_type_double;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INT] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINT] = &ffi_type_uint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_CHAR] = &ffi_type_schar;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UCHAR] = &ffi_type_uchar;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SHORT] = &ffi_type_sshort;
    sky_native_code_ffi_types[SKY_DATA_TYPE_USHORT] = &ffi_type_ushort;
    sky_native_code_ffi_types[SKY_DATA_TYPE_LONG] = &ffi_type_slong;
    sky_native_code_ffi_types[SKY_DATA_TYPE_ULONG] = &ffi_type_ulong;
#if LLONG_MAX == INT64_MAX
    sky_native_code_ffi_types[SKY_DATA_TYPE_LONG_LONG] = &ffi_type_sint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_ULONG_LONG] = &ffi_type_uint64;
#else
#   error implementation missing
#endif
    sky_native_code_ffi_types[SKY_DATA_TYPE_INT8] = &ffi_type_sint8;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINT8] = &ffi_type_uint8;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INT16] = &ffi_type_sint16;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINT16] = &ffi_type_uint16;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INT32] = &ffi_type_sint32;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINT32] = &ffi_type_uint32;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INT64] = &ffi_type_sint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINT64] = &ffi_type_uint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_BOOL] = &ffi_type_sint32;
#if SIZE_MAX == ULONG_MAX
    sky_native_code_ffi_types[SKY_DATA_TYPE_SIZE_T] = &ffi_type_ulong;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SSIZE_T] = &ffi_type_slong;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INDEX] = &ffi_type_slong;
#else
#   error implementation missing
#endif
#if INTMAX_MAX == INT64_MAX
    sky_native_code_ffi_types[SKY_DATA_TYPE_INTMAX_T] = &ffi_type_sint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINTMAX_T] = &ffi_type_uint64;
#else
#   error implementation missing
#endif
#if SKY_ARCH_64BIT
    sky_native_code_ffi_types[SKY_DATA_TYPE_PTRDIFF_T] = &ffi_type_sint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UPTRDIFF_T] = &ffi_type_uint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INTPTR_T] = &ffi_type_sint64;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINTPTR_T] = &ffi_type_uint64;
#elif SKY_ARCH_32BIT
    sky_native_code_ffi_types[SKY_DATA_TYPE_PTRDIFF_T] = &ffi_type_sint32;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UPTRDIFF_T] = &ffi_type_uint32;
    sky_native_code_ffi_types[SKY_DATA_TYPE_INTPTR_T] = &ffi_type_sint32;
    sky_native_code_ffi_types[SKY_DATA_TYPE_UINTPTR_T] = &ffi_type_uint32;
#else
#   error implementation missing
#endif
    sky_native_code_ffi_types[SKY_DATA_TYPE_VOID_PTR] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_STRING] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_ERRNO] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SYSCALL_VOID] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SYSCALL_INT] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_SYSCALL_BOOL] = &ffi_type_sint;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_BOOLEAN] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_BYTEARRAY] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_BYTES] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_COMPLEX] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_DICT] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_FLOAT] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_INTEGER] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_LIST] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_MODULE] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_SET] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_STRING] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_TUPLE] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_TYPE] = &ffi_type_pointer;
    sky_native_code_ffi_types[SKY_DATA_TYPE_OBJECT_INSTANCEOF] = &ffi_type_pointer;

    i = sizeof(sky_native_code_ffi_types) / sizeof(sky_native_code_ffi_types[0]);
    while (--i > 0) {
        if (sky_native_code_ffi_types[i]) {
            sky_native_code_max_width =
                    SKY_MAX(sky_native_code_max_width,
                            sky_native_code_ffi_types[i]->size);
        }
    }
}


sky_native_code_t
sky_native_code_create(sky_native_code_function_t   function,
                       sky_data_type_t              return_type,
                       sky_parameter_list_t         parameters,
                       ...)
{

    ffi_status              status;
    unsigned int            i;
    sky_data_type_t         atype;
    sky_native_code_t       code;
    sky_native_code_data_t  *code_data;

    sky_once_run(&sky_native_code_once, sky_native_code_once_runner, NULL);

    code = sky_object_allocate(sky_native_code_type);
    code_data = sky_native_code_data(code);
    code_data->function = function;

    if (parameters) {
        sky_parameter_list_data_t   *list_data;
        sky_parameter_list_item_t   *parameter;

        list_data = sky_parameter_list_data(parameters);
        code_data->natypes = sky_hashtable_length(&(list_data->keywords));
        if (code_data->natypes) {
            code_data->ffi_atypes = sky_malloc(code_data->natypes *
                                               sizeof(ffi_type));
            for (i = 0, parameter = list_data->first_keyword;
                 parameter;
                 ++i, parameter = parameter->next)
            {
                atype = parameter->data_type & SKY_DATA_TYPE_MASK;
                sky_error_validate_debug((int)atype > 0 && (int)atype <= 255);
                code_data->ffi_atypes[i] = sky_native_code_ffi_types[atype];
            }
        }
    }

    code_data->return_type = return_type;
    if (sky_data_type_isextraneeded(return_type)) {
        va_list ap;

        va_start(ap, parameters);
        sky_object_gc_set(&(code_data->return_type_extra),
                          va_arg(ap, sky_object_t),
                          code);
        va_end(ap);
    }
    return_type &= SKY_DATA_TYPE_MASK;
    sky_error_validate_debug((int)return_type >= 0 && (int)return_type <= 255);
    code_data->ffi_rtype = sky_native_code_ffi_types[return_type];

    status = ffi_prep_cif(&(code_data->cif),
                          FFI_DEFAULT_ABI,
                          code_data->natypes,
                          code_data->ffi_rtype,
                          code_data->ffi_atypes);
    sky_error_validate(FFI_OK == status);

    return code;
}


sky_object_t
sky_native_code_execute(sky_native_code_t       code,
                        void **                 avalues,
                        SKY_UNUSED sky_dict_t   globals,
                        SKY_UNUSED sky_object_t locals)
{
    sky_native_code_data_t  *code_data = sky_native_code_data(code);

    uint8_t         rvalue[SKY_MAX(sizeof(double), sizeof(intmax_t))];
    sky_object_t    return_value;

    sky_error_validate_debug(sky_native_code_max_width <= sizeof(rvalue));

    ffi_call(&(code_data->cif), code_data->function, rvalue, avalues);
    return_value = sky_data_type_load(code_data->return_type,
                                      &rvalue,
                                      code_data->return_type_extra);

    return return_value;
}


sky_native_code_function_t
sky_native_code_function(sky_native_code_t code)
{
    return sky_native_code_data(code)->function;
}


#if defined(HAVE_DLFCN_H)
sky_string_t
sky_native_code_repr(sky_native_code_t self)
{
    sky_native_code_data_t  *self_data = sky_native_code_data(self);

    Dl_info info;

    if (!dladdr(self_data->function, &info) || !info.dli_sname) {
        return sky_string_createfromformat("<%@ object at %p>",
                                           sky_type_name(sky_object_type(self)),
                                           self);
    }

    return sky_string_createfromformat("<%@ object \"%s\" in \"%s\" at %p>",
                                       sky_type_name(sky_object_type(self)),
                                       info.dli_sname,
                                       info.dli_fname,
                                       self);
}
#endif


SKY_TYPE_DEFINE_SIMPLE(native_code,
                       "native_code",
                       sizeof(sky_native_code_data_t),
                       NULL,
                       sky_native_code_instance_finalize,
                       sky_native_code_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_native_code_initialize_library(void)
{
    sky_type_setmethodslots(sky_native_code_type,
#if defined(HAVE_DLFCN_H)
            "__repr__", sky_native_code_repr,
#endif
            NULL);
}
