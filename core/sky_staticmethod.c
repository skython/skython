#include "sky_private.h"


typedef struct sky_staticmethod_data_s {
    sky_object_t                        function;
} sky_staticmethod_data_t;

SKY_EXTERN_INLINE sky_staticmethod_data_t *
sky_staticmethod_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_staticmethod_type) {
        return (sky_staticmethod_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_staticmethod_type);
}


static void
sky_staticmethod_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_staticmethod_data_t *method_data = data;

    sky_object_visit(method_data->function, visit_data);
}


static sky_object_t
sky_staticmethod_isabstractmethod_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_object_t    function = sky_staticmethod_data(self)->function;

    sky_object_t    isabstract;

    isabstract = sky_object_getattr(function,
                                    SKY_STRING_LITERAL("__isabstractmethod__"),
                                    sky_False);
    return (sky_object_bool(isabstract) ? sky_True : sky_False);
}


sky_object_t
sky_staticmethod_get(           sky_object_t    self,
                     SKY_UNUSED sky_object_t    instance,
                     SKY_UNUSED sky_type_t      owner)
{
    return sky_staticmethod_data(self)->function;
}


void
sky_staticmethod_init(sky_object_t self, sky_object_t function)
{
    sky_staticmethod_data_t *method_data = sky_staticmethod_data(self);

    sky_object_gc_set(&(method_data->function), function, self);
}


sky_staticmethod_t
sky_staticmethod_create(sky_object_t function)
{
    sky_staticmethod_t      staticmethod;
    sky_staticmethod_data_t *method_data;

    staticmethod = sky_object_allocate(sky_staticmethod_type);
    method_data = sky_staticmethod_data(staticmethod);
    sky_object_gc_set(&(method_data->function), function, staticmethod);

    return staticmethod;
}


sky_object_t
sky_staticmethod_function(sky_staticmethod_t self)
{
    return sky_staticmethod_data(self)->function;
}


static const char sky_staticmethod_type_doc[] =
"staticmethod(function) -> method\n"
"\n"
"Convert a function to be a static method.\n"
"\n"
"A static method does not receive an implicit first argument.\n"
"To declare a static method, use this idiom:\n"
"\n"
"  class C:\n"
"      def f(arg1, arg2, ...): ...\n"
"      f = staticmethod(f)\n"
"\n"
"It can be called either on the class (e.g. C.f()) or on an instance\n"
"(e.g. C().f()). The instance is ignored except for its class.\n"
"\n"
"Static methods in Python are similar to those found in Java or C++.\n"
"For a more advanced concept, see the classmethod builtin.";


SKY_TYPE_DEFINE_SIMPLE(staticmethod,
                       "staticmethod",
                       sizeof(sky_staticmethod_data_t),
                       NULL,
                       NULL,
                       sky_staticmethod_instance_visit,
                       SKY_TYPE_FLAG_HAS_DICT,
                       sky_staticmethod_type_doc);


void
sky_staticmethod_initialize_library(void)
{
    sky_type_setattr_member(
            sky_staticmethod_type,
            "__func__",
            NULL,
            offsetof(sky_staticmethod_data_t, function),
            SKY_DATA_TYPE_OBJECT,
            SKY_MEMBER_FLAG_READONLY);
    sky_type_setattr_getset(
            sky_staticmethod_type,
            "__isabstractmethod__",
            NULL,
            sky_staticmethod_isabstractmethod_getter,
            NULL);

    sky_type_setmethodslotwithparameters(
            sky_staticmethod_type,
            "__init__",
            (sky_native_code_function_t)sky_staticmethod_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "function", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_staticmethod_type,
            "__get__", sky_staticmethod_get,
            NULL);
}
