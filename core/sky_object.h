/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_OBJECT_H__
#define __SKYTHON_CORE_SKY_OBJECT_H__ 1


#include "sky_base.h"
#include "sky_object_types.h"
#include "sky_None.h"
#include "sky_NotSpecified.h"


SKY_CDECLS_BEGIN


/** A structure containing visitation data passed around during object
  * visitation.
  */
typedef struct sky_object_visit_data_s sky_object_visit_data_t;

/** The signature of a function called to visit an object.
  * Primarily used during garbage collection, implementations of this function
  * should call sky_object_visit() for every object to which it is holding a
  * strong reference. The visitation data (@a visit_data) should be passed for
  * each call as-is, because it may contain additional data that is opaque to
  * the visited objects.
  *
  * @param[in]  self        the object instance being visited.
  * @param[in]  data        the object's instance data for the type in the MRO
  *                         for which the visitation function is being called.
  * @param[in]  visit_data  visitation data.
  */
typedef void
        (*sky_object_visit_t)(sky_object_t              self,
                              void *                    data,
                              sky_object_visit_data_t * visit_data);

/** A set of constants defining the reason why an object is being visited. **/
typedef enum sky_object_visit_reason_e {
    /** The reason is unspecified. **/
    SKY_OBJECT_VISIT_REASON_UNKNOWN         =   0,
    /** The object is transitioning from ephemeral to concrete. **/
    SKY_OBJECT_VISIT_REASON_CONCRETE        =   1,
    /** The object is transitioning from thread-local to global (implies
      * concrete).
      */
    SKY_OBJECT_VISIT_REASON_GLOBAL          =   2,
    /** The object is being resurrected (implies concrete). **/
    SKY_OBJECT_VISIT_REASON_RESURRECT       =   3,
    /** The object is being marked by a running garbage collection cycle. **/
    SKY_OBJECT_VISIT_REASON_GC_MARK         =   4,
    /** The object is being marked for preservation due to weakref during
      * garbage collection sweep.
      */
    SKY_OBJECT_VISIT_REASON_WEAKREF_MARK    =   5,
} sky_object_visit_reason_t;

/** The signature of a function called to perform a visitation action.
  * During object visitation, objects are visited with an action function,
  * which is defined by this signature. This function should do whatever is
  * needed to be done with the object, and then return @c SKY_TRUE if the
  * visitation of the object should continue (its type visitation functions
  * should be called).
  *
  * @param[in]  object      the object instance being visited.
  * @param[in]  visit_data  visitation data.
  * @return     @c SKY_TRUE if the object's type visitation functions should
  *             be called, or @c SKY_FALSE if they should not.
  */
typedef sky_bool_t
        (*sky_object_visit_action_t)(sky_object_t               object,
                                     sky_object_visit_data_t *  visit_data);

/** A structure containing visitation data passed around during object
  * visitation.
  */
struct sky_object_visit_data_s {
    /** The reason why the object is being visited. **/
    sky_object_visit_reason_t           reason;

    /** The arbitrary pointer passed to sky_object_walk(). **/
    void *                              arg;

    /** Internal data used by sky_object_walk() and sky_object_visit(). **/
    void *                              reserved;
};


/** The signature of a function to be called for an object in the child process
  * after forking.
  *
  * @param[in]  object  the object for which the function is being called.
  */
typedef void (*sky_object_atfork_child_t)(sky_object_t object);

/** The signature of a function to be called for an object in the parent
  * process after forking.
  *
  * @param[in]  object  the object for which the function is being called.
  */
typedef void (*sky_object_atfork_parent_t)(sky_object_t object);

/** The signature of a function to be called for an object before forking.
  *
  * @param[in]  object  the object for which the function is being called.
  */
typedef void (*sky_object_atfork_prepare_t)(sky_object_t object);


/** Allocate a new instance of an object type.
  *
  * @param[in]  type    the type to allocate.
  * @return     memory for a new object of the requested type.
  */
SKY_EXTERN sky_object_t
sky_object_allocate(sky_type_t type);


/** Return an ASCII representation of an object.
  * Most often, the result will be exactly the same as sky_object_repr(), but
  * if sky_object_repr() returns a string containing non-ASCII characters, they
  * will be escaped with \\x, \\u, or \\U as appropriate.
  *
  * @param[in]  object  the object to represent in ASCII.
  * @return     a string object containing only codepoints that are in the
  *             ASCII character set (0x00..0x7F).
  */
SKY_EXTERN sky_string_t
sky_object_ascii(sky_object_t object);


/** Register an object instance for atfork handling.
  * Only a single set of functions may be registered for an object. If a second
  * set of functions is registered, it will overwrite the first. If an object
  * instance is registered for atfork handling, it must be unregistered before
  * the instance is freed. This may be done from the object instance type's
  * finalize handler.
  *
  * @param[in]  object  the object to register.
  * @param[in]  prepare the function to be called before forking.
  * @param[in]  parent  the function to be called in the parent process after
  *                     forking.
  * @param[in]  child   the function to be called in the child process after
  *                     forking.
  */
SKY_EXTERN void
sky_object_atfork_register(sky_object_t                 object,
                           sky_object_atfork_prepare_t  prepare,
                           sky_object_atfork_parent_t   parent,
                           sky_object_atfork_child_t    child);

/** Unregister an object instance for atfork handling.
  *
  * @param[in]  object  the object to unregister.
  */
SKY_EXTERN void
sky_object_atfork_unregister(sky_object_t object);


/** Return the Boolean truth value for an object.
  * Returns either @c SKY_TRUE or @c SKY_FALSE for an object. This is roughly
  * equivalent to calling the object's __bool__() method. If the object does
  * not have a __bool__() method, its __len__() method will be called, and a
  * non-zero result will be treated as true. In the absence of both methods,
  * the object will unconditionally be considered true.
  *
  * @param[in]  object  the object for which a truth value is required.
  * @return     @c SKY_TRUE if the object is considered true, or @c SKY_FALSE
  *             if it is considered false.
  */
SKY_EXTERN sky_bool_t
sky_object_bool(sky_object_t object);


/** Create a new object based on a format string that defines the object.
  * Using a format string, a new object will be built. This is most useful
  * for building dicts, but it can be used to build any basic object type. If
  * the format string is empty, @c sky_None will be returned. If the format
  * string contains a single format specifier, an appropriate object for the
  * specifier will be returned (e.g., for 'i' a sky_integer will be returned).
  * If a format string has more than a single specifier without a container, a
  * list will be created. In all cases where a pointer value is expected, if
  * the pointer value is @c NULL, @c sky_None wil be used. The recognized
  * format specifiers are as follows:
  *
  * @li [...]   enclosing format specifiers in square brackets will cause a
  *             sky_list object to be created with everything between the
  *             brackets inserted into the list.
  * @li (...)   enclosing format specifiers in parentheses will cause a
  *             sky_tuple object to be created with everything between the
  *             brackets inserted into the tuple.
  * @li {...}   enclosing format specifiers in braces will cause a sky_dict
  *             object to be created with everything between the braces
  *             inserted into the dict. Keys and values are paired, and so
  *             there must always be an even number of specifiers between the
  *             braces.
  * @li <...>   enclosing format specifiers in angle brackets will cause a
  *             sky_set object to be created with everything between the angle
  *             brackets inserted into the set.
  *
  * @li O       pass an object instance through as is (sky_object_t).
  * @li s       create a sky_string using UTF-8 encoding from a C-style string
  *             (const char *).
  * @li s#      create a sky_string using UTF-8 encoding from an array of bytes
  *             and a length (const void *, size_t)
  * @li es      create a sky_string using the specified encoding from a C-style
  *             string (sky_string_t encoding, const char *string)
  * @li es#     create a sky_string using the specified encoding from an array
  *             of bytes and a length (sky_string_t encoding, const void *bytes,
  *             size_t nbytes)
  * @li y#      create a sky_bytes object from an array of bytes and a length
  *             (const void *, size_t)
  * @li Y#      create a sky_bytearray object from an array of bytes and a
  *             length (const void *, size_t)
  *
  * @li B       create a sky_boolean (true or false) from an integral truth
  *             value (int).
  * @li d       create a sky_float from a C double type (double)
  * @li p       create a sky_capsule with a @c NULL name from a pointer (void *)
  * @li P       create a sky_capsule with a name from a pointer (void *pointer,
  *             const char *name);
  *
  * For the integer types ('i' and 'u'), the default is to use int or unsigned
  * int unless the size is specified after it as either a number of bits (8,
  * 16, 32, or 64), or using a standard size suffix, which may be 'j' for
  * @c intmax_t, or 'z' for @c size_t.
  * 
  * @li i       create a sky_integer from a signed integer type.
  * @li u       create a sky_integer from an unsigned integer type.
  *
  * @param[in]  format      the format string to use to build the object.
  * @param[in]  ...         values as required to satisfy the format string.
  * @return     the new object instance built from the format string.
  */
SKY_EXTERN sky_object_t
sky_object_build(const char *format, ...);

/** Create a new object based on a format string that defines the object.
  * See sky_object_build() for a detailed description of the behavior of this
  * function.
  *
  * @param[in]  format      the format string to use to build the object.
  * @param[in]  ap          values as required to satisfy the format string.
  * @return     the new object instance built from the format string.
  */
SKY_EXTERN sky_object_t
sky_object_buildv(const char *format, va_list ap);


/** Return a bytes object instance created from an arbitrary object.
  * If the object's type defines a __bytes__() method, that will be used to
  * create the bytes object instance to be returned; otherwise, a call to
  * sky_bytes_createfromobject() will be made to create the bytes object,
  * which may then raise a @c sky_TypeError.
  *
  * @param[in]  object      the object to convert to a bytes object.
  * @return     a bytes object instance created from @a object.
  */
SKY_EXTERN sky_bytes_t
sky_object_bytes(sky_object_t object);


/** Execute the __call__() method of a callable object.
  * This is equivalent to calling the object's __call__() method.
  *
  * @param[in]  object      the callable object to call.
  * @param[in]  args        the positional arguments to pass to the __call__()
  *                         method of @a object. May be @c NULL.
  * @param[in]  kws         the keyword arguments to pass to the __call__()
  *                         method of @a object. May be @c NULL.
  * @return     the return value from the __call__() method, which will never
  *             be @c NULL.
  */
SKY_EXTERN sky_object_t
sky_object_call(sky_object_t object, sky_tuple_t args, sky_dict_t kws);


/** Determine whether an object is callable.
  *
  * @param[in]  object      the object to check.
  * @return     @c SKY_TRUE if the object is callable, or @c SKY_FALSE if it is
  *             not.
  */
SKY_EXTERN sky_bool_t
sky_object_callable(sky_object_t object);


/** Call a named callable attribute of an object.
  * This is a convenience function that effectively calls a method of an
  * of an object by looking up the callable object by name as an attribute of
  * the object, and then calling sky_object_call() for that callable object.
  *
  * @param[in]  object      the object instance for which the method is to be
  *                         called.
  * @param[in]  name        the name of the method to call.
  * @param[in]  args        the positional arguments to be passed to the call.
  *                         May be @c NULL.
  * @param[in]  kws         the keyword arguments to be passed to the call.
  *                         May be @c NULL.
  * @return     the return value from the __call__() method, or @c NULL if the
  *             attribute named @a method does not exist for @a object.
  */
SKY_EXTERN sky_object_t
sky_object_callmethod(sky_object_t  object,
                      sky_string_t  name,
                      sky_tuple_t   args,
                      sky_dict_t    kws);


/** Compare two objects.
  * Two objects will be compared using the specified operator. If the specified
  * order does not implement the comparison, the inverse order will be tried.
  * If neither ordering implements the comparison, a @c sky_TypeError exception
  * will be raised for comparisons other than @c SKY_COMPARE_OP_NOT_EQUAL and
  * @c SKY_COMPARE_OP_EQUAL. Pointer comparisons will be used for equality
  * operators.
  *
  * @param[in]  object      the object to compare.
  * @param[in]  other       the other object to compare.
  * @param[in]  compare_op  the comparison operator to use.
  * @return     @c SKY_TRUE if the comparison result is true, or
  *             @c SKY_FALSE if the compareison result is false.
  */
SKY_EXTERN sky_bool_t
sky_object_compare(sky_object_t     object,
                   sky_object_t     other,
                   sky_compare_op_t compare_op);


/** Determine whether an object contains another object.
  * This is equivalent to calling the object's __contains__() method. If the
  * object does not implement __contains__() but implements __iter__(), simple
  * iteration will be performed.
  *
  * @param[in]  object      the container object in which to look.
  * @param[in]  item        the object to be looked for.
  * @return     @c SKY_TRUE if @a object contains @a item, or @c SKY_FALSE if
  *             it does not.
  */
SKY_EXTERN sky_bool_t
sky_object_contains(sky_object_t object, sky_object_t item);


/** Return the data pointer for a type in an object instance.
  * Every type in the inheritance hierarchy for a type may have instance data
  * associated with it; this function returns a pointer to the instance data.
  * If @a object is not an instance of @a type, a type error is raised.
  * No error is raised if @a object is @c NULL.
  *
  * @param[in]  object      the object for which the data pointer is to be
  *                         returned. May be @c NULL, in which case @c NULL
  *                         will be returned regardless of @a type.
  * @param[in]  type        the type for which the data pointer is to be
  *                         returned. Must not be @c NULL.
  * @return     the data pointer.
  */
SKY_EXTERN void *
sky_object_data(sky_object_t object, sky_type_t type);


/** Delete an attribute from an object.
  * This is equivalent to calling the object's __delattr__() method.
  *
  * @param[in]  object      the object from which the attribute is to be
  *                         deleted.
  * @param[in]  name        the name of the attribute to delete.
  */
SKY_EXTERN void
sky_object_delattr(sky_object_t object, sky_string_t name);


/** Delete an item from a container.
  * This is equivalent to calling the object's __delitem__() method.
  *
  * @param[in]  object      the container from which @a item is to be deleted.
  * @param[in]  item        the item to be deleted from @a object.
  */
SKY_EXTERN void
sky_object_delitem(sky_object_t object, sky_object_t item);


/** Return an object's instance dictionary, creating it if necessary.
  * An object instance may only have a dictionary if its type declares that it
  * has one. If an object instance does not have a dictionary, a new dictionary
  * will be created if the type declares that it has one; otherwise, the return
  * will be @c NULL.
  *
  * @param[in]  object      the object for which the instance dictionary is to
  *                         be returned.
  * @return     the instance dictionary for @a object, or @c NULL if it does
  *             not have one.
  */
SKY_EXTERN sky_dict_t
sky_object_dict(sky_object_t object);


/** Set an object's instance dictionary.
  * An object instance may only have a dictionary if its type declares that it
  * has one.
  *
  * @param[in]  object      the object for which the instance dictionary is to
  *                         be set.
  * @param[in]  dict        the dictionary to use.
  */
SKY_EXTERN void
sky_object_setdict(sky_object_t object, sky_dict_t dict);


/** Return a list of attributes defined by an object.
  * This is equivalent to calling the object's __dir__() method.
  *
  * @param[in]  object      the object for which attributes are to be returned.
  * @return     the list of attributes defined by the object according to its
  *             __dir__() method. The order of the list is not defined.
  */
SKY_EXTERN sky_list_t
sky_object_dir(sky_object_t object);


/** Dump information about an object to stdout.
  * This is only really useful for debugging. It dumps internal information
  * about an arbitrary object to @c stdout. The C runtime's @c stdout is
  * written to directly; output is not routed through @c sys.stdout.
  *
  * @param[in]  object      the object to dump to @c stdout.
  */
SKY_EXTERN void
sky_object_dump(sky_object_t object);


/** Compare two objects for equality.
  * This is equivalent to calling the object's __eq__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if the objects are equal, @c sky_False if they
  *             are not, or @c sky_NotImplemented if the comparison is not
  *             supported.
  */
SKY_EXTERN sky_object_t
sky_object_eq(sky_object_t object, sky_object_t other);


/** Generate a string representation of an object using a type-specific format
  * specifier.
  * This is equivalent to calling the object's __format__() method.
  *
  * @param[in]  object          the object to format.
  * @param[in]  format_spec     the format specifier to use.
  * @return     a formatted string representation of @a object.
  */
SKY_EXTERN sky_string_t
sky_object_format(sky_object_t object, sky_string_t format_spec);


/** Compare one object to determine if it is greater than or equal to another.
  * This is equivalent to calling the object's __ge__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if @a object is greater than or equal to @a other.
  *             @c sky_False if @a object is less than @a other.
  *             @c sky_NotImplemented if the comparison is not supported.
  */
SKY_EXTERN sky_object_t
sky_object_ge(sky_object_t object, sky_object_t other);


/** Get the value of an attribute from an object.
  * This is equivalent to calling the object's __getattribute__() method. If
  * the __getattribute__() method raises a @c sky_AttributeError exception, the
  * object's __getattr__() method will be called. If __getattr__() raises a
  * @c sky_AttributeError exception and @a value is not @c sky_NotSpecified,
  * the exception will be caught and @a value will be returned instead.
  *
  * @param[in]  object      the object to be queried.
  * @param[in]  name        the name of the attribute to return.
  * @param[in]  value       the value to return if the attribute @a name does
  *                         not exist. If @c sky_NotSpecified,
  *                         @c sky_AttributeError will be raised.
  * @return     the value of the attribute.
  */
SKY_EXTERN sky_object_t
sky_object_getattr(sky_object_t object, sky_string_t name, sky_object_t value);


/** Get an item from a container.
  * This is equivalent to calling the object's __getitem__() method. For
  * sequence objects, @a item should be an integer or slice object. For mapping
  * objects, @a item can be any hashable object. If the item does not exist,
  * an appropriate exception will be raised (e.g., @c sky_IndexError or
  * @c sky_KeyError).
  *
  * @param[in]  object      the container from which @a item is to be obtained.
  * @param[in]  item        the item to obtain.
  * @return     the object associated with @a item in the container @a object.
  */
SKY_EXTERN sky_object_t
sky_object_getitem(sky_object_t object, sky_object_t item);


/** Return arguments suitable for passing to an object type's constructor to
  * recreate the object.
  * This is equivalent to calling the object's __getnewargs__() method that is
  * normally used by the pickle module.
  *
  * @param[in]  object      the object for which constructor arguments are to
  *                         be returned.
  * @return     a tuple of arguments that may be passed to the object type's
  *             constructor to recreate @a object.
  */
SKY_EXTERN sky_tuple_t
sky_object_getnewargs(sky_object_t object);


/** Return an object's state for serialization.
  * This is equivalent to calling the object's __getstate__() method that is
  * normally used by the pickle module. If the object does not define a
  * __getstate__() method, the return value will be @c NULL.
  *
  * @param[in]  object      the object for which state information is to be
  *                         returned.
  * @return     state information as returned by the object's __getstate__()
  *             method.
  */
SKY_EXTERN sky_object_t
sky_object_getstate(sky_object_t object);


/** Compare one object to determine if it is greater than another.
  * This is equivalent to calling the object's __gt__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if @a object is greater than @a other.
  *             @c sky_False if @a object is less than or equal to @a other.
  *             @c sky_NotImplemented if the comparison is not supported.
  */
SKY_EXTERN sky_object_t
sky_object_gt(sky_object_t object, sky_object_t other);


/** Determine whether an object has a specifically named attribute.
  * This is a convenience wrapper around calling sky_object_getattr() and
  * handling the @c sky_AttributeError that may be raised of the attribute does
  * not exist.
  *
  * @param[in]  object      the object for which the existence of the named
  *                         attribute is to be checked.
  * @param[in]  name        the name of the attribute to look for.
  * @return     @c SKY_TRUE if the attribute exists; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_object_hasattr(sky_object_t object, sky_string_t name);


/** Return a hash value for an object.
  * Not all objects are hashable. In particular, an object that is mutable
  * should raise an exception rather than return a hash value, because the
  * hash value should remain constant through the lifetime of an object. This
  * is equivalent to calling the object's __hash__() method.
  *
  * @param[in]  object      the object for which a hash value is to be
  *                         returned.
  * @return     the hash value.
  */
SKY_EXTERN uintptr_t
sky_object_hash(sky_object_t object);


/** Initialize an object instance.
  * This function will call each of the initializer's for the types appearing
  * in the object's type's MRO. If an initializer raises an exception, the
  * finalizers matching the initializers run so far will be called, and the
  * object will be left in an uninitialized state rather than partially
  * initialized.
  *
  * This function is normally called by sky_object_allocate(), which will
  * further completely remove the allocated object from the calling thread's
  * object list before finally re-raising the raised exception if any
  * initializer raises an exception.
  *
  * @param[in]  object  the object instance to initialize.
  */
SKY_EXTERN void
sky_object_initialize(sky_object_t object);


/** Check an object to determine if it is an instance of an abstract type.
  * This is the default implementation of __instancecheck__().
  *
  * @param[in]  cls         the class to check.
  * @param[in]  instance    the instance to check.
  * @return     @c SKY_TRUE if @a instance is an instance of @a cls.
  */
SKY_EXTERN sky_bool_t
sky_object_instancecheck(sky_object_t cls, sky_object_t instance);


/** Test an object to determine whether it's an instance of a specific type.
  * This test uses the type's MRO to determine whether it's an instance. It's
  * only guaranteed to work for types that do not define __instancecheck__()
  * methods.
  *
  * @param[in]  object      the object to be tested.
  * @param[in]  type        the type to be tested.
  * @return     @c SKY_TRUE if @a object is an instance of @a type, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_object_isa(sky_object_t object, sky_type_t type);


/** Test an object to determine whether it's globally visible.
  * When an object is first created, it is only visible to the thread that
  * created it. If it ever gets stored (via sky_object_gc_set() or other
  * similar write barrier) into a global object or explicitly made global via
  * sky_object_gc_setroot(), it will be forever global. The idea is that
  * non-global objects do not require any kind of synchronization if they're
  * mutated. By testing an object's global status, expensive synchronization
  * can often be avoided if an application is smart about sharing as little
  * data as possible across threads. Note that tagged objects are always
  * considered global; however, tagged objects are always immutable. A tagged
  * tuple (a tuple containing a single element) may not be considered global if
  * its content is not global.
  */
SKY_EXTERN sky_bool_t
sky_object_isglobal(sky_object_t object);


/** Test an object to determine whether it's an instance of a specific type.
  * This test differs from sky_object_isa() in that it uses __isinstance__()
  * to perform the test. It is most likely slower, but it is guaranteed to be
  * correct. This is the version that the builtin isinstance() function uses.
  *
  * @param[in]  object      the object to be tested.
  * @param[in]  cls         the object to be tested against, which may also be
  *                         a tuple of objects to be tested against.
  * @return     @c SKY_TRUE if @a object is an instance of @a cls, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_object_isinstance(sky_object_t object, sky_object_t cls);


/** Test an object to determine whether it's a subclass of another.
  * This test is performed by calling __issubclass__() methods as required.
  *
  * @param[in]  object      the object to be tested.
  * @param[in]  cls         the object to be tested against, which may also be
  *                         a tuple of objects to be tested against.
  * @return     @c SKY_TRUE if @a object is a subclass of @a cls, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_object_issubclass(sky_object_t object, sky_object_t cls);


/** Test an object to determine whether it is iterable.
  * An object is iterable if its type supports fast sequence iteration, if its
  * type defines an __iter__() method, or if its type defines both __len__()
  * and __getitem__() methods.
  *
  * @param[in]  object      the object to be tested.
  * @return     @c SKY_TRUE if @a object is iterable, or @c SKY_FALSE if it is
  *             not.
  */
SKY_EXTERN sky_bool_t
sky_object_isiterable(sky_object_t object);


/** Test an object to determine whether it should be considered @c NULL.
  * The existence of this function serves two purposes. First, it avoids ugly
  * casting to do simple pointer checks against @c sky_None. Second, it also
  * considers other possible values for @c sky_None: @c sky_NotSpecified and
  * @c NULL.
  *
  * @param[in]  object      the object to be tested.
  * @return     @c SKY_TRUE if @a object should be considered to be @c NULL.
  */
SKY_EXTERN_INLINE sky_bool_t
sky_object_isnull(sky_object_t object)
{
    return (!object || sky_None == object || sky_NotSpecified == object);
}


/** Return a new iterator object to iterate over all of the objects in a
  * container.
  * This is equivalent to calling the object's __iter__() method.
  *
  * @param[in]  object      the object for which a new iterator is to be
  *                         returned.
  * @return     a new iterator object capable of iterating over all of the
  *             objects contained by @a object.
  */
SKY_EXTERN sky_object_t
sky_object_iter(sky_object_t object);


/** Compare one object to determine if it is less than or equal to another.
  * This is equivalent to calling the object's __le__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if @a object is less than or equal to @a other.
  *             @c sky_False if @a object is greater than @a other.
  *             @c sky_NotImplemented if the comparison is not supported.
  */
SKY_EXTERN sky_object_t
sky_object_le(sky_object_t object, sky_object_t other);


/** Return the length of an object.
  * The length of an object is defined by the type object, but typically it is
  * the number of items in a container or sequence.
  *
  * @param[in]  object      the object for which the length is to be returned.
  * @return     the length of the object.
  */
SKY_EXTERN ssize_t
sky_object_len(sky_object_t object);


/** Return the length hint for an object.
  * A length hint is a possibly imprecise length of an object. It is used as an
  * optimization to presize arrays that will be initialized from an iterable
  * object. Not all iterable objects are sized, however, so sky_object_len() is
  * inappropriate and not necessarily implemented. This function will always
  * return a value >= 0.
  *
  * @param[in]  object      the object for which the length hint is to be
  *                         returned.
  * @param[in]  hint        the default hint to use if no __len__() or
  *                         __length_hint__() methods are available.
  * @return     the length hint for the object.
  */
SKY_EXTERN ssize_t
sky_object_length_hint(sky_object_t object, ssize_t hint);


/** Compare one object to determine if it is less than another.
  * This is equivalent to calling the object's __lt__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if @a object is less than @a other.
  *             @c sky_False if @a object is greater than or equal to @a other.
  *             @c sky_NotImplemented if the comparison is not supported.
  */
SKY_EXTERN sky_object_t
sky_object_lt(sky_object_t object, sky_object_t other);


/** Compare two objects for inequality.
  * This is equivalent to calling the object's __ne__() method.
  *
  * @param[in]  object      the first object to compare.
  * @param[in]  other       the second object to compare.
  * @return     @c sky_True if the objects are not equal, @c sky_False if they
  *             are, or @c sky_NotImplemented if the comparison is not
  *             supported.
  */
SKY_EXTERN sky_object_t
sky_object_ne(sky_object_t object, sky_object_t other);


/** Return the next object in an iteration.
  * This is equivalent to calling the object's __next__() method.
  *
  * @param[in]  object      the iterator object.
  * @param[in]  value       the value to return when the iterator is exhausted.
  * @return     the next object in the iteration.
  */
SKY_EXTERN sky_object_t
sky_object_next(sky_object_t object, sky_object_t value);


/** Return an "official" string representation of an object.
  * If at all possible, the "official" string representation of an object
  * should look like a valid expression that could be used to recreate an
  * object with the same value. If this is not possible, the string returned
  * should be enclosed in angle brackets. This is roughly equivalent to calling
  * the object's __repr__() method.
  * 
  * @param[in]  object      the object for which an "official" representation
  *                         is to be returned.
  * @return     a string object instance that is the "official" representation
  *             of @a object.
  */
SKY_EXTERN sky_string_t
sky_object_repr(sky_object_t object);


/** Return a new reversed iterator object to iterate over all of the objects in
  * a sequence.
  * This is equivalent to calling the object's __reversed__() method.
  *
  * @param[in]  object      the object for which a new reversed iterator is to
  *                         be returned.
  * @return     a new reversed iterator object capable of iterating over all of
  *             the objects contained by @a object.
  */
SKY_EXTERN sky_object_t
sky_object_reversed(sky_object_t object);


/** Return a new iterator object that iterates over the contents of a container
  * in reverse order.
  * This is equivalent to calling the object's __reversed__() method. If the
  * object does not implement __reversed__(), a general purpose iterator will
  * be used that is based on __getitem__() if the object supports it.
  *
  * @param[in]  object      the container object for which a new reverse
  *                         iterator object is to be returned.
  * @return     a new reverse iterator.
  */


/** Set the value of an attribute for an object.
  * This is equivalent to calling the object's __setattr__() method.
  *
  * @param[in]  object      the object for which the attribute is to be set.
  * @param[in]  name        the name of the attribute to set.
  * @param[in]  value       the value to set for the attribute.
  */
SKY_EXTERN void
sky_object_setattr(sky_object_t object, sky_string_t name, sky_object_t value);


/** Set an item in a container.
  * This is equivalent to calling the object's __setitem__() method. For
  * sequence objects, @a item should be an integer or a slice. For mapping
  * objects, @a item should be any hashable object to serve as a key.
  *
  * @param[in]  object      the container to set.
  * @param[in]  item        the item to set.
  * @param[in]  value       the value to associate with the item.
  */
SKY_EXTERN void
sky_object_setitem(sky_object_t object, sky_object_t item, sky_object_t value);


/** Set an object's state.
  * This is equivalent to calling the object's __setstate__() method that is
  * normally used by the pickle module. If the object does not define a
  * __setstate__() method, the return value will be @c SKY_FALSE.
  *
  * @param[in]  object      the object for which state information is to be
  *                         set.
  * @param[in]  state       the state to set.
  * @return     @c SKY_TRUE if the object defines a __setstate__() method, or
  *             @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_object_setstate(sky_object_t object, sky_object_t state);


/** Return the amount of memory (in bytes) that an object is using.
  * This is equivalent to calling the object's __sizeof__() method.
  *
  * @param[in]  object      the object for which its memory usage is to be
  *                         returned.
  * @return     the amount of memory (in bytes) that an object is using.
  */
SKY_EXTERN size_t
sky_object_sizeof(sky_object_t object);


/** Mark the start of processing for an object in the current thread of
  * execution.
  * When generating strings for sky_object_repr() and sky_object_str(),
  * it may be that container objects may contain references to themselves. The
  * use of sky_object_stack_push() and sky_object_stack_pop() is intended
  * to guard against infinite recursion. If sky_object_stack_push() returns
  * @c SKY_TRUE it means that the object is already on the stack. In this
  * case, the object is not added to the stack again, the caller should not
  * continue normally, and sky_object_stack_pop() should not be called for
  * the object at this time. If sky_object_stack_push() returns @c SKY_FALSE
  * it means that the object is not already on the stack, it is pushed onto the
  * stack, and sky_object_stack_pop() must be called after processing of the
  * object has completed.
  *
  * @param[in]  object          the object instance to push.
  * @return     @c SKY_TRUE if the object is already on the stack; otherwise,
  *             @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_object_stack_push(sky_object_t object);


/** Mark the end of processing for an object in the current thread of
  * execution.
  * When generating strings for sky_object_repr() and sky_object_str(),
  * it may be that container objects may contain references to themselves. The
  * use of sky_object_stack_push() and sky_object_stack_pop() is intended
  * to guard against infinite recursion. If sky_object_stack_push() returns
  * @c SKY_TRUE it means that the object is already on the stack. In this
  * case, the object is not added to the stack again, the caller should not
  * continue normally, and sky_object_stack_pop() should not be called for
  * the object at this time. If sky_object_stack_push() returns @c SKY_FALSE
  * it manes that the object is not already on the stack, it is pushed onto the
  * stack, and sky_object_stack_pop() must be called after processing of the
  * object has completed.
  *
  * @param[in]  object          the object instance to pop.
  */
SKY_EXTERN void
sky_object_stack_pop(sky_object_t object);


/** Return an "informal" string representation of an object.
  * The "informal" string representation is often a human-readable
  * representation of an object. It differs from sky_object_repr() in that
  * the representation shouldn't necessarily be a valid expression. This is
  * roughly equivalent to calling the object's __str__() method. If the object
  * does not have a __str__() method, its __repr__() method will be called
  * instead.
  *
  * @param[in]  object      the object for which an "informal" representation
  *                         is to be returned.
  * @return     a string object instance that is the "informal" representation
  *             of @a object.
  */
SKY_EXTERN sky_string_t
sky_object_str(sky_object_t object);


/** Check an object to determine if it is a subclass of another.
  * This is the default implementation of __subclasscheck__().
  *
  * @param[in]  cls         the class to check.
  * @param[in]  subclass    the subclass to check.
  * @return     @c SKY_TRUE if @a subclass is a subclass of @a cls.
  */
SKY_EXTERN sky_bool_t
sky_object_subclasscheck(sky_object_t cls, sky_object_t subclass);


/** Return the type of an object instance.
  *
  * @param[in]  object      the object for which the type is to be returned.
  * @return     the object's type.
  */
SKY_EXTERN sky_type_t
sky_object_type(sky_object_t object);


/** Visit an object.
  * In an object type's visitation function, each object referenced by the
  * object should also be visited. This is done by calling this function with
  * the object to be visited and the visitation data that was passed in to the
  * object type's visitation function.
  *
  * @param[in]  object      the object to be visited.
  * @param[in]  visit_data  the object visit data to be used.
  */
SKY_EXTERN void
sky_object_visit(sky_object_t object, sky_object_visit_data_t *visit_data);


/** Walk an object and all of its references.
  *
  * @param[in]  object      the object to be walked.
  * @param[in]  reason      the reason for the visitation.
  * @param[in]  action      the action function to be called for each object
  *                         visited.
  * @param[in]  arg         an arbitrary pointer to be included in the visit
  *                         data passed to each visitation call.
  */
SKY_EXTERN void
sky_object_walk(sky_object_t                object,
                sky_object_visit_reason_t   reason,
                sky_object_visit_action_t   action,
                void *                      arg);


/** A function called by the garbage collector when it needs a thread to call
  * sky_object_gc() for some reason.
  * If a thread needs to register a wakeup function, it normally does so using
  * sky_object_gc_register(). The wakeup function should do whatever is needed
  * to wake the thread up, but it will never be called on the thread being
  * woken up. The wakeup function may do something like signal a condition
  * object, for example.
  *
  * @param[in]  arg         the arbitrary pointer stored in the call to
  *                         sky_object_gc_register().
  */
typedef void
(*sky_object_gc_wakeup_t)(void *arg);


/** Register a thread to participate in object garbage collection.
  * Use of any of the object APIs within a thread will normally automatically
  * register the thread to participate in garbage collection, requiring the
  * thread to periodically call sky_object_gc() in order for garbage collection
  * to function. Explicit registration allows a thread to register a function
  * that the garbage collection infrastructure will call when it needs a thread
  * to make a call to sky_object_gc() for whatever reason. A thread may safely
  * call sky_object_gc_register() more than once, but only one function to call
  * will ever be remembered.
  *
  * @param[in]  function    an optional function to call when the garbage
  *                         collector needs the thread to call sky_object_gc().
  *                         May be @c NULL.
  * @param[in]  arg         an arbitrary pointer that will be passed to the
  *                         wake up function.
  */
SKY_EXTERN void
sky_object_gc_register(sky_object_gc_wakeup_t function, void *arg);


/** Check in with the garbage collector, allowing it to perform any thread
  * local work that is needed.
  * Every thread that manipulates objects in any way should periodically call
  * sky_object_gc() to allow the garbage collector to do any work that needs to
  * be done within the context of the calling thread. The function will return
  * either @c SKY_TRUE or @c SKY_FALSE to indicate whether there is more work
  * that needs to be done as soon as possible. If there is, sky_object_gc()
  * should be called again as soon as is reasonable; otherwise, it's not
  * necessary to call it again for a while, depending on the needs of the
  * application.
  *
  * @return     @c SKY_TRUE if there is more work that needs to be done as
  *             soon as possible; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_object_gc(void);


/** Assign an object reference to a memory location as an object root.
  * Object garbage collection requires a set of roots that are used to locate
  * all active objects in the process. During a garbage collection sweep, any
  * object that is no longer reachable from a root is collected. Therefore, it
  * is extremely important that roots are properly kept up-to-date; otherwise,
  * premature sweeps or leaks may occur.
  *
  * This function will "release" the old root reference, and replace it with
  * the new one. The root location must always hold a valid pointer before
  * calling this function. @c NULL is a valid pointer.
  *
  * If @a global is @c SKY_TRUE, @a new_object_reference will be made a global
  * object, which will also make it concrete if it is still ephemeral. If the
  * object is already global, whatever value passed for @a global is effectively
  * meaningless since a global object can not be made local again. Making an
  * object global, concrete, or both has the effect of making all objects that
  * it references global, concrete, or both, as well. Passing @c SKY_FALSE will
  * never do anything, which will effectively preserve the object's global and
  * concrete states. Note that if @a new_object_reference is @c NULL, @a global
  * is ignored.
  *
  * In all cases, making an object instance a root makes it a concrete object.
  *
  * @param[in]  root_object_pointer     the memory location that holds the root
  *                                     object reference.
  * @param[in]  new_object_reference    the new reference to assign to the
  *                                     root location.
  * @param[in]  global                  @c SKY_TRUE if the object is globally
  *                                     visible. That is, a globally visible
  *                                     object can be accessed from more than
  *                                     just the thread that instantiated it.
  */
SKY_EXTERN void
sky_object_gc_setroot(sky_object_t *root_object_pointer,
                      sky_object_t  new_object_reference,
                      sky_bool_t    global);


/** Assign an object reference to a memory location as an object root, marking
  * it to be cleared automatically during library finalization.
  * This function is equivalent to sky_object_gc_setroot() with the additional
  * feature of automatically clearing the root reference during the final
  * garbage collection cycles of library finalization. Because of the way that
  * garbage collection is handled at this point, there is otherwise no good way
  * to safely clear root references. In addition to clearing the root reference
  * at the proper time during library finalization, the memory location holding
  * the reference will also be set to @c NULL.
  *
  * This function will "release" the old root reference, and replace it with
  * the new one. The root location must always hold a valid pointer before
  * calling this function. @c NULL is a valid pointer.
  *
  * If @a global is @c SKY_TRUE, @a new_object_reference will be made a global
  * object, which will also make it concrete if it is still ephemeral. If the
  * object is already global, whatever value passed for @a global is effectively
  * meaningless since a global object cannot be made local again. Making an
  * object global, concrete, or both has the effect of making all objects that
  * it references global, concrete, or both, as well. Note that if
  * @a new_object_reference is @c NULL, @a global is ignored.
  *
  * In all cases, making an object instance a root makes it a concrete object.
  *
  * @param[in]  root_object_pointer     the memory location that holds the root
  *                                     object reference.
  * @param[in]  new_object_reference    the new reference to assign to the
  *                                     root location.
  * @param[in]  global                  @c SKY_TRUE if the object is globally
  *                                     visible. That is, a globally visible
  *                                     object can be accessed from more than
  *                                     just the thread that instantiated it.
  */
SKY_EXTERN void
sky_object_gc_setlibraryroot(sky_object_t * root_object_pointer,
                             sky_object_t   new_object_reference,
                             sky_bool_t     global);


/** Assign an object reference to a memory location that is not an object root.
  * Because the C compiler is unaware of anything going on with respect to
  * garbage collection, object pointer assignments must be done through a
  * "write barrier" so that the garbage collector can safely track them. For
  * memory locations that should be treated as roots, use
  * sky_object_gc_setroot(), but use sky_object_gc_set() for all others.
  * A memory location that is not a root is a memory location that will be
  * visited by tracing object references starting at a root.
  *
  * @param[in]  object_pointer          the memory location that holds the
  *                                     non-root object reference.
  * @param[in]  new_object_reference    the new reference to assign to the
  *                                     memory location.
  * @param[in]  referencing_object      the object instance that is holding the
  *                                     reference to @a new_object_reference.
  *                                     May be @c NULL, in which case the
  *                                     reference is considered ephemeral. This
  *                                     is very dangerous to do if in fact the
  *                                     reference is not actually ephemeral.
  */
SKY_EXTERN void
sky_object_gc_set(sky_object_t *object_pointer,
                  sky_object_t  new_object_reference,
                  sky_object_t  referencing_object);


/** Assign an object reference to a memory location that is not an object root,
  * but only if it contains an expected object reference.
  * Like sky_object_gc_set(), except that the memory location is examined to
  * ensure that it contains the expected object reference. If it does not, no
  * action will be taken. If it does, the new object reference will be stored.
  * The compare and set is done atomically (via sky_atomic_cas()). Note that
  * even if the new object reference isn't stored, it may be converted to a
  * concrete and/or global object. The conversion to global, in particular,
  * must be done before the object becomes globally visible.
  *
  * @param[in]  object_pointer              the memory location that holds the
  *                                         non-root object reference.
  * @param[in]  expected_object_reference   the expected reference to check for.
  * @param[in]  new_object_reference        the new reference to assign to the
  *                                         memory location.
  * @param[in]  referencing_object          the object instance that is holding
  *                                         the reference to
  *                                         @a new_object_reference. May be
  *                                         @c NULL, in which case the
  *                                         reference is considered ephemeral.
  *                                         This is very dangerous to do if in
  *                                         fact the reference is not actually
  *                                         ephemeral.
  * @return     @c SKY_TRUE if @a new_object_reference was stored, or
  *             @c SKY_FALSE if it was not.
  */
SKY_EXTERN sky_bool_t
sky_object_gc_cas(sky_object_t *object_pointer,
                  sky_object_t  expected_object_reference,
                  sky_object_t  new_object_reference,
                  sky_object_t  referencing_object);


/** Visit all objects in an array as if sky_object_gc_set() were called for
  * each one.
  *
  * @param[in]  nobjects                the number of objects in the array.
  * @param[in]  objects                 the array of objects.
  * @param[in]  referencing_object      the object instance that is holding the
  *                                     reference to @a new_object_reference.
  *                                     May be @c NULL, in which case the
  *                                     reference is considered ephemeral. This
  *                                     is very dangerous to do if in fact the
  *                                     reference is not actually ephemeral.
  */
SKY_EXTERN void
sky_object_gc_setarray(ssize_t          nobjects,
                       sky_object_t *   objects,
                       sky_object_t     referencing_object);


/** The signature a function to call when a weak reference is cleared.
  * When a weak reference is cleared (because there are no longer any strong
  * references), a call to a function with this signature can be made. The
  * object that no longer exists is, for all intents and purposes, no longer
  * valid; therefore, it is not passed. An optional argument, @a arg, can be
  * passed when creating the weak reference to be passed to the callback.
  *
  * @param[in]  arg     the optional argument that was passed with the callback
  *                     function pointer when the weak reference was created.
  */
typedef void
        (*sky_object_weakref_callback_t)(sky_object_t arg);


/** Add a reference to an object to be cleared when the object is finalized.
  * This is used primarily to implement weak referencing; however, it can also
  * be useful for other things as well. The given memory location is added to
  * the object's list of references. When the object is finalized, every memory
  * location in the object's list of references will be set to @c NULL, and an
  * optional function may be called.
  *
  * @param[in]  object_pointer          the memory location that holds the
  *                                     weak reference. This must not be
  *                                     @c NULL, and the memory to which it
  *                                     points must be properly initialized.
  * @param[in]  new_object_reference    the new object reference to store. May
  *                                     be @c NULL to clear an existing weak
  *                                     reference.
  * @param[in]  referencing_object      the object instance that is holding the
  *                                     reference to @a new_object_reference.
  *                                     May be @c NULL, in which case the
  *                                     reference is considered ephemeral. This
  *                                     is very dangerous to do if in fact the
  *                                     reference is not actually ephemeral.
  * @param[in]  callback                an optional function pointer that will
  *                                     be called when the object reference is
  *                                     cleared. May be @c NULL.
  * @param[in]  arg                     an optional argument to be passed to
  *                                     @a callback when it is called. This is
  *                                     a strong reference! May be @c NULL.
  *                                     This is completely ignored if
  *                                     @a callback is @c NULL.
  */
SKY_EXTERN void
sky_object_gc_setweakref(sky_object_t *                 object_pointer,
                         sky_object_t                   new_object_reference,
                         sky_object_t                   referencing_object,
                         sky_object_weakref_callback_t  callback,
                         sky_object_t                   arg);


/** Initiate a new garbage collection cycle.
  * If this is called while a garbage collection cycle is in progress, it will
  * have no effect. Garbage collection cycle start requests do not accumulate.
  */
SKY_EXTERN void
sky_object_gc_collect(void);


/** Set the threshold for triggering an automatic garbage collection cycle.
  * Garbage collection cycles can be triggered automatically by tracking object
  * allocation counts. The trigger is not necessarily precise. The default
  * threshold is 100000.
  *
  * @param[in]  threshold   the approximate number of object allocations before
  *                         triggering a garbage collection cycle. A value of
  *                         0 disables automatic garbage collection.
  */
SKY_EXTERN void
sky_object_gc_setthreshold(uint32_t threshold);


/** Return the threshold set for triggering an automatic garbage collection
  * cycle.
  */
SKY_EXTERN uint32_t
sky_object_gc_getthreshold(void);


/** Disable automatic garbage collection. **/
SKY_EXTERN void
sky_object_gc_disable(void);

/** Enable automatic garbage collection. **/
SKY_EXTERN void
sky_object_gc_enable(void);

/** Determine whether automatic garbage collection is enabled. **/
SKY_EXTERN sky_bool_t
sky_object_gc_isenabled(void);


/** Return the number of garbage collection cycles that have completed. **/
SKY_EXTERN uintmax_t
sky_object_gc_count(void);


/** Determine whether garbage collection is running for multiple threads.
  * When an application first calls sky_library_initialize(), the garbage
  * collector is initialized for running with a single active thread. If at any
  * time another thread makes calls that hook into the garbage collector in any
  * way, the garbage collector becomes multi-threaded, and will stay that way
  * until sky_library_finalize() finalizes it. This function does not indicate
  * whether a garbage collection cycle is in progress.
  *
  * @return     @c SKY_TRUE if the garbage collector is running for multiple
  *             threads, or @c SKY_FALSE if it is running for a single thread.
  */
SKY_EXTERN sky_bool_t
sky_object_gc_isthreaded(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_OBJECT_H__ */

/** @} **/
