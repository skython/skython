#ifndef __SKYTHON_CORE_SKY_PRIVATE_H__
#define __SKYTHON_CORE_SKY_PRIVATE_H__ 1


#include "skython.h"

#include "../config.h"

#if defined(HAVE_LIMITS_H)
#   include <limits.h>
#endif

#include <assert.h>
#include <fcntl.h>
#include <pthread.h>
#if defined(HAVE_SIGNAL_H)
#   include <signal.h>
#endif

#include "sky_atfork.h"
#include "sky_error_private.h"
#include "sky_hazard_pointer_private.h"
#include "sky_tls_private.h"
#include "sky_tlsdata.h"

#include "sky_pointer_set.h"

#include "sky_object_private.h"
#include "sky_type_private.h"


SKY_CDECLS_BEGIN


#define SKY_TOKEN_PASTE(a, b)           a##b
#define SKY_TOKEN_PASTE_EXPANDED(a, b)  SKY_TOKEN_PASTE(a, b)


#if defined(__GNUC__) || defined(__clang__)
extern void sky_library_construct(void) __attribute__((constructor));
extern void sky_library_destruct(void)  __attribute__((destructor));
#elif defined(_WIN32)
extern void sky_library_construct(void);
extern void sky_library_destruct(void);
#else
#   error implementation missing
#endif


/* Exposed for sky_module__warnings.c */
extern sky_dict_t   sky_error_warn__onceregistry;
extern sky_list_t   sky_error_warn_filters;
extern sky_string_t sky_error_warn__defaultaction;


/* Core malloc package initialization and finalization, called from the
 * library constructor and destructor, respectively.
 */
extern void sky_core_malloc_initialize(void);
extern void sky_core_malloc_finalize(void);


/* Fork handlers. */
extern void sky_format_atfork_child(void);
extern void sky_signal_atfork_child(void);


/* Module initializers called from sky_library_initialize() */
extern void sky_codec_initialize_library(unsigned int flags);
extern void sky_error_warn_initialize_library(unsigned int flags);
extern void sky_file_initialize_library(unsigned int flags);
extern void sky_object_initialize_library(unsigned int flags);
extern void sky_python_ast_initialize_library(unsigned int flags);
extern void sky_python_symtable_initialize_library(unsigned int flags);
extern void sky_signal_initialize_library(unsigned int flags);

extern void sky_interpreter_initialize_library(const char * argv0,
                                               int          argc,
                                               char *       argv[],
                                               unsigned int flags);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PRIVATE_H__ */
