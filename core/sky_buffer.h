/** @file
  * @brief Direct memory access to object data.
  * This is an implementation of PEP 3118. Much of the included documentation
  * here is taken directly from that PEP, which is in the public domain.
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_buffer Buffer Protocol
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_BUFFER_H__
#define __SKYTHON_CORE_SKY_BUFFER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The maximum number of dimensions that a buffer may hold. **/
#define SKY_BUFFER_MAX_NDIM             64


/** Constants that may be combined together to make up the @c flags argument
  * in calls to sky_buffer_acquire().
  */
typedef enum sky_buffer_flag_e {
    /** This is the default flag state. The returned buffer may or may not have
      * writable memory. The format will be assumed to be unsigned bytes. This
      * is a "stand-alone" flag constant, meaning that it never needs to be
      * combined to the others. The exporter will raise an error if it cannot
      * provide such a contiguous buffer of bytes.
      */
    SKY_BUFFER_FLAG_SIMPLE              =   0,

    /** The returned buffer must be writable. If it is not writable, then raise
      * an error.
      */
    SKY_BUFFER_FLAG_WRITABLE            =   0x0001,

    /** The returned buffer must have true format information if this flag is
      * provided. This would be used when the consumer is going to be checking
      * for what "kind" of data is actually stored. An exporter should always
      * be able to provide this information if requested. If format is not
      * explicitly requested, then the format must be returned as @c NULL
      * (which means "B" or unsigned bytes).
      */
    SKY_BUFFER_FLAG_FORMAT              =   0x0004,

    /** The returned buffer must provide shape information. The memory will be
      * assumed C-style contiguous (last dimension varies the fastest). The
      * exporter may raise an error if it cannot provide this kind of
      * contiguous buffer. If this is not given then the @c shape field in the
      * @c sky_buffer_t structure will be @c NULL.
      */
    SKY_BUFFER_FLAG_ND                  =   0x0008,

    /** The returned buffer must provide strides information. This would be
      * used when the consumer can handle strided, discontiguous arrays.
      * Handling strides automatically assumes you can handle shape. The
      * exporter may raise an error if it cannot provide a strided-only
      * representation of the data (i.e., without the suboffsets). This flag
      * implies @c SKY_BUFFER_FLAG_ND.
      */
    SKY_BUFFER_FLAG_STRIDES             =   (0x0010 | SKY_BUFFER_FLAG_ND),

    /** Indicates that the returned buffer must be C-style contiguous (last
      * dimension varies the fastest). This flag implies
      * @c SKY_BUFFER_FLAG_STRIDES.
      */
    SKY_BUFFER_FLAG_C_CONTIGUOUS        =   (0x0020 | SKY_BUFFER_FLAG_STRIDES),

    /** Indicates that the returned buffer must be Fortran-style contiguous
      * (first dimension varies the fastest). This flag implies
      * @c SKY_BUFFER_FLAG_STRIDES.
      */
    SKY_BUFFER_FLAG_F_CONTIGUOUS        =   (0x0040 | SKY_BUFFER_FLAG_STRIDES),

    /** Indicates the the returned buffer may be either C-style contiguous or
      * Fortran-style contiguous. This flag implies @c SKY_BUFFER_FLAG_STRIDES.
      */
    SKY_BUFFER_FLAG_ANY_CONTIGUOUS      =   (0x0080 | SKY_BUFFER_FLAG_STRIDES),

    /** The returned buffer must have suboffsets information, which may be
      * @c NULL if no suboffsets are needed. This would be used when the
      * consumer can handle indirect array referencing implied by these
      * suboffsets. This flag implies @c SKY_BUFFER_FLAG_STRIDES.
      */
    SKY_BUFFER_FLAG_INDIRECT            =   (0x0100 | SKY_BUFFER_FLAG_STRIDES),

    /** Writable multi-dimensional (but contiguous) **/
    SKY_BUFFER_FLAG_CONTIG              =   (SKY_BUFFER_FLAG_ND |
                                             SKY_BUFFER_FLAG_WRITABLE),
    /** Readonly multi-dimensional (but contiguous) **/
    SKY_BUFFER_FLAG_CONTIG_RO           =   SKY_BUFFER_FLAG_ND,

    /** Writable multi-dimensional using strides but aligned **/
    SKY_BUFFER_FLAG_STRIDED             =   (SKY_BUFFER_FLAG_STRIDES |
                                             SKY_BUFFER_FLAG_WRITABLE),
    /** Readonly multi-dimensional using strides but aligned **/
    SKY_BUFFER_FLAG_STRIDED_RO          =   SKY_BUFFER_FLAG_STRIDES,

    /** Writable multi-dimensional using strides and not necessarily aligned. **/
    SKY_BUFFER_FLAG_RECORDS             =   (SKY_BUFFER_FLAG_STRIDES |
                                             SKY_BUFFER_FLAG_FORMAT |
                                             SKY_BUFFER_FLAG_WRITABLE),

    /** Readonly multi-dimensional using strides and not necessarily aligned. **/
    SKY_BUFFER_FLAG_RECORDS_RO          =   (SKY_BUFFER_FLAG_STRIDES |
                                             SKY_BUFFER_FLAG_FORMAT),

    /** Writable multi-dimensional using sub-offsets **/
    SKY_BUFFER_FLAG_FULL                =   (SKY_BUFFER_FLAG_INDIRECT |
                                             SKY_BUFFER_FLAG_FORMAT |
                                             SKY_BUFFER_FLAG_WRITABLE),

    /** Readonly multi-dimensional using sub-offsets **/
    SKY_BUFFER_FLAG_FULL_RO             =   (SKY_BUFFER_FLAG_INDIRECT |
                                             SKY_BUFFER_FLAG_FORMAT),
} sky_buffer_flag_t;


/** A structure representing a view into the memory of an object.
  * This structure is filled in by types implementing the buffer protocol when
  * a buffer is acquired via sky_buffer_acquire(). This same structure must be
  * later released via sky_buffer_release() when it is no longer needed.
  */
typedef struct sky_buffer_s {
    /** A pointer to the start of the memory for the object. **/
    void *                              buf;
    /** The total bytes of memory the object uses. This should be the same as
      * the product of the shape array multiplied by the number of bytes per
      * item of memory.
      */
    ssize_t                             len;
    /** @c SKY_TRUE if the memory is readonly, or @c SKY_FALSE if it is
      * writable.
      */
    sky_bool_t                          readonly;
    /** A @c NULL-terminated format string indicating what is in each element
      * of memory. The number of elements is @c len / @c itemsize, where
      * @c itemsize is the number of bytes implied by the format. This can be
      * @c NULL, which implies standard unsigned bytes ("B").
      */
    const char *                        format;
    /** The number of dimensions the memory represents, which must be >= 0. A
      * value of 0 means that @c shape, @c strides, and @c suboffsets must all
      * be @c NULL (i.e., the memory represents a scalar).
      */
    ssize_t                             ndim;
    /** An array of @c ssize_t length @c ndim that indicates the shape of the
      * memory as an N-D array. If @c ndim is 0 (indicating a scalar), then
      * this must be @c NULL.
      */
    ssize_t *                           shape;
    /** An array of @c ssize_t length @c ndim that indicates the number of
      * bytes to skip to get to the next element in each dimension. If this is
      * not requested (@c SKY_BUFFER_FLAG_STRIDES is not set for the call to
      * sky_buffer_acquire()), then this should be set to @c NULL, which
      * indicates a C-style contiguous array, or @c sky_BufferError should be
      * raised if this is not possible.
      */
    ssize_t *                           strides;
    /** An array of @c ssize_t length @c ndim that indicates how many bytes
      * should be added to each element of the memory before dereferencing.
      * A suboffset value that is negative indicates that no dereferencing
      * should occur (striding in a contiguous memory block). If all suboffsets
      * are negative, then this must be @c NULL. If this is not requested
      * (@c SKY_BUFFER_FLAG_INDIRECT is not set), then this should be set to
      * @c NULL, or @c sky_BufferError should be raised if this is not
      * possible.
      */
    ssize_t *                           suboffsets;
    /** The size of each element of the memory in bytes. **/
    ssize_t                             itemsize;
    /** An arbitrary pointer that may be used by the object implementing the
      * buffer protocol. This should never be modified except by the object
      * implementing the buffer protocol.
      */
    void *                              internal;
    /** This holds a reference to the object for which this structure is a
      * view. This reference is set by a normal pointer assignment. It is up
      * to the caller to note the store for garbage collection purposes if the
      * reference is to exist across calls to sky_object_gc().
      */
    sky_object_t                        object;
} sky_buffer_t;


/** The signature of a function called to acquire a buffer for an object.
  *
  * @param[in]      object  the object for which a buffer view is to be
  *                         acquired.
  * @param[in]      data    the object's instance data for the type in the MRO
  *                         for which the acquire function is being called.
  * @param[in,out]  buffer  a buffer structure to be filled in.
  * @param[in]      flags   flags describing the type of buffer requested.
  */
typedef void
        (*sky_buffer_acquire_t)(sky_object_t    object,
                                void *          data,
                                sky_buffer_t *  buffer,
                                unsigned int    flags);

/** The signature of a function called to release a buffer for an object.
  *
  * @param[in]  object      the object for which the buffer was acquired.
  * @param[in]  data        the object's instance data for the type in the MRO
  *                         for which the release function is being called.
  * @param[in]  buffer      the buffer to release.
  */
typedef void
        (*sky_buffer_release_t)(sky_object_t    object,
                                void *          data,
                                sky_buffer_t *  buffer);


/** Acquire a new buffer for an object.
  *
  * @param[out] buffer  a sky_buffer_t structure to be filled in with the
  *                     memory view of @a object.
  * @param[in]  object  the object for which a buffer is to be acquired.
  * @param[in]  flags   flags describing the type of buffer requested.
  */
SKY_EXTERN void
sky_buffer_acquire(sky_buffer_t *   buffer,
                   sky_object_t     object,
                   unsigned int     flags);


/** Acquire a new buffer for an object that is C-contiguous.
  * This is a convenience function that is typically used to get a C-contiguous
  * buffer for an object as an argument to function that is likely to be using
  * the buffer with an external C function that is expecting a buffer of bytes.
  * The buffer bytes may or may not be terminated with a @c NUL byte.
  *
  * @param[out] buffer  a sky_buffer_t structure to be filled in with the
  *                     memory view of @a object.
  * @param[in]  object  the object for which a buffer is to be acquired.
  * @param[in]  flags   flags describing the type of buffer requested.
  * @param[in]  source  a descriptive string of object that may be @c NULL,
  *                     in which case @a message is used instead.
  * @param[in]  message a message to use for errors acquiring the buffer, or
  *                     @c NULL to construct a message using @a source.
  */
SKY_EXTERN void
sky_buffer_acquirecbuffer(sky_buffer_t *buffer,
                          sky_object_t  object,
                          unsigned int  flags,
                          const char *  source,
                          const char *  message);


/** Check whether an object implements the buffer protocol.
  *
  * @param[in]  object      the object to query.
  * @return     @c SKY_TRUE if the object supports the buffer protocol, or
  *             @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_buffer_check(sky_object_t object);


/** Copy a source buffer to a target buffer.
  * The target buffer must be initialized with a memory pointer to receive the
  * copied data, as well as shape, stride, and suboffsets information to
  * control the shape of the copy. Source and target formats and shapes must be
  * equivalent for the copy to be possible. Formats are considered equivalent
  * if the itemsizes are the same and the format strings are identical. Shapes
  * are considered equivalent if they are either equal or identical up to a
  * zero element at the same position. For example, in NumPy arrays having the
  * shapes [1, 0, 5] and [1, 0, 7] are equivalent.
  *
  * @param[in]  target_buffer   the target buffer.
  * @param[in]  source_buffer   the source buffer.
  */
SKY_EXTERN void
sky_buffer_copy(sky_buffer_t *      target_buffer,
                const sky_buffer_t *source_buffer);


/** Compare two buffers for equality.
  * Two buffers can be considered equal if their formats, shapes, and contents
  * are equivalent.
  *
  * @param[in]  buffer_1    the first buffer to compare.
  * @param[in]  buffer_2    the second buffer to compare.
  * @return     @c SKY_TRUE if the two buffers are equal, or @c SKY_FALSE if
  *             are not.
  */
SKY_EXTERN sky_bool_t
sky_buffer_equal(const sky_buffer_t *   buffer_1,
                 const sky_buffer_t *   buffer_2);


/** Release a previously acquired buffer for an object.
  *
  * @param[in]  buffer      the buffer to release.
  */
SKY_EXTERN void
sky_buffer_release(sky_buffer_t *buffer);


/** Initialize a buffer view structure.
  * This is a convenience function that may be used by object types that are
  * implementing the buffer protocol. It is most useful for object types that
  * are capable of exporting a simple buffer layout consisting of unstructured
  * contiguous bytes.
  *
  * @param[in]  buffer      the buffer view structure to be initialized.
  * @param[in]  bytes       a pointer to the bytes to be exposed.
  * @param[in]  nbytes      the size of @a bytes.
  * @param[in]  readonly    @c SKY_TRUE if @a bytes is read-only.
  * @param[in]  flags       the set of flags passed to sky_buffer_acquire().
  */
SKY_EXTERN void
sky_buffer_initialize(sky_buffer_t *buffer,
                      void *        bytes,
                      ssize_t       nbytes,
                      sky_bool_t    readonly,
                      unsigned int  flags);


/** Determine whether a buffer is contiguous for a specific order.
  * Supported orders are 'A' for any, 'C' for C-contiguous, and 'F' for Fortran
  * contiguous.
  *
  * @param[in]  buffer      the buffer to test.
  * @param[in]  order       the order to set.
  * @return     @c SKY_TRUE if @a buffer is contiguous for the order @a order.
  */
SKY_EXTERN sky_bool_t
sky_buffer_iscontiguous(const sky_buffer_t *buffer,
                        int                 order);


/** Return a pointer to buffer memory for a specified offset.
  *
  * @param[in]  buffer      the buffer to index.
  * @param[in]  offset      the offset of the desired pointer.
  * @return     a pointer to buffer memory for the specified offset.
  */
SKY_EXTERN void *
sky_buffer_pointer(const sky_buffer_t *buffer, ssize_t offset);


/** Convert a buffer to a contiguous representation.
  * A properly sized buffer must be supplied to hold the contiguous data, which
  * must be at least as large as the original data. It is the responsibility of
  * the caller to ensure that the underlying data is not changing while the
  * copy is made; otherwise, the results are unpredictable. If the buffer is
  * already arranged in the requested order, an exact copy will be made.
  *
  * @param[in]  buffer      the source buffer.
  * @param[in]  bytes       an array of bytes to receive the contiguous data.
  * @param[in]  nbytes      the size of @a bytes, used only as a safeguard to
  *                         ensure that enough space is available.
  * @param[in]  order       the order to arrange the resulting data into, which
  *                         must be one of 'C' for C-contiguous layout, 'F' for
  *                         Fortran contiguous layout, or 'A' for any (the same
  *                         as 'C').
  */
SKY_EXTERN void
sky_buffer_tocontiguous(const sky_buffer_t *buffer,
                        void *              bytes,
                        size_t              nbytes,
                        int                 order);


/** Construct a full buffer representation from a buffer.
  * If a buffer does not contain full information (i.e., it was not created
  * with @c SKY_BUFFER_FLAG_FULL), the information can still be constructed
  * from other information in the buffer that will be present. This function
  * will allocate a new @c sky_buffer_t structure and initialize it to hold
  * full information from the original source buffer. Note that only one of
  * the two buffers should be released via sky_buffer_release(). The newly
  * created buffer is not acquired via sky_buffer_acquire().
  *
  * @param[in]  buffer      the source buffer.
  * @return     a newly allocated buffer structure containing full information
  *             constructed from the original buffer. The caller should release
  *             the buffer using sky_free(), not sky_buffer_release().
  */
SKY_EXTERN sky_buffer_t *
sky_buffer_tofull(const sky_buffer_t *buffer);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BUFFER_H__ */

/** @} **/
/** @} **/
