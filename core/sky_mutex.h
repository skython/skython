/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_mutex Mutual Exclusion Locks
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MUTEX_H__
#define __SKYTHON_CORE_SKY_MUTEX_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_time.h"


SKY_CDECLS_BEGIN


/** Create a new mutex object instance.
  *
  * @param[in]  reentrant   a re-entrant mutex object may be locked multiple
  *                         times by the same thread. For each lock, there
  *                         must be a matching unlock.
  * @param[in]  strict      a strict mutex object raises errors when the
  *                         locking thread attempts to lock the mutex again for
  *                         a non-re-entrant mutex, and if any thread other
  *                         than the locking thread attempts to unlock the
  *                         mutex.
  * @return     a new mutex object instance.
  */
SKY_EXTERN sky_mutex_t
sky_mutex_create(sky_bool_t reentrant, sky_bool_t strict);


/** Determine if a mutex is locked.
  *
  * @param[in]  self    the mutex to query.
  * @return     @c SKY_TRUE if the mutex is locked, or @c SKY_FALSE if it is
  *             not.
  */
SKY_EXTERN sky_bool_t
sky_mutex_islocked(sky_mutex_t self);


/** Determine if the calling thread is the owner of a mutex.
  *
  * @param[in]  self    the mutex to query.
  * @return     @c SKY_TRUE if the mutex is locked by the calling thread, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_mutex_isowned(sky_mutex_t self);


/** Lock a mutex.
  *
  * @param[in]  self    the mutex to lock.
  * @param[in]  timeout how long to wait to acquire the mutex.
  * @return     @c SKY_TRUE if the lock was acquired, or @c SKY_FALSE if it
  *             was not (due to timeout).
  */
SKY_EXTERN sky_bool_t
sky_mutex_lock(sky_mutex_t self, sky_time_t timeout);


/** Unlock a mutex.
  *
  * @param[in]  self    the mutex to unlock.
  */
SKY_EXTERN void
sky_mutex_unlock(sky_mutex_t self);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MUTEX_H__ */

/** @} **/
/** @} **/
