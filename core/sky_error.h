/** @file
  * @brief Error Handling
  * @defgroup sky_error Error Handling
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ERROR_H__
#define __SKYTHON_CORE_SKY_ERROR_H__ 1


#include "sky_base.h"
#include "sky_malloc.h"
#include "sky_object.h"

#include <errno.h>


SKY_CDECLS_BEGIN


/** Record of an exceptional condition as raised via sky_error_raise_object().
  */
typedef struct sky_error_record_s {
    /** The type of the exception that was raised. **/
    sky_type_t                          type;
    /** The value associated with the exception that was raised. **/
    sky_object_t                        value;
    /** The interpreter traceback for the raised exception. **/
    sky_traceback_t                     traceback;
    /** The C source function where the exception was initially raised. **/
    const char *                        function;
    /** The C source file where the exception was initially raised. **/
    const char *                        filename;
    /** The C line number where the exception was initially raised. **/
    uint32_t                            lineno;
    /** The number of references held on the record (internal use). **/
    uint32_t                            refs;
} sky_error_record_t;   /* sizeof: 28/48 */


/** Abort program execution when a fatal error occurs.
  * The specified message is written to @c stderr, and program execution is
  * aborted. The message may be @c NULL, in which case execution is simply
  * aborted without displaying any message of any kind.
  *
  * @param[in] message  the message to display immediately prior to aborting
  *                     program execution. May be @c NULL if no message is to
  *                     be displayed.
  */
#define sky_error_fatal(message) \
    sky_error_fatal_(message, __FUNCTION__, __FILE__, __LINE__)


/** Abort program execution if the specified expression's truth value is false.
  * This function behaves much like an assert would behave. The difference
  * is that the expression is always evaluated, regardless of whether Skython
  * is built with assertions enabled or disabled. Its purpose is to trap
  * common programming errors.
  *
  * @param[in]  expr    the expression to be evaluated. If the expression
  *                     evaluates false, sky_error_fatal() will be called with
  *                     the string representation of the expression as the
  *                     message to displayed.
  */
#define sky_error_validate(expr) \
    do { if (unlikely(!(expr))) { sky_error_fatal(#expr); } } while (0)

/** Abort program execution if the specified expression's truth value is false,
  * but only perform the check in a debug build.
  */
#if !defined(NDEBUG)
#   define sky_error_validate_debug(expr) \
        do { if (unlikely(!(expr))) { sky_error_fatal(#expr); } } while (0)
#else
#   define sky_error_validate_debug(expr) \
        do { } while (0)
#endif


/** Raise an exception.
  * The current thread of execution will stop, and the stack will be unwound to
  * find an exception handler to handle the exception that was raised. If an
  * appropriate exception handler cannot be found, the exception is uncaught,
  * and the application will terminate immediately via a call to
  * sky_error_fatal().
  *
  * @param[in]  type    the exception to raise. It must be a type, and it must
  *                     be derived from @c sky_BaseException.
  * @param[in]  value   the value to associate with the exception, often a
  *                     string.
  */
#define sky_error_raise_object(type, value) \
    sky_error_raise_(type, value, __FUNCTION__, __FILE__, __LINE__)

/** Raise an exception.
  * The current thread of execution will stop, and the stack will be unwound to
  * find an exception handler to handle the exception that was raised. If an
  * appropriate exception handler cannot be found, the exception is uncaught,
  * and the application will terminate immediately via a call to
  * sky_error_fatal().
  *
  * @param[in]  type    the exception to raise. It must be a type, and it must
  *                     be derived from @c sky_BaseException.
  * @param[in]  string  the string to associate with the exception, which is
  *                     expected to be UTF-8 encoded.
  */
#define sky_error_raise_string(type, string)                                \
    sky_error_raise_(type,                                                  \
                     sky_string_createfrombytes(string, strlen(string),     \
                                                NULL, NULL),                \
                     __FUNCTION__, __FILE__, __LINE__)

/** Raise an exception.
  * The current thread of execution will stop, and the stack will be unwound to
  * find an exception handler to handle the exception that was raised. If an
  * appropriate exception handler cannot be found, the exception is uncaught,
  * and the application will terminate immediately via a call to
  * sky_error_fatal().
  *
  * @param[in]  type    the exception to raise. It must be a type, and it must
  *                     be derived from @c sky_BaseException.
  * @param[in]  format  the string to associate with the exception, which is
  *                     expected to be UTF-8 encoded.
  */
#define sky_error_raise_format(type, format, ...)                           \
    sky_error_raise_(type,                                                  \
                     sky_string_createfromformat(format, __VA_ARGS__),      \
                     __FUNCTION__, __FILE__, __LINE__)

/** Raise an exception converted from an errno error code.
  * Raises a @c sky_OSError exception (or a more appropriate sub-type of
  * @c sky_OSError).
  *
  * @param[in]  type    the exception to raise. It must be a type, and it must
  *                     be derived from @c sky_BaseException.
  * @param[in]  errno   the errno value to use.
  */
#define sky_error_raise_errno(type, errno) \
    sky_error_raise_errno_(type, errno, __FUNCTION__, __FILE__, __LINE__)

#if defined(_WIN32)
/** Raise an exception convert from a Windows error code.
  * Raises an appropriate exception based on a Windows error code.
  *
  * @param[in]  win32_code  the Windows error code.
  */
#define sky_error_raise_win32(win32_code) \
    sky_error_raise_win32_(win32_code, __FUNCTION__, __FILE__, __LINE__)
#endif


/** Reraise the current exception.
  * This function should only ever be called from within an exception handler
  * block. It is a fatal error to attempt to reraise the current exception when
  * there is no current exception. The current exception handler will exit,
  * and the stack will continue to be unwound to find an appropriate exception
  * handler. If an appropriate exception handler cannot be found, the exception
  * is uncaught, and the application will terminate immediately via a call to
  * sky_error_fatal().
  */
SKY_EXTERN SKY_NORETURN void
sky_error_raise(void);

/** Raise an exception using a specific error record. **/
SKY_EXTERN SKY_NORETURN void
sky_error_raise_record(sky_error_record_t *record);

/** Clear the most recent error.
  * This function will only clear the most recent error on the stack. There may
  * be others below it. No asset clean up is done.
  *
  * @return     @c SKY_TRUE if there was an error cleared, or @c SKY_FALSE if
  *             there was not.
  */
SKY_EXTERN sky_bool_t
sky_error_clear(void);

/** Retrieve the current error record.
  * If there is a current exception, which can only be true when code in an
  * exception handler catch block is executing, a pointer to the error record
  * will be returned. If there is no current error, @c NULL will be returned.
  * This function does not increase the error record's reference count.
  *
  * @return     a pointer to the current error record if there is a current
  *             exception; otherwise, @c NULL.
  */
SKY_EXTERN sky_error_record_t *
sky_error_current(void);

/** Create a new error record.
  *
  * @param[in]  type        the type for the exception.
  * @param[in]  value       the value for the exception.
  * @param[in]  traceback   the traceback for the exception.
  * @param[in]  function    the C source function name for the exception.
  * @param[in]  filename    the C source filename for the exception.
  * @param[in]  lineno      the C source line number for the exception.
  * @return     a new error record structure, which must eventually be released
  *             using sky_error_record_release().
  */
SKY_EXTERN sky_error_record_t *
sky_error_record_create(sky_type_t      type,
                        sky_object_t    value,
                        sky_traceback_t traceback,
                        const char *    function,
                        const char *    filename,
                        uint32_t        lineno);


/** Release a reference on an error record.
  * The error record will be properly destroyed when its reference count
  * reaches zero. This function should only be called if a reference has
  * been explicitly added. Note that sky_error_current() does not increase
  * the returned record's reference count.
  *
  * @param[in]  record      the reference to release.
  */
SKY_EXTERN void
sky_error_record_release(sky_error_record_t *record);

/** Retain a reference on an error record.
  *
  * @param[in]  record      the reference to retain.
  */
SKY_EXTERN void
sky_error_record_retain(sky_error_record_t *record);

/** Normalize an error record. **/
SKY_EXTERN void
sky_error_record_normalize(sky_error_record_t *record);

/** Handle an exception by displaying it with a traceback on sys.stderr.
  *
  * @param[in]  record      the error record to display.
  */
SKY_EXTERN void
sky_error_display(sky_error_record_t *record);

/** Begin a new exception handler block.
  * A new exception handler block will be started, meaning that the code
  * contained between the @c SKY_ERROR_TRY and exception handler blocks will be
  * executed through to its end unless an exception is raised, in which case
  * execution will continue at the appropriate exception handler block. If no
  * exception is raised, the exception handler blocks will be skipped, and
  * execution will continue at @c SKY_ERROR_TRY_END. @c SKY_ERROR_TRY contains
  * an implicit @c SKY_ASSET_BLOCK_BEGIN, so assets may be saved within it.
  */
#define SKY_ERROR_TRY                                                       \
    do {                                                                    \
        int                 __setjmp_value;                                 \
        sky_error_block_t   __try_block;                                    \
                                                                            \
        sky_error_block_push(&__try_block, SKY_ERROR_BLOCK_TYPE_TRY);       \
        __setjmp_value = sky_setjmp(__try_block.setjmp_context);            \
        if (SKY_ERROR_SETJMP_TRY == __setjmp_value) {                       \
            do {


/** Begin a block of code to handle raised exceptions of a specified type
  * within an exception handler block.
  * When an exception is raised, execution will be transferred to the block of
  * code started with SKY_ERROR_EXCEPT() if the exception type matches @a type.
  * From within the exception block, it's legal to use sky_error_raise() to
  * re-raise the current exception. If the error is not re-raised, execution
  * will fall through to @c SKY_ERROR_TRY_END, at which point the whole
  * exception handler block's assets will be cleaned up.
  */
#define SKY_ERROR_EXCEPT(exc_type)                                          \
            } while (0);                                                    \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_NONE;             \
        }                                                                   \
        if (SKY_ERROR_SETJMP_RAISE == __setjmp_value &&                     \
            SKY_ERROR_BLOCK_TYPE_TRY == __try_block.block_type &&           \
            sky_type_issubtype(sky_error_current()->type, exc_type))        \
        {                                                                   \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_EXCEPT;           \
            do {


/** Begin a block of code to handle raised exceptions within an exception
  * handler block.
  * When an exception is raised, execution will be transferred to the block of
  * code started with @c SKY_ERROR_EXCEPT_ANY if no more specific error handler
  * has yet handled the exception. From within the exception block, it's legal
  * to use sky_error_raise() to re-raise the current exception. If the error is
  * not re-raised, execution will fall through to @c SKY_ERROR_TRY_END, at
  * which point the whole exception handler block's assets will be cleaned up.
  */
#define SKY_ERROR_EXCEPT_ANY                                                \
            } while (0);                                                    \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_NONE;             \
        }                                                                   \
        if (SKY_ERROR_SETJMP_RAISE == __setjmp_value &&                     \
            SKY_ERROR_BLOCK_TYPE_TRY == __try_block.block_type)             \
        {                                                                   \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_EXCEPT;           \
            do {

/** End an exception handler block.
  * The current exception handler block started with @c SKY_ERROR_TRY will be
  * ended, cleaning up all assets saved within the exception handler block as
  * appropriate.
  */
#define SKY_ERROR_TRY_END                                                   \
            } while (0);                                                    \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_NONE;             \
        }                                                                   \
        if (SKY_ERROR_SETJMP_RAISE == __setjmp_value &&                     \
            SKY_ERROR_BLOCK_TYPE_TRY == __try_block.block_type)             \
        {                                                                   \
            __try_block.block_type = SKY_ERROR_BLOCK_TYPE_EXCEPT;           \
            sky_error_raise();                                              \
        }                                                                   \
        sky_error_block_pop();                                              \
    } while (0)


/** Generate a simple warning.
  * Generate a warning with a message and category. The category must be a type
  * derived from @c sky_Warning, or @c NULL. The @a stacklevel argument is used
  * to figure out location information from the interpreter's call stack. This
  * API will eventually call sky_error_warn_explicit().
  *
  * @param[in]  message     the warning message, which should normally be a
  *                         string object. If the type is a warning type, the
  *                         category will be set to the type, and the message
  *                         to use will be determined by a call to the type's
  *                         message() method.
  * @param[in]  category    the category for the warning, which must be a type
  *                         derived from @c sky_Warning. May be @c NULL, in
  *                         which case @c sky_RuntimeWarning will be used.
  * @param[in]  stacklevel  the stack frame in the interpreter call stack to
  *                         use to find location information. Normally 1.
  */
SKY_EXTERN void
sky_error_warn(sky_object_t message, sky_type_t category, ssize_t stacklevel);

/** Generate a warning (full API)
  * Generate a warning. The @a message and @a category parameters are the same
  * as for sky_error_warn(). Instead of using the interpreter call stack to
  * determine location information, it must be specified explicitly via the
  * @a filename, @a lineno, and @a module parameters. The @a registry parameter
  * specifies a dict that's used to record instances of warnings to prevent
  * duplicate warnings at the same location.
  *
  * @param[in]  message         the warning message, which should normally be a
  *                             string object. If the type is a warning type,
  *                             the category will be set to the type, and the
  *                             message to use will be determined by a call to
  *                             the type's message() method.
  * @param[in]  category        the category for the warning, which must be a
  *                             type derived from @c sky_Warning. May be
  *                             @c NULL, in which case @c sky_RuntimeWarning
  *                             will be used.
  * @param[in]  filename        the filename for the source location of the
  *                             warning.
  * @param[in]  lineno          the line number for the source location of the
  *                             warning.
  * @param[in]  module          the module for the source location of the
  *                             warning.
  * @param[in]  registry        the registry to use for recording instances of
  *                             warnings to prevent duplicate warnings (unless
  *                             the filter action for the warning is "always").
  * @param[in]  module_globals  the global variables of the module. If not
  *                             @c NULL, they'll be used to attempt to get the
  *                             source line text to render with the warning.
  */
SKY_EXTERN void
sky_error_warn_explicit(sky_object_t    message,
                        sky_type_t      category,
                        sky_string_t    filename,
                        ssize_t         lineno,
                        sky_string_t    module,
                        sky_dict_t      registry,
                        sky_dict_t      module_globals);

/** Generate a warning.
  * This is a simple wrapper around sky_error_warn(). The message is
  * constructed from a format string and variable arguments to create a string
  * object. The stacklevel argument used is 1.
  *
  * @param[in]  category    the category for the warning, which must be a type
  *                         derived from @c sky_Warning. May be @c NULL, in
  *                         which case @c sky_RuntimeWarning will be used.
  * @param[in]  format      the format string for the message.
  * @param[in]  ...         variable arguments needed by the format string.
  */
#define sky_error_warn_format(category, format, ...)                        \
    sky_error_warn(sky_string_createfromformat(format, __VA_ARGS__),        \
                   category, 1)

/** Generate a warning.
  * This is a simple wrapper around sky_error_warn(). The stacklevel argument
  * used is 1. The @a message and @a category arguments are passed as-is.
  *
  * @param[in]  message     the warning message, which should normally be a
  *                         string object. If the type is a warning type, the
  *                         category will be set to the type, and the message
  *                         to use will be determined by a call to the type's
  *                         message() method.
  * @param[in]  category    the category for the warning, which must be a type
  *                         derived from @c sky_Warning. May be @c NULL, in
  *                         which case @c sky_RuntimeWarning will be used.
  */
#define sky_error_warn_string(category, message)                            \
    do {                                                                    \
        const char *_str = message;                                         \
                                                                            \
        sky_error_warn(sky_string_createfrombytes(_str, strlen(_str),       \
                                                  NULL, NULL),              \
                       category, 1);                                        \
    } while (0)


/** @cond **/

/* These are the types and functions used to implement error blocks and their
 * associated functionality. They must be exposed publicly in order to be able
 * to be used from the macros; however, they should not be used externally.
 * Consider everything beyond this point to be undocumented and for internal
 * use only.
 */

typedef struct sky_error_asset_s    sky_error_asset_t;
typedef struct sky_error_block_s    sky_error_block_t;

typedef enum sky_error_setmp_e {
    SKY_ERROR_SETJMP_TRY            =   0,
    SKY_ERROR_SETJMP_RAISE          =   1,
} sky_error_setjmp_t;

typedef enum sky_error_block_type_e {
    SKY_ERROR_BLOCK_TYPE_NONE       =   0,
    SKY_ERROR_BLOCK_TYPE_TRY        =   1,
    SKY_ERROR_BLOCK_TYPE_EXCEPT     =   2,
    SKY_ERROR_BLOCK_TYPE_ASSET      =   3,
} sky_error_block_type_t;

struct sky_error_block_s {
    sky_error_block_t *                 next;
    sky_error_asset_t *                 assets;
    sky_error_record_t *                error_record;

    sky_error_block_type_t              block_type;

    jmp_buf                             setjmp_context;
};

SKY_EXTERN SKY_NORETURN void
sky_error_fatal_(const char *   message,
                 const char *   function,
                 const char *   filename,
                 uint32_t       lineno);

SKY_EXTERN SKY_NORETURN void
sky_error_raise_(sky_type_t     type,
                 sky_object_t   value,
                 const char *   function,
                 const char *   filename,
                 uint32_t       lineno);

SKY_EXTERN SKY_NORETURN void
sky_error_raise_errno_(sky_type_t   type,
                       int          errno_value,
                       const char * function,
                       const char * filename,
                       uint32_t     lineno);

#if defined(_WIN32)
SKY_EXTERN SKY_NORETURN void
sky_error_raise_errno_(DWORD        win32_value,
                       const char * function,
                       const char * filename,
                       uint32_t     lineno);
#endif


SKY_EXTERN void
sky_error_block_pop(void);

SKY_EXTERN void
sky_error_block_push(sky_error_block_t *    block,
                     sky_error_block_type_t block_type);

/** @endcond **/


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ERROR_H__ */

/** @} **/
