#ifndef __SKYTHON_CORE_SKY_PYTHON_SYMTABLE_H__
#define __SKYTHON_CORE_SKY_PYTHON_SYMTABLE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_python_ast.h"


SKY_CDECLS_BEGIN


typedef struct sky_python_symtable_block_s *sky_python_symtable_block_t;
extern sky_type_t const sky_python_symtable_block_type;


typedef enum sky_python_symtable_block_type_e {
    SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION =   0,
    SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS    =   1,
    SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE   =   2,
} sky_python_symtable_block_type_t;


typedef enum sky_python_symtable_optimized_e {
    SKY_PYTHON_SYMTABLE_OPTIMIZED_NONE        = 0x0,
    SKY_PYTHON_SYMTABLE_OPTIMIZED_IMPORT_STAR = 0x1,
    SKY_PYTHON_SYMTABLE_OPTIMIZED_TOP_LEVEL   = 0x2,
} sky_python_symtable_optimized_t;


typedef enum sky_python_symtable_symbol_flag_e {
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_GLOBAL      =   0x0001,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL       =   0x0002,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM       =   0x0004,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_NONLOCAL    =   0x0008,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE         =   0x0010,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_FREE        =   0x0020,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_FREE_CLASS  =   0x0040,
    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_IMPORT      =   0x0080,
} sky_python_symtable_symbol_flag_t;


typedef enum sky_python_symtable_symbol_scope_e {
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_UNKNOWN            =   0x0,
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL              =   0x1,
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_EXPLICIT    =   0x2,
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_IMPLICIT    =   0x3,
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE               =   0x4,
    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL               =   0x5,

} sky_python_symtable_symbol_scope_t;
#define SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_MASK               0x7
#define SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_SHIFT              11


SKY_EXTERN_INLINE int
sky_python_symtable_symbol_isbound(intmax_t flags)
{
    return (flags & (SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL |
                     SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM |
                     SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_IMPORT));
}


SKY_EXTERN_INLINE sky_python_symtable_symbol_scope_t
sky_python_symtable_symbol_scope(intmax_t flags)
{
    return (sky_python_symtable_symbol_scope_t)
           ((flags >> SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_SHIFT) &
            SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_MASK);
}


SKY_EXTERN_INLINE void
sky_python_symtable_symbol_setscope(intmax_t *                          flags,
                                    sky_python_symtable_symbol_scope_t  scope)
{
    *flags = (*flags & ~(SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_MASK <<
                         SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_SHIFT))
           | (scope << SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_SHIFT);
}


typedef struct sky_python_symtable_block_data_s {
    sky_object_t                        id;
    sky_string_t                        name;
    sky_dict_t                          symbols;
    sky_list_t                          varnames;
    sky_list_t                          children;
    sky_python_symtable_block_t         parent;

    sky_python_symtable_block_type_t    type;
    int                                 lineno;
    int                                 col_offset;
    sky_python_symtable_optimized_t     optimized;
    sky_bool_t                          nested;
} sky_python_symtable_block_data_t;

struct sky_python_symtable_block_s {
    sky_object_data_t                   object_data;
    sky_python_symtable_block_data_t    block_data;
};


static inline sky_python_symtable_block_data_t *
sky_python_symtable_block_data(sky_object_t object)
{
    if (sky_python_symtable_block_type == sky_object_type(object)) {
        return (sky_python_symtable_block_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_python_symtable_block_type);
}


SKY_EXTERN sky_dict_t
sky_python_symtable_create(sky_python_ast_AST_t ast,
                           sky_string_t         filename);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PYTHON_SYMTABLE_H__ */
