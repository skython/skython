#include "sky_private.h"

#include <math.h>
#if defined(__APPLE__)
#   include <mach/mach_time.h>
#endif


sky_time_t
sky_time_current(void)
{
#if defined(__APPLE__)
    static mach_timebase_info_data_t    info;

    if (unlikely(!info.denom)) {
        mach_timebase_info(&info);
    }
    return (mach_absolute_time() * info.numer) / info.denom;
#elif defined(HAVE_CLOCK_GETTIME)
    struct timespec timespec;

    clock_gettime(CLOCK_MONOTONIC, &timespec);
    return (timespec.tv_sec * UINT64_C(1000000000)) + timespec.tv_nsec;
#elif defined(_WIN32)
    static int64_t  denominator = 0;

    LARGE_INTEGER   li;

    if (unlikely(!denominator)) {
        QueryPerformanceFrequency(&li);
        denominator = li.QuadPart;
    }
    QueryPerformanceCounter(&li);
    return (li.QuadPart * UINT64_C(1000000000)) / denominator;
#else
#   error implementation missing
#endif
}


sky_time_t
sky_time_fromobject(sky_object_t object)
{
    double  d, floatpart, intpart;

    if (sky_object_isa(object, sky_float_type)) {
        d = sky_float_value(object);
        floatpart = modf(d, &intpart);
        if (floatpart < 0) {
            floatpart = 1.0 + floatpart;
            intpart -= 1.0;
        }

        return ((uint64_t)intpart * UINT64_C(1000000000)) +
               (uint64_t)(floatpart * 1000000000.0);
    }
    if (sky_object_isa(object, sky_integer_type)) {
        return sky_integer_value(object, INT64_MIN, INT64_MAX, NULL) *
               UINT64_C(1000000000);
    }
    sky_error_raise_format(sky_TypeError,
                           "expected int or float; got %#@",
                           sky_type_name(sky_object_type(object)));
}
