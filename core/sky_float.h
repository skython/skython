/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_float Floating Point Number Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FLOAT_H__
#define __SKYTHON_CORE_SKY_FLOAT_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Constants defining the machine format of a floating-point value. **/
typedef enum sky_float_format_e {
    SKY_FLOAT_FORMAT_UNKNOWN,
    SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN,
    SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN,
} sky_float_format_t;


/** The floating-point format for a double. **/
SKY_EXTERN sky_float_format_t sky_float_format_double;
/** The floating-point format for a double as detected automatically. **/
SKY_EXTERN sky_float_format_t sky_float_format_double_detected;
/** The floating-point format for a float. **/
SKY_EXTERN sky_float_format_t sky_float_format_float;
/** The floating-point format for a float as detected automatically. **/
SKY_EXTERN sky_float_format_t sky_float_format_float_detected;


/** A float object instance representing NaN. **/
SKY_EXTERN sky_float_t const sky_float_nan;

/** A float object instance representing infinity. **/
SKY_EXTERN sky_float_t const sky_float_inf;

/** A float object instance representing negative infinity. **/
SKY_EXTERN sky_float_t const sky_float_negative_inf;

/** A float object instance representing the number 1.0. **/
SKY_EXTERN sky_float_t const sky_float_one;

/** A float object instance representing the number -1.0. **/
SKY_EXTERN sky_float_t const sky_float_negative_one;

/** A float object instance represneting the number 0.0. **/
SKY_EXTERN sky_float_t const sky_float_zero;

/** A float object instance representing the number -0.0. **/
SKY_EXTERN sky_float_t const sky_float_negative_zero;


/** Return the absolute value of a floating point number.
  * This is the implementation of __abs__().
  *
  * @param[in]  self    the floating point number for which the absolute value
  *                     is to be returned.
  * @return     the absolute value of @a self.
  */
SKY_EXTERN sky_float_t
sky_float_abs(sky_float_t self);


/** Add two floating point numbers.
  * This is the implementation of __add__().
  *
  * @param[in]  self    the addend.
  * @param[in]  other   the augend. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     a new floating point number that is the sum of @a self and
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_add(sky_float_t self, sky_object_t other);


/** Return the value of a floating point number as a ratio of two integers.
  *
  * @param[in]  self    the floating point number to be converted into a ratio
  *                     of two integers.
  * @return     a tuple containing the numerator and denominator of the ratio
  *             equivalent to @a self. The denominator will always be positive.
  */
SKY_EXTERN sky_tuple_t
sky_float_as_integer_ratio(sky_float_t self);


/** Return the complex conjugate of a floating point number.
  * The complex conjugate of a floating point number is always itself. This
  * function is exactly equivalent to sky_float_copy().
  *
  * @param[in]  self    the floating point number to conjugate.
  * @return     a copy of @a self if it is a sub-type of @c sky_float_type;
  *             otherwise, @a self.
  */
SKY_EXTERN sky_float_t
sky_float_conjugate(sky_float_t self);


/** Make a copy of a floating point number object.
  *
  * @param[in]  self    the floating point number object to copy.
  * @return     a copy of @a self, which may be @a self if its type is
  *             @c sky_float_type rather than a sub-type.
  */
SKY_EXTERN sky_float_t
sky_float_copy(sky_float_t self);


/** Create a new floating point number object instance from a double value.
  * 
  * @param[in]  value       the double value to be represented by the returned
  *                         floating point number object instance.
  * @return     a new floating point number object instance representing
  *             @a value.
  **/
SKY_EXTERN sky_float_t
sky_float_create(double value);


/** Create a new floating point number object instance from an array of ASCII
  * characters.
  *
  * @param[in]  bytes       the array of bytes to parse.
  * @param[in]  nbytes      the number of bytes present in @a bytes.
  * @return     a new floating point number object instance.
  */
SKY_EXTERN sky_float_t
sky_float_createfromascii(const char *bytes, size_t nbytes);


/** Create a new floating point number object instance from a string.
  *
  * @param[in]  source      the string to parse, which may be a string object
  *                         or any object supporting the buffer protocol.
  * @return     a new floating point number object instance.
  */
SKY_EXTERN sky_float_t
sky_float_createfromstring(sky_object_t source);


/** Return the floor of the quotient and remainder resulting from the division
  * of two floating point numbers.
  * This is the implementation of __divmod__().
  *
  * @param[in]  self    the dividend.
  * @param[in]  other   the divisor. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     a tuple containing the floor of the quotient and the remainder,
  *             or @c sky_NotImplemented if @a other cannot be converted to a
  *             floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_divmod(sky_float_t self, sky_object_t other);


/** Divide two floating point numbers and return the floor of the quotient.
  * This is the implementation of __floordiv__().
  *
  * @param[in]  self    the dividend.
  * @param[in]  other   the divisor. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     the floor of the quotient resulting from the division of
  *             @a self by @a other, or @c sky_NotImplemented if @a other
  *             cannot be converted to a floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_floordiv(sky_float_t self, sky_object_t other);


/** Create a new floating point number object from a hexadecimal string
  * representation.
  *
  * @param[in]  cls     the object type to instantiate. Must be a sub-type of
  *                     sky_float_type.
  * @param[in]  string  the hexadecimal string to convert to a floating point
  *                     number.
  * @return     a new object instance of @a cls representing the floating
  *             point number parsed as hexadecimal from @a string.
  */
SKY_EXTERN sky_float_t
sky_float_fromhex(sky_type_t cls, sky_string_t string);


/** Return a hexadecimal string representation of a floating point number.
  * The output from this function can be reversed using sky_float_fromhex().
  *
  * @param[in]  self    the floating point number object to convert to a
  *                     hexadecimal string representation.
  * @return     string  the hexadecimal string represnetation of @a self.
  */
SKY_EXTERN sky_string_t
sky_float_hex(sky_float_t self);


/** Determine if a floating point number is also an integral value.
  *
  * @param[in]  self    the floating number to be tested.
  * @return     @c SKY_TRUE if @a self is also an integral value, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_float_is_integer(sky_float_t self);


/** Return the remainder resulting from the division of two floating point
  * numbers.
  * This is the implementation if __mod__().
  *
  * @param[in]  self    the dividend.
  * @param[in]  other   the divisor. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     the remainder resulting from the division of @a self by
  *             @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_mod(sky_float_t self, sky_object_t other);


/** Multiply two floating point numbers.
  * This is the implementation of __mul__().
  *
  * @param[in]  self    the multicand.
  * @param[in]  other   the mulitplier. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     the product of @a self and @a other, or @c sky_NotImplemented
  *             if @a other cannot be converted to a floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_mul(sky_float_t self, sky_object_t other);


/** Negate a floating point number.
  * This is the implementation of __neg__().
  *
  * @param[in]  self    the floating point number to be negated.
  * @return     the negative of @a self.
  */
SKY_EXTERN sky_float_t
sky_float_neg(sky_float_t self);


/** Raise a floating point number to the power of another.
  * This is the implementation of __pow__().
  *
  * @param[in]  self    the base.
  * @param[in]  other   the exponent. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @param[in]  third   not supported; must be @c NULL or @c sky_None.
  * @return     a new floating point number that is @a self raised to the power
  *             of @a other, or @c sky_NotImplemented if @a other cannot be
  *             converted to a floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_pow(sky_float_t self, sky_object_t other, sky_object_t third);


/** Return a string representation of a floating point number.
  *
  * @param[in]  self    the floating point number for which the string
  *                     representation is to be returned.
  * @return     the string representation of @a self, which may be passed back
  *             to sky_float_createfromstring() to create a new floating point
  *             number object that is equal to @a self.
  */
SKY_EXTERN sky_string_t
sky_float_repr(sky_float_t self);


/** Round a floating point number.
  * This is the implementation of __round__().
  *
  * @param[in]  self    the floating point number to be rounded.
  * @param[in]  ndigits the number of digits to round to. May be @c NULL;
  *                     otherwise, any object implementing the __index__()
  *                     method.
  * @return     @a self rounded.
  */
SKY_EXTERN sky_object_t
sky_float_round(sky_float_t self, sky_object_t ndigits);


/** Return the sign of a floating point number.
  *
  * @param[in]  self    the float for which the sign is to be returned.
  * @return     one of -1, 0, or 1 to indicate negative, zero, or positive,
  *             respectively.
  */
SKY_EXTERN int
sky_float_sign(sky_float_t self);


/** Subtract to floating point numbers.
  * This is the implementation of __sub__().
  *
  * @param[in]  self    the minuend.
  * @param[in]  other   the subtrahend. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     the difference of @a self and @a other, or
  *             @c sky_NotImplemented if @a other cannot be converted to a
  *             floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_sub(sky_float_t self, sky_object_t other);


/** Divide two floating point numbers.
  * This is the implementation of __truediv__().
  *
  * @param[in]  self    the dividend.
  * @param[in]  other   the divisor. May be any object that implements the
  *                     __float__() method to return a floating point number.
  * @return     the quotient of @a self and @a other, or
  *             @c sky_NotImplemented if @a other cannot be converted to a
  *             floating point number.
  */
SKY_EXTERN sky_object_t
sky_float_truediv(sky_float_t self, sky_object_t other);


/** Return the value of an floating point number object instance as a primitive
  * type.
  */
SKY_EXTERN double
sky_float_value(sky_float_t self);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FLOAT_H__ */

/** @} **/
/** @} **/
