/** @file
  * @brief
  * @defgroup sky_function Functions
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_FUNCTION_H__
#define __SKYTHON_CORE_SKY_FUNCTION_H__ 1


#include "sky_base.h"
#include "sky_data_type.h"
#include "sky_object.h"
#include "sky_native_code.h"


SKY_CDECLS_BEGIN


/** Create a new function object instance for a bytecode function.
  *
  * @param[in]  qualname    the qualified name for the function.
  * @param[in]  code        the code object providing the function's
  *                         implementation.
  * @param[in]  globals     the globals to use when evaluating the function.
  * @param[in]  closure     xxx
  * @param[in]  defaults    optional defaults for positional arguments.
  * @param[in]  kwdefaults  optional defaults for keyword-only arguments.
  * @return     the new function object instance.
  */
SKY_EXTERN sky_function_t
sky_function_create(sky_string_t    qualname,
                    sky_code_t      code,
                    sky_dict_t      globals,
                    sky_tuple_t     closure,
                    sky_tuple_t     defaults,
                    sky_dict_t      kwdefaults);


/** Create a new function object instance for a built-in function.
  * This is primarily a convenience wrapper for building built-in functions in
  * C source code. It assumes that all strings are UTF-8 encoded. Parameters
  * are passed as variable arguments with a @c NULL terminating the list. For
  * each parameter, there must be three arguments passed: the keyword name for
  * the parameter; the native code type for the parameter; and a default value
  * for the parameter, which must be an object instance of @c NULL if there is
  * to be no default for the parameter. Once a default has been specified for a
  * parameter, all parameters following it must also have a default. If the
  * parameter name begins with "*" the varargs flag is set, and then every
  * parameter following it becomes a keyword only parameter that need not have
  * a default. If the parameter name starts with "**" the varkws flag is set,
  * and it must be the final parameter.
  *
  * @param[in]  qualname        the qualified name for the function.
  * @param[in]  doc             the documentation string for the function.
  *                             May be @c NULL.
  * @param[in]  function        the native function to call when this
  *                             function is invoked.
  * @param[in]  return_type     the native code type for the function's
  *                             return value.
  * @param[in]  ...
  * @return     the new function object instance.
  */
SKY_EXTERN sky_function_t
sky_function_createbuiltin(const char *                 qualname,
                           const char *                 doc,
                           sky_native_code_function_t   function,
                           sky_data_type_t              return_type,
                           ...);


/** Create a new function object instance for a built-in function with an
  * existing parameter list object.
  * This is primarily a convenience wrapper for building built-in functions in
  * C source code. It assumes that all strings are UTF-8 encoded. Parameters
  * for the function are specified with an already constructed parameter list
  * object.
  *
  * @param[in]  qualname            the qualified name for the function.
  * @param[in]  doc                 the documentation string for the function.
  *                                 May be @c NULL.
  * @param[in]  function            the native function to call when this
  *                                 function is invoked.
  * @param[in]  return_type         the native code type for the function's
  *                                 return value.
  * @param[in]  return_type_extra   Extra data that may be required by
  *                                 @a return_type. Ignored if @a return_type
  *                                 requires nothing extra.
  * @param[in]  parameters          parameters for the function.
  *
  * @return     the new function object instance.
  */
SKY_EXTERN sky_function_t
sky_function_createbuiltinwithparameterlist(
        const char *                qualname,
        const char *                doc,
        sky_native_code_function_t  function,
        sky_data_type_t             return_type,
        sky_object_t                return_type_extra,
        sky_parameter_list_t        parameters);


/** Return the code object that provides the implementation for a function.
  *
  * @param[in]  function    the function for which the code object is to be
  *                         returned.
  * @return     the code object instance that provides the implementation for
  *             @a function.
  */
SKY_EXTERN sky_object_t
sky_function_code(sky_function_t function);


/** Mark a function as deprecated.
  * This exists as a special case handling for compatibility with CPython and
  * the standard library unit tests. Skython normally does argument parsing
  * before calling the native code function, but CPython does argument parsing
  * in the native code function. Normally, CPython emits the deprecation
  * warning before the argument parsing is done, which means that some unit
  * tests fail on Skython because the argument parsing raises an exception
  * (rightly), but the warning doesn't get emitted first.
  *
  * @param[in]  function    the function to mark deprecated.
  * @param[in]  message     the message to present with the warning.
  */
SKY_EXTERN void
sky_function_setdeprecated(sky_function_t function, const char *message);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FUNCTION_H__ */

/** @} **/
