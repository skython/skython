#include "sky_private.h"


typedef void
        (*sky_descriptor_delete_t)(sky_object_t, sky_object_t);

void
sky_descriptor_delete(sky_object_t  descriptor,
                      sky_object_t  instance)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_descriptor_delete_t cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(descriptor, DELETE))) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ object has no attribute '__delete__'",
                               sky_type_name(descriptor));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_descriptor_delete_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (descriptor, instance));
    }
    else {
        method = sky_descriptor_get(method,
                                    descriptor,
                                    sky_object_type(descriptor));
        args = sky_tuple_pack(1, instance);
        sky_object_call(method, args, NULL);
    }
}


typedef sky_object_t
        (*sky_descriptor_get_t)(sky_object_t, sky_object_t, sky_type_t);

sky_object_t
sky_descriptor_get(sky_object_t descriptor,
                   sky_object_t instance,
                   sky_type_t   owner)
{
    sky_tuple_t             args;
    sky_object_t            method, o;
    sky_descriptor_get_t    cfunction;
    
    if (!(method = SKY_OBJECT_METHOD_SLOT(descriptor, GET))) {
        return descriptor;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_descriptor_get_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (descriptor, instance, owner));
        return o;
    }

    method = sky_descriptor_get(method, descriptor, sky_object_type(descriptor));
    args = sky_tuple_pack(2, instance, owner);
    return sky_object_call(method, args, NULL);
}


sky_bool_t
sky_descriptor_isdata(sky_object_t descriptor)
{
    sky_type_t  type = sky_object_type(descriptor);

    return (SKY_TYPE_METHOD_SLOT(type, GET) != NULL &&
            SKY_TYPE_METHOD_SLOT(type, SET) != NULL);
}


typedef void
        (*sky_descriptor_set_t)(sky_object_t, sky_object_t, sky_object_t);

void
sky_descriptor_set(sky_object_t descriptor,
                   sky_object_t instance,
                   sky_object_t value)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_descriptor_set_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(descriptor, SET))) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ object has no attribute '__set__'",
                               sky_type_name(descriptor));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_descriptor_set_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (descriptor, instance, value));
    }
    else {
        method = sky_descriptor_get(method,
                                    descriptor,
                                    sky_object_type(descriptor));
        args = sky_tuple_pack(2, instance, value);
        sky_object_call(method, args, NULL);
    }
}
