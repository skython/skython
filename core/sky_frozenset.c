#include "sky_private.h"


/* Note that the hash algorithm used here is the same as in collections.abc.Set
 * which is also the same as used by frozenset in CPython. The hash algorithm
 * must be the same everywhere so that sets always hash the same and match the
 * same. If the algorithm is changed here, it needs to be changed in
 * collections.abc.Set too.
 */


static sky_type_t sky_frozenset_iterator_type = NULL;


typedef struct sky_frozenset_item_s sky_frozenset_item_t;
struct sky_frozenset_item_s {
    uintptr_t                           key_hash;
    sky_object_t                        key;
};

typedef struct sky_frozenset_data_s {
    ssize_t                             len;
    size_t                              mask;   /* capacity - 1 */
    uintptr_t                           hash;
    sky_frozenset_item_t *              table;

    /* This is perfect on 64-bit, but there's waste on 32-bit. */
    sky_frozenset_item_t                tiny_table[8];
} sky_frozenset_data_t;

static sky_frozenset_data_t sky_frozenset_empty_data = {
    0,
    7,
    (1927868237UL * 69069UL) + 907133923UL,
    NULL,
    { { 0, NULL }, { 0, NULL }, { 0, NULL }, { 0, NULL },
      { 0, NULL }, { 0, NULL }, { 0, NULL }, { 0, NULL } },
};

SKY_EXTERN_INLINE sky_frozenset_data_t *
sky_frozenset_data(sky_object_t object)
{
    if (sky_frozenset_empty == object) {
        return &(sky_frozenset_empty_data);
    }
    if (sky_object_type(object) == sky_frozenset_type) {
        return (sky_frozenset_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_frozenset_type);
}


typedef struct sky_frozenset_iterator_data_s {
    sky_frozenset_t                     set;
    ssize_t                             index;
    ssize_t                             len;
} sky_frozenset_iterator_data_t;

SKY_EXTERN_INLINE sky_frozenset_iterator_data_t *
sky_frozenset_iterator_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_frozenset_iterator_type) {
        return (sky_frozenset_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_frozenset_iterator_type);
}


struct sky_frozenset_s {
    sky_object_data_t                   object_data;
    sky_frozenset_data_t                frozenset_data;
};

typedef struct sky_frozenset_iterator_s {
    sky_object_data_t                   object_data;
    sky_frozenset_iterator_data_t       iterator_data;
} *sky_frozenset_iterator_t;


static void
sky_frozenset_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_frozenset_data_t    *set_data = data;

    set_data->mask = (sizeof(set_data->tiny_table) /
                      sizeof(set_data->tiny_table[0])) - 1;
    set_data->hash = (uintptr_t)-1;
    set_data->table = set_data->tiny_table;
}


static void
sky_frozenset_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_frozenset_data_t    *set_data = data;

    if (set_data->table != set_data->tiny_table) {
        sky_free(set_data->table);
    }
}


static void
sky_frozenset_instance_iterate(
        SKY_UNUSED  sky_object_t                    self,
                    void *                          data,
                    sky_sequence_iterate_state_t *  state,
                    sky_object_t *                  objects,
                    ssize_t                         nobjects)
{
    sky_frozenset_data_t    *self_data = data;

    state->objects = objects;
    state->nobjects = 0;

    if (sky_frozenset_empty != self && state->state < self_data->len) {
        for (;;) {
            sky_frozenset_item_t    *item;

            item = &(self_data->table[state->extra[0]++]);
            if (item->key) {
                state->objects[state->nobjects++] = item->key;
                if (++state->state >= self_data->len ||
                    state->nobjects >= nobjects)
                {
                    break;
                }
            }
        }
    }
}


static void
sky_frozenset_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_frozenset_data_t    *self_data = data;

    ssize_t                 count, index;
    sky_frozenset_item_t    *item;

    for (count = self_data->len, index = 0; count > 0; --count) {
        do {
            item = &(self_data->table[index++]);
        } while (!item->key);
        sky_object_visit(item->key, visit_data);
    }
}


static void
sky_frozenset_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_frozenset_iterator_data_t   *self_data = data;

    sky_object_visit(self_data->set, visit_data);
}


static sky_object_t
sky_frozenset_compare(sky_frozenset_t   self,
                      sky_object_t      other,
                      sky_compare_op_t  compare_op)
{
    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            if (sky_object_len(self) != sky_object_len(other)) {
                return sky_False;
            }
            return (sky_frozenset_issubset(self, other) ? sky_True
                                                        : sky_False);
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (sky_object_len(self) == sky_object_len(other)) {
                return sky_True;
            }
            return (sky_frozenset_issubset(self, other) ? sky_False
                                                        : sky_True);
        case SKY_COMPARE_OP_LESS:
            if (sky_object_len(self) >= sky_object_len(other)) {
                return sky_False;
            }
            return (sky_frozenset_issubset(self, other) ? sky_True
                                                        : sky_False);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (sky_frozenset_issubset(self, other) ? sky_True
                                                        : sky_False);
        case SKY_COMPARE_OP_GREATER:
            if (sky_object_len(self) <= sky_object_len(other)) {
                return sky_False;
            }
            return (sky_frozenset_issuperset(self, other) ? sky_True
                                                          : sky_False);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (sky_frozenset_issuperset(self, other) ? sky_True
                                                          : sky_False);

        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
            break;
    }

    sky_error_fatal("internal error");
}


#define PERTURB_SHIFT   5

static sky_frozenset_item_t *
sky_frozenset_data_lookup(sky_frozenset_data_t *set_data,
                          uintptr_t             key_hash,
                          sky_object_t          key)
{
    size_t                  i, index, mask, perturb;
    sky_frozenset_item_t    *item;

    mask = set_data->mask;
    i = index = (size_t)key_hash & mask;
    for (perturb = key_hash; ; perturb >>= PERTURB_SHIFT) {
        item = &(set_data->table[index]);
        if (!item->key ||
            item->key == key ||
            (item->key_hash == key_hash &&
             sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL)))
        {
            return item;
        }
        i = (i << 2) + i + perturb + 1;
        index = i & mask;
    }

    sky_error_fatal("unreachable");
}


static sky_bool_t
sky_frozenset_insert(sky_frozenset_t set, sky_object_t key)
{
    sky_frozenset_data_t    *set_data = sky_frozenset_data(set);

    uintptr_t               key_hash;
    sky_frozenset_item_t    *item;

    if (sky_object_type(key) == sky_string_type) {
        key = sky_string_intern(key);
    }
    key_hash = sky_object_hash(key);
    item = sky_frozenset_data_lookup(set_data, key_hash, key);

    if (item->key) {
        return SKY_FALSE;
    }
    ++set_data->len;
    item->key_hash = key_hash;
    sky_object_gc_set(&(item->key), key, set);
    return SKY_TRUE;
}


static void
sky_frozenset_setcapacity(sky_frozenset_t set, size_t capacity)
{
    sky_frozenset_data_t    *set_data = sky_frozenset_data(set);

    size_t                  i, mask, perturb;
    ssize_t                 count, new_index, old_index;
    sky_frozenset_item_t    *new_item, *new_table, *old_item;

    capacity = sky_util_roundpow2(capacity);
    if (capacity < 8) {
        capacity = 8;
    }
    if (capacity - 1 == set_data->mask) {
        return;
    }
    mask = capacity - 1;

    new_table = sky_calloc(capacity, sizeof(sky_frozenset_item_t));
    for (count = set_data->len, old_index = 0; count > 0; --count) {
        do {
            old_item = &(set_data->table[old_index++]);
        } while (!old_item->key);

        i = new_index = (size_t)old_item->key_hash & mask;
        for (perturb = old_item->key_hash; ; perturb >>= PERTURB_SHIFT) {
            new_item = &(set_data->table[new_index]);
            if (!new_item->key) {
                break;
            }
            i = (i << 2) + i + perturb + 1;
            new_index = i & mask;
        }

        new_item->key_hash = old_item->key_hash;
        sky_object_gc_set(&(new_item->key), old_item->key, set);
    }

    if (set_data->table != set_data->tiny_table) {
        sky_free(set_data->table);
    }
    set_data->table = new_table;
    set_data->mask = mask;
}


static sky_object_t
sky_frozenset_createwithtype(sky_object_t object, sky_type_t type)
{
    ssize_t                 len;
    sky_object_t            iter, key;
    sky_frozenset_t         set;
    sky_frozenset_data_t    *set_data;

    sky_error_validate_debug(sky_type_issubtype(type, sky_frozenset_type));
    if (sky_object_isa(object, type)) {
        return object;
    }

    if (!sky_object_isiterable(object)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(object)));
    }

    /* Use fast iteration if possible */
    if (sky_sequence_isiterable(object)) {
        if (!(len = sky_object_len(object))) {
            if (sky_frozenset_type == type) {
                return sky_frozenset_empty;
            }
            return sky_object_allocate(type);
        }

        set = sky_object_allocate(type);
        set_data = sky_frozenset_data(set);
        sky_frozenset_setcapacity(set, len);

        SKY_SEQUENCE_FOREACH(object, key) {
            sky_frozenset_insert(set, key);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        len = sky_object_length_hint(object, 8);
        set = sky_object_allocate(type);
        set_data = sky_frozenset_data(set);
        sky_frozenset_setcapacity(set, len * 3 / 2);

        iter = sky_object_iter(object);
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            if ((size_t)set_data->len > (set_data->mask + 1) * 2 / 3) {
                sky_frozenset_setcapacity(set, (set_data->mask + 1) * 2);
            }
            sky_frozenset_insert(set, key);
        }

        if (!set_data->len && sky_frozenset_type == type) {
            set = sky_frozenset_empty;
        }
    }

    return set;
}


sky_object_t
sky_frozenset_and(sky_frozenset_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_frozenset_intersection(self, sky_tuple_pack(1, other));
}


sky_bool_t
sky_frozenset_contains(sky_frozenset_t self, sky_object_t key)
{
    if (sky_frozenset_empty != self) {
        sky_frozenset_data_t    *self_data = sky_frozenset_data(self);

        sky_frozenset_item_t    *item;

        item = sky_frozenset_data_lookup(self_data, sky_object_hash(key), key);
        if (item->key) {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_frozenset_t
sky_frozenset_copy(sky_frozenset_t self)
{
    return sky_frozenset_createwithtype(self, sky_frozenset_type);
}


sky_frozenset_t
sky_frozenset_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t         i;
    sky_frozenset_t set;

    sky_error_validate_debug(count >= 0);
    if (!count) {
        return sky_frozenset_empty;
    }

    set = sky_object_allocate(sky_frozenset_type);
    sky_frozenset_setcapacity(set, count);

    for (i = 0; i < count; ++i) {
        sky_frozenset_insert(set, (objects[i] ? objects[i] : sky_None));
    }

    return set;
}


sky_frozenset_t
sky_frozenset_difference(sky_frozenset_t self, sky_object_t args)
{
    ssize_t                 count, index;
    sky_bool_t              inserted, skip;
    sky_object_t            set;
    sky_frozenset_t         new_set;
    sky_frozenset_data_t    *self_data;
    sky_frozenset_item_t    *item;

    if (sky_frozenset_empty == self) {
        return sky_frozenset_empty;
    }

    inserted = SKY_FALSE;
    self_data = sky_frozenset_data(self);
    new_set = sky_object_allocate(sky_frozenset_type);
    for (count = self_data->len, index = 0; count > 0; --count) {
        do {
            item = &(self_data->table[index++]);
        } while (!item->key);

        skip = SKY_FALSE;
        SKY_SEQUENCE_FOREACH(args, set) {
            if (sky_object_contains(set, item->key)) {
                skip = SKY_TRUE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;

        if (!skip) {
            inserted = sky_frozenset_insert(new_set, item->key);
        }
    }

    return (inserted ? new_set : sky_frozenset_empty);
}


sky_object_t
sky_frozenset_eq(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_object_t
sky_frozenset_ge(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_frozenset_gt(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_GREATER);
}


uintptr_t
sky_frozenset_hash(sky_frozenset_t self)
{
    sky_frozenset_data_t    *self_data = sky_frozenset_data(self);

    ssize_t                 count, index;
    uintptr_t               hash;
    sky_frozenset_item_t    *item;

    if ((uintptr_t)-1 == self_data->hash) {
        hash = 1927868237UL * (self_data->len + 1);
        for (count = self_data->len, index = 0; count > 0; --count) {
            do {
                item = &(self_data->table[index++]);
            } while (!item->key);
            hash ^= (item->key_hash ^ (item->key_hash << 16) ^ 89869747UL) *
                    3644798167UL;
        }
        hash = hash * 69069UL + 3644798167UL;
        if ((uintptr_t)-1 == hash) {
            hash = 590923713UL;
        }
        self_data->hash = hash;
    }

    return self_data->hash;
}


sky_frozenset_t
sky_frozenset_intersection(sky_frozenset_t self, sky_object_t args)
{
    ssize_t                 count, index;
    sky_bool_t              insert, inserted;
    sky_object_t            set;
    sky_frozenset_t         new_set;
    sky_frozenset_data_t    *self_data;
    sky_frozenset_item_t    *item;

    if (sky_frozenset_empty == self) {
        return sky_frozenset_empty;
    }

    inserted = SKY_FALSE;
    self_data = sky_frozenset_data(self);
    new_set = sky_object_allocate(sky_frozenset_type);
    for (count = self_data->len, index = 0; count > 0; --count) {
        do {
            item = &(self_data->table[index++]);
        } while (!item->key);

        insert = SKY_TRUE;
        SKY_SEQUENCE_FOREACH(args, set) {
            if (!sky_object_contains(set, item->key)) {
                insert = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;

        if (insert) {
            inserted = sky_frozenset_insert(new_set, item->key);
        }
    }

    return (inserted ? new_set : sky_frozenset_empty);
}


sky_bool_t
sky_frozenset_isdisjoint(sky_frozenset_t self, sky_object_t other)
{
    sky_bool_t              result;
    sky_object_t            item, iter;
    sky_frozenset_data_t    *self_data;

    if (self == other) {
        return (sky_frozenset_len(self) ? SKY_FALSE : SKY_TRUE);
    }

    if (sky_object_type(other) == sky_set_type ||
        sky_object_type(other) == sky_frozenset_type)
    {
        if (sky_object_len(other) < sky_object_len(self) &&
            sky_object_type(other) == sky_frozenset_type)
        {
            item = other;
            other = self;
            self = item;
        }

        result = SKY_TRUE;
        SKY_SEQUENCE_FOREACH(self, item) {
            if (sky_object_contains(other, item)) {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;

        return result;
    }

    result = SKY_TRUE;
    self_data = sky_frozenset_data(self);
    if (sky_sequence_isiterable(other)) {
        SKY_SEQUENCE_FOREACH(other, item) {
            if (sky_frozenset_data_lookup(self_data,
                                          sky_object_hash(item),
                                          item))
            {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(other);
        while ((item = sky_object_next(iter, NULL)) != NULL) {
            if (sky_frozenset_data_lookup(self_data,
                                          sky_object_hash(item),
                                          item))
            {
                result = SKY_FALSE;
                break;
            }
        }
    }

    return result;
}


sky_bool_t
sky_frozenset_issubset(sky_frozenset_t self, sky_object_t other)
{
    sky_bool_t      result;
    sky_object_t    item;

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        other = sky_frozenset_createwithtype(other, sky_frozenset_type);
    }

    if (sky_frozenset_len(self) > sky_object_len(other)) {
        return SKY_FALSE;
    }

    result = SKY_TRUE;
    SKY_SEQUENCE_FOREACH(self, item) {
        if (!sky_object_contains(other, item)) {
            result = SKY_FALSE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return result;
}


sky_bool_t
sky_frozenset_issuperset(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_issubset(other, self);
}


sky_object_t
sky_frozenset_iter(sky_frozenset_t self)
{
    sky_frozenset_iterator_t        iterator;
    sky_frozenset_iterator_data_t   *iterator_data;

    if (sky_frozenset_empty == self) {
        return sky_object_allocate(sky_frozenset_iterator_type);
    }

    iterator = sky_object_allocate(sky_frozenset_iterator_type);
    iterator_data = sky_frozenset_iterator_data(iterator);
    iterator_data->len = sky_frozenset_data(self)->len;
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->set)), self, iterator);

    return iterator;
}


sky_object_t
sky_frozenset_le(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


ssize_t
sky_frozenset_len(sky_frozenset_t self)
{
    return sky_frozenset_data(self)->len;
}


sky_object_t
sky_frozenset_lt(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_frozenset_ne(sky_frozenset_t self, sky_object_t other)
{
    return sky_frozenset_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_object_t
sky_frozenset_new(sky_type_t cls, sky_object_t iterable)
{
    if (!iterable) {
        if (sky_frozenset_type == cls) {
            return sky_frozenset_empty;
        }
        return sky_object_allocate(cls);
    }

    return sky_frozenset_createwithtype(iterable, cls);
}


sky_object_t
sky_frozenset_or(sky_frozenset_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_frozenset_union(self, sky_tuple_pack(1, other));
}


sky_object_t
sky_frozenset_reduce(sky_frozenset_t self)
{
    sky_tuple_t     args;
    sky_object_t    dict;

    args = sky_tuple_pack(1, sky_list_create(self));
    dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), sky_None);

    return sky_tuple_pack(3, sky_object_type(self), args, dict);
}


sky_string_t
sky_frozenset_repr(sky_object_t self)
{
    ssize_t                 count, index;
    sky_string_t            joiner;
    sky_frozenset_data_t    *self_data;
    sky_frozenset_item_t    *item;
    sky_string_builder_t    builder;

    if (sky_frozenset_empty == self) {
        return sky_string_createfromformat(
                        "%@()",
                        sky_type_name(sky_object_type(self)));
    }

    if (sky_object_stack_push(self)) {
        return sky_string_createfromformat(
                        "%@(...)",
                        sky_type_name(sky_object_type(self)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        sky_string_builder_append(builder,
                                  sky_type_name(sky_object_type(self)));
        sky_string_builder_appendbytes(builder, "({", 2, NULL, NULL);

        self_data = sky_frozenset_data(self);
        sky_error_validate_debug(self_data->len > 0);
        if (self_data->len > 1) {
            joiner = SKY_STRING_LITERAL(", ");
        }

        for (count = self_data->len, index = 0; count > 0; --count) {
            do {
                item = &(self_data->table[index++]);
            } while (!item->key);

            sky_string_builder_append(builder, sky_object_repr(item->key));
            if (count > 1) {
                sky_string_builder_append(builder, joiner);
            }
        }

        sky_string_builder_appendbytes(builder, "})", 2, NULL, NULL);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


sky_object_t
sky_frozenset_rsub(sky_frozenset_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_frozenset_difference(other, sky_tuple_pack(1, self));
}


size_t
sky_frozenset_sizeof(sky_frozenset_t self)
{
    sky_frozenset_data_t    *self_data;

    if (sky_frozenset_empty == self) {
        return 0;
    }

    self_data = sky_frozenset_data(self);
    if (self_data->table && self_data->table != self_data->tiny_table) {
        return sky_memsize(self) + sky_memsize(self_data->table);
    }
    return sky_memsize(self);
}


sky_object_t
sky_frozenset_sub(sky_frozenset_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_frozenset_difference(self, sky_tuple_pack(1, other));
}


sky_frozenset_t
sky_frozenset_symmetric_difference(sky_frozenset_t self, sky_object_t other)
{
    sky_frozenset_t i, u;

    /* (self | other) - (self & other) */
    other = sky_tuple_pack(1, other);
    u = sky_frozenset_union(self, other);
    i = sky_frozenset_intersection(self, other);
    return sky_frozenset_difference(u, sky_tuple_pack(1, i));
}


sky_frozenset_t
sky_frozenset_union(sky_frozenset_t self, sky_object_t args)
{
    sky_bool_t      inserted;
    sky_object_t    object, set;
    sky_frozenset_t new_set;

    inserted = SKY_FALSE;
    new_set = sky_object_allocate(sky_frozenset_type);

    SKY_SEQUENCE_FOREACH(self, object) {
        inserted = sky_frozenset_insert(self, object);
    } SKY_SEQUENCE_FOREACH_END;

    SKY_SEQUENCE_FOREACH(args, set) {
        if (!sky_sequence_isiterable(set)) {
            set = sky_frozenset_createwithtype(set, sky_frozenset_type);
        }
        SKY_SEQUENCE_FOREACH(set, object) {
            inserted = sky_frozenset_insert(self, object);
        } SKY_SEQUENCE_FOREACH_END;
    } SKY_SEQUENCE_FOREACH_END;

    return (inserted ? new_set : sky_frozenset_empty);
}


sky_object_t
sky_frozenset_xor(sky_frozenset_t self, sky_object_t other)
{
    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type))
    {
        return sky_NotImplemented;
    }
    return sky_frozenset_symmetric_difference(self, other);
}


static sky_object_t
sky_frozenset_iterator_iter(sky_object_t self)
{
    return self;
}


static ssize_t
sky_frozenset_iterator_len(sky_object_t self)
{
    return sky_frozenset_iterator_data(self)->len;
}


static sky_object_t
sky_frozenset_iterator_next(sky_object_t self)
{
    sky_frozenset_iterator_data_t   *self_data =
                                    sky_frozenset_iterator_data(self);

    sky_frozenset_data_t    *set_data;
    sky_frozenset_item_t    *item;
    
    if (!self_data->len) {
        return NULL;
    }

    set_data = sky_frozenset_data(self_data->set);
    do {
        item = &(set_data->table[self_data->index++]);
    } while (!item->key);

    --self_data->len;
    return item->key;
}


static const char sky_frozenset_type_doc[] =
"frozenset() -> empty frozenset object\n"
"frozenset(iterable) -> frozenset object\n"
"\n"
"Build an immutable unordered collection of unique elements.";


SKY_TYPE_DEFINE_ITERABLE(frozenset,
                         "frozenset",
                         sizeof(sky_frozenset_data_t),
                         sky_frozenset_instance_initialize,
                         sky_frozenset_instance_finalize,
                         sky_frozenset_instance_visit,
                         sky_frozenset_instance_iterate,
                         NULL,
                         0,
                         sky_frozenset_type_doc);

sky_frozenset_t const sky_frozenset_empty = SKY_OBJECT_TAGGED_FROZENSET_EMPTY;


void
sky_frozenset_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_type_initialize_builtin(sky_frozenset_type, 0);

    sky_type_setattr_builtin(
            sky_frozenset_type,
            "copy",
            sky_function_createbuiltin(
                    "frozenset.copy",
                    "Return a shallow copy of a set.",
                    (sky_native_code_function_t)sky_frozenset_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "difference",
            sky_function_createbuiltin(
                    "frozenset.difference",
                    "Return the difference of two or more sets as a new set.\n\n"
                    "(i.e. all elements that are in this set but not the others.)",
                    (sky_native_code_function_t)sky_frozenset_difference,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "intersection",
            sky_function_createbuiltin(
                    "frozenset.intersection",
                    "Return the intersection of two or more sets as a new set.\n\n"
                    "(i.e., all elements that are in both sets.)",
                    (sky_native_code_function_t)sky_frozenset_intersection,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "isdisjoint",
            sky_function_createbuiltin(
                    "frozenset.isdisjoint",
                    "Return True if two sets have a null intersection.",
                    (sky_native_code_function_t)sky_frozenset_isdisjoint,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "issubset",
            sky_function_createbuiltin(
                    "frozenset.issubset",
                    "Report whether another set contains this set.",
                    (sky_native_code_function_t)sky_frozenset_issubset,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "issuperset",
            sky_function_createbuiltin(
                    "frozenset.issuperset",
                    "Report whether this set contains another set.",
                    (sky_native_code_function_t)sky_frozenset_issuperset,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "symmetric_difference",
            sky_function_createbuiltin(
                    "frozenset.symmetric_difference",
                    "Return the symmetric difference of two sets as a new set.\n\n"
                    "(i.e. all elements that are in exactly one of the sets.)",
                    (sky_native_code_function_t)sky_frozenset_symmetric_difference,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "other", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_frozenset_type,
            "union",
            sky_function_createbuiltin(
                    "frozenset.union",
                    "Return the union of sets as a new set.\n\n"
                    "(i.e. all elements that are in either set.)",
                    (sky_native_code_function_t)sky_frozenset_union,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_frozenset_type,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_frozenset_type,
            "__new__",
            (sky_native_code_function_t)sky_frozenset_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_frozenset_type,
            "__repr__", sky_frozenset_repr,
            "__sizeof__", sky_frozenset_sizeof,
            "__lt__", sky_frozenset_lt,
            "__le__", sky_frozenset_le,
            "__eq__", sky_frozenset_eq,
            "__ne__", sky_frozenset_ne,
            "__gt__", sky_frozenset_gt,
            "__ge__", sky_frozenset_ge,
            "__hash__", sky_frozenset_hash,
            "__sizeof__", sky_frozenset_sizeof,
            "__len__", sky_frozenset_len,
            "__iter__", sky_frozenset_iter,
            "__contains__", sky_frozenset_contains,
            "__sub__", sky_frozenset_sub,
            "__and__", sky_frozenset_and,
            "__xor__", sky_frozenset_xor,
            "__or__", sky_frozenset_or,
            "__rsub__", sky_frozenset_rsub,
            "__rand__", sky_frozenset_and,
            "__rxor__", sky_frozenset_xor,
            "__ror__", sky_frozenset_or,
            "__reduce__", sky_frozenset_reduce,
            NULL);


    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_frozenset_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("frozenset_iterator"),
                                   NULL,
                                   0,
                                   sizeof(sky_frozenset_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setmethodslots(type,
            "__len__", sky_frozenset_iterator_len,
            "__iter__", sky_frozenset_iterator_iter,
            "__next__", sky_frozenset_iterator_next,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_frozenset_iterator_type),
            type,
            SKY_TRUE);
}
