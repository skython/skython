/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_notimplemented The NotImplemented Object
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NOTIMPLEMENTED_H__
#define __SKYTHON_CORE_SKY_NOTIMPLEMENTED_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An object representing the NotImplemented object. **/
SKY_EXTERN sky_NotImplemented_t const sky_NotImplemented;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NOTIMPLEMENTED_H__ */

/** @} **/
/** @} **/
