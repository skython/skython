/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_range Range Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_RANGE_H__
#define __SKYTHON_CORE_SKY_RANGE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new range object instance.
  *
  * @param[in]  start   the starting value for the range.
  * @param[in]  stop    the stopping value for the range.
  * @param[in]  step    the stepping value for the range.
  * @return     the new range object instance.
  */
SKY_EXTERN sky_range_t
sky_range_create(sky_object_t start, sky_object_t stop, sky_object_t step);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_RANGE_H__ */

/** @} **/
/** @} **/
