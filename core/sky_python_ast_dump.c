#include "sky_private.h"
#include "sky_python_ast.h"

#define TAB SKY_STRING_LITERAL("  ")

static void
sky_python_ast_dump_format(sky_object_t         node,
                           sky_bool_t           annotate_fields,
                           sky_bool_t           include_attributes,
                           sky_string_builder_t builder,
                           sky_string_t         prefix);

static inline void
sky_python_ast_dump_fields_equals(sky_object_t         field,
                                  sky_string_builder_t builder,
                                  sky_string_t         prefix)
{
    if (prefix) {
        sky_string_builder_append(builder, prefix);
    }
    sky_string_builder_append(builder, field);
    if (prefix) {
        sky_string_builder_appendcodepoints(builder, " = ", 3, 1);
    }
    else {
        sky_string_builder_appendcodepoint(builder, '=', 1);
    }
}

static inline void
sky_python_ast_dump_newline_prefix(sky_string_builder_t builder,
                                   sky_string_t         prefix)
{
    if (prefix) {
        sky_string_builder_appendcodepoint(builder, '\n', 1);
        sky_string_builder_append(builder, prefix);
    }
}

static void
sky_python_ast_dump_fields(sky_object_t         fields,
                           sky_object_t         node,
                           sky_bool_t           annotate_fields,
                           sky_bool_t           include_attributes,
                           sky_string_builder_t builder,
                           sky_string_t         prefix)
{
    ssize_t         i, len;
    sky_object_t    field;
    sky_string_t    new_prefix;

    new_prefix = prefix ? sky_string_add(prefix, TAB) : NULL;

    if (!sky_object_isa(fields, sky_tuple_type)) {
        fields = sky_tuple_create(fields);
    }
    if ((len = sky_object_len(fields)) > 0) {
        field = sky_tuple_get(fields, 0);
        if (annotate_fields) {
            sky_python_ast_dump_fields_equals(field, builder, prefix);
        }
        field = sky_object_getattr(node, field, sky_NotSpecified);
        sky_python_ast_dump_format(field,
                                   annotate_fields,
                                   include_attributes,
                                   builder,
                                   new_prefix);

        for (i = 1; i < len; ++i) {
            sky_string_builder_appendcodepoints(builder, ", ", 2, 1);
            if (prefix) {
                sky_string_builder_appendcodepoint(builder, '\n', 1);
            }
            field = sky_tuple_get(fields, i);
            if (annotate_fields) {
                sky_python_ast_dump_fields_equals(field, builder, prefix);
            }
            field = sky_object_getattr(node, field, sky_NotSpecified);
            sky_python_ast_dump_format(field,
                                       annotate_fields,
                                       include_attributes,
                                       builder,
                                       new_prefix);
        }
    }
}

static void
sky_python_ast_dump_format(sky_object_t         node,
                           sky_bool_t           annotate_fields,
                           sky_bool_t           include_attributes,
                           sky_string_builder_t builder,
                           sky_string_t         prefix)
{
    ssize_t         i, len;
    sky_object_t    attrs, fields;
    sky_string_t    new_prefix;

    new_prefix = prefix ? sky_string_add(prefix, TAB) : NULL;

    if (sky_object_isa(node, sky_python_ast_AST_type)) {
        sky_string_builder_append(
                builder,
                sky_object_getattr(
                        sky_object_getattr(
                                node,
                                SKY_STRING_LITERAL("__class__"),
                                sky_NotSpecified),
                        SKY_STRING_LITERAL("__name__"),
                        sky_NotSpecified));
        if (prefix) {
            sky_string_builder_appendcodepoint(builder, ' ', 1);
        }
        sky_string_builder_appendcodepoint(builder, '(', 1);

        fields = sky_object_getattr(node,
                                    SKY_STRING_LITERAL("_fields"),
                                    sky_NotSpecified);
        attrs = sky_object_getattr(node,
                                   SKY_STRING_LITERAL("_attributes"),
                                   sky_NotSpecified);

        if (sky_object_len(attrs) || sky_object_len(fields)) {
            if (prefix) {
                sky_string_builder_appendcodepoint(builder, '\n', 1);
            }
            sky_python_ast_dump_fields(fields,
                                       node,
                                       annotate_fields,
                                       include_attributes,
                                       builder,
                                       new_prefix);

            if (include_attributes) {
                if (sky_object_len(attrs)) {
                    sky_string_builder_appendcodepoints(builder, ", ", 2, 1);
                    if (prefix) {
                        sky_string_builder_appendcodepoint(builder, '\n', 1);
                    }
                    sky_python_ast_dump_fields(attrs,
                                               node,
                                               annotate_fields,
                                               include_attributes,
                                               builder,
                                               NULL);
                }
            }
            sky_python_ast_dump_newline_prefix(builder, prefix);
        }

        sky_string_builder_appendcodepoint(builder, ')', 1);
    }
    else if (sky_object_isa(node, sky_list_type)) {
        sky_string_builder_appendcodepoint(builder, '[', 1);
        if ((len = sky_object_len(node)) > 0) {
            sky_python_ast_dump_newline_prefix(builder, new_prefix);
            sky_python_ast_dump_format(sky_list_get(node, 0),
                                       annotate_fields,
                                       include_attributes,
                                       builder,
                                       new_prefix);
            for (i = 1; i < len; ++i) {
                sky_string_builder_appendcodepoints(builder, ", ", 2, 1);
                sky_python_ast_dump_newline_prefix(builder, new_prefix);
                sky_python_ast_dump_format(sky_list_get(node, i),
                                           annotate_fields,
                                           include_attributes,
                                           builder,
                                           new_prefix);
            }
            sky_python_ast_dump_newline_prefix(builder, prefix);
        }
        sky_string_builder_appendcodepoint(builder, ']', 1);
    }
    else {
        sky_string_builder_append(builder, sky_object_repr(node));
    }
}


sky_string_t
sky_python_ast_dump(sky_object_t    node,
                    sky_bool_t      annotate_fields,
                    sky_bool_t      include_attributes,
                    sky_bool_t      pretty)
{
    sky_string_builder_t    builder;

    if (!sky_object_isa(node, sky_python_ast_AST_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected AST, got '%s'",
                               sky_type_name(sky_object_type(node)));
    }

    builder = sky_string_builder_create();
    sky_python_ast_dump_format(node,
                               annotate_fields,
                               include_attributes,
                               builder,
                               (pretty ? sky_string_empty : NULL));
    return sky_string_builder_finalize(builder);
}
