#ifndef __SKYTHON_CORE_SKY_TLSDATA_H__
#define __SKYTHON_CORE_SKY_TLSDATA_H__ 1


#include "sky_base.h"
#include "sky_atfork.h"
#include "sky_core_malloc_private.h"
#include "sky_hazard_pointer_private.h"
#include "sky_object_private.h"
#include "sky_thread_private.h"
#include "sky_tls_private.h"


#if defined(_WIN32)
#   include <wincrypt.h>
#endif


SKY_CDECLS_BEGIN


typedef struct sky_tlsdata_block_s sky_tlsdata_block_t;
typedef struct sky_tlsdata_orphan_block_s sky_tlsdata_orphan_block_t;


typedef enum sky_tlsdata_flag_e {
    /* Flags this thread as alive, meaning that it is active. The flag is
     * immediately cleared as soon as the tlsdata clean up function begins,
     * but the rest of the tlsdata structure may remain mostly intact for a
     * while if other threads have a hazard pointer on it.
     */
    SKY_TLSDATA_FLAG_ALIVE          =   0x1,

    /* Flags this thread as having registered with the atfork handling
     * infrastructure.
     */
    SKY_TLSDATA_FLAG_ATFORK         =   0x2,

    /* Flags this thread as tracing. This is normally set by default, but it is
     * temporarily cleared whenever the tracing function is called.
     */
    SKY_TLSDATA_FLAG_TRACING        =   0x4,
} sky_tlsdata_flag_t;


typedef SKY_ALIGNED(16) union sky_tlsdata_elimination_u {
    struct {
        void *                          object;
        void *                          pointer;
    } data;
#if defined(SKY_ARCH_32BIT)
    int64_t                             cas_value;
#elif defined(SKY_ARCH_64BIT)
    char                                cas_value[16];
#endif
} sky_tlsdata_elimination_t;


/* The organization of this structure may seem to be completely haphazard, but
 * some thought has actually gone into it. The main idea is to keep the most
 * frequently used bits together on the same cache line at the head of the
 * structure. Everything else doesn't really matter, but the least frequently
 * used bits should definitely go at the bottom by the padding.
 */
typedef struct sky_tlsdata_s {
    /* This is used only by sky_core_malloc. There is a bunch of code around
     * in various places that assumes this to be true.
     */
    sky_core_malloc_tlsdata_t *         malloc_tlsdata;

    /* error_blocks is used by both sky_asset and sky_error. */
    sky_error_block_t *                 error_blocks;

    sky_error_asset_t *                 error_asset_freelist;

    /* This is a convenience. It can be computed from tlsdata_block, but it
     * will be used frequently by memory allocation and other things, so keep
     * it stored in the structure, and on the same cache line as the malloc
     * structures.
     */
    uint32_t                            thread_id;

    /* A bit mask of flags made up of constants defined in the
     * sky_tlsdata_flag_t enum.
     */
    uint32_t                            flags;

    /* The current frame being evaluated, if any. */
    sky_frame_t                         current_frame;
    ssize_t                             recursiondepth;
    ssize_t                             recursionlimit;

    sky_object_t                        trace_function;
#if 0
    sky_task_queue_t                    current_task_queue;
    sky_task_runner_t                   task_runner;
#endif

    sky_object_tlsdata_t                object_tlsdata;

    sky_thread_t                        thread_self;

    sky_thread_wait_t                   wait;

    sky_tlsdata_elimination_t           elimination;

    sky_hazard_pointer_tlsdata_t        hazard_pointer_tlsdata;

    sky_atfork_tlsdata_t                atfork_tlsdata;

    sky_tls_tlsdata_t                   tls_tlsdata;

    /* This will typically only be used when allocating a tlsdata structure
     * for a new thread, and when cleaning it up. It's possible that it may
     * otherwise be used, but it ought to be rare if so.
     */
    SKY_CACHE_LINE_ALIGNED
    sky_tlsdata_block_t *               tlsdata_block;
    pthread_t                           pthread_self;

    /* Because tlsdata_block is aligned on a cache line, only the fields from
     * that point in the structure need to be included for padding to make sure
     * that the structure's size is a multiple of the cache line size.
     */
    int8_t cache_line_padding[SKY_CACHE_LINE_PAD(
                              sizeof(sky_tlsdata_block_t *) +
                              sizeof(pthread_t))];
} sky_tlsdata_t;


struct sky_tlsdata_block_s {
    sky_atomic_lifo_t                   freelist;

    int32_t                             total;
    int32_t                             used;

    sky_tlsdata_block_t *               next;
    sky_tlsdata_block_t *               prev;
    uint32_t                            first_thread_id;

    SKY_CACHE_LINE_ALIGNED
    sky_tlsdata_t *                     slots[0];
};


typedef struct sky_tlsdata_orphan_s sky_tlsdata_orphan_t;
struct sky_tlsdata_orphan_s {
    sky_tlsdata_orphan_t *              next;
    sky_tlsdata_orphan_block_t *        orphan_block;

    sky_core_malloc_tlsdata_t *         malloc_tlsdata;
    sky_object_tlsdata_orphan_t         object_tlsdata;
    sky_hazard_pointer_tlsdata_orphan_t hazard_pointer_tlsdata;
};


struct sky_tlsdata_orphan_block_s {
    sky_atomic_lifo_t                   freelist;

    int32_t                             total;
    int32_t                             used;

    sky_tlsdata_orphan_block_t *        next;
    sky_tlsdata_orphan_block_t *        prev;

    sky_tlsdata_orphan_t                slots[0];
};


extern pthread_key_t    sky_tlsdata_key;

extern void sky_tlsdata_atfork_child(void);
extern void sky_tlsdata_atfork_parent(void);
extern void sky_tlsdata_atfork_prepare(void);

extern sky_tlsdata_t *  sky_tlsdata_create(void);
extern void             sky_tlsdata_finalize(void);
extern void             sky_tlsdata_initialize(void);


SKY_EXTERN_INLINE sky_tlsdata_t *
sky_tlsdata_get(void)
{
    sky_tlsdata_t   *tlsdata;

    if (unlikely(!(tlsdata = pthread_getspecific(sky_tlsdata_key)))) {
        tlsdata = sky_tlsdata_create();
        pthread_setspecific(sky_tlsdata_key, tlsdata);
    }

    return tlsdata;
}


extern sky_bool_t               sky_tlsdata_adopt(void);
extern sky_hazard_pointer_t *   sky_tlsdata_getid(uint32_t id);
extern uint32_t                 sky_tlsdata_highestid(void);
extern void                     sky_tlsdata_retire(void *arg);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TLSDATA_H__ */
