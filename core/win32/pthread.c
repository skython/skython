#define  __SKYTHON_WIN32_PTHREAD_C__ 1
#include "pthread.h"

#include "../sky_private.h"

#include <errno.h>
#include <process.h>


typedef void (*pthread_key_destructor_t)(void *);


struct pthread_s {
    HANDLE                              handle;
    void *                              exit_value;

    /* For pthread_cond_timedwait(), each thread has an event object that it
     * waits on. When pthread_cond_broadcast() or pthread_cond_signal() is
     * called to wake up a waiting thread, this is the object that is signaled.
     */
    HANDLE                              win32_event;
    pthread_t                           next_cond_waiter;
    int                                 signaled;

    int                                 detach_state;
    int                                 cancel_state;
    int                                 cancel_type;

    void *(*start_routine)(void *);
    void *start_routine_arg;
};


static DWORD        pthread_self_key  = ~(DWORD)0;
static sky_once_t   pthread_self_once = SKY_ONCE_INITIALIZER;


/* Per MSDN (http://msdn.microsoft.com/en-us/library/ms686749(v=VS.85).aspx),
 * the maximum number of TLS indexes available per process is 10888. The
 * minimum is guaranteed to be at least 64 for all systems.
 */
static pthread_key_destructor_t pthread_key_destructors[1088];


static uint64_t
pthread_current_time(void)
{
    FILETIME        filetime;
    uint64_t        msec;
    ULARGE_INTEGER  uli;

    /* 100-nanosecond intervals since January 1, 1601 UTC */
    GetSystemTimeAsFileTime(&filetime);

    uli.LowPart  = filetime.dwLowDateTime;
    uli.HighPart = filetime.dwHighDateTime;

    /* Milliseconds since January 1, 1601 UTC */
    msec = (uli.QuadPart * 100) / 1000000;

    /* Shift from January 1, 1601 to January 1, 1970 */
    return msec - UINT64_C(11644473600000);
}


int
pthread_key_create(pthread_key_t *key, void (*destructor)(void *))
{
    if ((*key = TlsAlloc()) == TLS_OUT_OF_INDEXES) {
        return ENOMEM;
    }
    pthread_key_destructors[*key] = destructor;
    return 0;
}


int
pthread_key_delete(pthread_key_t key)
{
    if (key < sizeof(pthread_key_destructors) / sizeof(pthread_key_destructors[0])) {
        pthread_key_destructors[key] = NULL;
        if (TlsFree(key)) {
            return 0;
        }
    }
    return EINVAL;
}


int
pthread_setspecific(pthread_key_t key, const void *value)
{
    if (!TlsSetValue(key, (void *)value)) {
        return EINVAL;
    }
    return 0;
}



int
pthread_atfork(SKY_UNUSED void (*prepare)(void),
               SKY_UNUSED void (*parent)(void),
               SKY_UNUSED void (*child)(void))
{
    /* Windows has no concept of fork() in its process model. Do nothing,
     * but indicate success. Any handlers set will just never get called.
     */
    return 0;
}


int
pthread_attr_destroy(SKY_UNUSED pthread_attr_t *attr)
{
    return 0;
}


int
pthread_attr_init(SKY_UNUSED pthread_attr_t *attr)
{
    memset(attr, 0x00, sizeof(pthread_attr_t));
    return 0;
}


int
pthread_attr_getdetachstate(const pthread_attr_t * restrict attr,
                            int * restrict detach_state)
{
    *detach_state = attr->detach_state;
    return 0;
}


int
pthread_attr_getstacksize(const pthread_attr_t * restrict attr,
                          size_t * restrict stack_size)
{
    *stack_size = (size_t)attr->stack_size;
    return 0;
}


int
pthread_attr_setdetachstate(pthread_attr_t *attr, int detach_state)
{
    if (detach_state != PTHREAD_CREATE_JOINABLE &&
        detach_state != PTHREAD_CREATE_DETACHED)
    {
        return EINVAL;
    }
    attr->detach_state = detach_state;
    return 0;
}


int
pthread_attr_setstacksize(pthread_attr_t *attr, size_t stack_size)
{
    if (stack_size > UINT_MAX) {
        return EINVAL;
    }
    attr->stack_size = (unsigned)stack_size;
    return 0;
}


static void
pthread_self_initialize(void *arg)
{
    *(DWORD *)arg = TlsAlloc();
    sky_error_validate(*(DWORD *)arg != TLS_OUT_OF_INDEXES);
}


static unsigned __stdcall
pthread_start_routine(void *arg)
{
    pthread_t   thread = arg;

    sky_once_run_unsafe(&pthread_self_once,
                        pthread_self_initialize, &pthread_self_key);
    TlsSetValue(pthread_self_key, thread);

    thread->exit_value = thread->start_routine(thread->start_routine_arg);

    return 0;
}


static pthread_attr_t pthread_attr_default = {
    PTHREAD_CREATE_JOINABLE,            /* detach_state */
    0,                                  /* stack_size   */
};


int
pthread_create(pthread_t * restrict thread,
               const pthread_attr_t * restrict attr,
               void *(*start_routine)(void *), void *arg)
{
    if (!attr) {
        attr = &pthread_attr_default;
    }

    *thread = HeapAlloc(GetProcessHeap(),
                        HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY,
                        sizeof(struct pthread_s));
    (*thread)->win32_event       = CreateEvent(NULL, FALSE, FALSE, NULL);
    (*thread)->next_cond_waiter  = NULL;
    (*thread)->signaled          = 0;
    (*thread)->detach_state      = attr->detach_state;
    (*thread)->cancel_state      = PTHREAD_CANCEL_ENABLE;
    (*thread)->cancel_type       = PTHREAD_CANCEL_DEFERRED;
    (*thread)->start_routine     = start_routine;
    (*thread)->start_routine_arg = arg;

    if (!((*thread)->handle = (HANDLE)_beginthreadex(NULL,
                                                     attr->stack_size,
                                                     pthread_start_routine,
                                                     *thread,
                                                     CREATE_SUSPENDED,
                                                     NULL)))
    {
        CloseHandle((*thread)->win32_event);
        HeapFree(GetProcessHeap(), 0, *thread);
        *thread = NULL;
        return errno;
    }

    ResumeThread((*thread)->handle);
    return 0;
}


int
pthread_detach(pthread_t thread)
{
    thread->detach_state = PTHREAD_CREATE_DETACHED;
    return 0;
}


int
pthread_equal(pthread_t thread_1, pthread_t thread_2)
{
    return (int)((intptr_t)thread_1 - (intptr_t)thread_2);
}


void
pthread_exit(void *exit_value)
{
    pthread_t   thread = pthread_self();

    thread->exit_value = exit_value;
    _endthreadex(0);
}


int
pthread_join(pthread_t thread, void **exit_value)
{
    if (pthread_self() == thread) {
        return EDEADLK;
    }
    if (WaitForSingleObject(thread->handle, INFINITE) == WAIT_OBJECT_0) {
        *exit_value = thread->exit_value;
        CloseHandle(thread->win32_event);
        CloseHandle(thread->handle);
        HeapFree(GetProcessHeap(), 0, thread);
        return 0;
    }
    return ESRCH;
}


pthread_t
pthread_self(void)
{
    pthread_t   thread;

    sky_once_run_unsafe(&pthread_self_once,
                        pthread_self_initialize, &pthread_self_key);
    if (!(thread = TlsGetValue(pthread_self_key))) {
        thread = HeapAlloc(GetProcessHeap(),
                           HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY,
                           sizeof(struct pthread_s));
        thread->handle           = GetCurrentThread();
        thread->win32_event      = CreateEvent(NULL, FALSE, FALSE, NULL);
        thread->next_cond_waiter = NULL;
        thread->signaled         = 0;
        thread->detach_state     = PTHREAD_CREATE_JOINABLE;
        thread->cancel_state     = PTHREAD_CANCEL_ENABLE;
        thread->cancel_type      = PTHREAD_CANCEL_DEFERRED;
        TlsSetValue(pthread_self_key, thread);
    }

    return thread;
}


int
pthread_setcancelstate(int new_state, int *old_state)
{
    pthread_t   self = pthread_self();

    if (new_state != PTHREAD_CANCEL_DISABLE &&
        new_state != PTHREAD_CANCEL_ENABLE)
    {
        return EINVAL;
    }
    if (old_state) {
        *old_state = self->cancel_state;
    }
    self->cancel_state = new_state;
    return 0;
}


int
pthread_setcanceltype(int new_type, int *old_type)
{
    pthread_t   self = pthread_self();

    if (new_type != PTHREAD_CANCEL_DEFERRED &&
        new_type != PTHREAD_CANCEL_ASYNCHRONOUS)
    {
        return EINVAL;
    }
    if (old_type) {
        *old_type = self->cancel_type;
    }
    self->cancel_type = new_type;
    return 0;
}


int
pthread_mutexattr_destroy(pthread_mutexattr_t *attr)
{
    return 0;
}


int
pthread_mutexattr_gettype(const pthread_mutexattr_t * restrict attr,
                          int * restrict type)
{
    *type = attr->type;
    return 0;
}


int
pthread_mutexattr_init(pthread_mutexattr_t *attr)
{
    memset(attr, 0, sizeof(pthread_mutexattr_t));
    attr->type = PTHREAD_MUTEX_DEFAULT;
    return 0;
}


int
pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type)
{
    if (type != PTHREAD_MUTEX_NORMAL &&
        type != PTHREAD_MUTEX_ERRORCHECK &&
        type != PTHREAD_MUTEX_RECURSIVE)
    {
        return EINVAL;
    }
    attr->type = type;
    return 0;
}


struct pthread_mutex_s {
    sky_once_t                          once_init;

    HANDLE                              win32_mutex;
    pthread_t                           lock_owner;
    int32_t                             lock_count;

    int                                 type;
};


static void
pthread_mutex_once_init(void *arg)
{
    pthread_mutex_t *mutex = arg;

    if (!(mutex->win32_mutex = CreateMutex(NULL, FALSE, NULL))) {
        sky_error_throw_win32(GetLastError());
    }
    mutex->lock_owner = NULL;
    mutex->lock_count = 0;
}


int
pthread_mutex_destroy(pthread_mutex_t *mutex)
{
    if (mutex->once_init) {
        if (mutex->lock_owner) {
            return EBUSY;
        }
        if (mutex->win32_mutex) {
            CloseHandle(mutex->win32_mutex);
            mutex->win32_mutex = NULL;
        }
    }
    return 0;
}


int
pthread_mutex_init(pthread_mutex_t * restrict mutex,
                   const pthread_mutexattr_t * restrict attr)
{
    sky_once_init(&(mutex->once_init));
    mutex->type = (attr ? attr->type : PTHREAD_MUTEX_DEFAULT);

    return 0;
}


int
pthread_mutex_lock(pthread_mutex_t *mutex)
{
    return pthread_mutex_timedlock_np(mutex, NULL);
}


int
pthread_mutex_timedlock_np(pthread_mutex_t * restrict mutex, 
                           const struct timespec * restrict timeout)
{
    pthread_t   thread = pthread_self();

    DWORD       dwMilliseconds, dwResult;

    sky_once_run(&(mutex->once_init), pthread_mutex_once_init, mutex);

    if (mutex->type != PTHREAD_MUTEX_RECURSIVE) {
        sky_error_validate(mutex->lock_owner != thread);
    }
    if (!timeout) {
        dwMilliseconds = INFINITE;
    }
    else {
        uint64_t    current_time = pthread_current_time(),
                    future_time  = ((timeout->tv_sec * 1000) +
                                    (timeout->tv_nsec / 1000000));

        if (future_time <= current_time) {
            dwMilliseconds = 0;
        }
        else if (future_time - current_time > ULONG_MAX) {
            return EINVAL;
        }
        else {
            dwMilliseconds = (DWORD)(future_time - current_time);
        }
    }

    dwResult = WaitForSingleObject(mutex->win32_mutex, dwMilliseconds);
    if (dwResult == WAIT_OBJECT_0 || dwResult == WAIT_ABANDONED) {
        mutex->lock_owner = thread;
        if (dwResult == WAIT_OBJECT_0) {
            ++mutex->lock_count;
        }
        else {
            mutex->lock_count = 1;
        }
        return 0;
    }
    if (dwResult == WAIT_TIMEOUT) {
        return ETIMEDOUT;
    }

    return EINVAL;
}


int
pthread_mutex_trylock(pthread_mutex_t *mutex)
{
    int             rc;
    struct timespec timeout;

    timeout.tv_sec  = 0;
    timeout.tv_nsec = 0;
    rc = pthread_mutex_timedlock_np(mutex, &timeout);
    return (rc == ETIMEDOUT ? EBUSY : rc);
}


int
pthread_mutex_unlock(pthread_mutex_t *mutex)
{
    pthread_t   thread = pthread_self();

    sky_once_run(&(mutex->once_init), pthread_mutex_once_init, mutex);

    if (mutex->lock_owner != thread) {
        return EPERM;
    }
    if (!--mutex->lock_count) {
        mutex->lock_owner = NULL;
    }
    if (!ReleaseMutex(mutex->win32_mutex)) {
        return EPERM;
    }
    return 0;
}


int
pthread_condattr_destroy(pthread_condattr_t *attr)
{
    return 0;
}


int
pthread_condattr_init(pthread_condattr_t *attr)
{
    return 0;
}


struct pthread_cond_s {
    sky_once_t                          once_init;

    /* If any thread is waiting on the cond, this is the mutex that was used.
     * Every wait must use the same mutex.
     */
    pthread_mutex_t *                   mutex;

    /* List of threads waiting on the cond, which is protected by the mutex. */
    pthread_t                           waiting_threads;
};


static void
pthread_cond_once_init(void *arg)
{
    pthread_cond_t  *cond = arg;

    cond->mutex = NULL;
    cond->waiting_threads = NULL;
}


int
pthread_cond_broadcast(pthread_cond_t *cond)
{
    pthread_t   thread = pthread_self();

    pthread_t   waiting_threads;

    sky_once_run(&(cond->once_init), pthread_cond_once_init, cond);

    sky_error_validate(cond->mutex != NULL);
    sky_error_validate(cond->mutex->lock_owner == thread);

    waiting_threads = cond->waiting_threads;
    cond->waiting_threads = NULL;

    while ((thread = waiting_threads) != NULL) {
        waiting_threads = thread->next_cond_waiter;
        thread->signaled = 1;
        SetEvent(thread->win32_event);
    }

    return 0;
}


int
pthread_cond_destroy(pthread_cond_t *cond)
{
    if (cond->once_init) {
        if (cond->waiting_threads) {
            return EBUSY;
        }
    }
    return 0;
}


int
pthread_cond_init(pthread_cond_t * restrict cond,
                  const pthread_condattr_t * restrict attr)
{
    sky_once_init(&(cond->once_init));
    return 0;
}


int
pthread_cond_signal(pthread_cond_t *cond)
{
    pthread_t   thread = pthread_self();

    sky_once_run(&(cond->once_init), pthread_cond_once_init, cond);

    sky_error_validate(cond->mutex != NULL);
    sky_error_validate(cond->mutex->lock_owner == thread);

    if ((thread = cond->waiting_threads) != NULL) {
        thread->signaled = 1;
        SetEvent(thread->win32_event);
        cond->waiting_threads = thread->next_cond_waiter;
    }

    return 0;
}


int
pthread_cond_timedwait(pthread_cond_t * restrict cond,
                       pthread_mutex_t * restrict mutex,
                       const struct timespec * restrict timeout)
{
    pthread_t   this_thread = pthread_self();

    DWORD       dwMilliseconds, dwResultEvent, dwResultMutex;
    pthread_t   *prev_thread, thread;

    sky_once_run(&(cond->once_init), pthread_cond_once_init, cond);

    sky_error_validate(!cond->mutex || cond->mutex == mutex);
    sky_error_validate(mutex->lock_owner == this_thread);
    sky_error_validate(mutex->lock_count == 1);

    if (!timeout) {
        dwMilliseconds = INFINITE;
    }
    else {
        uint64_t    current_time = pthread_current_time(),
                    future_time  = ((timeout->tv_sec * 1000) +
                                    (timeout->tv_nsec / 1000000));

        if (future_time <= current_time) {
            dwMilliseconds = 0;
        }
        else if (future_time - current_time > ULONG_MAX) {
            return EINVAL;
        }
        else {
            dwMilliseconds = (DWORD)(future_time - current_time);
        }
    }

    this_thread->signaled = 0;
    this_thread->next_cond_waiter = cond->waiting_threads;
    cond->waiting_threads = this_thread;
    cond->mutex = mutex;

    /* Unlock the mutex, wait for this thread's event to be signaled, and
     * re-acquire the mutex. The mutex must always be held on entry and
     * exit from this function.
     */
    mutex->lock_owner = NULL;
    mutex->lock_count = 0;
    ReleaseMutex(mutex->win32_mutex);
    dwResultEvent = WaitForSingleObject(this_thread->win32_event,
                                        dwMilliseconds);

    /* Re-acquire the mutex before checking the status of the event. */
    dwResultMutex = WaitForSingleObject(mutex->win32_mutex, INFINITE);
    sky_error_validate(dwResultMutex == WAIT_ABANDONED ||
                       dwResultMutex == WAIT_OBJECT_0);
    mutex->lock_owner = this_thread;
    mutex->lock_count = 1;

    /* If the thread is signaled, nothing else matters. Return success. */
    if (this_thread->signaled) {
        return 0;
    }

    /* Remove the thread from the list of waiters. This should only happen
     * on a timeout. Anything else is an internal error.
     */
    prev_thread = &(cond->waiting_threads);
    for (thread = cond->waiting_threads;
         thread;
         thread = thread->next_cond_waiter)
    {
        if (thread == this_thread) {
            *prev_thread = thread->next_cond_waiter;
            break;
        }
        prev_thread = &(thread->next_cond_waiter);
    }

    if (dwResultEvent == WAIT_TIMEOUT) {
        return ETIMEDOUT;
    }
    sky_error_fatal("internal error");
}


int
pthread_cond_wait(pthread_cond_t * restrict cond,
                  pthread_mutex_t * restrict mutex)
{
    return pthread_cond_timedwait(cond, mutex, NULL);
}


void
pthread_finalize_thread_np(void)
{
    int                         i;
    void                        *value;
    pthread_t                   thread;
    pthread_key_t               key;
    pthread_key_destructor_t    destructor;

    for (i = 0;  i < PTHREAD_DESTRUCTOR_ITERATIONS;  ++i) {
        for (key = 0;
             key < sizeof(pthread_key_destructors) / sizeof(pthread_key_destructors[0]);
             ++key)
        {
            if ((destructor = pthread_key_destructors[key]) != NULL &&
                (value = TlsGetValue(key)) != NULL)
            {
                TlsSetValue(key, NULL);
                destructor(value);
            }
        }
    }

    if ((thread = TlsGetValue(pthread_self_key)) != NULL) {
        TlsSetValue(pthread_self_key, NULL);
        if (PTHREAD_CREATE_DETACHED == thread->detach_state) {
            CloseHandle(thread->win32_event);
            CloseHandle(thread->handle);
            HeapFree(GetProcessHeap(), 0, thread);
        }
    }
}
