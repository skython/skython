#include "../sky_private.h"


BOOL WINAPI
DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason) {
        case DLL_PROCESS_ATTACH:
            sky_library_construct();
            break;
        case DLL_PROCESS_DETACH:
            /* Normally everything done for DLL_THREAD_DETACH should also be
             * done for DLL_PROCESS_DETACH, but for better parity with what
             * happens on every platform other than Win32, don't.
             */
            sky_library_destruct();
            break;
        case DLL_THREAD_ATTACH:
            break;
        case DLL_THREAD_DETACH:
            pthread_finalize_thread_np();
            break;
    }

    return TRUE;
}
