#ifndef __SKYTHON_WIN32_PTHREAD_H__
#define __SKYTHON_WIN32_PTHREAD_H__ 1


#include "sky_base.h"
#include "sky_once.h"


SKY_CDECLS_BEGIN


typedef DWORD                       pthread_key_t;

typedef struct pthread_s *          pthread_t;
typedef struct pthread_cond_s       pthread_cond_t;
typedef struct pthread_mutex_s      pthread_mutex_t;


#define PTHREAD_KEYS_MAX                TLS_MINIMUM_AVAILABLE
#define PTHREAD_DESTRUCTOR_ITERATIONS   4


extern int pthread_key_create(pthread_key_t *key, void (*destructor)(void *));
extern int pthread_key_delete(pthread_key_t key);
extern int pthread_setspecific(pthread_key_t key, const void *value);


SKY_EXTERN_INLINE void *
pthread_getspecific(pthread_key_t key)
{
    return TlsGetValue(key);
}


extern int pthread_atfork(void (*prepare)(void), void (*parent)(void), void (*child)(void));


#define PTHREAD_CANCEL_ENABLE       0x01
#define PTHREAD_CANCEL_DISABLE      0x00
#define PTHREAD_CANCEL_DEFERRED     0x02
#define PTHREAD_CANCEL_ASYNCHRONOUS 0x00

#define PTHREAD_CREATE_JOINABLE 0
#define PTHREAD_CREATE_DETACHED 1

typedef struct pthread_attr_s {
    int                                 detach_state;
    unsigned                            stack_size;
} pthread_attr_t;


extern int pthread_attr_destroy(pthread_attr_t *attr);
extern int pthread_attr_getdetachstate(const pthread_attr_t * restrict attr,
                                       int * restrict detach_state);
extern int pthread_attr_getstacksize(const pthread_attr_t * restrict attr,
                                     size_t * restrict stack_size);
extern int pthread_attr_init(pthread_attr_t *attr);
extern int pthread_attr_setdetachstate(pthread_attr_t *attr, int detach_state);
extern int pthread_attr_setstacksize(pthread_attr_t *attr, size_t stack_size);


extern int          pthread_create(pthread_t * restrict thread,
                                   const pthread_attr_t * restrict attr,
                                   void *(*start_routine)(void *), void *arg);
extern int          pthread_detach(pthread_t thread);
extern int          pthread_equal(pthread_t thread_1, pthread_t thread_2);
extern void         pthread_exit(void *exit_value);
extern int          pthread_join(pthread_t thread, void **exit_value);
extern pthread_t    pthread_self(void);
extern int          pthread_setcancelstate(int new_state, int *old_state);
extern int          pthread_setcanceltype(int new_type, int *old_type);


/* This is called from DllMain() for DLL_THREAD_DETACH */
extern void pthread_finalize_thread_np(void);


#define PTHREAD_MUTEX_NORMAL            0
#define PTHREAD_MUTEX_ERRORCHECK        1
#define PTHREAD_MUTEX_RECURSIVE         2
#define PTHREAD_MUTEX_DEFAULT           PTHREAD_MUTEX_NORMAL


typedef struct pthread_mutexattr_s {
    int                                 type;
} pthread_mutexattr_t;

extern int pthread_mutexattr_destroy(pthread_mutexattr_t *attr);
extern int pthread_mutexattr_gettype(const pthread_mutexattr_t * restrict attr,
                                     int * restrict type);
extern int pthread_mutexattr_init(pthread_mutexattr_t *attr);
extern int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type);


#ifndef __SKYTHON_WIN32_PTHREAD_C__
/* This is intentionally incomplete. Enough for an initializer only. */
struct pthread_mutex_s {
    sky_once_t                          once_init;
    char                                opaque[24];
};
#endif
#define PTHREAD_MUTEX_INITIALIZER       { SKY_ONCE_INITIALIZER }

extern int pthread_mutex_destroy(pthread_mutex_t *mutex);
extern int pthread_mutex_init(pthread_mutex_t * restrict mutex,
                              const pthread_mutexattr_t * restrict attr);
extern int pthread_mutex_lock(pthread_mutex_t *mutex);
extern int pthread_mutex_timedlock_np(pthread_mutex_t * restrict mutex,
                                      const struct timespec * restrict timeout);
extern int pthread_mutex_trylock(pthread_mutex_t *mutex);
extern int pthread_mutex_unlock(pthread_mutex_t *mutex);


typedef struct pthread_condattr_s {
    int                                 dummy;
} pthread_condattr_t;

extern int pthread_condattr_destroy(pthread_condattr_t *attr);
extern int pthread_condattr_init(pthread_condattr_t *attr);


#ifndef __SKYTHON_WIN32_PTHREAD_C__
/* This is intentionally incomplete. Enough for an initializer only. */
struct pthread_cond_s {
    sky_once_t                          once_init;
    char                                opaque[16];
};
#define PTHREAD_COND_INITIALIZER        { SKY_ONCE_INITIALIZER }
#endif

extern int pthread_cond_broadcast(pthread_cond_t *cond);
extern int pthread_cond_destroy(pthread_cond_t *cond);
extern int pthread_cond_init(pthread_cond_t * restrict cond,
                             const pthread_condattr_t * restrict attr);
extern int pthread_cond_signal(pthread_cond_t *cond);
extern int pthread_cond_timedwait(pthread_cond_t * restrict cond,
                                  pthread_mutex_t * restrict mutex,
                                  const struct timespec * restrict timeout);
extern int pthread_cond_wait(pthread_cond_t * restrict cond,
                             pthread_mutex_t * restrict mutex);


SKY_CDECLS_END


#endif  /* __SKYTHON_WIN32_PTHREAD_H__ */
