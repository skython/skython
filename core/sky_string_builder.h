/** @file
  * @brief
  * String builder objects are subtypes of strings that are mutable. They're
  * intended to help build complex strings while minimizing the amount of
  * copying that using immutable strings to achieve the same end would require.
  * At this time, they are intended to be used from C code only.
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_string String Objects
  * @{
  * @defgroup sky_string_builder String Builder Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_STRING_BUILDER_H__
#define __SKYTHON_CORE_SKY_STRING_BUILDER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new, empty string builder object.
  *
  * @return     a new string builder object instance that contains no data.
  */
SKY_EXTERN sky_string_builder_t
sky_string_builder_create(void);


/** Create a new, empty string builder object with space pre-allocated.
  *
  * @param[in]  capacity    the amount of space to pre-allocate.
  * @return     a new string builder object instance that contains no data.
  */
SKY_EXTERN sky_string_builder_t
sky_string_builder_createwithcapacity(ssize_t capacity);


/** Append the contents of a string to the end of a string builder.
  *
  * @param[in]  builder     the string builder to which @a string is to be
  *                         appended.
  * @param[in]  string      the string to append to @a builder.
  */
SKY_EXTERN void
sky_string_builder_append(sky_string_builder_t builder, sky_string_t string);


/** Append an array of encoded bytes to the end of a string builder.
  * The contents of @a bytes will be decoded using @a encoding, handling errors
  * in the manner prescribed by @a errors.
  *
  * @param[in]  builder     the string builder to which the decoded data is to
  *                         be appended.
  * @param[in]  bytes       an array of bytes containing encoded data.
  * @param[in]  nbytes      the number of bytes present in @a bytes.
  * @param[in]  encoding    the encoding to use to decode @a bytes. May be
  *                         @c NULL, in which case "utf-8" is used.
  * @param[in]  errors      how to handle errors. May be @c NULL, in which case
  *                         the default is "strict".
  */
SKY_EXTERN void
sky_string_builder_appendbytes(sky_string_builder_t builder,
                               const void *         bytes,
                               size_t               nbytes,
                               sky_string_t         encoding,
                               sky_string_t         errors);


/** Append a single Unicode code point to the end of a string builder.
  *
  * @param[in]  builder     the string builder to which the code point is to be
  *                         appended.
  * @param[in]  codepoint   the code point to append to @a builder.
  * @param[in]  count       the number of times to append @a codepoint to
  *                         @a builder.
  */
SKY_EXTERN void
sky_string_builder_appendcodepoint(sky_string_builder_t builder,
                                   sky_unicode_char_t   codepoint,
                                   ssize_t              count);


/** Append an array of Unicode code points to the end of a string builder.
  *
  * @param[in]  builder     the string builder to which the code point is to be
  *                         appended.
  * @param[in]  codepoints  the code points to append to @a builder.
  * @param[in]  ncodepoints the number of code points in @a codepoints.
  * @param[in]  width       the width of the code points in @a codepoints. Must
  *                         be one of 1, 2, or 4. The code points are assumed
  *                         to be in the native endian.
  */
SKY_EXTERN void
sky_string_builder_appendcodepoints(sky_string_builder_t    builder,
                                    const void *            codepoints,
                                    ssize_t                 ncodepoints,
                                    size_t                  width);


/** Append a filename to the end of a string builder.
  * The appropriate operating system encoding and error handler will be used to
  * encode the filename into Unicode for appending onto the end of the string
  * builder.
  *
  * @param[in]  builder     the string builder to which the encoded data is to
  *                         be written.
  * @param[in]  bytes       an array of bytes containing the filename to be
  *                         encoded.
  * @param[in]  nbytes      the number of bytes in @a bytes to be considered
  *                         for encoding.
  */
SKY_EXTERN void
sky_string_builder_appendfilename(sky_string_builder_t  builder,
                                  const void *          bytes,
                                  size_t                nbytes);


/** Append a formatted string to the end of a string builder.
  * The data resulting from the call to sky_format_output() is decoded as
  * UTF-8 with strict handling of errors.
  *
  * @param[in]  builder     the string builder to which the formatted data is
  *                         to be written.
  * @param[in]  format      the format string to use.
  * @param[in]  ...         additional argument as required by the format
  *                         string.
  */
SKY_EXTERN void
sky_string_builder_appendformat(sky_string_builder_t    builder,
                                const char *            format,
                                ...);


/** Append a formatted string to the end of a string builder.
  * The data resulting from the call to sky_format_output() is decoded as
  * UTF-8 with strict handling of errors.
  *
  * @param[in]  builder     the string builder to which the formatted data is
  *                         to be written.
  * @param[in]  format      the format string to use.
  * @param[in]  ap          additional argument as required by the format
  *                         string.
  */
SKY_EXTERN void
sky_string_builder_appendformatv(sky_string_builder_t   builder,
                                 const char *           format,
                                 va_list                ap);


/** Append sequence of strings to the end of a string builder.
  * This is an in-place implementation of separator.join(iterable).
  *
  * @param[in]  builder     the string builder to which the strings are to be
  *                         appended.
  * @param[in]  iterable    an iterable providing a sequence of strings to be
  *                         appended.
  * @param[in]  separator   a string to be appended inbetween each of the
  *                         appended strings. May be @c NULL, in which case
  *                         @c sky_string_empty will be used.
  */
SKY_EXTERN void
sky_string_builder_appendjoined(sky_string_builder_t    builder,
                                sky_object_t            iterable,
                                sky_string_t            separator);


/** Append a slice of a string to the end of a string builder.
  *
  * @param[in]  builder     the string builder to which the strings are to be
  *                         appended.
  * @param[in]  string      the string to be sliced.
  * @param[in]  length      the length of the slice.
  * @param[in]  start       the starting position of the slice.
  * @param[in]  step        the step of the slice.
  */
SKY_EXTERN void
sky_string_builder_appendslice(sky_string_builder_t builder,
                               sky_string_t         string,
                               ssize_t              length,
                               ssize_t              start,
                               ssize_t              step);


/** Create a copy of a string builder object.
  *
  * @param[in]  builder     the string bulider to copy.
  * @return     a new string builder object instance that is a copy of
  *             @a builder.
  */
SKY_EXTERN sky_string_builder_t
sky_string_builder_copy(sky_string_builder_t builder);


/** Finalize a string builder, converting it into an immutable string object.
  * The string builder is converted into a string object, rather than creating
  * a new string object using the data from the string builder. Once the string
  * builder is frozen, it can never be mutated again.
  *
  * @param[in]  builder     the string builder to finalize.
  * @return     as a convenience, a pointer to @a builder cast as
  *             @c sky_string_t instead of @c sky_string_builder_t.
  */
SKY_EXTERN sky_string_t
sky_string_builder_finalize(sky_string_builder_t builder);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_STRING_BUILDER_H__ */

/** @} **/
/** @} **/
/** @} **/
