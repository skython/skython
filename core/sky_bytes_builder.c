#include "sky_private.h"
#include "sky_bytes_private.h"


SKY_EXTERN_INLINE sky_bytestring_t *
sky_bytes_builder_data(sky_object_t object)
{
    sky_bytes_data_t    *bytes_data;

    if (!sky_object_isa(object, sky_bytes_builder_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected %#@; got %#@",
                               sky_type_name(sky_bytes_builder_type),
                               sky_type_name(sky_object_type(object)));
    }
    bytes_data = sky_object_data(object, sky_bytes_type);
    return &(bytes_data->bytes);
}


void
sky_bytes_builder_appendbytes(sky_bytes_builder_t   builder,
                              const void *          bytes,
                              ssize_t               nbytes)
{
    sky_bytestring_appendbytes(sky_bytes_builder_data(builder), bytes, nbytes);
}


static void
sky_bytes_builder_appendformat_output(const void *  bytes,
                                      ssize_t       nbytes,
                           SKY_UNUSED size_t        width,
                                      void *        arg)
{
    sky_bytestring_appendbytes(arg, bytes, nbytes);
}


void
sky_bytes_builder_appendformat(sky_bytes_builder_t  builder,
                               const char *         format,
                               ...)
{
    va_list ap;

    va_start(ap, format);
    sky_format_voutput(sky_bytes_builder_appendformat_output,
                       sky_bytes_builder_data(builder),
                       format,
                       ap);
    va_end(ap);
}


void
sky_bytes_builder_appendformatv(sky_bytes_builder_t builder,
                                const char *        format,
                                va_list             ap)
{
    sky_format_voutput(sky_bytes_builder_appendformat_output,
                       sky_bytes_builder_data(builder),
                       format,
                       ap);
}


sky_bytes_builder_t
sky_bytes_builder_copy(sky_bytes_builder_t builder)
{
    sky_bytestring_t    *bytestring = sky_bytes_builder_data(builder);

    sky_bytes_builder_t new_builder;

    new_builder = sky_object_allocate(sky_bytes_builder_type);
    sky_bytestring_initfrombytes(sky_bytes_builder_data(new_builder),
                                 bytestring->bytes,
                                 bytestring->size,
                                 bytestring->size);

    return new_builder;
}


sky_bytes_builder_t
sky_bytes_builder_create(void)
{
   return sky_object_allocate(sky_bytes_builder_type);
}


sky_bytes_builder_t
sky_bytes_builder_createwithcapacity(ssize_t capacity)
{
    sky_bytes_builder_t     builder;
    sky_bytestring_t        *bytestring;

    builder = sky_object_allocate(sky_bytes_builder_type);
    if (capacity > 0) {
        bytestring = sky_bytes_builder_data(builder);
        sky_bytestring_resize(bytestring, capacity);
    }

    return builder;
}


sky_bytes_t
sky_bytes_builder_finalize(sky_bytes_builder_t builder)
{
    sky_bytestring_t    *bytestring;
    sky_object_data_t   *object_data;

    bytestring = sky_bytes_builder_data(builder);
    object_data = SKY_OBJECT_DATA(builder);
    sky_object_gc_set(SKY_AS_OBJECTP(&(object_data->u.type)),
                      sky_bytes_type,
                      builder);

    if (0 == bytestring->used) {
        return sky_bytes_empty;
    }
    if (1 == bytestring->used) {
        return SKY_BYTES_SINGLE(bytestring->bytes[0]);
    }

    return (sky_bytes_t)builder;
}


SKY_TYPE_DEFINE_SIMPLE(bytes_builder,
                       "bytes_builder",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_bytes_builder_initialize_library(void)
{
    /* bytes_builder objects are mutable; therefore, they are not hashable */
    sky_type_setattr_builtin(sky_bytes_builder_type, "__hash__", sky_None);
}
