/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_generator Generator objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_GENERATOR_H__
#define __SKYTHON_CORE_SKY_GENERATOR_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Close a generator.
  *
  * @param[in]  generator   the generator to close.
  */
SKY_EXTERN void
sky_generator_close(sky_generator_t generator);


/** Create a new generator object instance with a frame.
  *
  * @param[in]  frame   the frame to use with the generator.
  * @return     the new generator object instance.
  */
SKY_EXTERN sky_generator_t
sky_generator_createwithframe(sky_frame_t frame);


/** Send a value to a generator.
  *
  * @param[in]  generator   the generator to receive the value.
  * @param[in]  value       the value to be sent to @a generator.
  * @return     the value returned by the generator.
  */
SKY_EXTERN sky_object_t
sky_generator_send(sky_generator_t generator, sky_object_t value);


/** Throw an exception in a generator.
  *
  * @param[in]  generator   the generator to throw the exception.
  * @param[in]  typ         the exception type to be thrown.
  * @param[in]  val         the exception value to be thrown.
  * @param[in]  tb          the exception traceback to be thrown.
  * @return     the value returned by the generator.
  */
SKY_EXTERN sky_object_t
sky_generator_throw(sky_generator_t generator,
                    sky_object_t    typ,
                    sky_object_t    val,
                    sky_object_t    tb);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_GENERATOR_H__ */


/** @} **/
/** @} **/
