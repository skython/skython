#include "sky_private.h"
#include "sky_mutex_private.h"

#include <math.h>


static void
sky_mutex_instance_atfork_child(sky_object_t self)
{
    sky_mutex_data_t    *self_data = sky_mutex_data(self);

    if (self_data->owner && self_data->owner != sky_thread_self()) {
        /* If the lock is held by a non-existant thread after the fork, make
         * sure it stays held by setting the owner to an invalid pointer. The
         * reason for doing this is that it's possible for another thread to
         * get the same pointer address as the original later on. If the mutex
         * is strict, that the lock is held must be a permanent state of
         * affairs. It's not safe to just unlock the mutex, because the state
         * of what it is protecting is unknown.
         */
        self_data->owner = (sky_thread_t)~(uintptr_t)0;
    }

    /* Prune non-existant threads from the waitlist. */
    sky_thread_waitlist_reset(&(self_data->waitlist), sky_thread_wait());
}

static void
sky_mutex_instance_finalize(sky_object_t self, void *data)
{
    sky_mutex_data_t    *self_data = data;

    sky_object_atfork_unregister(self);
    sky_thread_waitlist_cleanup(&(self_data->waitlist));
}

static void
sky_mutex_instance_initialize(sky_object_t self, void *data)
{
    sky_mutex_data_t    *self_data = data;

    sky_thread_waitlist_init(&(self_data->waitlist));
    sky_object_atfork_register(self,
                               NULL,
                               NULL,
                               sky_mutex_instance_atfork_child);
}

static void
sky_mutex_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_mutex_data_t    *self_data = data;

    sky_object_visit(self_data->owner, visit_data);
}


static void
sky_mutex_setup(sky_mutex_data_t *  self_data,
                sky_bool_t          reentrant,
                sky_bool_t          strict)
{
    self_data->owner = NULL;
    self_data->count = 0;
    self_data->reentrant = reentrant;
    self_data->strict = strict;
}


sky_mutex_t
sky_mutex_create(sky_bool_t reentrant, sky_bool_t strict)
{
    sky_mutex_t self;

    self = sky_object_allocate(sky_mutex_type);
    sky_mutex_setup(sky_mutex_data(self), reentrant, strict);

    return self;
}


sky_object_t
sky_mutex_enter(sky_mutex_t self)
{
    return (sky_mutex_lock(self, SKY_TIME_INFINITE) ? sky_True : sky_False);
}


sky_bool_t
sky_mutex_exit(
                    sky_mutex_t     self,
        SKY_UNUSED  sky_type_t      exc_type,
        SKY_UNUSED  sky_object_t    exc_value,
        SKY_UNUSED  sky_object_t    exc_traceback)
{
    sky_mutex_unlock(self);
    return SKY_FALSE;
}


sky_bool_t
sky_mutex_islocked(sky_mutex_t self)
{
    return (sky_mutex_data(self)->owner ? SKY_TRUE : SKY_FALSE);
}

static const char sky_mutex_islocked_doc[] =
"islocked() -> bool\n\n"
"Returns True if the mutex is locked, or False if it is not.";


sky_bool_t
sky_mutex_isowned(sky_mutex_t self)
{
    return (sky_mutex_data(self)->owner == sky_thread_self() ? SKY_TRUE
                                                             : SKY_FALSE);
}

static const char sky_mutex_isowned_doc[] =
"isowned() -> bool\n\n"
"Returns True if the mutex is locked by the calling thread.";


static sky_bool_t
sky_mutex_lock_acquire(sky_mutex_t self)
{
    sky_mutex_data_t    *self_data = sky_mutex_data(self);

    if (!sky_atomic_cas(NULL,
                        sky_thread_self(),
                        SKY_AS_VOIDP(&(self_data->owner))))
    {
        return SKY_FALSE;
    }
    self_data->count = 1;
    return SKY_TRUE;
}

static void
sky_mutex_lock_cleanup(sky_mutex_data_t *self_data)
{
    sky_thread_waitlist_dequeue(&(self_data->waitlist), sky_thread_wait());
}

sky_bool_t
sky_mutex_lock(sky_mutex_t self, sky_time_t timeout)
{
    sky_mutex_data_t    *self_data = sky_mutex_data(self);

    int rc;

    if (sky_thread_self() == self_data->owner) {
        if (self_data->reentrant) {
            if (INT32_MAX == self_data->count) {
                sky_error_raise_string(sky_OverflowError,
                                       "Internal lock count overflowed");
            }
            ++self_data->count;
            return SKY_TRUE;
        }
        if (self_data->strict) {
            sky_error_raise_string(sky_RuntimeError, "deadlock");
        }
    }

    if (sky_mutex_lock_acquire(self)) {
        return SKY_TRUE;
    }
    if (!timeout) {
        return SKY_FALSE;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_thread_waitlist_enqueue(&(self_data->waitlist), sky_thread_wait());
        sky_asset_save(self_data,
                       (sky_free_t)sky_mutex_lock_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if ((rc = sky_thread_wait_condition(
                            NULL,
                            (sky_thread_wait_condition_t)sky_mutex_lock_acquire,
                            self,
                            timeout)) < 0)
        {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;

    if (rc || sky_mutex_lock_acquire(self)) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}

static const char sky_mutex_lock_doc[] =
"lock([timeout]) -> bool\n\n"
"Lock the mutex. If timeout is specified, it may be negative to wait forever,\n"
"0.0 to never wait at all, or a positive value to wait that amount of time.\n"
"The return is True if the mutex was locked successfully, or False if not.";


static sky_bool_t
sky_mutex_lock_wrapper(sky_mutex_t self, double timeout)
{
    sky_time_t  time_timeout;

    if (timeout < 0.0) {
        time_timeout = SKY_TIME_INFINITE;
    }
    else {
        double  floatpart, intpart;

        floatpart = modf(timeout, &intpart);
        time_timeout = ((uint64_t)intpart * UINT64_C(1000000000)) +
                       ((uint64_t)(floatpart * 1000000000.0));
    }

    return sky_mutex_lock(self, time_timeout);
}


sky_mutex_t
sky_mutex_new(sky_type_t cls, sky_bool_t reentrant, sky_bool_t strict)
{
    sky_mutex_t self;

    self = sky_object_allocate(cls);
    sky_mutex_setup(sky_mutex_data(self), reentrant, strict);

    return self;
}


void
sky_mutex_unlock(sky_mutex_t self)
{
    sky_mutex_data_t    *self_data = sky_mutex_data(self);

    if (!self_data->owner) {
        sky_error_raise_string(sky_RuntimeError,
                               "cannot release un-acquired lock");
    }
    if ((self_data->reentrant || self_data->strict) &&
        sky_thread_self() != self_data->owner)
    {
        sky_error_raise_string(sky_RuntimeError,
                               "cannot release un-acquired lock");
    }
    sky_error_validate_debug(self_data->count > 0);
    if (!--self_data->count) {
        self_data->owner = NULL;
        sky_thread_waitlist_signal(&(self_data->waitlist));
    }
}

static const char sky_mutex_unlock_doc[] =
"unlock() -> None\n\n"
"Unlock the mutex. If the mutex is strict, the calling thread must be the\n"
"thread owning the lock. If the mutex is reentrant, one unlock for each\n"
"lock must be done before other threads may lock the mutex.";


static const char sky_mutex_type_doc[] =
"mutex(reentrant=False, strict=True) -> new mutex object\n\n"
"Creates a new mutex object, which is a mutual exclusion lock used for\n"
"synchronization across multiple threads. Only a single thread may lock\n"
"the mutex at a time. Other threads wanting to lock the mutex must wait\n"
"until the mutex is not locked.\n"
"\n"
"If reentrant is True, the mutex may be locked multiple times by the same\n"
"thread. For each lock, there must be a matching unlock before another\n"
"thread may obtain the lock.\n"
"\n"
"If strict is True, an attempt to lock a non-reentrant lock by the thread\n"
"that locked the mutex will raise an exception; otherwise, a deadlock will\n"
"occur. Additionally, the thread that locked the mutex must be the same\n"
"thread to unlock the mutex.";


SKY_TYPE_DEFINE_SIMPLE(mutex,
                       "mutex",
                       sizeof(sky_mutex_data_t),
                       sky_mutex_instance_initialize,
                       sky_mutex_instance_finalize,
                       sky_mutex_instance_visit,
                       0,
                       sky_mutex_type_doc);


void
sky_mutex_initialize_library(void)
{
    sky_type_initialize_builtin(sky_mutex_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_mutex_type,
            "__new__",
            (sky_native_code_function_t)sky_mutex_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "reentrant", SKY_DATA_TYPE_BOOL, sky_False,
            "strict", SKY_DATA_TYPE_BOOL, sky_True,
            NULL);

    sky_type_setmethodslots(sky_mutex_type,
            "__enter__", sky_mutex_enter,
            "__exit__", sky_mutex_exit,
            NULL);

    sky_type_setattr_builtin(
            sky_mutex_type,
            "islocked",
            sky_function_createbuiltin(
                    "mutex.islocked",
                    sky_mutex_islocked_doc,
                    (sky_native_code_function_t)sky_mutex_islocked,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mutex_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_mutex_type,
            "isowned",
            sky_function_createbuiltin(
                    "mutex.isowned",
                    sky_mutex_isowned_doc,
                    (sky_native_code_function_t)sky_mutex_isowned,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mutex_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_mutex_type,
            "lock",
            sky_function_createbuiltin(
                    "mutex.lock",
                    sky_mutex_lock_doc,
                    (sky_native_code_function_t)sky_mutex_lock_wrapper,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mutex_type,
                    "timeout", SKY_DATA_TYPE_DOUBLE, sky_float_create(-1.0),
                    NULL));
    sky_type_setattr_builtin(
            sky_mutex_type,
            "unlock",
            sky_function_createbuiltin(
                    "mutex.unlock",
                    sky_mutex_unlock_doc,
                    (sky_native_code_function_t)sky_mutex_unlock,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_mutex_type,
                    NULL));
}
