/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_iterator Iterators
  * @{
  * @defgroup sky_callable_iterator Callable Iterators
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_CALLABLE_ITERATOR_H__
#define __SKYTHON_CORE_SKY_CALLABLE_ITERATOR_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new callable iterator object instance.
  * The iterator that is created calls @a callable for each iteration. If the
  * object returned by @a callable is equal to @a sentinel, then
  * @c sky_StopIteration will be raised; otherwise, the object will be
  * returned. @a callable may, of course, raise @c sky_StopIteration itself to
  * end iteration.
  *
  * @param[in]  callable    the callable to be called to obtain the next item.
  *                         It is called with no arguments.
  * @param[in]  sentinel    the sentinel value indicating end of iteration.
  * @return     a new callable iterator object instance.
  */
SKY_EXTERN sky_callable_iterator_t
sky_callable_iterator_create(sky_object_t callable, sky_object_t sentinel);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CALLABLE_ITERATOR_H__ */

/** @} **/
/** @} **/
/** @} **/
