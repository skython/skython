#include "sky_private.h"


static void *       sky_malloc_calloc_hook      (size_t count, size_t nbytes);
static sky_bool_t   sky_malloc_malloc_idle_hook (void);
static void *       sky_malloc_memdup_hook      (const void *bytes, size_t nbytes);
static char *       sky_malloc_strdup_hook      (const char *string);
static char *       sky_malloc_strndup_hook     (const char *string, size_t string_length);


static sky_calloc_t         sky_malloc_calloc       =   sky_malloc_calloc_hook;
static sky_free_t           sky_malloc_free         =   NULL;
static sky_malloc_t         sky_malloc_malloc       =   NULL;
static sky_malloc_idle_t    sky_malloc_malloc_idle  =   sky_malloc_malloc_idle_hook;
static sky_memalign_t       sky_malloc_memalign     =   NULL;
static sky_memdup_t         sky_malloc_memdup       =   sky_malloc_memdup_hook;
static sky_memsize_t        sky_malloc_memsize      =   NULL;
static sky_realloc_t        sky_malloc_realloc      =   NULL;
static sky_strdup_t         sky_malloc_strdup       =   sky_malloc_strdup_hook;
static sky_strndup_t        sky_malloc_strndup      =   sky_malloc_strndup_hook;


#if !defined(NDEBUG)
static void *
sky_malloc_allocation_breakpoint(void *pointer)
{
    return pointer;
}
#else
#   define sky_malloc_allocation_breakpoint(pointer)    pointer
#endif


static void *
sky_malloc_calloc_hook(size_t count, size_t nbytes)
{
    void    *pointer;

    nbytes *= count;
    if (likely((pointer = sky_malloc_malloc(nbytes)) != NULL)) {
        memset(pointer, 0x00, nbytes);
    }

    return pointer;
}


static sky_bool_t
sky_malloc_malloc_idle_hook(void)
{
    return SKY_FALSE;
}


static void *
sky_malloc_memdup_hook(const void *bytes, size_t nbytes)
{
    void    *new_bytes;

    if (likely((new_bytes = sky_malloc_malloc(nbytes)) != NULL)) {
        memcpy(new_bytes, bytes, nbytes);
    }

    return new_bytes;
}


static char *
sky_malloc_strdup_hook(const char *string)
{
    char    *new_string;
    size_t  string_length;

    string_length = strlen(string) + 1;
    if (likely((new_string = sky_malloc_malloc(string_length)) != NULL)) {
        memcpy(new_string, string, string_length);
    }

    return new_string;
}


static char *
sky_malloc_strndup_hook(const char *string, size_t string_length)
{
    char        *new_string, *outc;
    const char  *c, *end;

    if (likely((new_string = outc = sky_malloc_malloc(string_length + 1)) != NULL)) {
        for (end = (c = string) + string_length;  c < end && *c;  c++) {
            *outc++ = *c;
        }
        *outc = '\0';
    }

    return new_string;
}


void
sky_malloc_sethooks(sky_malloc_t        malloc_hook,
                    sky_free_t          free_hook,
                    sky_realloc_t       realloc_hook,
                    sky_calloc_t        calloc_hook,
                    sky_memdup_t        memdup_hook,
                    sky_strdup_t        strdup_hook,
                    sky_strndup_t       strndup_hook,
                    sky_memalign_t      memalign_hook,
                    sky_memsize_t       memsize_hook,
                    sky_malloc_idle_t   malloc_idle_hook)
{
    if (!malloc_hook || !free_hook || !realloc_hook || !memalign_hook) {
        sky_error_fatal("malloc/free/realloc/memalign hooks cannot be NULL");
    }

    sky_malloc_calloc = (calloc_hook ? calloc_hook : sky_malloc_calloc_hook);
    sky_malloc_free = free_hook;
    sky_malloc_malloc = malloc_hook;
    sky_malloc_malloc_idle = (malloc_idle_hook ? malloc_idle_hook : sky_malloc_malloc_idle_hook);
    sky_malloc_memalign = memalign_hook;
    sky_malloc_memdup = (memdup_hook ? memdup_hook : sky_malloc_memdup_hook);
    sky_malloc_memsize = memsize_hook;
    sky_malloc_realloc = realloc_hook;
    sky_malloc_strdup = (strdup_hook ? strdup_hook : sky_malloc_strdup_hook);
    sky_malloc_strndup = (strndup_hook ? strndup_hook : sky_malloc_strndup_hook);
}


void *
sky_calloc(size_t count, size_t nbytes)
{
    void    *pointer;

    sky_error_validate_debug(count > 0);
    sky_error_validate_debug(nbytes > 0);
    sky_error_validate_debug(SIZE_MAX / count >= nbytes);
    if (unlikely(!(pointer = sky_malloc_calloc(count, nbytes)))) {
        sky_error_fatal("out of memory");
    }
    sky_error_validate_debug(!((intptr_t)pointer & 15));

    return sky_malloc_allocation_breakpoint(pointer);
}


void
sky_free(void *pointer)
{
    /* There's no validation of arguments to be done here.
     * It is legal to pass a NULL pointer.
     */
    if (likely(pointer)) {
        sky_error_validate_debug(!((intptr_t)pointer & 15));
        sky_malloc_free(pointer);
    }
}


void *
sky_malloc(size_t nbytes)
{
    void    *pointer;

    sky_error_validate_debug(nbytes > 0);
    if (unlikely(!(pointer = sky_malloc_malloc(nbytes)))) {
        sky_error_fatal("out of memory");
    }
    sky_error_validate_debug(!((intptr_t)pointer & 15));

    return sky_malloc_allocation_breakpoint(pointer);
}


sky_bool_t
sky_malloc_idle(void)
{
    return sky_malloc_malloc_idle();
}


void *
sky_memalign(size_t alignment, size_t nbytes)
{
    void    *pointer;

    sky_error_validate_debug(nbytes > 0);
    sky_error_validate_debug(alignment >= 16);
    sky_error_validate_debug(sky_util_roundpow2(alignment) == alignment);
    sky_error_validate_debug(alignment < sky_system_pagesize());

    if (unlikely(!(pointer = sky_malloc_memalign(alignment, nbytes)))) {
        sky_error_fatal("out of memory");
    }
    sky_error_validate_debug(!((intptr_t)pointer & (alignment - 1)));

    return sky_malloc_allocation_breakpoint(pointer);
}


void *
sky_memdup(const void *bytes, size_t nbytes)
{
    void    *new_bytes;

    /* There's no validation of arguments to be done here.
     * It is legal to pass a NULL pointer for bytes, and it is legal to pass 0
     * for nbytes. In both cases, the return value is NULL.
     */

    if (unlikely(unlikely(!bytes) || unlikely(!nbytes))) {
        return NULL;
    }
    if (unlikely(!(new_bytes = sky_malloc_memdup(bytes, nbytes)))) {
        sky_error_fatal("out of memory");
    }
    sky_error_validate_debug(!((intptr_t)new_bytes & 15));

    return sky_malloc_allocation_breakpoint(new_bytes);
}


size_t
sky_memsize(const void *pointer)
{
    if (unlikely(!pointer)) {
        return 0;
    }
    return sky_malloc_memsize(pointer);
}


void *
sky_realloc(void *old_pointer, size_t new_nbytes)
{
    void    *new_pointer;

    sky_error_validate_debug(new_nbytes > 0);
    if (!old_pointer) {
        new_pointer = sky_malloc_malloc(new_nbytes);
    }
    else {
        sky_error_validate_debug(!((intptr_t)old_pointer & 15));
        new_pointer = sky_malloc_realloc(old_pointer, new_nbytes);
    }
    if (unlikely(!new_pointer)) {
        sky_error_fatal("out of memory");
    }

    return sky_malloc_allocation_breakpoint(new_pointer);
}


char *
sky_strdup(const char *string)
{
    char    *new_string;

    /* There's no validation of arguments to be done here.
     * It is legal to pass * a NULL pointer for string.
     */

    if (unlikely(!string)) {
        return NULL;
    }
    if (unlikely(!(new_string = sky_malloc_strdup(string)))) {
        sky_error_fatal("out of memory");
    }

    return sky_malloc_allocation_breakpoint(new_string);
}


char *
sky_strndup(const char *string, size_t string_length)
{
    char    *new_string;

    /* There's no validation of arguments to be done here.
     * It is legal to pass a NULL pointer for string, and it is legal to pass 0
     * for string_length. In the former case, the return value is NULL. In the
     * latter case, the return value is a newly allocated empty string.
     *
     * XXX: Is it worthwhile to return an empty string constant that sky_free()
     *      also recognizes as being special?
     */

    if (unlikely(!string)) {
        return NULL;
    }
    if (unlikely(!(new_string = sky_malloc_strndup(string, string_length)))) {
        sky_error_fatal("out of memory");
    }

    return sky_malloc_allocation_breakpoint(new_string);
}
