#include "sky_core_malloc_private.h"
#include "sky_private.h"


/* Notes:
 *  --  For slab movement, it would be better if the slabs were maintained in
 *      order of emptiness. That is, keep the fuller ones closer to the head,
 *      but completely full ones still at the tail. This way fuller ones stay
 *      fuller, and emptier ones are more likely to become completely empty and
 *      released back to the operating system.
 *  --  The same goes for slabs_head/slabs_tail management, although right now
 *      there's no attempt to keep them in any sort of order at all. I've
 *      dropped the ball here, but I've done so figuring that slab chunk alloc
 *      and release should not be happening that frequently as to make any real
 *      noticeable difference.
 */


/* The one-shot initializer for handling the core malloc package initialization.
 * When this fires via sky_once_run(), it must run
 * sky_core_malloc_once_initialize().
 */
static sky_once_t sky_core_malloc_once_initializer = SKY_ONCE_INITIALIZER;


/* This is the page map. On 32-bit architectures, it is a flat array with one
 * entry per page. On 64-bit architectures, it is a tiered structure consisting
 * of three tiers. The first tier points to the second, the second points to
 * the third, and the third contains the data entries. Assuming a 4096-byte
 * page size on a 64-bit platform, the first tier will contain (1 << 18) entries
 * for the upper 18 bits of an address, the second tier will contain (1 << 17)
 * entries for the middle 17 bits of an address, and the third tier will contain
 * (1 << 17) entries for the lower 17 bits. Because the page size is 4096 bytes
 * in this example, the lowest 12 bits are not important, because they will
 * always be 0.
 */
#if defined(SKY_ARCH_32BIT)
static uintptr_t *sky_core_malloc_page_map;
#else
static uintptr_t ***sky_core_malloc_page_map;
#endif

#if !defined(SKY_CORE_MALLOC_STATIC_SIZES)
#   if !defined(SKY_ARCH_32BIT)
static uintptr_t    sky_core_malloc_page_map_bits_0,
                    sky_core_malloc_page_map_bits_1,
                    sky_core_malloc_page_map_bits_2;
static uintptr_t    sky_core_malloc_page_map_mask_0,
                    sky_core_malloc_page_map_mask_1,
                    sky_core_malloc_page_map_mask_2;
static uintptr_t    sky_core_malloc_page_map_shift_0,
                    sky_core_malloc_page_map_shift_1,
                    sky_core_malloc_page_map_shift_2;
#   endif

static size_t   sky_core_malloc_page_size;
static size_t   sky_core_malloc_page_size_bits;

/* Anything greater than this size after rounding up to the nearest multiple of
 * 16 is no longer considered a small allocation.
 */
static const size_t sky_core_malloc_small_size_max =
                            SKY_CORE_MALLOC_SMALL_SIZE_MAX;
static const size_t sky_core_malloc_heap_size =
                            SKY_CORE_MALLOC_HEAP_SIZE;
static const size_t sky_core_malloc_huge_page_size =
                            SKY_CORE_MALLOC_HUGE_PAGE_SIZE;

#else

#   if defined(SKY_ARCH_64BIT)
#       define sky_core_malloc_page_map_bits_0  (uintptr_t)(1 << 18)
#       define sky_core_malloc_page_map_bits_1  (uintptr_t)(1 << 17)
#       define sky_core_malloc_page_map_bits_2  (uintptr_t)(1 << 17)
#       define sky_core_malloc_page_map_mask_0  (uintptr_t)0x3ffff
#       define sky_core_malloc_page_map_mask_1  (uintptr_t)0x1ffff
#       define sky_core_malloc_page_map_mask_2  (uintptr_t)0x1ffff
#       define sky_core_malloc_page_map_shift_0 (uintptr_t)46
#       define sky_core_malloc_page_map_shift_1 (uintptr_t)29
#       define sky_core_malloc_page_map_shift_2 (uintptr_t)12
#   endif

#   define sky_core_malloc_page_size            4096
#   define sky_core_malloc_page_size_bits       12

#   define sky_core_malloc_small_size_max       SKY_CORE_MALLOC_SMALL_SIZE_MAX
#   define sky_core_malloc_heap_size            SKY_CORE_MALLOC_HEAP_SIZE
#   define sky_core_malloc_huge_page_size       SKY_CORE_MALLOC_HUGE_PAGE_SIZE

#endif


static void
sky_core_malloc_tlsdata_localize(sky_core_malloc_tlsdata_t *tlsdata);


static inline uintptr_t
sky_core_malloc_page_value(const void *address)
{
#if defined(SKY_ARCH_32BIT)
    uintptr_t   page_index;

    page_index = ((uintptr_t)address & ~(sky_core_malloc_page_size - 1)) >>
                 sky_core_malloc_page_size_bits;
    return sky_core_malloc_page_map[page_index];
#else
    uintptr_t   page_index_0, page_index_1, page_index_2;

    page_index_0 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_0) &
                   sky_core_malloc_page_map_mask_0;
    page_index_1 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_1) &
                   sky_core_malloc_page_map_mask_1;
    page_index_2 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_2) &
                   sky_core_malloc_page_map_mask_2;

    return sky_core_malloc_page_map[page_index_0][page_index_1][page_index_2];
#endif
}


static void
sky_core_malloc_page_maskvalue(const void * address,
                               size_t       npages,
                               uintptr_t    value)
{
    static const uintptr_t mask = SKY_CORE_MALLOC_PAGE_FLAG_PRESERVE;

    size_t      i;
#if defined(SKY_ARCH_32BIT)
    uintptr_t   page_index;
#else
    uintptr_t   page_index_0, page_index_1, page_index_2;
    uintptr_t   **page_map_1, *page_map_2;
#endif

#if defined(SKY_ARCH_32BIT)
    page_index = ((uintptr_t)address & ~(sky_core_malloc_page_size - 1)) >>
                 sky_core_malloc_page_size_bits;
    for (i = 0; i < npages; ++i) {
        sky_core_malloc_page_map[page_index + i] =
                (sky_core_malloc_page_map[page_index + i] & mask) | value;
    }
#else
    page_index_0 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_0) &
                   sky_core_malloc_page_map_mask_0;
    page_map_1 = sky_core_malloc_page_map[page_index_0];

    page_index_1 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_1) &
                   sky_core_malloc_page_map_mask_1;
    page_map_2 = page_map_1[page_index_1];

    page_index_2 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_2) &
                   sky_core_malloc_page_map_mask_2;
    page_map_2[page_index_2] = (page_map_2[page_index_2] & mask) | value;
    for (i = 1; i < npages; ++i) {
        if (++page_index_2 == sky_core_malloc_page_map_bits_2) {
            page_index_2 = 0;
            if (++page_index_1 == sky_core_malloc_page_map_bits_1) {
                page_index_1 = 0;
                if (++page_index_0 == sky_core_malloc_page_map_bits_0) {
                    sky_error_fatal("internal error");
                }
                page_map_1 = sky_core_malloc_page_map[page_index_0];
            }
            page_map_2 = page_map_1[page_index_1];
        }
        page_map_2[page_index_2] = (page_map_2[page_index_2] & mask) | value;
    }
#endif
}


static void
sky_core_malloc_page_setvalue(const void *  address,
                              size_t        npages,
                              uintptr_t     value)
{
    size_t      i;
#if defined(SKY_ARCH_32BIT)
    uintptr_t   page_index;
#else
    size_t      map_nbytes;
    uintptr_t   page_index_0, page_index_1, page_index_2;
    uintptr_t   **page_map_1, *page_map_2;
#endif

#if defined(SKY_ARCH_32BIT)
    page_index = ((uintptr_t)address & ~(sky_core_malloc_page_size - 1)) >>
                 sky_core_malloc_page_size_bits;
    for (i = 0; i < npages; ++i) {
        sky_core_malloc_page_map[page_index + i] = value;
    }
#else
    page_index_0 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_0) &
                   sky_core_malloc_page_map_mask_0;
    if (!(page_map_1 = sky_core_malloc_page_map[page_index_0])) {
        map_nbytes = sizeof(uintptr_t *) * sky_core_malloc_page_map_bits_1;
        page_map_1 = memset(sky_system_alloc(map_nbytes), 0x00, map_nbytes);
        if (!sky_atomic_cas(
                NULL,
                page_map_1,
                (void **)&(sky_core_malloc_page_map[page_index_0])))
        {
            sky_system_free(page_map_1, map_nbytes);
            page_map_1 = sky_core_malloc_page_map[page_index_0];
        }
    }

    page_index_1 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_1) &
                   sky_core_malloc_page_map_mask_1;
    if (!(page_map_2 = page_map_1[page_index_1])) {
        map_nbytes = sizeof(uintptr_t) * sky_core_malloc_page_map_bits_2;
        page_map_2 = memset(sky_system_alloc(map_nbytes), 0x00, map_nbytes);
        if (!sky_atomic_cas(
                NULL,
                page_map_2,
                (void **)&(page_map_1[page_index_1])))
        {
            sky_system_free(page_map_2, map_nbytes);
            page_map_2 = page_map_1[page_index_1];
        }
    }

    page_index_2 = ((uintptr_t)address >> sky_core_malloc_page_map_shift_2) &
                   sky_core_malloc_page_map_mask_2;
    page_map_2[page_index_2] = value;
    for (i = 1; i < npages; ++i) {
        if (++page_index_2 == sky_core_malloc_page_map_bits_2) {
            page_index_2 = 0;
            if (++page_index_1 == sky_core_malloc_page_map_bits_1) {
                page_index_1 = 0;
                if (++page_index_0 == sky_core_malloc_page_map_bits_0) {
                    sky_error_fatal("internal error");
                }
                if (!(page_map_1 = sky_core_malloc_page_map[page_index_0])) {
                    map_nbytes = sizeof(uintptr_t *) *
                                 sky_core_malloc_page_map_bits_1;
                    page_map_1 = memset(sky_system_alloc(map_nbytes),
                                        0x00,
                                        map_nbytes);
                    if (!sky_atomic_cas(
                            NULL,
                            page_map_1,
                            (void **)&(sky_core_malloc_page_map[page_index_0])))
                    {
                        sky_system_free(page_map_1, map_nbytes);
                        page_map_1 = sky_core_malloc_page_map[page_index_0];
                    }
                }
            }
            if (!(page_map_2 = page_map_1[page_index_1])) {
                map_nbytes = sizeof(uintptr_t) *
                             sky_core_malloc_page_map_bits_2;
                page_map_2 = memset(sky_system_alloc(map_nbytes),
                                    0x00,
                                    map_nbytes);
                if (!sky_atomic_cas(
                        NULL,
                        page_map_2,
                        (void **)&(page_map_1[page_index_1])))
                {
                    sky_system_free(page_map_2, map_nbytes);
                    page_map_2 = page_map_1[page_index_1];
                }
            }
        }
        page_map_2[page_index_2] = value;
    }
#endif
}


static sky_core_malloc_chunk_t *
sky_core_malloc_chunk_insert(sky_core_malloc_tlsdata_t *tlsdata,
                             void *                     pointer,
                             size_t                     nbytes)
{
    sky_core_malloc_chunk_t *chunk, *new_chunk = pointer;

    /* The size of the chunk being inserted must be a multiple of
     * sky_core_malloc_heap_size; otherwise, assumptions made elsewhere will
     * fail, and bad things will happen.
     */
    sky_error_validate_debug(!(nbytes % sky_core_malloc_heap_size));

    for (chunk = tlsdata->free_chunks_head; chunk; chunk = chunk->next_chunk) {
        if (new_chunk < chunk) {
            if ((char *)new_chunk + nbytes == (char *)chunk) {
                new_chunk->chunk_size = chunk->chunk_size + nbytes;
                new_chunk->next_chunk = chunk->next_chunk;
                new_chunk->prev_chunk = chunk->prev_chunk;
                *(chunk->next_chunk ? &(chunk->next_chunk->prev_chunk)
                                    : &(tlsdata->free_chunks_tail)) = new_chunk;
                *(chunk->prev_chunk ? &(chunk->prev_chunk->next_chunk)
                                    : &(tlsdata->free_chunks_head)) = new_chunk;
                return new_chunk;
            }
            new_chunk->chunk_size = nbytes;
            new_chunk->next_chunk = chunk;
            new_chunk->prev_chunk = chunk->prev_chunk;
            *(chunk->prev_chunk ? &(chunk->prev_chunk->next_chunk)
                                : &(tlsdata->free_chunks_head)) = new_chunk;
            chunk->prev_chunk = new_chunk;
            return new_chunk;
        }
        if ((char *)chunk + chunk->chunk_size == (char *)new_chunk) {
            chunk->chunk_size += nbytes;
            if ((char *)chunk + chunk->chunk_size == (char *)chunk->next_chunk) {
                chunk->chunk_size += chunk->next_chunk->chunk_size;
                chunk->next_chunk  = chunk->next_chunk->next_chunk;
                *(chunk->next_chunk ? &(chunk->next_chunk->prev_chunk)
                                    : &(tlsdata->free_chunks_tail)) = chunk;
            }
            return chunk;
        }
    }

    new_chunk->chunk_size = nbytes;
    new_chunk->next_chunk = NULL;
    new_chunk->prev_chunk = tlsdata->free_chunks_tail;
    *(new_chunk->prev_chunk ? &(new_chunk->prev_chunk->next_chunk)
                            : &(tlsdata->free_chunks_head)) = new_chunk;
    tlsdata->free_chunks_tail = new_chunk;
    return new_chunk;
}


static inline void
sky_core_malloc_chunk_append(sky_core_malloc_tlsdata_t *tlsdata,
                             void *                     pointer,
                             size_t                     nbytes)
{
    const sky_core_malloc_chunk_t * const tail = tlsdata->free_chunks_tail;

    sky_core_malloc_chunk_t *chunk = pointer;

    /* The size of the chunk being inserted must be a multiple of
     * sky_core_malloc_heap_size; otherwise, assumptions made elsewhere will
     * fail, and bad things will happen.
     */
    sky_error_validate_debug(!(nbytes % sky_core_malloc_heap_size));

    if (!tail) {
        chunk->chunk_size = nbytes;
        chunk->next_chunk = NULL;
        chunk->prev_chunk = NULL;
        tlsdata->free_chunks_head = tlsdata->free_chunks_tail = chunk;
    }
    else if ((char *)chunk == (char *)tail + tail->chunk_size) {
        tlsdata->free_chunks_tail->chunk_size += nbytes;
    }
    else {
        chunk->chunk_size = nbytes;
        chunk->next_chunk = NULL;
        chunk->prev_chunk = tlsdata->free_chunks_tail;
        tlsdata->free_chunks_tail->next_chunk = chunk;
        tlsdata->free_chunks_tail = chunk;
    }
}


static void
sky_core_malloc_chunk_cleanup(sky_core_malloc_tlsdata_t *tlsdata)
{
    char                        *chunk_pointer;
    size_t                      chunk_size;
    uintptr_t                   page_value;
    sky_core_malloc_chunk_t     *chunk, *next_chunk;

    /* Go through the whole of the thread's free chunk list, releasing any huge
     * pages back to the operating system that can be released.
     * Note that since chunks are only ever broken up in multiples of
     * sky_core_malloc_heap_size, it's only necessary to use that increment
     * rather than a page size increment.
     */
    next_chunk = tlsdata->free_chunks_head;
    tlsdata->free_chunks_head = tlsdata->free_chunks_tail = NULL;
    while ((chunk = next_chunk) != NULL) {
        next_chunk = chunk->next_chunk;
        chunk_size = chunk->chunk_size;
        chunk_pointer = (char *)chunk;

        while (chunk_size >= sky_core_malloc_huge_page_size) {
            page_value = sky_core_malloc_page_value(chunk_pointer);
            if (page_value & SKY_CORE_MALLOC_PAGE_FLAG_FIRST) {
                sky_core_malloc_page_setvalue(
                        chunk_pointer,
                        (sky_core_malloc_huge_page_size >>
                         sky_core_malloc_page_size_bits),
                        0);
                sky_system_free(chunk_pointer, sky_core_malloc_huge_page_size);
                chunk_pointer += sky_core_malloc_huge_page_size;
                chunk_size -= sky_core_malloc_huge_page_size;
            }
            else {
                sky_core_malloc_chunk_append(tlsdata,
                                             chunk_pointer,
                                             sky_core_malloc_heap_size);
                chunk_pointer += sky_core_malloc_heap_size;
                chunk_size -= sky_core_malloc_heap_size;
            }
        }
        if (chunk_size) {
            sky_core_malloc_chunk_append(tlsdata, chunk_pointer, chunk_size);
        }
    }
}


static void *
sky_core_malloc_chunk_acquire(sky_core_malloc_tlsdata_t *tlsdata, size_t nbytes)
{
    void                    *pointer;
    sky_core_malloc_chunk_t *chunk, *first_chunk;

    sky_core_malloc_tlsdata_localize(tlsdata);

    nbytes = (nbytes + sky_core_malloc_heap_size - 1) &
             ~(sky_core_malloc_heap_size - 1);
    for (chunk = first_chunk = NULL; !chunk; first_chunk = NULL) {
        for (chunk = tlsdata->free_chunks_head;
             chunk && chunk->chunk_size != nbytes;
             chunk = chunk->next_chunk)
        {
            if (chunk->chunk_size > nbytes && !first_chunk) {
                first_chunk = chunk;
            }
        }
        if (unlikely(!chunk) && !(chunk = first_chunk)) {
            /* More core memory is needed. First try to see if there's
             * anything available for adoption. If so, adopt and try to use
             * that. Otherwise, allocate directly from the operating system.
             */
            if (!sky_tlsdata_adopt()) {
                chunk = sky_system_alloc(sky_core_malloc_huge_page_size);
                sky_core_malloc_page_setvalue(
                        chunk,
                        1,
                        SKY_CORE_MALLOC_PAGE_FLAG_FIRST);
                sky_core_malloc_page_setvalue(
                        (char *)chunk + sky_core_malloc_page_size,
                        (sky_core_malloc_huge_page_size >>
                         sky_core_malloc_page_size_bits) - 1,
                        0);
                chunk = sky_core_malloc_chunk_insert(
                                tlsdata,
                                chunk,
                                sky_core_malloc_huge_page_size);
            }
        }
    }

    pointer = ((char *)chunk + chunk->chunk_size - nbytes);
    chunk->chunk_size -= nbytes;
    if (!chunk->chunk_size) {
        *(chunk->next_chunk ? &(chunk->next_chunk->prev_chunk)
                            : &(tlsdata->free_chunks_tail)) = chunk->prev_chunk;
        *(chunk->prev_chunk ? &(chunk->prev_chunk->next_chunk)
                            : &(tlsdata->free_chunks_head)) = chunk->next_chunk;
    }

    return pointer;
}


static void
sky_core_malloc_chunk_release(sky_core_malloc_tlsdata_t *tlsdata, void *pointer)
{
    size_t      chunk_size;
    uintptr_t   page_value;

    page_value = sky_core_malloc_page_value(pointer);
    if (!(page_value & SKY_CORE_MALLOC_PAGE_FLAG_LARGE)) {
        chunk_size = sky_core_malloc_heap_size;
    }
    else {
        chunk_size = ((page_value & ~SKY_CORE_MALLOC_PAGE_FLAG_MASK) +
                      sky_core_malloc_heap_size - 1) &
                     ~(sky_core_malloc_heap_size - 1);
    }

    sky_core_malloc_page_maskvalue(pointer,
                                   chunk_size >> sky_core_malloc_page_size_bits,
                                   0);

    sky_core_malloc_chunk_insert(tlsdata, pointer, chunk_size);
}


static void
sky_core_malloc_once_initialize(SKY_UNUSED void *unused)
{
    size_t  nbytes;

#if !defined(SKY_CORE_MALLOC_STATIC_SIZES)
#   if !defined(SKY_ARCH_32BIT)
    size_t  bits[3], pointer_bits = sizeof(void *) * 8;
#   endif

    sky_core_malloc_page_size =
            sky_system_pagesize();
    sky_core_malloc_page_size_bits =
            sky_util_flsl(sky_core_malloc_page_size) - 1;
#endif

#if defined(SKY_ARCH_32BIT)
    nbytes = sizeof(uintptr_t) * (1 << ((sizeof(uintptr_t) * 8) -
                                        sky_core_malloc_page_size_bits));
#else
#   if !defined(SKY_CORE_MALLOC_STATIC_SIZES)
    bits[2] = (pointer_bits - sky_core_malloc_page_size_bits) / 3;
    bits[1] = bits[2];
    bits[0] = pointer_bits - sky_core_malloc_page_size_bits - bits[1] - bits[2];

    sky_core_malloc_page_map_shift_0 = pointer_bits - bits[0];
    sky_core_malloc_page_map_mask_0 = (1 << bits[0]) - 1;
    sky_core_malloc_page_map_bits_0 = 1 << bits[0];

    sky_core_malloc_page_map_shift_1 = sky_core_malloc_page_size_bits + bits[2];
    sky_core_malloc_page_map_mask_1 = (1 << bits[1]) - 1;
    sky_core_malloc_page_map_bits_1 = 1 << bits[1];

    sky_core_malloc_page_map_shift_2 = sky_core_malloc_page_size_bits;
    sky_core_malloc_page_map_mask_2 = (1 << bits[2]) - 1;
    sky_core_malloc_page_map_bits_2 = 1 << bits[2];
#   endif

    nbytes = sizeof(void *) * sky_core_malloc_page_map_bits_0;
#endif
    sky_core_malloc_page_map = memset(sky_system_alloc(nbytes), 0x00, nbytes);
}


static inline sky_bool_t
sky_core_malloc_slab_update(sky_core_malloc_slab_t *        slab,
                            sky_core_malloc_slab_remote_t * old_remote,
                            sky_core_malloc_slab_remote_t * new_remote)
{
    new_remote->data.aba_tag = old_remote->data.aba_tag + 1;
#if defined(SKY_ARCH_32BIT)
    return sky_atomic_cas64(old_remote->cas_value, new_remote->cas_value,
                            &(slab->remote.cas_value));
#elif defined(SKY_ARCH_64BIT)
    return sky_atomic_cas128(old_remote, new_remote,
                             SKY_AS_VOIDP(&(slab->remote)));
#else
#   error implementation missing
#endif
}


static inline sky_bool_t
sky_core_malloc_slab_isempty(sky_core_malloc_slab_t *slab)
    /* A slab is empty if none of its slots are in use. */
{
    return (slab &&
            (slab->local.available_count ==
             slab->local.allocation_count) ? SKY_TRUE : SKY_FALSE);
}


static inline sky_bool_t
sky_core_malloc_slab_isfull(sky_core_malloc_slab_t *slab)
    /* A slab is full if all of its slots are in use. */
{
    return (!slab ||
            slab->local.available_count == 0 ? SKY_TRUE : SKY_FALSE);
}


static inline void
sky_core_malloc_slab_become_head(sky_core_malloc_tlsdata_t *tlsdata,
                                 sky_core_malloc_slab_t *   slab)
{
    size_t  slab_index = slab->local.slab_index;

    /* If there is a prev slab and it is also not full, don't move. */
    if (!sky_core_malloc_slab_isfull(slab->local.prev)) {
        return;
    }

    *(slab->local.next ? &(slab->local.next->local.prev)
                       : &(tlsdata->slabs[slab_index].tail)) = slab->local.prev;
    slab->local.prev->local.next = slab->local.next;
    slab->local.next = tlsdata->slabs[slab_index].head;
    slab->local.prev = NULL;
    tlsdata->slabs[slab_index].head->local.prev = slab;
    tlsdata->slabs[slab_index].head = slab;
}


static inline void
sky_core_malloc_slab_become_tail(sky_core_malloc_tlsdata_t *tlsdata,
                                 sky_core_malloc_slab_t *   slab)
{
    size_t  slab_index = slab->local.slab_index;

    /* If there is a next slab and it is also full, don't move. */
    if (sky_core_malloc_slab_isfull(slab->local.next)) {
        return;
    }

    slab->local.next->local.prev = slab->local.prev;
    *(slab->local.prev ? &(slab->local.prev->local.next)
                       : &(tlsdata->slabs[slab_index].head)) = slab->local.next;
    slab->local.next = NULL;
    slab->local.prev = tlsdata->slabs[slab_index].tail;
    tlsdata->slabs[slab_index].tail->local.next = slab;
    tlsdata->slabs[slab_index].tail = slab;
}


static inline uint32_t
sky_core_malloc_slab_index(size_t nbytes)
{
    /* Anything <= 64 is broken up into 16-byte increments.
     * 4 slots. Slab indices 0 through 3, inclusive.
     */
    if (nbytes <= 64) {
        return (((nbytes + 15) & ~15) >> 4) - 1;
    }

    /* Anything <= 512 is broken up into 64-byte increments.
     * 7 slots. Slab indices 4 through 10, inclusive.
     */
    if (nbytes <= 512) {
        return (((nbytes + 63) & ~63) >> 6) + 2;
    }

    /* Anything <= 4096 is broken up into 256-byte increments.
     * 14 slots. Slab indices 11 through 24, inclusive.
     */
    if (nbytes <= 4096) {
        return (((nbytes + 255) & ~255) >> 8) + 8;
    }

    /* Anything else is broken up into 4096-byte increments.
     * 7 slots. Slab indices 25 through 31, inclusive.
     */
    return (((nbytes + 4095) & ~4095) >> 12) + 23;
}


static inline sky_bool_t
sky_core_malloc_slab_localize(sky_core_malloc_slab_t *slab)
{
    void                            *free_pointer;
    sky_core_malloc_slab_remote_t   new_remote, old_remote;

    if (slab->remote.data.freelist) {
        do {
            old_remote = slab->remote;
            new_remote.data.freelist = NULL;
        } while (!sky_core_malloc_slab_update(slab, &old_remote, &new_remote));
        while ((free_pointer = old_remote.data.freelist) != NULL) {
            old_remote.data.freelist = *(void **)free_pointer;
            *(void **)free_pointer = slab->local.malloc_freelist;
            slab->local.malloc_freelist = free_pointer;
            ++slab->local.available_count;
        }
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


static size_t
sky_core_malloc_slab_index_table[SKY_CORE_MALLOC_SMALL_SIZE_COUNT] = {
/*    16-byte increments */   16,    32,    48,    64,
/*    64-byte increments */  128,   192,   256,   320,   384,   448,   512,
/*   256-byte increments */  768,  1024,  1280,  1536,  1792,  2048,  2304,
                            2560,  2816,  3072,  3328,  3584,  3840,  4096,
/*  4096-byte increments */ 8192, 12288, 16384, 20480, 24576, 28672, 32768,
};

static inline size_t
sky_core_malloc_slab_size(size_t slab_index)
{
    return sky_core_malloc_slab_index_table[slab_index];
}


static sky_core_malloc_slab_t *
sky_core_malloc_slab_alloc(sky_core_malloc_tlsdata_t *tlsdata)
{
    sky_core_malloc_slab_t          *slab;
    sky_core_malloc_slab_chunk_t    *schunk;

    for (schunk = tlsdata->slabs_head; schunk; schunk = schunk->next) {
        if ((slab = schunk->freelist) != NULL) {
            schunk->freelist = slab->local.next;
            --schunk->free_count;
            return slab;
        }
        if (schunk->used_count < schunk->avail_count) {
            ++schunk->used_count;
            slab = ((sky_core_malloc_slab_t *)schunk) + schunk->used_count;
            return slab;
        }
    }

    schunk = sky_core_malloc_chunk_acquire(tlsdata, sky_core_malloc_heap_size);
    schunk->next = tlsdata->slabs_head;
    schunk->prev = NULL;
    schunk->freelist = NULL;
    schunk->avail_count = (sky_core_malloc_heap_size /
                           sizeof(sky_core_malloc_slab_t)) - 1;
    schunk->free_count = 0;
    schunk->used_count = 1;

    *(tlsdata->slabs_head ? &(tlsdata->slabs_head->prev)
                          : &(tlsdata->slabs_tail)) = schunk;
    tlsdata->slabs_head = schunk;

    return ((sky_core_malloc_slab_t *)schunk) + 1;
}

#define ALLOC_COUNT(allocation_size) \
        (SKY_CORE_MALLOC_HEAP_SIZE / (allocation_size))
static int32_t
sky_core_malloc_allocation_counts[SKY_CORE_MALLOC_SMALL_SIZE_COUNT] = {
    ALLOC_COUNT(   16), ALLOC_COUNT(   32), ALLOC_COUNT(   48),
    ALLOC_COUNT(   64),

    ALLOC_COUNT(  128), ALLOC_COUNT(  192), ALLOC_COUNT(  256),
    ALLOC_COUNT(  320), ALLOC_COUNT(  384), ALLOC_COUNT(  448),
    ALLOC_COUNT(  512),

    ALLOC_COUNT(  768), ALLOC_COUNT( 1024), ALLOC_COUNT( 1280),
    ALLOC_COUNT( 1536), ALLOC_COUNT( 1792), ALLOC_COUNT( 2048),
    ALLOC_COUNT( 2304), ALLOC_COUNT( 2560), ALLOC_COUNT( 2816),
    ALLOC_COUNT( 3072), ALLOC_COUNT( 3328), ALLOC_COUNT( 3584),
    ALLOC_COUNT( 3840), ALLOC_COUNT( 4096),

    ALLOC_COUNT( 8192), ALLOC_COUNT(12288), ALLOC_COUNT(16384),
    ALLOC_COUNT(20480), ALLOC_COUNT(24576), ALLOC_COUNT(28672),
    ALLOC_COUNT(32768),
};
#undef ALLOC_COUNT

static sky_core_malloc_slab_t *
sky_core_malloc_slab_create(sky_tlsdata_t *tlsdata, uint32_t slab_index)
{
    void                    *pointer;
    sky_core_malloc_slab_t  *slab;

    slab = sky_core_malloc_slab_alloc(tlsdata->malloc_tlsdata);
    pointer = sky_core_malloc_chunk_acquire(tlsdata->malloc_tlsdata,
                                            sky_core_malloc_heap_size);
    sky_core_malloc_page_maskvalue(
            pointer,
            sky_core_malloc_heap_size >> sky_core_malloc_page_size_bits,
            (uintptr_t)slab);

    slab->remote.data.freelist = NULL;
    slab->remote.data.aba_tag = 0;
    slab->local.thread_id = tlsdata->thread_id;
    slab->local.slab_index = slab_index;
    slab->local.allocation_count = sky_core_malloc_allocation_counts[slab_index];
    slab->local.available_count = slab->local.allocation_count;
    slab->local.pointer = pointer;
    slab->local.first_unallocated = pointer;
    slab->local.calloc_freelist = NULL;
    slab->local.malloc_freelist = NULL;
    slab->local.next = slab->local.prev = NULL;

    return slab;
}


static inline void
sky_core_malloc_slab_release(sky_core_malloc_tlsdata_t *tlsdata,
                             sky_core_malloc_slab_t *   slab)
{
    sky_core_malloc_slab_chunk_t    *schunk;

    *(slab->local.next ? &(slab->local.next->local.prev)
                       : &(tlsdata->slabs[slab->local.slab_index].tail)) =
            slab->local.prev;
    *(slab->local.prev ? &(slab->local.prev->local.next)
                       : &(tlsdata->slabs[slab->local.slab_index].head)) =
            slab->local.next;

    sky_core_malloc_chunk_release(tlsdata, slab->local.pointer);
    for (schunk = tlsdata->slabs_head; schunk; schunk = schunk->next) {
        if ((char *)slab >= (char *)schunk &&
            (char *)slab <  (char *)schunk + sky_core_malloc_heap_size)
        {
            break;
        }
    }
    sky_error_validate_debug(schunk != NULL);

    slab->local.next = schunk->freelist;
    schunk->freelist = slab;
    if (++schunk->free_count == schunk->used_count) {
        *(schunk->next ? &(schunk->next->prev)
                       : &(tlsdata->slabs_tail)) = schunk->prev;
        *(schunk->prev ? &(schunk->prev->next)
                       : &(tlsdata->slabs_head)) = schunk->next;
        sky_core_malloc_chunk_release(tlsdata, schunk);
    }
}


static void
sky_core_malloc_slab_cleanup(sky_core_malloc_tlsdata_t *tlsdata,
                             size_t                     slab_index)
{
    int32_t                 available_count;
    sky_core_malloc_slab_t  *next_slab, *slab;

    for (slab = tlsdata->slabs[slab_index].head; slab; slab = next_slab) {
        next_slab = slab->local.next;
        if (slab->remote.data.freelist) {
            available_count = slab->local.available_count;
            sky_core_malloc_slab_localize(slab);
            if (!available_count && tlsdata->slabs[slab_index].head != slab) {
                sky_core_malloc_slab_become_head(tlsdata, slab);
            }
        }

        /* If the slab has nothing allocated within it, release it. */
        if (slab->local.available_count == slab->local.allocation_count) {
            sky_core_malloc_slab_release(tlsdata, slab);
            continue;
        }

        /* Mark the slab orphaned. */
        slab->local.thread_id = 0;
    }
}


static inline void *
sky_core_malloc_slab_use_calloc_freelist(sky_core_malloc_tlsdata_t *tlsdata,
                                         sky_core_malloc_slab_t *   slab)
{
    void    *pointer;
    
    pointer = slab->local.calloc_freelist;
    slab->local.calloc_freelist = *(void **)pointer;
    *(uintptr_t *)pointer = 0;

    sky_error_validate_debug(slab->local.available_count >= 0);
    --slab->local.available_count;
    if (unlikely(unlikely(!slab->local.available_count) &&
                 unlikely(tlsdata->slabs[slab->local.slab_index].tail != slab)))
    {
        sky_core_malloc_slab_become_tail(tlsdata, slab);
    }

    return pointer;
}


static inline void *
sky_core_malloc_slab_use_first_unallocated(sky_core_malloc_tlsdata_t *  tlsdata,
                                           sky_core_malloc_slab_t *     slab)
{
    void    *pointer;
    size_t  allocation_size;

    pointer = slab->local.first_unallocated;
    allocation_size = sky_core_malloc_slab_size(slab->local.slab_index);
    slab->local.first_unallocated = ((char *)pointer + allocation_size);
    if ((char *)slab->local.first_unallocated >=
        (char *)slab->local.pointer +
                (allocation_size * slab->local.allocation_count))
    {
        slab->local.first_unallocated = NULL;
    }

    sky_error_validate_debug(slab->local.available_count >= 0);
    --slab->local.available_count;
    if (unlikely(unlikely(!slab->local.available_count) &&
                 unlikely(tlsdata->slabs[slab->local.slab_index].tail != slab)))
    {
        sky_core_malloc_slab_become_tail(tlsdata, slab);
    }

#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
    memset(pointer, 0xA5, allocation_size);
#endif
    return pointer;
}


static inline void *
sky_core_malloc_slab_use_malloc_freelist(sky_core_malloc_tlsdata_t *tlsdata,
                                         sky_core_malloc_slab_t *   slab)
{
    void    *pointer;

    pointer = slab->local.malloc_freelist;
    slab->local.malloc_freelist = *(void **)pointer;

    sky_error_validate_debug(slab->local.available_count >= 0);
    --slab->local.available_count;
    if (unlikely(unlikely(!slab->local.available_count) &&
                 unlikely(tlsdata->slabs[slab->local.slab_index].tail != slab)))
    {
        sky_core_malloc_slab_become_tail(tlsdata, slab);
    }

#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
    memset(pointer, 0xA5, sky_core_malloc_slab_size(slab->local.slab_index));
#endif
    return pointer;
}


void
sky_core_malloc_tlsdata_adopt(sky_core_malloc_tlsdata_t *tlsdata,
                              sky_core_malloc_tlsdata_t *orphan_tlsdata)
{
    size_t                      i;
    sky_core_malloc_slab_t      *next_slab, *slab;
    sky_core_malloc_chunk_t     *chunk;
    sky_core_malloc_tlsdata_t   *next_tlsdata, *tlsdata_tail;

    /* 1. Move the orphan tlsdata's chunk freelist into this thread's
     *    free list.
     */
    while ((chunk = orphan_tlsdata->free_chunks_head) != NULL) {
        orphan_tlsdata->free_chunks_head = chunk->next_chunk;
        sky_core_malloc_chunk_insert(tlsdata, chunk, chunk->chunk_size);
    }
    orphan_tlsdata->free_chunks_head = NULL;
    orphan_tlsdata->free_chunks_tail = NULL;

    /* 2. Move the orphan tlsdata's slab chunk list into this thread's slab
     *    chunk list. Put it at the end of the list so that it's least likely
     *    to be re-used, and hopefully released sooner. See the notes at the
     *    top of this file for more discussion on how this should be managed.
     */
    if (orphan_tlsdata->slabs_head) {
        if (!tlsdata->slabs_head) {
            tlsdata->slabs_head = orphan_tlsdata->slabs_head;
        }
        else {
            tlsdata->slabs_tail->next = orphan_tlsdata->slabs_head;
            orphan_tlsdata->slabs_head->prev = tlsdata->slabs_tail;
        }
        tlsdata->slabs_tail = orphan_tlsdata->slabs_tail;
        orphan_tlsdata->slabs_head = NULL;
        orphan_tlsdata->slabs_tail = NULL;
    }

    /* 3. Move slab chains for each slab index from the orphan tlsdata into
     *    this thread's.
     */
    for (i = 0; i < sizeof(tlsdata->slabs) / sizeof(tlsdata->slabs[0]); ++i) {
        for (slab = orphan_tlsdata->slabs[i].head; slab; slab = next_slab) {
            next_slab = slab->local.next;
            slab->local.thread_id = tlsdata->thread_id;

            /* Move the slab into this thread's list of slabs. If the slab has
             * any available allocations, it goes at the head; otherwise, it
             * goes at the tail.
             */
            if (slab->local.available_count > 0) {
                slab->local.next = tlsdata->slabs[i].head;
                slab->local.prev = NULL;
                *(tlsdata->slabs[i].head ? &(tlsdata->slabs[i].head->local.prev)
                                         : &(tlsdata->slabs[i].tail)) = slab;
                tlsdata->slabs[i].head = slab;
            }
            else {
                slab->local.next = NULL;
                slab->local.prev = tlsdata->slabs[i].tail;
                *(tlsdata->slabs[i].tail ? &(tlsdata->slabs[i].tail->local.next)
                                         : &(tlsdata->slabs[i].head)) = slab;
                tlsdata->slabs[i].tail = slab;
            }
        }
        orphan_tlsdata->slabs[i].head = NULL;
        orphan_tlsdata->slabs[i].tail = NULL;
    }

    /* 4. Check the orphan tlsdata for possible localization. If there are
     *    no large allocations in the orphan tlsdata, the tlsdata structure
     *    itself can be discarded; otherwise, it should be linked into the
     *    chain of tlsdata structures owned by this thread.
     */
    while (!orphan_tlsdata->large_count) {
        next_tlsdata = orphan_tlsdata->next;
        sky_system_free(orphan_tlsdata, sizeof(sky_core_malloc_tlsdata_t));
        if (!(orphan_tlsdata = next_tlsdata)) {
            return;
        }
    }

    for (tlsdata_tail = tlsdata;
         tlsdata_tail->next;
         tlsdata_tail = tlsdata_tail->next);
    tlsdata_tail->next = orphan_tlsdata;
}


void
sky_core_malloc_tlsdata_cleanup(sky_core_malloc_tlsdata_t *tlsdata,
                                sky_core_malloc_tlsdata_t **orphan_tlsdata)
    /* WARNING!  While this function is responsible for cleaning up the tlsdata
     * for a thread, there is no guarantee that the thread calling the function
     * will be the owner of said tlsdata.
     */
{
    size_t                      i;
    sky_bool_t                  orphan;
    sky_core_malloc_tlsdata_t   *next_tlsdata;

    /* Start out in a state of not orphaning this tlsdata. */
    orphan = SKY_FALSE;

    /* Process this thread's large allocations. Clean up anything that is in
     * the remote free queue first. If there are any large allocations that
     * remain, the list must be orphaned; otherwise, it should be freed so that
     * the chunk can get cleaned up.
     */
    sky_core_malloc_tlsdata_localize(tlsdata);
    if (tlsdata->large_count) {
        orphan = SKY_TRUE;
    }

    /* Clean up all of the thread's slabs. For each slab, orphan it if it's
     * not empty after cleanup.
     */
    for (i = 0; i < sizeof(tlsdata->slabs) / sizeof(tlsdata->slabs[0]); ++i) {
        sky_core_malloc_slab_cleanup(tlsdata, i);
        if (tlsdata->slabs[i].head) {
            orphan = SKY_TRUE;
        }
    }

    /* Clean up the thread's free chunk list. */
    sky_core_malloc_chunk_cleanup(tlsdata);
    if (tlsdata->free_chunks_head) {
        orphan = SKY_TRUE;
    }

    /* If the tlsdata should not be orphaned, release its memory back to the
     * operating system. Any tlsdata that remains linked to it must be
     * orphaned, however.
     */
    if (orphan) {
        *orphan_tlsdata = tlsdata;
    }
    else {
        next_tlsdata = tlsdata->next;
        sky_system_free(tlsdata, sizeof(sky_core_malloc_tlsdata_t));
        *orphan_tlsdata = next_tlsdata;
    }

}


static inline sky_tlsdata_t *
sky_core_malloc_tlsdata_get(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_core_malloc_tlsdata_t   *malloc_tlsdata;

    if (!tlsdata->malloc_tlsdata) {
        tlsdata->malloc_tlsdata = malloc_tlsdata =
                memset(sky_system_alloc(sizeof(sky_core_malloc_tlsdata_t)),
                       0x00,
                       sizeof(sky_core_malloc_tlsdata_t));
        sky_atomic_lifo_init(&(malloc_tlsdata->large_freelist), 0);
        malloc_tlsdata->thread_id = tlsdata->thread_id;
    }

    return tlsdata;
}


static void
sky_core_malloc_tlsdata_localize(sky_core_malloc_tlsdata_t *malloc_tlsdata)
{
    void                        *pointer;
    sky_atomic_lifo_t           *freelist;
    sky_core_malloc_tlsdata_t   *tlsdata, *next_tlsdata, **prev_tlsdata;

    prev_tlsdata = &malloc_tlsdata;
    for (tlsdata = *prev_tlsdata; tlsdata; tlsdata = next_tlsdata) {
        next_tlsdata = tlsdata->next;
        if (!tlsdata->large_count) {
            prev_tlsdata = &(tlsdata->next);
        }
        else {
            freelist = &(tlsdata->large_freelist);
            while ((pointer = sky_atomic_lifo_pop(freelist)) != NULL) {
                --tlsdata->large_count;
                sky_core_malloc_chunk_release(malloc_tlsdata, pointer);
            }
            if (tlsdata->large_count || tlsdata == malloc_tlsdata) {
                prev_tlsdata = &(tlsdata->next);
            }
            else {
                *prev_tlsdata = next_tlsdata;
                sky_system_free(tlsdata, sizeof(sky_core_malloc_tlsdata_t));
            }
        }
    }
}


static inline void *
sky_core_malloc_allocate(sky_tlsdata_t *tlsdata,
                         size_t         nbytes,
                         size_t         alignment,
                         sky_bool_t     zero)
{
    sky_core_malloc_tlsdata_t   *malloc_tlsdata = tlsdata->malloc_tlsdata;

    void                    *pointer;
    int32_t                 available_count;
    uint32_t                slab_index;
    sky_core_malloc_slab_t  *calloc_slab, *slab;

    nbytes = (nbytes + alignment - 1) & ~(alignment - 1);
    if (unlikely(nbytes > sky_core_malloc_small_size_max)) {
        if (nbytes >=
            sky_core_malloc_huge_page_size - sky_core_malloc_heap_size)
        {
            /* This is a huge allocation. */
            nbytes = (nbytes + sky_core_malloc_page_size - 1) &
                     ~(sky_core_malloc_page_size - 1);
            pointer = sky_system_alloc(nbytes);
            sky_core_malloc_page_setvalue(
                    pointer,
                    1,
                    nbytes | SKY_CORE_MALLOC_PAGE_FLAG_FIRST |
                             SKY_CORE_MALLOC_PAGE_FLAG_HUGE);
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
            return memset(pointer, (zero ? 0x00 : 0xA5), nbytes);
#else
            return (zero ? memset(pointer, 0x00, nbytes) : pointer);
#endif
        }

        /* This is a large allocation. Simply get a new chunk, and increment
         * the large allocation counter.
         */
        nbytes = (nbytes + sky_core_malloc_heap_size - 1) &
                 ~(sky_core_malloc_heap_size - 1);
        pointer = sky_core_malloc_chunk_acquire(malloc_tlsdata, nbytes);
        sky_core_malloc_page_maskvalue(
                pointer,
                1,
                nbytes | SKY_CORE_MALLOC_PAGE_FLAG_LARGE);
        sky_core_malloc_page_maskvalue(
                (char *)pointer + sky_core_malloc_page_size,
                1,
                (uintptr_t)malloc_tlsdata);
        ++malloc_tlsdata->large_count;
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
        return memset(pointer, (zero ? 0x00 : 0xA5), nbytes);
#else
        return (zero ? memset(pointer, 0x00, nbytes) : pointer);
#endif
    }

    /* Look for an available pointer in a slab. Stop when a completely full
     * slab is reached. Give preference to the malloc_freelist in a slab
     * before using first_unallocated, if possible. Along the way, remember
     * where slabs having calloc freelists were seen. Use those before
     * creating a new slab.
     */
    slab_index = sky_core_malloc_slab_index(nbytes);
    calloc_slab = NULL;
    for (slab = malloc_tlsdata->slabs[slab_index].head;
         likely(slab != NULL) && likely(slab->local.available_count);
         slab = slab->local.next)
    {
        if (slab->local.calloc_freelist) {
            if (zero) {
                return sky_core_malloc_slab_use_calloc_freelist(malloc_tlsdata,
                                                                slab);
            }
            calloc_slab = slab;
        }
        if (slab->local.malloc_freelist) {
            pointer = sky_core_malloc_slab_use_malloc_freelist(malloc_tlsdata,
                                                               slab);
            goto return_pointer;
        }
        if (slab->local.first_unallocated) {
            pointer = sky_core_malloc_slab_use_first_unallocated(malloc_tlsdata,
                                                                 slab);
            goto return_pointer;
        }
    }

    if (calloc_slab) {
        /* There's a slab containing a calloc freelist. It's better to use the
         * zeroed memory than to scan for a remote freelist or create a whole
         * new slab.
         */
        pointer = sky_core_malloc_slab_use_calloc_freelist(malloc_tlsdata,
                                                           calloc_slab);
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
        if (!zero) {
            memset(pointer, 0xA5, sky_core_malloc_slab_size(slab_index));
        }
#endif
        return pointer;
    }

    /* Look for a remote freelist before creating a new slab. */
    for (slab = malloc_tlsdata->slabs[slab_index].head;
         likely(slab != NULL);
         slab = slab->local.next)
    {
        if (slab->remote.data.freelist) {
            /* Localize the freelist, and then use the local malloc_freelist
             * to get a pointer.
             */
            available_count = slab->local.available_count;

            sky_core_malloc_slab_localize(slab);
            if (!available_count &&
                slab != malloc_tlsdata->slabs[slab_index].head)
            {
                sky_core_malloc_slab_become_head(malloc_tlsdata, slab);
            }
            pointer = sky_core_malloc_slab_use_malloc_freelist(malloc_tlsdata,
                                                               slab);
            goto return_pointer;
        }
    }

    /* All existing slabs for the size (if any) are fully committed. The only
     * remaining option is to create a new slab.
     */
    slab = sky_core_malloc_slab_create(tlsdata, slab_index);
    slab->local.next = malloc_tlsdata->slabs[slab_index].head;
    *(malloc_tlsdata->slabs[slab_index].head
            ? &(malloc_tlsdata->slabs[slab_index].head->local.prev)
            : &(malloc_tlsdata->slabs[slab_index].tail)) = slab;
    malloc_tlsdata->slabs[slab_index].head = slab;
    pointer = sky_core_malloc_slab_use_first_unallocated(malloc_tlsdata, slab);

return_pointer:
    nbytes = sky_core_malloc_slab_size(slab_index);
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
    return memset(pointer, (zero ? 0x00 : 0xA5), nbytes);
#else
    return (zero ? memset(pointer, 0x00, nbytes) : pointer);
#endif
}


#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
static inline void
sky_core_malloc_local_free(sky_core_malloc_tlsdata_t *  tlsdata,
                           sky_core_malloc_slab_t *     slab,
                           void *                       pointer)
{
    *(void **)pointer = slab->local.malloc_freelist;
    slab->local.malloc_freelist = pointer;
    if (0 == slab->local.available_count++ &&
        tlsdata->slabs[slab->local.slab_index].head != slab)
    {
        sky_core_malloc_slab_become_head(tlsdata, slab);
    }
    else if (sky_core_malloc_slab_isempty(slab) &&
             !sky_core_malloc_slab_isfull(slab->local.next))
    {
        sky_core_malloc_slab_release(tlsdata, slab);
    }
}
#endif


static inline void
sky_core_malloc_deallocate(sky_tlsdata_t *tlsdata, void *pointer)
{
#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
    sky_core_malloc_tlsdata_t   *malloc_tlsdata = tlsdata->malloc_tlsdata;
#endif

    uintptr_t                   page_value;
    sky_core_malloc_slab_t      *slab;
    sky_core_malloc_tlsdata_t   *large_tlsdata;

    page_value = sky_core_malloc_page_value(pointer);
    if (unlikely(page_value & SKY_CORE_MALLOC_PAGE_FLAG_HUGE)) {
        page_value &= ~(uintptr_t)SKY_CORE_MALLOC_PAGE_FLAG_MASK;
#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
        sky_system_free(pointer, page_value);
#elif defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
        memset(pointer, 0x5A, page_value);
#endif
        return;
    }
    if (unlikely(page_value & SKY_CORE_MALLOC_PAGE_FLAG_LARGE)) {
        page_value &= ~(uintptr_t)SKY_CORE_MALLOC_PAGE_FLAG_MASK;
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
        memset(pointer, 0x5A, page_value);
#endif
        page_value = sky_core_malloc_page_value((char *)pointer +
                                                sky_core_malloc_page_size);
        large_tlsdata = (sky_core_malloc_tlsdata_t *)
                (page_value & ~(uintptr_t)SKY_CORE_MALLOC_PAGE_FLAG_MASK);

#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
        while (large_tlsdata) {
            if (large_tlsdata == malloc_tlsdata) {
                sky_core_malloc_chunk_release(malloc_tlsdata, pointer);
                --large_tlsdata->large_count;
                return;
            }
            large_tlsdata = large_tlsdata->next;
        }

        /* Remote free. Push the allocation into a LIFO that'll be consumed
         * by the thread that initially made the allocation.
         */
        sky_atomic_lifo_push(&(malloc_tlsdata->large_freelist), pointer);
#endif
        return;
    }

    slab = (sky_core_malloc_slab_t *)
                    (page_value & ~(uintptr_t)SKY_CORE_MALLOC_PAGE_FLAG_MASK);
#if defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
    memset(pointer, 0x5A, sky_core_malloc_slab_size(slab->local.slab_index));
#endif
    if (likely(slab->local.thread_id == tlsdata->thread_id)) {
        /* Fast path for local free. This saves a couple of branches, as well
         * as a 64/128-byte copy that's only needed for a remote free.
         */
#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
        sky_core_malloc_local_free(malloc_tlsdata, slab, pointer);
#endif
        return;
    }

#if !defined(SKY_CORE_MALLOC_NEVER_FREE)
    /* This is a remote free. Put the pointer on the slab's remote freelist.
     * Retry until the operation succeeds.
     */
    do {
        sky_core_malloc_slab_remote_t   new_remote, old_remote;

        do {
            old_remote = slab->remote;
            *(void **)pointer = old_remote.data.freelist;
            new_remote.data.freelist = pointer;
        } while (!sky_core_malloc_slab_update(slab, &old_remote, &new_remote));
    } while (0);
#endif
}


static void *
sky_core_malloc_calloc(size_t count, size_t nbytes)
{
    sky_once_run(&sky_core_malloc_once_initializer,
                 sky_core_malloc_once_initialize,
                 NULL);

    return sky_core_malloc_allocate(sky_core_malloc_tlsdata_get(),
                                    (count * nbytes),
                                    16,
                                    SKY_TRUE);
}


static void
sky_core_malloc_free(void *pointer)
{
    sky_core_malloc_deallocate(sky_core_malloc_tlsdata_get(), pointer);
}


static sky_bool_t
sky_core_malloc_idle(void)
{
    size_t                      i;
    int32_t                     available_count;
    sky_core_malloc_slab_t      *slab;
    sky_core_malloc_tlsdata_t   *tlsdata;

    /* XXX  this could almost certainly use some optimization. */

    sky_once_run(&sky_core_malloc_once_initializer,
                 sky_core_malloc_once_initialize,
                 NULL);

    tlsdata = sky_core_malloc_tlsdata_get()->malloc_tlsdata;
    for (i = 0; i < sizeof(tlsdata->slabs) / sizeof(tlsdata->slabs[i]); ++i) {
        for (slab = tlsdata->slabs[i].head; slab; slab = slab->local.next) {
            /* If the slab's remote freelist is not empty, localize it. */
            if (slab->remote.data.freelist) {
                available_count = slab->local.available_count;
                sky_core_malloc_slab_localize(slab);
                if (!available_count && tlsdata->slabs[i].head != slab) {
                    sky_core_malloc_slab_become_head(tlsdata, slab);
                }
                /* If the slab has nothing allocated within it, release it,
                 * but only if it's not the only slab with available slots.
                 */
                if (sky_core_malloc_slab_isempty(slab) &&
                    !sky_core_malloc_slab_isfull(slab->local.next))
                {
                    sky_core_malloc_slab_release(tlsdata, slab);
                }
                return SKY_TRUE;
            }

#if !defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
            /* If the slab has any entries in its malloc freelist, turn it into
             * a calloc freelist.
             */
            if (slab->local.malloc_freelist) {
                void    *pointer;
                size_t  slab_size = sky_core_malloc_slab_size(i);

                while ((pointer = slab->local.malloc_freelist) != NULL) {
                    slab->local.malloc_freelist = *(void **)pointer;
                    memset(pointer, 0x00, slab_size);
                    *(void **)pointer = slab->local.calloc_freelist;
                    slab->local.calloc_freelist = pointer;
                }
                return SKY_TRUE;
            }
#endif
        }
    }

#if !defined(SKY_CORE_MALLOC_DEBUG_MEMORY)
    /* Scan through the free chunks list, releasing anything that can be
     * released back to the operating system.
     */
    sky_core_malloc_chunk_cleanup(tlsdata);
#endif

    return SKY_FALSE;
}


static void *
sky_core_malloc_malloc(size_t nbytes)
{
    sky_once_run(&sky_core_malloc_once_initializer,
                 sky_core_malloc_once_initialize,
                 NULL);

    return sky_core_malloc_allocate(sky_core_malloc_tlsdata_get(),
                                    nbytes,
                                    16,
                                    SKY_FALSE);
}


static void *
sky_core_malloc_memalign(size_t alignment, size_t nbytes)
{
    sky_once_run(&sky_core_malloc_once_initializer,
                 sky_core_malloc_once_initialize,
                 NULL);

    return sky_core_malloc_allocate(sky_core_malloc_tlsdata_get(),
                                    nbytes,
                                    alignment,
                                    SKY_TRUE);
}


size_t
sky_core_malloc_memsize(const void *pointer)
{
    uintptr_t               page_value;
    sky_core_malloc_slab_t  *slab;

    page_value = sky_core_malloc_page_value(pointer);
    if (unlikely(page_value & (SKY_CORE_MALLOC_PAGE_FLAG_LARGE |
                               SKY_CORE_MALLOC_PAGE_FLAG_HUGE)))
    {
        return page_value & ~SKY_CORE_MALLOC_PAGE_FLAG_MASK;
    }
    page_value &= ~SKY_CORE_MALLOC_PAGE_FLAG_MASK;
    slab = (sky_core_malloc_slab_t *)page_value;
    return sky_core_malloc_slab_size(slab->local.slab_index);
}


static void *
sky_core_malloc_realloc(void *old_pointer, size_t new_nbytes)
{
    void                    *new_pointer;
    size_t                  old_allocation_size;
    uintptr_t               old_page_value;
    sky_tlsdata_t           *tlsdata;
    sky_core_malloc_slab_t  *old_slab;

    old_page_value = sky_core_malloc_page_value(old_pointer);
    if (unlikely(old_page_value & (SKY_CORE_MALLOC_PAGE_FLAG_LARGE |
                                   SKY_CORE_MALLOC_PAGE_FLAG_HUGE)))
    {
        old_allocation_size = old_page_value & ~SKY_CORE_MALLOC_PAGE_FLAG_MASK;
    }
    else {
        old_page_value &= ~SKY_CORE_MALLOC_PAGE_FLAG_MASK;
        old_slab = (sky_core_malloc_slab_t *)old_page_value;
        old_allocation_size =
                 sky_core_malloc_slab_size(old_slab->local.slab_index);
    }
    if (old_allocation_size >= new_nbytes) {
        return old_pointer;
    }

    tlsdata = sky_tlsdata_get();
    new_pointer = sky_core_malloc_allocate(tlsdata, new_nbytes, 16, SKY_FALSE);
    memcpy(new_pointer, old_pointer, SKY_MIN(old_allocation_size, new_nbytes));
    sky_core_malloc_deallocate(tlsdata, old_pointer);

    return new_pointer;
}


void
sky_core_malloc_finalize(void)
{
}


void
sky_core_malloc_initialize(void)
{
    sky_malloc_sethooks(sky_core_malloc_malloc,
                        sky_core_malloc_free,
                        sky_core_malloc_realloc,
                        sky_core_malloc_calloc,
                        NULL,                           /* sky_memdup       */
                        NULL,                           /* sky_strdup       */
                        NULL,                           /* sky_strndup      */
                        sky_core_malloc_memalign,
                        sky_core_malloc_memsize,
                        sky_core_malloc_idle);
}
