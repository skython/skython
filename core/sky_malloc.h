/** @file
  * @brief Memory allocation
  * @defgroup memory_management Memory Management
  * @{
  * @defgroup sky_malloc Memory Allocation
  * The default memory allocation implementation used by Skython is not the
  * system malloc, because Skython is designed to be used primarily in highly
  * concurrent environments. Most commonly, system malloc is implemented using
  * a global lock around the heap, which does not scale. Instead, the default
  * memory allocation implementation used by Skython is a custom one that is
  * completely lock-free.
  *
  * The design of Skython's memory allocator is somewhat similar to Streamflow
  * (http://www.cs.wm.edu/streamflow), but there are some differences. Some of
  * the notable differences are:
  *   - 100% lock-free. Streamflow uses a global spinlock when allocating
  *     memory from the operating system for its superpages, and another one
  *     for doing initialization checks on every call to malloc().
  *   - Small object classes are 16-byte increments instead of 4-byte.
  *   - Allocations are considered small up to 32K instead of 2K.
  *   - Thread heaps contain two free lists for local allocations: one for
  *     calloc, and another for malloc. The calloc freelist is populated by
  *     an idle task if the application calls it.
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MALLOC_H__
#define __SKYTHON_CORE_SKY_MALLOC_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** A function pointer that can be called by sky_calloc() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the values of
  * @a count and @a nbytes will have been validated. The function should return
  * @c NULL if the allocation request cannot be satisfied, in which case it
  * should not do any special error handling of its own. The function must
  * return memory that has been filled with zeroes. Allocations should be
  * 16-byte aligned.
  *
  * @param[in]  count   the number of elements to allocate
  * @param[in]  nbytes  the number of bytes to allocate per element
  */
typedef void *(*sky_calloc_t)(size_t count, size_t nbytes);

/** A function pointer that can be called by sky_free() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the value of
  * @a pointer is guaranteed to never be @c NULL; however, it is not guaranteed
  * to be a valid pointer.
  *
  * @param[in]  pointer the pointer to the block of memory to deallocate.
  */
typedef void (*sky_free_t)(void *pointer);

/** A function pointer that can be called by sky_malloc() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the value of
  * @a nbytes will have been validated. The function should return @ NULL if
  * the allocation request cannot be satisfied, in which case it should not do
  * any special error handling of its own. Allocations should be 16-byte
  * aligned.
  *
  * @param[in]  nbytes  the number of bytes to allocate
  */
typedef void *(*sky_malloc_t)(size_t nbytes);

/** A function pointer that can be called by sky_malloc_idle() to provide a
  * custom implementation.
  * The implementation should return @c SKY_TRUE if there's possibly more work
  * to be done, or @c SKY_FALSE if there's definitely no more work to be done
  * for the time being.
  */
typedef sky_bool_t (*sky_malloc_idle_t)(void);

/** A function pointer that can be called by sky_memalign() to provide a custom
  * implementation. By the time that this function pointer will be called, the
  * values of @a nbytes and @a alignment will have been validated. The function
  * should return @c NULL if the request cannot be satisfied, in which case it
  * should not do any special error handling of its own. Allocations should be
  * aligned to the specified byte alignment, which will always be a power of
  * two in the range of 16 to a maximum of the hardware's page size.
  *
  * @param[in]  alignment   the number of bytes to align to, which will always
  *                         be a power of 2 in the range of 16 to a maximum of
  *                         the hardware's page size.
  * @param[in]  nbytes      the number of bytes to allocate
  */
typedef void *(*sky_memalign_t)(size_t alignment, size_t nbytes);

/** A function pointer that can be called by sky_memdup() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the values of
  * @a bytes and @a nbytes will have been validated. The function should return
  * @c NULL if the allocation request cannot be satisfied, in which case it
  * should not do any special error handling of its own. The function is
  * responsible for copying the data from @a bytes into the new allocation.
  * Allocations should be 16-byte aligned.
  *
  * @param[in]  bytes   the pointer to the memory to copy. It is guaranteed
  *                     to never be @c NULL.
  * @param[in]  nbytes  the number of bytes to copy. It is guaranteed to be
  *                     greater than zero.
  */
typedef void *(*sky_memdup_t)(const void *bytes, size_t nbytes);

/** A function pointer that can be called by sky_memsize() to return the size
  * of an allocation.
  * The size returned by this function must be at least as large as the size
  * that was requested when the allocation was created; however, it may be
  * larger, depending on how the underlying memory allocator implementation
  * works. The implementation of this function should strive to return the
  * largest possible size rather than attempt to return the smallest valid
  * size.
  *
  * @param[in]  pointer     the pointer for which the size of its allocation is
  *                         to be returned.
  * @return     the size of the allocation in bytes.
  */
typedef size_t (*sky_memsize_t)(const void *pointer);

/** A function pointer that can be called by sky_realloc() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the values of
  * @a old_pointer and @a new_nbytes will have been validated. The function
  * should return @c NULL if the allocation request cannot be satisfied, in
  * which case it should not do any special error handling of its own. The
  * function is responsible for copying the data from the original allocation
  * to the new allocation if necessary. It is acceptable to return the original
  * pointer as the new pointer if the original allocation is large enough to
  * satisfy the new allocation size. Allocations should be 16-byte aligned.
  *
  * @param[in]  old_pointer the pointer to the original allocation. It is
  *             guaranteed to never be @c NULL.
  * @param[in]  new_nbytes  the new allocation size
  */
typedef void *(*sky_realloc_t)(void *old_pointer, size_t new_nbytes);

/** A function pointer that can be called by sky_strdup() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the value of string
  * will have been validated. The function should return @c NULL if the
  * allocation request cannot be satisfied, in which case it should not do any
  * special error handling of its own. The function is responsible for copying
  * the string into the new allocation.
  *
  * @param[in]  string  the string to copy. It is guaranteed to never be
  *                     @c NULL.
  */
typedef char *(*sky_strdup_t)(const char *string);

/** A function pointer that can be called by sky_strndup() to provide a custom
  * implementation.
  * By the time that this function pointer will be called, the values of
  * @a string and @a string_length will have been validated. The function
  * should return @c NULL if the allocation request cannot be satisfied, in
  * which case it should not do any special error handling of its own. The
  * function is responsible for copying the string into the new allocation.
  *
  * @param[in]  string          the string to copy. It may or may not have a
  *                             termination byte.
  * @param[in]  string_length   the number of bytes from string to copy into
  *                             the new allocation. The original string may be
  *                             shorter than this length.
  */
typedef char *(*sky_strndup_t)(const char *string, size_t string_length);


/** Set alternate functions to implement memory allocation behaviors.
  * The default memory allocation functions use the Skython memory allocation
  * implementation, which is designed to be extremely fast and friendly to
  * concurrent applications. It should be noted that while Skython's memory
  * allocation implementation will significantly out-perform most system/libc
  * memory allocators, it does have some overhead that most system-default
  * allocators do not have. If the extra overhead is unacceptable for your
  * use, sky_system_malloc is provided to use the system/libc implementation
  * native to the platform.
  *
  * If an alternate function to implement sky_malloc() is set, corresponding
  * functions to implement sky_free(), sky_memalign(), and sky_realloc() must
  * be set. All of the other hooks are optional, and may be specified as
  * @c NULL, in which case wrappers built around sky_malloc() will be used to
  * implement them.
  *
  * @note   If memory allocation hooks other than the defaults are to be used,
  *         they should be installed as early as possible, even before calling
  *         the library initialization function sky_library_initialize().
  *
  * @warning    It is unsafe to change the memory allocation hooks after any
  *             memory has been allocated.
  *
  * @param[in]  malloc      the function to call to satisfy a call to sky_malloc().
  * @param[in]  free        the function to call to satisfy a call to sky_free().
  * @param[in]  realloc     the function to call to satisfy a call to sky_realloc().
  * @param[in]  calloc      the function to call to satisfy a call to sky_calloc().
  * @param[in]  memdup      the function to call to satisfy a call to sky_memdup().
  * @param[in]  strdup      the function to call to satisfy a call to sky_strdup().
  * @param[in]  strndup     the function to call to satisfy a call to sky_strndup().
  * @param[in]  memalign    the function to call to satisfy a call to sky_memalign().
  * @param[in]  memsize     the function to call to satisfy a call to sky_memsize().
  * @param[in]  malloc_idle the function to call to satisfy a call to sky_malloc_idle().
  */
SKY_EXTERN void sky_malloc_sethooks(sky_malloc_t malloc,
                                    sky_free_t free,
                                    sky_realloc_t realloc,
                                    sky_calloc_t calloc,
                                    sky_memdup_t memdup,
                                    sky_strdup_t strdup,
                                    sky_strndup_t strndup,
                                    sky_memalign_t memalign,
                                    sky_memsize_t memsize,
                                    sky_malloc_idle_t malloc_idle);

/** Allocate a new block of zeroed memory.
  * The number of bytes to allocate is computed by multiplying @a count by
  * @a nbytes. The number of bytes to allocate must be greater than zero. The
  * content of the newly allocated memory is guaranteed to be zeroed.
  * If zeroed memory is not required, use sky_malloc() instead, but if zeroed
  * memory is required, sky_calloc() should be used instead of manually zeroing
  * the memory returned by sky_malloc(), because the underlying implementation
  * could be doing something special to quickly return zeroed memory. Memory
  * allocated via sky_calloc() is guaranteed to be 16-byte aligned.
  *
  * @param[in]  count   the number of elements to allocate
  * @param[in]  nbytes  the number of bytes to allocate per element
  */
SKY_EXTERN void *
sky_calloc(size_t count, size_t nbytes);

/** Deallocate a previously allocated block of memory.
 *  All memory allocated via sky_calloc(), sky_malloc(), sky_realloc(),
 *  sky_memdup(), sky_strdup(), or sky_strndup() must have a corresponding call
 *  to sky_free(); otherwise, the memory will be leaked. There is no garbage
 *  collection at this level.
 *
 *  @param[in]  pointer the pointer to the block of memory to deallocate. It
 *                      is safe to pass @c NULL for the pointer, in which case
 *                      the call will essentially be a NOP.
 */
SKY_EXTERN void
sky_free(void *pointer);

/** Allocate a new block of memory.
  * The content of the newly allocated memory is undefined. If zeroed memory
  * is needed, use sky_calloc() instead of manually zeroing the memory returned
  * by sky_malloc(), because the underlying implementation of sky_calloc()
  * could be doing something special to quickly return zeroed memory. Memory
  * allocated via sky_malloc() is guaranteed to be 16-byte aligned.
  *
  * @param[in]  nbytes  the number of bytes to allocate, which must be greater
  *                     than zero.
  */
SKY_EXTERN void *
sky_malloc(size_t nbytes);

/** Perform idle tasks while the calling thread has nothing else to do.
  * This function can be called by a thread when it has no other work to do to
  * allow the malloc implementation to do anything that it might have to do.
  *
  * @return     @c SKY_TRUE if the implementation still potentially has more
  *             work to be done, or @ SKY_FALSE if there's nothing more that
  *             to do.
  */
SKY_EXTERN sky_bool_t
sky_malloc_idle(void);

/** Allocate a new block of memory with a specific byte alignment.
  * The newly allocated memory is guaranteed to be allocated as requested,
  * provided that the requested alignment is legal. Alignment must always be
  * a power of 2, and within the range of 16 and the hardware's page size.
  * The content of the newly allocated memory is undefined. Memory allocated
  * via sky_memalign() should be deallocated via sky_free().
  *
  * @param[in]  alignment   the requested byte alignment, which must always be
  *                         a power of 2 in the range of 16 and the hardware's
  *                         page size.
  * @param[in]  nbytes      the number of bytes to allocate, which must be
  *                         greater than zero.
  */
SKY_EXTERN void *
sky_memalign(size_t alignment, size_t nbytes);

/** Copy a range of bytes into newly allocated memory.
  * Equivalent to memcpy(malloc(@a nbytes), @a bytes, @a nbytes);
  * Memory allocated via sky_memdup() is guaranteed to be 16-byte aligned.
  * @param[in]  bytes   pointer to the source bytes to copy. May be @c NULL, in
  *                     which case the nbytes parameter will be ignored, and
  *                     the return value will be @c NULL.
  * @param[in]  nbytes  the number of bytes to copy.
  */
SKY_EXTERN void *
sky_memdup(const void *bytes, size_t nbytes);

/** Return the size of an allocation.
  * The size returned by this function will be at least as large as the size
  * that was requested when the allocation was created; however, it may be
  * larger, depending on how the underlying memory allocator implementation
  * works.
  *
  * @param[in]  pointer     the pointer for which the size of its allocation is
  *                         to be returned.
  * @return     the size of the allocation in bytes.
  */
SKY_EXTERN size_t
sky_memsize(const void *pointer);

/** Resize a previously allocated block of memory.
  * If @a old_pointer is passed as @c NULL, sky_realloc() simply calls
  * sky_malloc(). The content of the memory before the resize is copied into
  * the new memory. If the new size is greater than the old size, the content
  * of the additional area is undefined. Memory allocated via sky_realloc() is
  * guaranteed to be 16-byte aligned.
  * 
  * @param[in]  old_pointer the pointer to the block of memory to resize. It
  *                         may be @c NULL, in which case sky_realloc() is the
  *                         same as sky_malloc().
  * @param[in]  new_nbytes  the new size of the allocation, which must be
  *                         greater than zero.
  */
SKY_EXTERN void *
sky_realloc(void *old_pointer, size_t new_nbytes);

/** Copy a C-style string into newly allocated memory.
  * Equivalent to strcpy(malloc(strlen(@a string) + 1), @a string);
  *
  * @param[in]  string  the C-style string to copy. May be @c NULL, in which
  *                     case the return value will be @c NULL.
  */
SKY_EXTERN char *
sky_strdup(const char *string);

/** Copy a limited portion of a C-style string into newly allocated memory.
  * The new allocation will be sized to at most @a string_length + 1 to make
  * room for a zero-byte to terminate the string. If the original string to
  * copy is shorter than @a string_length, the resulting string will be no
  * longer than the original.
  *
  * @param[in]  string          the C-style string to copy. May be @c NULL, in
  *                             which case @a string_length will be ignored,
  *                             and the return value will be @c NULL.
  * @param[in]  string_length   the maximum number of bytes to copy from the
  *                             string.
  */
SKY_EXTERN char *
sky_strndup(const char *string, size_t string_length);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MALLOC_H__ */

/** @} **/
/** @} **/
