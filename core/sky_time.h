/** @file
  * @brief
  * @defgroup sky_time Time Utilities
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_TIME_H__
#define __SKYTHON_CORE_SKY_TIME_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** A representation of time expressed in nanoseconds. **/
typedef uint64_t sky_time_t;


/** The constant value representing infinite time. **/
#define SKY_TIME_INFINITE               UINT64_C(0xFFFFFFFFFFFFFFFF)


/** Get the current time value in nanoseconds.
  * The reference point for the start of the time value is undefined. That is,
  * a return value of 0 may mean something different on different platforms, or
  * it may even mean something different every time the program starts. The
  * time value will always be monotonic, and it will always be in units of
  * nanoseconds.
  */
SKY_EXTERN sky_time_t
sky_time_current(void);


/** Convert an object to a sky_time_t representation. **/
SKY_EXTERN sky_time_t
sky_time_fromobject(sky_object_t object);


/** Convert a @c struct @c timespec to @c sky_time_t. **/
SKY_EXTERN_INLINE sky_time_t
sky_time_fromtimespec(const struct timespec *ts)
{
    return (ts->tv_sec * UINT64_C(1000000000)) + ts->tv_nsec;
}


/** Convert a @c sky_time_t to @c struct @c timespec. **/
SKY_EXTERN_INLINE struct timespec
sky_time_totimespec(sky_time_t time)
{
    struct timespec timespec;

    timespec.tv_sec  = (long)(time / UINT64_C(1000000000));
    timespec.tv_nsec = time % UINT64_C(1000000000);

    return timespec;
}


/** Convert a @c struct @c timeval to @c sky_time_t. **/
SKY_EXTERN_INLINE sky_time_t
sky_time_fromtimeval(const struct timeval *tv)
{
    return (tv->tv_sec * UINT64_C(1000000000)) + (tv->tv_usec * UINT64_C(1000));
}


/** Convert a @c sky_time_t to a @c struct @c timeval. **/
SKY_EXTERN_INLINE struct timeval
sky_time_totimeval(sky_time_t time)
{
    struct timeval  timeval;

    timeval.tv_sec  = (long)(time / UINT64_C(1000000000));
    timeval.tv_usec = (time % UINT64_C(1000000000)) / 1000;

    return timeval;
}


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TIME_H__ */

/** @} **/
