#include "sky_private.h"
#include "sky_module_private.h"


static pthread_cond_t               sky_atfork_cond_parent   = PTHREAD_COND_INITIALIZER;
static pthread_cond_t               sky_atfork_cond_prepare  = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t              sky_atfork_mutex         = PTHREAD_MUTEX_INITIALIZER;
static volatile int32_t             sky_atfork_count_active  = 0;
static volatile int32_t             sky_atfork_count_waiting = 0;
static volatile sky_atfork_state_t  sky_atfork_state         = SKY_ATFORK_STATE_IDLE;


static sky_bool_t
sky_atfork_state_sync(sky_atfork_tlsdata_t *tlsdata)
    /* This must be called with sky_atfork_mutex locked by the calling thread.
     * Returns SKY_TRUE if the state was updated.
     */
{
    switch (sky_atfork_state) {
        case SKY_ATFORK_STATE_IDLE:
            if (SKY_ATFORK_STATE_IDLE == tlsdata->state) {
                return SKY_FALSE;
            }
            sky_error_validate_debug(SKY_ATFORK_STATE_IDLE == tlsdata->state);
            return SKY_FALSE;

        case SKY_ATFORK_STATE_PREPARING:
            if (SKY_ATFORK_STATE_IDLE == tlsdata->state) {
                tlsdata->state = SKY_ATFORK_STATE_PREPARING;
                if (!--sky_atfork_count_waiting) {
                    pthread_cond_signal(&sky_atfork_cond_prepare);
                }
                return SKY_TRUE;
            }
            if (SKY_ATFORK_STATE_PREPARING == tlsdata->state) {
                return SKY_FALSE;
            }
            break;

        case SKY_ATFORK_STATE_RESUMING:
            if (SKY_ATFORK_STATE_PREPARING == tlsdata->state) {
                tlsdata->state = SKY_ATFORK_STATE_IDLE;
                if (!--sky_atfork_count_waiting) {
                    pthread_cond_signal(&sky_atfork_cond_prepare);
                }
                return SKY_TRUE;
            }
            if (SKY_ATFORK_STATE_IDLE == tlsdata->state) {
                return SKY_FALSE;
            }
            break;
    }

    sky_error_fatal("internal error");
    return SKY_FALSE;
}


void
sky_atfork_tlsdata_cleanup(sky_atfork_tlsdata_t *tlsdata)
{
    tlsdata->wakeup_function = NULL;
    tlsdata->wakeup_arg      = NULL;

    pthread_mutex_lock(&sky_atfork_mutex);
    --sky_atfork_count_active;
    sky_atfork_state_sync(tlsdata);
    pthread_mutex_unlock(&sky_atfork_mutex);
}


void
sky_atfork_check(void)
    /* Skython "owned" threads will periodically call this function to check
     * for a process fork.
     */
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    if (!(tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK)) {
        return;
    }

    /* If this thread is holding any hazard pointers, it cannot check-in. It
     * must let them go first.
     */
    if (tlsdata->hazard_pointer_tlsdata.active_hazard_count) {
        return;
    }

    /* Check the atfork status flag before acquiring the lock to reduce
     * contention on the lock. Threads will be checking in often, but forks
     * should not be occurring very often at all. Avoid trying to acquire the
     * lock as much as possible.
     */
    if (SKY_ATFORK_STATE_IDLE != sky_atfork_state) {
        pthread_mutex_lock(&sky_atfork_mutex);
        if (sky_atfork_state_sync(&(tlsdata->atfork_tlsdata))) {
            while (SKY_ATFORK_STATE_PREPARING == sky_atfork_state) {
                pthread_cond_wait(&sky_atfork_cond_parent,
                                  &sky_atfork_mutex);
            }
            sky_atfork_state_sync(&(tlsdata->atfork_tlsdata));
        }
        pthread_mutex_unlock(&sky_atfork_mutex);
    }
}


void
sky_atfork_register(sky_atfork_wakeup_t function, void *arg)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    tlsdata->atfork_tlsdata.wakeup_function = NULL;
    sky_atomic_sfence();

    tlsdata->atfork_tlsdata.wakeup_arg = arg;
    sky_atomic_sfence();

    tlsdata->atfork_tlsdata.wakeup_function = function;

    if (!(tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK)) {
        pthread_mutex_lock(&sky_atfork_mutex);
        tlsdata->flags |= SKY_TLSDATA_FLAG_ATFORK;
        ++sky_atfork_count_active;
        if (SKY_ATFORK_STATE_PREPARING == sky_atfork_state) {
            ++sky_atfork_count_waiting;
        }
        pthread_mutex_unlock(&sky_atfork_mutex);
    }
}


void
sky_atfork_prepare(void)
    /* The prepare handler starts by making sure that all running threads that
     * are "owned" by Skython itself are suspended. Once everything is stopped,
     * control is given to the tlsdata module to do its preparation. Threads
     * remain stopped when the prepare handler exits.
     */
{
    sky_tlsdata_t           *tlsdata = sky_tlsdata_get();

    uint32_t                id;
    sky_tlsdata_t           *alien_tlsdata;
    sky_hazard_pointer_t    *hazard;

    sky_module_import_lock_acquire();

    pthread_mutex_lock(&sky_atfork_mutex);
    while (SKY_ATFORK_STATE_IDLE != sky_atfork_state) {
        /* Another thread is in the process of forking. Do our check-in and
         * wait our turn.
         */
        pthread_mutex_unlock(&sky_atfork_mutex);
        sky_atfork_check();
        pthread_mutex_lock(&sky_atfork_mutex);
    }
    sky_atfork_state = SKY_ATFORK_STATE_PREPARING;
    tlsdata->atfork_tlsdata.state = SKY_ATFORK_STATE_PREPARING;

    sky_atfork_count_waiting = sky_atfork_count_active;
    if (tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK) {
        --sky_atfork_count_waiting;
    }

    pthread_mutex_unlock(&sky_atfork_mutex);
    for (id = 1;  id <= sky_tlsdata_highestid();  ++id) {
        if ((hazard = sky_tlsdata_getid(id)) != NULL) {
            if ((alien_tlsdata = hazard->pointer) != NULL &&
                (alien_tlsdata->flags & SKY_TLSDATA_FLAG_ALIVE) &&
                (alien_tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK) &&
                alien_tlsdata->atfork_tlsdata.wakeup_function)
            {
                alien_tlsdata->atfork_tlsdata.wakeup_function(
                        alien_tlsdata->atfork_tlsdata.wakeup_arg);
            }
            sky_hazard_pointer_release(hazard);
        }
    }
    pthread_mutex_lock(&sky_atfork_mutex);

    while (sky_atfork_count_waiting > 0) {
        pthread_cond_wait(&sky_atfork_cond_prepare, &sky_atfork_mutex);
    }

    /* At this point, all Skython "owned" threads are now suspended waiting for
     * the parent cond to be signaled. The sky_atfork_mutex remains
     * locked until after the fork completes so that any new threads starting
     * will be blocked until after the fork finishes, leaving Skython in an
     * internally consistent state.
     */

    sky_tlsdata_atfork_prepare();
}


void
sky_atfork_parent(void)
/* The parent handler runs the object parent atfork handlers. The handlers are
 * run in the reverse order that the prepare handlers were run. Finally, the
 * Skython "owned" threads are allowed to continue running.
 */
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_tlsdata_atfork_parent();

    /* Wait for all threads to wake up again */
    sky_atfork_state = SKY_ATFORK_STATE_RESUMING;
    sky_atfork_count_waiting = sky_atfork_count_active;
    if (tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK) {
        --sky_atfork_count_waiting;
    }
    pthread_cond_broadcast(&sky_atfork_cond_parent);
    while (sky_atfork_count_waiting > 0) {
        pthread_cond_wait(&sky_atfork_cond_prepare, &sky_atfork_mutex);
    }
    sky_atfork_state = SKY_ATFORK_STATE_IDLE;
    tlsdata->atfork_tlsdata.state = SKY_ATFORK_STATE_IDLE;
    pthread_mutex_unlock(&sky_atfork_mutex);

    /* Resume all Skython "owned" threads. */
    sky_module_import_lock_release();
}


void
sky_atfork_child(void)
    /* The child handler runs the object child atfork handlers. The handlers are
     * run in the reverse order that the prepare handlers were run. Finally, all
     * of the thread-local storage is cleaned up for the threads that no longer
     * exist in the process.
     */
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    /* Reset the atfork thread suspension machinery. There's really no right
     * thing to do here with sky_atfork_mutex. It's not really a valid mutex
     * anymore, especially since we know that every thread running in the
     * parent is waiting for it, but they don't exist anymore in the child.
     * It's also not valid to re-initialize it, because resources could be
     * leaked, but also because it's an initialized mutex, so an implementation
     * could return EBUSY and refuse to re-initialize. It's not valid to
     * destroy the mutex either, because it's locked and because it's not a
     * valid mutex anymore. The re-initialization problem seems to be the best
     * option, because an implementation isn't likely going to be able to
     * reliably check for re-initialization since the standard doesn't say
     * anything about the memory requirements before initializing. We'll zero
     * the memory first, just in case (sizeof(pthread_mutex_t) should be known
     * so that things like PTHREAD_MUTEX_INITIALIZER can work).
     */
    sky_atfork_state = SKY_ATFORK_STATE_IDLE;
    tlsdata->atfork_tlsdata.state = SKY_ATFORK_STATE_IDLE;
    memset(&sky_atfork_mutex, 0, sizeof(sky_atfork_mutex));
    pthread_mutex_init(&sky_atfork_mutex, NULL);
    memset(&sky_atfork_cond_parent, 0, sizeof(sky_atfork_cond_parent));
    pthread_cond_init(&sky_atfork_cond_parent, NULL);
    memset(&sky_atfork_cond_prepare, 0, sizeof(sky_atfork_cond_prepare));
    pthread_cond_init(&sky_atfork_cond_prepare, NULL);

    sky_format_atfork_child();
#if 0   /* ATFORK AIO */
    sky_aio_handle_atfork_child();
#endif
    sky_tlsdata_atfork_child();
    sky_signal_atfork_child();

    /* Call threading._after_fork(), but only if the threading module has been
     * imported already. Otherwise, it will deadlock, because the import lock
     * is still held.
     */
    if (!(sky_interpreter_flags() & SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES)) {
        sky_module_t    threading;

        threading = sky_dict_get(sky_interpreter_modules(),
                                 SKY_STRING_LITERAL("threading"),
                                 NULL);
        if (threading) {
            SKY_ERROR_TRY {
                sky_object_callmethod(threading,
                                      SKY_STRING_LITERAL("_after_fork"),
                                      NULL,
                                      NULL);
            } SKY_ERROR_EXCEPT_ANY {
                sky_error_display(sky_error_current());
            } SKY_ERROR_TRY_END;
        }
    }

    sky_module_import_lock_release();
}


void
sky_atfork_initialize(void)
{
    sky_atfork_register(NULL, NULL);
}
