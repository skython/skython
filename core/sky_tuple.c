#include "sky_private.h"
#include "sky_tuple_private.h"


typedef struct sky_tuple_iterator_data_s {
    sky_tuple_t                         tuple;
    ssize_t                             next_index;
} sky_tuple_iterator_data_t;

SKY_EXTERN_INLINE sky_tuple_iterator_data_t *
sky_tuple_iterator_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_tuple_iterator_type) {
        return (sky_tuple_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_tuple_iterator_type);
}


struct sky_tuple_iterator_s {
    sky_object_data_t                   object_data;
    sky_tuple_iterator_data_t           iterator_data;
};


sky_tuple_data_t *
sky_tuple_data(sky_object_t object, sky_tuple_data_t *tagged_data)
{
    if (sky_tuple_empty == object) {
        tagged_data->len = 0;
        tagged_data->objects = tagged_data->u.tiny_objects;
        return tagged_data;
    }
    if (sky_object_istaggedtuple(object)) {
        tagged_data->len = 1;
        tagged_data->objects = tagged_data->u.tiny_objects;
        tagged_data->u.tiny_objects[0] = sky_object_taggedtupleobject(object);
        return tagged_data;
    }
    if (sky_object_type(object) == sky_tuple_type) {
        return (sky_tuple_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_tuple_type);
}


static void
sky_tuple_instance_iterate(sky_object_t                     self,
                           void *                           data,
                           sky_sequence_iterate_state_t *   state,
                           sky_object_t *                   objects,
                           SKY_UNUSED ssize_t               nobjects)
{
    if (sky_tuple_empty == self || state->state) {
        state->objects = NULL;
        state->nobjects = 0;
    }
    else if (sky_object_istaggedtuple(self)) {
        state->state = 1;
        state->objects = objects;
        state->nobjects = 1;
        objects[0] = sky_object_taggedtupleobject(self);
    }
    else {
        sky_tuple_data_t    *tuple_data = data;

        state->state = tuple_data->len;
        state->objects = tuple_data->objects;
        state->nobjects = tuple_data->len;
    }
}


static void
sky_tuple_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
    /* This can never be called for a tagged tuple. */
{
    sky_tuple_data_t    *tuple_data = data;

    if (tuple_data->objects != tuple_data->u.tiny_objects &&
        tuple_data->u.free)
    {
        tuple_data->u.free(tuple_data->objects);
    }
    tuple_data->objects = NULL;
}


static void
sky_tuple_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
    /* This can never be called for a tagged tuple. */
{
    sky_tuple_data_t    *tuple_data = data;

    register ssize_t    i;

    for (i = tuple_data->len - 1; i >= 0; --i) {
        sky_object_visit(tuple_data->objects[i], visit_data);
    }
}


static void
sky_tuple_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_tuple_iterator_data_t   *iterator_data = data;

    sky_object_visit(iterator_data->tuple, visit_data);
}


static sky_tuple_t
sky_tuple_allocate(sky_type_t type, ssize_t len)
{
    sky_tuple_t         tuple;
    sky_tuple_data_t    *tuple_data;

    sky_error_validate_debug(len > 0);
    tuple = sky_object_allocate(type);
    tuple_data = sky_tuple_data(tuple, NULL);
    tuple_data->len = len;
    if (len <= SKY_TUPLE_TINY_COUNT) {
        tuple_data->objects = tuple_data->u.tiny_objects;
    }
    else {
        tuple_data->objects = sky_calloc(len, sizeof(sky_object_t));
        tuple_data->u.free = sky_free;
    }

    return tuple;
}


static sky_tuple_t
sky_tuple_concatenate(sky_tuple_t tuple, sky_tuple_t other)
{
    sky_tuple_t         new_tuple;
    sky_tuple_data_t    *new_data, *other_data, other_tagged_data,
                        *tuple_data, tuple_tagged_data;

    if (sky_tuple_empty == tuple) {
        return other;
    }
    if (sky_tuple_empty == other) {
        return tuple;
    }
    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    other_data = sky_tuple_data(other, &other_tagged_data);

    new_tuple = sky_tuple_allocate(sky_tuple_type,
                                   tuple_data->len + other_data->len);
    new_data = sky_tuple_data(new_tuple, NULL);
    memcpy(&(new_data->objects[0]),
           &(tuple_data->objects[0]),
           tuple_data->len * sizeof(sky_object_t));
    memcpy(&(new_data->objects[tuple_data->len]),
           &(other_data->objects[0]),
           other_data->len * sizeof(sky_object_t));
    sky_object_gc_setarray(new_data->len, new_data->objects, new_tuple);

    return new_tuple;
}


static sky_tuple_t
sky_tuple_pack1(sky_type_t type, sky_object_t object)
{
    sky_tuple_t         tuple;
    sky_tuple_data_t    *tuple_data;

    if (!object) {
        object = sky_None;
    }
    if (sky_tuple_type == type && !sky_object_istaggedtuple(object)) {
        return (sky_tuple_t)((uintptr_t)object | 0x8);
    }
    tuple = sky_tuple_allocate(type, 1);
    tuple_data = sky_tuple_data(tuple, NULL);
    sky_object_gc_set(&(tuple_data->objects[0]), object, tuple);

    return tuple;
}


sky_tuple_t
sky_tuple_add(sky_tuple_t tuple, sky_object_t other)
{
    if (!sky_object_isa(other, sky_tuple_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only concatenate tuple (not %#@) to tuple",
                sky_type_name(sky_object_type(other)));
    }
    return sky_tuple_concatenate(tuple, other);
}


sky_object_t
sky_tuple_compare(sky_object_t      object_1,
                  sky_object_t      object_2,
                  sky_compare_op_t  compare_op)
{
    ssize_t             i, len;
    sky_tuple_data_t    tagged_data_1, tagged_data_2,
                        *tuple_data_1, *tuple_data_2;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (object_1 == object_2) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (object_1 == object_2) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(object_1, sky_tuple_type) ||
        !sky_object_isa(object_2, sky_tuple_type))
    {
        return sky_NotImplemented;
    }

    tuple_data_1 = sky_tuple_data(object_1, &tagged_data_1);
    tuple_data_2 = sky_tuple_data(object_2, &tagged_data_2);

    if (tuple_data_1->len != tuple_data_2->len) {
        if (SKY_COMPARE_OP_EQUAL == compare_op) {
            return sky_False;
        }
        if (SKY_COMPARE_OP_NOT_EQUAL == compare_op) {
            return sky_True;
        }
    }

    len = SKY_MIN(tuple_data_1->len, tuple_data_2->len);
    for (i = 0; i < len; ++i) {
        if (!sky_object_compare(tuple_data_1->objects[i],
                                tuple_data_2->objects[i],
                                SKY_COMPARE_OP_EQUAL))
        {
            switch (compare_op) {
                case SKY_COMPARE_OP_EQUAL:
                    return sky_False;
                case SKY_COMPARE_OP_NOT_EQUAL:
                    return sky_True;
                default:
                    return (sky_object_compare(tuple_data_1->objects[i],
                                               tuple_data_2->objects[i],
                                               compare_op) ? sky_True
                                                           : sky_False);
            }
        }
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            return (tuple_data_1->len == tuple_data_2->len ? sky_True
                                                           : sky_False);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (tuple_data_1->len != tuple_data_2->len ? sky_True
                                                           : sky_False);
        case SKY_COMPARE_OP_LESS:
            return (tuple_data_1->len < tuple_data_2->len ? sky_True
                                                          : sky_False);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (tuple_data_1->len <= tuple_data_2->len ? sky_True
                                                           : sky_False);
        case SKY_COMPARE_OP_GREATER:
            return (tuple_data_1->len > tuple_data_2->len ? sky_True
                                                          : sky_False);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (tuple_data_1->len >= tuple_data_2->len ? sky_True
                                                           : sky_False);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }

    sky_error_fatal("internal error");
}


sky_bool_t
sky_tuple_contains(sky_tuple_t tuple, sky_object_t item)
{
    ssize_t             i;
    sky_tuple_data_t    tagged_data, *tuple_data;

    if (sky_tuple_empty == tuple) {
        return SKY_FALSE;
    }

    tuple_data = sky_tuple_data(tuple, &tagged_data);
    for (i = 0; i < tuple_data->len; ++i) {
        if (sky_object_compare(tuple_data->objects[i],
                               item,
                               SKY_COMPARE_OP_EQUAL))
        {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


ssize_t
sky_tuple_count(sky_tuple_t tuple, sky_object_t value)
{
    ssize_t             i, count;
    sky_tuple_data_t    *tuple_data;

    if (sky_tuple_empty == tuple) {
        return 0;
    }
    if (sky_object_istaggedtuple(tuple)) {
        return (sky_object_compare(sky_object_taggedtupleobject(tuple),
                                   value,
                                   SKY_COMPARE_OP_EQUAL) ? 1 : 0);
    }

    count = 0;
    tuple_data = sky_tuple_data(tuple, NULL);
    for (i = 0; i < tuple_data->len; ++i) {
        if (sky_object_compare(tuple_data->objects[i],
                               value,
                               SKY_COMPARE_OP_EQUAL))
        {
            ++count;
        }
    }

    return count;
}


static sky_tuple_t
sky_tuple_createwithtype(sky_object_t object, sky_type_t type)
{
    ssize_t             avail, i, j, len;
    sky_tuple_t         tuple;
    sky_object_t        item, iterator, *new_objects;
    sky_tuple_data_t    *tuple_data;

    sky_error_validate_debug(sky_type_issubtype(type, sky_tuple_type));
    if (sky_object_isa(object, type)) {
        return object;
    }

    if (!sky_object_isiterable(object)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(object)));
    }
    len = sky_object_length_hint(object, 8);

    /* Use fast iteration if possible */
    if (sky_sequence_isiterable(object)) {
        if (!len) {
            if (sky_tuple_type == type) {
                return sky_tuple_empty;
            }
            return sky_object_allocate(type);
        }
        if (1 == len) {
            /* Fast iterable does not imply indexable. Try to index, but fall
             * back to iterating if it fails.
             */
            SKY_ERROR_TRY {
                tuple = sky_tuple_pack1(type, sky_sequence_get(object, 0));
            } SKY_ERROR_EXCEPT_ANY {
                tuple = NULL;
            } SKY_ERROR_TRY_END;
            if (tuple) {
                return tuple;
            }
        }

        tuple = sky_tuple_allocate(type, len);
        tuple_data = sky_tuple_data(tuple, NULL);

        i = 0;
        SKY_SEQUENCE_FOREACH(object, item) {
            sky_object_gc_set(&(tuple_data->objects[i++]), item, tuple);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        tuple = sky_tuple_allocate(type, (len ? len : 8));
        tuple_data = sky_tuple_data(tuple, NULL);

        if (tuple_data->objects == tuple_data->u.tiny_objects) {
            avail = SKY_TUPLE_TINY_COUNT;
        }
        else {
            avail = sky_memsize(tuple_data->objects) / sizeof(sky_object_t);
        }
        iterator = sky_object_iter(object);
        for (i = 0; (item = sky_object_next(iterator, NULL)) != NULL; ++i) {
            if (i >= avail) {
                /* Can't do realloc, because gc mark could be happening right
                 * now. Have to do it the slow way. The good news is that this
                 * case should be highly unlikely.
                 */
                new_objects = sky_calloc(++avail, sizeof(sky_object_t));
                for (j = 0; j < i; ++j) {
                    sky_object_gc_set(&(new_objects[j]),
                                      tuple_data->objects[j],
                                      tuple);
                }
                if (tuple_data->objects != tuple_data->u.tiny_objects) {
                    sky_free(tuple_data->objects);
                }
                tuple_data->objects = new_objects;
                avail = sky_memsize(tuple_data->objects) / sizeof(sky_object_t);
            }
            sky_object_gc_set(&(tuple_data->objects[i]), item, tuple);
        }
        tuple_data->len = i;
    }

    if (sky_tuple_type == type && !tuple_data->len) {
        return sky_tuple_empty;
    }

    return tuple;
}


sky_tuple_t
sky_tuple_create(sky_object_t object)
{
    return sky_tuple_createwithtype(object, sky_tuple_type);
}


sky_tuple_t
sky_tuple_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t             i;
    sky_tuple_t         tuple;
    sky_object_t        object;
    sky_tuple_data_t    *tuple_data;

    if (!count) {
        return sky_tuple_empty;
    }
    if (1 == count) {
        return sky_tuple_pack1(sky_tuple_type, objects[0]);
    }

    tuple = sky_tuple_allocate(sky_tuple_type, count);
    tuple_data = sky_tuple_data(tuple, NULL);
    for (i = 0; i < count; ++i) {
        if (!(object = objects[i])) {
            object = sky_None;
        }
        tuple_data->objects[i] = object;
    }
    sky_object_gc_setarray(count, tuple_data->objects, tuple);

    return tuple;
}


sky_tuple_t
sky_tuple_createwitharray(ssize_t count, sky_object_t *objects, sky_free_t free)
{
    ssize_t             i;
    sky_tuple_t         tuple;
    sky_tuple_data_t    *tuple_data;

    if (!count) {
        if (free) {
            free(objects);
        }
        return sky_tuple_empty;
    }
    if (1 == count) {
        tuple = sky_tuple_pack1(sky_tuple_type, objects[0]);
        if (free) {
            free(objects);
        }
        return tuple;
    }

    tuple = sky_object_allocate(sky_tuple_type);
    tuple_data = sky_tuple_data(tuple, NULL);

    tuple_data->len = count;
    tuple_data->objects = objects;
    tuple_data->u.free = free;

    for (i = 0; i < count; ++i) {
        if (!objects[i]) {
            objects[i] = sky_None;
        }
    }
    sky_object_gc_setarray(count, objects, tuple);

    return tuple;
}


sky_object_t
sky_tuple_eq(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_EQUAL);
}


ssize_t
sky_tuple_find(sky_tuple_t tuple, sky_object_t item)
{
    ssize_t             i;
    sky_tuple_data_t    *tuple_data;

    if (sky_tuple_empty == tuple) {
        return -1;
    }
    if (sky_object_istaggedtuple(tuple)) {
        return (sky_object_compare(sky_object_taggedtupleobject(tuple),
                                   item,
                                   SKY_COMPARE_OP_EQUAL) ? 0 : -1);
    }

    tuple_data = sky_tuple_data(tuple, NULL);
    for (i = 0; i < tuple_data->len; ++i) {
        if (sky_object_compare(tuple_data->objects[i],
                               item,
                               SKY_COMPARE_OP_EQUAL))
        {
            return i;
        }
    }

    return -1;
}


sky_object_t
sky_tuple_ge(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_tuple_get(sky_tuple_t tuple, ssize_t index)
{
    sky_tuple_data_t    *tuple_data, tuple_tagged_data;

    if (sky_tuple_empty == tuple) {
        sky_error_raise_string(sky_IndexError, "tuple index out of range");
    }
    if (sky_object_istaggedtuple(tuple)) {
        if (index < 0) {
            ++index;
        }
        if (index) {
            sky_error_raise_string(sky_IndexError, "tuple index out of range");
        }
        return sky_object_taggedtupleobject(tuple);
    }
    
    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    if (index < 0) {
        index += tuple_data->len;
        if (index < 0) {
            sky_error_raise_string(sky_IndexError, "tuple index out of range");
        }
    }
    if (index >= tuple_data->len) {
        sky_error_raise_string(sky_IndexError, "tuple index out of range");
    }

    return tuple_data->objects[index];
}


sky_object_t
sky_tuple_getitem(sky_tuple_t tuple, sky_object_t item)
{
    ssize_t             i, j, len, start, step, stop;
    sky_object_t        slice;
    sky_tuple_data_t    *slice_data, slice_tagged_data,
                        *tuple_data, tuple_tagged_data;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        return sky_tuple_get(tuple,
                             sky_integer_value_clamped(sky_number_index(item),
                                                       SSIZE_MIN, SSIZE_MAX));
    }
    if (!sky_object_isa(item, sky_slice_type)) {
        sky_error_raise_string(sky_TypeError,
                               "tuple indices must be integers");
    }

    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    if (!(len = sky_slice_range(item, tuple_data->len, &start, &stop, &step))) {
        return sky_tuple_empty;
    }
    if (1 == step && len == tuple_data->len && sky_tuple_type == sky_object_type(tuple)) {
        return tuple;
    }

    slice = sky_tuple_allocate(sky_tuple_type, len);
    slice_data = sky_tuple_data(slice, &slice_tagged_data);
    for (i = start, j = 0; j < len; i+= step, ++j) {
        sky_object_gc_set(&(slice_data->objects[j]),
                          tuple_data->objects[i],
                          slice);
    }

    return slice;
}


sky_tuple_t
sky_tuple_getnewargs(sky_tuple_t tuple)
{
    return sky_object_build("(O)",
                            sky_tuple_slice(tuple, 0, sky_tuple_len(tuple), 1));
}


sky_object_t
sky_tuple_gt(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_GREATER);
}


uintptr_t
sky_tuple_hash(sky_object_t object)
{
    uintptr_t               mult;
    sky_tuple_data_t        *tuple_data, tuple_tagged_data;
    register ssize_t        length;
    register uintptr_t      x, y;
    register sky_object_t   *p;

    x = 0x345678L;
    if (sky_tuple_empty != object) {
        tuple_data = sky_tuple_data(object, &tuple_tagged_data);
        length = tuple_data->len;
        mult = 1000003L;
        p = tuple_data->objects;
        while (--length >= 0) {
            y = sky_object_hash(*p++);
            x = (x ^ y) * mult;
            /* the cast might truncate len; that doesn't change hash stability */
            mult += (size_t)(82520L + length + length);
        }
    }
    return x + 97531L;
}


ssize_t
sky_tuple_index(sky_tuple_t     tuple,
                sky_object_t    value,
                ssize_t         start,
                ssize_t         stop)
{
    ssize_t             i;
    sky_tuple_data_t    *tuple_data, tuple_tagged_data;

    if (sky_tuple_empty != tuple) {
        tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);

        if (start < 0) {
            start += tuple_data->len;
            if (start < 0) {
                start = 0;
            }
        }
        if (stop < 0) {
            stop += tuple_data->len;
            if (stop < 0) {
                stop = 0;
            }
        }
        if (stop > tuple_data->len) {
            stop = tuple_data->len;
        }

        for (i = start; i < stop; ++i) {
            if (sky_object_compare(tuple_data->objects[i],
                                   value,
                                   SKY_COMPARE_OP_EQUAL))
            {
                return i;
            }
        }
    }

    sky_error_raise_string(sky_ValueError,
                           "tuple.index(x): x not in tuple");
}


sky_tuple_t
sky_tuple_insert(sky_tuple_t tuple, ssize_t index, sky_object_t object)
{
    ssize_t             i, j;
    sky_tuple_t         new_tuple;
    sky_tuple_data_t    *new_tuple_data, *tuple_data, tuple_tagged_data;

    if (sky_tuple_empty == tuple &&
        !sky_object_istaggedtuple(object) &&
        !index)
    {
        return sky_tuple_pack1(sky_tuple_type, object);
    }

    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    if (index < 0) {
        index += tuple_data->len;
        if (index < 0) {
            index = 0;
        }
    }
    if (index > tuple_data->len) {
        index = tuple_data->len;
    }

    new_tuple = sky_tuple_allocate(sky_tuple_type, tuple_data->len + 1);
    new_tuple_data = sky_tuple_data(new_tuple, NULL);

    i = j = 0;
    while (i < index) {
        sky_object_gc_set(&(new_tuple_data->objects[j++]),
                          tuple_data->objects[i++],
                          new_tuple);
    }
    sky_object_gc_set(&(new_tuple_data->objects[j++]), object, new_tuple);
    while (i < tuple_data->len) {
        sky_object_gc_set(&(new_tuple_data->objects[j++]),
                          tuple_data->objects[i++],
                          new_tuple);
    }

    return new_tuple;
}


sky_object_t
sky_tuple_iter(sky_tuple_t tuple)
{
    sky_tuple_iterator_t        iterator;
    sky_tuple_iterator_data_t   *iterator_data;

    iterator = sky_object_allocate(sky_tuple_iterator_type);
    iterator_data = sky_tuple_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->tuple)), tuple, iterator);

    return iterator;
}


sky_object_t
sky_tuple_le(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_LESS_EQUAL);
}


ssize_t
sky_tuple_len(sky_tuple_t tuple)
{
    if (sky_tuple_empty == tuple) {
        return 0;
    }
    if (sky_object_istaggedtuple(tuple)) {
        return 1;
    }
    return sky_tuple_data(tuple, NULL)->len;
}


sky_object_t
sky_tuple_lt(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_tuple_mul(sky_tuple_t tuple, sky_object_t other)
{
    ssize_t count;

    if (!SKY_OBJECT_METHOD_SLOT(other, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "can't multiply tuple by non-int of type %#@",
                               sky_type_name(sky_object_type(other)));
    }
    other = sky_number_index(other);
    count = sky_integer_value(other, SSIZE_MIN, SSIZE_MAX, NULL);

    return sky_tuple_repeat(tuple, count);
}


sky_object_t
sky_tuple_ne(sky_tuple_t tuple, sky_object_t other)
{
    return sky_tuple_compare(tuple, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_object_t
sky_tuple_new(sky_object_t cls, sky_object_t sequence)
{
    if (!sequence) {
        if (sky_tuple_type == cls) {
            return sky_tuple_empty;
        }
        return sky_object_allocate(cls);
    }

    return sky_tuple_createwithtype(sequence, cls);
}


sky_tuple_t
sky_tuple_pack(ssize_t count, ...)
{
    va_list     ap;
    sky_tuple_t tuple;

    if (!count) {
        return sky_tuple_empty;
    }

    va_start(ap, count);
    if (count > 1) {
        tuple = sky_tuple_packv(count, ap);
    }
    else {
        tuple = sky_tuple_pack1(sky_tuple_type, va_arg(ap, sky_object_t));
    }
    va_end(ap);

    return tuple;
}


sky_tuple_t
sky_tuple_packv(ssize_t count, va_list ap)
{
    ssize_t             i;
    sky_tuple_t         tuple;
    sky_object_t        object;
    sky_tuple_data_t    *tuple_data;

    if (!count) {
        return sky_tuple_empty;
    }
    if (1 == count) {
        return sky_tuple_pack1(sky_tuple_type, va_arg(ap, sky_object_t));
    }

    tuple = sky_tuple_allocate(sky_tuple_type, count);
    tuple_data = sky_tuple_data(tuple, NULL);
    for (i = 0; i < count; ++i) {
        if (!(object = va_arg(ap, sky_object_t))) {
            object = sky_None;
        }
        sky_object_gc_set(&(tuple_data->objects[i]), object, tuple);
    }

    return tuple;
}


sky_tuple_t
sky_tuple_radd(sky_tuple_t tuple, sky_object_t other)
{
    if (!sky_object_isa(other, sky_tuple_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only concatenate tuple (not %#@) to tuple",
                sky_type_name(sky_object_type(other)));
    }
    return sky_tuple_concatenate(other, tuple);
}


sky_tuple_t
sky_tuple_repeat(sky_tuple_t tuple, ssize_t count)
{
    ssize_t             i, j, k;
    sky_tuple_t         new_tuple;
    sky_tuple_data_t    *new_tuple_data, *tuple_data, tuple_tagged_data;

    if (count <= 0 || sky_tuple_empty == tuple) {
        return sky_tuple_empty;
    }
    if (1 == count && sky_tuple_type == sky_object_type(tuple)) {
        return tuple;
    }

    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    if (SSIZE_MAX / count < tuple_data->len) {
        sky_error_raise_string(sky_OverflowError, "tuple too big");
    }
    new_tuple = sky_tuple_allocate(sky_tuple_type, tuple_data->len * count);
    new_tuple_data = sky_tuple_data(new_tuple, NULL);

    k = 0;
    for (j = 0; j < tuple_data->len; ++j, ++k) {
        sky_object_gc_set(&(new_tuple_data->objects[k]),
                          tuple_data->objects[j],
                          new_tuple);
    }
    for (i = 1; i < count; ++i) {
        for (j = 0; j < tuple_data->len; ++j, ++k) {
            new_tuple_data->objects[k] = tuple_data->objects[j];
        }
    }

    return new_tuple;
}


static sky_string_t
sky_tuple_repr(sky_tuple_t tuple)
{
    ssize_t                 i;
    sky_string_t            joiner;
    sky_tuple_data_t        *tuple_data, tuple_tagged_data;
    sky_string_builder_t    builder;

    if (sky_tuple_empty == tuple) {
        return SKY_STRING_LITERAL("()");
    }

    if (sky_object_stack_push(tuple)) {
        return SKY_STRING_LITERAL("(...)");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(tuple, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        sky_string_builder_appendcodepoint(builder, '(', 1);

        tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
        if (tuple_data->len) {
            sky_string_builder_append(builder,
                                      sky_object_repr(tuple_data->objects[0]));
            if (1 == tuple_data->len) {
                sky_string_builder_appendcodepoint(builder, ',', 1);
            }
            else {
                joiner = SKY_STRING_LITERAL(", ");
                for (i = 1; i < tuple_data->len; ++i) {
                    sky_string_builder_append(builder, joiner);
                    sky_string_builder_append(
                            builder,
                            sky_object_repr(tuple_data->objects[i]));
                }
            }
        }

        sky_string_builder_appendcodepoint(builder, ')', 1);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


size_t
sky_tuple_sizeof(sky_object_t object)
{
    sky_tuple_data_t    *tuple_data;

    if (sky_object_istagged(object)) {
        return 0;
    }

    tuple_data = sky_tuple_data(object, NULL);
    if (tuple_data->objects != tuple_data->u.tiny_objects) {
        return sky_memsize(object) + sky_memsize(tuple_data->objects);
    }
    return sky_memsize(object);
}


sky_tuple_t
sky_tuple_slice(sky_tuple_t tuple, ssize_t start, ssize_t stop, ssize_t step)
{
    ssize_t             i, j, len;
    sky_tuple_t         slice;
    sky_tuple_data_t    *slice_data, *tuple_data, tuple_tagged_data;

    tuple_data = sky_tuple_data(tuple, &tuple_tagged_data);
    if (!(len = sky_sequence_indices(tuple_data->len, &start, &stop, &step))) {
        return sky_tuple_empty;
    }
    if (len == tuple_data->len && sky_tuple_type == sky_object_type(tuple)) {
        return tuple;
    }

    slice = sky_tuple_allocate(sky_tuple_type, len);
    slice_data = sky_tuple_data(slice, NULL);
    for (i = start, j = 0; j < len; i+= step, ++j) {
        sky_object_gc_set(&(slice_data->objects[j]),
                          tuple_data->objects[i],
                          slice);
    }

    return slice;
}

void
sky_tuple_iterator_init(sky_object_t self, sky_tuple_t tuple)
{
    sky_tuple_iterator_data_t   *iterator_data = sky_tuple_iterator_data(self);

    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->tuple)), tuple, self);
}


sky_object_t
sky_tuple_iterator_iter(sky_object_t self)
{
    return self;
}


ssize_t
sky_tuple_iterator_len(sky_object_t self)
{
    sky_tuple_iterator_data_t   *iterator_data = sky_tuple_iterator_data(self);

    if (!iterator_data->tuple) {
        return 0;
    }
    return (sky_tuple_len(iterator_data->tuple) - iterator_data->next_index);
}


sky_object_t
sky_tuple_iterator_next(sky_object_t self)
{
    sky_tuple_iterator_data_t   *iterator_data = sky_tuple_iterator_data(self);

    sky_object_t    object;

    if (!iterator_data->tuple) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }
    if (iterator_data->next_index >= sky_tuple_len(iterator_data->tuple)) {
        iterator_data->tuple = NULL;
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    object = sky_tuple_get(iterator_data->tuple, iterator_data->next_index);
    ++iterator_data->next_index;

    return object;
}


sky_object_t
sky_tuple_iterator_reduce(sky_object_t self)
{
    sky_tuple_iterator_data_t   *iterator_data = sky_tuple_iterator_data(self);

    if (!iterator_data->tuple) {
        return sky_object_build("(O(()))", sky_module_getbuiltin("iter"));
    }
    return sky_object_build("(O(O)iz)",
                            sky_module_getbuiltin("iter"),
                            iterator_data->tuple,
                            iterator_data->next_index);
}


void
sky_tuple_iterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_tuple_iterator_data_t   *self_data = sky_tuple_iterator_data(self);

    if (self_data->tuple) {
        self_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
        if (self_data->next_index < 0) {
            self_data->next_index = 0;
        }
        else if (self_data->next_index > sky_object_len(self_data->tuple)) {
            self_data->next_index = sky_object_len(self_data->tuple);
        }
    }
}


static const char sky_tuple_type_doc[] =
"tuple() -> empty tuple\n"
"tuple(iterable) -> tuple initialized from iterable's items\n"
"\n"
"If the argument is a tuple, the return value is the same object.";


SKY_TYPE_DEFINE_ITERABLE(tuple,
                         "tuple",
                         sizeof(sky_tuple_data_t),
                         NULL,
                         sky_tuple_instance_finalize,
                         sky_tuple_instance_visit,
                         sky_tuple_instance_iterate,
                         NULL,
                         0,
                         sky_tuple_type_doc);


SKY_TYPE_DEFINE_SIMPLE(tuple_iterator,
                       "tuple_iterator",
                       sizeof(sky_tuple_iterator_data_t),
                       NULL,
                       NULL,
                       sky_tuple_iterator_instance_visit,
                       0,
                       NULL);


sky_tuple_t const sky_tuple_empty = SKY_OBJECT_TAGGED_TUPLE_EMPTY;


void
sky_tuple_initialize_library(void)
{
    sky_type_setattr_builtin(
            sky_tuple_type,
            "__getnewargs__",
            sky_function_createbuiltin(
                    "tuple.__getnewargs__",
                    NULL,
                    (sky_native_code_function_t)sky_tuple_getnewargs,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_tuple_type,
            "count",
            sky_function_createbuiltin(
                    "tuple.count",
                    "T.count(value) -> integer -- return number of occurrences of value",
                    (sky_native_code_function_t)sky_tuple_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_tuple_type,
            "index",
            sky_function_createbuiltin(
                    "tuple.index",
                    "T.index(value, [start, [stop]]) -> integer -- return first index of value.\n"
                    "Raises ValueError if the value is not present.",
                    (sky_native_code_function_t)sky_tuple_index,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    "start", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_zero,
                    "stop", SKY_DATA_TYPE_INDEX | SKY_DATA_TYPE_INTEGER_CLAMP, sky_integer_create(SSIZE_MAX),
                    NULL));

    sky_type_setmethodslotwithparameters(sky_tuple_type,
            "__new__",
            (sky_native_code_function_t)sky_tuple_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "sequence", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_tuple_type,
            "__repr__", sky_tuple_repr,
            "__lt__", sky_tuple_lt,
            "__le__", sky_tuple_le,
            "__eq__", sky_tuple_eq,
            "__ne__", sky_tuple_ne,
            "__gt__", sky_tuple_gt,
            "__ge__", sky_tuple_ge,
            "__hash__", sky_tuple_hash,
            "__sizeof__", sky_tuple_sizeof,
            "__len__", sky_tuple_len,
            "__getitem__", sky_tuple_getitem,
            "__iter__", sky_tuple_iter,
            "__contains__", sky_tuple_contains,
            "__add__", sky_tuple_add,
            "__mul__", sky_tuple_mul,
            "__radd__", sky_tuple_radd,
            "__rmul__", sky_tuple_mul,
            NULL);


    sky_type_initialize_builtin(sky_tuple_iterator_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_tuple_iterator_type,
            "__init__",
            (sky_native_code_function_t)sky_tuple_iterator_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "tuple", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
            NULL);

    sky_type_setmethodslots(sky_tuple_iterator_type,
            "__len__", sky_tuple_iterator_len,
            "__iter__", sky_tuple_iterator_iter,
            "__next__", sky_tuple_iterator_next,
            "__reduce__", sky_tuple_iterator_reduce,
            "__setstate__", sky_tuple_iterator_setstate,
            NULL);
}
