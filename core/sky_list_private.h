#ifndef __SKYTHON_CORE_SKY_LIST_PRIVATE_H__
#define __SKYTHON_CORE_SKY_LIST_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_object_private.h"


SKY_CDECLS_BEGIN


typedef struct sky_list_table_s {
    ssize_t                             len;
    ssize_t                             capacity;
    sky_object_t                        objects[0];
} sky_list_table_t;

typedef struct sky_list_data_s {
    sky_list_table_t * volatile         table;
} sky_list_data_t;

SKY_EXTERN_INLINE sky_list_data_t *
sky_list_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_list_type) {
        return (sky_list_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_list_type);
}

static inline sky_bool_t
sky_list_isshared(sky_list_t self)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(self));
}

extern sky_list_table_t *
sky_list_data_settable(sky_list_data_t *    list_data,
                       sky_list_table_t *   new_table,
                       sky_bool_t           shared);

SKY_CDECLS_END

#endif  /* __SKYTHON_CORE_SKY_LIST_PRIVATE_H__ */
