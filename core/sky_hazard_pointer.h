/** @file
  * @brief Safe memory reclamation using hazard pointers.
  * @defgroup memory_management Memory Management
  * @{
  * @defgroup sky_hazard_pointer Hazard Pointers (Safe Memory Reclamation)
  * Hazard pointers are described in the paper entitled "Hazard Pointers: Safe
  * Memory Reclamation for Lock-Free Objects" by Maged M Michael, which can
  * be found at http://www.research.ibm.com/people/m/michael/ieeetpds-2004.pdf.
  * 
  * In general, a thread may acquire a hazard pointer to protect a pointer from
  * being deallocated while it's still in use. The intent is for the lifetime
  * of a hazard pointer to be extremely short, existing only as long as is
  * absolutely necessary. Cooperation is required when deallocating a pointer
  * for which a hazard may exist to retire the pointer using the appropriate
  * API rather than directly deallocating it.
  *
  * This implementation assumes that a hazard pointer will not exist for very
  * long, and that a thread will only hold a very small number of hazard
  * pointers at any one time, generally no more than three or four; however,
  * the implementation does not impose any hard limits on how many hazard
  * pointers that a thread may hold at a time.
  *
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_HAZARD_POINTER_H__
#define __SKYTHON_CORE_SKY_HAZARD_POINTER_H__ 1


#include "sky_base.h"
#include "sky_malloc.h"


SKY_CDECLS_BEGIN


/** A hazard pointer. **/
typedef struct sky_hazard_pointer_s {
    void * volatile                     pointer;    /**< the pointer that the hazard pointer is protecting. **/
} sky_hazard_pointer_t;


/** Acquire a new hazard pointer.
  * A new hazard pointer is returned. As a convenience, if the optional address
  * argument is non-NULL, sky_hazard_pointer_update() is also called before the
  * new hazard pointer is returned.
  *
  * @param[in]  address     a memory location to be used to initialize the
  *                         hazard pointer's pointer member. May be NULL, in
  *                         which case the returned hazard pointer's pointer
  *                         member will also be NULL.
  * @return     a new hazard pointer.
  */
SKY_EXTERN sky_hazard_pointer_t *
sky_hazard_pointer_acquire(void * volatile *address);

/** Release a previously acquired hazard pointer.
  * The hazard pointer is released, allowing it to be reused. Once allocated,
  * hazard pointers are never deallocated, but instead reused as necessary.
  * When a hazard pointer is released, the pointer that it is protecting will
  * no longer be protected.
  *
  * @param[in] hazard       the hazard pointer to release.
  */
SKY_EXTERN void
sky_hazard_pointer_release(sky_hazard_pointer_t *hazard);

/** Retire a pointer to be cleaned up at a later time when no hazard pointers
  * are protecting it.
  * Before a pointer is retired, it must be made unreachable. A scan may or
  * may not be immediately triggered by the retirement.
  *
  * @param[in]  pointer     the pointer to retire.
  * @param[in]  free        the function to call to clean up the pointer when
  *                         it is safe to do so.
  */
SKY_EXTERN void
sky_hazard_pointer_retire(void *pointer, sky_free_t free);

/** Scan all pointers retired by the calling thread to clean up those for which
  * there exist no hazard pointers protecting them.
  * Scans are normally triggered periodically when a pointer is retired, but it
  * may sometimes be desirable to manually force a scan. When an automatic
  * scan is triggered, this function is called.
  */
SKY_EXTERN void
sky_hazard_pointer_scan(void);

/** Update a hazard pointer's pointer with the pointer stored at a specific
  * memory location.
  * Any pointer that the specified hazard is currently protecting will no longer
  * be protected, and the hazard will be updated to protect the pointer stored
  * at the specified memory location instead.
  *
  * @param[in]  hazard      the hazard pointer to update.
  * @param[in]  address     the memory location from which the new pointer to
  *                         protect will be obtained.
  */
SKY_EXTERN void
sky_hazard_pointer_update(sky_hazard_pointer_t *hazard, void * volatile *address);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_HAZARD_POINTER_H__ */

/** @} **/
/** @} **/
