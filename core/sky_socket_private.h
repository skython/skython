#ifndef __SKYTHON_SKY_SOCKET_PRIVATE_H__
#define __SKYTHON_SKY_SOCKET_PRIVATE_H__ 1


#include "sky_base.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>


SKY_CDECLS_BEGIN


typedef union sky_socket_address_u {
    struct sockaddr                     sockaddr;
    struct sockaddr_in                  sockaddr_in;
    struct sockaddr_in6                 sockaddr_in6;
    struct sockaddr_un                  sockaddr_un;
} sky_socket_address_t;


/* Convert a struct sockaddr into an address object. */
SKY_EXTERN sky_object_t
sky_socket_address_object(const sky_socket_address_t *addr);

/* Parse an IP address for an address family into a sockaddr struct. The empty
 * string and "<broadcast>" are special.
 */
SKY_EXTERN void
sky_socket_address_setipaddr(int                    family,
                             sky_object_t           address,
                             sky_socket_address_t * addr);

/* Convert an address object into a struct sockaddr. */
SKY_EXTERN void
sky_socket_address_sockaddr(int                     family,
                            sky_object_t            address,
                            sky_socket_address_t *  addr,
                            socklen_t *             addrlen);


SKY_CDECLS_END


#endif  /* __SKYTHON_SKY_SOCKET_PRIVATE_H__ */
