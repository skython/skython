/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_namespace Namespace Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_NAMESPACE_H__
#define __SKYTHON_CORE_SKY_NAMESPACE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new namespace object instance from an array of objects.
  * An even number of objects must be specified and ordered by key/value pair.
  *
  * @param[in]  count       the number of objects in @a objects. Must be even.
  * @param[in]  objects     the objects to use to initialize the namespace.
  * @return     a new namespace object instance initialized from @a objects.
  */
SKY_EXTERN sky_namespace_t
sky_namespace_createfromarray(ssize_t count, sky_object_t *objects);


/** Create a new namespace object instance from a dictionary.
  *
  * @param[in]  dict        the dict to use to initialize the namespace.
  * @return     a new namespace object instance initialized from @a dict.
  */
SKY_EXTERN sky_namespace_t
sky_namespace_createfromdict(sky_dict_t dict);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_NAMESPACE_H__ */

/** @} **/
/** @} **/
