#include "sky_private.h"

#include <gmp.h>


static int sky_library_initialization_count = 0;


typedef struct sky_library_finalizer_s sky_library_finalizer_t;
struct sky_library_finalizer_s {
    sky_library_finalizer_t * volatile  next;
    sky_library_finalize_t              function;
};

static sky_atomic_lifo_t sky_library_finalizers =
        SKY_ATOMIC_LIFO_INITIALIZER(offsetof(sky_library_finalizer_t, next));


void
sky_library_atfinalize(sky_library_finalize_t function)
{
    sky_library_finalizer_t *finalizer;

    finalizer = sky_malloc(sizeof(sky_library_finalizer_t));
    finalizer->function = function;

    sky_atomic_lifo_push(&sky_library_finalizers, finalizer);
}


void
sky_library_finalize(void)
{
    sky_library_finalizer_t *finalizer;

    if (!--sky_library_initialization_count) {
        while ((finalizer = sky_atomic_lifo_pop(&sky_library_finalizers)) != NULL) {
            finalizer->function();
            sky_free(finalizer);
        }
    }
}


static void *
sky_gmp_alloc(size_t size)
{
    return sky_malloc(size);
}

static void
sky_gmp_free(void *ptr, SKY_UNUSED size_t size)
{
    sky_free(ptr);
}

static void *
sky_gmp_realloc(void *ptr, SKY_UNUSED size_t old_size, size_t new_size)
{
    return sky_realloc(ptr, new_size);
}

sky_bool_t
sky_library_initialize(const char * argv0,
                       int          argc,
                       char *       argv[],
                       unsigned int flags)
{
    if (++sky_library_initialization_count == 1) {
        mp_set_memory_functions(sky_gmp_alloc, sky_gmp_realloc, sky_gmp_free);
        sky_atfork_initialize();
        sky_object_initialize_library(flags);
        sky_signal_initialize_library(flags);
        sky_python_ast_initialize_library(flags);
        sky_python_symtable_initialize_library(flags);
        sky_codec_initialize_library(flags);
        sky_file_initialize_library(flags);

        sky_interpreter_initialize_library(argv0, argc, argv, flags);
    }
    return SKY_TRUE;
}


void
sky_library_construct(void)
{
    sky_core_malloc_initialize();
    sky_tlsdata_initialize();
}


void
sky_library_destruct(void)
{
    sky_tlsdata_finalize();
    sky_core_malloc_finalize();
}
