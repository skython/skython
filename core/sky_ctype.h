/** @file
  * @brief Locale unaware version of commonly used ctype functions.
  * @defgroup sky_base Base Functions, Macros, and Types
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_CTYPE_H__
#define __SKYTHON_CORE_SKY_CTYPE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** @cond **/
SKY_EXTERN int  sky_ctype_base64_table[256];
SKY_EXTERN int  sky_ctype_hexdigit_table[256];
SKY_EXTERN int  sky_ctype_isalnum_table[256];
SKY_EXTERN int  sky_ctype_isalpha_table[256];
SKY_EXTERN int  sky_ctype_isdigit_table[256];
SKY_EXTERN int  sky_ctype_islower_table[256];
SKY_EXTERN int  sky_ctype_isodigit_table[256];
SKY_EXTERN int  sky_ctype_isprint_table[256];
SKY_EXTERN int  sky_ctype_isspace_table[256];
SKY_EXTERN int  sky_ctype_isupper_table[256];
SKY_EXTERN int  sky_ctype_isxdigit_table[256];
SKY_EXTERN int  sky_ctype_swapcase_table[256];
SKY_EXTERN int  sky_ctype_tolower_table[256];
SKY_EXTERN int  sky_ctype_toupper_table[256];
/** @endcond **/


/** The set of base64 digit characters: "A-Za-z0-9+/". **/
SKY_EXTERN const char * const sky_ctype_base64_digits;

/** The set of ASCII digit characters: "0123456789". **/
SKY_EXTERN const char * const sky_ctype_digits;

/** The set of lowercase ASCII hexadecimal characters: "0123456789abcdef". **/
SKY_EXTERN const char * const sky_ctype_lower_hexdigits;

/** The set of uppercase ASCII hexadecimal characters: "0123456789ABCDEF". **/
SKY_EXTERN const char * const sky_ctype_upper_hexdigits;

/** The set of ASCII octal characters: "01234567". **/
SKY_EXTERN const char * const sky_ctype_octdigits;


/** Return the numberical value of a base64 character. **/
SKY_EXTERN_INLINE int
sky_ctype_base64digit(int c)
{
    return sky_ctype_base64_table[(unsigned char)(c)];
}


/** Return the numerical value of a hexadecimal character. **/
SKY_EXTERN_INLINE int
sky_ctype_hexdigit(int c)
{
    return sky_ctype_hexdigit_table[(unsigned char)(c)];
}


/** Test an ASCII character to determine whether or not it's alphanumeric. **/
SKY_EXTERN_INLINE int
sky_ctype_isalnum(int c)
{
    return sky_ctype_isalnum_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's alphabetic. **/
SKY_EXTERN_INLINE int
sky_ctype_isalpha(int c)
{
    return sky_ctype_isalpha_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's base64. **/
SKY_EXTERN_INLINE int
sky_ctype_isbase64(int c)
{
    return (sky_ctype_base64_table[(unsigned char)(c)] >= 0);
}

/** Test an ASCII character to determine whether or not it's numeric. **/
SKY_EXTERN_INLINE int
sky_ctype_isdigit(int c)
{
    return sky_ctype_isdigit_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's alphabetic and
  * lowercase.
  */
SKY_EXTERN_INLINE int
sky_ctype_islower(int c)
{
    return sky_ctype_islower_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's a valid octal
  * digit.
  */
SKY_EXTERN_INLINE int
sky_ctype_isodigit(int c)
{
    return sky_ctype_isodigit_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's printable. **/
SKY_EXTERN_INLINE int
sky_ctype_isprint(int c)
{
    return sky_ctype_isprint_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's whitespace. **/
SKY_EXTERN_INLINE int
sky_ctype_isspace(int c)
{
    return sky_ctype_isspace_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's alphabetic and
  * uppercase.
  */
SKY_EXTERN_INLINE int
sky_ctype_isupper(int c)
{
    return sky_ctype_isupper_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's a valid
  * hexadecimal digit.
  */
SKY_EXTERN_INLINE int
sky_ctype_isxdigit(int c)
{
    return sky_ctype_isxdigit_table[(unsigned char)(c)];
}

/** Swap the case of an ASCII character. **/
SKY_EXTERN_INLINE int
sky_ctype_swapcase(int c)
{
    return sky_ctype_swapcase_table[(unsigned char)(c)];
}

/** Return the lowercase equivalent of an ASCII character. **/
SKY_EXTERN_INLINE int
sky_ctype_tolower(int c)
{
    return sky_ctype_tolower_table[(unsigned char)(c)];
}

/** Return the uppercase equivalent of an ASCII character. **/
SKY_EXTERN_INLINE int
sky_ctype_toupper(int c)
{
    return sky_ctype_toupper_table[(unsigned char)(c)];
}

/** Test an ASCII character to determine whether or not it's a filesystem path
  * separator.
  */
SKY_EXTERN_INLINE int
sky_ctype_ispathsep(int c)
{
#if defined(_WIN32)
    return (c == '/' || c == '\\');
#else
    return (c == '/');
#endif
}


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CTYPE_H__ */

/** @} **/
