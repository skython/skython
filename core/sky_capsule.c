#include "sky_private.h"


typedef struct sky_capsule_data_s {
    void *                              pointer;
    const char *                        name;
    void *                              context;
    sky_capsule_visit_t                 visit;
    sky_free_t                          destructor;
} sky_capsule_data_t;

SKY_EXTERN_INLINE sky_capsule_data_t *
sky_capsule_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_capsule_type) {
        return (sky_capsule_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_capsule_type);
}


static inline sky_bool_t
sky_capsule_names_match(const char *name_1, const char *name_2)
{
    if (!name_1 || !name_2) {
        return (name_1 == name_2 ? SKY_TRUE : SKY_FALSE);
    }
    return (strcmp(name_1, name_2) == 0 ? SKY_TRUE : SKY_FALSE);
}


static void
sky_capsule_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_capsule_data_t  *capsule_data = data;

    if (capsule_data->destructor) {
        capsule_data->destructor(capsule_data->pointer);
    }
}


static void
sky_capsule_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_capsule_data_t  *capsule_data = data;

    if (capsule_data->visit) {
        capsule_data->visit(object, visit_data);
    }
}


sky_bool_t
sky_capsule_bool(sky_capsule_t capsule)
{
    return (sky_capsule_data(capsule)->pointer != NULL ? SKY_TRUE : SKY_FALSE);
}


sky_object_t
sky_capsule_eq(sky_capsule_t capsule, sky_object_t other)
{
    sky_capsule_data_t  *capsule_data, *other_data;

    if (capsule == other) {
        return sky_True;
    }
    if (!sky_object_isa(other, sky_capsule_type)) {
        return sky_NotImplemented;
    }
    capsule_data = sky_capsule_data(capsule);
    other_data = sky_capsule_data(other);
    if (capsule_data->pointer != other_data->pointer) {
        return sky_False;
    }
    if (!sky_capsule_names_match(capsule_data->name, other_data->name)) {
        return sky_False;
    }
    return sky_True;
}


uintptr_t
sky_capsule_hash(sky_capsule_t capsule)
{
    return (uintptr_t)(sky_capsule_data(capsule)->pointer) >> 3;
}


sky_bool_t
sky_capsule_isvalid(sky_object_t object, const char *name)
{
    sky_capsule_data_t  *data;

    if (!sky_object_isa(object, sky_capsule_type)) {
        return SKY_FALSE;
    }
    data = sky_capsule_data(object);
    return (data->pointer &&
            sky_capsule_names_match(data->name, name) ? SKY_TRUE : SKY_FALSE);
}


void *
sky_capsule_pointer(sky_capsule_t self, const char *name)
{
    sky_capsule_data_t  *self_data = sky_capsule_data(self);

    if (!sky_capsule_names_match(name, self_data->name)) {
        sky_error_raise_string(
                sky_ValueError,
                "sky_capsule_pointer() called with incorrect name");
    }

    return self_data->pointer;
}


sky_string_t
sky_capsule_repr(sky_capsule_t self)
{
    sky_capsule_data_t  *capsule_data = sky_capsule_data(self);

    if (!capsule_data->name) {
        return sky_string_createfromformat("<capsule object NULL at %p>", self);
    }
    return sky_string_createfromformat("<capsule object \"%s\" at %p>",
                                       capsule_data->name,
                                       self);
}


sky_capsule_t
sky_capsule_create(void *               pointer,
                   const char *         name,
                   sky_free_t           destructor,
                   sky_capsule_visit_t  visit)
{
    sky_capsule_t       capsule;
    sky_capsule_data_t  *capsule_data;

    capsule = sky_object_allocate(sky_capsule_type);
    capsule_data = sky_capsule_data(capsule);
    capsule_data->pointer = pointer;
    capsule_data->name = name;
    capsule_data->visit = visit;
    capsule_data->destructor = destructor;

    return capsule;
}


void *
sky_capsule_value(sky_capsule_t capsule, const char *name)
{
    sky_capsule_data_t  *capsule_data = sky_capsule_data(capsule);

    if (!sky_capsule_names_match(capsule_data->name, name)) {
        sky_error_raise_string(sky_ValueError, "names do not match");
    }

    return capsule_data->pointer;
}


const char *
sky_capsule_name(sky_capsule_t capsule)
{
    return sky_capsule_data(capsule)->name;
}


static const char sky_capsule_type_doc[] =
"Capsule objects let you wrap a C \"void *\" pointer in a Python\n"
"object. They're a way of passing data through the Python interpreter\n"
"without creating your own custom type.\n"
"\n"
"Capsules are used for communication between extension modules.\n"
"They provide a way for an extension module to export a C interface\n"
"to other extension modules, so that extension modules can use the\n"
"Python import mechanism to link to one another.\n";


SKY_TYPE_DEFINE_SIMPLE(capsule,
                       "capsule",
                       sizeof(sky_capsule_data_t),
                       NULL,
                       sky_capsule_instance_finalize,
                       sky_capsule_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       sky_capsule_type_doc);


void
sky_capsule_initialize_library(void)
{
    sky_type_setattr_builtin(sky_capsule_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_capsule_type,
            "__bool__", sky_capsule_bool,
            "__eq__", sky_capsule_eq,
            "__hash__", sky_capsule_hash,
            "__repr__", sky_capsule_repr,
            NULL);
}
