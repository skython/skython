#include "sky_private.h"
#include "sky_string_private.h"


static uint32_t
sky_string_format_align_flag(sky_unicode_char_t cp)
{
    switch (cp) {
        case '<':
            return SKY_FORMAT_FLAG_ALIGN_LEFT;
        case '>':
            return SKY_FORMAT_FLAG_ALIGN_RIGHT;
        case '=':
            return SKY_FORMAT_FLAG_ALIGN_PADDED;
        case '^':
            return SKY_FORMAT_FLAG_ALIGN_CENTER;
    }

    return 0;
}


static uint32_t
sky_string_format_sign_flag(sky_unicode_char_t cp)
{
    switch (cp) {
        case '-':
            return SKY_FORMAT_FLAG_SIGN_MINUS;
        case '+':
            return SKY_FORMAT_FLAG_SIGN_PLUS;
        case ' ':
            return SKY_FORMAT_FLAG_SIGN_SPACE;
    }

    return 0;
}


#define SKY_STRING_WIDTH 1
#include "templates/sky_string_format_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 2
#include "templates/sky_string_format_t.c"
#undef SKY_STRING_WIDTH

#define SKY_STRING_WIDTH 4
#include "templates/sky_string_format_t.c"
#undef SKY_STRING_WIDTH


sky_string_t
sky_string___format__(sky_string_t self, sky_string_t format_spec)
{
    size_t                      width;
    ssize_t                     left, len, right;
    sky_string_data_t           *self_data, self_tagged_data;
    sky_string_builder_t        builder;
    sky_format_specification_t  spec;

    if (!sky_string_parseformatspecification(format_spec, &spec)) {
        return sky_object_str(self);
    }

    /* Validate the format specifier. */
    if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED) {
        sky_error_raise_string(
                sky_ValueError,
                "'=' alignment not allowed in string format specifier");
    }
    if (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_SIGN) {
        sky_error_raise_string(
                sky_ValueError,
                "Sign not allowed in string format specifier");
    }
    if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
        sky_error_raise_string(
                sky_ValueError,
                "Alternate form (#) not allowed in string format specifier");
    }
    if (spec.type && spec.type != 's') {
        if (spec.type >= 0x20 && spec.type <= 0x7E) {
            sky_error_raise_format(
                    sky_ValueError,
                    "Unknown format code '%c' for object of type %#@",
                    (char)spec.type,
                    sky_type_name(sky_object_type(self)));
        }
        else {
            sky_error_raise_format(
                    sky_ValueError,
                    "Unknown format code '\\x%x' for object of type %#@",
                    (unsigned int)spec.type,
                    sky_type_name(sky_object_type(self)));
        }
    }
    if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
        sky_error_raise_string(
                sky_ValueError,
                "Cannot specify ',' with 's'");
    }

    /* If neither width nor precision were specified, alignment has no effect,
     * so just return sky_object_str(self).
     */
    if (!(spec.flags & (SKY_FORMAT_FLAG_SPECIFIED_WIDTH |
                        SKY_FORMAT_FLAG_SPECIFIED_PRECISION)))
    {
        return sky_object_str(self);
    }

    /* If precision was specified, limit the number of characters from the
     * string to the specified precision. If width was not specified and the
     * precision is greater than the length of the string, just return
     * sky_object_str(self).
     */
    self_data = sky_string_data(self, &self_tagged_data);
    len = self_data->len;
    if (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION) {
        if (len > spec.precision) {
            len = spec.precision;
        }
        else if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH)) {
            return sky_object_str(self);
        }
    }

    left = right = 0;
    if ((spec.flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH) && spec.width > len) {
        switch (sky_format_alignment(&spec)) {
            case SKY_FORMAT_FLAG_ALIGN_LEFT:
                right = spec.width - len;
                break;
            case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                left = spec.width - len;
                break;
            case SKY_FORMAT_FLAG_ALIGN_CENTER:
                left = (spec.width - len) / 2;
                right = spec.width - left - len;
                break;
            case SKY_FORMAT_FLAG_ALIGN_PADDED:
                sky_error_fatal("unreachable");
        }
    }
    if (!left && !right && len >= self_data->len) {
        return sky_object_str(self);
    }

    builder = sky_string_builder_createwithcapacity(left + len + right);
    if (left > 0) {
        sky_string_builder_appendcodepoint(builder, spec.fill, left);
    }
    if (len >= self_data->len) {
        sky_string_builder_append(builder, self);
    }
    else {
        width = sky_string_data_width(self_data);
        sky_string_builder_appendcodepoints(builder,
                                            self_data->data.ascii,
                                            len,
                                            width);
    }
    if (right > 0) {
        sky_string_builder_appendcodepoint(builder, spec.fill, right);
    }

    return sky_string_builder_finalize(builder);
}


sky_bool_t
sky_string_parseformatspecification(sky_string_t                self,
                                    sky_format_specification_t *spec)
{
    sky_string_data_t   *self_data, self_tagged_data;

    spec->fill = ' ';
    spec->type = 0;
    spec->width = 0;
    spec->precision = 0;
    spec->flags = SKY_FORMAT_FLAG_ALIGN_LEFT | SKY_FORMAT_FLAG_SIGN_MINUS;

    self_data = sky_string_data(self, &self_tagged_data);
    if (self_data->len > 0) {
        if (self_data->highest_codepoint < 0x100) {
            sky_string_parseformatspecification_latin1(self_data, spec);
        }
        else if (self_data->highest_codepoint < 0x10000) {
            sky_string_parseformatspecification_ucs2(self_data, spec);
        }
        else {
            sky_string_parseformatspecification_ucs4(self_data, spec);
        }
    }

    if (spec->width > SSIZE_MAX / 10 || spec->precision > SSIZE_MAX / 10) {
        sky_error_raise_string(sky_ValueError,
                               "Too many decimal digits in format string");
    }

    if (' ' == spec->fill && !spec->type &&
        !(spec->flags & (SKY_FORMAT_FLAG_ALTERNATE_FORM |
                         SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS |
                         SKY_FORMAT_FLAG_SPECIFIED_WIDTH |
                         SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) &&
        ((spec->flags & SKY_FORMAT_FLAG_ALIGN_MASK) == SKY_FORMAT_FLAG_ALIGN_LEFT) &&
        ((spec->flags & SKY_FORMAT_FLAG_SIGN_MASK) == SKY_FORMAT_FLAG_SIGN_MINUS))
    {
        return SKY_FALSE;
    }
    return SKY_TRUE;
}


sky_string_t
sky_string_format(sky_string_t  self,
                  sky_tuple_t   args,
                  sky_dict_t    kws)
{
    sky_string_data_t   *self_data, tagged_data;
    
    self_data = sky_string_data(self, &tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_format_latin1(self_data, args, kws);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_format_ucs2(self_data, args, kws);
    }
    return sky_string_format_ucs4(self_data, args, kws);
}


sky_string_t
sky_string_format_map(sky_string_t  self,
                      sky_object_t  map)
{
    sky_string_data_t   *self_data, tagged_data;
    
    self_data = sky_string_data(self, &tagged_data);
    if (self_data->highest_codepoint < 0x100) {
        return sky_string_format_latin1(self_data, NULL, map);
    }
    if (self_data->highest_codepoint < 0x10000) {
        return sky_string_format_ucs2(self_data, NULL, map);
    }
    return sky_string_format_ucs4(self_data, NULL, map);
}
