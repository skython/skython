#include "sky_private.h"
#include "sky_string_private.h"
#include "sky_codec_private.h"


SKY_EXTERN_INLINE sky_string_data_t *
sky_string_builder_data(sky_object_t object)
{
    if (!sky_object_isa(object, sky_string_builder_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected %#@; got %#@",
                               sky_type_name(sky_string_builder_type),
                               sky_type_name(sky_object_type(object)));
    }
    return sky_object_data(object, sky_string_type);
}


void
sky_string_builder_append(sky_string_builder_t builder, sky_string_t string)
{
    sky_string_data_t   tagged_data;

    sky_string_data_combine(sky_string_builder_data(builder),
                            sky_string_data(string, &tagged_data));
}


void
sky_string_builder_appendbytes(sky_string_builder_t builder,
                               const void *         bytes,
                               size_t               nbytes,
                               sky_string_t         encoding,
                               sky_string_t         errors)
{
    sky_string_data_t   *string_data = sky_string_builder_data(builder);

    sky_string_data_decode(string_data, encoding, errors, bytes, nbytes);
}


void
sky_string_builder_appendcodepoint(sky_string_builder_t builder,
                                   sky_unicode_char_t   codepoint,
                                   ssize_t              count)
{
    sky_string_data_t   *string_data = sky_string_builder_data(builder);

    sky_string_data_append_codepoint(string_data, codepoint, count);
}


void
sky_string_builder_appendcodepoints(sky_string_builder_t    builder,
                                    const void *            codepoints,
                                    ssize_t                 ncodepoints,
                                    size_t                  width)
{
    sky_string_data_t   *string_data = sky_string_builder_data(builder);

    sky_string_data_append(string_data, codepoints, ncodepoints * width, width);
}


void
sky_string_builder_appendfilename(sky_string_builder_t  builder,
                                  const void *          bytes,
                                  size_t                nbytes)
{
    sky_string_data_t   *string_data = sky_string_builder_data(builder);

    sky_string_data_decode(string_data,
                           sky_codec_filesystemencoding_string,
                           sky_codec_filesystemerrors_string,
                           bytes,
                           nbytes);
}


static void
sky_string_builder_appendformat_output(const void * bytes,
                                       ssize_t      nbytes,
                            SKY_UNUSED size_t       width,
                                       void *       arg)
{
    sky_string_data_decode(arg, NULL, NULL, bytes, nbytes);
}


void
sky_string_builder_appendformat(sky_string_builder_t    builder,
                                const char *            format,
                                ...)
{
    va_list ap;

    va_start(ap, format);
    sky_format_voutput(sky_string_builder_appendformat_output,
                       sky_string_builder_data(builder),
                       format,
                       ap);
    va_end(ap);
}


void
sky_string_builder_appendformatv(sky_string_builder_t   builder,
                                 const char *           format,
                                 va_list                ap)
{
    sky_format_voutput(sky_string_builder_appendformat_output,
                       sky_string_builder_data(builder),
                       format,
                       ap);
}


void
sky_string_builder_appendjoined(sky_string_builder_t    builder,
                                sky_object_t            iterable,
                                sky_string_t            separator)
{
    sky_string_data_t   *builder_data = sky_string_builder_data(builder);

    ssize_t             i, navail, nstrings;
    sky_bool_t          need_separator;
    sky_object_t        iterator, object;
    sky_string_data_t   *separator_data, separator_tagged_data,
                        **strings, *tagged_data;

    if (!sky_object_isiterable(iterable)) {
        sky_error_raise_format(sky_TypeError,
                               "expected iterable; got %#@",
                               sky_type_name(sky_object_type(iterable)));
    }
    if (!(nstrings = sky_object_length_hint(iterable, 8))) {
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        strings = sky_asset_malloc(nstrings * (sizeof(sky_string_data_t *) +
                                               sizeof(sky_string_data_t)),
                                   SKY_ASSET_CLEANUP_ALWAYS);
        navail = sky_memsize(strings) / (sizeof(sky_string_data_t *) +
                                         sizeof(sky_string_data_t));
        tagged_data = (sky_string_data_t *)(strings + navail);

        if (!separator) {
            separator = sky_string_empty;
        }
        separator_data = sky_string_data(separator, &separator_tagged_data);

        i = 0;
        if (sky_sequence_isiterable(iterable)) {
            SKY_SEQUENCE_FOREACH(iterable, object) {
                if (!sky_object_isa(object, sky_string_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "sequence item %zd: expected %#@; got %#@",
                            i,
                            sky_type_name(sky_string_type),
                            sky_type_name(sky_object_type(object)));
                }
                strings[i] = sky_string_data(object, &(tagged_data[i]));
                ++i;
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            need_separator = SKY_FALSE;
            iterator = sky_object_iter(iterable);
            while ((object = sky_object_next(iterator, NULL)) != NULL) {
                if (!sky_object_isa(object, sky_string_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "sequence item %zd: expected %#@; got %#@",
                            i,
                            sky_type_name(sky_string_type),
                            sky_type_name(sky_object_type(object)));
                }
                if (i >= navail) {
                    if (need_separator) {
                        sky_string_data_combine(builder_data, separator_data);
                    }
                    sky_string_data_join(builder_data,
                                         separator_data,
                                         strings,
                                         navail);
                    need_separator = SKY_TRUE;
                    i = 0;
                }

                strings[i] = sky_string_data(object, &(tagged_data[i]));
                ++i;
            }
            nstrings = i;
            if (need_separator && nstrings > 0) {
                sky_string_data_combine(builder_data, separator_data);
            }
        }

        sky_string_data_join(builder_data, separator_data, strings, nstrings);
    } SKY_ASSET_BLOCK_END;
}


void
sky_string_builder_appendslice(sky_string_builder_t builder,
                               sky_string_t         string,
                               ssize_t              length,
                               ssize_t              start,
                               ssize_t              step)
{
    sky_string_data_t   *builder_data = sky_string_builder_data(builder);

    sky_string_data_t   *string_data, tagged_data;

    string_data = sky_string_data(string, &tagged_data);

    sky_string_data_append_slice(builder_data,
                                 string_data,
                                 length,
                                 start,
                                 step);
}


sky_string_builder_t
sky_string_builder_copy(sky_string_builder_t builder)
{
    sky_string_builder_t    new_builder;

    new_builder = sky_object_allocate(sky_string_builder_type);
    sky_string_data_copy(sky_string_builder_data(new_builder),
                         sky_string_builder_data(builder));

    return new_builder;
}


sky_string_builder_t
sky_string_builder_create(void)
{
   return sky_object_allocate(sky_string_builder_type);
}


sky_string_builder_t
sky_string_builder_createwithcapacity(ssize_t capacity)
{
    sky_string_data_t       *builder_data;
    sky_string_builder_t    builder;

    builder = sky_object_allocate(sky_string_builder_type);
    if (capacity > 0) {
        builder_data = sky_string_builder_data(builder);
        sky_string_data_resize(builder_data, capacity, 1);
    }

    return builder;
}


sky_string_t
sky_string_builder_finalize(sky_string_builder_t builder)
{
    sky_object_data_t   *object_data;
    
    if (!sky_object_isa(builder, sky_string_builder_type)) {
        sky_error_raise_format(sky_TypeError,
                               "expected %#@; got %#@",
                               sky_type_name(sky_string_builder_type),
                               sky_type_name(sky_object_type(builder)));
    }

    object_data = SKY_OBJECT_DATA(builder);
    sky_object_gc_set(SKY_AS_OBJECTP(&(object_data->u.type)),
                      sky_string_type,
                      builder);

    return sky_string_cache_string(sky_string_cache_instance,
                                   SKY_AS_TYPE(builder, sky_string_t),
                                   SKY_FALSE);
}


SKY_TYPE_DEFINE_SIMPLE(string_builder,
                       "string_builder",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_string_builder_initialize_library(void)
{
    /* string_builder objects are mutable; therefore, they are not hashable */
    sky_type_setattr_builtin(sky_string_builder_type, "__hash__", sky_None);
}
