#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_integer_private.h"

#include "sky_marshal.h"


#define SKY_MARSHAL_MAX_STACK_DEPTH     2000


typedef enum sky_marshal_tag_e {
    SKY_MARSHAL_TAG_NULL            =   '0',
    SKY_MARSHAL_TAG_NONE            =   'N',
    SKY_MARSHAL_TAG_FALSE           =   'F',
    SKY_MARSHAL_TAG_TRUE            =   'T',
    SKY_MARSHAL_TAG_STOPITER        =   'S',
    SKY_MARSHAL_TAG_ELLIPSIS        =   '.',
    SKY_MARSHAL_TAG_INT             =   'i',
    SKY_MARSHAL_TAG_INT64           =   'I', /* deprecated - removed in 3.4 */
    SKY_MARSHAL_TAG_FLOAT           =   'f',
    SKY_MARSHAL_TAG_BINARY_FLOAT    =   'g',
    SKY_MARSHAL_TAG_COMPLEX         =   'x',
    SKY_MARSHAL_TAG_BINARY_COMPLEX  =   'y',
    SKY_MARSHAL_TAG_LONG            =   'l',
    SKY_MARSHAL_TAG_STRING          =   's',
    SKY_MARSHAL_TAG_TUPLE           =   '(',
    SKY_MARSHAL_TAG_LIST            =   '[',
    SKY_MARSHAL_TAG_DICT            =   '{',
    SKY_MARSHAL_TAG_CODE            =   'c',
    SKY_MARSHAL_TAG_UNICODE         =   'u',
    SKY_MARSHAL_TAG_UNKNOWN         =   '?',
    SKY_MARSHAL_TAG_SET             =   '<',
    SKY_MARSHAL_TAG_FROZENSET       =   '>',
} sky_marshal_tag_t;


typedef struct sky_marshal_dump_context_s {
    sky_marshal_dump_t                  callback;
    void *                              arg;

    uint8_t *                           buffer;
    ssize_t                             buffer_size;
    ssize_t                             buffer_used;

    int                                 depth;
    int                                 version;
} sky_marshal_dump_context_t;


static void
sky_marshal_dump_object(sky_marshal_dump_context_t *context,
                        sky_object_t                object);


static inline void
sky_marshal_flush(sky_marshal_dump_context_t *context)
{
    if (context->buffer_used > 0) {
        context->callback(context->arg, context->buffer, context->buffer_used);
        context->buffer_used = 0;
    }
}


static inline void
sky_marshal_write(sky_marshal_dump_context_t *  context,
                  const void *                  void_bytes,
                  ssize_t                       nbytes)
{
    const char *bytes = void_bytes;

    if (unlikely(!nbytes)) {
        return;
    }
    for (;;) {
        ssize_t n = context->buffer_size - context->buffer_used;

        n = SKY_MIN(n, nbytes);
        memcpy(context->buffer + context->buffer_used, bytes, n);
        context->buffer_used += n;
        if (context->buffer_used == context->buffer_size) {
            sky_marshal_flush(context);
        }
        if (nbytes == n) {
            break;
        }
        nbytes -= n;
        bytes += n;
    }
}


static inline void
sky_marshal_write_tag(sky_marshal_dump_context_t *  context,
                      sky_marshal_tag_t             tag)
{
    uint8_t byte = tag;

    sky_marshal_write(context, &byte, sizeof(byte));
}


static inline void
sky_marshal_write_int32(sky_marshal_dump_context_t *context,
                        int32_t                     value)
{
    value = sky_endian_htol32(value);
    sky_marshal_write(context, &value, sizeof(value));
}


static inline void
sky_marshal_write_size(sky_marshal_dump_context_t * context,
                       ssize_t                      value)
{
    if (value < INT32_MIN || value > INT32_MAX) {
        sky_error_raise_string(sky_ValueError, "unmarshallable object");
    }
    sky_marshal_write_int32(context, value);
}


static void
sky_marshal_write_buffer(sky_marshal_dump_context_t *   context,
                         sky_object_t                   object)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_marshal_write_size(context, buffer.len);
        sky_marshal_write(context, buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_marshal_write_code(sky_marshal_dump_context_t * context,
                       sky_object_t                 object)
{
    sky_code_data_t *code_data = sky_code_data(object);

    sky_marshal_write_tag(context, SKY_MARSHAL_TAG_CODE);
    sky_marshal_write_size(context, code_data->co_argcount);
    sky_marshal_write_size(context, code_data->co_kwonlyargcount);
    sky_marshal_write_size(context, code_data->co_nlocals);
    sky_marshal_write_size(context, code_data->co_stacksize);
    sky_marshal_write_int32(context, code_data->co_flags);
    sky_marshal_dump_object(context, code_data->co_code);
    sky_marshal_dump_object(context, code_data->co_consts);
    sky_marshal_dump_object(context, code_data->co_names);
    sky_marshal_dump_object(context, code_data->co_varnames);
    sky_marshal_dump_object(context, code_data->co_freevars);
    sky_marshal_dump_object(context, code_data->co_cellvars);
    sky_marshal_dump_object(context, code_data->co_filename);
    sky_marshal_dump_object(context, code_data->co_name);
    sky_marshal_write_int32(context, code_data->co_firstlineno);
    sky_marshal_dump_object(context, code_data->co_lnotab);
}


static void
sky_marshal_write_complex(sky_marshal_dump_context_t *  context,
                          sky_complex_t                 value)
{
    if (context->version > 1) {
        uint8_t buffer[8];

        if (!sky_util_double_encode(sky_complex_real(value),
                                    buffer,
                                    sizeof(buffer),
                                    SKY_ENDIAN_LITTLE))
        {
            sky_error_raise_string(sky_ValueError, "unmarshallable object");
        }
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_BINARY_COMPLEX);
        sky_marshal_write(context, buffer, sizeof(buffer));

        if (!sky_util_double_encode(sky_complex_imag(value),
                                    buffer,
                                    sizeof(buffer),
                                    SKY_ENDIAN_LITTLE))
        {
            sky_error_raise_string(sky_ValueError, "unmarshallable object");
        }
        sky_marshal_write(context, buffer, sizeof(buffer));
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            char    *buffer;
            uint8_t len;

            buffer = sky_util_dtoa(sky_complex_real(value), 'g', 17, 0);
            sky_asset_save(buffer, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            len = strlen(buffer);
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_COMPLEX);
            sky_marshal_write(context, &len, sizeof(len));
            sky_marshal_write(context, buffer, len);

            buffer = sky_util_dtoa(sky_complex_imag(value), 'g', 17, 0);
            sky_asset_save(buffer, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            len = strlen(buffer);
            sky_marshal_write(context, &len, sizeof(len));
            sky_marshal_write(context, buffer, len);
        } SKY_ASSET_BLOCK_END;
    }
}


static void
sky_marshal_write_dict(sky_marshal_dump_context_t * context,
                       sky_dict_t                   dict)
{
    sky_object_t    key, value;

    if (sky_object_stack_push(dict)) {
        sky_error_raise_string(sky_ValueError,
                               "object too deeply nested to marshal");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(dict,
                       sky_object_stack_pop,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_DICT);
        SKY_SEQUENCE_FOREACH(dict, key) {
            value = sky_dict_getitem(dict, key);
            sky_marshal_dump_object(context, key);
            sky_marshal_dump_object(context, value);
        } SKY_SEQUENCE_FOREACH_END;
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_NULL);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_marshal_write_double(sky_marshal_dump_context_t *   context,
                         double                         value)
{
    if (context->version > 1) {
        uint8_t buffer[8];

        if (!sky_util_double_encode(value,
                                    buffer,
                                    sizeof(buffer),
                                    SKY_ENDIAN_LITTLE))
        {
            sky_error_raise_string(sky_ValueError, "unmarshallable object");
        }
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_BINARY_FLOAT);
        sky_marshal_write(context, buffer, sizeof(buffer));
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            char    *buffer;
            uint8_t len;

            buffer = sky_util_dtoa(value, 'g', 17, 0);
            sky_asset_save(buffer, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            len = strlen(buffer);
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_FLOAT);
            sky_marshal_write(context, &len, sizeof(len));
            sky_marshal_write(context, buffer, len);
        } SKY_ASSET_BLOCK_END;
    }
}


static void
sky_marshal_write_integer(sky_marshal_dump_context_t *  context,
                          sky_integer_t                 integer)
{
    /* If the integer fits in a signed 32-bit space, write it out as INT. */
    SKY_ERROR_TRY {
        int32_t value;

        value = sky_integer_value(integer, INT32_MIN, INT32_MAX, NULL);
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_INT);
        sky_marshal_write_int32(context, value);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        void                *bytes;
        size_t              nwords, written;
        mpz_ptr             mpz;
        ssize_t             nbytes;
        sky_integer_data_t  *integer_data, tagged_data;

        integer_data = sky_integer_data(integer, &tagged_data);
        mpz = integer_data->mpz;
        nwords = (mpz_sizeinbase(mpz, 2) + 14) / 15;

        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_LONG);
        sky_marshal_write_size(context, (mpz_sgn(mpz) < 0 ? -nwords : nwords));

        if (nwords) {
            nbytes = nwords * 2;
            if (nbytes <= context->buffer_size) {
                if (nbytes > (context->buffer_size - context->buffer_used)) {
                    sky_marshal_flush(context);
                }
                mpz_export(context->buffer + context->buffer_used,
                           &written,
                           -1,
                           2,
                           -1,
                           1,
                           mpz);
                sky_error_validate_debug(written == nwords);
                context->buffer_used += (written * 2);
            }
            else {
                SKY_ASSET_BLOCK_BEGIN {
                    bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
                    mpz_export(bytes, &written, -1, 2, -1, 1, mpz);
                    sky_error_validate_debug(written == nwords);
                    sky_marshal_write(context, bytes, written * 2);
                } SKY_ASSET_BLOCK_END;
            }
        }
    } SKY_ERROR_TRY_END;
}


static void
sky_marshal_write_sequence(sky_marshal_dump_context_t * context,
                           sky_object_t                 sequence)
{
    sky_object_t    item;

    if (sky_object_stack_push(sequence)) {
        sky_error_raise_string(sky_ValueError,
                               "object too deeply nested to marshal");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(sequence,
                       sky_object_stack_pop,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_marshal_write_size(context, sky_object_len(sequence));
        SKY_SEQUENCE_FOREACH(sequence, item) {
            sky_marshal_dump_object(context, item);
        } SKY_SEQUENCE_FOREACH_END;
    } SKY_ASSET_BLOCK_END;
}


static void
sky_marshal_write_string(sky_marshal_dump_context_t *   context,
                         sky_string_t                   string)
{
    SKY_ASSET_BLOCK_BEGIN {
        uint8_t *bytes;
        ssize_t nbytes;

        bytes = sky_codec_encodetocbytes(sky_codec_utf8,
                                         string,
                                         NULL,
                                         SKY_STRING_LITERAL("surrogatepass"),
                                         0,
                                         &nbytes);
        sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_UNICODE);
        sky_marshal_write_size(context, nbytes);
        sky_marshal_write(context, bytes, nbytes);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_marshal_dump_object(sky_marshal_dump_context_t *context,
                        sky_object_t                object)
{
    sky_type_t  type;

    if (++context->depth > SKY_MARSHAL_MAX_STACK_DEPTH) {
        sky_error_raise_string(sky_ValueError,
                               "object too deeply nested to marshal");
    }

    if (!object) {
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_NULL);
        --context->depth;
        return;
    }

    /* Check for singletons */
    if (sky_object_istagged(object) &&
        ((uintptr_t)object & 0xF0FF) == 0x0007)
    {
        sky_error_validate_debug(!((uintptr_t)object & ~(uintptr_t)0x0FFF));
        switch (((uintptr_t)object >> 8) & 0x0F) {
            case 0x00:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_NONE);
                --context->depth;
                return;
            case 0x01:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_ELLIPSIS);
                --context->depth;
                return;
            case 0x04:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_FALSE);
                --context->depth;
                return;
            case 0x05:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_TRUE);
                --context->depth;
                return;
            case 0x06:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_STRING);
                sky_marshal_write_buffer(context, object);
                --context->depth;
                return;
            case 0x07:
                sky_marshal_write_string(context, object);
                --context->depth;
                return;
            case 0x08:
                sky_marshal_write_tag(context, SKY_MARSHAL_TAG_TUPLE);
                sky_marshal_write_sequence(context, object);
                --context->depth;
                return;
        }
    }

    if (sky_StopIteration == object) {
        sky_marshal_write_tag(context, SKY_MARSHAL_TAG_STOPITER);
    }
    else {
        type = sky_object_type(object);
        if (sky_code_type == type) {
            sky_marshal_write_code(context, object);
        }
        else if (sky_complex_type == type) {
            sky_marshal_write_complex(context, object);
        }
        else if (sky_dict_type == type) {
            sky_marshal_write_dict(context, object);
        }
        else if (sky_float_type == type) {
            sky_marshal_write_double(context, sky_float_value(object));
        }
        else if (sky_frozenset_type == type) {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_FROZENSET);
            sky_marshal_write_sequence(context, object);
        }
        else if (sky_integer_type == type) {
            sky_marshal_write_integer(context, object);
        }
        else if (sky_list_type == type) {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_LIST);
            sky_marshal_write_sequence(context, object);
        }
        else if (sky_set_type == type) {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_SET);
            sky_marshal_write_sequence(context, object);
        }
        else if (sky_string_type == type) {
            sky_marshal_write_string(context, object);
        }
        else if (sky_tuple_type == type) {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_TUPLE);
            sky_marshal_write_sequence(context, object);
        }
        else if (sky_buffer_check(object)) {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_STRING);
            sky_marshal_write_buffer(context, object);
        }
        else {
            sky_marshal_write_tag(context, SKY_MARSHAL_TAG_UNKNOWN);
            sky_error_raise_string(sky_ValueError, "unmarshallable object");
        }
    }

    --context->depth;
}


void
sky_marshal_dumpwithcallback(sky_object_t       object,
                             uint32_t           version,
                             sky_marshal_dump_t callback,
                             void *             arg)
{
    sky_marshal_dump_context_t  context;

    SKY_ASSET_BLOCK_BEGIN {
        context.callback = callback;
        context.arg = arg;
        context.depth = 0;
        context.version = version;
        context.buffer = sky_asset_malloc(sky_system_pagesize(),
                                          SKY_ASSET_CLEANUP_ALWAYS);
        context.buffer_size = sky_memsize(context.buffer);
        context.buffer_used = 0;

        sky_marshal_dump_object(&context, object);
        sky_marshal_flush(&context);
    } SKY_ASSET_BLOCK_END;
}


static void
sky_marshal_dumptofile(void *arg, const void *bytes, ssize_t nbytes)
{
    sky_buffer_t        *buffer;
    sky_memoryview_t    view;

    SKY_ASSET_BLOCK_BEGIN {
        SKY_ASSET_BLOCK_BEGIN {
            buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                      SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_initialize(buffer,
                                  (void *)bytes,
                                  nbytes,
                                  SKY_TRUE,
                                  SKY_BUFFER_FLAG_CONTIG_RO);
            view = sky_memoryview_createwithbuffer(buffer);
        } SKY_ASSET_BLOCK_END;

        sky_asset_save(view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_file_write(arg, view);
    } SKY_ASSET_BLOCK_END;
}

void
sky_marshal_dump(sky_object_t   object,
                 sky_object_t   file,
                 uint32_t       version)
{
    sky_marshal_dumpwithcallback(object,
                                 version,
                                 sky_marshal_dumptofile,
                                 file);
}


typedef struct sky_marshal_buffer_s {
    uint8_t *                           bytes;
    ssize_t                             used;
    ssize_t                             size;
} sky_marshal_buffer_t;

static void
sky_marshal_dumptobuffer(void *arg, const void *bytes, ssize_t nbytes)
{
    sky_marshal_buffer_t    *buffer = arg;

    if (buffer->used + nbytes + 1 > buffer->size) {
        buffer->bytes = sky_asset_realloc(buffer->bytes,
                                          buffer->size * 2,
                                          SKY_ASSET_CLEANUP_ON_ERROR,
                                          SKY_ASSET_UPDATE_FIRST_DEEP);
        buffer->size = sky_memsize(buffer->bytes);
    }
    memcpy(buffer->bytes + buffer->used, bytes, nbytes);
    buffer->used += nbytes;
    buffer->bytes[buffer->used] = 0;
}

sky_bytes_t
sky_marshal_dumps(sky_object_t  object,
                  uint32_t      version)
{
    sky_bytes_t             bytes;
    sky_marshal_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        buffer.bytes = sky_asset_malloc(sky_system_pagesize(),
                                        SKY_ASSET_CLEANUP_ON_ERROR);
        buffer.used = 0;
        buffer.size = sky_memsize(buffer.bytes);

        sky_marshal_dumpwithcallback(object,
                                     version,
                                     sky_marshal_dumptobuffer,
                                     &buffer);

        bytes = sky_bytes_createwithbytes(buffer.bytes, buffer.used, sky_free);
    } SKY_ASSET_BLOCK_END;

    return bytes;
}


typedef struct sky_marshal_load_context_s {
    sky_marshal_load_t                  callback;
    void *                              arg;

    int                                 depth;
} sky_marshal_load_context_t;


static sky_object_t
sky_marshal_load_object(sky_marshal_load_context_t *context);


static void
sky_marshal_read(sky_marshal_load_context_t *   context,
                 void *                         bytes,
                 ssize_t                        nbytes)
{
    ssize_t nbytes_read;

    nbytes_read = context->callback(context->arg, bytes, nbytes);
    sky_error_validate_debug(nbytes_read <= nbytes);
    if (nbytes_read < nbytes) {
        sky_error_raise_string(sky_EOFError, "premature end of input");
    }
}


static double
sky_marshal_read_binary_double(sky_marshal_load_context_t *context)
{
    double  value;
    uint8_t buffer[8];

    sky_marshal_read(context, buffer, sizeof(buffer));
    if (!sky_util_double_decode(buffer,
                                sizeof(buffer),
                                SKY_ENDIAN_LITTLE,
                                &value))
    {
        sky_error_raise_string(sky_ValueError, "bad object data");
    }

    return value;
}


static double
sky_marshal_read_double(sky_marshal_load_context_t *context)
{
    char    buffer[256], *endptr;
    double  value;
    uint8_t len;

    sky_marshal_read(context, &len, sizeof(len));
    sky_marshal_read(context, buffer, len);
    buffer[len] = '\0';

    value = strtod(buffer, &endptr);
    if (endptr != &(buffer[len])) {
        sky_error_raise_string(sky_ValueError, "bad object data");
    }

    return value;
}


static inline int32_t
sky_marshal_read_int32(sky_marshal_load_context_t *context)
{
    int32_t value;

    sky_marshal_read(context, &value, sizeof(value));
    return sky_endian_ltoh32(value);
}


static inline int64_t
sky_marshal_read_int64(sky_marshal_load_context_t *context)
{
    int64_t hi, lo;

    lo = sky_marshal_read_int32(context);
    hi = sky_marshal_read_int32(context);

    return (hi << 32) | (lo & 0xFFFFFFFFL);
}


static inline ssize_t
sky_marshal_read_size(sky_marshal_load_context_t *context)
{
    return sky_marshal_read_int32(context);
}


static sky_bytes_t
sky_marshal_read_bytes(sky_marshal_load_context_t *context)
{
    ssize_t     nbytes;
    sky_bytes_t result;

    if (!(nbytes = sky_marshal_read_size(context))) {
        return sky_bytes_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        char    *bytes;

        bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ON_ERROR);
        sky_marshal_read(context, bytes, nbytes);
        result = sky_bytes_createwithbytes(bytes, nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_code_t
sky_marshal_read_code(sky_marshal_load_context_t *context)
{
    ssize_t         argcount, firstlineno, flags, kwonlyargcount, nlocals,
                    stacksize;
    sky_object_t    cellvars, code, consts, filename, freevars, lnotab, name,
                    names, varnames;

    argcount = sky_marshal_read_size(context);
    kwonlyargcount = sky_marshal_read_size(context);
    nlocals = sky_marshal_read_size(context);
    stacksize = sky_marshal_read_size(context);
    flags = sky_marshal_read_int32(context);

    if (!(code = sky_marshal_load_object(context)) ||
        !(consts = sky_marshal_load_object(context)) ||
        !(names = sky_marshal_load_object(context)) ||
        !(varnames = sky_marshal_load_object(context)) ||
        !(freevars = sky_marshal_load_object(context)) ||
        !(cellvars = sky_marshal_load_object(context)) ||
        !(filename = sky_marshal_load_object(context)) ||
        !(name = sky_marshal_load_object(context)))
    {
        sky_error_raise_string(sky_TypeError,
                               "NULL object in marshal data for code");
    }

    firstlineno = sky_marshal_read_size(context);
    if (!(lnotab = sky_marshal_load_object(context))) {
        sky_error_raise_string(sky_TypeError,
                               "NULL object in marshal data for code");
    }

    return sky_code_create(argcount,
                           kwonlyargcount,
                           nlocals,
                           stacksize,
                           flags,
                           code,
                           consts,
                           names,
                           varnames,
                           filename,
                           name,
                           firstlineno,
                           lnotab,
                           freevars,
                           cellvars);
}


static sky_dict_t
sky_marshal_read_dict(sky_marshal_load_context_t *context)
{
    sky_dict_t      dict;
    sky_object_t    key, value;

    dict = sky_dict_create();
    for (;;) {
        if (!(key = sky_marshal_load_object(context))) {
            break;
        }
        if ((value = sky_marshal_load_object(context)) != NULL) {
            sky_dict_setitem(dict, key, value);
        }
    }

    return dict;
}


static sky_frozenset_t
sky_marshal_read_frozenset(sky_marshal_load_context_t *context)
{
    ssize_t         count;
    sky_frozenset_t result;

    if (!(count = sky_marshal_read_size(context))) {
        return sky_frozenset_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         i;
        sky_object_t    *objects;

        objects = sky_asset_malloc(count * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ALWAYS);

        for (i = 0; i < count; ++i) {
            if (!(objects[i] = sky_marshal_load_object(context))) {
                sky_error_raise_string(sky_TypeError,
                                       "NULL object in marshal data for set");
            }
        }

        result = sky_frozenset_createfromarray(count, objects);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_integer_t
sky_marshal_read_integer(sky_marshal_load_context_t *context)
{
    ssize_t         nwords;
    sky_integer_t   result;

    if (!(nwords = sky_marshal_read_size(context))) {
        return sky_integer_zero;
    }

    SKY_ASSET_BLOCK_BEGIN {
        char    *bytes;
        mpz_t   mpz;
        ssize_t nbytes;

        nbytes = (nwords < 0 ? -nwords : nwords) * 2;
        bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
        sky_marshal_read(context, bytes, nbytes);

        /* topmost marshal word should be non-zero */
        if (!bytes[nbytes - 1] && !bytes[nbytes - 2]) {
            sky_error_raise_string(
                    sky_ValueError,
                    "bad marshal data (unnormalized long data)");
        }

        mpz_init(mpz);
        sky_asset_save(mpz, (sky_free_t)mpz_clear, SKY_ASSET_CLEANUP_ON_ERROR);

        mpz_import(mpz, nbytes / 2, -1, 2, -1, 1, bytes);
        if (nwords < 0) {
            mpz_neg(mpz, mpz);
        }
        result = sky_integer_createwithmpz(sky_integer_type, mpz);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_list_t
sky_marshal_read_list(sky_marshal_load_context_t *context)
{
    int64_t         count;
    sky_list_t      result;
    sky_object_t    object;

    count = sky_marshal_read_size(context);
    result = sky_list_createwithcapacity(count);
    while (count--) {
        if (!(object = sky_marshal_load_object(context))) {
            sky_error_raise_string(sky_TypeError,
                                   "NULL object in marshal data for list");
        }
        sky_list_append(result, object);
    }

    return result;
}


static sky_set_t
sky_marshal_read_set(sky_marshal_load_context_t *context)
{
    ssize_t         count;
    sky_set_t       result;
    sky_object_t    object;

    if (!(count = sky_marshal_read_size(context))) {
        return sky_set_create(NULL);
    }

    result = sky_set_createwithcapacity(count);
    while (count--) {
        if (!(object = sky_marshal_load_object(context))) {
            sky_error_raise_string(sky_TypeError,
                                   "NULL object in marshal data for set");
        }
        sky_set_add(result, object);
    }

    return result;
}


static sky_string_t
sky_marshal_read_string(sky_marshal_load_context_t *context)
{
    ssize_t         nbytes;
    sky_string_t    result;

    if (!(nbytes = sky_marshal_read_size(context))) {
        return sky_string_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        char    *bytes;

        bytes = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
        sky_marshal_read(context, bytes, nbytes);
        result = sky_string_createfrombytes(bytes,
                                            nbytes,
                                            SKY_STRING_LITERAL("utf-8"),
                                            SKY_STRING_LITERAL("surrogatepass"));
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_tuple_t
sky_marshal_read_tuple(sky_marshal_load_context_t *context)
{
    ssize_t     count;
    sky_tuple_t result;

    if (!(count = sky_marshal_read_size(context))) {
        return sky_tuple_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t         i;
        sky_object_t    *objects;

        objects = sky_asset_malloc(count * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 0; i < count; ++i) {
            if (!(objects[i] = sky_marshal_load_object(context))) {
                sky_error_raise_string(sky_TypeError,
                                       "NULL object in marshal data for tuple");
            }
        }

        result = sky_tuple_createwitharray(count, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_object_t
sky_marshal_load_object(sky_marshal_load_context_t *context)
{
    uint8_t         tag;
    sky_object_t    result;

    if (++context->depth > SKY_MARSHAL_MAX_STACK_DEPTH) {
        sky_error_raise_string(sky_ValueError, "recursion limit exceeded");
    }
    result = (sky_object_t)~(uintptr_t)0;

    sky_marshal_read(context, &tag, sizeof(tag));
    switch ((sky_marshal_tag_t)tag) {
        case SKY_MARSHAL_TAG_NULL:
            result = NULL;
            break;
        case SKY_MARSHAL_TAG_NONE:
            result = sky_None;
            break;
        case SKY_MARSHAL_TAG_FALSE:
            result = sky_False;
            break;
        case SKY_MARSHAL_TAG_TRUE:
            result = sky_True;
            break;
        case SKY_MARSHAL_TAG_STOPITER:
            result = sky_StopIteration;
            break;
        case SKY_MARSHAL_TAG_ELLIPSIS:
            result = sky_Ellipsis;
            break;
        case SKY_MARSHAL_TAG_INT:
            result = sky_integer_create(sky_marshal_read_int32(context));
            break;
        case SKY_MARSHAL_TAG_INT64:
            result = sky_integer_create(sky_marshal_read_int64(context));
            break;
        case SKY_MARSHAL_TAG_FLOAT:
            result = sky_float_create(sky_marshal_read_double(context));
            break;
        case SKY_MARSHAL_TAG_BINARY_FLOAT:
            result = sky_float_create(sky_marshal_read_binary_double(context));
            break;
        case SKY_MARSHAL_TAG_COMPLEX:
            result = sky_complex_create(sky_marshal_read_double(context),
                                        sky_marshal_read_double(context));
            break;
        case SKY_MARSHAL_TAG_BINARY_COMPLEX:
            result = sky_complex_create(sky_marshal_read_binary_double(context),
                                        sky_marshal_read_binary_double(context));
            break;
        case SKY_MARSHAL_TAG_LONG:
            result = sky_marshal_read_integer(context);
            break;
        case SKY_MARSHAL_TAG_STRING:
            result = sky_marshal_read_bytes(context);
            break;
        case SKY_MARSHAL_TAG_TUPLE:
            result = sky_marshal_read_tuple(context);
            break;
        case SKY_MARSHAL_TAG_LIST:
            result = sky_marshal_read_list(context);
            break;
        case SKY_MARSHAL_TAG_DICT:
            result = sky_marshal_read_dict(context);
            break;
        case SKY_MARSHAL_TAG_CODE:
            result = sky_marshal_read_code(context);
            break;
        case SKY_MARSHAL_TAG_UNICODE:
            result = sky_marshal_read_string(context);
            break;
        case SKY_MARSHAL_TAG_UNKNOWN:
            break;
        case SKY_MARSHAL_TAG_SET:
            result = sky_marshal_read_set(context);
            break;
        case SKY_MARSHAL_TAG_FROZENSET:
            result = sky_marshal_read_frozenset(context);
            break;
    }

    if ((sky_object_t)~(uintptr_t)0 == result) {
        sky_error_raise_string(sky_ValueError,
                               "bad marshal data (unknown type code)");
    }
    --context->depth;
    return result;
}


sky_object_t
sky_marshal_loadwithcallback(sky_marshal_load_t callback,
                             void *             arg)
{
    sky_marshal_load_context_t  context;

    context.callback = callback;
    context.arg = arg;
    context.depth = 0;

    return sky_marshal_load_object(&context);
}


static ssize_t
sky_marshal_loadfromfile(void *arg, void *bytes, ssize_t nbytes)
{
    ssize_t result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_memoryview_t    view;

        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_t    *buffer;

            buffer = sky_asset_calloc(1, sizeof(sky_buffer_t),
                                      SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_initialize(buffer,
                                  bytes,
                                  nbytes,
                                  SKY_TRUE,
                                  SKY_BUFFER_FLAG_CONTIG);
            view = sky_memoryview_createwithbuffer(buffer);
        } SKY_ASSET_BLOCK_END;

        sky_asset_save(view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_file_readinto(arg, view);
    } SKY_ASSET_BLOCK_END;

    return result;
}

sky_object_t
sky_marshal_load(sky_object_t file)
{
    return sky_marshal_loadwithcallback(sky_marshal_loadfromfile, file);
}


static ssize_t
sky_marshal_loadfrombuffer(void *arg, void *bytes, ssize_t nbytes)
{
    sky_marshal_buffer_t    *buffer = arg;

    nbytes = SKY_MIN(nbytes, buffer->size - buffer->used);
    memcpy(bytes, buffer->bytes + buffer->used, nbytes);
    buffer->used += nbytes;

    return nbytes;
}

sky_object_t
sky_marshal_loads(sky_object_t string)
{
    sky_object_t    object;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t            buffer;
        sky_marshal_buffer_t    marshal_buffer;

        sky_buffer_acquire(&buffer, string, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        marshal_buffer.bytes = buffer.buf;
        marshal_buffer.used = 0;
        marshal_buffer.size = buffer.len;

        object = sky_marshal_loadwithcallback(sky_marshal_loadfrombuffer,
                                              &marshal_buffer);
    } SKY_ASSET_BLOCK_END;

    return object;
}
