#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"

#include "sky_code.h"
#include "sky_generator.h"


typedef struct sky_generator_data_s {
    sky_frame_t                         frame;
    sky_code_t                          code;
    sky_bool_t                          running;
} sky_generator_data_t;

SKY_EXTERN_INLINE sky_generator_data_t *
sky_generator_data(sky_object_t object)
{
    if (sky_generator_type == sky_object_type(object)) {
        return (sky_generator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_generator_type);
}


static void
sky_generator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_generator_data_t    *self_data = data;

    sky_object_visit(self_data->frame, visit_data);
}


static sky_object_t
sky_generator_delegating_generator(sky_generator_t self)
{
    sky_frame_t         frame;
    sky_frame_data_t    *frame_data;
    sky_code_opcode_t   opcode;

    if (!(frame = sky_generator_data(self)->frame)) {
        return NULL;
    }
    frame_data = sky_frame_data(frame);
    if (!frame_data->bytecode) {
        return NULL;
    }
    opcode = (sky_code_opcode_t)frame_data->bytecode[frame_data->pc - 1];
    if (opcode != SKY_CODE_OPCODE_YIELD_FROM) {
        return NULL;
    }
    return frame_data->sp[-1];
}


static void
sky_generator_close_iterator(sky_object_t iterator)
{
    sky_object_t    close;

    if (sky_object_type(iterator) == sky_generator_type) {
        sky_generator_close(iterator);
    }
    else if ((close = sky_object_getattr(iterator,
                                         SKY_STRING_LITERAL("close"),
                                         NULL)) != NULL)
    {
        sky_object_call(close, NULL, NULL);
    }
}


static sky_object_t
sky_generator_run(sky_generator_t       self,
                  sky_object_t          arg,
                  sky_error_record_t *  error)
{
    sky_generator_data_t * volatile self_data = sky_generator_data(self);

    sky_object_t    value;

    if (self_data->running) {
        sky_error_raise_string(sky_ValueError,
                               "generator already executing");
    }
    if (!self_data->frame) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    SKY_ERROR_TRY {
        sky_frame_data_t    *frame_data = sky_frame_data(self_data->frame);

        sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_back)),
                          sky_tlsdata_get()->current_frame,
                          self_data->frame);
        self_data->running = SKY_TRUE;
        value = sky_frame_evaluate(self_data->frame, arg, error);
        self_data->running = SKY_FALSE;
    } SKY_ERROR_EXCEPT_ANY {
        self_data->running = SKY_FALSE;
        self_data->frame = NULL;
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    return value;
}


void
sky_generator_close(sky_generator_t self)
{
    sky_object_t        delegating_generator;
    sky_error_record_t  *record;

    delegating_generator = sky_generator_delegating_generator(self);
    if (delegating_generator) {
        sky_generator_data_t * volatile self_data = sky_generator_data(self);

        SKY_ERROR_TRY {
            self_data->running = SKY_TRUE;
            sky_generator_close_iterator(delegating_generator);
            self_data->running = SKY_FALSE;
        } SKY_ERROR_EXCEPT_ANY {
            self_data->running = SKY_FALSE;
            sky_error_raise();
        } SKY_ERROR_TRY_END;
    }

    SKY_ERROR_TRY {
        record = sky_error_record_create(sky_GeneratorExit,
                                         sky_None,
                                         NULL,
                                         __FUNCTION__,
                                         __FILE__,
                                         __LINE__);
        sky_asset_save(record,
                       (sky_free_t)sky_error_record_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_error_record_normalize(record);

        sky_generator_run(self, sky_None, record);
        sky_error_raise_string(sky_RuntimeError,
                               "generator ignored GeneratorExit");
    } SKY_ERROR_EXCEPT(sky_GeneratorExit) {
        /* pass */
    } SKY_ERROR_EXCEPT(sky_StopIteration) {
        /* pass */
    } SKY_ERROR_TRY_END;
}


void
sky_generator_del(sky_generator_t self)
{
    sky_generator_data_t    *self_data = sky_generator_data(self);

    if (self_data->frame) {
        sky_generator_close(self);
    }
}


sky_generator_t
sky_generator_createwithframe(sky_frame_t frame)
{
    sky_frame_data_t    *frame_data = sky_frame_data(frame);
    sky_code_data_t     *code_data = sky_code_data(frame_data->f_code);

    sky_generator_t         generator;
    sky_generator_data_t    *generator_data;

    if (!(code_data->co_flags & SKY_CODE_FLAG_GENERATOR)) {
        sky_error_raise_string(sky_TypeError,
                               "frame for generator is not for generator code");
    }

    generator = sky_object_allocate(sky_generator_type);
    generator_data = sky_generator_data(generator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(generator_data->frame)),
                      frame,
                      generator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(generator_data->code)),
                      frame_data->f_code,
                      generator);

    return generator;
}


sky_object_t
sky_generator_iter(sky_generator_t self)
{
    return self;
}


static sky_object_t
sky_generator_name_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_generator_data_t    *self_data = sky_generator_data(self);
    sky_code_data_t         *code_data = sky_code_data(self_data->code);

    return code_data->co_name;
}


sky_object_t
sky_generator_next(sky_generator_t self)
{
    return sky_generator_run(self, sky_None, NULL);
}


sky_string_t
sky_generator_repr(sky_generator_t self)
{
    sky_generator_data_t    *self_data = sky_generator_data(self);
    sky_code_data_t         *code_data = sky_code_data(self_data->code);

    return sky_string_createfromformat("<%@ object %@ at %p>",
                                       sky_type_name(sky_object_type(self)),
                                       code_data->co_name,
                                       self);
}


sky_object_t
sky_generator_send(sky_generator_t self, sky_object_t arg)
{
    return sky_generator_run(self, arg, NULL);
}


sky_object_t
sky_generator_throw(sky_generator_t self,
                    sky_object_t    typ,
                    sky_object_t    val,
                    sky_object_t    tb)
{
    sky_object_t        delegating_generator, throw, value;
    sky_frame_data_t    *frame_data;
    sky_error_record_t  *record;

    delegating_generator = sky_generator_delegating_generator(self);
    if (delegating_generator) {
        sky_generator_data_t * volatile self_data = sky_generator_data(self);

        if (sky_type_issubtype(typ, sky_GeneratorExit)) {
            record = NULL;
            SKY_ERROR_TRY {
                self_data->running = SKY_TRUE;
                sky_generator_close_iterator(delegating_generator);
                self_data->running = SKY_FALSE;
            } SKY_ERROR_EXCEPT_ANY {
                self_data->running = SKY_FALSE;
                record = sky_error_current();
                sky_error_record_retain(record);
            } SKY_ERROR_TRY_END;

            if (record) {
                SKY_ASSET_BLOCK_BEGIN {
                    sky_asset_save(record,
                                   (sky_free_t)sky_error_record_release,
                                   SKY_ASSET_CLEANUP_ALWAYS);
                    value = sky_generator_run(self, sky_None, record);
                } SKY_ASSET_BLOCK_END;
                return value;
            }
        }
        else {
            SKY_ERROR_TRY {
                self_data->running = SKY_TRUE;
                if (sky_object_type(delegating_generator) == sky_generator_type) {
                    value = sky_generator_throw(delegating_generator,
                                                typ,
                                                val,
                                                tb);
                }
                else if ((throw = sky_object_getattr(delegating_generator,
                                                     SKY_STRING_LITERAL("throw"),
                                                     NULL)) != NULL)
                {
                    value = sky_object_call(throw,
                                            sky_tuple_pack(3, typ, val, tb),
                                            NULL);
                }
                self_data->running = SKY_FALSE;
            } SKY_ERROR_EXCEPT_ANY {
                self_data->running = SKY_FALSE;
                frame_data = sky_frame_data(self_data->frame);
                --frame_data->sp;
                ++frame_data->pc;

                SKY_ASSET_BLOCK_BEGIN {
                    record = sky_error_current();
                    sky_error_record_retain(record);
                    sky_asset_save(record,
                                   (sky_free_t)sky_error_record_release,
                                   SKY_ASSET_CLEANUP_ALWAYS);

                    if (!sky_object_isa(record->value, sky_StopIteration)) {
                        sky_generator_run(self, sky_None, record);
                    }
                    else {
                        value = sky_object_getattr(record->value,
                                                   SKY_STRING_LITERAL("value"),
                                                   sky_NotSpecified);
                        sky_generator_run(self, value, NULL);
                    }
                } SKY_ASSET_BLOCK_END;
                sky_error_raise();
            } SKY_ERROR_TRY_END;
        }
    }

    /* This could be handled by parameter_list (and was originally written to),
     * but the test_generators unit test looks specifically for a different
     * error message than the standard one.
     * #REDUNDANT_ERROR_CHECKING_CODE
     */
    if (sky_object_isnull(tb)) {
        tb = NULL;
    }
    else if (!sky_object_isa(tb, sky_traceback_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "throw() third argument must be a traceback object");
    }

    if (sky_object_isinstance(typ, sky_BaseException)) {
        if (!sky_object_isnull(val)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "instance exception may not have a separate value");
        }
        val = typ;
        typ = sky_object_type(val);
        if (sky_object_isnull(tb)) {
            tb = sky_BaseException_traceback(val);
        }
    }
    else if (!sky_object_isa(typ, sky_type_type) ||
             !sky_type_issubtype(typ, sky_BaseException))
    {
        sky_error_raise_format(
                sky_TypeError,
                "exceptions must be classes or instances deriving from "
                "BaseException, not %@",
                sky_type_name(sky_object_type(typ)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        record = sky_error_record_create(typ,
                                         val,
                                         tb,
                                         __FUNCTION__,
                                         __FILE__,
                                         __LINE__);
        sky_asset_save(record,
                       (sky_free_t)sky_error_record_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_error_record_normalize(record);

        value = sky_generator_run(self, sky_None, record);
    } SKY_ASSET_BLOCK_END;

    return value;
}


SKY_TYPE_DEFINE_SIMPLE(generator,
                       "generator",
                       sizeof(sky_generator_data_t),
                       NULL,
                       NULL,
                       sky_generator_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_generator_initialize_library(void)
{
    sky_type_initialize_builtin(sky_generator_type, 0);

    sky_type_setattr_builtin(sky_generator_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_generator_type,
            "__del__", sky_generator_del,
            "__repr__", sky_generator_repr,
            "__iter__", sky_generator_iter,
            "__next__", sky_generator_next,
            NULL);

    sky_type_setattr_getset(
            sky_generator_type,
            "__name__",
            "Return the name of the generator's associated code object.",
            sky_generator_name_getter,
            NULL);

    sky_type_setmembers(sky_generator_type,
            "gi_code",      NULL, offsetof(sky_generator_data_t, code),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "gi_frame",     NULL, offsetof(sky_generator_data_t, frame),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "gi_running",   NULL, offsetof(sky_generator_data_t, running),
                            SKY_DATA_TYPE_BOOL, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(sky_generator_type,
            "close",
            sky_function_createbuiltin(
                    "generator.close",
                    "close() -> raise GeneratorExit inside generator.",
                    (sky_native_code_function_t)sky_generator_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_generator_type,
                    NULL));
    sky_type_setattr_builtin(sky_generator_type,
            "send",
            sky_function_createbuiltin(
                    "generator.send",
                    "send(arg) -> send 'arg' into generator,\n"
                    "return next yielded value or raise StopIteration.",
                    (sky_native_code_function_t)sky_generator_send,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_generator_type,
                    "arg", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_generator_type,
            "throw",
            sky_function_createbuiltin(
                    "generator.throw",
                    "throw(typ[,val[,tb]]) -> raise exception in generator,\n"
                    "return next yielded value or raise StopIteration.",
                    (sky_native_code_function_t)sky_generator_throw,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_generator_type,
                    "typ", SKY_DATA_TYPE_OBJECT, NULL,
                    "val", SKY_DATA_TYPE_OBJECT, sky_None,
                    "tb", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
}
