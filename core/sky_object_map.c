#include "sky_private.h"


/* This is a special implementation of sky_dict that is used specifically for
 * tracking object roots for use by the garbage collector.
 */


static inline uintptr_t
sky_object_map_key_hash(void *key)
{
    return (uintptr_t)key >> 4;
}


static sky_bool_t
sky_object_map_item_compare(sky_hashtable_item_t *item, void *key)
{
    return (((sky_object_map_item_t *)item)->object == key);
}


void
sky_object_map_combine(sky_object_map_t * restrict map,
                       sky_object_map_t * restrict other_map)
{
    sky_hashtable_item_t        *item;
    sky_hashtable_iterator_t    iterator;

    sky_hashtable_iterator_init(&iterator, &(other_map->table));
    while ((item = sky_hashtable_iterator_next(&iterator)) != NULL) {
        sky_object_map_update(map,
                              ((sky_object_map_item_t *)item)->object,
                              ((sky_object_map_item_t *)item)->counter);
    }
    sky_hashtable_iterator_cleanup(&iterator);
}


sky_object_map_t *
sky_object_map_create(void)
{
    sky_object_map_t    *map;

    map = sky_calloc(1, sizeof(sky_object_map_t));
    sky_hashtable_init(&(map->table), sky_object_map_item_compare);

    return map;
}


void
sky_object_map_destroy(sky_object_map_t *map)
{
    sky_hashtable_cleanup(&(map->table), sky_free);
    sky_free(map);
}


sky_bool_t
sky_object_map_contains(sky_object_map_t *map, sky_object_t object)
{
    uintptr_t   key_hash;

    key_hash = sky_object_map_key_hash(object);
    return (sky_hashtable_lookup(&(map->table), key_hash, object) != NULL);
}


void
sky_object_map_mark(sky_object_map_t *map, uint32_t mark)
{
    sky_hashtable_item_t        *item;
    sky_hashtable_iterator_t    iterator;

    SKY_ASSET_BLOCK_BEGIN {
        sky_hashtable_iterator_init(&iterator, &(map->table));
        sky_asset_save(&iterator,
                       (sky_free_t)sky_hashtable_iterator_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        while ((item = sky_hashtable_iterator_next(&iterator)) != NULL) {
            sky_object_gc_mark(((sky_object_map_item_t *)item)->object, mark);
        }
    } SKY_ASSET_BLOCK_END;
}


void
sky_object_map_update(sky_object_map_t *map,
                      sky_object_t object,
                      intmax_t delta)
{
    uintptr_t               key_hash;
    sky_object_map_item_t   *item;

    key_hash = sky_object_map_key_hash(object);
    if (!(item = (sky_object_map_item_t *)sky_hashtable_lookup(&(map->table),
                                                               key_hash,
                                                               object)))
    {
        item = sky_calloc(1, sizeof(sky_object_map_item_t));
        item->object = object;
        item->counter = delta;
        sky_hashtable_insert(&(map->table), &(item->base), key_hash, object);
    }
    else {
        item->counter += delta;
        if (!item->counter) {
            sky_free(sky_hashtable_delete(&(map->table), key_hash, object));
        }
    }
}
