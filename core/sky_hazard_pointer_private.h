#ifndef __SKYTHON_CORE_SKY_HAZARD_POINTER_PRIVATE_H__
#define __SKYTHON_CORE_SKY_HAZARD_POINTER_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_malloc.h"


SKY_CDECLS_BEGIN


typedef struct sky_hazard_pointer_private_s
        sky_hazard_pointer_private_t;
typedef struct sky_hazard_pointer_retiree_s
        sky_hazard_pointer_retiree_t;
typedef struct sky_hazard_pointer_set_s
        sky_hazard_pointer_set_t;
typedef struct sky_hazard_pointer_block_s
        sky_hazard_pointer_block_t;
typedef struct sky_hazard_pointer_retiree_block_s
        sky_hazard_pointer_retiree_block_t;


struct sky_hazard_pointer_private_s {
    void * volatile                     pointer;
    sky_hazard_pointer_private_t *      next;

    int8_t cache_line_padding[SKY_CACHE_LINE_PAD(
                              sizeof(void *) +
                              sizeof(sky_hazard_pointer_private_t *))];
};


struct sky_hazard_pointer_retiree_s {
    void *                              pointer;
    sky_free_t                          free;
    sky_hazard_pointer_retiree_t *      next;
    sky_hazard_pointer_retiree_block_t *block;
};


struct sky_hazard_pointer_set_s {
    size_t                              capacity_mask;
    void *                              table[];
};


struct sky_hazard_pointer_block_s {
    sky_hazard_pointer_block_t *        next;
    int32_t                             avail_count;
    int32_t                             inuse_count;

    SKY_CACHE_LINE_ALIGNED
    sky_hazard_pointer_private_t        hazard_pointers[];
};


struct sky_hazard_pointer_retiree_block_s {
    sky_hazard_pointer_retiree_block_t *next;
    sky_hazard_pointer_retiree_block_t *prev;
    size_t                              avail_count;
    size_t                              inuse_count;
    size_t                              inuse_index;
    sky_hazard_pointer_retiree_t *      freelist;
    sky_hazard_pointer_retiree_t        retirees[];
};


typedef struct sky_hazard_pointer_tlsdata_s {
    sky_hazard_pointer_private_t *      unused_hazard_pointers;
    sky_hazard_pointer_retiree_t *      retired_pointers;
    int32_t                             retired_pointer_count;
    int32_t                             active_hazard_count;
    sky_hazard_pointer_retiree_block_t *retiree_blocks_head;
    sky_hazard_pointer_retiree_block_t *retiree_blocks_tail;
} sky_hazard_pointer_tlsdata_t;


typedef struct sky_hazard_pointer_tlsdata_orphan_s {
    sky_hazard_pointer_private_t *      unused_hazard_pointers;
    sky_hazard_pointer_retiree_t *      retired_pointers;
} sky_hazard_pointer_tlsdata_orphan_t;


extern void
sky_hazard_pointer_tlsdata_adopt(
        sky_hazard_pointer_tlsdata_t *          tlsdata,
        sky_hazard_pointer_tlsdata_orphan_t *   orphan_tlsdata);


extern void
sky_hazard_pointer_tlsdata_cleanup(
        sky_hazard_pointer_tlsdata_t *          tlsdata,
        sky_hazard_pointer_tlsdata_orphan_t *   orphan_tlsdata,
        void *                                  tlsdata_pointer,
        sky_free_t                              tlsdata_free);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_HAZARD_POINTER_PRIVATE_H__ */
