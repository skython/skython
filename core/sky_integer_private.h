#ifndef __SKYTHON_CORE_SKY_INTEGER_PRIVATE_H__
#define __SKYTHON_CORE_SKY_INTEGER_PRIVATE_H__ 1


#include "sky_base.h"

#include <gmp.h>


SKY_CDECLS_BEGIN


typedef struct sky_integer_data_s {
    mpz_t                               mpz;
    mp_limb_t                           value;
} sky_integer_data_t;

extern sky_integer_data_t *
sky_integer_data(sky_object_t object, sky_integer_data_t *tagged_data);


struct sky_integer_s {
    sky_object_data_t                   object_data;
    sky_integer_data_t                  integer_data;
};

struct sky_boolean_s {
    sky_object_data_t                   object_data;
    sky_integer_data_t                  integer_data;
};


static inline int
sky_integer_data_isnegative(const sky_integer_data_t *integer_data)
{
    return (integer_data->mpz[0]._mp_size < 0 ? SKY_TRUE : SKY_FALSE);
}


static inline int
sky_integer_data_iszero(sky_integer_data_t *integer_data)
{
    return !integer_data->mpz[0]._mp_size;
}


static inline int
sky_integer_data_size(sky_integer_data_t *integer_data)
{
    return abs(integer_data->mpz[0]._mp_size);
}


extern sky_integer_t
sky_integer_createwithmpz(sky_type_t cls, mpz_ptr mpz);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_INTEGER_PRIVATE_H__ */
