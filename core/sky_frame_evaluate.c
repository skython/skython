#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"

#include "sky_cell.h"
#include "sky_traceback.h"


#define CHECK_STACK_SPACE(_n)                                               \
        sky_error_validate_debug(sp + (_n) <= frame_data->stack +           \
                                              code_data->co_stacksize)


sky_bool_t
sky_frame_evaluate_trace(sky_frame_t                frame,
                         sky_frame_evaluate_trace_t trace,
                         sky_object_t               arg)
{
    sky_tlsdata_t       *tlsdata = sky_tlsdata_get();
    sky_frame_data_t    *frame_data = sky_frame_data(frame);

    sky_object_t    result, trace_function;
    sky_string_t    event;

    if (!(tlsdata->flags & SKY_TLSDATA_FLAG_TRACING)) {
        return SKY_FALSE;
    }

    /* Call the global trace function for "call" events, but call the local
     * trace function for everything else.
     */
    if (SKY_FRAME_EVALUATE_TRACE_CALL == trace) {
        trace_function = tlsdata->trace_function;
    }
    else {
        trace_function = frame_data->f_trace;
    }
    if (!trace_function) {
        return SKY_FALSE;
    }

    event = NULL;
    switch (trace) {
        case SKY_FRAME_EVALUATE_TRACE_CALL:
            event = SKY_STRING_LITERAL("call");
            break;
        case SKY_FRAME_EVALUATE_TRACE_LINE:
            event = SKY_STRING_LITERAL("line");
            break;
        case SKY_FRAME_EVALUATE_TRACE_RETURN:
            event = SKY_STRING_LITERAL("return");
            break;
        case SKY_FRAME_EVALUATE_TRACE_EXCEPTION:
            event = SKY_STRING_LITERAL("exception");
            break;
    }
    sky_error_validate_debug(NULL != event);

    SKY_ERROR_TRY {
        /* Disable tracing while the trace function is running. Restore it
         * regardless of whether an exception is raised.
         */
        tlsdata->flags &= ~SKY_TLSDATA_FLAG_TRACING;
        result = sky_object_call(trace_function,
                                 sky_tuple_pack(3, frame, event, arg),
                                 NULL);
        tlsdata->flags |= SKY_TLSDATA_FLAG_TRACING;
    } SKY_ERROR_EXCEPT_ANY {
        /* If there's an error in the trace function, disable all tracing. */
        tlsdata->flags |= SKY_TLSDATA_FLAG_TRACING;
        frame_data->f_trace = NULL;
        sky_object_gc_setroot(&(tlsdata->trace_function), NULL, SKY_FALSE);

        sky_error_raise();
    } SKY_ERROR_TRY_END;

    /* Set the local trace function. If the result from the trace function is
     * None, do no local tracing; otherwise, the return is the local trace
     * function to call.
     */
    if (sky_object_isnull(result)) {
        frame_data->f_trace = NULL;
        return SKY_FALSE;
    }

    if (frame_data->f_trace != result) {
        sky_object_gc_set(&(frame_data->f_trace), result, frame);
    }
    return SKY_TRUE;
}


static sky_bool_t
sky_frame_evaluate_trace_line(sky_frame_t       frame,
                              ssize_t *         instr_lb,
                              ssize_t *         instr_ub,
                              ssize_t *         instr_prev,
                              const uint8_t *   lnotab,
                              const uint8_t *   lnotab_end)
{
    sky_frame_data_t    *frame_data = sky_frame_data(frame);

    int             line;
    ssize_t         address;
    sky_bool_t      result;
    const uint8_t   *b;

    result = SKY_TRUE;
    line = frame_data->f_lineno;
    if (lnotab) {
        if (frame_data->f_lasti < *instr_lb ||
            frame_data->f_lasti >= *instr_ub)
        {
            address = 0;
            line = sky_code_data(frame_data->f_code)->co_firstlineno;
            for (b = lnotab; b < lnotab_end; b += 2) {
                if (address + b[0] > frame_data->f_lasti) {
                    break;
                }
                address += b[0];
                if (b[1]) {
                    *instr_lb = address;
                }
                line += b[1];
            }
            if (b >= lnotab_end) {
                *instr_ub = lnotab_end - lnotab;
            }
            else {
                while (b < lnotab_end) {
                    address += b[0];
                    if (b[1]) {
                        break;
                    }
                    b += 2;
                }
            }
        }
    }

    if (frame_data->f_lasti == *instr_lb || frame_data->f_lasti < *instr_prev) {
        sky_error_validate_debug(line);
        frame_data->f_lineno = line;
        result = sky_frame_evaluate_trace(frame,
                                          SKY_FRAME_EVALUATE_TRACE_LINE,
                                          sky_None);
    }
    *instr_prev = frame_data->f_lasti;

    return result;
}


static inline sky_bool_t
sky_frame_local_del(sky_frame_data_t *frame_data, sky_string_t name)
{
    sky_bool_t  bound;

    if (sky_object_type(frame_data->f_locals) == sky_dict_type) {
        return sky_dict_delete(frame_data->f_locals, name);
    }

    SKY_ERROR_TRY {
        sky_object_delitem(frame_data->f_locals, name);
        bound = SKY_TRUE;
    }
    SKY_ERROR_EXCEPT(sky_KeyError) {
        bound = SKY_FALSE;
    } SKY_ERROR_TRY_END;

    return bound;
}


static inline sky_object_t
sky_frame_local_get(sky_frame_data_t *frame_data, sky_string_t name)
{
    sky_object_t    value;

    if (sky_object_type(frame_data->f_locals) == sky_dict_type) {
        return sky_dict_get(frame_data->f_locals, name, NULL);
    }

    SKY_ERROR_TRY {
        value = sky_object_getitem(frame_data->f_locals, name);
    }
    SKY_ERROR_EXCEPT(sky_KeyError) {
        value = NULL;
    } SKY_ERROR_TRY_END;

    return value;
}


static sky_object_t
sky_frame_lookup_name(sky_frame_data_t *frame_data, sky_string_t name)
{
    sky_object_t    value;

    if (frame_data->nlocals >= 0 ||
        !(value = sky_frame_local_get(frame_data, name)))
    {
        if (!(value = sky_dict_get(frame_data->f_globals, name, NULL))) {
            SKY_ERROR_TRY {
                value = sky_object_getitem(
                                frame_data->f_builtins,
                                name);
            }
            SKY_ERROR_EXCEPT(sky_KeyError) {
                sky_error_clear();
                sky_error_raise_format(sky_NameError,
                                       "name %#@ is not defined",
                                       name);
            } SKY_ERROR_TRY_END;
        }
    }

    return value;
}


static sky_type_t
sky_frame_evaluate_build_class(sky_frame_data_t *   frame_data,
                               size_t               flags,
                               ssize_t              nbases,
                               ssize_t              nkeywords,
                               sky_object_t *       values)
{
    sky_object_t    bases, closure, code, keywords, metaclass, qualname;

    sky_error_validate_debug(sky_object_isa(values[0], sky_string_type));
    sky_error_validate_debug(sky_object_isa(values[1], sky_code_type));

    qualname = *values++;
    code = *values++;

    closure = keywords = metaclass = NULL;
    bases = sky_tuple_empty;
    if (flags & SKY_CODE_BUILD_CLASS_FLAG_CLOSURE) {
        closure = *values++;
    }
    if (nbases) {
        bases = sky_tuple_createfromarray(nbases, values);
        values += nbases;
    }
    if (flags & SKY_CODE_BUILD_CLASS_FLAG_ARGS) {
        bases = sky_number_add(bases, *values++);
    }
    if (nkeywords) {
        keywords = sky_dict_createfromarray(nkeywords * 2, values);
        values += (nkeywords * 2);
    }
    if (flags & SKY_CODE_BUILD_CLASS_FLAG_KWS) {
        if (!keywords) {
            keywords = *values++;
        }
        else {
            sky_dict_merge(keywords, *values++, SKY_TRUE);
        }
    }

    if (keywords &&
        (metaclass = sky_dict_get(keywords,
                                  SKY_STRING_LITERAL("metaclass"),
                                  NULL)) != NULL)
    {
        keywords = sky_dict_copy(keywords);
        sky_dict_delete(keywords, SKY_STRING_LITERAL("metaclass"));
    }

    return sky_type_create(qualname,
                           bases,
                           metaclass,
                           code,
                           closure,
                           frame_data->f_globals,
                           keywords);
}


static sky_function_t
sky_frame_evaluate_build_function(sky_frame_data_t *frame_data,
                                  size_t            flags,
                                  ssize_t           ndefaults,
                                  ssize_t           nkwdefaults,
                                  sky_object_t *    values)
{
    sky_object_t    annotations, closure, code, defaults, kwdefaults, qualname;
    sky_function_t  function;
    sky_code_data_t *code_data;

    sky_error_validate_debug(sky_object_isa(values[0], sky_string_type));
    sky_error_validate_debug(sky_object_isa(values[1], sky_code_type));

    qualname = *values++;
    code = *values++;

    annotations = closure = defaults = kwdefaults = NULL;
    if (flags & SKY_CODE_BUILD_FUNCTION_FLAG_ANNOTATIONS) {
        annotations = *values++;
    }
    if (flags & SKY_CODE_BUILD_FUNCTION_FLAG_CLOSURE) {
        closure = *values++;

        /* 'closure' must be a tuple that is exactly the same size as
         * code_data->co_freevars. Furthermore, each element must be a cell
         * object. Do a cursory check for validity, but let PUSH_FREE, etc.
         * later do validation of the elements being a proper cell object.
         * Do a full check in a debug build.
         */
        code_data = sky_code_data(code);
        if (sky_tuple_type != sky_object_type(closure) ||
            sky_object_len(closure) != sky_object_len(code_data->co_freevars))
        {
            sky_error_raise_format(sky_SystemError,
                                   "invalid closure for %@",
                                   code_data->co_name);
        }
#if !defined(NDEBUG)
        do {
            sky_object_t    cell;

            SKY_SEQUENCE_FOREACH(closure, cell) {
                if (sky_cell_type != sky_object_type(cell)) {
                    sky_error_raise_format(sky_SystemError,
                                           "invalid closure for %@",
                                           code_data->co_name);
                }
            } SKY_SEQUENCE_FOREACH_END;
        } while (0);
#endif
    }

    if (ndefaults) {
        defaults = sky_tuple_createfromarray(ndefaults, values);
        values += ndefaults;
    }
    if (nkwdefaults) {
        kwdefaults = sky_dict_createfromarray(nkwdefaults * 2, values);
        values += (nkwdefaults * 2);
    }

    function = sky_function_create(qualname,
                                   code,
                                   frame_data->f_globals,
                                   closure,
                                   defaults,
                                   kwdefaults);
    if (annotations) {
        sky_object_setattr(function,
                           SKY_STRING_LITERAL("__annotations__"),
                           annotations);
    }

    return function;
}


static sky_object_t
sky_frame_evaluate_call(SKY_UNUSED sky_frame_t     frame,
                                   sky_object_t *  values,
                                   size_t          npositional,
                                   sky_bool_t      varargs,
                                   size_t          nkeywords,
                                   sky_bool_t      varkws)
{
    sky_dict_t      kws;
    sky_tuple_t     args;
    sky_object_t    callable;

    /* First, the object to be called. */
    callable = *values++;

    /* Second, positional arguments, possibly followed by *args */
    if (!npositional && !varargs) {
        args = NULL;
    }
    else if (!npositional) {
        args = sky_tuple_create(*values++);
    }
    else {
        args = sky_tuple_createfromarray(npositional, values);
        values += npositional;
        if (varargs) {
            args = sky_number_add(args, sky_tuple_create(*values++));
        }
    }

    /* Third, keyword arguments, possibly followed by **kws */
    if (!nkeywords && !varkws) {
        kws = NULL;
    }
    else {
        kws = sky_dict_createfromarray(nkeywords * 2, values);
        values += (nkeywords * 2);
        if (varkws) {
            sky_dict_merge(kws, *values++, SKY_TRUE);
        }
    }

    /* Finally, make the call and return the result. */
    return sky_object_call(callable, args, kws);
}


static sky_module_t
sky_frame_evaluate_import_module(sky_frame_data_t * frame_data,
                                 sky_string_t       name,
                                 sky_tuple_t        fromlist,
                                 sky_integer_t      level)
{
    sky_object_t    args, builtins, __import__;

    if (!(builtins = sky_dict_get(frame_data->f_globals,
                                  SKY_STRING_LITERAL("__builtins__"),
                                  NULL)))
    {
        builtins = sky_interpreter_builtins();
    }
    if (sky_object_isa(builtins, sky_module_type)) {
        builtins = sky_object_dict(builtins);
    }
    __import__ = sky_object_getitem(builtins,
                                    SKY_STRING_LITERAL("__import__"));

    args = sky_tuple_pack(5, name,
                             frame_data->f_globals,
                             frame_data->f_locals,
                             fromlist,
                             level);

    return sky_object_call(__import__, args, NULL);
}


static sky_object_t
sky_frame_evaluate_import_name(SKY_UNUSED sky_frame_data_t *   frame_data,
                                          sky_module_t         module,
                                          sky_string_t         name)
{
    sky_object_t    value;

    if (!(value = sky_object_getattr(module, name, NULL))) {
        sky_error_raise_format(sky_ImportError, "cannot import name %@", name);
    }

    return value;
}


static void
sky_frame_evaluate_import_star(sky_frame_data_t *   frame_data,
                               sky_module_t         module)
{
    sky_dict_t      dict;
    sky_object_t    iter, names, object;
    sky_string_t    name, underscore;

    if (!(names = sky_object_getattr(module,
                                     SKY_STRING_LITERAL("__all__"),
                                     NULL)))
    {
        /* names = [x for x in module.__dict__ if not x.startswith('_')] */
        dict = sky_object_dict(module);
        names = sky_list_createwithcapacity(sky_object_len(dict));
        underscore = SKY_STRING_LITERAL("_");
        iter = sky_object_iter(dict);
        while ((name = sky_object_next(iter, NULL)) != NULL) {
            if (!sky_string_startswith(name, underscore, 0, SSIZE_MAX)) {
                sky_list_append(names, name);
            }
        }
    }

    /* Cannot use fast locals when 'from module import *' is used. */
    sky_error_validate_debug(frame_data->f_locals);

    SKY_SEQUENCE_FOREACH(names, name) {
        object = sky_object_getattr(module, name, sky_NotSpecified);
        sky_object_setitem(frame_data->f_locals, name, object);
    } SKY_SEQUENCE_FOREACH_END;
}


static void SKY_NORETURN
sky_frame_evaluate_raise(sky_frame_t    frame,
                         sky_object_t   value,
                         sky_object_t   cause)
{
    sky_object_t        exc_value, context;
    sky_frame_data_t    *frame_data;
    sky_frame_block_t   *block;
    sky_error_record_t  *error, *new_error;

    for (error = NULL; frame && !error; frame = frame_data->f_back) {
        frame_data = sky_frame_data(frame);
        for (block = frame_data->blocks;
             block && !(error = block->error);
             block = block->next);
    }

    if (!value) {
        if (error) {
            sky_error_raise_record(error);
        }
        sky_error_raise_string(sky_RuntimeError,
                               "No active exception to reraise");
    }

    if (sky_object_isa(value, sky_type_type) &&
        sky_type_issubtype(value, sky_BaseException))
    {
        value = sky_object_call(value, NULL, NULL);
    }
    if (cause) {
        if (sky_object_isa(cause, sky_type_type) &&
            sky_type_issubtype(cause, sky_BaseException))
        {
            cause = sky_object_call(cause, NULL, NULL);
        }
        sky_BaseException_setcause(value, cause);
    }

    /* If there's no existing error, simply raise the exception and be done
     * with it; otherwise, more work must be done to handle implicit chaining.
     */
    if (!error) {
        sky_error_raise_object(sky_object_type(value), value);
    }

    SKY_ASSET_BLOCK_BEGIN {
        new_error = sky_error_record_create(sky_object_type(value),
                                            value,
                                            NULL,
                                            __FUNCTION__,
                                            __FILE__,
                                            __LINE__);
        sky_asset_save(new_error,
                       (sky_free_t)sky_error_record_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        /* The error must be normalized immediately, which is why we've gone
         * through the hassle of creating an error record ourselves.
         */
        sky_error_record_normalize(new_error);
        sky_error_record_normalize(error);

        /* Guard against cycles in the context chain. */
        exc_value = error->value;
        context = sky_BaseException_context(exc_value);
        while (!sky_object_isnull(context)) {
            if (context == new_error->value) {
                sky_BaseException_setcontext(exc_value, NULL);
                break;
            }
            exc_value = context;
            context = sky_BaseException_context(exc_value);
        }
        sky_BaseException_setcontext(new_error->value, error->value);

        sky_error_raise_record(new_error);
    } SKY_ASSET_BLOCK_END;
}


static sky_object_t *
sky_frame_evaluate_unpack_sequence(sky_frame_t  frame,
                                   ssize_t      count,
                                   sky_object_t value)
{
    sky_frame_data_t    *frame_data = sky_frame_data(frame);
    sky_object_t        *sp = frame_data->sp;
#if !defined(NDEBUG)
    sky_code_data_t     *code_data = sky_code_data(frame_data->f_code);
#endif

    ssize_t         avail, i;
    sky_object_t    iter, object;

    CHECK_STACK_SPACE(count);
    sp += count;
    i = 0;

    if (sky_sequence_isiterable(value)) {
        if ((avail = sky_object_len(value)) != count) {
            if (avail < count) {
                sky_error_raise_format(sky_ValueError,
                                       "need more than %zd value%s to unpack",
                                       avail,
                                       (avail == 1 ? "" : "s"));
            }
            sky_error_raise_format(sky_ValueError,
                                   "too many values to unpack (expected %zd)",
                                   count);
        }

        SKY_SEQUENCE_FOREACH(value, object) {
            sky_object_gc_set(&(sp[--i]), object, frame);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        avail = 0;
        iter = sky_object_iter(value);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            if (++avail > count) {
                sky_error_raise_format(
                        sky_ValueError,
                        "too many values to unpack (expected %zd)",
                        count);
            }
            sky_object_gc_set(&(sp[--i]), object, frame);
        }
        if (avail != count) {
            sky_error_raise_format(sky_ValueError,
                                   "need more than %zd value%s to unpack",
                                   avail,
                                   (avail == 1 ? "" : "s"));
        }
    }

    return sp;
}


static sky_object_t
sky_frame_evaluate_with_enter(sky_object_t mgr)
{
    typedef sky_object_t (*with_enter_t)(sky_object_t);

    sky_object_t    method, value;
    with_enter_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(mgr, ENTER))) {
        sky_error_raise_string(sky_AttributeError, "__enter__");
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (with_enter_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(value, cfunction, (mgr));
    }
    else {
        method = sky_descriptor_get(method, mgr, sky_object_type(mgr));
        value = sky_object_call(method, NULL, NULL);
    }

    return value;
}


static void
sky_frame_evaluate_with_exit(sky_frame_block_t *block)
{
    typedef sky_bool_t (*with_exit_t)(sky_object_t,
                                      sky_type_t,
                                      sky_object_t,
                                      sky_traceback_t);

    sky_bool_t      result;
    sky_type_t      exc_type;
    sky_tuple_t     args;
    with_exit_t     cfunction;
    sky_object_t    exc_value, method, mgr;
    sky_traceback_t exc_traceback;

    sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_WITH == block->type);
    block->type = SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP;

    if (!block->error) {
        exc_type = exc_value = exc_traceback = NULL;
    }
    else {
        exc_type = block->error->type;
        exc_value = block->error->value;
        exc_traceback = block->error->traceback;
    }

    mgr = block->with_mgr;
    method = block->with_exit;

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (with_exit_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result,
                               cfunction,
                               (mgr, exc_type, exc_value, exc_traceback));
    }
    else {
        args = sky_tuple_pack(3, exc_type, exc_value, exc_traceback);
        method = sky_descriptor_get(method, mgr, sky_object_type(mgr));
        result = sky_object_bool(sky_object_call(method, args, NULL));
    }
    if (result && block->error) {
        sky_error_record_release(block->error);
        block->error = NULL;
    }
}


static sky_object_t
sky_frame_evaluate_bytecode(sky_frame_t         frame,
                            sky_object_t        arg,
                            sky_error_record_t *error,
                            const uint8_t *     bytecode,
                            ssize_t             bytecode_length,
                            const uint8_t *     lnotab,
                            ssize_t             lnotab_length)
{
#if defined(HAVE_GNUC_COMPUTED_GOTO)
#   include "sky_frame_evaluate_labels.h"
#endif

    sky_tlsdata_t               *tlsdata = sky_tlsdata_get();
    sky_frame_data_t * volatile frame_data = sky_frame_data(frame);
    sky_code_data_t * volatile  code_data = sky_code_data(frame_data->f_code);

    int                 setjmp_value;
    size_t              argv[3], count, index;
    ssize_t             exit_offset, i, instr_lb, instr_prev, instr_ub, len,
                        offset;
    sky_bool_t          first_pass, tracing;
    sky_cell_t          cell;
    sky_slice_t         slice;
    sky_object_t        object, value;
    sky_string_t        name;
    const uint8_t       *lnotab_end;
    sky_traceback_t     traceback;
    sky_compare_op_t    compare_op;
    sky_code_opcode_t   opcode;
    sky_error_block_t   error_block;
    sky_frame_block_t   *block;
    sky_error_record_t  *current_error;

    sky_object_t            * volatile sp;
    const uint8_t           * volatile bytecode_end, * volatile pc;
    sky_object_t volatile   return_value;

#define CHECK_STACK(_n)                                                     \
        sky_error_validate_debug(sp - (_n) >= frame_data->stack)

#define DECODE_OPCODE_ARGUMENT(_v)                                          \
    do {                                                                    \
        pc = sky_util_varint_decode(pc, bytecode_end - pc, (_v));           \
    } while (0)

#define DECODE_OPCODE_OFFSET(_v)                                            \
    do {                                                                    \
        sky_error_validate_debug(pc + 1 < bytecode_end);                    \
        *(_v) = (pc[0] << 8) | pc[1];                                       \
        pc += 2;                                                            \
    } while (0)

#define CHECK_IN()                                                          \
    do {                                                                    \
        sky_atfork_check();                                                 \
        sky_object_gc();                                                    \
        sky_signal_check_pending();                                         \
    } while (0)

#define JUMP(_o)                                                            \
    do {                                                                    \
        CHECK_IN();                                                         \
        pc = bytecode + (_o);                                               \
    } while (0)

#define POP_BLOCK()                                                         \
    do {                                                                    \
        frame_data->blocks = block->next;                                   \
        block->next = (sky_frame_block_t *)((uintptr_t)block->next | 1);    \
        sky_hazard_pointer_retire(block, (sky_free_t)sky_frame_block_free); \
    } while (0)

#define POP(_v)                                                             \
    do {                                                                    \
        CHECK_STACK(1);                                                     \
        *(_v) = *--sp;                                                      \
    } while (0)

#define POP_N(_n)                                                           \
    do {                                                                    \
        ptrdiff_t   __n = (_n);                                             \
        CHECK_STACK(__n);                                                   \
        sp -= (__n);                                                        \
    } while (0);

#define PUSH(_v)                                                            \
    do {                                                                    \
        CHECK_STACK_SPACE(1);                                               \
        sky_object_gc_set(sp, (_v), frame);                                 \
        ++sp;                                                               \
    } while (0)

#define RERAISE()                                                           \
    do {                                                                    \
        sky_error_raise_record(block->error);                               \
    } while (0)

#define SYNC_FRAME()                                                        \
    do {                                                                    \
        frame_data->pc = pc - bytecode;                                     \
        frame_data->sp = sp;                                                \
    } while (0)

#define CALL(varargs, varkws)                                               \
    do {                                                                    \
        CHECK_IN();                                                         \
        POP_N(count);                                                       \
        SYNC_FRAME();                                                       \
        value = sky_frame_evaluate_call(frame,                              \
                                        sp,                                 \
                                        argv[0],                            \
                                        varargs,                            \
                                        argv[1],                            \
                                        varkws);                            \
        CHECK_IN();                                                         \
        if (tlsdata->trace_function) {                                      \
            tracing = SKY_TRUE;                                             \
        }                                                                   \
        PUSH(value);                                                        \
    } while (0)

#if defined(NDEBUG)
#   define VERIFY_CONDITIONAL_JUMP_SAFETY(_o)  do {} while (0)
#else
#   define VERIFY_CONDITIONAL_JUMP_SAFETY(_o)                               \
        do {                                                                \
            for (block = frame_data->blocks; block; block = block->next) {  \
                if ((pc - 3) - bytecode >= block->enter_offset &&           \
                    (pc - 3) - bytecode <= block->exit_offset)              \
                {                                                           \
                    sky_error_validate_debug((_o) > block->enter_offset &&  \
                                             (_o) <= block->exit_offset);   \
                }                                                           \
            }                                                               \
        } while (0)
#endif

#if defined(HAVE_GNUC_COMPUTED_GOTO)
#   define DISPATCH()                                       \
        if (likely(!tracing)) {                             \
            sky_error_validate_debug(pc < bytecode_end);    \
            frame_data->f_lasti = pc - bytecode;            \
            goto *opcode_handlers[*pc++];                   \
        }                                                   \
        goto evaluate_loop
#   define HANDLE_OPCODE(_opcode)                           \
        HANDLE_OPCODE_##_opcode:                            \
        case SKY_CODE_OPCODE_##_opcode:
#else
#   define DISPATCH()                                       \
        goto evaluate_loop
#   define HANDLE_OPCODE(_opcode)                           \
        case SKY_CODE_OPCODE_##_opcode:
#endif

    first_pass = SKY_TRUE;
    return_value = NULL;
    bytecode_end = bytecode + bytecode_length;
    tracing = SKY_FALSE;
    instr_lb = 0;
    instr_prev = instr_ub = -1;
    frame_data->bytecode = bytecode;

restart:
    if (lnotab) {
        lnotab_end = lnotab + lnotab_length;
    }
    sky_error_block_push(&error_block, SKY_ERROR_BLOCK_TYPE_TRY);
    setjmp_value = sky_setjmp(error_block.setjmp_context);
    if (setjmp_value != SKY_ERROR_SETJMP_TRY) {
        sky_error_validate_debug(SKY_ERROR_SETJMP_RAISE == setjmp_value);
        sky_error_validate_debug(SKY_ERROR_BLOCK_TYPE_TRY == error_block.block_type);

        error_block.block_type = SKY_ERROR_BLOCK_TYPE_EXCEPT;
        SYNC_FRAME();

        /* Find the block to handle the exception. If the first block already
         * has an error saved, a new exception has been raised in the handling
         * of that one, so pop the block and handle the new exception. If there
         * is no block to handle the exception, re-raise it to let the normal
         * error handling machinery take over.
         */
        while ((block = frame_data->blocks) != NULL && block->error) {
            POP_BLOCK();
        }
        current_error = sky_error_current();

        /* The handler for SKY_CODE_OPCODE_FINALLY_EXIT will re-raise the
         * block's error, which brings us here; however, that re-raise should
         * not be included in the traceback, because it's an implementation
         * detail.
         *
         * Note that frame_data->f_lasti can be -1 here if an error is being
         * sent to a generator that hasn't been started yet. Treat the current
         * instruction as RAISE.
         */
        if (-1 == frame_data->f_lasti ||
            (SKY_CODE_OPCODE_FINALLY_EXIT != bytecode[frame_data->f_lasti] &&
             !(SKY_CODE_OPCODE_WITH_EXIT == bytecode[frame_data->f_lasti] &&
               block && SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP == block->type)))
        {
            traceback = sky_traceback_create(current_error->traceback, frame);
            sky_object_gc_setroot(SKY_AS_OBJECTP(&(current_error->traceback)),
                                  traceback,
                                  SKY_FALSE);
            if (-1 == frame_data->f_lasti ||
                (SKY_CODE_OPCODE_RAISE == bytecode[frame_data->f_lasti] &&
                 bytecode[frame_data->f_lasti + 1] > 0))
            {
                tracing = sky_frame_evaluate_trace(
                                frame,
                                SKY_FRAME_EVALUATE_TRACE_EXCEPTION,
                                sky_tuple_pack(3, current_error->type,
                                                  current_error->value,
                                                  current_error->traceback));
            }
        }
        if (!block) {
            /* This frame is exiting. Generate a "return" trace with no return
             * value to indicate to the tracer that the function has exited.
             */
            sky_frame_evaluate_trace(frame,
                                     SKY_FRAME_EVALUATE_TRACE_RETURN,
                                     sky_None);
            sky_error_raise();
        }
        block->error = current_error;
        sky_error_record_retain(block->error);

        /* Mark the error block as handled and pop it. From this point onward,
         * the only valid way out of here is to goto restart.
         */
        error_block.block_type = SKY_ERROR_BLOCK_TYPE_NONE;
        sky_error_block_pop();
        goto restart;
    }

    pc = bytecode + frame_data->pc;
    sp = frame_data->sp;

    if (first_pass) {
        first_pass = SKY_FALSE;

        /* The trace for "call" has to be done after the error handler is set
         * up but only for the first time. Don't do it again after an exception
         * has been handled.
         */
        tracing = sky_frame_evaluate_trace(frame,
                                           SKY_FRAME_EVALUATE_TRACE_CALL,
                                           sky_None);

        if (code_data->co_flags & SKY_CODE_FLAG_GENERATOR) {
            if (arg) {
                if (-1 != frame_data->f_lasti) {
                    PUSH(arg);
                }
                else if (!sky_object_isnull(arg)) {
                    sky_error_raise_string(
                            sky_TypeError,
                            "can't send non-None value to a just-started generator");
                }
            }
            if (error) {
                sky_error_raise_record(error);
            }
        }
    }
    else if ((block = frame_data->blocks) != NULL && block->error) {
        switch (block->type) {
            case SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT:
            case SKY_FRAME_BLOCK_TYPE_TRY_FINALLY:
                frame_data->pc = block->jump_offset;
                frame_data->sp = block->sp;
                break;

            case SKY_FRAME_BLOCK_TYPE_WITH:
                sky_frame_evaluate_with_exit(block);
                if (block->error) {
                    RERAISE();
                }
                frame_data->pc = block->jump_offset;
                frame_data->sp = block->sp;
                POP_BLOCK();
                break;

            case SKY_FRAME_BLOCK_TYPE_EXCEPT:
            case SKY_FRAME_BLOCK_TYPE_FINALLY:
            case SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP:
                RERAISE();
        }
        pc = bytecode + frame_data->pc;
        sp = frame_data->sp;
    }

evaluate_loop:
    for (;;) {
        sky_error_validate_debug(pc < bytecode_end);
        frame_data->f_lasti = pc - bytecode;

        if (tracing) {
            sky_frame_evaluate_trace_line(frame,
                                          &instr_lb,
                                          &instr_ub,
                                          &instr_prev,
                                          lnotab,
                                          lnotab_end);
        }

        opcode = (sky_code_opcode_t)*pc++;
        switch (opcode) {
            HANDLE_OPCODE(NOP)
                DISPATCH();

            HANDLE_OPCODE(PUSH_CONST)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                value = sky_tuple_get(code_data->co_consts, argv[0]);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_LOCAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                if (frame_data->nlocals >= 0) {
                    value = frame_data->locals[argv[0]];
                }
                else {
                    name = sky_tuple_get(code_data->co_varnames, argv[0]);
                    value = sky_frame_local_get(frame_data, name);
                }
                if (!value) {
                    if (frame_data->nlocals >= 0) {
                        name = sky_tuple_get(code_data->co_varnames, argv[0]);
                    }
                    sky_error_raise_format(
                            sky_UnboundLocalError,
                            "local variable %#@ referenced before assignment",
                            name);
                }
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_GLOBAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                name = sky_tuple_get(code_data->co_names, argv[0]);
                value = sky_dict_get(frame_data->f_globals, name, NULL);
                if (!value) {
                    sky_error_raise_format(
                            sky_NameError,
                            "global name %#@ is not defined",
                            name);
                }
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_NAME)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                name = sky_tuple_get(code_data->co_names, argv[0]);
                value = sky_frame_lookup_name(frame_data, name);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_FREE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                cell = frame_data->freevars[argv[0]];
                value = sky_cell_contents(cell);
                if (!value) {
                    name = sky_tuple_get(code_data->co_freevars, argv[0]);
                    sky_error_raise_format(
                            sky_NameError,
                            "free variable %#@ referenced before assignment "
                            "in enclosing scope",
                            name);
                }
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_CELL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                cell = frame_data->cellvars[argv[0]];
                value = sky_cell_contents(cell);
                if (!value) {
                    name = sky_tuple_get(code_data->co_cellvars, argv[0]);
                    sky_error_raise_format(
                            sky_NameError,
                            "local variable %#@ referenced before assignment",
                            name);
                }
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_ATTRIBUTE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                name = sky_tuple_get(code_data->co_names, argv[0]);
                POP(&object);
                value = sky_object_getattr(object, name, sky_NotSpecified);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_ITEM)
                POP_N(2);
                value = sky_object_getitem(sp[0], sp[1]);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_ITERATOR)
                POP(&object);
                value = sky_object_iter(object);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(PUSH_ITERATOR_NEXT)
                DECODE_OPCODE_OFFSET(&offset);
                CHECK_STACK(1);
                value = sky_object_next(sp[-1], NULL);
                if (value) {
                    PUSH(value);
                }
                else {
                    PUSH(sky_None);
                    VERIFY_CONDITIONAL_JUMP_SAFETY(offset);
                    JUMP(offset);
                }
                DISPATCH();

            HANDLE_OPCODE(PUSH_TOP)
                CHECK_STACK(1);
                PUSH(sp[-1]);
                DISPATCH();

            HANDLE_OPCODE(PUSH_CLOSURE_CELL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                PUSH(frame_data->cellvars[argv[0]]);
                DISPATCH();

            HANDLE_OPCODE(PUSH_CLOSURE_FREE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                PUSH(frame_data->freevars[argv[0]]);
                DISPATCH();

            HANDLE_OPCODE(POP)
                POP(&value);
                DISPATCH();

            HANDLE_OPCODE(POP_LOCAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&value);
                if (frame_data->nlocals >= 0) {
                    sky_object_gc_set(&(frame_data->locals[argv[0]]),
                                      value,
                                      frame);
                }
                else {
                    name = sky_tuple_get(code_data->co_varnames, argv[0]);
                    sky_object_setitem(frame_data->f_locals, name, value);
                }
                DISPATCH();

            HANDLE_OPCODE(POP_GLOBAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&value);
                name = sky_tuple_get(code_data->co_names, argv[0]);
                sky_dict_setitem(frame_data->f_globals, name, value);
                DISPATCH();

            HANDLE_OPCODE(POP_NAME)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&value);
                name = sky_tuple_get(code_data->co_names, argv[0]);
                sky_error_validate_debug(frame_data->f_locals);
                sky_object_setitem(frame_data->f_locals, name, value);
                DISPATCH();

            HANDLE_OPCODE(POP_FREE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&value);
                cell = frame_data->freevars[argv[0]];
                sky_cell_setcontents(cell, value);
                DISPATCH();

            HANDLE_OPCODE(POP_CELL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&value);
                cell = frame_data->cellvars[argv[0]];
                sky_cell_setcontents(cell, value);
                DISPATCH();

            HANDLE_OPCODE(POP_ATTRIBUTE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP_N(2);
                name = sky_tuple_get(code_data->co_names, argv[0]);
                sky_object_setattr(sp[1], name, sp[0]);
                DISPATCH();

            HANDLE_OPCODE(POP_ITEM)
                POP_N(3);
                sky_object_setitem(sp[1], sp[2], sp[0]);
                DISPATCH();

            HANDLE_OPCODE(DELETE_LOCAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                if (frame_data->nlocals >= 0) {
                    if (frame_data->locals[argv[0]]) {
                        frame_data->locals[argv[0]] = NULL;
                        DISPATCH();
                    }
                    name = sky_tuple_get(code_data->co_varnames, argv[0]);
                }
                else {
                    name = sky_tuple_get(code_data->co_varnames, argv[0]);
                    if (sky_frame_local_del(frame_data, name)) {
                        DISPATCH();
                    }
                }
                sky_error_raise_format(
                        sky_UnboundLocalError,
                        "local variable %#@ referenced before assignment",
                        name);
                DISPATCH();

            HANDLE_OPCODE(DELETE_GLOBAL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                name = sky_tuple_get(code_data->co_names, argv[0]);
                if (!sky_dict_delete(frame_data->f_globals, name)) {
                    sky_error_raise_format(
                            sky_NameError,
                            "global name %#@ is not defined",
                            name);
                }
                DISPATCH();

            HANDLE_OPCODE(DELETE_NAME)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                sky_error_validate_debug(frame_data->f_locals);
                name = sky_tuple_get(code_data->co_names, argv[0]);
                if (!sky_frame_local_del(frame_data, name)) {
                    sky_error_raise_format(
                            sky_UnboundLocalError,
                            "local variable %#@ referenced before assignment",
                            name);
                }
                DISPATCH();

            HANDLE_OPCODE(DELETE_FREE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                cell = frame_data->freevars[argv[0]];
                if (!sky_cell_contents(cell)) {
                    name = sky_tuple_get(code_data->co_freevars, argv[0]);
                    sky_error_raise_format(
                            sky_UnboundLocalError,
                            "local variable %#@ referenced before assignment",
                            name);
                }
                sky_cell_setcontents(cell, NULL);
                DISPATCH();

            HANDLE_OPCODE(DELETE_CELL)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                cell = frame_data->cellvars[argv[0]];
                if (!sky_cell_contents(cell)) {
                    name = sky_tuple_get(code_data->co_cellvars, argv[0]);
                    sky_error_raise_format(
                            sky_UnboundLocalError,
                            "local variable %#@ referenced before assignment",
                            name);
                }
                sky_cell_setcontents(cell, NULL);
                DISPATCH();

            HANDLE_OPCODE(DELETE_ATTRIBUTE)
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                POP(&object);
                name = sky_tuple_get(code_data->co_names, argv[0]);
                sky_object_delattr(object, name);
                DISPATCH();

            HANDLE_OPCODE(DELETE_ITEM)
                POP_N(2);
                sky_object_delitem(sp[0], sp[1]);
                DISPATCH();

            HANDLE_OPCODE(BINARY_ADD)
                CHECK_STACK(2);
                value = sky_number_add(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_SUB)
                CHECK_STACK(2);
                value = sky_number_subtract(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_MUL)
                CHECK_STACK(2);
                value = sky_number_multiply(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_DIV)
                CHECK_STACK(2);
                value = sky_number_truedivide(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_MOD)
                CHECK_STACK(2);
                value = sky_number_modulo(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_POW)
                CHECK_STACK(2);
                value = sky_number_pow(sp[-2], sp[-1], NULL);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_LSHIFT)
                CHECK_STACK(2);
                value = sky_number_lshift(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_RSHIFT)
                CHECK_STACK(2);
                value = sky_number_rshift(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_OR)
                CHECK_STACK(2);
                value = sky_number_or(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_XOR)
                CHECK_STACK(2);
                value = sky_number_xor(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_AND)
                CHECK_STACK(2);
                value = sky_number_and(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(BINARY_FLOORDIV)
                CHECK_STACK(2);
                value = sky_number_floordivide(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_ADD)
                CHECK_STACK(2);
                value = sky_number_inplace_add(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_SUB)
                CHECK_STACK(2);
                value = sky_number_inplace_subtract(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_MUL)
                CHECK_STACK(2);
                value = sky_number_inplace_multiply(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_DIV)
                CHECK_STACK(2);
                value = sky_number_inplace_truedivide(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_MOD)
                CHECK_STACK(2);
                value = sky_number_inplace_modulo(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_POW)
                CHECK_STACK(2);
                value = sky_number_inplace_pow(sp[-2], sp[-1], NULL);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_LSHIFT)
                CHECK_STACK(2);
                value = sky_number_inplace_lshift(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_RSHIFT)
                CHECK_STACK(2);
                value = sky_number_inplace_rshift(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_OR)
                CHECK_STACK(2);
                value = sky_number_inplace_or(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_XOR)
                CHECK_STACK(2);
                value = sky_number_inplace_xor(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_AND)
                CHECK_STACK(2);
                value = sky_number_inplace_and(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(INPLACE_FLOORDIV)
                CHECK_STACK(2);
                value = sky_number_inplace_floordivide(sp[-2], sp[-1]);
                --sp;
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(UNARY_NEGATIVE)
                CHECK_STACK(1);
                value = sky_number_negative(sp[-1]);
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(UNARY_POSITIVE)
                CHECK_STACK(1);
                value = sky_number_positive(value);
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(UNARY_INVERT)
                CHECK_STACK(1);
                value = sky_number_invert(value);
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(UNARY_NOT)
                CHECK_STACK(1);
                value = (sky_object_bool(sp[-1]) ? sky_False : sky_True);
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(DISPLAY)
                POP(&value);
                object = sky_object_getattr(
                                sky_interpreter_module_sys(),
                                SKY_STRING_LITERAL("displayhook"),
                                sky_NotSpecified);
                sky_object_call(object, sky_tuple_pack(1, value), NULL);
                DISPATCH();

            HANDLE_OPCODE(COMPARE_EXCEPTION)
                POP_N(2);
                if (sky_object_isa(sp[1], sky_tuple_type)) {
                    value = sky_False;
                    SKY_SEQUENCE_FOREACH(sp[1], object) {
                        if (sky_object_isa(sp[0], object)) {
                            value = sky_True;
                            SKY_SEQUENCE_FOREACH_BREAK;
                        }
                    } SKY_SEQUENCE_FOREACH_END;
                }
                else if (sky_object_isa(sp[0], sp[1])) {
                    value = sky_True;
                }
                else {
                    value = sky_False;
                }
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(COMPARE)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* compare_op */
                compare_op = argv[0];
                CHECK_STACK(2);
                --sp;
                if (sky_object_compare(sp[-1], sp[0], compare_op)) {
                    sp[-1] = sky_True;
                }
                else {
                    sp[-1] = sky_False;
                }
                DISPATCH();

            HANDLE_OPCODE(UNPACK_SEQUENCE)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                POP(&value);
                sky_error_validate_debug(argv[0] <= SSIZE_MAX);

                frame_data->sp = sp;
                sp = sky_frame_evaluate_unpack_sequence(frame, argv[0], value);
                DISPATCH();

            HANDLE_OPCODE(UNPACK_STARRED)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* before */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* after */
                POP(&value);
                sky_error_validate_debug(argv[0] <= SSIZE_MAX);
                sky_error_validate_debug((ssize_t)argv[0] >= 0);
                sky_error_validate_debug(argv[1] <= SSIZE_MAX);
                sky_error_validate_debug((ssize_t)argv[1] >= 0);
                sky_error_validate_debug(SSIZE_MAX - argv[0] >= argv[1]);
                sky_error_validate_debug(SSIZE_MAX - argv[1] >= argv[0]);
                sky_error_validate_debug(sky_object_len(value) >=
                                         (ssize_t)(argv[0] + argv[1]));

                count = (argv[0] + argv[1]) + 1;
                CHECK_STACK_SPACE(count);
                sp += count;
                i = 0;
                len = sky_object_len(value);

                for (index = 0; index < argv[0]; ++index) {
                    sky_object_gc_set(&(sp[--i]),
                                      sky_sequence_get(value, index),
                                      frame);
                }
                slice = sky_slice_create(
                                sky_integer_create(argv[0]),
                                sky_integer_create(len - argv[0] - argv[1] + 1),
                                sky_integer_one);
                sky_object_gc_set(&(sp[--i]),
                                  sky_object_getitem(value, slice),
                                  frame);
                offset = (len - argv[1]);
                for (index = 0; index < argv[1]; ++index) {
                    sky_object_gc_set(&(sp[--i]),
                                      sky_sequence_get(value, index + offset),
                                      frame);
                }
                DISPATCH();

            HANDLE_OPCODE(LIST_APPEND)
                POP_N(2);
                sky_list_append(sp[0], sp[1]);
                DISPATCH();

            HANDLE_OPCODE(SET_ADD)
                POP_N(2);
                sky_set_add(sp[0], sp[1]);
                DISPATCH();

            HANDLE_OPCODE(IMPORT_MODULE)
                POP_N(3);
                PUSH(sky_frame_evaluate_import_module(frame_data,
                                                      sp[0],
                                                      sp[1],
                                                      sp[2]));
                DISPATCH();

            HANDLE_OPCODE(IMPORT_STAR)
                CHECK_STACK(1); /* module */
                sky_frame_evaluate_import_star(frame_data, sp[-1]);
                DISPATCH();

            HANDLE_OPCODE(IMPORT_NAME)
                CHECK_STACK(1); /* module */
                DECODE_OPCODE_ARGUMENT(&(argv[0]));
                name = sky_tuple_get(code_data->co_names, argv[0]);
                value = sky_frame_evaluate_import_name(frame_data,
                                                       sp[-1],
                                                       name);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_CLASS)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* flags */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* nbases */
                DECODE_OPCODE_ARGUMENT(&(argv[2])); /* nkeywords */
                sky_error_validate_debug(argv[1] <= SSIZE_MAX);
                sky_error_validate_debug(argv[2] <= SSIZE_MAX / 2);

                /* stack is:
                 *      qualname
                 *      code
                 *      [bases]
                 *      [*args]
                 *      [keywords]
                 *      [*kwargs]
                 */
                count = (2 + argv[1] + (2 * argv[2]));
                if (argv[0] & SKY_CODE_BUILD_CLASS_FLAG_ARGS) {
                    ++count;
                }
                if (argv[0] & SKY_CODE_BUILD_CLASS_FLAG_KWS) {
                    ++count;
                }
                if (argv[0] & SKY_CODE_BUILD_CLASS_FLAG_CLOSURE) {
                    ++count;
                }
                POP_N(count);
                value = sky_frame_evaluate_build_class(frame_data,
                                                       argv[0],
                                                       argv[1],
                                                       argv[2],
                                                       sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_DICT)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                POP_N(argv[0] * 2);
                value = sky_dict_createfromarray(argv[0] * 2, sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_FUNCTION)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* flags */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* ndefaults */
                DECODE_OPCODE_ARGUMENT(&(argv[2])); /* nkwdefaults */
                sky_error_validate_debug(argv[1] <= SSIZE_MAX);
                sky_error_validate_debug(argv[2] <= SSIZE_MAX / 2);

                /* stack is:
                 *      qualname
                 *      code
                 *      [annotations]
                 *      [closure]
                 *      [defaults]
                 *      [kwdefaults key, value]
                 */
                count = (2 + argv[1] + (2 * argv[2]));
                if (argv[0] & SKY_CODE_BUILD_FUNCTION_FLAG_ANNOTATIONS) {
                    ++count;
                }
                if (argv[0] & SKY_CODE_BUILD_FUNCTION_FLAG_CLOSURE) {
                    ++count;
                }
                POP_N(count);
                value = sky_frame_evaluate_build_function(frame_data,
                                                          argv[0],
                                                          argv[1],
                                                          argv[2],
                                                          sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_LIST)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                POP_N(argv[0]);
                value = sky_list_createfromarray(argv[0], sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_SET)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                POP_N(argv[0]);
                value = sky_set_createfromarray(argv[0], sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_SLICE)
                POP_N(3);
                value = sky_slice_create(sp[0], sp[1], sp[2]);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(BUILD_TUPLE)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                POP_N(argv[0]);
                value = sky_tuple_createfromarray(argv[0], sp);
                PUSH(value);
                DISPATCH();

            HANDLE_OPCODE(ROTATE_TWO)
                CHECK_STACK(2);
                value = sp[-2];
                sky_object_gc_set(&(sp[-2]), sp[-1], frame);
                sky_object_gc_set(&(sp[-1]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(ROTATE_THREE)
                CHECK_STACK(3);
                value = sp[-3];
                sky_object_gc_set(&(sp[-3]), sp[-1], frame);
                sky_object_gc_set(&(sp[-1]), sp[-2], frame);
                sky_object_gc_set(&(sp[-2]), value, frame);
                DISPATCH();

            HANDLE_OPCODE(CALL)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* # positional */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* # keyword */
                count = argv[0] + (2 * argv[1]) + 1;
                CALL(SKY_FALSE, SKY_FALSE);
                DISPATCH();

            HANDLE_OPCODE(CALL_ARGS)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* # positional */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* # keyword */
                count = argv[0] + (2 * argv[1]) + 2;
                CALL(SKY_TRUE, SKY_FALSE);
                DISPATCH();

            HANDLE_OPCODE(CALL_KWS)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* # positional */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* # keyword */
                count = argv[0] + (2 * argv[1]) + 2;
                CALL(SKY_FALSE, SKY_TRUE);
                DISPATCH();

            HANDLE_OPCODE(CALL_ARGS_KWS)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* # positional */
                DECODE_OPCODE_ARGUMENT(&(argv[1])); /* # keyword */
                count = argv[0] + (2 * argv[1]) + 3;
                CALL(SKY_TRUE, SKY_TRUE);
                DISPATCH();

            HANDLE_OPCODE(RETURN)
                POP(&return_value);
                if (code_data->co_flags & SKY_CODE_FLAG_GENERATOR) {
                    sky_error_raise_object(sky_StopIteration, return_value);
                }
                goto exit_frame;

            HANDLE_OPCODE(YIELD)
                POP(&return_value);
                if (!(code_data->co_flags & SKY_CODE_FLAG_GENERATOR)) {
                    sky_error_raise_string(sky_SystemError,
                                           "yield outside of generator");
                }
                /* The value expected on the stack gets pushed near the entry
                 * to this function from the arg parameter, which is normally
                 * specified from a generator's send() method.
                 */
                goto exit_frame;

            HANDLE_OPCODE(YIELD_FROM)
                CHECK_STACK(2);
                value = *--sp;
                object = sp[-1];
                SYNC_FRAME();
                SKY_ERROR_TRY {
                    if (sky_object_isnull(value)) {
                        return_value = sky_object_next(object,
                                                       sky_NotSpecified);
                    }
                    else {
                        return_value =
                                sky_object_callmethod(
                                        object,
                                        SKY_STRING_LITERAL("send"),
                                        sky_tuple_pack(1, value),
                                        NULL);
                    }
                }
                SKY_ERROR_EXCEPT(sky_StopIteration) {
                    return_value = NULL;
                    value = sky_object_getattr(sky_error_current()->value,
                                               SKY_STRING_LITERAL("value"),
                                               sky_NotSpecified);
                    sky_object_gc_set(&(sp[-1]), value, frame);
                } SKY_ERROR_TRY_END;
                if (return_value) {
                    --pc;
                    goto exit_frame;
                }
                DISPATCH();

           HANDLE_OPCODE(RAISE)
                DECODE_OPCODE_ARGUMENT(&(argv[0])); /* count */
                if (1 == argv[0]) {
                    POP(&value);
                    sky_frame_evaluate_raise(frame, value, NULL);
                }
                if (2 == argv[0]) {
                    POP(&object);
                    POP(&value);
                    sky_frame_evaluate_raise(frame, value, object);
                }
                sky_error_validate_debug(!argv[0]);
                sky_frame_evaluate_raise(frame, NULL, NULL);
                DISPATCH();

            HANDLE_OPCODE(JUMP)
                DECODE_OPCODE_OFFSET(&offset);
                while ((block = frame_data->blocks) &&
                       (offset <= block->enter_offset ||
                        offset > block->exit_offset))
                {
                    switch (block->type) {
                        case SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT:
                        case SKY_FRAME_BLOCK_TYPE_EXCEPT:
                            break;
                        case SKY_FRAME_BLOCK_TYPE_TRY_FINALLY:
                            JUMP(block->jump_offset);
                            block->jump_offset = offset;
                            /* goto evaluate_loop (cannot be break!) */
                            DISPATCH();
                        case SKY_FRAME_BLOCK_TYPE_FINALLY:
                            return_value = NULL;
                            break;
                        case SKY_FRAME_BLOCK_TYPE_WITH:
                            sky_frame_evaluate_with_exit(block);
                            break;
                        case SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP:
                            sky_error_fatal("internal error");
                    }
                    POP_BLOCK();
                }
                JUMP(offset);
                DISPATCH();

            HANDLE_OPCODE(JUMP_IF_FALSE)
                DECODE_OPCODE_OFFSET(&offset);
                CHECK_STACK(1);
                if (sky_object_bool(sp[-1])) {
                    CHECK_IN();
                }
                else {
                    VERIFY_CONDITIONAL_JUMP_SAFETY(offset);
                    JUMP(offset);
                }
                DISPATCH();

            HANDLE_OPCODE(JUMP_IF_TRUE)
                DECODE_OPCODE_OFFSET(&offset);
                CHECK_STACK(1);
                if (!sky_object_bool(sp[-1])) {
                    CHECK_IN();
                }
                else {
                    VERIFY_CONDITIONAL_JUMP_SAFETY(offset);
                    JUMP(offset);
                }
                DISPATCH();

            HANDLE_OPCODE(WITH_ENTER)
                DECODE_OPCODE_OFFSET(&offset);
                DECODE_OPCODE_OFFSET(&exit_offset);
                POP(&object);

                SKY_ASSET_BLOCK_BEGIN {
                    block = sky_asset_calloc(1, sizeof(sky_frame_block_t),
                                             SKY_ASSET_CLEANUP_ON_ERROR);
                    block->next = frame_data->blocks;
                    block->type = SKY_FRAME_BLOCK_TYPE_WITH;
                    block->enter_offset = (pc - bytecode) - 5;
                    block->exit_offset = exit_offset;
                    block->jump_offset = offset;
                    block->sp = sp;
                    sky_object_gc_set(&(block->with_mgr), object, frame);
                    sky_object_gc_set(&(block->with_exit),
                                      SKY_OBJECT_METHOD_SLOT(object, EXIT),
                                      frame);
                    if (!block->with_exit) {
                        sky_error_raise_string(sky_AttributeError, "__exit__");
                    }
                    value = sky_frame_evaluate_with_enter(block->with_mgr);
                    PUSH(value);
                } SKY_ASSET_BLOCK_END;
                frame_data->blocks = block;
                DISPATCH();

            HANDLE_OPCODE(WITH_EXIT)
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);
                sky_error_validate_debug(NULL == block->error);
                sky_frame_evaluate_with_exit(block);
                POP_BLOCK();
                DISPATCH();

            HANDLE_OPCODE(TRY_EXCEPT)
                DECODE_OPCODE_OFFSET(&offset);
                DECODE_OPCODE_OFFSET(&exit_offset);
                block = sky_calloc(1, sizeof(sky_frame_block_t));
                block->next = frame_data->blocks;
                block->type = SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT;
                block->enter_offset = (pc - bytecode) - 5;
                block->exit_offset = exit_offset;
                block->jump_offset = offset;
                block->sp = sp;
                frame_data->blocks = block;
                DISPATCH();

            HANDLE_OPCODE(TRY_FINALLY)
                DECODE_OPCODE_OFFSET(&offset);
                DECODE_OPCODE_OFFSET(&exit_offset);
                block = sky_calloc(1, sizeof(sky_frame_block_t));
                block->next = frame_data->blocks;
                block->type = SKY_FRAME_BLOCK_TYPE_TRY_FINALLY;
                block->enter_offset = (pc - bytecode) - 5;
                block->exit_offset = exit_offset;
                block->jump_offset = offset;
                block->sp = sp;
                frame_data->blocks = block;
                DISPATCH();

            HANDLE_OPCODE(TRY_EXIT)
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);

                CHECK_IN();

                /* For TRY_FINALLY, run the FINALLY block. Treat this as a jump
                 * to the very next instruction.
                 */
                if (SKY_FRAME_BLOCK_TYPE_TRY_FINALLY == block->type) {
                    offset = pc - bytecode;
                    pc = bytecode + block->jump_offset;
                    block->jump_offset = offset;
                }
                else {
                    sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT == block->type);
                    POP_BLOCK();
                }
                DISPATCH();

            HANDLE_OPCODE(EXCEPT_ENTER)
                DECODE_OPCODE_OFFSET(&exit_offset);
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);
                sky_error_validate_debug(NULL != block->error);
                sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT == block->type);

                block->type = SKY_FRAME_BLOCK_TYPE_EXCEPT;
                block->enter_offset = (pc - bytecode) - 3;
                block->exit_offset = exit_offset;
                block->jump_offset = -1;

                PUSH(block->error->value);
                DISPATCH();

            HANDLE_OPCODE(EXCEPT_EXIT)
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);
                sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_EXCEPT == block->type);
                POP_BLOCK();
                DISPATCH();

            HANDLE_OPCODE(FINALLY_ENTER)
                DECODE_OPCODE_OFFSET(&exit_offset);
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);
                sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_TRY_FINALLY == block->type);

                block->type = SKY_FRAME_BLOCK_TYPE_FINALLY;
                block->enter_offset = (pc - bytecode) - 3;
                block->exit_offset = exit_offset;
                DISPATCH();

            HANDLE_OPCODE(FINALLY_EXIT)
                block = frame_data->blocks;
                sky_error_validate_debug(NULL != block);
                sky_error_validate_debug(SKY_FRAME_BLOCK_TYPE_FINALLY == block->type);
                if (block->error) {
                    RERAISE();
                }
                if (return_value) {
                    goto exit_frame;
                }
                if (block->jump_offset != -1) {
                    JUMP(block->jump_offset);
                }
                POP_BLOCK();
                DISPATCH();

#if defined(HAVE_GNUC_COMPUTED_GOTO)
            invalid_opcode:
#endif
            default:
                sky_error_raise_string(sky_SystemError, "unknown opcode");
        }
    }

exit_frame:
    if (!(code_data->co_flags & SKY_CODE_FLAG_GENERATOR)) {
        while ((block = frame_data->blocks) != NULL) {
            switch (block->type) {
                case SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT:
                case SKY_FRAME_BLOCK_TYPE_EXCEPT:
                case SKY_FRAME_BLOCK_TYPE_FINALLY:
                case SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP:
                    break;

                case SKY_FRAME_BLOCK_TYPE_TRY_FINALLY:
                    sky_error_validate_debug(NULL == block->error);
                    JUMP(block->jump_offset);
                    sp = block->sp;
                    goto evaluate_loop;

                case SKY_FRAME_BLOCK_TYPE_WITH:
                    sky_error_validate_debug(NULL == block->error);
                    sky_frame_evaluate_with_exit(block);
                    break;
            }
            POP_BLOCK();
        }
    }

    SYNC_FRAME();
    error_block.block_type = SKY_ERROR_BLOCK_TYPE_NONE;
    sky_error_block_pop();

    sky_error_validate_debug(NULL != return_value);
    sky_frame_evaluate_trace(frame,
                             SKY_FRAME_EVALUATE_TRACE_RETURN,
                             return_value);
    return return_value;
}


static void
sky_frame_evaluate_tlsdata_cleanup(sky_frame_t frame)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                          frame,
                          SKY_FALSE);
}


sky_object_t
sky_frame_evaluate(sky_frame_t          frame,
                   sky_object_t         arg,
                   sky_error_record_t * error)
{
    sky_tlsdata_t       *tlsdata = sky_tlsdata_get();
    sky_frame_data_t    *frame_data = sky_frame_data(frame);
    sky_code_data_t     *code_data = sky_code_data(frame_data->f_code);

    sky_buffer_t    code_buffer, lnotab_buffer;
    sky_object_t    return_value;

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(tlsdata->current_frame,
                       (sky_free_t)sky_frame_evaluate_tlsdata_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                              frame,
                              SKY_FALSE);
        sky_object_call_enter();

        sky_buffer_acquire(&code_buffer,
                           code_data->co_code,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&code_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (sky_object_isnull(code_data->co_lnotab)) {
            lnotab_buffer.buf = NULL;
            lnotab_buffer.len = 0;
        }
        else {
            sky_buffer_acquire(&lnotab_buffer,
                               code_data->co_lnotab,
                               SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&lnotab_buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }

        return_value = sky_frame_evaluate_bytecode(frame,
                                                   arg,
                                                   error,
                                                   code_buffer.buf,
                                                   code_buffer.len,
                                                   lnotab_buffer.buf,
                                                   lnotab_buffer.len);
    } SKY_ASSET_BLOCK_END;

    return return_value;
}
