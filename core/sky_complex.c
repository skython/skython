#include "sky_private.h"

#include <math.h>

#include <locale.h>
#if defined(HAVE_XLOCALE_H)
#   include <xlocale.h>
#endif


typedef struct sky_complex_data_s {
    double                              real;
    double                              imag;
} sky_complex_data_t;

SKY_EXTERN_INLINE sky_complex_data_t *
sky_complex_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_complex_type) {
        return (sky_complex_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_complex_type);
}


static const sky_complex_data_t sky_complex_one = { 1.0, 0.0 };


static void
sky_complex_divide(sky_complex_data_t *         result_data,
                   const sky_complex_data_t *   dividend_data,
                   const sky_complex_data_t *   divisor_data)
{
    double  abs_imag, abs_real, denom, ratio;

    abs_real = (divisor_data->real < 0 ? -divisor_data->real
                                       : divisor_data->real);
    abs_imag = (divisor_data->imag < 0 ? -divisor_data->imag
                                       : divisor_data->imag);
    if (abs_real >= abs_imag) {
        if (0.0 == abs_real) {
            sky_error_raise_string(sky_ZeroDivisionError,
                                   "complex division by zero");
        }
        ratio = divisor_data->imag / divisor_data->real;
        denom = divisor_data->real + divisor_data->imag * ratio;

        result_data->real =
                (dividend_data->real + dividend_data->imag * ratio) / denom;
        result_data->imag =
                (dividend_data->imag - dividend_data->real * ratio) / denom;
    }
    else {
        ratio = divisor_data->real / divisor_data->imag;
        denom = divisor_data->real * ratio + divisor_data->imag;

        result_data->real =
                (dividend_data->real * ratio + dividend_data->imag) / denom;
        result_data->imag =
            (dividend_data->imag * ratio - dividend_data->real) / denom;
    }
}


static void
sky_complex_multiply(sky_complex_data_t *       result_data,
                     const sky_complex_data_t * multicand_data,
                     const sky_complex_data_t * multiplier_data)
{
    result_data->real = (multicand_data->real * multiplier_data->real) -
                        (multicand_data->imag * multiplier_data->imag);
    result_data->imag = (multicand_data->real * multiplier_data->imag) +
                        (multicand_data->imag * multiplier_data->real);
}


static void
sky_complex_pow_complex(sky_complex_data_t *        result_data,
                        const sky_complex_data_t *  base_data,
                        const sky_complex_data_t *  exponent_data)
{
    if (0.0 == exponent_data->real && 0.0 == exponent_data->imag) {
        *result_data = sky_complex_one;
    }
    else if (0.0 == base_data->real && 0.0 == base_data->imag) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "0.0 to a negative or complex power");
    }
    else {
        double  at, len, phase, vabs;

        vabs = hypot(base_data->real, base_data->imag);
        len = pow(vabs, exponent_data->real);
        at = atan2(base_data->imag, base_data->real);
        phase = at * exponent_data->real;
        if (exponent_data->imag != 0.0) {
            len /= exp(at * exponent_data->imag);
            phase += exponent_data->imag * log(vabs);
        }
        result_data->real = len * cos(phase);
        result_data->imag = len * sin(phase);
    }
}


static void
sky_complex_pow_long_impl(sky_complex_data_t *      result_data,
                          const sky_complex_data_t *base_data,
                          long                      exponent)
{
    long                mask;
    sky_complex_data_t  temp_data;

    *result_data = sky_complex_one;
    temp_data = *base_data;
    for (mask = 1; mask > 0 && exponent >= mask; mask <<= 1) {
        if (exponent & mask) {
            sky_complex_multiply(result_data, result_data, &temp_data);
        }
        sky_complex_multiply(&temp_data, &temp_data, &temp_data);
    }
}


static void
sky_complex_pow_long(sky_complex_data_t *       result_data,
                     const sky_complex_data_t * base_data,
                     long                       exponent)
{
    sky_complex_data_t  temp_data;

    if (exponent > 100 || exponent < -100) {
        temp_data.real = (double)exponent;
        temp_data.imag = 0.0;
        sky_complex_pow_complex(result_data, base_data, &temp_data);
    }
    else if (exponent > 0) {
        sky_complex_pow_long_impl(result_data, base_data, exponent);
    }
    else {
        sky_complex_pow_long_impl(&temp_data, base_data, -exponent);
        sky_complex_divide(result_data, &sky_complex_one, &temp_data);
    }
}


sky_float_t
sky_complex_abs(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    double  result;

    if (!isfinite(self_data->real) || !isfinite(self_data->imag)) {
        if (isinf(self_data->real)) {
            return sky_float_create(fabs(self_data->real));
        }
        if (isinf(self_data->imag)) {
            return sky_float_create(fabs(self_data->imag));
        }
        return sky_float_create(HUGE_VAL * 0.0);
    }

    result = hypot(self_data->real, self_data->imag);
    if (!isfinite(result)) {
        sky_error_raise_string(sky_OverflowError, "absolute value too large");
    }
    return sky_float_create(result);
}


sky_object_t
sky_complex_add(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *self_data  = sky_complex_data(self);

    sky_complex_t       result;
    sky_complex_data_t  *other_data, *result_data;

    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }
    other_data = sky_complex_data(sky_number_complex(other));

    result = sky_object_allocate(sky_complex_type);
    result_data = sky_complex_data(result);
    result_data->real = self_data->real + other_data->real;
    result_data->imag = self_data->imag + other_data->imag;

    return result;
}


sky_bool_t
sky_complex_bool(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    return (0.0 == self_data->real &&
            0.0 == self_data->imag ? SKY_TRUE : SKY_FALSE);
}


sky_complex_t
sky_complex_clone(sky_complex_t self, sky_type_t type)
{
    sky_complex_t       clone;
    sky_complex_data_t  *clone_data, *self_data;

    if (sky_complex_type == sky_object_type(self) && sky_complex_type == type) {
        return self;
    }
    if (!sky_type_issubtype(type, sky_complex_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%@ is not a subtype of %@",
                               sky_type_name(type),
                               sky_type_name(sky_complex_type));
    }

    self_data = sky_complex_data(self);
    clone = sky_object_allocate(type);
    clone_data = sky_complex_data(clone);
    clone_data->real = self_data->real;
    clone_data->imag = self_data->imag;

    return clone;
}


sky_complex_t
sky_complex_complex(sky_complex_t self)
{
    return sky_complex_copy(self);
}


sky_complex_t
sky_complex_conjugate(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    return sky_complex_create(self_data->real, -self_data->imag);
}

static const char *sky_complex_conjugate_doc =
"complex.conjugate() -> complex\n\n"
"Return the complex conjugate of its argument. (3-4j).conjugate() == 3+4j.";


sky_complex_t
sky_complex_copy(sky_complex_t self)
{
    return sky_complex_clone(self, sky_complex_type);
}


sky_complex_t
sky_complex_create(double real, double imag)
{
    sky_complex_t       complex;
    sky_complex_data_t  *complex_data;

    complex = sky_object_allocate(sky_complex_type);
    complex_data = sky_complex_data(complex);
    complex_data->real = real;
    complex_data->imag = imag;

    return complex;
}


sky_complex_t
sky_complex_createfromascii(const char *bytes, size_t nbytes)
{
    char                *nextc;
    double              value;
    sky_bool_t          wrapped;
    const char          *c, *end;
    sky_complex_t       complex_object;
    sky_complex_data_t  complex_data;

    /* Skip leading whitespace. */
    end = bytes + nbytes;
    for (c = bytes; c < end && sky_ctype_isspace(*c); ++c);
    /* Handle repr()'s wrapper. */
    wrapped = SKY_FALSE;
    if (c < end && *c == '(') {
        wrapped = SKY_TRUE;
        ++c;
        while (c < end && sky_ctype_isspace(*c)) {
            ++c;
        }
    }

    /* Parse the meat. For compatibility with CPython, we will accept the
     * following forms:
     *
     *      <float>                 real part only
     *      <float>j                imaginary part only
     *      <float><signed-float>j  real and imaginary parts
     *      <float><sign>j          (compat.) real and imaginary parts
     *      <sign>j                 (compat.) imaginary part only (+/-1.0)
     *      j                       (compat.) same as <float>j,
     *                                        where <float> is 0
     */
    complex_data.real = complex_data.imag = 0.0;
    value = sky_util_strtod(c, end - c, &nextc);
    if (nextc > c) {
        c = nextc;
        if (*c == '-' || *c == '+') {
            complex_data.real = value;
            value = sky_util_strtod(c, end - c, &nextc);
            if (nextc > c) {
                c = nextc;
                complex_data.imag = value;
            }
            else {
                complex_data.imag = (*c == '-' ? -1.0 : 1.0);
                ++c;
            }
            if (*c != 'j' && *c != 'J') {
                goto parse_error;
            }
            ++c;
        }
        else if (*c == 'j' || *c == 'J') {
            complex_data.imag = value;
            ++c;
        }
        else {
            complex_data.real = value;
        }
    }
    else {
        if (*c == '-') {
            complex_data.imag = -1.0;
            ++c;
        }
        else if (*c == '+') {
            complex_data.imag = 1.0;
            ++c;
        }
        else {
            complex_data.imag = 1.0;
        }
        if (*c != 'j' && *c != 'J') {
            goto parse_error;
        }
        ++c;
    }

    while (c < end && sky_ctype_isspace(*c)) {
        ++c;
    }
    if (wrapped) {
        if (c >= end || *c != ')') {
            goto parse_error;
        }
        while (c < end && sky_ctype_isspace(*c)) {
            ++c;
        }
    }
    if (c != end) {
        goto parse_error;
    }

    complex_object = sky_object_allocate(sky_complex_type);
    *sky_complex_data(complex_object) = complex_data;
    return complex_object;

parse_error:
    sky_error_raise_string(sky_ValueError,
                           "cannot parse string as a complex number");
}


sky_complex_t
sky_complex_createfromstring(sky_object_t source)
{
    sky_complex_t   result;

    if (sky_object_isa(source, sky_string_type)) {
        const char  *c, *end;

        if (!(source = sky_string_transform_numeric(source))) {
            sky_error_raise_string(sky_ValueError,
                                   "cannot parse string as a complex number");
        }

        c = sky_string_cstring(source);
        end = c + sky_object_len(source);
        return sky_complex_createfromascii(c, end - c);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, source, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_complex_createfromascii(buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_tuple_t
sky_complex_divmod(SKY_UNUSED sky_complex_t self, SKY_UNUSED sky_object_t other)
{
    sky_error_raise_string(sky_TypeError,
                           "can't take floor or mod of complex number");
}


sky_object_t
sky_complex_eq(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *other_data, *self_data;

    if (self == other) {
        return sky_True;
    }
    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }

    other = sky_number_complex(other);
    other_data = sky_complex_data(other);
    self_data = sky_complex_data(self);

    return ((other_data->real == self_data->real &&
             other_data->imag == self_data->imag) ? sky_True : sky_False);
}


sky_complex_t
sky_complex_floordiv(SKY_UNUSED sky_complex_t   self,
                     SKY_UNUSED sky_object_t    other)
{
    sky_error_raise_string(sky_TypeError,
                           "can't take floor complex number");
}


static void
sky_complex_format_output(const void *  bytes,
                          ssize_t       nbytes,
               SKY_UNUSED size_t        width,
                          void *        arg)
{
    if (1 == nbytes) {
        **(char **)arg = *(char *)bytes;
        ++(*(char **)arg);
    }
    else if (nbytes > 0) {
        memcpy(*(void **)arg, bytes, nbytes);
        *(void **)arg = *(char **)arg + nbytes;
    }
}

sky_string_t
sky_complex_format(sky_complex_t self, sky_string_t format_spec)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    char                        *grouped_bytes, *imag_bytes, *real_bytes, type;
    size_t                      grouped_nbytes;
    ssize_t                     imag_length, imag_nbytes, left, length,
                                real_length, real_nbytes, right;
    const char                  *decimal_point, *grouping, *thousands_sep;
    sky_bool_t                  parentheses, skip_real;
    struct lconv                *lc;
    unsigned int                flags;
    sky_unicode_char_t          imag_sign, real_sign;
    sky_string_builder_t        builder;
    sky_format_specification_t  spec;

    if (!sky_string_parseformatspecification(format_spec, &spec)) {
        return sky_object_str(self);
    }

    if (spec.flags & SKY_FORMAT_FLAG_ZERO_PAD) {
        sky_error_raise_string(
                sky_ValueError,
                "Zero padding is not allowed in complex format specifier");
    }
    if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED) {
        sky_error_raise_string(
                sky_ValueError,
                "'=' alignment flag is not allowed in complex format specifier");
    }

    real_sign = imag_sign = '\0';
    parentheses = SKY_FALSE;
    skip_real = SKY_FALSE;
    flags = 0;
    if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
        flags |= SKY_UTIL_DTOA_ALTERNATE_FORM;
    }

    if (!spec.type) {
        if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
            type = 'r';
        }
        else {
            type = 'g';
        }
        if (0.0 == self_data->real && copysign(1.0, self_data->real) == 1.0) {
            skip_real = SKY_TRUE;
        }
        else {
            parentheses = SKY_TRUE;
        }
    }
    else {
        if (spec.type < 0x20 || spec.type >= 0x7F) {
            sky_error_raise_format(
                    sky_ValueError,
                    "Unknown format code '\\x%x' for object of type %#@",
                    (unsigned int)spec.type,
                    sky_type_name(sky_object_type(self)));
        }
        if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
            spec.precision = 6;
        }
        switch ((char)spec.type) {
            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
                type = (char)spec.type;
                break;
            case 'n':
                if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
                    sky_error_raise_string(sky_ValueError,
                                           "Cannot specify ',' with 'n'");
                }
                type = 'g';
                break;
            default:
                sky_error_raise_format(
                        sky_ValueError,
                        "Unknown format code '%c' for object of type %#@",
                        (char)spec.type,
                        sky_type_name(sky_object_type(self)));
        }
    }

    decimal_point = grouping = thousands_sep = NULL;
    if ('n' == spec.type) {
#if defined(HAVE_LOCALECONV_L)
        lc = localeconv_l(uselocale(NULL));
#else
        lc = localeconv();
#endif
        sky_error_validate_debug(decimal_point != NULL);
        decimal_point = lc->decimal_point;
        thousands_sep = lc->thousands_sep;
        grouping = lc->grouping;
    }
    else if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
        decimal_point = ".";
        thousands_sep = ",";
        grouping = "\x03";
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (skip_real) {
            real_bytes = NULL;
            real_length = real_nbytes = 0;
        }
        else {
            real_bytes = sky_util_dtoa(self_data->real,
                                       type,
                                       spec.precision,
                                       flags);
            sky_asset_save(real_bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            if (*real_bytes == '-') {
                ++real_bytes;
                real_sign = '-';
            }
            real_nbytes = strlen(real_bytes);
            if ((decimal_point &&
                 (*decimal_point != '.' || decimal_point[1])) ||
                (thousands_sep && grouping))
            {
                grouped_nbytes = real_nbytes + strlen(decimal_point);
                if (thousands_sep && grouping) {
                    grouped_nbytes += (strlen(thousands_sep) * real_nbytes);
                }
                grouped_bytes = sky_asset_malloc(grouped_nbytes,
                                                 SKY_ASSET_CLEANUP_ALWAYS);
                real_nbytes = sky_format_number(sky_complex_format_output,
                                                &grouped_bytes,
                                                real_bytes,
                                                real_nbytes,
                                                0,
                                                decimal_point,
                                                thousands_sep,
                                                grouping);
                real_bytes = grouped_bytes;
            }
            real_length = real_nbytes;
            if (real_sign) {
                ++real_length;
            }
            else if (sky_format_sign(&spec) != SKY_FORMAT_FLAG_SIGN_MINUS) {
                ++real_length;
                if (sky_format_sign(&spec) == SKY_FORMAT_FLAG_SIGN_PLUS) {
                    real_sign = '+';
                }
                else {
                    real_sign = ' ';
                }
            }
        }

        imag_bytes = sky_util_dtoa(self_data->imag,
                                   type,
                                   spec.precision,
                                   flags);
        sky_asset_save(imag_bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        if (*imag_bytes == '-') {
            ++imag_bytes;
            imag_sign = '-';
        }
        imag_nbytes = strlen(imag_bytes);
        if ((decimal_point &&
             (*decimal_point != '.' || decimal_point[1])) ||
            (thousands_sep && grouping))
        {
            grouped_nbytes = imag_nbytes + strlen(decimal_point);
            if (thousands_sep && grouping) {
                grouped_nbytes += (strlen(thousands_sep) * imag_nbytes);
            }
            grouped_bytes = sky_asset_malloc(grouped_nbytes,
                                             SKY_ASSET_CLEANUP_ALWAYS);
            imag_nbytes = sky_format_number(sky_complex_format_output,
                                            &grouped_bytes,
                                            imag_bytes,
                                            imag_nbytes,
                                            0,
                                            decimal_point,
                                            thousands_sep,
                                            grouping);
            imag_bytes = grouped_bytes;
        }
        imag_length = real_nbytes;
        if (imag_sign) {
            ++imag_length;
        }
        else if (!skip_real) {
            ++imag_length;
            imag_sign = '+';
        }
        else if (sky_format_sign(&spec) != SKY_FORMAT_FLAG_SIGN_MINUS) {
            ++imag_length;
            if (sky_format_sign(&spec) == SKY_FORMAT_FLAG_SIGN_PLUS) {
                imag_sign = '+';
            }
            else {
                imag_sign = ' ';
            }
        }

        length = real_length + imag_length + 1;     /* +1 for 'j' */
        if (parentheses) {
            length += 2;
        }

        left = right = 0;
        if ((spec.flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH) &&
            length < spec.width)
        {
            switch (sky_format_alignment(&spec)) {
                case SKY_FORMAT_FLAG_ALIGN_LEFT:
                    break;
                case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                    left = spec.width - length;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_CENTER:
                    left = (spec.width - length) / 2;
                    break;
            }
            right = spec.width - (length + left);
        }

        builder = sky_string_builder_createwithcapacity(left + length + right);
        if (left > 0) {
            sky_string_builder_appendcodepoint(builder, spec.fill, left);
        }
        if (parentheses) {
            sky_string_builder_appendcodepoint(builder, '(', 1);
        }
        if (real_bytes) {
            if (real_sign) {
                sky_string_builder_appendcodepoint(builder, real_sign, 1);
            }
            sky_string_builder_appendcodepoints(builder,
                                                real_bytes,
                                                real_nbytes,
                                                1);
        }
        if (imag_sign) {
            sky_string_builder_appendcodepoint(builder, imag_sign, 1);
        }
        sky_string_builder_appendcodepoints(builder,
                                            imag_bytes,
                                            imag_nbytes,
                                            1);
        sky_string_builder_appendcodepoint(builder, 'j', 1);
        if (parentheses) {
            sky_string_builder_appendcodepoint(builder, ')', 1);
        }
        if (right > 0) {
            sky_string_builder_appendcodepoint(builder, spec.fill, right);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


sky_tuple_t
sky_complex_getnewargs(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    return sky_object_build("(dd)", self_data->real, self_data->imag);
}


uintptr_t
sky_complex_hash(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    return sky_util_hashdouble(self_data->real) +
           (sky_util_hashdouble(self_data->imag) * 1000003);
}


double
sky_complex_imag(sky_complex_t self)
{
    return sky_complex_data(self)->imag;
}


sky_complex_t
sky_complex_mod(SKY_UNUSED sky_complex_t self, SKY_UNUSED sky_object_t other)
{
    sky_error_raise_string(sky_TypeError,
                           "can't mod complex numbers");
}


sky_object_t
sky_complex_mul(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    sky_complex_t       result;
    sky_complex_data_t  *other_data, *result_data;

    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }
    other_data = sky_complex_data(sky_number_complex(other));

    result = sky_object_allocate(sky_complex_type);
    result_data = sky_complex_data(result);
    sky_complex_multiply(result_data, self_data, other_data);

    return result;
}


sky_object_t
sky_complex_ne(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *other_data, *self_data;

    if (self == other) {
        return sky_False;
    }
    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }

    other = sky_number_complex(other);
    other_data = sky_complex_data(other);
    self_data = sky_complex_data(self);

    return ((other_data->real != self_data->real ||
             other_data->imag != self_data->imag) ? sky_True : sky_False);
}


sky_complex_t
sky_complex_neg(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    return sky_complex_create(-self_data->real, -self_data->imag);
}


sky_object_t
sky_complex_new(sky_type_t cls, sky_object_t real, sky_object_t imag)
{
    sky_complex_t       complex_object;
    sky_complex_data_t  *imag_data, imag_data_stub, *real_data, real_data_stub;

    if (sky_complex_type != cls) {
        if (!sky_type_issubtype(cls, sky_complex_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "%@.__new__(%@): %@ is not a subtype of %@",
                                   sky_type_name(sky_complex_type),
                                   sky_type_name(cls),
                                   sky_type_name(cls),
                                   sky_type_name(sky_complex_type));
        }
        complex_object = sky_complex_new(sky_complex_type, real, imag);
        return sky_complex_clone(complex_object, cls);
    }

    if (real && !imag &&
        sky_complex_type == sky_object_type(real) &&
        sky_complex_type == cls)
    {
        return real;
    }
    if (sky_object_isa(real, sky_string_type) || sky_buffer_check(real)) {
        if (imag) {
            sky_error_raise_string(
                    sky_TypeError,
                    "complex() can't take second arg if first is a string");
        }
        return sky_complex_createfromstring(real);
    }

    if (SKY_OBJECT_METHOD_SLOT(real, COMPLEX)) {
        real = sky_number_complex(real);
    }
    if (!sky_number_check(real) ||
        (imag && !sky_number_check(imag)))
    {
        sky_error_raise_string(
                sky_TypeError,
                "complex() argument must be a string or a number");
    }

    if (sky_object_isa(real, sky_complex_type)) {
        real_data = sky_complex_data(real);
    }
    else {
        real_data = &real_data_stub;
        real_data->real = sky_float_value(sky_number_float(real));
        real_data->imag = 0.0;
    }
    if (sky_object_isa(imag, sky_complex_type)) {
        imag_data = sky_complex_data(imag);
    }
    else {
        imag_data = &imag_data_stub;
        if (imag) {
            imag_data->real = sky_float_value(sky_number_float(imag));
        }
        else {
            imag_data->real = 0.0;
        }
        imag_data->imag = 0.0;
    }

    return sky_complex_create(real_data->real - imag_data->imag,
                              imag_data->real + real_data->imag);
}


sky_complex_t
sky_complex_pos(sky_complex_t self)
{
    return sky_complex_copy(self);
}


sky_object_t
sky_complex_pow(sky_complex_t self, sky_object_t other, sky_object_t third)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    sky_complex_t       result;
    sky_complex_data_t  *other_data, *result_data;

    if (!sky_object_isnull(third)) {
        sky_error_raise_string(sky_ValueError, "can't mod complex numbers");
    }
    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }
    other_data = sky_complex_data(sky_number_complex(other));

    result = sky_object_allocate(sky_complex_type);
    result_data = sky_complex_data(result);

    if (0.0 == other_data->imag && other_data->real == (long)other_data->real) {
        sky_complex_pow_long(result_data, self_data, (long)other_data->real);
    }
    else {
        sky_complex_pow_complex(result_data, self_data, other_data);
    }
    if (isinf(result_data->real) || isinf(result_data->imag)) {
        sky_error_raise_string(sky_OverflowError, "complex exponentiation");
    }

    return result;
}


double
sky_complex_real(sky_complex_t self)
{
    return sky_complex_data(self)->real;
}


sky_string_t
sky_complex_repr(sky_complex_t self)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    char            *imag, *real;
    sky_string_t    string;

    SKY_ASSET_BLOCK_BEGIN {
        if (0.0 == self_data->real && 1.0 == copysign(1.0, self_data->real)) {
            imag = sky_util_dtoa(self_data->imag, 'r', 0, 0);
            sky_asset_save(imag, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            string = sky_string_createfromformat("%sj", imag);
        }
        else {
            real = sky_util_dtoa(self_data->real, 'r', 0, 0);
            sky_asset_save(real, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
            imag = sky_util_dtoa(self_data->imag, 'r', 0,
                                 SKY_UTIL_DTOA_ALWAYS_SIGN);
            sky_asset_save(imag, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            string = sky_string_createfromformat("(%s%sj)", real, imag);
        }
    } SKY_ASSET_BLOCK_END;

    return string;
}


sky_object_t
sky_complex_sub(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    sky_complex_t       result;
    sky_complex_data_t  *other_data, *result_data;

    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }
    other_data = sky_complex_data(sky_number_complex(other));

    result = sky_object_allocate(sky_complex_type);
    result_data = sky_complex_data(result);
    result_data->real = self_data->real - other_data->real;
    result_data->imag = self_data->imag - other_data->imag;

    return result;
}


sky_object_t
sky_complex_truediv(sky_complex_t self, sky_object_t other)
{
    sky_complex_data_t  *self_data = sky_complex_data(self);

    sky_complex_t       result;
    sky_complex_data_t  *other_data, *result_data;

    if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {
        return sky_NotImplemented;
    }
    other_data = sky_complex_data(sky_number_complex(other));

    result = sky_object_allocate(sky_complex_type);
    result_data = sky_complex_data(result);
    sky_complex_divide(result_data, self_data, other_data);

    return result;
}


#define SKY_COMPLEX_REVERSE_BINARY(op)                              \
        sky_object_t                                                \
        sky_complex_r##op(sky_complex_t self, sky_object_t other)   \
        {                                                           \
            if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {          \
                return sky_NotImplemented;                          \
            }                                                       \
            other = sky_number_complex(other);                      \
            return sky_complex_##op(other, self);                   \
        }

#define SKY_COMPLEX_REVERSE_TERNARY(op)                             \
        sky_object_t                                                \
        sky_complex_r##op(sky_complex_t self, sky_object_t other)   \
        {                                                           \
            if (!SKY_OBJECT_METHOD_SLOT(other, COMPLEX)) {          \
                return sky_NotImplemented;                          \
            }                                                       \
            other = sky_number_complex(other);                      \
            return sky_complex_##op(other, self, sky_None);         \
        }

SKY_COMPLEX_REVERSE_BINARY(add)
SKY_COMPLEX_REVERSE_BINARY(divmod)
SKY_COMPLEX_REVERSE_BINARY(floordiv)
SKY_COMPLEX_REVERSE_BINARY(mod)
SKY_COMPLEX_REVERSE_BINARY(mul)
SKY_COMPLEX_REVERSE_TERNARY(pow)
SKY_COMPLEX_REVERSE_BINARY(sub)
SKY_COMPLEX_REVERSE_BINARY(truediv)


static const char sky_complex_type_doc[] =
"complex(real[, imag]) -> complex number\n"
"\n"
"Create a complex number from a real part and an optional imaginary part.\n"
"This is equivalent to (real + imag*1j) where imag defaults to 0.";


SKY_TYPE_DEFINE_SIMPLE(complex,
                       "complex",
                       sizeof(sky_complex_data_t),
                       NULL,
                       NULL,
                       NULL,
                       0,
                       sky_complex_type_doc);


void
sky_complex_initialize_library(void)
{
    sky_type_setmembers(sky_complex_type,
            "real", "the real part of a complex number",
                    offsetof(sky_complex_data_t, real),
                    SKY_DATA_TYPE_DOUBLE, SKY_MEMBER_FLAG_READONLY,
            "imag", "the imaginary part of a complex number",
                    offsetof(sky_complex_data_t, imag),
                    SKY_DATA_TYPE_DOUBLE, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(
            sky_complex_type,
            "conjugate",
            sky_function_createbuiltin(
                    "complex.conjugate",
                    sky_complex_conjugate_doc,
                    (sky_native_code_function_t)sky_complex_conjugate,
                    SKY_DATA_TYPE_OBJECT_COMPLEX,
                    "self", SKY_DATA_TYPE_OBJECT_COMPLEX, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_complex_type,
            "__new__",
            (sky_native_code_function_t)sky_complex_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "real", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
            "imag", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_complex_type,
            "__repr__", sky_complex_repr,
            "__format__", sky_complex_format,
            "__eq__", sky_complex_eq,
            "__ne__", sky_complex_ne,
            "__hash__", sky_complex_hash,
            "__bool__", sky_complex_bool,
            "__add__", sky_complex_add,
            "__sub__", sky_complex_sub,
            "__mul__", sky_complex_mul,
            "__truediv__", sky_complex_truediv,
            "__floordiv__", sky_complex_floordiv,
            "__mod__", sky_complex_mod,
            "__divmod__", sky_complex_divmod,
            "__pow__", sky_complex_pow,
            "__radd__", sky_complex_radd,
            "__rsub__", sky_complex_rsub,
            "__rmul__", sky_complex_rmul,
            "__rtruediv__", sky_complex_rtruediv,
            "__rfloordiv__", sky_complex_rfloordiv,
            "__rmod__", sky_complex_rmod,
            "__rdivmod__", sky_complex_rdivmod,
            "__rpow__", sky_complex_rpow,
            "__neg__", sky_complex_neg,
            "__pos__", sky_complex_pos,
            "__abs__", sky_complex_abs,
            "__complex__", sky_complex_complex,
            "__getnewargs__", sky_complex_getnewargs,
            NULL);
}
