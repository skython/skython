#include "sky_private.h"


sky_object_t
sky_boolean_and(sky_boolean_t self, sky_object_t other)
{
    if (!sky_object_isa(self, sky_boolean_type) ||
        !sky_object_isa(other, sky_boolean_type))
    {
        return sky_integer_and((sky_integer_t)self, other);
    }
    return (((self == sky_True) & (other == sky_True)) ? sky_True : sky_False);
}


sky_object_t
sky_boolean_new(SKY_UNUSED sky_type_t cls, sky_bool_t x)
{
    return (x ? sky_True : sky_False);
}


sky_object_t
sky_boolean_or(sky_boolean_t self, sky_object_t other)
{
    if (!sky_object_isa(self, sky_boolean_type) ||
        !sky_object_isa(other, sky_boolean_type))
    {
        return sky_integer_or((sky_integer_t)self, other);
    }
    return (((self == sky_True) | (other == sky_True)) ? sky_True : sky_False);
}


sky_string_t
sky_boolean_repr(sky_boolean_t self)
{
    if (sky_True == self) {
        return SKY_STRING_LITERAL("True");
    }
    if (sky_False == self) {
        return SKY_STRING_LITERAL("False");
    }
    sky_error_fatal("internal error");
}


sky_object_t
sky_boolean_xor(sky_boolean_t self, sky_object_t other)
{
    if (!sky_object_isa(self, sky_boolean_type) ||
        !sky_object_isa(other, sky_boolean_type))
    {
        return sky_integer_xor((sky_integer_t)self, other);
    }
    return (((self == sky_True) ^ (other == sky_True)) ? sky_True : sky_False);
}


static const char sky_boolean_type_doc[] =
"bool(x) -> bool\n"
"\n"
"Returns True when the argument x is true, False otherwise.\n"
"The builtins True and False are the only two instances of the class bool.\n"
"The class bool is a subclass of the class int, and cannot be subclassed.";

SKY_TYPE_DEFINE_SIMPLE(boolean,
                       "bool",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       sky_boolean_type_doc);


sky_boolean_t const sky_False = SKY_OBJECT_TAGGED_FALSE;
sky_boolean_t const sky_True = SKY_OBJECT_TAGGED_TRUE;


void
sky_boolean_initialize_library(void)
{
    sky_type_setmethodslotwithparameters(
            sky_boolean_type,
            "__new__",
            (sky_native_code_function_t)sky_boolean_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "x", SKY_DATA_TYPE_BOOL, sky_False,
            NULL);

    sky_type_setmethodslots(sky_boolean_type,
            "__repr__", sky_boolean_repr,
            "__and__", sky_boolean_and,
            "__xor__", sky_boolean_xor,
            "__or__", sky_boolean_or,
            "__rand__", sky_boolean_and,
            "__rxor__", sky_boolean_xor,
            "__ror__", sky_boolean_or,
            NULL);
}
