#include "sky_private.h"
#include "sky_string_private.h"


static sky_bool_t
sky_object_issubclass_recursive(sky_object_t object, sky_object_t cls)
{
    sky_bool_t      issubclass;
    sky_tuple_t     bases;
    sky_object_t    base;

    for (;;) {
        if (object == cls) {
            return SKY_TRUE;
        }
        bases = sky_object_getattr(object,
                                   SKY_STRING_LITERAL("__bases__"),
                                   NULL);
        if (!sky_object_isa(bases, sky_tuple_type)) {
            return SKY_FALSE;
        }
        if (sky_tuple_len(bases) == 1) {
            object = sky_tuple_get(bases, 0);
            continue;
        }
        issubclass = SKY_FALSE;
        SKY_SEQUENCE_FOREACH(bases, base) {
            issubclass = sky_object_issubclass_recursive(base, cls);
            if (issubclass) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
        return issubclass;
    }
}


sky_object_t
sky_object_allocate(sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_object_t                    methods, object;
    sky_object_data_t * volatile    object_data;
    sky_object_tlsdata_t * volatile tlsdata;

    /* This only happens once per thread, so it should be unlikely. */
    tlsdata = &(sky_tlsdata_get()->object_tlsdata);
    if (unlikely(!tlsdata->gc_registered)) {
        sky_object_gc_initialize_thread(tlsdata);
    }

    if (type_data->flags & SKY_TYPE_FLAG_ABSTRACT) {
        /* ", ".join(sorted(type.__abstractmethods__)) */
        methods = sky_list_create(
                        sky_object_getattr(
                                type,
                                SKY_STRING_LITERAL("__abstractmethods__"),
                                sky_NotSpecified));
        sky_list_sort(methods, NULL, SKY_FALSE);
        methods = sky_string_join(SKY_STRING_LITERAL(", "), methods);

        sky_error_raise_format(sky_TypeError,
                               "Can't instantiate abstract class %@ "
                               "with abstract methods %@",
                               sky_type_name(type),
                               methods);
    }

    object = type_data->allocate(type_data->instance_alignment,
                                 type_data->instance_size);
    object_data = SKY_OBJECT_DATA(object);

    sky_error_validate_debug(!object_data->u.type);
    sky_object_gc_set(SKY_AS_OBJECTP(&(object_data->u.type)), type, object);

    object_data->gc_epoch = tlsdata->gc_epoch;

#if defined(SKY_EPHEMERAL_OBJECTS)
    sky_object_list_prepend(&(tlsdata->gc_ephemeral_objects), object);
#else
    sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
    object_data->concrete = 1;
#endif

    SKY_ERROR_TRY {
        sky_object_initialize(object);
    }
    SKY_ERROR_EXCEPT_ANY {
#if defined(SKY_EPHEMERAL_OBJECTS)
        sky_object_t    o, prev = NULL;

        /* Remove the object from gc_ephemeral_objects. Yep, O(n), but this
         * should be extremely rare, and the object should be very near to the
         * head of the list, if not actually the head of the list.
         */
        for (o = tlsdata->gc_ephemeral_objects.head;
             o && o != object;
             o = SKY_OBJECT_DATA(o)->gc_link)
        {
            prev = o;
        }
        sky_error_validate(o != NULL);

        if (!prev) {
            tlsdata->gc_ephemeral_objects.head = SKY_OBJECT_DATA(o)->gc_link;
        }
        else if (!SKY_OBJECT_DATA(o)->gc_link) {
            SKY_OBJECT_DATA(prev)->gc_link = NULL;
            tlsdata->gc_ephemeral_objects.tail = prev;
        }
        else {
            SKY_OBJECT_DATA(prev)->gc_link = SKY_OBJECT_DATA(o)->gc_link;
        }
#endif

        sky_object_type(object)->type_data.deallocate(object);
        sky_error_raise();
    } SKY_ERROR_TRY_END;

    sky_object_gc_notenew(tlsdata, object);

    return object;
}


sky_string_t
sky_object_ascii(sky_object_t object)
{
    sky_bytes_t     bytes;
    sky_string_t    repr;

    repr = sky_object_repr(object);
    if (sky_string_cstring(repr)) {
        return repr;
    }

    bytes = sky_codec_encode(sky_codec_ascii,
                             repr,
                             NULL,
                             SKY_STRING_LITERAL("backslashreplace"),
                             0);

    return sky_bytes_decode(bytes, SKY_STRING_LITERAL("ASCII"), NULL);
}


void
sky_object_atfork_child(sky_object_tlsdata_t *tlsdata)
{
    sky_object_atfork_item_t    *item;

    sky_object_gc_atfork_child(tlsdata);
    if (tlsdata->atfork_table) {
        for (item = tlsdata->atfork_table->first; item; item = item->next) {
            if (item->child) {
                SKY_ERROR_TRY {
                    item->child(item->object);
                } SKY_ERROR_EXCEPT_ANY {
                    sky_error_display(sky_error_current());
                } SKY_ERROR_TRY_END;
            }
        }
    }
}


void
sky_object_atfork_parent(sky_object_tlsdata_t *tlsdata)
{
    sky_object_atfork_item_t    *item;

    if (tlsdata->atfork_table) {
        for (item = tlsdata->atfork_table->first; item; item = item->next) {
            if (item->parent) {
                SKY_ERROR_TRY {
                    item->parent(item->object);
                } SKY_ERROR_EXCEPT_ANY {
                    sky_error_display(sky_error_current());
                } SKY_ERROR_TRY_END;
            }
        }
    }
}


void
sky_object_atfork_prepare(sky_object_tlsdata_t *tlsdata)
{
    sky_object_atfork_item_t    *item;

    while (sky_tlsdata_adopt());

    if (tlsdata->atfork_table) {
        for (item = tlsdata->atfork_table->first; item; item = item->next) {
            if (item->prepare) {
                SKY_ERROR_TRY {
                    item->prepare(item->object);
                } SKY_ERROR_EXCEPT_ANY {
                    sky_error_display(sky_error_current());
                } SKY_ERROR_TRY_END;
            }
        }
    }
}


static sky_bool_t
sky_object_atfork_item_compare(sky_hashtable_item_t *item, void *key)
{
    return (((sky_object_atfork_item_t *)item)->object == key ? SKY_TRUE
                                                              : SKY_FALSE);
}

void
sky_object_atfork_register(sky_object_t                 object,
                           sky_object_atfork_prepare_t  prepare,
                           sky_object_atfork_parent_t   parent,
                           sky_object_atfork_child_t    child)
{
    sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

    sky_object_atfork_item_t    *new_item, *old_item;

    new_item = sky_malloc(sizeof(sky_object_atfork_item_t));
    new_item->object = object;
    new_item->prepare = prepare;
    new_item->parent = parent;
    new_item->child = child;

    if (!tlsdata->atfork_table) {
        tlsdata->atfork_table = sky_calloc(1, sizeof(sky_object_atfork_table_t));
        sky_hashtable_init(&(tlsdata->atfork_table->base),
                           sky_object_atfork_item_compare);
    }

    old_item = (sky_object_atfork_item_t *)
               sky_hashtable_insert(&(tlsdata->atfork_table->base),
                                    &(new_item->base),
                                    (uintptr_t)object >> 4,
                                    object);
    if (old_item != new_item) {
        sky_free(new_item);
        old_item->prepare = prepare;
        old_item->parent = parent;
        old_item->child = child;
    }
    else {
        new_item->next = NULL;
        if (!(new_item->prev = tlsdata->atfork_table->last)) {
            tlsdata->atfork_table->first = new_item;
        }
        else {
            tlsdata->atfork_table->last->next = new_item;
        }
        tlsdata->atfork_table->last = new_item;
    }
}


void
sky_object_atfork_unregister(sky_object_t object)
{
    sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

    sky_object_atfork_item_t    *item;

    if (!tlsdata->atfork_table) {
        return;
    }
    if (!(item = (sky_object_atfork_item_t *)
                 sky_hashtable_delete(&(tlsdata->atfork_table->base),
                                      (uintptr_t)object >> 4,
                                      object)))
    {
        return;
    }

    *(item->next ? &(item->next->prev)
                 : &(tlsdata->atfork_table->last)) = item->prev;
    *(item->prev ? &(item->prev->next)
                 : &(tlsdata->atfork_table->first)) = item->next;
    sky_free(item);
}


typedef sky_bool_t
        (*sky_object_bool_t)(sky_object_t object);

sky_bool_t
sky_object_bool(sky_object_t object)
{
    sky_bool_t          cresult;
    sky_object_t        method, result;
    sky_object_bool_t   cfunction;

    if (sky_False == object || sky_object_isnull(object)) {
        return SKY_FALSE;
    }
    if (sky_True == object) {
        return SKY_TRUE;
    }
    if (!(method = SKY_OBJECT_METHOD_SLOT(object, BOOL))) {
        /* if __len__() is defined, call it; otherwise, return SKY_TRUE. */
        if (SKY_OBJECT_METHOD_SLOT(object, LEN)) {
            return (sky_object_len(object) != 0 ? SKY_TRUE : SKY_FALSE);
        }
        return SKY_TRUE;
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_bool_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(cresult, cfunction, (object));
        return cresult;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    result = sky_object_call(method, NULL, NULL);
    if (sky_True == result) {
        return SKY_TRUE;
    }
    if (sky_False == result) {
        return SKY_FALSE;
    }
    sky_error_raise_string(sky_TypeError,
                           "__bool__ method should return a Boolean");
}


typedef sky_bytes_t
        (*sky_object_bytes_t)(sky_object_t object);

sky_bytes_t
sky_object_bytes(sky_object_t object)
{
    sky_object_t        method, result;
    sky_object_bytes_t  cfunction;

    if (sky_bytes_type == sky_object_type(object)) {
        return object;
    }
    if (!(method = SKY_OBJECT_METHOD_SLOT(object, BYTES))) {
        return sky_bytes_createfromobject(object);
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_bytes_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_bytes_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__bytes__ method should return a bytes object");
    }
    return result;
}


typedef sky_object_t
        (*sky_object_call_t)(sky_object_t, sky_tuple_t args, sky_dict_t kws);

sky_object_t
sky_object_call(sky_object_t object, sky_tuple_t args, sky_dict_t kws)
{
    sky_object_t        method, result;
    sky_object_call_t   cfunction;

    for (;;) {
        if (!(method = SKY_OBJECT_METHOD_SLOT(object, CALL))) {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ object is not callable",
                                   sky_type_name(sky_object_type(object)));
        }

        if (sky_object_isa(method, sky_native_code_type)) {
            cfunction = (sky_object_call_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE(result, cfunction, (object, args, kws));
            return (result ? result : sky_None);
        }

        object = sky_descriptor_get(method, object, sky_object_type(object));
        if (method == object) {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ object is not callable",
                                   sky_type_name(sky_object_type(object)));
        }
    }
}


sky_bool_t
sky_object_callable(sky_object_t object)
{
    return (SKY_OBJECT_METHOD_SLOT(object, CALL) != NULL);
}


sky_object_t
sky_object_callmethod(sky_object_t  object,
                      sky_string_t  name,
                      sky_tuple_t   args,
                      sky_dict_t    kws)
{
    return sky_object_call(sky_object_getattr(object, name, sky_NotSpecified),
                           args, kws);
}


void
sky_object_call_enter(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    ssize_t limit;

    limit = sky_interpreter_recursionlimit() + tlsdata->recursionlimit;
    if (tlsdata->recursiondepth >= limit) {
        SKY_ERROR_TRY {
            tlsdata->recursionlimit += 50;
            sky_error_raise_string(sky_RuntimeError,
                                   "maximum recursion depth exceeded");
        } SKY_ERROR_EXCEPT_ANY {
            tlsdata->recursionlimit -= 50;
            sky_error_raise();
        } SKY_ERROR_TRY_END;
    }
    ++tlsdata->recursiondepth;
    sky_asset_save(tlsdata, sky_object_call_leave, SKY_ASSET_CLEANUP_ALWAYS);
}


void
sky_object_call_leave(void *pointer)
{
    sky_tlsdata_t   *tlsdata = (pointer ? pointer : sky_tlsdata_get());

    sky_error_validate(--tlsdata->recursiondepth >= 0);
}


static sky_object_t
sky_object_compare_normal(sky_object_t      object,
                          sky_object_t      other,
                          sky_compare_op_t  compare_op)
{
    switch (compare_op) {
        case SKY_COMPARE_OP_NOT_EQUAL:
            return sky_object_ne(object, other);
        case SKY_COMPARE_OP_EQUAL:
            return sky_object_eq(object, other);
        case SKY_COMPARE_OP_GREATER:
            return sky_object_gt(object, other);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return sky_object_ge(object, other);
        case SKY_COMPARE_OP_LESS:
            return sky_object_lt(object, other);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return sky_object_le(object, other);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }
    sky_error_fatal("internal error");
}


static sky_object_t
sky_object_compare_swapped(sky_object_t     object,
                           sky_object_t     other,
                           sky_compare_op_t compare_op)
{
    switch (compare_op) {
        case SKY_COMPARE_OP_NOT_EQUAL:
            return sky_object_ne(other, object);
        case SKY_COMPARE_OP_EQUAL:
            return sky_object_eq(other, object);
        case SKY_COMPARE_OP_GREATER:
            return sky_object_lt(other, object);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return sky_object_le(other, object);
        case SKY_COMPARE_OP_LESS:
            return sky_object_gt(other, object);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return sky_object_ge(other, object);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }
    sky_error_fatal("internal error");
}


sky_bool_t
sky_object_compare(sky_object_t     object,
                   sky_object_t     other,
                   sky_compare_op_t compare_op)
{
    static const char *op_strings[] = { "!=", "==", ">", ">=", "<", "<=" };

    sky_bool_t      checked_reverse;
    sky_object_t    result;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
        case SKY_COMPARE_OP_NOT_EQUAL:
        case SKY_COMPARE_OP_LESS:
        case SKY_COMPARE_OP_GREATER:
            break;

        case SKY_COMPARE_OP_IS:
            return (object == other);
        case SKY_COMPARE_OP_IS_NOT:
            return (object != other);
        case SKY_COMPARE_OP_IN:
            return sky_object_contains(other, object);
        case SKY_COMPARE_OP_NOT_IN:
            return !sky_object_contains(other, object);
    }

    checked_reverse = SKY_FALSE;
    if (sky_object_type(object) != sky_object_type(other) &&
        sky_type_issubtype(sky_object_type(other), sky_object_type(object)))
    {
        checked_reverse = SKY_TRUE;
        result = sky_object_compare_swapped(object, other, compare_op);
        if (result != sky_NotImplemented) {
            return sky_object_bool(result);
        }
    }

    result = sky_object_compare_normal(object, other, compare_op);
    if (result != sky_NotImplemented) {
        return sky_object_bool(result);
    }

    if (!checked_reverse) {
        result = sky_object_compare_swapped(object, other, compare_op);
        if (result != sky_NotImplemented) {
            return sky_object_bool(result);
        }
    }

    /* If neither object implements the requested op, do a pointer equality
     * check for EQUAL and NOT_EQUAL, or raise a TypeError for everything
     * else.
     */
    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            return (object == other ? SKY_TRUE : SKY_FALSE);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (object != other ? SKY_TRUE : SKY_FALSE);
        default:
            sky_error_raise_format(sky_TypeError,
                                   "unorderable types: %@ %s %@",
                                   sky_type_name(sky_object_type(object)),
                                   op_strings[compare_op],
                                   sky_type_name(sky_object_type(other)));
    }
}


typedef sky_bool_t
        (*sky_object_contains_t)(sky_object_t, sky_object_t);

sky_bool_t
sky_object_contains(sky_object_t object, sky_object_t item)
{
    sky_bool_t              contains;
    sky_tuple_t             args;
    sky_object_t            iter, iter_item, method, result;
    sky_object_contains_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, CONTAINS))) {
        contains = SKY_FALSE;
        if (sky_sequence_isiterable(object)) {
            SKY_SEQUENCE_FOREACH(object, iter_item) {
                if (sky_object_compare(item, iter_item, SKY_COMPARE_OP_EQUAL)) {
                    contains = SKY_TRUE;
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(object);
            while ((iter_item = sky_object_next(iter, NULL)) != NULL) {
                if (sky_object_compare(item, iter_item, SKY_COMPARE_OP_EQUAL)) {
                    contains = SKY_TRUE;
                    break;
                }
            }
        }
        return contains;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_contains_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(contains, cfunction, (object, item));
        return contains;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    args = sky_tuple_pack(1, item);
    result = sky_object_call(method, args, NULL);
    if (sky_True == result) {
        return SKY_TRUE;
    }
    if (sky_False == result) {
        return SKY_FALSE;
    }
    sky_error_raise_string(sky_TypeError,
                           "__contains__ method should return a Boolean");
}


void *
sky_object_data(sky_object_t object, sky_type_t type)
{
    size_t          offset;
    ssize_t         i;
    sky_type_data_t *type_data;

    sky_error_validate_debug(object != NULL);

    if (!sky_object_istagged(object)) {
        type_data = sky_type_data(sky_object_type(object));
        for (i = 0; i < type_data->mro->len; ++i) {
            if (type == type_data->mro->items[i].type) {
                offset = type_data->data_offsets->items[i].offset;
                sky_error_validate_debug(offset != 0);
                return ((char *)object + offset);
            }
        }
    }

    sky_error_raise_format(sky_TypeError,
                           "expected %#@; got %#@",
                           sky_type_name(type),
                           sky_type_name(sky_object_type(object)));
}


typedef void
        (*sky_object_del_t)(sky_object_t);

void
sky_object_del(sky_object_t object)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    sky_object_t        method;
    sky_object_del_t    cfunction;

    if (object_data->deleted) {
        return;
    }
    object_data->deleted = 1;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, DEL))) {
        return;
    }

    SKY_ERROR_TRY {
        if (sky_object_isa(method, sky_native_code_type)) {
            cfunction = (sky_object_del_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object));
        }
        else {
            method = sky_descriptor_get(method,
                                        object,
                                        sky_object_type(object));
            sky_object_call(method, NULL, NULL);
        }
    }
    SKY_ERROR_EXCEPT_ANY {
        /* swallow __del__() exceptions, but print them to stderr */
        sky_error_display(sky_error_current());
    } SKY_ERROR_TRY_END;
}


typedef void
        (*sky_object_delattr_t)(sky_object_t, sky_string_t);

void
sky_object_delattr(sky_object_t object, sky_string_t name)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_object_delattr_t    cfunction;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    method = SKY_OBJECT_METHOD_SLOT(object, DELATTR);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_delattr_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object, name));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, name);
        sky_object_call(method, args, NULL);
    }
}


typedef void
        (*sky_object_delitem_t)(sky_object_t, sky_object_t);

void
sky_object_delitem(sky_object_t object, sky_object_t item)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_object_delitem_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, DELITEM))) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ object does not support item deletion",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_delitem_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object, item));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, item);
        sky_object_call(method, args, NULL);
    }
}


sky_dict_t
sky_object_dict(sky_object_t object)
{
    sky_dict_t      dict;
    sky_object_t    *dict_pointer;
    sky_type_data_t *type_data;

    if (sky_object_istagged(object)) {
        return NULL;
    }

    type_data = sky_type_data(sky_object_type(object));
    if (!(type_data->flags & SKY_TYPE_FLAG_HAS_DICT)) {
        return NULL;
    }
    sky_error_validate_debug(!sky_object_istagged(object));

    dict_pointer = (sky_object_t *)((char *)object + type_data->dict_offset);
    if (!(dict = *dict_pointer)) {
        dict = sky_dict_create();
        if (!sky_object_gc_isthreaded() && !sky_object_isglobal(object)) {
            sky_object_gc_set(dict_pointer, dict, object);
        }
        else if (!sky_object_gc_cas(dict_pointer, NULL, dict, object)) {
            dict = *dict_pointer;
            sky_error_validate_debug(NULL != dict);
        }
    }

    return dict;
}


void
sky_object_setdict(sky_object_t object, sky_dict_t dict)
{
    sky_type_data_t *type_data;

    if (sky_object_istagged(object)) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ objects cannot have dicts.",
                               sky_type_name(sky_object_type(object)));
    }

    type_data = sky_type_data(sky_object_type(object));
    if (!(type_data->flags & SKY_TYPE_FLAG_HAS_DICT)) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ objects cannot have dicts.",
                               sky_type_name(sky_object_type(object)));
    }
    if (type_data->flags & SKY_TYPE_FLAG_DICT_READONLY) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ object attribute '__dict__' is read-only",
                               sky_type_name(sky_object_type(object)));
    }
    sky_error_validate_debug(!sky_object_istagged(object));

    if (!dict) {
        sky_error_raise_string(sky_TypeError, "cannot delete __dict__");
    }
    if (!sky_object_isa(dict, sky_dict_type)) {
        sky_error_raise_format(sky_TypeError,
                               "__dict__ must be set to a dictionary, not %#@",
                               sky_type_name(sky_object_type(dict)));
    }

    sky_object_gc_set(
            (sky_object_t *)((char *)object + type_data->dict_offset),
            dict,
            object);
}


typedef sky_object_t
        (*sky_object_dir_t)(sky_object_t);

sky_list_t
sky_object_dir(sky_object_t object)
{
    sky_object_t        method, result;
    sky_object_dir_t    cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, DIR);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_dir_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }

    if (!sky_object_isa(result, sky_list_type)) {
        result = sky_list_create(result);
    }
    return result;
}


void
sky_object_dump(sky_object_t object)
{
    sky_dict_t          dict;
    sky_type_t          object_type;
    sky_type_data_t     *type_data;
    sky_object_data_t   *object_data;

    if (!object) {
        sky_format_fprintf(stdout, "NULL\n");
        return;
    }

    object_type = sky_object_type(object);
    type_data = sky_type_data(object_type);
    dict = NULL;

    sky_format_fprintf(stdout, "type:  %@\n",
                       sky_type_name(object_type));

    if (sky_object_istagged(object)) {
        sky_format_fprintf(stdout, "epoch: <current>\n");
        sky_format_fprintf(stdout, "flags: global concrete dead tagged\n");
    }
    else {
        if ((type_data->flags & SKY_TYPE_FLAG_HAS_DICT)) {
            dict = *(sky_dict_t *)((char *)object + type_data->dict_offset);
        }
        object_data = SKY_OBJECT_DATA(object);

        sky_format_fprintf(stdout, "epoch: %u\n",
                           (unsigned int)object_data->gc_epoch);
        sky_format_fprintf(stdout, "flags: %s%s%s\n",
                           (object_data->global ? "global " : ""),
                           (object_data->concrete ? "concrete " : ""),
                           (object_data->dead ? "dead " : ""));
    }
    sky_format_fprintf(stdout, "dict:  %@\n", dict);
    sky_format_fprintf(stdout, "repr:  %#@\n", object);
}


typedef sky_object_t
        (*sky_object_eq_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_eq(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_eq_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, EQ);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_eq_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


void
sky_object_finalize(sky_object_t object)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    void            *data;
    ssize_t         i, len;
    sky_type_data_t *type_data;
    sky_type_list_t *mro, *offsets;

    sky_error_validate_debug(object_data->dead != 0);
    sky_error_validate_debug(object_data->finalized == 0);

    type_data = sky_type_data(object_data->u.type);
    mro = type_data->mro;
    offsets = type_data->data_offsets;
    len = mro->len;

    for (i = 0; i < len; ++i) {
        type_data = sky_type_data(mro->items[i].type);
        data = (void *)((char *)object + offsets->items[i].offset);
        if (type_data->finalize) {
            SKY_ERROR_TRY {
                type_data->finalize(object, data);
            }
            SKY_ERROR_EXCEPT_ANY {
                sky_error_fatal("exception raised in object finalization");
            } SKY_ERROR_TRY_END;
            sky_error_validate_debug(object_data->dead);
        }
    }
}


typedef sky_string_t
        (*sky_object_format_t)(sky_object_t, sky_string_t);

sky_string_t
sky_object_format(sky_object_t object, sky_string_t format_spec)
{
    sky_object_t        args, method, result;
    sky_object_format_t cfunction;

    if (sky_object_isnull(format_spec)) {
        format_spec = sky_string_empty;
    }
    method = SKY_OBJECT_METHOD_SLOT(object, FORMAT);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_format_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object, format_spec));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, format_spec);
        result = sky_object_call(method, args, NULL);
    }
    if (!sky_object_isa(result, sky_string_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__format__ method did not return string");
    }
    return result;
}


typedef sky_object_t
        (*sky_object_ge_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_ge(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_ge_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, GE);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_ge_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


typedef sky_object_t
        (*sky_object_getattr_t)(sky_object_t, sky_object_t);

static sky_object_t
sky_object_getattribute(sky_object_t    object,
                        sky_object_t    method,
                        sky_string_t    name)
{
    sky_object_t            result;
    sky_object_getattr_t    getattr;

    if (sky_object_isa(method, sky_native_code_type)) {
        getattr = (sky_object_getattr_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, getattr, (object, name));
        return result;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    return sky_object_call(method, sky_tuple_pack(1, name), NULL);
}

sky_object_t
sky_object_getattr(sky_object_t object, sky_string_t name, sky_object_t value)
{
    sky_object_t volatile   attr, getattr, getattribute;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    getattribute = SKY_OBJECT_METHOD_SLOT(object, GETATTRIBUTE);
    getattr = SKY_OBJECT_METHOD_SLOT(object, GETATTR);

    SKY_ERROR_TRY {
        if (!(attr = sky_object_getattribute(object, getattribute, name))) {
            if (!getattr) {
                if (sky_NotSpecified == value) {
                    sky_error_raise_format(
                            sky_AttributeError,
                            "%#@ object has no attribute %#@",
                            sky_type_name(sky_object_type(object)),
                            name);
                }
                attr = value;
            }
        }
    }
    SKY_ERROR_EXCEPT(sky_AttributeError) {
        if (getattr) {
            attr = NULL;
        }
        else if (sky_NotSpecified == value) {
            sky_error_raise();
        }
        else {
            attr = value;
        }
    } SKY_ERROR_TRY_END;

    if (!attr && getattr) {
        SKY_ERROR_TRY {
            if (!(attr = sky_object_getattribute(object, getattr, name))) {
                if (sky_NotSpecified == value) {
                    sky_error_raise_format(
                            sky_AttributeError,
                            "%#@ object has no attribute %#@",
                            sky_type_name(sky_object_type(object)),
                            name);
                }
                attr = value;
            }
        }
        SKY_ERROR_EXCEPT(sky_AttributeError) {
            if (sky_NotSpecified == value) {
                sky_error_raise();
            }
            attr = value;
        } SKY_ERROR_TRY_END;
    }

    return attr;
}


typedef sky_object_t
        (*sky_object_getitem_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_getitem(sky_object_t object, sky_object_t item)
{
    sky_tuple_t             args;
    sky_object_t            method, result;
    sky_object_getitem_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, GETITEM))) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ object is not subscriptable",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_getitem_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object, item));
        if (!result) {
            sky_error_raise_object(sky_IndexError, item);
        }
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, item);
        result = sky_object_call(method, args, NULL);
    }
    return result;
}


typedef sky_tuple_t
        (*sky_object_getnewargs_t)(sky_object_t);

sky_tuple_t
sky_object_getnewargs(sky_object_t object)
{
    sky_object_t            method, result;
    sky_object_getnewargs_t cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, GETNEWARGS))) {
        return sky_tuple_empty;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_getnewargs_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_tuple_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "__getnewargs__ should return a tuple, not %#@",
                sky_type_name(sky_object_type(result)));
    }
    return result;
}


typedef sky_object_t
        (*sky_object_getstate_t)(sky_object_t);

sky_object_t
sky_object_getstate(sky_object_t object)
{
    sky_object_t            method, result;
    sky_object_getstate_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, GETSTATE))) {
        return NULL;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_getstate_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


typedef sky_object_t
        (*sky_object_gt_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_gt(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_gt_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, GT);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_gt_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


sky_bool_t
sky_object_hasattr(sky_object_t object, sky_string_t name)
{
    return (sky_object_getattr(object, name, NULL) ? SKY_TRUE : SKY_FALSE);
}


typedef uintptr_t
        (*sky_object_hash_t)(sky_object_t);

uintptr_t
sky_object_hash(sky_object_t object)
{
    uintptr_t           hash;
    sky_object_t        result, method;
    sky_object_hash_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, HASH)) ||
        sky_None == method)
    {
        sky_error_raise_format(sky_TypeError,
                               "unhashable type: %#@",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_hash_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(hash, cfunction, (object));
        return hash;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    result = sky_object_call(method, NULL, NULL);
    if (!sky_object_isa(result, sky_integer_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__hash__ method should return an integer");
    }

    SKY_ERROR_TRY {
        hash = sky_integer_value(result, 0, UINTPTR_MAX, NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        hash = sky_object_hash(result);
    } SKY_ERROR_TRY_END;

    return hash;
}


sky_bool_t
sky_object_hasrefs(sky_object_t object)
{
    sky_object_reference_t  *ref;

    for (ref = SKY_OBJECT_DATA(object)->refs; ref; ref = ref->next) {
        if (ref->ref) {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


void
sky_object_initialize(sky_object_t object)
{
    void                *data;
    sky_type_data_t     *base_type_data, *type_data;
    sky_type_list_t     * volatile  mro, * volatile offsets;
    volatile ssize_t    i, len;

    type_data = sky_type_data(sky_object_type(object));
    mro = type_data->mro;
    offsets = type_data->data_offsets;
    len = mro->len;

    for (i = len - 1; i >= 0; --i) {
        type_data = sky_type_data(mro->items[i].type);
        if (type_data->initialize) {
            SKY_ERROR_TRY {
                data = (void *)((char *)object + offsets->items[i].offset);
                type_data->initialize(object, data);
            }
            SKY_ERROR_EXCEPT_ANY {
                while (++i < len - 1) {
                    base_type_data = sky_type_data(mro->items[i].type);
                    if (base_type_data->finalize) {
                        SKY_ERROR_TRY {
                            data = (void *)((char *)object +
                                            offsets->items[i].offset);
                            base_type_data->finalize(object, data);
                        }
                        SKY_ERROR_EXCEPT_ANY {
                            sky_error_fatal("exception raised in object finalization");
                        } SKY_ERROR_TRY_END;
                    }
                }
                sky_error_raise();
            } SKY_ERROR_TRY_END;
        }
    }
}


sky_bool_t
sky_object_instancecheck(sky_object_t cls, sky_object_t instance)
{
    sky_tuple_t     bases;
    sky_object_t    instance_cls;

    bases = sky_object_getattr(cls, SKY_STRING_LITERAL("__bases__"), NULL);
    if (!sky_object_isa(bases, sky_tuple_type)) {
        sky_error_raise_format(sky_TypeError,
                               "type expected; got %#@",
                               sky_type_name(sky_object_type(cls)));
    }
    if (!(instance_cls = sky_object_getattr(instance,
                                            SKY_STRING_LITERAL("__class__"),
                                            NULL)))
    {
        return SKY_FALSE;
    }

    return sky_object_issubclass_recursive(instance_cls, cls);
}


sky_bool_t
sky_object_isa(sky_object_t object, sky_type_t type)
{
    ssize_t         i;
    sky_type_data_t *type_data;

    if (object) {
        if (sky_object_type(object) == type) {
            return SKY_TRUE;
        }
        type_data = sky_type_data(sky_object_type(object));
        for (i = 0; i < type_data->mro->len; ++i) {
            if (type == type_data->mro->items[i].type) {
                return SKY_TRUE;
            }
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_object_isglobal(sky_object_t object)
{
    if (sky_object_istaggedtuple(object)) {
        object = sky_object_taggedtupleobject(object);
    }
    if (sky_object_istagged(object)) {
        return SKY_TRUE;
    }
    return SKY_OBJECT_DATA(object)->global;
}


static sky_bool_t
sky_object_isinstance_worker(sky_object_t object, sky_object_t cls)
{
    typedef sky_bool_t (*instancecheck_t)(sky_object_t, sky_object_t);

    sky_bool_t      cresult;
    sky_object_t    method, result;
    instancecheck_t cfunction;

    if (sky_object_type(object) == cls) {
        return SKY_TRUE;
    }

    if (!(method = SKY_OBJECT_METHOD_SLOT(cls, INSTANCECHECK))) {
        return sky_object_instancecheck(cls, object);
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (instancecheck_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(cresult, cfunction, (cls, object));
        return cresult;
    }
    method = sky_descriptor_get(method, cls, sky_object_type(cls));
    result = sky_object_call(method, sky_tuple_pack(1, object), NULL);
    return sky_object_bool(result);
}


sky_bool_t
sky_object_isinstance(sky_object_t object, sky_object_t cls)
{
    sky_bool_t      isinstance;
    sky_object_t    o;

    if (sky_object_type(object) == cls) {
        return SKY_TRUE;
    }

    if (sky_object_isa(cls, sky_tuple_type)) {
        isinstance = SKY_FALSE;
        SKY_SEQUENCE_FOREACH(cls, o) {
            if (sky_object_isa(o, sky_tuple_type)) {
                isinstance = sky_object_isinstance(object, o);
            }
            else {
                isinstance = sky_object_isinstance_worker(object, o);
            }
            if (isinstance) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
        return isinstance;
    }

    return sky_object_isinstance_worker(object, cls);
}


static sky_bool_t
sky_object_issubclass_worker(sky_object_t object, sky_object_t cls)
{
    typedef sky_bool_t (*subclasscheck_t)(sky_object_t, sky_object_t);

    sky_bool_t      cresult;
    sky_object_t    method, result;
    subclasscheck_t cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(cls, SUBCLASSCHECK))) {
        return sky_object_subclasscheck(cls, object);
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (subclasscheck_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(cresult, cfunction, (cls, object));
        return cresult;
    }
    method = sky_descriptor_get(method, cls, sky_object_type(cls));
    result = sky_object_call(method, sky_tuple_pack(1, object), NULL);
    return sky_object_bool(result);
}

sky_bool_t
sky_object_issubclass(sky_object_t object, sky_object_t cls)
{
    sky_bool_t      issubclass;
    sky_object_t    o;

    if (sky_object_isa(cls, sky_tuple_type)) {
        issubclass = SKY_FALSE;
        SKY_SEQUENCE_FOREACH(cls, o) {
            if (sky_object_isa(o, sky_tuple_type)) {
                issubclass = sky_object_issubclass(object, o);
            }
            else {
                issubclass = sky_object_issubclass_worker(object, o);
            }
            if (issubclass) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
        return issubclass;
    }

    return sky_object_issubclass_worker(object, cls);
}


sky_bool_t
sky_object_isiterable(sky_object_t object)
{
    if (SKY_OBJECT_METHOD_SLOT(object, ITER)) {
        return SKY_TRUE;
    }
    if (sky_sequence_check(object)) {
        return SKY_TRUE;
    }
    if (sky_type_data(sky_object_type(object))->iterate) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


typedef sky_object_t
        (*sky_object_iter_t)(sky_object_t);

sky_object_t
sky_object_iter(sky_object_t object)
{
    sky_object_t        method, result;
    sky_object_iter_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, ITER))) {
        if (sky_sequence_check(object)) {
            result = sky_object_call(sky_iterator_type,
                                   sky_tuple_pack(1, object),
                                   NULL);
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ object is not iterable",
                                   sky_type_name(sky_object_type(object)));
        }
    }
    else if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_iter_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
        if (!result) {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ object is not iterable",
                                   sky_type_name(sky_object_type(object)));
        }
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!SKY_OBJECT_METHOD_SLOT(result, NEXT)) {
        sky_error_raise_format(sky_TypeError,
                               "iter() returned non-iterator of type %#@",
                               sky_type_name(sky_object_type(result)));
    }
    return result;
}


typedef sky_object_t
        (*sky_object_le_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_le(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_le_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, LE);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_le_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


typedef ssize_t
        (*sky_object_len_t)(sky_object_t);

ssize_t
sky_object_len(sky_object_t object)
{
    ssize_t             cresult;
    sky_object_t        method, result;
    sky_object_len_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, LEN))) {
        sky_error_raise_format(sky_TypeError,
                               "object of type %#@ has no len()",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_len_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(cresult, cfunction, (object));
        return cresult;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    result = sky_object_call(method, NULL, NULL);
    if (!sky_object_isa(result, sky_integer_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__len__ method should return an integer");
    }
    return sky_integer_value(result, 0, SSIZE_MAX, NULL);
}


typedef sky_object_t
        (*sky_object_length_hint_t)(sky_object_t);

ssize_t
sky_object_length_hint(sky_object_t object, ssize_t hint)
{
    ssize_t len = hint;

    sky_object_t                method, result;
    volatile sky_bool_t         error;
    sky_object_length_hint_t    cfunction;

    if (SKY_OBJECT_METHOD_SLOT(object, LEN)) {
        error = SKY_FALSE;
        SKY_ERROR_TRY {
            len = sky_object_len(object);
        }
        SKY_ERROR_EXCEPT(sky_TypeError) {
            error = SKY_TRUE;
        } SKY_ERROR_TRY_END;
        if (!error) {
            return len;
        }
    }

    if ((method = SKY_OBJECT_METHOD_SLOT(object, LENGTH_HINT)) != NULL) {
        error = SKY_FALSE;
        SKY_ERROR_TRY {
            if (sky_object_isa(method, sky_native_code_type)) {
                cfunction = (sky_object_length_hint_t)
                            sky_native_code_function(method);
                SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
            }
            else {
                method = sky_descriptor_get(method,
                                            object,
                                            sky_object_type(object));
                result = sky_object_call(method, NULL, NULL);
            }
        } SKY_ERROR_EXCEPT(sky_TypeError) {
            error = SKY_TRUE;
        } SKY_ERROR_TRY_END;
        if (error) {
            len = hint;
        }
        else if (sky_NotImplemented == result) {
            len = hint;
        }
        else if (!sky_object_isa(result, sky_integer_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "__length_hint__ method should return an integer");
        }
        else {
            len = sky_integer_value(result, SSIZE_MIN, SSIZE_MAX, NULL);
        }
    }

    if (len < 0) {
        sky_error_raise_string(sky_ValueError,
                               "__length_hint__() should return >= 0");
    }
    return len;
}


typedef sky_object_t
        (*sky_object_lt_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_lt(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_lt_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, LT);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_lt_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


typedef sky_object_t
        (*sky_object_ne_t)(sky_object_t, sky_object_t);

sky_object_t
sky_object_ne(sky_object_t object, sky_object_t other)
{
    sky_tuple_t     args;
    sky_object_t    method, o;
    sky_object_ne_t cfunction;

    method = SKY_OBJECT_METHOD_SLOT(object, NE);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_ne_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(o, cfunction, (object, other));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(1, other);
        o = sky_object_call(method, args, NULL);
    }
    if (sky_True == o || sky_False == o || sky_NotImplemented == o) {
        return o;
    }
    return (sky_object_bool(o) ? sky_True : sky_False);
}


typedef sky_object_t
        (*sky_object_next_t)(sky_object_t);

sky_object_t
sky_object_next(sky_object_t object, sky_object_t value)
{
    sky_object_t        method, result;
    sky_object_next_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, NEXT))) {
        sky_error_raise_format(sky_AttributeError,
                               "%#@ object has no attribute '__next__'",
                               sky_type_name(sky_object_type(object)));
    }

    SKY_ERROR_TRY {
        if (sky_object_isa(method, sky_native_code_type)) {
            cfunction = (sky_object_next_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
            if (!result) {
                if (sky_NotSpecified == value) {
                    sky_error_raise_object(sky_StopIteration, sky_None);
                }
                result = value;
            }
        }
        else {
            method = sky_descriptor_get(method, object, sky_object_type(object));
            result = sky_object_call(method, NULL, NULL);
        }
    }
    SKY_ERROR_EXCEPT(sky_StopIteration) {
        if (sky_NotSpecified == value) {
            sky_error_raise();
        }
        result = value;
    } SKY_ERROR_TRY_END;

    return result;
}


typedef sky_string_t
        (*sky_object_repr_t)(sky_object_t);

sky_string_t
sky_object_repr(sky_object_t object)
{
    sky_object_t        method, result;
    sky_object_repr_t   cfunction;

    if (!object) {
        return SKY_STRING_LITERAL("<NULL>");
    }

    method = SKY_OBJECT_METHOD_SLOT(object, REPR);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_repr_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_string_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__repr__ method should return a string");
    }
    return result;
}


typedef sky_object_t
        (*sky_object_reversed_t)(sky_object_t);

sky_object_t
sky_object_reversed(sky_object_t object)
{
    sky_object_t            method, result;
    sky_object_reversed_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, REVERSED))) {
        if (SKY_OBJECT_METHOD_SLOT(object, LEN) &&
            SKY_OBJECT_METHOD_SLOT(object, GETITEM))
        {
            return sky_object_call(sky_reversed_type,
                                   sky_tuple_pack(1, object),
                                   NULL);
        }
        sky_error_raise_format(sky_TypeError,
                               "%#@ object is not reverse iterable",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_reversed_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
        if (!result) {
            sky_error_raise_format(sky_TypeError,
                                   "%#@ object is not reverse iterable",
                                   sky_type_name(sky_object_type(object)));
        }
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(method));
        result = sky_object_call(method, NULL, NULL);
    }
    return result;
}


typedef void
        (*sky_object_setattr_t)(sky_object_t, sky_string_t, sky_object_t);

void
sky_object_setattr(sky_object_t object,
                   sky_string_t name,
                   sky_object_t value)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_object_setattr_t    cfunction;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    method = SKY_OBJECT_METHOD_SLOT(object, SETATTR);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_setattr_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object, name, value));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(2, name, value);
        sky_object_call(method, args, NULL);
    }
}


typedef void
        (*sky_object_setitem_t)(sky_object_t, sky_object_t, sky_object_t);

void
sky_object_setitem(sky_object_t object, sky_object_t item, sky_object_t value)
{
    sky_tuple_t             args;
    sky_object_t            method;
    sky_object_setitem_t    cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, SETITEM))) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ object does not support item assignment",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_setitem_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object, item, value));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        args = sky_tuple_pack(2, item, value);
        sky_object_call(method, args, NULL);
    }
}


typedef void
        (*sky_object_setstate_t)(sky_object_t, sky_object_t);

sky_bool_t
sky_object_setstate(sky_object_t object, sky_object_t state)
{
    sky_object_t            method;
    sky_object_setstate_t   cfunction;

    if (!(method = SKY_OBJECT_METHOD_SLOT(object, SETSTATE))) {
        return SKY_FALSE;
    }

    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_setstate_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE_VOID(cfunction, (object, state));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        sky_object_call(method, NULL, NULL);
    }
    return SKY_TRUE;
}


typedef size_t
        (*sky_object_sizeof_t)(sky_object_t);

size_t
sky_object_sizeof(sky_object_t object)
{
    size_t              cresult;
    sky_object_t        method, result;
    sky_object_sizeof_t cfunction;

    if (sky_object_istagged(object)) {
        return 0;
    }

    method = SKY_OBJECT_METHOD_SLOT(object, SIZEOF);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_sizeof_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(cresult, cfunction, (object));
        return cresult;
    }

    method = sky_descriptor_get(method, object, sky_object_type(object));
    result = sky_object_call(method, NULL, NULL);
    if (!sky_object_isa(result, sky_integer_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__sizeof__ method should return an integer");
    }
    return sky_integer_value(result, 0, SIZE_MAX, NULL);
}


void
sky_object_stack_pop(sky_object_t object)
{
    sky_list_t  object_stack = sky_tlsdata_get()->object_tlsdata.object_stack;

    sky_error_validate_debug(sky_list_len(object_stack) > 0);
    sky_error_validate(object == sky_list_pop(object_stack, -1));
}


sky_bool_t
sky_object_stack_push(sky_object_t object)
{
    sky_object_tlsdata_t    *object_tlsdata =
                            &(sky_tlsdata_get()->object_tlsdata);

    sky_bool_t      result;
    sky_object_t    item;

    result = SKY_FALSE;
    if (!object_tlsdata->object_stack) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(object_tlsdata->object_stack)),
                              sky_list_create(NULL),
                              SKY_FALSE);
    }
    else {
        SKY_SEQUENCE_FOREACH(object_tlsdata->object_stack, item) {
            if (item == object) {
                result = SKY_TRUE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }

    if (!result) {
        sky_list_append(object_tlsdata->object_stack, object);
    }
    return result;
}


typedef sky_string_t
        (*sky_object_str_t)(sky_object_t);

sky_string_t
sky_object_str(sky_object_t object)
{
    sky_object_t        method, result;
    sky_object_str_t    cfunction;

    if (!object) {
        return SKY_STRING_LITERAL("<NULL>");
    }

    method = SKY_OBJECT_METHOD_SLOT(object, STR);
    if (sky_object_isa(method, sky_native_code_type)) {
        cfunction = (sky_object_str_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(result, cfunction, (object));
    }
    else {
        method = sky_descriptor_get(method, object, sky_object_type(object));
        result = sky_object_call(method, NULL, NULL);
    }
    if (!sky_object_isa(result, sky_string_type)) {
        sky_error_raise_string(sky_TypeError,
                               "__str__ method should return a string");
    }
    return result;
}


sky_bool_t
sky_object_subclasscheck(sky_object_t cls, sky_object_t subclass)
{
    sky_tuple_t bases;

    if (sky_object_isa(cls, sky_type_type) &&
        sky_object_isa(subclass, sky_type_type))
    {
        return sky_type_issubtype(subclass, cls);
    }

    bases = sky_object_getattr(subclass, SKY_STRING_LITERAL("__bases__"), NULL);
    if (!sky_object_isa(bases, sky_tuple_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "cls must be a class");
    }
    bases = sky_object_getattr(cls, SKY_STRING_LITERAL("__bases__"), NULL);
    if (!sky_object_isa(bases, sky_tuple_type)) {
        sky_error_raise_string(
                sky_TypeError,
                "subclass must be a class or tuple of classes");
    }

    return sky_object_issubclass_recursive(subclass, cls);
}


static sky_object_atfork_table_t *
sky_object_atfork_table_combine(sky_object_atfork_table_t * target,
                                sky_object_atfork_table_t * source)
{
    sky_hashtable_item_t        *existing_item;
    sky_object_atfork_item_t    *item, *new_item;

    if (!source) {
        return target;
    }
    if (!target) {
        return source;
    }
    if (!target->last) {
        sky_hashtable_destroy(&(target->base), sky_free);
        return source;
    }

    for (item = source->first; item; item = item->next) {
        new_item = sky_memdup(item, sizeof(sky_object_atfork_item_t));
        existing_item = sky_hashtable_insert(&(target->base),
                                             &(new_item->base),
                                             ((uintptr_t)item->object >> 4),
                                             item->object);
        if (&(new_item->base) != existing_item) {
            sky_free(new_item);
        }
        else {
            new_item->next = NULL;
            if (!(new_item->prev = target->last)) {
                target->first = new_item;
            }
            else {
                target->last->next = new_item;
            }
            target->last = new_item;
        }
    }
    sky_hashtable_destroy(&(source->base), sky_free);

    return target;
}


void
sky_object_tlsdata_adopt(sky_object_tlsdata_t *tlsdata,
                         sky_object_tlsdata_orphan_t *orphan)
{
    tlsdata->atfork_table =
            sky_object_atfork_table_combine(tlsdata->atfork_table,
                                            orphan->atfork_table);
    orphan->atfork_table = NULL;

    if (orphan->gc_concrete_weakref_set) {
        if (!tlsdata->gc_concrete_weakref_set) {
            tlsdata->gc_concrete_weakref_set = orphan->gc_concrete_weakref_set;
        }
        else {
            sky_pointer_set_merge(tlsdata->gc_concrete_weakref_set,
                                  orphan->gc_concrete_weakref_set);
            sky_pointer_set_destroy(orphan->gc_concrete_weakref_set);
        }
        orphan->gc_concrete_weakref_set = NULL;
    }
    sky_object_list_extend(&(tlsdata->gc_concrete_objects),
                           &(orphan->gc_concrete_objects));
    sky_object_list_extend(&(tlsdata->gc_concrete_sweep),
                           &(orphan->gc_concrete_sweep));
    sky_object_list_extend(&(tlsdata->gc_concrete_dying),
                           &(orphan->gc_concrete_dying));
    sky_object_list_extend(&(tlsdata->gc_concrete_del),
                           &(orphan->gc_concrete_del));
    sky_object_list_extend(&(tlsdata->gc_concrete_finalize),
                           &(orphan->gc_concrete_finalize));
    sky_object_list_extend(&(tlsdata->gc_concrete_deallocate),
                           &(orphan->gc_concrete_deallocate));

    /* The orphan's mode/state could be ahead of ours, behind ours, or the same
     * as ours. We only care about it being behind ours, in which case we must
     * change ours to match.
     */
    if (orphan->gc_mode < tlsdata->gc_mode) {
        tlsdata->gc_mode = orphan->gc_mode;
    }
}


void
sky_object_tlsdata_cleanup(sky_object_tlsdata_t *tlsdata,
                           sky_object_tlsdata_orphan_t *orphan_tlsdata,
                           sky_bool_t final)
{
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->object_stack)),
                          NULL,
                          SKY_FALSE);

    orphan_tlsdata->atfork_table =
            sky_object_atfork_table_combine(orphan_tlsdata->atfork_table,
                                            tlsdata->atfork_table);
    tlsdata->atfork_table = NULL;

    sky_object_gc_finalize_thread(tlsdata, orphan_tlsdata, final);

    sky_object_list_empty(&(tlsdata->gc_concrete_objects));
    sky_object_list_empty(&(tlsdata->gc_concrete_sweep));
    sky_object_list_empty(&(tlsdata->gc_concrete_dying));
    sky_object_list_empty(&(tlsdata->gc_concrete_del));
    sky_object_list_empty(&(tlsdata->gc_concrete_finalize));
    sky_object_list_empty(&(tlsdata->gc_concrete_deallocate));
    tlsdata->gc_concrete_weakref_set = NULL;
}


sky_type_t
sky_object_type(sky_object_t object)
{
    if (!sky_object_istagged(object)) {
        return SKY_OBJECT_DATA(object)->u.type;
    }
    if (sky_object_istaggedtuple(object)) {
        return sky_tuple_type;
    }
    switch (SKY_OBJECT_TAG(object)) {
        case 0x1: case 0x2:
            return sky_integer_type;
        case 0x3: case 0x4:
            return sky_string_type;
        case 0x7:
            switch (((uintptr_t)object >> 4) & 0xF) {
                case 0x1: case 0x2: case 0x3: case 0x4:
                case 0x5: case 0x6: case 0x7:
                    return sky_bytes_type;
                case 0x0:
                    switch (((uintptr_t)object >> 12) & 0xF) {
                        case 0x0:
                            switch (((uintptr_t)object >> 8) & 0xF) {
                                case 0x0:
                                    return sky_None_type;
                                case 0x1:
                                    return sky_Ellipsis_type;
                                case 0x2:
                                    return sky_NotImplemented_type;
                                case 0x3:
                                    return sky_NotSpecified_type;
                                case 0x4: case 0x5:
                                    return sky_boolean_type;
                                case 0x6:
                                    return sky_bytes_type;
                                case 0x7:
                                    return sky_string_type;
                                case 0x8:
                                    return sky_tuple_type;
                                case 0x9:
                                    return sky_frozenset_type;
                            }
                        case 0x1:
                            if ((((uintptr_t)object >> 12) & 0xF) <= 6) {
                                return sky_float_type;
                            }
                            break;
                        case 0x2:
                            if ((((uintptr_t)object >> 12) & 0xF) <= 5) {
                                return sky_integer_type;
                            }
                            break;
                    }
            }
    }

    sky_error_fatal("internal error");
}


typedef struct sky_object_visit_reserved_s {
    sky_object_visit_action_t           action;
    sky_pointer_set_t *                 pending;
    size_t                              depth;
} sky_object_visit_reserved_t;

static void
sky_object_visit_cleanup(sky_object_visit_reserved_t *reserved)
{
    if (reserved->pending) {
        sky_pointer_set_destroy(reserved->pending);
    }
}

SKY_STATIC_INLINE void
sky_object_visit_object(sky_object_t                object,
                        sky_object_visit_data_t *   visit_data)
{
    sky_object_visit_reserved_t *reserved = visit_data->reserved;

    void            *data;
    ssize_t         i, len;
    sky_type_data_t *type_data;
    sky_type_list_t *mro, *offsets;

    type_data = sky_type_data(SKY_OBJECT_DATA(object)->u.type);
    mro = type_data->mro;
    offsets = type_data->data_offsets;
    len = mro->len;

    ++reserved->depth;
    for (i = 0; i < len; ++i) {
        type_data = sky_type_data(mro->items[i].type);
        if (type_data->visit) {
            data = (void *)((char *)object + offsets->items[i].offset);
            type_data->visit(object, data, visit_data);
        }
    }
    --reserved->depth;
}

void
sky_object_visit(sky_object_t object, sky_object_visit_data_t *visit_data)
{
    sky_object_visit_reserved_t *reserved = visit_data->reserved;

    if (!object) {
        return;
    }
    if (sky_object_istagged(object)) {
        if (!sky_object_istaggedtuple(object)) {
            return;
        }
        object = sky_object_taggedtupleobject(object);
        sky_error_validate_debug(NULL != object);
        if (sky_object_istagged(object)) {
            return;
        }
    }

    if (reserved->action(object, visit_data)) {
        /* The limit here is complete arbitrary. It may need to be adjusted.
         * Maybe make it configurable somewhere somehow or something?
         */
        if (reserved->depth <= 50) {
            sky_object_visit_object(object, visit_data);
        }
        else {
            if (!reserved->pending) {
                reserved->pending = sky_pointer_set_create();
            }
            sky_pointer_set_add(reserved->pending, object);
        }
    }
}


void
sky_object_walk(sky_object_t                object,
                sky_object_visit_reason_t   reason,
                sky_object_visit_action_t   action,
                void *                      arg)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_object_visit_data_t     visit_data;
        sky_object_visit_reserved_t reserved;

        reserved.action = action;
        reserved.pending = NULL;
        reserved.depth = 0;
        sky_asset_save(&reserved,
                       (sky_free_t)sky_object_visit_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        visit_data.reason = reason;
        visit_data.arg = arg;
        visit_data.reserved = &reserved;

        sky_object_visit(object, &visit_data);
        if (reserved.pending) {
            sky_object_t    o;

            while ((o = sky_pointer_set_pop_pointer(reserved.pending)) != NULL) {
                sky_object_visit_object(o, &visit_data);
            }
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_object_finalize_library(void)
{
    sky_object_gc_finalize_library();
}


typedef struct sky_object_primary_type_s {
    sky_type_t                          type;
    sky_bool_t                          split;
    sky_type_t                          base_type;
} sky_object_primary_type_t;

void
sky_object_initialize_library(SKY_UNUSED unsigned int flags)
{
#define TYPE(n) &sky_##n##_type_struct
    static sky_object_primary_type_t primary_types[] = {
        { TYPE(base_object),    SKY_TRUE,   NULL,           },
        { TYPE(type),           SKY_TRUE,   NULL,           },
        { TYPE(parameter_list), SKY_TRUE,   NULL,           },
        { TYPE(native_code),    SKY_TRUE,   NULL,           },
        { TYPE(string_cache),   SKY_TRUE,   NULL,           },
        { TYPE(dict),           SKY_TRUE,   NULL,           },
        { TYPE(type_dict),      SKY_TRUE,   TYPE(dict),     },
        { TYPE(mappingproxy),   SKY_TRUE,   NULL,           },
        { TYPE(capsule),        SKY_TRUE,   NULL,           },
        { TYPE(getset),         SKY_TRUE,   NULL,           },
        { TYPE(string),         SKY_TRUE,   NULL,           },
        { TYPE(integer),        SKY_TRUE,   NULL,           },

        { TYPE(Ellipsis),       SKY_FALSE,  NULL,           },
        { TYPE(None),           SKY_FALSE,  NULL,           },
        { TYPE(NotImplemented), SKY_FALSE,  NULL,           },
        { TYPE(NotSpecified),   SKY_FALSE,  NULL,           },
        { TYPE(complex),        SKY_FALSE,  NULL,           },
        { TYPE(float),          SKY_FALSE,  NULL,           },
        { TYPE(boolean),        SKY_FALSE,  TYPE(integer),  },
        { TYPE(bytes),          SKY_FALSE,  NULL,           },
        { TYPE(bytes_builder),  SKY_FALSE,  TYPE(bytes),    },
        { TYPE(bytearray),      SKY_FALSE,  NULL,           },
        { TYPE(string_builder), SKY_FALSE,  TYPE(string),   },
        { TYPE(list),           SKY_FALSE,  NULL,           },
        { TYPE(set),            SKY_FALSE,  NULL,           },
        { TYPE(tuple),          SKY_FALSE,  NULL,           },
        { TYPE(function),       SKY_FALSE,  NULL,           },
        { TYPE(classmethod),    SKY_FALSE,  NULL,           },
        { TYPE(staticmethod),   SKY_FALSE,  NULL,           },
        { TYPE(member),         SKY_FALSE,  NULL,           },
        { TYPE(method),         SKY_FALSE,  NULL,           },
        { NULL,                 SKY_FALSE,  NULL,           },
    };
#undef TYPE

    size_t          i;
    sky_object_t    *slots;

    /* First and foremost, get everything up and running for garbage
     * collection. This absolutely must come first.
     */
    sky_object_gc_initialize_library();

    /* Do very early initialization of static types. These are the types that
     * are needed to bootstrap the object type code so that other types can be
     * initialized and created.
     *
     * The first step is to do early initialization, which primarily involves
     * setting up the mro and method_slots fields in the type data.
     */
    for (i = 0; primary_types[i].type && primary_types[i].split; ++i) {
        if (!primary_types[i].base_type) {
            sky_type_initialize_static(primary_types[i].type, 0);
        }
        else {
            sky_type_initialize_static(primary_types[i].type,
                                       1,
                                       primary_types[i].base_type);
        }
    }

    /* Next, string objects need some method slots filled so that dicts and
     * sets can be created using strings and integers as keys. This is a gross
     * abuse of the way that method slots work, but it's unavoidable and only
     * temporary. Strings are needed for object attributes, function names,
     * etc. Integers are needed for exceptions to initialize cleanly.
     */
    slots = sky_string_type->type_data.method_slots;
#if !defined(NDEBUG)
    /* Also make strings printable for debugging */
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_REPR]),    /* __repr__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_string_repr,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL),
            sky_string_type);
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_STR]),     /* __str__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_string_str,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    NULL),
            sky_string_type);
#endif
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_EQ]),      /* __eq__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_string_eq,
                    SKY_DATA_TYPE_OBJECT,
                    NULL),
            sky_string_type);
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_HASH]),    /* __hash__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_string_hash,
                    SKY_DATA_TYPE_UINTPTR_T,
                    NULL),
            sky_string_type);

    /* Also for the integer type to make it hashable. */
    slots = sky_integer_type->type_data.method_slots;
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_EQ]),      /* __eq__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_integer_eq,
                    SKY_DATA_TYPE_OBJECT,
                    NULL),
            sky_integer_type);
    sky_object_gc_set(
            &(slots[SKY_TYPE_METHOD_SLOT_HASH]),    /* __hash__ */
            sky_native_code_create(
                    (sky_native_code_function_t)sky_integer_hash,
                    SKY_DATA_TYPE_UINTPTR_T,
                    NULL),
            sky_integer_type);

    /* Finally, complete static object initialization for the early types so
     * that they're in a state equivalent to other static built-in types that
     * will call sky_type_initialize_builtin().
     */
    for (i = 0; primary_types[i].type && primary_types[i].split; ++i) {
        sky_type_complete_static(primary_types[i].type);
    }

    /* Initialize other static types that need initialization so that they can
     * be instantiated.
     */
    for (i = 0; primary_types[i].type; ++i) {
        if (primary_types[i].split) {
            continue;
        }
        if (!primary_types[i].base_type) {
            sky_type_initialize_builtin(primary_types[i].type, 0);
        }
        else {
            sky_type_initialize_builtin(primary_types[i].type,
                                        1,
                                        primary_types[i].base_type);
        }
    }

    /* Prepare sky_base_object for use. */
    sky_base_object_initialize_library();
    sky_type_initialize_library();

    sky_exceptions_initialize_library();

    /* Prepare built-in types for use. In some cases order is important! */
    sky_integer_initialize_library();
    sky_boolean_initialize_library();   /* depends on sky_integer */

    /* Initialize mutex early so that later initialization can use mutex
     * objects to guard internal caches and such.
     */
    sky_mutex_initialize_library();

    sky_bytearray_initialize_library();
    sky_bytes_initialize_library();
    sky_bytes_builder_initialize_library();     /* depends on sky_bytes */
    sky_callable_iterator_initialize_library();
    sky_capsule_initialize_library();
    sky_cell_initialize_library();
    sky_classmethod_initialize_library();
    sky_code_initialize_library();
    sky_complex_initialize_library();
    sky_deque_initialize_library();
    sky_dict_initialize_library();
    sky_type_dict_initialize_library();         /* depends on sky_dict */
    sky_Ellipsis_initialize_library();
    sky_enumerate_initialize_library();
    sky_filter_initialize_library();
    sky_float_initialize_library();
    sky_frame_initialize_library();
    sky_frozenset_initialize_library();
    sky_function_initialize_library();
    sky_generator_initialize_library();
    sky_getset_initialize_library();
    sky_iterator_initialize_library();
    sky_list_initialize_library();
    sky_map_initialize_library();
    sky_mappingproxy_initialize_library();
    sky_member_initialize_library();
    sky_memoryview_initialize_library();
    sky_method_initialize_library();
    sky_module_initialize_library();
    sky_namespace_initialize_library();
    sky_native_code_initialize_library();
    sky_None_initialize_library();
    sky_NotImplemented_initialize_library();
    sky_NotSpecified_initialize_library();
    sky_parameter_list_initialize_library();
    sky_property_initialize_library();
    sky_range_initialize_library();
    sky_reversed_initialize_library();
    sky_set_initialize_library();
    sky_slice_initialize_library();
    sky_socket_initialize_library();
    sky_staticmethod_initialize_library();
    sky_string_initialize_library();
    sky_string_builder_initialize_library();    /* depends on sky_string */
    sky_struct_sequence_initialize_library();
    sky_super_initialize_library();
    sky_thread_initialize_library();
    sky_traceback_initialize_library();
    sky_tuple_initialize_library();
    sky_weakproxy_initialize_library();
    sky_weakref_initialize_library();
    sky_zip_initialize_library();

    sky_library_atfinalize(sky_object_finalize_library);
}
