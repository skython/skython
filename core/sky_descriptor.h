/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_descriptor Descriptors
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_DESCRIPTOR_H__
#define __SKYTHON_CORE_SKY_DESCRIPTOR_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Call the __delete__() method for a descriptor object.
  * Calls __delete__(descriptor, instance). If @a descriptor does not define
  * a __delete__() method, @c sky_AttributeError will be raised.
  *
  * @param[in]  descriptor  the descriptor object possibly defining the
  *                         __delete__() method.
  * @param[in]  instance    the object instance for which the __delete__()
  *                         method is to be applied.
  */
SKY_EXTERN void
sky_descriptor_delete(sky_object_t  descriptor,
                      sky_object_t  instance);


/** Call the __get__() method for a descriptor object.
  * Calls __get__(descriptor, instance, owner). If @a descriptor does not
  * define a __get__() method, @a descriptor will be returned. This behavior
  * is inconsistent with the behavior of sky_descriptor_delete() and
  * sky_descriptor_set(); however, those two functions have very specific uses,
  * whereas this function is much more general. The overhead of raising and
  * exception and requiring every call to handle that exception has severe
  * performance consequences. The expectation is that this function will often
  * be called when there is no __get__() method to be called, but the other
  * functions will only be called when there is a strong expectation that there
  * is either __delete__() or __set__() methods to be called.
  *
  * @param[in]  descriptor  the descriptor object possibly defining the
  *                         __get__() method.
  * @param[in]  instance    the object instance for which the __get__()
  *                         method is to be applied.
  * @param[in]  owner       the type that owns the descriptor object.
  * @return     the object instance returned by the __get__() method, or
  *             @a descriptor if there is no __get__() method to be called.
  */
SKY_EXTERN sky_object_t
sky_descriptor_get(sky_object_t descriptor,
                   sky_object_t instance,
                   sky_type_t   owner);


/** Determine whether a descriptor is a data descriptor.
  * A data descriptor is a descriptor that defines both __get__() and __set__()
  * methods.
  *
  * @param[in]  descriptor  the descriptor object to query.
  * @return     @c SKY_TRUE if the descriptor is a data descriptor, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_descriptor_isdata(sky_object_t descriptor);


/** Call the __set__() method for a descriuptor object.
  * Calls __set__(descriptor, instance, value). If @a descriptor does not
  * define a __set__() method, @c sky_AttributeError will be raised.
  *
  * @param[in]  descriptor  the descriptor object possibly defining the
  *                         __set__() method.
  * @param[in]  instance    the object instance for which the __set__()
  *                         method is to be applied.
  * @param[in]  value       the value to be set.
  */
SKY_EXTERN void
sky_descriptor_set(sky_object_t descriptor,
                   sky_object_t instance,
                   sky_object_t value);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_DESCRIPTOR_H__ */

/** @} **/
/** @} **/
