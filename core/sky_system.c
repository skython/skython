#include "sky_private.h"

#if defined(HAVE_LANGINFO_H)
#   include <langinfo.h>
#endif
#if defined(HAVE_SCHED_H)
#   include <sched.h>
#endif
#if defined(HAVE_UTIL_H)
#   include <util.h>
#endif
#if defined(HAVE_SYS_MMAN_H)
#   include <sys/mman.h>
#endif
#if defined(HAVE_SYS_RESOURCE_H)
#   include <sys/resource.h>
#endif
#if defined(HAVE_SYS_SYSCTL_H)
#   include <sys/sysctl.h>
#endif
#if defined(HAVE_SYS_TIME_H)
#   include <sys/time.h>
#endif


void *
sky_system_alloc(size_t nbytes)
{
    void    *pointer;

#if defined(_WIN32)
#   define MAP_FAILED NULL
    pointer = VirtualAlloc(NULL,
                           nbytes,
                           MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#elif defined(HAVE_MMAP)
    pointer = mmap(NULL,
                   nbytes,
                   PROT_READ | PROT_WRITE,
                   MAP_ANON | MAP_PRIVATE,
                   -1,
                   0);
#else
#   error implementation missing
#endif

    if (unlikely(MAP_FAILED == pointer)) {
        sky_error_fatal("out of memory");
    }
    return pointer;
}


#if (defined(__GNUC__) || defined(__clang__)) && \
    (defined(__i386__) || defined(__x86_64__))
static unsigned int
sky_system_controlfpu_x87(unsigned short cw)
{
    unsigned int    x86_values = 0;

    if (cw & 0x0001) x86_values |= _EM_INVALID;
    if (cw & 0x0002) x86_values |= _EM_DENORMAL;
    if (cw & 0x0004) x86_values |= _EM_ZERODIVIDE;
    if (cw & 0x0008) x86_values |= _EM_OVERFLOW;
    if (cw & 0x0010) x86_values |= _EM_UNDERFLOW;
    if (!(cw & 0x0020)) x86_values |= _EM_INEXACT;
    switch ((cw & 0x0300)) {    /* precision control */
        case 0x0000: x86_values |= _PC_24;                  break;
        case 0x0100: sky_error_fatal("reserved PC set");    break;
        case 0x0200: x86_values |= _PC_53;                  break;
        case 0x0300: x86_values |= _PC_64;                  break;
    }
    switch ((cw & 0x0C00)) {    /* rounding control */
        case 0x0000: x86_values |= _RC_NEAR;    break;
        case 0x0400: x86_values |= _RC_DOWN;    break;
        case 0x0800: x86_values |= _RC_UP;      break;
        case 0x0C00: x86_values |= _RC_CHOP;    break;
    }
    if ((cw & 0x1000)) {        /* infinity control */
        x86_values |= _IC_AFFINE;
    }
    else {
        x86_values |= _IC_PROJECTIVE;
    }

    return x86_values;
}
#endif

int
sky_system_controlfpu(unsigned int  new_values,
                      unsigned int  mask,
                      unsigned int *x86_values,
                      unsigned int *sse2_values)
{
#if defined(_MSC_VER) && !defined(_WIN64)
    return __control87_2(new_values, mask, x86_values, sse2_values);
#elif (defined(__GNUC__) || defined(__clang__)) && \
      (defined(__i386__) || defined(__x86_64__))
    /* yay, inline asm for x87 */
    if (!mask) {
        /* Simply get the control word for either x86 or sse2 and return it. */
        if (x86_values) {
            unsigned short  cw;

            __asm__ __volatile__ ("fnstcw %0" : "=m" (cw));
            *x86_values = sky_system_controlfpu_x87(cw);
        }
        if (sse2_values) {
            sky_error_fatal("not implemented");
        }
    }
    else if (x86_values && sse2_values) {
        return 0;
    }
    else if (x86_values) {
        unsigned short  cw;

        __asm__ __volatile__ ("fnstcw %0" : "=m" (cw));
        /* _MCW_DN is NOP on x86 */
        if ((mask & _MCW_EM) == _MCW_EM) {
            cw &= ~0x003F;
            if (new_values & _EM_INVALID) cw |= 0x0001;
            if (new_values & _EM_DENORMAL) cw |= 0x0002;
            if (new_values & _EM_ZERODIVIDE) cw |= 0x0004;
            if (new_values & _EM_OVERFLOW) cw |= 0x0008;
            if (new_values & _EM_UNDERFLOW) cw |= 0x0010;
            if (!(new_values & _EM_INEXACT)) cw |= 0x0020;
        }
        if ((mask & _MCW_IC) == _MCW_IC) {
            if (new_values & _IC_AFFINE) cw |= 0x1000;
            else cw &= ~0x1000;
        }
        if ((mask & _MCW_RC) == _MCW_RC) {
            cw &= ~0x0C00;
            switch (new_values & _MCW_RC) {
                case _RC_CHOP:  cw |= 0x0C00;   break;
                case _RC_UP:    cw |= 0x0800;   break;
                case _RC_DOWN:  cw |= 0x0400;   break;
                case _RC_NEAR:  cw |= 0x0000;   break;
            }
        }
        if ((mask & _MCW_PC) == _MCW_PC) {
            cw &= ~0x0300;
            switch (new_values & _MCW_PC) {
                case _PC_24:    cw |= 0x0000;   break;
                case _PC_53:    cw |= 0x0200;   break;
                case _PC_64:    cw |= 0x0300;   break;
            }
        }

        __asm__ __volatile__ ("fldcw %0" : : "m" (cw));
        *x86_values = sky_system_controlfpu_x87(cw);
    }
    else if (sse2_values) {
        sky_error_fatal("not implemented");
    }
    return 1;
#else
#   warn sky_system_controlfpu() implementation missing
    return 0;
#endif
}


uint32_t
sky_system_cpucount(void)
{
    static uint32_t cached_cpucount = 0;

    if (unlikely(!cached_cpucount)) {
#if defined(_WIN32)
        SYSTEM_INFO si;

        GetSystemInfo(&si);
        cached_cpucount = (uint32_t)si.dwNumberOfProcessors;
#elif defined(HAVE_SYSCONF) && defined(_SC_NPROCESSORS_ONLN)
        cached_cpucount = (uint32_t)sysconf(_SC_NPROCESSORS_ONLN);
#else
#   error implementation missing
#endif
    }

    return cached_cpucount;
}


const char *
sky_system_filesystemencoding(void)
{
#if defined(__APPLE__)
    return "utf-8";
#elif defined(_WIN32)
    return "mbcs";
#else
    char    *codeset;

    if (!(codeset = nl_langinfo(CODESET))) {
        return "utf-8";
    }
    return codeset;
#endif
}


pid_t
sky_system_fork(void)
{
    pid_t   result;

    sky_atfork_prepare();
    if (!(result = fork())) {
        sky_atfork_child();
    }
    else if (-1 != result) {
        sky_atfork_parent();
    }

    return result;
}


#if defined(HAVE_FORK1)
pid_t
sky_system_fork1(void)
{
    pid_t   result;

    sky_atfork_prepare();
    if (!(result = fork1())) {
        sky_atfork_child();
    }
    else if (-1 != result) {
        sky_atfork_parent();
    }

    return result;
}
#endif


pid_t
sky_system_forkpty(int *            amaster,
                   char *           name,
                   struct termios * termp,
                   struct winsize * winp)
{
    pid_t   result;

    sky_atfork_prepare();
    if (!(result = forkpty(amaster, name, termp, winp))) {
        sky_atfork_child();
    }
    else if (-1 != result) {
        sky_atfork_parent();
    }

    return result;
}


void
sky_system_free(void *pointer, size_t nbytes)
{
#if defined(_WIN32)
    if (!VirtualFree(pointer, 0, MEM_RELEASE)) {
        sky_error_fatal("internal error");
    }
#elif defined(HAVE_MMAP)
    if (munmap(pointer, nbytes) == -1) {
        sky_error_fatal("internal error");
    }
#else
#   error implementation missing
#endif
}


uint32_t
sky_system_maxopenfiles(void)
{
#if defined(_WIN32)
    return UINT32_MAX;
#elif defined(RLIMIT_NOFILE)
#   if defined(__linux__)
    static int      name[] = { CTL_FS, FS_MAXFILE };
#   endif
#   if defined(__APPLE__) || defined(__FreeBSD__) || defined(__linux__)
    int             value;
    size_t          value_size;
#   endif

    struct rlimit   rlim;

    if (unlikely(getrlimit(RLIMIT_NOFILE, &rlim) == -1)) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    if (likely(RLIM_INFINITY != rlim.rlim_cur)) {
        return rlim.rlim_cur;
    }
#   if defined(__APPLE__) || defined(__FreeBSD__)
    value_size = sizeof(value);
    if (sysctlbyname("kern.maxfilesperproc", &value, &value_size, NULL, 0) != -1) {
        return value;
    }
#   elif defined(__linux__)
    value_size = sizeof(value);
    if (sysctl(name, sizeof(name) / sizeof(name[0]),
               &value, &value_size, NULL, 0) != -1)
    {
        return value;
    }
#   endif
    return UINT32_MAX;
#else
#   error implementation missing
#endif
}


size_t
sky_system_pagesize(void)
{
    static size_t   cached_pagesize = 0;

    if (unlikely(!cached_pagesize)) {
#if defined(_WIN32)
        SYSTEM_INFO si;

        GetSystemInfo(&si);
        cached_pagesize = (size_t)si.dwPageSize;
#elif defined(HAVE_GETPAGESIZE)
        cached_pagesize = (size_t)getpagesize();
#else
#   error implementation missing
#endif
    }

    return cached_pagesize;
}


int
sky_system_somaxconn(void)
{
#if defined(__APPLE__) || defined(__FreeBSD__) || defined(__linux__)
#if defined(__linux__)
    int     name[] = { CTL_NET, NET_CORE, NET_CORE_SOMAXCONN };
#else
    int     name[] = { CTL_KERN, KERN_IPC, KIPC_SOMAXCONN };
#endif

    int     value;
    size_t  value_size = sizeof(value);

    if (sysctl(name, sizeof(name) / sizeof(name[0]),
               &value, &value_size, NULL, 0) != -1)
    {
        return value;
    }
    if (errno != ENOENT) {
        sky_error_raise_errno(sky_OSError, errno);
    }
#endif

    return SOMAXCONN;
}


sky_time_t
sky_system_time(void)
{
#if defined(_WIN32)
    FILETIME        filetime;
    ULARGE_INTEGER  uli;

    /* 100-nanosecond intervals since January 1, 1601 UTC */
    GetSystemTimeAsFileTime(&filetime);

    uli.LowPart  = filetime.dwLowDateTime;
    uli.HighPart = filetime.dwHighDateTime;

    /* Shift from January 1, 1601 to January 1, 1970 */
    return (uli.QuadPart * 100) - UINT64_C(11644473600000000000);
#else
    struct timeval  tv;

    gettimeofday(&tv, NULL);
    return sky_time_fromtimeval(&tv);
#endif
}


void
sky_system_yield(void)
{
#if defined(_WIN32)
    Sleep(0);
#elif defined(HAVE_SCHED_YIELD)
    sched_yield();
#endif
}
