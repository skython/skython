#include "sky_private.h"
#include "sky_marshal.h"
#include "sky_python_compiler.h"
#include "sky_python_parser.h"

#include <sys/stat.h>


static void
freeze_output(void *arg, const void *bytes, ssize_t nbytes)
{
    ssize_t i;

    for (i = 0; i < nbytes; ++i) {
        if (!(i % 16)) {
            fputs("    ", arg);
        }
        sky_format_fprintf(arg,
                           "0x%02hhX,%c",
                           ((uint8_t *)bytes)[i],
                           (i % 16 == 15 ? '\n' : ' '));
    }

    if (i % 16) {
        fputc('\n', arg);
    }
}


static void
close_fd(void *arg)
{
    close(*(int *)arg);
}

static sky_bytes_t
read_source(const char *filename)
{
    int         fd;
    char        *bytes;
    ssize_t     n, nread = 0;
    sky_bytes_t source;
    struct stat st;

    SKY_ASSET_BLOCK_BEGIN {
        if ((fd = open(filename, O_RDONLY)) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }
        sky_asset_save(&fd, close_fd, SKY_ASSET_CLEANUP_ALWAYS);

        if (fstat(fd, &st) == -1) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        bytes = sky_asset_malloc(st.st_size + 1, SKY_ASSET_CLEANUP_ON_ERROR);
        while (nread < st.st_size) {
            n = read(fd, bytes + nread, st.st_size - nread);
            if (n < 0) {
                if (EINTR == errno) {
                    continue;
                }
                sky_error_raise_errno(sky_OSError, errno);
            }
            nread += n;
        }

        bytes[st.st_size] = 0;
        source = sky_bytes_createwithbytes(bytes, st.st_size, sky_free);
    } SKY_ASSET_BLOCK_END;

    return source;
}


int
main(int argc, char *argv[])
{
    int                 module_name_length;
    char                *dotpy, *module_name;
    FILE                *output_file;
    sky_code_t          code;
    sky_object_t        ast, source;
    sky_string_t        input_filename;
    sky_python_parser_t parser;

    if (argc != 4) {
        fprintf(stderr,
                "usage: %s <module name> <input file> <output file>\n",
                argv[0]);
        return 1;
    }

    if (!sky_library_initialize(argv[0], argc, argv,
                                SKY_LIBRARY_INITIALIZE_FLAG_NO_MODULES))
    {
        sky_error_fatal("sky_library_initialize() FAIL");
    }

    if (!(module_name = strrchr(argv[2], '/'))) {
        module_name = argv[2];
    }
    else {
        while (*++module_name == '/');
    }
    if (!(dotpy = strstr(module_name, ".py"))) {
        module_name_length = strlen(module_name);
    }
    else {
        module_name_length = dotpy - module_name;
    }
    input_filename = sky_string_createfromformat("<frozen %s.%.*s>",
                                                 argv[1],
                                                 module_name_length,
                                                 module_name);
    source = read_source(argv[2]);

    SKY_ASSET_BLOCK_BEGIN {
        sky_python_parser_init(&parser,
                               source,
                               NULL,
                               input_filename,
                               NULL,
                               NULL,
                               0);
        sky_asset_save(&parser,
                       (sky_free_t)sky_python_parser_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        ast = sky_python_parser_file_input(&parser);
        code = sky_python_compiler_compile(ast,
                                           input_filename,
                                           SKY_PYTHON_COMPILER_MODE_FILE,
                                           0,
                                           0);
    } SKY_ASSET_BLOCK_END;

    SKY_ASSET_BLOCK_BEGIN {
        output_file = fopen(argv[3], "wb");
        sky_asset_save(output_file,
                       (sky_free_t)fclose,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_format_fprintf(output_file, "static const uint8_t sky_modules_builtin_frozen_%s_bytes[] = {\n", argv[1]);

        sky_marshal_dumpwithcallback(code, 0, freeze_output, output_file);

        sky_format_fprintf(output_file, "};\n");
        sky_format_fprintf(output_file, "#define sky_modules_builtin_frozen_%s_nbytes \\\n", argv[1]);
        sky_format_fprintf(output_file, "        sizeof(sky_modules_builtin_frozen_%s_bytes)\n", argv[1]);
    } SKY_ASSET_BLOCK_END;

    sky_library_finalize();
    return 0;
}
