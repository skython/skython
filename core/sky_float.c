/* Much of the implementation contained herein is taken from CPython 3.3. */

#include "sky_private.h"

#include <float.h>
#include <math.h>

#include <locale.h>
#if defined(HAVE_XLOCALE_H)
#   include <xlocale.h>
#endif


sky_float_format_t  sky_float_format_double_detected,
                    sky_float_format_double,
                    sky_float_format_float_detected,
                    sky_float_format_float;


typedef struct sky_float_data_s {
    double                              value;
} sky_float_data_t;

SKY_EXTERN_INLINE sky_float_data_t *
sky_float_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_float_type) {
        return (sky_float_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_float_type);
}

SKY_EXTERN_INLINE double
sky_float_double(sky_object_t object)
{
    if (SKY_OBJECT_TAG(object) == 7) {
        if (SKY_OBJECT_TAGGED_FLOAT_ZERO == object) {
            return 0.0;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_ONE == object) {
            return 1.0;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ONE == object) {
            return -1.0;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ZERO == object) {
            return -0.0;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_NAN == object) {
            return NAN;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_INF == object) {
            return INFINITY;
        }
        if (SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_INF == object) {
            return -INFINITY;
        }
    }

    return sky_float_data(object)->value;
}


sky_float_t
sky_float_abs(sky_float_t self)
{
    return sky_float_create(fabs(sky_float_double(self)));
}


sky_object_t
sky_float_add(sky_float_t self, sky_object_t other)
{
    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    return sky_float_create(sky_float_double(self) + sky_float_double(other));
}


sky_tuple_t
sky_float_as_integer_ratio(sky_float_t self)
{
    double  value = sky_float_double(self);

    int             exponent, i;
    double          fraction;
    sky_integer_t   denominator, numerator, temp;

    if (isinf(value)) {
        sky_error_raise_string(
                sky_OverflowError,
                "Cannot pass infinity to float.as_integer_ratio.");
    }
    if (isnan(value)) {
        sky_error_raise_string(
                sky_ValueError,
                "Cannot pass NaN to float.as_integer_ratio.");
    }

    fraction = frexp(value, &exponent);
    for (i = 0; i < 300 && fraction != floor(fraction); ++i) {
        fraction *= 2.0;
        --exponent;
    }

    numerator = sky_integer_createfromdouble(fraction);
    temp = sky_integer_lshift(sky_integer_one,
                              sky_integer_create(labs((long)exponent)));
    if (exponent > 0) {
        numerator = sky_integer_mul(numerator, temp);
        denominator = sky_integer_one;
    }
    else {
        denominator = temp;
    }

    return sky_tuple_pack(2, numerator, denominator);
}

const char *sky_float_as_integer_ratio_doc =
"float.as_integer_ratio() -> (int, int)\n\n"
"Return a pair of integers, whose ratio is exactly equal to the original\n"
"float and with a positive denominator.\n"
"Raise OverflowError on infinities and a ValueError on NaNs.\n\n"
">>> (10.0).as_integer_ratio()\n"
"(10, 1)\n"
">>> (0.0).as_integer_ratio()\n"
"(0, 1)\n"
">>> (-.25).as_integer_ratio()\n"
"(-1, 4)";


sky_bool_t
sky_float_bool(sky_float_t self)
{
    return (sky_float_double(self) != 0.0 ? SKY_TRUE : SKY_FALSE);
}


static sky_float_t
sky_float_clone(sky_float_t self, sky_type_t type)
{
    sky_float_t clone;

    if (sky_float_type == type) {
        if (sky_float_type == sky_object_type(self)) {
            return self;
        }
        return sky_float_create(sky_float_double(self));
    }

    clone = sky_object_allocate(type);
    sky_float_data(clone)->value = sky_float_double(self);

    return clone;
}


static sky_object_t
sky_float_compare_values(double x, double y, sky_compare_op_t compare_op)
{
    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
            return (x == y ? sky_True : sky_False);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (x != y ? sky_True : sky_False);
        case SKY_COMPARE_OP_GREATER:
            return (x > y ? sky_True : sky_False);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (x >= y ? sky_True : sky_False);
        case SKY_COMPARE_OP_LESS:
            return (x < y ? sky_True : sky_False);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (x <= y ? sky_True : sky_False);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }
    sky_error_fatal("internal error");
}


sky_object_t
sky_float_compare(sky_float_t       self,
                  sky_object_t      other,
                  sky_compare_op_t  compare_op)
{
    double  self_value = sky_float_double(self);

    int             exponent, other_sign, self_sign;
    double          fraction, integer, other_value;
    ssize_t         bit_length;
    sky_integer_t   self_integer;

    /* Comparing a float against an integer shouldn't coerce the integer to a
     * float, because it may cause an overflow error. This gets complicated ...
     */
    if (!sky_object_isa(other, sky_integer_type)) {
        if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
            return sky_NotImplemented;
        }
        other_value = sky_float_double(sky_number_float(other));
        return sky_float_compare_values(self_value, other_value, compare_op);
    }
    if (!isfinite(self_value)) {
        return sky_float_compare_values(self_value, 0.0, compare_op);
    }

    self_sign = sky_float_sign(self);
    other_sign = sky_integer_sign(other);
    if (self_sign != other_sign) {
        return sky_float_compare_values((double)self_sign,
                                        (double)other_sign,
                                        compare_op);
    }

    if ((bit_length = sky_integer_bit_length(other)) < 0) {
        /* The number of bits required to represent the integer won't fit into
         * a ssize_t.
         */
        return sky_float_compare_values((double)self_sign,
                                        (double)other_sign * 2.0,
                                        compare_op);
    }
    if (bit_length <= 48) {
        other_value = sky_integer_value_double(other);
        return sky_float_compare_values(self_value, other_value, compare_op);
    }

    /* Make self_value positive, which also requires swapping the comparison
     * operator.
     */
    if (self_sign < 0) {
        self_value = -self_value;
        switch (compare_op) {
            case SKY_COMPARE_OP_EQUAL:
            case SKY_COMPARE_OP_NOT_EQUAL:
            case SKY_COMPARE_OP_IS:
            case SKY_COMPARE_OP_IS_NOT:
            case SKY_COMPARE_OP_IN:
            case SKY_COMPARE_OP_NOT_IN:
                break;
            case SKY_COMPARE_OP_GREATER:
                compare_op = SKY_COMPARE_OP_LESS;
                break;
            case SKY_COMPARE_OP_GREATER_EQUAL:
                compare_op = SKY_COMPARE_OP_LESS_EQUAL;
                break;
            case SKY_COMPARE_OP_LESS:
                compare_op = SKY_COMPARE_OP_GREATER;
                break;
            case SKY_COMPARE_OP_LESS_EQUAL:
                compare_op = SKY_COMPARE_OP_GREATER_EQUAL;
                break;
        }
    }

    frexp(self_value, &exponent);
    if (exponent < 0 || (ssize_t)exponent < bit_length) {
        return sky_float_compare_values(1.0, 2.0, compare_op);
    }
    if ((ssize_t)exponent > bit_length) {
        return sky_float_compare_values(2.0, 1.0, compare_op);
    }

    if (other_sign < 0) {
        other = sky_number_negative(other);
    }
    fraction = modf(self_value, &integer);
    self_integer = sky_integer_createfromdouble(integer);
    if (fraction != 0.0) {
        other = sky_integer_lshift(other, sky_integer_one);
        self_integer = sky_integer_lshift(self_integer, sky_integer_one);
        self_integer = sky_integer_or(self_integer, sky_integer_one);
    }

    return (sky_object_compare(self_integer, other, compare_op) ? sky_True
                                                                : sky_False);
}


sky_complex_t
sky_float_complex(sky_float_t self)
{
    return sky_complex_create(sky_float_double(self), 0.0);
}


sky_float_t
sky_float_conjugate(sky_float_t self)
{
    return sky_float_copy(self);
}


sky_float_t
sky_float_copy(sky_float_t self)
{
    if (sky_float_type == sky_object_type(self)) {
        return self;
    }
    return sky_float_create(sky_float_double(self));
}


sky_float_t
sky_float_create(double value)
{
    sky_float_t         float_object;
    sky_float_data_t    *float_data;

    switch (fpclassify(value)) {
        case FP_INFINITE:
            return (signbit(value) ? sky_float_negative_inf : sky_float_inf);
        case FP_NAN:
            return sky_float_nan;
        case FP_ZERO:
            return (signbit(value) ? sky_float_negative_zero : sky_float_zero);
    }

    if (1.0 == value) {
        return sky_float_one;
    }
    if (-1.0 == value) {
        return sky_float_negative_one;
    }

    float_object = sky_object_allocate(sky_float_type);
    float_data = sky_float_data(float_object);
    float_data->value = value;

    return float_object;
}


sky_float_t
sky_float_createfromascii(const char *bytes, size_t nbytes)
{
    char        *nextc;
    double      value;
    const char  *c, *end;

    /* Skip leading whitespace. */
    end = bytes + nbytes;
    for (c = bytes; c < end && sky_ctype_isspace(*c); ++c);
    value = sky_util_strtod(c, end - c, &nextc);
    if (nextc == c) {
        goto parse_error;
    }
    for (c = nextc; c < end && sky_ctype_isspace(*c); ++c);
    if (c != end) {
        goto parse_error;
    }

    return sky_float_create(value);

parse_error:
    sky_error_raise_string(sky_ValueError,
                           "cannot parse string as a float");
}


sky_float_t
sky_float_createfromstring(sky_object_t source)
{
    sky_float_t result;

    if (sky_object_isa(source, sky_string_type)) {
        const char  *c, *end;

        if (!(source = sky_string_transform_numeric(source))) {
            sky_error_raise_string(sky_ValueError,
                                   "cannot parse string as a float");
        }

        c = sky_string_cstring(source);
        end = c + sky_object_len(source);
        return sky_float_createfromascii(c, end - c);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, source, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_float_createfromascii(buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static double
sky_float_divmod_impl(double dividend, double divisor, double *remainder)
{
    double  div, floordiv, mod;

    if (0.0 == divisor) {
        sky_error_raise_string(sky_ZeroDivisionError, "float divmod()");
    }
    mod = fmod(dividend, divisor);
    div = (dividend - mod) / divisor;
    if (0.0 == mod) {
        mod = copysign(0.0, divisor);
    }
    else if ((divisor < 0.0) != (mod < 0.0)) {
        mod += divisor;
        div -= 1.0;
    }
    if (0.0 == div) {
        floordiv = copysign(0.0, dividend / divisor);
    }
    else {
        floordiv = floor(div);
        if (div - floordiv > 0.5) {
            floordiv += 1.0;
        }
    }
    if (remainder) {
        *remainder = mod;
    }
    return floordiv;
}


sky_object_t
sky_float_divmod(sky_float_t self, sky_object_t other)
{
    double  quotient, remainder;

    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    quotient = sky_float_divmod_impl(sky_float_double(self),
                                     sky_float_double(other),
                                     &remainder);
    return sky_object_build("(dd)", quotient, remainder);
}


sky_object_t
sky_float_eq(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


sky_float_t
sky_float_float(sky_float_t self)
{
    return sky_float_copy(self);
}


sky_object_t
sky_float_floordiv(sky_float_t self, sky_object_t other)
{
    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    return sky_float_create(sky_float_divmod_impl(sky_float_double(self),
                                                  sky_float_double(other),
                                                  NULL));
}


typedef struct sky_float_buffer_s {
    char *                              bytes;
    ssize_t                             len;
    ssize_t                             size;
} sky_float_buffer_t;


static void
sky_float_format_output(const void *bytes,
                        ssize_t     nbytes,
             SKY_UNUSED size_t      width,
                        void *      arg)
{
    sky_float_buffer_t  *buffer = arg;

    if (buffer->len + nbytes > buffer->size) {
        buffer->bytes = sky_asset_realloc(buffer->bytes,
                                          buffer->len + nbytes,
                                          SKY_ASSET_CLEANUP_ALWAYS,
                                          SKY_ASSET_UPDATE_FIRST_CURRENT);
        buffer->size = sky_memsize(buffer->bytes);
    }
    memcpy(buffer->bytes + buffer->len, bytes, nbytes);
    buffer->len += nbytes;
}

sky_string_t
sky_float_format(sky_float_t self, sky_string_t format_spec)
{
    double  value = sky_float_double(self);

    char                        *bytes, type;
    size_t                      grouped_nbytes;
    ssize_t                     length, lpad, minimum_width, nbytes, padding,
                                rpad, spad;
    const char                  *c, *decimal_point, *dot, *grouping,
                                *thousands_sep;
    unsigned int                flags;
    struct lconv                *lc;
    sky_float_buffer_t          buffer;
    sky_unicode_char_t          sign;
    sky_string_builder_t        builder;
    sky_format_specification_t  spec;

    if (!sky_string_parseformatspecification(format_spec, &spec)) {
        return sky_object_str(self);
    }

    sign = '\0';
    flags = 0;
    if (spec.flags & SKY_FORMAT_FLAG_ALTERNATE_FORM) {
        flags |= SKY_UTIL_DTOA_ALTERNATE_FORM;
    }

    if (!spec.type) {
        /* similar to 'g', except that it prints at least one digit after the
         * decimal point.
         */
        flags |= SKY_UTIL_DTOA_ADD_DOT_0;
        if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
            type = 'r';
        }
        else {
            type = 'g';
        }
    }
    else {
        if (spec.type < 0x20 || spec.type >= 0x7F) {
            sky_error_raise_format(
                    sky_ValueError,
                    "Unknown format code '\\x%x' for object of type %#@",
                    (unsigned int)spec.type,
                    sky_type_name(sky_object_type(self)));
        }
        if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_PRECISION)) {
            spec.precision = 6;
        }
        type = (char)spec.type;
        switch ((char)spec.type) {
            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
                break;
            case 'n':
                if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
                    sky_error_raise_string(sky_ValueError,
                                           "Cannot specify ',' with 'n'");
                }
                type = 'g';
                break;
            case '%':
                /* Percentage. Multiplies the number by 100 and displays in
                 * fixed ('f') format, followed by a percent sign.
                 */
                value *= 100.0;
                type = 'f';
                break;

            default:
                sky_error_raise_format(
                        sky_ValueError,
                        "Unknown format code '%c' for object of type %#@",
                        (char)spec.type,
                        sky_type_name(sky_object_type(self)));
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_util_dtoa(value, type, spec.precision, flags);
        sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        nbytes = strlen(bytes);
        if ('%' == spec.type) {
            bytes = sky_asset_realloc(bytes,
                                      nbytes + 1,
                                      SKY_ASSET_CLEANUP_ALWAYS,
                                      SKY_ASSET_UPDATE_FIRST_CURRENT);
            bytes[nbytes++] = '%';
        }

        if (*bytes == '-') {
            sign = '-';
            ++bytes;
            --nbytes;
        }

        length = 0;
        if (sign) {
            ++length;
        }
        else if (sky_format_sign(&spec) != SKY_FORMAT_FLAG_SIGN_MINUS) {
            ++length;
            if (sky_format_sign(&spec) == SKY_FORMAT_FLAG_SIGN_PLUS) {
                sign = '+';
            }
            else {
                sign = ' ';
            }
        }

        /* Maybe add comma separators in. Use a comma for '' or 'd', but look
         * up the current locale data for 'n', which may or may not have a
         * thousands separator defined for it.
         */
        decimal_point = grouping = thousands_sep = NULL;
        if ('n' == spec.type) {
#if defined(HAVE_LOCALECONV_L)
            lc = localeconv_l(uselocale(NULL));
#else
            lc = localeconv();
#endif
            decimal_point = lc->decimal_point;
            thousands_sep = lc->thousands_sep;
            grouping = lc->grouping;
        }
        else if (spec.flags & SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS) {
            decimal_point = ".";
            thousands_sep = ",";
            grouping = "\x03";
        }
        if ((decimal_point && (*decimal_point != '.' || decimal_point[1])) ||
            (thousands_sep && grouping))
        {
            if ((spec.flags & SKY_FORMAT_FLAG_ZERO_PAD) &&
                sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED)
            {
                for (c = bytes + nbytes - 1; c >= bytes && *c != '.'; --c);
                dot = (c >= bytes ? c : NULL);
                minimum_width = spec.width - length - (nbytes - (dot - bytes));
            }
            else {
                minimum_width = 0;
            }
            sky_error_validate_debug(decimal_point != NULL);
            grouped_nbytes = nbytes - 1 + strlen(decimal_point);
            if (thousands_sep && *thousands_sep &&
                grouping && *grouping && *grouping != CHAR_MAX)
            {
                grouped_nbytes += (strlen(thousands_sep) * nbytes);
            }
            buffer.bytes = sky_asset_malloc(grouped_nbytes,
                                            SKY_ASSET_CLEANUP_ALWAYS);
            buffer.len = 0;
            buffer.size = sky_memsize(buffer.bytes);
            nbytes = sky_format_number(sky_float_format_output,
                                       &buffer,
                                       bytes,
                                       nbytes,
                                       minimum_width,
                                       decimal_point,
                                       thousands_sep,
                                       grouping);
            sky_error_validate_debug(nbytes == buffer.len);
            bytes = buffer.bytes;
        }
        length += nbytes;

        lpad = rpad = spad = 0;
        padding = spec.width - length;
        if (padding && (spec.flags & SKY_FORMAT_FLAG_SPECIFIED_WIDTH)) {
            if (!(spec.flags & SKY_FORMAT_FLAG_SPECIFIED_ALIGN)) {
                spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_ALIGN_RIGHT;
            }
            switch (sky_format_alignment(&spec)) {
                case SKY_FORMAT_FLAG_ALIGN_LEFT:
                    rpad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_RIGHT:
                    lpad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_PADDED:
                    spad = padding;
                    break;
                case SKY_FORMAT_FLAG_ALIGN_CENTER:
                    lpad = padding / 2;
                    rpad = padding - lpad;
                    break;
            }
        }

        builder = sky_string_builder_createwithcapacity(length + padding);
        if (sky_format_alignment(&spec) == SKY_FORMAT_FLAG_ALIGN_PADDED) {
            if (sign) {
                sky_string_builder_appendcodepoint(builder, sign, 1);
            }
            if (spad > 0) {
                sky_string_builder_appendcodepoint(builder, spec.fill, spad);
            }
        }
        else {
            if (lpad > 0) {
                sky_string_builder_appendcodepoint(builder, spec.fill, lpad);
            }
            if (sign) {
                sky_string_builder_appendcodepoint(builder, sign, 1);
            }
        }
        sky_string_builder_appendcodepoints(builder, bytes, nbytes, 1);
        if (rpad > 0) {
            sky_string_builder_appendcodepoint(builder, spec.fill, rpad);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


 /* For the sake of simplicity and correctness, we impose an artificial
  * limit on ndigits, the total number of hex digits in the coefficient
  * The limit is chosen to ensure that, writing exp for the exponent,
  *
  *   (1) if exp > LONG_MAX/2 then the value of the hex string is
  *   guaranteed to overflow (provided it's nonzero)
  *
  *   (2) if exp < LONG_MIN/2 then the value of the hex string is
  *   guaranteed to underflow to 0.
  *
  *   (3) if LONG_MIN/2 <= exp <= LONG_MAX/2 then there's no danger of
  *   overflow in the calculation of exp and top_exp below.
  *
  * More specifically, ndigits is assumed to satisfy the following
  * inequalities:
  *
  *   4*ndigits <= DBL_MIN_EXP - DBL_MANT_DIG - LONG_MIN/2
  *   4*ndigits <= LONG_MAX/2 + 1 - DBL_MAX_EXP
  *
  * If either of these inequalities is not satisfied, a ValueError is
  * raised.  Otherwise, write x for the value of the hex string, and
  * assume x is nonzero.  Then
  *
  *   2**(exp-4*ndigits) <= |x| < 2**(exp+4*ndigits).
  *
  * Now if exp > LONG_MAX/2 then:
  *
  *   exp - 4*ndigits >= LONG_MAX/2 + 1 - (LONG_MAX/2 + 1 - DBL_MAX_EXP)
  *                    = DBL_MAX_EXP
  *
  * so |x| >= 2**DBL_MAX_EXP, which is too large to be stored in C
  * double, so overflows.  If exp < LONG_MIN/2, then
  *
  *   exp + 4*ndigits <= LONG_MIN/2 - 1 + (
  *                      DBL_MIN_EXP - DBL_MANT_DIG - LONG_MIN/2)
  *                    = DBL_MIN_EXP - DBL_MANT_DIG - 1
  *
  * and so |x| < 2**(DBL_MIN_EXP-DBL_MANT_DIG-1), hence underflows to 0
  * when converted to a C double.
  *
  * It's easy to show that if LONG_MIN/2 <= exp <= LONG_MAX/2 then both
  * exp+4*ndigits and exp-4*ndigits are within the range of a long.
  */
sky_float_t
sky_float_fromhex(sky_type_t cls, sky_string_t string)
{
    int         digit, half_eps;
    char        *nextc;
    long        exponent, key_digit, lsb, top_exp;
    double      value;
    ssize_t     fdigits, i, ndigits;
    const char  *c, *cend, *cstart, *end, *fstart;
    sky_bool_t  negate, round_up;

    if (!sky_type_issubtype(cls, sky_float_type)) {
        sky_error_raise_string(sky_TypeError,
                               "cls argument must be float or sub-type");
    }

    if (!(string = sky_string_transform_numeric(string))) {
        goto parse_error;
    }

    /* This really shouldn't ever fail if sky_string_transform_numeric() does
     * not fail. Also, we are guaranteed that the ASCII string will be Nul
     * terminated, but we use an end pointer for speed since we know where it
     * will be.
     */
    if (!(c = sky_string_cstring(string))) {
        goto parse_error;
    }
    end = c + sky_string_len(string);
    sky_error_validate('\0' == *end);

    /* Skip leading whitespace */
    while (c < end && sky_ctype_isspace(*c)) {
        ++c;
    }

    /* Handle inf[inity], -inf[inity], nan */
    value = sky_util_strtod(c, end - c, &nextc);
    if (nextc > c && (isinf(value) || isnan(value))) {
        c = nextc;
        goto finished;
    }

    /* Handle optional sign: - or + */
    if (*c == '-') {
        negate = SKY_TRUE;
        ++c;
    }
    else {
        negate = SKY_FALSE;
        if (*c == '+') {
            ++c;
        }
    }

    /* Handle optional prefix: 0x or 0X */
    if (*c == '0' && (*(c + 1) == 'x' || (*c + 1) == 'X')) {
        c += 2;
    }

    /* Figure out the number of digits present */
    cstart = c;
    while (sky_ctype_hexdigit(*c) >= 0) {
        ++c;
    }
    fstart = c;
    if (*c != '.') {
        cend = c;
    }
    else {
        ++c;
        while (sky_ctype_hexdigit(*c) >= 0) {
            ++c;
        }
        cend = c - 1;
    }
    if (!(ndigits = cend - cstart)) {
        goto parse_error;
    }
    if (ndigits > SKY_MIN(DBL_MIN_EXP - DBL_MANT_DIG - LONG_MIN / 2,
                          LONG_MAX / 2 + 1 - DBL_MAX_EXP) / 4)
    {
        sky_error_raise_string(sky_ValueError,
                               "hexadecimal string too long to convert");
    }
    fdigits = cend - fstart;

    /* Handle optional exponent */
    if (*c != 'p' && *c != 'P') {
        exponent = 0;
    }
    else {
        exponent = strtol(++c, &nextc, 10);
        if (nextc == c) {
            goto parse_error;
        }
        c = nextc;
    }

/* for 0 <= j < ndigits, HEX_DIGIT(j) gives the j-th most significant digit */
#define HEX_DIGIT(j) sky_ctype_hexdigit(*((j) < fdigits ? cend - (j)       \
                                                        : cend - 1 - (j)))
    while (ndigits > 0 && HEX_DIGIT(ndigits - 1) == 0) {
        --ndigits;
    }
    if (!ndigits || exponent < LONG_MIN / 2) {
        value = 0.0;
        goto finished;
    }
    if (exponent > LONG_MAX / 2) {
        goto overflow_error;
    }

    /* Adjust exponent for fractional part. */
    exponent = exponent - 4 * (long)fdigits;

    /* top_exp = 1 more than exponent of most signficant bit of coefficient */
    top_exp = exponent + 4 * ((long)ndigits - 1);
    for (digit = HEX_DIGIT(ndigits - 1); digit != 0; digit /= 2) {
        ++top_exp;
    }

    /* Catch almost all non-extreme cases of overflow and underflow here */
    if (top_exp < DBL_MIN_EXP - DBL_MANT_DIG) {
        value = 0.0;
        goto finished;
    }
    if (top_exp > DBL_MAX_EXP) {
        goto overflow_error;
    }

    /* lsb = exponent of least significant bit of the *rounded* value.
     * This is top_exp - DBL_MANT_DIG unless result is subnormal.
     */
    lsb = SKY_MAX(top_exp, (long)DBL_MIN_EXP) - DBL_MANT_DIG;

    value = 0.0;
    if (exponent >= lsb) {
        /* no rounding required */
        for (i = ndigits - 1; i >= 0; --i) {
            value = (16.0 * value) + HEX_DIGIT(i);
        }
        value = ldexp(value, (int)exponent);
    }
    else {
        /* rounding required. key_digit is the index of the hex digit
         * containing the first bit to be rounded away.
         */
        half_eps = 1 << (int)((lsb - exponent - 1) % 4);
        key_digit = (lsb - exponent - 1) / 4;
        for (i = ndigits - 1; i > key_digit; --i) {
            value = (16.0 * value) + HEX_DIGIT(i);
        }
        digit = HEX_DIGIT(key_digit);
        value = (16.0 * value) + (double)(digit & (16 - 2 * half_eps));

        /* round-half-even: round up if bit lsb - 1 is 1 and at least one of
         * bits lsb, lsb - 2, lsb - 3, lsb - 4, ... is 1.
         */
        if ((digit & half_eps) != 0) {
            round_up = SKY_FALSE;
            if ((digit & (3 * half_eps - 1)) != 0 ||
                (8 == half_eps && (HEX_DIGIT(key_digit + 1) & 1) != 0))
            {
                round_up = SKY_TRUE;
            }
            else {
                for (i = key_digit - 1; i >= 0; --i) {
                    if (HEX_DIGIT(i) != 0) {
                        round_up = SKY_TRUE;
                        break;
                    }
                }
            }
            if (round_up) {
                value += (2 * half_eps);
                if (DBL_MAX_EXP == top_exp &&
                    ldexp((double)(2 * half_eps), DBL_MANT_DIG) == value)
                {
                    /* overflow corner case: pre-rounded value <
                     * 2 ** DBL_MAX_EXP; rounded = 2 ** DBL_MAX_EXP
                     */
                    goto overflow_error;
                }
            }
        }
        value = ldexp(value, (int)(exponent + 4 * key_digit));
    }

finished:
    while (c < end && sky_ctype_isspace(*c)) {
        ++c;
    }
    if (c != end) {
        goto parse_error;
    }
    if (negate) {
        value = -value;
    }
    return sky_object_call(cls, sky_object_build("(d)", value), NULL);

overflow_error:
    sky_error_raise_string(
            sky_OverflowError,
            "hexadecimal value too large to represent as a float");

parse_error:
    sky_error_raise_string(
            sky_ValueError,
            "invalid hexadecimal floating-point string");
}

static const char *sky_float_fromhex_doc =
"float.fromhex(string) -> float\n\n"
"Create a floating-point number from a hexadecimal string.\n"
">>> float.fromhex('0x1.ffffp10')\n"
"2047.984375\n"
">>> float.fromhex('-0x1p-1074')\n"
"-4.9406564584124654e-324";


sky_object_t
sky_float_ge(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_string_t
sky_float_getformat(SKY_UNUSED sky_type_t cls, sky_string_t typestr)
{
    const char          *cstring;
    sky_float_format_t  type;

    if (!(cstring = sky_string_cstring(typestr))) {
        sky_error_raise_string(
                sky_ValueError,
                "__getformat__() argument 1 must be 'double' or 'float'");
    }
    if (!strcmp(cstring, "double")) {
        type = sky_float_format_double;
    }
    else if (!strcmp(cstring, "format")) {
        type = sky_float_format_float;
    }
    else {
        sky_error_raise_string(
                sky_ValueError,
                "__getformat__() argument 1 must be 'double' or 'float'");
    }

    switch (type) {
        case SKY_FLOAT_FORMAT_UNKNOWN:
            return SKY_STRING_LITERAL("unknown");
        case SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN:
            return SKY_STRING_LITERAL("IEEE, big-endian");
        case SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN:
            return SKY_STRING_LITERAL("IEEE, little-endian");
    }

    sky_error_fatal("internal error");
}

static const char *sky_float_getformat_doc =
"float.__getformat__(typestr) -> string\n\n"
"You probably don't want to use this function.  It exists mainly to be\n"
"used in Python's test suite.\n"
"\n"
"typestr must be 'double' or 'float'.  This function returns whichever of\n"
"'unknown', 'IEEE, big-endian' or 'IEEE, little-endian' best describes the\n"
"format of floating point numbers used by the C type named by typestr.";



sky_tuple_t
sky_float_getnewargs(sky_float_t self)
{
    return sky_object_build("(d)", sky_float_double(self));
}


sky_object_t
sky_float_gt(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_GREATER);
}


uintptr_t
sky_float_hash(sky_float_t self)
{
    return sky_util_hashdouble(sky_float_double(self));
}


#define SKY_FLOAT_HEX_NBITS (DBL_MANT_DIG + 3 - (DBL_MANT_DIG + 2) % 4)

sky_string_t
sky_float_hex(sky_float_t self)
{
    double  value = sky_float_double(self);

    int     esign, e, i, shift;
    char    *c, s[(SKY_FLOAT_HEX_NBITS - 1) / 4 + 3];
    double  m;

    if (isnan(value) || isinf(value)) {
        return sky_float_repr(self);
    }

    if (0.0 == value) {
        if (copysign(1.0, value) == -1.0) {
            return SKY_STRING_LITERAL("-0x0.0p+0");
        }
        return SKY_STRING_LITERAL("0x0.0p+0");
    }

    m = frexp(fabs(value), &e);
    shift = 1 - SKY_MAX(DBL_MIN_EXP - e, 0);
    m = ldexp(m, shift);
    e -= shift;

    c = s;
    *c++ = sky_ctype_lower_hexdigits[(unsigned char)(int)m];
    m -= (int)m;
    *c++ = '.';
    for (i = 0; i < (SKY_FLOAT_HEX_NBITS - 1) / 4; ++i) {
        m *= 16.0;
        *c++ = sky_ctype_lower_hexdigits[(unsigned char)(int)m];
        m -= (int)m;
    }
    *c = '\0';

    if (e >= 0) {
        esign = '+';
    }
    else {
        esign = '-';
        e = -e;
    }

    if (value < 0.0) {
        return sky_string_createfromformat("-0x%sp%c%d", s, esign, e);
    }
    return sky_string_createfromformat("0x%sp%c%d", s, esign, e);
}

static const char *sky_float_hex_doc =
"float.hex() -> string\n\n"
"Return a hexadecimal representation of a float-point number.\n"
">>> (-0.1).hex()\n"
"'-0x1.999999999999ap-4'\n"
">>> (3.14159).hex()\n"
"'0x1.921f9f01b866ep+1'";


static sky_object_t
sky_float_imag_getter(SKY_UNUSED sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_float_zero;
}


sky_integer_t
sky_float_int(sky_float_t self)
{
    double  integral;

    modf(sky_float_double(self), &integral);
    return sky_integer_createfromdouble(integral);
}


sky_bool_t
sky_float_is_integer(sky_float_t self)
{
    double  value = sky_float_value(self);

    sky_bool_t  is_integer;

    if (!isfinite(value)) {
        return SKY_FALSE;
    }

    errno = 0;
    is_integer = (floor(value) == value ? SKY_TRUE : SKY_FALSE);
    if (errno) {
        sky_error_raise_errno(
                (errno == ERANGE ? sky_OverflowError : sky_ValueError),
                errno);
    }

    return is_integer;
}


sky_object_t
sky_float_le(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


sky_object_t
sky_float_lt(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_float_mod(sky_float_t self, sky_object_t other)
{
    double divisor, remainder;

    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    if ((divisor = sky_float_double(other)) == 0.0) {
        sky_error_raise_string(sky_ZeroDivisionError, "float modulo");
    }
    if ((remainder = fmod(sky_float_double(self), divisor)) == 0.0) {
        remainder = copysign(0.0, divisor);
    }
    else if ((divisor < 0) != (remainder < 0)) {
        remainder += divisor;
    }

    return sky_float_create(remainder);
}


sky_object_t
sky_float_mul(sky_float_t self, sky_object_t other)
{
    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    return sky_float_create(sky_float_double(self) * sky_float_double(other));
}


sky_object_t
sky_float_ne(sky_float_t self, sky_object_t other)
{
    return sky_float_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


sky_float_t
sky_float_neg(sky_float_t self)
{
    return sky_float_create(-sky_float_double(self));
}


sky_float_t
sky_float_new(sky_type_t cls, sky_object_t x)
{
    if (sky_float_type != cls) {
        if (!sky_type_issubtype(cls, sky_float_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "%@.__new__(%@): %@ is not a subtype of %@",
                                   sky_type_name(sky_float_type),
                                   sky_type_name(cls),
                                   sky_type_name(cls),
                                   sky_type_name(sky_float_type));
        }
        return sky_float_clone(sky_float_new(sky_float_type, x), cls);
    }

    if (sky_object_isa(x, sky_string_type) || sky_buffer_check(x)) {
        return sky_float_createfromstring(x);
    }
    return sky_number_float(x);
}


sky_float_t
sky_float_pos(sky_float_t self)
{
    return sky_float_copy(self);
}


sky_object_t
sky_float_pow(sky_float_t self, sky_object_t other, sky_object_t third)
{
    double      base, exponent, result;
    sky_bool_t  negate, odd;

    if (!sky_object_isnull(third)) {
        sky_error_raise_string(
                sky_TypeError,
                "pow() 3rd argument not allowed unless all arguments are integers");
    }

    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    base = sky_float_double(self);
    exponent = sky_float_double(other);
    negate = SKY_FALSE;

    if (0.0 == exponent) {
        return sky_float_create(1.0);
    }
    if (isnan(base)) {
        return sky_float_copy(self);
    }
    if (isnan(exponent)) {
        return (1.0 == base ? sky_float_copy(self) : sky_float_copy(other));
    }
    if (isinf(exponent)) {
        base = fabs(base);
        if (1.0 == base) {
            return sky_float_create(1.0);
        }
        if ((exponent > 0.0) == (base > 1.0)) {
            return sky_float_create(fabs(exponent));
        }
        return sky_float_create(0.0);
    }
    if (isinf(base)) {
        odd = (fmod(fabs(exponent), 2.0) == 1.0 ? SKY_TRUE : SKY_FALSE);
        if (exponent > 0.0) {
            return sky_float_create((odd ? base : fabs(base)));
        }
        return sky_float_create((odd ? copysign(0.0, base) : 0.0));
    }
    if (0.0 == base) {
        odd = (fmod(fabs(exponent), 2.0) == 1.0 ? SKY_TRUE : SKY_FALSE);
        if (exponent < 0.0) {
            sky_error_raise_string(sky_ZeroDivisionError,
                                   "0.0 cannot be raised to a negative power");
        }
        return (odd ? sky_float_copy(self) : sky_float_create(0.0));
    }
    if (base < 0.0) {
        /* Whether this is an error is a mess and bumps into libm bugs, so we
         * have to figure it out ourselves.
         */
        if (exponent != floor(exponent)) {
            /* Negative numbers raised to fractional powers become complex. */
            return sky_complex_pow(sky_float_complex(self), other, sky_None);
        }
        /* exponent is an exact integer, albeit perhaps a very large one.
         * Replace base by its absolute value and remember to negate the
         * result if the exponent is odd.
         */
        base = -base;
        negate = (fmod(fabs(exponent), 2.0) == 1.0 ? SKY_TRUE : SKY_FALSE);
    }
    if (1.0 == base) {
        /* (-1) ** large_integer also ends up here. Here's an extract from the
         * comments for the previous implementation explaining why this special
         * case is necessary:
         *
         * -1 raised to an exact integer should never be exceptional. Alas,
         * some libms (chiefly glibc as of early 2003) return NaN and set EDOM
         * on pow(-1, large_int) if the int doesn't happen to be representable
         * in a *C* integer. That's a bug.
         */
        return sky_float_create((negate ? -1.0 : 1.0));
    }

    errno = 0;
    result = pow(base, exponent);
    if (!errno) {
        if (isinf(result)) {
            sky_error_raise_errno(sky_OverflowError, ERANGE);
        }
    }
    if (negate) {
        result = -result;
    }
    if (errno == ERANGE) {
        if (0.0 != result) {
            sky_error_raise_errno(sky_OverflowError, ERANGE);
        }
    }
    else if (errno) {
        sky_error_raise_errno(sky_ValueError, errno);
    }

    return sky_float_create(result);
}


static sky_object_t
sky_float_real_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return self;
}


sky_string_t
sky_float_repr(sky_float_t self)
{
    double  value = sky_float_double(self);

    char            *buffer;
    sky_string_t    string;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_util_dtoa(value, 'r', 0, SKY_UTIL_DTOA_ADD_DOT_0);
        sky_asset_save(buffer, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        string = sky_string_createfrombytes(buffer,
                                            strlen(buffer),
                                            NULL,
                                            NULL);
    } SKY_ASSET_BLOCK_END;

    return string;
}


static double
sky_float_round_double_fallback(double value, ssize_t ndigits)
{
    double  pow1, pow2, y, z;

    if (ndigits >= 0) {
        if (ndigits > 22) {
            /* pow1 and pow2 are each safe from overflow, but
             * pow1 * pow2 ~= pow(10.0, ndigits) might overflow
             */
            pow1 = pow(10.0, (double)(ndigits - 22));
            pow2 = 1e22;
        }
        else {
            pow1 = pow(10.0, (double)ndigits);
            pow2 = 1.0;
        }
        /* if y overflows, then rounded value is exactly value */
        y = (value * pow1) * pow2;
        if (!isfinite(y)) {
            return value;
        }
    }
    else {
        pow1 = pow(10.0, (double)-ndigits);
        pow2 = 1.0;
        y = value / pow1;
    }

    z = round(y);
    if (fabs(y - z) == 0.5) {
        /* halfway between two integers; use round-half-even */
        z = 2.0 * round(y / 2.0);
    }

    if (ndigits >= 0) {
        z = (z / pow2) / pow1;
    }
    else {
        z *= pow1;
    }

    if (!isfinite(z)) {
        sky_error_raise_string(sky_OverflowError,
                               "overflow occurred during round");
    }

    return z;
}


static double
sky_float_round_double(double value, ssize_t ndigits)
{
    extern char *dtoa(double, int, int, int *, int *, char **);
    extern void freedtoa(void *);

    int             decpt, sign;
    char            *buf, *buf_end, *fbuf;
    double          rounded;
    size_t          fbuf_len;
    unsigned int    new_control_word, old_control_word, out_control_word;

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_system_controlfpu(0, 0, &old_control_word, NULL)) {
            rounded = sky_float_round_double_fallback(value, ndigits);
        }
        else {
            new_control_word = (old_control_word & ~(_MCW_PC | _MCW_RC)) |
                               (_PC_53 | _RC_NEAR);
            if (new_control_word != old_control_word) {
                sky_system_controlfpu(new_control_word,
                                      _MCW_PC | _MCW_RC,
                                      &out_control_word,
                                      NULL);
            }
            buf = dtoa(value, 3, ndigits, &decpt, &sign, &buf_end);
            sky_asset_save(buf, freedtoa, SKY_ASSET_CLEANUP_ALWAYS);
            if (new_control_word != old_control_word) {
                sky_system_controlfpu(old_control_word,
                                      _MCW_PC | _MCW_RC,
                                      &out_control_word,
                                      NULL);
            }

            fbuf_len = sky_format_asprintf(&fbuf,
                                           "%s0%se%d",
                                           (sign ? "-" : ""),
                                           buf,
                                           decpt - (buf_end - buf));
            sky_asset_save(fbuf, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            sky_system_controlfpu(0, 0, &old_control_word, NULL);
            new_control_word = (old_control_word & ~(_MCW_PC | _MCW_RC)) |
                               (_PC_53 | _RC_NEAR);
            if (new_control_word != old_control_word) {
                sky_system_controlfpu(new_control_word,
                                      _MCW_PC | _MCW_RC,
                                      &out_control_word,
                                      NULL);
            }
            errno = 0;
            rounded = sky_util_strtod(fbuf, fbuf_len, NULL);
            if (new_control_word != old_control_word) {
                sky_system_controlfpu(old_control_word,
                                      _MCW_PC | _MCW_RC,
                                      &out_control_word,
                                      NULL);
            }

            if (errno == ERANGE && fabs(rounded) >= 1.0) {
                sky_error_raise_string(sky_OverflowError,
                                       "rounded value too large to represent");
            }
        }
    } SKY_ASSET_BLOCK_END;

    return rounded;
}


sky_object_t
sky_float_round(sky_float_t self, sky_object_t ndigits_object)
{
    double  value = sky_float_double(self);

    double  rounded;
    ssize_t ndigits;

    if (sky_object_isnull(ndigits_object)) {
        rounded = round(value);
        if (fabs(value - rounded) == 0.5) {
            rounded = 2.0 * round(value / 2.0);
        }
        return sky_integer_createfromdouble(rounded);
    }

    ndigits = sky_integer_value(sky_number_index(ndigits_object),
                                SSIZE_MIN, SSIZE_MAX,
                                NULL);
    if (!isfinite(value)) {
        return sky_float_copy(self);
    }

    /* Deal with extreme values for ndigits. For ndigits > NDIGITS_MAX, value
     * always rounds to itself. For ndigits < NDIGITS_MIN, value always rounds
     * to +-0.0. Here 0.30103 is an upper bound for log10(2).
     */
#define NDIGITS_MAX ((ssize_t)((DBL_MANT_DIG - DBL_MIN_EXP) * 0.30103))
#define NDIGITS_MIN (-(ssize_t)((DBL_MAX_EXP + 1) * 0.30103))
    if (ndigits > NDIGITS_MAX) {
        return sky_float_copy(self);
    }
    if (ndigits < NDIGITS_MIN) {
        return sky_float_create(0.0 * value);
    }
#undef NDIGITS_MIN
#undef NDIGITS_MAX

    return sky_float_create(sky_float_round_double(value, ndigits));
}


void
sky_float_setformat(SKY_UNUSED sky_type_t   cls,
                               sky_string_t typestr,
                               sky_string_t fmt)
{
    const char          *cstring;
    sky_float_format_t  detected, *lvalue, rvalue;

    if (!(cstring = sky_string_cstring(typestr))) {
        sky_error_raise_string(
                sky_ValueError,
                "__setformat__() argument 1 must be 'double' or 'float'");
    }
    if (!strcmp(cstring, "double")) {
        lvalue = &sky_float_format_double;
        detected = sky_float_format_double_detected;
    }
    else if (!strcmp(cstring, "float")) {
        lvalue = &sky_float_format_float;
        detected = sky_float_format_float_detected;
    }
    else {
        sky_error_raise_string(
                sky_ValueError,
                "__setformat__() argument 1 must be 'double' or 'float'");
    }

    if (!(cstring = sky_string_cstring(fmt))) {
        sky_error_raise_string(
                sky_ValueError,
                "__setformat__() argument 2 must be 'unknown', "
                "'IEEE, little-endian' or 'IEEE, big-endian'");
    }
    if (!strcmp(cstring, "unknown")) {
        rvalue = SKY_FLOAT_FORMAT_UNKNOWN;
    }
    else if (!strcmp(cstring, "IEEE, big-endian")) {
        rvalue = SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN;
    }
    else if (!strcmp(cstring, "IEEE, little-endian")) {
        rvalue = SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN;
    }
    else {
        sky_error_raise_string(
                sky_ValueError,
                "__setformat__() argument 2 must be 'unknown', "
                "'IEEE, little-endian' or 'IEEE, big-endian'");
    }

    if (rvalue != SKY_FLOAT_FORMAT_UNKNOWN && rvalue != detected) {
        sky_error_raise_format(
                sky_ValueError,
                "can only set %@ format to 'unknown' or the detected platform value",
                typestr);
    }
    *lvalue = rvalue;
}

static const char *sky_float_setformat_doc =
"float.__setformat__(typestr, fmt) -> None\n\n"
"You probably don't want to use this function.  It exists mainly to be\n"
"used in Python's test suite.\n"
"\n"
"typestr must be 'double' or 'float'.  fmt must be one of 'unknown',\n"
"'IEEE, big-endian' or 'IEEE, little-endian', and in addition can only be\n"
"one of the latter two if it appears to match the underlying C reality.\n"
"\n"
"Override the automatic determination of C-level floating point type.\n"
"This affects how floats are converted to and from binary strings.";


int
sky_float_sign(sky_float_t self)
{
    double  value = sky_float_double(self);

    return (0.0 == value ? 0 : (value < 0.0 ? -1 : 1));
}


sky_object_t
sky_float_sub(sky_float_t self, sky_object_t other)
{
    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    return sky_float_create(sky_float_double(self) - sky_float_double(other));
}


sky_object_t
sky_float_truediv(sky_float_t self, sky_object_t other)
{
    double divisor;

    if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {
        return sky_NotImplemented;
    }
    other = sky_number_float(other);
    if ((divisor = sky_float_double(other)) == 0.0) {
        sky_error_raise_string(sky_ZeroDivisionError,
                               "float division by zero");
    }
    return sky_float_create(sky_float_double(self) / divisor);
}


sky_integer_t
sky_float_trunc(sky_float_t self)
{
    return sky_float_int(self);
}


double
sky_float_value(sky_float_t self)
{
    return sky_float_double(self);
}


#define SKY_FLOAT_REVERSE_BINARY(op)                            \
        sky_object_t                                            \
        sky_float_r##op(sky_float_t self, sky_object_t other)   \
        {                                                       \
            if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {        \
                return sky_NotImplemented;                      \
            }                                                   \
            other = sky_number_float(other);                    \
            return sky_float_##op(other, self);                 \
        }

#define SKY_FLOAT_REVERSE_TERNARY(op)                           \
        sky_object_t                                            \
        sky_float_r##op(sky_float_t self, sky_object_t other)   \
        {                                                       \
            if (!SKY_OBJECT_METHOD_SLOT(other, FLOAT)) {        \
                return sky_NotImplemented;                      \
            }                                                   \
            other = sky_number_float(other);                    \
            return sky_float_##op(other, self, sky_None);       \
        }

SKY_FLOAT_REVERSE_BINARY(add)
SKY_FLOAT_REVERSE_BINARY(divmod)
SKY_FLOAT_REVERSE_BINARY(floordiv)
SKY_FLOAT_REVERSE_BINARY(mod)
SKY_FLOAT_REVERSE_BINARY(mul)
SKY_FLOAT_REVERSE_TERNARY(pow)
SKY_FLOAT_REVERSE_BINARY(sub)
SKY_FLOAT_REVERSE_BINARY(truediv)


static const char sky_float_type_doc[] =
"float(x) -> floating point number\n"
"\n"
"Convert a string or number to a floating point number, if possible.";


SKY_TYPE_DEFINE_SIMPLE(float,
                       "float",
                       sizeof(sky_float_data_t),
                       NULL,
                       NULL,
                       NULL,
                       0,
                       sky_float_type_doc);

sky_float_t const sky_float_nan = SKY_OBJECT_TAGGED_FLOAT_NAN;
sky_float_t const sky_float_inf = SKY_OBJECT_TAGGED_FLOAT_INF;
sky_float_t const sky_float_negative_inf = SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_INF;
sky_float_t const sky_float_one = SKY_OBJECT_TAGGED_FLOAT_ONE;
sky_float_t const sky_float_negative_one = SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ONE;
sky_float_t const sky_float_zero = SKY_OBJECT_TAGGED_FLOAT_ZERO;
sky_float_t const sky_float_negative_zero = SKY_OBJECT_TAGGED_FLOAT_NEGATIVE_ZERO;


void
sky_float_initialize_library(void)
{
    sky_type_setattr_getset(
            sky_float_type,
            "real",
            "the real part of a complex number",
            sky_float_real_getter,
            NULL);
    sky_type_setattr_getset(
            sky_float_type,
            "imag",
            "the imaginary part of a complex number",
            sky_float_imag_getter,
            NULL);

    sky_type_setattr_builtin(
            sky_float_type,
            "as_integer_ratio",
            sky_function_createbuiltin(
                    "float.as_integer_ratio",
                    sky_float_as_integer_ratio_doc,
                    (sky_native_code_function_t)sky_float_as_integer_ratio,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_FLOAT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_float_type,
            "conjugate",
            sky_function_createbuiltin(
                    "float.conjugate",
                    "Return self, the complex conjugate of any float.",
                    (sky_native_code_function_t)sky_float_conjugate,
                    SKY_DATA_TYPE_OBJECT_FLOAT,
                    "self", SKY_DATA_TYPE_OBJECT_FLOAT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_float_type,
            "fromhex",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "float.fromhex",
                            sky_float_fromhex_doc,
                            (sky_native_code_function_t)sky_float_fromhex,
                            SKY_DATA_TYPE_OBJECT_FLOAT,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "string", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_float_type,
            "hex",
            sky_function_createbuiltin(
                    "float.hex",
                    sky_float_hex_doc,
                    (sky_native_code_function_t)sky_float_hex,
                    SKY_DATA_TYPE_OBJECT_STRING,
                    "self", SKY_DATA_TYPE_OBJECT_FLOAT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_float_type,
            "is_integer",
            sky_function_createbuiltin(
                    "float.is_integer",
                    "Return True if the float is an integer.",
                    (sky_native_code_function_t)sky_float_is_integer,
                    SKY_DATA_TYPE_BOOL,
                    "self", SKY_DATA_TYPE_OBJECT_FLOAT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_float_type,
            "__getformat__",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "float.__getformat__",
                            sky_float_getformat_doc,
                            (sky_native_code_function_t)sky_float_getformat,
                            SKY_DATA_TYPE_OBJECT_STRING,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "typestr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_float_type,
            "__setformat__",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "float.__setformat__",
                            sky_float_setformat_doc,
                            (sky_native_code_function_t)sky_float_setformat,
                            SKY_DATA_TYPE_VOID,
                            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "typestr", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            "fmt", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_float_type,
            "__trunc__",
            sky_function_createbuiltin(
                    "float.__trunc__",
                    "Return the Integral closest to x between 0 and x.",
                    (sky_native_code_function_t)sky_float_trunc,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "self", SKY_DATA_TYPE_OBJECT_FLOAT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_float_type,
            "__new__",
            (sky_native_code_function_t)sky_float_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "x", SKY_DATA_TYPE_OBJECT, sky_float_create(0.0),
            NULL);

    sky_type_setmethodslots(sky_float_type,
            "__repr__", sky_float_repr,
            "__format__", sky_float_format,
            "__lt__", sky_float_lt,
            "__le__", sky_float_le,
            "__eq__", sky_float_eq,
            "__ne__", sky_float_ne,
            "__gt__", sky_float_gt,
            "__ge__", sky_float_ge,
            "__hash__", sky_float_hash,
            "__bool__", sky_float_bool,
            "__add__", sky_float_add,
            "__sub__", sky_float_sub,
            "__mul__", sky_float_mul,
            "__truediv__", sky_float_truediv,
            "__floordiv__", sky_float_floordiv,
            "__mod__", sky_float_mod,
            "__divmod__", sky_float_divmod,
            "__pow__", sky_float_pow,
            "__radd__", sky_float_radd,
            "__rsub__", sky_float_rsub,
            "__rmul__", sky_float_rmul,
            "__rtruediv__", sky_float_truediv,
            "__rfloordiv__", sky_float_rfloordiv,
            "__rmod__", sky_float_rmod,
            "__rdivmod__", sky_float_rdivmod,
            "__rpow__", sky_float_rpow,
            "__neg__", sky_float_neg,
            "__pos__", sky_float_pos,
            "__abs__", sky_float_abs,
            "__complex__", sky_float_complex,
            "__int__", sky_float_int,
            "__float__", sky_float_float,
            "__round__", sky_float_round,
            "__getnewargs__", sky_float_getnewargs,
            NULL);


    /* We attempt to determine if this machine is using IEEE floating point
     * formats by peering at the bits of some carefully chosen values. If it
     * looks like we are on an IEEE platform, the float packing/unpacking
     * routines can just copy bits, if not they resort to arithmetic & shifts
     * and masks. The shifts & masks approach works on all finite values, but
     * what happens to infinities, NaNs and signed zeroes on packing is an
     * accident, and attempting to unpack a NaN or an infinity will raise an
     * exception.
     *
     * Note that if we're on some whacked-out platform which uses IEEE formats
     * but isn't strictly little-endian or big-endian, we will fall back to the
     * portable shifts & masks method.
     */
    sky_float_format_double_detected = SKY_FLOAT_FORMAT_UNKNOWN;
    if (sizeof(double) == 8) {
        double x = 9006104071832581.0;

        if (!memcmp(&x, "\x43\x3f\xff\x01\x02\x03\x04\x05", 8)) {
            sky_float_format_double_detected = SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN;
        }
        else if (!memcmp(&x, "\x05\x04\x03\x02\x01\xff\x3f\x43", 8)) {
            sky_float_format_double_detected = SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN;
        }
    }

    sky_float_format_float_detected = SKY_FLOAT_FORMAT_UNKNOWN;
    if (sizeof(float) == 4) {
        float   y = 16711938.0;

        if (!memcmp(&y, "\x4b\x7f\x01\x02", 4)) {
            sky_float_format_float_detected = SKY_FLOAT_FORMAT_IEEE_BIG_ENDIAN;
        }
        else if (!memcmp(&y, "\x02\x01\x7f\x4b", 4)) {
            sky_float_format_float_detected = SKY_FLOAT_FORMAT_IEEE_LITTLE_ENDIAN;
        }
    }

    sky_float_format_double = sky_float_format_double_detected;
    sky_float_format_float = sky_float_format_float_detected;
}
