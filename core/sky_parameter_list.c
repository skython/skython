#include "sky_private.h"
#include "sky_parameter_list_private.h"


static sky_bool_t
sky_parameter_list_item_compare(sky_hashtable_item_t *item, void *key)
{
    sky_parameter_list_item_t   *list_item =
                                (sky_parameter_list_item_t *)item;

    return (key == list_item->keyword ||
            sky_object_compare(key, list_item->keyword, SKY_COMPARE_OP_EQUAL));
}


static void
sky_parameter_list_item_visit(sky_hashtable_item_t *    item,
                              sky_object_visit_data_t * visit_data)
{
    sky_parameter_list_item_t   *list_item =
                                (sky_parameter_list_item_t *)item;

    sky_object_visit(list_item->keyword, visit_data);
    sky_object_visit(list_item->data_type_extra, visit_data);
}


static void
sky_parameter_list_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_parameter_list_data_t   *list_data = data;

    sky_hashtable_cleanup(&(list_data->keywords), sky_free);
}


static void
sky_parameter_list_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_parameter_list_data_t   *list_data = data;

    sky_hashtable_init(&(list_data->keywords),
                       sky_parameter_list_item_compare);

    /* Initialize both keyword_count and args_index to -1 to indicate that no
     * *args parameter has been added yet. If a *args parameter gets added,
     * keyword_count becomes 0, but args_index only gets changed if the
     * parameter is not anonymous (i.e., it has a name other than simply "*").
     */
    list_data->keyword_count = -1;
    list_data->args_index = -1;
    list_data->kws_index = -1;
}


static void
sky_parameter_list_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_parameter_list_data_t   *list_data = data;

    sky_hashtable_visit(&(list_data->keywords),
                        sky_parameter_list_item_visit,
                        visit_data);
    sky_object_visit(list_data->defaults, visit_data);
    sky_object_visit(list_data->kwdefaults, visit_data);
}


sky_parameter_list_t
sky_parameter_list_create(void)
{
    return sky_object_allocate(sky_parameter_list_type);
}

sky_parameter_list_t
sky_parameter_list_createwithparameters(const char *keyword, ...)
{
    va_list                 ap;
    sky_parameter_list_t    parameters;

    if (!keyword) {
        parameters = sky_parameter_list_create();
    }
    else {
        va_start(ap, keyword);
        parameters = sky_parameter_list_createwithparametersv(keyword, ap);
        va_end(ap);
    }

    return parameters;
}

sky_parameter_list_t
sky_parameter_list_createwithparametersv(const char *keyword, va_list ap)
{
    sky_parameter_list_t    parameters;

    parameters = sky_parameter_list_create();
    if (keyword) {
        do {
            sky_object_t    data_type_extra, value;
            sky_string_t    string;
            sky_data_type_t data_type;

            data_type = va_arg(ap, sky_data_type_t);
            value = va_arg(ap, sky_object_t);
            data_type_extra = (sky_data_type_isextraneeded(data_type)
                               ? va_arg(ap, sky_object_t)
                               : NULL);
            string = sky_string_createfrombytes(keyword,
                                                strlen(keyword),
                                                NULL,
                                                NULL);
            sky_parameter_list_add(parameters,
                                   string,
                                   data_type,
                                   value,
                                   data_type_extra);
        } while ((keyword = va_arg(ap, const char *)) != NULL);
    }

    return parameters;
}

static void
sky_parameter_list_validate_keyword(sky_string_t keyword)
{
    sky_unicode_char_t  cp;

    if (sky_string_len(keyword)) {
        cp = sky_string_charat(keyword, 0);
        if ('.' == cp) {
            /* ".0" is an argument fabricated by the compiler for generators */
            if (sky_object_compare(keyword,
                                   SKY_STRING_LITERAL(".0"),
                                   SKY_COMPARE_OP_EQUAL))
            {
                return;
            }
        }
        else if ('*' == cp) {
            if (sky_string_len(keyword) == 1) {
                return;
            }
            else {
                cp = sky_string_charat(keyword, 1);
                if ('*' == cp) {
                    keyword = sky_string_slice(keyword, 2, SSIZE_MAX, 1);
                }
                else {
                    keyword = sky_string_slice(keyword, 1, SSIZE_MAX, 1);
                }
            }
        }
        if (sky_string_isidentifier(keyword)) {
            return;
        }
    }

    sky_error_raise_string(sky_ValueError, "illegal identifier");
}

void
sky_parameter_list_add(sky_parameter_list_t list,
                       sky_string_t         keyword,
                       sky_data_type_t      data_type,
                       sky_object_t         value,
                       ...)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    ssize_t                     keyword_len;
    va_list                     ap;
    uintptr_t                   key_hash;
    sky_string_t                id;
    sky_parameter_list_item_t   *item, *list_item;

    /* Errors at this level aren't technically syntax errors. They're really C
     * programmer error at this point. However, to maintain parity with the
     * errors that are raised at parse time, raise syntax errors. Instead of
     * using sky_error_fatal().
     */

    if (list_data->kws_index != -1) {
        sky_error_raise_string(sky_ValueError,
                               "**kws must be last in a parameter list");
    }

    keyword_len = sky_string_len(keyword);
    if (sky_string_charat(keyword, 0) == '*') {
        if (keyword_len > 1 && sky_string_charat(keyword, 1) == '*') {
            if (SKY_DATA_TYPE_OBJECT_DICT != data_type) {
                sky_error_raise_string(sky_TypeError,
                                       "**kws must be dict");
            }
            if (NULL != value) {
                sky_error_raise_string(sky_ValueError,
                                       "**kws cannot have a default");
            }
            id = sky_string_slice(keyword, 2, SSIZE_MAX, 1);
            if (!sky_string_isidentifier(id)) {
                sky_error_raise_string(sky_ValueError,
                                       "illegal identifier");
            }
        }
        else {
            if (list_data->keyword_count != -1) {
                sky_error_raise_string(sky_ValueError,
                                       "duplicate *args parameter");
            }
            if (SKY_DATA_TYPE_OBJECT_TUPLE != data_type) {
                sky_error_raise_string(sky_TypeError,
                                       "*args must be tuple");
            }
            if (NULL != value) {
                sky_error_raise_string(sky_ValueError,
                                       "*args cannot have a default");
            }
            if (keyword_len > 1) {
                id = sky_string_slice(keyword, 1, SSIZE_MAX, 1);
                if (!sky_string_isidentifier(id)) {
                    sky_error_raise_string(sky_ValueError,
                                           "illegal identifier");
                }
            }
            else {
                list_data->keyword_count = 0;
                return;
            }
        }
    }
    else if (!value) {
        if ((-1 == list_data->keyword_count && list_data->defaults) ||
            (list_data->keyword_count >= 0 && list_data->kwdefaults))
        {
            sky_error_raise_string(
                    sky_ValueError,
                    "non-default parameter follows default parameter");
        }
    }
    sky_parameter_list_validate_keyword(keyword);

    SKY_ASSET_BLOCK_BEGIN {
        list_item = sky_asset_calloc(1, sizeof(sky_parameter_list_item_t),
                                     SKY_ASSET_CLEANUP_ON_ERROR);
        list_item->index = sky_hashtable_length(&(list_data->keywords));
        sky_object_gc_set(SKY_AS_OBJECTP(&(list_item->keyword)),
                          sky_string_intern(keyword),
                          list);
        if (sky_None == value) {
            data_type |= SKY_DATA_TYPE_OBJECT_OR_NONE;
        }
        list_item->data_type = data_type;
        if (sky_data_type_isextraneeded(data_type)) {
            va_start(ap, value);
            sky_object_gc_set(SKY_AS_OBJECTP(&(list_item->data_type_extra)),
                              va_arg(ap, sky_object_t),
                              list);
            va_end(ap);
        }

        /* It's a nice idea to validate the default type against the type
         * expected, but it doesn't work in practice, because all of the checks
         * for early initialization will fail.
         */
#if !defined(NDEBUG) && 0
        if (value && sky_NotSpecified != value &&
            !sky_data_type_check(list_item->data_type,
                                 value,
                                 list_item->data_type_extra))
        {
            sky_error_fatal("default value is invalid data type");
        }
#endif

        key_hash = sky_object_hash(list_item->keyword);
        item = (sky_parameter_list_item_t *)
               sky_hashtable_insert(&(list_data->keywords),
                                    &(list_item->base),
                                    key_hash,
                                    list_item->keyword);
        if (item != list_item) {
            sky_free(list_item);
            if (!item) {
                sky_error_raise_object(sky_MemoryError, sky_None);
            }
            sky_error_raise_format(sky_ValueError,
                                   "duplicate parameter %#@",
                                   list_item->keyword);
        }
        if (!list_data->first_keyword) {
            list_data->first_keyword = list_item;
        }
        else {
            list_data->last_keyword->next = list_item;
        }
        list_data->last_keyword = list_item;

        if (sky_string_charat(keyword, 0) == '*') {
            if (keyword_len > 1 && sky_string_charat(keyword, 1) == '*') {
                list_data->kws_index = list_item->index;
            }
            else {
                list_data->keyword_count = 0;
                if (keyword_len > 1) {
                    list_data->args_index = list_item->index;
                }
            }
        }
        else if (list_data->keyword_count >= 0) {
            ++list_data->keyword_count;
        }
        else {
            ++list_data->positional_count;
        }
    } SKY_ASSET_BLOCK_END;

    if (value) {
        if (list_data->keyword_count == -1) {
            if (!list_data->defaults) {
                sky_object_gc_set(&(list_data->defaults),
                                  sky_list_create(NULL),
                                  list);
            }
            else {
                sky_object_gc_set(&(list_data->defaults),
                                  sky_list_create(list_data->defaults),
                                  list);
            }
            sky_list_append(list_data->defaults, value);
        }
        else {
            if (!list_data->kwdefaults) {
                sky_object_gc_set(SKY_AS_OBJECTP(&(list_data->kwdefaults)),
                                  sky_dict_create(),
                                  list);
            }
            sky_dict_setitem(list_data->kwdefaults, list_item->keyword, value);
        }
    }
}


sky_tuple_t
sky_parameter_list_defaults(sky_parameter_list_t list)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    if (!sky_object_bool(list_data->defaults)) {
        return sky_tuple_empty;
    }
    return sky_tuple_create(list_data->defaults);
}


void
sky_parameter_list_setdefaults(sky_parameter_list_t list,
                               sky_tuple_t          defaults)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    if (!defaults || sky_None == (sky_object_t)defaults) {
        list_data->defaults = NULL;
    }
    else {
        defaults = sky_tuple_create(defaults);
        if (sky_tuple_empty == defaults) {
            list_data->defaults = NULL;
        }
        else {
            sky_object_gc_set(&(list_data->defaults), defaults, list);
        }
    }
}


sky_dict_t
sky_parameter_list_kwdefaults(sky_parameter_list_t list)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    if (sky_object_isnull(list_data->kwdefaults)) {
        return sky_dict_create();
    }
    return list_data->kwdefaults;
}


void
sky_parameter_list_setkwdefaults(sky_parameter_list_t   list,
                                 sky_dict_t             kwdefaults)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    if (!kwdefaults || sky_None == (sky_object_t)kwdefaults) {
        list_data->kwdefaults = NULL;
    }
    else {
        if (!sky_object_isa(kwdefaults, sky_dict_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "__kwdefaults__ must be dict; got %#@",
                                   sky_type_name(sky_object_type(kwdefaults)));
        }
        if (!sky_dict_len(kwdefaults)) {
            list_data->kwdefaults = NULL;
        }
        else {
            sky_object_gc_set(SKY_AS_OBJECTP(&(list_data->kwdefaults)),
                              kwdefaults,
                              list);
        }
    }
}


ssize_t
sky_parameter_list_len(sky_parameter_list_t list)
{
    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    return sky_hashtable_length(&(list_data->keywords));
}


static void SKY_NORETURN
sky_parameter_list_apply_missing(sky_string_t   name,
                                 sky_list_t     names,
                                 const char *   kind)
{
    ssize_t                 names_len;
    sky_string_t            names_string;
    sky_string_builder_t    builder;

    names_len = sky_list_len(names);
    switch (names_len) {
        case 1:
            names_string = sky_list_get(names, 0);
            break;
        case 2:
            names_string = sky_string_createfromformat("%@ and %@",
                                                       sky_list_get(names, 0),
                                                       sky_list_get(names, 1));
            break;
        default:
            builder = sky_string_builder_create();
            sky_string_builder_appendjoined(builder, 
                                            sky_list_slice(names, 0, -1, 1),
                                            SKY_STRING_LITERAL(", "));
            sky_string_builder_append(builder, SKY_STRING_LITERAL(", and "));
            sky_string_builder_append(builder, sky_list_get(names, -1));
            names_string = sky_string_builder_finalize(builder);
            break;
    }

    sky_error_raise_format(
            sky_TypeError,
            "%@() missing %zd required %s argument%s: %@",
            name,
            names_len,
            kind,
            (names_len == 1 ? "" : "s"),
            names_string);
}


void **
sky_parameter_list_apply(sky_parameter_list_t   list,
                         sky_tuple_t            args,
                         sky_dict_t             kws,
                         sky_string_t           name)
{
    static const size_t argument_size = SKY_MAX(sizeof(double),
                                                SKY_MAX(sizeof(uintmax_t),
                                                        sizeof(void *)));

    sky_parameter_list_data_t   *list_data = sky_parameter_list_data(list);

    char                        *argument;
    void                        **arguments;
    ssize_t                     i, parameter_count, required_positional,
                                supplied_positional;
    sky_dict_t                  kwargs;
    sky_list_t                  missing_keywords, missing_positional;
    sky_object_t                key, object;
    sky_parameter_list_item_t   *parameter;

    parameter_count = sky_hashtable_length(&(list_data->keywords));
    if (-1 == list_data->args_index &&
        args && sky_tuple_len(args) > list_data->positional_count)
    {
        sky_error_raise_format(
                sky_TypeError,
                "%@() takes %zd positional arguments but %zd were given",
                name,
                list_data->positional_count,
                sky_tuple_len(args));
    }

    supplied_positional = (args ? sky_object_len(args) : 0);
    required_positional = list_data->positional_count;
    if (list_data->defaults) {
        if (!sky_object_isa(list_data->defaults, sky_tuple_type)) {
            sky_object_gc_set(&(list_data->defaults),
                              sky_tuple_create(list_data->defaults),
                              list);
        }
        /* This may go negative if there are too many defaults */
        required_positional -= sky_tuple_len(list_data->defaults);
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (parameter_count) {
            arguments = sky_asset_calloc(parameter_count,
                                         argument_size + sizeof(void *),
                                         SKY_ASSET_CLEANUP_ON_ERROR);
            argument = (char *)arguments + (parameter_count * sizeof(void *));
        }
        else {
            arguments = NULL;
            argument = NULL;
        }

        if (args) {
            for (parameter = list_data->first_keyword;
                 parameter &&
                 parameter->index < list_data->positional_count &&
                 parameter->index < supplied_positional;
                 parameter = parameter->next)
            {
                arguments[parameter->index] = argument;
                sky_data_type_store(parameter->data_type,
                                    NULL,
                                    sky_tuple_get(args, parameter->index),
                                    argument,
                                    parameter->data_type_extra);
                argument += argument_size;
            }
            if (list_data->args_index >= 0) {
                arguments[list_data->args_index] = argument;
                sky_object_gc_set(
                        (sky_object_t *)argument,
                        sky_tuple_slice(args,
                                        list_data->args_index,
                                        sky_tuple_len(args),
                                        1),
                        NULL);
                argument += argument_size;
            }
        }
        else if (list_data->args_index >= 0) {
            arguments[list_data->args_index] = argument;
            sky_object_gc_set((sky_object_t *)argument, sky_tuple_empty, NULL);
            argument += argument_size;
        }

        if (list_data->kws_index < 0) {
            kwargs = NULL;
        }
        else {
            kwargs = sky_dict_create();
            arguments[list_data->kws_index] = argument;
            sky_object_gc_set((sky_object_t *)argument, kwargs, NULL);
            argument += argument_size;
        }
        if (kws) {
            /* For each key in kws, look up the parameter in the
             * list_data->keywords hash table to get the parameter data.
             * Store the value from kws in the proper arguments slot.
             */
            SKY_SEQUENCE_FOREACH(kws, key) {
                object = sky_dict_getitem(kws, key);
                if (!sky_object_isa(key, sky_string_type)) {
                    sky_error_raise_string(sky_TypeError,
                                           "keywords must be strings");
                }
                parameter = (sky_parameter_list_item_t *)
                            sky_hashtable_lookup(&(list_data->keywords),
                                                 sky_object_hash(key),
                                                 key);
                if (!parameter) {
                    if (!kwargs) {
                        sky_error_raise_format(
                                sky_TypeError,
                                "%@() got an unexpected keyword argument %#@",
                                name,
                                key);
                    }
                    sky_dict_setitem(kwargs, key, object);
                    continue;
                }
                if (arguments[parameter->index]) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "%@() got multiple values for argument %#@",
                            name,
                            key);
                }
                arguments[parameter->index] = argument;
                sky_data_type_store(parameter->data_type,
                                    NULL,
                                    object,
                                    argument,
                                    parameter->data_type_extra);
                argument += argument_size;
            } SKY_SEQUENCE_FOREACH_END;
        }

        sky_error_validate_debug(list_data->args_index < 0 ||
                                 arguments[list_data->args_index] != NULL);
        sky_error_validate_debug(list_data->kws_index < 0 ||
                                 arguments[list_data->kws_index] != NULL);

        /* Not all of the arguments may be filled. Apply defaults to empty
         * slots as appropriate. If there's still an empty slot, raise a
         * sky_TypeError exception for a missing argument.
         */
        missing_keywords = missing_positional = NULL;
        for (i = 0, parameter = list_data->first_keyword;
             i < parameter_count;
             ++i, parameter = parameter->next)
        {
            if (arguments[i]) {
                continue;
            }
            if (i < list_data->positional_count) {
                if (i >= required_positional) {
                    /* Get the default value from list_data->defaults, which is
                     * guaranteed to be a non-empty tuple at this point, and the
                     * default value is guaranteed to be there.
                     */
                    arguments[i] = argument;
                    object = sky_tuple_get(list_data->defaults,
                                           -(list_data->positional_count - i));
                    sky_data_type_store(
                            parameter->data_type,
                            NULL,
                            object,
                            argument,
                            parameter->data_type_extra);
                    argument += argument_size;
                }
                else {
                    if (!missing_positional) {
                        missing_positional = sky_list_create(NULL);
                    }
                    sky_list_append(missing_positional,
                                    sky_object_repr(parameter->keyword));
                }
                continue;
            }

            if (list_data->kwdefaults &&
                (object = sky_dict_get(list_data->kwdefaults,
                                       parameter->keyword,
                                       NULL)) != NULL)
            {
                arguments[i] = argument;
                sky_data_type_store(parameter->data_type,
                                    NULL,
                                    object,
                                    argument,
                                    parameter->data_type_extra);
                argument += argument_size;
            }
            else {
                if (!missing_keywords) {
                    missing_keywords = sky_list_create(NULL);
                }
                sky_list_append(missing_keywords,
                                sky_object_repr(parameter->keyword));
            }
        }
        if (missing_positional) {
            sky_parameter_list_apply_missing(name,
                                             missing_positional,
                                             "positional");
        }
        if (missing_keywords) {
            sky_parameter_list_apply_missing(name,
                                             missing_keywords,
                                             "keyword-only");
        }
    } SKY_ASSET_BLOCK_END;

    return arguments;
}


SKY_TYPE_DEFINE_SIMPLE(parameter_list,
                       "parameter_list",
                       sizeof(sky_parameter_list_data_t),
                       sky_parameter_list_instance_initialize,
                       sky_parameter_list_instance_finalize,
                       sky_parameter_list_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_parameter_list_initialize_library(void)
{
    sky_type_setattr_builtin(sky_parameter_list_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_parameter_list_type,
            "__len__", sky_parameter_list_len,
            NULL);
}
