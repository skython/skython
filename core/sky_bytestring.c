#include "sky_private.h"
#include "sky_bytestring.h"


/* Notes:
 *  -   search (count, find, rfind) are based on code from CPython. See
 *      http://effbot.org/zone/stringlib.htm for details
 *  -   translate documentation states that the table is mandatory, but the
 *      CPython implementation allows it to be specified as None. The
 *      implementation here follows the documentation.
 */


SKY_EXTERN_INLINE unsigned long
sky_bytestring_mask(uint8_t b)
{
    return 1UL << (b & ((sizeof(unsigned long) * 8) - 1));
}


static ssize_t
sky_bytestring_range(const sky_bytestring_t *self, ssize_t *start, ssize_t *end)
{
    if (*start < 0) {
        *start += self->used;
        if (*start < 0) {
            *start = 0;
        }
    }
    if (*end < 0) {
        *end += self->used;
        if (*end < 0) {
            *end = 0;
        }
    }
    else if (*end > self->used) {
        *end = self->used;
    }
    return (*start >= *end ? 0 : *end - *start);
}


void
sky_bytestring_appendbytes(sky_bytestring_t *   self,
                           const void *         bytes,
                           size_t               nbytes)
{
    if (nbytes > SSIZE_MAX || SSIZE_MAX - self->used - 1 < (ssize_t)nbytes) {
        sky_error_raise_string(sky_OverflowError, "result too big");
    }
    sky_bytestring_resize(self, self->used + nbytes);
    memcpy(self->bytes + self->used, bytes, nbytes);
    self->used += nbytes;
    self->bytes[self->used] = '\0';
}


void
sky_bytestring_cleanup(sky_bytestring_t *self)
{
    if (self->bytes && self->bytes != self->u.tiny_bytes && self->u.free) {
        self->u.free(self->bytes);
    }
    self->bytes = NULL;
    self->used = 0;
    self->size = 0;
    self->u.free = NULL;
}


sky_bool_t
sky_bytestring_capitalize(const sky_bytestring_t *  self,
                          sky_bytestring_t *        target)
{
    ssize_t i;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used && !sky_ctype_islower(self->bytes[0])) {
        return SKY_FALSE;
    }

    sky_bytestring_resize(target, self->used);
    target->bytes[0] = sky_ctype_toupper(self->bytes[0]);
    for (i = 1; i < self->used; ++i) {
        target->bytes[i] = sky_ctype_tolower(self->bytes[i]);
    }
    target->used = self->used;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_center(const sky_bytestring_t *  self,
                      ssize_t                   width,
                      uint8_t                   fillchar,
                      sky_bytestring_t *        target)
{
    ssize_t head, margin, tail;

    if (width <= self->used) {
        return SKY_FALSE;
    }

    margin = width - self->used;
    head = (margin / 2) + (margin & width & 1);
    tail = margin - head;

    sky_bytestring_resize(target, width);
    memmove(target->bytes + head, self->bytes, self->used);
    memset(target->bytes, fillchar, head);
    memset(target->bytes + head + self->used, fillchar, tail);
    target->used = width;

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_compare(const sky_bytestring_t * self,
                       const sky_bytestring_t * other,
                       sky_compare_op_t         compare_op)
{
    int     cmprc;
    ssize_t used;

    if (self->used == other->used) {
        cmprc = memcmp(self->bytes, other->bytes, self->used);
    }
    else if (SKY_COMPARE_OP_NOT_EQUAL == compare_op) {
        return SKY_TRUE;
    }
    else {
        used = SKY_MIN(self->used, other->used);
        if (!(cmprc = memcmp(self->bytes, other->bytes, used))) {
            cmprc = (self->used < other->used ? -1 : 1);
        }
    }

    switch (compare_op) {
        case SKY_COMPARE_OP_LESS:
            return (cmprc < 0);
        case SKY_COMPARE_OP_LESS_EQUAL:
            return (cmprc <= 0);
        case SKY_COMPARE_OP_EQUAL:
            return (cmprc == 0);
        case SKY_COMPARE_OP_NOT_EQUAL:
            return (cmprc != 0);
        case SKY_COMPARE_OP_GREATER:
            return (cmprc > 0);
        case SKY_COMPARE_OP_GREATER_EQUAL:
            return (cmprc >= 0);

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }

    sky_error_fatal("internal error");
}


void
sky_bytestring_concat(sky_bytestring_t *self, const sky_bytestring_t *other)
{
    sky_bytestring_resize(self, self->used + other->used);
    memcpy(self->bytes + self->used, other->bytes, other->used);
    self->used += other->used;
    self->bytes[self->used] = '\0';
}


ssize_t
sky_bytestring_count(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   sub,
                     ssize_t                    start,
                     ssize_t                    end,
                     ssize_t                    maxcount)
{
    ssize_t         count, i, j, length, skip, sub_last;
    const uint8_t   *b, *endb, *startb;
    unsigned long   mask;

    if (!maxcount) {
        return 0;
    }
    if (maxcount < 0) {
        maxcount = SSIZE_MAX;
    }

    length = sky_bytestring_range(self, &start, &end);
    if (start > self->used) {
        return 0;
    }
    if (!sub->used) {
        return (length < maxcount ? length + 1 : maxcount);
    }
    if (!length || length < sub->used) {
        return 0;
    }

    count = 0;
    startb = &(self->bytes[start]);

    if (1 == sub->used) {
        endb = startb + length;
        while (startb < endb) {
            if (!(b = memchr(startb, sub->bytes[0], endb - startb))) {
                break;
            }
            if (++count >= maxcount) {
                return count;
            }
            startb = b + 1;
        }
    }
    else {
        sub_last = sub->used - 1;
        skip = sub_last - 1;
        mask = 0;

        for (i = 0; i < sub_last; ++i) {
            mask |= sky_bytestring_mask(sub->bytes[i]);
            if (sub->bytes[i] == sub->bytes[sub_last]) {
                skip = sub_last - i - 1;
            }
        }
        mask |= sky_bytestring_mask(sub->bytes[sub_last]);

        length -= sub->used;
        for (i = 0; i <= length; ++i) {
            if (startb[i + sub->used - 1] == sub->bytes[sub->used - 1]) {
                for (j = 0; j < sub_last; ++j) {
                    if (startb[i + j] != sub->bytes[j]) {
                        break;
                    }
                }
                if (j == sub_last) {
                    if (++count >= maxcount) {
                        return count;
                    }
                    i += sub_last;
                }
                else if (!(mask & sky_bytestring_mask(startb[i + sub->used]))) {
                    i += sub->used;
                }
                else {
                    i += skip;
                }
            }
            else if (!(mask & sky_bytestring_mask(startb[i + sub->used]))) {
                i += sub->used;
            }
        }
    }

    return count;
}


sky_string_t
sky_bytestring_decode(const sky_bytestring_t *  self,
                      sky_string_t              encoding,
                      sky_string_t              errors)
{
    return sky_string_createfrombytes(self->bytes,
                                      self->used,
                                      encoding,
                                      errors);
}


void
sky_bytestring_delete(sky_bytestring_t *self,
                      ssize_t           start,
                      ssize_t           end,
                      ssize_t           step)
{
    size_t  j;
    ssize_t i, length;

    if (step > 0) {
        length = (end - start - 1) / step + 1;
    }
    else {
        length = (end - start + 1) / step + 1;
        end = start + 1;
        start = end + step * (length - 1) - 1;
        step = -step;
    }
    if (start >= end) {
        return;
    }

    if (1 == step) {
        memmove(self->bytes + start, self->bytes + end, self->used - end);
    }
    else {
        for (i = 0, j = start; i < length; ++i, j += step) {
            if (likely(j + step < (size_t)self->used)) {
                memmove(self->bytes + j - i, self->bytes + j + 1, step - 1);
            }
            else {
                memmove(self->bytes + j - i,
                        self->bytes + j + 1,
                        self->used - j - 1);
            }
        }
        if ((j = start + (length * step)) < (size_t)self->used) {
            memmove(self->bytes + j - length, self->bytes + j, self->used - j);
        }
    }
    self->used -= length;
}


void
sky_bytestring_destroy(sky_bytestring_t *self)
{
    if (self->bytes && self->bytes != self->u.tiny_bytes && self->u.free) {
        self->u.free(self->bytes);
    }
    sky_free(self);
}


sky_bool_t
sky_bytestring_endswith(const sky_bytestring_t *self,
                        const sky_bytestring_t *suffix,
                        ssize_t                 start,
                        ssize_t                 end)
{
    ssize_t length;

    length = sky_bytestring_range(self, &start, &end);
    if (start > self->used || length < suffix->used) {
        return SKY_FALSE;
    }
    if (!length) {
        return SKY_TRUE;
    }

    return (!memcmp(&(self->bytes[end - suffix->used]),
                    suffix->bytes,
                    suffix->used) ? SKY_TRUE : SKY_FALSE);
}


sky_bool_t
sky_bytestring_expandtabs(const sky_bytestring_t *  self,
                          ssize_t                   tabsize,
                          sky_bytestring_t *        target)
{
    ssize_t         i, size, x;
    uint8_t         *outb;
    sky_bool_t      has_tabs;
    const uint8_t   *b, *endb;

    /* While most of the functions in this translation unit are written to be
     * able to work in-place, this function is not.
     */
    sky_error_validate_debug(self != target);

    if (tabsize < 0) {
        sky_error_raise_string(sky_ValueError, "negative tab size");
    }
    if (!self->used) {
        return SKY_FALSE;
    }

    i = size = 0;
    has_tabs = SKY_FALSE;
    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (*b == '\t') {
            has_tabs = SKY_TRUE;
            if (tabsize > 0) {
                x = (tabsize - (i % tabsize));
                if (size > SSIZE_MAX - x) {
                    sky_error_raise_string(sky_OverflowError, "result too long");
                }
                i += x;
            }
        }
        else {
            if (size > SSIZE_MAX - 1) {
                sky_error_raise_string(sky_OverflowError, "result too long");
            }
            ++i;
            if (*b == '\n' || *b == '\r') {
                if (size > SSIZE_MAX - i) {
                    sky_error_raise_string(sky_OverflowError, "result too long");
                }
                size += i;
                i = 0;
            }
        }
    }
    if (!has_tabs) {
        return SKY_FALSE;
    }
    if (size > SSIZE_MAX - i) {
        sky_error_raise_string(sky_OverflowError, "result too long");
    }
    size += i;
    i = 0;
    outb = sky_bytestring_resize(target, size);

    for (b = self->bytes; b < endb; ++b) {
        if (*b == '\t') {
            if (tabsize > 0) {
                x = tabsize - (i % tabsize);
                memset(outb, ' ', x);
                outb += x;
                i += x;
            }
        }
        else {
            *outb++ = *b;
            ++i;
            if (*b == '\n' || *b == '\r') {
                i = 0;
            }
        }
    }

    target->used = outb - target->bytes;
    target->bytes[target->used] = '\0';
    return SKY_TRUE;
}


ssize_t
sky_bytestring_find(const sky_bytestring_t *self,
                    const sky_bytestring_t *sub,
                    ssize_t                 start,
                    ssize_t                 end)
{
    ssize_t         i, j, length, skip, sub_last;
    const uint8_t   *b, *startb;
    unsigned long   mask;

    length = sky_bytestring_range(self, &start, &end);
    if (start > self->used) {
        return -1;
    }
    if (!sub->used) {
        return start;
    }
    if (!length || length < sub->used) {
        return -1;
    }

    startb = &(self->bytes[start]);
    if (1 == sub->used) {
        b = memchr(startb, sub->bytes[0], length);
        return (b ? b - self->bytes : -1);
    }

    sub_last = sub->used - 1;
    skip = sub_last - 1;
    mask = 0;

    for (i = 0; i < sub_last; ++i) {
        mask |= sky_bytestring_mask(sub->bytes[i]);
        if (sub->bytes[i] == sub->bytes[sub_last]) {
            skip = sub_last - i - 1;
        }
    }
    mask |= sky_bytestring_mask(sub->bytes[sub_last]);

    length -= sub->used;
    for (i = 0; i <= length; ++i) {
        if (startb[i + sub->used - 1] == sub->bytes[sub->used - 1]) {
            for (j = 0; j < sub_last; ++j) {
                if (startb[i + j] != sub->bytes[j]) {
                    break;
                }
            }
            if (j == sub_last) {
                return i + start;
            }
            if (!(mask & sky_bytestring_mask(startb[i + sub->used]))) {
                i += sub->used;
            }
            else {
                i += skip;
            }
        }
        else if (!(mask & sky_bytestring_mask(startb[i + sub->used]))) {
            i += sub->used;
        }
    }

    return -1;
}


void
sky_bytestring_fromhex(sky_bytestring_t *   self,
                       const char *         hex,
                       size_t               hexlen)
{
    uint8_t     *outb;
    const char  *hexend;

    /* This may be too much if hex contains spaces, but the over-allocation
     * is better than an additional scan to get the real number since malloc
     * is probably going to over-allocate anyway.
     */
    outb = sky_bytestring_resize(self, self->used + (hexlen / 2)) + self->used;

    hexend = hex + hexlen;
    while (hex < hexend) {
        if (sky_ctype_isspace(*hex)) {
            ++hex;
        }
        else if (hexend - hex < 2 ||
                 !sky_ctype_isxdigit(hex[0]) ||
                 !sky_ctype_isxdigit(hex[1]))
        {
            sky_error_raise_string(sky_ValueError,
                                   "invalid literal for fromhex()");
        }
        else {
            *outb++ = (sky_ctype_hexdigit(hex[0]) << 4) |
                       sky_ctype_hexdigit(hex[1]);
            hex += 2;
        }
    }

    self->used = outb - self->bytes;
    self->bytes[self->used] = '\0';
}


void
sky_bytestring_initfrombytes(sky_bytestring_t * self,
                             const void *       bytes,
                             ssize_t            nbytes,
                             ssize_t            capacity)
{
    /* Always add one for capacity to include the convenience nul byte */
    ++capacity;
    if (capacity < nbytes + 1) {
        capacity = nbytes + 1;
    }

    if (capacity > (ssize_t)sizeof(self->u.tiny_bytes)) {
        self->bytes = sky_malloc(capacity);
        self->size = sky_memsize(self->bytes);
        self->u.free = sky_free;
    }
    else {
        self->bytes = self->u.tiny_bytes;
        self->size = sizeof(self->u.tiny_bytes);
    }

    if (bytes) {
        memcpy(self->bytes, bytes, nbytes);
    }
    self->bytes[nbytes] = '\0';
    self->used = nbytes;
}


void
sky_bytestring_insert(sky_bytestring_t *self,
                      ssize_t           index,
                      const void *      bytes,
                      ssize_t           nbytes)
{
    if (SSIZE_MAX - self->used <= nbytes) {
        sky_error_raise_string(sky_OverflowError, "result too big");
    }

    sky_bytestring_resize(self, self->used + nbytes);
    if (index < 0) {
        index += self->used;
        if (index < 0) {
            index = 0;
        }
    }
    if (index > self->used) {
        index = self->used;
    }
    memmove(self->bytes + index + nbytes,
            self->bytes + index,
            self->used - index);
    memcpy(self->bytes + index, bytes, nbytes);
    self->used += nbytes;
}


sky_bool_t
sky_bytestring_isalnum(const sky_bytestring_t *self)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isalnum(self->bytes[0]);
    }

    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (!sky_ctype_isalnum(*b)) {
            return SKY_FALSE;
        }
    }
    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_isalpha(const sky_bytestring_t *self)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isalpha(self->bytes[0]);
    }

    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (!sky_ctype_isalpha(*b)) {
            return SKY_FALSE;
        }
    }
    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_isdigit(const sky_bytestring_t *self)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isdigit(self->bytes[0]);
    }

    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (!sky_ctype_isdigit(*b)) {
            return SKY_FALSE;
        }
    }
    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_islower(const sky_bytestring_t *self)
{
    sky_bool_t      cased;
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_islower(self->bytes[0]);
    }

    cased = SKY_FALSE;
    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (sky_ctype_isupper(*b)) {
            return SKY_FALSE;
        }
        if (!cased && sky_ctype_islower(*b)) {
            cased = SKY_TRUE;
        }
    }

    return cased;
}


sky_bool_t
sky_bytestring_isspace(const sky_bytestring_t *self)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isspace(self->bytes[0]);
    }

    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (!sky_ctype_isspace(*b)) {
            return SKY_FALSE;
        }
    }
    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_istitle(const sky_bytestring_t *self)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isupper(self->bytes[0]);
    }

    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (sky_ctype_islower(*b)) {
            return SKY_FALSE;
        }
        if (sky_ctype_isupper(*b)) {
            while (++b < endb) {
                if (sky_ctype_isupper(*b)) {
                    return SKY_FALSE;
                }
                if (!sky_ctype_islower(*b)) {
                    break;
                }
            }
        }
    }

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_isupper(const sky_bytestring_t *self)
{
    sky_bool_t      cased;
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used) {
        return sky_ctype_isupper(self->bytes[0]);
    }

    cased = SKY_FALSE;
    endb = self->bytes + self->used;
    for (b = self->bytes; b < endb; ++b) {
        if (sky_ctype_islower(*b)) {
            return SKY_FALSE;
        }
        if (!cased && sky_ctype_isupper(*b)) {
            cased = SKY_TRUE;
        }
    }

    return cased;
}


sky_bool_t
sky_bytestring_ljust(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     uint8_t                    fillchar,
                     sky_bytestring_t *         target)
{
    if (width <= self->used) {
        return SKY_FALSE;
    }

    sky_bytestring_resize(target, width);
    if (target != self) {
        memcpy(target->bytes, self->bytes, self->used);
        target->used = self->used;
    }
    memset(target->bytes + target->used, fillchar, width - target->used);
    target->used = width;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_lower(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target)
{
    ssize_t i;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used && !sky_ctype_isupper(self->bytes[0])) {
        return SKY_FALSE;
    }

    for (i = 0; i < self->used; ++i) {
        if (sky_ctype_isupper(self->bytes[i])) {
            sky_bytestring_resize(target, self->used);
            memcpy(target->bytes, self->bytes, i);
            do {
                target->bytes[i] = sky_ctype_tolower(self->bytes[i]);
            } while (++i < self->used);
            target->used = self->used;
            target->bytes[target->used] = '\0';

            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_bytestring_lstrip(const sky_bytestring_t *  self,
                      const sky_bytestring_t *  bytes,
                      sky_bytestring_t *        target)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }

    endb = self->bytes + self->used;
    if (!bytes) {
        for (b = self->bytes; b < endb && sky_ctype_isspace(*b); ++b);
    }
    else {
        for (b = self->bytes;
             b < endb && memchr(bytes->bytes, *b, bytes->used);
             ++b);
    }
    if (b == self->bytes) {
        return SKY_FALSE;
    }
    sky_bytestring_resize(target, endb - b);
    memmove(target->bytes, b, endb - b);
    target->used = endb - b;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


void
sky_bytestring_maketrans(const sky_bytestring_t *   from,
                         const sky_bytestring_t *   to,
                         sky_bytestring_t *         target)
{
    ssize_t i;

    if (from->used != to->used) {
        sky_error_raise_string(sky_ValueError,
                               "maketrans arguments must have same length");
    }

    sky_bytestring_resize(target, 256);
    target->used = 256;
    for (i = 0; i < 256; ++i) {
        target->bytes[i] = (char)i;
    }
    for (i = from->used - 1; i >= 0; --i) {
        target->bytes[from->bytes[i]] = to->bytes[i];
    }
}


uint8_t
sky_bytestring_pop(sky_bytestring_t *self, ssize_t index)
{
    uint8_t byte;

    if (index < 0) {
        index += self->used;
    }
    if (index < 0 || index >= self->used) {
        sky_error_raise_string(sky_IndexError, "pop index out of range");
    }
    byte = self->bytes[index];
    memmove(self->bytes + index, self->bytes + index + 1, self->used - index);
    --self->used;

    return byte;
}


void
sky_bytestring_repeat(sky_bytestring_t *self, ssize_t count)
{
    ssize_t nbytes;
    uint8_t *p;

    if (count <= 0) {
        self->used = 0;
        self->bytes[0] = '\0';
    }
    else if (count > 1) {
        if ((SSIZE_MAX / count) - 1 < self->used) {
            sky_error_raise_string(sky_OverflowError, "result too big");
        }
        sky_bytestring_resize(self, self->used * count);

        nbytes = self->used;
        self->used *= count;
        self->bytes[self->used] = '\0';
        for (p = self->bytes + nbytes; --count > 0; p += nbytes) {
            memcpy(p, self->bytes, nbytes);
        }
    }
}


static sky_bool_t
sky_bytestring_replace_delete(const sky_bytestring_t *  self,
                              const sky_bytestring_t *  old,
                              ssize_t                   count,
                              sky_bytestring_t *        target)
{
    ssize_t         i, size;
    uint8_t         *outb;
    const uint8_t   *b, *endb, *startb;

    if (!(i = sky_bytestring_count(self, old, 0, self->used, count))) {
        return SKY_FALSE;
    }
    count = SKY_MIN(count, i);

    size = self->used - (count * old->used);
    outb = sky_bytestring_resize(target, size);

    startb = self->bytes;
    endb = self->bytes + self->used;
    while (count-- > 0) {
        b = self->bytes + sky_bytestring_find(self,
                                              old,
                                              startb - self->bytes,
                                              endb - self->bytes);
        memcpy(outb, startb, b - startb);
        outb += b - startb;
        startb = b + old->used;
    }
    memcpy(outb, startb, endb - startb);
    outb += endb - startb;

    target->used = outb - target->bytes;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}

static sky_bool_t
sky_bytestring_replace_insert(const sky_bytestring_t *  self,
                              const sky_bytestring_t *  new,
                              ssize_t                   count,
                              sky_bytestring_t *        target)
{
    ssize_t i, size;
    uint8_t *outb;

    if (count > self->used + 1) {
        count = self->used + 1;
    }
    if ((SSIZE_MAX - self->used) / count < new->used) {
        sky_error_raise_string(sky_OverflowError, "result too big");
    }
    size = self->used + (count * new->used);
    outb = sky_bytestring_resize(target, size);
    --count;

    if (1 == new->used) {
        *outb++ = new->bytes[0];
        for (i = 0; i < count; ++i) {
            *outb++ = self->bytes[i];
            *outb++ = new->bytes[0];
        }
    }
    else {
        memcpy(outb, new->bytes, new->used);
        outb += new->used;
        for (i = 0; i < count; ++i) {
            *outb++ = self->bytes[i];
            memcpy(outb, new->bytes, new->used);
            outb += new->used;
        }
    }
    if (i < self->used) {
        memcpy(outb, &(self->bytes[i]), self->used - i);
        outb += (self->used - i);
    }

    target->used = outb - target->bytes;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_replace(const sky_bytestring_t * self,
                       const sky_bytestring_t * old,
                       const sky_bytestring_t * new,
                       ssize_t                  count,
                       sky_bytestring_t *       target)
{
    ssize_t         i, size;
    uint8_t         *outb;
    const uint8_t   *b, *endb, *startb;

    /* While most of the functions in this translation unit are written to be
     * able to work in-place, this function is not.
     */
    sky_error_validate_debug(self != target);

    if (!count || (!old->used && !new->used)) {
        return SKY_FALSE;
    }
    if (count < 0) {
        count = SSIZE_MAX;
    }
    if (!old->used) {
        /* b'bytes'.replace(b'', b' ') -> b' b y t e s ' */
        return sky_bytestring_replace_insert(self, new, count, target);
    }
    if (!self->used) {
        return SKY_FALSE;
    }
    if (!new->used) {
        /* b' b y t e s '.replace(b' ', b'') -> b'bytes' */
        return sky_bytestring_replace_delete(self, old, count, target);
    }

    if (!(i = sky_bytestring_count(self, old, 0, self->used, count))) {
        return SKY_FALSE;
    }
    count = SKY_MIN(count, i);

    size = self->used - (count * old->used);
    if ((SSIZE_MAX - size) / count < new->used) {
        sky_error_raise_string(sky_OverflowError, "result too big");
    }
    size += (count * new->used);
    outb = sky_bytestring_resize(target, size);

    startb = self->bytes;
    endb = self->bytes + self->used;
    while (count-- > 0) {
        b = self->bytes + sky_bytestring_find(self,
                                              old,
                                              startb - self->bytes,
                                              endb - self->bytes);
        memcpy(outb, startb, b - startb);
        outb += b - startb;
        if (1 == new->used) {
            *outb++ = new->bytes[0];
        }
        else {
            memcpy(outb, new->bytes, new->used);
            outb += new->used;
        }
        startb = b + old->used;
    }
    memcpy(outb, startb, endb - startb);
    outb += endb - startb;

    target->used = outb - target->bytes;
    sky_error_validate_debug(target->used == size);
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_string_t
sky_bytestring_repr(sky_bytestring_t *  self,
                    const char *        prefix,
                    const char *        suffix)
{
    char                    hex_buffer[4];
    size_t                  hex_buffer_len;
    ssize_t                 dquote, incr, len, squote;
    const uint8_t           *c, *end, *start;
    sky_unicode_char_t      quote;
    sky_string_builder_t    builder;

    dquote = squote = 0;
    len = 2 + (prefix ? strlen(prefix) : 0) + (suffix ? strlen(suffix) : 0);
    end = self->bytes + self->used;
    for (c = self->bytes; c < end; ++c) {
        switch (*c) {
            case '\'':
                ++squote;
                incr = 1;
                break;
            case '\"':
                ++dquote;
                incr = 1;
                break;
            case '\\': case '\t': case '\r': case '\n':
                incr = 2;
                break;
            default:
                if (*c < 0x20 || *c >= 0x7F) {
                    incr = 4;
                }
                else {
                    incr = 1;
                }
                break;
        }
        if (len > SSIZE_MAX - incr) {
            sky_error_raise_string(sky_OverflowError,
                                   "bytes object is too large to make repr");
        }
        len += incr;
    }

    /* Decide which quote to use, preferring a single quote over a double
     * quote. If both are present, use the single quote, but make room to
     * escape the one(s) embedded in the middle.
     */
    quote = '\'';
    if (squote) {
        if (dquote) {
            if (len > SSIZE_MAX - squote) {
                sky_error_raise_string(sky_OverflowError,
                                       "bytes object is too large to make repr");
            }
            len += squote;
        }
        else {
            quote = '\"';
        }
    }

    builder = sky_string_builder_createwithcapacity(len);
    if (prefix) {
        sky_string_builder_appendcodepoints(builder, prefix, strlen(prefix), 1);
    }
    sky_string_builder_appendcodepoint(builder, quote, 1);

    hex_buffer[0] = '\\';
    for (c = start = self->bytes; c < end; ++c) {
        if (*c == quote || *c == '\\') {
            hex_buffer[1] = *c;
            hex_buffer_len = 2;
        }
        else if (*c == '\t') {
            hex_buffer[1] = 't';
            hex_buffer_len = 2;
        }
        else if (*c == '\n') {
            hex_buffer[1] = 'n';
            hex_buffer_len = 2;
        }
        else if (*c == '\r') {
            hex_buffer[1] = 'r';
            hex_buffer_len = 2;
        }
        else if (*c < 0x20 || *c >= 0x7F) {
            hex_buffer[1] = 'x';
            hex_buffer[2] = sky_ctype_lower_hexdigits[(*c >> 4) & 0xF];
            hex_buffer[3] = sky_ctype_lower_hexdigits[(*c     ) & 0xF];
            hex_buffer_len = 4;
        }
        else {
            continue;
        }
        if (c > start) {
            sky_string_builder_appendcodepoints(builder, start, c - start, 1);
        }
        sky_string_builder_appendcodepoints(builder,
                                            hex_buffer,
                                            hex_buffer_len,
                                            1);
        start = c + 1;
    }
    if (c > start) {
        sky_string_builder_appendcodepoints(builder,
                                            start,
                                            c - start,
                                            1);
    }

    sky_string_builder_appendcodepoint(builder, quote, 1);
    if (suffix) {
        sky_string_builder_appendcodepoints(builder, suffix, strlen(suffix), 1);
    }
    return sky_string_builder_finalize(builder);
}


uint8_t *
sky_bytestring_resize(sky_bytestring_t *self, ssize_t size)
{
    uint8_t *new_bytes;

    if (size + 1 > self->size) {
        new_bytes = memcpy(sky_malloc(size + 1), self->bytes, self->used);
        new_bytes[self->used] = '\0';
        if (self->bytes && self->bytes != self->u.tiny_bytes && self->u.free) {
            self->u.free(self->bytes);
        }
        self->bytes = new_bytes;
        self->size = sky_memsize(new_bytes);
        self->u.free = sky_free;
    }
    return self->bytes;
}


#if !defined(HAVE_MEMRCHR)
static void *
memrchr(const void *s, int c, size_t n)
{
    const uint8_t   *b, *startb = s;

    sky_error_validate_debug(c >= 0 && c <= 255);
    for (b = startb + n - 1; b >= startb; --b) {
        if (*b == (uint8_t)c) {
            return (void *)b;
        }
    }

    return NULL;
}
#endif


ssize_t
sky_bytestring_rfind(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   sub,
                     ssize_t                    start,
                     ssize_t                    end)
{
    ssize_t         i, j, length, skip, sub_last;
    const uint8_t   *b, *startb;
    unsigned long   mask;

    length = sky_bytestring_range(self, &start, &end);
    if (start > self->used) {
        return -1;
    }
    if (!sub->used) {
        return end;
    }
    if (!length || length < sub->used) {
        return -1;
    }

    if (1 == sub->used) {
        b = memrchr(&(self->bytes[start]), sub->bytes[0], length);
        return (b ? b - self->bytes : -1);
    }

    startb = &(self->bytes[start]);
    sub_last = sub->used - 1;
    skip = sub_last - 1;

    mask = sky_bytestring_mask(sub->bytes[0]);
    for (i = sub_last; i > 0; --i) {
        mask |= sky_bytestring_mask(sub->bytes[i]);
        if (sub->bytes[i] == sub->bytes[0]) {
            skip = i - 1;
        }
    }

    length -= sub->used;
    for (i = length; i >= 0; --i) {
        if (startb[i] == sub->bytes[0]) {
            for (j = sub_last; j > 0; --j) {
                if (startb[i + j] != sub->bytes[j]) {
                    break;
                }
            }
            if (!j) {
                return start + i;
            }
            if (i > 0 && !(mask & sky_bytestring_mask(startb[i - 1]))) {
                i -= sub->used;
            }
            else {
                i -= skip;
            }
        }
        else if (i > 0 && !(mask & sky_bytestring_mask(startb[i - 1]))) {
            i -= sub->used;
        }
    }

    return -1;
}


sky_bool_t
sky_bytestring_rjust(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     uint8_t                    fillchar,
                     sky_bytestring_t *         target)
{
    if (width <= self->used) {
        return SKY_FALSE;
    }

    sky_bytestring_resize(target, SKY_MAX(self->used, width));
    memmove(target->bytes + width - self->used, self->bytes, self->used);
    memset(target->bytes, fillchar, width - self->used);
    target->used = width;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_list_t
sky_bytestring_rsplit(const sky_bytestring_t *          self,
                      const sky_bytestring_t *          sep,
                      ssize_t                           maxsplit,
                      sky_bytestring_createfrombytes_t  createfrombytes)
{
    ssize_t         index;
    sky_list_t      list;
    const uint8_t   *b, *endb, *startb;

    if (maxsplit >= 0) {
        list = sky_list_createwithcapacity(SKY_MIN(maxsplit, 12));
    }
    else {
        maxsplit = SSIZE_MAX;
        list = sky_list_createwithcapacity(12);
    }

    startb = self->bytes;
    endb = self->bytes + self->used;

    if (!sep) {
        /* split by whitespace runs */
        while (maxsplit-- > 0 && endb > startb) {
            while (endb > startb && sky_ctype_isspace(*(endb - 1))) {
                --endb;
            }
            if (endb <= startb) {
                break;
            }

            for (b = endb - 1;
                 b > startb && !sky_ctype_isspace(*(b - 1));
                 --b);

            sky_list_append(list, createfrombytes(b, endb - b));
            endb = b - 1;
        }
        if (endb > startb) {
            while (endb > startb && sky_ctype_isspace(*(endb - 1))) {
                --endb;
            }
            if (endb > startb) {
                sky_list_append(list, createfrombytes(startb, endb - startb));
            }
        }
    }
    else if (!sep->used) {
        sky_error_raise_string(sky_ValueError, "empty separator");
    }
    else {
        /* split by a separator */
        while (maxsplit-- > 0 && endb > startb) {
            index = sky_bytestring_rfind(self,
                                         sep,
                                         startb - self->bytes,
                                         endb - self->bytes);
            if (index < 0) {
                break;
            }
            b = self->bytes + index + sep->used;
            sky_list_append(list, createfrombytes(b, endb - b));
            endb = b - sep->used;
        }

        if (startb <= endb) {
            sky_list_append(list, createfrombytes(startb, endb - startb));
        }
        else if (!sky_list_len(list)) {
            sky_list_append(list, createfrombytes((uint8_t *)"", 0));
        }
    }

    sky_list_reverse(list);
    return list;
}


sky_bool_t
sky_bytestring_rstrip(const sky_bytestring_t *  self,
                      const sky_bytestring_t *  bytes,
                      sky_bytestring_t *        target)
{
    const uint8_t   *b, *endb;

    if (!self->used) {
        return SKY_FALSE;
    }

    endb = self->bytes + self->used;
    if (!bytes) {
        for (b = endb; b > self->bytes && sky_ctype_isspace(*(b - 1)); --b);
    }
    else {
        for (b = endb;
             b > self->bytes && memchr(bytes->bytes, *(b - 1), bytes->used);
             --b);
    }
    if (b < endb) {
        sky_bytestring_resize(target, b - self->bytes);
        if (target != self) {
            memcpy(target->bytes, self->bytes, b - self->bytes);
        }
        target->used = b - self->bytes;
        target->bytes[target->used] = '\0';

        return SKY_TRUE;
    }

    return SKY_FALSE;
}


sky_list_t
sky_bytestring_split(const sky_bytestring_t *           self,
                     const sky_bytestring_t *           sep,
                     ssize_t                            maxsplit,
                     sky_bytestring_createfrombytes_t   createfrombytes)
{
    ssize_t             index;
    sky_list_t          list;
    const uint8_t       *b, *endb, *startb;

    if (maxsplit >= 0) {
        list = sky_list_createwithcapacity(SKY_MIN(maxsplit, 12));
    }
    else {
        maxsplit = SSIZE_MAX;
        list = sky_list_createwithcapacity(12);
    }

    startb = self->bytes;
    endb = self->bytes + self->used;

    if (!sep) {
        /* split by whitespace runs */
        while (maxsplit-- > 0 && startb < endb) {
            while (startb < endb && sky_ctype_isspace(*startb)) {
                ++startb;
            }
            if (startb >= endb) {
                break;
            }

            for (b = startb + 1; b < endb && !sky_ctype_isspace(*b); ++b);
            sky_list_append(list, createfrombytes(startb, b - startb));
            startb = b + 1;
        }
        if (startb < endb) {
            while (startb < endb && sky_ctype_isspace(*startb)) {
                ++startb;
            }
            if (startb < endb) {
                sky_list_append(list, createfrombytes(startb, endb - startb));
            }
        }
    }
    else if (!sep->used) {
        sky_error_raise_string(sky_ValueError, "empty separator");
    }
    else {
        /* split by a separator */
        while (maxsplit-- > 0 && startb < endb) {
            index = sky_bytestring_find(self,
                                        sep,
                                        startb - self->bytes,
                                        endb - self->bytes);
            if (index < 0) {
                break;
            }
            b = self->bytes + index;
            sky_list_append(list, createfrombytes(startb, b - startb));
            startb = b + sep->used;
        }
        sky_list_append(list, createfrombytes(startb, endb - startb));
    }

    return list;
}


sky_bool_t
sky_bytestring_startswith(const sky_bytestring_t *  self,
                          const sky_bytestring_t *  prefix,
                          ssize_t                   start,
                          ssize_t                   end)
{
    ssize_t length;

    length = sky_bytestring_range(self, &start, &end);
    if (start > self->used || length < prefix->used) {
        return SKY_FALSE;
    }
    if (!length) {
        return SKY_TRUE;
    }

    return (!memcmp(&(self->bytes[start]),
                    prefix->bytes,
                    prefix->used) ? SKY_TRUE : SKY_FALSE);
}


sky_bool_t
sky_bytestring_strip(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   bytes,
                     sky_bytestring_t *         target)
{
    const uint8_t   *b, *endb, *startb;

    if (!self->used) {
        return SKY_FALSE;
    }

    startb = self->bytes;
    endb = self->bytes + self->used;
    if (!bytes) {
        while (startb < endb && sky_ctype_isspace(*startb)) {
            ++startb;
        }
        for (b = endb; b > startb && sky_ctype_isspace(*(b - 1)); --b);
    }
    else {
        while (startb < endb && memchr(bytes->bytes, *startb, bytes->used)) {
            ++startb;
        }
        for (b = endb;
             b > startb && memchr(bytes->bytes, *(b - 1), bytes->used);
             --b);
    }
    if (b - startb == self->used) {
        return SKY_FALSE;
    }
    sky_bytestring_resize(target, b - startb);
    if (startb != self->bytes) {
        memmove(target->bytes, startb, b - startb);
    }
    else if (self != target) {
        memcpy(target->bytes, startb, b - startb);
    }
    target->used = b - startb;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_swapcase(const sky_bytestring_t *self,
                        sky_bytestring_t *      target)
{
    ssize_t i;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used && !sky_ctype_isalpha(self->bytes[0])) {
        return SKY_FALSE;
    }

    for (i = 0; i < self->used; ++i) {
        if (sky_ctype_isalpha(self->bytes[i])) {
            sky_bytestring_resize(target, self->used);
            memcpy(target->bytes, self->bytes, i);
            do {
                target->bytes[i] = sky_ctype_swapcase(self->bytes[i]);
            } while (++i < self->used);
            target->used = self->used;
            target->bytes[target->used] = '\0';

            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_bytestring_title(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target)
{
    ssize_t i;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used && !sky_ctype_islower(self->bytes[0])) {
        return SKY_FALSE;
    }

    for (i = 0; i < self->used; ++i) {
        if (sky_ctype_isalpha(self->bytes[i])) {
            sky_bytestring_resize(target, self->used);
            memcpy(target->bytes, self->bytes, self->used);
            do {
                target->bytes[i] = sky_ctype_toupper(self->bytes[i]);
                while (++i < self->used) {
                    if (!sky_ctype_isalpha(self->bytes[i])) {
                        do {
                            target->bytes[i] = self->bytes[i];
                        } while (++i < self->used &&
                                 !sky_ctype_isalpha(self->bytes[i]));
                        break;
                    }
                    target->bytes[i] = sky_ctype_tolower(self->bytes[i]);
                }
            } while (i < self->used);
            target->used = self->used;
            target->bytes[target->used] = '\0';

            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_bytestring_translate(const sky_bytestring_t *   self,
                         const sky_bytestring_t *   table,
                         const sky_bytestring_t *   deletechars,
                         sky_bytestring_t *         target)
{
    uint8_t         *outb;
    const uint8_t   *b, *endb;

    if (table && table->used != 256) {
        sky_error_raise_string(sky_ValueError,
                               "translation table must be 256 characters long");
    }
    if (deletechars && !deletechars->used) {
        deletechars = NULL;
    }

    if (!table && !deletechars) {
        sky_error_raise_string(
                sky_TypeError,
                "expected bytes, bytearray or buffer compatible object");
    }

    if (!self->used) {
        return SKY_FALSE;
    }

    /* This may not be ideal. There's no check to determine whether any
     * translation or deletion is actually done, so it basically always
     * returns indicating that a change was made, which may not be true.
     */

    outb = sky_bytestring_resize(target, self->used);
    b = self->bytes;
    endb = self->bytes + self->used;

    if (deletechars) {
        if (table) {
            while (b < endb) {
                if (!memchr(deletechars->bytes, *b, deletechars->used)) {
                    *outb++ = table->bytes[*b];
                }
                ++b;
            }
        }
        else {
            while (b < endb) {
                if (!memchr(deletechars->bytes, *b, deletechars->used)) {
                    *outb++ = *b;
                }
                ++b;
            }
        }
    }
    else {
        sky_error_validate_debug(table != NULL);
        while (b < endb) {
            *outb++ = table->bytes[*b++];
        }
    }
    target->used = outb - target->bytes;
    target->bytes[target->used] = '\0';

    return SKY_TRUE;
}


sky_bool_t
sky_bytestring_upper(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target)
{
    ssize_t i;

    if (!self->used) {
        return SKY_FALSE;
    }
    if (1 == self->used && !sky_ctype_islower(self->bytes[0])) {
        return SKY_FALSE;
    }

    for (i = 0; i < self->used; ++i) {
        if (sky_ctype_islower(self->bytes[i])) {
            sky_bytestring_resize(target, self->used);
            memcpy(target->bytes, self->bytes, i);
            do {
                target->bytes[i] = sky_ctype_toupper(self->bytes[i]);
            } while (++i < self->used);
            target->used = self->used;
            target->bytes[target->used] = '\0';

            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_bytestring_zfill(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     sky_bytestring_t *         target)
{
    if (width <= self->used) {
        return SKY_FALSE;
    }

    sky_bytestring_resize(target, SKY_MAX(self->used, width));
    memmove(target->bytes + width - self->used, self->bytes, self->used);
    memset(target->bytes, '0', width - self->used);
    target->used = width;
    target->bytes[target->used] = '\0';
    if (target->bytes[width - self->used] == '+' ||
        target->bytes[width - self->used] == '-')
    {
        target->bytes[0] = target->bytes[width - self->used];
        target->bytes[width - self->used] = '0';
    }

    return SKY_TRUE;
}
