#ifndef __SKYTHON_CORE_SKY_TUPLE_PRIVATE_H__
#define __SKYTHON_CORE_SKY_TUPLE_PRIVATE_H__ 1


SKY_CDECLS_BEGIN


#define SKY_TUPLE_TINY_COUNT                                        \
        ((ssize_t)((SKY_CACHE_LINE_SIZE -                           \
                    sizeof(sky_object_data_t) -                     \
                    (sizeof(ssize_t) - sizeof(sky_object_t *))) /   \
                   sizeof(sky_object_t)))


typedef struct sky_tuple_data_s {
    ssize_t                             len;
    sky_object_t *                      objects;
    union {
        sky_free_t                      free;
        sky_object_t                    tiny_objects[SKY_TUPLE_TINY_COUNT];
    } u;
} sky_tuple_data_t;


struct sky_tuple_s {
    sky_object_data_t                   object_data;
    sky_tuple_data_t                    tuple_data;
};


extern sky_tuple_data_t *
sky_tuple_data(sky_object_t object, sky_tuple_data_t *tagged_data);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TUPLE_PRIVATE_H__ */
