/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_object_types Object Types
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_OBJECT_TYPES_H__
#define __SKYTHON_CORE_SKY_OBJECT_TYPES_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** Declare the C type and Skython type descriptor to represent an object.
  *
  * @param[in]  name        the base name of the object as it is used in code
  *                         (e.g., "cond")
  * @param[in]  fullname    the descriptive name of the object as it is used in
  *                         English (e.g., "condition")
  */
#define SKY_OBJECT_TYPE_DECLARE(name, fullname)                     \
    /** The C type representing a fullname object. **/              \
    typedef struct sky_##name##_s *sky_##name##_t;                  \
    /** The Skython type object instance for a fullname object. **/ \
    SKY_EXTERN sky_type_t const sky_##name##_type


/** The C type representing an object instance. **/
typedef void *sky_object_t;


SKY_OBJECT_TYPE_DECLARE(type, type);
SKY_OBJECT_TYPE_DECLARE(base_object, base object);

SKY_OBJECT_TYPE_DECLARE(cell, cell);
SKY_OBJECT_TYPE_DECLARE(code, code);
SKY_OBJECT_TYPE_DECLARE(frame, frame);
SKY_OBJECT_TYPE_DECLARE(generator, generator);
SKY_OBJECT_TYPE_DECLARE(native_code, native code);
SKY_OBJECT_TYPE_DECLARE(parameter_list, parameter list);
SKY_OBJECT_TYPE_DECLARE(traceback, traceback);

SKY_OBJECT_TYPE_DECLARE(Ellipsis, Ellipsis);
SKY_OBJECT_TYPE_DECLARE(None, None);
SKY_OBJECT_TYPE_DECLARE(NotImplemented, NotImplemented);
SKY_OBJECT_TYPE_DECLARE(NotSpecified, NotSpecified);

SKY_OBJECT_TYPE_DECLARE(boolean, Boolean);
SKY_OBJECT_TYPE_DECLARE(bytearray, bytearray);
SKY_OBJECT_TYPE_DECLARE(bytearray_iterator, bytearray iterator);
SKY_OBJECT_TYPE_DECLARE(bytes, bytes);
SKY_OBJECT_TYPE_DECLARE(bytes_builder, bytes builder);
SKY_OBJECT_TYPE_DECLARE(bytes_iterator, bytes iterator);
SKY_OBJECT_TYPE_DECLARE(callable_iterator, callable iterator);
SKY_OBJECT_TYPE_DECLARE(complex, complex);
SKY_OBJECT_TYPE_DECLARE(dict, dictionary);
SKY_OBJECT_TYPE_DECLARE(float, float);
SKY_OBJECT_TYPE_DECLARE(frozenset, frozen set);
SKY_OBJECT_TYPE_DECLARE(integer, integer);
SKY_OBJECT_TYPE_DECLARE(iterator, iterator);
SKY_OBJECT_TYPE_DECLARE(list, list);
SKY_OBJECT_TYPE_DECLARE(mappingproxy, mapping proxy);
SKY_OBJECT_TYPE_DECLARE(memoryview, memory view);
SKY_OBJECT_TYPE_DECLARE(module, module);
SKY_OBJECT_TYPE_DECLARE(namespace, namespace);
SKY_OBJECT_TYPE_DECLARE(reversed, reverse iterator);
SKY_OBJECT_TYPE_DECLARE(set, set);
SKY_OBJECT_TYPE_DECLARE(string, string);
SKY_OBJECT_TYPE_DECLARE(string_builder, string builder);
SKY_OBJECT_TYPE_DECLARE(string_iterator, string iterator);
SKY_OBJECT_TYPE_DECLARE(tuple, tuple);
SKY_OBJECT_TYPE_DECLARE(tuple_iterator, tuple iterator);
SKY_OBJECT_TYPE_DECLARE(type_dict, type dictionary);

SKY_OBJECT_TYPE_DECLARE(callableweakproxy, callable weak reference proxy);
SKY_OBJECT_TYPE_DECLARE(capsule, capsule);
SKY_OBJECT_TYPE_DECLARE(classmethod, classmethod);
SKY_OBJECT_TYPE_DECLARE(deque, double ended queue);
SKY_OBJECT_TYPE_DECLARE(deque_iterator, deque iterator);
SKY_OBJECT_TYPE_DECLARE(deque_reverse_iterator, deque reverse iterator);
SKY_OBJECT_TYPE_DECLARE(enumerate, enumerate);
SKY_OBJECT_TYPE_DECLARE(filter, filter);
SKY_OBJECT_TYPE_DECLARE(function, function);
SKY_OBJECT_TYPE_DECLARE(getset, getset descriptor);
SKY_OBJECT_TYPE_DECLARE(map, map);
SKY_OBJECT_TYPE_DECLARE(member, member descriptor);
SKY_OBJECT_TYPE_DECLARE(method, method);
SKY_OBJECT_TYPE_DECLARE(mutex, mutex);
SKY_OBJECT_TYPE_DECLARE(property, property);
SKY_OBJECT_TYPE_DECLARE(range, range);
SKY_OBJECT_TYPE_DECLARE(slice, slice);
SKY_OBJECT_TYPE_DECLARE(socket, socket);
SKY_OBJECT_TYPE_DECLARE(staticmethod, staticmethod);
SKY_OBJECT_TYPE_DECLARE(super, super);
SKY_OBJECT_TYPE_DECLARE(thread, thread);
SKY_OBJECT_TYPE_DECLARE(weakproxy, weak reference proxy);
SKY_OBJECT_TYPE_DECLARE(weakref, weak reference);
SKY_OBJECT_TYPE_DECLARE(zip, zip);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_OBJECT_TYPES_H__ */

/** @} **/
/** @} **/
