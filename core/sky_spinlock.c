#include "sky_private.h"


void
sky_spinlock_init(sky_spinlock_t *spinlock)
{
    *spinlock = 0;
}


void
sky_spinlock_lock(volatile sky_spinlock_t *spinlock)
{
#if !defined(NDEBUG)
    sky_spinlock_t  spinlock_check;

    spinlock_check = *spinlock;
    assert(spinlock_check == 0 || spinlock_check == -1);
#endif

    do {
        while (*spinlock != 0) {
            sky_atomic_pause();
            sky_system_yield();
        }
    } while (!sky_atomic_cas32(0, -1, spinlock));
}


sky_bool_t
sky_spinlock_trylock(volatile sky_spinlock_t *spinlock)
{
#if !defined(NDEBUG)
    sky_spinlock_t  spinlock_check;

    spinlock_check = *spinlock;
    assert(spinlock_check == 0 || spinlock_check == -1);
#endif

    if (!*spinlock && !sky_atomic_cas32(0, -1, spinlock)) {
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


void
sky_spinlock_unlock(volatile sky_spinlock_t *spinlock)
{
    assert(*spinlock == -1);
    *spinlock = 0;
}
