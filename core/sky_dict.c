#include "sky_private.h"


/* These numbers assume using sky_core_malloc for memory management.
 * They also assume a cache-line size of 64 bytes. For both 32-bit and 64-bit
 * they'll end up working out to 320 bytes, leaving no waste except for the
 * cache-line padding, which cannot be avoided. Note that the numbers must also
 * be powers of 2.
 */
#if defined(SKY_ARCH_32BIT) && SKY_CACHE_LINE_SIZE == 64
#   define SKY_DICT_MINIMUM_CAPACITY        16
#   define SKY_DICT_TINY_CAPACITY           16
#elif defined(SKY_ARCH_64BIT) && SKY_CACHE_LINE_SIZE == 64
#   define SKY_DICT_MINIMUM_CAPACITY        8
#   define SKY_DICT_TINY_CAPACITY           8
#else
#   error implementation missing
#endif


static sky_type_t sky_dict_itemiterator_type = NULL;
static sky_type_t sky_dict_keyiterator_type = NULL;
static sky_type_t sky_dict_valueiterator_type = NULL;

static sky_type_t sky_dict_items_type = NULL;
static sky_type_t sky_dict_keys_type = NULL;
static sky_type_t sky_dict_values_type = NULL;


typedef struct sky_dict_item_s {
    uintptr_t                           key_hash;
    sky_object_t                        key;
    sky_object_t                        value;
} sky_dict_item_t;


typedef struct sky_dict_table_s {
    ssize_t                             len;
    ssize_t                             filled;
    size_t                              mask;
    volatile uintptr_t                  revision;

    sky_dict_item_t *                   table;

    /* Don't use SKY_CACHE_LINE_ALIGN here, because it'll screw up the
     * alignment in struct sky_data_s down below. Manage alignment manually.
     */
    char                                alignment[SKY_CACHE_LINE_SIZE -
                                                  (sizeof(ssize_t) * 2) -
                                                  sizeof(size_t) -
                                                  sizeof(uintptr_t) -
                                                  sizeof(sky_dict_item_t *)];
    sky_spinlock_t                      spinlock;
} sky_dict_table_t;


typedef struct sky_dict_data_s {
    sky_dict_table_t * volatile         table;

    /* Don't use SKY_CACHE_LINE_ALIGN here, because the type code is actually
     * placing this struct immediately after the sky_object_data_t struct in
     * the allocated object, but there's no way to tell the compiler this.
     * Manage alignment manually.
     */
    char                                alignment[SKY_CACHE_LINE_SIZE -
                                                  sizeof(sky_object_data_t) -
                                                  sizeof(sky_dict_table_t *)];

    sky_dict_item_t                     tiny_items[SKY_DICT_TINY_CAPACITY];

    sky_dict_table_t                    tiny_table;
} sky_dict_data_t;

SKY_EXTERN_INLINE sky_dict_data_t *
sky_dict_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_dict_type) {
        return (sky_dict_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_dict_type);
}


/* A table pointer is stored to check for changes during iteration, but it is
 * not otherwise used.
 */
typedef struct sky_dict_iterator_data_s {
    sky_dict_t                          dict;
    sky_dict_table_t *                  table;
    uintptr_t                           revision;
    size_t                              index;
    ssize_t                             len;
} sky_dict_iterator_data_t;

static inline sky_dict_iterator_data_t *
sky_dict_iterator_data(sky_object_t object)
{
    if (sky_object_isa(object, sky_dict_itemiterator_type)) {
        return sky_object_data(object, sky_dict_itemiterator_type);
    }
    if (sky_object_isa(object, sky_dict_keyiterator_type)) {
        return sky_object_data(object, sky_dict_keyiterator_type);
    }
    if (sky_object_isa(object, sky_dict_valueiterator_type)) {
        return sky_object_data(object, sky_dict_valueiterator_type);
    }

    sky_error_raise_format(sky_TypeError,
                           "expected 'dict_iterator'; got %#@",
                           sky_type_name(sky_object_type(object)));
}


typedef struct sky_dict_view_data_s {
    sky_dict_t                          dict;
} sky_dict_view_data_t;

static inline sky_dict_view_data_t *
sky_dict_view_data(sky_object_t object)
{
    if (sky_object_isa(object, sky_dict_items_type)) {
        return sky_object_data(object, sky_dict_items_type);
    }
    if (sky_object_isa(object, sky_dict_keys_type)) {
        return sky_object_data(object, sky_dict_keys_type);
    }
    if (sky_object_isa(object, sky_dict_values_type)) {
        return sky_object_data(object, sky_dict_values_type);
    }

    sky_error_raise_format(sky_TypeError,
                           "expected 'dict_view'; got %#@",
                           sky_type_name(sky_object_type(object)));
}


struct sky_dict_s {
    sky_object_data_t                   object_data;
    sky_dict_data_t                     dict_data;
};

typedef struct sky_dict_iterator_s {
    sky_object_data_t                   object_data;
    sky_dict_iterator_data_t            iterator_data;
} *sky_dict_itemiterator_t,
  *sky_dict_keyiterator_t,
  *sky_dict_valueiterator_t;

typedef struct sky_dict_view_s {
    sky_object_data_t                   object_data;
    sky_dict_view_data_t                view_data;
} *sky_dict_items_t,
  *sky_dict_keys_t,
  *sky_dict_values_t;


static inline sky_bool_t
sky_dict_isshared(sky_dict_t self)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(self));
}


static inline sky_bool_t
sky_dict_item_isalive(sky_dict_item_t *item)
{
    return (item->key && sky_NotSpecified != item->key && item->value);
}


#define SKY_DICT_FOREACH(__data, __table, _item)                            \
    do {                                                                    \
        sky_dict_data_t     *_data = __data;                                \
        sky_dict_table_t    *_table = __table;                              \
                                                                            \
        size_t      _index;                                                 \
        ssize_t     _count = _table->len;                                   \
        uintptr_t   _revision = _table->revision;                           \
        sky_bool_t  _break = SKY_FALSE;                                     \
                                                                            \
        for (_index = 0; _count > 0; --_count) {                            \
            do {                                                            \
                _item = &(_table->table[_index++]);                         \
            } while (!sky_dict_item_isalive(_item));                        \
            do {

#define SKY_DICT_FOREACH_BREAK                                              \
                _break = SKY_TRUE;                                          \
                break

#define SKY_DICT_FOREACH_END                                                \
            } while (0);                                                    \
            if (_break) {                                                   \
                break;                                                      \
            }                                                               \
            if (_data->table != _table || _table->revision != _revision) {  \
                sky_error_raise_string(sky_RuntimeError,                    \
                                       "dict modified during iteration");   \
            }                                                               \
        }                                                                   \
    } while (0)


#define SKY_DICT_FOREACH_SHARED(__data, __table, _item)                     \
    do {                                                                    \
        sky_dict_data_t     *_data = __data;                                \
        sky_dict_table_t    *_table = __table;                              \
                                                                            \
        size_t      _index, _mask = _table->mask;                           \
        ssize_t     _count = _table->len;                                   \
        uintptr_t   _revision = _table->revision;                           \
        sky_bool_t  _break = SKY_FALSE;                                     \
                                                                            \
        for (_index = 0; _count > 0; --_count) {                            \
            while (_index <= _mask) {                                       \
                _item = &(_table->table[_index++]);                         \
                if (_data->table != _table ||                               \
                    _revision != _table->revision)                          \
                {                                                           \
                    sky_error_raise_string(                                 \
                            sky_RuntimeError,                               \
                            "dict modified during iteration");              \
                }                                                           \
                if (sky_dict_item_isalive(_item)) {                         \
                    break;                                                  \
                }                                                           \
            }                                                               \
            do {

#define SKY_DICT_FOREACH_SHARED_BREAK                                       \
                _break = SKY_TRUE;                                          \
                break

#define SKY_DICT_FOREACH_SHARED_END                                         \
            } while (0);                                                    \
            if (_break) {                                                   \
                break;                                                      \
            }                                                               \
            if (_data->table != _table || _table->revision != _revision) {  \
                sky_error_raise_string(sky_RuntimeError,                    \
                                       "dict modified during iteration");   \
            }                                                               \
        }                                                                   \
    } while (0)


#define PERTURB_SHIFT 5

static sky_dict_item_t *
sky_dict_data_lookup(sky_dict_data_t *      dict_data,
                     sky_hazard_pointer_t * hazard,
                     uintptr_t              key_hash,
                     sky_object_t           key)
{
    size_t              i, mask, perturb;
    uintptr_t           revision;
    sky_bool_t          match;
    sky_dict_item_t     *item, *tombstone;
    sky_dict_table_t    *table;

retry:
    sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(dict_data->table)));
    table = hazard->pointer;
    revision = table->revision;
    mask = table->mask;

    i = (size_t)key_hash & mask;
    item = &(table->table[i]);
    if (!item->key || item->key == key) {
        return item;
    }
    if (sky_NotSpecified == item->key) {
        tombstone = item;
    }
    else {
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (table != dict_data->table || revision != table->revision) {
                goto retry;
            }
            if (match) {
                return item;
            }
        }
        tombstone = NULL;
    }

    for (perturb = key_hash; ; perturb >>= PERTURB_SHIFT) {
        i = (i << 2) + i + perturb + 1;
        item = &(table->table[i & mask]);
        if (!item->key) {
            return (tombstone ? tombstone : item);
        }
        if (item->key == key) {
            return item;
        }
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (table != dict_data->table || revision != table->revision) {
                goto retry;
            }
            if (match) {
                return item;
            }
        }
        if (sky_NotSpecified == item->key && !tombstone) {
            tombstone = item;
        }
    }

    sky_error_fatal("not reached");
}


static sky_dict_table_t *
sky_dict_table_create(size_t capacity)
{
    sky_dict_table_t    *table;

    capacity = sky_util_roundpow2(capacity);
    if (capacity < SKY_DICT_MINIMUM_CAPACITY) {
        capacity = SKY_DICT_MINIMUM_CAPACITY;
    }

    table = sky_calloc(1, sizeof(sky_dict_table_t));
    table->mask = capacity - 1;
    table->table = sky_calloc(capacity, sizeof(sky_dict_item_t));
    sky_spinlock_init(&(table->spinlock));

    return table;
}


static void
sky_dict_table_destroy(sky_dict_table_t *table)
{
    sky_free(table->table);
    sky_free(table);
}


static sky_dict_table_t *
sky_dict_data_table(sky_dict_data_t *dict_data)
{
    sky_hazard_pointer_t    *hazard;

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(dict_data->table)));
    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);
    return hazard->pointer;
}


static void
sky_dict_data_settable(sky_dict_data_t *    dict_data,
                       sky_dict_table_t *   new_table,
                       sky_bool_t           shared)
{
    sky_dict_table_t    *old_table;

    if (!shared) {
        old_table = dict_data->table;
        dict_data->table = new_table;
    }
    else {
        old_table = sky_atomic_exchange(new_table,
                                        SKY_AS_VOIDP(&(dict_data->table)));
    }
    if (old_table && &(dict_data->tiny_table) != old_table) {
        sky_hazard_pointer_retire(old_table,
                                  (sky_free_t)sky_dict_table_destroy);
    }
}


static sky_dict_table_t *
sky_dict_table_copy(sky_dict_table_t *table, sky_dict_t dict, size_t capacity)
{
    size_t              i, mask, perturb;
    ssize_t             count, index, new_index;
    sky_dict_item_t     *item, *new_item;
    sky_dict_table_t    *new_table;

    new_table = sky_dict_table_create(capacity);
    sky_error_validate_debug(new_table->mask + 1 > (size_t)table->len * 3 / 2);
    new_table->len = table->len;
    new_table->filled = new_table->len;

    mask = new_table->mask;
    for (count = table->len, index = 0; count > 0; --count) {
        do {
            item = &(table->table[index++]);
        } while (!sky_dict_item_isalive(item));

        i = new_index = (size_t)item->key_hash & mask;
        for (perturb = item->key_hash; ; perturb >>= PERTURB_SHIFT) {
            new_item = &(new_table->table[new_index]);
            if (!new_item->key) {
                break;
            }
            i = (i << 2) + i + perturb + 1;
            new_index = i & mask;
        }

        sky_object_gc_set(&(new_item->value), item->value, dict);
        sky_object_gc_set(&(new_item->key), item->key, dict);
        new_item->key_hash = item->key_hash;
    }

    return new_table;
}


static sky_bool_t
sky_dict_table_insert_clean(sky_dict_table_t ** table,
                            sky_dict_t          dict,
                            uintptr_t           key_hash,
                            sky_object_t        key,
                            sky_object_t        value)
{
    size_t              i, index, mask, new_capacity, perturb;
    uintptr_t           revision;
    sky_bool_t          match;
    sky_dict_data_t     *dict_data;
    sky_dict_item_t     *item, *items;
    sky_dict_table_t    *new_table;

retry:
    revision = (*table)->revision;
    mask = (*table)->mask;
    items = (*table)->table;
    i = index = (size_t)key_hash & mask;
    for (perturb = key_hash; ; perturb >>= PERTURB_SHIFT) {
        item = &(items[index]);
        if (!item->key) {
            break;
        }
        if (item->key == key) {
            return SKY_FALSE;
        }
        if (item->key_hash == key_hash) {
            match = sky_object_compare(item->key, key, SKY_COMPARE_OP_EQUAL);
            if (revision != (*table)->revision) {
                goto retry;
            }
            if (match) {
                return SKY_FALSE;
            }
        }
        i = (i << 2) + i + perturb + 1;
        index = i & mask;
    }

    sky_object_gc_set(&(item->value), value, dict);
    sky_object_gc_set(&(item->key), key, dict);
    item->key_hash = key_hash;
    ++(*table)->len;
    if ((size_t)++(*table)->filled > (mask + 1) * 2 / 3) {
        new_capacity = (mask + 1) * (mask + 1 >= 0x10000 ? 2 : 4);
        new_table = sky_dict_table_copy(*table, dict, new_capacity);
        dict_data = sky_dict_data(dict);
        if (dict_data->table == *table) {
            sky_dict_data_settable(dict_data,
                                   new_table,
                                   sky_dict_isshared(dict));
        }
        *table = new_table;
    }
    return SKY_TRUE;
}


static void
sky_dict_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_dict_data_t *dict_data = data;

    dict_data->table = &(dict_data->tiny_table);
    dict_data->table->mask = SKY_DICT_TINY_CAPACITY - 1;
    dict_data->table->table = dict_data->tiny_items;

    sky_spinlock_init(&(dict_data->table->spinlock));
}


static void
sky_dict_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_dict_data_t *dict_data = data;

    if (&(dict_data->tiny_table) != dict_data->table) {
        sky_dict_table_destroy(dict_data->table);
    }
    dict_data->table = NULL;
}


static void
sky_dict_instance_iterate(sky_object_t                  self,
                          void *                        data,
                          sky_sequence_iterate_state_t *state,
                          sky_object_t *                objects,
                          ssize_t                       nobjects)
{
    sky_dict_data_t *self_data = data;

    uintptr_t               revision;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    state->objects = objects;
    state->nobjects = 0;

    if (!state->state) {
        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
        table = (sky_dict_table_t *)hazard->pointer;
        if (!table->len) {
            sky_hazard_pointer_release(hazard);
            return;
        }
        revision = table->revision;

        state->extra[0] = (uintptr_t)hazard;
        state->extra[1] = (uintptr_t)table;
        state->extra[2] = revision;
        state->extra[3] = 0;
    }
    else {
        table = (sky_dict_table_t *)state->extra[1];
        revision = state->extra[2];
    }

    if (self_data->table != table || revision != table->revision) {
        sky_error_raise_string(sky_RuntimeError,
                               "dict modified during iteration");
    }
    if (state->state >= table->len) {
        return;
    }

    if (!sky_dict_isshared(self)) {
        while (state->nobjects < nobjects) {
            do {
                item = &(table->table[state->extra[3]++]);
            } while (!sky_dict_item_isalive(item));
            state->objects[state->nobjects++] = item->key;
            if (++state->state >= table->len) {
                return;
            }
        }
    }
    else {
        while (state->nobjects < nobjects) {
            while (state->extra[3] <= table->mask) {
                item = &(table->table[state->extra[3]++]);
                if (self_data->table != table || revision != table->revision) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }
                if (sky_dict_item_isalive(item)) {
                    break;
                }
            }
            state->objects[state->nobjects] = item->key;
            if (sky_NotSpecified == state->objects[state->nobjects]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            ++state->nobjects;
            if (++state->state >= table->len) {
                return;
            }
        }
    }
}


static void
sky_dict_instance_iterate_cleanup(
        SKY_UNUSED sky_object_t                     self,
        SKY_UNUSED void *                           data,
                   sky_sequence_iterate_state_t *   state)
{
    sky_hazard_pointer_t    *hazard;

    if ((hazard = (sky_hazard_pointer_t *)state->extra[0]) != NULL) {
        sky_hazard_pointer_release(hazard);
    }
}


static void
sky_dict_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_dict_data_t *self_data = data;

    size_t                  index;
    ssize_t                 count;
    uintptr_t               revision;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(self_data->table)));
        table = hazard->pointer;
        revision = table->revision;

        for (count = table->len, index = 0; count > 0; --count) {
            while (index <= table->mask) {
                item = &(table->table[index++]);
                if (table != self_data->table ||
                    revision != table->revision)
                {
                    goto retry;
                }
                if (sky_dict_item_isalive(item)) {
                    break;
                }
            }
            sky_object_visit(item->key, visit_data);
            sky_object_visit(item->value, visit_data);
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_dict_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_dict_iterator_data_t    *self_data = data;

    sky_object_visit(self_data->dict, visit_data);
}


static void
sky_dict_view_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_dict_view_data_t    *self_data = data;

    sky_object_visit(self_data->dict, visit_data);
}


static void
sky_dict_keys_instance_iterate(
        SKY_UNUSED  sky_object_t                    self,
                    void *                          data,
                    sky_sequence_iterate_state_t *  state,
                    sky_object_t *                  objects,
                    ssize_t                         nobjects)
{
    sky_dict_view_data_t    *self_data = data;

    sky_dict_instance_iterate(self_data->dict,
                              sky_dict_data(self_data->dict),
                              state,
                              objects,
                              nobjects);
}


static void
sky_dict_keys_instance_iterate_cleanup(
        SKY_UNUSED  sky_object_t                    self,
        SKY_UNUSED  void *                          data,
                    sky_sequence_iterate_state_t *  state)
{
    sky_dict_view_data_t    *self_data = data;

    sky_dict_instance_iterate_cleanup(self_data->dict,
                                      sky_dict_data(self_data->dict),
                                      state);
}


static void
sky_dict_setcapacity(sky_dict_t self, size_t capacity)
    /* Assumes that self_data->spinlock is locked (if necessary).
     * Must NOT call any external code at all.
     */
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    sky_dict_table_t    *new_table, *old_table;

    old_table = self_data->table;
    capacity = sky_util_roundpow2(capacity);
    if (capacity < SKY_DICT_MINIMUM_CAPACITY) {
        capacity = SKY_DICT_MINIMUM_CAPACITY;
    }
    if (capacity - 1 > SSIZE_MAX) {
        sky_error_raise_string(sky_OverflowError, "dict too big");
    }
    if (old_table->filled == old_table->len &&
        old_table->mask == capacity - 1)
    {
        return;
    }

    new_table = sky_dict_table_copy(old_table, self, capacity);
    sky_dict_data_settable(self_data, new_table, sky_dict_isshared(self));
}


static void
sky_dict_validatekeywords(sky_object_t kws)
{
    sky_object_t    key;

    if (!sky_object_isa(kws, sky_dict_type)) {
        sky_error_raise_string(sky_SystemError,
                               "bad argument to internal function");
    }

    SKY_SEQUENCE_FOREACH(kws, key) {
        if (!sky_object_isa(key, sky_string_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "keyword arguments must be strings");
        }
    } SKY_SEQUENCE_FOREACH_END;
}


sky_bool_t
sky_dict_add(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    size_t                  new_capacity;
    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        new_capacity = 0;
        if (sky_dict_item_isalive(item)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (sky_object_type(key) == sky_string_type) {
                key = sky_string_intern(key);
            }
            if (sky_dict_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (sky_dict_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            if (!item->key) {
                ++table->filled;
                if ((size_t)table->filled + 1 > (table->mask + 1) * 2 / 3) {
                    new_capacity = (table->mask + 1) *
                                   (table->mask >= 0x10000 ? 2 : 4);
                }
            }
            sky_object_gc_set(&(item->value), value, self);
            sky_object_gc_set(&(item->key), key, self);
            item->key_hash = key_hash;
            ++table->len;
            ++table->revision;
            if (new_capacity) {
                sky_dict_setcapacity(self, new_capacity);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
sky_dict_clear(sky_dict_t self)
{
    sky_dict_data_settable(sky_dict_data(self),
                           sky_dict_table_create(SKY_DICT_MINIMUM_CAPACITY),
                           sky_dict_isshared(self));
}


sky_bool_t
sky_dict_contains(sky_dict_t self, sky_object_t key)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    sky_bool_t              result;
    sky_dict_item_t         *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        item = sky_dict_data_lookup(self_data,
                                    hazard,
                                    sky_object_hash(key),
                                    key);
        result = sky_dict_item_isalive(item);
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_dict_t
sky_dict_copy(sky_dict_t self)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    uintptr_t           key_hash;
    sky_dict_t          new_dict;
    sky_object_t        key, value;
    sky_dict_item_t     *item;
    sky_dict_table_t    *new_table, *table;

    if (!sky_dict_isshared(self)) {
        table = self_data->table;

        new_dict = sky_object_allocate(sky_dict_type);
        sky_dict_setcapacity(new_dict, table->len * 3 / 2);
        new_table = sky_dict_data(new_dict)->table;

        SKY_DICT_FOREACH(self_data, table, item) {
            sky_dict_table_insert_clean(&new_table,
                                        new_dict,
                                        item->key_hash,
                                        item->key,
                                        item->value);
        } SKY_DICT_FOREACH_END;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(self_data);

            new_dict = sky_object_allocate(sky_dict_type);
            sky_dict_setcapacity(new_dict, table->len * 3 / 2);
            new_table = sky_dict_data(new_dict)->table;

            SKY_DICT_FOREACH_SHARED(self_data, table, item) {
                key = item->key;
                value = item->value;
                key_hash = sky_object_hash(key);
                if (sky_NotSpecified == key || !value) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }

                sky_dict_table_insert_clean(&new_table,
                                            new_dict,
                                            key_hash,
                                            key,
                                            value);
            } SKY_DICT_FOREACH_SHARED_END;
        } SKY_ASSET_BLOCK_END;
    }

    return new_dict;
}


sky_dict_t
sky_dict_create(void)
{
    sky_dict_t  dict;

    dict = sky_object_allocate(sky_dict_type);
    sky_dict_setcapacity(dict, SKY_DICT_MINIMUM_CAPACITY);

    return dict;
}


sky_dict_t
sky_dict_createfromarray(ssize_t count, sky_object_t *objects)
{
    ssize_t             i;
    uintptr_t           key_hash;
    sky_dict_t          dict;
    sky_object_t        key, value;
    sky_dict_table_t    *table;

    sky_error_validate_debug(count >= 0 && !(count & 1));

    dict = sky_object_allocate(sky_dict_type);
    sky_dict_setcapacity(dict, (size_t)count * 3 / 2);

    table = sky_dict_data(dict)->table;
    for (i = 0; i < count; i += 2) {
        if (!(key = objects[i])) {
            key = sky_None;
        }
        else if (sky_object_type(key) == sky_string_type) {
            key = sky_string_intern(key);
        }
        value = objects[i + 1];

        key_hash = sky_object_hash(key);
        sky_dict_table_insert_clean(&table, dict, key_hash, key, value);
    }

    return dict;
}


sky_dict_t
sky_dict_createwithcapacity(size_t capacity)
{
    sky_dict_t  dict;

    dict = sky_object_allocate(sky_dict_type);
    sky_dict_setcapacity(dict, capacity * 3 / 2);

    return dict;
}


sky_bool_t
sky_dict_delete(sky_dict_t self, sky_object_t key)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        if (!sky_dict_item_isalive(item)) {
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (sky_dict_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (!sky_dict_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            item->value = NULL;
            sky_object_gc_set(&(item->key), sky_NotSpecified, self);
            item->key_hash = 0;
            --table->len;
            ++table->revision;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
sky_dict_delitem(sky_dict_t self, sky_object_t key)
{
    if (!sky_dict_delete(self, key)) {
        sky_error_raise_object(sky_KeyError, key);
    }
}


sky_object_t
sky_dict_eq(sky_dict_t self, sky_object_t other)
{
    uintptr_t               key_hash, other_revision;
    sky_object_t            key, other_key, other_value, result, value;
    sky_dict_data_t         *other_data, *self_data;
    sky_dict_item_t         *other_item, *self_item;
    sky_dict_table_t        *other_table, *self_table;
    sky_hazard_pointer_t    *hazard;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(self, sky_dict_type) ||
        !sky_object_isa(other, sky_dict_type))
    {
        return sky_NotImplemented;
    }

    result = sky_True;
    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        self_data = sky_dict_data(self);
        self_table = sky_dict_data_table(self_data);

        other_data = sky_dict_data(other);
        other_table = sky_dict_data_table(other_data);
        other_revision = other_table->revision;

        if (self_table->len != other_table->len) {
            result = sky_False;
        }
        else if (!sky_dict_isshared(self)) {
            SKY_DICT_FOREACH(self_data, self_table, self_item) {
                other_item = sky_dict_data_lookup(other_data,
                                                  hazard,
                                                  self_item->key_hash,
                                                  self_item->key);
                other_key = other_item->key;
                other_value = other_item->value;

                if (hazard->pointer != other_table ||
                    other_table->revision != other_revision)
                {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }
                if (!other_key ||
                    sky_NotSpecified == other_key ||
                    !other_value ||
                    !sky_object_compare(self_item->value,
                                        other_value,
                                        SKY_COMPARE_OP_EQUAL))
                {
                    result = sky_False;
                    SKY_DICT_FOREACH_BREAK;
                }
            } SKY_DICT_FOREACH_END;
        }
        else {
            SKY_DICT_FOREACH_SHARED(self_data, self_table, self_item) {
                key_hash = self_item->key_hash;
                key = self_item->key;
                value = self_item->value;
                if (sky_NotSpecified == key || !value) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }

                other_item = sky_dict_data_lookup(other_data,
                                                  hazard,
                                                  key_hash,
                                                  key);
                other_key = other_item->key;
                other_value = other_item->value;

                if (hazard->pointer != other_table ||
                    other_table->revision != other_revision)
                {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }
                if (!other_key ||
                    sky_NotSpecified == other_key ||
                    !other_value ||
                    !sky_object_compare(self_item->value,
                                        other_value,
                                        SKY_COMPARE_OP_EQUAL))
                {
                    result = sky_False;
                    SKY_DICT_FOREACH_SHARED_BREAK;
                }
            } SKY_DICT_FOREACH_SHARED_END;
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_object_t
sky_dict_fromkeys(sky_type_t cls, sky_object_t seq, sky_object_t value)
{
    sky_object_t    dict, iterator, key;

    if (sky_object_isnull(value)) {
        value = sky_None;
    }

    if (sky_dict_type == cls) {
        dict = sky_object_allocate(sky_dict_type);
        sky_dict_setcapacity(dict,
                             sky_object_length_hint(seq,
                                                    SKY_DICT_MINIMUM_CAPACITY));
    }
    else {
        dict = sky_object_call(cls, NULL, NULL);
    }

    if (sky_sequence_isiterable(seq)) {
        SKY_SEQUENCE_FOREACH(seq, key) {
            sky_object_setitem(dict, key, value);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iterator = sky_object_iter(seq);
        while ((key = sky_object_next(iterator, NULL)) != NULL) {
            sky_object_setitem(dict, key, value);
        }
    }

    return dict;
}


sky_object_t
sky_dict_get(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    sky_object_t            result;
    sky_dict_item_t         *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        item = sky_dict_data_lookup(self_data,
                                    hazard,
                                    sky_object_hash(key),
                                    key);
        result = (sky_dict_item_isalive(item) ? item->value : NULL);
    } SKY_ASSET_BLOCK_END;

    return (result ? result : value);
}


sky_object_t
sky_dict_getitem(sky_dict_t self, sky_object_t key)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    sky_type_t              type;
    sky_object_t            method, result;
    sky_dict_item_t         *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        item = sky_dict_data_lookup(self_data,
                                    hazard,
                                    sky_object_hash(key),
                                    key);
        result = (sky_dict_item_isalive(item) ? item->value : NULL);
    } SKY_ASSET_BLOCK_END;

    if (!result) {
        type = sky_object_type(self);
        method = (type != sky_dict_type
                        ? sky_type_lookup(type,
                                          SKY_STRING_LITERAL("__missing__"))
                        : NULL);
        if (sky_object_isnull(method)) {
            sky_error_raise_object(sky_KeyError, key);
        }
        result = sky_object_call(sky_descriptor_get(method, self, type),
                                 sky_tuple_pack(1, key),
                                 NULL);
    }

    return result;
}


void
sky_dict_init(sky_dict_t self, sky_tuple_t args, sky_dict_t kws)
{
    if (args && sky_tuple_len(args) > 0) {
        if (sky_tuple_len(args) > 1) {
            sky_error_raise_format(sky_TypeError,
                                   "update expected at most 1 argument, got %zd",
                                   sky_tuple_len(args));
        }
        sky_dict_merge(self, sky_tuple_get(args, 0), SKY_TRUE);
    }
    if (kws && sky_dict_len(kws) > 0) {
        sky_dict_validatekeywords(kws);
        sky_dict_merge(self, kws, SKY_TRUE);
    }
}


sky_object_t
sky_dict_items(sky_dict_t self)
{
    sky_dict_items_t        items;
    sky_dict_view_data_t    *items_data;

    items = sky_object_allocate(sky_dict_items_type);
    items_data = sky_dict_view_data(items);
    sky_object_gc_set(SKY_AS_OBJECTP(&(items_data->dict)), self, items);

    return items;
}


static sky_bool_t
sky_dict_items_contains(sky_object_t self, sky_object_t item)
{
    sky_dict_t  dict = sky_dict_view_data(self)->dict;

    sky_object_t    value;

    if (!sky_object_isa(item, sky_tuple_type) || sky_tuple_len(item) != 2) {
        return SKY_FALSE;
    }

    if (!(value = sky_dict_get(dict, sky_tuple_get(item, 0), NULL))) {
        return SKY_FALSE;
    }
    return sky_object_compare(sky_tuple_get(item, 1),
                              value,
                              SKY_COMPARE_OP_EQUAL);
}

static sky_object_t
sky_dict_items_iter(sky_object_t self)
{
    sky_dict_view_data_t    *self_data = sky_dict_view_data(self);
    sky_dict_data_t         *dict_data = sky_dict_data(self_data->dict);

    sky_object_t                iterator;
    sky_dict_table_t            *table;
    sky_dict_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_dict_itemiterator_type);
    iterator_data = sky_dict_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->dict)),
                      self_data->dict,
                      iterator);

    if (!sky_dict_isshared(self)) {
        table = dict_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->len = table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(dict_data);

            iterator_data->table = table;
            iterator_data->revision = table->revision;
            iterator_data->len = table->len;
        } SKY_ASSET_BLOCK_END;
    }

    return iterator;
}


sky_object_t
sky_dict_iter(sky_dict_t self)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    sky_object_t                iterator;
    sky_dict_table_t            *table;
    sky_dict_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_dict_keyiterator_type);
    iterator_data = sky_dict_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->dict)), self, iterator);

    if (!sky_dict_isshared(self)) {
        table = self_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->len = table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(self_data);

            iterator_data->table = table;
            iterator_data->revision = table->revision;
            iterator_data->len = table->len;
        } SKY_ASSET_BLOCK_END;
    }

    return iterator;
}


sky_object_t
sky_dict_keys(sky_dict_t self)
{
    sky_dict_keys_t         keys;
    sky_dict_view_data_t    *keys_data;

    keys = sky_object_allocate(sky_dict_keys_type);
    keys_data = sky_dict_view_data(keys);
    sky_object_gc_set(SKY_AS_OBJECTP(&(keys_data->dict)), self, keys);

    return keys;
}


static sky_bool_t
sky_dict_keys_contains(sky_object_t self, sky_object_t item)
{
    return sky_dict_contains(sky_dict_view_data(self)->dict, item);
}


static sky_object_t
sky_dict_keys_iter(sky_object_t self)
{
    sky_dict_view_data_t    *self_data = sky_dict_view_data(self);
    sky_dict_data_t         *dict_data = sky_dict_data(self_data->dict);

    sky_object_t                iterator;
    sky_dict_table_t            *table;
    sky_dict_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_dict_keyiterator_type);
    iterator_data = sky_dict_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->dict)),
                      self_data->dict,
                      iterator);

    if (!sky_dict_isshared(self)) {
        table = dict_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->len = table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(dict_data);

            iterator_data->table = table;
            iterator_data->revision = table->revision;
            iterator_data->len = table->len;
        } SKY_ASSET_BLOCK_END;
    }

    return iterator;
}


ssize_t
sky_dict_len(sky_dict_t self)
{
    ssize_t len;

    if (!sky_dict_isshared(self)) {
        len = sky_dict_data(self)->table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            len = sky_dict_data_table(sky_dict_data(self))->len;
        } SKY_ASSET_BLOCK_END;
    }

    return len;
}


static inline void
sky_dict_mergeitem(sky_dict_t       dict,
                   sky_object_t     key,
                   sky_object_t     value,
                   sky_bool_t       overwrite)
{
    if (!overwrite) {
        sky_dict_add(dict, key, value);
    }
    else {
        sky_dict_setitem(dict, key, value);
    }
}

static void
sky_dict_mergedict(sky_dict_t dict, sky_dict_t other, sky_bool_t overwrite)
{
    sky_dict_data_t *other_data = sky_dict_data(other);

    sky_object_t        key, value;
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    if (!sky_dict_isshared(other)) {
        SKY_DICT_FOREACH(other_data, other_data->table, item) {
            sky_dict_mergeitem(dict, item->key, item->value, overwrite);
        } SKY_DICT_FOREACH_END;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(other_data);
            SKY_DICT_FOREACH_SHARED(other_data, table, item) {
                key = item->key;
                value = item->value;

                if (!key || !value || sky_NotSpecified == key) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }
                sky_dict_mergeitem(dict, key, value, overwrite);
            } SKY_DICT_FOREACH_SHARED_END;
        } SKY_ASSET_BLOCK_END;
    }
}

static void
sky_dict_mergedictlike(sky_dict_t   dict,
                       sky_object_t other,
                       sky_bool_t   overwrite)
{
    sky_object_t    iterator, key, keys, value;

    keys = sky_object_getattr(other,
                              SKY_STRING_LITERAL("keys"),
                              sky_NotSpecified);
    iterator = sky_object_iter(sky_object_call(keys, NULL, NULL));
    while ((key = sky_object_next(iterator, NULL)) != NULL) {
        value = sky_object_getitem(other, key);
        sky_dict_mergeitem(dict, key, value, overwrite);
    }
}

static void
sky_dict_mergesequence(sky_dict_t   dict,
                       sky_object_t other,
                       sky_bool_t   overwrite)
{
    ssize_t         i;
    sky_object_t    iterator, key, object, value;

    if (!sky_object_isiterable(other)) {
        sky_error_raise_format(sky_TypeError,
                               "expected dict or iterable; got %#@",
                               sky_type_name(sky_object_type(other)));
    }

    i = 0;
    if (sky_sequence_isiterable(other)) {
        SKY_SEQUENCE_FOREACH(other, object) {
            if (!sky_sequence_check(object)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "cannot convert dictionary update sequence element "
                        "#%zd to a sequence",
                        i);
            }
            if (sky_object_len(object) != 2) {
                sky_error_raise_format(
                        sky_ValueError,
                        "dictionary update sequence element #%zd has length "
                        "%zd; 2 is required",
                        i,
                        sky_object_len(object));
            }
            key = sky_sequence_get(object, 0);
            value = sky_sequence_get(object, 1);

            sky_dict_mergeitem(dict, key, value, overwrite);
            ++i;
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iterator = sky_object_iter(other);
        while ((object = sky_object_next(iterator, NULL)) != NULL) {
            if (!sky_sequence_check(object)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "cannot convert dictionary update sequence element "
                        "#%zd to a sequence",
                        i);
            }
            if (sky_object_len(object) != 2) {
                sky_error_raise_format(
                        sky_ValueError,
                        "dictionary update sequence element #%zd has length "
                        "%zd; 2 is required",
                        i,
                        sky_object_len(object));
            }
            key = sky_sequence_get(object, 0);
            value = sky_sequence_get(object, 1);

            sky_dict_mergeitem(dict, key, value, overwrite);
            ++i;
        }
    }
}


void
sky_dict_merge(sky_dict_t self, sky_object_t other, sky_bool_t overwrite)
{
    if (sky_dict_type == sky_object_type(other)) {
        sky_dict_mergedict(self, other, overwrite);
        return;
    }
    if (sky_object_hasattr(other, SKY_STRING_LITERAL("keys"))) {
        sky_dict_mergedictlike(self, other, overwrite);
        return;
    }
    sky_dict_mergesequence(self, other, overwrite);
}


sky_object_t
sky_dict_pop(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    uintptr_t               key_hash;
    sky_object_t            result;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        if (!sky_dict_item_isalive(item)) {
            result = value;
        }
        else {
            if (sky_dict_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (!sky_dict_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            result = item->value;
            item->value = NULL;
            sky_object_gc_set(&(item->key), sky_NotSpecified, self);
            item->key_hash = 0;
            --table->len;
            ++table->revision;
        }
    } SKY_ASSET_BLOCK_END;

    if (!result) {
        sky_error_raise_object(sky_KeyError, key);
    }
    return result;
}


sky_tuple_t
sky_dict_popitem(sky_dict_t self)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    size_t              index, mask;
    uintptr_t           revision;
    sky_object_t        objects[2];
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    objects[0] = NULL;
    if (!sky_dict_isshared(self)) {
        table = self_data->table;
        if (table->len) {
            index = 0;
            do {
                item = &(table->table[index++]);
            } while (!sky_dict_item_isalive(item));

            objects[0] = item->key;
            objects[1] = item->value;

            item->value = NULL;
            sky_object_gc_set(&(item->key), sky_NotSpecified, self);
            item->key_hash = 0;
            --table->len;
            ++table->revision;
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(self_data);
            if (table->len) {
                revision = table->revision;
                mask = table->mask;
                for (index = 0; index <= mask; ++index) {
                    item = &(table->table[index]);
                    if (table != self_data->table ||
                        revision != table->revision)
                    {
                        sky_error_raise_string(
                                sky_RuntimeError,
                                "dict modified during iteration");
                    }
                    if (!sky_dict_item_isalive(item)) {
                        continue;
                    }
                    sky_spinlock_lock(&(table->spinlock));
                    if (!sky_dict_item_isalive(item)) {
                        sky_spinlock_unlock(&(table->spinlock));
                        sky_error_raise_string(
                                sky_RuntimeError,
                                "dict modified during iteration");
                    }

                    objects[0] = item->key;
                    objects[1] = item->value;

                    item->value = NULL;
                    sky_object_gc_set(&(item->key), sky_NotSpecified, self);
                    item->key_hash = 0;
                    --table->len;
                    ++table->revision;

                    sky_spinlock_unlock(&(table->spinlock));
                    break;
                }
            }
        } SKY_ASSET_BLOCK_END;
    }

    if (!objects[0] || !objects[1] || sky_NotSpecified == objects[0]) {
        sky_error_raise_string(sky_KeyError, "popitem(): dictionary is empty");
    }
    return sky_tuple_createfromarray(2, objects);
}


sky_bool_t
sky_dict_replace(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_dict_item_t         *item;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        key_hash = sky_object_hash(key);
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);

        if ((result = sky_dict_item_isalive(item)) != SKY_FALSE) {
            sky_object_gc_set(&(item->value), value, self);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_string_t
sky_dict_repr(sky_dict_t self)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    ssize_t                 count, len;
    sky_string_t            item_joiner, key_joiner;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_string_builder_t    builder;

    if (sky_object_stack_push(self)) {
        return SKY_STRING_LITERAL("{...}");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        sky_string_builder_appendcodepoint(builder, '{', 1);

        table = sky_dict_data_table(self_data);
        if ((len = table->len) > 0) {
            item_joiner = SKY_STRING_LITERAL(", ");
            key_joiner = SKY_STRING_LITERAL(": ");

            count = 0;
            if (!sky_dict_isshared(self)) {
                SKY_DICT_FOREACH(self_data, table, item) {
                    sky_string_builder_append(builder,
                                              sky_object_repr(item->key));
                    sky_string_builder_append(builder, key_joiner);
                    sky_string_builder_append(builder,
                                              sky_object_repr(item->value));
                    if (++count < len) {
                        sky_string_builder_append(builder, item_joiner);
                    }
                } SKY_DICT_FOREACH_END;
            }
            else {
                SKY_DICT_FOREACH_SHARED(self_data, table, item) {
                    sky_string_builder_append(builder,
                                              sky_object_repr(item->key));
                    sky_string_builder_append(builder, key_joiner);
                    sky_string_builder_append(builder,
                                              sky_object_repr(item->value));
                    if (++count < len) {
                        sky_string_builder_append(builder, item_joiner);
                    }
                } SKY_DICT_FOREACH_SHARED_END;
            }
        }

        sky_string_builder_appendcodepoint(builder, '}', 1);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


sky_object_t
sky_dict_setdefault(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    size_t                  new_capacity;
    uintptr_t               key_hash;
    sky_object_t            result;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        new_capacity = 0;
        if (!(result = item->value)) {
            result = value;
            if (sky_object_type(key) == sky_string_type) {
                key = sky_string_intern(key);
            }
            if (sky_dict_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (sky_dict_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            if (!item->key) {
                ++table->filled;
                if ((size_t)table->filled + 1 > (table->mask + 1) * 2 / 3) {
                    new_capacity = (table->mask + 1) *
                                   (table->mask >= 0x10000 ? 2 : 4);
                }
            }
            sky_object_gc_set(&(item->value), value, self);
            sky_object_gc_set(&(item->key), key, self);
            item->key_hash = key_hash;
            ++table->len;
            ++table->revision;
            if (new_capacity) {
                sky_dict_setcapacity(self, new_capacity);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bool_t
sky_dict_setitem(sky_dict_t self, sky_object_t key, sky_object_t value)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    size_t                  new_capacity;
    uintptr_t               key_hash;
    sky_bool_t              result;
    sky_dict_item_t         *item;
    sky_dict_table_t        *table;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        key_hash = sky_object_hash(key);
        hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

retry:
        item = sky_dict_data_lookup(self_data, hazard, key_hash, key);
        table = hazard->pointer;

        new_capacity = 0;
        if (sky_dict_item_isalive(item)) {
            sky_object_gc_set(&(item->value), value, self);
            result = SKY_FALSE;
        }
        else {
            result = SKY_TRUE;
            if (sky_object_type(key) == sky_string_type) {
                key = sky_string_intern(key);
            }
            if (sky_dict_isshared(self)) {
                sky_spinlock_lock(&(table->spinlock));
                if (sky_dict_item_isalive(item)) {
                    sky_spinlock_unlock(&(table->spinlock));
                    goto retry;
                }
                sky_asset_save(&(table->spinlock),
                               (sky_free_t)sky_spinlock_unlock,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }

            if (!item->key) {
                ++table->filled;
                if ((size_t)table->filled + 1 > (table->mask + 1) * 2 / 3) {
                    new_capacity = (table->mask + 1) *
                                   (table->mask >= 0x10000 ? 2 : 4);
                }
            }
            sky_object_gc_set(&(item->value), value, self);
            sky_object_gc_set(&(item->key), key, self);
            item->key_hash = key_hash;
            ++table->len;
            ++table->revision;
            if (new_capacity) {
                sky_dict_setcapacity(self, new_capacity);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


size_t
sky_dict_sizeof(sky_object_t self)
{
    sky_dict_data_t *self_data = sky_dict_data(self);

    size_t              size;
    sky_dict_table_t    *table;

    size = sky_memsize(self);
    if (!sky_dict_isshared(self)) {
        table = self_data->table;
        if (&(self_data->tiny_table) != table) {
            size += sky_memsize(table) + sky_memsize(table->table);
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(self_data);
            if (&(self_data->tiny_table) != table) {
                size += sky_memsize(table) + sky_memsize(table->table);
            }
        } SKY_ASSET_BLOCK_END;
    }

    return size;
}


void
sky_dict_update(sky_dict_t self, sky_tuple_t args, sky_dict_t kws)
{
    if (args && sky_tuple_len(args) > 0) {
        if (sky_tuple_len(args) > 1) {
            sky_error_raise_format(sky_TypeError,
                                   "update expected at most 1 argument, got %zd",
                                   sky_tuple_len(args));
        }
        sky_dict_merge(self, sky_tuple_get(args, 0), SKY_TRUE);
    }
    if (kws && sky_dict_len(kws) > 0) {
        sky_dict_validatekeywords(kws);
        sky_dict_merge(self, kws, SKY_TRUE);
    }
}


sky_object_t
sky_dict_values(sky_dict_t self)
{
    sky_dict_values_t       values;
    sky_dict_view_data_t    *values_data;

    values = sky_object_allocate(sky_dict_values_type);
    values_data = sky_dict_view_data(values);
    sky_object_gc_set(SKY_AS_OBJECTP(&(values_data->dict)), self, values);

    return values;
}


static sky_object_t
sky_dict_values_iter(sky_object_t self)
{
    sky_dict_view_data_t    *self_data = sky_dict_view_data(self);
    sky_dict_data_t         *dict_data = sky_dict_data(self_data->dict);

    sky_object_t                iterator;
    sky_dict_table_t            *table;
    sky_dict_iterator_data_t    *iterator_data;

    iterator = sky_object_allocate(sky_dict_valueiterator_type);
    iterator_data = sky_dict_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->dict)),
                      self_data->dict,
                      iterator);

    if (!sky_dict_isshared(self)) {
        table = dict_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->len = table->len;
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(dict_data);

            iterator_data->table = table;
            iterator_data->revision = table->revision;
            iterator_data->len = table->len;
        } SKY_ASSET_BLOCK_END;
    }

    return iterator;
}


static sky_object_t
sky_dict_iterator_iter(sky_object_t self)
{
    return self;
}


static ssize_t
sky_dict_iterator_len(sky_object_t self)
{
    return sky_dict_iterator_data(self)->len;
}


static sky_dict_item_t *
sky_dict_iterator_data_next(sky_dict_iterator_data_t *  self_data,
                            sky_dict_table_t *          table)
{
    sky_dict_item_t *item;

    if (table != self_data->table ||
        table->revision != self_data->revision)
    {
        sky_error_raise_string(sky_RuntimeError,
                               "dict modified during iteration");
    }
    if (!self_data->len) {
        return NULL;
    }

    if (!sky_dict_isshared(self_data->dict)) {
        do {
            item = &(table->table[self_data->index++]);
        } while (!sky_dict_item_isalive(item));
    }
    else {
        while (self_data->index <= table->mask) {
            item = &(table->table[self_data->index++]);
            if (table != self_data->table ||
                table->revision != self_data->revision)
            {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            if (sky_dict_item_isalive(item)) {
                break;
            }
        }
    }

    --self_data->len;
    return item;
}


static sky_object_t
sky_dict_itemiterator_next(sky_object_t self)
{
    sky_dict_iterator_data_t    *self_data = sky_dict_iterator_data(self);

    sky_dict_t          dict;
    sky_object_t        *objects, result;
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    if (!(dict = self_data->dict)) {
        return NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_dict_isshared(dict)) {
            table = sky_dict_data(dict)->table;
        }
        else {
            table = sky_dict_data_table(sky_dict_data(dict));
        }
        if (!(item = sky_dict_iterator_data_next(self_data, table))) {
            self_data->dict = NULL;
            result = NULL;
        }
        else {
            objects = sky_asset_calloc(2, sizeof(sky_object_t),
                                       SKY_ASSET_CLEANUP_ON_ERROR);
            objects[0] = item->key;
            objects[1] = item->value;

            if (sky_NotSpecified == objects[0] || !objects[1]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            result = sky_tuple_createwitharray(2, objects, sky_free);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_object_t
sky_dict_keyiterator_next(sky_object_t self)
{
    sky_dict_iterator_data_t    *self_data = sky_dict_iterator_data(self);

    sky_object_t        objects[2], result;
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    if (!self_data->dict) {
        return NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_dict_isshared(self_data->dict)) {
            table = sky_dict_data(self_data->dict)->table;
        }
        else {
            table = sky_dict_data_table(sky_dict_data(self_data->dict));
        }
        if (!(item = sky_dict_iterator_data_next(self_data, table))) {
            self_data->dict = NULL;
            result = NULL;
        }
        else {
            objects[0] = item->key;
            objects[1] = item->value;

            if (sky_NotSpecified == objects[0] || !objects[1]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            result = objects[0];
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_object_t
sky_dict_valueiterator_next(sky_object_t self)
{
    sky_dict_iterator_data_t    *self_data = sky_dict_iterator_data(self);

    sky_object_t        objects[2], result;
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    if (!self_data->dict) {
        return NULL;
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_dict_isshared(self_data->dict)) {
            table = sky_dict_data(self_data->dict)->table;
        }
        else {
            table = sky_dict_data_table(sky_dict_data(self_data->dict));
        }
        if (!(item = sky_dict_iterator_data_next(self_data, table))) {
            self_data->dict = NULL;
            result = NULL;
        }
        else {
            objects[0] = item->key;
            objects[1] = item->value;

            if (sky_NotSpecified == objects[0] || !objects[1]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            result = objects[1];
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_object_t
sky_dict_iterator_reduce_item(sky_object_t items[2])
{
    return sky_tuple_createfromarray(2, items);
}

static sky_object_t
sky_dict_iterator_reduce_key(sky_object_t items[2])
{
    return items[0];
}

static sky_object_t
sky_dict_iterator_reduce_value(sky_object_t items[2])
{
    return items[1];
}

static sky_object_t
sky_dict_iterator_reduce(sky_object_t   self,
                         sky_object_t (*reducer)(sky_object_t items[2]))
{
    sky_dict_iterator_data_t    *self_data = sky_dict_iterator_data(self);

    size_t              index;
    ssize_t             len;
    sky_dict_t          dict;
    sky_list_t          list;
    sky_object_t        objects[2];
    sky_dict_data_t     *dict_data;
    sky_dict_item_t     *item;
    sky_dict_table_t    *table;

    if (!(dict = self_data->dict)) {
        return sky_object_build("(O([]))", sky_module_getbuiltin("iter"));
    }

    /* Iterate into a temporary list, preserving original iterator state. */
    len = self_data->len;
    index = self_data->index;
    dict_data = sky_dict_data(dict);
    list = sky_list_createwithcapacity(len);

    if (!sky_dict_isshared(self)) {
        table = dict_data->table;
        if (table != self_data->table ||
            table->revision != self_data->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "dict modified during iteration");
        }

        while (len > 0) {
            do {
                item = &(table->table[index++]);
            } while (!sky_dict_item_isalive(item));

            --len;
            objects[0] = item->key;
            objects[1] = item->value;
            if (sky_NotSpecified == objects[0] || !objects[1]) {
                sky_error_raise_string(sky_RuntimeError,
                                       "dict modified during iteration");
            }
            sky_list_append(list, reducer(objects));
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            table = sky_dict_data_table(dict_data);
            while (len > 0) {
                while (index <= table->mask) {
                    item = &(table->table[index++]);
                    if (table != self_data->table ||
                        table->revision != self_data->revision)
                    {
                        sky_error_raise_string(sky_RuntimeError,
                                               "dict modified during iteration");
                    }
                    if (sky_dict_item_isalive(item)) {
                        break;
                    }
                }

                --len;
                objects[0] = item->key;
                objects[1] = item->value;
                if (sky_NotSpecified == objects[0] || !objects[1]) {
                    sky_error_raise_string(sky_RuntimeError,
                                           "dict modified during iteration");
                }
                sky_list_append(list, reducer(objects));
            }
        } SKY_ASSET_BLOCK_END;
    }

    return sky_object_build("(O(O))", sky_module_getbuiltin("iter"), list);
}


static sky_object_t
sky_dict_itemiterator_reduce(sky_object_t self)
{
    return sky_dict_iterator_reduce(self, sky_dict_iterator_reduce_item);
}


static sky_object_t
sky_dict_keyiterator_reduce(sky_object_t self)
{
    return sky_dict_iterator_reduce(self, sky_dict_iterator_reduce_key);
}


static sky_object_t
sky_dict_valueiterator_reduce(sky_object_t self)
{
    return sky_dict_iterator_reduce(self, sky_dict_iterator_reduce_value);
}


static sky_bool_t
sky_dict_view_issubset(sky_object_t self, sky_object_t other)
{
    sky_bool_t      result;
    sky_object_t    iter, object;

    result = SKY_TRUE;
    if (sky_sequence_isiterable(self)) {
        SKY_SEQUENCE_FOREACH(self, object) {
            if (!sky_object_contains(other, object)) {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(self);
        while ((object = sky_object_next(iter, NULL)) != NULL) {
            if (!sky_object_contains(other, object)) {
                result = SKY_FALSE;
                break;
            }
        }
    }

    return result;
}


static sky_object_t
sky_dict_view_and(sky_object_t self, sky_object_t other)
{
    sky_set_t   result;

    result = sky_set_create(self);
    sky_set_intersection_update(result, other);

    return result;
}


static sky_object_t
sky_dict_view_compare(sky_object_t      self,
                      sky_object_t      other,
                      sky_compare_op_t  compare_op)
{
    ssize_t     other_len, self_len;
    sky_bool_t  result;

    switch (compare_op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(other, sky_set_type) &&
        !sky_object_isa(other, sky_frozenset_type) &&
        !sky_object_isa(other, sky_dict_items_type) &&
        !sky_object_isa(other, sky_dict_keys_type))
    {
        return sky_NotImplemented;
    }

    self_len = sky_object_len(self);
    other_len = sky_object_len(other);

    result = SKY_FALSE;
    switch (compare_op) {
        case SKY_COMPARE_OP_LESS:
            if (self_len < other_len) {
                result = sky_dict_view_issubset(self, other);
            }
            break;
        case SKY_COMPARE_OP_LESS_EQUAL:
            if (self_len <= other_len) {
                result = sky_dict_view_issubset(self, other);
            }
            break;
        case SKY_COMPARE_OP_EQUAL:
            if (self_len == other_len) {
                result = sky_dict_view_issubset(self, other);
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self_len == other_len) {
                result = sky_dict_view_issubset(self, other);
            }
            result = !result;
            break;
        case SKY_COMPARE_OP_GREATER:
            if (self_len > other_len) {
                result = sky_dict_view_issubset(other, self);
            }
            break;
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self_len >= other_len) {
                result = sky_dict_view_issubset(other, self);
            }
            break;

        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            sky_error_fatal("internal error");
    }

    return (result ? sky_True : sky_False);
}


static sky_object_t
sky_dict_view_eq(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_EQUAL);
}


static sky_object_t
sky_dict_view_ge(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


static sky_object_t
sky_dict_view_gt(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_GREATER);
}


static sky_bool_t
sky_dict_view_isdisjoint(sky_object_t self, sky_object_t other)
{
    sky_bool_t      result;
    sky_object_t    iter, key;

    if (self == other) {
        return (sky_object_len(self) ? SKY_FALSE : SKY_TRUE);
    }

    result = SKY_TRUE;

    if ((sky_object_isa(other, sky_set_type) ||
         sky_object_isa(other, sky_frozenset_type) ||
         sky_object_isa(other, sky_dict_items_type) ||
         sky_object_isa(other, sky_dict_keys_type)) &&
        sky_object_len(other) > sky_object_len(self))
    {
        key = other;
        other = self;
        self = key;
    }

    if (sky_sequence_isiterable(other)) {
        SKY_SEQUENCE_FOREACH(other, key) {
            if (sky_object_contains(self, key)) {
                result = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    else {
        iter = sky_object_iter(other);
        while ((key = sky_object_next(iter, NULL)) != NULL) {
            if (sky_object_contains(self, key)) {
                result = SKY_FALSE;
                break;
            }
        }
    }

    return result;
}


static sky_object_t
sky_dict_view_le(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


static ssize_t
sky_dict_view_len(sky_object_t self)
{
    return sky_object_len(sky_dict_view_data(self)->dict);
}


static sky_object_t
sky_dict_view_lt(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_LESS);
}


static sky_object_t
sky_dict_view_ne(sky_object_t self, sky_object_t other)
{
    return sky_dict_view_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}


static sky_object_t
sky_dict_view_or(sky_object_t self, sky_object_t other)
{
    sky_set_t   result;

    result = sky_set_create(self);
    sky_set_update(result, sky_tuple_pack(1, other));

    return result;
}


static sky_string_t
sky_dict_view_repr(sky_object_t self)
{
    return sky_string_createfromformat("%@(%#@)",
                                       sky_type_name(sky_object_type(self)),
                                       sky_list_create(self));
}


static sky_object_t
sky_dict_view_rsub(sky_object_t self, sky_object_t other)
{
    sky_set_t   result;

    result = sky_set_create(other);
    sky_set_difference_update(result, sky_tuple_pack(1, self));

    return result;
}


static sky_object_t
sky_dict_view_sub(sky_object_t self, sky_object_t other)
{
    sky_set_t   result;

    result = sky_set_create(self);
    sky_set_difference_update(result, sky_tuple_pack(1, other));

    return result;
}


static sky_object_t
sky_dict_view_xor(sky_object_t self, sky_object_t other)
{
    sky_set_t   result;

    result = sky_set_create(self);
    sky_set_symmetric_difference_update(result, other);

    return result;
}


static const char sky_dict_type_doc[] =
"dict() -> new empty dictionary\n"
"dict(mapping) -> new dictionary initialized from a mapping object's\n"
"    (key, value) pairs\n"
"dict(iterable) -> new dictionary initialized as if via:\n"
"    d = {}\n"
"    for k, v in iterable:\n"
"        d[k] = v\n"
"dict(**kwargs) -> new dictionary initialized with the name=value pairs\n"
"    in the keyword argument list. For example: dict(one=1, two=2)";


SKY_TYPE_DEFINE_ITERABLE(dict,
                         "dict",
                         sizeof(sky_dict_data_t),
                         sky_dict_instance_initialize,
                         sky_dict_instance_finalize,
                         sky_dict_instance_visit,
                         sky_dict_instance_iterate,
                         sky_dict_instance_iterate_cleanup,
                         0,
                         sky_dict_type_doc);


void
sky_dict_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    /* Dictionaries are mutable; therefore, they are not hashable! */
    sky_type_setattr_builtin(sky_dict_type, "__hash__", sky_None);

    sky_type_setattr_builtin(
            sky_dict_type,
            "clear",
            sky_function_createbuiltin(
                    "dict.clear",
                    "D.clear() -> None. Remove all items from D.",
                    (sky_native_code_function_t)sky_dict_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "copy",
            sky_function_createbuiltin(
                    "dict.copy",
                    "D.copy() -> a shallow copy of D",
                    (sky_native_code_function_t)sky_dict_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "fromkeys",
            sky_classmethod_create(sky_function_createbuiltin(
                    "dict.fromkeys",
                    "dict.fromkeys(S[,v]) -> New dict with keys from S and values equal to v.\n"
                    "v defaults to None.",
                    (sky_native_code_function_t)sky_dict_fromkeys,
                    SKY_DATA_TYPE_OBJECT,
                    "cls", SKY_DATA_TYPE_OBJECT, NULL,
                    "seq", SKY_DATA_TYPE_OBJECT, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL)));
    sky_type_setattr_builtin(
            sky_dict_type,
            "get",
            sky_function_createbuiltin(
                    "dict.get",
                    "D.get(k[,d]) -> D[k] if k in D, else d. d defaults to None.",
                    (sky_native_code_function_t)sky_dict_get,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "items",
            sky_function_createbuiltin(
                    "dict.items",
                    "D.items() -> a set-like object providing a view on D's items",
                    (sky_native_code_function_t)sky_dict_items,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "keys",
            sky_function_createbuiltin(
                    "dict.keys",
                    "D.keys() -> a set-like object providing a view on D's keys",
                    (sky_native_code_function_t)sky_dict_keys,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "pop",
            sky_function_createbuiltin(
                    "dict.pop",
                    "D.pop(k[,d]) -> v, remove speicifed key and return the corresponding value.\n"
                    "If key is not found, d is returned if given, otherwise KeyError is raised",
                    (sky_native_code_function_t)sky_dict_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "popitem",
            sky_function_createbuiltin(
                    "dict.popitem",
                    "D.popitem() -> (k, v), remove and return some (key, value) pair as a\n"
                    "2-tuple; but raise KeyError if D is empty.",
                    (sky_native_code_function_t)sky_dict_popitem,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "setdefault",
            sky_function_createbuiltin(
                    "dict.setdefault",
                    "D.setdefault(k[,d]) -> d.get(k, d), also set D[k]=d if k not in D",
                    (sky_native_code_function_t)sky_dict_setdefault,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "key", SKY_DATA_TYPE_OBJECT, NULL,
                    "default", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "update",
            sky_function_createbuiltin(
                    "dict.update",
                    "D.update([E, ]**F) -> None.  Update D from dict/iterable E and F.\n"
                    "If E present and has a .keys() method, does:     for k in E: D[k] = E[k]\n"
                    "If E present and lacks .keys() method, does:     for (k, v) in E: D[k] = v\n"
                    "In either case, this is followed by: for k in F: D[k] = F[k]",
                    (sky_native_code_function_t)sky_dict_update,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_dict_type,
            "values",
            sky_function_createbuiltin(
                    "dict.values",
                    "D.values() -> an object providing a view on D's values",
                    (sky_native_code_function_t)sky_dict_values,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslots(sky_dict_type,
            "__init__", sky_dict_init,
            "__repr__", sky_dict_repr,
            "__eq__", sky_dict_eq,
            "__sizeof__", sky_dict_sizeof,
            "__len__", sky_dict_len,
            "__getitem__", sky_dict_getitem,
            "__setitem__", sky_dict_setitem,
            "__delitem__", sky_dict_delitem,
            "__iter__", sky_dict_iter,
            "__contains__", sky_dict_contains,
            NULL);

    /* sky_dict_itemiterator */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_itemiterator"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__len__", sky_dict_iterator_len,
            "__iter__", sky_dict_iterator_iter,
            "__next__", sky_dict_itemiterator_next,
            "__reduce__", sky_dict_itemiterator_reduce,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_itemiterator_type),
            type,
            SKY_TRUE);

    /* sky_dict_keyiterator */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_keyiterator"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__len__", sky_dict_iterator_len,
            "__iter__", sky_dict_iterator_iter,
            "__next__", sky_dict_keyiterator_next,
            "__reduce__", sky_dict_keyiterator_reduce,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_keyiterator_type),
            type,
            SKY_TRUE);

    /* sky_dict_valueiterator */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_valueiterator"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_iterator_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__len__", sky_dict_iterator_len,
            "__iter__", sky_dict_iterator_iter,
            "__next__", sky_dict_valueiterator_next,
            "__reduce__", sky_dict_valueiterator_reduce,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_valueiterator_type),
            type,
            SKY_TRUE);

    /* sky_dict_items */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_view_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_items"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_view_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__repr__", sky_dict_view_repr,
            "__lt__", sky_dict_view_lt,
            "__le__", sky_dict_view_le,
            "__eq__", sky_dict_view_eq,
            "__ne__", sky_dict_view_ne,
            "__gt__", sky_dict_view_gt,
            "__ge__", sky_dict_view_ge,
            "__len__", sky_dict_view_len,
            "__iter__", sky_dict_items_iter,
            "__contains__", sky_dict_items_contains,
            "__sub__", sky_dict_view_sub,
            "__and__", sky_dict_view_and,
            "__xor__", sky_dict_view_xor,
            "__or__", sky_dict_view_or,
            "__rsub__", sky_dict_view_rsub,
            "__rand__", sky_dict_view_and,
            "__rxor__", sky_dict_view_xor,
            "__ror__", sky_dict_view_or,
            NULL);
    sky_type_setattr_builtin(type,
            "isdisjoint",
            sky_function_createbuiltin(
                "dict_items.isdisjoint",
                "Return True if the view and the given iterable have a null intersection.",
                (sky_native_code_function_t)sky_dict_view_isdisjoint,
                SKY_DATA_TYPE_BOOL,
                "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                "other", SKY_DATA_TYPE_OBJECT, NULL,
                NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_items_type),
            type,
            SKY_TRUE);

    /* sky_dict_keys */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_view_instance_visit;
    template.iterate = sky_dict_keys_instance_iterate;
    template.iterate_cleanup = sky_dict_keys_instance_iterate_cleanup;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_keys"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_view_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__repr__", sky_dict_view_repr,
            "__lt__", sky_dict_view_lt,
            "__le__", sky_dict_view_le,
            "__eq__", sky_dict_view_eq,
            "__ne__", sky_dict_view_ne,
            "__gt__", sky_dict_view_gt,
            "__ge__", sky_dict_view_ge,
            "__len__", sky_dict_view_len,
            "__iter__", sky_dict_keys_iter,
            "__contains__", sky_dict_keys_contains,
            "__sub__", sky_dict_view_sub,
            "__and__", sky_dict_view_and,
            "__xor__", sky_dict_view_xor,
            "__or__", sky_dict_view_or,
            "__rsub__", sky_dict_view_rsub,
            "__rand__", sky_dict_view_and,
            "__rxor__", sky_dict_view_xor,
            "__ror__", sky_dict_view_or,
            NULL);
    sky_type_setattr_builtin(type,
            "isdisjoint",
            sky_function_createbuiltin(
                "dict_keys.isdisjoint",
                "Return True if the view and the given iterable have a null intersection.",
                (sky_native_code_function_t)sky_dict_view_isdisjoint,
                SKY_DATA_TYPE_BOOL,
                "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, type,
                "other", SKY_DATA_TYPE_OBJECT, NULL,
                NULL));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_keys_type),
            type,
            SKY_TRUE);

    /* sky_dict_values */
    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_dict_view_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("dict_values"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_dict_view_data_t),
                                   0,
                                   NULL,
                                   &template);

    sky_type_setattr_builtin(type, "__new__", sky_None);
    sky_type_setmethodslots(type,
            "__repr__", sky_dict_view_repr,
            "__len__", sky_dict_view_len,
            "__iter__", sky_dict_values_iter,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_dict_values_type),
            type,
            SKY_TRUE);
}
