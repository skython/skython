/** @file
  * @brief Generic dynamically sized hash table
  * This hash table implementation is a low level hash table that is intended
  * to be used where Skython objects cannot otherwise be used (where sky_dict
  * would normally suffice). This implementation is not thread-safe.
  *
  * @defgroup sky_hashtable Generic Hash Table
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_HASHTABLE_H__
#define __SKYTHON_CORE_SKY_HASHTABLE_H__ 1


#include "sky_base.h"
#include "sky_hazard_pointer.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** @cond **/
/* SKY_HASHTABLE_SEGMENT_COUNT should be at least
 * sky_util_fls(SKY_HASHTABLE_MAX_LENGTH). There's no benefit to being bigger,
 * but smaller is super ultra mega bad.
 */
#define SKY_HASHTABLE_MAX_LENGTH        INT32_MAX
#define SKY_HASHTABLE_SEGMENT_COUNT     31
/** @endcond **/


/** A structure representing an item stored in the hash table.
  * This structure should be present at the head of a larger structure that
  * includes the data to be stored in the hash table. Normally this might
  * include a copy or reference to the actual key, as well as additional data
  * that is associated with the key.
  *
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private, and
  * should only be accessed via the proper functions.
  */
typedef struct sky_hashtable_item_s sky_hashtable_item_t;
struct sky_hashtable_item_s {
    /** @cond **/
    sky_hashtable_item_t *              next;
    uintptr_t                           key_hash;
    /** @endcond **/
};

/** An initializer macro that can be used to initialize a hash table item
  * statically.
  */
#define SKY_HASHTABLE_ITEM_INITIALIZER  { NULL, 0 }

/** @cond **/
typedef struct sky_hashtable_segment_s sky_hashtable_segment_t;
struct sky_hashtable_segment_s {
    sky_hashtable_item_t *              items[0];
};
/** @endcond **/


typedef struct sky_hashtable_s sky_hashtable_t;

/** The signature of a function to be called to compare an existing hash table
  * item with a key.
  * This function will only ever be called for an item that has the same hash
  * value as the key.
  *
  * @param[in]  item    the existing hash table item to be compared.
  * @param[in]  key     the key to be compared.
  * @return     @c SKY_TRUE if the @a key matches the @a item, or
  *             @c SKY_FALSE if it does not.
  */
typedef sky_bool_t (*sky_hashtable_item_compare_t)(sky_hashtable_item_t *item,
                                                   void *                key);

/** The signature of a function to be called to visit an item in a hash table
  * during garbage collection.
  *
  * @param[in]  item    the item being visited.
  * @param[in]  data    the visitation data.
  */
typedef void (*sky_hashtable_item_visit_t)(sky_hashtable_item_t *   item,
                                           sky_object_visit_data_t *data);

/** A structure representing a hash table.
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private, and
  * should only be accessed via the proper functions.
  */
struct sky_hashtable_s {
    /** @cond **/
    ssize_t                             length;
    ssize_t                             max_length;
    size_t                              dummy_nbytes;
    sky_hashtable_item_compare_t        item_compare;
    sky_hashtable_segment_t *           segments[SKY_HASHTABLE_SEGMENT_COUNT];
    /** @endcond **/
};


/** Initialize a hash table for use.
  * Initializes a hash table for use. Sufficient memory for the table
  * itself must be allocated prior to calling this function, and the memory
  * must be initialized with zero bytes. Use sky_hashtable_cleanup() when
  * the table is no longer needed.
  *
  * @param[in]  table           the table to initialize.
  * @param[in]  item_compare    the function to call to compare an existing
  *                             item with a key. Must not be @c NULL.
  */
SKY_EXTERN void
sky_hashtable_init(sky_hashtable_t *            table,
                   sky_hashtable_item_compare_t item_compare);

/** Allocate memory for a hash table and initialize it for use.
  * Use sky_hashtable_destroy() when the table is no longer needed.
  *
  * @param[in]  item_compare    the function to call to compare an existing
  *                             item with a key. Must not be @c NULL.
  * @return     a new hash table ready for use.
  */
SKY_EXTERN sky_hashtable_t *
sky_hashtable_create(sky_hashtable_item_compare_t item_compare);

/** Clean up a previously initialized hash table when it is no longer needed.
  * The memory for the hash table itself is not released in any way. This
  * function should be used for tables initialized via sky_hashtable_init().
  *
  * @param[in]  table       the table to clean up.
  * @param[in]  item_free   the function to call to free an item that remains
  *                         in the table. May be @c NULL.
  */
SKY_EXTERN void
sky_hashtable_cleanup(sky_hashtable_t * table,
                      sky_free_t        item_free);

/** Clean up a previously initialized hash table and deallocate its memory.
  * This function should be used for tables initialized via
  * sky_hashtable_create().
  *
  * @param[in]  table       the table to destroy.
  * @param[in]  item_free   the function to call to free an item that remains
  *                         in the table. May be @c NULL.
  */
SKY_EXTERN void
sky_hashtable_destroy(sky_hashtable_t * table,
                      sky_free_t        item_free);

/** Visit a table's contents during a garbage collection cycle.
  * This function should only be called if the items stored in the table
  * contain Skython objects that need to be visited for garbage collection.
  * The table itself does not use any object references, and therefore does
  * not itself need visiting.
  *
  * @param[in]  table       the table to visit.
  * @param[in]  item_visit  the function to call to visit an item stored in the
  *                         table.
  * @param[in]  data        the visitation data to pass through to each
  *                         invocation of @a item_visit.
  */
SKY_EXTERN void
sky_hashtable_visit(sky_hashtable_t *           table,
                    sky_hashtable_item_visit_t  item_visit,
                    sky_object_visit_data_t *   data);

/** Return the number of items present in the hash table.
  *
  * @param[in]  table       the table for which the number of items it contains
  *                         is to be returned.
  * @return     the number of items contained by @a table.
  */
SKY_EXTERN_INLINE ssize_t
sky_hashtable_length(sky_hashtable_t *table)
{
    return table->length;
}


/** Return the number of bytes being used internally by a hash table.
  * The value returned does not include memory allocated to the hash table
  * itself if it was allocated via sky_hashtable_create(). It includes only
  * internal memory that is not accessible externally. The value returned can
  * be considered overhead. It does not include memory allocated for items
  * added to the table via sky_hashtable_insert().
  *
  * @param[in]  table       the table for which the amount of memory being
  *                         used internally is to be returned.
  * @return     the amount of memory being used internally.
  */
SKY_EXTERN_INLINE size_t
sky_hashtable_sizeof(sky_hashtable_t *table)
{
    return table->dummy_nbytes;
}


/** Set internal capacity for a hash table.
  * The internal capacity for a table can only ever be increased. This is
  * useful primarily when a hash table is first created if it is known that it
  * is going to be a very large table.
  *
  * @param[in]  table       the table on which to set the capacity.
  * @param[in]  capacity    the capacity to set, which will be rounded up to
  *                         the next closest power of 2.
  */
SKY_EXTERN void
sky_hashtable_setcapacity(sky_hashtable_t *table, ssize_t capacity);


/** Delete an item from a hash table.
  * The @a key_hash and @a key parameters will be used to find the item in the
  * hash table. If the hash value is not found in the table at all, the return
  * will be @c NULL. If the hash value is found, the table's item comparison
  * function will be called with @a key for each item having the same hash
  * value. If the item is found, it will be removed from the hash table and
  * returned. It is the responsibility of the caller to free it as appropriate.
  * If the item comparator for the hashtable could potentially delete an item
  * from the table (meaning, it's possible to mutate the hashtable during
  * item lookup), the caller must use sky_hazard_pointer_retire() to free the
  * item returned by sky_hashtable_delete(). If @a key is @c NULL, an arbitrary
  * key will be chosen to be removed from the table.
  *
  * @param[in]  table       the table from which the item is to be deleted.
  * @param[in]  key_hash    the hash value for the key to be deleted.
  * @param[in]  key         the key to be deleted.
  * @return     @c NULL if the key is not found; otherwise, the item that was
  *             deleted from the table. It is the caller's responsibility to
  *             free the returned item as appropriate.
  */
SKY_EXTERN sky_hashtable_item_t *
sky_hashtable_delete(sky_hashtable_t *      table, 
                     uintptr_t              key_hash,
                     void *                 key);

/** Insert a new item into a hash table.
  * The caller is responsible for pre-allocating the new item to be inserted
  * into the hash table. The @a key_hash and @a key parameters will be used
  * to find the appropriate location in the hash table for the item. If an
  * existing item is found with a matching hash value and key, the new item
  * will not be inserted, and the return will be the existing item; otherwise,
  * the new item will be inserted, and the return will be the inserted item. A
  * copy of the item is not made, and @a new_item becomes property of the table
  * when it is inserted; however, if it is not inserted, it is the caller's
  * responsibility to release its memory as appropriate.
  *
  * @param[in]  table       the table into which @a new_item is to be inserted.
  * @param[in]  new_item    the new item to insert into the @a table.
  * @param[in]  key_hash    the hash value of the key.
  * @param[in]  key         the key.
  * @return     if the key already exists in the hash table, the return will be
  *             the existing item; otherwise, it will be @a new_item. If the
  *             table is full, the return will be @c NULL.
  */
SKY_EXTERN sky_hashtable_item_t *
sky_hashtable_insert(sky_hashtable_t *      table,
                     sky_hashtable_item_t * new_item,
                     uintptr_t              key_hash,
                     void *                 key);

/** Find an item in a hash table.
  * The @a key_hash and @a key parameters will be used to find the item in the
  * hash table. If the hash value is not found in the table at all, the return
  * will be @c NULL. If the hash value is found, the table's item comparison
  * function will be called with @a key for each item having the same hash
  * value. If none of the keys match, the return will be @c NULL; otherwise,
  * the matching item will be returned.
  *
  * @param[in]  table       the table to search.
  * @param[in]  key_hash    the hash value of the key to be found.
  * @param[in]  key         the key to be found.
  * @return     the item if its key is found; otherwise, @c NULL.
  */
SKY_EXTERN sky_hashtable_item_t *
sky_hashtable_lookup(sky_hashtable_t *      table,
                     uintptr_t              key_hash,
                     void *                 key);


/** A structure representing a hash table iterator.
  * While the internals of this structure are, by necessity, exposed publicly,
  * they should not be treated as being public. The data is private, and
  * should only be accessed via the proper functions.
  */
typedef struct sky_hashtable_iterator_s {
/** @cond **/
    sky_hashtable_t *                       table;
    sky_hashtable_item_t *                  next_item;
/** @endcond **/
} sky_hashtable_iterator_t;

/** Initialize a hash table iterator for use.
  * Initializes an iterator for use. Sufficient memory for the iterator itself
  * must be allocated prior to calling this function.
  * Use sky_hashtable_iterator_cleanup() when the table is no longer needed.
  *
  * @param[in]  iterator    the iterator to initialize.
  * @param[in]  table       the table to iterate.
  */
SKY_EXTERN void
sky_hashtable_iterator_init(sky_hashtable_iterator_t *  iterator,
                            sky_hashtable_t *           table);

/** Allocate memory for a hash table iterator and initialize it.
  * Use sky_hashtable_iterator_destroy() when the iterator is no longer needed.
  *
  * @param[in]  table       the table to iterate.
  * @return     a new hash table iterator ready for use.
  */
SKY_EXTERN sky_hashtable_iterator_t *
sky_hashtable_iterator_create(sky_hashtable_t *table);

/** Clean up a previously initialized hash table iterator when it is no longer
  * needed.
  * The memory for the iterator itself is not released in any way. This
  * function should be used for iterators initialized via
  * sky_hashtable_iterator_init().
  *
  * @param[in]  iterator    the iterator to clean up.
  */
SKY_EXTERN void
sky_hashtable_iterator_cleanup(sky_hashtable_iterator_t *iterator);

/** Clean up a previously initialized hash table iterator and deallocate its
  * memory.
  * This function should be used for iterators initialized via
  * sky_hashtable_iterator_create().
  *
  * @param[in]  iterator    the iterator to destroy.
  */
SKY_EXTERN void
sky_hashtable_iterator_destroy(sky_hashtable_iterator_t *iterator);

/** Get the next item from a hash table iterator.
  *
  * @param[in]  iterator    the iterator.
  * @return     the next item from the hash table via @a iterator, or @c NULL
  *             if there are no more items.
  */
SKY_EXTERN sky_hashtable_item_t *
sky_hashtable_iterator_next(sky_hashtable_iterator_t *iterator);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_HASHTABLE_H__ */

/** @} **/
