#include "sky_private.h"


typedef struct sky_range_data_s {
    sky_integer_t                       start;
    sky_integer_t                       stop;
    sky_integer_t                       step;
    sky_integer_t                       len;
} sky_range_data_t;

SKY_EXTERN_INLINE sky_range_data_t *
sky_range_data(sky_object_t object)
{
    if (sky_range_type == sky_object_type(object)) {
        return (sky_range_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_range_type);
}

void
sky_range_init(sky_object_t self,
               sky_object_t start,
               sky_object_t stop,
               sky_object_t step);


static sky_type_t sky_range_fast_iterator_type;

typedef struct sky_range_fast_iterator_data_s {
    ssize_t                             index;
    ssize_t                             start;
    ssize_t                             step;
    ssize_t                             limit;
} sky_range_fast_iterator_data_t;

typedef struct sky_range_fast_iterator_s {
    sky_object_data_t                   object_data;
    sky_range_fast_iterator_data_t      iterator_data;
} *sky_range_fast_iterator_t;

SKY_EXTERN_INLINE sky_range_fast_iterator_data_t *
sky_range_fast_iterator_data(sky_object_t object)
{
    if (sky_range_fast_iterator_type == sky_object_type(object)) {
        return (sky_range_fast_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_range_fast_iterator_type);
}


static sky_type_t sky_range_slow_iterator_type;

typedef struct sky_range_slow_iterator_data_s {
    sky_range_t                         range;
    sky_integer_t                       index;
} sky_range_slow_iterator_data_t;

typedef struct sky_range_slow_iterator_s {
    sky_object_data_t                   object_data;
    sky_range_slow_iterator_data_t      iterator_data;
} *sky_range_slow_iterator_t;

SKY_EXTERN_INLINE sky_range_slow_iterator_data_t *
sky_range_slow_iterator_data(sky_object_t object)
{
    if (sky_range_slow_iterator_type == sky_object_type(object)) {
        return (sky_range_slow_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_range_slow_iterator_type);
}


static void
sky_range_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_range_data_t    *self_data = data;

    sky_object_visit(self_data->start, visit_data);
    sky_object_visit(self_data->stop, visit_data);
    sky_object_visit(self_data->step, visit_data);
    sky_object_visit(self_data->len, visit_data);
}


static void
sky_range_slow_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_range_slow_iterator_data_t  *self_data = data;

    sky_object_visit(self_data->range, visit_data);
    sky_object_visit(self_data->index, visit_data);
}


sky_bool_t
sky_range_contains(sky_range_t self, sky_object_t item)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_object_t    iter, o;
    sky_integer_t   x;

    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        item = sky_number_index(item);
        if (sky_object_compare(self_data->step,
                               sky_integer_zero,
                               SKY_COMPARE_OP_GREATER))
        {
            if (sky_object_compare(item,
                                   self_data->start,
                                   SKY_COMPARE_OP_LESS) ||
                sky_object_compare(item,
                                   self_data->stop,
                                   SKY_COMPARE_OP_GREATER_EQUAL))
            {
                return SKY_FALSE;
            }
        }
        else {
            if (sky_object_compare(item,
                                   self_data->stop,
                                   SKY_COMPARE_OP_LESS_EQUAL) ||
                sky_object_compare(item,
                                   self_data->start,
                                   SKY_COMPARE_OP_GREATER))
            {
                return SKY_FALSE;
            }
        }

        /* return (item - self.start) % self.step == 0 */
        x = sky_number_subtract(item, self_data->start);
        x = sky_number_modulo(x, self_data->step);
        return sky_object_compare(x, sky_integer_zero, SKY_COMPARE_OP_EQUAL);
    }

    iter = sky_object_iter(self);
    while ((o = sky_object_next(iter, NULL)) != NULL) {
        if (sky_object_compare(item, o, SKY_COMPARE_OP_EQUAL)) {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


static const char sky_range_count_doc[] =
"rangeobject.count(value) -> integer -- return number of occurrences of value";

static sky_integer_t
sky_range_count(sky_range_t self, sky_object_t value)
{
    sky_object_t    iter, o;
    sky_integer_t   count;

    if (SKY_OBJECT_METHOD_SLOT(value, INDEX)) {
        return (sky_range_contains(self, value) ? sky_integer_one
                                                : sky_integer_zero);
    }

    count = sky_integer_zero;
    iter = sky_object_iter(self);
    while ((o = sky_object_next(iter, NULL)) != NULL) {
        if (sky_object_compare(value, o, SKY_COMPARE_OP_EQUAL)) {
            count = sky_number_inplace_add(count, sky_integer_one);
        }
    }

    return count;
}


sky_range_t
sky_range_create(sky_object_t start, sky_object_t stop, sky_object_t step)
{
    sky_range_t range;

    if (sky_object_isnull(start)) {
        sky_error_raise_string(sky_TypeError,
                               "range requires at least one argument");
    }

    range = sky_object_allocate(sky_range_type);
    sky_range_init(range, start, stop, step);

    return range;
}


sky_object_t
sky_range_eq(sky_range_t    self,
             sky_object_t   other)
{
    sky_range_data_t    *self_data = sky_range_data(self),
                        *other_data = sky_range_data(other);

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(other, sky_range_type)) {
        return sky_NotImplemented;
    }

    /* if len(self) != len(other): return False */
    /* if not len(self): return True */
    /* if self.start != other.start: return False */
    /* if len(self) == 1: return True */
    /* return self.step == other.step */

    if (sky_object_compare(self_data->len,
                           other_data->len,
                           SKY_COMPARE_OP_NOT_EQUAL))
    {
        return sky_False;
    }
    if (!sky_object_bool(self_data->len)) {
        return sky_True;
    }
    if (!sky_object_compare(self_data->start,
                            other_data->start,
                            SKY_COMPARE_OP_EQUAL))
    {
        return sky_False;
    }
    if (sky_object_compare(self_data->len,
                           sky_integer_one,
                           SKY_COMPARE_OP_EQUAL))
    {
        return sky_True;
    }
    return (sky_object_compare(self_data->step,
                               other_data->step,
                               SKY_COMPARE_OP_EQUAL) ? sky_True : sky_False);
}


static sky_object_t
sky_range_getitem_index(sky_range_t self, sky_integer_t index)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    if (sky_object_compare(index,
                           sky_integer_zero,
                           SKY_COMPARE_OP_LESS))
    {
        index = sky_number_add(self_data->len, index);
    }

    if (sky_integer_isnegative(index) ||
        sky_object_compare(index,
                           self_data->len,
                           SKY_COMPARE_OP_GREATER_EQUAL))
    {
        sky_error_raise_string(sky_IndexError,
                               "range object index out of range");
    }

    return sky_number_add(sky_number_multiply(index, self_data->step),
                          self_data->start);
}

static sky_object_t
sky_range_getitem_slice(sky_range_t self, sky_slice_t slice)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_bool_t      has_elements;
    sky_integer_t   candidate, start, step, stop, substart, substep, substop;

    step = sky_slice_step(slice);
    if (sky_object_isnull(step)) {
        step = sky_integer_one;
    }
    else {
        step = sky_number_index(step);
        if (sky_object_compare(step, sky_integer_zero, SKY_COMPARE_OP_EQUAL)) {
            sky_error_raise_string(sky_ValueError, "step cannot be zero");
        }
    }

    start = sky_slice_start(slice);
    if (sky_object_isnull(start)) {
        if (sky_integer_isnegative(step)) {
            start = sky_number_subtract(self_data->len, sky_integer_one);
        }
        else {
            start = sky_integer_zero;
        }
    }
    else {
        candidate = sky_number_index(start);
        if (sky_integer_isnegative(candidate)) {
            start = sky_number_add(self_data->len, candidate);
        }
        else {
            start = candidate;
        }
        if (sky_integer_isnegative(start)) {
            if (sky_integer_isnegative(step)) {
                start = sky_integer_create(-1);
            }
            else {
                start = sky_integer_zero;
            }
        }
        else if (sky_object_compare(start,
                                    self_data->len,
                                    SKY_COMPARE_OP_GREATER_EQUAL))
        {
            if (sky_integer_isnegative(step)) {
                start = sky_number_subtract(self_data->len, sky_integer_one);
            }
            else {
                start = self_data->len;
            }
        }
    }

    stop = sky_slice_stop(slice);
    if (sky_object_isnull(stop)) {
        if (sky_integer_isnegative(step)) {
            stop = sky_integer_create(-1);
        }
        else {
            stop = self_data->len;
        }
    }
    else {
        candidate = sky_number_index(stop);
        if (sky_integer_isnegative(candidate)) {
            stop = sky_number_add(self_data->len, candidate);
        }
        else {
            stop = candidate;
        }
        if (sky_integer_isnegative(stop)) {
            if (sky_integer_isnegative(step)) {
                stop = sky_integer_create(-1);
            }
            else {
                stop = sky_integer_zero;
            }
        }
        else if (sky_object_compare(stop,
                                    self_data->len,
                                    SKY_COMPARE_OP_GREATER_EQUAL))
        {
            if (sky_integer_isnegative(step)) {
                stop = sky_number_subtract(self_data->len, sky_integer_one);
            }
            else {
                stop = self_data->len;
            }
        }
    }

    if (sky_integer_isnegative(step)) {
        has_elements = sky_object_compare(start, stop, SKY_COMPARE_OP_GREATER);
    }
    else {
        has_elements = sky_object_compare(start, stop, SKY_COMPARE_OP_LESS);
    }

    substep = sky_number_multiply(self_data->step, step);
    substart = sky_number_add(sky_number_multiply(start, self_data->step),
                              self_data->start);
    if (has_elements) {
        substop = sky_number_add(sky_number_multiply(stop, self_data->step),
                                 self_data->start);
    }
    else {
        substop = substart;
    }

    return sky_range_create(substart, substop, substep);
}

sky_object_t
sky_range_getitem(sky_range_t self, sky_object_t item)
{
    if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        return sky_range_getitem_index(self, sky_number_index(item));
        item = sky_number_index(item);
    }

    if (sky_object_isa(item, sky_slice_type)) {
        return sky_range_getitem_slice(self, item);
    }

    sky_error_raise_format(sky_TypeError,
                           "item must be an index or slice; got %#@",
                           sky_type_name(sky_object_type(item)));
}


uintptr_t
sky_range_hash(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_tuple_t tuple;

    /* if not len(self): return hash((len(self), None, None)) */
    /* if len(self) == 1: return hash((len(self), self.start, None)) */
    /* return hash((len(self), self.start, self.step)) */

    if (sky_object_compare(self_data->len,
                           sky_integer_zero,
                           SKY_COMPARE_OP_EQUAL))
    {
        tuple = sky_tuple_pack(3, self_data->len,
                                  sky_None,
                                  sky_None);
    }
    else if (sky_object_compare(self_data->len,
                                sky_integer_one,
                                SKY_COMPARE_OP_EQUAL))
    {
        tuple = sky_tuple_pack(3, self_data->len,
                                  self_data->start,
                                  sky_None);
    }
    else {
        tuple = sky_tuple_pack(3, self_data->len,
                                  self_data->start,
                                  self_data->step);
    }

    return sky_object_hash(tuple);
}


static const char sky_range_index_doc[] =
"rangeobject.index(value) -> integer -- return index of value.\n"
"Raises ValueError if the value is not present.";


sky_integer_t
sky_range_index(sky_range_t self, sky_object_t value)
{
    sky_object_t        iter, o;
    sky_integer_t       index;
    sky_range_data_t    *self_data;

    if (SKY_OBJECT_METHOD_SLOT(value, INDEX)) {
        value = sky_number_index(value);
        if (sky_range_contains(self, value)) {
            self_data = sky_range_data(self);
            index = sky_number_subtract(value, self_data->start);
            index = sky_number_floordivide(index, self_data->step);
            return index;
        }
    }
    else {
        index = sky_integer_zero;
        iter = sky_object_iter(self);
        while ((o = sky_object_next(iter, NULL)) != NULL) {
            if (sky_object_compare(value, o, SKY_COMPARE_OP_EQUAL)) {
                return index;
            }
            index = sky_number_inplace_add(index, sky_integer_one);
        }
    }

    sky_error_raise_format(sky_ValueError, "%#@ is not in range", value);
}


void
sky_range_init(sky_object_t self,
               sky_object_t start,
               sky_object_t stop,
               sky_object_t step)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_integer_t   len, lower, upper;

    if (sky_object_isnull(stop) && sky_object_isnull(step)) {
        step = sky_integer_one;
        stop = sky_number_index(start);
        start = sky_integer_zero;
    }
    else {
        start = sky_number_index(start);
        stop = sky_number_index(stop);
        if (sky_object_isnull(step)) {
            step = sky_integer_one;
        }
        else {
            step = sky_number_index(step);
            if (sky_object_compare(step,
                                   sky_integer_zero,
                                   SKY_COMPARE_OP_EQUAL))
            {
                sky_error_raise_string(sky_ValueError,
                                       "range step must not be zero");
            }
        }
    }

    /* Calculate the range's length */
    if (sky_object_compare(step, sky_integer_zero, SKY_COMPARE_OP_GREATER)) {
        lower = start;
        upper = stop;
    }
    else {
        lower = stop;
        upper = start;
    }
    if (sky_object_compare(lower, upper, SKY_COMPARE_OP_GREATER_EQUAL)) {
        len = sky_integer_zero;
    }
    else {
        /* len = (((upper - lower) - 1) // step) + 1 */
        len = sky_number_subtract(upper, lower);
        len = sky_number_subtract(len, sky_integer_one);
        len = sky_number_floordivide(len, sky_number_absolute(step));
        len = sky_number_add(len, sky_integer_one);
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->start)), start, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->stop)), stop, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->step)), step, self);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->len)), len, self);
}


sky_object_t
sky_range_iter(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_object_t    iterator;
    sky_integer_t   x, y;

    /* if ((len(self) - 1) * range.step) + range.start >= SSIZE_MAX - 1:
     *     slow path (use sky_number API instead of native ssize_t)
     */
    x = sky_number_subtract(self_data->len, sky_integer_one);
    x = sky_number_multiply(x, self_data->step);
    x = sky_number_add(x, self_data->start);
    y = sky_number_subtract(sky_integer_create(SSIZE_MAX), sky_integer_one);

    if (sky_object_compare(x, y, SKY_COMPARE_OP_GREATER_EQUAL) ||
        sky_object_compare(self_data->start,
                           sky_integer_create(SSIZE_MIN),
                           SKY_COMPARE_OP_LESS) ||
        sky_object_compare(self_data->start,
                           sky_integer_create(SSIZE_MAX),
                           SKY_COMPARE_OP_GREATER) ||
        sky_object_compare(self_data->step,
                           sky_integer_create(SSIZE_MIN),
                           SKY_COMPARE_OP_LESS) ||
        sky_object_compare(self_data->step,
                           sky_integer_create(SSIZE_MAX),
                           SKY_COMPARE_OP_GREATER) ||
        sky_object_compare(self_data->len,
                           sky_integer_create(SSIZE_MIN),
                           SKY_COMPARE_OP_LESS) ||
        sky_object_compare(self_data->len,
                           sky_integer_create(SSIZE_MAX),
                           SKY_COMPARE_OP_GREATER))
    {
        sky_range_slow_iterator_data_t  *iterator_data;

        iterator = sky_object_allocate(sky_range_slow_iterator_type);
        iterator_data = sky_range_slow_iterator_data(iterator);
        sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->range)),
                          self,
                          iterator);
        sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->index)),
                          sky_integer_zero,
                          iterator);
    }
    else {
        sky_range_fast_iterator_data_t  *iterator_data;

        iterator = sky_object_allocate(sky_range_fast_iterator_type);
        iterator_data = sky_range_fast_iterator_data(iterator);
        iterator_data->index = 0;
        iterator_data->start = sky_integer_value(self_data->start,
                                                 SSIZE_MIN,
                                                 SSIZE_MAX,
                                                 NULL);
        iterator_data->step = sky_integer_value(self_data->step,
                                                SSIZE_MIN,
                                                SSIZE_MAX,
                                                NULL);
        iterator_data->limit = sky_integer_value(self_data->len,
                                                 SSIZE_MIN,
                                                 SSIZE_MAX,
                                                 NULL);
    }

    return iterator;
}


ssize_t
sky_range_len(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    return sky_integer_value(self_data->len, SSIZE_MIN, SSIZE_MAX, NULL);
}


sky_object_t
sky_range_reduce(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    return sky_object_build("(O(OOO))",
                            sky_object_type(self),
                            self_data->start,
                            self_data->stop,
                            self_data->step);
}


sky_object_t
sky_range_reversed(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    sky_integer_t   start, step, stop;

    /* the reversed range is:
     *      range(start + (len - 1) * step, start - step, -step)
     */
    start = sky_number_subtract(self_data->len, sky_integer_one);
    start = sky_number_multiply(start, self_data->step);
    start = sky_number_add(start, self_data->start);

    stop = sky_number_subtract(self_data->start, self_data->step);
    step = sky_number_negative(self_data->step);

    return sky_object_iter(sky_range_create(start, stop, step));
}


sky_string_t
sky_range_repr(sky_range_t self)
{
    sky_range_data_t    *self_data = sky_range_data(self);

    if (sky_object_compare(self_data->step,
                           sky_integer_one,
                           SKY_COMPARE_OP_EQUAL))
    {
        return sky_string_createfromformat("range(%@, %@)",
                                           self_data->start,
                                           self_data->stop);
    }

    return sky_string_createfromformat("range(%@, %@, %@)",
                                       self_data->start,
                                       self_data->stop,
                                       self_data->step);
}


sky_object_t
sky_range_iterator_new(sky_type_t cls, sky_range_t range)
{
    if (sky_range_fast_iterator_type != cls &&
        sky_range_slow_iterator_type != cls)
    {
        sky_error_raise_string(sky_TypeError,
                               "cls must be a range iterator type");
    }

    return sky_object_iter(range);
}


sky_object_t
sky_range_iterator_iter(sky_object_t self)
{
    return self;
}


ssize_t
sky_range_fast_iterator_len(sky_range_fast_iterator_t self)
{
    sky_range_fast_iterator_data_t  *self_data =
                                    sky_range_fast_iterator_data(self);

    return self_data->limit - self_data->index;
}


ssize_t
sky_range_slow_iterator_len(sky_range_slow_iterator_t self)
{
    sky_range_slow_iterator_data_t  *self_data =
                                    sky_range_slow_iterator_data(self);

    sky_integer_t       len;
    sky_range_data_t    *range_data;

    range_data = sky_range_data(self_data->range);
    len = sky_number_subtract(range_data->len, self_data->index);
    return sky_integer_value(len, SSIZE_MIN, SSIZE_MAX, NULL);
}


sky_object_t
sky_range_fast_iterator_next(sky_range_fast_iterator_t self)
{
    sky_range_fast_iterator_data_t  *self_data =
                                    sky_range_fast_iterator_data(self);

    if (self_data->index >= self_data->limit) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    return sky_integer_create(self_data->start +
                              (self_data->index++ * self_data->step));
}


sky_object_t
sky_range_slow_iterator_next(sky_range_slow_iterator_t self)
{
    sky_range_slow_iterator_data_t  *self_data =
                                    sky_range_slow_iterator_data(self);

    sky_integer_t       result;
    sky_range_data_t    *range_data;

    range_data = sky_range_data(self_data->range);
    if (sky_object_compare(self_data->index,
                           range_data->len,
                           SKY_COMPARE_OP_GREATER_EQUAL))
    {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    result = sky_number_multiply(self_data->index, range_data->step);
    result = sky_number_add(result, range_data->start);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                      sky_number_inplace_add(self_data->index,
                                             sky_integer_one),
                      self);

    return result;
}


sky_object_t
sky_range_fast_iterator_reduce(sky_range_fast_iterator_t self)
{
    sky_range_fast_iterator_data_t  *self_data =
                                    sky_range_fast_iterator_data(self);

    sky_object_t    start, step, stop;

    start = sky_integer_create(self_data->start);
    stop = sky_integer_create(self_data->start +
                              (self_data->limit * self_data->step));
    step = sky_integer_create(self_data->step);

    return sky_object_build("(O(O)iz)",
                            sky_module_getbuiltin("iter"),
                            sky_range_create(start, stop, step),
                            self_data->index);
}


sky_object_t
sky_range_slow_iterator_reduce(sky_range_slow_iterator_t self)
{
    sky_range_slow_iterator_data_t  *self_data =
                                    sky_range_slow_iterator_data(self);

    return sky_object_build("(O(O)O)",
                            sky_module_getbuiltin("iter"),
                            self_data->range,
                            self_data->index);
}


void
sky_range_fast_iterator_setstate(sky_range_fast_iterator_t  self,
                                 sky_object_t               state)
{
    sky_range_fast_iterator_data_t  *self_data =
                                    sky_range_fast_iterator_data(self);

    self_data->index = sky_integer_value(state, SSIZE_MIN, SSIZE_MAX, NULL);
    if (self_data->index < 0) {
        self_data->index = 0;
    }
    else if (self_data->index > self_data->limit) {
        self_data->index = self_data->limit;
    }
}


void
sky_range_slow_iterator_setstate(sky_range_slow_iterator_t  self,
                                 sky_object_t               state)
{
    sky_range_slow_iterator_data_t  *self_data =
                                    sky_range_slow_iterator_data(self);

    sky_range_data_t    *range_data;

    range_data = sky_range_data(self_data->range);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                      sky_number_index(state),
                      self);
    if (sky_object_compare(self_data->index,
                           sky_integer_zero,
                           SKY_COMPARE_OP_LESS))
    {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                          sky_integer_zero,
                          self);
    }
    else if (sky_object_compare(self_data->index,
                                range_data->len,
                                SKY_COMPARE_OP_GREATER))
    {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                          range_data->len,
                          self);
    }
}


static const char sky_range_type_doc[] =
"range(stop) -> range object\n"
"range(start, stop[, step]) -> range object\n"
"\n"
"Return a virtual sequence of numbers from start to stop by step.";


SKY_TYPE_DEFINE_SIMPLE(range,
                       "range",
                       sizeof(sky_range_data_t),
                       NULL,
                       NULL,
                       sky_range_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       sky_range_type_doc);


void
sky_range_initialize_library(void)
{
    sky_type_t          type;
    sky_type_template_t template;

    sky_type_initialize_builtin(sky_range_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_range_type,
            "__init__",
            (sky_native_code_function_t)sky_range_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "start", SKY_DATA_TYPE_OBJECT, NULL,
            "stop", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "step", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);

    sky_type_setmethodslots(sky_range_type,
            "__repr__", sky_range_repr,
            "__eq__", sky_range_eq,
            "__hash__", sky_range_hash,
            "__len__", sky_range_len,
            "__getitem__", sky_range_getitem,
            "__iter__", sky_range_iter,
            "__reversed__", sky_range_reversed,
            "__contains__", sky_range_contains,
            "__reduce__", sky_range_reduce,
            NULL);

    sky_type_setattr_builtin(
            sky_range_type,
            "count",
            sky_function_createbuiltin(
                    "range.count",
                    sky_range_count_doc,
                    (sky_native_code_function_t)sky_range_count,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_range_type,
            "index",
            sky_function_createbuiltin(
                    "range.index",
                    sky_range_index_doc,
                    (sky_native_code_function_t)sky_range_index,
                    SKY_DATA_TYPE_OBJECT_INTEGER,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmembers(sky_range_type,
            "start",    NULL, offsetof(sky_range_data_t, start),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "stop",     NULL, offsetof(sky_range_data_t, stop),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "step",     NULL, offsetof(sky_range_data_t, step),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);


    /* Iterators */
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("range_fast_iterator"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_range_fast_iterator_data_t),
                                   0,
                                   NULL,
                                   NULL);

    sky_type_setmethodslotwithparameters(
            type,
            "__new__",
            (sky_native_code_function_t)sky_range_iterator_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "range", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_range_type,
            NULL);

    sky_type_setmethodslots(type,
            "__len__", sky_range_fast_iterator_len,
            "__iter__", sky_range_iterator_iter,
            "__next__", sky_range_fast_iterator_next,
            "__reduce__", sky_range_fast_iterator_reduce,
            "__setstate__", sky_range_fast_iterator_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_range_fast_iterator_type),
            type,
            SKY_TRUE);

    memset(&template, 0, sizeof(template));
    template.sizeof_struct = sizeof(template);
    template.visit = sky_range_slow_iterator_instance_visit;
    type = sky_type_createbuiltin(SKY_STRING_LITERAL("range_slow_iterator"),
                                   NULL,
                                   SKY_TYPE_CREATE_FLAG_FINAL,
                                   sizeof(sky_range_slow_iterator_data_t),
                                   0,
                                   NULL,
                                   NULL);

    sky_type_setmethodslotwithparameters(
            type,
            "__new__",
            (sky_native_code_function_t)sky_range_iterator_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "range", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_range_type,
            NULL);

    sky_type_setmethodslots(type,
            "__len__", sky_range_slow_iterator_len,
            "__iter__", sky_range_iterator_iter,
            "__next__", sky_range_slow_iterator_next,
            "__reduce__", sky_range_slow_iterator_reduce,
            "__setstate__", sky_range_slow_iterator_setstate,
            NULL);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_range_slow_iterator_type),
            type,
            SKY_TRUE);
}
