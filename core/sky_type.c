#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_cell.h"
#include "sky_function_private.h"
#include "sky_type_methods.h"
#include "sky_python_compiler.h"


#define sky_type_struct_includes(_p, _s, _f) \
        (_p->sizeof_struct >= offsetof(_s, _f) + sizeof(_p->_f))


static sky_object_t
sky_type_dict_getter(sky_object_t self, sky_type_t type);

static void
sky_type_dict_setter(sky_object_t self, sky_object_t value, sky_type_t type);


/* Default allocation functions. The type initializer code will pick the
 * appropriate one based on the size and alignment. In all cases, sky_free()
 * will be the default free function.
 */
void *
sky_type_allocate_calloc(SKY_UNUSED size_t alignment, size_t size)
{
    return sky_calloc(1, size);
}

void *
sky_type_allocate_memalign(size_t alignment, size_t size)
{
    return memset(sky_memalign(alignment, size), 0x00, size);
}


void
sky_type_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    if (sky_object_bool(kws)) {
        sky_error_raise_string(
                sky_TypeError,
                "type.__init__() takes no keyword arguments");
    }
    if (sky_object_isnull(args) ||
        (sky_tuple_len(args) != 1 && sky_tuple_len(args) != 3))
    {
        sky_error_raise_string(
                sky_TypeError,
                "type.__init__() takes 1 or 3 arguments");
    }

    sky_base_object_init(self, NULL, NULL);
}


sky_bool_t
sky_type_instancecheck(sky_object_t cls, sky_object_t instance)
{
    sky_object_t    instance_cls;

    if (sky_object_isa(instance, cls)) {
        return SKY_TRUE;
    }
    if (!(instance_cls = sky_object_getattr(instance,
                                            SKY_STRING_LITERAL("__class__"),
                                            NULL)))
    {
        return SKY_FALSE;
    }
    if (sky_object_type(instance) != instance_cls &&
        sky_object_isa(instance_cls, sky_type_type))
    {
        return sky_type_issubtype(instance_cls, cls);
    }
    return SKY_FALSE;
}


static void
sky_type_instance_deallocate(void *ptr)
{
    sky_type_t  type = ptr;

    /* This is done here instead of in sky_type_instance_finalize(), because
     * the information is needed during finalization of instances of this type
     * that may occur during the same sweep. If the MRO is destroyed during
     * finalization, the information will not be available and object instances
     * may not be fully finalized.
     */
    if (sky_base_object_type != ptr && sky_type_type != ptr) {
        sky_free(type->type_data.bases);
        type->type_data.bases = NULL;
        sky_free(type->type_data.mro);
        type->type_data.mro = NULL;
        sky_free(type->type_data.data_offsets);
        type->type_data.data_offsets = NULL;
    }

    if (!(type->type_data.flags & SKY_TYPE_FLAG_STATIC)) {
        sky_free(ptr);
    }
    else {
        /* For built-in types, simply leave their object header in a newly
         * initialized state. They're not dynamically allocated, so an attempt
         * to free them will end badly. It's necessary for them to be cleared
         * out so that they can be re-initialized for use again later if
         * necessary.
         */
        SKY_OBJECT_INITIALIZE(ptr, sky_type_type);
    }
}


static sky_type_t
sky_type_metatype(sky_type_t metatype, sky_object_t bases)
{
    sky_type_t  base;

    SKY_SEQUENCE_FOREACH(bases, base) {
        base = sky_object_type(base);
        if (!sky_type_issubtype(metatype, base)) {
            if (sky_type_issubtype(base, metatype)) {
                metatype = base;
            }
            else {
                sky_error_raise_string(
                        sky_TypeError,
                        "metaclass conflict: the metaclass of a derived class "
                        "must be a (non-strict) subclass of the metaclasses "
                        "of all its bases");
            }
        }
    } SKY_SEQUENCE_FOREACH_END;

    return metatype;
}


static inline sky_type_t
sky_type_merge_list_contains(sky_type_list_t ** merge_list,
                             ssize_t            merge_list_size,
                             ssize_t *          heads,
                             ssize_t            i)
{
    ssize_t     j, k, len;
    sky_type_t  head;

    head = merge_list[i]->items[heads[i]].type;
    for (j = 0; j < merge_list_size; ++j) {
        len = merge_list[j]->len;
        for (k = heads[j] + 1; k < len; ++k) {
            if (merge_list[j]->items[k].type == head) {
                return NULL;
            }
        }
    }

    return head;
}


static sky_type_list_t *
sky_type_list_create(sky_type_t type, ssize_t len, sky_object_t const *types)
{
    ssize_t         i;
    sky_type_list_t *list;

    list = sky_calloc(1, sizeof(sky_type_list_t) +
                         (sizeof(sky_type_list_item_t) * len));
    list->len = len;
    for (i = 0; i < list->len; ++i) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(list->items[i].type)),
                          types[i],
                          type);
    }

    return list;
}


static void
sky_type_compute_mro(sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(type);

    ssize_t         count, empty, *heads, i, j, merge_list_size;
    sky_type_t      base_type, head;
    sky_object_t    *result;
    sky_type_list_t **merge_list;

    SKY_ASSET_BLOCK_BEGIN {
        merge_list_size = type_data->bases->len + 1;
        merge_list = sky_asset_malloc((merge_list_size *
                                       sizeof(sky_type_list_t *)),
                                      SKY_ASSET_CLEANUP_ALWAYS);
        count = merge_list_size + 1;    /* include type, base_object_type */
        for (i = merge_list_size - 2; i >= 0; --i) {
            base_type = type_data->bases->items[i].type;
            merge_list[i] = sky_type_data(base_type)->mro;
            count += merge_list[i]->len;
        }
        merge_list[merge_list_size - 1] =
                sky_type_list_create(
                        type,
                        type_data->bases->len,
                        (sky_object_t *)type_data->bases->items);
        sky_asset_save(merge_list[merge_list_size - 1],
                       sky_free,
                       SKY_ASSET_CLEANUP_ALWAYS);

        result = sky_asset_malloc(count * sizeof(sky_object_t),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        count = 1;
        result[0] = type;

        heads = sky_asset_calloc(merge_list_size,
                                 sizeof(ssize_t),
                                 SKY_ASSET_CLEANUP_ALWAYS);
        empty = i = 0;
        while (i < merge_list_size) {
            if (heads[i] >= merge_list[i]->len) {
                ++empty;
                ++i;
                continue;
            }

            if (!(head = sky_type_merge_list_contains(merge_list,
                                                      merge_list_size,
                                                      heads,
                                                      i)))
            {
                ++i;
                continue;
            }

            result[count++] = head;
            for (j = 0; j < merge_list_size; ++j) {
                if (heads[j] < merge_list[j]->len &&
                    merge_list[j]->items[heads[j]].type == head)
                {
                    ++heads[j];
                }
            }
            empty = i = 0;
        }

        if (empty != merge_list_size) {
            /* TODO generate a better error message for an incalculable MRO */
            sky_error_raise_string(
                    sky_TypeError,
                    "Cannot create a consistent method resolution order");
        }

        if (!type_data->mro) {
            type_data->mro = sky_type_list_create(type, count, result);
        }
        else {
            /* Keep the original MRO. It was likely created by sky_type_mro(),
             * and so should be accurate. Verify its accuracy.
             */
            sky_error_validate(type_data->mro->len == 2);
            sky_error_validate(type_data->mro->len == count);
            sky_error_validate(type_data->mro->items[0].type == type);
            sky_error_validate(type_data->mro->items[1].type == sky_base_object_type);
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_type_initialize_bases(sky_type_t        type,
                          ssize_t           nbases,
                          const sky_type_t *bases)
{
    sky_type_data_t *type_data = sky_type_data(type);

    size_t          instance_alignment, instance_size;
    ssize_t         i, j;
    sky_type_data_t *base_type_data;

    if (!nbases) {
        sky_error_validate_debug(sky_base_object_type != type);
        sky_error_validate_debug(sky_type_type != type);

        type_data->bases = sky_calloc(1, sizeof(sky_type_list_t) +
                                         sizeof(sky_type_list_item_t));
        type_data->bases->len = 1;
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(type_data->bases->items[0].type)),
                sky_base_object_type,
                type);
        type_data->mro = sky_calloc(1, sizeof(sky_type_list_t) +
                                       (sizeof(sky_type_list_item_t) * 2));
        type_data->mro->len = 2;
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(type_data->mro->items[0].type)),
                type,
                type);
        sky_object_gc_set(
                SKY_AS_OBJECTP(&(type_data->mro->items[1].type)),
                sky_base_object_type,
                type);
    }
    else {
        for (i = 1; i < nbases; ++i) {
            if (sky_type_data(bases[i])->flags & SKY_TYPE_FLAG_FINAL) {
                sky_error_raise_format(sky_TypeError,
                                       "type %#@ is not an acceptable base type",
                                       sky_type_name(bases[i]));
            }
            for (j = 0; j < i; ++j) {
                if (bases[i] == bases[j]) {
                    sky_error_raise_format(sky_TypeError,
                                           "duplicate base class %#@",
                                           sky_type_name(bases[i]));
                }
            }
        }
        type_data->bases =
                sky_type_list_create(type, nbases, (sky_object_t *)bases);
        sky_type_compute_mro(type);
    }

    /* Walk the MRO list, making sure that flags are set correctly. */
    for (i = 1; i < type_data->mro->len; ++i) {
        base_type_data = sky_type_data(type_data->mro->items[i].type);
        if (base_type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
            type_data->flags |= SKY_TYPE_FLAG_HAS_DICT;
        }
    }

    /* Figure out the total size and alignment for object instances of this
     * new type. Start out with alignment the size of a pointer, and only
     * increase it from there if necessary. For built-in types, we compute
     * the instance alignment and size to validate that they've been set
     * correctly in code.
     */
    instance_size = sizeof(sky_object_data_t);
    instance_alignment = sizeof(void *);

    /* The last type in the MRO will always be sky_base_object, and it will
     * always be at offset 0. Skip it and start with the next type in the list,
     * working backwards.
     */
    type_data->data_offsets = sky_calloc(1, sizeof(sky_type_list_t) +
                                            (sizeof(sky_type_list_item_t) *
                                             type_data->mro->len));
    type_data->data_offsets->len = type_data->mro->len;
    for (i = type_data->mro->len - 1; i > 0; --i) {
        base_type_data = sky_type_data(type_data->mro->items[i - 1].type);
        if (!base_type_data->type_size) {
            continue;
        }

        /* Increase the instance alignment if it's less than the base type's
         * required alignment. Adjust the instance size for the type to pad out
         * for alignment if necessary.
         */
        if (base_type_data->type_alignment > type_data->instance_alignment) {
            instance_alignment = base_type_data->type_alignment;
        }
        instance_size = (instance_size +
                         (base_type_data->type_alignment - 1)) /
                        base_type_data->type_alignment *
                        base_type_data->type_alignment;

        type_data->data_offsets->items[i - 1].offset = instance_size;
        instance_size += base_type_data->type_size;
    }

    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        type_data->dict_offset = instance_size;
        instance_size += sizeof(sky_dict_t);
    }

    type_data->instance_alignment = instance_alignment;
    type_data->instance_size = instance_size;
}


typedef struct sky_type_method_item_s {
    sky_hashtable_item_t                base;
    sky_type_method_slot_descriptor_t   method;
} sky_type_method_item_t;

static sky_bool_t
sky_type_method_item_compare(sky_hashtable_item_t *item, void *key)
{
    sky_type_method_item_t              *method_item = (sky_type_method_item_t *)item;
    sky_type_method_slot_descriptor_t   *method = &(method_item->method);

    if (method->name == key || !strcmp(method->name, key)) {
        return SKY_TRUE;
    }

    return SKY_FALSE;
}

static sky_once_t       sky_type_methods_once = SKY_ONCE_INITIALIZER;
static sky_hashtable_t  sky_type_method_slots_hash;

static void
sky_type_methods_once_runner(SKY_UNUSED void *unused)
{
    uintptr_t                           key_hash;
    sky_type_method_item_t              *method_item;
    sky_type_method_slot_descriptor_t   *method;

    sky_hashtable_init(&sky_type_method_slots_hash,
                       sky_type_method_item_compare);
    for (method = sky_type_method_slot_table; method->name; ++method) {
        key_hash = sky_util_hashbytes(method->name, strlen(method->name), 0);
        method_item = sky_calloc(1, sizeof(sky_type_method_item_t));
        method_item->method = *method;
        sky_hashtable_insert(&sky_type_method_slots_hash,
                             &(method_item->base),
                             key_hash,
                             (void *)method->name);
    }
}


static void
sky_type_methods_update(sky_type_t type, sky_string_t key, sky_object_t value)
{
    sky_type_data_t *type_data = sky_type_data(type);

    size_t                  index;
    ssize_t                 cstring_len;
    uintptr_t               key_hash;
    const char              *cstring;
    sky_object_t            object;
    sky_type_method_item_t  *method_item;

    /* check to see if the key starts and ends with "__". If it does, do a
     * special attribute lookup to see if the value should be cached in the
     * type object.
     */
    if ((cstring = sky_string_cstring(key)) != NULL &&
        (cstring_len = sky_string_len(key)) > 4 &&
        cstring[0] == '_' &&
        cstring[1] == '_' &&
        cstring[cstring_len - 2] == '_' &&
        cstring[cstring_len - 1] == '_')
    {
        sky_once_run(&sky_type_methods_once, sky_type_methods_once_runner, NULL);

        key_hash = sky_string_hash(key);
        method_item = (sky_type_method_item_t *)
                      sky_hashtable_lookup(&sky_type_method_slots_hash,
                                           key_hash,
                                           (void *)cstring);
        if (method_item) {
            index = method_item->method.slot;
            if (sky_object_isa(value, sky_function_type) &&
                sky_function_fastcallable(value) &&
                sky_object_isa((object = sky_function_code(value)), sky_native_code_type))
            {
                value = object;
            }
            else if (sky_object_isa(value, sky_staticmethod_type) &&
                     sky_object_isa((object = sky_staticmethod_function(value)), sky_function_type) &&
                     sky_function_fastcallable(object) &&
                     sky_object_isa((object = sky_function_code(object)), sky_native_code_type))
            {
                value = object;
            }
            else if (sky_object_isa(value, sky_classmethod_type) &&
                     sky_object_isa((object = sky_classmethod_function(value)), sky_function_type) &&
                     sky_function_fastcallable(object) &&
                     sky_object_isa((object = sky_function_code(object)), sky_native_code_type))
            {
                value = object;
            }
            sky_object_gc_set(&(type_data->method_slots[index]), value, type);
        }
    }
}


static sky_object_t
sky_type_setattr_remap(sky_string_t key, sky_object_t value)
{
    ssize_t     cstring_len;
    const char  *cstring;

    if (!(cstring = sky_string_cstring(key))) {
        return value;
    }
    cstring_len = sky_string_len(key);

    if (cstring_len == sizeof("__new__") - 1 &&
        !memcmp(cstring, "__new__", sizeof("__new__") - 1) &&
        sky_object_isa(value, sky_function_type))
    {
        return sky_staticmethod_create(value);
    }

    return value;
}


static void
sky_type_subclass_add(sky_type_t type, sky_type_t subclass)
{
    sky_type_data_t *type_data = sky_type_data(type);

    ssize_t                     slot;
    sky_type_subclass_list_t    *list;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_gc_isthreaded() && sky_object_isglobal(type)) {
            sky_type_data_t *type_data = sky_type_data(type);

            sky_spinlock_lock(&(type_data->spinlock));
            sky_asset_save(&(type_data->spinlock),
                           (sky_free_t)sky_spinlock_unlock,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }

        if (!(list = type_data->subclasses)) {
            list = sky_calloc(1, SKY_CACHE_LINE_SIZE);
            slot = 0;
            type_data->subclasses = list;
        }
        else {
            for (;;) {
                for (slot = 0; slot < list->len && list->types[slot]; ++slot);
                if ((size_t)slot < (sky_memsize(list) -
                                    sizeof(sky_type_subclass_list_t)) /
                                   sizeof(sky_type_t))
                {
                    break;
                }
                if (!list->next) {
                    list->next = sky_calloc(1, SKY_CACHE_LINE_SIZE);
                }
                list = list->next;
            }
        }

        list->types[slot] = NULL;
        sky_object_gc_setweakref(SKY_AS_OBJECTP(&(list->types[slot])),
                                 subclass,
                                 type,
                                 NULL,
                                 NULL);
        if (slot >= list->len) {
            ++list->len;
        }
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_type_method_slot(sky_type_t type, sky_type_method_slot_t slot)
{
    sky_type_list_t *mro = sky_type_data(type)->mro;

    ssize_t         i;
    sky_object_t    o;

    sky_error_validate_debug((int)slot >= 0 &&
                             (int)slot < SKY_TYPE_METHOD_SLOT_COUNT);

    o = NULL;
    switch (mro->len) {
        case 1:
            sky_error_validate_debug(sky_base_object_type == type);
            o = mro->items[0].type->type_data.method_slots[slot];
            break;

        case 2:
            if (!(o = mro->items[0].type->type_data.method_slots[slot])) {
                o = mro->items[1].type->type_data.method_slots[slot];
            }
            break;

        default:
            for (i = 0; !o && i < mro->len; ++i) {
                o = mro->items[i].type->type_data.method_slots[slot];
            }
            break;
    }

    return o;
}


void
sky_type_complete_static(sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_dict_t      type_dict;
    sky_object_t    docstr;

    sky_object_gc_setroot(NULL, type, SKY_TRUE);

    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        sky_type_setattr_getset(type,
                                "__dict__",
                                NULL,
                                sky_type_dict_getter,
                                sky_type_dict_setter);
    }

    type_dict = sky_object_dict(type);
    if (!sky_dict_get(type_dict, SKY_STRING_LITERAL("__doc__"), NULL)) {
        if (!type_data->doc) {
            docstr = sky_None;
        }
        else {
            docstr = sky_string_createfrombytes(type_data->doc,
                                                strlen(type_data->doc),
                                                NULL,
                                                NULL);
        }
        sky_dict_setitem(type_dict, SKY_STRING_LITERAL("__doc__"), docstr);
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(type_data->qualname)),
                      sky_string_createfrombytes(type_data->name,
                                                 strlen(type_data->name),
                                                 NULL,
                                                 NULL),
                      type);

    if (type_data->bases->len > 0) {
        sky_type_subclass_add(type_data->bases->items[0].type, type);
    }
}


static void
sky_type_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_type_data_t *type_data = data;

    sky_spinlock_init(&(type_data->spinlock));
}


static void
sky_type_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_type_data_t *type_data = data;

    sky_type_subclass_list_t    *subclasses;

    /* The name field for builtin-in types is normally a source-level string
     * literal, so don't try to free it!
     */
    if (!(type_data->flags & SKY_TYPE_FLAG_STATIC)) {
        sky_free(type_data->name);
        type_data->name = NULL;
        sky_free(type_data->doc);
        type_data->doc = NULL;
    }
    type_data->qualname = NULL;

    while ((subclasses = type_data->subclasses) != NULL) {
        type_data->subclasses = subclasses->next;
        sky_free(subclasses);
    }

    sky_free(type_data->method_slots);
    type_data->method_slots = NULL;
}


static void
sky_type_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_type_data_t *type_data = data;

    ssize_t                     i;
    sky_object_t                object;
    sky_type_subclass_list_t    *list;

    if (type_data->bases) {
        for (i = 0; i < type_data->bases->len; ++i) {
            sky_object_visit(type_data->bases->items[i].type, visit_data);
        }
    }
    if (type_data->mro) {
        for (i = 0; i < type_data->mro->len; ++i) {
            sky_object_visit(type_data->mro->items[i].type, visit_data);
        }
    }

    if (SKY_OBJECT_VISIT_REASON_GC_MARK != visit_data->reason) {
        for (list = type_data->subclasses; list; list = list->next) {
            for (i = 0; i < list->len; ++i) {
                if ((object = list->types[i]) != NULL) {
                    sky_object_visit(object, visit_data);
                }
            }
        }
    }

    if (type_data->method_slots) {
        for (i = 0; i < SKY_TYPE_METHOD_SLOT_COUNT; ++i) {
            sky_object_visit(type_data->method_slots[i], visit_data);
        }
    }

    sky_object_visit(type_data->qualname, visit_data);
}


static sky_object_t
sky_type_abstractmethods_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_object_t    value = NULL;

    if (sky_type_type != self) {
        value = sky_dict_get(sky_object_dict(self),
                             SKY_STRING_LITERAL("__abstractmethods__"),
                             NULL);
    }
    if (!value) {
        sky_error_raise_string(sky_AttributeError, "__abstractmethods__");
    }

    return value;
}


static void
sky_type_abstractmethods_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    if (!value) {
        if (!sky_dict_delete(sky_object_dict(self),
                             SKY_STRING_LITERAL("__abstractmethods__")))
        {
            sky_error_raise_object(sky_AttributeError,
                                   SKY_STRING_LITERAL("__abstractmethods__"));
        }
        sky_type_data(self)->flags &= ~SKY_TYPE_FLAG_ABSTRACT;
    }
    else {
        sky_dict_setitem(sky_object_dict(self),
                         SKY_STRING_LITERAL("__abstractmethods__"),
                         value);
        if (sky_object_bool(value)) {
            sky_type_data(self)->flags |= SKY_TYPE_FLAG_ABSTRACT;
        }
        else {
            sky_type_data(self)->flags &= ~SKY_TYPE_FLAG_ABSTRACT;
        }
    }
}


static sky_object_t
sky_type_base_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    if (!type_data->bases || !type_data->bases->len) {
        return sky_None;
    }
    return type_data->bases->items[0].type;
}


static sky_object_t
sky_type_bases_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    if (!type_data->bases || !type_data->bases->len) {
        return sky_tuple_empty;
    }
    return sky_tuple_createfromarray(type_data->bases->len,
                                     (sky_object_t *)type_data->bases->items);
}


typedef sky_object_t
        (*sky_type_new_t)(sky_type_t, sky_tuple_t args, sky_dict_t kws);
typedef void
        (*sky_type_init_t)(sky_object_t, sky_tuple_t args, sky_dict_t kws);

sky_object_t
sky_type_call(sky_type_t type, sky_tuple_t args, sky_dict_t kws)
{
    sky_tuple_t     new_args;
    sky_object_t    method, object;
    sky_type_new_t  new_cfunction;
    sky_type_init_t init_cfunction;

    /* Call __new__(type, args, kws) */
    method = SKY_TYPE_METHOD_SLOT(type, NEW);
    if (sky_object_isnull(method)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot create %#@ instances",
                               sky_type_name(type));
    }
    if (sky_object_isa(method, sky_native_code_type)) {
        new_cfunction = (sky_type_new_t)sky_native_code_function(method);
        SKY_OBJECT_CALL_NATIVE(object, new_cfunction, (type, args, kws));
    }
    else {
        method = sky_descriptor_get(method, NULL, type);
        new_args = (args ? sky_tuple_insert(args, 0, type)
                         : sky_tuple_pack(1, type));
        object = sky_object_call(method, new_args, kws);
    }

    /* If the result from __new__() is a sub-type of type, call __init__(). */
    if (sky_type_issubtype(sky_object_type(object), type)) {
        method = SKY_OBJECT_METHOD_SLOT(object, INIT);
        if (sky_object_isa(method, sky_native_code_type)) {
            init_cfunction = (sky_type_init_t)sky_native_code_function(method);
            SKY_OBJECT_CALL_NATIVE_VOID(init_cfunction, (object, args, kws));
        }
        else {
            method = sky_descriptor_get(method, object, sky_object_type(object));
            sky_object_call(method, args, kws);
        }
    }

    return object;
}


sky_type_t
sky_type_create(sky_string_t    qualname,
                sky_object_t    bases,
                sky_object_t    metaclass,
                sky_code_t      code,
                sky_tuple_t     closure,
                sky_dict_t      globals,
                sky_dict_t      kws)
{
    ssize_t         dot;
    sky_cell_t      cell;
    sky_type_t      type;
    sky_object_t    locals, object, prepare;
    sky_string_t    name;
    sky_code_data_t *code_data;

    if (!metaclass) {
        if (!sky_object_bool(bases)) {
            metaclass = sky_type_type;
        }
        else {
            metaclass = sky_object_type(sky_sequence_get(bases, 0));
        }
    }
    if (sky_object_isa(metaclass, sky_type_type)) {
        metaclass = sky_type_metatype(metaclass, bases);
    }

    dot = sky_string_rfind(qualname, SKY_STRING_LITERAL("."), 0, SSIZE_MAX);
    if (-1 == dot) {
        name = qualname;
    }
    else {
        name = sky_string_slice(qualname, dot + 1, SSIZE_MAX, 1);
    }

    if (!(prepare = sky_object_getattr(metaclass,
                                       SKY_STRING_LITERAL("__prepare__"),
                                       NULL)))
    {
        locals = sky_dict_create();
    }
    else {
        locals = sky_object_call(prepare,
                                 sky_tuple_pack(2, name, bases),
                                 kws);
    }

    if (qualname != name) {
        sky_object_setitem(locals,
                           SKY_STRING_LITERAL("__qualname__"),
                           qualname);
    }
    if (!code) {
        cell = NULL;
        sky_object_setitem(locals, SKY_STRING_LITERAL("__doc__"), sky_None);
    }
    else {
        code_data = sky_code_data(code);
        if (!sky_tuple_len(code_data->co_consts)) {
            sky_object_setitem(locals, SKY_STRING_LITERAL("__doc__"), sky_None);
        }
        else {
            object = sky_tuple_get(code_data->co_consts, 0);
            sky_object_setitem(locals, SKY_STRING_LITERAL("__doc__"), object);
        }
        cell = sky_code_execute(code, globals, locals, closure, 0, NULL);
    }

    type = sky_object_call(metaclass,
                           sky_tuple_pack(3, name, bases, locals),
                           kws);
    if (sky_object_isa(cell, sky_cell_type)) {
        sky_cell_setcontents(cell, type);
    }
    return type;
}


sky_type_t
sky_type_createbuiltin(sky_string_t         qualname,
                       const char *         doc,
                       uint32_t             flags,
                       size_t               data_size,
                       size_t               data_alignment,
                       sky_tuple_t          bases,
                       sky_type_template_t *template)
{
    ssize_t         dot;
    sky_dict_t      globals;
    sky_type_t      type;
    sky_string_t    module_name, name;
    sky_type_data_t *type_data;

    sky_error_validate(!(flags & ~(SKY_TYPE_CREATE_FLAG_HAS_DICT |
                                   SKY_TYPE_CREATE_FLAG_FINAL)));

    dot = sky_string_rfind(qualname, SKY_STRING_LITERAL("."), 0, SSIZE_MAX);
    if (-1 == dot) {
        name = qualname;
    }
    else {
        name = sky_string_slice(qualname, dot + 1, SSIZE_MAX, 1);
    }

    if (!sky_object_bool(bases)) {
        bases = sky_tuple_pack(1, sky_base_object_type);
    }

    type = sky_object_allocate(sky_type_type);
    type_data = sky_type_data(type);
    sky_type_setname(type, name);
    type_data->flags = SKY_TYPE_FLAG_BUILTIN | flags;
    type_data->type_size = data_size;
    type_data->type_alignment = (data_alignment ? data_alignment : 16);
    sky_object_gc_set(SKY_AS_OBJECTP(&(type_data->qualname)),
                      qualname,
                      type);

#define set_from_template(_field)                                       \
    do {                                                                \
        if (template->sizeof_struct >=                                  \
                offsetof(sky_type_template_t, _field) + sizeof(void *)) \
        {                                                               \
            type_data->_field = template->_field;                       \
        }                                                               \
    } while (0)

    if (template) {
        set_from_template(allocate);
        set_from_template(deallocate);
        set_from_template(initialize);
        set_from_template(finalize);
        set_from_template(visit);
        set_from_template(iterate);
        set_from_template(iterate_cleanup);
        set_from_template(buffer_acquire);
        set_from_template(buffer_release);
    }

#undef set_from_template

    if (!type_data->allocate) {
        type_data->allocate = sky_type_allocate_calloc;
    }
    if (!type_data->deallocate) {
        type_data->deallocate = sky_free;
    }

    if (doc) {
        type_data->doc = sky_strdup(doc);
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t                    objects[16];
        sky_sequence_iterate_state_t    state;

        memset(&state, 0, sizeof(state));
        state.sequence = bases;
        sky_sequence_iterate(bases, &state, objects, 16);
        sky_asset_save(&state,
                       (sky_free_t)sky_sequence_iterate_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_type_initialize_bases(type,
                                  state.nobjects,
                                  (const sky_type_t *)state.objects);
#if !defined(NDEBUG)
        sky_sequence_iterate(bases, &state, objects, 16);
        sky_error_validate(!state.objects && !state.nobjects);
#endif
    } SKY_ASSET_BLOCK_END;

    type_data->method_slots = sky_calloc(SKY_TYPE_METHOD_SLOT_COUNT,
                                         sizeof(sky_object_t));

    if ((globals = sky_interpreter_globals()) != NULL &&
        (module_name = sky_dict_get(globals,
                                    SKY_STRING_LITERAL("__name__"),
                                    NULL)) != NULL)
    {
        sky_type_setattr_builtin(type, "__module__", module_name);
    }

    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        sky_type_setattr_getset(type,
                                "__dict__",
                                NULL,
                                sky_type_dict_getter,
                                sky_type_dict_setter);
    }

    sky_type_subclass_add(type_data->bases->items[0].type, type);

    return type;
}


void
sky_type_merge_class_dict(sky_type_t type, sky_dict_t dict)
{
    sky_object_t    base, bases, iter, type_dict;

    type_dict = sky_object_getattr(type, SKY_STRING_LITERAL("__dict__"), NULL);
    if (type_dict) {
        sky_dict_update(dict, sky_tuple_pack(1, type_dict), NULL);
    }

    bases = sky_object_getattr(type, SKY_STRING_LITERAL("__bases__"), NULL);
    if (bases && sky_object_isiterable(bases)) {
        if (sky_sequence_isiterable(bases)) {
            SKY_SEQUENCE_FOREACH(bases, base) {
                sky_type_merge_class_dict(base, dict);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else {
            iter = sky_object_iter(bases);
            while ((base = sky_object_next(iter, NULL)) != NULL) {
                sky_type_merge_class_dict(base, dict);
            }
        }
    }
}


static sky_object_t
sky_type_dict_getter(sky_object_t self, sky_type_t type)
{
    if (sky_type_type == type) {
        return sky_mappingproxy_create(sky_object_dict(self));
    }
    return sky_object_dict(self);
}


static void
sky_type_dict_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_object_setdict(self, value);
}


sky_object_t
sky_type_dir(sky_type_t type)
{
    sky_dict_t  dict;

    dict = sky_dict_create();
    sky_type_merge_class_dict(type, dict);

    return sky_dict_keys(dict);
}


static sky_object_t
sky_type_doc_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    sky_object_t    doc;

    if (type_data->doc) {
        return sky_string_createfrombytes(type_data->doc,
                                          strlen(type_data->doc),
                                          NULL,
                                          NULL);
    }

    SKY_ERROR_TRY {
        doc = sky_object_getitem(sky_object_dict(self),
                                 SKY_STRING_LITERAL("__doc__"));
    } SKY_ERROR_EXCEPT(sky_KeyError) {
        doc = sky_None;
    } SKY_ERROR_TRY_END;

    return sky_descriptor_get(doc, NULL, self);
}


static void
sky_type_doc_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    char    *cstring;

    if (type_data->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_format(
                sky_TypeError,
                "can't set attributes of built-in type %#@",
                sky_type_name(self));
    }

    if (sky_object_isnull(value)) {
        sky_free(type_data->doc);
        type_data->doc = NULL;
    }
    else {
        cstring = sky_codec_encodetocstring(value, 0);
        sky_free(type_data->doc);
        type_data->doc = cstring;
    }
}


static sky_object_t
sky_type_flags_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    uintmax_t   flags = 0;

    /* bits 0..8 are not used */
    if (!(type_data->flags & SKY_TYPE_FLAG_BUILTIN)) {
        flags |= (1L << 9);             /* Py_TPFLAGS_HEAPTYPE */
    }
    if (!(type_data->flags & SKY_TYPE_FLAG_FINAL)) {
        flags |= (1L << 10);            /* Py_TPFLAGS_BASETYPE */
    }
                                        /* (bit 11) not used */
    flags |= (1L << 12);                /* Py_TPFLAGS_READY */
                                        /* Py_TPFLAGS_READYING */
    flags |= (1L << 14);                /* Py_TPFLAGS_HAVE_GC */
                                        /* (bit 15) used by stackless */
                                        /* (bit 16) used by stackless */
                                        /* (bit 17) not used */
                                        /* Py_TPFLAGS_HAVE_VERSION_TAG */
                                        /* Py_TPFLAGS_VALID_VERSION_TAG */
    if (type_data->flags & SKY_TYPE_FLAG_ABSTRACT) {
        flags |= (1L << 20);            /* Py_TPFLAGS_ABSTRACT */
    }
                                        /* (bit 21) not used */
                                        /* (bit 22) not used */

                                        /* (bit 23) PY_TPFLAGS_INT_SUBCLASS */
                                        /*          not used - int type and */
                                        /*          long type are the same  */
    if (sky_integer_type != self && sky_object_isa(self, sky_integer_type)) {
        flags |= (1L << 24);            /* Py_TPFLAGS_LONG_SUBCLASS */
    }
    if (sky_list_type != self && sky_object_isa(self, sky_list_type)) {
        flags |= (1L << 25);            /* Py_TPFLAGS_LIST_SUBCLASS */
    }
    if (sky_tuple_type != self && sky_object_isa(self, sky_tuple_type)) {
        flags |= (1L << 26);            /* Py_TPFLAGS_TUPLE_SUBCLASS */
    }
    if (sky_bytes_type != self && sky_object_isa(self, sky_bytes_type)) {
        flags |= (1L << 27);            /* Py_TPFLAGS_BYTES_SUBCLASS */
    }
    if (sky_string_type != self && sky_object_isa(self, sky_string_type)) {
        flags |= (1L << 28);            /* Py_TPFLAGS_UNICODE_SUBCLASS */
    }
    if (sky_dict_type != self && sky_object_isa(self, sky_dict_type)) {
        flags |= (1L << 29);            /* Py_TPFLAGS_DICT_SUBCLASS */
    }
    if (sky_BaseException && sky_object_isa(self, sky_BaseException)) {
        flags |= (1L << 30);            /* Py_TPFLAGS_BASE_EXC_SUBCLASS */
    }
    if (sky_type_type != self && sky_object_isa(self, sky_type_type)) {
        flags |= (1L << 31);            /* Py_TPFLAGS_TYPE_SUBCLASS */
    }

    return sky_integer_createfromunsigned(flags);
}


sky_object_t
sky_type_getattribute(sky_type_t type, sky_string_t name)
    /* This is the same as sky_base_object_getattribute(), except that it uses
     * sky_type_lookup(type) for the SKY_TYPE_FLAG_HAS_DICT case.
     */
{
    sky_object_t    descriptor, object;

    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "attribute name must be string, not %#@",
                               sky_type_name(sky_object_type(name)));
    }

    descriptor = sky_type_lookup(sky_object_type(type), name);
    if (descriptor && sky_descriptor_isdata(descriptor)) {
        return sky_descriptor_get(descriptor, type, sky_object_type(type));
    }

    if ((object = sky_type_lookup(type, name)) != NULL) {
        return sky_descriptor_get(object, NULL, type);
    }

    if (descriptor) {
        return sky_descriptor_get(descriptor, type, sky_object_type(type));
    }

    sky_error_raise_format(sky_AttributeError,
                           "type object %#@ has no attribute %#@",
                           sky_type_name(type), name);
}


sky_bool_t
sky_type_issubtype(sky_type_t type, sky_type_t subtype)
{
    sky_type_data_t *type_data = sky_type_data(type);

    ssize_t i;

    for (i = 0; i < type_data->mro->len; ++i) {
        if (type_data->mro->items[i].type == subtype) {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_object_t
sky_type_lookup(sky_type_t type, sky_string_t name)
{
    sky_type_data_t *type_data = sky_type_data(type);

    ssize_t         i;
    sky_dict_t      dict;
    sky_object_t    object;

    for (i = 0; i < type_data->mro->len; ++i) {
        dict = sky_object_dict(type_data->mro->items[i].type);
        if ((object = sky_dict_get(dict, name, NULL)) != NULL) {
            return object;
        }
    }

    return NULL;
}


sky_string_t
sky_type_module(sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_object_t    default_value;
    sky_string_t    module;

    if (!(type_data->flags & SKY_TYPE_FLAG_BUILTIN)) {
        default_value = sky_NotSpecified;
    }
    else if (type == sky_type_type) {
        return SKY_STRING_LITERAL("builtins");
    }
    else {
        default_value = NULL;
    }

    if (!(module = sky_dict_get(sky_object_dict(type),
                                SKY_STRING_LITERAL("__module__"),
                                default_value)))
    {
        return SKY_STRING_LITERAL("builtins");
    }

    return module;
}


static sky_object_t
sky_type_module_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_type_module(self);
}


static void
sky_type_module_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    if (type_data->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_format(
                sky_TypeError,
                "can't set attributes of built-in type %#@",
                sky_type_name(self));
    }

    sky_dict_setitem(sky_object_dict(self),
                     SKY_STRING_LITERAL("__module__"),
                     value);
}


static sky_tuple_t
sky_type_mro(sky_type_t self)
{
    sky_type_data_t *type_data = sky_type_data(self);

    if (!type_data->mro || !type_data->mro->len) {
        return sky_tuple_pack(1, sky_base_object_type);
    }
    return sky_tuple_createfromarray(type_data->mro->len,
                                     (sky_object_t *)type_data->mro->items);
}


static sky_object_t
sky_type_mro_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_type_mro(self);
}


sky_string_t
sky_type_name(sky_type_t type)
{
    const char  *name = sky_type_data(type)->name;

    return sky_string_createfrombytes(name, strlen(name), NULL, NULL);
}


static sky_object_t
sky_type_name_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_type_name(self);
}


static void
sky_type_name_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_type_setname(self, value);
}


static sky_parameter_list_t sky_type_new_parameters = NULL;

sky_object_t
sky_type_new(sky_type_t cls, sky_tuple_t args, sky_dict_t kws)
{
    void            **values;
    size_t          offset;
    uint32_t        base_flags, flags;
    const char      *cstring;
    sky_bool_t      subtype;
    sky_dict_t      dict, globals, *type_dict;
    sky_list_t      list;
    sky_type_t      base, metatype, type;
    sky_tuple_t     bases;
    sky_object_t    key, slot, slots, value;
    sky_string_t    module_name, name;
    sky_type_data_t *base_type_data, *type_data;

    /* type(object) -> object.__class__ */
    if (sky_type_type == cls &&
        sky_object_isa(args, sky_tuple_type) && sky_tuple_len(args) == 1 &&
        !sky_object_bool(kws))
    {
        return sky_object_type(sky_tuple_get(args, 0));
    }

    /* type(name, bases, dict) */
    values = sky_parameter_list_apply(sky_type_new_parameters,
                                      args, kws,
                                      SKY_STRING_LITERAL("type"));
    name = *(sky_string_t *)values[0];
    bases = *(sky_tuple_t *)values[1];
    dict = *(sky_dict_t *)values[2];
    sky_free(values);

    /* Make sure that this is the proper type to handle the type instantiation
     * for the supplied bases. If it isn't, call the right one.
     */
    metatype = sky_type_metatype(cls, bases);
    if (metatype != cls) {
        return sky_object_call(metatype, args, kws);
    }

    subtype = SKY_FALSE;
    if (!sky_object_bool(bases)) {
        bases = sky_tuple_pack(1, sky_base_object_type);
        base_flags = sky_base_object_type->type_data.flags;
    }
    else {
        /* Make sure that bases is a tuple and that it contains only type
         * objects, and that they are acceptable bases (i.e., not final).
         */
        base_flags = 0;
        bases = sky_tuple_create(bases);
        SKY_SEQUENCE_FOREACH(bases, base) {
            if (!sky_object_isa(base, sky_type_type)) {
                sky_error_raise_string(sky_TypeError, "bases must be types");
            }
            else if (sky_type_issubtype(base, sky_type_type)) {
                subtype = SKY_TRUE;
            }
            base_type_data = sky_type_data(base);
            base_flags |= base_type_data->flags;
            if (base_type_data->flags & SKY_TYPE_FLAG_FINAL) {
                sky_error_raise_format(
                        sky_TypeError,
                        "type %#@ is not an acceptable base type",
                        sky_type_name(base));
            }
        } SKY_SEQUENCE_FOREACH_END;
    }

    if (sky_object_isnull(dict)) {
        dict = sky_dict_create();
    }
    else {
        dict = sky_dict_copy(dict);
    }

    flags = 0;
    base = sky_tuple_get(bases, 0);
    base_type_data = sky_type_data(base);

    if (!(slots = sky_dict_get(dict, SKY_STRING_LITERAL("__slots__"), NULL))) {
        flags |= SKY_TYPE_FLAG_HAS_DICT;
    }
    else {
        if (sky_object_isa(slots, sky_string_type)) {
            slots = sky_tuple_pack(1, slots);
        }
        else {
            slots = sky_tuple_create(slots);
        }
        SKY_SEQUENCE_FOREACH(slots, slot) {
            if (!sky_object_isa(slot, sky_string_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "__slots__ items must be strings, not %#@",
                        sky_type_name(sky_object_type(slot)));
            }
            if (!sky_string_isidentifier(slot)) {
                sky_error_raise_string(sky_TypeError,
                                       "__slots__ must be identifiers");
            }
            if ((cstring = sky_string_cstring(slot)) != NULL) {
                if (!strcmp(cstring, "__dict__")) {
                    if ((base_type_data->flags & SKY_TYPE_FLAG_HAS_DICT) ||
                        (flags & SKY_TYPE_FLAG_HAS_DICT))
                    {
                        sky_error_raise_string(
                                sky_TypeError,
                                "__dict__ slot disallowed: we already got one");
                    }
                    flags |= SKY_TYPE_FLAG_HAS_DICT;
                }
                /* CPython restricts __weakref__ too, but Skython's weakref
                 * implementation is completely different, so we'll just ignore
                 * it for now.
                 */
            }
        } SKY_SEQUENCE_FOREACH_END;

        /* Build a new, clean list of slots. Mangle the names and sort it. */
        list = sky_list_createwithcapacity(sky_tuple_len(slots));
        SKY_SEQUENCE_FOREACH(slots, slot) {
            if (!(cstring = sky_string_cstring(slot)) ||
                (strcmp(cstring, "__dict__") &&
                 strcmp(cstring, "__weakref__")))
            {
                slot = sky_python_compiler_mangle_name(name, slot);
                if (sky_dict_get(dict, slot, NULL)) {
                    sky_error_raise_format(
                            sky_ValueError,
                            "%#@ in __slots__ conflicts with class variable",
                            slot);
                }
                sky_list_append(list, slot);
            }
        } SKY_SEQUENCE_FOREACH_END;

        sky_list_sort(list, NULL, SKY_FALSE);
        slots = sky_tuple_create(list);
    }

    type = sky_object_allocate(metatype);
    type_data = sky_type_data(type);
    type_data->flags = flags;
    type_data->allocate = sky_type_allocate_calloc;
    type_data->deallocate = (subtype ? sky_type_instance_deallocate
                                     : sky_free);

    if (slots) {
        type_data->type_size = sky_tuple_len(slots) * sizeof(sky_object_t);
        type_data->type_alignment = 16;
    }

    sky_type_setname(type, name);

    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t                    objects[16];
        sky_sequence_iterate_state_t    state;

        memset(&state, 0, sizeof(state));
        state.sequence = bases;
        sky_sequence_iterate(bases, &state, objects, 16);
        sky_asset_save(&state,
                       (sky_free_t)sky_sequence_iterate_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_type_initialize_bases(type,
                                  state.nobjects,
                                  (const sky_type_t *)state.objects);
#if !defined(NDEBUG)
        sky_sequence_iterate(bases, &state, objects, 16);
        sky_error_validate(!state.objects && !state.nobjects);
#endif
    } SKY_ASSET_BLOCK_END;

    type_data->method_slots = sky_calloc(SKY_TYPE_METHOD_SLOT_COUNT,
                                         sizeof(sky_object_t));

    type_dict = (sky_dict_t *)
                ((char *)type + sky_type_data(sky_type_type)->dict_offset);
    sky_object_gc_set(SKY_AS_OBJECTP(type_dict), dict, type);

    /* Set __module__ from globals()['__name__'] */
    if (!sky_dict_get(dict, SKY_STRING_LITERAL("__module__"), NULL) &&
        (globals = sky_interpreter_globals()) != NULL &&
        (module_name = sky_dict_get(globals,
                                    SKY_STRING_LITERAL("__name__"),
                                    NULL)) != NULL)
    {
        sky_dict_setitem(dict, SKY_STRING_LITERAL("__module__"), module_name);
    }
    if ((type_data->flags & SKY_TYPE_FLAG_HAS_DICT) &&
        !(base_flags & SKY_TYPE_FLAG_HAS_DICT) &&
        !sky_dict_get(dict, SKY_STRING_LITERAL("__dict__"), NULL))
    {
        sky_dict_setitem(
                dict,
                SKY_STRING_LITERAL("__dict__"),
                sky_getset_create(type,
                                  "__dict__",
                                  NULL,
                                  sky_type_dict_getter,
                                  sky_type_dict_setter));
    }

    SKY_SEQUENCE_FOREACH(dict, key) {
        value = sky_type_setattr_remap(key, sky_dict_getitem(dict, key));
        sky_type_methods_update(type, key, value);

        if ((cstring = sky_string_cstring(key)) != NULL) {
            if (!strcmp(cstring, "__qualname__")) {
                sky_object_gc_set(SKY_AS_OBJECTP(&(type_data->qualname)),
                                  value,
                                  type);
            }
        }
    } SKY_SEQUENCE_FOREACH_END;

    if (!type_data->qualname) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(type_data->qualname)),
                          name,
                          type);
    }

    /* Create descriptors in the type object for each slot in __slots__. */
    if (slots) {
        offset = 0;
        SKY_SEQUENCE_FOREACH(slots, slot) {
            sky_dict_setitem(
                    dict,
                    slot,
                    sky_member_create(type,
                                      slot,
                                      NULL,
                                      offset,
                                      SKY_DATA_TYPE_OBJECT, 
                                      (SKY_MEMBER_FLAG_NULL_EXCEPTION |
                                       SKY_MEMBER_FLAG_DELETEABLE |
                                       SKY_MEMBER_FLAG_ALLOW_NONE)));
            offset += sizeof(sky_object_t);
        } SKY_SEQUENCE_FOREACH_END;
    }

    sky_type_subclass_add(type_data->bases->items[0].type, type);

    return type;
}


sky_dict_t
sky_type_prepare(SKY_UNUSED sky_type_t     metaclass,
                 SKY_UNUSED sky_string_t   name,
                 SKY_UNUSED sky_tuple_t    bases,
                 SKY_UNUSED sky_dict_t     kws)
{
    return sky_dict_create();
}


sky_string_t
sky_type_qualname(sky_type_t type)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_string_t    qualname;

    if (!type_data->qualname) {
        sky_object_gc_set(SKY_AS_OBJECTP(&qualname),
                          sky_string_createfrombytes(type_data->name,
                                                     strlen(type_data->name),
                                                     NULL,
                                                     NULL),
                          type);
        type_data->qualname = qualname;
    }

    return type_data->qualname;
}


static sky_object_t
sky_type_qualname_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_type_qualname(self);
}


static void
sky_type_qualname_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_type_data_t *type_data = sky_type_data(self);

    if (!sky_object_isa(value, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only assign string to %@.__qualname__, not %#@",
                sky_type_name(self),
                sky_type_name(sky_object_type(value)));
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(type_data->qualname)), value, self);
}


sky_string_t
sky_type_repr(sky_type_t self)
{
    sky_string_t    module;

    module = sky_type_module(self);
    if (sky_string_eq(module, SKY_STRING_LITERAL("builtins")) == sky_True) {
        return sky_string_createfromformat("<class %#@>",
                                           sky_type_name(self));
    }

    return sky_string_createfromformat("<class '%@.%@'>",
                                       module,
                                       sky_type_qualname(self));
}


void
sky_type_delattr(sky_type_t type, sky_string_t key)
{
    if (!sky_object_isa(key, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "attribute name must be string, not %#@",
                sky_type_name(sky_object_type(key)));
    }
    if (sky_type_data(type)->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_format(
                sky_TypeError,
                "can't set attributes of built-in/extension type %#@",
                sky_type_name(type));
    }

    sky_base_object_delattr(type, key);
    sky_type_methods_update(type, key, NULL);
}


void
sky_type_setattr(sky_type_t type, sky_string_t key, sky_object_t value)
{
    if (!sky_object_isa(key, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "attribute name must be string, not %#@",
                sky_type_name(sky_object_type(key)));
    }
    if (sky_type_data(type)->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_format(
                sky_TypeError,
                "can't set attributes of built-in/extension type %#@",
                sky_type_name(type));
    }

    value = sky_type_setattr_remap(key, value);
    sky_base_object_setattr(type, key, value);
    sky_type_methods_update(type, key, value);
}


void
sky_type_setattr_builtin(sky_type_t     type,
                         const char *   key,
                         sky_object_t   value)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_string_t    string;

    sky_error_validate(type_data->flags & SKY_TYPE_FLAG_BUILTIN);
    string = sky_string_createfromliteral(key, strlen(key));

    value = sky_type_setattr_remap(string, value);
    sky_dict_setitem(sky_object_dict(type), string, value);
    sky_type_methods_update(type, string, value);
}


void
sky_type_setgetsets(sky_type_t type, ...)
{
    va_list             ap;
    const char          *doc, *name;
    sky_getset_t        getset;
    sky_getset_get_t    get;
    sky_getset_set_t    set;

    va_start(ap, type);
    while ((name = va_arg(ap, const char *)) != NULL) {
        doc = va_arg(ap, const char *);
        get = va_arg(ap, sky_getset_get_t);
        set = va_arg(ap, sky_getset_set_t);
        getset = sky_getset_create(type, name, doc, get, set);
        sky_type_setattr_builtin(type, name, getset);
    }
    va_end(ap);
}


void
sky_type_setattr_member(sky_type_t      type,
                        const char *    name,
                        const char *    doc,
                        size_t          offset,
                        sky_data_type_t data_type,
                        unsigned int    flags,
                        ...)
{
    va_list         ap;
    sky_string_t    doc_string, name_string;

    va_start(ap, flags);
    name_string = sky_string_createfrombytes(name, strlen(name), NULL, NULL);
    if (doc) {
        doc_string = sky_string_createfrombytes(doc, strlen(doc), NULL, NULL);
    }
    sky_type_setattr_builtin(
            type,
            name,
            sky_member_create(type,
                              name_string,
                              (doc ? doc_string : NULL),
                              offset,
                              data_type,
                              flags,
                              va_arg(ap, void *)));
    va_end(ap);
}


void
sky_type_setmembers(sky_type_t type, ...)
{
    size_t          offset;
    va_list         ap;
    const char      *doc, *name;
    sky_object_t    data_type_extra;
    sky_string_t    doc_string, name_string;
    unsigned int    flags;
    sky_data_type_t data_type;

    va_start(ap, type);
    while ((name = va_arg(ap, const char *)) != NULL) {
        name_string = sky_string_createfrombytes(name, strlen(name), NULL, NULL);
        if ((doc = va_arg(ap, const char *)) != NULL) {
            doc_string = sky_string_createfrombytes(doc,
                                                    strlen(doc),
                                                    NULL,
                                                    NULL);
        }
        offset = va_arg(ap, size_t);
        data_type = va_arg(ap, sky_data_type_t);
        flags = va_arg(ap, unsigned int);
        if (sky_data_type_isextraneeded(data_type)) {
            data_type_extra = va_arg(ap, sky_object_t);
        }
        else {
            data_type_extra = NULL;
        }
        sky_type_setattr_builtin(
                type,
                name,
                sky_member_create(type,
                                  name_string,
                                  (doc ? doc_string : NULL),
                                  offset,
                                  data_type,
                                  flags,
                                  data_type_extra));
    }
    va_end(ap);
}


static sky_parameter_list_t
            sky_type_method_slot_parameter_lists[SKY_TYPE_METHOD_SLOT_COUNT];

static sky_parameter_list_t
sky_type_method_slot_parameter_list(sky_type_method_slot_t slot)
{
    sky_parameter_list_t    parameters;

    sky_error_validate_debug(slot >= 0);
    sky_error_validate_debug(slot < SKY_TYPE_METHOD_SLOT_COUNT);
    parameters = sky_type_method_slot_parameter_lists[slot];

    if (!parameters) {
        switch (slot) {
            case SKY_TYPE_METHOD_SLOT_NEW:
                parameters = sky_parameter_list_createwithparameters(
                        "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                        "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                        "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_INIT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                        "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DEL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_REPR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_STR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_BYTES:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_FORMAT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "format_spec", SKY_DATA_TYPE_OBJECT_STRING, sky_string_empty,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_LT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_LE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_EQ:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_NE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_HASH:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_BOOL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SIZEOF:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GETATTR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "name", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GETATTRIBUTE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "name", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SETATTR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "name", SKY_DATA_TYPE_OBJECT, NULL,
                        "value", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DELATTR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "name", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DIR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GET:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "instance", SKY_DATA_TYPE_OBJECT, NULL,
                        "owner", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SET:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "instance", SKY_DATA_TYPE_OBJECT, NULL,
                        "value", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DELETE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "instance", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_CALL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                        "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_INSTANCECHECK:
                parameters = sky_parameter_list_createwithparameters(
                        "cls", SKY_DATA_TYPE_OBJECT, NULL,
                        "instance", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SUBCLASSCHECK:
                parameters = sky_parameter_list_createwithparameters(
                        "cls", SKY_DATA_TYPE_OBJECT, NULL,
                        "subclass", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SUBCLASSHOOK:
                parameters = sky_parameter_list_createwithparameters(
                        "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                        "subclass", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ENTER:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_EXIT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "exc_type", SKY_DATA_TYPE_OBJECT_TYPE | SKY_DATA_TYPE_OBJECT_OR_NONE, NULL,
                        "exc_value", SKY_DATA_TYPE_OBJECT, NULL,
                        "traceback", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_LEN:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_LENGTH_HINT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GETITEM:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "key", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SETITEM:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "key", SKY_DATA_TYPE_OBJECT, NULL,
                        "value", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DELITEM:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "key", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ITER:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_NEXT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_REVERSED:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_CONTAINS:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "item", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ADD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SUB:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_MUL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_TRUEDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_FLOORDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_MOD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_DIVMOD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_POW:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        "modulo", SKY_DATA_TYPE_OBJECT, sky_None,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_LSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_AND:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_XOR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_OR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RADD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RSUB:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RMUL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RTRUEDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RFLOORDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RMOD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RDIVMOD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RPOW:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RLSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RRSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RAND:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_RXOR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ROR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IADD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ISUB:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IMUL:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ITRUEDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IFLOORDIV:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IMOD:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IPOW:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ILSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IRSHIFT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IAND:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IXOR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_IOR:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "other", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_NEG:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_POS:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ABS:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_INVERT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_COMPLEX:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_INT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_FLOAT:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_ROUND:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "ndigits", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_INDEX:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_REDUCE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_REDUCE_EX:
                /* Pickle documentation says the proto argument is required,
                 * but CPython implementation makes it optional with a default
                 * of 0.
                 */
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "proto", SKY_DATA_TYPE_INT, sky_integer_zero,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GETNEWARGS:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_GETSTATE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;
            case SKY_TYPE_METHOD_SLOT_SETSTATE:
                parameters = sky_parameter_list_createwithparameters(
                        "self", SKY_DATA_TYPE_OBJECT, NULL,
                        "state", SKY_DATA_TYPE_OBJECT, NULL,
                        NULL);
                break;

            case SKY_TYPE_METHOD_SLOT_COUNT:
                sky_error_fatal("internal error");
        }

        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&(sky_type_method_slot_parameter_lists[slot])),
                parameters,
                SKY_TRUE);
    }

    return parameters;
}


void
sky_type_setmethodslot(sky_type_t                   type,
                       const char *                 name,
                       sky_native_code_function_t   funcptr)
{
    char                                *qualname;
    uintptr_t                           name_hash;
    sky_string_t                        name_string;
    sky_object_t                        function;
    sky_type_method_item_t              *method_item;
    sky_type_method_slot_descriptor_t   *descriptor;

    sky_once_run(&sky_type_methods_once, sky_type_methods_once_runner, NULL);

    name_string = sky_string_createfromliteral(name, strlen(name));
    name_hash = sky_string_hash(name_string);
    method_item = (sky_type_method_item_t *)
                  sky_hashtable_lookup(&sky_type_method_slots_hash,
                                       name_hash,
                                       (void *)name);
    sky_error_validate_debug(NULL != method_item);
    descriptor = &(method_item->method);

    SKY_ASSET_BLOCK_BEGIN {
        sky_format_asprintf(&qualname, "%s.%s", type->type_data.name, name);
        sky_asset_save(qualname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        function = sky_function_createbuiltinwithparameterlist(
                qualname,
                descriptor->docstr,
                funcptr,
                descriptor->return_type,
                NULL,
                sky_type_method_slot_parameter_list(descriptor->slot));
        sky_function_setfastcallable(function, SKY_TRUE);
        if (descriptor->staticmethod) {
            function = sky_staticmethod_create(function);
        }
        else if (descriptor->classmethod) {
            function = sky_classmethod_create(function);
        }
    } SKY_ASSET_BLOCK_END;

    sky_dict_setitem(sky_object_dict(type), name_string, function);
    sky_type_methods_update(type, name_string, function);
}


void
sky_type_setmethodslots(sky_type_t type, ...)
{
    va_list                     ap;
    const char                  *name;
    sky_native_code_function_t  function;

    va_start(ap, type);
    while ((name = va_arg(ap, const char *)) != NULL) {
        function = va_arg(ap, sky_native_code_function_t);
        sky_type_setmethodslot(type, name, function);
    }
    va_end(ap);
}


void
sky_type_setmethodslotwithparameterlist(sky_type_t                  type,
                                        const char *                name,
                                        sky_native_code_function_t  funcptr,
                                        sky_parameter_list_t        parameters)
{
    char                                *qualname;
    uintptr_t                           name_hash;
    sky_string_t                        name_string;
    sky_object_t                        function;
    sky_type_method_item_t              *method_item;
    sky_type_method_slot_descriptor_t   *descriptor;

    sky_once_run(&sky_type_methods_once, sky_type_methods_once_runner, NULL);

    name_string = sky_string_createfromliteral(name, strlen(name));
    name_hash = sky_string_hash(name_string);
    method_item = (sky_type_method_item_t *)
                  sky_hashtable_lookup(&sky_type_method_slots_hash,
                                       name_hash,
                                       (void *)name);
    sky_error_validate_debug(NULL != method_item);
    descriptor = &(method_item->method);

    SKY_ASSET_BLOCK_BEGIN {
        sky_format_asprintf(&qualname, "%s.%s", type->type_data.name, name);
        sky_asset_save(qualname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

        function = sky_function_createbuiltinwithparameterlist(
                qualname,
                descriptor->docstr,
                funcptr,
                descriptor->return_type,
                NULL,
                parameters);
        if (descriptor->staticmethod) {
            function = sky_staticmethod_create(function);
        }
        else if (descriptor->classmethod) {
            function = sky_classmethod_create(function);
        }
    } SKY_ASSET_BLOCK_END;

    sky_dict_setitem(sky_object_dict(type), name_string, function);
    sky_type_methods_update(type, name_string, function);
}


void
sky_type_setmethodslotwithparameters(sky_type_t                 type,
                                     const char *               name,
                                     sky_native_code_function_t funcptr,
                                     const char *               keyword,
                                     ...)
{
    va_list                 ap;
    sky_parameter_list_t    parameters;

    va_start(ap, keyword);
    parameters = sky_parameter_list_createwithparametersv(keyword, ap);
    va_end(ap);

    sky_type_setmethodslotwithparameterlist(type, name, funcptr, parameters);
}


void
sky_type_setname(sky_type_t type, sky_string_t name)
{
    sky_type_data_t *type_data = sky_type_data(type);

    sky_bytes_t     bytes;
    sky_buffer_t    buffer;

    if (type_data->flags & SKY_TYPE_FLAG_BUILTIN) {
        sky_error_raise_format(
                sky_TypeError,
                "can't set attributes of built-in type %#@",
                sky_type_name(type));
    }
    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "can only assign string to %@.__name__, not %#@",
                sky_type_name(type),
                sky_type_name(sky_object_type(name)));
    }

    SKY_ASSET_BLOCK_BEGIN {
        bytes = sky_codec_encode(NULL, name, NULL, NULL, 0);
        sky_buffer_acquire(&buffer, bytes, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (memchr(buffer.buf, 0, buffer.len)) {
            sky_error_raise_string(sky_ValueError,
                                   "__name__ must not contain null bytes");
        }

        sky_free(type_data->name);
        type_data->name = sky_strndup(buffer.buf, buffer.len);
    } SKY_ASSET_BLOCK_END;
}


size_t
sky_type_sizeof(sky_type_t self)
{
    sky_type_data_t *type_data = sky_type_data(self);

    size_t  size;

    size = sky_memsize(self) +
           sky_memsize(type_data->method_slots) +
           sky_memsize(type_data->bases) +
           sky_memsize(type_data->mro) +
           sky_memsize(type_data->data_offsets);
    if (!(type_data->flags & SKY_TYPE_FLAG_STATIC)) {
        size += sky_memsize(type_data->name);
        if (type_data->doc) {
            size += sky_memsize(type_data->doc);
        }
    }
    return size;
}


sky_list_t
sky_type_subclasses(sky_type_t self)
{
    sky_type_data_t *type_data = sky_type_data(self);

    ssize_t                     i;
    sky_list_t                  subclasses;
    sky_object_t                object;
    sky_type_subclass_list_t    *list;

    subclasses = sky_list_create(NULL);
    for (list = type_data->subclasses; list; list = list->next) {
        for (i = 0; i < list->len; ++i) {
            if ((object = list->types[i]) != NULL) {
                sky_list_append(subclasses, object);
            }
        }
    }

    return subclasses;
}


void
sky_type_initialize_builtin(sky_type_t type, ssize_t nbases, ...)
{
#if !defined(NDEBUG)
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(type);
#endif
    sky_type_data_t     *type_data = sky_type_data(type);

    ssize_t     i;
    va_list     ap;
    sky_dict_t  dict;
    sky_type_t  *bases;

    /* Sanity checks */
    sky_error_validate_debug(sky_type_type == object_data->u.type);
    sky_error_validate_debug(NULL == object_data->gc_link);
    sky_error_validate_debug(2 == object_data->gc_epoch);
    sky_error_validate_debug((type_data->flags & SKY_TYPE_FLAG_BUILTIN) != 0);
    sky_error_validate_debug(NULL == object_data->refs);
    sky_error_validate_debug(NULL == type_data->method_slots);
    sky_error_validate_debug(NULL == type_data->qualname);
    sky_error_validate_debug(NULL == type_data->subclasses);
    dict = *(sky_dict_t *)
            ((char *)type + sky_type_type->type_data.dict_offset);
    sky_error_validate_debug(NULL == dict);
    sky_error_validate_debug(NULL == type_data->bases);
    sky_error_validate_debug(NULL == type_data->mro);
    sky_error_validate_debug(NULL == type_data->data_offsets);

    if (!nbases) {
        sky_type_initialize_bases(type, nbases, NULL);
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            bases = sky_asset_malloc(nbases * sizeof(sky_type_t),
                                     SKY_ASSET_CLEANUP_ALWAYS);
            va_start(ap, nbases);
            for (i = 0; i < nbases; ++i) {
                bases[i] = va_arg(ap, sky_type_t);
            }
            va_end(ap);
            sky_type_initialize_bases(type, nbases, bases);
        } SKY_ASSET_BLOCK_END;
    }

    type_data->method_slots = sky_calloc(SKY_TYPE_METHOD_SLOT_COUNT,
                                         sizeof(sky_object_t));

    sky_type_complete_static(type);
}


void
sky_type_initialize_static(sky_type_t type, ssize_t nbases, ...)
{
#if !defined(NDEBUG)
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(type);
#endif
    sky_type_data_t     *type_data = sky_type_data(type);

    ssize_t     i;
    va_list     ap;
    sky_dict_t  dict;
    sky_type_t  *bases;

    /* Sanity checks */
    sky_error_validate_debug(sky_type_type == object_data->u.type);
    sky_error_validate_debug(NULL == object_data->gc_link);
    sky_error_validate_debug(2 == object_data->gc_epoch);
    sky_error_validate_debug((type_data->flags & SKY_TYPE_FLAG_BUILTIN) != 0);
    sky_error_validate_debug(NULL == object_data->refs);
    sky_error_validate_debug(NULL == type_data->method_slots);
    sky_error_validate_debug(NULL == type_data->qualname);
    sky_error_validate_debug(NULL == type_data->subclasses);
    dict = *(sky_dict_t *)
            ((char *)type + sky_type_type->type_data.dict_offset);
    sky_error_validate_debug(NULL == dict);
    if (sky_base_object_type == type || sky_type_type == type) {
        sky_error_validate_debug(NULL != type_data->bases);
        sky_error_validate_debug(NULL != type_data->mro);
        sky_error_validate_debug(NULL != type_data->data_offsets);
    }
    else {
        sky_error_validate_debug(NULL == type_data->bases);
        sky_error_validate_debug(NULL == type_data->mro);
        sky_error_validate_debug(NULL == type_data->data_offsets);

        if (!nbases) {
            sky_type_initialize_bases(type, nbases, NULL);
        }
        else {
            SKY_ASSET_BLOCK_BEGIN {
                bases = sky_asset_malloc(nbases * sizeof(sky_type_t),
                                         SKY_ASSET_CLEANUP_ALWAYS);
                va_start(ap, nbases);
                for (i = 0; i < nbases; ++i) {
                    bases[i] = va_arg(ap, sky_type_t);
                }
                va_end(ap);
                sky_type_initialize_bases(type, nbases, bases);
            } SKY_ASSET_BLOCK_END;
        }
    }

    type_data->method_slots = sky_calloc(SKY_TYPE_METHOD_SLOT_COUNT,
                                         sizeof(sky_object_t));
}


static SKY_ALIGNED(16) union {
    struct {
        size_t      len;
        sky_type_t  items[1];
    }                                   static_list;
    sky_type_list_t                     type_list;
} sky_type_type_bases = {
    { 1, { &sky_base_object_type_struct } },
};

static SKY_ALIGNED(16) union {
    struct {
        size_t      len;
        sky_type_t  items[2];
    }                                   static_list;
    sky_type_list_t                     type_list;
} sky_type_type_mro = {
    { 2, { &sky_type_type_struct, &sky_base_object_type_struct } },
};

static SKY_ALIGNED(16) union {
    struct {
        size_t      len;
        size_t      items[2];
    }                                   static_list;
    sky_type_list_t                     type_list;
} sky_type_type_data_offsets = {
    { 2, { sizeof(sky_object_data_t), 0 } },
};

static const char sky_type_type_doc[] =
"type(object) -> the object's type\n"
"type(name, bases, dict) -> a new type";

SKY_ALIGNED(16)
struct sky_type_s sky_type_type_struct = {
    SKY_OBJECT_DATA_INITIALIZER(&sky_type_type_struct),
    {
        "type",                                     /* name                 */
        NULL,                                       /* method_slots         */
        offsetof(struct sky_type_s, dict),          /* dict_offset          */
        (SKY_TYPE_FLAG_BUILTIN |                    /* flags                */
         SKY_TYPE_FLAG_HAS_DICT |
         SKY_TYPE_FLAG_DICT_READONLY |
         SKY_TYPE_FLAG_STATIC), 
        sizeof(struct sky_type_s),                  /* instance_size        */
        sizeof(void *),                             /* instance_alignment   */
        sizeof(sky_type_data_t),                    /* type_size            */
        sizeof(void *),                             /* type_alignment       */
        &(sky_type_type_bases.type_list),           /* bases                */
        &(sky_type_type_mro.type_list),             /* mro                  */
        &(sky_type_type_data_offsets.type_list),    /* data_offsets         */
        NULL,                                       /* subclasses           */
        sky_type_allocate_calloc,                   /* allocate             */
        sky_type_instance_deallocate,               /* deallocate           */
        sky_type_instance_initialize,               /* initialize           */
        sky_type_instance_finalize,                 /* finalize             */
        sky_type_instance_visit,                    /* visit                */
        NULL,                                       /* iterate              */
        NULL,                                       /* iterate_cleanup      */
        NULL,                                       /* buffer_acquire       */
        NULL,                                       /* buffer_release       */
        (char *)sky_type_type_doc,                  /* doc                  */
        NULL,                                       /* qualname             */
        SKY_SPINLOCK_INITIALIZER,                   /* spinlock             */
    },
    NULL,
};
sky_type_t const sky_type_type = &sky_type_type_struct;


void
sky_type_initialize_library(void)
{
    sky_type_setmembers(sky_type_type,
            "__basicsize__",    NULL, offsetof(sky_type_data_t, instance_size),
                                SKY_DATA_TYPE_SIZE_T, SKY_MEMBER_FLAG_READONLY,
            "__dictoffset__",   NULL, offsetof(sky_type_data_t, dict_offset),
                                SKY_DATA_TYPE_SIZE_T, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(
            sky_type_type,
            "__itemsize__",
            sky_integer_zero);
    sky_type_setattr_builtin(
            sky_type_type,
            "__weakrefoffset__",
            sky_integer_create(offsetof(sky_object_data_t, refs)));

    sky_type_setattr_getset(
            sky_type_type,
            "__abstractmethods__",
            NULL,
            sky_type_abstractmethods_getter,
            sky_type_abstractmethods_setter);
    sky_type_setattr_getset(
            sky_type_type,
            "__base__",
            NULL,
            sky_type_base_getter,
            NULL);
    sky_type_setattr_getset(
            sky_type_type,
            "__bases__",
            NULL,
            sky_type_bases_getter,
            NULL);  /* TODO sky_type_bases_setter */
    sky_type_setattr_getset(
            sky_type_type,
            "__dict__",
            NULL,
            sky_type_dict_getter,
            NULL);
    sky_type_setattr_getset(
            sky_type_type,
            "__doc__",
            NULL,
            sky_type_doc_getter,
            sky_type_doc_setter);
    sky_type_setattr_getset(
            sky_type_type,
            "__flags__",
            NULL,
            sky_type_flags_getter,
            NULL);
    sky_type_setattr_getset(
            sky_type_type,
            "__module__",
            NULL,
            sky_type_module_getter,
            sky_type_module_setter);
    sky_type_setattr_getset(
            sky_type_type,
            "__mro__",
            NULL,
            sky_type_mro_getter,
            NULL);
    sky_type_setattr_getset(
            sky_type_type,
            "__name__",
            NULL,
            sky_type_name_getter,
            sky_type_name_setter);
    sky_type_setattr_getset(
            sky_type_type,
            "__qualname__",
            NULL,
            sky_type_qualname_getter,
            sky_type_qualname_setter);

    sky_type_setattr_builtin(
            sky_type_type,
            "mro",
            sky_function_createbuiltin(
                    "type.mro",
                    NULL,
                    (sky_native_code_function_t)sky_type_mro,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_type_type,
            "__prepare__",
            sky_classmethod_create(
                    sky_function_createbuiltin(
                            "type.__prepare__",
                            NULL,
                            (sky_native_code_function_t)sky_type_prepare,
                            SKY_DATA_TYPE_OBJECT_DICT,
                            "metaclass", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                            "name", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                            "bases", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                            "**kws", SKY_DATA_TYPE_OBJECT_DICT, NULL,
                            NULL)));
    sky_type_setattr_builtin(
            sky_type_type,
            "__subclasses__",
            sky_function_createbuiltin(
                    "type.__subclasses__",
                    NULL,
                    (sky_native_code_function_t)sky_type_subclasses,
                    SKY_DATA_TYPE_OBJECT_LIST,
                    "self", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
                    NULL));

    sky_error_validate(NULL == sky_type_new_parameters);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky_type_new_parameters),
                                 sky_parameter_list_create(),
                                 SKY_TRUE);
    sky_parameter_list_add(sky_type_new_parameters,
                           SKY_STRING_LITERAL("name"),
                           SKY_DATA_TYPE_OBJECT_STRING,
                           NULL);
    sky_parameter_list_add(sky_type_new_parameters,
                           SKY_STRING_LITERAL("bases"),
                           SKY_DATA_TYPE_OBJECT_TUPLE,
                           NULL);
    sky_parameter_list_add(sky_type_new_parameters,
                           SKY_STRING_LITERAL("dict"),
                           SKY_DATA_TYPE_OBJECT_DICT,
                           NULL);

    sky_type_setmethodslots(sky_type_type,
            "__new__", sky_type_new,
            "__init__", sky_type_init,
            "__repr__", sky_type_repr,
            "__sizeof__", sky_type_sizeof,
            "__getattribute__", sky_type_getattribute,
            "__setattr__", sky_type_setattr,
            "__delattr__", sky_type_delattr,
            "__dir__", sky_type_dir,
            "__call__", sky_type_call,
            "__instancecheck__", sky_type_instancecheck,
            "__subclasscheck__", sky_object_subclasscheck,
            NULL);
}
