/** @file
  * @brief
  * @defgroup containers Containers
  * @{
  * @defgroup sky_list Ordered Lists
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_LIST_H__
#define __SKYTHON_CORE_SKY_LIST_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new list object instance.
  * If @a object is @c NULL, a new empty list will be created; otherwise,
  * @a object must be iterable, and a new list will be created by iterating
  * it. Even if @a object is already a list, a new list will be created. Any
  * object that implements __iter__() can be converted into a list.
  *
  * @param[in]  object      the object to be converted into a list.
  * @return     the new list object instance.
  */
SKY_EXTERN sky_list_t
sky_list_create(sky_object_t object);


/** Create a new list object instance from an array of objects.
  *
  * @param[in]  count   the number of objects present in the @a objects list.
  * @param[in]  objects the objects to be used to populate the list. Any
  *                     @c NULL pointers will be replaced with @c sky_None
  *                     objects.
  * @return     the new list object instance.
  */
SKY_EXTERN sky_list_t
sky_list_createfromarray(ssize_t count, sky_object_t *objects);


/** Create a new list object instance.
  *
  * @param[in]  initial_capacity    the number of objects that the list will
  *                                 initially be able to store without the need
  *                                 to grow. If specified as 0, a default
  *                                 initial capacity will be used.
  * @return     the newly allocated and initialized list structure.
  */
SKY_EXTERN sky_list_t
sky_list_createwithcapacity(ssize_t initial_capacity);


/** Append an object to the end of a list.
  *
  * @param[in]  list    the list onto which the object will be appended.
  * @param[in]  object  the object to append to the end of the list.
  */
SKY_EXTERN void
sky_list_append(sky_list_t list, sky_object_t object);


/** Remove all objects from a list.
  * Removes all objects from a list, leaving it empty.
  *
  * @param[in]  list    the list to be cleared.
  */
SKY_EXTERN void
sky_list_clear(sky_list_t list);


/** Return a shallow copy of a list.
  *
  * @param[in]  list    the list to be copied.
  * @return     a new list object that is a shallow copy of @a list.
  */
SKY_EXTERN sky_list_t
sky_list_copy(sky_list_t list);


/** Delete an object or objects from a list.
  * Starting from the specified @a index, @a count objects will be deleted from
  * the list. A @c sky_IndexError exception will be raised if @a index is out
  * of range.
  *
  * @param[in]  list    the list from which the object is to be deleted.
  * @param[in]  start   the first index position to delete, inclusive.
  * @param[in]  stop    the last index position to delete, exclusive.
  * @param[in]  step    the step to use while iterating from @a start to
  *                     @a stop. Must not be 0.
  */
SKY_EXTERN void
sky_list_delete(sky_list_t list, ssize_t start, ssize_t stop, ssize_t step);


/** Extend a list by appending another list to it.
  * Each object in source_list is appended to list. This function is a more
  * efficient way to combine two lists than iterating a list and calling
  * sky_list_append() for each object.
  *
  * @param[in]  list        the list to be extended.
  * @param[in]  sequence    the list to be appended to list.
  */
SKY_EXTERN void
sky_list_extend(sky_list_t list, sky_object_t sequence);


/** Search a list to find the index position of an object.
  * The list will be searched in the specified range to find the object. If the
  * object is found, its absolute index (counted starting from 0) will be
  * returned; otherwise, -1 will be returned.
  *
  * @param[in]  list            the list to be searched.
  * @param[in]  value           the object to find.
  * @param[in]  start           the starting index (inclusive).
  * @param[in]  stop            the ending index (exclusive).
  * @return     the absolute index position (counted starting from 0) of the
  *             found object, or -1 if the object is not found.
  */
SKY_EXTERN ssize_t
sky_list_find(sky_list_t list, sky_object_t value, ssize_t start, ssize_t stop);


/** Get an object at a specific index position in a list.
  * 
  * @param[in]  list    the list to query.
  * @param[in]  index   the index position in the list. If the index position
  *                     is out of range, @c sky_IndexError will be raised.
  * @return     the object at the requested index position in the list.
  */
SKY_EXTERN sky_object_t
sky_list_get(sky_list_t list, ssize_t index);


/** Search a list to find the index position of an object.
  * The list will be searched in the specified range to find the object. If the
  * object is found, its absolute index (counted starting from 0) will be
  * returned; otherwise, @c sky_ValueError will be raised.
  *
  * @param[in]  list            the list to be searched.
  * @param[in]  value           the object to find.
  * @param[in]  start           the starting index (inclusive).
  * @param[in]  stop            the ending index (exclusive).
  * @return     the absolute index position (counted starting from 0) of the
  *             found object.
  */
SKY_EXTERN ssize_t
sky_list_index(sky_list_t list, sky_object_t value, ssize_t start, ssize_t stop);


/** Insert an object into a list.
  *
  * @param[in]  list    the list into which the object is to be inserted.
  * @param[in]  index   the position at which to insert the object. The index
  *                     position is counted starting from 0, and may extend
  *                     one position beyond the end of the list to append.
  *                     If the index position is out of range (negative or too
  *                     far beyond the end of the list), @c sky_IndexError
  *                     will be raised.
  * @param[in]  object  the object to be inserted.
  */
SKY_EXTERN void
sky_list_insert(sky_list_t list, ssize_t index, sky_object_t object);


/** Return the number of elements contained in a list.
  *
  * @param[in]  list    the list for which the number of elements it contains
  *                     is to be returned.
  * @return     the number of elements contained in @a list.
  */
SKY_EXTERN ssize_t
sky_list_len(sky_list_t list);


/** Remove an item from a list by index and return it.
  *
  * @param[in]  list    the list from which an item is to be popped.
  * @param[in]  index   the index position from which the item is to be popped.
  * @return     the item popped from @a list.
  */
SKY_EXTERN sky_object_t
sky_list_pop(sky_list_t list, ssize_t index);


/** Repeat a list.
  *
  * @param[in]  list    the list to repeat.
  * @param[in]  count   the number of times to repeat @a list.
  * @param[in]  inplace @c SKY_TRUE to repeat the list in place, or
  *                     @c SKY_FALSE to create a new list.
  * @return     a list instance that is @a list repeated @a count times.
  */
sky_list_t
sky_list_repeat(sky_list_t list, ssize_t count, sky_bool_t inplace);


/** Reverse the order of items in a list.
  * The items in the list are reversed in place.
  *
  * @param[in]  list    the list to reverse.
  */
SKY_EXTERN void
sky_list_reverse(sky_list_t list);


/** Set an object in a list.
  *
  * @param[in]  list    the list into which the object is to be set.
  * @param[in]  index   the index position into which the object is to be set.
  * @param[in]  object  the object to set.
  */
SKY_EXTERN void
sky_list_set(sky_list_t list, ssize_t index, sky_object_t object);


/** Set a value or values into a list.
  * This is the implementation of __setitem__().
  *
  * @param[in]  list    the list into which the value or values are to be set.
  * @param[in]  item    the index position or slice into which the value or
  *                     values are to be set.
  * @param[in]  value   the value or values to set.
  */
SKY_EXTERN void
sky_list_setitem(sky_list_t list, sky_object_t item, sky_object_t value);


/** Create a new list that is a slice of an existing one.
  *
  * @param[in]  list    the existing list to slice.
  * @param[in]  start   the starting index for the slice, inclusive.
  * @param[in]  stop    the ending index for the slice, exclusive.
  * @param[in]  step    the step to use between @a start and @a stop.
  * @return     a new list instance that is the specified slice of an existing
  *             list.
  */
SKY_EXTERN sky_list_t
sky_list_slice(sky_list_t list, ssize_t start, ssize_t stop, ssize_t step);


/** Sort a list in place.
  *
  * @param[in]  list    the list to be sorted.
  * @param[in]  key     an optional callable object to be called for each item
  *                     in the list to obtain the value to be sorted. For each
  *                     call, the only argument passed is the item from the
  *                     list for which it is being called.
  * @param[in]  reverse @c SKY_TRUE to sort the list in descending order, or
  *                     @c SKY_FALSE to sort it in ascending order.
  */
SKY_EXTERN void
sky_list_sort(sky_list_t list, sky_object_t key, sky_bool_t reverse);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_LIST_H__ */

/** @} **/
/** @} **/
