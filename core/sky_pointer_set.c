#include "sky_private.h"


static sky_bool_t
sky_pointer_set_item_compare(sky_hashtable_item_t * item,
                             void *                 key)
{
    return (((sky_pointer_set_item_t *)item)->pointer == key ? SKY_TRUE
                                                             : SKY_FALSE);
}


sky_bool_t
sky_pointer_set_add(sky_pointer_set_t *set, void *pointer)
{
    uintptr_t               key_hash;
    sky_pointer_set_item_t  *item, *new_item;

    key_hash = sky_pointer_set_key_hash(pointer);
    new_item = sky_calloc(1, sizeof(sky_pointer_set_item_t));
    new_item->pointer = pointer;

    item = (sky_pointer_set_item_t *)
           sky_hashtable_insert(&(set->table),
                                &(new_item->base),
                                key_hash,
                                pointer);
    if (item != new_item) {
        sky_free(new_item);
        if (!item) {
            sky_error_raise_object(sky_MemoryError, sky_None);
        }
        return SKY_FALSE;
    }
    return SKY_TRUE;
}


void
sky_pointer_set_cleanup(sky_pointer_set_t *set)
{
    sky_hashtable_cleanup(&(set->table), sky_free);
}


sky_pointer_set_t *
sky_pointer_set_create(void)
{
    sky_pointer_set_t   *set;

    set = sky_calloc(1, sizeof(sky_pointer_set_t));
    sky_hashtable_init(&(set->table), sky_pointer_set_item_compare);

    return set;
}


sky_bool_t
sky_pointer_set_delete(sky_pointer_set_t *set, void *pointer)
{
    uintptr_t               key_hash;
    sky_hashtable_item_t    *item;

    key_hash = sky_pointer_set_key_hash(pointer);
    if (!(item = sky_hashtable_delete(&(set->table), key_hash, pointer))) {
        return SKY_FALSE;
    }
    sky_free(item);

    return SKY_TRUE;
}


void
sky_pointer_set_destroy(sky_pointer_set_t *set)
{
    sky_hashtable_cleanup(&(set->table), sky_free);
    sky_free(set);
}


void
sky_pointer_set_init(sky_pointer_set_t *set)
{
    memset(set, 0, sizeof(sky_pointer_set_t));
    sky_hashtable_init(&(set->table), sky_pointer_set_item_compare);
}


void
sky_pointer_set_insert(sky_pointer_set_t *      set,
                       sky_hashtable_item_t *   item,
                       uintptr_t                pointer_hash,
                       void *                   pointer)
{
    sky_hashtable_item_t    *result;

    result = sky_hashtable_insert(&(set->table), item, pointer_hash, pointer);
    sky_error_validate_debug(result == item);
}


void
sky_pointer_set_merge(sky_pointer_set_t *set, sky_pointer_set_t *other)
{
    uintptr_t               key_hash;
    sky_pointer_set_item_t  *item, *set_item;

    sky_hashtable_setcapacity(&(set->table),
                              sky_hashtable_length(&(set->table)) +
                              sky_hashtable_length(&(other->table)));

    while ((item = sky_pointer_set_pop(other)) != NULL) {
        key_hash = sky_pointer_set_key_hash(item->pointer);
        set_item = (sky_pointer_set_item_t *)
                   sky_hashtable_insert(&(set->table),
                                        &(item->base),
                                        key_hash,
                                        item->pointer);
        if (set_item != item) {
            sky_free(item);
            sky_error_validate(set_item != NULL);
        }
    }
}


void *
sky_pointer_set_pop_pointer(sky_pointer_set_t *set)
{
    void                    *pointer = NULL;
    sky_pointer_set_item_t  *item;

    if ((item = sky_pointer_set_pop(set)) != NULL) {
        pointer = item->pointer;
        sky_free(item);
    }

    return pointer;
}
