#include "sky_private.h"


static inline char *
sky_buffer_adjust_pointer(const char *      pointer,
                          const ssize_t *   suboffsets,
                          ssize_t           index)
{
    if (suboffsets && suboffsets[index]) {
        pointer = *((char **)pointer) + suboffsets[index];
    }
    return (char *)pointer;
}


typedef struct sky_buffer_unpack_s {
    sky_object_t                        unpack_from;
    sky_tuple_t                         unpack_from_args;
    sky_memoryview_t                    view;
    sky_buffer_t *                      buffer;
} sky_buffer_unpack_t;


static sky_buffer_unpack_t *
sky_buffer_unpack_create(const char *format, ssize_t itemsize)
{
    sky_module_t        struct_module;
    sky_object_t        o;
    sky_buffer_unpack_t *unpacker;

    SKY_ASSET_BLOCK_BEGIN {
        unpacker = sky_asset_malloc(sizeof(sky_buffer_unpack_t),
                                    SKY_ASSET_CLEANUP_ON_ERROR);

        SKY_ASSET_BLOCK_BEGIN {
            unpacker->buffer = sky_asset_malloc(sizeof(sky_buffer_t) + itemsize,
                                                SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_initialize(
                    unpacker->buffer,
                    (char *)unpacker->buffer + sizeof(sky_buffer_t),
                    itemsize,
                    SKY_TRUE,
                    SKY_BUFFER_FLAG_CONTIG_RO);
            unpacker->buffer->itemsize = itemsize;
            unpacker->view = sky_memoryview_createwithbuffer(unpacker->buffer);
        } SKY_ASSET_BLOCK_END;

        sky_asset_save(unpacker->view,
                       (sky_free_t)sky_memoryview_release,
                       SKY_ASSET_CLEANUP_ON_ERROR);

        struct_module = sky_module_import(SKY_STRING_LITERAL("struct"));
        o = sky_object_callmethod(struct_module,
                                  SKY_STRING_LITERAL("Struct"),
                                  sky_object_build("(s)", format),
                                  NULL);
        unpacker->unpack_from =
                sky_object_getattr(o,
                                   SKY_STRING_LITERAL("unpack_from"),
                                   sky_NotSpecified);
        unpacker->unpack_from_args = sky_tuple_pack(1, unpacker->view);
    } SKY_ASSET_BLOCK_END;

    return unpacker;
}


static void
sky_buffer_unpack_destroy(sky_buffer_unpack_t *unpacker)
{
    sky_memoryview_release(unpacker->view);
    sky_free(unpacker);
}


void
sky_buffer_acquire(sky_buffer_t *   buffer,
                   sky_object_t     object,
                   unsigned int     flags)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(object));

    void            *data;
    sky_type_list_t *offsets;

    if (!type_data->buffer_acquire) {
        sky_error_raise_format(sky_TypeError,
                               "%#@ does not support the buffer interface",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_istagged(object)) {
        data = NULL;
    }
    else {
        offsets = type_data->data_offsets;
        data = (void *)((char *)object + offsets->items[0].offset);
    }

    type_data->buffer_acquire(object, data, buffer, flags);

    buffer->object = object;
}


void
sky_buffer_acquirecbuffer(sky_buffer_t *buffer,
                          sky_object_t  object,
                          unsigned int  flags,
                          const char *  source,
                          const char *  message)
{
    SKY_ERROR_TRY {
        sky_buffer_acquire(buffer, object, flags);
    } SKY_ERROR_EXCEPT_ANY {
        sky_error_clear();
        if (message) {
            sky_error_raise_string(sky_TypeError, message);
        }
        else {
            const char  *what;

            if (flags & SKY_BUFFER_FLAG_WRITABLE) {
                what = "read-write buffer";
            }
            else {
                what = "bytes or buffer";
            }
            sky_error_raise_format(sky_TypeError,
                                   "%s must be %s, not %#@",
                                   source,
                                   what,
                                   sky_type_name(sky_object_type(object)));
        }
    } SKY_ERROR_TRY_END;

    if (!sky_buffer_iscontiguous(buffer, 'C')) {
        sky_buffer_release(buffer);
        if (message) {
            sky_error_raise_string(sky_TypeError, message);
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "%s must be contiguous buffer, not %#@",
                                   source,
                                   sky_type_name(sky_object_type(buffer)));
        }
    }
}


sky_bool_t
sky_buffer_check(sky_object_t object)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(object));

    return (type_data->buffer_acquire ? SKY_TRUE : SKY_FALSE);
}


static sky_bool_t
sky_buffer_isformatequivalent(const sky_buffer_t *  buffer_1,
                              const sky_buffer_t *  buffer_2)
{
    const char  *format_1, *format_2;

    format_1 = (buffer_1->format ? buffer_1->format : "B");
    if (*format_1 == '@') {
        ++format_1;
    }
    format_2 = (buffer_2->format ? buffer_2->format : "B");
    if (*format_2 == '@') {
        ++format_2;
    }

    return (strcmp(format_1, format_2) ||
            buffer_1->itemsize != buffer_2->itemsize ? SKY_FALSE
                                                     : SKY_TRUE);
}


static sky_bool_t
sky_buffer_isshapeequivalent(const sky_buffer_t *   buffer_1,
                             const sky_buffer_t *   buffer_2)
{
    ssize_t i;

    if (buffer_1->ndim != buffer_2->ndim) {
        return SKY_FALSE;
    }
    if (buffer_1->ndim <= 0) {
        return (buffer_1->len != buffer_2->len ? SKY_FALSE : SKY_TRUE);
    }
    if (1 == buffer_1->ndim) {
        return (buffer_1->len != buffer_2->len ||
                buffer_1->itemsize != buffer_2->itemsize ? SKY_FALSE
                                                         : SKY_TRUE);
    }

    for (i = 0; i < buffer_1->ndim; ++i) {
        if (buffer_1->shape[i] != buffer_2->shape[i]) {
            return SKY_FALSE;
        }
        if (!buffer_1->shape[i]) {
            break;
        }
    }

    return SKY_TRUE;
}


static void
sky_buffer_copy_base(const ssize_t *shape,
                     ssize_t        itemsize,
                     char *         target_buf,
                     const ssize_t *target_strides,
                     const ssize_t *target_suboffsets,
                     const char *   source_buf,
                     const ssize_t *source_strides,
                     const ssize_t *source_suboffsets,
                     char *         memory)
{
    char    *p, *source_ptr, *target_ptr;
    ssize_t i, size;

    if (!memory) {
        size = shape[0] * itemsize;
        if (target_buf + size < source_buf || source_buf + size < target_buf) {
            memcpy(target_buf, source_buf, size);
        }
        else {
            memmove(target_buf, source_buf, size);
        }
    }
    else {
        for (i = 0, p = memory;
             i < shape[0];
             p += itemsize, source_buf += source_strides[0], ++i)
        {
            source_ptr = sky_buffer_adjust_pointer(source_buf,
                                                   source_suboffsets,
                                                   0);
            memcpy(p, source_ptr, itemsize);
        }
        for (i = 0, p = memory;
             i < shape[0];
             p += itemsize, target_buf += target_strides[0], ++i)
        {
            target_ptr = sky_buffer_adjust_pointer(target_buf,
                                                   target_suboffsets,
                                                   0);
            memcpy(target_ptr, p, itemsize);
        }
    }
}


static void
sky_buffer_copy_records(const ssize_t * shape,
                        ssize_t         ndim,
                        ssize_t         itemsize,
                        char *          target_buf,
                        const ssize_t * target_strides,
                        const ssize_t * target_suboffsets,
                        const char *    source_buf,
                        const ssize_t * source_strides,
                        const ssize_t * source_suboffsets,
                        char *          memory)
{
    char    *source_ptr, *target_ptr;
    ssize_t i;

    if (1 == ndim) {
        sky_buffer_copy_base(shape,
                             itemsize,
                             target_buf,
                             target_strides,
                             target_suboffsets,
                             source_buf,
                             source_strides,
                             source_suboffsets,
                             memory);
        return;
    }

    for (i = 0; i < shape[0]; ++i) {
        target_ptr = sky_buffer_adjust_pointer(target_buf,
                                               target_suboffsets,
                                               0);
        source_ptr = sky_buffer_adjust_pointer(source_buf,
                                               source_suboffsets,
                                               0);

        sky_buffer_copy_records(
                shape + 1,
                ndim - 1,
                itemsize,
                target_ptr,
                target_strides + 1,
                (target_suboffsets ? target_suboffsets + 1 : NULL),
                source_ptr,
                source_strides + 1,
                (source_suboffsets ? source_suboffsets + 1 : NULL),
                memory);

        target_buf += target_strides[0];
        source_buf += source_strides[0];
    }
}


static inline sky_bool_t
sky_buffer_contiguous_last_dim(const sky_buffer_t *buffer)
{
    return (!(buffer->suboffsets &&
              buffer->suboffsets[buffer->ndim - 1] >= 0) &&
            buffer->strides[buffer->ndim - 1] == buffer->itemsize ? SKY_TRUE
                                                                  : SKY_FALSE);
}


void
sky_buffer_copy(sky_buffer_t *      target_buffer,
                const sky_buffer_t *source_buffer)
{
    void    *memory = NULL;
    size_t  nbytes;

    if (!sky_buffer_isformatequivalent(target_buffer, source_buffer) ||
        !sky_buffer_isshapeequivalent(target_buffer, source_buffer))
    {
        sky_error_raise_string(
                sky_ValueError,
                "ndarray assignment: "
                "lvalue and rvalue have different structures");
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!sky_buffer_contiguous_last_dim(source_buffer) ||
            !sky_buffer_contiguous_last_dim(target_buffer))
        {
            nbytes = target_buffer->shape[target_buffer->ndim - 1] *
                     target_buffer->itemsize;
            memory = sky_asset_malloc(nbytes, SKY_ASSET_CLEANUP_ALWAYS);
        }

        sky_buffer_copy_records(target_buffer->shape,
                                target_buffer->ndim,
                                target_buffer->itemsize,
                                target_buffer->buf,
                                target_buffer->strides,
                                target_buffer->suboffsets,
                                source_buffer->buf,
                                source_buffer->strides,
                                source_buffer->suboffsets,
                                memory);
    } SKY_ASSET_BLOCK_END;
}


static sky_bool_t
sky_buffer_equal_unpack(const char *            buf_1,
                        const char *            buf_2,
                        const char *            format,
                        sky_buffer_unpack_t *   unpack_1,
                        sky_buffer_unpack_t *   unpack_2)
{
    sky_object_t    data_1, data_2;

    /* A smarty pants compiler will do right by this. */
#define SKY_BUFFER_COMPARE_VALUES(type)         \
    do {                                        \
        type x;                                 \
        type y;                                 \
        memcpy(&x, buf_1, sizeof(type));        \
        memcpy(&y, buf_2, sizeof(type));        \
        return (x == y ? SKY_TRUE : SKY_FALSE); \
    } while (0)

    if (!unpack_1) {
        sky_error_validate_debug(!unpack_2);
        switch (*format) {
            case 'B':
                SKY_BUFFER_COMPARE_VALUES(unsigned char);
            case 'b':
                SKY_BUFFER_COMPARE_VALUES(signed char);
            case 'c':
                return (*buf_1 == *buf_2);
            case 'd':
                SKY_BUFFER_COMPARE_VALUES(double);
            case 'f':
                SKY_BUFFER_COMPARE_VALUES(float);
            case 'H':
                SKY_BUFFER_COMPARE_VALUES(unsigned short);
            case 'h':
                SKY_BUFFER_COMPARE_VALUES(signed short);
            case 'I':
                SKY_BUFFER_COMPARE_VALUES(unsigned int);
            case 'i':
                SKY_BUFFER_COMPARE_VALUES(signed int);
            case 'L':
                SKY_BUFFER_COMPARE_VALUES(unsigned long);
            case 'l':
                SKY_BUFFER_COMPARE_VALUES(signed long);
            case 'N':
                SKY_BUFFER_COMPARE_VALUES(size_t);
            case 'n':
                SKY_BUFFER_COMPARE_VALUES(ssize_t);
            case 'P':
                SKY_BUFFER_COMPARE_VALUES(void *);
            case 'Q':
                SKY_BUFFER_COMPARE_VALUES(unsigned long long);
            case 'q':
                SKY_BUFFER_COMPARE_VALUES(signed long long);
            case '?':
                SKY_BUFFER_COMPARE_VALUES(char);
        }
    }

    memcpy(unpack_1->buffer->buf, buf_1, unpack_1->buffer->itemsize);
    data_1 = sky_object_call(unpack_1->unpack_from,
                             unpack_1->unpack_from_args,
                             NULL);
    memcpy(unpack_2->buffer->buf, buf_2, unpack_2->buffer->itemsize);
    data_2 = sky_object_call(unpack_2->unpack_from,
                             unpack_2->unpack_from_args,
                             NULL);
    return sky_object_compare(data_1, data_2, SKY_COMPARE_OP_EQUAL);
}


static sky_bool_t
sky_buffer_equal_base(const ssize_t *       shape,
                      const char *          buf_1,
                      const ssize_t *       strides_1,
                      const ssize_t *       suboffsets_1,
                      const char *          buf_2,
                      const ssize_t *       strides_2,
                      const ssize_t *       suboffsets_2,
                      const char *          format,
                      sky_buffer_unpack_t * unpack_1,
                      sky_buffer_unpack_t * unpack_2)
{
    char        *ptr_1, *ptr_2;
    ssize_t     i;
    sky_bool_t  equal;

    for (i = 0; i < shape[0]; ++i) {
        ptr_1 = sky_buffer_adjust_pointer(buf_1, suboffsets_1, 0);
        ptr_2 = sky_buffer_adjust_pointer(buf_2, suboffsets_2, 0);

        if (!(equal = sky_buffer_equal_unpack(ptr_1,
                                              ptr_2,
                                              format,
                                              unpack_1,
                                              unpack_2)))
        {
            return SKY_FALSE;
        }
            
        buf_1 += strides_1[0];
        buf_2 += strides_2[0];
    }

    return SKY_TRUE;
}


static sky_bool_t
sky_buffer_equal_records(ssize_t                ndim,
                         const ssize_t *        shape,
                         const char *           buf_1,
                         const ssize_t *        strides_1,
                         const ssize_t *        suboffsets_1,
                         const char *           buf_2,
                         const ssize_t *        strides_2,
                         const ssize_t *        suboffsets_2,
                         const char *           format,
                         sky_buffer_unpack_t *  unpack_1,
                         sky_buffer_unpack_t *  unpack_2)
{
    char        *ptr_1, *ptr_2;
    ssize_t     i;
    sky_bool_t  equal;

    if (1 == ndim) {
        return sky_buffer_equal_base(shape,
                                     buf_1,
                                     strides_1,
                                     suboffsets_1,
                                     buf_2,
                                     strides_2,
                                     suboffsets_2,
                                     format,
                                     unpack_1,
                                     unpack_2);
    }

    for (i = 0; i < shape[0]; ++i) {
        ptr_1 = sky_buffer_adjust_pointer(buf_1, suboffsets_1, 0);
        ptr_2 = sky_buffer_adjust_pointer(buf_2, suboffsets_2, 0);

        equal = sky_buffer_equal_records(
                        ndim - 1,
                        shape + 1,
                        ptr_1,
                        strides_1 + 1,
                        (suboffsets_1 ? suboffsets_1 + 1 : NULL),
                        ptr_2,
                        strides_2 + 1,
                        (suboffsets_2 ? suboffsets_2 + 1 : NULL),
                        format,
                        unpack_1,
                        unpack_2);
        if (!equal) {
            return SKY_FALSE;
        }

        buf_1 += strides_1[0];
        buf_2 += strides_2[0];
    }

    return SKY_TRUE;
}


sky_bool_t
sky_buffer_equal(const sky_buffer_t *   buffer_1,
                 const sky_buffer_t *   buffer_2)
{
    const char          *format;
    sky_bool_t          equal, native;
    sky_buffer_unpack_t *unpack_1, *unpack_2;

    /* The buffers must have equivalent formats and shapes in order to be
     * considered equal. After this, ndim, format, and itemsize are guaranteed
     * to be the same for the two buffers. For ndim <= 1, len is also
     * guaranteed to be the same.
     */
    if (!sky_buffer_isformatequivalent(buffer_1, buffer_2) ||
        !sky_buffer_isshapeequivalent(buffer_1, buffer_2))
    {
        return SKY_FALSE;
    }

    /* Figure out how to unpack the data for comparison. This is necessary for
     * comparison because of uninitialized padding bytes and NaNs. If the
     * format is simple, use fast unpacking (via sky_data_type_unpack_native())
     * instead of the struct module.
     */
    native = SKY_FALSE;
    format = (buffer_1->format ? buffer_1->format : "B");
    if (*format == '@') {
        ++format;
    }
    if (format[0] && !format[1]) {
        switch (format[0]) {
            case 'B': case 'b':
            case 'c':
            case 'd':
            case 'f':
            case 'H': case 'h':
            case 'I': case 'i':
            case 'L': case 'l':
            case 'N': case 'n':
            case 'P':
            case 'Q': case 'q':
            case '?':
                native = SKY_TRUE;
                unpack_1 = unpack_2 = NULL;
                break;
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        if (!native) {
            unpack_1 = sky_buffer_unpack_create(format, buffer_1->itemsize);
            sky_asset_save(unpack_1,
                           (sky_free_t)sky_buffer_unpack_destroy,
                           SKY_ASSET_CLEANUP_ALWAYS);
            unpack_2 = sky_buffer_unpack_create(format, buffer_2->itemsize);
            sky_asset_save(unpack_2,
                           (sky_free_t)sky_buffer_unpack_destroy,
                           SKY_ASSET_CLEANUP_ALWAYS);
        }
        if (buffer_1->ndim <= 0) {
            equal = sky_buffer_equal_unpack(buffer_1->buf,
                                            buffer_2->buf,
                                            format,
                                            unpack_1,
                                            unpack_2);
        }
        else if (1 == buffer_1->ndim) {
            equal = sky_buffer_equal_base(buffer_1->shape,
                                          buffer_1->buf,
                                          buffer_1->strides,
                                          buffer_1->suboffsets,
                                          buffer_2->buf,
                                          buffer_2->strides,
                                          buffer_2->suboffsets,
                                          format,
                                          unpack_1,
                                          unpack_2);
        }
        else {
            equal = sky_buffer_equal_records(buffer_1->ndim,
                                             buffer_1->shape,
                                             buffer_1->buf,
                                             buffer_1->strides,
                                             buffer_1->suboffsets,
                                             buffer_2->buf,
                                             buffer_2->strides,
                                             buffer_2->suboffsets,
                                             format,
                                             unpack_1,
                                             unpack_2);
        }
    } SKY_ASSET_BLOCK_END;

    return equal;
}


void
sky_buffer_initialize(sky_buffer_t *buffer,
                      void *        bytes,
                      ssize_t       nbytes,
                      sky_bool_t    readonly,
                      unsigned int  flags)
{
    if (readonly && (flags & SKY_BUFFER_FLAG_WRITABLE)) {
        sky_error_raise_string(sky_BufferError, "Object is not writable.");
    }

    buffer->buf = bytes;
    buffer->len = nbytes;
    buffer->readonly = readonly;
    buffer->format = (flags & SKY_BUFFER_FLAG_FORMAT ? "B" : NULL);
    if (flags & (SKY_BUFFER_FLAG_ND | SKY_BUFFER_FLAG_STRIDES)) {
        buffer->ndim = 1;
    }
    else {
        buffer->ndim = 0;
    }
    buffer->shape = (flags & SKY_BUFFER_FLAG_ND ? &(buffer->len) : NULL);
    buffer->strides = (flags & SKY_BUFFER_FLAG_STRIDES ? &(buffer->itemsize) : NULL);
    buffer->suboffsets = NULL;
    buffer->itemsize = 1;
    buffer->internal = NULL;
}


static sky_bool_t
sky_buffer_iscontiguous_c(const sky_buffer_t *buffer)
{
    ssize_t i, sd;

    if (!buffer->ndim || !buffer->strides) {
        return SKY_TRUE;
    }
    sd = buffer->itemsize;
    if (1 == buffer->ndim) {
        return (1 == buffer->shape[0] || sd == buffer->strides[0]);
    }
    for (i = buffer->ndim - 1; i >= 0; --i) {
        if (!buffer->shape[i]) {
            return SKY_TRUE;
        }
        if (buffer->strides[i] != sd) {
            return SKY_FALSE;
        }
        sd *= buffer->shape[i];
    }

    return SKY_TRUE;
}


static sky_bool_t
sky_buffer_iscontiguous_f(const sky_buffer_t *buffer)
{
    ssize_t i, sd;

    if (!buffer->ndim) {
        return SKY_TRUE;
    }
    if (!buffer->strides) {
        return (1 == buffer->ndim);
    }
    sd = buffer->itemsize;
    if (1 == buffer->ndim) {
        return (1 == buffer->shape[0] || sd == buffer->strides[0]);
    }
    for (i = 0; i < buffer->ndim; ++i) {
        if (!buffer->shape[i]) {
            return SKY_TRUE;
        }
        if (buffer->strides[i] != sd) {
            return SKY_FALSE;
        }
        sd *= buffer->shape[i];
    }

    return SKY_TRUE;
}


sky_bool_t
sky_buffer_iscontiguous(const sky_buffer_t *buffer,
                        int                 order)
{
    if (!buffer->suboffsets) {
        switch (order) {
            case 'A':
                return sky_buffer_iscontiguous_c(buffer) ||
                       sky_buffer_iscontiguous_f(buffer);
            case 'C':
                return sky_buffer_iscontiguous_c(buffer);
            case 'F':
                return sky_buffer_iscontiguous_f(buffer);
        }
    }
    return SKY_FALSE;
}


void *
sky_buffer_pointer(const sky_buffer_t *buffer, ssize_t offset)
{
    char    *pointer;
    ssize_t nitems;

    nitems = buffer->shape[0];
    if (offset < 0) {
        offset += nitems;
    }
    if (offset < 0 || offset >= nitems) {
        sky_error_raise_string(sky_IndexError, "index out of bounds");
    }

    pointer = (char *)buffer->buf + (buffer->strides[0] * offset);
    return sky_buffer_adjust_pointer(pointer, buffer->suboffsets, 0);
}


void
sky_buffer_release(sky_buffer_t *buffer)
{
    sky_type_data_t *type_data = sky_type_data(sky_object_type(buffer->object));

    if (type_data->buffer_release) {
        void            *data;
        sky_type_list_t *offsets;

        if (sky_object_istagged(buffer->object)) {
            data = NULL;
        }
        else {
            offsets = type_data->data_offsets;
            data = (void *)((char *)buffer->object + offsets->items[0].offset);
        }

        type_data->buffer_release(buffer->object, data, buffer);
    }
    buffer->object = NULL;
}


static void
sky_buffer_init_strides_c(sky_buffer_t *buffer)
{
    ssize_t i;

    buffer->strides[buffer->ndim - 1] = buffer->itemsize;
    for (i = buffer->ndim - 2; i >= 0; --i) {
        buffer->strides[i] = buffer->strides[i + 1] * buffer->shape[i + 1];
    }
}


static void
sky_buffer_init_strides_f(sky_buffer_t *buffer)
{
    ssize_t i;

    buffer->strides[0] = buffer->itemsize;
    for (i = 1; i < buffer->ndim; ++i) {
        buffer->strides[i] = buffer->strides[i - 1] * buffer->shape[i - 1];
    }
}


void
sky_buffer_tocontiguous(const sky_buffer_t *buffer,
                        void *              bytes,
             SKY_UNUSED size_t              nbytes,
                        int                 order)
{
    sky_buffer_t    *full_buffer, *target_buffer;

    sky_error_validate_debug(nbytes > SSIZE_MAX ||
                             (ssize_t)nbytes >= buffer->len);
    sky_error_validate_debug('A' == order || 'C' == order || 'F' == order);

    if (sky_buffer_iscontiguous(buffer, order)) {
        memcpy(bytes, buffer->buf, buffer->len);
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        /* Don't try to make a full buffer if the only thing missing is
         * suboffsets, because suboffsets can't be constructed anyway, and it's
         * legitimate for suboffsets to be NULL if no dereferencing is needed.
         */
        if (!buffer->shape || !buffer->strides) {
            buffer = full_buffer = sky_buffer_tofull(buffer);
            sky_asset_save(full_buffer, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        /* Create a dummy target buffer with shape and stride information
         * initialized properly for the desired layout. Also, stuff the bytes
         * argument into the target buffer's buf as the memory location to
         * receive the copy that we'll do later.
         */
        target_buffer = sky_asset_malloc(sizeof(sky_buffer_t) +
                                         (buffer->ndim * sizeof(ssize_t)),
                                         SKY_ASSET_CLEANUP_ALWAYS);
        *target_buffer = *buffer;
        target_buffer->buf = bytes;
        target_buffer->suboffsets = NULL;
        target_buffer->strides = (ssize_t *)((char *)target_buffer + 
                                             sizeof(sky_buffer_t));
        if ('F' == order) {
            sky_buffer_init_strides_c(target_buffer);
        }
        else {  /* 'A', 'C' */
            sky_buffer_init_strides_f(target_buffer);
        }

        /* Copy the buffer from full_buffer to dest_buffer */
        sky_buffer_copy(target_buffer, buffer);
    } SKY_ASSET_BLOCK_END;
}


sky_buffer_t *
sky_buffer_tofull(const sky_buffer_t *buffer)
{
    char            *ptr;
    size_t          c, format_len, full_buffer_len;
    ssize_t         i;
    sky_buffer_t    *full_buffer;

    if (buffer->ndim > 1 && !buffer->shape) {
        sky_error_raise_string(sky_ValueError,
                               "buffer shape must not be NULL if ndim > 1");
    }

    /* Figure out how much memory is needed and allocate it. */
    c = (buffer->ndim > 0 ? 2 : 0) + (buffer->suboffsets ? 1 : 0);
    format_len = (buffer->format ? strlen(buffer->format) + 1 : 0);
    full_buffer_len = sizeof(sky_buffer_t) + format_len +
                             (c * buffer->ndim * sizeof(ssize_t));
    full_buffer = sky_malloc(full_buffer_len);
    *full_buffer = *buffer;

    /* Fill in shape and stride information. If ndim <= 0, the buffer is
     * contiguous, and no shape or stride information is required. If ndim is
     * 1, the buffer is contiguous, but shape and stride information is needed,
     * which is easily constructed if missing. For all other values of ndim,
     * shape information is required in the source (checked above), but the
     * strides can be constructed from the shape information.
     */
    ptr = (char *)full_buffer + sizeof(sky_buffer_t);
    if (full_buffer->ndim <= 0) {
        full_buffer->ndim = 0;
        full_buffer->shape = NULL;
        full_buffer->strides = NULL;
    }
    else {
        full_buffer->shape = (ssize_t *)ptr;
        ptr += (sizeof(ssize_t) * buffer->ndim);
        full_buffer->strides = (ssize_t *)ptr;
        ptr += (sizeof(ssize_t) * buffer->ndim);

        if (1 == full_buffer->ndim) {
            full_buffer->shape[0] =
                    (buffer->shape ? buffer->shape[0]
                                   : buffer->len / buffer->itemsize);
            full_buffer->strides[0] =
                    (buffer->strides ? buffer->strides[0]
                                     : buffer->itemsize);
        }
        else {
            for (i = 0; i < buffer->ndim; ++i) {
                full_buffer->shape[i] = buffer->shape[i];
            }
            if (!buffer->strides) {
                sky_buffer_init_strides_c(full_buffer);
            }
            else {
                for (i = 0; i < buffer->ndim; ++i) {
                    full_buffer->strides[i] = buffer->strides[i];
                }
            }
        }
    }

    /* PEP 3118 states that suboffsets should be NULL if no dereferencing is
     * required (e.g., all suboffsets are negative), so assume no dereferencing
     * is required if it's NULL; otherwise, just make a copy of the source
     * information.
     */
    if (!buffer->suboffsets) {
        full_buffer->suboffsets = NULL;
    }
    else {
        full_buffer->suboffsets = (ssize_t *)ptr;
        ptr += (sizeof(ssize_t) * buffer->ndim);

        for (i = 0; i < buffer->ndim; ++i) {
            full_buffer->suboffsets[i] = buffer->suboffsets[i];
        }
    }

    /* Stick the format string at the end of everything so that it doesn't
     * screw up alignment for the shape, strides, and suboffsets arrays.
     */
    if (buffer->format) {
        full_buffer->format = memcpy(ptr, buffer->format, format_len);
        ptr += format_len;
    }

    return full_buffer;
}
