/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_enumerate Enumerate Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ENUMERATE_H__
#define __SKYTHON_CORE_SKY_ENUMERATE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new enumerate object instance.
  *
  * @param[in]  iterable    an iterable object.
  * @param[in]  start       the starting index for the enumeration.
  * @return     a new enumerate object instance.
  */
SKY_EXTERN sky_enumerate_t
sky_enumerate_create(sky_object_t iterable, sky_integer_t start);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ENUMERATE_H__ */

/** @} **/
/** @} **/
