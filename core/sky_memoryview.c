#include "sky_private.h"


typedef enum sky_memoryview_flag_e {
    SKY_MEMORYVIEW_FLAG_C           =   0x01,   /* C-contiguous layout */
    SKY_MEMORYVIEW_FLAG_FORTRAN     =   0x02,   /* Fortran contiguous layout */
    SKY_MEMORYVIEW_FLAG_SCALAR      =   0x04,   /* scalar: ndim = 0 */
    SKY_MEMORYVIEW_FLAG_PIL         =   0x08,   /* PIL style layout */
    SKY_MEMORYVIEW_FLAG_HASHED      =   0x10,   /* cached hash is valid */
} sky_memoryview_flag_t;

#define SKY_MEMORYVIEW_CONTIGUOUS(_f)           \
        ((_f) & (SKY_MEMORYVIEW_FLAG_C |        \
                 SKY_MEMORYVIEW_FLAG_FORTRAN |  \
                 SKY_MEMORYVIEW_FLAG_SCALAR))
#define SKY_MEMORYVIEW_C_CONTIGUOUS(_f)         \
        ((_f) & (SKY_MEMORYVIEW_FLAG_C |        \
                 SKY_MEMORYVIEW_FLAG_SCALAR))
#define SKY_MEMORYVIEW_F_CONTIGUOUS(_f)         \
        ((_f) & (SKY_MEMORYVIEW_FLAG_FORTRAN |  \
                 SKY_MEMORYVIEW_FLAG_SCALAR))


typedef struct sky_memoryview_data_s {
    sky_buffer_t * volatile             buffer;
    uintptr_t                           hash;  
    int32_t                             nexports;
    volatile uint32_t                   flags;

    /* master_view will be NULL if this is a master view. If this is a subview
     * that has been released, it's not considered a subview any more, but is
     * now a master view of nothing, so master_view will also be NULL. A
     * master view that still has a buffer has master_buffer as NULL. So a
     * released subview has both master_view and master_buffer as NULL, a true
     * master has master_view as NULL, and master_buffer non-NULL, an active
     * subview has master_view non-NULL and master_buffer NULL. In any case, a
     * NULL buffer (above) means that the view has been released.
     */
    sky_memoryview_t                    master_view;
    sky_buffer_t *                      master_buffer;
    int32_t                             nsubviews;

    sky_spinlock_t                      spinlock;
} sky_memoryview_data_t;

SKY_EXTERN_INLINE sky_memoryview_data_t *
sky_memoryview_data(sky_object_t object)
{
    if (sky_memoryview_type == sky_object_type(object)) {
        return (sky_memoryview_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_memoryview_type);
}


static inline const char *
sky_memoryview_adjust_pointer(const char *      pointer,
                              const ssize_t *   suboffsets,
                              ssize_t           index)
{
    if (suboffsets && suboffsets[index] >= 0) {
        pointer = *((char **)pointer) + suboffsets[index];
    }
    return pointer;
}


static size_t
sky_memoryview_format_size(const char *format)
{
    if (!format) {
        return sizeof(unsigned char);
    }
    if ('@' == format[0]) {
        ++format;
    }
    if (!format[0]) {
        return sizeof(unsigned char);
    }
    if (format[1]) {
        switch (format[0]) {
            case 'B':
                return sizeof(unsigned char);
            case 'b':
                return sizeof(signed char);
            case 'c':
                return sizeof(char);
            case 'd':
                return sizeof(double);
            case 'f':
                return sizeof(float);
            case 'H':
                return sizeof(unsigned short);
            case 'h':
                return sizeof(signed short);
            case 'I':
                return sizeof(unsigned int);
            case 'i':
                return sizeof(signed int);
            case 'L':
                return sizeof(unsigned long);
            case 'l':
                return sizeof(signed long);
            case 'N':
                return sizeof(size_t);
            case 'n':
                return sizeof(ssize_t);
            case 'P':
                return sizeof(void *);
            case 'Q':
                return sizeof(unsigned long long);
            case 'q':
                return sizeof(signed long long);
            case '?':
                return sizeof(unsigned char);
        }
    }
    return 0;
}


static void
sky_memoryview_initbuffer(sky_memoryview_t self)
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(self);
    sky_buffer_t            *buffer = view_data->buffer;

    if (!view_data->master_view) {
        view_data->master_buffer = buffer;
    }

    if (!buffer->ndim) {
        view_data->flags |= (SKY_MEMORYVIEW_FLAG_C |
                             SKY_MEMORYVIEW_FLAG_FORTRAN |
                             SKY_MEMORYVIEW_FLAG_SCALAR);
    }
    else if (1 == buffer->ndim) {
        if (1 == buffer->shape[0] ||
            buffer->strides[0] == buffer->itemsize)
        {
            view_data->flags |= (SKY_MEMORYVIEW_FLAG_C |
                                 SKY_MEMORYVIEW_FLAG_FORTRAN);
        }
    }
    else {
        if (sky_buffer_iscontiguous(buffer, 'C')) {
            view_data->flags |= SKY_MEMORYVIEW_FLAG_C;
        }
        if (sky_buffer_iscontiguous(buffer, 'F')) {
            view_data->flags |= SKY_MEMORYVIEW_FLAG_FORTRAN;
        }
    }

    if (buffer->suboffsets) {
        view_data->flags |= SKY_MEMORYVIEW_FLAG_PIL;
        view_data->flags &= ~(SKY_MEMORYVIEW_FLAG_C |
                              SKY_MEMORYVIEW_FLAG_FORTRAN);
    }
}


static void
sky_memoryview_unlock(sky_memoryview_t view)
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(view);

    sky_spinlock_unlock(&(view_data->spinlock));
}


static inline void
sky_memoryview_lock(sky_memoryview_t view, sky_memoryview_data_t *view_data)
{
    if (!sky_object_gc_isthreaded() || !sky_object_isglobal(view)) {
        return;
    }

    sky_spinlock_lock(&(view_data->spinlock));
    sky_asset_save(view,
                   (sky_free_t)sky_memoryview_unlock,
                   SKY_ASSET_CLEANUP_ALWAYS);
}


static const char *
sky_memoryview_native_format(const char *format)
{
    if (!format) {
        return "B";
    }
    if ('@' == format[0]) {
        ++format;
    }
    if (!format[1]) {
        switch (format[0]) {
            case 'B': case 'b':
            case 'c':
            case 'd':
            case 'f':
            case 'H': case 'h':
            case 'I': case 'i':
            case 'L': case 'l':
            case 'N': case 'n':
            case 'P':
            case 'Q': case 'q':
            case '?':
                return format;
        }
    }
    return NULL;
}


static void
sky_memoryview_release_buffer(void *pointer)
{
    sky_buffer_t    *buffer = pointer;

    if (buffer->object) {
        sky_buffer_release(buffer);
    }
    sky_free(buffer);
}


static sky_tuple_t
sky_memoryview_ssize_tuple(ssize_t nvalues, ssize_t *values)
{
    ssize_t         i;
    sky_tuple_t     tuple;
    sky_object_t    *objects;

    if (!nvalues || !values) {
        return sky_tuple_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_calloc(nvalues,
                                   sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);
        for (i = 0; i < nvalues; ++i) {
            objects[i] = sky_integer_create(values[i]);
        }
        tuple = sky_tuple_createwitharray(nvalues, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}


static const char *
sky_memoryview_usable_format(sky_buffer_t *buffer)
{
    const char  *format;

    if (!buffer->format) {
        return "B";
    }
    format = buffer->format + (buffer->format[0] == '@' ? 1 : 0);
    if (format[0] && !format[1]) {
        return format;
    }
    sky_error_raise_format(sky_NotImplementedError,
                           "memoryview: unsupported format %s",
                           buffer->format);
}


#define SKY_MEMORYVIEW_BEGIN(_v)                                            \
        sky_memoryview_t    _view = (_v);                                   \
                                                                            \
        SKY_ASSET_BLOCK_BEGIN {                                             \
            sky_buffer_t            *buffer;                                \
            sky_memoryview_data_t   *view_data = sky_memoryview_data(_view);\
                                                                            \
            sky_memoryview_lock(_view, view_data);                          \
            if (!(buffer = view_data->buffer)) {                            \
                sky_error_raise_string(sky_ValueError,                      \
                                       "operation forbidden on released "   \
                                       "memoryview object");                \
            }                                                               \
            do {

#define SKY_MEMORYVIEW_END                                                  \
            } while (0);                                                    \
        } SKY_ASSET_BLOCK_END


static void
sky_memoryview_cast_to_1D(sky_memoryview_t  master_view,
                          sky_memoryview_t  subview,
                          const char *      format)
{
    sky_memoryview_data_t   *master_data = sky_memoryview_data(master_view),
                            *subview_data = sky_memoryview_data(subview);

    char        *writable_format;
    size_t      itemsize;
    const char  *master_format, *subview_format;

    master_format = sky_memoryview_native_format(master_data->buffer->format);
    if (!master_format) {
        sky_error_raise_string(
                sky_ValueError,
                "memoryview: source format must be a native single character "
                "format prefixed with an optional '@'");
    }
    if (!(subview_format = sky_memoryview_native_format(format))) {
        sky_error_raise_string(
                sky_ValueError,
                "memoryview: destination format must be a native single "
                "character format prefixed with an optional '@'");
    }
    itemsize = sky_memoryview_format_size(subview_format);

    if ((master_format[0] != 'B' && master_format[0] != 'b' &&
         master_format[0] != 'c') ||
        (subview_format[0] != 'B' && subview_format[0] != 'c' &&
         subview_format[0] != 'c'))
    {
        sky_error_raise_string(
                sky_TypeError,
                "memoryview: cannot cast between two non-byte formats");
    }

    writable_format = (char *)subview_data->buffer->format;
    writable_format[0] = subview_format[0];
    writable_format[1] = 0;

    subview_data->buffer->ndim = 1;
    subview_data->buffer->shape[0] = subview_data->buffer->len / itemsize;
    subview_data->buffer->strides[0] = itemsize;
    subview_data->buffer->suboffsets = NULL;
}


static void
sky_memoryview_cast_to_ND(sky_memoryview_t  subview,
                          sky_object_t      shape,
                          ssize_t           ndim)
{
    sky_memoryview_data_t   *subview_data = sky_memoryview_data(subview);
    sky_buffer_t            *buffer = subview_data->buffer;

    ssize_t         i, len, value;
    sky_object_t    object;

    if (!(buffer->ndim = ndim)) {
        buffer->shape = NULL;
        buffer->strides = NULL;
    }
    else {
        len = buffer->itemsize;
        for (i = 0; i < ndim; ++i) {
            object = sky_sequence_get(shape, i);
            if (!sky_object_isa(object, sky_integer_type)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "memoryview.cast(): elements of shape must be integers");
            }
            value = sky_integer_value(object, SSIZE_MIN, SSIZE_MAX, NULL);
            if (value <= 0) {
                sky_error_raise_string(
                        sky_ValueError,
                        "memoryview.cast(): elements of shape must be > 0");
            }
            if (value > SSIZE_MAX / len) {
                sky_error_raise_string(
                        sky_ValueError,
                        "memoryview.cast(): product(shape) > SSIZE_MAX");
            }
            len *= value;
            buffer->shape[i] = value;
        }

        buffer->strides[buffer->ndim - 1] = buffer->itemsize;
        for (i = buffer->ndim - 2; i >= 0; --i) {
            buffer->strides[i] = buffer->strides[i + 1] * buffer->shape[i + 1];
        }
    }
}


sky_memoryview_t
sky_memoryview_cast(sky_memoryview_t    self,
                    sky_string_t        format,
                    sky_object_t        shape)
{
    char                    *ptr;
    size_t                  c, subview_buffer_len;
    ssize_t                 i, ndim;
    const char              *format_cstring;
    sky_memoryview_t        subview;
    sky_memoryview_data_t   *subview_data;

    if (!(format_cstring = sky_string_cstring(format))) {
        sky_error_raise_format(sky_ValueError,
                               "memoryview: invalid format %#@",
                               format);
    }

    if (sky_object_isnull(shape)) {
        ndim = 1;
    }
    else {
        if (!sky_object_isa(shape, sky_tuple_type) &&
            !sky_object_isa(shape, sky_list_type))
        {
            sky_error_raise_string(
                    sky_TypeError,
                    "memoryview: shape must be a list or a tuple");
        }
        if ((ndim = sky_object_len(shape)) > SKY_BUFFER_MAX_NDIM) {
            sky_error_raise_format(
                    sky_ValueError,
                    "memoryview: number of dimensions must not exceed %d",
                    SKY_BUFFER_MAX_NDIM);
        }
    }

    SKY_MEMORYVIEW_BEGIN(self) {
        if (!SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "memoryview: casts are restricted to C-contiguous views");
        }
        for (i = 0; i < buffer->ndim; ++i) {
            if (!buffer->shape[i]) {
                sky_error_raise_string(
                        sky_TypeError,
                        "memoryview: cannot cast view with zeros in shape");
            }
        }
        if (!sky_object_isnull(shape) &&
            buffer->ndim != 1 && ndim != 1)
        {
            sky_error_raise_string(
                    sky_TypeError,
                    "memoryview: cast must be 1D -> ND or ND -> 1D");
        }

        subview = sky_object_allocate(sky_memoryview_type);
        subview_data = sky_memoryview_data(subview);

        /* Allocate a buffer. Too much memory may be allocated here, but
         * there's no easy way to avoid that.
         */
        c = (ndim >= 0 ? 2 : 0) + (buffer->suboffsets ? 1 : 0);
        subview_buffer_len = sizeof(sky_buffer_t) + 2 +
                             (c * (ndim ? ndim : 1) * sizeof(ssize_t));
        subview_data->buffer = sky_malloc(subview_buffer_len);
        *subview_data->buffer = *buffer;

        ptr = (char *)subview_data->buffer + sizeof(sky_buffer_t);
        subview_data->buffer->shape = (ssize_t *)ptr;
        ptr += (sizeof(ssize_t) * (ndim ? ndim : 1));
        subview_data->buffer->strides = (ssize_t *)ptr;
        ptr += (sizeof(ssize_t) * (ndim ? ndim : 1));
        if (buffer->suboffsets) {
            subview_data->buffer->suboffsets = (ssize_t *)ptr;
            ptr += (sizeof(ssize_t) * (ndim ? ndim : 1));
        }
        subview_data->buffer->format = ptr;
        ptr += 2;

        /* At this point, format, shapes, strides, and suboffsets have memory
         * allocated for them, but they're uninitialized. Casting here will
         * fill those in as appropriate.
         */
        sky_memoryview_cast_to_1D(self, subview, format_cstring);
        if (!sky_object_isnull(shape)) {
            sky_memoryview_cast_to_ND(subview, shape, ndim);
        }

        subview_data->master_view = self;
        sky_memoryview_initbuffer(subview);
        ++view_data->nsubviews;
    } SKY_MEMORYVIEW_END;

    return subview;
}

static const char sky_memoryview_cast_doc[] =
"M.cast(format[, shape]) -> memoryview\n\n"
"Cast a memoryview to a new format or shape.";


static sky_object_t
sky_memoryview_contiguous_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_boolean_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (SKY_MEMORYVIEW_CONTIGUOUS(view_data->flags) ? sky_True
                                                              : sky_False);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_c_contiguous_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_boolean_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags) ? sky_True
                                                                : sky_False);
    } SKY_MEMORYVIEW_END;

    return result;
}


sky_object_t
sky_memoryview_enter(sky_memoryview_t self)
{
    SKY_MEMORYVIEW_BEGIN(self) {
    } SKY_MEMORYVIEW_END;

    return self;
}


sky_object_t
sky_memoryview_eq(sky_memoryview_t self, sky_object_t other)
{
    sky_object_t    result = NULL;
    sky_buffer_t    *self_buffer, *other_buffer;
    sky_buffer_t    dummy_buffer;

    if (self == other) {
        return sky_True;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_memoryview_data_t   *self_data = sky_memoryview_data(self);

        sky_memoryview_lock(self, self_data);
        if (!(self_buffer = self_data->buffer)) {
            result = (self == other ? sky_True : sky_False);
        }
        else {
            if (sky_object_isa(other, sky_memoryview_type)) {
                sky_memoryview_data_t   *other_data = sky_memoryview_data(other);

                sky_memoryview_lock(other, other_data);
                if (!(other_buffer = other_data->buffer)) {
                    result = (self == other ? sky_True : sky_False);
                }
            }
            else if (!sky_buffer_check(other)) {
                result = sky_NotImplemented;
            }
            else {
                other_buffer = &dummy_buffer;
                sky_buffer_acquire(&dummy_buffer,
                                   other,
                                   SKY_BUFFER_FLAG_FULL_RO);
                sky_asset_save(&dummy_buffer,
                               (sky_free_t)sky_buffer_release,
                               SKY_ASSET_CLEANUP_ALWAYS);
            }
        }
        if (!result) {
            result = (sky_buffer_equal(self_buffer, other_buffer) ? sky_True
                                                                  : sky_False);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


sky_bool_t
sky_memoryview_exit(sky_object_t        self,
         SKY_UNUSED sky_type_t          exc_type,
         SKY_UNUSED sky_object_t        exc_value,
         SKY_UNUSED sky_object_t        exc_traceback)
{
    sky_memoryview_release(self);
    return SKY_FALSE;
}


static sky_object_t
sky_memoryview_format_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_string_t    result;

    SKY_MEMORYVIEW_BEGIN(self) {
        if (!buffer->format) {
            result = SKY_STRING_LITERAL("B");
        }
        else {
            result = sky_string_createfrombytes(buffer->format,
                                                strlen(buffer->format),
                                                NULL,
                                                NULL);
        }
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_f_contiguous_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_boolean_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (SKY_MEMORYVIEW_F_CONTIGUOUS(view_data->flags) ? sky_True
                                                                : sky_False);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_index(sky_memoryview_t self, ssize_t index)
    /* NOTE self is assumed to be locked on entry */
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(self);

    char        *pointer;
    const char  *format;

    format = sky_memoryview_usable_format(view_data->buffer);

    if (1 == view_data->buffer->ndim) {
        pointer = sky_buffer_pointer(view_data->buffer, index);
        return sky_data_type_unpack_native(pointer, format);
    }

    sky_error_raise_string(sky_NotImplementedError,
                           "multi-dimensional sub-views are not implemented");
}


static sky_object_t
sky_memoryview_slice(sky_memoryview_t self, sky_slice_t slice)
    /* NOTE self is assumed to be locked on entry */
{
    sky_memoryview_data_t   *master_data = sky_memoryview_data(self);

    ssize_t                 length, start, step, stop;
    sky_memoryview_t        view;
    sky_memoryview_data_t   *view_data;

    length = sky_slice_range(slice,
                             master_data->buffer->shape[0],
                             &start,
                             &stop,
                             &step);

    view = sky_object_allocate(sky_memoryview_type);
    view_data = sky_memoryview_data(view);
    view_data->buffer = sky_buffer_tofull(master_data->buffer);
    view_data->buffer->buf = (char *)view_data->buffer->buf +
                             view_data->buffer->strides[0] * start;
    view_data->buffer->len = length;
    view_data->buffer->shape[0] = length;
    view_data->buffer->strides[0] = view_data->buffer->strides[0] * step;
    view_data->master_view = self;
    sky_memoryview_initbuffer(view);
    ++master_data->nsubviews;

    return view;
}


sky_object_t
sky_memoryview_getitem(sky_memoryview_t self, sky_object_t item)
{
    ssize_t         index;
    const char      *format;
    sky_bool_t      multislice;
    sky_object_t    element, value;

    SKY_MEMORYVIEW_BEGIN(self) {
        value = NULL;
        if (buffer->ndim <= 0) {
            if (sky_tuple_empty == item) {
                format = sky_memoryview_usable_format(buffer);
                value = sky_data_type_unpack_native(buffer->buf, format);
            }
            else if (sky_Ellipsis == item) {
                value = self;
            }
            else {
                sky_error_raise_string(sky_TypeError,
                                       "invalid indexing of 0-dim memory");
            }
        }
        else if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            index = sky_integer_value(sky_number_index(item),
                                      SSIZE_MIN,
                                      SSIZE_MAX,
                                      NULL);
            value = sky_memoryview_index(self, index);
        }
        else if (sky_object_isa(item, sky_slice_type)) {
            value = sky_memoryview_slice(self, item);
        }
        else if (sky_object_isa(item, sky_tuple_type) && sky_tuple_len(item)) {
            multislice = SKY_TRUE;
            SKY_SEQUENCE_FOREACH(item, element) {
                if (!sky_object_isa(element, sky_slice_type)) {
                    multislice = SKY_FALSE;
                    SKY_SEQUENCE_FOREACH_BREAK;
                }
            } SKY_SEQUENCE_FOREACH_END;
            if (multislice) {
                sky_error_raise_string(
                        sky_NotImplementedError,
                        "multi-dimensional slicing is not implemented");
            }
        }
        if (!value) {
            sky_error_raise_string(sky_TypeError,
                                   "memoryview: invalid slice key");
        }
    } SKY_MEMORYVIEW_END;

    return value;
}


static uintptr_t
sky_memoryview_hash(sky_memoryview_t self)
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(self);

    void        *bytes;
    const char  *format;

    if (view_data->flags & SKY_MEMORYVIEW_FLAG_HASHED) {
        return view_data->hash;
    }

    SKY_MEMORYVIEW_BEGIN(self) {
        if (!buffer->readonly) {
            sky_error_raise_string(
                    sky_ValueError,
                    "cannot hash writable memoryview object");
        }

        format = sky_memoryview_native_format(buffer->format);
        if (!format || (*format != 'B' && *format != 'b' && *format != 'c')) {
            sky_error_raise_string(
                    sky_ValueError,
                    "memoryview: hashing is restricted to formats "
                    "'B', 'b' or 'c'");
        }

        /* If the original object is not hashable, its memoryview shouldn't
         * be either.
         */
        if (buffer->object) {
            sky_object_hash(buffer->object);
        }

        if (SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags)) {
            bytes = buffer->buf;
        }
        else {
            bytes = sky_asset_malloc(buffer->len, SKY_ASSET_CLEANUP_ALWAYS);
            sky_buffer_tocontiguous(buffer, bytes, buffer->len, 'C');
        }

        view_data->hash = sky_util_hashbytes(bytes, buffer->len, 0);
        sky_atomic_or32_old(SKY_MEMORYVIEW_FLAG_HASHED, &(view_data->flags));
    } SKY_MEMORYVIEW_END;

    return view_data->hash;
}


static void
sky_memoryview_initwithobject(sky_memoryview_t  self,
                              sky_object_t      object)
{
    sky_memoryview_data_t   *self_data = sky_memoryview_data(self);

    if (sky_object_isa(object, sky_memoryview_type)) {
        SKY_MEMORYVIEW_BEGIN(object) {
            self_data->buffer = sky_buffer_tofull(view_data->buffer);
            self_data->buffer->buf = (char *)self_data->buffer->buf;
            self_data->buffer->len = view_data->buffer->shape[0];
            self_data->buffer->shape[0] = view_data->buffer->shape[0];
            self_data->buffer->strides[0] = self_data->buffer->strides[0];
            self_data->master_view = object;
            sky_memoryview_initbuffer(self);
            ++view_data->nsubviews;
        } SKY_MEMORYVIEW_END;
    }
    else {
        self_data->buffer = sky_calloc(1, sizeof(sky_buffer_t));
        sky_buffer_acquire(self_data->buffer, object, SKY_BUFFER_FLAG_FULL_RO);
        sky_memoryview_initbuffer(self);
    }
}


static sky_object_t
sky_memoryview_itemsize_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_integer_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_integer_create(buffer->itemsize);
    } SKY_MEMORYVIEW_END;

    return result;
}


ssize_t
sky_memoryview_len(sky_memoryview_t self)
{
    ssize_t result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (buffer->ndim ? buffer->shape[0] : 1);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_nbytes_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_integer_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_integer_create(buffer->len);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_ndim_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_integer_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_integer_create(buffer->ndim);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_new(sky_type_t cls, sky_object_t object)
{
    sky_memoryview_t    self;

    self = sky_object_allocate(cls);
    sky_memoryview_initwithobject(self, object);

    return self;
}


static sky_object_t
sky_memoryview_obj_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_object_t    result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (buffer->object ? buffer->object : sky_None);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_readonly_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_boolean_t   result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = (buffer->readonly ? sky_True : sky_False);
    } SKY_MEMORYVIEW_END;

    return result;
}


sky_string_t
sky_memoryview_repr(sky_memoryview_t self)
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(self);

    if (!view_data->buffer) {
        return sky_string_createfromformat("<released memory at %p>", self);
    }
    return sky_string_createfromformat("<memory at %p>", self);
}


void
sky_memoryview_release(sky_memoryview_t self)
{
    sky_memoryview_data_t   *view_data = sky_memoryview_data(self);

    sky_buffer_t            *buffer;
    sky_memoryview_t        master_view;
    sky_memoryview_data_t   *master_data;

    SKY_ASSET_BLOCK_BEGIN {
        sky_memoryview_lock(self, view_data);

        if (view_data->nexports) {
            sky_error_validate(view_data->nexports > 0);
            sky_error_raise_format(sky_BufferError,
                                   "memoryview has %zd exported buffer%s",
                                   view_data->nexports,
                                   (view_data->nexports == 1 ? "" : "s"));
        }

        if ((buffer = view_data->buffer) != NULL) {
            view_data->buffer = NULL;
            if (!view_data->master_view && !view_data->nsubviews) {
                view_data->master_buffer = NULL;
                sky_hazard_pointer_retire(buffer,
                                          sky_memoryview_release_buffer);
            }
        }
    } SKY_ASSET_BLOCK_END;

    if ((master_view = view_data->master_view) != NULL &&
        sky_atomic_cas(master_view,
                       NULL,
                       SKY_AS_VOIDP(&(view_data->master_view))))
    {
        SKY_ASSET_BLOCK_BEGIN {
            master_data = sky_memoryview_data(master_view);
            sky_memoryview_lock(master_view, master_data);
            if (!--master_data->nsubviews &&
                !master_data->buffer &&
                (buffer = master_data->master_buffer) != NULL)
            {
                master_data->master_buffer = NULL;
                sky_hazard_pointer_retire(buffer,
                                          sky_memoryview_release_buffer);
            }
        } SKY_ASSET_BLOCK_END;
    }
}

static const char sky_memoryview_release_doc[] =
"M.release() -> None\n\n"
"Release the underlying buffer exposed by the memoryview object.";


void
sky_memoryview_setitem(sky_memoryview_t self,
                       sky_object_t     item,
                       sky_object_t     value)
{
    void            *pointer;
    ssize_t         index, length, shape, start, step, stop, stride, suboffset;
    const char      *format;
    sky_buffer_t    source_buffer, target_buffer;

    SKY_MEMORYVIEW_BEGIN(self) {
        format = sky_memoryview_usable_format(buffer);
        if (buffer->readonly) {
            sky_error_raise_string(sky_TypeError,
                                   "cannot modify read-only memory");
        }
        if (!buffer->ndim) {
            if (sky_Ellipsis == item || sky_tuple_empty == item) {
                sky_data_type_pack_native(buffer->buf, format, value);
                return;
            }
            sky_error_raise_string(sky_TypeError,
                                   "invalid indexing of 0-dim memory");
        }
        if (buffer->ndim != 1) {
            sky_error_raise_string(
                    sky_NotImplementedError,
                    "memoryview assignments are currently restricted to ndim = 1");
        }
        if (SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
            index = sky_integer_value(sky_number_index(item),
                                      SSIZE_MIN,
                                      SSIZE_MAX,
                                      NULL);
            pointer = sky_buffer_pointer(buffer, index);
            sky_data_type_pack_native(pointer, format, value);
            return;
        }
        if (!sky_object_isa(item, sky_slice_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "memoryview: invalid slice key");
        }

        sky_buffer_acquire(&source_buffer, value, SKY_BUFFER_FLAG_FULL_RO);
        sky_asset_save(&source_buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        target_buffer = *buffer;
        target_buffer.shape = &shape;
        target_buffer.strides = &stride;
        if (target_buffer.suboffsets) {
            target_buffer.suboffsets = &suboffset;
            target_buffer.suboffsets[0] = buffer->suboffsets[0];
        }

        length = sky_slice_range(item,
                                 buffer->shape[0],
                                 &start,
                                 &stop,
                                 &step);
        target_buffer.buf = (char *)buffer->buf +
                            buffer->strides[0] * start;
        target_buffer.len = length * target_buffer.itemsize;
        target_buffer.shape[0] = length;
        target_buffer.strides[0] = buffer->strides[0] * step;

        sky_buffer_copy(&target_buffer, &source_buffer);
    } SKY_MEMORYVIEW_END;
}


static sky_object_t
sky_memoryview_shape_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_tuple_t     result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_memoryview_ssize_tuple(buffer->ndim, buffer->shape);
    } SKY_MEMORYVIEW_END;

    return result;
}


size_t
sky_memoryview_sizeof(sky_memoryview_t self)
{
    size_t  result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_memsize(self) + sky_memsize(buffer);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_strides_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_tuple_t result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_memoryview_ssize_tuple(buffer->ndim, buffer->strides);
    } SKY_MEMORYVIEW_END;

    return result;
}


static sky_object_t
sky_memoryview_suboffsets_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_tuple_t result;

    SKY_MEMORYVIEW_BEGIN(self) {
        result = sky_memoryview_ssize_tuple(buffer->ndim, buffer->suboffsets);
    } SKY_MEMORYVIEW_END;

    return result;
}


sky_bytes_t
sky_memoryview_tobytes(sky_memoryview_t self)
{
    void        *memory;
    sky_bytes_t result;

    SKY_MEMORYVIEW_BEGIN(self) {
        if (SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags)) {
            result = sky_bytes_createfrombytes(buffer->buf, buffer->len);
        }
        else {
            memory = sky_asset_malloc(buffer->len, SKY_ASSET_CLEANUP_ON_ERROR);
            sky_buffer_tocontiguous(buffer, memory, buffer->len, 'C');
            result = sky_bytes_createwithbytes(memory, buffer->len, sky_free);
        }
    } SKY_MEMORYVIEW_END;

    return result;
}

static const char sky_memoryview_tobytes_doc[] =
"M.tobytes() -> bytes\n\n"
"Return the data in the buffer as a byte string.";


static sky_object_t
sky_memoryview_tolist_base(const char *     ptr,
                           const ssize_t *  shape,
                           const ssize_t *  strides,
                           const ssize_t *  suboffsets,
                           const char *     format)
{
    ssize_t         i;
    const char      *adjusted_ptr;
    sky_list_t      list;
    sky_object_t    item;

    list = sky_list_createwithcapacity(shape[0]);
    for (i = 0; i < shape[0]; ptr += strides[0], ++i) {
        adjusted_ptr = sky_memoryview_adjust_pointer(ptr, suboffsets, 0);
        item = sky_data_type_unpack_native(adjusted_ptr, format);
        sky_list_append(list, item);
    }

    return list;
}


static sky_object_t
sky_memoryview_tolist_records(const char *      ptr,
                              ssize_t           ndim,
                              const ssize_t *   shape,
                              const ssize_t *   strides,
                              const ssize_t *   suboffsets,
                              const char *      format)
{
    ssize_t         i;
    const char      *adjusted_pointer;
    sky_list_t      list;
    sky_object_t    item;

    if (1 == ndim) {
        return sky_memoryview_tolist_base(ptr,
                                          shape,
                                          strides,
                                          suboffsets,
                                          format);
    }

    list = sky_list_createwithcapacity(shape[0]);
    for (i = 0; i < shape[0]; ptr += strides[0], ++i) {
        adjusted_pointer = sky_memoryview_adjust_pointer(ptr, suboffsets, 0);
        item = sky_memoryview_tolist_records(
                        adjusted_pointer,
                        ndim - 1,
                        shape + 1,
                        strides + 1,
                        (suboffsets ? suboffsets + 1 : NULL),
                        format);
        sky_list_append(list, item);
    }

    return list;
}


sky_object_t
sky_memoryview_tolist(sky_memoryview_t self)
{
    const char      *format;
    sky_object_t    result;

    SKY_MEMORYVIEW_BEGIN(self) {
        format = sky_memoryview_usable_format(buffer);
        if (!buffer->ndim) {
            result = sky_data_type_unpack_native(buffer, format);
        }
        else {
            result = sky_memoryview_tolist_records(buffer->buf,
                                                   buffer->ndim,
                                                   buffer->shape,
                                                   buffer->strides,
                                                   buffer->suboffsets,
                                                   format);
        }
    } SKY_MEMORYVIEW_END;

    return result;
}

static const char sky_memoryview_tolist_doc[] =
"M.tolist() -> list\n\n"
"Return the data in the buffer as a list of elements.";


sky_memoryview_t
sky_memoryview_createwithbuffer(sky_buffer_t *buffer)
{
    sky_memoryview_t        view;
    sky_memoryview_data_t   *view_data;

    view = sky_object_allocate(sky_memoryview_type);
    view_data = sky_memoryview_data(view);
    view_data->buffer = buffer;
    sky_memoryview_initbuffer(view);

    return view;
}


sky_memoryview_t
sky_memoryview_createwithobject(sky_object_t object)
{
    sky_memoryview_t    view;

    view = sky_object_allocate(sky_memoryview_type);
    sky_memoryview_initwithobject(view, object);

    return view;
}


static void
sky_memoryview_instance_initialize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_memoryview_data_t   *view_data = data;

    sky_spinlock_init(&(view_data->spinlock));
}


static void
sky_memoryview_instance_finalize(SKY_UNUSED sky_object_t object, void *data)
{
    sky_memoryview_data_t   *view_data = data;

    sky_memoryview_data_t   *master_data;

    sky_error_validate_debug(!view_data->nexports);
    sky_error_validate_debug(!view_data->nsubviews);

    if (view_data->master_view) {
        master_data = sky_memoryview_data(view_data->master_view);
        if (!--master_data->nsubviews && !master_data->buffer) {
            sky_memoryview_release_buffer(master_data->master_buffer);
        }
    }
    else if (view_data->master_buffer) {
        sky_memoryview_release_buffer(view_data->master_buffer);
    }
    else if (view_data->buffer) {
        sky_memoryview_release_buffer(view_data->buffer);
    }
}


static void
sky_memoryview_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_memoryview_data_t   *view_data = data;

    sky_buffer_t            *buffer;
    sky_hazard_pointer_t    *hazard;

    SKY_ASSET_BLOCK_BEGIN {
        hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(view_data->buffer)));
        sky_asset_save(hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if ((buffer = hazard->pointer) != NULL) {
            sky_object_visit(buffer->object, visit_data);
        }
        sky_object_visit(view_data->master_view, visit_data);
        sky_hazard_pointer_update(hazard,
                                  SKY_AS_VOIDP(&(view_data->master_buffer)));
        if ((buffer = hazard->pointer) != NULL) {
            sky_object_visit(buffer->object, visit_data);
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_memoryview_instance_buffer_acquire(sky_object_t     object,
                                       void *           data,
                                       sky_buffer_t *   buffer,
                                       unsigned int     flags)
{
    sky_memoryview_data_t   *view_data = data;

    *buffer = *view_data->buffer;
    buffer->object = NULL;

    SKY_ASSET_BLOCK_BEGIN {
        sky_memoryview_lock(object, view_data);
        if (!view_data->buffer) {
            sky_error_raise_string(
                    sky_ValueError,
                    "operation forbidden on released memoryview object");
        }

        /* Make sure the requested flags match the flags for our buffer */
        if ((flags & SKY_BUFFER_FLAG_WRITABLE) &&
            view_data->buffer->readonly)
        {
            sky_error_raise_string(
                    sky_BufferError,
                    "memoryview: underlying buffer is not writable");
        }
        if (!(flags & SKY_BUFFER_FLAG_FORMAT)) {
            buffer->format = NULL;
        }
        if ((flags & SKY_BUFFER_FLAG_C_CONTIGUOUS) &&
            !SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags))
        {
            sky_error_raise_string(
                    sky_BufferError,
                    "memoryview: underlying buffer is not C-contiguous");
        }
        if ((flags & SKY_BUFFER_FLAG_F_CONTIGUOUS) &&
            !SKY_MEMORYVIEW_F_CONTIGUOUS(view_data->flags))
        {
            sky_error_raise_string(
                    sky_BufferError,
                    "memoryview: underlying buffer is not Fortran contiguous");
        }
        if ((flags & SKY_BUFFER_FLAG_ANY_CONTIGUOUS) &&
            !SKY_MEMORYVIEW_CONTIGUOUS(view_data->flags))
        {
            sky_error_raise_string(
                    sky_BufferError,
                    "memoryview: underlying buffer is not contiguous");
        }
        if (!(flags & SKY_BUFFER_FLAG_INDIRECT) &&
            (view_data->flags & SKY_MEMORYVIEW_FLAG_PIL))
        {
            sky_error_raise_string(
                    sky_BufferError,
                    "memoryview: underlying buffer requires suboffsets");
        }
        if (!(flags & SKY_BUFFER_FLAG_STRIDES)) {
            if (!SKY_MEMORYVIEW_C_CONTIGUOUS(view_data->flags)) {
                sky_error_raise_string(
                        sky_BufferError,
                        "memoryview: underlying buffer is not C-contiguous");
            }
            buffer->strides = NULL;
        }
        if (!(flags & SKY_BUFFER_FLAG_ND)) {
            if (buffer->format) {
                sky_error_raise_string(
                        sky_BufferError,
                        "memoryview: cannot cast to unsigned bytes if the format "
                        "flag is present");
            }
            buffer->ndim = 1;
            buffer->shape = NULL;
        }

        ++view_data->nexports;
    } SKY_ASSET_BLOCK_END;
}


static void
sky_memoryview_instance_buffer_release(           sky_object_t      object,
                                                  void *            data,
                                       SKY_UNUSED sky_buffer_t *    buffer)
{
    sky_memoryview_data_t   *view_data = data;

    SKY_ASSET_BLOCK_BEGIN {
        sky_memoryview_lock(object, view_data);
        --view_data->nexports;
    } SKY_ASSET_BLOCK_END;
}


static const char sky_memoryview_doc[] =
"memoryview(object)\n"
"\n"
"Create a new memoryview object which references the given object.";

SKY_TYPE_DEFINE(memoryview,
                "memoryview",
                sizeof(sky_memoryview_data_t),
                sky_memoryview_instance_initialize,
                sky_memoryview_instance_finalize,
                sky_memoryview_instance_visit,
                NULL,
                NULL,
                sky_memoryview_instance_buffer_acquire,
                sky_memoryview_instance_buffer_release,
                SKY_TYPE_FLAG_FINAL,
                sky_memoryview_doc);


void
sky_memoryview_initialize_library(void)
{
    sky_type_initialize_builtin(sky_memoryview_type, 0);

    sky_type_setgetsets(sky_memoryview_type,
            "obj",
                    "The underlying object of the memoryview.",
                    sky_memoryview_obj_getter,
                    NULL,
            "nbytes",
                    "The amount of space in bytes that the array would use in\n"
                    " a contiguous representation.",
                    sky_memoryview_nbytes_getter,
                    NULL,
            "readonly",
                    "A bool indicating whether the memory is read only.",
                    sky_memoryview_readonly_getter,
                    NULL,
            "itemsize",
                    "The size in bytes of each element of the memoryview.",
                    sky_memoryview_itemsize_getter,
                    NULL,
            "format",
                    "A string containing the format (in struct module style)\n"
                    " for each element in the view.",
                    sky_memoryview_format_getter,
                    NULL,
            "ndim",
                    "An integer indicating how many dimensions of a multi-dimensional\n"
                    " array the memory represents.",
                    sky_memoryview_ndim_getter,
                    NULL,
            "shape",
                    "A tuple of ndim integers giving the shape of the memory\n"
                    " as an N-dimensional array.",
                    sky_memoryview_shape_getter,
                    NULL,
            "strides",
                    "A tuple of ndim integers giving the size in bytes to access\n"
                    " each element for each dimension of the array.",
                    sky_memoryview_strides_getter,
                    NULL,
            "suboffsets",
                    "A tuple of integers used internally for PIL-style arrays.",
                    sky_memoryview_suboffsets_getter,
                    NULL,
            "c_contiguous",
                    "A bool indicating whether the memory is C contiguous.",
                    sky_memoryview_c_contiguous_getter,
                    NULL,
            "f_contiguous",
                    "A bool indicating whether the memory is Fortran contiguous.",
                    sky_memoryview_f_contiguous_getter,
                    NULL,
            "contiguous",
                    "A bool indicating whether the memory is contiguous.",
                    sky_memoryview_contiguous_getter,
                    NULL,
            NULL);

    sky_type_setattr_builtin(
            sky_memoryview_type,
            "release",
            sky_function_createbuiltin(
                    "memoryview.release",
                    sky_memoryview_release_doc,
                    (sky_native_code_function_t)sky_memoryview_release,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_memoryview_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_memoryview_type,
            "tobytes",
            sky_function_createbuiltin(
                    "memoryview.tobytes",
                    sky_memoryview_tobytes_doc,
                    (sky_native_code_function_t)sky_memoryview_tobytes,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_memoryview_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_memoryview_type,
            "tolist",
            sky_function_createbuiltin(
                    "memoryview.tolist",
                    sky_memoryview_tolist_doc,
                    (sky_native_code_function_t)sky_memoryview_tolist,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_memoryview_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_memoryview_type,
            "cast",
            sky_function_createbuiltin(
                    "memoryview.cast",
                    sky_memoryview_cast_doc,
                    (sky_native_code_function_t)sky_memoryview_cast,
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF, sky_memoryview_type,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_memoryview_type,
                    "format", SKY_DATA_TYPE_OBJECT_STRING, NULL,
                    "shape", SKY_DATA_TYPE_OBJECT, sky_None,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_memoryview_type,
            "__new__",
            (sky_native_code_function_t)sky_memoryview_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "object", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_memoryview_type,
            "__repr__", sky_memoryview_repr,
            "__eq__", sky_memoryview_eq,
            "__hash__", sky_memoryview_hash,
            "__sizeof__", sky_memoryview_sizeof,
            "__len__", sky_memoryview_len,
            "__getitem__", sky_memoryview_getitem,
            "__setitem__", sky_memoryview_setitem,
            "__enter__", sky_memoryview_enter,
            "__exit__", sky_memoryview_exit,
            NULL);
}
