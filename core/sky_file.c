#include "sky_private.h"

#include <langinfo.h>


typedef enum sky_file_method_e {
    SKY_FILE_METHOD_CLOSE       =   0,
    SKY_FILE_METHOD_CLOSED      =   1,
    SKY_FILE_METHOD_FILENO      =   2,
    SKY_FILE_METHOD_FLUSH       =   3,
    SKY_FILE_METHOD_ISATTY      =   4,
    SKY_FILE_METHOD_OPEN        =   5,
    SKY_FILE_METHOD_READ        =   6,
    SKY_FILE_METHOD_READABLE    =   7,
    SKY_FILE_METHOD_READALL     =   8,
    SKY_FILE_METHOD_READINTO    =   9,
    SKY_FILE_METHOD_READLINE    =   10,
    SKY_FILE_METHOD_READLINES   =   11,
    SKY_FILE_METHOD_SEEK        =   12,
    SKY_FILE_METHOD_SEEKABLE    =   13,
    SKY_FILE_METHOD_TELL        =   14,
    SKY_FILE_METHOD_TRUNCATE    =   15,
    SKY_FILE_METHOD_WRITABLE    =   16,
    SKY_FILE_METHOD_WRITE       =   17,
    SKY_FILE_METHOD_WRITELINES  =   18,
} sky_file_method_t;

typedef struct sky_file_method_name_s {
    const char *                        literal;
    sky_string_t                        string;
} sky_file_method_name_t;

static sky_file_method_name_t sky_file_methods[] = {
    { "close", NULL },
    { "closed", NULL },
    { "fileno", NULL },
    { "flush", NULL },
    { "isatty", NULL },
    { "open", NULL },
    { "read", NULL },
    { "readable", NULL },
    { "readall", NULL },
    { "readinto", NULL },
    { "readline", NULL },
    { "readlines", NULL },
    { "seek", NULL },
    { "seekable", NULL },
    { "tell", NULL },
    { "truncate", NULL },
    { "writable", NULL },
    { "write", NULL },
    { "writelines", NULL },
};

#define SKY_FILE_METHOD(m)  sky_file_methods[SKY_FILE_METHOD_##m].string


void
sky_file_close(sky_object_t file)
{
    sky_object_callmethod(file, SKY_FILE_METHOD(CLOSE), NULL, NULL);
}


sky_bool_t
sky_file_closed(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_getattr(file, SKY_FILE_METHOD(CLOSED), sky_NotSpecified);
    return sky_object_bool(o);
}


sky_string_t
sky_file_device_encoding(int fileno)
{
    char    *codeset;

    if (isatty(fileno) && (codeset = nl_langinfo(CODESET)) != NULL) {
        return sky_string_createfrombytes(codeset,
                                          strlen(codeset),
                                          NULL,
                                          NULL);
    }
    return (sky_string_t)sky_None;
}


int
sky_file_fileno(sky_object_t file)
{
    int             fd;
    sky_object_t    method, o;

    if (sky_object_isa(file, sky_integer_type)) {
        fd = sky_integer_value(file, INT_MIN, INT_MAX, NULL);
    }
    else if (!(method = sky_object_getattr(file,
                                           SKY_FILE_METHOD(FILENO),
                                           NULL)))
    {
        sky_error_raise_string(
                sky_TypeError,
                "argument must be an int, or have a fileno() method.");
    }
    else {
        o = sky_object_call(method, NULL, NULL);
        if (!sky_object_isa(o, sky_integer_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "fileno() returned a non-integer");
        }
        fd = sky_integer_value(o, INT_MIN, INT_MAX, NULL);
    }

    if (fd < 0) {
        sky_error_raise_format(
                sky_ValueError,
                "file descriptor cannot be a negative integer (%d)",
                fd);
    }
    return fd;
}


void
sky_file_flush(sky_object_t file)
{
    sky_object_callmethod(file, SKY_FILE_METHOD(FLUSH), NULL, NULL);
}


sky_bool_t
sky_file_isatty(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_callmethod(file, SKY_FILE_METHOD(ISATTY), NULL, NULL);
    return sky_object_bool(o);
}


sky_object_t
sky_file_open(sky_string_t  filename,
              sky_string_t  mode,
              ssize_t       buffering,
              sky_string_t  encoding,
              sky_string_t  errors,
              sky_string_t  newline,
              sky_bool_t    closefd,
              sky_object_t  opener)
{
    sky_tuple_t args;

    args = sky_object_build("(OOizOOOBO)",
                            filename,
                            (mode ? mode : SKY_STRING_LITERAL("r")),
                            buffering,
                            (encoding ? encoding : (sky_string_t)sky_None),
                            (errors ? errors : (sky_string_t)sky_None),
                            (newline ? newline : (sky_string_t)sky_None),
                            closefd,
                            opener);
    return sky_object_callmethod(sky_interpreter_module_builtins(),
                                 SKY_FILE_METHOD(OPEN),
                                 args,
                                 NULL);
}


sky_object_t
sky_file_read(sky_object_t file, ssize_t n)
{
    return sky_object_callmethod(file,
                                 SKY_FILE_METHOD(READ),
                                 sky_object_build("(iz)", n),
                                 NULL);
}


sky_bool_t
sky_file_readable(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_callmethod(file, SKY_FILE_METHOD(READABLE), NULL, NULL);
    return sky_object_bool(o);
}


sky_object_t
sky_file_readall(sky_object_t file)
{
    return sky_object_callmethod(file, SKY_FILE_METHOD(READALL), NULL, NULL);
}


ssize_t
sky_file_readinto(sky_object_t file, sky_object_t b)
{
    sky_object_t    o;

    o = sky_object_callmethod(file,
                              SKY_FILE_METHOD(READINTO),
                              sky_tuple_pack(1, b),
                              NULL);
    if (sky_object_isnull(o)) {
        return -1;
    }
    return sky_integer_value(o, SSIZE_MIN, SSIZE_MAX, NULL);
}


sky_object_t
sky_file_readline(sky_object_t file, ssize_t limit)
{
    sky_tuple_t args;

    args = (limit >= 0 ? sky_object_build("(iz)", limit) : NULL);
    return sky_object_callmethod(file,
                                 SKY_FILE_METHOD(READLINE),
                                 args,
                                 NULL);
}


sky_list_t
sky_file_readlines(sky_object_t file, ssize_t hint)
{
    sky_object_t    o;

    o = sky_object_callmethod(file,
                              SKY_FILE_METHOD(READLINES),
                              sky_object_build("(iz)", hint),
                              NULL);
    if (!sky_object_isa(o, sky_list_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "readlines() returned %#@; expected 'list'",
                sky_type_name(sky_object_type(o)));
    }

    return o;
}


ssize_t
sky_file_seek(sky_object_t file, ssize_t pos, int whence)
{
    sky_object_t    o;

    o = sky_object_callmethod(file,
                              SKY_FILE_METHOD(SEEK),
                              sky_object_build("(izi)", pos, whence),
                              NULL);
    return sky_integer_value(o, SSIZE_MIN, SSIZE_MAX, NULL);
}


sky_bool_t
sky_file_seekable(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_callmethod(file, SKY_FILE_METHOD(SEEKABLE), NULL, NULL);
    return sky_object_bool(o);
}


ssize_t
sky_file_tell(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_callmethod(file, SKY_FILE_METHOD(TELL), NULL, NULL);
    return sky_integer_value(o, SSIZE_MIN, SSIZE_MAX, NULL);
}


void
sky_file_truncate(sky_object_t file, ssize_t pos)
{
    sky_object_callmethod(file,
                          SKY_FILE_METHOD(TRUNCATE), 
                          sky_object_build("(iz)", pos),
                          NULL);
}


sky_bool_t
sky_file_writable(sky_object_t file)
{
    sky_object_t    o;

    o = sky_object_callmethod(file, SKY_FILE_METHOD(WRITABLE), NULL, NULL);
    return sky_object_bool(o);
}


ssize_t
sky_file_write(sky_object_t file, sky_object_t b)
{
    sky_object_t    o;

    o = sky_object_callmethod(file,
                              SKY_FILE_METHOD(WRITE),
                              sky_tuple_pack(1, b),
                              NULL);
    if (sky_object_isnull(o)) {
        return -1;
    }
    return sky_integer_value(o, SSIZE_MIN, SSIZE_MAX, NULL);
}


void
sky_file_writelines(sky_object_t file, sky_object_t lines)
{
    sky_object_callmethod(file,
                          SKY_FILE_METHOD(WRITELINES),
                          sky_tuple_pack(1, lines),
                          NULL);
}


void
sky_file_initialize_library(SKY_UNUSED unsigned int flags)
{
    size_t  i;

    for (i = 0;
         i < sizeof(sky_file_methods) / sizeof(sky_file_methods[0]);
         ++i)
    {
        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&(sky_file_methods[i].string)),
                sky_string_createfrombytes(sky_file_methods[i].literal,
                                           strlen(sky_file_methods[i].literal),
                                           NULL,
                                           NULL),
                SKY_TRUE);
    }
}
