#ifndef __SKYTHON_CORE_SKY_MODULE_PRIVATE_H__
#define __SKYTHON_CORE_SKY_MODULE_PRIVATE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


extern sky_bool_t
sky_module_import_lock_acquire(void);

extern sky_bool_t
sky_module_import_lock_held(void);

extern sky_bool_t
sky_module_import_lock_release(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MODULE_PRIVATE_H__ */
