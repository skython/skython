/** @file
  * @brief
  * @defgroup containers Containers
  * @{
  * @defgroup sky_tuple Tuples
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_TUPLE_H__
#define __SKYTHON_CORE_SKY_TUPLE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** A tuple object instance that contains nothing. **/
SKY_EXTERN sky_tuple_t const sky_tuple_empty;


/** Create a new tuple by adding two existing tuples together.
  *
  * @param[in]  tuple   the first tuple to combine.
  * @param[in]  other   the second tuple to combine.
  * @return     a new tuple object instance that is the combination of @a tuple
  *             and @a other.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_add(sky_tuple_t tuple, sky_object_t other);


/** Create a tuple from an iterable object.
  * If @a object is already a tuple, it will simply be returned as is. Any
  * object type that implements __iter__() can be converted into a tuple.
  *
  * @param[in]  object      the object to be converted into a tuple.
  * @return     a tuple object instance created from @a object.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_create(sky_object_t object);


/** Create a new tuple object instance from an array of objects.
  *
  * @param[in]  count       the number of objects present in the @a objects
  *                         list.
  * @param[in]  objects     a list of objects to be used to populate the tuple.
  *                         Any @c NULL pointers will be replaced with
  *                         @c sky_None objects.
  * @return     the new tuple object instance.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_createfromarray(ssize_t count, sky_object_t *objects);


/** Create a new tuple object instance with an array of objects.
  *
  * @param[in]  count       the number of objects present in the @a objects
  *                         list.
  * @param[in]  objects     a list of objects to populate the tuple.
  *                         Any @c NULL pointers will be replaced with
  *                         @c sky_None objects, and the created instance
  *                         will take ownership of the array.
  * @param[in]  free        a function to be called to free the memory for
  *                         @a objects when the tuple object is destroyed.
  * @return     the new tuple object instance.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_createwitharray(ssize_t       count,
                          sky_object_t *objects,
                          sky_free_t    free);


/** Find the index of the first occurrence of an item in a tuple.
  *
  * @param[in]  tuple   the tuple to be searched.
  * @param[in]  item    the item to be found.
  * @return     the index of @a item in @a tuple if it is found; otherwise, -1.
  */
SKY_EXTERN ssize_t
sky_tuple_find(sky_tuple_t tuple, sky_object_t item);


/** Get an object at a specific index position in a tuple.
  * 
  * @param[in]  tuple   the tuple to query.
  * @param[in]  index   the index position in the tuple. If the index position
  *                     is out of range, @c sky_IndexError will be raised.
  * @return     the object at the requested index position in the tuple.
  */
SKY_EXTERN sky_object_t
sky_tuple_get(sky_tuple_t tuple, ssize_t index);


/** Create a new tuple from an existing tuple with a new object inserted.
  * A new tuple is created from an existing tuple with a new object inserted
  * at a specific index position in the new tuple.
  *
  * @param[in]  tuple   the existing tuple to be copied.
  * @param[in]  index   the index position at which to insert.
  * @param[in]  object  the object to insert.
  * @return     a new tuple object instance that is a copy of the original
  *             tuple with @a object inserted at the requested index position.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_insert(sky_tuple_t tuple, ssize_t index, sky_object_t object);


/** Return the length of a tuple.
  *
  * @param[in]  tuple   the tuple for which the length is to be returned.
  * @return     the length of the tuple.
  */
SKY_EXTERN ssize_t
sky_tuple_len(sky_tuple_t tuple);


/** Create a new tuple object instance from a variable argument list.
  *
  * @param[in]  count       the number of objects present in the argument list.
  * @param[in]  ...         the variable argument list containing the objects
  *                         to populate the tuple. Any @c NULL pointers will
  *                         be replaced with @c sky_None objects.
  * @return     the new tuple object instance.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_pack(ssize_t count, ...);

/** Create a new tuple object instance from a variable argument list.
  *
  * @param[in]  count       the number of objects present in the argument list.
  * @param[in]  ap          the variable argument list containing the objects
  *                         to populate the tuple. Any @c NULL pointers will
  *                         be replaced with @c sky_None objects.
  * @return     the new tuple object instance.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_packv(ssize_t count, va_list ap);


/** Repeat a tuple.
  *
  * @param[in]  tuple   the tuple to be repeated.
  * @param[in]  count   the number of times to repeat @a tuple.
  * @return     a tuple instance that is @a tuple repeated @a count times.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_repeat(sky_tuple_t tuple, ssize_t count);


/** Create a new tuple that is a slice of an existing one.
  *
  * @param[in]  tuple   the existing tuple to slice.
  * @param[in]  start   the starting index for the slice, inclusive.
  * @param[in]  stop    the ending index for the slice, exclusive.
  * @param[in]  step    the step to use between @a start and @a stop.
  * @return     a new tuple instance that is the specified slice of an existing
  *             tuple.
  */
SKY_EXTERN sky_tuple_t
sky_tuple_slice(sky_tuple_t tuple, ssize_t start, ssize_t stop, ssize_t step);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TUPLE_H__ */

/** @} **/
/** @} **/
