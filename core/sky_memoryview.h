#ifndef __SKYTHON_CORE_SKY_MEMORYVIEW_H__
#define __SKYTHON_CORE_SKY_MEMORYVIEW_H__ 1


#include "sky_base.h"
#include "sky_buffer.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new memoryview object instance with an existing buffer.
  * The memoryview object will take ownership of the buffer that is passed in
  * without making a copy of it. When sky_memoryview_release() is called, the
  * buffer will be released via sky_buffer_release(), and its memory will be
  * freed by calling sky_free(). If the buffer is not actually attached to an
  * object, sky_buffer_release() will not be called.
  *
  * @param[in]  buffer      the buffer object to use as a source.
  * @return     the new memoryview object instance.
  */
SKY_EXTERN sky_memoryview_t
sky_memoryview_createwithbuffer(sky_buffer_t *buffer);


/** Create a new memoryview object instance from an object that supports the
  * buffer protocol.
  *
  * @param[in]  object      the object to use as a source.
  * @return     the new memoryview object instance.
  */
SKY_EXTERN sky_memoryview_t
sky_memoryview_createwithobject(sky_object_t object);


SKY_EXTERN sky_memoryview_t
sky_memoryview_cast(sky_memoryview_t    self,
                    sky_string_t        format,
                    sky_object_t        shape);


/** Release the underlying buffer backing a memoryview object instance.
  * Once the underlying buffer backing the memoryview is released, the
  * memoryview object is no longer usable. It is not an error to release a
  * memoryview object that has already been released.
  *
  * @param[in]  self        the memoryview object to release.
  */
SKY_EXTERN void
sky_memoryview_release(sky_memoryview_t self);


/** Create a bytes object instance from a memoryview object instance.
  * The data from the buffer backing the memoryview object is used to create
  * the new bytes object. The bytes object is a copy of the data in the
  * memoryview.
  *
  * @param[in]  self        the memoryview object instance to use as a source.
  * @return     a new bytes object instance that is a copy of the data backing
  *             the memoryview object.
  */
SKY_EXTERN sky_bytes_t
sky_memoryview_tobytes(sky_memoryview_t self);


SKY_EXTERN sky_object_t
sky_memoryview_tolist(sky_memoryview_t self);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MEMORYVIEW_H__ */
