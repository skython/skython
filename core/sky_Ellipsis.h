/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_ellipsis The Ellipsis Object
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ELLIPSIS_H__
#define __SKYTHON_CORE_SKY_ELLIPSIS_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** An object representing the Ellipsis object. **/
SKY_EXTERN sky_Ellipsis_t const sky_Ellipsis;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ELLIPSIS_H__ */

/** @} **/
/** @} **/
