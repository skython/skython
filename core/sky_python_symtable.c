#include "sky_private.h"

#include "sky_python_compiler.h"
#include "sky_python_symtable.h"


typedef struct sky_python_symtable_s {
    sky_string_t                        filename;
    int                                 lineno;
    int                                 col_offset;

    sky_dict_t                          blocks;

    sky_python_symtable_block_t         block;
    sky_python_symtable_block_t         class_block;
    sky_python_symtable_block_t         top_block;
    sky_dict_t                          global_symbols;
} sky_python_symtable_t;


static void
sky_python_symtable_block_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_python_symtable_block_data_t    *block_data = data;

    sky_object_visit(block_data->id, visit_data);
    sky_object_visit(block_data->name, visit_data);
    sky_object_visit(block_data->symbols, visit_data);
    sky_object_visit(block_data->varnames, visit_data);
    sky_object_visit(block_data->children, visit_data);
    sky_object_visit(block_data->parent, visit_data);
}


static void SKY_NORETURN
sky_python_symtable_raise_(sky_python_symtable_t *  table,
                           sky_type_t               type,
                           const char *             c_function,
                           const char *             c_filename,
                           int                      c_lineno,
                           const char *             fmt,
                           ...)
{
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformatv(fmt, ap);
    va_end(ap);

    text = sky_python_compiler_line(table->filename, table->lineno);
    if (!sky_object_bool(text)) {
        text = (sky_string_t)sky_None;
    }

    sky_error_raise_(type,
                     sky_object_build("(O(OiiO))",
                                      message,
                                      table->filename,
                                      table->lineno,
                                      table->col_offset,
                                      text),
                     c_function,
                     c_filename,
                     c_lineno);
}

#define sky_python_symtable_raise(table, type, fmt, ...)                    \
        sky_python_symtable_raise_(table, type,                             \
                                   __FUNCTION__, __FILE__, __LINE__,        \
                                   fmt, ## __VA_ARGS__)


static void
sky_python_symtable_warn_(sky_python_symtable_t *   table,
                          const char *              c_function,
                          const char *              c_filename,
                          int                       c_lineno,
                          const char *              fmt,
                          ...)
{
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformatv(fmt, ap);
    va_end(ap);

    SKY_ERROR_TRY {
        sky_error_warn_explicit(message,
                                sky_SyntaxWarning,
                                table->filename,
                                table->lineno,
                                NULL,
                                NULL,
                                NULL);
    } SKY_ERROR_EXCEPT(sky_SyntaxWarning) {
        text = sky_python_compiler_line(table->filename, table->lineno);
        if (!sky_object_bool(text)) {
            text = (sky_string_t)sky_None;
        }

        sky_error_raise_(sky_SyntaxError,
                         sky_object_build("(O(OiiO))",
                                          message,
                                          table->filename,
                                          table->lineno,
                                          table->col_offset,
                                          text),
                         c_function,
                         c_filename,
                         c_lineno);
    } SKY_ERROR_TRY_END;
}

#define sky_python_symtable_warn(table, fmt, ...)                           \
        sky_python_symtable_warn_(table,                                    \
                                  __FUNCTION__, __FILE__, __LINE__,         \
                                  fmt, ## __VA_ARGS__)


sky_string_t
sky_python_symtable_block_repr(sky_python_symtable_block_t self)
{
    sky_python_symtable_block_data_t    *block_data =
                                        sky_python_symtable_block_data(self);

    return sky_string_createfromformat("<%@ %@(%@), line %d>",
                                       sky_type_name(sky_object_type(self)),
                                       block_data->name,
                                       block_data->id,
                                       block_data->lineno);
}


static sky_python_symtable_block_t
sky_python_symtable_block_create(sky_python_symtable_block_type_t   type,
                                 sky_object_t                       id,
                                 sky_string_t                       name,
                                 int                                lineno,
                                 int                                col_offset)
{
    sky_python_symtable_block_t         block;
    sky_python_symtable_block_data_t    *block_data;

    block = sky_object_allocate(sky_python_symtable_block_type);
    block_data = sky_python_symtable_block_data(block);

    block_data->id = id;
    block_data->name = name;
    block_data->symbols = sky_dict_create();
    block_data->varnames = sky_list_create(NULL);
    block_data->children = sky_list_create(NULL);
    block_data->type = type;
    block_data->lineno = lineno;
    block_data->col_offset = col_offset;
    block_data->optimized = SKY_PYTHON_SYMTABLE_OPTIMIZED_NONE;
    block_data->nested = SKY_FALSE;

    return block;
}


static sky_python_symtable_block_t
sky_python_symtable_block_enter(sky_python_symtable_t *             table,
                                sky_object_t                        node,
                                sky_python_symtable_block_type_t    type,
                                sky_string_t                        name,
                                int                                 lineno,
                                int                                 col_offset)
{
    sky_python_symtable_block_t         block, return_block;
    sky_python_symtable_block_data_t    *parent_data, *block_data;

    block = sky_python_symtable_block_create(type,
                                             node,
                                             name,
                                             lineno,
                                             col_offset);
    block_data = sky_python_symtable_block_data(block);
    block_data->parent = table->block;
    table->block = block;
    if (!sky_dict_add(table->blocks, node, block)) {
        sky_error_raise_string(sky_SystemError, "duplicate AST block node");
    }

    if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE == type) {
        if (block_data->parent) {
            sky_error_raise_string(sky_SystemError, "nested module block");
        }
        block_data->optimized = SKY_PYTHON_SYMTABLE_OPTIMIZED_TOP_LEVEL;
    }
    else {
        parent_data = sky_python_symtable_block_data(block_data->parent);
        sky_list_append(parent_data->children, block);
        if (parent_data->nested ||
            SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION == parent_data->type)
        {
            block_data->nested = SKY_TRUE;
        }
    }

    return_block = table->class_block;
    if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS == type) {
        table->class_block = block;
    }
    return return_block;
}


static void
sky_python_symtable_block_leave(sky_python_symtable_t *     table,
                                sky_python_symtable_block_t saved_block)
{
    sky_python_symtable_block_data_t    *block_data;

    block_data = sky_python_symtable_block_data(table->block);
    table->block = block_data->parent;
    table->class_block = saved_block;
}


static intmax_t
sky_python_symtable_symbol_lookup(sky_python_symtable_t *   table,
                                  sky_string_t              name)
{
    sky_string_t                        mangled_name;
    sky_integer_t                       existing_flags;
    sky_python_symtable_block_data_t    *block_data;

    sky_error_validate_debug(sky_object_isa(name, sky_string_type));
    if (!table->class_block) {
        mangled_name = name;
    }
    else {
        block_data = sky_python_symtable_block_data(table->class_block);
        mangled_name = sky_python_compiler_mangle_name(block_data->name,
                                                       name);
    }

    block_data = sky_python_symtable_block_data(table->block);
    if (!(existing_flags = sky_dict_get(block_data->symbols,
                                        mangled_name,
                                        NULL)))
    {
        return 0;
    }
    return sky_integer_value(existing_flags, INTMAX_MIN, INTMAX_MAX, NULL);
}


static void
sky_python_symtable_symbol_add(sky_python_symtable_t *  table,
                               sky_string_t             name,
                               intmax_t                 new_flags)
{
    intmax_t                            all_flags;
    sky_string_t                        mangled_name;
    sky_integer_t                       existing_flags;
    sky_python_symtable_block_data_t    *block_data;

    sky_error_validate_debug(sky_object_isa(name, sky_string_type));
    if (!table->class_block) {
        mangled_name = name;
    }
    else {
        block_data = sky_python_symtable_block_data(table->class_block);
        mangled_name = sky_python_compiler_mangle_name(block_data->name,
                                                       name);
    }

    block_data = sky_python_symtable_block_data(table->block);
    if (!(existing_flags = sky_dict_get(block_data->symbols,
                                        mangled_name,
                                        NULL)))
    {
        all_flags = new_flags;
    }
    else {
        all_flags = sky_integer_value(existing_flags,
                                      INTMAX_MIN,
                                      INTMAX_MAX,
                                      NULL);
        if ((all_flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM) &&
            (new_flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM))
        {
            sky_python_symtable_raise(
                    table,
                    sky_SyntaxError,
                    "duplicate argument %#@ in function definition",
                    name);
        }
        all_flags |= new_flags;
    }
    sky_dict_setitem(block_data->symbols,
                     mangled_name,
                     sky_integer_create(all_flags));

    if (new_flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM) {
        sky_list_append(block_data->varnames, mangled_name);
    }

    /* This can only happen from the Global AST node handler, but it's better
     * to put it here since the mangling is already done. Note that it's legal
     * to use 'global' at module level, but it really has no effect. Don't do
     * a double update if the current block is the module block.
     */
    if (block_data->symbols != table->global_symbols &&
        (new_flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_GLOBAL))
    {
        if (!(existing_flags = sky_dict_get(table->global_symbols,
                                            mangled_name,
                                            NULL)))
        {
            all_flags = new_flags;
        }
        else {
            all_flags = sky_integer_value(existing_flags,
                                          INTMAX_MIN,
                                          INTMAX_MAX,
                                          NULL);
            all_flags |= new_flags;
        }
        sky_dict_setitem(table->global_symbols,
                         mangled_name,
                         sky_integer_create(all_flags));
    }
}


static void
sky_python_symtable_visit_expr(sky_python_symtable_t *  table,
                               sky_object_t             node);

static void
sky_python_symtable_visit_stmt(sky_python_symtable_t *  table,
                               sky_object_t             node);


static void
sky_python_symtable_visit_slice(sky_python_symtable_t * table,
                                sky_object_t            node)
{
    sky_object_t    element, sequence;

    if (sky_object_isnull(node)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Slice_type)) {
        sky_python_symtable_visit_expr(table,
                                       ((sky_python_ast_Slice_t)node)->lower);
        sky_python_symtable_visit_expr(table,
                                       ((sky_python_ast_Slice_t)node)->upper);
        sky_python_symtable_visit_expr(table,
                                       ((sky_python_ast_Slice_t)node)->step);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ExtSlice_type)) {
        sequence = ((sky_python_ast_ExtSlice_t)node)->dims;
        SKY_SEQUENCE_FOREACH(sequence, element) {
            sky_python_symtable_visit_slice(table, element);
        } SKY_SEQUENCE_FOREACH_END;
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Index_type)) {
        sky_python_symtable_visit_expr(table,
                                       ((sky_python_ast_Index_t)node)->value);
        return;
    }
    sky_error_raise_format(sky_TypeError,
                           "malformed AST: expected slice; got %#@",
                           sky_type_name(sky_object_type(node)));
}


static inline void
sky_python_symtable_visit_expr_sequence(sky_python_symtable_t * table,
                                        sky_object_t            sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_symtable_visit_expr(table, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static inline void
sky_python_symtable_visit_arguments(sky_python_symtable_t *     table,
                                    sky_python_ast_arguments_t  args)
{
    sky_object_t    element;

    sky_error_validate_debug(sky_object_isa(args, sky_python_ast_arguments_type));

    SKY_SEQUENCE_FOREACH(args->args, element) {
        if (!sky_object_isa(element, sky_python_ast_arg_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "malformed AST: expected arg; got %#@",
                                   sky_type_name(sky_object_type(element)));
        }
        sky_python_symtable_symbol_add(table, 
                                       ((sky_python_ast_arg_t)element)->arg,
                                       SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);
    } SKY_SEQUENCE_FOREACH_END;
    SKY_SEQUENCE_FOREACH(args->kwonlyargs, element) {
        if (!sky_object_isa(element, sky_python_ast_arg_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "malformed AST: expected arg; got %#@",
                                   sky_type_name(sky_object_type(element)));
        }
        sky_python_symtable_symbol_add(table,
                                       ((sky_python_ast_arg_t)element)->arg,
                                       SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);
    } SKY_SEQUENCE_FOREACH_END;
    if (!sky_object_isnull(args->vararg)) {
        sky_python_symtable_symbol_add(table,
                                       args->vararg,
                                       SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);
    }
    if (!sky_object_isnull(args->kwarg)) {
        sky_python_symtable_symbol_add(table,
                                       args->kwarg,
                                       SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);
    }
}


static inline void
sky_python_symtable_visit_keyword(sky_python_symtable_t *   table,
                                  sky_object_t              node)
{
    if (sky_object_isa(node, sky_python_ast_keyword_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_keyword_t)node)->value);
        return;
    }
    sky_error_raise_format(sky_TypeError,
                           "malformed AST: expected keyword; got %#@",
                           sky_type_name(sky_object_type(node)));
}


static inline void
sky_python_symtable_visit_keyword_sequence(sky_python_symtable_t *  table,
                                           sky_object_t             sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_symtable_visit_keyword(table, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_symtable_visit_comprehension(sky_python_symtable_t *table,
                                        sky_object_t           node)
{
    ssize_t                     i, limit;
    sky_object_t                element, elt, generators, value;
    sky_string_t                name;
    sky_python_symtable_block_t saved_block;

    /* Comprehensions fabricate a function block to run in. The outermost
     * iterator (which is often the only iterator) is passed as an argument
     * to the fabricated function, which means a parameter must also be
     * fabricated. For DictComp, ListComp, and SetComp, a temporary variable
     * must be fabricated to hold the object being created. For fabricated
     * variables, use names that are not actually valid identifiers.
     */

    if (sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        name = SKY_STRING_LITERAL("genexpr");
        elt = ((sky_python_ast_GeneratorExp_t)node)->elt;
        value = NULL;
        generators = ((sky_python_ast_GeneratorExp_t)node)->generators;
    }
    else if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
        name = SKY_STRING_LITERAL("dictcomp");
        elt = ((sky_python_ast_DictComp_t)node)->key;
        value = ((sky_python_ast_DictComp_t)node)->value;
        generators = ((sky_python_ast_DictComp_t)node)->generators;
    }
    else if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
        name = SKY_STRING_LITERAL("listcomp");
        elt = ((sky_python_ast_ListComp_t)node)->elt;
        value = NULL;
        generators = ((sky_python_ast_ListComp_t)node)->generators;
    }
    else if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
        name = SKY_STRING_LITERAL("setcomp");
        elt = ((sky_python_ast_SetComp_t)node)->elt;
        value = NULL;
        generators = ((sky_python_ast_SetComp_t)node)->generators;
    }
    else {
        sky_error_raise_format(
                sky_TypeError,
                "malformed AST: expected a comprehension type; got %#@",
                sky_type_name(sky_object_type(node)));
    }

    /* The generators node will always be a list when it comes from the
     * parser; however, it is possible that it could be another sequence
     * type if the AST has been manipulated manually.
     */
    if (sky_list_type != sky_object_type(generators)) {
        generators = sky_list_create(generators);
    }
    element = sky_list_get(generators, 0);
    if (!sky_object_isa(element, sky_python_ast_comprehension_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "malformed AST: expected comprehension; got %#@",
                sky_type_name(sky_object_type(element)));
    }
    sky_python_symtable_visit_expr(
            table,
            ((sky_python_ast_comprehension_t)element)->iter);

    saved_block =
            sky_python_symtable_block_enter(
                    table,
                    node,
                    SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION,
                    name,
                    table->lineno,
                    table->col_offset);

    /* Fabricate an argument. CPython uses ".0" so we will too. */
    sky_python_symtable_symbol_add(table,
                                   SKY_STRING_LITERAL(".0"),
                                   SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);

    /* For DictComp, ListComp, and SetComp, fabricate a variable to hold
     * the resulting dict, list, or set object. CPython uses "_[1]" for some
     * reason (why not ".1"?). We will too.
     */
    if (!sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        sky_python_symtable_symbol_add(table,
                                       SKY_STRING_LITERAL("_[1]"),
                                       SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
    }

    /* Visit the rest of the outermost generator (iter was done earlier
     * as an argument to the fake function outside of its block).
     */
    sky_python_symtable_visit_expr(
            table,
            ((sky_python_ast_comprehension_t)element)->target);
    sky_python_symtable_visit_expr_sequence(
            table,
            ((sky_python_ast_comprehension_t)element)->ifs);

    /* Visit the rest of the generators. */
    if ((limit = sky_list_len(generators)) > 1) {
        for (i = 1; i < limit; ++i) {
            element = sky_list_get(generators, i);
            if (!sky_object_isa(element, sky_python_ast_comprehension_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "malformed AST: expected comprehension; got %#@",
                        sky_type_name(sky_object_type(element)));
            }
            sky_python_symtable_visit_expr(
                    table,
                    ((sky_python_ast_comprehension_t)element)->target);
            sky_python_symtable_visit_expr(
                    table,
                    ((sky_python_ast_comprehension_t)element)->iter);
            sky_python_symtable_visit_expr_sequence(
                    table,
                    ((sky_python_ast_comprehension_t)element)->ifs);
        }
    }

    sky_python_symtable_visit_expr(table, elt);
    sky_python_symtable_visit_expr(table, value);   /* DictComp only */

    sky_python_symtable_block_leave(table, saved_block);
}


static void
sky_python_symtable_visit_expr(sky_python_symtable_t *  table,
                               sky_object_t             node)
{
    sky_python_ast_arguments_t          args;
    sky_python_symtable_block_t         saved_block;
    sky_python_symtable_block_data_t    *block_data;

    if (sky_object_isnull(node)) {
        return;
    }
    if (!sky_object_isa(node, sky_python_ast_expr_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected expr; got %#@",
                               sky_type_name(sky_object_type(node)));
    }
    table->lineno = ((sky_python_ast_expr_t)node)->lineno;
    table->col_offset = ((sky_python_ast_expr_t)node)->col_offset;

    if (sky_object_isa(node, sky_python_ast_BoolOp_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_BoolOp_t)node)->values);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_BinOp_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_BinOp_t)node)->left);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_BinOp_t)node)->right);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_UnaryOp_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_UnaryOp_t)node)->operand);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Lambda_type)) {
        args = ((sky_python_ast_Lambda_t)node)->args;
        if (!sky_object_isa(args, sky_python_ast_arguments_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "malformed AST: expected arguments; got %#@",
                    sky_type_name(sky_object_type(args)));
        }
        /* args->varargannotation and args->kwargannotation should both be
         * NULL, but may be set if AST is manipulated manually. Ignore them
         * since the compiler will do nothing with them anyway.
         */
        sky_python_symtable_visit_expr_sequence(table, args->defaults);
        sky_python_symtable_visit_expr_sequence(table, args->kw_defaults);

        saved_block =
                sky_python_symtable_block_enter(
                        table,
                        node,
                        SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION,
                        SKY_STRING_LITERAL("lambda"),
                        ((sky_python_ast_Lambda_t)node)->lineno,
                        ((sky_python_ast_Lambda_t)node)->col_offset);
        sky_python_symtable_visit_arguments(table, args);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Lambda_t)node)->body);
        sky_python_symtable_block_leave(table, saved_block);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_IfExp_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_IfExp_t)node)->test);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_IfExp_t)node)->body);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_IfExp_t)node)->orelse);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Dict_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Dict_t)node)->keys);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Dict_t)node)->values);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Set_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Set_t)node)->elts);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
        sky_python_symtable_visit_comprehension(table, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
        sky_python_symtable_visit_comprehension(table, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
        sky_python_symtable_visit_comprehension(table, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        sky_python_symtable_visit_comprehension(table, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Yield_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Yield_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_YieldFrom_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_YieldFrom_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Compare_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Compare_t)node)->left);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Compare_t)node)->comparators);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Call_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Call_t)node)->func);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Call_t)node)->args);
        sky_python_symtable_visit_keyword_sequence(
                table,
                ((sky_python_ast_Call_t)node)->keywords);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Call_t)node)->starargs);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Call_t)node)->kwargs);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Num_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Str_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Bytes_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Ellipsis_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Attribute_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Attribute_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Subscript_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Subscript_t)node)->value);
        sky_python_symtable_visit_slice(
                table,
                ((sky_python_ast_Subscript_t)node)->slice);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Starred_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Starred_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Name_type)) {
        if (sky_python_ast_Load == ((sky_python_ast_Name_t)node)->ctx) {
            sky_python_symtable_symbol_add(
                    table,
                    ((sky_python_ast_Name_t)node)->id,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE);

            /* super() needs __class__ to work if it's called without
             * arguments, so add a __class__ definition here.
             */
            block_data = sky_python_symtable_block_data(table->block);
            if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION == block_data->type &&
                sky_object_compare(((sky_python_ast_Name_t)node)->id,
                                   SKY_STRING_LITERAL("super"),
                                   SKY_COMPARE_OP_EQUAL))
            {
                sky_python_symtable_symbol_add(
                        table,
                        SKY_STRING_LITERAL("__class__"),
                        SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE);
            }
        }
        else {
            sky_python_symtable_symbol_add(
                    table,
                    ((sky_python_ast_Name_t)node)->id,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
        }
        return;
    }
    if (sky_object_isa(node, sky_python_ast_List_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_List_t)node)->elts);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Tuple_t)node)->elts);
        return;
    }
    sky_error_raise_format(
            sky_TypeError,
            "malformed AST: expected an expression type; got %#@",
            sky_type_name(sky_object_type(node)));
}


static inline void
sky_python_symtable_visit_stmt_sequence(sky_python_symtable_t * table,
                                        sky_object_t            sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_symtable_visit_stmt(table, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static inline void
sky_python_symtable_visit_annotations(sky_python_symtable_t *       table,
                                      sky_python_ast_arguments_t    args)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(args->args, element) {
        if (!sky_object_isa(element, sky_python_ast_arg_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "malformed AST: expected arg; got %#@",
                                   sky_type_name(sky_object_type(element)));
        }
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_arg_t)element)->annotation);
    } SKY_SEQUENCE_FOREACH_END;
    SKY_SEQUENCE_FOREACH(args->kwonlyargs, element) {
        if (!sky_object_isa(element, sky_python_ast_arg_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "malformed AST: expected arg; got %#@",
                                   sky_type_name(sky_object_type(element)));
        }
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_arg_t)element)->annotation);
    } SKY_SEQUENCE_FOREACH_END;
    sky_python_symtable_visit_expr(table, args->varargannotation);
    sky_python_symtable_visit_expr(table, args->kwargannotation);
}


static inline void
sky_python_symtable_visit_excepthandler(sky_python_symtable_t * table,
                                        sky_object_t            node)
{
    sky_string_t    name;

    if (!sky_object_isa(node, sky_python_ast_excepthandler_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "malformed AST: expected excepthandler; got %#@",
                sky_type_name(sky_object_type(node)));
    }
    table->lineno = ((sky_python_ast_excepthandler_t)node)->lineno;
    table->col_offset = ((sky_python_ast_excepthandler_t)node)->col_offset;

    if (sky_object_isa(node, sky_python_ast_ExceptHandler_type)) {
        name = ((sky_python_ast_ExceptHandler_t)node)->name;
        if (!sky_object_isnull(name)) {
            sky_python_symtable_symbol_add(
                    table,
                    name,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
        }
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_ExceptHandler_t)node)->type);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_ExceptHandler_t)node)->body);
        return;
    }
    sky_error_raise_format(sky_TypeError,
                           "malformed AST: expected ExceptHandler; got %#@",
                           sky_type_name(sky_object_type(node)));
}


static inline void
sky_python_symtable_visit_excepthandler_sequence(sky_python_symtable_t *table,
                                                 sky_object_t           sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_symtable_visit_excepthandler(table, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static inline void
sky_python_symtable_visit_withitem(sky_python_symtable_t *  table,
                                   sky_object_t             node)
{
    if (sky_object_isa(node, sky_python_ast_withitem_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_withitem_t)node)->context_expr);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_withitem_t)node)->optional_vars);
        return;
    }
    sky_error_raise_format(sky_TypeError,
                           "malformed AST: expected withitem; got %#@",
                           sky_type_name(sky_object_type(node)));
}


static inline void
sky_python_symtable_visit_withitem_sequence(sky_python_symtable_t * table,
                                            sky_object_t            sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_symtable_visit_withitem(table, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_symtable_visit_aliases(sky_python_symtable_t *   table,
                                  sky_object_t              sequence)
{
    ssize_t                             dot;
    sky_object_t                        element;
    sky_string_t                        name;
    sky_python_ast_alias_t              alias;
    sky_python_symtable_block_data_t    *block_data;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        if (!sky_object_isa(element, sky_python_ast_alias_type)) {
            sky_error_raise_format(sky_TypeError,
                                   "malformed AST: expected alias; got %#@",
                                   sky_type_name(sky_object_type(element)));
        }

        alias = (sky_python_ast_alias_t)element;
        if (sky_string_eq(SKY_STRING_LITERAL("*"), alias->name) == sky_True) {
            block_data = sky_python_symtable_block_data(table->block);
            if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE != block_data->type) {
                sky_python_symtable_raise(
                        table,
                        sky_SyntaxError,
                        "import * only allowed at module level");
            }
            block_data->optimized |= SKY_PYTHON_SYMTABLE_OPTIMIZED_IMPORT_STAR;
        }
        else {
            if (!sky_object_isnull(alias->asname)) {
                name = alias->asname;
            }
            else {
                name = alias->name;
                if ((dot = sky_string_find(alias->name,
                                           SKY_STRING_LITERAL("."),
                                           0,
                                           SSIZE_MAX)) != -1)
                {
                    name = sky_string_slice(name, 0, dot, 1);
                }
            }

            sky_python_symtable_symbol_add(
                    table,
                    name,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_IMPORT);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_symtable_visit_stmt(sky_python_symtable_t *  table,
                               sky_object_t             node)
{
    intmax_t                    flags;
    const char                  *s;
    sky_string_t                name;
    sky_python_symtable_block_t saved_block;

    if (sky_object_isnull(node)) {
        return;
    }
    if (!sky_object_isa(node, sky_python_ast_stmt_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected stmt; got %#@",
                               sky_type_name(sky_object_type(node)));
    }
    table->lineno = ((sky_python_ast_stmt_t)node)->lineno;
    table->col_offset = ((sky_python_ast_stmt_t)node)->col_offset;

    if (sky_object_isa(node, sky_python_ast_FunctionDef_type)) {
        sky_python_ast_arguments_t  args;

        sky_python_symtable_symbol_add(
                table,
                ((sky_python_ast_FunctionDef_t)node)->name,
                SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
        args = ((sky_python_ast_FunctionDef_t)node)->args;
        if (!sky_object_isa(args, sky_python_ast_arguments_type)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "malformed AST: expected arguments; got %#@",
                    sky_type_name(sky_object_type(args)));
        }
        sky_python_symtable_visit_annotations(table, args);
        sky_python_symtable_visit_expr_sequence(table, args->defaults);
        sky_python_symtable_visit_expr_sequence(table, args->kw_defaults);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_FunctionDef_t)node)->decorator_list);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_FunctionDef_t)node)->returns);

        saved_block =
                sky_python_symtable_block_enter(
                        table,
                        node,
                        SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION,
                        ((sky_python_ast_FunctionDef_t)node)->name,
                        ((sky_python_ast_FunctionDef_t)node)->lineno,
                        ((sky_python_ast_FunctionDef_t)node)->col_offset);
        sky_python_symtable_visit_arguments(table, args);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_FunctionDef_t)node)->body);
        sky_python_symtable_block_leave(table, saved_block);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ClassDef_type)) {
        sky_python_symtable_symbol_add(
                table,
                ((sky_python_ast_ClassDef_t)node)->name,
                SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_ClassDef_t)node)->bases);
        sky_python_symtable_visit_keyword_sequence(
                table,
                ((sky_python_ast_ClassDef_t)node)->keywords);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_ClassDef_t)node)->starargs);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_ClassDef_t)node)->kwargs);
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_ClassDef_t)node)->decorator_list);

        saved_block =
                sky_python_symtable_block_enter(
                        table,
                        node,
                        SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS,
                        ((sky_python_ast_ClassDef_t)node)->name,
                        ((sky_python_ast_ClassDef_t)node)->lineno,
                        ((sky_python_ast_ClassDef_t)node)->col_offset);
        sky_python_symtable_symbol_add(
                table,
                SKY_STRING_LITERAL("__class__"),
                SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL);
        sky_python_symtable_symbol_add(
                table,
                SKY_STRING_LITERAL("__locals__"),
                SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_ClassDef_t)node)->body);
        sky_python_symtable_block_leave(table, saved_block);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Return_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Return_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Delete_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Delete_t)node)->targets);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Assign_type)) {
        sky_python_symtable_visit_expr_sequence(
                table,
                ((sky_python_ast_Assign_t)node)->targets);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Assign_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_AugAssign_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_AugAssign_t)node)->target);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_AugAssign_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_For_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_For_t)node)->target);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_For_t)node)->iter);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_For_t)node)->body);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_For_t)node)->orelse);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_While_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_While_t)node)->test);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_While_t)node)->body);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_While_t)node)->orelse);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_If_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_If_t)node)->test);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_If_t)node)->body);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_If_t)node)->orelse);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_With_type)) {
        sky_python_symtable_visit_withitem_sequence(
                table,
                ((sky_python_ast_With_t)node)->items);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_With_t)node)->body);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Raise_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Raise_t)node)->exc);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Raise_t)node)->cause);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Try_type)) {
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_Try_t)node)->body);
        sky_python_symtable_visit_excepthandler_sequence(
                table,
                ((sky_python_ast_Try_t)node)->handlers);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_Try_t)node)->orelse);
        sky_python_symtable_visit_stmt_sequence(
                table,
                ((sky_python_ast_Try_t)node)->finalbody);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Assert_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Assert_t)node)->test);
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Assert_t)node)->msg);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Import_type)) {
        sky_python_symtable_visit_aliases(
                table,
                ((sky_python_ast_Import_t)node)->names);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ImportFrom_type)) {
        sky_python_symtable_visit_aliases(
                table,
                ((sky_python_ast_ImportFrom_t)node)->names);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Global_type)) {
        SKY_SEQUENCE_FOREACH(((sky_python_ast_Global_t)node)->names, name) {
            if (!sky_object_isa(name, sky_string_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "malformed AST: expected identifier; got %#@",
                        sky_type_name(sky_object_type(name)));
            }
            if (table->block == table->class_block &&
                (s = sky_string_cstring(name)) != NULL &&
                !strcmp(s, "__class__"))
            {
                sky_python_symtable_raise(table,
                                          sky_SyntaxError,
                                          "cannot make __class__ global");
            }
            flags = sky_python_symtable_symbol_lookup(table, name);
            if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL) {
                sky_python_symtable_warn(
                        table,
                        "name %#@ is assigned to before global declaration",
                        name);
            }
            else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE) {
                sky_python_symtable_warn(
                        table,
                        "name %#@ is used prior to global declaration",
                        name);
            }
            sky_python_symtable_symbol_add(
                    table,
                    name,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_GLOBAL);
        } SKY_SEQUENCE_FOREACH_END;
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Nonlocal_type)) {
        if (table->top_block == table->block) {
            sky_python_symtable_raise(
                    table,
                    sky_SyntaxError,
                    "nonlocal declaration not allowed at module level");
        }
        SKY_SEQUENCE_FOREACH(((sky_python_ast_Nonlocal_t)node)->names, name) {
            if (!sky_object_isa(name, sky_string_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "malformed AST: expected identifier; got %#@",
                        sky_type_name(sky_object_type(name)));
            }
            flags = sky_python_symtable_symbol_lookup(table, name);
            if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL) {
                sky_python_symtable_warn(
                        table,
                        "name %#@ is assigned to before nonlocal declaration",
                        name);
            }
            else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE) {
                sky_python_symtable_warn(
                        table,
                        "name %#@ is used prior to nonlocal declaration",
                        name);
            }
            sky_python_symtable_symbol_add(
                    table,
                    name,
                    SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_NONLOCAL);
        } SKY_SEQUENCE_FOREACH_END;
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Expr_type)) {
        sky_python_symtable_visit_expr(
                table,
                ((sky_python_ast_Expr_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Pass_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Break_type)) {
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Continue_type)) {
        return;
    }
    sky_error_raise_format(
            sky_TypeError,
            "malformed AST: expected a statement type; got %#@",
            sky_type_name(sky_object_type(node)));
}


static sky_python_symtable_block_t
sky_python_symtable_bind_nonlocal(sky_python_symtable_t *       table,
                                  sky_python_symtable_block_t   local_block,
                                  sky_string_t                  name)
{
    sky_python_symtable_block_t         block, bound_block;
    sky_python_symtable_block_data_t    *block_data;

    block_data = sky_python_symtable_block_data(local_block);
    while ((block = block_data->parent) != NULL) {
        intmax_t        flags;
        const char      *s;
        sky_object_t    value;

        if (block == table->top_block) {
            break;
        }
        block_data = sky_python_symtable_block_data(block);

        if (!(value = sky_dict_get(block_data->symbols, name, NULL))) {
            continue;
        }

        /* If the symbol is not bound, it's as good as not existing. */
        flags = sky_integer_value(value, INTMAX_MIN, INTMAX_MAX, NULL);
        if (!sky_python_symtable_symbol_isbound(flags)) {
            continue;
        }

        /* Bind to __class__ in a class block (implicitly or via nonlocal),
         * but otherwise stop looking to bind when a class block is reached,
         * either erroring out (use of nonlocal) or falling back to global
         * implicit. Rationale (besides this is what CPython does) ...?
         */
        if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS == block_data->type &&
            (!(s = sky_string_cstring(name)) || strcmp(s, "__class__")))
        {
            break;
        }
        sky_python_symtable_symbol_setscope(
                &flags,
                SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL);
        sky_dict_replace(block_data->symbols, name, sky_integer_create(flags));

        bound_block = block;
        block_data = sky_python_symtable_block_data(local_block);
        while ((block = block_data->parent) != bound_block) {
            block_data = sky_python_symtable_block_data(block);
            if (!(value = sky_dict_get(block_data->symbols, name, NULL))) {
                flags = 0;
            }
            else {
                flags = sky_integer_value(value, INTMAX_MIN, INTMAX_MAX, NULL);
            }
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE);
            sky_dict_setitem(block_data->symbols,
                             name,
                             sky_integer_create(flags));
        }

        return bound_block;
    }

    return NULL;
}


/* Recursively determine scope for each symbol in a block. The function is
 * recursive, because it will figure out the scope information for a block's
 * symbols first, then it will call itself again for each child of the block.
 */
static void
sky_python_symtable_block_analyze(sky_python_symtable_t *       table,
                                  sky_python_symtable_block_t   block)
{
    sky_python_symtable_block_data_t    *block_data =
                                        sky_python_symtable_block_data(block);

    intmax_t                            flags;
    sky_object_t                        child, name, value;
    sky_python_symtable_block_t         bound_block;
    sky_python_symtable_block_data_t    *bound_block_data;

    SKY_SEQUENCE_FOREACH(block_data->symbols, name) {
        value = sky_dict_getitem(block_data->symbols, name);
        flags = sky_integer_value(value, INTMAX_MIN, INTMAX_MAX, NULL);

        if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_GLOBAL) {
            if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM) {
                sky_python_symtable_raise(
                        table,
                        sky_SyntaxError,
                        "name %#@ is parameter and global",
                        name);
            }
            if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_NONLOCAL) {
                sky_python_symtable_raise(
                        table,
                        sky_SyntaxError,
                        "name %#@ is nonlocal and global",
                        name);
            }
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_EXPLICIT);
        }
        else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_NONLOCAL) {
            /* Walk up parents to find a binding. Do not consider the top
             * block. Error if no binding is found; otherwise, mark it free
             * and change the binding to a cell.
             */
            bound_block = sky_python_symtable_bind_nonlocal(table, block, name);
            if (!bound_block) {
                sky_python_symtable_raise(
                        table,
                        sky_SyntaxError,
                        "no binding for nonlocal %#@ found",
                        name);
            }
            bound_block_data = sky_python_symtable_block_data(bound_block);
            if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS ==
                        bound_block_data->type)
            {
                flags |= SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_FREE_CLASS;
            }
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE);
        }
        else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_LOCAL) {
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL);
        }
        else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_PARAM) {
            if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_NONLOCAL) {
                sky_python_symtable_raise(
                        table,
                        sky_SyntaxError,
                        "name %#@ is parameter and nonlocal",
                        name);
            }
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL);
        }
        else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_IMPORT) {
            sky_python_symtable_symbol_setscope(
                    &flags,
                    SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL);
        }
        else if (flags & SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_USE) {
            /* Walk up parents looking for a binding. Do not consider the top
             * block. If no binding is found, mark it global implicit;
             * otherwise, mark it free and change the binding to cell.
             */
            bound_block = sky_python_symtable_bind_nonlocal(table, block, name);
            if (!bound_block) {
                sky_python_symtable_symbol_setscope(
                        &flags,
                        SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_IMPLICIT);
            }
            else {
                bound_block_data = sky_python_symtable_block_data(bound_block);
                if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS ==
                            bound_block_data->type)
                {
                    flags |= SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_FREE_CLASS;
                }
                sky_python_symtable_symbol_setscope(
                        &flags,
                        SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE);
            }
        }
        else {
            sky_error_fatal("internal error");
        }

        sky_dict_replace(block_data->symbols, name, sky_integer_create(flags));
    } SKY_SEQUENCE_FOREACH_END;

    SKY_SEQUENCE_FOREACH(block_data->children, child) {
        sky_python_symtable_block_analyze(table, child);
    } SKY_SEQUENCE_FOREACH_END;
}


sky_dict_t
sky_python_symtable_create(sky_python_ast_AST_t ast,
                           sky_string_t         filename)
{
    sky_dict_t                          blocks;
    sky_object_t                        body, stmt;
    sky_python_symtable_t               *table;
    sky_python_symtable_block_t         saved_block;
    sky_python_symtable_block_data_t    *block_data;

    SKY_ASSET_BLOCK_BEGIN {
        table = sky_asset_calloc(1, sizeof(sky_python_symtable_t),
                                 SKY_ASSET_CLEANUP_ALWAYS);
        table->filename = filename;
        table->blocks = sky_dict_create();

        /* Pass 1.  Walk the AST to find all blocks and symbols. Determine
         *          symbol flags GLOBAL, LOCAL, PARAM, NONLOCAL, USE, IMPORT
         *          from context.
         */
        saved_block =
                sky_python_symtable_block_enter(
                        table,
                        ast,
                        SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE,
                        SKY_STRING_LITERAL("top"),
                        0,
                        0);

        block_data = sky_python_symtable_block_data(table->block);
        table->top_block = table->block;
        table->global_symbols = block_data->symbols;

        if (sky_object_isa(ast, sky_python_ast_Module_type)) {
            body = ((sky_python_ast_Module_t)ast)->body;
            SKY_SEQUENCE_FOREACH(body, stmt) {
                sky_python_symtable_visit_stmt(table, stmt);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else if (sky_object_isa(ast, sky_python_ast_Interactive_type)) {
            body = ((sky_python_ast_Interactive_t)ast)->body;
            SKY_SEQUENCE_FOREACH(body, stmt) {
                sky_python_symtable_visit_stmt(table, stmt);
            } SKY_SEQUENCE_FOREACH_END;
        }
        else if (sky_object_isa(ast, sky_python_ast_Expression_type)) {
            body = ((sky_python_ast_Expression_t)ast)->body;
            sky_python_symtable_visit_expr(table, body);
        }
        else {
            sky_error_raise_format(sky_TypeError,
                                   "expected block AST node; got %#@",
                                   sky_type_name(sky_object_type(ast)));
        }
        sky_python_symtable_block_leave(table, saved_block);

        /* Pass 2.  Walk blocks startng with table->top_block (the module) and 
         *          all children to determine scope of unbound symbols.
         */
        sky_python_symtable_block_analyze(table, table->top_block);

        blocks = table->blocks;
    } SKY_ASSET_BLOCK_END;

    return blocks;
}


SKY_TYPE_DEFINE_SIMPLE(python_symtable_block,
                       "symtable block",
                       sizeof(sky_python_symtable_block_data_t),
                       NULL,
                       NULL,
                       sky_python_symtable_block_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_python_symtable_initialize_library(SKY_UNUSED unsigned int flags)
{
    sky_type_initialize_builtin(sky_python_symtable_block_type, 0);

    sky_type_setmembers(sky_python_symtable_block_type,
            "id",        NULL,
                         offsetof(sky_python_symtable_block_data_t, id),
                         SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "name",      NULL,
                         offsetof(sky_python_symtable_block_data_t, name),
                         SKY_DATA_TYPE_OBJECT_STRING, SKY_MEMBER_FLAG_READONLY,
            "symbols",   NULL,
                         offsetof(sky_python_symtable_block_data_t, symbols),
                         SKY_DATA_TYPE_OBJECT_DICT, SKY_MEMBER_FLAG_READONLY,
            "varnames",  NULL,
                         offsetof(sky_python_symtable_block_data_t, varnames),
                         SKY_DATA_TYPE_OBJECT_LIST, SKY_MEMBER_FLAG_READONLY,
            "children",  NULL,
                         offsetof(sky_python_symtable_block_data_t, children),
                         SKY_DATA_TYPE_OBJECT_LIST, SKY_MEMBER_FLAG_READONLY,
            "optimized", NULL,
                         offsetof(sky_python_symtable_block_data_t, optimized),
                         SKY_DATA_TYPE_UINT, SKY_MEMBER_FLAG_READONLY,
            "nested",    NULL,
                         offsetof(sky_python_symtable_block_data_t, nested),
                         SKY_DATA_TYPE_BOOL, SKY_MEMBER_FLAG_READONLY,
            "type",      NULL,
                         offsetof(sky_python_symtable_block_data_t, type),
                         SKY_DATA_TYPE_UINT, SKY_MEMBER_FLAG_READONLY,
            "lineno",    NULL,
                         offsetof(sky_python_symtable_block_data_t, lineno),
                         SKY_DATA_TYPE_INT, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(sky_python_symtable_block_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_python_symtable_block_type,
            "__repr__", sky_python_symtable_block_repr,
            NULL);
}
