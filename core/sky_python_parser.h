#ifndef __SKYTHON_CORE_SKY_PYTHON_PARSER_H__
#define __SKYTHON_CORE_SKY_PYTHON_PARSER_H__ 1


#include "sky_base.h"
#include "sky_object.h"

#include "sky_python_ast.h"
#include "sky_python_lexer.h"


SKY_CDECLS_BEGIN


typedef struct sky_python_parser_s {
    sky_python_lexer_t                  lexer;

    sky_python_token_t                  token_stack[5];
    sky_python_token_t *                token_sp;

    unsigned int                        flags;
} sky_python_parser_t;




/*  These are start rules for the grammar. Use cases for each are as follows:
 *
 *      file_input
 *          - a complete Python program (from file or string)
 *          - a module
 *          - a string passed to the exec() function
 *      interactive_input
 *          - interactive input
 *      eval_input
 *          - a string passed to the eval() function
 */

SKY_EXTERN sky_python_ast_Expression_t
sky_python_parser_eval_input(sky_python_parser_t *parser);

SKY_EXTERN sky_python_ast_Module_t
sky_python_parser_file_input(sky_python_parser_t *parser);

SKY_EXTERN sky_python_ast_Interactive_t
sky_python_parser_interactive_input(sky_python_parser_t *parser);


SKY_EXTERN void
sky_python_parser_init(sky_python_parser_t *parser,
                       sky_object_t         source,
                       sky_string_t         encoding,
                       sky_string_t         filename,
                       sky_string_t         ps1,
                       sky_string_t         ps2,
                       unsigned int         flags);

SKY_EXTERN sky_python_parser_t *
sky_python_parser_create(sky_object_t   source,
                         sky_string_t   encoding,
                         sky_string_t   filename,
                         sky_string_t   ps1,
                         sky_string_t   ps2,
                         unsigned int   flags);


SKY_EXTERN void
sky_python_parser_cleanup(sky_python_parser_t *parser);

SKY_EXTERN void
sky_python_parser_destroy(sky_python_parser_t *parser);



SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PYTHON_PARSER_H__ */
