/** @file
  * @brief
  * @defgroup sky_codec Codec Registry
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_CODEC_H__
#define __SKYTHON_CORE_SKY_CODEC_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Return the encoding used by the file system. **/
SKY_EXTERN sky_string_t
sky_codec_filesystemencoding(void);


/** The signature of a function to be called to output data during encoding or
  * decoding.
  *
  * @param[in]  bytes       an array of bytes.
  * @param[in]  nbytes      the number of bytes present in @a bytes.
  * @param[in]  width       the width of each character in @a bytes. This will
  *                         be one of 1, 2, or 4. If the output is Unicode, the
  *                         number of Unicode characters present in @a bytes is
  *                         @a nbytes / @a width. @a nbytes will always be
  *                         evenly divisible by @a width.
  * @param[in]  arg         the arbitrary pointer that was originally passed to
  *                         the encoding or decoding function.
  */
typedef void
        (*sky_codec_output_t)(const void *  bytes,
                              ssize_t       nbytes,
                              size_t        width,
                              void *        arg);


typedef struct sky_codec_s sky_codec_t;
typedef struct sky_codec_context_s sky_codec_context_t;

/** A structure containing information about a decoding or encoding operation.
  */
struct sky_codec_context_s {
    /** The codec definition that is in use. **/
    const sky_codec_t *                 codec;

    /** The input object being decoded or encoded. It may be @c NULL when
      * decoding.
      */
    sky_object_t                        input;
    /** The raw bytes of input. **/
    const void *                        bytes;
    /** The number of bytes present in the input. **/
    ssize_t                             nbytes;
    /** The width of characters in the input. One of 1, 2, or 4. **/
    size_t                              width;

    /** The name of the encoding that is in use. **/
    sky_string_t                        encoding;
    /** The name of the error handler that is in use. **/
    sky_string_t                        errors;
    /** For charmap encoding/decoding, the mapping to use. **/
    sky_object_t                        mapping;
    /** Cached exception object. Initially @c NULL. **/
    sky_object_t                        exception;

    /** The function to call to output data. **/
    sky_codec_output_t                  callback;
    /** An arbitrary pointer to pass to the output function. **/
    void *                              arg;

    /** Flags to control encoding. **/
    uint32_t                            flags;
};


/** Constants defining the flags that can be set for use by callers for
  * decoding.
  */
typedef enum sky_codec_decode_flag_e {
    /** The endianness of decoded data is big endian. Used on input if no BOM
      * is present (when applicable by the decoder), set on output if a BOM was
      * present indicating big endian.
      */
    SKY_CODEC_DECODE_FLAG_ENDIAN_BIG    =   0x1,
    /** The endianness of decoded data is little endian. Used on input if no
      * BOM is present (when application by the decoder), set on output if a
      * BOM was present indicating little endian.
      */
    SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE =   0x2,

    /** Require that a text codec is used. **/
    SKY_CODEC_DECODE_FLAG_REQUIRE_TEXT  =   0x10,
} sky_codec_decode_flag_t;


/** Constants defining the flags that can be set for decoder attributes.
  * These flag constants are used in codec definitions to define decoder
  * attributes.
  */
typedef enum sky_codec_decoder_flag_e {
    /** If string input is given, it should be encoded first using UTF-8. **/
    SKY_CODEC_DECODER_FLAG_ENCODE_STRING_INPUT  =   0x1,

    /** If string input is given, it should be returned as-is. **/
    SKY_CODEC_DECODER_FLAG_RETURN_STRING_INPUT  =   0x2,

    /** Default output from the decoder should by bytes instead of a string. **/
    SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES         =   0x4,
} sky_codec_decoder_flag_t;

/** The signature of a function to be called to initialize a context for
  * decoding.
  *
  * @param[in]      encoding    the encoding to use for decoding
  *                             (default is "utf-8" if @c NULL).
  * @param[in]      errors      the error handler to use for errors
  *                             (default is "strict" if @c NULL).
  * @param[in]      output      the function to call to output decoded data.
  * @param[in]      output_arg  an arbitrary pointer to be passed with every
  *                             call to @a output.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @return     an arbitrary pointer that will be passed to the decoding
  *             function.
  */
typedef void *
        (*sky_codec_decoder_initialize_t)(sky_string_t          encoding,
                                          sky_string_t          errors,
                                          sky_codec_output_t    output,
                                          void *                output_arg,
                                          uint32_t *            flags);

/** The signature of a function to be called to finalize a context after
  * decoding. The finalization function will always be called if the
  * initialization is successful, regardless of whether the decoding is
  * successful or not.
  *
  * @param[in]      state       the arbitrary pointer returned from the decoder
  *                             initialization function.
  * @param[in]      encoding    the encoding to use for decoding
  *                             (default is "utf-8" if @c NULL).
  * @param[in]      errors      the error handler to use for errors
  *                             (default is "strict" if @c NULL).
  * @param[in]      output      the function to call to output decoded data.
  * @param[in]      output_arg  an arbitrary pointer to be passed with every
  *                             call to @a output.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  */
typedef void
        (*sky_codec_decoder_finalize_t)(void *              state,
                                        sky_string_t        encoding,
                                        sky_string_t        errors,
                                        sky_codec_output_t  output,
                                        void *              output_arg,
                                        uint32_t *          flags);

/** The signature of a function to be called to reset the context during
  * decoding.
  *
  * @param[in]      state       the arbitrary pointer returned from the decoder
  *                             initialization function.
  * @param[in]      encoding    the encoding to use for decoding
  *                             (default is "utf-8" if @c NULL).
  * @param[in]      errors      the error handler to use for errors
  *                             (default is "strict" if @c NULL).
  * @param[in]      output      the function to call to output decoded data.
  * @param[in]      output_arg  an arbitrary pointer to be passed with every
  *                             call to @a output.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  */
typedef void
        (*sky_codec_decoder_reset_t)(void *             state,
                                     sky_string_t       encoding,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags);

/** The signature of a function to be called to decode data.
  *
  * @param[in]      state       the arbitrary pointer returned from the decoder
  *                             initialization function.
  * @param[in]      encoding    the encoding to use for decoding
  *                             (default is "utf-8" if @c NULL).
  * @param[in]      errors      the error handler to use for errors
  *                             (default is "strict" if @c NULL).
  * @param[in]      output      the function to call to output decoded data.
  * @param[in]      output_arg  an arbitrary pointer to be passed with every
  *                             call to @a output.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @param[in]      bytes       the bytes to be decoded.
  * @param[in]      nbytes      the number of bytes to be decoded.
  * @param[in]      final       @c SKY_TRUE if this is not an incremental
  *                             decode, in which case an exception should be
  *                             raised if only partial data is supplied.
  * @return         the number of bytes consumed from @a bytes.
  */
typedef ssize_t
        (*sky_codec_decoder_decode_t)(void *                state,
                                      sky_string_t          encoding,
                                      sky_string_t          errors,
                                      sky_codec_output_t    output,
                                      void *                output_arg,
                                      uint32_t *            flags,
                                      const void *          bytes,
                                      ssize_t               nbytes,
                                      sky_bool_t            final);

/** Constants defining the flags that can be set for encoder attributes. **/
typedef enum sky_codec_encode_flag_e {
    /** Output is variable-width characters. **/
    SKY_CODEC_ENCODE_FLAG_WIDTH_VARIABLE    =   0x0,
    /** Output is characters 1 byte wide. **/
    SKY_CODEC_ENCODE_FLAG_WIDTH_1           =   0x1,
    /** Output is characters 2 bytes wide. **/
    SKY_CODEC_ENCODE_FLAG_WIDTH_2           =   0x2,
    /** Output is characters 4 bytes wide. **/
    SKY_CODEC_ENCODE_FLAG_WIDTH_4           =   0x4,
    /** Mask to determine character width from flags. **/
    SKY_CODEC_ENCODE_FLAG_WIDTH_MASK        =   0x7,

    /** The low 7 bits of the first byte of a character are ASCII equivalent. **/
    SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII       =   0x10,
    /** Set this if non-string input should be decoded as UTF-8 into a buffer. **/
    SKY_CODEC_ENCODE_FLAG_DECODE_BUFFER_INPUT   =   0x20,
    /** Set this if input is required to support the buffer interface (as
      * opposed to string objects, which is the normal input type for encoding.
      */
    SKY_CODEC_ENCODE_FLAG_BUFFER_INPUT_REQUIRED =   0x40,
} sky_codec_encode_flag_t;

/** The signature of a function to be called to initialize a context for
  * encoding.
  */
typedef void (*sky_codec_encode_initialize_t)(sky_codec_context_t *context);

/** The signature of a function to be called to finalize a context after
  * encoding. The finalization function will always be called if the
  * initialization is successful, regardless of whether the encoding is
  * successful or not.
  */
typedef void (*sky_codec_encode_finalize_t)(sky_codec_context_t *context);

/** The signature of a function to be called to reset the context during
  * encoding.
  */
typedef void (*sky_codec_encode_reset_t)(sky_codec_context_t *context);

/** The signature of a function to be called to encode data.
  *
  * @return the number of bytes that were output.
  */
typedef void (*sky_codec_encode_update_t)(sky_codec_context_t *context);

/** A structure defining a built-in codec. **/
struct sky_codec_s {
    /** The name of the codec. **/
    const char *                        name;

    /** Attributes of the decoder. **/
    unsigned int                        decoder_flags;
    /** The function to call to initialize a decoding operation.
      * May be @c NULL.
      */
    sky_codec_decoder_initialize_t      decoder_initialize;
    /** The function to call to finalize a decoding operation.
      * May be @c NULL.
      */
    sky_codec_decoder_finalize_t        decoder_finalize;
    /** The function to call to reset a decoding operation.
      * May be @c NULL.
      */
    sky_codec_decoder_reset_t           decoder_reset;
    /** The function to call to decode. Must not be @c NULL. **/
    sky_codec_decoder_decode_t          decoder_decode;

    /** Attributes of the encoder. **/
    unsigned int                        encode_flags;
    /** The function to call to initialize an encoding operation.
      * May be @c NULL.
      */
    sky_codec_encode_initialize_t       encode_initialize;
    /** The function to call to finalize an encoding operation.
      * May be @c NULL.
      */
    sky_codec_encode_finalize_t         encode_finalize;
    /** The function to call to reset an encoding operation.
      * May be @c NULL.
      */
    sky_codec_encode_reset_t            encode_reset;
    /** The function to call to encode. Must not be @c NULL. **/
    sky_codec_encode_update_t           encode_update;
};


/** The definition for the ASCII built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_ascii;

/** The definition for the charmap built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_charmap;

/** The definition for the escape built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_escape;

/** The definition for the iso8859-1 built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_latin1;

/** The definition for the locale built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_locale;

/** The definition for the raw-unicode-escape built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_raw_unicode_escape;

/** The definition for the unicode-escape built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_unicode_escape;

/** The definition for the unicode-internal built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_unicode_internal;

/** The definition for the utf-16 built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf16;

/** The definition for the utf-16-be built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf16_be;

/** The definition for the utf-16-le built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf16_le;

/** The definition for the utf-32 built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf32;

/** The definition for the utf-32-be built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf32_be;

/** The definition for the utf-32-le built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf32_le;

/** The definition for the utf-7 built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf7;

/** The definition for the utf-8 built-in codec. **/
SKY_EXTERN sky_codec_t const * const    sky_codec_utf8;


/** Convert an input object to the appropriate type for decoding with a codec.
  *
  * @param[in]  codec           the codec definition to use for decoding. May
  *                             be @c NULL, in which case @a input will be
  *                             returned unchanged.
  * @param[in]  input           the input object to be decoded. This is the
  *                             object that may be converted if necessary per
  *                             the rules set by @a codec. If no conversion is
  *                             needed, the object will be returned as-is.
  * @return     an object suitable for use as a source of data for decoding,
  *             which may be @a input or some other object derived from it.
  */
SKY_EXTERN sky_object_t
sky_codec_decode_prepareinput(const sky_codec_t *codec, sky_object_t input);


/** Decode a stream of bytes into a string object.
  *
  * @param[in]      codec       the codec definition to use for decoding.
  *                             May be @c NULL, in which case @a encoding will
  *                             be used. Otherwise, @a encoding is ignored.
  * @param[in]      encoding    the encoding to use to decode @a input (default
  *                             is UTF-8 if @a codec and @a encoding are both
  *                             @c NULL).
  * @param[in]      errors      the error handler to use for errors (default is
  *                             "strict" if @c NULL).
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @param[in]      input       the source of bytes to be decoded, which may be
  *                             any object supporting the buffer protocol.
  * @return         the object resulting from decoding @a input, which is
  *                 normally a string, but may also be a bytes object for some
  *                 codecs.
  */
SKY_EXTERN sky_object_t
sky_codec_decode(const sky_codec_t *codec,
                 sky_string_t       encoding,
                 sky_string_t       errors,
                 uint32_t *         flags,
                 sky_object_t       input);


/** Decode a filesystem name into a string object.
  * This function will use the appropriate encoding and error handling for the
  * operating system to decode a bytes-like object (any object supporting the
  * buffer interface) into a Unicode string.
  *
  * @param[in]  filename    the filename to decode.
  * @return     the string resulting from decoding @a input.
  */
SKY_EXTERN sky_string_t
sky_codec_decodefilename(sky_object_t filename);


/** Decode an array of bytes using a specified encoding, making callbacks to
  * communicate runs of decoded data.
  *
  * @param[in]      codec       the codec definition to use for decoding. May
  *                             be @c NULL, in which case @a encoding will be
  *                             used. Otherwise, @a encoding is ignored.
  * @param[in]      encoding    the encoding to use to decode @a bytes
  *                             (default is "UTF-8" if @a codec and @a encoding
  *                             are both @c NULL).
  * @param[in]      errors      the error handler to use for errors
  *                             (default is "strict" if @c NULL).
  * @param[in]      output      the callback function to use to communicate the
  *                             decoded data back to the caller.
  * @param[in]      output_arg  an arbitrary pointer to pass to each call made
  *                             to @a callback.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @param[in]      bytes       the array of bytes to be decoded.
  * @param[in]      nbytes      the number of bytes present in @a bytes.
  */
SKY_EXTERN void
sky_codec_decodewithcallback(const sky_codec_t *codec,
                             sky_string_t       encoding,
                             sky_string_t       errors,
                             sky_codec_output_t output,
                             void *             output_arg,
                             uint32_t *         flags,
                             const void *       bytes,
                             ssize_t            nbytes);


/** Convert an input object to the appropriate type for encoding with a codec.
  *
  * @param[in]  codec           the codec definition to use for encoding. May
  *                             be @c NULL, in which case @a input will be
  *                             returned unchanged.
  * @param[in]  input           the input object to be encoded. This is the
  *                             object that may be converted if necessary per
  *                             the rules set by @a codec. If no conversion is
  *                             needed, the object will be returned as-is.
  * @return     an object suitable for use as a source of data for encoding,
  *             which may be @a input or some other object derived from it.
  */
SKY_EXTERN sky_object_t
sky_codec_encode_prepareinput(const sky_codec_t *codec, sky_object_t input);


/** Encode a Unicode string using a specified encoding, resulting in a bytes
  * object.
  *
  * @param[in]  codec       the codec definition to use for encoding. May be
  *                         @c NULL, in which case @a encoding will be used.
  *                         Otherwise, @a encoding is ignored.
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  encoding    the encoding to use to encode @a input (default is
  *                         UTF-8 if @a codec and @a encoding are both @c NULL).
  * @param[in]  errors      the error handler to use for errors (default is
  *                         "strict" if @c NULL).
  * @param[in]  flags       codec-specific flags.
  * @return     a new object instance containing the encoded data.
  */
SKY_EXTERN sky_object_t
sky_codec_encode(const sky_codec_t *codec,
                 sky_object_t       input,
                 sky_string_t       encoding,
                 sky_string_t       errors,
                 uint32_t           flags);


/** Encode a Unicode string into a filesystem name.
  * This function will use the appropriate encoding and error handling for the
  * operating system to encode a Unicode string into a bytes object that can be
  * used in system calls requiring a filename.
  *
  * @param[in]  input       the Unicode string to decode.
  * @return     a new object instance containing the encoded data.
  */
SKY_EXTERN sky_bytes_t
sky_codec_encodefilename(sky_string_t input);


/** Encode a Unicode string into a pre-allocated buffer of bytes.
  * If the buffer is too small, as much data as is possible will be written
  * into it before @c sky_OverflowError is raised. Note that this could result
  * in an incomplete character encoding at the end of the buffer. This function
  * will not write a trailing null character into the output.
  *
  * @param[in]  codec       the codec definition to use for encoding. May be
  *                         @c NULL, in which case @a encoding will be used.
  *                         Otherwise, @a encoding is ignored.
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  encoding    the encoding to use to encode @a input (default is
  *                         UTF-8 if @a codec and @a encoding are both @c NULL).
  * @param[in]  errors      the error handler to use for errors (default is
  *                         "strict" if @c NULL).
  * @param[in]  flags       encoder flags to use, typically 0.
  * @param[in]  bytes       the buffer to write into, pre-allocated by the
  *                         caller.
  * @param[in]  nbytes      the size of @a bytes in bytes.
  * @return     the number of bytes written into @a bytes.
  */
SKY_EXTERN ssize_t
sky_codec_encodeintocbytes(const sky_codec_t *  codec,
                           sky_object_t         input,
                           sky_string_t         encoding,
                           sky_string_t         errors,
                           uint32_t             flags,
                           void *               bytes,
                           ssize_t              nbytes);


/** Encode a Unicode string into a dynamically allocated buffer of bytes.
  * The memory into which the encoded data will be written is dynamically
  * allocated and must be deallocated by the caller using sky_free(). This
  * function will not write a trailing null character into the output.
  *
  * @param[in]  codec       the codec definition to use for encoding. May be
  *                         @c NULL, in which case @a encoding will be used.
  *                         Otherwise, @a encoding is ignored.
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  encoding    the encoding to use to encode @a input (default is
  *                         UTF-8 if @a codec and @a encoding are both @c NULL).
  * @param[in]  errors      the error handler to use for errors (default is
  *                         "strict" if @c NULL).
  * @param[in]  flags       encoder flags to use, typically 0.
  * @param[out] nbytes      the number of bytes written into the allocated
  *                         buffer.
  * @return     a pointer to the dynamically allocated buffer that contains the
  *             encoded data, which must be deallocated by the caller using
  *             sky_free().
  */
SKY_EXTERN uint8_t *
sky_codec_encodetocbytes(const sky_codec_t *codec,
                         sky_object_t       input,
                         sky_string_t       encoding,
                         sky_string_t       errors,
                         uint32_t           flags,
                         ssize_t *          nbytes);


/** Encode a Unicode string using UTF-8, resulting in a C-style string.
  * Unicode strings can always be encoded to UTF-8 without error, so there's no
  * need to specify how to handle errors. The UTF-8 codec does not presently
  * support any flags, so @a flags should always be 0. The returned C-style
  * string will be dynamically allocated with sky_malloc(), so it should be
  * freed by the caller using sky_free() when it's no longer needed.
  *
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  flags       codec-specific flags. Should always be 0.
  */
SKY_EXTERN char *
sky_codec_encodetocstring(sky_object_t  input,
                          uint32_t      flags);


/** Encode a Unicode string using a specified encoding, making callbacks to
  * communicate runs of encoded data.
  *
  * @param[in]  codec       the codec definition to use for encoding. May be
  *                         @c NULL, in which case @a encoding will be used.
  *                         Otherwise, @a encoding is ignored.
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  encoding    the encoding to use to encode @a input (default is
  *                         UTF-8 if @a codec and @a encoding are both @c NULL).
  * @param[in]  errors      the error handler to use for errors (default is
  *                         "strict" if @c NULL).
  * @param[in]  flags       codec-specific flags.
  * @param[in]  callback    the callback function to use to communicate the
  *                         encoded data back to the caller.
  * @param[in]  arg         an arbitrary pointer to pass to each call made
  *                         to @a callback.
  */
SKY_EXTERN void
sky_codec_encodewithcallback(const sky_codec_t *codec,
                             sky_object_t       input,
                             sky_string_t       encoding,
                             sky_string_t       errors,
                             uint32_t           flags,
                             sky_codec_output_t callback,
                             void *             arg);


/** Create a character mapping table for encoding.
  * Given a string that is at most 256 characters in length, create a mapping
  * from source to target characters. The source character is the index
  * position in the string, and the target character is the character in that
  * position.
  * 
  * @param[in]  string      a string that contains character mappings from
  *                         the index position to the character in the index
  *                         position.
  * @return     an object representing a character mapping that can be used
  *             for encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_charmap_build(sky_string_t string);


/** Decode a stream of bytes into a string object using a character mapping.
  *
  * @param[in]      mapping     the character mapping to use to decode @a input.
  * @param[in]      errors      the error handler to use for errors (default is
  *                             "strict" if @c NULL).
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @param[in]      input       the source of bytes to be decoded, which may be
  *                             any object supporting the buffer protocol.
  * @return     the string resulting from decoding @a input.
  */
SKY_EXTERN sky_string_t
sky_codec_charmap_decode(sky_object_t   mapping,
                         sky_string_t   errors,
                         uint32_t *     flags,
                         sky_object_t   input);


/** Decode an array of bytes using a character mapping, making callbacks to
  * communicate runs of decoded data.
  *
  * @param[in]      mapping     the character mapping to use to decode @a bytes.
  * @param[in]      errors      the error handler to use for errors (default is
  *                             "strict" if @c NULL).
  * @param[in]      output      the callback function to use to communicate the
  *                             decoded data back to the caller.
  * @param[in]      output_arg  an arbitrary pointer to pass to each call made
  *                             to @a output.
  * @param[in,out]  flags       flags for decoding, which may be altered by the
  *                             decoder to reflect what was actually done.
  * @param[in]      bytes       the array of bytes to be decoded.
  * @param[in]      nbytes      the number of bytes present in @a nbytes.
  */
SKY_EXTERN void
sky_codec_charmap_decodewithcallback(sky_object_t       mapping,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags,
                                     const void *       bytes,
                                     ssize_t            nbytes);


/** Encode a Unicode string using a character mapping, resulting in a bytes
  * object.
  *
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  errors      how to handle encoding errors, which may be
  *                         @c NULL to use the default ("strict").
  * @param[in]  mapping     the character mapping to use.
  * @return     a new @c sky_bytes object instance containing the encoded data.
  */
SKY_EXTERN sky_bytes_t
sky_codec_charmap_encode(sky_object_t   input,
                         sky_string_t   errors,
                         sky_object_t   mapping);


/** Encode a Unicode string using a character mapping, making callbacks to
  * communicate runs of encoded data.
  *
  * @param[in]  input       the Unicode string to be encoded.
  * @param[in]  errors      how to handle encoding errors, which may be
  * @param[in]  mapping     the character mapping to use.
  *                         @c NULL to use the default ("strict").
  * @param[in]  callback    the callback function to use to communicate the
  *                         encoded data back to the caller.
  * @param[in]  arg         an arbitrary pointer to pass to each call made
  *                         to @a callback.
  */
SKY_EXTERN void
sky_codec_charmap_encodewithcallback(sky_object_t       input,
                                     sky_string_t       errors,
                                     sky_object_t       mapping,
                                     sky_codec_output_t callback,
                                     void *             arg);


/** Return a stateless decoder object for an encoding.
  * The returned decoder object is callable, requiring two arguments: the
  * bytes object (or any object supporting the buffer protocol) to decode and
  * the error handler to use. The return shall be a string object.
  *
  * @param[in]  encoding    the encoding.
  * @return     a stateless decoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_decoder(sky_string_t encoding);


/** Return an incremental (stateful) decoder object for an encoding.
  * The returned decoder object is callable, requiring two arguments: the
  * bytes object (or any object supporting the buffer protocol) to decode and
  * the error handler to use. The return shall be a string object.
  *
  * @param[in]  codec       the CodecInfo object as returned by either
  *                         sky_codec_lookup() or sky_codec_lookup_text().
  * @param[in]  errors      how to handle decoding errors, which may be
  *                         @c NULL to use the default ("strict").
  * @return     an incremental decoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_incremental_decoder(sky_object_t  codec,
                              sky_string_t  errors);


/** Return a stateless text decoder object for an encoding.
  * The returned decoder object is callable, requiring two arguments: the
  * bytes object (or any object supporting the buffer protocol) to decode and
  * the error handler to use. The return shall be a string object. This is the
  * same as sky_codec_decoder(), except that it will raise @c sky_LookupError
  * if the encoding is not a text encoding.
  *
  * @param[in]  encoding    the encoding.
  * @return     a stateless decoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_text_decoder(sky_string_t encoding);


/** Return a stateless encoder object for an encoding.
  * The returned encoder object is callable, requiring two arguments: the
  * string object to encode and the error handler to use. The return shall be
  * a bytes object.
  *
  * @param[in]  encoding    the encoding.
  * @return     a stateless encoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_encoder(sky_string_t encoding);


/** Return an incremental (stateful) encoder object for an encoding.
  * The returned encoder object is callable, requiring two arguments: the
  * string object to encode and the error handler to use. The return shall be
  * a bytes object.
  *
  * @param[in]  codec       the CodecInfo object as returned by either
  *                         sky_codec_lookup() or sky_codec_lookup_text().
  * @param[in]  errors      how to handle encoding errors, which may be
  *                         @c NULL to use the default ("strict").
  * @return     an incremental encoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_incremental_encoder(sky_object_t  codec,
                              sky_string_t  errors);


/** Return a stateless encoder object for an encoding.
  * The returned encoder object is callable, requiring two arguments: the
  * string object to encode and the error handler to use. The return shall be
  * a bytes object. This is the same as sky_codec_encoder(), except that it
  * will raise @c sky_LookupError if the encoding is not a text encoding.
  *
  * @param[in]  encoding    the encoding.
  * @return     a stateless encoder object for @a encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_text_encoder(sky_string_t encoding);


/** Register a codec search function.
  * Register a codec search function. Search functions are expected to take one
  * argument: the encoding name in all lower case letters, and return a
  * @c CodecInfo object having the following attributes:
  *
  * @li @c name The name of the encoding.
  * @li @c encode The stateless encoding function.
  * @li @c decode The stateless decoding function.
  * @li @c incrementalencoder An incremental encoder class or factory function.
  * @li @c incrementaldecoder An incremental decoder class or factory function.
  * @li @c streamwriter A stream writer class or factory function.
  * @li @c streamreader A stream reader class or factory function.
  *
  * @param[in]  search_function     the search function to register, which
  *                                 should be a callable object, typically a
  *                                 @c sky_function_type object.
  */
SKY_EXTERN void
sky_codec_register(sky_object_t search_function);


/** Lookup a codec for an encoding.
  * Looks up the codec information in the codec registry and returns a
  * @c CodecInfo object as defined by sky_codec_register().
  *
  * Encodings are first looked up in the registry's cache. If not found, the
  * list of registered search functions is scanned. If no @c CodecInfo object
  * is found, a @c sky_LookupError is raised. Otherwise, the @c CodecInfo
  * object is stored in the cache and returned to the caller.
  *
  * @param[in]  encoding    the encoding name to search for.
  * @return     a @c CodecInfo object containing the information stored in the
  *             codec registry for the encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_lookup(sky_string_t encoding);


/** Lookup a codec for an encoding.
  * Looks up the codec information in the codec registry and returns a
  * @c CodecInfo object as defined by sky_codec_register().
  *
  * This is the same as sky_codec_lookup(), except that it will raise
  * @c sky_LookupError if the encoding is not a text encoding.
  *
  * @param[in]  encoding    the encoding name to search for.
  * @param[in]  alternate   a C-style string indicating the proper way to use
  *                         an arbitrary (non-text) encoding.
  * @return     a @c CodecInfo object containing the information stored in the
  *             codec registry for the encoding.
  */
SKY_EXTERN sky_object_t
sky_codec_lookup_text(sky_string_t encoding, const char *alternate);


/** Register an error handling function.
  * Register the error handling function @a error_handler under the name
  * @a name. @a error_handler will be called during encoding and decoding in
  * case of an error, when @a name is specified as the errors parameter.
  *
  * For encoding @a error_handler will be called with a
  * @c sky_UnicodeEncodeError instance, which contains information about the
  * location of the error. The error handler must either raise this or a
  * different exception or return a tuple with a replacement for the
  * unencodable part of the input and a position where encoding should
  * continue. The replacement may be either a @c sky_string_t or @c sky_bytes_t
  * object. If the replacement is bytes, the encoder will simply copy them into
  * the output buffer. If the replacement is a string, the encoder will encode
  * the replacement. Encoding continues on original input at the specified
  * position. Negative position values will be treated as being relative to the
  * end of the input string. If the resulting position is out of bound a
  * @c sky_IndexError will be raised.
  *
  * Decoding and translating works similarly, except @c sky_UnicodeDecodeError
  * or @c sky_UnicodeTranslateError will be passed to the handler and that the
  * replacement from the error handler will be put into the output directly.
  *
  * @param[in]  name            the name of the error handler.
  * @param[in]  error_handler   the error handler function, which should be a
  *                             callable object, typically a
  *                             @c sky_function_type object.
  */
SKY_EXTERN void
sky_codec_register_error(sky_string_t   name,
                         sky_object_t   error_handler);


/** Lookup an error handler.
  * Return the error handler previously registered under the name @a name.
  * Raises @c sky_LookupError if @a name is not registered.
  *
  * @param[in]  name    the name to search for.
  * @return     the error handler previously registered as @a name.
  */
SKY_EXTERN sky_object_t
sky_codec_lookup_error(sky_string_t name);


/** Constants representing flags supported by several encoders. **/
typedef enum sky_codec_encoder_flag_e {
    /** Include a byte-order-marker (BOM) **/
    SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM      =   0x1,
} sky_codec_encoder_flag_t;


/** Constants representing flags supported by the UTF-7 codec for encoding. **/
typedef enum sky_codec_utf7_encoder_flag_e {
    /** Directly encode Set O (optional) characters. **/
    SKY_CODEC_UTF7_ENCODER_FLAG_OPTIONAL    =   0x2,
    /** Directly encode whitespace (space, tab, cr, lf) characters. **/
    SKY_CODEC_UTF7_ENCODER_FLAG_WHITESPACE  =   0x4,
} sky_codec_utf7_encoder_flag_t;


/** Determine if an encoding is a built-in encoding.
  * If the encoding name is not recognized as a built-in codec, the returned
  * codec constant will be @c NULL. The codec registry is not consulted.
  *
  * @param[in]  encoding    the encoding name to lookup.
  * @return     the codec definition for the requested built-in codec, or
  *             @c NULL if the encoding is not known to be a built-in codec.
  */
SKY_EXTERN const sky_codec_t *
sky_codec_builtin(sky_string_t encoding);


/** The signature of a function to be called for a native code object if the
  * implementation of an error handler callable object is native code.
  *
  * @param[in]  exc     the exception raised during encoding, decoding, or
  *                     translation.
  * @return     a 2-tuple containing the replacement bytes or string object
  *             and the location where encoding, decoding, or translation is
  *             to continue.
  */
typedef sky_tuple_t
        (*sky_codec_error_function_t)(sky_object_t exc);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_CODEC_H__ */

/** @} **/
