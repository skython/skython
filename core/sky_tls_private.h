#ifndef __SKYTHON_CORE_SKY_TLS_PRIVATE_H__
#define __SKYTHON_CORE_SKY_TLS_PRIVATE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


typedef struct sky_tls_tlsdata_s {
    void *                              tlskey_data[255];
} sky_tls_tlsdata_t;


extern void
sky_tls_cleanup_tlsdata(sky_tls_tlsdata_t *tlsdata);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_TLS_PRIVATE_H__ */
