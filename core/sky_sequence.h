/** @file
  * @brief Functions for working with sequence objects
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_sequence Sequence Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SEQUENCE_H__
#define __SKYTHON_CORE_SKY_SEQUENCE_H__ 1


#include "sky_base.h"
#include "sky_asset.h"
#include "sky_object.h"
#include "sky_type.h"


SKY_CDECLS_BEGIN


/** A structure to maintain state during fast sequence iteration.
  * This structure is passed to every call of sky_sequence_iterate() and
  * sky_sequence_iterate_cleanup(). Types implementing those functions are
  * expected to manipulate the structure.
  */
typedef struct sky_sequence_iterate_state_s {
    /** This is expected to be used as a state counter of a sort.
      * Implementations are free to store any value they need to in this field.
      */
    ssize_t                             state;
    /** This is the sequence object being iterated.
      * This field is set before sky_sequence_iterate() is called for the first
      * time, but it is done without using sky_object_gc_set().
      */
    sky_object_t                        sequence;
    /** This is an array of objects to be iterated.
      * Implementations are expected to fill this field in. If it is set to
      * @c NULL, iteration will terminate.
      */
    sky_object_t *                      objects;
    /** This is the number of valid objects stored in the array of objects.
      * Implementations are expected to fill this field in along with the
      * objects field. If it is less than or equal to zero, iteration will
      * terminate.
      */
    ssize_t                             nobjects;
    /** This is a set of pointer-sized integers that may be used by
      * implementations to store any data needed.
      */
    uintptr_t                           extra[5];
} sky_sequence_iterate_state_t;


/** The signature of a function called to perform fast iteration of a sequence.
  * The first time that this function is called for an iteration, all members
  * of the @a state structure will be zeroed out. Implementations are expected
  * to fill in the @c objects and @c nobjects fields of the @a state structure
  * for iteration to continue. When no more objects remain to be iterated, the
  * @c objects field should be set to @c NULL or the @c nobjects field should
  * be set to some value less than or equal to 0.
  *
  * The @a objects and @a nobjects parameters are used to pass a buffer for
  * objects to be copied to for those types implementing fast iteration that
  * do not store their elements in contiguous order. Implementations may fill
  * in the @a objects array (that has room for @a nobjects elements at a time)
  * and point the @a state structure's @c objects field to @a objects.
  *
  * @param[in]  sequence    the sequence object to iterate.
  * @param[in]  data        the object's instance data for the type in the MRO
  *                         for which the iterate function is being called.
  * @param[in]  state       the fast iterate state structure for the iteration.
  * @param[in]  objects     a static array of objects that implementations may
  *                         use.
  * @param[in]  nobjects    the number of objects that @a objects has room for.
  */
typedef void
        (*sky_sequence_iterate_t)(sky_object_t                  sequence,
                                  void *                        data,
                                  sky_sequence_iterate_state_t *state,
                                  sky_object_t *                objects,
                                  ssize_t                       nobjects);


/** The signature of a function called to clean up a fast iterate state.
  * This function will always be called when a fast iteration completes,
  * regardless of whether an exception is raised or not.
  *
  * @param[in]  sequence    the sequence object being iterated.
  * @param[in]  data        the object's instance data for the type in the MRO
  *                         for which the cleanup function is being called.
  * @param[in]  state       the fast iteration state to clean up.
  */
typedef void
        (*sky_sequence_iterate_cleanup_t)(sky_object_t                  sequence,
                                          void *                        data,
                                          sky_sequence_iterate_state_t *state);


/** Begin a fast sequence iteration block.
  * The block of code that is placed between the beginning of a fast iteration
  * and its end is free to do most anything that it needs to; however, there
  * are some restrictions. First, @c break and @c continue statements will both
  * have the same effect, which is to continue to the next object in the
  * sequence. Second, it is not legal to use @c return or @c goto outside of
  * the iteration block. Doing so will have undefined (and likely catastrophic)
  * results. It is absolutely legal to raise an exception within the block.
  * This is a macro; however, it does guarantee that @a sequence will only be
  * evaluated once.
  *
  * @param[in]  sequence    the sequence to iterate.
  * @param[out] object      the variable to which each object in the iteration
  *                         will be assigned.
  */
#define SKY_SEQUENCE_FOREACH(sequence, object)                              \
        SKY_ASSET_BLOCK_BEGIN {                                             \
            sky_object_t                    _seq   = sequence;              \
            sky_sequence_iterate_state_t    _state = { 0, _seq, NULL, 0,    \
                                                       { 0, 0, 0, 0, 0 } }; \
                                                                            \
            ssize_t         _i;                                             \
            sky_bool_t      _foreach_break = SKY_FALSE;                     \
            sky_object_t    _objects[16];                                   \
                                                                            \
            sky_sequence_iterate(_seq, &_state, _objects, 16);              \
            sky_asset_save(&_state,                                         \
                           (sky_free_t)sky_sequence_iterate_cleanup,        \
                           SKY_ASSET_CLEANUP_ALWAYS);                       \
            while (_state.objects && _state.nobjects > 0) {                 \
                for (_i = 0; _i < _state.nobjects; ++_i) {                  \
                    object = _state.objects[_i];                            \
                    do {

/** Break out of a fast sequence iteration block.
  * It is not legal to use @c break, @c continue, @c goto, or @c return while
  * inside a fast sequence iteration block because of unpredictable behavior.
  * If it is necessary to break out of a sequence iteration, use this macro
  * instead.
  */
#define SKY_SEQUENCE_FOREACH_BREAK                                          \
        do { _foreach_break = SKY_TRUE; } while (0)

/** End a fast sequence iteration block. **/
#define SKY_SEQUENCE_FOREACH_END                                            \
                    } while (0);                                            \
                    if (_foreach_break) {                                   \
                        break;                                              \
                    }                                                       \
                }                                                           \
                if (_foreach_break) {                                       \
                    break;                                                  \
                }                                                           \
                sky_sequence_iterate(_seq, &_state, _objects, 16);          \
            }                                                               \
        } SKY_ASSET_BLOCK_END


/** Determine if all of a sequence's contained objects are True.
  * To determine an object's truth value, sky_object_bool() is used. Iteration
  * of the sequence is stopped at the first object that is False.
  *
  * @param[in]  sequence    the sequence to check.
  * @return     @c SKY_TRUE if all objects contained by @a sequence have a
  *             truth value of True; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_sequence_all(sky_object_t sequence);


/** Determine if any of a sequence's contained objects are True.
  * To determine an object's truth value, sky_object_bool() is used. Iteration
  * of the sequence is stopped at the first object that is True.
  *
  * @param[in]  sequence    the sequence to check.
  * @return     @c SKY_TRUE if any objects contained by @a sequence have a
  *             truth value of True; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_sequence_any(sky_object_t sequence);


/** Check to see if an object is a sequence.
  * A sequence is any object except a dictionary that implements __getitem__().
  *
  * @param[in]  object      the object to query.
  * @return     @c SKY_TRUE if the object is a sequence, or
  *             @c SKY_FALSE if it is not.
  */
SKY_EXTERN sky_bool_t
sky_sequence_check(sky_object_t object);


/** Concatenate two sequences.
  *
  * @param[in]  sequence    the first sequence in the concatenation.
  * @param[in]  other       the other sequence in the concatenation.
  * @return     a new sequence that is @a sequence and @a other concatenated.
  */
SKY_EXTERN sky_object_t
sky_sequence_concatenate(sky_object_t sequence, sky_object_t other);


/** Count the number of occurrences of an object in a sequence.
  *
  * @param[in]  sequence    the sequence to count from.
  * @param[in]  object      the object to count.
  * @return     the number of times an object equivalent to @a object is found
  *             in the sequence @a sequence.
  */
SKY_EXTERN ssize_t
sky_sequence_count(sky_object_t sequence, sky_object_t object);


/** Return the index position of the first occurrence of an object in a
  * sequence.
  *
  * @param[in]  sequence    the sequence to search.
  * @param[in]  object      the object to search for.
  * @return     the index position, counted from zero, of the first occurrence
  *             of @a object in @a sequence. An exception will be raised if
  *             @a object is not found.
  */
SKY_EXTERN ssize_t
sky_sequence_index(sky_object_t sequence, sky_object_t object);


/** Normalize slice parameters to the specified length.
  *
  * @param[in]      length  the length to which the parameters are to be
  *                         normalized.
  * @param[in,out]  start   the starting index, inclusive.
  * @param[in,out]  stop    the stopping index, exclusive.
  * @param[in,out]  step    the step.
  * @return         the actual length of the slice, which will always be >= 0,
  *                 and never be > @a length.
  */
SKY_EXTERN ssize_t
sky_sequence_indices(ssize_t    length,
                     ssize_t *  start,
                     ssize_t *  stop,
                     ssize_t *  step);


/** Concatenate two sequences in place.
  *
  * @param[in]  sequence    the first sequence in the concatenation.
  * @param[in]  other       the other sequence in the concatenation.
  * @return     a sequence that is the concatenation of @a sequence and
  *             @a other, which may be @a sequence if that type supports
  *             extension; otherwise, it'll be a new sequence.
  */
SKY_EXTERN sky_object_t
sky_sequence_inplace_concatenate(sky_object_t sequence, sky_object_t other);


/** Determine whether an object supports fast iteration.
  * If sky_sequence_iterate() or @c SKY_SEQUENCE_FOREACH can be used, the
  * return will be true; otherwise it will be false. A false return does not
  * necessarily mean that the object is not at all iterable. It may still be
  * iterable via sky_object_iter().
  *
  * @param[in]  sequence    the sequence object to check.
  * @return     @c SKY_TRUE if @a sequence supports fast iteration, or
  *             @c SKY_FALSE if it does not.
  */
SKY_EXTERN sky_bool_t
sky_sequence_isiterable(sky_object_t sequence);


/** Get the next set of objects for fast iteration.
  * This function should not normally be called directly. It is used to help
  * implement fast sequence iteration, and its signature is exactly the same
  * as that of @c sky_sequence_iterate_t, which is the signature of a function
  * to implement fast iteration for an object type. Instead of calling this
  * function directly, most code should use @c SKY_SEQUENCE_FOREACH.
  *
  * @param[in]  sequence    the sequence object to iterate.
  * @param[in]  state       the fast iterate state structure for the iteration.
  * @param[in]  objects     a static array of objects that implementations may
  *                         use.
  * @param[in]  nobjects    the number of objects that @a objects has room for.
  */
SKY_EXTERN void
sky_sequence_iterate(sky_object_t                   sequence,
                     sky_sequence_iterate_state_t * state,
                     sky_object_t *                 objects,
                     ssize_t                        nobjects);


/** Clean up a fast iteration state.
  * This function should not normally be called directly. It is used to help
  * implement fast sequence iteration. Instead of calling this function
  * directly, most code should use @c SKY_SEQUENCE_FOREACH.
  *
  * @param[in]  state   the fast iteration state to clean up.
  */
SKY_EXTERN void
sky_sequence_iterate_cleanup(sky_sequence_iterate_state_t *state);


/** Get an object at a specific index position in a sequence.
  *
  * @param[in]  sequence    the sequence to query.
  * @param[in]  index       the index position in the sequence. If the index
  *                         position is out of range, @c sky_IndexError will
  *                         be raised.
  * @return     the object at the requested index position in the list.
  */
SKY_EXTERN sky_object_t
sky_sequence_get(sky_object_t sequence, ssize_t index);


/** Return the largest value contained in a sequence.
  *
  * @param[in]  sequence    the sequence to inspect.
  * @param[in]  key         if not @c NULL or @c sky_None, a callable object
  *                         that will be called for each element in the
  *                         sequence with a single argument being that element,
  *                         the result of which will be used for comparison.
  * @return     the largest object in @a sequence.
  */
SKY_EXTERN sky_object_t
sky_sequence_max(sky_object_t sequence, sky_object_t key);


/** Return the smallest value contained in a sequence.
  *
  * @param[in]  sequence    the sequence to inspect.
  * @param[in]  key         if not @c NULL or @c sky_None, a callable object
  *                         that will be called for each element in the
  *                         sequence with a single argument being that element,
  *                         the result of which will be used for comparison.
  * @return     the largest object in @a sequence.
  */
SKY_EXTERN sky_object_t
sky_sequence_min(sky_object_t sequence, sky_object_t key);


/** Return the sum of all objects in a sequence.
  *
  * @param[in]  sequence    the sequence.
  * @param[in]  start       the starting value for the sum, or @c NULL to
  *                         default to 0.
  * @return     the sum of all values in the sequence plus @a start.
  */
SKY_EXTERN sky_object_t
sky_sequence_sum(sky_object_t sequence, sky_object_t start);


SKY_CDECLS_BEGIN


#endif  /* __SKYTHON_CORE_SKY_SEQUENCE_H__ */

/** @} **/
/** @} **/
