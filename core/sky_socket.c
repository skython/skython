#include "sky_private.h"
#include "sky_socket_private.h"
#include "sky_exceptions_private.h"

#include <math.h>
#include <netdb.h>


#define SKY_SOCKET_FD_SET(fd, _set)                 \
    do {                                            \
        int _f = (fd);                              \
                                                    \
        sky_error_validate_debug(_f > 0);           \
        sky_error_validate_debug(_f < FD_SETSIZE);  \
        FD_SET(_f, (_set));                         \
    } while (0)


#if defined(EAGAIN)
#   if defined(EWOULDBLOCK)
#       if EAGAIN == EWOULDBLOCK
#           define  SKY_SOCKET_WOULDBLOCK(_e) \
                    (EAGAIN == (_e))
#       else
#           define  SKY_SOCKET_WOULDBLOCK(_e) \
                    (EAGAIN == (_e) || EWOULDBLOCK == (_e))
#       endif
#   else
#       define  SKY_SOCKET_WOULDBLOCK(_e) \
                (EAGAIN == (_e))
#   endif
#elif defined(EWOULDBLOCK)
#   define  SKY_SOCKET_WOULDBLOCK(_e) \
            (EWOULDBLOCK == (_e))
#else
#   error   implementation missing
#endif


static double sky_socket_timeout_default = -1.0;


typedef struct sky_socket_data_s {
    int                                 fd;
    int                                 family;
    int                                 type;
    int                                 proto;
    double                              timeout;
} sky_socket_data_t;

SKY_EXTERN_INLINE sky_socket_data_t *
sky_socket_data(sky_object_t object)
{
    return sky_object_data(object, sky_socket_type);
}

struct sky_socket_s {
    sky_object_data_t                   object_data;
    sky_socket_data_t                   socket_data;
};


static void
sky_socket_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_socket_data_t   *self_data = data;

    if (self_data->fd != -1) {
        close(self_data->fd);
        self_data->fd = -1;
    }
}

static void
sky_socket_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_socket_data_t   *self_data = data;

    self_data->fd = -1;
    self_data->timeout = -1.0;
}


void
sky_socket_address_setipaddr(int                    family,     /* in   */
                             sky_object_t           address,    /* in   */
                             sky_socket_address_t * addr)       /* out  */
{
    const char  *cstring;

    if ((cstring = sky_string_cstring(address)) != NULL &&
        !strcmp(cstring, "<broadcast>"))
    {
        /* There is no broadcast address for IPv6 */
        if (AF_INET != family && AF_UNSPEC != family) {
            sky_error_raise_string(sky_OSError, "address family mismatched");
        }
        memset(&(addr->sockaddr_in), 0, sizeof(addr->sockaddr_in));
        addr->sockaddr_in.sin_family = AF_INET;
        addr->sockaddr_in.sin_addr.s_addr = INADDR_BROADCAST;
        return;
    }

    SKY_ASSET_BLOCK_BEGIN {
        int             rc;
        char            *hostname;
        ssize_t         nbytes;
        const char      *servname;
        struct addrinfo hints, *res;

        memset(&hints, 0, sizeof(hints));
        hints.ai_family = family;

        if (!sky_object_len(address)) {
            hints.ai_socktype = SOCK_DGRAM;
            hints.ai_flags = AI_PASSIVE;
            hostname = NULL;
            servname = "0";
        }
        else {
            servname = NULL;
            hostname = (char *)
                       sky_codec_encodetocbytes(NULL,
                                                address,
                                                SKY_STRING_LITERAL("idna"),
                                                NULL,
                                                0,
                                                &nbytes);
            hostname = sky_realloc(hostname, nbytes + 1);
            hostname[nbytes] = '\0';
            sky_asset_save(hostname, sky_free, SKY_ASSET_CLEANUP_ALWAYS);
        }

        /* RFC 3493 says that getaddrinfo() must be thread-safe, so for now
         * assume that all relevant implementations are compliant.
         */
        if ((rc = getaddrinfo(hostname, servname, &hints, &res)) != 0) {
            sky_socket_raise_gaierror(rc);
        }
        sky_asset_save(res,
                       (sky_free_t)freeaddrinfo,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (AF_INET != res->ai_family && AF_INET6 != res->ai_family) {
            sky_error_raise_string(sky_OSError,
                                   "unsupported address family");
        }
        if (!hostname && res->ai_next) {
            sky_error_raise_string(sky_OSError,
                                   "wildcard resolved to multiple address");
        }
        memcpy(addr, res->ai_addr, res->ai_addrlen);
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_socket_address_object(const sky_socket_address_t *addr)
{
    int     error;
    char    host[NI_MAXHOST];

    switch (addr->sockaddr.sa_family) {
        case AF_INET:
            if ((error = getnameinfo(&(addr->sockaddr),
                                     sizeof(struct sockaddr_in),
                                     host,
                                     sizeof(host),
                                     NULL,
                                     0,
                                     NI_NUMERICHOST)) != 0)
            {
                sky_socket_raise_gaierror(error);
            }
            return sky_object_build("(su16)",
                                    host,
                                    ntohs(addr->sockaddr_in.sin_port));
        case AF_INET6:
            if ((error = getnameinfo(&(addr->sockaddr),
                                     sizeof(struct sockaddr_in),
                                     host,
                                     sizeof(host),
                                     NULL,
                                     0,
                                     NI_NUMERICHOST)) != 0)
            {
                sky_socket_raise_gaierror(error);
            }
            return sky_object_build(
                            "(su16u32u32)",
                            host,
                            ntohs(addr->sockaddr_in6.sin6_port),
                            (unsigned)ntohl(addr->sockaddr_in6.sin6_flowinfo),
                            (unsigned)addr->sockaddr_in6.sin6_scope_id);
        case AF_UNIX:
            return sky_string_createfromfilename(
                            addr->sockaddr_un.sun_path,
                            strlen(addr->sockaddr_un.sun_path));
    }

    return sky_object_build("(iy#)",
                            addr->sockaddr.sa_family,
                            addr->sockaddr.sa_data,
                            sizeof(addr->sockaddr.sa_data));
}


void
sky_socket_address_sockaddr(int                     family,
                            sky_object_t            address,
                            sky_socket_address_t *  addr,
                            socklen_t *             addrlen)
{
    switch (family) {
        case AF_INET:
            if (!sky_object_isa(address, sky_tuple_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "AF_INET address must be tuple, not %@",
                        sky_type_name(sky_object_type(address)));
            }
            *addrlen = sizeof(struct sockaddr_in);
            sky_socket_address_setipaddr(AF_INET,
                                         sky_tuple_get(address, 0),
                                         addr);
            addr->sockaddr_in.sin_port =
                    htons(sky_integer_value(sky_tuple_get(address, 1),
                                            0, USHRT_MAX,
                                            NULL));
            break;
        case AF_INET6:
            if (!sky_object_isa(address, sky_tuple_type)) {
                sky_error_raise_format(
                        sky_TypeError,
                        "AF_INET6 address must be tuple, not %@",
                        sky_type_name(sky_object_type(address)));
            }
            *addrlen = sizeof(struct sockaddr_in6);
            sky_socket_address_setipaddr(AF_INET6,
                                         sky_tuple_get(address, 0),
                                         addr);
            addr->sockaddr_in6.sin6_port =
                    htons(sky_integer_value(sky_tuple_get(address, 1),
                                            0, USHRT_MAX,
                                            NULL));
            if (sky_tuple_len(address) > 2) {
                addr->sockaddr_in6.sin6_flowinfo =
                        htonl(sky_integer_value(sky_tuple_get(address, 2),
                                                0, 0xFFFFF,
                                                NULL));
                if (sky_tuple_len(address) > 3) {
                    addr->sockaddr_in6.sin6_scope_id =
                            sky_integer_value(sky_tuple_get(address, 3),
                                              0, UINT_MAX,
                                              NULL);
                }
            }
            break;
        case AF_UNIX:
            *addrlen = sizeof(addr->sockaddr_un);
            if (sky_object_isa(address, sky_string_type)) {
                address = sky_codec_encodefilename(address);
            }
            SKY_ASSET_BLOCK_BEGIN {
                sky_buffer_t    buffer;

                sky_buffer_acquire(&buffer,
                                   address,
                                   SKY_BUFFER_FLAG_SIMPLE);
                sky_asset_save(&buffer,
                               (sky_free_t)sky_buffer_release,
                               SKY_ASSET_CLEANUP_ALWAYS);

                if ((size_t)buffer.len >= sizeof(addr->sockaddr_un.sun_path)) {
                    sky_error_raise_string(sky_OSError,
                                           "AF_UNIX path too long");
                }
                memcpy(addr->sockaddr_un.sun_path, buffer.buf, buffer.len);
                addr->sockaddr_un.sun_path[buffer.len] = '\0';
                addr->sockaddr_un.sun_family = AF_UNIX;
            } SKY_ASSET_BLOCK_END;
            break;
        default:
            sky_error_raise_string(sky_OSError,
                                   "getsockaddrarg: bad family");
    }
}


static void
sky_socket_data_initialize(sky_socket_data_t *  self_data,
                           int                  fd,
                           int                  family,
                           int                  type,
                           int                  proto)
{
    self_data->fd = fd;
    self_data->family = family;
    self_data->type = type;
    self_data->proto = proto;
    self_data->timeout = sky_socket_timeout_default;
    fcntl(self_data->fd, F_SETFL, fcntl(self_data->fd, F_GETFL) | O_NONBLOCK);
}


static inline sky_time_t
sky_socket_timeout_setup(double timeout)
{
    double  floatpart, intpart;

    if (timeout < 0.0) {
        return SKY_TIME_INFINITE;
    }
    floatpart = modf(timeout, &intpart);
    return sky_time_current() +
           ((uint64_t)intpart * UINT64_C(1000000000)) +
           ((uint64_t)(floatpart * 1000000000.0));
}

static inline sky_time_t
sky_socket_timeout_remaining(sky_time_t timeout)
{
    sky_time_t  now;

    if (SKY_TIME_INFINITE == timeout) {
        return SKY_TIME_INFINITE;
    }
    now = sky_time_current();
    return (timeout < now ? 0 : timeout - now);
}


static sky_tuple_t
sky_socket__accept(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    sky_time_t  abs_timeout;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int                     n, new_fd, nfds;
        fd_set                  readfds;
        socklen_t               address_len;
        sky_time_t              timeout;
        sky_socket_address_t    address;

        address_len = sizeof(address);
        new_fd = accept(self_data->fd, &(address.sockaddr), &address_len);
        if (new_fd >= 0) {
            return sky_object_build("(iO)",
                                    new_fd,
                                    sky_socket_address_object(&address));
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&readfds);
        SKY_SOCKET_FD_SET(self_data->fd, &readfds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, &readfds, NULL, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}

static const char sky_socket__accept_doc[] =
"_accept() -> (integer, address info)\n\n"
"Wait for an incoming connection.  Return a new socket file descriptor\n"
"representing the connection, and the address of the client.\n"
"For IP sockets, the address is a pair (hostaddr, port).";


sky_socket_t
sky_socket_accept(sky_socket_t self, sky_object_t *remote_address)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    sky_time_t  abs_timeout;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int                     n, new_fd, nfds;
        fd_set                  readfds;
        socklen_t               address_len;
        sky_time_t              timeout;
        sky_socket_address_t    address;

        address_len = sizeof(address);
        new_fd = accept(self_data->fd, &(address.sockaddr), &address_len);
        if (new_fd >= 0) {
            sky_socket_t    new_socket;

            new_socket = sky_object_allocate(sky_socket_type);
            sky_socket_data_initialize(sky_socket_data(new_socket),
                                       new_fd,
                                       self_data->family,
                                       self_data->type,
                                       self_data->proto);
            if (remote_address) {
                *remote_address = sky_socket_address_object(&address);
            }
            return new_socket;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&readfds);
        SKY_SOCKET_FD_SET(self_data->fd, &readfds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, &readfds, NULL, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}


void
sky_socket_bind(sky_socket_t self, sky_object_t address)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    socklen_t               addrlen;
    sky_socket_address_t    addr;

    sky_socket_address_sockaddr(self_data->family, address, &addr, &addrlen);
    if (bind(self_data->fd, &(addr.sockaddr), addrlen) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_socket_bind_doc[] =
"bind(address)\n\n"
"Bind the socket to a local address.  For IP sockets, the address is a\n"
"pair (host, port); the host must refer to the local host. For raw packet\n"
"sockets the address is a tuple (ifname, proto [,pkttype [,hatype]])";


void
sky_socket_close(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    if (self_data->fd != -1) {
        if (close(self_data->fd) == -1 && EINTR == errno) {
            sky_signal_check_pending();
        }
        self_data->fd = -1;
    }
}

static const char sky_socket_close_doc[] =
"close()\n\n"
"Close the socket.  It cannot be used after this call.";


size_t
sky_socket_CMSG_LEN(ssize_t length)
{
    size_t  result;

    if (length < 0 || 
        (size_t)length > INT32_MAX - CMSG_LEN(0) ||
        (result = CMSG_LEN(length)) > INT32_MAX ||
        result < (size_t)length)
    {
        sky_error_raise_string(sky_OverflowError,
                               "CMSG_LEN() argument out of range");
    }   

    return result;
}   


size_t
sky_socket_CMSG_SPACE(ssize_t length)
{
    size_t  result;

    if (length < 0 ||
        (size_t)length > INT32_MAX - CMSG_SPACE(1) ||
        (result = CMSG_SPACE(length)) > INT32_MAX ||
        result < (size_t)length)
    {
        sky_error_raise_string(sky_OverflowError,
                               "CMSG_SPACE() argument out of range");
    }

    return result;
}


static int
sky_socket_connect_impl(sky_socket_t self, sky_object_t address, sky_bool_t ex)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    int                     nfds, option_value;
    fd_set                  writefds;
    socklen_t               addrlen, option_len;
    sky_time_t              abs_timeout, timeout;
    sky_socket_address_t    addr;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    sky_socket_address_sockaddr(self_data->family, address, &addr, &addrlen);

    if (!connect(self_data->fd, &(addr.sockaddr), addrlen)) {
        return 0;
    }
    if (EINPROGRESS != errno) {
        if (EINTR == errno) {
            sky_signal_check_pending();
            return EINTR;
        }
        return errno;
    }

    FD_ZERO(&writefds);
    SKY_SOCKET_FD_SET(self_data->fd, &writefds);
    nfds = self_data->fd + 1;
    timeout = sky_socket_timeout_remaining(abs_timeout);
    switch (sky_thread_wait_select(NULL, nfds, NULL, &writefds, NULL, timeout)) {
        case -1:
            /* error */
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            return errno;
        case 0:
            /* timeout */
            if (!ex) {
                sky_error_raise_string(sky_socket_timeout, "timed out");
            }
            return EWOULDBLOCK;
        default:
            /* ready */
            break;
    }

    option_len = sizeof(option_value);
    getsockopt(self_data->fd, SOL_SOCKET, SO_ERROR, &option_value, &option_len);
    if (EISCONN == option_value) {
        return 0;
    }
    return option_value;
}


void
sky_socket_connect(sky_socket_t self, sky_object_t address)
{
    int error;

    if ((error = sky_socket_connect_impl(self, address, SKY_FALSE)) != 0) {
        sky_error_raise_errno(sky_OSError, error);
    }
}

static const char sky_socket_connect_doc[] =
"connect(address)\n\n"
"Connect the socket to a remove address.  For IP sockest, the address\n"
"is a pair (host, port).";


int
sky_socket_connect_ex(sky_socket_t self, sky_object_t address)
{
    return sky_socket_connect_impl(self, address, SKY_TRUE);
}

static const char sky_socket_connect_ex_doc[] =
"connect_ex(address) -> errno\n\n"
"This is like connect(address), but returns an error code (the errno value)\n"
"instead of raising an exception when an error occurs.";


sky_socket_t
sky_socket_create(int family, int type, int proto)
{
    int fd;

    if ((fd = socket(family, type, proto)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    return sky_socket_createwithdescriptor(fd, family, type, proto);
}


sky_socket_t
sky_socket_createwithdescriptor(int fd, int family, int type, int proto)
{
    sky_socket_t        socket;
    sky_socket_data_t   *socket_data;

    if (fd < 0) {
        sky_error_raise_errno(sky_OSError, EBADF);
    }

    socket = sky_object_allocate(sky_socket_type);
    socket_data = sky_socket_data(socket);
    sky_socket_data_initialize(socket_data, fd, family, type, proto);

    return socket;
}


static void
sky_socket_del(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    if (self_data->fd != -1) {
        SKY_ERROR_TRY {
            sky_error_warn_format(sky_ResourceWarning, "unclosed %#@", self);
        } SKY_ERROR_EXCEPT(sky_Warning) {
            sky_error_display(sky_error_current());
        } SKY_ERROR_TRY_END;
        sky_socket_close(self);
    }
}


int
sky_socket_detach(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    int fd;

    fd = self_data->fd;
    self_data->fd = -1;

    return fd;
}

static const char sky_socket_detach_doc[] =
"detach()\n\n"
"Close the socket object without closing the underlying file descriptor.\n"
"The object cannot be used after this call, but the file descriptor\n"
"can be reused for other purposes.  The file descriptor is returned.";


int
sky_socket_fileno(sky_socket_t self)
{
    return sky_socket_data(self)->fd;
}

static const char sky_socket_fileno_doc[] =
"fileno() -> integer\n\n"
"Return the integer file descriptor of the socket.";


double
sky_socket_getdefaulttimeout(void)
{
    return sky_socket_timeout_default;
}


sky_object_t
sky_socket_getpeername(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    socklen_t               address_len;
    sky_socket_address_t    address;

    address_len = sizeof(address);
    if (getpeername(self_data->fd, &(address.sockaddr), &address_len) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_socket_address_object(&address);
}

static const char sky_socket_getpeername_doc[] =
"getpeername() -> address info\n\n"
"Return the address of the remote endpoint.  For IP sockets, the address\n"
"info is a pair (hostaddr, port).";


sky_object_t
sky_socket_getsockname(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    socklen_t               address_len;
    sky_socket_address_t    address;

    address_len = sizeof(address);
    if (getsockname(self_data->fd, &(address.sockaddr), &address_len) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    return sky_socket_address_object(&address);
}

static const char sky_socket_getsockname_doc[] =
"getsockname() -> address info\n\n"
"Return the address of the local endpoint.  For IP sockets, the address\n"
"info is a pair (hostaddr, port).";


sky_object_t
sky_socket_getsockopt(sky_socket_t  self,
                      int           level,
                      int           option,
                      ssize_t       buffersize)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    socklen_t       option_len;
    sky_object_t    result;

    if (!buffersize) {
        int option_value;

        option_len = sizeof(option_value);
        if (getsockopt(self_data->fd,
                       level,
                       option,
                       &option_value,
                       &option_len) == -1)
        {
            sky_error_raise_errno(sky_OSError, errno);
        }
        result = sky_integer_create(option_value);
    }
    else {
        if (buffersize < 0 || buffersize > 1024) {
            sky_error_raise_string(sky_OSError,
                                   "getsockopt buflen out of range");
        }

        SKY_ASSET_BLOCK_BEGIN {
            char    *bytes;

            bytes = sky_asset_malloc(buffersize, SKY_ASSET_CLEANUP_ON_ERROR);
            option_len = buffersize;
            if (getsockopt(self_data->fd,
                           level,
                           option,
                           bytes,
                           &option_len) == -1)
            {
                sky_error_raise_errno(sky_OSError, errno);
            }

            result = sky_bytes_createwithbytes(bytes, option_len, sky_free);
        } SKY_ASSET_BLOCK_END;
    }

    return result;
}

static const char sky_socket_getsockopt_doc[] =
"getsockopt(level, option[, buffersize]) -> value\n\n"
"Get a socket option.  See the Unix manual for level and option.\n"
"If a nonzero buffersize argument is given, the return value is a\n"
"string of that length; otherwise it is an integer.";


double
sky_socket_gettimeout(sky_socket_t self)
{
    return sky_socket_data(self)->timeout;
}

static sky_object_t
sky_socket_gettimeout_wrapper(sky_socket_t self)
{
    double  timeout;

    if ((timeout = sky_socket_gettimeout(self)) < 0.0) {
        return sky_None;
    }
    return sky_float_create(timeout);
}


static const char sky_socket_gettimeout_doc[] =
"gettimeout() -> timeout\n\n"
"Returns the timeout in seconds (float) associated with socket \n"
"operations. A timeout of None indicates that timeouts on socket \n"
"operations are disabled.";


static void
sky_socket_init(sky_socket_t    self,
                int             family,
                int             type,
                int             proto,
                sky_object_t    fileno)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    int fd;

    if (sky_object_isnull(fileno)) {
        fd = socket(family, type, proto);
    }
    else {
        fd = sky_file_fileno(fileno);
    }
    if (fd < 0) {
        sky_error_raise_errno(sky_OSError, errno);
    }
    sky_socket_data_initialize(self_data, fd, family, type, proto);
}


void
sky_socket_listen(sky_socket_t self, int backlog)
{
    if (listen(sky_socket_data(self)->fd, SKY_MIN(backlog, 0)) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_socket_listen_doc[] =
"listen(backlog)\n\n"
"Enable a server to accept connections.  The backlog argument must be at\n"
"least 0 (if it is lower, it is set to 0); it specifies the number of\n"
"unaccepted connections that the system will allow before refusing new\n"
"connections.";


void
sky_socket_raise_gaierror(int error)
{
    sky_object_t    value;

    /* system error returned in errno */
    if (EAI_SYSTEM == error) {
        sky_error_raise_errno(sky_OSError, errno);
    }

    value = sky_object_build("(is)", error, gai_strerror(error));
    sky_error_raise_object(sky_socket_gaierror, value);
}


ssize_t
sky_socket_recv_native(sky_socket_t self,
                       void *       bytes,
                       ssize_t      nbytes,
                       int          flags)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    sky_time_t  abs_timeout;

    sky_error_validate_debug(nbytes >= 0);
    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int         n, nfds;
        fd_set      readfds;
        ssize_t     nread;
        sky_time_t  timeout;

        if ((nread = recv(self_data->fd, bytes, nbytes, flags)) >= 0) {
            return nread;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&readfds);
        SKY_SOCKET_FD_SET(self_data->fd, &readfds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, &readfds, NULL, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}


sky_bytes_t
sky_socket_recv(sky_socket_t self, ssize_t buffersize, int flags)
{
    sky_bytes_t result;

    if (buffersize < 0) {
        sky_error_raise_string(sky_ValueError, "negative buffersize in recv");
    }

    SKY_ASSET_BLOCK_BEGIN {
        void    *bytes;
        ssize_t nbytes;

        if (!buffersize) {
            bytes = "";
        }
        else {
            bytes = sky_asset_malloc(buffersize, SKY_ASSET_CLEANUP_ON_ERROR);
        }
        nbytes = sky_socket_recv_native(self, bytes, buffersize, flags);
        result = sky_bytes_createwithbytes(bytes, nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_socket_recv_doc[] =
"recv(buffersize[, flags]) -> data\n\n"
"Receive up to buffersize bytes from the socket.  For the optional flags\n"
"argument, see the Unix manual.  When no data is available, block until\n"
"at least one byte is available or until the remote end is closed.  When\n"
"the remote end is closed and all data is read, return the empty string.";


ssize_t
sky_socket_recv_into(sky_socket_t   self,
                     sky_object_t   buffer,
                     ssize_t        nbytes,
                     int            flags)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer_struct;

        sky_buffer_acquirecbuffer(
                &buffer_struct,
                buffer,
                (SKY_BUFFER_FLAG_SIMPLE | SKY_BUFFER_FLAG_WRITABLE),
                "recv_into() argument 1",
                NULL);
        sky_asset_save(&buffer_struct,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (nbytes < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "negative buffersize in recv_into");
        }
        if (!nbytes) {
            nbytes = buffer_struct.len;
        }
        else if (buffer_struct.len < nbytes) {
            sky_error_raise_string(sky_ValueError,
                                   "buffer too small for requested bytes");
        }

        nbytes = sky_socket_recv_native(self, buffer_struct.buf, nbytes, flags);
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}

static const char sky_socket_recv_into_doc[] =
"recv_into(buffer, [nbytes[, flags]]) -> nbytes_read\n\n"
"A version of recv() that stores its data into a buffer rather than creating \n"
"a new string.  Receive up top buffersize bytes from the socket.  If buffersize \n"
"is not specified (or 0), receive up to the size available in the given buffer.\n"
"\n"
"See recv() for documentation about the flags.";


ssize_t
sky_socket_recvfrom_native(sky_socket_t     self,
                           void *           bytes,
                           ssize_t          nbytes,
                           int              flags,
                           sky_object_t *   remote_address)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    sky_time_t  abs_timeout;

    sky_error_validate_debug(nbytes >= 0);
    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int                     n, nfds;
        fd_set                  readfds;
        ssize_t                 nread;
        socklen_t               address_len;
        sky_time_t              timeout;
        sky_socket_address_t    address;

        address_len = sizeof(sky_socket_address_t);
        if ((nread = recvfrom(self_data->fd,
                              bytes,
                              nbytes,
                              flags,
                              &(address.sockaddr),
                              &address_len)) >= 0)
        {
            if (remote_address) {
                *remote_address = sky_socket_address_object(&address);
            }
            return nread;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&readfds);
        SKY_SOCKET_FD_SET(self_data->fd, &readfds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, &readfds, NULL, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}


sky_tuple_t
sky_socket_recvfrom(sky_socket_t self, ssize_t buffersize, int flags)
{
    sky_bytes_t     data;
    sky_object_t    remote_address;

    if (buffersize < 0) {
        sky_error_raise_string(sky_ValueError,
                               "negative buffersize in recvfrom");
    }

    SKY_ASSET_BLOCK_BEGIN {
        void    *bytes;
        ssize_t nbytes;

        if (!buffersize) {
            bytes = "";
        }
        else {
            bytes = sky_asset_malloc(buffersize, SKY_ASSET_CLEANUP_ON_ERROR);
        }
        nbytes = sky_socket_recvfrom_native(self,
                                            bytes,
                                            buffersize,
                                            flags,
                                            &remote_address);
        data = sky_bytes_createwithbytes(bytes, nbytes, sky_free);
    } SKY_ASSET_BLOCK_END;

    return sky_tuple_pack(2, data, remote_address);
}

static const char sky_socket_recvfrom_doc[] =
"recvfrom(buffersize[, flags]) -> (data, address info)\n\n"
"Like recv(buffersize, flags) but also return the sender's address info.";


sky_tuple_t
sky_socket_recvfrom_into(sky_socket_t   self,
                         sky_object_t   buffer,
                         ssize_t        nbytes,
                         int            flags)
{
    sky_object_t    remote_address;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer_struct;

        sky_buffer_acquirecbuffer(
                &buffer_struct,
                buffer,
                (SKY_BUFFER_FLAG_SIMPLE | SKY_BUFFER_FLAG_WRITABLE),
                "recvfrom_into() argument 1",
                NULL);
        sky_asset_save(&buffer_struct,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (nbytes < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "negative buffersize in recvfrom_into");
        }
        if (!nbytes) {
            nbytes = buffer_struct.len;
        }
        else if (buffer_struct.len < nbytes) {
            sky_error_raise_string(sky_ValueError,
                                   "buffer too small for requested bytes");
        }

        nbytes = sky_socket_recvfrom_native(self,
                                            buffer_struct.buf,
                                            nbytes,
                                            flags,
                                            &remote_address);
    } SKY_ASSET_BLOCK_END;

    return sky_object_build("(izO)", nbytes, remote_address);
}

static const char sky_socket_recvfrom_into_doc[] =
"recvfrom_into(buffer[, nbytes[, flags]]) -> (nbytes, address info)\n\n"
"Like recv_into(buffer[, nbytes[, flags]]) but also return the sender's address info.";


ssize_t
sky_socket_recvmsg_native(sky_socket_t          self,
                          struct msghdr *       msg,
                          int                   flags)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    ssize_t     nbytes;
    sky_time_t  abs_timeout;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int         n, nfds;
        fd_set      readfds;
        sky_time_t  timeout;

        if ((nbytes = recvmsg(self_data->fd, msg, flags)) >= 0) {
            return nbytes;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&readfds);
        SKY_SOCKET_FD_SET(self_data->fd, &readfds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, &readfds, NULL, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}


/* Return true iff msg->msg_controllen is valid, h is a valid pointer in
 * msg->msg_control with at least "space" bytes after it, and its cmsg_len
 * member is inside the buffer.
 */
static sky_bool_t
sky_socket_cmsg_min_space(const struct msghdr * msg,
                          const struct cmsghdr *h,
                          size_t                space)
{
    static const size_t cmsg_len_end = (offsetof(struct cmsghdr, cmsg_len) +
                                        sizeof(h->cmsg_len));

    size_t  offset;

    if (!h || !msg->msg_control || (size_t)msg->msg_controllen > INT_MAX) {
        return SKY_FALSE;
    }
    if (space < cmsg_len_end) {
        space = cmsg_len_end;
    }
    offset = (char *)h - (char *)msg->msg_control;
    return (offset <= (size_t)-1 - space &&
            offset + space <= msg->msg_controllen);
}


/* If pointer CMSG_DATA(h) is in buffer msg->msg_control, set *space to number
 * of bytes following it in the buffer and return true; otherwise, return
 * false. Assumes h, msg->msg_control, and msg->msg_controllen are valid.
 */
static sky_bool_t
sky_socket_cmsg_data_space(const struct msghdr *    msg,
                           const struct cmsghdr *   h,
                           size_t *                 space)
{
    char    *data;
    size_t  offset;

    if (!(data = (char *)CMSG_DATA(h))) {
        return SKY_FALSE;
    }
    offset = data - (char *)msg->msg_control;
    if (offset > msg->msg_controllen) {
        return SKY_FALSE;
    }
    *space = msg->msg_controllen - offset;
    return SKY_TRUE;
}


/* If h is invalid or not contained in the buffer pointed to by
 * msg->msg_control, return -1. If h is valid and its associated data is
 * entirely contained in the buffer, set *len to the length of the associated
 * data and return 0. If only part of the associated data is contained in the
 * buffer but h is otherwise valid, set *len to the length contained in the
 * buffer and return 1.
 */
static int
sky_socket_cmsg_data_len(struct msghdr *msg, struct cmsghdr *h, size_t *len)
{
    size_t  data_len, space;

    if (!sky_socket_cmsg_min_space(msg, h, CMSG_LEN(0)) ||
        h->cmsg_len < CMSG_LEN(0))
    {
        return -1;
    }
    data_len = h->cmsg_len - CMSG_LEN(0);
    if (!sky_socket_cmsg_data_space(msg, h, &space)) {
        return -1;
    }
    if (space >= data_len) {
        *len = data_len;
        return 0;
    }
    *len = space;
    return 1;
}


static sky_list_t
sky_socket_ancillarydata(struct msghdr *msg)
{
    sky_list_t  list;

    list = sky_list_create(NULL);
    if (msg->msg_controllen) {
        int             status;
        size_t          len;
        struct cmsghdr  *h;

        SKY_ERROR_TRY {
            for (h = CMSG_FIRSTHDR(msg); h; h = CMSG_NXTHDR(msg, h)) {
                status = sky_socket_cmsg_data_len(msg, h, &len);
                if (status) {
                    sky_error_warn_string(
                            sky_RuntimeWarning,
                            "received malformed or improperly-truncated "
                            "ancillary data");
                }
                if (status < 0) {
                    break;
                }

                if (len > SSIZE_MAX) {
                    sky_error_raise_string(sky_OSError,
                                           "control message too long");
                }
                sky_list_append(list,
                                sky_object_build("(iiO)",
                                                 (int)h->cmsg_level,
                                                 (int)h->cmsg_type,
                                                 sky_bytes_createfrombytes(
                                                        CMSG_DATA(h),
                                                        len)));
            }
#if defined(SCM_RIGHTS)
        } SKY_ERROR_EXCEPT_ANY {
            for (h = CMSG_FIRSTHDR(msg); h; h = CMSG_NXTHDR(msg, h)) {
                status = sky_socket_cmsg_data_len(msg, h, &len);
                if (status < 0) {
                    break;
                }
                if (SOL_SOCKET == h->cmsg_level &&
                    SCM_RIGHTS == h->cmsg_type)
                {
                    int     *fds;
                    size_t  nfds;

                    nfds = (len - CMSG_LEN(0)) / sizeof(int);
                    fds = (int *)CMSG_DATA(h);
                    while (nfds-- > 0) {
                        close(*fds++);
                    }
                }
            }
            sky_error_raise();
#endif
        } SKY_ERROR_TRY_END;
    }

    return list;
}


sky_tuple_t
sky_socket_recvmsg(sky_socket_t self,
                   ssize_t      bufsize,
                   ssize_t      ancbufsize,
                   int          flags)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 nbytes;
        sky_list_t              ancdata;
        sky_bytes_t             data;
        struct iovec            iov;
        struct msghdr           msg;
        sky_socket_address_t    address;

        if (bufsize < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "negative buffer size in recvmsg()");
        }
        if (ancbufsize < 0 || ancbufsize > INT_MAX) {
            sky_error_raise_string(sky_ValueError,
                                   "invalid ancillary data buffer length");
        }

        iov.iov_base = sky_asset_malloc(bufsize, SKY_ASSET_CLEANUP_ON_ERROR);
        iov.iov_len = bufsize;

        memset(&address, 0, sizeof(address));
        address.sockaddr.sa_family = AF_UNSPEC;

        memset(&msg, 0, sizeof(msg));
        msg.msg_name = &(address.sockaddr);
        msg.msg_namelen = sizeof(address);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        if (ancbufsize) {
            msg.msg_control = sky_asset_malloc(ancbufsize,
                                               SKY_ASSET_CLEANUP_ALWAYS);
            msg.msg_controllen = ancbufsize;
        }

        nbytes = sky_socket_recvmsg_native(self, &msg, flags);
        data = sky_bytes_createwithbytes(iov.iov_base, nbytes, sky_free);
        ancdata = sky_socket_ancillarydata(&msg);
        result = sky_tuple_pack(4, data,
                                   ancdata,
                                   sky_integer_create(msg.msg_flags),
                                   sky_socket_address_object(&address));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_socket_recvmsg_doc[] =
"recvmsg(bufsize[, ancbufsize[, flags]]) -> (data, ancdata, msg_flags, address)\n\n"
"Receive normal data (up to bufsize bytes) and ancillary data from the\n"
"socket.  The ancbufsize argument sets the size in bytes of the\n"
"internal buffer used to receive the ancillary data; it defaults to 0,\n"
"meaning that no ancillary data will be received.  Appropriate buffer\n"
"sizes for ancillary data can be calculated using CMSG_SPACE() or\n"
"CMSG_LEN(), and items which do not fit into the buffer might be\n"
"truncated or discarded.  The flags argument defaults to 0 and has the\n"
"same meaning as for recv().\n"
"\n"
"The return value is a 4-tuple: (data, ancdata, msg_flags, address).\n"
"The data item is a bytes object holding the non-ancillary data\n"
"received.  The ancdata item is a list of zero or more tuples\n"
"(cmsg_level, cmsg_type, cmsg_data) representing the ancillary data\n"
"(control messages) received: cmsg_level and cmsg_type are integers\n"
"specifying the protocol level and protocol-specific type respectively,\n"
"and cmsg_data is a bytes object holding the associated data.  The\n"
"msg_flags item is the bitwise OR of various flags indicating\n"
"details.  If the receiving socket is unconnected, address is the\n"
"address of the sending socket, if available; otherwise, its value is\n"
"unspecified.\n"
"\n"
"If recvmsg() raises an exception after the system call returns, it\n"
"will first attempt to close any file descriptors received via the\n"
"SCM_RIGHTS mechanism.";


typedef struct sky_socket_bufferlist_s {
    ssize_t                             nbuffers;
    sky_buffer_t *                      buffers;
} sky_socket_bufferlist_t;

static void
sky_socket_bufferlist_cleanup(sky_socket_bufferlist_t *list)
{
    ssize_t i;

    for (i = list->nbuffers; i > 0; --i) {
        sky_buffer_release(&(list->buffers[i - 1]));
    }
    sky_free(list->buffers);
}


sky_tuple_t
sky_socket_recvmsg_into(sky_socket_t    self,
                        sky_object_t    buffers,
                        ssize_t         ancbufsize,
                        int             flags)
{
    sky_tuple_t result;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 iovlen, nbytes;
        sky_list_t              ancdata;
        struct iovec            *iov;
        struct msghdr           msg;
        sky_socket_address_t    address;

        if (!sky_object_isiterable(buffers)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "recvmsg_into() argument 1 must be an iterable");
        }
        if (!sky_sequence_isiterable(buffers)) {
            buffers = sky_list_create(buffers);
        }
        if ((iovlen = sky_object_len(buffers)) > INT_MAX) {
            sky_error_raise_string(sky_OSError,
                                   "recvmsg_into() argument 1 is too long");
        }
        if (ancbufsize < 0 || ancbufsize > INT_MAX) {
            sky_error_raise_string(sky_ValueError,
                                   "invalid ancillary data buffer length");
        }

        if (!iovlen) {
            iov = NULL;
        }
        else {
            sky_object_t            object;
            sky_socket_bufferlist_t list;

            iov = sky_asset_malloc(sizeof(struct iovec) * iovlen,
                                   SKY_ASSET_CLEANUP_ALWAYS);
            list.nbuffers = 0;
            list.buffers = sky_malloc(sizeof(sky_buffer_t) * iovlen);
            sky_asset_save(&list,
                           (sky_free_t)sky_socket_bufferlist_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);

            SKY_SEQUENCE_FOREACH(buffers, object) {
                sky_buffer_t    *buffer = &(list.buffers[list.nbuffers]);

                sky_buffer_acquirecbuffer(
                        buffer,
                        object,
                        (SKY_BUFFER_FLAG_SIMPLE | SKY_BUFFER_FLAG_WRITABLE),
                        NULL,
                        "recvmsg_into() argument 1 must be an iterable "
                        "of single-segment read-write buffers");

                iov[list.nbuffers].iov_base = buffer->buf;
                iov[list.nbuffers].iov_len = buffer->len;
                ++list.nbuffers;
            } SKY_SEQUENCE_FOREACH_END;
        }

        memset(&address, 0, sizeof(address));
        address.sockaddr.sa_family = AF_UNSPEC;

        memset(&msg, 0, sizeof(msg));
        msg.msg_name = &(address.sockaddr);
        msg.msg_namelen = sizeof(address);
        msg.msg_iov = iov;
        msg.msg_iovlen = iovlen;
        if (ancbufsize) {
            msg.msg_control = sky_asset_malloc(ancbufsize,
                                               SKY_ASSET_CLEANUP_ALWAYS);
            msg.msg_controllen = ancbufsize;
        }

        nbytes = sky_socket_recvmsg_native(self, &msg, flags);
        ancdata = sky_socket_ancillarydata(&msg);
        result = sky_tuple_pack(4, sky_integer_create(nbytes),
                                   ancdata,
                                   sky_integer_create(msg.msg_flags),
                                   sky_socket_address_object(&address));
    } SKY_ASSET_BLOCK_END;

    return result;
}

static const char sky_socket_recvmsg_into_doc[] =
"recvmsg_into(buffers[, ancbufsize[, flags]]) -> (nbytes, ancdata, msg_flags, address)\n\n"
"Receive normal data and ancillary data from the socket, scattering the\n"
"non-ancillary data into a series of buffers.  The buffers argument\n"
"must be an iterable of objects that export writable buffers\n"
"(e.g. bytearray objects); these will be filled with successive chunks\n"
"of the non-ancillary data until it has all been written or there are\n"
"no more buffers.  The ancbufsize argument sets the size in bytes of\n"
"the internal buffer used to receive the ancillary data; it defaults to\n"
"0, meaning that no ancillary data will be received.  Appropriate\n"
"buffer sizes for ancillary data can be calculated using CMSG_SPACE()\n"
"or CMSG_LEN(), and items which do not fit into the buffer might be\n"
"truncated or discarded.  The flags argument defaults to 0 and has the\n"
"same meaning as for recv().\n"
"\n"
"The return value is a 4-tuple: (nbytes, ancdata, msg_flags, address).\n"
"The nbytes item is the total number of bytes of non-ancillary data\n"
"written into the buffers.  The ancdata item is a list of zero or more\n"
"tuples (cmsg_level, cmsg_type, cmsg_data) representing the ancillary\n"
"data (control messages) received: cmsg_level and cmsg_type are\n"
"integers specifying the protocol level and protocol-specific type\n"
"respectively, and cmsg_data is a bytes object holding the associated\n"
"data.  The msg_flags item is the bitwise OR of various flags\n"
"indicating conditions on the received message; see your system\n"
"documentation for details.  If the receiving socket is unconnected,\n"
"address is the address of the sending socket, if available; otherwise,\n"
"its value is unspecified.\n"
"\n"
"If recvmsg_into() raises an exception after the system call returns,\n"
"it will first attempt to close any file descriptors received via the\n"
"SCM_RIGHTS mechanism.";


static sky_string_t
sky_socket_repr(sky_socket_t self)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    return sky_string_createfromformat(
            "<socket object, fd=%jd, family=%d, type=%d, proto=%d>",
            (intmax_t)self_data->fd,
            self_data->family,
            self_data->type,
            self_data->proto);
}


ssize_t
sky_socket_send_native(sky_socket_t self,
                       const void * bytes,
                       ssize_t      nbytes,
                       int          flags)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    ssize_t     nbytes_sent;
    sky_time_t  abs_timeout;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    for (;;) {
        int         n, nfds;
        fd_set      writefds;
        sky_time_t  timeout;

        nbytes_sent = send(self_data->fd, bytes, nbytes, flags);
        if (nbytes_sent >= 0) {
            break;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&writefds);
        SKY_SOCKET_FD_SET(self_data->fd, &writefds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, NULL, &writefds, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }

    return nbytes_sent;
}


ssize_t
sky_socket_send(sky_socket_t self, sky_object_t data, int flags)
{
    ssize_t nbytes;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquirecbuffer(&buffer,
                                  data,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "send() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nbytes = sky_socket_send_native(self, buffer.buf, buffer.len, flags);
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}

static const char sky_socket_send_doc[] =
"send(data[, flags]) -> count\n\n"
"Send a data string to the socket.  For the optional flags\n"
"argument, see the Unix manual  Return the number of bytes\n"
"sent; this may be less than len(data) if the network is busy.";


void
sky_socket_sendall_native(sky_socket_t  self,
                          const void *  bytes,
                          ssize_t       nbytes,
                          int           flags)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    const char  *buffer;
    sky_time_t  abs_timeout;

    buffer = bytes;
    abs_timeout = sky_socket_timeout_setup(self_data->timeout);

    while (nbytes > 0) {
        int         n, nfds;
        fd_set      writefds;
        ssize_t     nbytes_sent;
        sky_time_t  timeout;

        nbytes_sent = send(self_data->fd, buffer, nbytes, flags);
        if (nbytes_sent >= 0) {
            buffer += nbytes_sent;
            nbytes -= nbytes_sent;
            continue;
        }
        if (EINTR == errno) {
            sky_signal_check_pending();
            continue;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno)) {
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&writefds);
        SKY_SOCKET_FD_SET(self_data->fd, &writefds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, NULL, &writefds, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
            }
            else {
                sky_error_raise_errno(sky_OSError, errno);
            }
        }
        else if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }
}


void
sky_socket_sendall(sky_socket_t self, sky_object_t data, int flags)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquirecbuffer(&buffer,
                                  data,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "sendall() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_socket_sendall_native(self, buffer.buf, buffer.len, flags);
    } SKY_ASSET_BLOCK_END;
}

static const char sky_socket_sendall_doc[] =
"sendall(data[, flags])\n\n"
"Send a data string to the socket.  For the optional flags\n"
"argument, see the Unix manual.  This calls send() repeatedly\n"
"until all data is sent.  If an error occurs, it's impossible\n"
"to tell how much data has been sent.";


ssize_t
sky_socket_sendmsg(sky_socket_t self,
                   sky_object_t buffers,
                   sky_object_t ancdata,
                   int          flags,
                   sky_object_t address)
{
    ssize_t nbytes;

    SKY_ASSET_BLOCK_BEGIN {
        sky_socket_data_t   *self_data = sky_socket_data(self);

        ssize_t                 iovlen;
        socklen_t               addrlen;
        sky_time_t              abs_timeout;
        struct iovec            *iov;
        struct msghdr           msg;
        sky_socket_address_t    addr;

        memset(&msg, 0, sizeof(msg));

        /* Optional: destination address */
        if (!sky_object_isnull(address)) {
            sky_socket_address_sockaddr(self_data->family,
                                        address,
                                        &addr,
                                        &addrlen);
            msg.msg_name = &addr;
            msg.msg_namelen = addrlen;
        }

        /* Required: buffers */
        if (!sky_object_isiterable(buffers)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "sendmsg() argument 1 must be an iterable");
        }
        if (!sky_sequence_isiterable(buffers)) {
            buffers = sky_list_create(buffers);
        }
        if ((iovlen = sky_object_len(buffers)) > INT_MAX) {
            sky_error_raise_string(sky_OSError,
                                   "sendmsg() argument 1 is too long");
        }
        if (!iovlen) {
            iov = NULL;
        }
        else {
            sky_object_t            object;
            sky_socket_bufferlist_t list;

            iov = sky_asset_malloc(sizeof(struct iovec) * iovlen,
                                   SKY_ASSET_CLEANUP_ALWAYS);
            list.nbuffers = 0;
            list.buffers = sky_malloc(sizeof(sky_buffer_t) * iovlen);
            sky_asset_save(&list,
                           (sky_free_t)sky_socket_bufferlist_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);

            SKY_SEQUENCE_FOREACH(buffers, object) {
                sky_buffer_t    *buffer = &(list.buffers[list.nbuffers]);

                sky_buffer_acquirecbuffer(
                        buffer,
                        object,
                        SKY_BUFFER_FLAG_SIMPLE,
                        NULL,
                        "sendmsg() argument 1 must be an iterable of "
                        "buffer-compatible objects");
                iov[list.nbuffers].iov_base = buffer->buf;
                iov[list.nbuffers].iov_len = buffer->len;
                ++list.nbuffers;
            } SKY_SEQUENCE_FOREACH_END;
        }
        msg.msg_iov = iov;
        msg.msg_iovlen = iovlen;

        /* Optional: ancillary data */
        if (sky_object_bool(ancdata)) {
            if (!sky_object_isiterable(ancdata)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "sendmsg() argument 2 must be an iterable");
            }
            if (!sky_sequence_isiterable(ancdata)) {
                ancdata = sky_list_create(ancdata);
            }
        }
        if (sky_object_bool(ancdata)) {
            int                     *levels, *types;
            ssize_t                 i, cmsglen;
            sky_object_t            item, sequence;
            struct cmsghdr          *h;
            sky_socket_bufferlist_t list;

            cmsglen = sky_object_len(ancdata);

            list.nbuffers = 0;
            list.buffers = sky_malloc(sizeof(sky_buffer_t) * cmsglen);
            sky_asset_save(&list,
                           (sky_free_t)sky_socket_bufferlist_cleanup,
                           SKY_ASSET_CLEANUP_ALWAYS);

            levels = sky_asset_malloc(sizeof(int) * cmsglen * 2,
                                      SKY_ASSET_CLEANUP_ALWAYS);
            types = levels + cmsglen;

            SKY_SEQUENCE_FOREACH(ancdata, sequence) {
                int             level, type;
                size_t          bufsize, space;
                sky_buffer_t    *buffer;
                sky_object_t    data;

                if (list.nbuffers >= cmsglen) {
                    sky_error_raise_string(
                            sky_OSError,
                            "ancillary data mutated during sendmsg()");
                }
                if (!sky_sequence_check(sequence)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "[sendmsg() ancillary data items]() argument must "
                            "be 3-item sequence, not %@",
                            sky_type_name(sky_object_type(sequence)));
                }
                if (sky_object_len(sequence) != 3) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "[sendmsg() ancillary data items]() argument must "
                            "be sequence of length 3, not %zd",
                            sky_object_len(sequence));
                }

                item = sky_sequence_get(sequence, 0);
                if (!sky_object_isa(item, sky_integer_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "an integer is required (got type %@)",
                            sky_type_name(sky_object_type(item)));
                }
                level = sky_integer_value(item, INT_MIN, INT_MAX, NULL);
                levels[list.nbuffers] = level;

                item = sky_sequence_get(sequence, 1);
                if (!sky_object_isa(item, sky_integer_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "an integer is required (got type %@)",
                            sky_type_name(sky_object_type(item)));
                }
                type = sky_integer_value(item, INT_MIN, INT_MAX, NULL);
                types[list.nbuffers] = type;

                data = sky_sequence_get(sequence, 2);
                if (!sky_buffer_check(data)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "%#@ does not support the buffer interface",
                            sky_type_name(sky_object_type(data)));
                }
                buffer = &(list.buffers[list.nbuffers]);
                sky_buffer_acquirecbuffer(
                        buffer,
                        data,
                        SKY_BUFFER_FLAG_SIMPLE,
                        "[sendmsg() ancillary data items]() argument",
                        NULL);
                ++list.nbuffers;

                bufsize = buffer->len;
                SKY_ERROR_TRY {
                    space = sky_socket_CMSG_SPACE(bufsize);
                } SKY_ERROR_EXCEPT(sky_OverflowError) {
                    sky_error_clear();
                    sky_error_raise_string(sky_OSError,
                                           "ancillary data item too large");
                } SKY_ERROR_TRY_END;
                if (INT_MAX - msg.msg_controllen < space) {
                    sky_error_raise_string(sky_OSError,
                                           "too much ancillary data");
                }
                msg.msg_controllen += space;
            } SKY_SEQUENCE_FOREACH_END;

            /* Zero memory is needed to work around glibc's CMSG_NXTHDR()
             * implementation, which checks the otherwise uninitialized
             * cmsg_len member to see if the "message" fits in the buffer,
             * returning NULL if it doesn't.
             */
            msg.msg_control = sky_asset_calloc(1, msg.msg_controllen,
                                               SKY_ASSET_CLEANUP_ALWAYS);
            h = CMSG_FIRSTHDR(&msg);
            for (h = CMSG_FIRSTHDR(&msg), i = 0;
                 i < cmsglen;
                 h = CMSG_NXTHDR(&msg, h), ++i)
            {
                size_t      data_len, msg_len;
                sky_bool_t  enough_space;

                if (!h) {
                    sky_error_raise_format(
                            sky_RuntimeError,
                            "unexpected NULL result from %s()",
                            (i ? "CMSG_NXTHDR" : "CMSG_FIRSTHDR"));
                }

                SKY_ERROR_TRY {
                    data_len = list.buffers[i].len;
                    msg_len = sky_socket_CMSG_LEN(data_len);
                } SKY_ERROR_EXCEPT(sky_OverflowError) {
                    sky_error_clear();
                    sky_error_raise_string(
                            sky_RuntimeError,
                            "item size out of range for CMSG_LEN()");
                } SKY_ERROR_TRY_END;

                enough_space = SKY_FALSE;
                if (sky_socket_cmsg_min_space(&msg, h, msg_len)) {
                    size_t  space;

                    h->cmsg_len = msg_len;
                    if (sky_socket_cmsg_data_space(&msg, h, &space)) {
                        enough_space = (space >= data_len);
                    }
                }
                if (!enough_space) {
                    sky_error_raise_string(
                            sky_RuntimeError,
                            "ancillary data does not fit in calculated space");
                }
                h->cmsg_level = levels[i];
                h->cmsg_type = types[i];
                memcpy(CMSG_DATA(h), list.buffers[i].buf, data_len);
            }
        }

        abs_timeout = sky_socket_timeout_setup(self_data->timeout);
        for (;;) {
            int         n, nfds;
            fd_set      writefds;
            sky_time_t  timeout;

            if ((nbytes = sendmsg(self_data->fd, &msg, flags)) >= 0) {
                break;
            }
            if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
                if (EINTR == errno) {
                    sky_signal_check_pending();
                    errno = EINTR;
                }
                sky_error_raise_errno(sky_OSError, errno);
            }

            FD_ZERO(&writefds);
            SKY_SOCKET_FD_SET(self_data->fd, &writefds);
            nfds = self_data->fd + 1;
            timeout = sky_socket_timeout_remaining(abs_timeout);
            n = sky_thread_wait_select(NULL, nfds, NULL, &writefds, NULL, timeout);
            if (n < 0) {
                if (EINTR == errno) {
                    sky_signal_check_pending();
                    errno = EINTR;
                }
                sky_error_raise_errno(sky_OSError, errno);
            }
            if (!n) {
                sky_error_raise_string(sky_socket_timeout, "timed out");
            }
        }
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}

static const char sky_socket_sendmsg_doc[] =
"sendmsg(buffers[, ancdata[, flags[, address]]]) -> count\n\n"
"Send normal and ancillary data to the socket, gathering the\n"
"non-ancillary data from a series of buffers and concatenating it into\n"
"a single message.  The buffers argument specifies the non-ancillary\n"
"data as an iterable of buffer-compatible objects (e.g. bytes objects).\n"
"The ancdata argument specifies the ancillary data (control messages)\n"
"as an iterable of zero or more tuples (cmsg_level, cmsg_type,\n"
"cmsg_dat), where cmsg_level and cmsg_type are integers specifying the\n"
"protocol level and protocol-specific type respectively, and cmsg_data\n"
"is a buffer-compatible object holding the associated data.  The flags\n"
"argument defaults to 0 and has the same meaning as for send().  If\n"
"address is supplied and not None, it sets a destination address for\n"
"the message.  The return value is the number of bytes of non-ancillary\n"
"data sent.";


ssize_t
sky_socket_sendto_native(sky_socket_t   self,
                         const void *   bytes,
                         ssize_t        nbytes,
                         int            flags,
                         sky_object_t   address)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    ssize_t                 nbytes_sent;
    socklen_t               addrlen;
    sky_time_t              abs_timeout;
    sky_socket_address_t    addr;

    abs_timeout = sky_socket_timeout_setup(self_data->timeout);
    sky_socket_address_sockaddr(self_data->family, address, &addr, &addrlen);

    for (;;) {
        int         n, nfds;
        fd_set      writefds;
        sky_time_t  timeout;

        nbytes_sent = sendto(self_data->fd,
                             bytes,
                             nbytes,
                             flags,
                             &(addr.sockaddr),
                             addrlen);
        if (nbytes_sent >= 0) {
            break;
        }
        if (!SKY_SOCKET_WOULDBLOCK(errno) || 0.0 == self_data->timeout) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }

        FD_ZERO(&writefds);
        SKY_SOCKET_FD_SET(self_data->fd, &writefds);
        nfds = self_data->fd + 1;
        timeout = sky_socket_timeout_remaining(abs_timeout);
        n = sky_thread_wait_select(NULL, nfds, NULL, &writefds, NULL, timeout);
        if (n < 0) {
            if (EINTR == errno) {
                sky_signal_check_pending();
                errno = EINTR;
            }
            sky_error_raise_errno(sky_OSError, errno);
        }
        if (!n) {
            sky_error_raise_string(sky_socket_timeout, "timed out");
        }
    }

    return nbytes_sent;
}


ssize_t
sky_socket_sendto(sky_socket_t  self,
                  sky_object_t  data,
                  int           flags,
                  sky_object_t  address)
{
    ssize_t nbytes;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquirecbuffer(&buffer,
                                  data,
                                  SKY_BUFFER_FLAG_SIMPLE,
                                  "sendto() argument 1",
                                  NULL);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        nbytes = sky_socket_sendto_native(self,
                                          buffer.buf,
                                          buffer.len,
                                          flags,
                                          address);
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}


static ssize_t
sky_socket_sendto_wrapper(sky_socket_t  self,
                          sky_object_t  data,
                          sky_tuple_t   args)
{
    int             flags;
    sky_object_t    address, object;

    /* We have to parse out the arguments ourselves because somebody thought it
     * would be a brilliant idea to have an optional parameter in the middle of
     * argument list. For simplicity, do not accept keyword arguments since
     * CPython doesn't anyway.
     */
    switch (sky_tuple_len(args)) {
        case 1:
            flags = 0;
            address = sky_tuple_get(args, 0);
            break;
        case 2:
            object = sky_tuple_get(args, 0);
            if (!SKY_OBJECT_METHOD_SLOT(object, INT)) {
                sky_error_raise_format(sky_TypeError,
                                       "an integer is required (got type %@)",
                                       sky_type_name(sky_object_type(object)));
            }
            flags = sky_integer_value(sky_number_integer(object),
                                      INT_MIN, INT_MAX,
                                      NULL);
            address = sky_tuple_get(args, 1);
            break;
        default:
            sky_error_raise_format(sky_TypeError,
                                   "sendto() takes 2 or 3 arguments (%zd given)",
                                   sky_tuple_len(args) + 1);
    }

    return sky_socket_sendto(self, data, flags, address);
}

static const char sky_socket_sendto_doc[] =
"sendto(data[, flags], address) -> count\n\n"
"Like send(data, flags) but allows specifying the destination address.\n"
"For IP sockets, the address is a pair (hostaddr, port).";


void
sky_socket_setblocking(sky_socket_t self, sky_bool_t flag)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    self_data->timeout = (flag ? -1.0 : 0.0);
}

static const char sky_socket_setblocking_doc[] =
"setblocking(flag)\n\n"
"Set the socket to blocking (flag is true) or non-blocking (false).\n"
"setblocking(True) is equivalent to settimeout(None);\n"
"setblocking(False) is equivalent to settimeout(0.0);";


void
sky_socket_setdefaulttimeout(double timeout)
{
    if (timeout < 0.0) {
        timeout = -1.0;
    }
    sky_socket_timeout_default = timeout;
}


void
sky_socket_setsockopt(sky_socket_t  self,
                      int           level,
                      int           option,
                      sky_object_t  value)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    int             int_value;
    void            *option_value;
    socklen_t       option_len;
    sky_buffer_t    buffer;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isa(value, sky_integer_type)) {
            int_value = sky_integer_value(value, INT_MIN, INT_MAX, NULL);
            option_value = &int_value;
            option_len = sizeof(int_value);
        }
        else {
            sky_buffer_acquirecbuffer(&buffer,
                                      value,
                                      SKY_BUFFER_FLAG_SIMPLE,
                                      "setsockopt() argument 3",
                                      NULL);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            option_value = buffer.buf;
            option_len = buffer.len;
        }
        if (setsockopt(self_data->fd,
                       level,
                       option,
                       option_value,
                       option_len) == -1)
        {
            sky_error_raise_errno(sky_OSError, errno);
        }
    } SKY_ASSET_BLOCK_END;
}

static const char sky_socket_setsockopt_doc[] =
"setsockopt(level, option, value)\n\n"
"Set a socket option.  See the Unix manual for level and option.\n"
"The value argument can either be an integer or a string.";


void
sky_socket_settimeout(sky_socket_t self, double timeout)
{
    sky_socket_data_t   *self_data = sky_socket_data(self);

    if (timeout < 0.0) {
        timeout = -1.0;
    }
    self_data->timeout = timeout;
}

static void
sky_socket_settimeout_wrapper(sky_socket_t self, sky_object_t timeout)
{
    double  value;

    if (sky_object_isnull(timeout)) {
        value = -1.0;
    }
    else {
        value = sky_float_value(sky_number_float(timeout));
        if (value < 0.0) {
            sky_error_raise_string(sky_ValueError,
                                   "Timeout value out of range");
        }
    }
    sky_socket_settimeout(self, value);
}

static const char sky_socket_settimeout_doc[] =
"settimeout(timeout)\n\n"
"Set a timeout on socket operations.  'timeout' can be a float,\n"
"giving in seconds, or None.  Setting a timeout of None disables\n"
"the timeout feature and is equivalent to setblocking(1).\n"
"Setting a timeout of zero is the same as setblocking(0).";


void
sky_socket_shutdown(sky_socket_t self, int flag)
{
    if (shutdown(sky_socket_data(self)->fd, flag) == -1) {
        sky_error_raise_errno(sky_OSError, errno);
    }
}

static const char sky_socket_shutdown_doc[] =
"shutdown(flag)\n\n"
"Shut down the reading side of the socket (flag == SHUT_RD), the writing side\n"
"of the socket (flag == SHUT_WR), or both ends (flag == SHUT_RDWR).";


static const char sky_socket_type_doc[] =
"socket([family[, type[, proto]]]) -> socket object\n"
"\n"
"Open a socket of the given type.  The family argument specifies the\n"
"address family; it defaults to AF_INET.  The type argument specifies\n"
"whether this is a stream (SOCK_STREAM, this is the default)\n"
"or datagram (SOCK_DGRAM) socket.  The protocol argument defaults to 0,\n"
"specifying the default protocol.  Keyword arguments are accepted.\n"
"\n"
"A socket object represents one endpoint of a network connection.\n"
"\n"
"Methods of socket objects (keyword arguments not allowed):\n"
"\n"
"_accept() -- accept connection, returning new socket fd and client address\n"
"bind(addr) -- bind the socket to a local address\n"
"close() -- close the socket\n"
"connect(addr) -- connect the socket to a remote address\n"
"connect_ex(addr) -- connect, return an error code instead of an exception\n"
"_dup() -- return a new socket fd duplicated from fileno()\n"
"fileno() -- return underlying file descriptor\n"
"getpeername() -- return remote address [*]\n"
"getsockname() -- return local address\n"
"getsockopt(level, optname[, buflen]) -- get socket options\n"
"gettimeout() -- return timeout or None\n"
"listen(n) -- start listening for incoming connections\n"
"recv(buflen[, flags]) -- receive data\n"
"recv_into(buffer[, nbytes[, flags]]) -- receive data (into a buffer)\n"
"recvfrom(buflen[, flags]) -- receive data and sender's address\n"
"recvfrom_into(buffer[, nbytes, [, flags])\n"
"  -- receive data and sender's address (into a buffer)\n"
"sendall(data[, flags]) -- send all data\n"
"send(data[, flags]) -- send data, may not send all of it\n"
"sendto(data[, flags], addr) -- send data to a given address\n"
"setblocking(0 | 1) -- set or clear the blocking I/O flag\n"
"setsockopt(level, optname, value) -- set socket options\n"
"settimeout(None | float) -- set or clear the timeout\n"
"shutdown(how) -- shut down traffic in one or both directions\n"
"if_nameindex() -- return all network interface indices and names\n"
"if_nametoindex(name) -- return the corresponding interface index\n"
"if_indextoname(index) -- return the corresponding interface name\n"
"\n"
"  [*] not available on all platforms!\n";

SKY_EXCEPTION_DEFINE_TRIVIAL(socket_gaierror, NULL);
SKY_EXCEPTION_DEFINE_TRIVIAL(socket_timeout, NULL);

SKY_TYPE_DEFINE_SIMPLE(socket,
                       "socket",
                       sizeof(sky_socket_data_t),
                       sky_socket_instance_initialize,
                       sky_socket_instance_finalize,
                       NULL,
                       0,
                       sky_socket_type_doc);


void
sky_socket_initialize_library(void)
{
    sky_type_initialize_builtin(sky_socket_gaierror, 1, sky_OSError);
    sky_type_initialize_builtin(sky_socket_timeout, 1, sky_OSError);

    sky_type_initialize_builtin(sky_socket_type, 0);

    sky_type_setmembers(sky_socket_type,
            "family",   "the socket family",
                        offsetof(sky_socket_data_t, family),
                        SKY_DATA_TYPE_INT,
                        SKY_MEMBER_FLAG_READONLY,
            "type",     "the socket type",
                        offsetof(sky_socket_data_t, type),
                        SKY_DATA_TYPE_INT,
                        SKY_MEMBER_FLAG_READONLY,
            "proto",    "the socket protocol",
                        offsetof(sky_socket_data_t, proto),
                        SKY_DATA_TYPE_INT,
                        SKY_MEMBER_FLAG_READONLY,
            "timeout",  "the socket timeout",
                        offsetof(sky_socket_data_t, timeout),
                        SKY_DATA_TYPE_DOUBLE,
                        SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(
            sky_socket_type,
            "_accept",
            sky_function_createbuiltin(
                    "socket._accept",
                    sky_socket__accept_doc,
                    (sky_native_code_function_t)sky_socket__accept,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "bind",
            sky_function_createbuiltin(
                    "socket.bind",
                    sky_socket_bind_doc,
                    (sky_native_code_function_t)sky_socket_bind,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "address", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "close",
            sky_function_createbuiltin(
                    "socket.close",
                    sky_socket_close_doc,
                    (sky_native_code_function_t)sky_socket_close,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "connect",
            sky_function_createbuiltin(
                    "socket.connect",
                    sky_socket_connect_doc,
                    (sky_native_code_function_t)sky_socket_connect,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "address", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "connect_ex",
            sky_function_createbuiltin(
                    "socket.connect_ex",
                    sky_socket_connect_ex_doc,
                    (sky_native_code_function_t)sky_socket_connect_ex,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "address", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "detach",
            sky_function_createbuiltin(
                    "socket.detach",
                    sky_socket_detach_doc,
                    (sky_native_code_function_t)sky_socket_detach,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "fileno",
            sky_function_createbuiltin(
                    "socket.fileno",
                    sky_socket_fileno_doc,
                    (sky_native_code_function_t)sky_socket_fileno,
                    SKY_DATA_TYPE_INT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "getpeername",
            sky_function_createbuiltin(
                    "socket.getpeername",
                    sky_socket_getpeername_doc,
                    (sky_native_code_function_t)sky_socket_getpeername,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "getsockname",
            sky_function_createbuiltin(
                    "socket.getsockname",
                    sky_socket_getsockname_doc,
                    (sky_native_code_function_t)sky_socket_getsockname,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "getsockopt",
            sky_function_createbuiltin(
                    "socket.getsockopt",
                    sky_socket_getsockopt_doc,
                    (sky_native_code_function_t)sky_socket_getsockopt,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "level", SKY_DATA_TYPE_INT, NULL,
                    "option", SKY_DATA_TYPE_INT, NULL,
                    "buffersize", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "gettimeout",
            sky_function_createbuiltin(
                    "socket.gettimeout",
                    sky_socket_gettimeout_doc,
                    (sky_native_code_function_t)sky_socket_gettimeout_wrapper,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "listen",
            sky_function_createbuiltin(
                    "socket.listen",
                    sky_socket_listen_doc,
                    (sky_native_code_function_t)sky_socket_listen,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "backlog", SKY_DATA_TYPE_INT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recv",
            sky_function_createbuiltin(
                    "socket.recv",
                    sky_socket_recv_doc,
                    (sky_native_code_function_t)sky_socket_recv,
                    SKY_DATA_TYPE_OBJECT_BYTES,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffersize", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recv_into",
            sky_function_createbuiltin(
                    "socket.recv_into",
                    sky_socket_recv_into_doc,
                    (sky_native_code_function_t)sky_socket_recv_into,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "nbytes", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recvfrom",
            sky_function_createbuiltin(
                    "socket.recvfrom",
                    sky_socket_recvfrom_doc,
                    (sky_native_code_function_t)sky_socket_recvfrom,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffersize", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recvfrom_into",
            sky_function_createbuiltin(
                    "socket.recvfrom_into",
                    sky_socket_recvfrom_into_doc,
                    (sky_native_code_function_t)sky_socket_recvfrom_into,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffer", SKY_DATA_TYPE_OBJECT, NULL,
                    "nbytes", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recvmsg",
            sky_function_createbuiltin(
                    "socket.recvmsg",
                    sky_socket_recvmsg_doc,
                    (sky_native_code_function_t)sky_socket_recvmsg,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "bufsize", SKY_DATA_TYPE_SSIZE_T, NULL,
                    "ancbufsize", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "recvmsg_into",
            sky_function_createbuiltin(
                    "socket.recvmsg_into",
                    sky_socket_recvmsg_into_doc,
                    (sky_native_code_function_t)sky_socket_recvmsg_into,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffers", SKY_DATA_TYPE_OBJECT, NULL,
                    "ancbufsize", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "send",
            sky_function_createbuiltin(
                    "socket.send",
                    sky_socket_send_doc,
                    (sky_native_code_function_t)sky_socket_send,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "sendall",
            sky_function_createbuiltin(
                    "socket.sendall",
                    sky_socket_sendall_doc,
                    (sky_native_code_function_t)sky_socket_sendall,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "sendmsg",
            sky_function_createbuiltin(
                    "socket.sendmsg",
                    sky_socket_sendmsg_doc,
                    (sky_native_code_function_t)sky_socket_sendmsg,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "buffers", SKY_DATA_TYPE_OBJECT, NULL,
                    "ancdata", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    "flags", SKY_DATA_TYPE_INT, sky_integer_zero,
                    "address", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "sendto",
            sky_function_createbuiltin(
                    "socket.sendto",
                    sky_socket_sendto_doc,
                    (sky_native_code_function_t)sky_socket_sendto_wrapper,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "data", SKY_DATA_TYPE_OBJECT, NULL,
                    "*args", SKY_DATA_TYPE_OBJECT_TUPLE, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "setblocking",
            sky_function_createbuiltin(
                    "socket.setblocking",
                    sky_socket_setblocking_doc,
                    (sky_native_code_function_t)sky_socket_setblocking,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "flag", SKY_DATA_TYPE_BOOL, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "setsockopt",
            sky_function_createbuiltin(
                    "socket.setsockopt",
                    sky_socket_setsockopt_doc,
                    (sky_native_code_function_t)sky_socket_setsockopt,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "level", SKY_DATA_TYPE_INT, NULL,
                    "option", SKY_DATA_TYPE_INT, NULL,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "settimeout",
            sky_function_createbuiltin(
                    "socket.settimeout",
                    sky_socket_settimeout_doc,
                    (sky_native_code_function_t)sky_socket_settimeout_wrapper,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "timeout", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_socket_type,
            "shutdown",
            sky_function_createbuiltin(
                    "socket.shutdown",
                    sky_socket_shutdown_doc,
                    (sky_native_code_function_t)sky_socket_shutdown,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_socket_type,
                    "flag", SKY_DATA_TYPE_INT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_socket_type,
            "__init__",
            (sky_native_code_function_t)sky_socket_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "family", SKY_DATA_TYPE_INT, NULL,
            "type", SKY_DATA_TYPE_INT, NULL,
            "proto", SKY_DATA_TYPE_INT, NULL,
            "fileno", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);

    sky_type_setmethodslots(sky_socket_type,
            "__del__", sky_socket_del,
            "__repr__", sky_socket_repr,
            NULL);
}
