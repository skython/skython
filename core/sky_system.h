/** @file
  * @brief
  * @defgroup sky_base Base Functions, Macros, and Types
  * @{
  * @defgroup sky_system System Information
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SYSTEM_H__
#define __SKYTHON_CORE_SKY_SYSTEM_H__ 1


#include "sky_base.h"
#include "sky_time.h"

#if defined(_MSC_VER)
#   include <float.h>
#endif

#if defined(HAVE_PTY_H)
#   include <pty.h>
#endif
#if defined(HAVE_UTIL_H)
#   include <util.h>
#endif


SKY_CDECLS_BEGIN


/** @cond **/
/* Constants for sky_system_controlfpu(). */
#if !defined(_MSC_VER)
#   define _MCW_DN                      0x03000000
#   define _DN_SAVE                     0x00000000
#   define _DN_FLUSH                    0x01000000

#   define _MCW_EM                      0x0008001F
#   define _EM_INVALID                  0x00000010
#   define _EM_DENORMAL                 0x00080000
#   define _EM_ZERODIVIDE               0x00000008
#   define _EM_OVERFLOW                 0x00000004
#   define _EM_UNDERFLOW                0x00000002
#   define _EM_INEXACT                  0x00000001

#   define _MCW_IC                      0x00040000
#   define _IC_AFFINE                   0x00040000
#   define _IC_PROJECTIVE               0x00000000

#   define _MCW_RC                      0x00000300
#   define _RC_CHOP                     0x00000300
#   define _RC_UP                       0x00000200
#   define _RC_DOWN                     0x00000100
#   define _RC_NEAR                     0x00000000

#   define _MCW_PC                      0x00030000
#   define _PC_24                       0x00020000
#   define _PC_53                       0x00010000
#   define _PC_64                       0x00000000
#endif
/** @endcond **/


/** Control the FPU. **/
SKY_EXTERN int
sky_system_controlfpu(unsigned int  new_values,
                      unsigned int  mask,
                      unsigned int *old_x86,
                      unsigned int *old_sse2);


/** Get the number of active CPUs on the system. **/
SKY_EXTERN uint32_t
sky_system_cpucount(void);


/** Get the maximum number of open files for the process. **/
SKY_EXTERN uint32_t
sky_system_maxopenfiles(void);

/** Return the encoding used to convert Unicode filenames to operating system
  * filenames.
  */
SKY_EXTERN const char *
sky_system_filesystemencoding(void);

/** Create a new process.
  * This is a wrapper around fork(). It should be used if Skython is to
  * continue running in the child after forking.
  *
  * @return     In the parent process, the process id of the child process will
  *             be returned. In the child process, zero will be returned. The
  *             return will be -1 if an error occurs, and @c errno will be set.
  */
SKY_EXTERN pid_t
sky_system_fork(void);

#if defined(HAVE_FORK1)
/** Create a new process.
  * This is a wrapper around fork1(). It should be used if Skython is to
  * continue running in the child after forking.
  *
  * @return     In the parent process, the process id of the child process will
  *             be returned. In the child process, zero will be returned. The
  *             return will be -1 if an error occurs, and @c errno will be set.
  */
SKY_EXTERN pid_t
sky_system_fork1(void);
#endif

/** Create a new process operating in a pseudo-tty.
  * This is a wrapper around forkpty(). It should be used if Skython is to
  * continue running in the child after forking.
  *
  * @return     In the parent process, the process id of the child process will
  *             be returned. In the child process, zero will be returned. The
  *             return will be -1 if an error occurs, and @c errno will be set.
  */
SKY_EXTERN pid_t
sky_system_forkpty(int *            amaster,
                   char *           name,
                   struct termios * termp,
                   struct winsize * winp);

/** Get the hardware page size. **/
SKY_EXTERN size_t
sky_system_pagesize(void);

/** Get the maximum size for a listening socket's pending connection queue. **/
SKY_EXTERN int
sky_system_somaxconn(void);

/** Allocate memory directly from the operating system.
  *
  * @param[in]  nbytes      the number of bytes to allocate. The number of
  *                         bytes will always be rounded up to the nearest page
  *                         size.
  * @return     the memory allocated by the operating system.
  */
SKY_EXTERN void *
sky_system_alloc(size_t nbytes);

/** Release memory directly allocated from the operating system.
  *
  * @param[in]  pointer     the pointer returned by sky_system_alloc().
  * @param[in]  nbytes      the number of bytes that were allocated.
  */
SKY_EXTERN void
sky_system_free(void *pointer, size_t nbytes);

/** Return the current system time in nanoseconds.
  */
SKY_EXTERN sky_time_t
sky_system_time(void);

/** Yield the calling thread's scheduled timeslice.
  */
SKY_EXTERN void
sky_system_yield(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SYSTEM_H__ */

/** @} **/
/** @} **/
