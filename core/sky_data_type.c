#include "sky_private.h"


static inline sky_bool_t
sky_data_type_check_object(sky_data_type_t  data_type,
                           sky_object_t     object,
                           sky_type_t       type)
{
    if (sky_None == object && (data_type & SKY_DATA_TYPE_OBJECT_OR_NONE)) {
        return SKY_TRUE;
    }
    if (sky_NotSpecified == object) {
        return SKY_TRUE;
    }
    if (sky_object_isa(object, type)) {
        return SKY_TRUE;
    }

    if (sky_type_issubtype(type, sky_bytes_type) ||
        sky_type_issubtype(type, sky_bytearray_type))
    {
        if ((data_type & SKY_DATA_TYPE_CREATE_FROM_BUFFER) &&
            sky_buffer_check(object))
        {
            return SKY_TRUE;
        }
        if ((data_type & (SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME |
                          SKY_DATA_TYPE_ENCODE_STRING)) &&
            sky_object_isa(object, sky_string_type))
        {
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_bool_t
sky_data_type_check(sky_data_type_t data_type,
                    sky_object_t    object,
                    sky_object_t    extra)
{
    if ((data_type & SKY_DATA_TYPE_NONE_IS_EXTRA) &&
        sky_None == object &&
        extra)
    {
        object = extra;
    }

    switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
        case SKY_DATA_TYPE_VOID:
            return (sky_None == object ? SKY_TRUE : SKY_FALSE);
        case SKY_DATA_TYPE_FLOAT:
        case SKY_DATA_TYPE_DOUBLE:
            return (SKY_OBJECT_METHOD_SLOT(object, FLOAT) ? SKY_TRUE
                                                          : SKY_FALSE);
        case SKY_DATA_TYPE_INT:
        case SKY_DATA_TYPE_UINT:
        case SKY_DATA_TYPE_CHAR:
        case SKY_DATA_TYPE_UCHAR:
        case SKY_DATA_TYPE_SHORT:
        case SKY_DATA_TYPE_USHORT:
        case SKY_DATA_TYPE_LONG:
        case SKY_DATA_TYPE_ULONG:
        case SKY_DATA_TYPE_LONG_LONG:
        case SKY_DATA_TYPE_ULONG_LONG:
        case SKY_DATA_TYPE_INT8:
        case SKY_DATA_TYPE_UINT8:
        case SKY_DATA_TYPE_INT16:
        case SKY_DATA_TYPE_UINT16:
        case SKY_DATA_TYPE_INT32:
        case SKY_DATA_TYPE_UINT32:
        case SKY_DATA_TYPE_INT64:
        case SKY_DATA_TYPE_UINT64:
        case SKY_DATA_TYPE_SSIZE_T:
        case SKY_DATA_TYPE_SIZE_T:
        case SKY_DATA_TYPE_INTMAX_T:
        case SKY_DATA_TYPE_UINTMAX_T:
        case SKY_DATA_TYPE_PTRDIFF_T:
        case SKY_DATA_TYPE_UPTRDIFF_T:
        case SKY_DATA_TYPE_INTPTR_T:
        case SKY_DATA_TYPE_UINTPTR_T:
            return (SKY_OBJECT_METHOD_SLOT(object, INT) ? SKY_TRUE
                                                        : SKY_FALSE);
        case SKY_DATA_TYPE_BOOL:
            /* The rules for bool(object) mean that any object is suitable. */
            return SKY_TRUE;
        case SKY_DATA_TYPE_VOID_PTR:
        case SKY_DATA_TYPE_STRING:
            /* These types are never valid for storage. */
            return SKY_FALSE;
        case SKY_DATA_TYPE_INDEX:
            return (SKY_OBJECT_METHOD_SLOT(object, INDEX) ? SKY_TRUE
                                                          : SKY_FALSE);
        case SKY_DATA_TYPE_GID_T:
        case SKY_DATA_TYPE_UID_T:
        case SKY_DATA_TYPE_PID_T:
        case SKY_DATA_TYPE_MODE_T:
        case SKY_DATA_TYPE_OFF_T:
        case SKY_DATA_TYPE_DEV_T:
        case SKY_DATA_TYPE_FD:
        case SKY_DATA_TYPE_FD2:
        case SKY_DATA_TYPE_ERRNO:
        case SKY_DATA_TYPE_SYSCALL_VOID:
        case SKY_DATA_TYPE_SYSCALL_INT:
        case SKY_DATA_TYPE_SYSCALL_BOOL:
        case SKY_DATA_TYPE_SYSCALL_PID_T:
        case SKY_DATA_TYPE_SYSCALL_OFF_T:
            return (sky_object_isa(object, sky_integer_type) ? SKY_TRUE
                                                             : SKY_FALSE);

        case SKY_DATA_TYPE_CODEPOINT:
            return (sky_object_isa(object, sky_string_type) &&
                    sky_object_len(object) == 1 ? SKY_TRUE : SKY_FALSE);

        case SKY_DATA_TYPE_OBJECT:
            return SKY_TRUE;
        case SKY_DATA_TYPE_OBJECT_BOOLEAN:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_boolean_type);
        case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_bytearray_type);
        case SKY_DATA_TYPE_OBJECT_BYTES:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_bytes_type);
        case SKY_DATA_TYPE_OBJECT_COMPLEX:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_complex_type);
        case SKY_DATA_TYPE_OBJECT_DICT:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_dict_type);
        case SKY_DATA_TYPE_OBJECT_FLOAT:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_float_type);
        case SKY_DATA_TYPE_OBJECT_INTEGER:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_integer_type);
        case SKY_DATA_TYPE_OBJECT_LIST:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_list_type);
        case SKY_DATA_TYPE_OBJECT_MODULE:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_module_type);
        case SKY_DATA_TYPE_OBJECT_SET:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_set_type);
        case SKY_DATA_TYPE_OBJECT_STRING:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_string_type);
        case SKY_DATA_TYPE_OBJECT_TUPLE:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_tuple_type);
        case SKY_DATA_TYPE_OBJECT_TYPE:
            return sky_data_type_check_object(data_type,
                                              object,
                                              sky_type_type);
        case SKY_DATA_TYPE_OBJECT_INSTANCEOF:
            return sky_data_type_check_object(data_type,
                                              object,
                                              extra);

        /* Unreachable, but keep the compiler quiet. */
        case SKY_DATA_TYPE_OBJECT_OR_NONE:
        case SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED:
        case SKY_DATA_TYPE_OBJECT_PRESERVE_NULL:
        case SKY_DATA_TYPE_NONE_IS_EXTRA:
        case SKY_DATA_TYPE_INTEGER_CLAMP:
        case SKY_DATA_TYPE_CREATE_FROM_BUFFER:
        case SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME:
        case SKY_DATA_TYPE_ENCODE_STRING:
        case SKY_DATA_TYPE_OBJECT_BYTES_FSNAME:
            sky_error_fatal("unreachable case");
    }

    sky_error_fatal("internal error");
}


static sky_object_t
sky_data_type_coerce_object(sky_data_type_t   data_type,
                            sky_object_t      object,
                            sky_object_t      extra)
{
    sky_type_t  type;

    switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
        case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
            type = sky_bytearray_type;
            break;
        case SKY_DATA_TYPE_OBJECT_BYTES:
            type = sky_bytes_type;
            break;
        default:
            return object;
    }

    if (sky_object_isnull(object) || sky_object_isa(object, type)) {
        return object;
    }

    if (sky_object_isa(object, sky_string_type)) {
        if (data_type & SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME) {
            object = sky_codec_encodefilename(object);
        }
        else if (data_type & SKY_DATA_TYPE_ENCODE_STRING) {
            object = sky_codec_encode(NULL, object, extra, NULL, 0);
        }
        else {
            /* this should be unreachable */
            sky_error_fatal("internal error");
        }
        /* object will likely be bytes at this point. another check is needed
         * for the bytearray case and just to be sure.
         */
        if (sky_object_isa(object, type)) {
            return object;
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
            case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
                object = sky_bytearray_createfrombytes(buffer.buf,
                                                       buffer.len);
                break;
            case SKY_DATA_TYPE_OBJECT_BYTES:
                object = sky_bytes_createfrombytes(buffer.buf,
                                                   buffer.len);
                break;
            default:
                sky_error_fatal("internal error");
        }
    } SKY_ASSET_BLOCK_END;

    return object;
}


sky_bool_t
sky_data_type_isextraneeded(sky_data_type_t data_type)
{
    if (SKY_DATA_TYPE_OBJECT_INSTANCEOF == (data_type & SKY_DATA_TYPE_MASK)) {
        return SKY_TRUE;
    }

    if (data_type & (SKY_DATA_TYPE_NONE_IS_EXTRA |
                     SKY_DATA_TYPE_ENCODE_STRING))
    {
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


sky_object_t
sky_data_type_load(sky_data_type_t  data_type,
                   const void *     pointer,
                   sky_object_t     extra)
{
    int             int_value;
    char            alignment_buffer[sizeof(uintmax_t)];
    off_t           off_value;
    pid_t           pid_value;
    sky_object_t    object;

#define align_pointer(_align)                                       \
        do {                                                        \
            if (unlikely((uintptr_t)pointer & ((_align) - 1))) {    \
                memcpy(alignment_buffer, pointer, (_align));        \
                pointer = alignment_buffer;                         \
            }                                                       \
        } while (0)

    switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
        case SKY_DATA_TYPE_VOID:
            return sky_None;
        case SKY_DATA_TYPE_FLOAT:
            align_pointer(sizeof(float));
            return sky_float_create(*(float *)pointer);
        case SKY_DATA_TYPE_DOUBLE:
            align_pointer(sizeof(double));
            return sky_float_create(*(double *)pointer);
        case SKY_DATA_TYPE_INT:
            align_pointer(sizeof(int));
            return sky_integer_create(*(int *)pointer);
        case SKY_DATA_TYPE_UINT:
            align_pointer(sizeof(unsigned int));
            return sky_integer_createfromunsigned(*(unsigned int *)pointer);
        case SKY_DATA_TYPE_CHAR:
            return sky_integer_create(*(char *)pointer);
        case SKY_DATA_TYPE_UCHAR:
            return sky_integer_createfromunsigned(*(unsigned char *)pointer);
        case SKY_DATA_TYPE_SHORT:
            align_pointer(sizeof(short));
            return sky_integer_create(*(short *)pointer);
        case SKY_DATA_TYPE_USHORT:
            align_pointer(sizeof(unsigned short));
            return sky_integer_createfromunsigned(*(unsigned short *)pointer);
        case SKY_DATA_TYPE_LONG:
            align_pointer(sizeof(long));
            return sky_integer_create(*(long *)pointer);
        case SKY_DATA_TYPE_ULONG:
            align_pointer(sizeof(unsigned long));
            return sky_integer_createfromunsigned(*(unsigned long *)pointer);
        case SKY_DATA_TYPE_LONG_LONG:
            align_pointer(sizeof(long long));
            return sky_integer_create(*(long long *)pointer);
        case SKY_DATA_TYPE_ULONG_LONG:
            align_pointer(sizeof(unsigned long long));
            return sky_integer_createfromunsigned(*(unsigned long long *)pointer);
        case SKY_DATA_TYPE_INT8:
            return sky_integer_create(*(int8_t *)pointer);
        case SKY_DATA_TYPE_UINT8:
            return sky_integer_createfromunsigned(*(uint8_t *)pointer);
        case SKY_DATA_TYPE_INT16:
            align_pointer(sizeof(int16_t));
            return sky_integer_create(*(int16_t *)pointer);
        case SKY_DATA_TYPE_UINT16:
            align_pointer(sizeof(uint16_t));
            return sky_integer_createfromunsigned(*(uint16_t *)pointer);
        case SKY_DATA_TYPE_INT32:
            align_pointer(sizeof(int32_t));
            return sky_integer_create(*(int32_t *)pointer);
        case SKY_DATA_TYPE_UINT32:
            align_pointer(sizeof(uint32_t));
            return sky_integer_createfromunsigned(*(uint32_t *)pointer);
        case SKY_DATA_TYPE_INT64:
            align_pointer(sizeof(int64_t));
            return sky_integer_create(*(int64_t *)pointer);
        case SKY_DATA_TYPE_UINT64:
            align_pointer(sizeof(uint64_t));
            return sky_integer_createfromunsigned(*(uint64_t *)pointer);
        case SKY_DATA_TYPE_BOOL:
            align_pointer(sizeof(sky_bool_t));
            return (*(sky_bool_t *)pointer ? sky_True : sky_False);
        case SKY_DATA_TYPE_SSIZE_T:
            align_pointer(sizeof(ssize_t));
            return sky_integer_create(*(ssize_t *)pointer);
        case SKY_DATA_TYPE_SIZE_T:
            align_pointer(sizeof(size_t));
            return sky_integer_createfromunsigned(*(size_t *)pointer);
        case SKY_DATA_TYPE_INTMAX_T:
            align_pointer(sizeof(intmax_t));
            return sky_integer_create(*(intmax_t *)pointer);
        case SKY_DATA_TYPE_UINTMAX_T:
            align_pointer(sizeof(uintmax_t));
            return sky_integer_createfromunsigned(*(uintmax_t *)pointer);
        case SKY_DATA_TYPE_PTRDIFF_T:
            align_pointer(sizeof(ptrdiff_t));
            return sky_integer_create(*(ptrdiff_t *)pointer);
        case SKY_DATA_TYPE_UPTRDIFF_T:
            align_pointer(sizeof(uptrdiff_t));
            return sky_integer_createfromunsigned(*(uptrdiff_t *)pointer);
        case SKY_DATA_TYPE_INTPTR_T:
            align_pointer(sizeof(intptr_t));
            return sky_integer_create(*(intptr_t *)pointer);
        case SKY_DATA_TYPE_UINTPTR_T:
            align_pointer(sizeof(uintptr_t));
            return sky_integer_createfromunsigned(*(uintptr_t *)pointer);
        case SKY_DATA_TYPE_INDEX:
            align_pointer(sizeof(ssize_t));
            return sky_integer_create(*(ssize_t *)pointer);
        case SKY_DATA_TYPE_GID_T:
            align_pointer(sizeof(gid_t));
            return sky_integer_createfromunsigned(*(gid_t *)pointer);
        case SKY_DATA_TYPE_UID_T:
            align_pointer(sizeof(uid_t));
            return sky_integer_createfromunsigned(*(uid_t *)pointer);
        case SKY_DATA_TYPE_PID_T:
            align_pointer(sizeof(pid_t));
            return sky_integer_create(*(pid_t *)pointer);
        case SKY_DATA_TYPE_MODE_T:
            align_pointer(sizeof(mode_t));
            return sky_integer_create(*(mode_t *)pointer);
        case SKY_DATA_TYPE_OFF_T:
            align_pointer(sizeof(off_t));
            return sky_integer_create(*(off_t *)pointer);
        case SKY_DATA_TYPE_DEV_T:
            align_pointer(sizeof(dev_t));
            return sky_integer_create(*(dev_t *)pointer);
        case SKY_DATA_TYPE_FD:
            align_pointer(sizeof(int));
            /* TODO validate file descriptor */
            return sky_integer_create(*(int *)pointer);
        case SKY_DATA_TYPE_FD2:
            align_pointer(sizeof(int));
            /* TODO validate file descriptor */
            return sky_integer_create(*(int *)pointer);
        case SKY_DATA_TYPE_CODEPOINT:
            align_pointer(sizeof(sky_unicode_char_t));
            return sky_string_createfromcodepoint(*(sky_unicode_char_t *)pointer);

        case SKY_DATA_TYPE_ERRNO:
            align_pointer(sizeof(int));
            int_value = *(int *)pointer;
            if (int_value != 0) {
                sky_error_raise_errno(sky_OSError, int_value);
            }
            return sky_None;
        case SKY_DATA_TYPE_SYSCALL_VOID:
            align_pointer(sizeof(int));
            if (*(int *)pointer < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            return sky_None;
        case SKY_DATA_TYPE_SYSCALL_INT:
            align_pointer(sizeof(int));
            int_value = *(int *)pointer;
            if (int_value < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            return sky_integer_create(int_value);
        case SKY_DATA_TYPE_SYSCALL_BOOL:
            align_pointer(sizeof(int));
            int_value = *(int *)pointer;
            if (int_value < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            return (int_value ? sky_True : sky_False);
        case SKY_DATA_TYPE_SYSCALL_PID_T:
            align_pointer(sizeof(pid_t));
            pid_value = *(pid_t *)pointer;
            if (pid_value < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            return sky_integer_create(pid_value);
        case SKY_DATA_TYPE_SYSCALL_OFF_T:
            align_pointer(sizeof(off_t));
            off_value = *(off_t *)pointer;
            if (off_value < 0) {
                sky_error_raise_errno(sky_OSError, errno);
            }
            return sky_integer_create(off_value);

        case SKY_DATA_TYPE_VOID_PTR:
            align_pointer(sizeof(void **));
            return sky_integer_createfromunsigned((uintptr_t)*(void **)pointer);
        case SKY_DATA_TYPE_STRING:
            align_pointer(sizeof(char **));
            if (!*(const char **)pointer) {
                return NULL;
            }
            return sky_string_createfrombytes(*(const char **)pointer,
                                              strlen(*(const char **)pointer),
                                              NULL,
                                              NULL);

        case SKY_DATA_TYPE_OBJECT:
        case SKY_DATA_TYPE_OBJECT_BOOLEAN:
        case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
        case SKY_DATA_TYPE_OBJECT_BYTES:
        case SKY_DATA_TYPE_OBJECT_COMPLEX:
        case SKY_DATA_TYPE_OBJECT_DICT:
        case SKY_DATA_TYPE_OBJECT_FLOAT:
        case SKY_DATA_TYPE_OBJECT_INTEGER:
        case SKY_DATA_TYPE_OBJECT_LIST:
        case SKY_DATA_TYPE_OBJECT_MODULE:
        case SKY_DATA_TYPE_OBJECT_SET:
        case SKY_DATA_TYPE_OBJECT_STRING:
        case SKY_DATA_TYPE_OBJECT_TUPLE:
        case SKY_DATA_TYPE_OBJECT_TYPE:
        case SKY_DATA_TYPE_OBJECT_INSTANCEOF:
            align_pointer(sizeof(sky_object_t *));
            if (!(object = *(sky_object_t *)pointer) &&
                !(data_type & SKY_DATA_TYPE_OBJECT_PRESERVE_NULL))
            {
                object = sky_None;
            }
            else {
                object = sky_data_type_coerce_object(data_type, object, extra);
            }
            return object;

        /* Unreachable, but keep the compiler quiet. */
        case SKY_DATA_TYPE_OBJECT_OR_NONE:
        case SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED:
        case SKY_DATA_TYPE_OBJECT_PRESERVE_NULL:
        case SKY_DATA_TYPE_NONE_IS_EXTRA:
        case SKY_DATA_TYPE_INTEGER_CLAMP:
        case SKY_DATA_TYPE_CREATE_FROM_BUFFER:
        case SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME:
        case SKY_DATA_TYPE_ENCODE_STRING:
        case SKY_DATA_TYPE_OBJECT_BYTES_FSNAME:
            sky_error_fatal("unreachable case");
    }

    sky_error_fatal("internal error");
}


sky_string_t
sky_data_type_name(sky_data_type_t data_type, sky_object_t extra)
{
    switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
        case SKY_DATA_TYPE_VOID:
            return SKY_STRING_LITERAL("void");

        case SKY_DATA_TYPE_FLOAT:
        case SKY_DATA_TYPE_DOUBLE:
            return sky_type_name(sky_float_type);

        case SKY_DATA_TYPE_INT:
        case SKY_DATA_TYPE_UINT:
        case SKY_DATA_TYPE_CHAR:
        case SKY_DATA_TYPE_UCHAR:
        case SKY_DATA_TYPE_SHORT:
        case SKY_DATA_TYPE_USHORT:
        case SKY_DATA_TYPE_LONG:
        case SKY_DATA_TYPE_ULONG:
        case SKY_DATA_TYPE_LONG_LONG:
        case SKY_DATA_TYPE_ULONG_LONG:
        case SKY_DATA_TYPE_INT8:
        case SKY_DATA_TYPE_UINT8:
        case SKY_DATA_TYPE_INT16:
        case SKY_DATA_TYPE_UINT16:
        case SKY_DATA_TYPE_INT32:
        case SKY_DATA_TYPE_UINT32:
        case SKY_DATA_TYPE_INT64:
        case SKY_DATA_TYPE_UINT64:
        case SKY_DATA_TYPE_BOOL:
        case SKY_DATA_TYPE_SSIZE_T:
        case SKY_DATA_TYPE_SIZE_T:
        case SKY_DATA_TYPE_INTMAX_T:
        case SKY_DATA_TYPE_UINTMAX_T:
        case SKY_DATA_TYPE_PTRDIFF_T:
        case SKY_DATA_TYPE_UPTRDIFF_T:
        case SKY_DATA_TYPE_INTPTR_T:
        case SKY_DATA_TYPE_UINTPTR_T:
        case SKY_DATA_TYPE_INDEX:
        case SKY_DATA_TYPE_GID_T:
        case SKY_DATA_TYPE_UID_T:
        case SKY_DATA_TYPE_PID_T:
        case SKY_DATA_TYPE_MODE_T:
        case SKY_DATA_TYPE_OFF_T:
        case SKY_DATA_TYPE_DEV_T:
        case SKY_DATA_TYPE_FD:
        case SKY_DATA_TYPE_FD2:
        case SKY_DATA_TYPE_ERRNO:
        case SKY_DATA_TYPE_SYSCALL_VOID:
        case SKY_DATA_TYPE_SYSCALL_INT:
        case SKY_DATA_TYPE_SYSCALL_BOOL:
        case SKY_DATA_TYPE_SYSCALL_PID_T:
        case SKY_DATA_TYPE_SYSCALL_OFF_T:
            return sky_type_name(sky_integer_type);

        case SKY_DATA_TYPE_CODEPOINT:
            return sky_type_name(sky_string_type);

        case SKY_DATA_TYPE_OBJECT:
            return sky_type_name(sky_base_object_type);
        case SKY_DATA_TYPE_OBJECT_BOOLEAN:
            return sky_type_name(sky_boolean_type);
        case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
            return sky_type_name(sky_bytearray_type);
        case SKY_DATA_TYPE_OBJECT_BYTES:
            return sky_type_name(sky_bytes_type);
        case SKY_DATA_TYPE_OBJECT_COMPLEX:
            return sky_type_name(sky_complex_type);
        case SKY_DATA_TYPE_OBJECT_DICT:
            return sky_type_name(sky_dict_type);
        case SKY_DATA_TYPE_OBJECT_FLOAT:
            return sky_type_name(sky_float_type);
        case SKY_DATA_TYPE_OBJECT_INTEGER:
            return sky_type_name(sky_integer_type);
        case SKY_DATA_TYPE_OBJECT_LIST:
            return sky_type_name(sky_list_type);
        case SKY_DATA_TYPE_OBJECT_MODULE:
            return sky_type_name(sky_module_type);
        case SKY_DATA_TYPE_OBJECT_SET:
            return sky_type_name(sky_set_type);
        case SKY_DATA_TYPE_OBJECT_STRING:
            return sky_type_name(sky_string_type);
        case SKY_DATA_TYPE_OBJECT_TUPLE:
            return sky_type_name(sky_tuple_type);
        case SKY_DATA_TYPE_OBJECT_TYPE:
            return sky_type_name(sky_type_type);
        case SKY_DATA_TYPE_OBJECT_INSTANCEOF:
            return sky_type_name(extra);

        case SKY_DATA_TYPE_VOID_PTR:
            return SKY_STRING_LITERAL("const void *");
        case SKY_DATA_TYPE_STRING:
            return SKY_STRING_LITERAL("const char *");

        /* Unreachable, but keep the compiler quiet. */
        case SKY_DATA_TYPE_OBJECT_OR_NONE:
        case SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED:
        case SKY_DATA_TYPE_OBJECT_PRESERVE_NULL:
        case SKY_DATA_TYPE_NONE_IS_EXTRA:
        case SKY_DATA_TYPE_INTEGER_CLAMP:
        case SKY_DATA_TYPE_CREATE_FROM_BUFFER:
        case SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME:
        case SKY_DATA_TYPE_ENCODE_STRING:
        case SKY_DATA_TYPE_OBJECT_BYTES_FSNAME:
            sky_error_fatal("unreachable case");
    }

    sky_error_fatal("internal error");
}


static size_t
sky_data_type_pack_pascal_string(void *         pointer,
                                 size_t         nbytes,
                                 sky_object_t   value)
{
    unsigned char   *p = pointer;

    if (0 == nbytes) {
        return 0;
    }
    if (1 == nbytes) {
        *p = '\x00';
        return 1;
    }

    SKY_ASSET_BLOCK_BEGIN {
        size_t          fill;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, value, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_error_validate_debug(buffer.len >= 0);

        --nbytes;
        fill = nbytes - SKY_MIN(nbytes, (size_t)buffer.len);
        *p++ = SKY_MIN(255, SKY_MIN(nbytes, (size_t)buffer.len));

        memcpy(p, buffer.buf, nbytes - fill);
        memset(p + (nbytes - fill), '\x00', fill);
    } SKY_ASSET_BLOCK_END;

    return nbytes + 1;
}


static size_t
sky_data_type_pack_string(void *        pointer,
                          size_t        nbytes,
                          sky_object_t  value)
{
    unsigned char   *p = pointer;

    if (0 == nbytes) {
        return 0;
    }

    SKY_ASSET_BLOCK_BEGIN {
        size_t          fill;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, value, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        sky_error_validate_debug(buffer.len >= 0);

        fill = nbytes - SKY_MIN(nbytes, (size_t)buffer.len);
        memcpy(p, buffer.buf, nbytes - fill);
        memset(p + (nbytes - fill), '\x00', fill);
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}


size_t
sky_data_type_pack_native(void *        pointer,
                          const char *  format,
                          sky_object_t  value)
{
    size_t      nbytes;
    const char  *c;

    switch (*format) {
        case 'B':
            *(unsigned char *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, UCHAR_MAX, NULL);
            return sizeof(unsigned char);
        case 'b':
            *(signed char *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      SCHAR_MIN, SCHAR_MAX, NULL);
            return sizeof(signed char);
        case 'c':
            *(char *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      CHAR_MIN, CHAR_MAX, NULL);
            return sizeof(char);
        case 'd':
            *(double *)pointer = sky_float_value(sky_number_float(value));
            return sizeof(double);
        case 'f':
            *(float *)pointer = sky_float_value(sky_number_float(value));
            return sizeof(float);
        case 'H':
            *(unsigned short *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, USHRT_MAX, NULL);
            return sizeof(unsigned short);
        case 'h':
            *(short *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      SHRT_MIN, SHRT_MAX, NULL);
            return sizeof(short);
        case 'I':
            *(unsigned int *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, UINT_MAX, NULL);
            return sizeof(unsigned int);
        case 'i':
            *(int *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      INT_MIN, INT_MAX, NULL);
            return sizeof(int);
        case 'L':
            *(unsigned long *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, ULONG_MAX, NULL);
            return sizeof(unsigned long);
        case 'l':
            *(long *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      LONG_MIN, LONG_MAX, NULL);
            return sizeof(long);
        case 'N':
            *(size_t *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, SSIZE_MAX, NULL);
            return sizeof(size_t);
        case 'n':
            *(ssize_t *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      SSIZE_MIN, SSIZE_MAX, NULL);
            return sizeof(ssize_t);
        case 'P':
            *(uintptr_t *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, UINTPTR_MAX, NULL);
            return sizeof(uintptr_t);
        case 'p':
            return sky_data_type_pack_pascal_string(pointer, 1, value);
        case 'Q':
            *(unsigned long long *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      0, ULLONG_MAX, NULL);
            return sizeof(unsigned long long);
        case 'q':
            *(long long *)pointer =
                    sky_integer_value(sky_number_integer(value),
                                      LLONG_MIN, LLONG_MAX, NULL);
            return sizeof(long long);
        case 's':
            return sky_data_type_pack_string(pointer, 1, value);
        case '?':
            *(char *)pointer = (sky_object_bool(value) ? 1 : 0);
            return sizeof(char);
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            nbytes = 0;
            for (c = format; sky_ctype_isdigit(*c); ++c) {
                nbytes = (nbytes * 10) + (*c - '0');
            }
            if ('p' == *c) {
                return sky_data_type_pack_pascal_string(pointer, nbytes, value);
            }
            if ('s' == *c) {
                return sky_data_type_pack_string(pointer, nbytes, value);
            }
            break;
    }

    sky_error_raise_format(sky_NotImplementedError,
                           "unsupported format %s",
                           format);
}


static inline uintmax_t
sky_data_type_signed_max(size_t s)
{
    switch (s) {
        case 1:
            return INT8_MAX;
        case 2:
            return INT16_MAX;
        case 4:
            return INT32_MAX;
        case 8:
            return INT64_MAX;
    }
    sky_error_fatal("not implemented");
}

static inline intmax_t
sky_data_type_signed_min(size_t s)
{
    switch (s) {
        case 1:
            return INT8_MIN;
        case 2:
            return INT16_MIN;
        case 4:
            return INT32_MIN;
        case 8:
            return INT64_MIN;
    }
    sky_error_fatal("not implemented");
}

static inline uintmax_t
sky_data_type_unsigned_max(size_t s)
{
    switch (s) {
        case 1:
            return UINT8_MAX;
        case 2:
            return UINT16_MAX;
        case 4:
            return UINT32_MAX;
        case 8:
            return UINT64_MAX;
    }
    sky_error_fatal("not implemented");
}

static inline intmax_t
sky_data_type_unsigned_min(SKY_UNUSED size_t s)
{
    return 0;
}

void
sky_data_type_store(sky_data_type_t data_type,
                    sky_object_t    object,
                    sky_object_t    value,
                    void *          pointer,
                    sky_object_t    extra)
{
    if (!sky_data_type_check(data_type, value, extra)) {
        sky_error_raise_format(sky_TypeError,
                               "expected %#@; got %#@",
                               sky_data_type_name(data_type, extra),
                               sky_type_name(sky_object_type(value)));
    }

    if ((data_type & SKY_DATA_TYPE_NONE_IS_EXTRA) &&
        sky_None == value &&
        extra)
    {
        value = extra;
    }

#define integer_value(_min, _max)                                       \
    (data_type & SKY_DATA_TYPE_INTEGER_CLAMP ?                          \
     sky_integer_value_clamped(sky_number_integer(value), _min, _max) : \
     sky_integer_value(sky_number_integer(value), _min, _max, NULL))

    switch ((sky_data_type_t)(data_type & SKY_DATA_TYPE_MASK)) {
        case SKY_DATA_TYPE_VOID:
        case SKY_DATA_TYPE_VOID_PTR:
        case SKY_DATA_TYPE_STRING:
        case SKY_DATA_TYPE_ERRNO:
        case SKY_DATA_TYPE_SYSCALL_VOID:
        case SKY_DATA_TYPE_SYSCALL_INT:
        case SKY_DATA_TYPE_SYSCALL_BOOL:
        case SKY_DATA_TYPE_SYSCALL_PID_T:
        case SKY_DATA_TYPE_SYSCALL_OFF_T:
            sky_error_raise_format(sky_TypeError,
                                   "type %#@ cannot be stored",
                                   sky_data_type_name(data_type, extra));

        case SKY_DATA_TYPE_FLOAT:
            *(float *)pointer = sky_float_value(sky_number_float(value));
            return;
        case SKY_DATA_TYPE_DOUBLE:
            *(double *)pointer = sky_float_value(sky_number_float(value));
            return;

        case SKY_DATA_TYPE_INT:
            *(int *)pointer = integer_value(INT_MIN, INT_MAX);
            return;
        case SKY_DATA_TYPE_UINT:
            *(unsigned int *)pointer = integer_value(0, UINT_MAX);
            return;
        case SKY_DATA_TYPE_CHAR:
            *(char *)pointer = integer_value(CHAR_MIN, CHAR_MAX);
            return;
        case SKY_DATA_TYPE_UCHAR:
            *(unsigned char *)pointer = integer_value(0, UCHAR_MAX);
            return;
        case SKY_DATA_TYPE_SHORT:
            *(short *)pointer = integer_value(SHRT_MIN, SHRT_MAX);
            return;
        case SKY_DATA_TYPE_USHORT:
            *(unsigned short *)pointer = integer_value(0, USHRT_MAX);
            return;
        case SKY_DATA_TYPE_LONG:
            *(long *)pointer = integer_value(LONG_MIN, LONG_MAX);
            return;
        case SKY_DATA_TYPE_ULONG:
            *(unsigned long *)pointer = integer_value(0, ULONG_MAX);
            return;
        case SKY_DATA_TYPE_LONG_LONG:
            *(long long *)pointer = integer_value(LLONG_MIN, LLONG_MAX);
            return;
        case SKY_DATA_TYPE_ULONG_LONG:
            *(unsigned long long *)pointer = integer_value(0, ULLONG_MAX);
            return;
        case SKY_DATA_TYPE_INT8:
            *(int8_t *)pointer = integer_value(INT8_MIN, INT8_MAX);
            return;
        case SKY_DATA_TYPE_UINT8:
            *(uint8_t *)pointer = integer_value(0, UINT8_MAX);
            return;
        case SKY_DATA_TYPE_INT16:
            *(int16_t *)pointer = integer_value(INT16_MIN, INT16_MAX);
            return;
        case SKY_DATA_TYPE_UINT16:
            *(uint16_t *)pointer = integer_value(0, UINT16_MAX);
            return;
        case SKY_DATA_TYPE_INT32:
            *(int32_t *)pointer = integer_value(INT32_MIN, INT32_MAX);
            return;
        case SKY_DATA_TYPE_UINT32:
            *(uint32_t *)pointer = integer_value(0, UINT32_MAX);
            return;
        case SKY_DATA_TYPE_INT64:
            *(int64_t *)pointer = integer_value(INT64_MIN, INT64_MAX);
            return;
        case SKY_DATA_TYPE_UINT64:
            *(uint64_t *)pointer = integer_value(0, UINT64_MAX);
            return;
        case SKY_DATA_TYPE_BOOL:
            *(sky_bool_t *)pointer = sky_object_bool(value);
            return;
        case SKY_DATA_TYPE_SSIZE_T:
            *(ssize_t *)pointer = integer_value(SSIZE_MIN, SSIZE_MAX);
            return;
        case SKY_DATA_TYPE_SIZE_T:
            *(size_t *)pointer = integer_value(0, SIZE_MAX);
            return;
        case SKY_DATA_TYPE_INTMAX_T:
            *(intmax_t *)pointer = integer_value(INTMAX_MIN, INTMAX_MAX);
            return;
        case SKY_DATA_TYPE_UINTMAX_T:
            *(uintmax_t *)pointer = integer_value(0, UINTMAX_MAX);
            return;
        case SKY_DATA_TYPE_PTRDIFF_T:
            *(ptrdiff_t *)pointer = integer_value(PTRDIFF_MIN, PTRDIFF_MAX);
            return;
        case SKY_DATA_TYPE_UPTRDIFF_T:
            *(uptrdiff_t *)pointer = integer_value(0, UPTRDIFF_MAX);
            return;
        case SKY_DATA_TYPE_INTPTR_T:
            *(intptr_t *)pointer = integer_value(INTPTR_MIN, INTPTR_MAX);
            return;
        case SKY_DATA_TYPE_UINTPTR_T:
            *(uintptr_t *)pointer = integer_value(0, UINTPTR_MAX);
            return;
        case SKY_DATA_TYPE_INDEX:
            value = sky_number_index(value);
            if (data_type & SKY_DATA_TYPE_INTEGER_CLAMP) {
                *(ssize_t *)pointer = sky_integer_value_clamped(value,
                                                                SSIZE_MIN,
                                                                SSIZE_MAX);
            }
            else {
                *(ssize_t *)pointer = sky_integer_value(value,
                                                        SSIZE_MIN,
                                                        SSIZE_MAX,
                                                        NULL);
            }
            return;
        case SKY_DATA_TYPE_GID_T:
            *(gid_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_unsigned_min(sizeof(gid_t)),
                            sky_data_type_unsigned_max(sizeof(gid_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_UID_T:
            *(uid_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_unsigned_min(sizeof(uid_t)),
                            sky_data_type_unsigned_max(sizeof(uid_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_PID_T:
            *(pid_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_signed_min(sizeof(pid_t)),
                            sky_data_type_signed_max(sizeof(pid_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_MODE_T:
            *(mode_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_unsigned_min(sizeof(mode_t)),
                            sky_data_type_unsigned_max(sizeof(mode_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_OFF_T:
            *(off_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_signed_min(sizeof(off_t)),
                            sky_data_type_signed_max(sizeof(off_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_DEV_T:
            *(dev_t *)pointer =
                    sky_integer_value(
                            value,
                            sky_data_type_signed_min(sizeof(dev_t)),
                            sky_data_type_signed_max(sizeof(dev_t)),
                            NULL);
            return;
        case SKY_DATA_TYPE_FD:
        case SKY_DATA_TYPE_FD2:
            *(int *)pointer = sky_integer_value(value, INT_MIN, INT_MAX, NULL);
            return;
        case SKY_DATA_TYPE_CODEPOINT:
            *(sky_unicode_char_t *)pointer = sky_string_charat(value, 0);
            return;

        case SKY_DATA_TYPE_OBJECT:
        case SKY_DATA_TYPE_OBJECT_BOOLEAN:
        case SKY_DATA_TYPE_OBJECT_BYTEARRAY:
        case SKY_DATA_TYPE_OBJECT_BYTES:
        case SKY_DATA_TYPE_OBJECT_COMPLEX:
        case SKY_DATA_TYPE_OBJECT_DICT:
        case SKY_DATA_TYPE_OBJECT_FLOAT:
        case SKY_DATA_TYPE_OBJECT_INTEGER:
        case SKY_DATA_TYPE_OBJECT_LIST:
        case SKY_DATA_TYPE_OBJECT_MODULE:
        case SKY_DATA_TYPE_OBJECT_SET:
        case SKY_DATA_TYPE_OBJECT_STRING:
        case SKY_DATA_TYPE_OBJECT_TUPLE:
        case SKY_DATA_TYPE_OBJECT_TYPE:
        case SKY_DATA_TYPE_OBJECT_INSTANCEOF:
            if (sky_NotSpecified == value &&
                !(data_type & SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED))
            {
                value = NULL;
            }
            else {
                value = sky_data_type_coerce_object(data_type, value, extra);
            }
            sky_object_gc_set((sky_object_t *)pointer, value, object);
            return;

        /* Unreachable, but keep the compiler quiet. */
        case SKY_DATA_TYPE_OBJECT_OR_NONE:
        case SKY_DATA_TYPE_OBJECT_PASS_NOTSPECIFIED:
        case SKY_DATA_TYPE_OBJECT_PRESERVE_NULL:
        case SKY_DATA_TYPE_NONE_IS_EXTRA:
        case SKY_DATA_TYPE_INTEGER_CLAMP:
        case SKY_DATA_TYPE_CREATE_FROM_BUFFER:
        case SKY_DATA_TYPE_ENCODE_STRING_AS_FSNAME:
        case SKY_DATA_TYPE_ENCODE_STRING:
        case SKY_DATA_TYPE_OBJECT_BYTES_FSNAME:
            sky_error_fatal("unreachable case");
    }

#undef integer_value

    sky_error_fatal("internal error");
}


sky_object_t
sky_data_type_unpack_native(const void *pointer, const char *format)
{
    char        alignment_buffer[sizeof(uintmax_t)];
    ssize_t     nbytes;
    const char  *c;

    switch (*format) {
        case 'B':
            return sky_integer_createfromunsigned(*(unsigned char *)pointer);
        case 'b':
            return sky_integer_create(*(signed char *)pointer);
        case 'c':
            return sky_bytes_createfrombytes(pointer, 1);
        case 'd':
            align_pointer(sizeof(double));
            return sky_float_create(*(double *)pointer);
        case 'f':
            align_pointer(sizeof(float));
            return sky_float_create(*(float *)pointer);
        case 'H':
            align_pointer(sizeof(unsigned short));
            return sky_integer_createfromunsigned(*(unsigned short *)pointer);
        case 'h':
            align_pointer(sizeof(short));
            return sky_integer_create(*(short *)pointer);
        case 'I':
            align_pointer(sizeof(unsigned int));
            return sky_integer_createfromunsigned(*(unsigned int *)pointer);
        case 'i':
            align_pointer(sizeof(int));
            return sky_integer_create(*(int *)pointer);
        case 'L':
            align_pointer(sizeof(unsigned long));
            return sky_integer_createfromunsigned(*(unsigned long *)pointer);
        case 'l':
            align_pointer(sizeof(long));
            return sky_integer_create(*(long *)pointer);
        case 'N':
            align_pointer(sizeof(size_t));
            return sky_integer_createfromunsigned(*(size_t *)pointer);
        case 'n':
            align_pointer(sizeof(ssize_t));
            return sky_integer_create(*(ssize_t *)pointer);
        case 'P':
            align_pointer(sizeof(void *));
            return sky_integer_createfromunsigned((uintptr_t)*(void **)pointer);
        case 'p':
            return sky_bytes_empty;
        case 'Q':
            align_pointer(sizeof(unsigned long long));
            return sky_integer_createfromunsigned(*(unsigned long long *)pointer);
        case 'q':
            align_pointer(sizeof(long long));
            return sky_integer_create(*(long long *)pointer);
        case 's':
            return sky_bytes_createfrombytes(pointer, 1);
        case '?':
            return (*(uint8_t *)pointer ? sky_True : sky_False);
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            nbytes = 0;
            for (c = format; sky_ctype_isdigit(*c); ++c) {
                nbytes = (nbytes * 10) + (*c - '0');
            }
            if ('p' == *c) {
                return (nbytes ? sky_bytes_createfrombytes(pointer + 1,
                                                           nbytes - 1)
                               : sky_bytes_empty);
            }
            if ('s' == *c) {
                return (nbytes ? sky_bytes_createfrombytes(pointer, nbytes)
                               : sky_bytes_empty);
            }
            break;
    }

    sky_error_raise_format(sky_NotImplementedError,
                           "unsupported format %s",
                           format);
}
