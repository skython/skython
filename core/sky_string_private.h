#ifndef __SKYTHON_CORE_SKY_STRING_PRIVATE_H__
#define __SKYTHON_CORE_SKY_STRING_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_lockfree_hashtable.h"
#include "sky_object.h"
#include "sky_object_private.h"
#include "sky_string.h"
#include "sky_unicode_private.h"


SKY_CDECLS_BEGIN


typedef enum sky_string_flag_e {
    SKY_STRING_FLAG_HASHED          =   (1 << 0),
    SKY_STRING_FLAG_CACHED_LITERAL  =   (1 << 1),
    SKY_STRING_FLAG_NO_FREE         =   (1 << 2),
    SKY_STRING_FLAG_CTYPE_FLAGS_SET =   (1 << 4),
} sky_string_flag_t;


typedef struct sky_string_data_s {
    uintptr_t                           hash;
    ssize_t                             len;

    union {
        char *                          ascii;
        uint8_t *                       latin1;
        uint16_t *                      ucs2;
        sky_unicode_char_t *            ucs4;
    } data;

    sky_unicode_char_t                  highest_codepoint;
    unsigned int                        any_ctype_flags;
    unsigned int                        all_ctype_flags;

    volatile uint32_t                   flags;

    /* Size small_data differently depending on 32-bit or 64-bit. On 32-bit
     * builds, a string object instance will be 48 bytes up to this point, so
     * fill it up to 64 bytes for a (typical) cache line fill. On 64-bit
     * builds, a string object instance will be 96 bytes up to this point, so
     * fill it up to 128 bytes to use up the core malloc slab (there's nothing
     * between 64 and 128 bytes).
     */
    union {
        sky_free_t                      free;
#if defined(SKY_ARCH_32BIT)
        char                            small_data[16];
#elif defined(SKY_ARCH_64BIT)
        char                            small_data[56];
#else
#   error implementation missing
#endif
    } u;
} sky_string_data_t;

SKY_EXTERN sky_string_data_t *
sky_string_data(sky_object_t object, sky_string_data_t *tagged_data);

SKY_EXTERN sky_string_data_t *
sky_string_data_noctype(sky_object_t object, sky_string_data_t *tagged_data);


typedef struct sky_string_iterator_data_s {
    sky_string_t                        string;
    ssize_t                             next_offset;
} sky_string_iterator_data_t;

SKY_EXTERN_INLINE sky_string_iterator_data_t *
sky_string_iterator_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_string_iterator_type) {
        return (sky_string_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_string_iterator_type);
}


struct sky_string_s {
    sky_object_data_t                   object_data;
    sky_string_data_t                   string_data;
};

struct sky_string_builder_s {
    sky_object_data_t                   object_data;
    sky_string_data_t                   string_data;
};

struct sky_string_iterator_s {
    sky_object_data_t                   object_data;
    sky_string_iterator_data_t          iterator_data;
};


SKY_EXTERN_INLINE size_t
sky_string_data_width(const sky_string_data_t *string_data)
{
    if (string_data->highest_codepoint < 0x100) {
        return 1;
    }
    return (string_data->highest_codepoint < 0x10000 ? 2 : 4);
}


extern void
sky_string_data_append(sky_string_data_t *  string_data,
                       const void *         bytes,
                       size_t               nbytes,
                       size_t               width);

extern void
sky_string_data_append_1to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_1to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_1to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_2to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_2to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_2to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_4to1(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_4to2(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);

extern void
sky_string_data_append_4to4(sky_string_data_t * string_data,
                            const void *        bytes,
                            size_t              nbytes);


extern void
sky_string_data_append_codepoint(sky_string_data_t *string_data,
                                 sky_unicode_char_t codepoint,
                                 ssize_t            count);


extern void
sky_string_data_append_slice(sky_string_data_t *        string_data,
                             const sky_string_data_t *  other_data,
                             ssize_t                    length,
                             ssize_t                    start,
                             ssize_t                    step);


extern void
sky_string_data_clear(sky_string_data_t *   string_data);


extern void
sky_string_data_combine(sky_string_data_t *         string_data,
                        const sky_string_data_t *   other_data);


extern int
sky_string_data_compare(const sky_string_data_t *   string_data,
                        const sky_string_data_t *   other_data);

extern void
sky_string_data_copy(sky_string_data_t *        target_data,
                     const sky_string_data_t *  source_data);


extern void
sky_string_data_decode(sky_string_data_t *  string_data,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       const void *         bytes,
                       ssize_t              nbytes);

extern void
sky_string_data_finalize(sky_string_data_t *string_data);

extern void
sky_string_data_join(sky_string_data_t *        string_data,
                     const sky_string_data_t *  separator,
                     sky_string_data_t * const *strings,
                     ssize_t                    nstrings);

extern void
sky_string_data_resize(sky_string_data_t *  string_data,
                       size_t               needed_size,
                       size_t               width);

extern void
sky_string_data_widen(sky_string_data_t *   string_data,
                      size_t                needed,
                      size_t                old_width,
                      size_t                new_width);


static inline void
sky_string_data_update(sky_string_data_t *  string_data,
                       sky_unicode_char_t   cp)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(cp);

    if (!(string_data->flags & SKY_STRING_FLAG_CTYPE_FLAGS_SET)) {
        string_data->all_ctype_flags = ctype->flags;
        string_data->flags |= SKY_STRING_FLAG_CTYPE_FLAGS_SET;
    }
    else {
        string_data->all_ctype_flags &= ctype->flags;
    }
    string_data->any_ctype_flags |= ctype->flags;
    if (cp > string_data->highest_codepoint) {
        string_data->highest_codepoint = cp;
    }
}


static inline void
sky_string_data_update_1(sky_string_data_t *string_data,
                         sky_unicode_char_t cp)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup_latin1(cp);

    sky_error_validate_debug(cp < 0x100);
    if (!(string_data->flags & SKY_STRING_FLAG_CTYPE_FLAGS_SET)) {
        string_data->all_ctype_flags = ctype->flags;
        string_data->flags |= SKY_STRING_FLAG_CTYPE_FLAGS_SET;
    }
    else {
        string_data->all_ctype_flags &= ctype->flags;
    }
    string_data->any_ctype_flags |= ctype->flags;
    if (cp > string_data->highest_codepoint) {
        string_data->highest_codepoint = cp;
    }
}


SKY_EXTERN sky_string_t
sky_string_normalizewithdata(sky_string_t                       self,
                             sky_string_normalize_form_t        form,
                             const sky_unicode_decomp_data_t *  data);


SKY_OBJECT_TYPE_DECLARE(string_cache, string cache);

typedef struct sky_string_cache_data_s {
    sky_lockfree_hashtable_t            intern_table;
    sky_lockfree_hashtable_t            literal_table;
} sky_string_cache_data_t;

SKY_EXTERN_INLINE sky_string_cache_data_t *
sky_string_cache_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_string_cache_type) {
        return (sky_string_cache_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_string_cache_type);
}

struct sky_string_cache_s {
    sky_object_data_t                   object_data;
    sky_string_cache_data_t             cache_data;
};


extern sky_string_cache_t sky_string_cache_instance;


SKY_EXTERN_INLINE sky_string_cache_t
sky_string_cache(void)
{
    if (!sky_string_cache_instance) {
        sky_object_gc_setlibraryroot(
                SKY_AS_OBJECTP(&sky_string_cache_instance),
                sky_object_allocate(sky_string_cache_type),
                SKY_TRUE);
    }
    return sky_string_cache_instance;
}


extern void
sky_string_cache_initialize_library(void);


/* Enter a string into the string cache object's literal table. The string
 * returned may be different if another thread has already entered the string
 * into the table.
 */
extern sky_string_t
sky_string_cache_literal(sky_string_cache_t cache,
                         const char *       bytes,
                         size_t             nbytes,
                         sky_string_t       string);


/* Check a string cache for a string object based on raw byte data, assuming
 * the data is encoded using the default encoding (UTF-8).
 */
extern sky_string_t
sky_string_cache_lookup(sky_string_cache_t  cache,
                        const char *        bytes,
                        size_t              nbytes);

/* Check a string cache for an existing string the matches a string, which has
 * generally been newly created. Return a match, or enter the string into the
 * cache if enter is SKY_TRUE.
 */
extern sky_string_t
sky_string_cache_string(sky_string_cache_t  cache,
                        sky_string_t        string,
                        sky_bool_t          enter);


/* Create a new string object instance from a string_data structure. A copy of
 * the data is made.
 */
extern sky_string_t
sky_string_createfromstringdata(const sky_string_data_t *string_data);

extern sky_string_t
sky_string___format__(sky_string_t self, sky_string_t format_spec);

extern sky_string_t
sky_string_printf(sky_string_t format, sky_object_t values);

SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_STRING_PRIVATE_H__ */
