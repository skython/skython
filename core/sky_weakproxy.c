#include "sky_private.h"


typedef struct sky_weakproxy_data_s {
    sky_object_t                        object;
    sky_object_t                        callback;
} sky_weakproxy_data_t;

SKY_EXTERN_INLINE sky_weakproxy_data_t *
sky_weakproxy_data(sky_object_t object)
{
    if (sky_weakproxy_type == sky_object_type(object)) {
        return (sky_weakproxy_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    if (sky_callableweakproxy_type == sky_object_type(object)) {
        return (sky_weakproxy_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_weakproxy_type);
}

static void
sky_weakproxy_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_weakproxy_data_t    *self_data = data;

    sky_object_visit(self_data->callback, visit_data);
    if (SKY_OBJECT_VISIT_REASON_GC_MARK != visit_data->reason) {
        sky_object_visit(self_data->object, visit_data);
    }
}


void
sky_weakproxy_callback(sky_object_t object)
{
    sky_weakproxy_data_t    *proxy_data = sky_weakproxy_data(object);

    proxy_data->object = NULL;
    if (proxy_data->callback) {
        sky_object_call(proxy_data->callback, sky_tuple_pack(1, object), NULL);
    }
}


static void
sky_weakproxy_setup(sky_object_t    self,
                    sky_object_t    object,
                    sky_object_t    callback)
{
    sky_weakproxy_data_t    *self_data = sky_weakproxy_data(self);

    if (sky_object_istagged(object)) {
        sky_error_raise_format(sky_TypeError,
                               "cannot create weak reference to %#@ object",
                               sky_type_name(sky_object_type(object)));
    }

    if (sky_object_isnull(callback)) {
        callback = NULL;
    }

    sky_object_gc_set(&(self_data->callback), callback, self);
    sky_object_gc_setweakref(&(self_data->object),
                             object,
                             self,
                             sky_weakproxy_callback,
                             self);
}


static sky_parameter_list_t sky_weakproxy_init_parameters = NULL;

static void
sky_weakproxy_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    void            **values;
    sky_object_t    callback, object;

    values = sky_parameter_list_apply(sky_weakproxy_init_parameters,
                                      args, kws,
                                      sky_type_name(sky_object_type(self)));
    object = *(sky_object_t *)values[0];
    callback = *(sky_object_t *)values[1];
    sky_free(values);

    sky_weakproxy_setup(self, object, callback);
}


sky_string_t
sky_weakproxy_repr(sky_weakref_t self)
{
    sky_weakproxy_data_t    *self_data = sky_weakproxy_data(self);

    sky_object_t    object;
    sky_string_t    name, self_name, type_name;

    self_name = sky_type_name(sky_object_type(self));
    if (!(object = self_data->object)) {
        return sky_string_createfromformat("<%@ at %p; dead>", self_name, self);
    }

    name = sky_object_getattr(object, SKY_STRING_LITERAL("__name__"), NULL);
    type_name = sky_type_name(sky_object_type(object));

    if (!name) {
        return sky_string_createfromformat("<%@ at %p; to %#@ at %p>",
                                           self_name,
                                           self,
                                           type_name,
                                           object);
    }
    return sky_string_createfromformat("<%@ at %p; to %#@ at %p (%@)>",
                                       self_name,
                                       self,
                                       type_name,
                                       object,
                                       name);
}


static inline sky_object_t
sky_weakproxy_object(sky_object_t proxy)
{
    sky_weakproxy_data_t    *proxy_data = sky_weakproxy_data(proxy);

    sky_object_t    object;

    if (!(object = proxy_data->object)) {
        sky_error_raise_string(sky_ReferenceError,
                               "weakly-referenced object no longer exists");
    }

    return object;
}


#define SKY_WEAKPROXY_BINARY(_name, _type, _function)                       \
        static _type                                                        \
        sky_weakproxy_##_name(sky_object_t proxy, sky_object_t other)       \
        { return _function(sky_weakproxy_object(proxy), other); }

#define SKY_WEAKPROXY_REVERSE_BINARY(_name, _type, _function)               \
        static _type                                                        \
        sky_weakproxy_##_name(sky_object_t proxy, sky_object_t other)       \
        { return _function(other, sky_weakproxy_object(proxy)); }

#define SKY_WEAKPROXY_UNARY(_name, _type, _function)                        \
        static _type sky_weakproxy_##_name(sky_object_t proxy)              \
        { return _function(sky_weakproxy_object(proxy)); }

SKY_WEAKPROXY_UNARY(str, sky_string_t, sky_object_str)
SKY_WEAKPROXY_UNARY(bytes, sky_bytes_t, sky_object_bytes)
SKY_WEAKPROXY_BINARY(lt, sky_object_t, sky_object_lt)
SKY_WEAKPROXY_BINARY(le, sky_object_t, sky_object_eq)
SKY_WEAKPROXY_BINARY(eq, sky_object_t, sky_object_eq)
SKY_WEAKPROXY_BINARY(ne, sky_object_t, sky_object_ne)
SKY_WEAKPROXY_BINARY(gt, sky_object_t, sky_object_gt)
SKY_WEAKPROXY_BINARY(ge, sky_object_t, sky_object_ge)
SKY_WEAKPROXY_UNARY(bool, sky_bool_t, sky_object_bool)

SKY_WEAKPROXY_UNARY(len, ssize_t, sky_object_len)
SKY_WEAKPROXY_UNARY(iter, sky_object_t, sky_object_iter)
SKY_WEAKPROXY_BINARY(next, sky_object_t, sky_object_next)
SKY_WEAKPROXY_BINARY(contains, sky_bool_t, sky_object_contains)

SKY_WEAKPROXY_BINARY(add, sky_object_t, sky_number_add)
SKY_WEAKPROXY_BINARY(sub, sky_object_t, sky_number_subtract)
SKY_WEAKPROXY_BINARY(mul, sky_object_t, sky_number_multiply)
SKY_WEAKPROXY_BINARY(truediv, sky_object_t, sky_number_truedivide)
SKY_WEAKPROXY_BINARY(floordiv, sky_object_t, sky_number_floordivide)
SKY_WEAKPROXY_BINARY(mod, sky_object_t, sky_number_modulo)
SKY_WEAKPROXY_BINARY(divmod, sky_object_t, sky_number_divmod)
SKY_WEAKPROXY_BINARY(lshift, sky_object_t, sky_number_lshift)
SKY_WEAKPROXY_BINARY(rshift, sky_object_t, sky_number_rshift)
SKY_WEAKPROXY_BINARY(and, sky_object_t, sky_number_and)
SKY_WEAKPROXY_BINARY(xor, sky_object_t, sky_number_xor)
SKY_WEAKPROXY_BINARY(or, sky_object_t, sky_number_or)

SKY_WEAKPROXY_REVERSE_BINARY(radd, sky_object_t, sky_number_add)
SKY_WEAKPROXY_REVERSE_BINARY(rsub, sky_object_t, sky_number_subtract)
SKY_WEAKPROXY_REVERSE_BINARY(rmul, sky_object_t, sky_number_multiply)
SKY_WEAKPROXY_REVERSE_BINARY(rtruediv, sky_object_t, sky_number_truedivide)
SKY_WEAKPROXY_REVERSE_BINARY(rfloordiv, sky_object_t, sky_number_floordivide)
SKY_WEAKPROXY_REVERSE_BINARY(rmod, sky_object_t, sky_number_modulo)
SKY_WEAKPROXY_REVERSE_BINARY(rdivmod, sky_object_t, sky_number_divmod)
SKY_WEAKPROXY_REVERSE_BINARY(rlshift, sky_object_t, sky_number_lshift)
SKY_WEAKPROXY_REVERSE_BINARY(rrshift, sky_object_t, sky_number_rshift)
SKY_WEAKPROXY_REVERSE_BINARY(rand, sky_object_t, sky_number_and)
SKY_WEAKPROXY_REVERSE_BINARY(rxor, sky_object_t, sky_number_xor)
SKY_WEAKPROXY_REVERSE_BINARY(ror, sky_object_t, sky_number_or)

SKY_WEAKPROXY_BINARY(iadd, sky_object_t, sky_number_inplace_add)
SKY_WEAKPROXY_BINARY(isub, sky_object_t, sky_number_inplace_subtract)
SKY_WEAKPROXY_BINARY(imul, sky_object_t, sky_number_inplace_multiply)
SKY_WEAKPROXY_BINARY(itruediv, sky_object_t, sky_number_inplace_truedivide)
SKY_WEAKPROXY_BINARY(ifloordiv, sky_object_t, sky_number_inplace_floordivide)
SKY_WEAKPROXY_BINARY(imod, sky_object_t, sky_number_inplace_modulo)
SKY_WEAKPROXY_BINARY(ilshift, sky_object_t, sky_number_inplace_lshift)
SKY_WEAKPROXY_BINARY(irshift, sky_object_t, sky_number_inplace_rshift)
SKY_WEAKPROXY_BINARY(iand, sky_object_t, sky_number_inplace_and)
SKY_WEAKPROXY_BINARY(ixor, sky_object_t, sky_number_inplace_xor)
SKY_WEAKPROXY_BINARY(ior, sky_object_t, sky_number_inplace_or)

SKY_WEAKPROXY_UNARY(neg, sky_object_t, sky_number_negative);
SKY_WEAKPROXY_UNARY(pos, sky_object_t, sky_number_positive);
SKY_WEAKPROXY_UNARY(abs, sky_object_t, sky_number_absolute);
SKY_WEAKPROXY_UNARY(invert, sky_object_t, sky_number_invert);
SKY_WEAKPROXY_UNARY(int, sky_object_t, sky_number_integer);
SKY_WEAKPROXY_UNARY(float, sky_object_t, sky_number_float);
SKY_WEAKPROXY_UNARY(index, sky_object_t, sky_number_index);


static sky_object_t
sky_weakproxy_call(sky_object_t proxy, sky_tuple_t args, sky_dict_t kws)
{
    return sky_object_call(sky_weakproxy_object(proxy), args, kws);
}

static void
sky_weakproxy_delattr(sky_object_t proxy, sky_object_t attr)
{
    sky_object_delattr(sky_weakproxy_object(proxy), attr);
}

static void
sky_weakproxy_delitem(sky_object_t proxy, sky_object_t item)
{
    sky_object_delitem(sky_weakproxy_object(proxy), item);
}

static sky_object_t
sky_weakproxy_getattribute(sky_object_t proxy, sky_object_t attr)
{
    return sky_object_getattr(sky_weakproxy_object(proxy),
                              attr,
                              sky_NotSpecified);
}

static sky_object_t
sky_weakproxy_getitem(sky_object_t proxy, sky_object_t item)
{
    return sky_object_getitem(sky_weakproxy_object(proxy), item);
}

static sky_object_t
sky_weakproxy_ipow(sky_object_t proxy, sky_object_t other, sky_object_t modulo)
{
    return sky_number_inplace_pow(sky_weakproxy_object(proxy), other, modulo);
}

static sky_object_t
sky_weakproxy_pow(sky_object_t proxy, sky_object_t other, sky_object_t modulo)
{
    return sky_number_pow(sky_weakproxy_object(proxy), other, modulo);
}

static sky_object_t
sky_weakproxy_rpow(sky_object_t proxy, sky_object_t other, sky_object_t modulo)
{
    return sky_number_pow(other, sky_weakproxy_object(proxy), modulo);
}

static void
sky_weakproxy_setattr(sky_object_t proxy, sky_object_t attr, sky_object_t value)
{
    sky_object_setattr(sky_weakproxy_object(proxy), attr, value);
}

static void
sky_weakproxy_setitem(sky_object_t proxy, sky_object_t item, sky_object_t value)
{
    sky_object_setattr(sky_weakproxy_object(proxy), item, value);
}


sky_callableweakproxy_t
sky_callableweakproxy_create(sky_object_t object, sky_object_t callback)
{
    sky_callableweakproxy_t proxy;

    proxy = sky_object_allocate(sky_callableweakproxy_type);
    sky_weakproxy_setup(proxy, object, callback);

    return proxy;
}


sky_weakproxy_t
sky_weakproxy_create(sky_object_t object, sky_object_t callback)
{
    sky_weakproxy_t proxy;

    proxy = sky_object_allocate(sky_weakproxy_type);
    sky_weakproxy_setup(proxy, object, callback);

    return proxy;
}


SKY_TYPE_DEFINE_SIMPLE(weakproxy,
                       "weakproxy",
                       sizeof(sky_weakproxy_data_t),
                       NULL,
                       NULL,
                       sky_weakproxy_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

SKY_TYPE_DEFINE_SIMPLE(callableweakproxy,
                       "callableweakproxy",
                       sizeof(sky_weakproxy_data_t),
                       NULL,
                       NULL,
                       sky_weakproxy_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

static void
sky_weakproxy_type_setmethodslots(sky_type_t type)
{
    sky_type_setattr_builtin(type, "__hash__", sky_None);
    sky_type_setmethodslots(type,
            "__init__", sky_weakproxy_init,
            "__repr__", sky_weakproxy_repr,
            "__str__", sky_weakproxy_str,
            "__bytes__", sky_weakproxy_bytes,
            "__lt__", sky_weakproxy_lt,
            "__le__", sky_weakproxy_le,
            "__eq__", sky_weakproxy_eq,
            "__ne__", sky_weakproxy_ne,
            "__gt__", sky_weakproxy_gt,
            "__ge__", sky_weakproxy_ge,
            "__bool__", sky_weakproxy_bool,

            "__getattribute__", sky_weakproxy_getattribute,
            "__setattr__", sky_weakproxy_setattr,
            "__delattr__", sky_weakproxy_delattr,

            "__len__", sky_weakproxy_len,
            "__getitem__", sky_weakproxy_getitem,
            "__setitem__", sky_weakproxy_setitem,
            "__delitem__", sky_weakproxy_delitem,
            "__iter__", sky_weakproxy_iter,
            "__next__", sky_weakproxy_next,
            "__contains__", sky_weakproxy_contains,

            "__add__", sky_weakproxy_add,
            "__sub__", sky_weakproxy_sub,
            "__mul__", sky_weakproxy_mul,
            "__truediv__", sky_weakproxy_truediv,
            "__floordiv__", sky_weakproxy_floordiv,
            "__mod__", sky_weakproxy_mod,
            "__divmod__", sky_weakproxy_divmod,
            "__pow__", sky_weakproxy_pow,
            "__lshift__", sky_weakproxy_lshift,
            "__rshift__", sky_weakproxy_rshift,
            "__and__", sky_weakproxy_and,
            "__xor__", sky_weakproxy_xor,
            "__or__", sky_weakproxy_or,

            "__radd__", sky_weakproxy_radd,
            "__rsub__", sky_weakproxy_rsub,
            "__rmul__", sky_weakproxy_rmul,
            "__rtruediv__", sky_weakproxy_rtruediv,
            "__rfloordiv__", sky_weakproxy_rfloordiv,
            "__rmod__", sky_weakproxy_rmod,
            "__rdivmod__", sky_weakproxy_rdivmod,
            "__rpow__", sky_weakproxy_rpow,
            "__rlshift__", sky_weakproxy_rlshift,
            "__rrshift__", sky_weakproxy_rrshift,
            "__rand__", sky_weakproxy_rand,
            "__rxor__", sky_weakproxy_rxor,
            "__ror__", sky_weakproxy_ror,

            "__iadd__", sky_weakproxy_iadd,
            "__isub__", sky_weakproxy_isub,
            "__imul__", sky_weakproxy_imul,
            "__itruediv__", sky_weakproxy_itruediv,
            "__ifloordiv__", sky_weakproxy_ifloordiv,
            "__imod__", sky_weakproxy_imod,
            "__ipow__", sky_weakproxy_ipow,
            "__ilshift__", sky_weakproxy_ilshift,
            "__irshift__", sky_weakproxy_irshift,
            "__iand__", sky_weakproxy_iand,
            "__ixor__", sky_weakproxy_ixor,
            "__ior__", sky_weakproxy_ior,

            "__neg__", sky_weakproxy_neg,
            "__pos__", sky_weakproxy_pos,
            "__abs__", sky_weakproxy_abs,
            "__invert__", sky_weakproxy_invert,
            "__int__", sky_weakproxy_int,
            "__float__", sky_weakproxy_float,
            "__index__", sky_weakproxy_index,

            NULL);
}

void
sky_weakproxy_initialize_library(void)
{
    sky_error_validate_debug(NULL == sky_weakproxy_init_parameters);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_weakproxy_init_parameters),
            sky_parameter_list_create(),
            SKY_TRUE);
    sky_parameter_list_add(sky_weakproxy_init_parameters,
                           SKY_STRING_LITERAL("object"),
                           SKY_DATA_TYPE_OBJECT,
                           NULL);
    sky_parameter_list_add(sky_weakproxy_init_parameters,
                           SKY_STRING_LITERAL("callback"),
                           SKY_DATA_TYPE_OBJECT,
                           sky_None);

    sky_type_initialize_builtin(sky_weakproxy_type, 0);
    sky_weakproxy_type_setmethodslots(sky_weakproxy_type);

    sky_type_initialize_builtin(sky_callableweakproxy_type, 0);
    sky_weakproxy_type_setmethodslots(sky_callableweakproxy_type);
    sky_type_setmethodslots(sky_callableweakproxy_type,
            "__call__", sky_weakproxy_call,
            NULL);
}
