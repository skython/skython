#include "sky_private.h"


typedef struct sky_iterator_data_s {
    sky_object_t                        object;
    ssize_t                             next_index;
} sky_iterator_data_t;

SKY_EXTERN_INLINE sky_iterator_data_t *
sky_iterator_data(sky_object_t object)
{
    if (sky_iterator_type == sky_object_type(object)) {
        return (sky_iterator_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_iterator_type);
}

struct sky_iterator_s {
    sky_object_data_t                   object_data;
    sky_iterator_data_t                 iterator_data;
};


static void
sky_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_iterator_data_t *iterator_data = data;

    sky_object_visit(iterator_data->object, visit_data);
}


void
sky_iterator_init(sky_object_t self, sky_object_t sequence)
{
    sky_iterator_data_t *iterator_data = sky_iterator_data(self);

    sky_object_gc_set(&(iterator_data->object), sequence, self);
}


sky_object_t
sky_iterator_iter(sky_object_t self)
{
    return self;
}


sky_object_t
sky_iterator_length_hint(sky_object_t self)
{
    sky_iterator_data_t *iterator_data = sky_iterator_data(self);

    ssize_t hint;

    if (!iterator_data->object) {
        return sky_integer_zero;
    }
    hint = sky_object_len(iterator_data->object) - iterator_data->next_index;
    return sky_integer_create(hint);
}


sky_object_t
sky_iterator_next(sky_object_t self)
{
    sky_iterator_data_t * volatile  iterator_data = sky_iterator_data(self);

    sky_object_t    object;
    sky_integer_t   index;

    if (!iterator_data->object) {
        sky_error_raise_object(sky_StopIteration, sky_None);
    }

    SKY_ERROR_TRY {
        index = sky_integer_create(iterator_data->next_index);
        object = sky_object_getitem(iterator_data->object, index);
        ++iterator_data->next_index;
    } SKY_ERROR_EXCEPT(sky_IndexError) {
        iterator_data->object = NULL;
        object = NULL;
    } SKY_ERROR_EXCEPT(sky_StopIteration) {
        iterator_data->object = NULL;
        object = NULL;
    } SKY_ERROR_TRY_END;

    return object;
}


sky_object_t
sky_iterator_reduce(sky_object_t self)
{
    sky_iterator_data_t *iterator_data = sky_iterator_data(self);

    if (!iterator_data->object) {
        return sky_object_build("(O(()))", sky_module_getbuiltin("iter"));
    }
    return sky_object_build("(O(O)iz)",
                            sky_module_getbuiltin("iter"),
                            iterator_data->object,
                            iterator_data->next_index);
}


void
sky_iterator_setstate(sky_object_t self, sky_object_t state)
{
    sky_iterator_data_t *iterator_data = sky_iterator_data(self);

    iterator_data->next_index = sky_integer_value(sky_number_index(state),
                                                  SSIZE_MIN, SSIZE_MAX,
                                                  NULL);
    if (iterator_data->next_index < 0) {
        iterator_data->next_index = 0;
    }
}


static const char sky_iterator_type_doc[] =
"iterator(sequence) -> iterator over values of the sequence\n"
"\n"
"Return an iterator";


SKY_TYPE_DEFINE_SIMPLE(iterator,
                       "iterator",
                       sizeof(sky_iterator_data_t),
                       NULL,
                       NULL,
                       sky_iterator_instance_visit,
                       0,
                       sky_iterator_type_doc);


void
sky_iterator_initialize_library(void)
{
    sky_type_initialize_builtin(sky_iterator_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_iterator_type,
            "__init__",
            (sky_native_code_function_t)sky_iterator_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "sequence", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(sky_iterator_type,
            "__length_hint__", sky_iterator_length_hint,
            "__iter__", sky_iterator_iter,
            "__next__", sky_iterator_next,
            "__reduce__", sky_iterator_reduce,
            "__setstate__", sky_iterator_setstate,
            NULL);
}
