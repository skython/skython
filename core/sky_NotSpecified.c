#include "sky_private.h"


SKY_TYPE_DEFINE_SIMPLE(NotSpecified,
                       "NotSpecifiedType",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

sky_NotSpecified_t const sky_NotSpecified = SKY_OBJECT_TAGGED_NOTSPECIFIED;


sky_object_t
sky_NotSpecified_new(SKY_UNUSED sky_type_t type, sky_tuple_t args, sky_dict_t kws)
{
    if ((args && sky_object_len(args)) || (kws && sky_object_len(kws))) {
        sky_error_raise_string(sky_TypeError,
                               "NotSpecifiedType takes no arguments");
    }
    return sky_NotSpecified;
}


sky_string_t
sky_NotSpecified_repr(SKY_UNUSED sky_object_t self)
{
    return SKY_STRING_LITERAL("NotSpecified");
}


void
sky_NotSpecified_initialize_library(void)
{
    sky_type_setmethodslots(sky_NotSpecified_type,
            "__new__", sky_NotSpecified_new,
            "__repr__", sky_NotSpecified_repr,
            NULL);
}
