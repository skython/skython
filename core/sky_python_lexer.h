#ifndef __SKYTHON_CORE_SKY_PYTHON_LEXER_H__
#define __SKYTHON_CORE_SKY_PYTHON_LEXER_H__ 1


#include "sky_base.h"
#include "sky_buffer.h"
#include "sky_object.h"
#include "sky_python_token.h"
#include "sky_string_private.h"


SKY_CDECLS_BEGIN


typedef struct sky_python_lexer_s sky_python_lexer_t;

typedef sky_unicode_char_t (*sky_python_lexer_getc_t)(sky_python_lexer_t *);
typedef void (*sky_python_lexer_ungetc_t)(sky_python_lexer_t *);


typedef enum sky_python_lexer_flag_e {
    SKY_PYTHON_LEXER_FLAG_ENCODING_SET  =   0x01000000,
    SKY_PYTHON_LEXER_FLAG_NEWLINE       =   0x02000000,
    SKY_PYTHON_LEXER_FLAG_EOF           =   0x04000000,
    SKY_PYTHON_LEXER_FLAG_FILE_SOURCE   =   0x08000000,
    SKY_PYTHON_LEXER_FLAG_INTERACTIVE   =   0x10000000,
} sky_python_lexer_flag_t;


struct sky_python_lexer_s {
    sky_object_t                        filename;
    sky_object_t                        source;
    sky_buffer_t                        buffer;
    sky_object_t                        readline;
    sky_string_t                        ps1;
    sky_string_t                        ps2;

    int                                 lineno;
    int                                 col_offset;
    int                                 nesting_level;
    unsigned int                        flags;

    int                                 pending_dedents;
    int                                 indent_stack[100];
    int *                               indent_stack_pointer;

    int                                 tab_col_offset;
    int                                 tab_indent_stack[100];
    int *                               tab_indent_stack_pointer;

    const sky_codec_t *                 codec;
    sky_string_t                        encoding;
    sky_python_lexer_getc_t             decoder_getc;
    sky_python_lexer_ungetc_t           decoder_ungetc;

    const uint8_t *                     start;      /* points into buffer */
    const uint8_t *                     end;        /* points into buffer */
    const uint8_t *                     next_line;

    sky_string_data_t                   decode_buffer;
    union {
        const uint8_t *                 byte;
        ssize_t                         offset;
        const uint16_t *                ucs2;
        const sky_unicode_char_t *      ucs4;
    } next;

    sky_string_data_t                   source_tagged_data;
};


SKY_EXTERN void
sky_python_lexer_init(sky_python_lexer_t *  lexer,
                      sky_object_t          source,
                      sky_string_t          encoding,
                      sky_string_t          filename,
                      sky_string_t          ps1,
                      sky_string_t          ps2,
                      unsigned int          flags);

SKY_EXTERN sky_python_lexer_t *
sky_python_lexer_create(sky_object_t    source,
                        sky_string_t    encoding,
                        sky_string_t    filename,
                        sky_string_t    ps1,
                        sky_string_t    ps2,
                        unsigned int    flags);


SKY_EXTERN void
sky_python_lexer_cleanup(sky_python_lexer_t *lexer);

SKY_EXTERN void
sky_python_lexer_destroy(sky_python_lexer_t *lexer);


SKY_EXTERN sky_bool_t
sky_python_lexer_token(sky_python_lexer_t * lexer,
                       sky_python_token_t * token);


SKY_EXTERN sky_string_t
sky_python_lexer_line(sky_python_lexer_t *  lexer,
                      int                   lineno);

SKY_EXTERN sky_unicode_char_t
sky_python_lexer_getc(sky_python_lexer_t *lexer);

SKY_EXTERN void
sky_python_lexer_ungetc(sky_python_lexer_t *lexer);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PYTHON_LEXER_H__ */
