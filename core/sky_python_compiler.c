#include "sky_private.h"

#include <math.h>

#include "sky_code.h"
#include "sky_code_private.h"

#include "sky_python_ast.h"
#include "sky_python_compiler.h"
#include "sky_python_symtable.h"


/* NOTE errors normally caught by the parser generating AST must also be caught
 *      in the compiler as well, because it is possible for manually modified
 *      (or generated if someone is sick enough) AST to be compiled, bypassing
 *      the checks in the parser. Read: do NOT remove "redundant" error checks
 *      from the compiler code.
 */


/* Constant slices would be nice, but they're not possible because they're not
 * hashable. This can be worked around in the compiler code here, but it can't
 * be worked around in the code object, because the code object must be
 * hashable, and it must consider all of its constants in the hash. Slice
 * objects are not hashable for very good reason, so changing that for this is
 * not an option.
 */
#undef SKY_PYTHON_COMPILER_CONSTANT_SLICES


static void SKY_NORETURN
sky_python_compiler_raise_(sky_python_compiler_t *  compiler,
                           sky_type_t               type,
                           const char *             c_function,
                           const char *             c_filename,
                           int                      c_lineno,
                           const char *             fmt,
                           ...)
{
    va_list         ap;
    sky_string_t    message, text;

    va_start(ap, fmt);
    message = sky_string_createfromformat(fmt, ap);
    va_end(ap);

    text = sky_python_compiler_line(compiler->filename,
                                    compiler->code->current_block->lineno);
    if (!sky_object_bool(text)) {
        text = (sky_string_t)sky_None;
    }

    sky_error_raise_(type,
                     sky_object_build("(O(OiiO))",
                                      message,
                                      compiler->filename,
                                      compiler->code->current_block->lineno,
                                      compiler->code->current_block->col_offset,
                                      text),
                     c_function,
                     c_filename,
                     c_lineno);
}

#define sky_python_compiler_raise(compiler, type, fmt, ...)                 \
        sky_python_compiler_raise_(compiler, type,                          \
                                   __FUNCTION__, __FILE__, __LINE__,        \
                                   fmt, ## __VA_ARGS__)


static sky_bool_t
sky_python_compiler_compare_constants(sky_object_t x, sky_object_t y)
{
    ssize_t     i, len;
    double      x_double, y_double;
    sky_type_t  type = sky_object_type(x);

    /* Consider object types in constant comparisons. Types must be exact for
     * a comparison to be considered equal. Normally 0 == False and 1 == True,
     * but they must not be considered equal here.
     */
    if (sky_object_type(y) != type) {
        return SKY_FALSE;
    }

    /* Normally -0.0 will compare equal to 0.0, but for constants, that is not
     * acceptable. Similarly for complex numbers, the real and imaginary parts
     * should not compare equal for the same case. Handle tuples too, because
     * floats and complex numbers can appear in tuples.
     */
    if (type == sky_float_type) {
        x_double = sky_float_value(x);
        y_double = sky_float_value(y);
        return (x_double == y_double &&
                copysign(1.0, x_double) == copysign(1.0, y_double));
    }
    if (type == sky_complex_type) {
        x_double = sky_complex_real(x);
        y_double = sky_complex_real(y);
        if (x_double == y_double &&
            copysign(1.0, x_double) == copysign(1.0, y_double))
        {
            x_double = sky_complex_imag(x);
            y_double = sky_complex_imag(y);
            return (x_double == y_double &&
                    copysign(1.0, x_double) == copysign(1.0, y_double));
        }
        return SKY_FALSE;
    }
    if (type == sky_tuple_type) {
        if ((len = sky_tuple_len(x)) != sky_tuple_len(y)) {
            return SKY_FALSE;
        }
        for (i = 0; i < len; ++i) {
            if (!sky_python_compiler_compare_constants(sky_tuple_get(x, i),
                                                       sky_tuple_get(y, i)))
            {
                return SKY_FALSE;
            }
        }
        return SKY_TRUE;
    }

    return sky_object_compare(x, y, SKY_COMPARE_OP_EQUAL);
}


static sky_bool_t
sky_python_compiler_table_item_compare(sky_hashtable_item_t *   item,
                                       void *                   key)
{
    sky_python_compiler_table_item_t    *table_item;

    table_item = (sky_python_compiler_table_item_t *)item;
    return sky_object_compare(table_item->key, key, SKY_COMPARE_OP_EQUAL);
}


static sky_bool_t
sky_python_compiler_constant_table_item_compare(sky_hashtable_item_t *  item,
                                                void *                  key)
{
    sky_python_compiler_table_item_t    *table_item;

    table_item = (sky_python_compiler_table_item_t *)item;
    return sky_python_compiler_compare_constants(table_item->key, key);
}


static void
sky_python_compiler_table_init(sky_python_compiler_table_t *table,
                               sky_hashtable_item_compare_t compare)
{
    sky_hashtable_init(&(table->base), compare);
    table->items_head = NULL;
    table->items_tail = NULL;
    table->readonly = SKY_FALSE;
}


static void
sky_python_compiler_table_cleanup(sky_python_compiler_table_t *table)
{
    sky_hashtable_cleanup(&(table->base), sky_free);
}


static sky_python_compiler_table_item_t *
sky_python_compiler_table_set(sky_python_compiler_table_t * table,
                              sky_object_t                  key)
{
    uintptr_t                           key_hash;
    sky_python_compiler_table_item_t    *table_item;

    sky_error_validate_debug(!table->readonly);

    table_item = sky_malloc(sizeof(sky_python_compiler_table_item_t));
    table_item->key = key;
    table_item->index = sky_hashtable_length(&(table->base));
    table_item->next = NULL;

#if defined(SKY_PYTHON_COMPILER_CONSTANT_SLICES)
    if (!sky_object_isa(key, sky_slice_type)) {
        key_hash = sky_object_hash(key) ^
                   ((uintptr_t)sky_object_type(key) >> 4);
    }
    else {
        sky_tuple_t tuple = sky_tuple_pack(3, sky_slice_start(key),
                                              sky_slice_stop(key),
                                              sky_slice_step(key));
        key_hash = sky_object_hash(tuple) ^
                   ((uintptr_t)sky_object_type(key) >> 4);
    }
#else
    key_hash = sky_object_hash(key) ^
               ((uintptr_t)sky_object_type(key) >> 4);
#endif

    if (&(table_item->base) != sky_hashtable_insert(&(table->base),
                                                    &(table_item->base),
                                                    key_hash,
                                                    key))
    {
        sky_error_fatal("internal error");
    }
    *(table->items_tail ? &(table->items_tail->next)
                        : &(table->items_head)) = table_item;
    table->items_tail = table_item;

    return table_item;
}


static ssize_t
sky_python_compiler_table_get(sky_python_compiler_table_t * table,
                              sky_object_t                  key)
{
    uintptr_t                           key_hash;
    sky_python_compiler_table_item_t    *table_item;

#if defined(SKY_PYTHON_COMPILER_CONSTANT_SLICES)
    if (!sky_object_isa(key, sky_slice_type)) {
        key_hash = sky_object_hash(key) ^
                   ((uintptr_t)sky_object_type(key) >> 4);
    }
    else {
        sky_tuple_t tuple = sky_tuple_pack(3, sky_slice_start(key),
                                              sky_slice_stop(key),
                                              sky_slice_step(key));
        key_hash = sky_object_hash(tuple) ^
                   ((uintptr_t)sky_object_type(key) >> 4);
    }
#else
    key_hash = sky_object_hash(key) ^
               ((uintptr_t)sky_object_type(key) >> 4);
#endif

    if (!(table_item = (sky_python_compiler_table_item_t *)
                       sky_hashtable_lookup(&(table->base),
                                            key_hash,
                                            key)))
    {
        if (table->readonly) {
            return -1;
        }
        table_item = sky_python_compiler_table_set(table, key);
    }

    return table_item->index;
}


static void
sky_python_compiler_table_load(sky_python_compiler_table_t *table,
                               sky_dict_t                   symbols,
                               intmax_t                     scope,
                               intmax_t                     mask)
{
    intmax_t        flags;
    sky_list_t      keys;
    sky_object_t    key, value;

    keys = sky_list_create(symbols);
    sky_list_sort(keys, NULL, SKY_FALSE);

    SKY_SEQUENCE_FOREACH(keys, key) {
        value = sky_dict_getitem(symbols, key);
        flags = sky_integer_value(value, INTMAX_MIN, INTMAX_MAX, NULL);

        if (sky_python_symtable_symbol_scope(flags) == scope ||
            (flags & mask))
        {
            sky_python_compiler_table_set(table, key);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static sky_tuple_t
sky_python_compiler_table_tuple(sky_python_compiler_table_t *table)
{
    ssize_t                             len;
    sky_tuple_t                         tuple;
    sky_object_t                        *objects;
    sky_python_compiler_table_item_t    *item;

    if (!(len = sky_hashtable_length(&(table->base)))) {
        return sky_tuple_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_malloc(len * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);
        for (item = table->items_head; item; item = item->next) {
            objects[item->index] = item->key;
        }

        tuple = sky_tuple_createwitharray(len, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}


static ssize_t
sky_python_compiler_block_addop(sky_python_compiler_block_t *   block,
                                sky_code_opcode_t               opcode,
                                ...)
{
    ssize_t                     enter_op_index;
    va_list                     ap;
    sky_python_compiler_op_t    *enter_op, *last_op, *op;
    sky_python_compiler_block_t *enter_block;

    sky_error_validate_debug((int)opcode >= 0 && (int)opcode <= 255);
    sky_error_validate_debug(sky_code_opcodes[opcode].name != NULL);

    /* If the last op is a jump, no more ops can be added to this block. If the
     * jump is conditional, it is an internal error. An unconditional jump or
     * exit can legitimately occur, causing unreachable code.
     */
    if (block->ops_len) {
        last_op = &(block->ops[block->ops_len - 1]);
        sky_error_validate_debug(
                !sky_code_opcode_isconditional_jump(last_op->opcode) &&
                !sky_code_opcode_isguard_enter(last_op->opcode));
    }

    if (block->ops_len >= block->ops_size) {
        block->ops = sky_realloc(block->ops,
                                 (block->ops_size + 1) *
                                 sizeof(sky_python_compiler_op_t));
        block->ops_size = sky_memsize(block->ops) /
                          sizeof(sky_python_compiler_op_t);
    }
    op = &(block->ops[block->ops_len++]);

    op->opcode = opcode;
    op->lineno = block->lineno;
    op->offset = -1;

    if (sky_code_opcode_isguard_exit(opcode) ||
        sky_code_opcode_ishandler_exit(opcode))
    {
        va_start(ap, opcode);
        enter_block = va_arg(ap, sky_python_compiler_block_t *);
        enter_op_index = va_arg(ap, ssize_t);
        sky_error_validate_debug(enter_op_index >= 0);
        sky_error_validate_debug(enter_op_index < enter_block->ops_len);
        enter_op = &(enter_block->ops[enter_op_index]);
        if (sky_code_opcode_isguard_exit(opcode)) {
            sky_error_validate_debug(
                    sky_code_opcode_isguard_enter(enter_op->opcode));
        }
        else {
            sky_error_validate_debug(
                    sky_code_opcode_ishandler_enter(enter_op->opcode));
        }
        enter_op->u.enter.exit_block = block;
        enter_op->u.enter.exit_op_index = block->ops_len - 1;
        va_end(ap);

        op->u.exit.enter_block = enter_block;
        op->u.exit.enter_op_index = enter_op_index;
    }
    else if (sky_code_opcodes[opcode].argc > 0) {
        va_start(ap, opcode);
        if (sky_code_opcode_isguard_enter(opcode)) {
            op->u.enter.jump_block = va_arg(ap, sky_python_compiler_block_t *);
            op->u.enter.jump_offset = 0;
            op->u.enter.exit_block = NULL;
            op->u.enter.exit_op_index = -1;
            op->u.enter.exit_offset = 0;
            if (SKY_CODE_OPCODE_TRY_EXCEPT == opcode) {
                op->u.enter.orelse_block = va_arg(ap, sky_python_compiler_block_t *);
            }
            else {
                op->u.enter.orelse_block = NULL;
            }
            op->u.enter.guard_id = va_arg(ap, ssize_t);
        }
        else if (sky_code_opcode_ishandler_enter(opcode)) {
            op->u.enter.jump_block = NULL;
            op->u.enter.jump_offset = 0;
            op->u.enter.exit_block = NULL;
            op->u.enter.exit_op_index = -1;
            op->u.enter.exit_offset = 0;
            op->u.enter.orelse_block = NULL;
            op->u.enter.guard_id = va_arg(ap, ssize_t);
        }
        else if (sky_code_opcode_isjump(opcode)) {
            op->u.jump.target = va_arg(ap, sky_python_compiler_block_t *);
            op->u.jump.offset = 0;
        }
        else {
            op->u.argv[0] = va_arg(ap, size_t);
            if (sky_code_opcodes[opcode].argc > 1) {
                op->u.argv[1] = va_arg(ap, size_t);
                if (sky_code_opcodes[opcode].argc > 2) {
                    op->u.argv[2] = va_arg(ap, size_t);
                    sky_error_validate_debug(3 == sky_code_opcodes[opcode].argc);
                }
            }
        }
        va_end(ap);
    }

    return block->ops_len - 1;
}


static ssize_t
sky_python_compiler_block_count(sky_python_compiler_block_t *   block,
                                ssize_t                         count)
{
    sky_python_compiler_op_t    *op;

    if (block->counted) {
        return count;
    }
    block->counted = 1;

    if (block->flow_next) {
        count = sky_python_compiler_block_count(block->flow_next, count);
    }
    sky_error_validate_debug(block->ops_len > 0);
    op = &(block->ops[block->ops_len - 1]);
    if (sky_code_opcode_isjump(op->opcode)) {
        count = sky_python_compiler_block_count(op->u.jump.target, count);
    }
    else if (sky_code_opcode_isguard_enter(op->opcode)) {
        count = sky_python_compiler_block_count(op->u.enter.jump_block, count);
        if (op->u.enter.orelse_block) {
            count = sky_python_compiler_block_count(op->u.enter.orelse_block,
                                                    count);
        }
    }

    return count + 1;
}


static sky_python_compiler_block_t *
sky_python_compiler_block_create(sky_python_compiler_code_t *code)
{
    sky_python_compiler_block_t *block;

    block = sky_calloc(1, sizeof(sky_python_compiler_block_t));
    block->bytecode_size = -1;
    block->layout_index = -1;
    block->stack_entry = -1;
    block->guard_id = code->guard_id;

    if (code->current_block) {
        block->lineno = code->current_block->lineno;
        block->col_offset = code->current_block->col_offset;
    }

    block->alloc_next = code->blocks;
    code->blocks = block;

    return block;
}


static void
sky_python_compiler_block_destroy(sky_python_compiler_block_t *block)
{
    sky_free(block->ops);
    sky_free(block);
}


static inline void
sky_python_compiler_block_setflownext(sky_python_compiler_block_t * block,
                                      sky_python_compiler_block_t * flow_next)
{
    sky_error_validate_debug(!block->flow_next);
    sky_error_validate_debug(flow_next != block);
    block->flow_next = flow_next;
}


static void
sky_python_compiler_block_reachable(sky_python_compiler_block_t *block)
{
    sky_python_compiler_op_t    *last_op;

    if (block->reachable) {
        return;
    }
    block->reachable = 1;

    last_op = &(block->ops[block->ops_len - 1]);
    if (sky_code_opcode_isunconditional_jump(last_op->opcode)) {
        sky_python_compiler_block_reachable(last_op->u.jump.target);
        sky_error_validate_debug(NULL == block->flow_next);
        return;
    }
    if (sky_code_opcode_isexit(last_op->opcode)) {
        sky_error_validate_debug(NULL == block->flow_next);
        return;
    }
    if (SKY_CODE_OPCODE_FINALLY_EXIT == last_op->opcode) {
        sky_error_validate_debug(NULL == block->flow_next);
        return;
    }

    if (sky_code_opcode_isconditional_jump(last_op->opcode)) {
        sky_python_compiler_block_reachable(last_op->u.jump.target);
    }
    else if (sky_code_opcode_isguard_enter(last_op->opcode)) {
        sky_python_compiler_block_reachable(last_op->u.enter.jump_block);
        if (last_op->u.enter.orelse_block) {
            sky_python_compiler_block_reachable(last_op->u.enter.orelse_block);
        }
    }

    sky_error_validate_debug(NULL != block->flow_next);
    sky_python_compiler_block_reachable(block->flow_next);
}


static void
sky_python_compiler_block_analyze(sky_python_compiler_block_t *   block,
                                  sky_python_compiler_code_t *    code,
                                  const ssize_t                   stack_entry)
{
    ssize_t                     popc, pushc, stack_depth, target_stack_entry;
    sky_python_compiler_op_t    *last_op, *op;

    if (-1 != block->stack_entry) {
        sky_error_validate_debug(stack_entry == block->stack_entry);
        return;
    }
    block->stack_entry = stack_entry;
    block->analyzed = 1;

    if (!block->ops_len) {
        last_op = NULL;
        goto flow_next;
    }

    last_op = &(block->ops[block->ops_len - 1]);
    for (op = block->ops; op <= last_op; ++op) {
        if ((popc = sky_code_opcodes[op->opcode].popc) < 0) {
            switch (op->opcode) {
                case SKY_CODE_OPCODE_BUILD_CLASS:
                    popc = 2 + op->u.argv[1] + (op->u.argv[2] * 2);
                    if (op->u.argv[0] & SKY_CODE_BUILD_CLASS_FLAG_ARGS) {
                        ++popc;
                    }
                    if (op->u.argv[0] & SKY_CODE_BUILD_CLASS_FLAG_KWS) {
                        ++popc;
                    }
                    if (op->u.argv[0] & SKY_CODE_BUILD_CLASS_FLAG_CLOSURE) {
                        ++popc;
                    }
                    break;
                case SKY_CODE_OPCODE_BUILD_DICT:
                    popc = op->u.argv[0] * 2;
                    break;
                case SKY_CODE_OPCODE_BUILD_FUNCTION:
                    popc = 2 + op->u.argv[1] + (op->u.argv[2] * 2);
                    if (op->u.argv[0] & SKY_CODE_BUILD_FUNCTION_FLAG_ANNOTATIONS) {
                        ++popc;
                    }
                    if (op->u.argv[0] & SKY_CODE_BUILD_FUNCTION_FLAG_CLOSURE) {
                        ++popc;
                    }
                    break;
                case SKY_CODE_OPCODE_BUILD_LIST:
                    popc = op->u.argv[0];
                    break;
                case SKY_CODE_OPCODE_BUILD_SET:
                    popc = op->u.argv[0];
                    break;
                case SKY_CODE_OPCODE_BUILD_TUPLE:
                    popc = op->u.argv[0];
                    break;
                case SKY_CODE_OPCODE_CALL:
                    popc = op->u.argv[0] + (op->u.argv[1] * 2) + 1;
                    break;
                case SKY_CODE_OPCODE_CALL_ARGS:
                    popc = op->u.argv[0] + (op->u.argv[1] * 2) + 2;
                    break;
                case SKY_CODE_OPCODE_CALL_KWS:
                    popc = op->u.argv[0] + (op->u.argv[1] * 2) + 2;
                    break;
                case SKY_CODE_OPCODE_CALL_ARGS_KWS:
                    popc = op->u.argv[0] + (op->u.argv[1] * 2) + 3;
                    break;
                case SKY_CODE_OPCODE_RAISE:
                    popc = op->u.argv[0];
                    break;
                default:
                    sky_error_fatal("internal error");
            }
        }
        if ((pushc = sky_code_opcodes[op->opcode].pushc) < 0) {
            switch (op->opcode) {
                case SKY_CODE_OPCODE_UNPACK_SEQUENCE:
                    pushc = op->u.argv[0];
                    break;
                case SKY_CODE_OPCODE_UNPACK_STARRED:
                    pushc = op->u.argv[0] + op->u.argv[1] + 1;
                    break;
                default:
                    sky_error_fatal("internal error");
            }
        }

        if (block->stack_entry + block->stack_delta + pushc >
            block->stack_nadir)
        {
            block->stack_nadir = block->stack_entry +
                                 block->stack_delta + pushc;
            if (block->stack_nadir > code->stacksize) {
                code->stacksize = block->stack_nadir;
            }
        }
        block->stack_delta += (pushc - popc);
        target_stack_entry = block->stack_entry + block->stack_delta;
        sky_error_validate_debug(target_stack_entry >= 0);

        if (sky_code_opcode_isunconditional_jump(op->opcode)) {
            sky_python_compiler_block_analyze(op->u.jump.target,
                                              code,
                                              target_stack_entry);
            return;
        }
        if (sky_code_opcode_isexit(op->opcode)) {
            return;
        }
        if (sky_code_opcode_isguard_enter(op->opcode)) {
            if (SKY_CODE_OPCODE_WITH_ENTER == op->opcode) {
                sky_python_compiler_block_analyze(op->u.enter.jump_block,
                                                  code,
                                                  target_stack_entry - 1);
            }
            else {
                sky_python_compiler_block_analyze(op->u.enter.jump_block,
                                                  code,
                                                  target_stack_entry);
                if (op->u.enter.orelse_block) {
                    sky_python_compiler_block_analyze(op->u.enter.orelse_block,
                                                      code,
                                                      target_stack_entry);
                }
            }
            sky_error_validate_debug(op == last_op);
            break;
        }
        if (sky_code_opcode_isconditional_jump(op->opcode)) {
            sky_python_compiler_block_analyze(op->u.jump.target,
                                              code,
                                              target_stack_entry);
            sky_error_validate_debug(op == last_op);
            break;
        }
    }

flow_next:
    if (block->flow_next) {
        target_stack_entry = block->stack_entry + block->stack_delta;
        sky_error_validate_debug(target_stack_entry >= 0);
        sky_python_compiler_block_analyze(block->flow_next,
                                          code,
                                          target_stack_entry);
    }
    else {
        if (last_op && sky_code_opcode_isconditional_jump(last_op->opcode)) {
            sky_python_compiler_block_t *return_block;

            return_block = sky_python_compiler_block_create(code);
            return_block->stack_entry = block->stack_entry + block->stack_delta;
            return_block->analyzed = 1;
            sky_python_compiler_block_setflownext(block, return_block);
            block = return_block;
        }

        if (!last_op || SKY_CODE_OPCODE_FINALLY_EXIT != last_op->opcode) {
            stack_depth = block->stack_entry + block->stack_delta;
            if (stack_depth) {
                sky_error_validate_debug(1 == stack_depth);
            }
            else {
                sky_python_compiler_block_addop(
                        block,
                        SKY_CODE_OPCODE_PUSH_CONST,
                        sky_python_compiler_table_get(&(code->constants),
                                                      sky_None));
                ++block->stack_delta;
                if (block->stack_entry + block->stack_delta + 1 >
                            block->stack_nadir)
                {
                    block->stack_nadir = block->stack_entry +
                                         block->stack_delta + 1;
                    if (block->stack_nadir > code->stacksize) {
                        code->stacksize = block->stack_nadir;
                    }
                }
            }
            sky_python_compiler_block_addop(block, SKY_CODE_OPCODE_RETURN);
            --block->stack_delta;
        }
    }
}


static void
sky_python_compiler_block_eliminate(sky_python_compiler_block_t *   block,
                                    sky_python_compiler_block_t *   exit_block)
{
    sky_python_compiler_op_t    *enter_op, *exit_op, *last_op, *op;
    sky_python_compiler_block_t *op_block;

    while (block && !block->analyzed) {
        if (!block->ops_len) {
            block = block->flow_next;
            continue;
        }
        last_op = &(block->ops[block->ops_len - 1]);
        for (op = &(block->ops[0]); op <= last_op; ++op) {
            if (sky_code_opcode_isguard_exit(op->opcode) ||
                sky_code_opcode_ishandler_exit(op->opcode))
            {
                op_block = op->u.exit.enter_block;
                enter_op = &(op_block->ops[op->u.exit.enter_op_index]);

                op_block = enter_op->u.enter.exit_block;
                exit_op = &(op_block->ops[enter_op->u.enter.exit_op_index]);

                if (op == exit_op) {
                    enter_op->u.enter.exit_block = exit_block;
                    enter_op->u.enter.exit_op_index = exit_block->ops_len - 1;
                }
            }
            if (sky_code_opcode_isjump(op->opcode) &&
                !op->u.jump.target->analyzed)
            {
                /* XXX I'm not sure exit_block is right here, but I'm not sure
                 *     how to figure out what the right block really is. I can
                 *     look at a code dump for an example that triggers this
                 *     condition, and I can see what the exit block should be,
                 *     but I can't see a way to get there from here. It's also
                 *     8pm on a Friday. Using exit_block seems to work okay,
                 *     but it really shouldn't.
                 *
                 *      try:
                 *          try:
                 *              if True:
                 *                  return
                 *              else:
                 *                  return
                 *          finally:
                 *              print('finally')    # 'pass' won't work
                 *      except:
                 *          return
                 */
                sky_python_compiler_block_eliminate(op->u.jump.target,
                                                    exit_block);
            }
        }
        block = block->flow_next;
    }
}


static void
sky_python_compiler_block_truncate(sky_python_compiler_block_t *block,
                                   sky_python_compiler_op_t *   new_last_op,
                                   sky_python_compiler_op_t *   old_last_op)
{
    sky_python_compiler_op_t    *enter_op, *exit_op, *op;
    sky_python_compiler_block_t *op_block;

    block->ops_len = (new_last_op - block->ops) + 1;
    for (op = new_last_op + 1; op <= old_last_op; ++op) {
        if (sky_code_opcode_isguard_exit(op->opcode) ||
            sky_code_opcode_ishandler_exit(op->opcode))
        {
            op_block = op->u.exit.enter_block;
            enter_op = &(op_block->ops[op->u.exit.enter_op_index]);

            op_block = enter_op->u.enter.exit_block;
            exit_op = &(op_block->ops[enter_op->u.enter.exit_op_index]);

            if (op == exit_op) {
                enter_op->u.enter.exit_op_index = block->ops_len - 1;
            }
        }
        if (sky_code_opcode_isjump(op->opcode) &&
            !op->u.jump.target->analyzed)
        {
            sky_python_compiler_block_eliminate(op->u.jump.target, block);
        }
    }
}


static void
sky_python_compiler_code_dce(sky_python_compiler_code_t *   code,
                             sky_python_compiler_block_t *  block)
{
    sky_python_compiler_op_t    *last_op, *op;

    if (block->dce_visited) {
        return;
    }
    block->dce_visited = 1;
    sky_error_validate_debug(block->analyzed);

    if (block->ops_len) {
        last_op = &(block->ops[block->ops_len - 1]);
        for (op = block->ops; op <= last_op; ++op) {
            if (sky_code_opcode_isunconditional_jump(op->opcode)) {
                sky_python_compiler_block_truncate(block, op, last_op);
                sky_python_compiler_code_dce(code, op->u.jump.target);
                sky_python_compiler_block_eliminate(block->flow_next, block);
                block->flow_next = NULL;
                return;
            }
            if (sky_code_opcode_isexit(op->opcode)) {
                sky_python_compiler_block_truncate(block, op, last_op);
                sky_python_compiler_block_eliminate(block->flow_next, block);
                block->flow_next = NULL;
                return;
            }
            if (sky_code_opcode_isguard_enter(op->opcode)) {
                sky_python_compiler_block_truncate(block, op, last_op);
                sky_python_compiler_code_dce(code, op->u.enter.jump_block);
                if (op->u.enter.orelse_block) {
                    sky_python_compiler_code_dce(code,
                                                 op->u.enter.orelse_block);
                }
                break;
            }
            if (sky_code_opcode_isconditional_jump(op->opcode)) {
                sky_python_compiler_block_truncate(block, op, last_op);
                sky_python_compiler_code_dce(code, op->u.jump.target);
                break;
            }
        }
    }

    if (block->flow_next) {
        sky_python_compiler_code_dce(code, block->flow_next);
    }
}


static void
sky_python_compiler_code_destroy(sky_python_compiler_code_t *code)
{
    sky_python_compiler_block_t *block, *next_block;

    for (block = code->blocks; block; block = next_block) {
        next_block = block->alloc_next;
        sky_python_compiler_block_destroy(block);
    }

    sky_python_compiler_table_cleanup(&(code->cellvars));
    sky_python_compiler_table_cleanup(&(code->freevars));
    sky_python_compiler_table_cleanup(&(code->varnames));
    sky_python_compiler_table_cleanup(&(code->names));
    sky_python_compiler_table_cleanup(&(code->constants));
    sky_free(code->block_layout);
    sky_free(code);
}


static void
sky_python_compiler_code_pop(sky_python_compiler_t *compiler)
{
    sky_python_compiler_code_t  *code = compiler->code;

    compiler->code = code->parent;
    sky_python_compiler_code_destroy(code);
}


static sky_python_compiler_code_t *
sky_python_compiler_code_push(sky_python_compiler_t *   compiler,
                              sky_object_t              node,
                              sky_string_t              name)
{
    sky_python_compiler_code_t  *code;
    sky_python_symtable_block_t symtable_block;

    code = sky_calloc(1, sizeof(sky_python_compiler_code_t));
    code->name = name;
    sky_python_compiler_table_init(
            &(code->constants),
            sky_python_compiler_constant_table_item_compare);
    sky_python_compiler_table_init(
            &(code->names),
            sky_python_compiler_table_item_compare);
    sky_python_compiler_table_init(
            &(code->varnames),
            sky_python_compiler_table_item_compare);
    sky_python_compiler_table_init(
            &(code->freevars),
            sky_python_compiler_table_item_compare);
    sky_python_compiler_table_init(
            &(code->cellvars),
            sky_python_compiler_table_item_compare);

    symtable_block = sky_dict_getitem(compiler->symtable, node);
    code->symtable = sky_python_symtable_block_data(symtable_block);
    code->entry_block = sky_python_compiler_block_create(code);
    code->current_block = code->entry_block;

    code->firstlineno = code->symtable->lineno;
    if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION == code->symtable->type) {
        code->flags |= SKY_CODE_FLAG_NEWLOCALS;
        if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION == code->symtable->type) {
            if (SKY_PYTHON_SYMTABLE_OPTIMIZED_NONE ==
                code->symtable->optimized)
            {
                code->flags |= SKY_CODE_FLAG_OPTIMIZED;
            }
            if (code->symtable->nested) {
                code->flags |= SKY_CODE_FLAG_NESTED;
            }
        }
    }

    if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION == code->symtable->type ||
        SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS == code->symtable->type)
    {
        sky_python_compiler_table_load(
                &(code->cellvars),
                code->symtable->symbols,
                SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL,
                0);
        code->cellvars.readonly = SKY_TRUE;
        sky_python_compiler_table_load(
                &(code->freevars),
                code->symtable->symbols,
                SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE,
                SKY_PYTHON_SYMTABLE_SYMBOL_FLAG_FREE_CLASS);
        code->freevars.readonly = SKY_TRUE;
    }

    code->parent = compiler->code;
    compiler->code = code;

    return code;
}


static inline void
sky_python_compiler_code_push_const(sky_python_compiler_code_t *code,
                                    sky_object_t                object)
{
    sky_python_compiler_block_addop(
            code->current_block,
            SKY_CODE_OPCODE_PUSH_CONST,
            sky_python_compiler_table_get(&(code->constants), object));
}


static inline void
sky_python_compiler_code_push_None(sky_python_compiler_code_t *code)
{
    sky_python_compiler_block_addop(
            code->current_block,
            SKY_CODE_OPCODE_PUSH_CONST,
            sky_python_compiler_table_get(&(code->constants), sky_None));
}


static sky_string_t
sky_python_compiler_code_qualname(sky_python_compiler_code_t *code)
{
    ssize_t                     i, len, locals_len, npieces;
    sky_tuple_t                 tuple;
    sky_object_t                *pieces;
    sky_string_t                dot, locals;
    sky_string_builder_t        qualname;
    sky_python_compiler_code_t  *parent;

    npieces = 1;
    for (parent = code->parent; parent; parent = parent->parent) {
        switch (parent->symtable->type) {
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION:
                npieces += 2;
                break;
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS:
                ++npieces;
                break;
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE:
                break;
        }
    }
    if (1 == npieces) {
        return code->name;
    }

    dot = SKY_STRING_LITERAL(".");
    locals = SKY_STRING_LITERAL("<locals>");

    locals_len = sky_string_len(locals);
    len = (sky_object_len(dot) * npieces) + sky_string_len(code->name);

    i = npieces;
    pieces = sky_calloc(npieces, sizeof(sky_string_t));
    pieces[--i] = code->name;

    for (parent = code->parent; i > 0; parent = parent->parent) {
        switch (parent->symtable->type) {
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION:
                pieces[--i] = locals;
                pieces[--i] = parent->name;
                len += (sky_string_len(parent->name) + locals_len);
                break;
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_CLASS:
                pieces[--i] = parent->name;
                len += sky_string_len(parent->name);
                break;
            case SKY_PYTHON_SYMTABLE_BLOCK_TYPE_MODULE:
                break;
        }
    }

    tuple = sky_tuple_createwitharray(npieces, pieces, sky_free);
    qualname = sky_string_builder_createwithcapacity(len);
    sky_string_builder_appendjoined(qualname, tuple, dot);

    return sky_string_builder_finalize(qualname);
}


static void
sky_python_compiler_code_prune(sky_python_compiler_code_t *code)
{
    sky_python_compiler_op_t    *op;
    sky_python_compiler_block_t *block, *next_block;

    /* Update all jumps and flows that target an empty block with a flow.
     * An empty block is a block that has no ops. Update the origins with
     * the empty block's flow_next.
     */
    next_block = code->blocks;
    while ((block = next_block) != NULL) {
        if (block->flow_next &&
            !block->flow_next->ops_len &&
            block->flow_next->flow_next)
        {
            block->flow_next = block->flow_next->flow_next;
            continue;
        }

        if (block->ops_len) {
            op = &(block->ops[block->ops_len - 1]);
            if (sky_code_opcode_isjump(op->opcode)) {
                if (!op->u.jump.target->ops_len &&
                    op->u.jump.target->flow_next)
                {
                    op->u.jump.target = op->u.jump.target->flow_next;
                    continue;
                }
            }
            else if (sky_code_opcode_isguard_enter(op->opcode)) {
                if (!op->u.enter.jump_block->ops_len &&
                    op->u.enter.jump_block->flow_next)
                {
                    op->u.enter.jump_block = op->u.enter.jump_block->flow_next;
                    continue;
                }
                if (op->u.enter.orelse_block) {
                    if (!op->u.enter.orelse_block->ops_len &&
                        op->u.enter.orelse_block->flow_next)
                    {
                        op->u.enter.orelse_block =
                                op->u.enter.orelse_block->flow_next;
                    }
                }
            }
        }

        next_block = block->alloc_next;
    }

    while (!code->entry_block->ops_len &&
           code->entry_block->flow_next)
    {
        code->entry_block = code->entry_block->flow_next;
    }
}


static ssize_t
sky_python_compiler_code_layout(sky_python_compiler_code_t *    code,
                                ssize_t                         index,
                                sky_python_compiler_op_t *      entry_op)
{
    ssize_t                     guard_id, i, top;
    sky_python_compiler_op_t    *op;
    sky_python_compiler_block_t *block;

    if (!entry_op) {
        guard_id = 0;
    }
    else {
        sky_error_validate_debug(
                sky_code_opcode_isguard_enter(entry_op->opcode) ||
                sky_code_opcode_ishandler_enter(entry_op->opcode));
        guard_id = entry_op->u.enter.guard_id;
    }

    top = index - 1;
    while (index < code->nblocks) {
        block = code->block_layout[index - 1];
        sky_error_validate_debug(block->ops_len > 0);

        op = &(block->ops[block->ops_len - 1]);
        if (sky_code_opcode_isguard_enter(op->opcode)) {
            sky_error_validate_debug(NULL != block->flow_next);
            sky_error_validate_debug(-1 == block->flow_next->layout_index);
            code->block_layout[index] = block->flow_next;
            block->flow_next->layout_index = index++;
            index = sky_python_compiler_code_layout(code,
                                                    index,
                                                    op);

            sky_error_validate_debug(-1 == op->u.enter.jump_block->layout_index);
            code->block_layout[index] = op->u.enter.jump_block;
            op->u.enter.jump_block->layout_index = index++;
            if (SKY_CODE_OPCODE_WITH_ENTER != op->opcode) {
                index = sky_python_compiler_code_layout(
                                code,
                                index,
                                &(op->u.enter.jump_block->ops[0]));
            }
            continue;
        }

        /* If the last layout block has a flow pointer, that's the next block
         * in the layout, except if we're at the exit block. In that case,
         * finish all of the jumps, and then exit back out to our caller.
         */
        if (!(entry_op && entry_op->u.enter.exit_block == block) &&
            (block = block->flow_next) != NULL)
        {
            sky_error_validate_debug(-1 == block->layout_index);
        }
        else {
            int                         next_block_rank, rank;
            sky_python_compiler_block_t *next_block;

            /* Walk the layout backwards to find the next block to lay out.
             * Rank the blocks as follows (highest to lowest priority):
             * 1. guard orelse block (e.g., try...finally...else)
             * 2. unconditional jump immediately following try_exit (assumes
             *    that either an except or a finally handler block follows
             *    immediately and that the jump is just jumping around it)
             * 3. conditional jump
             * 4. unconditional jump
             */
            next_block = NULL;
            next_block_rank = INT_MAX;
            for (i = index - 1; i >= top; --i) {
                block = code->block_layout[i];
                op = &(block->ops[block->ops_len - 1]);
                if (sky_code_opcode_isguard_enter(op->opcode)) {
                    if (!(block = op->u.enter.orelse_block)) {
                        continue;
                    }
                    rank = 0;
                }
                else if (sky_code_opcode_isconditional_jump(op->opcode)) {
                    block = op->u.jump.target;
                    rank = 2;
                }
                else if (sky_code_opcode_isunconditional_jump(op->opcode)) {
                    if (block->ops_len >= 2 &&
                        SKY_CODE_OPCODE_TRY_EXIT == op[-1].opcode)
                    {
                        rank = 1;
                    }
                    else {
                        rank = 3;
                    }
                    block = op->u.jump.target;
                }
                else {
                    continue;
                }
                if (-1 == block->layout_index &&
                    guard_id == block->guard_id &&
                    (!next_block || rank < next_block_rank))
                {
                    next_block = block;
                    if (!(next_block_rank = rank)) {
                        break;
                    }
                }
            }

            if (!next_block) {
                /* If entry_op is NULL, it's an internal error, because it
                 * means that there's some rogue blocks hanging around without
                 * any connection to anything.
                 */
                sky_error_validate_debug(entry_op != NULL);
                return index;
            }
            block = next_block;
        }

        code->block_layout[index] = block;
        block->layout_index = index++;
    }

#if !defined(NDEBUG)
    if (!entry_op) {
        block = code->block_layout[code->nblocks - 1];
        sky_error_validate_debug(NULL == block->flow_next);
    }
#endif

    return index;
}


#if !defined(NDEBUG)
void
sky_python_compiler_block_dump(sky_python_compiler_block_t *block,
                               sky_python_compiler_code_t * code,
                               int *                        prev_lineno)
{
    int                         prev_lineno_stub = INT_MIN;
    ssize_t                     j;
    sky_tuple_t                 cellvars, constants, freevars, names, varnames;
    sky_python_compiler_op_t    *op;

    if (!code->t_constants) {
        code->t_constants = sky_python_compiler_table_tuple(&(code->constants));
        code->t_names = sky_python_compiler_table_tuple(&(code->names));
        code->t_varnames = sky_python_compiler_table_tuple(&(code->varnames));
        code->t_freevars = sky_python_compiler_table_tuple(&(code->freevars));
        code->t_cellvars = sky_python_compiler_table_tuple(&(code->cellvars));
    }
    constants = code->t_constants;
    names = code->t_names;
    varnames = code->t_varnames;
    freevars = code->t_freevars;
    cellvars = code->t_cellvars;

    if (!prev_lineno) {
        prev_lineno = &prev_lineno_stub;
    }
    sky_format_fprintf(stdout,
                       "    %p [%zd] {{ %zd, %zd, %zd / %zd }} (size %zd)",
                       block,
                       block->layout_index,
                       block->stack_entry,
                       block->stack_delta,
                       block->stack_nadir,
                       block->guard_id,
                       block->bytecode_size);
    if (block->flow_next) {
        sky_format_fprintf(stdout, " -> %p [%zd]",
                           block->flow_next,
                           block->flow_next->layout_index);
    }
    /* Don't print unreachable if reachability hasn't been determined yet */
    if (!block->reachable && code->entry_block->reachable) {
        sky_format_fprintf(stdout, " (unreachable)");
    }
    sky_format_fprintf(stdout, "\n");
    for (j = 0; j < block->ops_len; ++j) {
        op = &(block->ops[j]);
        if (op->lineno != *prev_lineno) {
            sky_format_fprintf(stdout, "    %4d    %-20s    ",
                               op->lineno,
                               sky_code_opcodes[op->opcode].name);
            *prev_lineno = op->lineno;
        }
        else {
            sky_format_fprintf(stdout, "            %-20s    ",
                               sky_code_opcodes[op->opcode].name);
        }
        switch (op->opcode) {
            case SKY_CODE_OPCODE_PUSH_CONST:
                if (!constants) {
                    sky_format_fprintf(stdout, "%zu", op->u.argv[0]);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%zu [%#@]",
                                       op->u.argv[0],
                                       sky_tuple_get(constants, op->u.argv[0]));
                }
                break;
            case SKY_CODE_OPCODE_PUSH_LOCAL:
            case SKY_CODE_OPCODE_POP_LOCAL:
            case SKY_CODE_OPCODE_DELETE_LOCAL:
                if (!varnames) {
                    sky_format_fprintf(stdout, "%zu", op->u.argv[0]);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%zu [%@]",
                                       op->u.argv[0],
                                       sky_tuple_get(varnames, op->u.argv[0]));
                }
                break;
            case SKY_CODE_OPCODE_PUSH_GLOBAL:
            case SKY_CODE_OPCODE_POP_GLOBAL:
            case SKY_CODE_OPCODE_DELETE_GLOBAL:
            case SKY_CODE_OPCODE_PUSH_NAME:
            case SKY_CODE_OPCODE_POP_NAME:
            case SKY_CODE_OPCODE_DELETE_NAME:
            case SKY_CODE_OPCODE_PUSH_ATTRIBUTE:
            case SKY_CODE_OPCODE_POP_ATTRIBUTE:
            case SKY_CODE_OPCODE_DELETE_ATTRIBUTE:
            case SKY_CODE_OPCODE_IMPORT_NAME:
                if (!names) {
                    sky_format_fprintf(stdout, "%zu", op->u.argv[0]);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%zu [%@]",
                                       op->u.argv[0],
                                       sky_tuple_get(names, op->u.argv[0]));
                }
                break;
            case SKY_CODE_OPCODE_PUSH_FREE:
            case SKY_CODE_OPCODE_POP_FREE:
            case SKY_CODE_OPCODE_DELETE_FREE:
                if (!freevars) {
                    sky_format_fprintf(stdout, "%zu", op->u.argv[0]);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%zu [%@]",
                                       op->u.argv[0],
                                       sky_tuple_get(freevars, op->u.argv[0]));
                }
                break;
            case SKY_CODE_OPCODE_PUSH_CELL:
            case SKY_CODE_OPCODE_POP_CELL:
            case SKY_CODE_OPCODE_DELETE_CELL:
                if (!cellvars) {
                    sky_format_fprintf(stdout, "%zu", op->u.argv[0]);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%zu [%@]",
                                       op->u.argv[0],
                                       sky_tuple_get(cellvars, op->u.argv[0]));
                }
                break;

            case SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT:
            case SKY_CODE_OPCODE_JUMP:
            case SKY_CODE_OPCODE_JUMP_IF_FALSE:
            case SKY_CODE_OPCODE_JUMP_IF_TRUE:
                sky_format_fprintf(stdout,
                                   "%p [%zd]",
                                   op->u.jump.target,
                                   op->u.jump.target->layout_index);
                break;

            case SKY_CODE_OPCODE_COMPARE:
                switch ((sky_compare_op_t)op->u.argv[0]) {
                    case SKY_COMPARE_OP_NOT_EQUAL:
                        sky_format_fprintf(stdout, "!=");
                        break;
                    case SKY_COMPARE_OP_EQUAL:
                        sky_format_fprintf(stdout, "==");
                        break;
                    case SKY_COMPARE_OP_GREATER:
                        sky_format_fprintf(stdout, ">");
                        break;
                    case SKY_COMPARE_OP_GREATER_EQUAL:
                        sky_format_fprintf(stdout, ">=");
                        break;
                    case SKY_COMPARE_OP_LESS:
                        sky_format_fprintf(stdout, ">");
                        break;
                    case SKY_COMPARE_OP_LESS_EQUAL:
                        sky_format_fprintf(stdout, ">=");
                        break;
                    case SKY_COMPARE_OP_IS:
                        sky_format_fprintf(stdout, "is");
                        break;
                    case SKY_COMPARE_OP_IS_NOT:
                        sky_format_fprintf(stdout, "is not");
                        break;
                    case SKY_COMPARE_OP_IN:
                        sky_format_fprintf(stdout, "in");
                        break;
                    case SKY_COMPARE_OP_NOT_IN:
                        sky_format_fprintf(stdout, "not in");
                        break;
                }
                break;

            case SKY_CODE_OPCODE_UNPACK_SEQUENCE:
            case SKY_CODE_OPCODE_BUILD_DICT:
            case SKY_CODE_OPCODE_BUILD_LIST:
            case SKY_CODE_OPCODE_BUILD_SET:
            case SKY_CODE_OPCODE_BUILD_TUPLE:
            case SKY_CODE_OPCODE_RAISE:
                sky_format_fprintf(stdout,
                                   "%zu",
                                   op->u.argv[0]);
                break;

            case SKY_CODE_OPCODE_UNPACK_STARRED:
                sky_format_fprintf(stdout,
                                   "%zu before, %zu after",
                                   op->u.argv[0],
                                   op->u.argv[1]);
                break;

            case SKY_CODE_OPCODE_BUILD_CLASS:
                sky_format_fprintf(stdout,
                                   "%zx, %zu bases, %zu keywords",
                                   op->u.argv[0],
                                   op->u.argv[1],
                                   op->u.argv[2]);
                break;

            case SKY_CODE_OPCODE_BUILD_FUNCTION:
                sky_format_fprintf(stdout,
                                   "%zx, %zu defaults, %zu kwdefaults",
                                   op->u.argv[0],
                                   op->u.argv[1],
                                   op->u.argv[2]);
                break;

            case SKY_CODE_OPCODE_CALL:
            case SKY_CODE_OPCODE_CALL_ARGS:
            case SKY_CODE_OPCODE_CALL_KWS:
            case SKY_CODE_OPCODE_CALL_ARGS_KWS:
                sky_format_fprintf(stdout,
                                   "%zu positional, %zu keyword",
                                   op->u.argv[0],
                                   op->u.argv[1]);
                break;

            case SKY_CODE_OPCODE_WITH_ENTER:
            case SKY_CODE_OPCODE_TRY_EXCEPT:
            case SKY_CODE_OPCODE_TRY_FINALLY:
                if (!op->u.enter.orelse_block) {
                    sky_format_fprintf(stdout,
                                       "%p [%zd], exit: %p [%zd]<%zd>",
                                       op->u.enter.jump_block,
                                       op->u.enter.jump_block->layout_index,
                                       op->u.enter.exit_block,
                                       op->u.enter.exit_block->layout_index,
                                       op->u.enter.exit_op_index);
                }
                else {
                    sky_format_fprintf(stdout,
                                       "%p [%zd], exit: %p [%zd]<%zd>, orelse: %p [%zd]",
                                       op->u.enter.jump_block,
                                       op->u.enter.jump_block->layout_index,
                                       op->u.enter.exit_block,
                                       op->u.enter.exit_block->layout_index,
                                       op->u.enter.exit_op_index,
                                       op->u.enter.orelse_block,
                                       op->u.enter.orelse_block->layout_index);
                }
                break;

            case SKY_CODE_OPCODE_EXCEPT_ENTER:
            case SKY_CODE_OPCODE_FINALLY_ENTER:
                sky_format_fprintf(stdout,
                                   "exit: %p [%zd]<%zd>",
                                   op->u.enter.exit_block,
                                   op->u.enter.exit_block->layout_index,
                                   op->u.enter.exit_op_index);
                break;

            default:
                break;
        }
        sky_format_fprintf(stdout, "\n");
    }
}

void
sky_python_compiler_code_dump_header(sky_python_compiler_code_t *code)
{
    sky_object_t    flags;

    if (!code->t_constants) {
        code->t_constants = sky_python_compiler_table_tuple(&(code->constants));
        code->t_names = sky_python_compiler_table_tuple(&(code->names));
        code->t_varnames = sky_python_compiler_table_tuple(&(code->varnames));
        code->t_freevars = sky_python_compiler_table_tuple(&(code->freevars));
        code->t_cellvars = sky_python_compiler_table_tuple(&(code->cellvars));
    }

    flags = sky_list_create(NULL);
    if (code->flags & SKY_CODE_FLAG_OPTIMIZED) {
        sky_list_append(flags, SKY_STRING_LITERAL("optimized"));
    }
    if (code->flags & SKY_CODE_FLAG_NEWLOCALS) {
        sky_list_append(flags, SKY_STRING_LITERAL("newlocals"));
    }
    if (code->flags & SKY_CODE_FLAG_VARARGS) {
        sky_list_append(flags, SKY_STRING_LITERAL("varargs"));
    }
    if (code->flags & SKY_CODE_FLAG_VARKEYWORDS) {
        sky_list_append(flags, SKY_STRING_LITERAL("varkeywords"));
    }
    if (code->flags & SKY_CODE_FLAG_NESTED) {
        sky_list_append(flags, SKY_STRING_LITERAL("nested"));
    }
    if (code->flags & SKY_CODE_FLAG_GENERATOR) {
        sky_list_append(flags, SKY_STRING_LITERAL("generator"));
    }
    if (code->flags & SKY_CODE_FLAG_NOFREE) {
        sky_list_append(flags, SKY_STRING_LITERAL("nofree"));
    }
    flags = sky_string_join(SKY_STRING_LITERAL(", "), flags);

    sky_format_fprintf(stdout, "%@:\n", code->name);
    sky_format_fprintf(stdout, "    argcount:       %zd\n", code->argcount);
    sky_format_fprintf(stdout, "    kwonlyargcount: %zd\n", code->kwonlyargcount);
    sky_format_fprintf(stdout, "    nlocals:        %zd\n", code->nlocals);
    sky_format_fprintf(stdout, "    stacksize:      %zd\n", code->stacksize);
    sky_format_fprintf(stdout, "    flags:          %@\n", flags);
    sky_format_fprintf(stdout, "    firstlineno:    %d\n", code->firstlineno);
    sky_format_fprintf(stdout, "    nblocks:        %zd\n", code->nblocks);
    sky_format_fprintf(stdout, "    bytecode_size:  %zd\n", code->bytecode_size);
    sky_format_fprintf(stdout, "    lnotab_size:    %zd\n", code->lnotab_size);
    sky_format_fprintf(stdout, "\n");

    sky_format_fprintf(stdout, "    constants: %@\n", code->t_constants);
    sky_format_fprintf(stdout, "    names:     %@\n", code->t_names);
    sky_format_fprintf(stdout, "    varnames:  %@\n", code->t_varnames);
    sky_format_fprintf(stdout, "    freevars:  %@\n", code->t_freevars);
    sky_format_fprintf(stdout, "    cellvars:  %@\n", code->t_cellvars);
    sky_format_fprintf(stdout, "\n");
}

void
sky_python_compiler_code_dump_blocks(sky_python_compiler_code_t *code)
{
    int                         prev_lineno;
    ssize_t                     i;
    sky_python_compiler_block_t *block;

    prev_lineno = INT_MIN;
    for (i = 0; i < code->nblocks; ++i) {
        block = code->block_layout[i];
        sky_python_compiler_block_dump(block, code, &prev_lineno);
    }
}

void
sky_python_compiler_code_dump(sky_python_compiler_code_t *code)
{
    sky_python_compiler_code_dump_header(code);
    sky_python_compiler_code_dump_blocks(code);
}
#endif


static void
sky_python_compiler_op_patch(sky_python_compiler_op_t * op,
                             SKY_UNUSED ssize_t         bytecode_size)
{
    sky_python_compiler_op_t    *exit_op, *jump_op;

    if (sky_code_opcode_isguard_enter(op->opcode)) {
        jump_op = &(op->u.enter.jump_block->ops[0]);
        sky_error_validate_debug(jump_op->offset >= 0);
        sky_error_validate_debug(jump_op->offset < bytecode_size);
        op->u.enter.jump_offset = jump_op->offset;
        exit_op = &(op->u.enter.exit_block->ops[op->u.enter.exit_op_index]);
        sky_error_validate_debug(exit_op->offset >= 0);
        sky_error_validate_debug(exit_op->offset < bytecode_size);
        op->u.enter.exit_offset = exit_op->offset;
    }
    else if (sky_code_opcode_ishandler_enter(op->opcode)) {
        exit_op = &(op->u.enter.exit_block->ops[op->u.enter.exit_op_index]);
        sky_error_validate_debug(exit_op->offset >= 0);
        sky_error_validate_debug(exit_op->offset < bytecode_size);
        op->u.enter.exit_offset = exit_op->offset;
    }
    else if (sky_code_opcode_isjump(op->opcode)) {
        jump_op = &(op->u.jump.target->ops[0]);
        sky_error_validate_debug(jump_op->offset >= 0);
        sky_error_validate_debug(jump_op->offset < bytecode_size);
        op->u.jump.offset = jump_op->offset;
    }
}


static void
sky_python_compiler_code_compute_jumps(sky_python_compiler_code_t *code)
{
    int                         lineno, lineno_delta;
    ssize_t                     address, address_delta, i, nbytes;
    sky_bool_t                  quick_patch;
    sky_python_compiler_op_t    *last_op, *op;
    sky_python_compiler_block_t *block;

    address = 0;
    lineno = code->firstlineno;
    quick_patch = SKY_TRUE;
    for (i = 0; i < code->nblocks; ++i) {
        block = code->block_layout[i];
        if (!block->reachable) {
            continue;
        }
        sky_error_validate_debug(block->ops_len > 0);
        sky_error_validate_debug(-1 == block->bytecode_size);
        block->bytecode_size = 0;

        last_op = &(block->ops[block->ops_len - 1]);
        for (op = &(block->ops[0]); op <= last_op; ++op) {
            op->offset = code->bytecode_size;

            if (op->lineno > lineno) {
                address_delta = op->offset - address;
                lineno_delta = op->lineno - lineno;
                do {
                    code->lnotab_size += 2;
                    address_delta -= 255;
                    lineno_delta -= 255;
                } while (address_delta > 0 || lineno_delta > 0);
                address = op->offset;
                lineno = op->lineno;
            }

            nbytes = 1;
            if (sky_code_opcode_isguard_enter(op->opcode)) {
                nbytes += 4;
            }
            else if (sky_code_opcode_ishandler_enter(op->opcode)) {
                if (op != last_op) {
                    quick_patch = SKY_FALSE;
                }
                nbytes += 2;
            }
            else if (sky_code_opcode_isjump(op->opcode)) {
                nbytes += 2;
            }
            else if (sky_code_opcodes[op->opcode].argc > 0) {
                switch (sky_code_opcodes[op->opcode].argc) {
                    default:
                        sky_error_fatal("internal error");
                    case 3:
                        nbytes += sky_util_varint_size(op->u.argv[2]);
                    case 2:
                        nbytes += sky_util_varint_size(op->u.argv[1]);
                    case 1:
                        nbytes += sky_util_varint_size(op->u.argv[0]);
                }
            }
            block->bytecode_size += nbytes;
            code->bytecode_size += nbytes;
        }
    }

    /* A 64K bytecode size limit seems pretty ridiculous, but so is requiring
     * four bytes per jump when a "big" function is generally about 2K in size.
     * Pretty much the only instances in which one would reasonably run into
     * this limit is with some giant literal construction (dict, list, set, or
     * tuple). If bigger code size is needed, we can introduce an instruction
     * prefix or something.
     */
    if (code->bytecode_size > 0x10000) {
        sky_error_raise_string(sky_OverflowError, "bytecode too big");
    }

    if (quick_patch) {
        for (i = 0; i < code->nblocks; ++i) {
            block = code->block_layout[i];
            if (!block->reachable) {
                continue;
            }
            last_op = &(block->ops[block->ops_len - 1]);
            sky_python_compiler_op_patch(last_op, code->bytecode_size);
        }
    }
    else {
        for (i = 0; i < code->nblocks; ++i) {
            block = code->block_layout[i];
            if (!block->reachable) {
                continue;
            }
            last_op = &(block->ops[block->ops_len - 1]);
            for (op = &(block->ops[0]); op <= last_op; ++op) {
                sky_python_compiler_op_patch(op, code->bytecode_size);
            }
        }
    }
}


static void
sky_python_compiler_code_rewrite_jumps(sky_python_compiler_code_t *code)
{
    ssize_t                     i;
    sky_python_compiler_op_t    *op, *target_op;
    sky_python_compiler_block_t *block, *target_block;

    for (i = 0; i < code->nblocks; ++i) {
        block = code->block_layout[i];
        op = &(block->ops[block->ops_len - 1]);

        if (sky_code_opcode_isconditional_jump(op->opcode)) {
            target_block = op->u.jump.target;
            target_op = &(target_block->ops[0]);
            if (sky_code_opcode_isunconditional_jump(target_op->opcode)) {
                op->u.jump.target = target_op->u.jump.target;
            }
        }
        else if (sky_code_opcode_isunconditional_jump(op->opcode)) {
            target_block = op->u.jump.target;
            target_op = &(target_block->ops[0]);
            if (sky_code_opcode_isunconditional_jump(target_op->opcode)) {
                op->u.jump.target = target_op->u.jump.target;
            }
            else if (sky_code_opcode_isexit(target_op->opcode)) {
                *op = *target_op;
            }
        }
    }
}


static void
sky_python_compiler_code_finalize(sky_python_compiler_code_t *code)
{
    int                         argc, lineno, lineno_delta;
    size_t                      address, address_delta;
    ssize_t                     i;
    uint8_t                     *bc, *bytecode_bytes, *bytecode_end,
                                *lb, *lnotab_bytes;
    sky_python_compiler_op_t    *last_op, *op;
    sky_python_compiler_block_t *block;

    if (!code->bytecode_size) {
        sky_error_validate_debug(!code->lnotab_size);
        code->bytecode = sky_bytes_empty;
        code->lnotab = sky_bytes_empty;
        return;
    }
    bc = bytecode_bytes = sky_malloc(code->bytecode_size);
    bytecode_end = bytecode_bytes + code->bytecode_size;

    if (!code->lnotab_size) {
        code->lnotab = sky_bytes_empty;
    }
    else {
        lb = lnotab_bytes = sky_malloc(code->lnotab_size);
    }

    address = 0;
    lineno = code->firstlineno;
    for (i = 0; i < code->nblocks; ++i) {
        block = code->block_layout[i];
        if (!block->reachable) {
            continue;
        }
        last_op = &(block->ops[block->ops_len - 1]);
        for (op = &(block->ops[0]); op <= last_op; ++op) {
            if (op->lineno > lineno) {
                address_delta = (bc - bytecode_bytes) - address;
                lineno_delta = op->lineno - lineno;
                while (address_delta > 255 || lineno_delta > 255) {
                    if (address_delta <= 255) {
                        *lb++ = 0;
                    }
                    else {
                        *lb++ = 255;
                        address_delta -= 255;
                    }
                    if (lineno_delta <= 255) {
                        *lb++ = 0;
                    }
                    else {
                        *lb++ = 255;
                        lineno_delta -= 255;
                    }
                }
                *lb++ = address_delta;
                *lb++ = lineno_delta;
                address = bc - bytecode_bytes;
                lineno = op->lineno;
            }

            *bc++ = (uint8_t)op->opcode;
            if (sky_code_opcode_isguard_enter(op->opcode)) {
                *bc++ = (op->u.enter.jump_offset >> 8) & 0xFF;
                *bc++ = (op->u.enter.jump_offset     ) & 0xFF;
                *bc++ = (op->u.enter.exit_offset >> 8) & 0xFF;
                *bc++ = (op->u.enter.exit_offset     ) & 0xFF;
            }
            else if (sky_code_opcode_ishandler_enter(op->opcode)) {
                *bc++ = (op->u.enter.exit_offset >> 8) & 0xFF;
                *bc++ = (op->u.enter.exit_offset     ) & 0xFF;
            }
            else if (sky_code_opcode_isjump(op->opcode)) {
                *bc++ = (op->u.jump.offset >> 8) & 0xFF;
                *bc++ = (op->u.jump.offset     ) & 0xFF;
            }
            else if (sky_code_opcodes[op->opcode].argc > 0) {
                argc = 0;
                switch (sky_code_opcodes[op->opcode].argc) {
                    default:
                        sky_error_fatal("internal error");
                    case 3:
                        bc = sky_util_varint_encode(op->u.argv[argc++],
                                                    bc,
                                                    bytecode_end - bc);
                        sky_error_validate_debug(bc != NULL);
                    case 2:
                        bc = sky_util_varint_encode(op->u.argv[argc++],
                                                    bc,
                                                    bytecode_end - bc);
                        sky_error_validate_debug(bc != NULL);
                    case 1:
                        bc = sky_util_varint_encode(op->u.argv[argc++],
                                                    bc,
                                                    bytecode_end - bc);
                        sky_error_validate_debug(bc != NULL);
                }
            }
        }
    }

    sky_error_validate_debug(bc == bytecode_bytes + code->bytecode_size);
    code->bytecode = sky_bytes_createwithbytes(bytecode_bytes,
                                               code->bytecode_size,
                                               sky_free);

    if (code->lnotab_size) {
        sky_error_validate_debug(lb == lnotab_bytes + code->lnotab_size);
        code->lnotab = sky_bytes_createwithbytes(lnotab_bytes,
                                                 code->lnotab_size,
                                                 sky_free);
    }
}


static sky_code_t
sky_python_compiler_code_assemble(sky_python_compiler_t *compiler)
{
    sky_python_compiler_code_t      *code = compiler->code;

    sky_code_t  code_object;

    /* This will always happen with the module-level code.
     * The entry block is always a leader, so there's always at least one op
     * unless there's no code present at all. For example, "lambda: pass"
     * would result in empty code, as would a proper function definition
     * containing only a single pass statement. Basically, it's enough to just
     * check the entry block for an op.
     */
    if (!code->firstlineno) {
        if (code->entry_block->ops_len) {
            code->firstlineno = code->entry_block->ops[0].lineno;
        }
        if (!code->firstlineno) {
            code->firstlineno = 1;
        }
    }

    code->nlocals = sky_hashtable_length(&(code->varnames.base));

    if (!sky_hashtable_length(&(code->freevars.base)) &&
        !sky_hashtable_length(&(code->cellvars.base)))
    {
        code->flags |= SKY_CODE_FLAG_NOFREE;
    }

    /* Compute the stack size for each block and add 'return None' anywhere
     * necessary.
     */
    sky_python_compiler_block_analyze(code->entry_block, code, 0);

    /* Eliminate dead code. */
    sky_python_compiler_code_dce(code, code->entry_block);

    /* Prune empty blocks. */
    sky_python_compiler_code_prune(code);

    /* Count the number of active blocks. */
    code->nblocks = sky_python_compiler_block_count(code->entry_block, 0);

    /* Layout blocks in the proper order. */
    code->block_layout = sky_calloc(code->nblocks,
                                    sizeof(sky_python_compiler_block_t));
    code->block_layout[0] = code->entry_block;
    code->entry_block->layout_index = 0;
    sky_python_compiler_code_layout(code, 1, NULL);

    /* Rewrite jumps (jump to jump, jump to exit, etc.) */
    sky_python_compiler_code_rewrite_jumps(code);

    /* Mark reachable blocks (they'll be skipped hereafter) */
    sky_python_compiler_block_reachable(code->entry_block);

    /* Compute block sizes and jump offsets. */
    sky_python_compiler_code_compute_jumps(code);

    /* Build codestring and lnotab. */
    sky_python_compiler_code_finalize(code);

    code_object =
            sky_code_create(code->argcount,
                            code->kwonlyargcount,
                            code->nlocals,
                            code->stacksize,
                            code->flags,
                            code->bytecode,
                            sky_python_compiler_table_tuple(&(code->constants)),
                            sky_python_compiler_table_tuple(&(code->names)),
                            sky_python_compiler_table_tuple(&(code->varnames)),
                            compiler->filename,
                            code->name,
                            code->firstlineno,
                            code->lnotab,
                            sky_python_compiler_table_tuple(&(code->freevars)),
                            sky_python_compiler_table_tuple(&(code->cellvars)));

    return code_object;
}


static sky_object_t
sky_python_compiler_constant(sky_python_compiler_t *compiler,
                             sky_object_t           node);


static sky_object_t
sky_python_compiler_constant_name(sky_python_compiler_t *   compiler,
                                  sky_string_t              string)
{
    const char  *name = sky_string_cstring(string);

    if (name) {
        if (!strcmp(name, "None")) {
            return sky_None;
        }
        if (!strcmp(name, "True")) {
            return sky_True;
        }
        if (!strcmp(name, "False")) {
            return sky_False;
        }
        if (!strcmp(name, "__debug__")) {
            return (compiler->optlevel > 0 ? sky_False : sky_True);
        }
    }

    return NULL;
}


static sky_tuple_t
sky_python_compiler_constant_tuple(sky_python_compiler_t *  compiler,
                                   sky_object_t             sequence)
{
    sky_bool_t  is_constant = SKY_TRUE;

    ssize_t         i, len;
    sky_tuple_t     tuple;
    sky_object_t    elt, *objects;

    if (!(len = sky_object_len(sequence))) {
        return sky_tuple_empty;
    }

    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_malloc(len * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);
        for (i = 0; i < len; ++i) {
            elt = sky_sequence_get(sequence, i);
            if (!(objects[i] = sky_python_compiler_constant(compiler, elt))) {
                is_constant = SKY_FALSE;
                break;
            }
        }
        tuple = (is_constant ? sky_tuple_createwitharray(len, objects, sky_free)
                             : NULL);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}


static sky_object_t
sky_python_compiler_constant(sky_python_compiler_t *compiler,
                             sky_object_t           node)
    /* This function assumes that node is constant. Nothing especially bad will
     * happen if not, but extra work may be done that gets abandoned if a
     * non-constant node is found. All of this would be easier and better if
     * there were a flag in the AST for whether a node is constant ...
     */
{
    if (sky_object_isnull(node)) {
        return sky_None;
    }
    if (sky_object_isa(node, sky_python_ast_Num_type)) {
        return ((sky_python_ast_Num_t)node)->n;
    }
    if (sky_object_isa(node, sky_python_ast_Str_type)) {
        return ((sky_python_ast_Str_t)node)->s;
    }
    if (sky_object_isa(node, sky_python_ast_Bytes_type)) {
        return ((sky_python_ast_Bytes_t)node)->s;
    }
    if (sky_object_isa(node, sky_python_ast_Ellipsis_type)) {
        return sky_Ellipsis;
    }
    if (sky_object_isa(node, sky_python_ast_Name_type)) {
        return sky_python_compiler_constant_name(
                        compiler,
                        ((sky_python_ast_Name_t)node)->id);
    }
    if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        return sky_python_compiler_constant_tuple(
                        compiler,
                        ((sky_python_ast_Tuple_t)node)->elts);
    }
#if defined(SKY_PYTHON_COMPILER_CONSTANT_SLICES)
    if (sky_object_isa(node, sky_python_ast_Slice_type)) {
        sky_python_ast_Slice_t  Slice = (sky_python_ast_Slice_t)node;

        sky_object_t    lower, step, upper;

        if (!(lower = sky_python_compiler_constant(compiler, Slice->lower)) ||
            !(upper = sky_python_compiler_constant(compiler, Slice->upper)) ||
            !(step = sky_python_compiler_constant(compiler, Slice->step)))
        {
            return NULL;
        }
        return sky_slice_create(lower, upper, step);
    }
#endif
    if (sky_object_isa(node, sky_python_ast_ExtSlice_type)) {
        return sky_python_compiler_constant_tuple(
                        compiler,
                        ((sky_python_ast_ExtSlice_t)node)->dims);
    }
    if (sky_object_isa(node, sky_python_ast_Index_type)) {
        return sky_python_compiler_constant(
                        compiler,
                        ((sky_python_ast_Index_t)node)->value);
    }
    return NULL;
}


static sky_bool_t
sky_python_compiler_is_constant(sky_object_t node)
{
    if (sky_object_isnull(node)) {
        return SKY_TRUE;
    }
    if (sky_object_isa(node, sky_python_ast_Num_type) ||
        sky_object_isa(node, sky_python_ast_Str_type) ||
        sky_object_isa(node, sky_python_ast_Bytes_type) ||
        sky_object_isa(node, sky_python_ast_Ellipsis_type))
    {
        return SKY_TRUE;
    }
    if (sky_object_isa(node, sky_python_ast_Name_type)) {
        const char  *name;
        
        name = sky_string_cstring(((sky_python_ast_Name_t)node)->id);
        if (name && (!strcmp(name, "None") ||
                     !strcmp(name, "True") ||
                     !strcmp(name, "False") ||
                     !strcmp(name, "__debug__")))
        {
            return SKY_TRUE;
        }
        return SKY_FALSE;
    }
    if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        sky_bool_t      is_constant = SKY_TRUE;
        sky_object_t    object, sequence;

        sequence = ((sky_python_ast_Tuple_t)node)->elts;
        SKY_SEQUENCE_FOREACH(sequence, object) {
            if (!sky_python_compiler_is_constant(object)) {
                is_constant = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;

        return is_constant;
    }
#if defined(SKY_PYTHON_COMPILER_CONSTANT_SLICES)
    if (sky_object_isa(node, sky_python_ast_Slice_type)) {
        sky_python_ast_Slice_t  slice = (sky_python_ast_Slice_t)node;

        return (sky_python_compiler_is_constant(slice->lower) &&
                sky_python_compiler_is_constant(slice->upper) &&
                sky_python_compiler_is_constant(slice->step));
    }
#endif
    if (sky_object_isa(node, sky_python_ast_ExtSlice_type)) {
        sky_bool_t      is_constant = SKY_TRUE;
        sky_object_t    object, sequence;

        sequence = ((sky_python_ast_ExtSlice_t)node)->dims;
        SKY_SEQUENCE_FOREACH(sequence, object) {
            if (!sky_python_compiler_is_constant(object)) {
                is_constant = SKY_FALSE;
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;

        return is_constant;
    }
    if (sky_object_isa(node, sky_python_ast_Index_type)) {
        sky_object_t    value = ((sky_python_ast_Index_t)node)->value;

        return sky_python_compiler_is_constant(value);
    }

    return SKY_FALSE;
}


static sky_bool_t
sky_python_compiler_is_emptybody(sky_object_t suite)
{
    sky_bool_t      result;
    sky_object_t    stmt;

    result = SKY_TRUE;
    SKY_SEQUENCE_FOREACH(suite, stmt) {
        if (!sky_object_isa(stmt, sky_python_ast_Pass_type)) {
            result = SKY_FALSE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return result;
}


static void
sky_python_compiler_visit_expr(sky_python_compiler_t *  compiler,
                               sky_object_t             node);


static void
sky_python_compiler_visit_expr_sequence(sky_python_compiler_t * compiler,
                                        sky_object_t            sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_compiler_visit_expr(compiler, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_compiler_visit_stmt(sky_python_compiler_t *  compiler,
                               sky_object_t             node);


static void
sky_python_compiler_visit_stmt_sequence(sky_python_compiler_t * compiler,
                                        sky_object_t            sequence)
{
    sky_object_t    element;

    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_compiler_visit_stmt(compiler, element);
    } SKY_SEQUENCE_FOREACH_END;
}


static sky_bool_t
sky_python_compiler_build_annotations(sky_python_compiler_t *       compiler,
                                      sky_python_ast_arguments_t    args,
                                      sky_object_t                  returns)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                 count = 0;
    sky_python_ast_arg_t    arg;

    if (!sky_object_isnull(args->args)) {
        SKY_SEQUENCE_FOREACH(args->args, arg) {
            if (!sky_object_isnull(arg->annotation)) {
                ++count;
                sky_python_compiler_code_push_const(code, arg->arg);
                sky_python_compiler_visit_expr(compiler, arg->annotation);
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    if (!sky_object_isnull(args->vararg) &&
        !sky_object_isnull(args->varargannotation))
    {
        ++count;
        sky_python_compiler_code_push_const(code, args->vararg);
        sky_python_compiler_visit_expr(compiler, args->varargannotation);
    }
    if (!sky_object_isnull(args->kwonlyargs)) {
        SKY_SEQUENCE_FOREACH(args->kwonlyargs, arg) {
            if (!sky_object_isnull(arg->annotation)) {
                ++count;
                sky_python_compiler_code_push_const(code, arg->arg);
                sky_python_compiler_visit_expr(compiler, arg->annotation);
            }
        } SKY_SEQUENCE_FOREACH_END;
    }
    if (!sky_object_isnull(args->kwarg) &&
        !sky_object_isnull(args->kwargannotation))
    {
        ++count;
        sky_python_compiler_code_push_const(code, args->kwarg);
        sky_python_compiler_visit_expr(compiler, args->kwargannotation);
    }
    if (!sky_object_isnull(returns)) {
        ++count;
        sky_python_compiler_code_push_const(code,
                                            SKY_STRING_LITERAL("return"));
        sky_python_compiler_visit_expr(compiler, returns);
    }

    if (!count) {
        return SKY_FALSE;
    }

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_DICT,
                                    count);
    return SKY_TRUE;
}


static sky_bool_t
sky_python_compiler_build_closure(sky_python_compiler_t *   compiler,
                                  sky_code_t                code)
{
    sky_code_data_t *code_data = sky_code_data(code);

    ssize_t             index, nfreevars;
    sky_string_t        name;
    sky_code_opcode_t   opcode;

    if (!(nfreevars = sky_object_len(code_data->co_freevars))) {
        return SKY_FALSE;
    }

    SKY_SEQUENCE_FOREACH(code_data->co_freevars, name) {
        opcode = SKY_CODE_OPCODE_PUSH_CLOSURE_CELL;
        index = sky_python_compiler_table_get(&(compiler->code->cellvars),
                                              name);
        if (-1 == index) {
            index = sky_python_compiler_table_get(
                            &(compiler->code->freevars),
                            name);
            sky_error_validate_debug(-1 != index);
            opcode = SKY_CODE_OPCODE_PUSH_CLOSURE_FREE;
        }
        sky_python_compiler_block_addop(compiler->code->current_block,
                                        opcode,
                                        index);
    } SKY_SEQUENCE_FOREACH_END;
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_TUPLE,
                                    nfreevars);

    return SKY_TRUE;
}


static void
sky_python_compiler_build_function(sky_python_compiler_t *      compiler,
                                   sky_string_t                 qualname,
                                   sky_python_ast_arguments_t   arguments,
                                   sky_object_t                 returns,
                                   sky_code_t                   code)
{
    ssize_t ndefaults = 0, nkwdefaults = 0;

    size_t                  flags;
    ssize_t                 index;
    sky_object_t            expr;
    sky_python_ast_arg_t    arg;

    sky_python_compiler_code_push_const(compiler->code, qualname);
    sky_python_compiler_code_push_const(compiler->code, code);

    flags = 0;
    if (sky_python_compiler_build_annotations(compiler, arguments, returns)) {
        flags |= SKY_CODE_BUILD_FUNCTION_FLAG_ANNOTATIONS;
    }

    if (sky_python_compiler_build_closure(compiler, code)) {
        flags |= SKY_CODE_BUILD_FUNCTION_FLAG_CLOSURE;
    }

    if (!sky_object_isnull(arguments->defaults)) {
        ndefaults = sky_object_len(arguments->defaults);
        sky_python_compiler_visit_expr_sequence(compiler,
                                                arguments->defaults);
    }
    if (!sky_object_isnull(arguments->kw_defaults)) {
        index = 0;
        SKY_SEQUENCE_FOREACH(arguments->kw_defaults, expr) {
            if (!sky_object_isnull(expr)) {
                arg = sky_sequence_get(arguments->kwonlyargs, index);
                sky_python_compiler_code_push_const(compiler->code, arg->arg);
                sky_python_compiler_visit_expr(compiler, expr);
                ++nkwdefaults;
            }
            ++index;
        } SKY_SEQUENCE_FOREACH_END;
    }

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_FUNCTION,
                                    flags,
                                    ndefaults,
                                    nkwdefaults);
}


static void
sky_python_compiler_build_generator(sky_python_compiler_t * compiler,
                                    sky_object_t            generators,
                                    ssize_t                 index,
                                    sky_code_opcode_t       opcode,
                                    sky_object_t            elt,
                                    sky_object_t            value)
{
    sky_python_compiler_code_t      *code = compiler->code;

    ssize_t                         result_index;
    sky_python_compiler_block_t     *for_body_block, *for_exit_block,
                                    *for_iter_block;
    sky_python_ast_comprehension_t  comprehension;

    result_index = sky_python_compiler_table_get(&(compiler->code->varnames),
                                                 SKY_STRING_LITERAL("_[1]"));

    /* Fabricate a for-loop using target and iter from comprehension. Optimize
     * away the current_loop stuff in the code object since break/continue is
     * not possible.
     */
    comprehension = sky_sequence_get(generators, index);
    if (index) {
        sky_python_compiler_visit_expr(compiler, comprehension->iter);
    }
    else {
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_PUSH_LOCAL,
                                        0);
    }
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_ITERATOR);

    for_iter_block = sky_python_compiler_block_create(code);
    for_body_block = sky_python_compiler_block_create(code);
    for_exit_block = sky_python_compiler_block_create(code);

    sky_python_compiler_block_setflownext(code->current_block, for_iter_block);
    code->current_block = for_iter_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT,
                                    for_exit_block);

    sky_python_compiler_block_setflownext(code->current_block, for_body_block);
    code->current_block = for_body_block;
    sky_python_compiler_visit_expr(compiler, comprehension->target);

    if (!sky_object_isnull(comprehension->ifs)) {
        sky_object_t                expr;
        sky_python_compiler_block_t *if_else_block, *if_then_block;

        if_else_block = sky_python_compiler_block_create(code);
        sky_python_compiler_block_addop(if_else_block,
                                        SKY_CODE_OPCODE_POP);
        sky_python_compiler_block_addop(if_else_block,
                                        SKY_CODE_OPCODE_JUMP,
                                        for_iter_block);

        SKY_SEQUENCE_FOREACH(comprehension->ifs, expr) {
            sky_python_compiler_visit_expr(compiler, expr);
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_JUMP_IF_FALSE,
                                            if_else_block);

            if_then_block = sky_python_compiler_block_create(code);
            sky_python_compiler_block_setflownext(code->current_block,
                                                  if_then_block);
            code->current_block = if_then_block;
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_POP);
        } SKY_SEQUENCE_FOREACH_END;
    }

    if (++index < sky_object_len(generators)) {
        sky_python_compiler_build_generator(compiler,
                                            generators,
                                            index,
                                            opcode,
                                            elt,
                                            value);
    }
    else {
        switch (opcode) {
            case SKY_CODE_OPCODE_POP_ITEM:    /* DictComp */
                sky_python_compiler_visit_expr(compiler, value);
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_PUSH_LOCAL,
                                                result_index);
                sky_python_compiler_visit_expr(compiler, elt);
                sky_python_compiler_block_addop(code->current_block,
                                                opcode,
                                                1);
                break;
            case SKY_CODE_OPCODE_LIST_APPEND:   /* ListComp */
            case SKY_CODE_OPCODE_SET_ADD:       /* SetComp */
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_PUSH_LOCAL,
                                                result_index);
                sky_python_compiler_visit_expr(compiler, elt);
                sky_python_compiler_block_addop(code->current_block,
                                                opcode,
                                                1);
                break;

            case SKY_CODE_OPCODE_YIELD:         /* GeneratorExp */
                sky_python_compiler_visit_expr(compiler, elt);
                sky_python_compiler_block_addop(code->current_block,
                                                opcode);
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_POP);
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_JUMP,
                                                for_iter_block);
                break;

            default:
                sky_error_fatal("internal error");
        }
    }

    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    for_iter_block);

    code->current_block = for_exit_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
}


static void
sky_python_compiler_build_comprehension(sky_python_compiler_t * compiler,
                                        sky_object_t            node,
                                        sky_string_t            name,
                                        sky_object_t            generators,
                                        sky_object_t            elt,
                                        sky_object_t            value)
{
    ssize_t                         index;
    sky_bool_t                      is_generator;
    sky_code_t                      code_object;
    sky_string_t                    qualname;
    sky_code_opcode_t               opcode;
    sky_python_ast_arguments_t      arguments;
    sky_python_compiler_code_t      *code;
    sky_python_ast_comprehension_t  comprehension;

    is_generator = sky_object_isa(node, sky_python_ast_GeneratorExp_type);

    /* Build a new code object that is the comprehension. It'll take a single
     * argument that is the first iter in the comprehension. Any others are
     * free/cell variables.
     */
    code = sky_python_compiler_code_push(compiler, node, name);
    sky_python_compiler_table_set(&(code->constants), sky_None);
    code->argcount = 1;
    sky_python_compiler_table_set(&(code->varnames), SKY_STRING_LITERAL(".0"));
    if (is_generator) {
        code->flags |= SKY_CODE_FLAG_GENERATOR;
        opcode = SKY_CODE_OPCODE_YIELD;
    }
    else {
        if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_BUILD_DICT,
                                            0);
            opcode = SKY_CODE_OPCODE_POP_ITEM;
        }
        else if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_BUILD_LIST,
                                            0);
            opcode = SKY_CODE_OPCODE_LIST_APPEND;
        }
        else if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_BUILD_SET,
                                            0);
            opcode = SKY_CODE_OPCODE_SET_ADD;
        }
        else {
            sky_error_fatal("internal error");
        }
        index = sky_python_compiler_table_get(&(code->varnames),
                                              SKY_STRING_LITERAL("_[1]"));
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_POP_LOCAL,
                                        index);
    }

    sky_python_compiler_build_generator(compiler,
                                        generators,
                                        0,
                                        opcode,
                                        elt,
                                        value);

    if (!is_generator) {
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_PUSH_LOCAL,
                                        index);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_RETURN);
    }

    code_object = sky_python_compiler_code_assemble(compiler);
    qualname = sky_python_compiler_code_qualname(code);
    sky_python_compiler_code_pop(compiler);

    /* Wrap the code object with a callable function object and call it,
     * leaving the result on the stack.
     */
    arguments =
            sky_python_ast_arguments_create(
                    sky_object_build("[O]",
                            sky_python_ast_arg_create(SKY_STRING_LITERAL(".0"),
                            NULL)),
                    NULL,
                    NULL,
                    sky_tuple_empty,
                    NULL,
                    NULL,
                    sky_tuple_empty,
                    sky_tuple_empty);
    sky_python_compiler_build_function(compiler,
                                       qualname,
                                       arguments,
                                       NULL,
                                       code_object);

    comprehension = (sky_python_ast_comprehension_t)
                    sky_sequence_get(generators, 0);
    sky_python_compiler_visit_expr(compiler, comprehension->iter);
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_PUSH_ITERATOR);
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_CALL,
                                    1,
                                    0);
}


static void
sky_python_compiler_visit_arguments(sky_python_compiler_t *     compiler,
                                    sky_python_ast_arguments_t  node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_ast_arg_t    arg;

    if ((code->argcount = sky_object_len(node->args)) > 0) {
        SKY_SEQUENCE_FOREACH(node->args, arg) {
            sky_python_compiler_table_set(&(code->varnames), arg->arg);
        } SKY_SEQUENCE_FOREACH_END;
    }
    if (!sky_object_isnull(node->vararg)) {
        code->flags |= SKY_CODE_FLAG_VARARGS;
        sky_python_compiler_table_set(&(code->varnames), node->vararg);
    }
    if ((code->kwonlyargcount = sky_object_len(node->kwonlyargs)) > 0) {
        SKY_SEQUENCE_FOREACH(node->kwonlyargs, arg) {
            sky_python_compiler_table_set(&(code->varnames), arg->arg);
        } SKY_SEQUENCE_FOREACH_END;
    }
    if (!sky_object_isnull(node->kwarg)) {
        code->flags |= SKY_CODE_FLAG_VARKEYWORDS;
        sky_python_compiler_table_set(&(code->varnames), node->kwarg);
    }
}


static void
sky_python_compiler_visit_Attribute(sky_python_compiler_t *     compiler,
                                    sky_python_ast_Attribute_t  node)
{
    ssize_t                         index;
    sky_string_t                    mangled_name;
    sky_code_opcode_t               opcode;
    sky_python_ast_expr_context_t   ctx;

    ctx = sky_python_ast_normalize_expr_context(node->ctx);
    if (sky_python_ast_Load == ctx) {
        opcode = SKY_CODE_OPCODE_PUSH_ATTRIBUTE;
    }
    else if (sky_python_ast_Store == ctx) {
        opcode = SKY_CODE_OPCODE_POP_ATTRIBUTE;
    }
    else if (sky_python_ast_Del == ctx) {
        opcode = SKY_CODE_OPCODE_DELETE_ATTRIBUTE;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected expr_context; got %#@",
                               sky_type_name(sky_object_type(ctx)));
    }

    mangled_name = sky_python_compiler_mangle_name(compiler->class_name,
                                                   node->attr);
    index = sky_python_compiler_table_get(&(compiler->code->names),
                                          mangled_name);

    sky_python_compiler_visit_expr(compiler, node->value);
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    opcode,
                                    index);
}


static void
sky_python_compiler_visit_BinOp(sky_python_compiler_t * compiler,
                                sky_python_ast_BinOp_t  node)
{
    sky_code_opcode_t   opcode;

    if (sky_python_ast_Add == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_ADD;
    }
    else if (sky_python_ast_Sub == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_SUB;
    }
    else if (sky_python_ast_Mult == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_MUL;
    }
    else if (sky_python_ast_Div == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_DIV;
    }
    else if (sky_python_ast_Mod == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_MOD;
    }
    else if (sky_python_ast_Pow == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_POW;
    }
    else if (sky_python_ast_LShift == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_LSHIFT;
    }
    else if (sky_python_ast_RShift == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_RSHIFT;
    }
    else if (sky_python_ast_BitOr == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_OR;
    }
    else if (sky_python_ast_BitXor == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_XOR;
    }
    else if (sky_python_ast_BitAnd == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_AND;
    }
    else if (sky_python_ast_FloorDiv == node->op) {
        opcode = SKY_CODE_OPCODE_BINARY_FLOORDIV;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected operator; got %#@",
                               sky_type_name(sky_object_type(node->op)));
    }

    sky_python_compiler_visit_expr(compiler, node->left);
    sky_python_compiler_visit_expr(compiler, node->right);
    sky_python_compiler_block_addop(compiler->code->current_block, opcode);
}


static void
sky_python_compiler_visit_BoolOp(sky_python_compiler_t *    compiler,
                                 sky_python_ast_BoolOp_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                     i, len;
    sky_code_opcode_t           opcode;
    sky_python_compiler_block_t *exit_block;

    if (sky_python_ast_And == node->op) {
        opcode = SKY_CODE_OPCODE_JUMP_IF_FALSE;
    }
    else if (sky_python_ast_Or == node->op) {
        opcode = SKY_CODE_OPCODE_JUMP_IF_TRUE;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected operator; got %#@",
                               sky_type_name(sky_object_type(node->op)));
    }

    if ((len = sky_object_len(node->values)) < 2) {
        sky_error_raise_format(
                sky_ValueError,
                "malformed AST: BoolOp requires at least 2 values; got %d",
                len);
    }

    sky_python_compiler_visit_expr(compiler,
                                   sky_sequence_get(node->values, 0));

    exit_block = sky_python_compiler_block_create(code);

    for (i = 1; i < len; ++i) {
        sky_python_compiler_block_addop(code->current_block,
                                        opcode,
                                        exit_block);

        sky_python_compiler_block_setflownext(
                code->current_block,
                sky_python_compiler_block_create(code));
        code->current_block = code->current_block->flow_next;

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_POP);
        sky_python_compiler_visit_expr(compiler,
                                       sky_sequence_get(node->values, i));
    }

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
}


static void
sky_python_compiler_visit_keyword(sky_python_compiler_t *   compiler,
                                  sky_python_ast_keyword_t  node,
                                  sky_set_t                 keywords)
{
    if (!sky_object_isa(node, sky_python_ast_keyword_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected keyword; got %#@",
                               sky_type_name(sky_object_type(node)));
    }

    if (!sky_set_add(keywords, node->arg)) {
        sky_python_compiler_raise(compiler,
                                  sky_SyntaxError,
                                  "keyword argument repeated");
    }

    sky_python_compiler_code_push_const(compiler->code, node->arg);
    sky_python_compiler_visit_expr(compiler, node->value);
}


static void
sky_python_compiler_visit_keyword_sequence(sky_python_compiler_t *  compiler,
                                           sky_object_t             sequence)
{
    sky_set_t       keywords;
    sky_object_t    element;

    keywords = sky_set_create(NULL);
    SKY_SEQUENCE_FOREACH(sequence, element) {
        sky_python_compiler_visit_keyword(compiler, element, keywords);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_compiler_visit_Call(sky_python_compiler_t *  compiler,
                               sky_python_ast_Call_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t             nargs, nkws;
    sky_code_opcode_t   opcode;

    sky_python_compiler_visit_expr(compiler, node->func);

    opcode = SKY_CODE_OPCODE_CALL;
    if (sky_object_isnull(node->args)) {
        nargs = 0;
    }
    else {
        nargs = sky_object_len(node->args);
        sky_python_compiler_visit_expr_sequence(compiler, node->args);
    }
    if (!sky_object_isnull(node->starargs)) {
        sky_python_compiler_visit_expr(compiler, node->starargs);
        opcode = SKY_CODE_OPCODE_CALL_ARGS;
    }
    if (sky_object_isnull(node->keywords)) {
        nkws = 0;
    }
    else {
        nkws = sky_object_len(node->keywords);
        sky_python_compiler_visit_keyword_sequence(compiler, node->keywords);
    }
    if (!sky_object_isnull(node->kwargs)) {
        sky_python_compiler_visit_expr(compiler, node->kwargs);
        if (SKY_CODE_OPCODE_CALL_ARGS == opcode) {
            opcode = SKY_CODE_OPCODE_CALL_ARGS_KWS;
        }
        else {
            opcode = SKY_CODE_OPCODE_CALL_KWS;
        }
    }

    sky_python_compiler_block_addop(code->current_block, opcode, nargs, nkws);
}


static sky_compare_op_t
sky_python_compiler_compare_op(sky_object_t op)
{
    sky_compare_op_t    compare_op;

    if (sky_python_ast_Eq == op) {
        compare_op = SKY_COMPARE_OP_EQUAL;
    }
    else if (sky_python_ast_NotEq == op) {
        compare_op = SKY_COMPARE_OP_NOT_EQUAL;
    }
    else if (sky_python_ast_Lt == op) {
        compare_op = SKY_COMPARE_OP_LESS;
    }
    else if (sky_python_ast_LtE == op) {
        compare_op = SKY_COMPARE_OP_LESS_EQUAL;
    }
    else if (sky_python_ast_Gt == op) {
        compare_op = SKY_COMPARE_OP_GREATER;
    }
    else if (sky_python_ast_GtE == op) {
        compare_op = SKY_COMPARE_OP_GREATER_EQUAL;
    }
    else if (sky_python_ast_Is == op) {
        compare_op = SKY_COMPARE_OP_IS;
    }
    else if (sky_python_ast_IsNot == op) {
        compare_op = SKY_COMPARE_OP_IS_NOT;
    }
    else if (sky_python_ast_In == op) {
        compare_op = SKY_COMPARE_OP_IN;
    }
    else if (sky_python_ast_NotIn == op) {
        compare_op = SKY_COMPARE_OP_NOT_IN;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                                "malformed AST: expected cmpop; got %#@",
                                sky_type_name(sky_object_type(op)));
    }

    return compare_op;
}


static void
sky_python_compiler_visit_Compare(sky_python_compiler_t *   compiler,
                                  sky_python_ast_Compare_t  node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    size_t                      compare_op;
    ssize_t                     i, len;
    sky_object_t                comparator, op;
    sky_python_compiler_block_t *block, *exit_block;

    len = sky_object_len(node->comparators);
    if (sky_object_len(node->ops) != len) {
        sky_error_raise_string(
                sky_ValueError,
                "malformed AST: Compare ops and comparators must be of equal length");
    }
    if (!len) {
        sky_error_raise_string(
                sky_ValueError,
                "malformed AST: Compare ops and comparators must be of at least length 1");
    }

    sky_python_compiler_visit_expr(compiler, node->left);

    if (!--len) {
        op = sky_sequence_get(node->ops, len);
        compare_op = sky_python_compiler_compare_op(op);
        comparator = sky_sequence_get(node->comparators, len);
        sky_python_compiler_visit_expr(compiler, comparator);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_COMPARE,
                                        compare_op);
    }
    else {
        exit_block = sky_python_compiler_block_create(code);

        for (i = 0; i < len; ++i) {
            op = sky_sequence_get(node->ops, i);
            compare_op = sky_python_compiler_compare_op(op);
            comparator = sky_sequence_get(node->comparators, i);
            sky_python_compiler_visit_expr(compiler, comparator);

            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_PUSH_TOP);
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_ROTATE_THREE);
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_COMPARE,
                                            compare_op);
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_JUMP_IF_FALSE,
                                            exit_block);

            block = sky_python_compiler_block_create(code);
            sky_python_compiler_block_setflownext(code->current_block, block);
            code->current_block = block;

            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_POP);
        }

        op = sky_sequence_get(node->ops, len);
        compare_op = sky_python_compiler_compare_op(op);
        comparator = sky_sequence_get(node->comparators, len);
        sky_python_compiler_visit_expr(compiler, comparator);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_PUSH_TOP);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_ROTATE_THREE);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_COMPARE,
                                        compare_op);

        sky_python_compiler_block_setflownext(code->current_block, exit_block);
        code->current_block = exit_block;

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_ROTATE_TWO);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_POP);
    }
}


static void
sky_python_compiler_visit_Dict(sky_python_compiler_t *  compiler,
                               sky_python_ast_Dict_t    node)
{
    ssize_t i, len;

    len = sky_object_len(node->keys);
    for (i = 0; i < len; ++i) {
        sky_python_compiler_visit_expr(compiler,
                                       sky_sequence_get(node->keys, i));
        sky_python_compiler_visit_expr(compiler,
                                       sky_sequence_get(node->values, i));
    }

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_DICT,
                                    len);
}


static void
sky_python_compiler_visit_DictComp(sky_python_compiler_t *      compiler,
                                   sky_python_ast_DictComp_t    node)
{
    sky_python_compiler_build_comprehension(compiler,
                                            node,
                                            SKY_STRING_LITERAL("<dictcomp>"),
                                            node->generators,
                                            node->key,
                                            node->value);
}


static void
sky_python_compiler_visit_slice(sky_python_compiler_t * compiler,
                                sky_python_ast_slice_t  node);

static void
sky_python_compiler_visit_ExtSlice(sky_python_compiler_t *      compiler,
                                   sky_python_ast_ExtSlice_t    node)
{
    ssize_t         ndims;
    sky_object_t    dim;

    if (sky_python_compiler_is_constant(node->dims)) {
        sky_python_compiler_code_push_const(
                compiler->code,
                sky_python_compiler_constant(compiler, node->dims));
        return;
    }

    ndims = 0;
    SKY_SEQUENCE_FOREACH(node->dims, dim) {
        sky_python_compiler_visit_slice(compiler, dim);
        ++ndims;
    } SKY_SEQUENCE_FOREACH_END;

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_TUPLE,
                                    ndims);
}


static void
sky_python_compiler_visit_GeneratorExp(sky_python_compiler_t *          compiler,
                                       sky_python_ast_GeneratorExp_t    node)
{
    sky_python_compiler_build_comprehension(compiler,
                                            node,
                                            SKY_STRING_LITERAL("<genexpr>"),
                                            node->generators,
                                            node->elt,
                                            NULL);
}


static void
sky_python_compiler_visit_IfExp(sky_python_compiler_t * compiler,
                                sky_python_ast_IfExp_t  node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_compiler_block_t *body_block, *exit_block, *orelse_block;

    body_block = sky_python_compiler_block_create(code);
    orelse_block = sky_python_compiler_block_create(code);
    exit_block = sky_python_compiler_block_create(code);

    sky_python_compiler_visit_expr(compiler, node->test);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP_IF_FALSE,
                                    orelse_block);

    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    sky_python_compiler_visit_expr(compiler, node->body);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    exit_block);

    code->current_block = orelse_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    sky_python_compiler_visit_expr(compiler, node->orelse);

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
}


static void
sky_python_compiler_visit_Lambda(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Lambda_t    node)
{
    sky_code_t      code_object;
    sky_string_t    qualname;

    sky_python_compiler_code_push(compiler,
                                  node,
                                  SKY_STRING_LITERAL("<lambda>"));
    sky_python_compiler_table_set(&(compiler->code->constants), sky_None);
    sky_python_compiler_visit_arguments(compiler, node->args);

    sky_python_compiler_visit_expr(compiler, node->body);

    code_object = sky_python_compiler_code_assemble(compiler);
    qualname = sky_python_compiler_code_qualname(compiler->code);
    sky_python_compiler_code_pop(compiler);

    sky_python_compiler_build_function(compiler,
                                       qualname,
                                       node->args,
                                       NULL,
                                       code_object);
}


static void
sky_python_compiler_visit_sequence(sky_python_compiler_t *          compiler,
                                   sky_code_opcode_t                opcode,
                                   sky_object_t                     sequence,
                                   sky_python_ast_expr_context_t    ctx)
{
    ssize_t         after, before, i, len;
    sky_object_t    elt;

    ctx = sky_python_ast_normalize_expr_context(ctx);
    len = sky_object_len(sequence);

    if (sky_python_ast_Store == ctx) {
        after = before = 0;
        for (i = 0; i < len; ++i) {
            elt = sky_sequence_get(sequence, i);
            if (sky_object_isa(elt, sky_python_ast_Starred_type)) {
                before = i;
                while (++i < len) {
                    elt = sky_sequence_get(sequence, i);
                    if (sky_object_isa(elt, sky_python_ast_Starred_type)) {
                        sky_python_compiler_raise(
                                compiler,
                                sky_SyntaxError,
                                "only one starred expression allowed in assignment");
                    }
                }
                after = len - before - 1;
                sky_python_compiler_block_addop(
                        compiler->code->current_block,
                        SKY_CODE_OPCODE_UNPACK_STARRED,
                        before,
                        after);
                SKY_SEQUENCE_FOREACH(sequence, elt) {
                    if (sky_object_isa(elt, sky_python_ast_Starred_type)) {
                        elt = ((sky_python_ast_Starred_t)elt)->value;
                    }
                    sky_python_compiler_visit_expr(compiler, elt);
                } SKY_SEQUENCE_FOREACH_END;
                return;
            }
        }
        sky_python_compiler_block_addop(compiler->code->current_block,
                                        SKY_CODE_OPCODE_UNPACK_SEQUENCE,
                                        len);
    }

    sky_python_compiler_visit_expr_sequence(compiler, sequence);

    if (sky_python_ast_Load == ctx) {
        sky_python_compiler_block_addop(compiler->code->current_block,
                                        opcode,
                                        len);
    }
}


static void
sky_python_compiler_visit_List(sky_python_compiler_t *  compiler,
                               sky_python_ast_List_t    node)
{
    sky_python_compiler_visit_sequence(compiler,
                                       SKY_CODE_OPCODE_BUILD_LIST,
                                       node->elts,
                                       node->ctx);
}


static void
sky_python_compiler_visit_ListComp(sky_python_compiler_t *      compiler,
                                   sky_python_ast_ListComp_t    node)
{
    sky_python_compiler_build_comprehension(compiler,
                                            node,
                                            SKY_STRING_LITERAL("<listcomp>"),
                                            node->generators,
                                            node->elt,
                                            NULL);
}


static void
sky_python_compiler_generate_name(sky_python_compiler_t *       compiler,
                                  sky_string_t                  name,
                                  sky_python_ast_expr_context_t ctx)
{
    sky_python_compiler_code_t          *code = compiler->code;
    sky_python_symtable_block_data_t    *symtable = code->symtable;

    ssize_t                     index;
    intmax_t                    flags;
    sky_object_t                value;
    sky_string_t                mangled_name;
    sky_code_opcode_t           opcode;
    sky_python_compiler_table_t *table;

    /* Handle None, True, False, etc. */
    if ((value = sky_python_compiler_constant_name(compiler, name)) != NULL) {
        sky_python_compiler_table_get(&(code->names), name);
        sky_python_compiler_code_push_const(code, value);
        return;
    }

    /* Find the name in the symbol table. Note that __doc__ does not get an
     * entry in the symbol table for its implicit assignment, so fake it.
     */
    mangled_name = sky_python_compiler_mangle_name(compiler->class_name, name);
    if ((value = sky_dict_get(symtable->symbols, mangled_name, NULL)) != NULL) {
        flags = sky_integer_value(value, INTMAX_MIN, INTMAX_MAX, NULL);
    }
    else {
        flags = 0;
        sky_python_symtable_symbol_setscope(
                &flags,
                SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_UNKNOWN);
    }

    ctx = sky_python_ast_normalize_expr_context(ctx);
    if (sky_python_ast_Load == ctx) {
        switch (sky_python_symtable_symbol_scope(flags)) {
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_UNKNOWN:
                opcode = SKY_CODE_OPCODE_PUSH_NAME;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL:
                if (symtable->type == SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION) {
                    opcode = SKY_CODE_OPCODE_PUSH_LOCAL;
                    table = &(code->varnames);
                }
                else {
                    opcode = SKY_CODE_OPCODE_PUSH_NAME;
                    table = &(code->names);
                }
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_EXPLICIT:
                opcode = SKY_CODE_OPCODE_PUSH_GLOBAL;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_IMPLICIT:
                opcode = SKY_CODE_OPCODE_PUSH_NAME;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE:
                opcode = SKY_CODE_OPCODE_PUSH_FREE;
                table = &(code->freevars);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL:
                opcode = SKY_CODE_OPCODE_PUSH_CELL;
                table = &(code->cellvars);
                break;
        }
    }
    else if (sky_python_ast_Store == ctx) {
        switch (sky_python_symtable_symbol_scope(flags)) {
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_UNKNOWN:
                opcode = SKY_CODE_OPCODE_POP_NAME;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL:
                if (symtable->type == SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION) {
                    opcode = SKY_CODE_OPCODE_POP_LOCAL;
                    table = &(code->varnames);
                }
                else {
                    opcode = SKY_CODE_OPCODE_POP_NAME;
                    table = &(code->names);
                }
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_EXPLICIT:
                opcode = SKY_CODE_OPCODE_POP_GLOBAL;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_IMPLICIT:
                opcode = SKY_CODE_OPCODE_POP_NAME;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE:
                opcode = SKY_CODE_OPCODE_POP_FREE;
                table = &(code->freevars);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL:
                opcode = SKY_CODE_OPCODE_POP_CELL;
                table = &(code->cellvars);
                break;
        }
    }
    else if (sky_python_ast_Del == ctx) {
        switch (sky_python_symtable_symbol_scope(flags)) {
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_UNKNOWN:
                opcode = SKY_CODE_OPCODE_DELETE_NAME;
                table = &(code->cellvars);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_LOCAL:
                if (symtable->type == SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION) {
                    opcode = SKY_CODE_OPCODE_DELETE_LOCAL;
                    table = &(code->varnames);
                }
                else {
                    opcode = SKY_CODE_OPCODE_DELETE_NAME;
                    table = &(code->names);
                }
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_EXPLICIT:
                opcode = SKY_CODE_OPCODE_DELETE_GLOBAL;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_GLOBAL_IMPLICIT:
                opcode = SKY_CODE_OPCODE_DELETE_NAME;
                table = &(code->names);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_FREE:
                opcode = SKY_CODE_OPCODE_DELETE_FREE;
                table = &(code->freevars);
                break;
            case SKY_PYTHON_SYMTABLE_SYMBOL_SCOPE_CELL:
                opcode = SKY_CODE_OPCODE_DELETE_CELL;
                table = &(code->cellvars);
                break;
        }
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected expr_context; got %#@",
                               sky_type_name(sky_object_type(ctx)));
    }

    index = sky_python_compiler_table_get(table, mangled_name);
    sky_python_compiler_block_addop(code->current_block, opcode, index);
}


static void
sky_python_compiler_visit_Name(sky_python_compiler_t *  compiler,
                               sky_python_ast_Name_t    node)
{
    sky_python_compiler_generate_name(compiler, node->id, node->ctx);
}


static void
sky_python_compiler_visit_Set(sky_python_compiler_t *   compiler,
                              sky_python_ast_Set_t      node)
{
    sky_object_t    elt;

    SKY_SEQUENCE_FOREACH(node->elts, elt) {
        sky_python_compiler_visit_expr(compiler, elt);
    } SKY_SEQUENCE_FOREACH_END;

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_SET,
                                    sky_object_len(node->elts));
}


static void
sky_python_compiler_visit_SetComp(sky_python_compiler_t *   compiler,
                                  sky_python_ast_SetComp_t  node)
{
    sky_python_compiler_build_comprehension(compiler,
                                            node,
                                            SKY_STRING_LITERAL("<setcomp>"),
                                            node->generators,
                                            node->elt,
                                            NULL);
}


static void
sky_python_compiler_visit_Slice(sky_python_compiler_t * compiler,
                                sky_python_ast_Slice_t  node)
{
    sky_slice_t slice;

    if (!sky_object_isa(node, sky_python_ast_Slice_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected Slice; got %#@",
                               sky_type_name(sky_object_type(node)));
    }

    if (sky_python_compiler_is_constant(node)) {
        slice = sky_python_compiler_constant(compiler, node);
        sky_python_compiler_code_push_const(compiler->code, slice);
    }
    else {
        if (sky_object_isnull(node->lower)) {
            sky_python_compiler_code_push_None(compiler->code);
        }
        else {
            sky_python_compiler_visit_expr(compiler, node->lower);
        }
        if (sky_object_isnull(node->upper)) {
            sky_python_compiler_code_push_None(compiler->code);
        }
        else {
            sky_python_compiler_visit_expr(compiler, node->upper);
        }
        if (sky_object_isnull(node->step)) {
            sky_python_compiler_code_push_None(compiler->code);
        }
        else {
            sky_python_compiler_visit_expr(compiler, node->step);
        }
        sky_python_compiler_block_addop(compiler->code->current_block,
                                        SKY_CODE_OPCODE_BUILD_SLICE);
    }
}


static void
sky_python_compiler_visit_slice(sky_python_compiler_t * compiler,
                                sky_python_ast_slice_t  node)
{
    if (sky_object_isa(node, sky_python_ast_Index_type)) {
        sky_python_compiler_visit_expr(
                compiler,
                ((sky_python_ast_Index_t)node)->value);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ExtSlice_type)) {
        sky_python_compiler_visit_ExtSlice(
                compiler,
                (sky_python_ast_ExtSlice_t)node);
        return;
    }
    sky_python_compiler_visit_Slice(compiler, (sky_python_ast_Slice_t)node);
}


static void
sky_python_compiler_visit_Subscript(sky_python_compiler_t *     compiler,
                                    sky_python_ast_Subscript_t  node)
{
    sky_code_opcode_t               opcode;
    sky_python_ast_expr_context_t   ctx;

    ctx = sky_python_ast_normalize_expr_context(node->ctx);
    if (sky_python_ast_Load == ctx) {
        opcode = SKY_CODE_OPCODE_PUSH_ITEM;
    }
    else if (sky_python_ast_Store == ctx) {
        opcode = SKY_CODE_OPCODE_POP_ITEM;
    }
    else if (sky_python_ast_Del == ctx) {
        opcode = SKY_CODE_OPCODE_DELETE_ITEM;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected expr_context; got %#@",
                               sky_type_name(sky_object_type(ctx)));
    }

    sky_python_compiler_visit_expr(compiler, node->value);
    sky_python_compiler_visit_slice(compiler, node->slice);
    sky_python_compiler_block_addop(compiler->code->current_block, opcode);
}


static void
sky_python_compiler_visit_Tuple(sky_python_compiler_t * compiler,
                                sky_python_ast_Tuple_t  node)
{
    if (sky_python_compiler_is_constant(node)) {
        sky_python_compiler_code_push_const(
                compiler->code,
                sky_python_compiler_constant(compiler, node));
    }
    else {
        sky_python_compiler_visit_sequence(compiler,
                                           SKY_CODE_OPCODE_BUILD_TUPLE,
                                           node->elts,
                                           node->ctx);
    }
}


static void
sky_python_compiler_visit_UnaryOp(sky_python_compiler_t *   compiler,
                                  sky_python_ast_UnaryOp_t  node)
{
    sky_code_opcode_t   opcode;

    if (sky_python_ast_Invert == node->op) {
        opcode = SKY_CODE_OPCODE_UNARY_INVERT;
    }
    else if (sky_python_ast_Not == node->op) {
        opcode = SKY_CODE_OPCODE_UNARY_NOT;
    }
    else if (sky_python_ast_UAdd == node->op) {
        opcode = SKY_CODE_OPCODE_UNARY_POSITIVE;
    }
    else if (sky_python_ast_USub == node->op) {
        opcode = SKY_CODE_OPCODE_UNARY_NEGATIVE;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected unaryop; got %#@",
                               sky_type_name(sky_object_type(node->op)));
    }

    sky_python_compiler_visit_expr(compiler, node->operand);
    sky_python_compiler_block_addop(compiler->code->current_block, opcode);
}


static void
sky_python_compiler_visit_expr(sky_python_compiler_t *  compiler,
                               sky_object_t             node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_ast_expr_context_t   ctx;

    if (!sky_object_isa(node, sky_python_ast_expr_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected expr; got %#@",
                               sky_type_name(sky_object_type(node)));
    }

    code->current_block->lineno = ((sky_python_ast_expr_t)node)->lineno;
    code->current_block->col_offset = ((sky_python_ast_expr_t)node)->col_offset;

    if (sky_object_isa(node, sky_python_ast_BoolOp_type)) {
        sky_python_compiler_visit_BoolOp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_BinOp_type)) {
        sky_python_compiler_visit_BinOp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_UnaryOp_type)) {
        sky_python_compiler_visit_UnaryOp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Lambda_type)) {
        sky_python_compiler_visit_Lambda(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_IfExp_type)) {
        sky_python_compiler_visit_IfExp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Dict_type)) {
        sky_python_compiler_visit_Dict(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Set_type)) {
        sky_python_compiler_visit_Set(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
        sky_python_compiler_visit_ListComp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
        sky_python_compiler_visit_SetComp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
        sky_python_compiler_visit_DictComp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        sky_python_compiler_visit_GeneratorExp(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Yield_type)) {
        if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION != code->symtable->type) {
            sky_python_compiler_raise(compiler,
                                      sky_SyntaxError,
                                      "'yield' outside function");
        }
        code->flags |= SKY_CODE_FLAG_GENERATOR;
        if (sky_object_isnull(((sky_python_ast_Yield_t)node)->value)) {
            sky_python_compiler_code_push_None(code);
        }
        else {
            sky_python_compiler_visit_expr(
                    compiler,
                    ((sky_python_ast_Yield_t)node)->value);
        }
        sky_python_compiler_block_addop(
                code->current_block,
                SKY_CODE_OPCODE_YIELD);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_YieldFrom_type)) {
        if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION != code->symtable->type) {
            sky_python_compiler_raise(compiler,
                                      sky_SyntaxError,
                                      "'yield' outside function");
        }
        code->flags |= SKY_CODE_FLAG_GENERATOR;
        sky_python_compiler_visit_expr(
                compiler,
                ((sky_python_ast_YieldFrom_t)node)->value);
        sky_python_compiler_block_addop(
                code->current_block,
                SKY_CODE_OPCODE_PUSH_ITERATOR);
        sky_python_compiler_code_push_None(code);
        sky_python_compiler_block_addop(
                code->current_block,
                SKY_CODE_OPCODE_YIELD_FROM);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Compare_type)) {
        sky_python_compiler_visit_Compare(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Call_type)) {
        sky_python_compiler_visit_Call(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Num_type)) {
        sky_python_compiler_code_push_const(
                code,
                ((sky_python_ast_Num_t)node)->n);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Str_type)) {
        sky_python_compiler_code_push_const(
                code,
                ((sky_python_ast_Str_t)node)->s);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Bytes_type)) {
        sky_python_compiler_code_push_const(
                code,
                ((sky_python_ast_Bytes_t)node)->s);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Ellipsis_type)) {
        sky_python_compiler_code_push_const(
                code,
                sky_Ellipsis);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Attribute_type)) {
        sky_python_compiler_visit_Attribute(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Subscript_type)) {
        sky_python_compiler_visit_Subscript(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Starred_type)) {
        ctx = sky_python_ast_normalize_expr_context(
                    ((sky_python_ast_Starred_t)node)->ctx);
        if (sky_python_ast_Store == ctx) {
            sky_python_compiler_raise(
                    compiler,
                    sky_SyntaxError,
                    "starred assignment target must be in a list or tuple");
        }
        else {
            sky_python_compiler_raise(
                    compiler,
                    sky_SyntaxError,
                    "starred expression must be an assignment target");
        }
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Name_type)) {
        sky_python_compiler_visit_Name(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_List_type)) {
        sky_python_compiler_visit_List(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        sky_python_compiler_visit_Tuple(compiler, node);
        return;
    }
    sky_error_fatal("internal error");
}


static void
sky_python_compiler_visit_Assert(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Assert_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                     index;
    sky_python_compiler_block_t *body_block, *exit_block;

    if (compiler->optlevel > 0) {
        return;
    }

    body_block = sky_python_compiler_block_create(code);
    exit_block = sky_python_compiler_block_create(code);

    sky_python_compiler_visit_expr(compiler, node->test);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP_IF_TRUE,
                                    exit_block);

    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;

    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    index = sky_python_compiler_table_get(&(code->names),
                                          SKY_STRING_LITERAL("AssertionError"));
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_NAME,
                                    index);
    if (!sky_object_isnull(node->msg)) {
        sky_python_compiler_visit_expr(compiler, node->msg);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_CALL,
                                        1,
                                        0);
    }
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_RAISE,
                                    1);

    code->current_block = exit_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
}


static void
sky_python_compiler_visit_Assign(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Assign_t    node)
{
    ssize_t i, len;

    sky_python_compiler_visit_expr(compiler, node->value);
    if ((len = sky_object_len(node->targets)) > 1) {
        for (i = 0; i < len - 1; ++i) {
            sky_python_compiler_block_addop(compiler->code->current_block,
                                            SKY_CODE_OPCODE_PUSH_TOP);
            sky_python_compiler_visit_expr(compiler,
                                           sky_sequence_get(node->targets, i));
        }
    }
    sky_python_compiler_visit_expr(compiler,
                                   sky_sequence_get(node->targets, len - 1));
}


static void
sky_python_compiler_visit_AugAssign(sky_python_compiler_t *     compiler,
                                    sky_python_ast_AugAssign_t  node)
{
    sky_object_t        target_load;
    sky_code_opcode_t   opcode;

    if (sky_python_ast_Add == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_ADD;
    }
    else if (sky_python_ast_Sub == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_SUB;
    }
    else if (sky_python_ast_Mult == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_MUL;
    }
    else if (sky_python_ast_Div == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_DIV;
    }
    else if (sky_python_ast_Mod == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_MOD;
    }
    else if (sky_python_ast_Pow == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_POW;
    }
    else if (sky_python_ast_LShift == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_LSHIFT;
    }
    else if (sky_python_ast_RShift == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_RSHIFT;
    }
    else if (sky_python_ast_BitOr == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_OR;
    }
    else if (sky_python_ast_BitXor == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_XOR;
    }
    else if (sky_python_ast_BitAnd == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_AND;
    }
    else if (sky_python_ast_FloorDiv == node->op) {
        opcode = SKY_CODE_OPCODE_INPLACE_FLOORDIV;
    }
    else {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected operator; got %#@",
                               sky_type_name(sky_object_type(node->op)));
    }

    /* For an augmented assignment, the node contains the target with a Store
     * context and the value with a Load context. Bytecode needs to be
     * generated to Load the target, Load the value, perform the operation, op,
     * and store the target. Visiting the target will always generate a store
     * op, so create a temporary copy of the target, changing the context to
     * Load, and visit that to get the target Load.
     */
    if (sky_object_isa(node->target, sky_python_ast_Attribute_type)) {
        sky_python_ast_Attribute_t  attr =
                                    (sky_python_ast_Attribute_t)node->target;

        target_load = sky_python_ast_Attribute_create(attr->value,
                                                      attr->attr,
                                                      sky_python_ast_Load,
                                                      attr->lineno,
                                                      attr->col_offset);
                        
    }
    else if (sky_object_isa(node->target, sky_python_ast_Subscript_type)) {
        sky_python_ast_Subscript_t  subs =
                                    (sky_python_ast_Subscript_t)node->target;

        target_load = sky_python_ast_Subscript_create(subs->value,
                                                      subs->slice,
                                                      sky_python_ast_Load,
                                                      subs->lineno,
                                                      subs->col_offset);
    }
    else if (sky_object_isa(node->target, sky_python_ast_Name_type)) {
        sky_python_ast_Name_t   name = (sky_python_ast_Name_t)node->target;

        target_load = sky_python_ast_Name_create(name->id,
                                                 sky_python_ast_Load,
                                                 name->lineno,
                                                 name->col_offset);
    }
    else {
        sky_error_raise_format(
                sky_TypeError,
                "malformed AST: expected Attribute, Subscript, or Name; got %#@",
                sky_type_name(sky_object_type(node->target)));
    }

    sky_python_compiler_visit_expr(compiler, target_load);
    sky_python_compiler_visit_expr(compiler, node->value);
    sky_python_compiler_block_addop(compiler->code->current_block, opcode);
    sky_python_compiler_visit_expr(compiler, node->target);
}


static void
sky_python_compiler_visit_ClassDef(sky_python_compiler_t *      compiler,
                                   sky_python_ast_ClassDef_t    node)
{
    size_t                  flags;
    ssize_t                 i, index, len, nbases, nkeywords;
    sky_code_t              code_object;
    sky_object_t            docstr;
    sky_string_t            previous_class_name, qualname;
    sky_python_ast_expr_t   value;
    sky_python_ast_stmt_t   stmt;

    if (sky_object_bool(node->decorator_list)) {
        sky_python_compiler_visit_expr_sequence(compiler,
                                                node->decorator_list);
    }

    i = 0;
    flags = 0;
    docstr = sky_None;
    if (!sky_object_isnull(node->body) &&
        (len = sky_object_len(node->body)) > 0)
    {
        stmt = sky_sequence_get(node->body, 0);
        if (sky_object_isa(stmt, sky_python_ast_Expr_type)) {
            value = ((sky_python_ast_Expr_t)stmt)->value;
            if (sky_object_isa(value, sky_python_ast_Str_type)) {
                ++i;
                if (compiler->optlevel < 2) {
                    docstr = ((sky_python_ast_Str_t)value)->s;
                }
            }
        }
    }

    previous_class_name = compiler->class_name;
    sky_python_compiler_code_push(compiler, node, node->name);
    sky_python_compiler_table_set(&(compiler->code->constants), docstr);
    compiler->class_name = compiler->code->name;

    sky_python_compiler_generate_name(compiler,
                                      SKY_STRING_LITERAL("__name__"),
                                      sky_python_ast_Load);
    sky_python_compiler_generate_name(compiler,
                                      SKY_STRING_LITERAL("__module__"),
                                      sky_python_ast_Store);

    qualname = sky_python_compiler_code_qualname(compiler->code);
    sky_python_compiler_code_push_const(compiler->code, qualname);
    sky_python_compiler_generate_name(compiler,
                                      SKY_STRING_LITERAL("__qualname__"),
                                      sky_python_ast_Store);

    if (!i) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    }
    else {
        while (i < len) {
            stmt = sky_sequence_get(node->body, i);
            sky_python_compiler_visit_stmt(compiler, stmt);
            ++i;
        }
    }

    index = sky_python_compiler_table_get(&(compiler->code->cellvars),
                                          SKY_STRING_LITERAL("__class__"));
    if (-1 == index) {
        sky_python_compiler_code_push_None(compiler->code);
    }
    else {
        sky_python_compiler_block_addop(compiler->code->current_block,
                                        SKY_CODE_OPCODE_PUSH_CLOSURE_CELL,
                                        index);
    }
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_RETURN);

    code_object = sky_python_compiler_code_assemble(compiler);

    sky_python_compiler_code_pop(compiler);
    compiler->class_name = previous_class_name;

    sky_python_compiler_code_push_const(compiler->code, qualname);
    sky_python_compiler_code_push_const(compiler->code, code_object);
    if (sky_python_compiler_build_closure(compiler, code_object)) {
        flags |= SKY_CODE_BUILD_CLASS_FLAG_CLOSURE;
    }

    if (sky_object_isnull(node->bases)) {
        nbases = 0;
    }
    else {
        nbases = sky_object_len(node->bases);
        sky_python_compiler_visit_expr_sequence(compiler, node->bases);
    }
    if (!sky_object_isnull(node->starargs)) {
        flags |= SKY_CODE_BUILD_CLASS_FLAG_ARGS;
        sky_python_compiler_visit_expr(compiler, node->starargs);
    }
    if (sky_object_isnull(node->keywords)) {
        nkeywords = 0;
    }
    else {
        nkeywords = sky_object_len(node->keywords);
        sky_python_compiler_visit_keyword_sequence(compiler, node->keywords);
    }
    if (!sky_object_isnull(node->kwargs)) {
        flags |= SKY_CODE_BUILD_CLASS_FLAG_KWS;
        sky_python_compiler_visit_expr(compiler, node->kwargs);
    }

    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_BUILD_CLASS,
                                    flags,
                                    nbases,
                                    nkeywords);

    if (sky_object_bool(node->decorator_list)) {
        sky_python_compiler_block_t *block = compiler->code->current_block;

        for (i = 0, len = sky_object_len(node->decorator_list); i < len; ++i) {
            sky_python_compiler_block_addop(block, SKY_CODE_OPCODE_CALL, 1, 0);
        }
    }

    sky_python_compiler_generate_name(compiler,
                                      node->name,
                                      sky_python_ast_Store);
}


static void
sky_python_compiler_visit_For(sky_python_compiler_t *   compiler,
                              sky_python_ast_For_t      node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_compiler_loop_t  loop;
    sky_python_compiler_block_t *body_block, *exit_block, *iter_block,
                                *orelse_block;

    iter_block = sky_python_compiler_block_create(code);
    body_block = sky_python_compiler_block_create(code);
    exit_block = sky_python_compiler_block_create(code);
    orelse_block = sky_python_compiler_block_create(code);

    loop.parent = code->current_loop;
    loop.break_block = exit_block;
    loop.continue_block = iter_block;
    loop.pop_stack = 1;
    code->current_loop = &loop;

    sky_python_compiler_visit_expr(compiler, node->iter);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_ITERATOR);

    sky_python_compiler_block_setflownext(code->current_block, iter_block);
    code->current_block = iter_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_ITERATOR_NEXT,
                                    orelse_block);

    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;

    sky_python_compiler_visit_expr(compiler, node->target);

    sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    loop.continue_block);

    code->current_block = orelse_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    if (sky_object_bool(node->orelse)) {
        loop.break_block = NULL;
        loop.continue_block = NULL;
        sky_python_compiler_visit_stmt_sequence(compiler, node->orelse);
    }

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);

    code->current_loop = loop.parent;
}


static void
sky_python_compiler_visit_FunctionDef(sky_python_compiler_t *       compiler,
                                      sky_python_ast_FunctionDef_t  node)
{
    ssize_t                     i, len;
    sky_code_t                  code_object;
    sky_object_t                docstr;
    sky_string_t                qualname;
    sky_python_ast_expr_t       value;
    sky_python_ast_stmt_t       stmt;
    sky_python_compiler_code_t  *code;

    if (sky_object_bool(node->decorator_list)) {
        sky_python_compiler_visit_expr_sequence(compiler,
                                                node->decorator_list);
    }

    i = 0;
    docstr = sky_None;
    if (!sky_object_isnull(node->body) &&
        (len = sky_object_len(node->body)) > 0)
    {
        stmt = sky_sequence_get(node->body, 0);
        if (sky_object_isa(stmt, sky_python_ast_Expr_type)) {
            value = ((sky_python_ast_Expr_t)stmt)->value;
            if (sky_object_isa(value, sky_python_ast_Str_type)) {
                ++i;
                if (compiler->optlevel < 2) {
                    docstr = ((sky_python_ast_Str_t)value)->s;
                }
            }
        }
    }

    code = sky_python_compiler_code_push(compiler, node, node->name);
    sky_python_compiler_table_set(&(code->constants), docstr);
    sky_python_compiler_visit_arguments(compiler, node->args);

    if (!i) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    }
    else {
        while (i < len) {
            stmt = sky_sequence_get(node->body, i);
            sky_python_compiler_visit_stmt(compiler, stmt);
            ++i;
        }
    }

    code_object = sky_python_compiler_code_assemble(compiler);
    qualname = sky_python_compiler_code_qualname(code);
    sky_python_compiler_code_pop(compiler);

    sky_python_compiler_build_function(compiler,
                                       qualname,
                                       node->args,
                                       node->returns,
                                       code_object);

    if (sky_object_bool(node->decorator_list)) {
        sky_python_compiler_block_t *block = compiler->code->current_block;

        for (i = 0, len = sky_object_len(node->decorator_list); i < len; ++i) {
            sky_python_compiler_block_addop(block, SKY_CODE_OPCODE_CALL, 1, 0);
        }
    }

    sky_python_compiler_generate_name(compiler,
                                      node->name,
                                      sky_python_ast_Store);
}


static void
sky_python_compiler_visit_If(sky_python_compiler_t *compiler,
                             sky_python_ast_If_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_compiler_block_t *body_block, *exit_block, *orelse_block;

    body_block = sky_python_compiler_block_create(code);
    orelse_block = sky_python_compiler_block_create(code);
    exit_block = sky_python_compiler_block_create(code);

    sky_python_compiler_visit_expr(compiler, node->test);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP_IF_FALSE,
                                    orelse_block);

    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    exit_block);

    code->current_block = orelse_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);

    if (sky_object_bool(node->orelse)) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->orelse);
    }

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
}


/* Module import behavior is a little weird. Some explanation is in order.
 * A simple import (no dot) is obvious: 'import module as name' results in
 * 'name' being assigned the module object 'module'. Similarly, 'import module'
 * leaves 'module' in the local namespace. Things get more complicated when a
 * dotted name is used for 'module'. Exactly what you would expect happens with
 * an alias: 'import module.submodule as name' results in 'name' being assigned
 * the module object 'submodule'. However, things get wonky if there's no alias
 * in this case: 'import module.submodule' leaves 'module' in the local
 * namespace rather than 'submodule'. I have no idea why this is so, but for
 * this to happen, some non-obvious behavior has to occur in the generated
 * bytecode. The only way to reasonably do this without introducing special
 * case opcodes is to have the base IMPORT_MODULE opcode always leave the root
 * module behind on the stack. When an alias is involved, generate the
 * appropriate PUSH_ATTRIBUTE instructions.
 */


static void
sky_python_compiler_visit_Import(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Import_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                         i, index, next_i;
    sky_string_t                    attr_name, dot, mangled_name, name;
    sky_python_ast_alias_t          alias;
    sky_python_ast_expr_context_t   ctx;

    SKY_SEQUENCE_FOREACH(node->names, alias) {
        /* Yep, module names get mangled. Weird, right? I'm not sure it's
         * intended, but for consistency's sake ... I think it's wrong,
         * because while 'import __A' would mangle __A, 'import __A.__B' won't
         * end up mangling anything (mangling doesn't mangle anything with '.'s
         * in it.
         */
        mangled_name = sky_python_compiler_mangle_name(compiler->class_name,
                                                       alias->name);

        /* __import__(mangled_name, fromlist=None, level=0)
         * The implementation of SKY_CODE_OPCODE_IMPORT_MODULE will drop the
         * appropriate globals/locals from the calling frame into the call.
         * Since fromlist is None, the result will be the left-most module.
         */
        sky_python_compiler_code_push_const(code, mangled_name);
        sky_python_compiler_code_push_None(code);
        sky_python_compiler_code_push_const(code, sky_integer_zero);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_IMPORT_MODULE);

        dot = SKY_STRING_LITERAL(".");
        i = sky_string_find(alias->name, dot, 0, SSIZE_MAX);

        name = alias->asname;
        if (sky_object_isnull(name)) {
            name = (-1 == i ? alias->name
                            : sky_string_slice(alias->name, 0, i, 1));
        }
        else if (-1 != i) {
            ctx = sky_python_ast_Load;
            while (sky_python_ast_Load == ctx) {
                next_i = sky_string_find(alias->name, dot, i + 1, SSIZE_MAX);
                if (-1 == next_i) {
                    next_i = sky_string_len(alias->name);
                    ctx = sky_python_ast_Store;
                }
                attr_name = sky_string_slice(alias->name, i + 1, next_i, 1);
                index = sky_python_compiler_table_get(&(compiler->code->names),
                                                      attr_name);
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_PUSH_ATTRIBUTE,
                                                index);
                i = next_i;
            }
        }

        sky_python_compiler_generate_name(compiler,
                                          name,
                                          sky_python_ast_Store);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_compiler_visit_ImportFrom(sky_python_compiler_t *        compiler,
                                     sky_python_ast_ImportFrom_t    node)
{
    sky_string_t                star = SKY_STRING_LITERAL("*");
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                 i, index, nnames;
    sky_tuple_t             fromlist;
    sky_object_t            *names;
    sky_string_t            mangled_name;
    sky_python_ast_alias_t  alias;

    i = 0;
    nnames = sky_object_len(node->names);
    names = sky_malloc(nnames * sizeof(sky_object_t));
    SKY_SEQUENCE_FOREACH(node->names, alias) {
        names[i++] = alias->name;
    } SKY_SEQUENCE_FOREACH_END;
    fromlist = sky_tuple_createwitharray(nnames, names, sky_free);

    /* __import__(mangled_name, fromlist=None, level=0)
     * The implementation of SKY_CODE_OPCODE_IMPORT_MODULE will drop the
     * appropriate globals/locals from the calling frame into the call.
     * Since fromlist is None, the result will be the left-most module.
     */
    if (sky_object_isnull(node->module)) {
        sky_python_compiler_code_push_const(code, sky_string_empty);
    }
    else {
        mangled_name = sky_python_compiler_mangle_name(compiler->class_name,
                                                       node->module);
        sky_python_compiler_code_push_const(code, mangled_name);
    }
    sky_python_compiler_code_push_const(code, fromlist);
    sky_python_compiler_code_push_const(code, sky_integer_create(node->level));
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_IMPORT_MODULE);

    /* Calling __import__() with a non-None list will leave the right-most
     * module on the stack. Assign the names now.
     */
    SKY_SEQUENCE_FOREACH(node->names, alias) {
        if (sky_object_compare(alias->name, star, SKY_COMPARE_OP_EQUAL)) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_IMPORT_STAR);
        }
        else {
            mangled_name = sky_python_compiler_mangle_name(compiler->class_name,
                                                           alias->name);
            index = sky_python_compiler_table_get(&(code->names), mangled_name);
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_IMPORT_NAME,
                                            index);
            sky_python_compiler_generate_name(
                    compiler,
                    (sky_object_isnull(alias->asname) ? alias->name
                                                      : alias->asname),
                    sky_python_ast_Store);
        }
    } SKY_SEQUENCE_FOREACH_END;

    /* This pop is for the imported module object. */
    sky_python_compiler_block_addop(code->current_block, SKY_CODE_OPCODE_POP);
}


static void
sky_python_compiler_visit_Raise(sky_python_compiler_t * compiler,
                                sky_python_ast_Raise_t  node)
{
    ssize_t popc = 0;

    if (!sky_object_isnull(node->exc)) {
        ++popc;
        sky_python_compiler_visit_expr(compiler, node->exc);
        if (!sky_object_isnull(node->cause)) {
            ++popc;
            sky_python_compiler_visit_expr(compiler, node->cause);
        }
    }
    sky_python_compiler_block_addop(compiler->code->current_block,
                                    SKY_CODE_OPCODE_RAISE,
                                    popc);
}


static void
sky_python_compiler_visit_Return(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Return_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    if (SKY_PYTHON_SYMTABLE_BLOCK_TYPE_FUNCTION != code->symtable->type) {
        sky_python_compiler_raise(compiler,
                                  sky_SyntaxError,
                                  "'return' outside function");
    }

    if (sky_object_isnull(node->value)) {
        sky_python_compiler_code_push_None(code);
    }
    else {
        sky_python_compiler_visit_expr(compiler, node->value);
    }
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_RETURN);
}


static void
sky_python_compiler_visit_ExceptHandler(
        sky_python_compiler_t *         compiler,
        sky_python_ast_ExceptHandler_t  node,
        sky_python_compiler_block_t *   if_exit_block)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_compiler_block_t *if_body_block, *if_orelse_block;

    code->current_block->lineno = node->lineno;
    code->current_block->col_offset = node->col_offset;

    if_body_block = sky_python_compiler_block_create(code);
    if_orelse_block = sky_python_compiler_block_create(code);

    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_PUSH_TOP);
    sky_python_compiler_visit_expr(compiler, node->type);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_COMPARE_EXCEPTION);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP_IF_FALSE,
                                    if_orelse_block);

    sky_python_compiler_block_setflownext(code->current_block, if_body_block);
    code->current_block = if_body_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);

    if (sky_object_isnull(node->name)) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    }
    else {
        ssize_t                     entry_guard_id, finally_enter_op;
        sky_python_compiler_block_t *body_block, *exit_block, *finally_block,
                                    *finally_enter_block;

        /* except type as name:
         *     name = <exception>
         *     try:
         *         # except handler body
         *     finally:
         *         name = None
         *         del name
         *
         *      [current]   try_finally [finally]       finally_guard_id
         *         [body]   << node->body >>            finally_guard_id
         *                  try_exit (exit try_finally)
         *                  jump [exit]
         * ----------------------------------
         *      [finally]   finally_enter               finally_block->guard_id
         *                  push_const (None)
         *                  pop_local (name)
         *                  delete_local (name)
         *                  finally_exit
         * ----------------------------------
         *         [exit]   ...                         entry_guard_id
         */

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_PUSH_TOP);
        sky_python_compiler_generate_name(compiler,
                                          node->name,
                                          sky_python_ast_Store);

        entry_guard_id = code->guard_id;
        code->guard_id = ++code->guard_id_counter;
        finally_block = sky_python_compiler_block_create(code);
        finally_block->guard_id = ++code->guard_id_counter;
        finally_enter_block = code->current_block;
        finally_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_TRY_FINALLY,
                                                finally_block,
                                                code->guard_id);

        exit_block = sky_python_compiler_block_create(code);
        exit_block->guard_id = entry_guard_id;

        body_block = sky_python_compiler_block_create(code);
        sky_python_compiler_block_setflownext(code->current_block,
                                              body_block);
        code->current_block = body_block;

        sky_python_compiler_visit_stmt_sequence(compiler, node->body);

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_TRY_EXIT,
                                        finally_enter_block,
                                        finally_enter_op);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_JUMP,
                                        exit_block);

        code->current_block = finally_block;
        code->guard_id = finally_block->guard_id;
        finally_enter_block = code->current_block;
        finally_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_FINALLY_ENTER,
                                                finally_block->guard_id);
        
        sky_python_compiler_code_push_None(code);
        sky_python_compiler_generate_name(compiler,
                                          node->name,
                                          sky_python_ast_Store);
        sky_python_compiler_generate_name(compiler,
                                          node->name,
                                          sky_python_ast_Del);

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_FINALLY_EXIT,
                                        finally_enter_block,
                                        finally_enter_op);

        code->current_block = exit_block;
        code->guard_id = entry_guard_id;
    }

    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    if_exit_block);

    code->current_block = if_orelse_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
}


static void
sky_python_compiler_visit_excepthandler_sequence(
        sky_python_compiler_t *         compiler,
        sky_object_t                    sequence)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                         i, nhandlers;
    sky_python_compiler_block_t     *exit_block;
    sky_python_ast_ExceptHandler_t  handler;

    nhandlers = sky_object_len(sequence) - 1;
    handler = sky_sequence_get(sequence, nhandlers);
    if (!nhandlers && sky_object_isnull(handler->type)) {
        sky_python_compiler_visit_stmt_sequence(compiler, handler->body);
        return;
    }

    exit_block = sky_python_compiler_block_create(code);

    for (i = 0; i < nhandlers; ++i) {
        handler = sky_sequence_get(sequence, i);
        if (sky_object_isnull(handler->type)) {
            sky_python_compiler_raise(compiler,
                                      sky_SyntaxError,
                                      "default 'except:' must be last");
        }
        sky_python_compiler_visit_ExceptHandler(compiler, handler, exit_block);
    }

    handler = sky_sequence_get(sequence, nhandlers);
    if (!sky_object_isnull(handler->type)) {
        sky_python_compiler_visit_ExceptHandler(compiler, handler, exit_block);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_RAISE,
                                        0);
    }
    else {
        sky_python_compiler_visit_stmt_sequence(compiler, handler->body);
    }

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
}


static void
sky_python_compiler_visit_Try(sky_python_compiler_t *   compiler,
                              sky_python_ast_Try_t      node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                     entry_guard_id, except_enter_op,
                                finally_enter_op, orelse_guard_id;
    sky_python_compiler_block_t *block, *body_block, *except_block,
                                *except_enter_block, *exit_block,
                                *finally_block, *finally_enter_block,
                                *orelse_block;

    /* 1. try..finally
     *
     *      [current]   try_finally [finally]       finally_guard_id
     *         [body]   << node->body >>            finally_guard_id
     *                  try_exit (exit try_finally)
     *                  jump [exit]
     * ----------------------------------
     *      [finally]   finally_enter               finally_block->guard_id
     *                  << node->finalbody >>
     *                  finally_exit
     * ----------------------------------
     *         [exit]   ...                         entry_guard_id
     *
     * 2. try..except..else
     *
     *      [current]   try_except [except]         except_guard_id
     *         [body]   << node->body >>            except_guard_id
     *                  try_exit (exit try_except)
     *                  jump [orelse] or [exit]
     * ----------------------------------
     *       [except]   except_enter                except_block->guard_id
     *                  << node->handlers >>
     *                  except_exit
     *                  jump [exit]
     * ----------------------------------
     *       [orelse]   << node->orelse >>          entry_guard_id
     *         [exit]   ...                         entry_guard_id
     *
     * 3. try..except..else..finally
     *
     *      [current]   try_finally [finally]       finally_guard_id
     *           [_1]   try_except [except]         except_guard_id
     *         [body]   << node->body >>            except_guard_id
     *                  try_exit (exit try_except)
     *                  jump [orelse]
     * ----------------------------------
     *       [except]   except_enter                except_block->guard_id
     *                  << node->handlers >>
     *                  except_exit
     *                  jump [exit]
     * ----------------------------------
     *       [orelse]   << node->orelse >>          finally_guard_id
     *                  try_exit (exit try_finally)
     *                  jump [exit]
     * ----------------------------------
     *      [finally]   finally_enter               finally_block->guard_id
     *                  << node->finalbody >>
     *                  finally_exit
     * ----------------------------------
     *         [exit]   ...                         entry_guard_id
     */

    entry_guard_id = code->guard_id;
    except_block = finally_block = orelse_block = NULL;

    if (!sky_python_compiler_is_emptybody(node->finalbody)) {
        code->guard_id = ++code->guard_id_counter;
        finally_block = sky_python_compiler_block_create(code);
        finally_block->guard_id = ++code->guard_id_counter;
        finally_enter_block = code->current_block;
        finally_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_TRY_FINALLY,
                                                finally_block,
                                                code->guard_id);
    }
    if (sky_object_bool(node->handlers)) {
        orelse_guard_id = code->guard_id;
        code->guard_id = ++code->guard_id_counter;
        if (finally_block) {
            block = sky_python_compiler_block_create(code);
            sky_python_compiler_block_setflownext(code->current_block, block);
            code->current_block = block;
        }
        if (finally_block || !sky_python_compiler_is_emptybody(node->orelse)) {
            orelse_block = sky_python_compiler_block_create(code);
            orelse_block->guard_id = orelse_guard_id;
        }
        except_block = sky_python_compiler_block_create(code);
        except_block->guard_id = ++code->guard_id_counter;
        except_enter_block = code->current_block;
        except_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_TRY_EXCEPT,
                                                except_block,
                                                orelse_block,
                                                code->guard_id);
    }

    /* This is try..finally with an empty finally body */
    if (!finally_block && !except_block) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->body);
        return;
    }

    exit_block = sky_python_compiler_block_create(code);
    exit_block->guard_id = entry_guard_id;

    body_block = sky_python_compiler_block_create(code);
    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;
    sky_python_compiler_visit_stmt_sequence(compiler, node->body);

    if (except_block) {
        sky_python_compiler_loop_t  loop;

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_TRY_EXIT,
                                        except_enter_block,
                                        except_enter_op);
        if (orelse_block) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_JUMP,
                                            orelse_block);
        }
        else {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_JUMP,
                                            exit_block);
        }

        loop.parent = code->current_loop;
        loop.break_block = NULL;
        loop.continue_block = NULL;
        loop.pop_stack = 1;

        code->current_block = except_block;
        code->guard_id = except_block->guard_id;
        except_enter_block = code->current_block;
        except_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_EXCEPT_ENTER,
                                                except_block->guard_id);

        code->current_loop = &loop;
        sky_python_compiler_visit_excepthandler_sequence(compiler,
                                                         node->handlers);
        code->current_loop = loop.parent;

        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_POP);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_EXCEPT_EXIT,
                                        except_enter_block,
                                        except_enter_op);
        if (!orelse_block) {
            sky_python_compiler_block_setflownext(code->current_block,
                                                  exit_block);
        }
        else {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_JUMP,
                                            exit_block);

            code->current_block = orelse_block;
            code->guard_id = orelse_block->guard_id;
            sky_python_compiler_visit_stmt_sequence(compiler, node->orelse);
            if (finally_block) {
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_TRY_EXIT,
                                                finally_enter_block,
                                                finally_enter_op);
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_JUMP,
                                                exit_block);
            }
            else {
                sky_python_compiler_block_setflownext(code->current_block,
                                                      exit_block);
            }
        }
    }
    else if (finally_block) {
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_TRY_EXIT,
                                        finally_enter_block,
                                        finally_enter_op);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_JUMP,
                                        exit_block);
    }
    else {
        /* Must have either except: and/or finally: */
        sky_error_fatal("internal error");
    }

    if (finally_block) {
        code->current_block = finally_block;
        code->guard_id = finally_block->guard_id;
        finally_enter_block = code->current_block;
        finally_enter_op =
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_FINALLY_ENTER,
                                                finally_block->guard_id);
        sky_python_compiler_visit_stmt_sequence(compiler, node->finalbody);
        sky_python_compiler_block_addop(code->current_block,
                                        SKY_CODE_OPCODE_FINALLY_EXIT,
                                        finally_enter_block,
                                        finally_enter_op);
    }

    code->current_block = exit_block;
    code->guard_id = entry_guard_id;
}


static void
sky_python_compiler_visit_While(sky_python_compiler_t * compiler,
                                sky_python_ast_While_t  node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    sky_python_compiler_loop_t  loop;
    sky_python_compiler_block_t *body_block, *loop_block, *orelse_block,
                                *exit_block;

    loop_block = sky_python_compiler_block_create(code);
    body_block = sky_python_compiler_block_create(code);
    orelse_block = sky_python_compiler_block_create(code);
    exit_block = sky_python_compiler_block_create(code);

    loop.parent = code->current_loop;
    loop.break_block = exit_block;
    loop.continue_block = loop_block;
    loop.pop_stack = 0;
    code->current_loop = &loop;

    sky_python_compiler_block_setflownext(code->current_block, loop_block);
    code->current_block = loop_block;

    sky_python_compiler_visit_expr(compiler, node->test);
    sky_python_compiler_block_addop(
            code->current_block,
            SKY_CODE_OPCODE_JUMP_IF_FALSE,
            (sky_object_isnull(node->orelse) ? exit_block : orelse_block));

    sky_python_compiler_block_setflownext(code->current_block, body_block);
    code->current_block = body_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    sky_python_compiler_visit_stmt_sequence(compiler, node->body);
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_JUMP,
                                    loop_block);

    code->current_loop = loop.parent;
    code->current_block = orelse_block;
    sky_python_compiler_block_addop(code->current_block,
                                    SKY_CODE_OPCODE_POP);
    if (sky_object_bool(node->orelse)) {
        sky_python_compiler_visit_stmt_sequence(compiler, node->orelse);
    }

    sky_python_compiler_block_setflownext(code->current_block, exit_block);
    code->current_block = exit_block;
}


typedef struct sky_python_compiler_with_block_s {
    ssize_t                             guard_id;
    sky_python_compiler_block_t *       enter_block;
    ssize_t                             enter_op_index;
    sky_python_compiler_block_t *       body_block;
    sky_python_compiler_block_t *       exit_block;
} sky_python_compiler_with_block_t;


static void
sky_python_compiler_visit_With(sky_python_compiler_t *  compiler,
                               sky_python_ast_With_t    node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                             i, nitems;
    sky_python_ast_withitem_t           withitem;
    sky_python_compiler_with_block_t    *blocks;

    SKY_ASSET_BLOCK_BEGIN {
        nitems = sky_object_len(node->items);
        blocks = sky_asset_calloc(nitems,
                                  sizeof(sky_python_compiler_with_block_t),
                                  SKY_ASSET_CLEANUP_ALWAYS);

        for (i = 0; i < nitems; ++i) {
            withitem = sky_sequence_get(node->items, i);

            sky_python_compiler_visit_expr(compiler, withitem->context_expr);

            blocks[i].guard_id = code->guard_id;
            blocks[i].exit_block = sky_python_compiler_block_create(code);

            code->guard_id = ++code->guard_id_counter;
            blocks[i].body_block = sky_python_compiler_block_create(code);

            blocks[i].enter_block = code->current_block;
            blocks[i].enter_op_index =
                    sky_python_compiler_block_addop(code->current_block,
                                                    SKY_CODE_OPCODE_WITH_ENTER,
                                                    blocks[i].exit_block,
                                                    code->guard_id);

            sky_python_compiler_block_setflownext(code->current_block,
                                                  blocks[i].body_block);
            code->current_block = blocks[i].body_block;
            if (sky_object_isnull(withitem->optional_vars)) {
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_POP);
            }
            else {
                sky_python_compiler_visit_expr(compiler,
                                               withitem->optional_vars);
            }
        }

        sky_python_compiler_visit_stmt_sequence(compiler, node->body);

        for (i = nitems - 1; i >= 0; --i) {
            sky_python_compiler_block_addop(code->current_block,
                                            SKY_CODE_OPCODE_WITH_EXIT,
                                            blocks[i].enter_block,
                                            blocks[i].enter_op_index);

            sky_python_compiler_block_setflownext(code->current_block,
                                                  blocks[i].exit_block);
            code->current_block = blocks[i].exit_block;
            code->guard_id = blocks[i].guard_id;
        }
    } SKY_ASSET_BLOCK_END;
}


static void
sky_python_compiler_visit_stmt(sky_python_compiler_t *  compiler,
                               sky_object_t             node)
{
    sky_python_compiler_code_t  *code = compiler->code;

    ssize_t                     i;
    sky_python_compiler_loop_t  *loop;

    if (!sky_object_isa(node, sky_python_ast_stmt_type)) {
        sky_error_raise_format(sky_TypeError,
                               "malformed AST: expected stmt; got %#@",
                               sky_type_name(sky_object_type(node)));
    }

    code->current_block->lineno = ((sky_python_ast_stmt_t)node)->lineno;
    code->current_block->col_offset = ((sky_python_ast_stmt_t)node)->col_offset;

    if (sky_object_isa(node, sky_python_ast_FunctionDef_type)) {
        sky_python_compiler_visit_FunctionDef(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ClassDef_type)) {
        sky_python_compiler_visit_ClassDef(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Return_type)) {
        sky_python_compiler_visit_Return(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Delete_type)) {
        sky_python_compiler_visit_expr_sequence(
                compiler,
                ((sky_python_ast_Delete_t)node)->targets);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Assign_type)) {
        sky_python_compiler_visit_Assign(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_AugAssign_type)) {
        sky_python_compiler_visit_AugAssign(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_For_type)) {
        sky_python_compiler_visit_For(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_While_type)) {
        sky_python_compiler_visit_While(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_If_type)) {
        sky_python_compiler_visit_If(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_With_type)) {
        sky_python_compiler_visit_With(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Raise_type)) {
        sky_python_compiler_visit_Raise(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Try_type)) {
        sky_python_compiler_visit_Try(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Assert_type)) {
        sky_python_compiler_visit_Assert(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Import_type)) {
        sky_python_compiler_visit_Import(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_ImportFrom_type)) {
        sky_python_compiler_visit_ImportFrom(compiler, node);
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Expr_type)) {
        sky_python_compiler_visit_expr(
                compiler,
                ((sky_python_ast_Expr_t)node)->value);
        if (SKY_PYTHON_COMPILER_MODE_INTERACTIVE == compiler->mode &&
            !code->parent)
        {
            sky_python_compiler_block_addop(
                    code->current_block,
                    SKY_CODE_OPCODE_DISPLAY);
        }
        else {
            sky_python_compiler_block_addop(
                    code->current_block,
                    SKY_CODE_OPCODE_POP);
        }
        return;
    }
    if (sky_object_isa(node, sky_python_ast_Break_type)) {
        for (loop = code->current_loop; loop; loop = loop->parent) {
            if (loop->break_block) {
                sky_python_compiler_block_addop(
                        code->current_block,
                        SKY_CODE_OPCODE_JUMP,
                        loop->break_block);
                return;
            }
            for (i = 0; i < loop->pop_stack; ++i) {
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_POP);
            }
        }
        sky_python_compiler_raise(compiler,
                                  sky_SyntaxError,
                                  "'break' outside loop");
    }
    if (sky_object_isa(node, sky_python_ast_Continue_type)) {
        for (loop = code->current_loop; loop; loop = loop->parent) {
            if (loop->continue_block) {
                sky_python_compiler_block_addop(
                        code->current_block,
                        SKY_CODE_OPCODE_JUMP,
                        loop->continue_block);
                return;
            }
            for (i = 0; i < loop->pop_stack; ++i) {
                sky_python_compiler_block_addop(code->current_block,
                                                SKY_CODE_OPCODE_POP);
            }
        }
        sky_python_compiler_raise(compiler,
                                  sky_SyntaxError,
                                  "'continue' not properly in loop");
    }

    if (sky_object_isa(node, sky_python_ast_Global_type) ||
        sky_object_isa(node, sky_python_ast_Nonlocal_type) ||
        sky_object_isa(node, sky_python_ast_Pass_type))
    {
        return;
    }
    sky_error_fatal("internal error");
}


static void
sky_python_compiler_visit_Module(sky_python_compiler_t *    compiler,
                                 sky_python_ast_Module_t    node)
{
    ssize_t         i, len;
    sky_object_t    body, docstr, stmt, value;

    body = node->body;

    i = 0;
    docstr = sky_None;
    if (sky_object_bool(body)) {
        stmt = sky_sequence_get(body, 0);
        if (sky_object_isa(stmt, sky_python_ast_Expr_type)) {
            value = ((sky_python_ast_Expr_t)stmt)->value;
            if (sky_object_isa(value, sky_python_ast_Str_type)) {
                ++i;
                if (compiler->optlevel < 2) {
                    docstr = ((sky_python_ast_Str_t)value)->s;
                }
            }
        }
    }

    if (!i) {
        sky_python_compiler_visit_stmt_sequence(compiler, body);
    }
    else {
        /* docstr can be None if doc strings are optimized away. */
        if (!sky_object_isnull(docstr)) {
            sky_python_compiler_code_push_const(compiler->code, docstr);
            sky_python_compiler_generate_name(compiler,
                                              SKY_STRING_LITERAL("__doc__"),
                                              sky_python_ast_Store);
        }
        for (len = sky_object_len(body); i < len; ++i) {
            stmt = sky_sequence_get(body, i);
            sky_python_compiler_visit_stmt(compiler, stmt);
        }
    }
}


static void
sky_python_compiler_cleanup(sky_python_compiler_t *compiler)
{
    sky_python_compiler_code_t  *code;

    while ((code = compiler->code) != NULL) {
        compiler->code = compiler->code->parent;
        sky_python_compiler_code_destroy(code);
    }
}


sky_code_t
sky_python_compiler_compile(sky_object_t                ast,
                            sky_string_t                filename,
                            sky_python_compiler_mode_t  mode,
                            unsigned int                flags,
                            int                         optlevel)
{
    sky_code_t              code_object;
    sky_object_t            body, stmt;
    sky_python_compiler_t   compiler;

    SKY_ASSET_BLOCK_BEGIN {
        memset(&compiler, 0, sizeof(compiler));
        sky_asset_save(&compiler,
                       (sky_free_t)sky_python_compiler_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        compiler.filename = (filename ? filename : SKY_STRING_LITERAL("???"));
        compiler.symtable = sky_python_symtable_create(ast, compiler.filename);
        compiler.flags = flags;
        if ((compiler.optlevel = optlevel) < 0) {
            compiler.optlevel = sky_interpreter_optlevel();
        }

        sky_python_compiler_code_push(&compiler,
                                      ast,
                                      SKY_STRING_LITERAL("<module>"));

        compiler.mode = mode;
        switch (mode) {
            case SKY_PYTHON_COMPILER_MODE_FILE:
                if (!sky_object_isa(ast, sky_python_ast_Module_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "expected Module node, got %@",
                            sky_type_name(sky_object_type(ast)));
                }
                sky_python_compiler_visit_Module(&compiler, ast);
                break;

            case SKY_PYTHON_COMPILER_MODE_EVAL:
                if (!sky_object_isa(ast, sky_python_ast_Expression_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "expected Expression node, got %@",
                            sky_type_name(sky_object_type(ast)));
                }
                body = ((sky_python_ast_Expression_t)ast)->body;
                sky_python_compiler_visit_expr(&compiler, body);
                break;

            case SKY_PYTHON_COMPILER_MODE_INTERACTIVE:
                if (!sky_object_isa(ast, sky_python_ast_Interactive_type)) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "expected Interactive node, got %@",
                            sky_type_name(sky_object_type(ast)));
                }
                body = ((sky_python_ast_Interactive_t)ast)->body;
                SKY_SEQUENCE_FOREACH(body, stmt) {
                    sky_python_compiler_visit_stmt(&compiler, stmt);
                } SKY_SEQUENCE_FOREACH_END;
                break;

            default:
                sky_error_fatal("internal error");
        }

        code_object = sky_python_compiler_code_assemble(&compiler);
        sky_python_compiler_code_pop(&compiler);
    } SKY_ASSET_BLOCK_END;

    return code_object;
}


sky_string_t
sky_python_compiler_line(sky_string_t filename, int lineno)
{
    sky_object_t    file, line;

    if (!sky_object_isa(filename, sky_string_type) ||
        sky_string_startswith(filename, SKY_STRING_LITERAL("<"), 0, SSIZE_MAX) ||
        lineno <= 0)
    {
        return NULL;
    }

    SKY_ERROR_TRY {
        file = sky_file_open(filename,
                             NULL,
                             -1,
                             NULL,
                             NULL,
                             NULL,
                             SKY_TRUE,
                             NULL);
        sky_asset_save(file, sky_file_close, SKY_ASSET_CLEANUP_ALWAYS);

        while (lineno-- > 0) {
            line = sky_file_readline(file, -1);
            if (!sky_object_bool(line)) {
                line = NULL;
                break;
            }
        }

        if (line && !sky_object_isa(line, sky_string_type)) {
            line = sky_codec_decode(NULL,
                                    NULL,
                                    SKY_STRING_LITERAL("replace"),
                                    NULL,
                                    line);
        }
    } SKY_ERROR_EXCEPT_ANY {
        line = NULL;
    } SKY_ERROR_TRY_END;

    return line;
}


sky_string_t
sky_python_compiler_mangle_name(sky_string_t    class_name,
                                sky_string_t    name)
{
    ssize_t                 mangled_name_len;
    sky_string_t            underscores;
    sky_string_builder_t    mangled_name;

    if (!class_name) {
        return name;
    }

    underscores = SKY_STRING_LITERAL("__");
    if (!sky_string_startswith(name, underscores, 0, SSIZE_MAX) ||
        sky_string_endswith(name, underscores, 0, SSIZE_MAX) ||
        sky_object_contains(name, SKY_STRING_LITERAL(".")))
    {
        return name;
    }

    /* Strip leading underscores from the class name. If the stripped name is
     * then empty, there is no mangling to be done.
     * Ref. Python Language Specification 6.2.1. Identifiers (Names)
     */
    class_name = sky_string_lstrip(class_name, SKY_STRING_LITERAL("_"));

    mangled_name_len = sky_string_len(class_name) + sky_string_len(name) + 1;
    mangled_name = sky_string_builder_createwithcapacity(mangled_name_len);
    sky_string_builder_appendcodepoint(mangled_name, '_', 1);
    sky_string_builder_append(mangled_name, class_name);
    sky_string_builder_append(mangled_name, name);

    return sky_string_builder_finalize(mangled_name);
}
