#include "sky_private.h"
#include "sky_exceptions_private.h"


static void
sky_BaseException_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_BaseException_data_t    *exception_data = data;

    sky_object_visit(exception_data->traceback, visit_data);
    sky_object_visit(exception_data->cause, visit_data);
    sky_object_visit(exception_data->context, visit_data);
    sky_object_visit(exception_data->args, visit_data);
}

static void
sky_ImportError_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_ImportError_data_t  *exception_data = data;

    sky_object_visit(exception_data->msg, visit_data);
    sky_object_visit(exception_data->name, visit_data);
    sky_object_visit(exception_data->path, visit_data);
}

static void
sky_OSError_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_OSError_data_t  *exception_data = data;

    sky_object_visit(exception_data->oserror, visit_data);
    sky_object_visit(exception_data->strerror, visit_data);
    sky_object_visit(exception_data->filename, visit_data);
#if defined(_WIN32)
    sky_object_visit(exception_data->winerror, visit_data);
#endif
}

static void
sky_StopIteration_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_StopIteration_data_t    *exception_data = data;

    sky_object_visit(exception_data->value, visit_data);
}

static void
sky_SyntaxError_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_SyntaxError_data_t  *exception_data = data;

    sky_object_visit(exception_data->msg, visit_data);
    sky_object_visit(exception_data->filename, visit_data);
    sky_object_visit(exception_data->lineno, visit_data);
    sky_object_visit(exception_data->offset, visit_data);
    sky_object_visit(exception_data->text, visit_data);
    sky_object_visit(exception_data->print_file_and_line, visit_data);
}

static void
sky_SystemExit_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_SystemExit_data_t   *exception_data = data;

    sky_object_visit(exception_data->code, visit_data);
}

static void
sky_UnicodeError_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_UnicodeError_data_t *exception_data = data;

    sky_object_visit(exception_data->encoding, visit_data);
    sky_object_visit(exception_data->object, visit_data);
    sky_object_visit(exception_data->reason, visit_data);
}


SKY_EXCEPTION_DEFINE(BaseException,
                     sizeof(sky_BaseException_data_t),
                     NULL,
                     NULL,
                     sky_BaseException_instance_visit,
                     0,
                     "Common base class for all exceptions");

SKY_EXCEPTION_DEFINE(SystemExit,
                     sizeof(sky_SystemExit_data_t),
                     NULL,
                     NULL,
                     sky_SystemExit_instance_visit,
                     0,
                     "Request to exit from the interpreter.");
SKY_EXCEPTION_DEFINE_TRIVIAL(KeyboardInterrupt,
        "Program interrupted by user.");
SKY_EXCEPTION_DEFINE_TRIVIAL(GeneratorExit,
        "Request that a generator exit.");
SKY_EXCEPTION_DEFINE_TRIVIAL(Exception,
        "Common base class for all non-exit exceptions");

SKY_EXCEPTION_DEFINE_TRIVIAL(ArithmeticError,
        "Base class for arithmetic errors.");
SKY_EXCEPTION_DEFINE_TRIVIAL(FloatingPointError,
        "Floating point operation failed.");
SKY_EXCEPTION_DEFINE_TRIVIAL(OverflowError,
        "Result too large to be represented.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ZeroDivisionError,
        "Second argument to a division or modulo operation was zero.");

SKY_EXCEPTION_DEFINE_TRIVIAL(AssertionError,
        "Assertion failed.");
SKY_EXCEPTION_DEFINE_TRIVIAL(AttributeError,
        "Attribute not found.");
SKY_EXCEPTION_DEFINE_TRIVIAL(BufferError,
        "Buffer error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(EOFError,
        "Read beyond end of file.");
SKY_EXCEPTION_DEFINE(ImportError,
                     sizeof(sky_ImportError_data_t),
                     NULL,
                     NULL,
                     sky_ImportError_instance_visit,
                     0,
                     "Import can't find module, or can't find name in module.");
SKY_EXCEPTION_DEFINE_TRIVIAL(LookupError,
        "Base class for lookup errors.");
SKY_EXCEPTION_DEFINE_TRIVIAL(IndexError,
        "Sequence index out of range.");
SKY_EXCEPTION_DEFINE_TRIVIAL(KeyError,
        "Mapping key not found.");
SKY_EXCEPTION_DEFINE_TRIVIAL(MemoryError,
        "Out of memory.");
SKY_EXCEPTION_DEFINE_TRIVIAL(NameError,
        "Name not found globally.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UnboundLocalError,
        "Local name referenced but not bound to a value.");

SKY_EXCEPTION_DEFINE_TRIVIAL(ReferenceError,
        "Weak ref proxy used after referent went away.");
SKY_EXCEPTION_DEFINE_TRIVIAL(RuntimeError,
        "Unspecified run-time error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(NotImplementedError,
        "Method or function hasn't been implemented yet.");
SKY_EXCEPTION_DEFINE(StopIteration,
                     sizeof(sky_StopIteration_data_t),
                     NULL,
                     NULL,
                     sky_StopIteration_instance_visit,
                     0,
                     "Signal the end from iterator.__next__().");
SKY_EXCEPTION_DEFINE(SyntaxError,
                     sizeof(sky_SyntaxError_data_t),
                     NULL,
                     NULL,
                     sky_SyntaxError_instance_visit,
                     0,
                     "Invalid syntax.");
SKY_EXCEPTION_DEFINE_TRIVIAL(IndentationError,
        "Improper indentation.");
SKY_EXCEPTION_DEFINE_TRIVIAL(TabError,
        "Improper mixture of spaces and tabs.");
SKY_EXCEPTION_DEFINE_TRIVIAL(SystemError,
        "Internal error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(TypeError,
        "Inappropriate argument type.");

SKY_EXCEPTION_DEFINE_TRIVIAL(ValueError,
        "Inappropriate argument value (of correct type).");
SKY_EXCEPTION_DEFINE(UnicodeError,
                     sizeof(sky_UnicodeError_data_t),
                     NULL,
                     NULL,
                     sky_UnicodeError_instance_visit,
                     0,
                     "Unicode related error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UnicodeEncodeError,
        "Unicode encoding error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UnicodeDecodeError,
        "Unicode decoding error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UnicodeTranslateError,
        "Unicode translation error.");

SKY_EXCEPTION_DEFINE(OSError,
                     sizeof(sky_OSError_data_t),
                     NULL,
                     NULL,
                     sky_OSError_instance_visit,
                     0,
                     "Base class for I/O related errors.");
SKY_EXCEPTION_DEFINE_TRIVIAL(BlockingIOError,
        "I/O operation would block.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ChildProcessError,
        "Child process error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ConnectionError,
        "Connection error.");
SKY_EXCEPTION_DEFINE_TRIVIAL(BrokenPipeError,
        "Broken pipe.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ConnectionAbortedError,
        "Connection aborted.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ConnectionRefusedError,
        "Connection refused.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ConnectionResetError,
        "Connection reset.");
SKY_EXCEPTION_DEFINE_TRIVIAL(FileExistsError,
        "File already exists.");
SKY_EXCEPTION_DEFINE_TRIVIAL(FileNotFoundError,
        "File not found.");
SKY_EXCEPTION_DEFINE_TRIVIAL(InterruptedError,
        "Interrupted by signal.");
SKY_EXCEPTION_DEFINE_TRIVIAL(IsADirectoryError,
        "Operation doesn't work on directories.");
SKY_EXCEPTION_DEFINE_TRIVIAL(NotADirectoryError,
        "Operation only works on directories.");
SKY_EXCEPTION_DEFINE_TRIVIAL(PermissionError,
        "Not enough permissions.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ProcessLookupError,
        "Process not found.");
SKY_EXCEPTION_DEFINE_TRIVIAL(TimeoutError,
        "Timeout expired.");

SKY_EXCEPTION_DEFINE_TRIVIAL(Warning,
        "Base class for warning categories.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UserWarning,
        "Base class for warnings generated by user code.");
SKY_EXCEPTION_DEFINE_TRIVIAL(DeprecationWarning,
        "Base class for warnings about deprecated features.");
SKY_EXCEPTION_DEFINE_TRIVIAL(PendingDeprecationWarning,
        "Base class for warnings about features which will be deprecated\n"
        "in the future.");
SKY_EXCEPTION_DEFINE_TRIVIAL(SyntaxWarning,
        "Base class for warnings about dubious syntax.");
SKY_EXCEPTION_DEFINE_TRIVIAL(RuntimeWarning,
        "Base class for warnings about dubious runtime behavior.");
SKY_EXCEPTION_DEFINE_TRIVIAL(FutureWarning,
        "Base class for warnings about constructs that will change semantically\n"
        "in the future.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ImportWarning,
        "Base class for warnings about probably mistakes in module imports.");
SKY_EXCEPTION_DEFINE_TRIVIAL(UnicodeWarning,
        "Base class for warnings about Unicode related problems, mostly\n"
        "related to conversion problems.");
SKY_EXCEPTION_DEFINE_TRIVIAL(BytesWarning,
        "Base class for warnings about bytes and buffer related problems, mostly\n"
        "related to conversion from str or comparing to str.");
SKY_EXCEPTION_DEFINE_TRIVIAL(ResourceWarning,
        "Base class for warnings about resource usage.");


/* CPython defines __new__() and __init__() for BaseException (well, as much as
 * it sort of can given its builtin object implementation). Both set self.args
 * from args. Only __init__() checks for keyword arguments.
 * Originally, Skython only defined __init__() to do all of this since defining
 * __new__() is a pain and there's really no good reason to do it ... except to
 * maintain compatibility with CPython. Skython will fail unit tests if it
 * doesn't match the behavior.
 */

void
sky_BaseException_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, *args) */
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (sky_object_bool(kws)) {
        sky_error_raise_format(sky_TypeError,
                               "%@ does not take keyword arguments",
                               sky_type_name(sky_object_type(self)));
    }

    sky_object_gc_set(&(exception_data->args), args, self);
}


sky_object_t
sky_BaseException_new(
                   sky_object_t cls,
                   sky_tuple_t  args,
        SKY_UNUSED sky_dict_t   kws)
{
    sky_object_t                self;
    sky_BaseException_data_t    *self_data;

    self = sky_object_allocate(cls);
    self_data = sky_BaseException_data(self);

    sky_object_gc_set(&(self_data->args),
                      (args ? args : sky_tuple_empty),
                      self);

    return self;
}


sky_object_t
sky_BaseException_reduce(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);
    sky_type_data_t             *type_data = sky_type_data(sky_object_type(self));

    sky_object_t    args, dict = NULL;

    args = (exception_data->args ? exception_data->args : sky_tuple_empty);
    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        dict = *(sky_dict_t *)((char *)self + type_data->dict_offset);
    }

    return sky_tuple_pack((dict ? 3 : 2), sky_object_type(self), args, dict);
}


sky_string_t
sky_BaseException_repr(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    sky_tuple_t     args;
    sky_string_t    name;

    name = sky_type_name(sky_object_type(self));
    args = (sky_object_isnull(exception_data->args) ? sky_tuple_empty
                                                    : exception_data->args);
    return sky_string_createfromformat("%@%#@", name, args);
}


void
sky_BaseException_setstate(sky_object_t self, sky_object_t state)
{
    sky_object_t    iterator, key;

    if (state != sky_None) {
        if (!sky_object_isa(state, sky_dict_type)) {
            sky_error_raise_string(sky_TypeError, "state is not a dictionary");
        }

        iterator = sky_object_iter(state);
        while ((key = sky_object_next(iterator, NULL)) != NULL) {
            sky_object_setattr(self, key, sky_dict_getitem(state, key));
        }
    }
}


sky_string_t
sky_BaseException_str(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (!exception_data->args || !sky_tuple_len(exception_data->args)) {
        return sky_string_empty;
    }
    if (sky_tuple_len(exception_data->args) == 1) {
        return sky_object_str(sky_tuple_get(exception_data->args, 0));
    }
    return sky_object_str(exception_data->args);
}


sky_object_t
sky_BaseException_with_traceback(sky_object_t self, sky_object_t tb)
{
    sky_object_setattr(self, SKY_STRING_LITERAL("__traceback__"), tb);
    return self;
}


static sky_object_t
sky_BaseException_args_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    return (exception_data->args ? exception_data->args : sky_tuple_empty);
}


static void
sky_BaseException_args_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (!value) {
        sky_error_raise_string(sky_TypeError,
                               "__args__ may not be deleted");
    }
    sky_object_gc_set(&(exception_data->args), sky_tuple_create(value), self);
}


sky_object_t
sky_BaseException_cause(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    return (exception_data->cause ? exception_data->cause : sky_None);
}


sky_object_t
sky_BaseException_cause_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_BaseException_cause(self);
}


static void
sky_BaseException_cause_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_BaseException_setcause(self, value);
}


sky_object_t
sky_BaseException_context(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    return (exception_data->context ? exception_data->context : sky_None);
}


void
sky_BaseException_setcause(sky_object_t self, sky_object_t value)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (!value) {
        sky_error_raise_string(sky_TypeError,
                               "__cause__ may not be deleted");
    }
    if (sky_object_isnull(value)) {
        value = NULL;
    }
    if (value && !sky_object_isa(value, sky_BaseException)) {
        sky_error_raise_string(
                sky_TypeError,
                "exception cause must be None or derive from BaseException");
    }
    sky_object_gc_set(&(exception_data->cause), value, self);
    exception_data->suppress_context = SKY_TRUE;
}


void
sky_BaseException_setcontext(sky_object_t self, sky_object_t value)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (!value) {
        sky_error_raise_string(sky_TypeError,
                               "__context__ may not be deleted");
    }
    if (sky_None == value) {
        exception_data->context = NULL;
    }
    else if (!sky_object_isa(value, sky_BaseException)) {
        sky_error_raise_string(
                sky_TypeError,
                "exception context must be None or derive from BaseException");
    }
    else {
        sky_object_gc_set(&(exception_data->context), value, self);
    }
}


void
sky_BaseException_settraceback(sky_object_t     self,
                               sky_traceback_t  traceback)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    if (sky_object_isnull(traceback)) {
        traceback = NULL;
    }
    if (traceback && !sky_object_isa(traceback, sky_traceback_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_traceback_type),
                sky_type_name(sky_object_type(traceback)));
    }
    else {
        sky_object_gc_set(&(exception_data->traceback), traceback, self);
    }
}


sky_bool_t
sky_BaseException_suppress_context(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    return exception_data->suppress_context;
}


sky_traceback_t
sky_BaseException_traceback(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    return (exception_data->traceback ? exception_data->traceback : sky_None);
}


void
sky_ImportError_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, *args, name=None, path=None) */
{
    sky_ImportError_data_t  *exception_data = sky_ImportError_data(self);

    if (kws) {
        sky_object_t    key, value;

        key = SKY_STRING_LITERAL("name");
        if ((value = sky_dict_get(kws, key, NULL)) != NULL) {
            sky_object_gc_set(&(exception_data->name), value, self);
            sky_dict_delete(kws, key);
        }
        key = SKY_STRING_LITERAL("path");
        if ((value = sky_dict_get(kws, key, NULL)) != NULL) {
            sky_object_gc_set(&(exception_data->path), value, self);
            sky_dict_delete(kws, key);
        }
    }

    sky_BaseException_init(self, args, kws);
    if (args && sky_tuple_len(args) == 1) {
        sky_object_gc_set(&(exception_data->msg), sky_tuple_get(args, 0), self);
    }
}


sky_string_t
sky_ImportError_str(sky_object_t self)
{
    sky_ImportError_data_t  *exception_data = sky_ImportError_data(self);

    if (exception_data->msg) {
        return sky_object_str(exception_data->msg);
    }
    return sky_BaseException_str(self);
}


sky_string_t
sky_KeyError_str(sky_object_t self)
{
    sky_BaseException_data_t    *exception_data = sky_BaseException_data(self);

    /* CPython 3.3 says:
     * If args is a tuple of exactly one item, apply repr to args[0].
     * This is done so that e.g. the exception raised by {}[''] prints
     *   KeyError: ''
     * rather than the confusing
     *   KeyError
     * alone. The downside is that if KeyError is raised with an explanatory
     * string, that string will be displayed in quotes. Too bad.
     * If args is anything else, use the default BaseException.__str__().
     */
    if (exception_data->args && sky_tuple_len(exception_data->args) == 1) {
        return sky_object_repr(sky_tuple_get(exception_data->args, 0));
    }
    return sky_BaseException_str(self);
}


int
sky_OSError_errno(sky_object_t self)
{
    sky_OSError_data_t  *self_data = sky_OSError_data(self);

    if (sky_object_isnull(self_data->oserror)) {
        return 0;
    }
    return sky_integer_value(self_data->oserror, INT_MIN, INT_MAX, NULL);
}


static sky_tuple_t
sky_OSError_parse_arguments(sky_object_t    cls,
                            sky_tuple_t     args,
                            sky_dict_t      kws,
                            sky_object_t *  oserror,
                            sky_object_t *  strerror,
                            sky_object_t *  filename,
                            sky_object_t *  winerror)
{
    ssize_t nargs;

    /* This is ugly, but for compatibility with CPython it has to be done this
     * way.
     */
    if (sky_object_bool(kws)) {
        sky_error_raise_format(sky_TypeError,
                               "%@ does not take keyword arguments",
                               sky_type_name(cls));
    }
    *oserror = *strerror = *filename = *winerror = NULL;
    nargs = (args ? sky_tuple_len(args) : 0);
#if defined(_WIN32)
    /* def __init__(self, errno, strerror, filename, winerror) */
    if (4 == nargs) {
        *strerror = sky_tuple_get(args, 1);
        *filename = sky_tuple_get(args, 2);
        *winerror = sky_tuple_get(args, 3);

        /* TODO map winerror to oserror */
        args = sky_tuple_pack(2, oserror, strerror);
    }
#else
    if (nargs >= 2 && nargs <= 3) {
        *oserror = sky_tuple_get(args, 0);
        *strerror = sky_tuple_get(args, 1);
        if (nargs > 2) {
            *filename = sky_tuple_get(args, 2);
            args = sky_tuple_pack(2, *oserror, *strerror);
        }
    }
#endif

    return args;
}


static void
sky_OSError_setup(sky_object_t  self,
                  sky_tuple_t   args,
                  sky_dict_t    kws,
                  sky_object_t  oserror,
                  sky_object_t  strerror,
                  sky_object_t  filename,
                  sky_object_t  winerror)
{
    sky_OSError_data_t  *exception_data = sky_OSError_data(self);

    sky_object_gc_set(&(exception_data->oserror), oserror, self);
    sky_object_gc_set(&(exception_data->strerror), strerror, self);
#if defined(_WIN32)
    sky_object_gc_set(&(exception_data->winerror), winerror, self);
#else
    sky_error_validate(NULL == winerror);
#endif

    if (filename) {
        if (sky_object_isa(self, sky_BlockingIOError) &&
            sky_number_check(filename))
        {
            exception_data->written = 
                    sky_integer_value(sky_number_index(filename),
                                      SSIZE_MIN, SSIZE_MAX,
                                      sky_ValueError);
        }
        else {
            sky_object_gc_set(&(exception_data->filename), filename, self);
        }
    }

    sky_BaseException_init(self, args, kws);
}


void
sky_OSError_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, errno, strerror, filename=None) */
{
    sky_type_t      cls;
    sky_object_t    filename, oserror, strerror, winerror;

    cls = sky_object_type(self);
    if (SKY_TYPE_METHOD_SLOT(cls, NEW) ==
            SKY_TYPE_METHOD_SLOT(sky_OSError, NEW) &&
        SKY_TYPE_METHOD_SLOT(cls, INIT) !=
            SKY_TYPE_METHOD_SLOT(sky_OSError, INIT))
    {
        args = sky_OSError_parse_arguments(sky_object_type(self),
                                           args,
                                           kws,
                                           &oserror,
                                           &strerror,
                                           &filename,
                                           &winerror);
        sky_OSError_setup(self,
                          args,
                          kws,
                          oserror,
                          strerror,
                          filename,
                          winerror);
    }
}


static sky_dict_t sky_OSError_type_map = NULL;


sky_object_t
sky_OSError_new(sky_type_t cls, sky_tuple_t args, sky_dict_t kws)
{
    sky_object_t    filename, oserror, self, strerror, winerror;

    if (SKY_TYPE_METHOD_SLOT(cls, NEW) !=
            SKY_TYPE_METHOD_SLOT(sky_OSError, NEW) ||
        SKY_TYPE_METHOD_SLOT(cls, INIT) ==
            SKY_TYPE_METHOD_SLOT(sky_OSError, INIT))
    {
        args = sky_OSError_parse_arguments(cls,
                                           args,
                                           kws,
                                           &oserror,
                                           &strerror,
                                           &filename,
                                           &winerror);
        if (sky_OSError == cls &&
            sky_object_isa(oserror, sky_integer_type) &&
            sky_OSError_type_map)
        {
            cls = sky_dict_get(sky_OSError_type_map, oserror, cls);
        }
    }

    self = sky_base_object_new(cls, NULL, NULL);

    if (SKY_TYPE_METHOD_SLOT(cls, NEW) !=
            SKY_TYPE_METHOD_SLOT(sky_OSError, NEW) ||
        SKY_TYPE_METHOD_SLOT(cls, INIT) ==
            SKY_TYPE_METHOD_SLOT(sky_OSError, INIT))
    {
        sky_OSError_setup(self,
                          args,
                          kws,
                          oserror,
                          strerror,
                          filename,
                          winerror);
    }

    return self;
}


sky_object_t
sky_OSError_reduce(sky_object_t self)
{
    sky_tuple_t         args            = sky_BaseException_data(self)->args;
    sky_OSError_data_t  *exception_data = sky_OSError_data(self);

    sky_dict_t      dict = NULL;
    sky_type_data_t *type_data;

    if (args && sky_tuple_len(args) == 2 && exception_data->filename) {
        args = sky_tuple_pack(3, sky_tuple_get(args, 0),
                                 sky_tuple_get(args, 1),
                                 exception_data->filename);
    }
    type_data = sky_type_data(sky_object_type(self));
    if (type_data->flags & SKY_TYPE_FLAG_HAS_DICT) {
        dict = *(sky_dict_t *)((char *)self + type_data->dict_offset);
    }
    return sky_tuple_pack((dict ? 3 : 2), sky_object_type(self), args, dict);
}


sky_string_t
sky_OSError_str(sky_object_t self)
{
    sky_OSError_data_t  *exception_data = sky_OSError_data(self);

#if defined(_WIN32)
    if (exception_data->winerror && exception_data->filename) {
        return sky_string_createfromformat("[Error %@] %@: %#@",
                                           exception_data->winerror,
                                           exception_data->strerror,
                                           exception_data->filename);
    }
    if (exception_data->winerror && exception_data->strerror) {
        return sky_string_createfromformat("[Error %@] %@",
                                           exception_data->winerror,
                                           exception_data->strerror);
    }
#endif
    if (exception_data->filename) {
        return sky_string_createfromformat("[Errno %@] %@: %#@",
                                           exception_data->oserror,
                                           exception_data->strerror,
                                           exception_data->filename);
    }
    if (exception_data->oserror && exception_data->strerror) {
        return sky_string_createfromformat("[Errno %@] %@",
                                           exception_data->oserror,
                                           exception_data->strerror);
    }

    return sky_BaseException_str(self);
}


void
sky_StopIteration_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, *args) */
{
    sky_StopIteration_data_t    *exception_data = sky_StopIteration_data(self);

    sky_BaseException_init(self, args, kws);
    if (args && sky_tuple_len(args) > 0) {
        sky_object_gc_set(&(exception_data->value),
                          sky_tuple_get(args, 0),
                          self);
    }
}


void
sky_SyntaxError_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, *args) */
{
    sky_SyntaxError_data_t  *exception_data = sky_SyntaxError_data(self);

    sky_BaseException_init(self, args, kws);
    if (args && sky_tuple_len(args) >= 1) {
        sky_object_gc_set(&(exception_data->msg), sky_tuple_get(args, 0), self);
        if (sky_tuple_len(args) == 2) {
            args = sky_tuple_create(sky_tuple_get(args, 1));
            if (sky_tuple_len(args) != 4) {
                sky_error_raise_string(sky_IndexError,
                                       "tuple index out of range");
            }
            sky_object_gc_set(&(exception_data->filename),
                              sky_tuple_get(args, 0),
                              self);
            sky_object_gc_set(&(exception_data->lineno),
                              sky_tuple_get(args, 1),
                              self);
            sky_object_gc_set(&(exception_data->offset),
                              sky_tuple_get(args, 2),
                              self);
            sky_object_gc_set(&(exception_data->text),
                              sky_tuple_get(args, 3),
                              self);
        }
    }
}


sky_string_t
sky_SyntaxError_str(sky_object_t self)
{
    sky_SyntaxError_data_t  *exception_data = sky_SyntaxError_data(self);

    sky_object_t    filename;

    if (!sky_object_isa(exception_data->filename, sky_string_type)) {
        filename = NULL;
    }
    else {
        /* TODO filename = os.path.basename(exception_data->filename) */
        filename = exception_data->filename;
    }
    if (!sky_object_isa(exception_data->lineno, sky_integer_type)) {
        if (!filename) {
            return sky_object_str((exception_data->msg ? exception_data->msg
                                                       : sky_None));
        }
        return sky_string_createfromformat("%@ (%@)",
                                           exception_data->msg,
                                           filename);
    }
    if (!filename) {
        return sky_string_createfromformat("%@ (line %@)",
                                           exception_data->msg,
                                           exception_data->lineno);
    }
    return sky_string_createfromformat("%@ (%@, line %@)",
                                       exception_data->msg,
                                       filename,
                                       exception_data->lineno);
}


void
sky_SystemExit_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
    /* def __init__(self, *args) */
{
    sky_SystemExit_data_t   *exception_data = sky_SystemExit_data(self);

    sky_BaseException_init(self, args, kws);
    if (args && sky_tuple_len(args) > 0) {
        sky_object_gc_set(&(exception_data->code),
                          sky_tuple_get(args, 0),
                          self);
    }
}


void
sky_UnicodeDecodeError_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    sky_BaseException_init(self, args, kws);

    if (!args || sky_tuple_len(args) != 5) {
        sky_error_raise_format(sky_TypeError,
                               "function takes exactly 5 arguments (%zd given)",
                               (args ? sky_tuple_len(args) : 0));
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(exception_data->encoding)),
                      sky_tuple_get(args, 0),
                      self);
    if (!sky_object_isa(exception_data->encoding, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->encoding)));
    }

    sky_object_gc_set(&(exception_data->object), sky_tuple_get(args, 1), self);
    if (sky_bytes_type != sky_object_type(exception_data->object)) {
        sky_buffer_t    buffer;

        SKY_ASSET_BLOCK_BEGIN {
            sky_buffer_acquire(&buffer,
                               exception_data->object,
                               SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            sky_object_gc_set(&(exception_data->object),
                              sky_bytes_createfrombytes(buffer.buf,
                                                        buffer.len),
                              self);
        } SKY_ASSET_BLOCK_END;
    }

    exception_data->start =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 2)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);
    exception_data->end =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 3)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);

    sky_object_gc_set(SKY_AS_OBJECTP(&(exception_data->reason)),
                      sky_tuple_get(args, 4),
                      self);
    if (!sky_object_isa(exception_data->reason, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->reason)));
    }
}


sky_string_t
sky_UnicodeDecodeError_str(sky_object_t self)
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    int cp;

    if ((sky_object_isa(exception_data->object, sky_bytes_type) ||
         sky_object_isa(exception_data->object, sky_bytearray_type)) &&
        exception_data->start < sky_object_len(exception_data->object) &&
        exception_data->end == exception_data->start + 1)
    {
        if (sky_object_isa(exception_data->object, sky_bytes_type)) {
            cp = sky_bytes_byteat(exception_data->object,
                                  exception_data->start);
        }
        else if (sky_object_isa(exception_data->object, sky_bytearray_type)) {
            cp = sky_bytearray_byteat(exception_data->object,
                                      exception_data->start);
        }
        else {
            sky_error_fatal("internal error");
        }
        return sky_string_createfromformat(
                        "%#@ codec can't decode byte 0x%02x in position %zd: %@",
                        sky_object_str(exception_data->encoding),
                        cp,
                        exception_data->start,
                        exception_data->reason);
    }

    return sky_string_createfromformat(
                    "%#@ codec can't decode bytes in position %zd-%zd: %@",
                    sky_object_str(exception_data->encoding),
                    exception_data->start,
                    exception_data->end - 1,
                    exception_data->reason);
}


void
sky_UnicodeEncodeError_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    sky_BaseException_init(self, args, kws);

    if (!args || sky_tuple_len(args) != 5) {
        sky_error_raise_format(sky_TypeError,
                               "function takes exactly 5 arguments (%zd given)",
                               (args ? sky_tuple_len(args) : 0));
    }

    sky_object_gc_set(SKY_AS_OBJECTP(&(exception_data->encoding)),
                      sky_tuple_get(args, 0),
                      self);
    if (!sky_object_isa(exception_data->encoding, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->encoding)));
    }

    sky_object_gc_set(&(exception_data->object), sky_tuple_get(args, 1), self);
    if (!sky_object_isa(exception_data->object, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->object)));
    }

    exception_data->start =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 2)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);
    exception_data->end =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 3)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);

    sky_object_gc_set(SKY_AS_OBJECTP(&(exception_data->reason)),
                      sky_tuple_get(args, 4),
                      self);
    if (!sky_object_isa(exception_data->reason, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->reason)));
    }
}


sky_string_t
sky_UnicodeEncodeError_str(sky_object_t self)
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    if (sky_object_isa(exception_data->object, sky_string_type) &&
        exception_data->start < sky_object_len(exception_data->object) &&
        exception_data->end == exception_data->start + 1)
    {
        const char          *format;
        sky_unicode_char_t  cp;

        cp = sky_string_charat(exception_data->object, exception_data->start);
        if (cp < 0x100) {
            format = "%#@ codec can't encode character '\\x%02x' in position %zd: %@";
        }
        else if (cp < 0x10000) {
            format = "%#@ codec can't encode character '\\u%04x' in position %zd: %@";
        }
        else {
            format = "%#@ codec can't encode character '\\U%08x' in position %zd: %@";
        }
        return sky_string_createfromformat(
                        format,
                        sky_object_str(exception_data->encoding),
                        (int)cp,
                        exception_data->start,
                        exception_data->reason);
    }

    return sky_string_createfromformat(
                    "%#@ codec can't encode characters in position %zd-%zd: %@",
                    sky_object_str(exception_data->encoding),
                    exception_data->start,
                    exception_data->end - 1,
                    exception_data->reason);
}


static sky_bytes_t
sky_exceptions_bytes_check(sky_object_t object, const char *attr_name)
{
    if (!object) {
        sky_error_raise_format(sky_TypeError,
                               "%s attribute not set",
                               attr_name);
    }
    if (!sky_object_isa(object, sky_bytes_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%s attribute must be bytes",
                               attr_name);
    }
    return object;
}


static sky_string_t
sky_exceptions_string_check(sky_object_t object, const char *attr_name)
{
    if (!object) {
        sky_error_raise_format(sky_TypeError,
                               "%s attribute not set",
                               attr_name);
    }
    if (!sky_object_isa(object, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "%s attribute must be unicode",
                               attr_name);
    }
    return object;
}


sky_string_t
sky_UnicodeError_encoding(sky_object_t self)
{
    return sky_exceptions_string_check(sky_UnicodeError_data(self)->encoding,
                                       "encoding");
}


ssize_t
sky_UnicodeError_end(sky_object_t self)
{
    sky_UnicodeError_data_t *self_data = sky_UnicodeError_data(self);

    ssize_t         end, len;
    sky_object_t    object;

    if (sky_object_isa(self, sky_UnicodeDecodeError)) {
        object = sky_exceptions_bytes_check(self_data->object, "object");
    }
    else if (sky_object_isa(self, sky_UnicodeEncodeError) ||
             sky_object_isa(self, sky_UnicodeTranslateError))
    {
        object = sky_exceptions_string_check(self_data->object, "object");
    }
    else {
        return self_data->end;
    }
    len = sky_object_len(object);
    end = self_data->end;

    return (end < 0 ? 0 : (end > len ? len : end));
}


sky_object_t
sky_UnicodeError_object(sky_object_t self)
{
    sky_UnicodeError_data_t *self_data = sky_UnicodeError_data(self);

    if (sky_object_isa(self, sky_UnicodeDecodeError)) {
        return sky_exceptions_bytes_check(self_data->object, "object");
    }
    if (sky_object_isa(self, sky_UnicodeEncodeError) ||
        sky_object_isa(self, sky_UnicodeTranslateError))
    {
        return sky_exceptions_string_check(self_data->object, "object");
    }
    return self_data->object;
}


ssize_t
sky_UnicodeError_start(sky_object_t self)
{
    sky_UnicodeError_data_t *self_data = sky_UnicodeError_data(self);

    ssize_t         start, len;
    sky_object_t    object;

    if (sky_object_isa(self, sky_UnicodeDecodeError)) {
        object = sky_exceptions_bytes_check(self_data->object, "object");
    }
    else if (sky_object_isa(self, sky_UnicodeEncodeError) ||
             sky_object_isa(self, sky_UnicodeTranslateError))
    {
        object = sky_exceptions_string_check(self_data->object, "object");
    }
    else {
        return self_data->start;
    }
    len = sky_object_len(object);
    start = self_data->start;

    return (start < 0 ? 0 : (start > len ? len : start));
}


void
sky_UnicodeTranslateError_init(sky_object_t self,
                               sky_tuple_t  args,
                               sky_dict_t   kws)
    /* def __init__(self, *args) */
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    sky_BaseException_init(self, args, kws);

    if (!args || sky_tuple_len(args) != 4) {
        sky_error_raise_format(sky_TypeError,
                               "function takes exactly 4 arguments (%zd given)",
                               (args ? sky_tuple_len(args) : 0));
    }

    sky_object_gc_set(&(exception_data->object), sky_tuple_get(args, 0), self);
    if (!sky_object_isa(exception_data->object, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->object)));
    }

    exception_data->start =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 1)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);
    exception_data->end =
            sky_integer_value(sky_number_index(sky_tuple_get(args, 2)),
                              SSIZE_MIN,
                              SSIZE_MAX,
                              NULL);

    sky_object_gc_set(SKY_AS_OBJECTP(&(exception_data->reason)),
                      sky_tuple_get(args, 3),
                      self);
    if (!sky_object_isa(exception_data->reason, sky_string_type)) {
        sky_error_raise_format(
                sky_TypeError,
                "expected %#@; got %#@",
                sky_type_name(sky_string_type),
                sky_type_name(sky_object_type(exception_data->reason)));
    }
}


sky_string_t
sky_UnicodeTranslateError_str(sky_object_t self)
{
    sky_UnicodeError_data_t *exception_data = sky_UnicodeError_data(self);

    if (sky_object_isa(exception_data->object, sky_string_type) &&
        exception_data->start < sky_object_len(exception_data->object) &&
        exception_data->start + 1 == exception_data->end)
    {
        const char          *format;
        sky_unicode_char_t  cp;

        cp = sky_string_charat(exception_data->object, exception_data->start);
        if (cp < 0x100) {
            format = "can't translate character '\\x%02x' in position %zd: %@";
        }
        else if (cp < 0x10000) {
            format = "can't translate character '\\u%04x' in position %zd: %@";
        }
        else {
            format = "can't translate character '\\U%08x' in position %zd: %@";
        }
        return sky_string_createfromformat(format,
                                           (int)cp,
                                           exception_data->start,
                                           exception_data->reason);
    }

    return sky_string_createfromformat(
                    "can't translate characters in position %zd-%zd: %@",
                    exception_data->start,
                    exception_data->end - 1,
                    exception_data->reason);
}


static inline void
sky_exceptions_OSError_map_type(int errno_code, sky_type_t type)
{
    sky_dict_setitem(sky_OSError_type_map,
                     sky_integer_create(errno_code),
                     type);
}


#define new_exception(_e, _b)                               \
    do {                                                    \
        sky_type_initialize_builtin(sky_##_e, 1, sky_##_b); \
    } while (0)


void
sky_exceptions_initialize_library(void)
{
    sky_type_initialize_builtin(sky_BaseException, 0);
    sky_type_setattr_builtin(
            sky_BaseException,
            "with_traceback",
            sky_function_createbuiltin(
                    "BaseException.with_traceback",
                    "Exeption.with_traceback(tb)\n"
                    "    set self.__traceback__ to tb and return self.",
                    (sky_native_code_function_t)sky_BaseException_with_traceback,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT, NULL,
                    "tb", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_getset(
            sky_BaseException,
            "args",
            NULL,
            sky_BaseException_args_getter,
            sky_BaseException_args_setter);
    sky_type_setattr_getset(
            sky_BaseException,
            "__cause__",
            "exception cause",
            sky_BaseException_cause_getter,
            sky_BaseException_cause_setter);
    sky_type_setmembers(sky_BaseException,
            "__context__",
                    "exception cause",
                    offsetof(sky_BaseException_data_t, context),
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF,
                    SKY_MEMBER_FLAG_ALLOW_NONE | SKY_MEMBER_FLAG_NONE_IS_NULL,
                    sky_BaseException,
            "__suppress_context__",
                    NULL,
                    offsetof(sky_BaseException_data_t, suppress_context),
                    SKY_DATA_TYPE_BOOL, 0,
            "__traceback__",
                    NULL,
                    offsetof(sky_BaseException_data_t, traceback),
                    SKY_DATA_TYPE_OBJECT_INSTANCEOF,
                    SKY_MEMBER_FLAG_ALLOW_NONE | SKY_MEMBER_FLAG_NONE_IS_NULL,
                    sky_traceback_type,
            NULL);
    sky_type_setmethodslots(sky_BaseException,
            "__new__", sky_BaseException_new,
            "__init__", sky_BaseException_init,
            "__repr__", sky_BaseException_repr,
            "__str__", sky_BaseException_str,
            "__reduce__", sky_BaseException_reduce,
            "__setstate__", sky_BaseException_setstate,
            NULL);

    new_exception(Exception, BaseException);

    new_exception(GeneratorExit, BaseException);

    new_exception(KeyboardInterrupt, BaseException);

    new_exception(SystemExit, BaseException);
    sky_type_setattr_member(sky_SystemExit,
                            "code",
                            "exception code",
                            offsetof(sky_SystemExit_data_t, code),
                            SKY_DATA_TYPE_OBJECT,
                            0);
    sky_type_setmethodslots(sky_SystemExit,
            "__init__", sky_SystemExit_init,
            NULL);

    new_exception(ArithmeticError, Exception);

    new_exception(FloatingPointError, ArithmeticError);

    new_exception(OverflowError, ArithmeticError);

    new_exception(ZeroDivisionError, ArithmeticError);

    new_exception(AssertionError, Exception);

    new_exception(AttributeError, Exception);

    new_exception(BufferError, Exception);

    new_exception(EOFError, Exception);

    new_exception(ImportError, Exception);
    sky_type_setmembers(sky_ImportError,
            "msg",  "exception message",
                    offsetof(sky_ImportError_data_t, msg),
                    SKY_DATA_TYPE_OBJECT, 0,
            "name", "module name",
                    offsetof(sky_ImportError_data_t, name),
                    SKY_DATA_TYPE_OBJECT, 0,
            "path", "module path",
                    offsetof(sky_ImportError_data_t, path),
                    SKY_DATA_TYPE_OBJECT, 0,
            NULL);
    sky_type_setmethodslots(sky_ImportError,
            "__init__", sky_ImportError_init,
            "__str__", sky_ImportError_str,
            NULL);

    new_exception(LookupError, Exception);

    new_exception(IndexError, LookupError);

    new_exception(KeyError, LookupError);
    sky_type_setmethodslots(sky_KeyError,
            "__str__", sky_KeyError_str,
            NULL);

    /* There's nothing especially fancy to be done for sky_MemoryError, really,
     * except that maybe a different allocator/deallocator should be used to
     * use pre-allocated instances in an out of memory condition. It seems a
     * waste, though, because Skython's memory allocation API will abort on a
     * true out of memory condition, with the rationale being that graceful
     * recovery from an out of memory condition never really works out very
     * well (or, you know, _at all_).
     */
    new_exception(MemoryError, Exception);

    new_exception(NameError, Exception);

    new_exception(UnboundLocalError, NameError);

    new_exception(OSError, Exception);
    sky_type_setmembers(sky_OSError,
            "errno",
                    "POSIX exception code",
                    offsetof(sky_OSError_data_t, oserror),
                    SKY_DATA_TYPE_OBJECT, 0,
            "strerror",
                    "exception strerror",
                    offsetof(sky_OSError_data_t, strerror),
                    SKY_DATA_TYPE_OBJECT, 0,
            "filename",
                    "exception filename",
                    offsetof(sky_OSError_data_t, filename),
                    SKY_DATA_TYPE_OBJECT, 0,
#if defined(_WIN32)
            "winerror",
                    "Win32 exception code",
                    offsetof(sky_OSError_data_t, winerror),
                    SKY_DATA_TYPE_OBJECT, 0,
#endif
            "characters_written",
                    NULL,
                    offsetof(sky_OSError_data_t, written),
                    SKY_DATA_TYPE_SSIZE_T, 0,
            NULL);
    sky_type_setmethodslots(sky_OSError,
            "__new__", sky_OSError_new,
            "__init__", sky_OSError_init,
            "__str__", sky_OSError_str,
            "__reduce__", sky_OSError_reduce,
            NULL);

    new_exception(BlockingIOError, OSError);

    new_exception(ChildProcessError, OSError);

    new_exception(ConnectionError, OSError);

    new_exception(BrokenPipeError, ConnectionError);

    new_exception(ConnectionAbortedError, ConnectionError);

    new_exception(ConnectionRefusedError, ConnectionError);

    new_exception(ConnectionResetError, ConnectionError);

    new_exception(FileExistsError, OSError);

    new_exception(FileNotFoundError, OSError);

    new_exception(IsADirectoryError, OSError);

    new_exception(NotADirectoryError, OSError);

    new_exception(InterruptedError, OSError);

    new_exception(PermissionError, OSError);

    new_exception(ProcessLookupError, OSError);

    new_exception(TimeoutError, OSError);

    new_exception(RuntimeError, Exception);

    new_exception(NotImplementedError, RuntimeError);

    new_exception(ReferenceError, Exception);

    new_exception(StopIteration, Exception);
    sky_type_setattr_member(sky_StopIteration,
                            "value",
                            "generator return value",
                            offsetof(sky_StopIteration_data_t, value),
                            SKY_DATA_TYPE_OBJECT,
                            0);
    sky_type_setmethodslots(sky_StopIteration,
            "__init__", sky_StopIteration_init,
            NULL);

    new_exception(SyntaxError, Exception);
    sky_type_setmembers(sky_SyntaxError,
            "msg",
                    "exception msg",
                    offsetof(sky_SyntaxError_data_t, msg),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "filename",
                    "exception filename",
                    offsetof(sky_SyntaxError_data_t, filename),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "lineno",
                    "exception lineno",
                    offsetof(sky_SyntaxError_data_t, lineno),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "offset",
                    "exception offset",
                    offsetof(sky_SyntaxError_data_t, offset),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "text",
                    "exception text",
                    offsetof(sky_SyntaxError_data_t, text),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "print_file_and_line",
                    "exception print_file_and_line",
                    offsetof(sky_SyntaxError_data_t, print_file_and_line),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            NULL);
    sky_type_setmethodslots(sky_SyntaxError,
            "__init__", sky_SyntaxError_init,
            "__str__", sky_SyntaxError_str,
            NULL);

    new_exception(IndentationError, SyntaxError);

    new_exception(TabError, IndentationError);

    new_exception(SystemError, Exception);

    new_exception(TypeError, Exception);

    new_exception(ValueError, Exception);

    new_exception(UnicodeError, ValueError);
    sky_type_setmembers(sky_UnicodeError,
            "encoding",
                    "exception encoding",
                    offsetof(sky_UnicodeError_data_t, encoding),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "object",
                    "exception object",
                    offsetof(sky_UnicodeError_data_t, object),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            "start",
                    "exception start",
                    offsetof(sky_UnicodeError_data_t, start),
                    SKY_DATA_TYPE_INDEX, 0,
            "end",
                    "exception end",
                    offsetof(sky_UnicodeError_data_t, end),
                    SKY_DATA_TYPE_INDEX, 0,
            "reason",
                    "exception reason",
                    offsetof(sky_UnicodeError_data_t, reason),
                    SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_NONE_IS_NULL,
            NULL);

    new_exception(UnicodeDecodeError, UnicodeError);
    sky_type_setmethodslots(sky_UnicodeDecodeError,
            "__init__", sky_UnicodeDecodeError_init,
            "__str__", sky_UnicodeDecodeError_str,
            NULL);

    new_exception(UnicodeEncodeError, UnicodeError);
    sky_type_setmethodslots(sky_UnicodeEncodeError,
            "__init__", sky_UnicodeEncodeError_init,
            "__str__", sky_UnicodeEncodeError_str,
            NULL);

    new_exception(UnicodeTranslateError, UnicodeError);
    sky_type_setmethodslots(sky_UnicodeTranslateError,
            "__init__", sky_UnicodeTranslateError_init,
            "__str__", sky_UnicodeTranslateError_str,
            NULL);

    /** Warnings **/

    new_exception(Warning, Exception);

    new_exception(BytesWarning, Warning);

    new_exception(DeprecationWarning, Warning);

    new_exception(PendingDeprecationWarning, Warning);

    new_exception(FutureWarning, Warning);

    new_exception(ImportWarning, Warning);

    new_exception(ResourceWarning, Warning);

    new_exception(RuntimeWarning, Warning);

    new_exception(SyntaxWarning, Warning);

    new_exception(UnicodeWarning, Warning);

    new_exception(UserWarning, Warning);

    /* Create the OSError type map for mapping errno codes to OSError sub-types
     * per PEP 3151.
     */
    sky_error_validate(NULL == sky_OSError_type_map);
    sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&sky_OSError_type_map),
                                 sky_dict_create(),
                                 SKY_TRUE);
    sky_exceptions_OSError_map_type(EAGAIN, sky_BlockingIOError);
    sky_exceptions_OSError_map_type(EALREADY, sky_BlockingIOError);
    sky_exceptions_OSError_map_type(EWOULDBLOCK, sky_BlockingIOError);
    sky_exceptions_OSError_map_type(EINPROGRESS, sky_BlockingIOError);
    sky_exceptions_OSError_map_type(ECHILD, sky_ChildProcessError);
    sky_exceptions_OSError_map_type(EPIPE, sky_BrokenPipeError);
    sky_exceptions_OSError_map_type(ESHUTDOWN, sky_BrokenPipeError);
    sky_exceptions_OSError_map_type(ECONNABORTED, sky_ConnectionAbortedError);
    sky_exceptions_OSError_map_type(ECONNREFUSED, sky_ConnectionRefusedError);
    sky_exceptions_OSError_map_type(ECONNRESET, sky_ConnectionResetError);
    sky_exceptions_OSError_map_type(EEXIST, sky_FileExistsError);
    sky_exceptions_OSError_map_type(ENOENT, sky_FileNotFoundError);
    sky_exceptions_OSError_map_type(EINTR, sky_InterruptedError);
    sky_exceptions_OSError_map_type(EISDIR, sky_IsADirectoryError);
    sky_exceptions_OSError_map_type(ENOTDIR, sky_NotADirectoryError);
    sky_exceptions_OSError_map_type(EACCES, sky_PermissionError);
    sky_exceptions_OSError_map_type(EPERM, sky_PermissionError);
    sky_exceptions_OSError_map_type(ESRCH, sky_ProcessLookupError);
    sky_exceptions_OSError_map_type(ETIMEDOUT, sky_TimeoutError);
}
