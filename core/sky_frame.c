#include "sky_private.h"
#include "sky_code_private.h"
#include "sky_frame_private.h"


static sky_string_t sky_frame_builtins;


void
sky_frame_block_free(sky_frame_block_t *block)
{
    if (block->error) {
        sky_error_record_release(block->error);
    }
    sky_free(block);
}


static void
sky_frame_instance_initialize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_frame_data_t    *frame_data = data;

    frame_data->f_lasti = -1;
}


static void
sky_frame_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_frame_data_t    *frame_data = data;

    sky_frame_block_t   *block;

    while ((block = frame_data->blocks) != NULL) {
        frame_data->blocks = block->next;
        sky_frame_block_free(block);
    }
    sky_free(frame_data->values);
}


static void
sky_frame_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_frame_data_t    *frame_data = data;

    ssize_t                 i;
    sky_object_t            *sp;
    sky_frame_block_t       * volatile block;
    sky_hazard_pointer_t    *hazard;

    sky_object_visit(frame_data->f_back, visit_data);
    sky_object_visit(frame_data->f_code, visit_data);
    sky_object_visit(frame_data->f_globals, visit_data);
    sky_object_visit(frame_data->f_locals, visit_data);
    sky_object_visit(frame_data->f_builtins, visit_data);

    if (sky_object_isa(frame_data->f_code, sky_code_type)) {
        for (i = 0; i < frame_data->nlocals; ++i) {
            sky_object_visit(frame_data->locals[i], visit_data);
        }
        for (sp = frame_data->stack; sp < frame_data->sp; ++sp) {
            sky_object_visit(*sp, visit_data);
        }
        for (i = 0; i < frame_data->ncellvars; ++i) {
            sky_object_visit(frame_data->cellvars[i], visit_data);
        }
        for (i = 0; i < frame_data->nfreevars; ++i) {
            sky_object_visit(frame_data->freevars[i], visit_data);
        }
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(frame_data->blocks)));
    while ((block = hazard->pointer) != NULL) {
        if ((uintptr_t)block & 1) {
            sky_hazard_pointer_update(hazard,
                                      SKY_AS_VOIDP(&(frame_data->blocks)));
            continue;
        }
        switch (block->type) {
            case SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT:
            case SKY_FRAME_BLOCK_TYPE_TRY_FINALLY:
            case SKY_FRAME_BLOCK_TYPE_EXCEPT:
            case SKY_FRAME_BLOCK_TYPE_FINALLY:
                break;
            case SKY_FRAME_BLOCK_TYPE_WITH:
            case SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP:
                sky_object_visit(block->with_mgr, visit_data);
                sky_object_visit(block->with_exit, visit_data);
                break;
        }
        sky_hazard_pointer_update(hazard, SKY_AS_VOIDP(&(block->next)));
    }
    sky_hazard_pointer_release(hazard);
}


sky_frame_t
sky_frame_create(sky_object_t   code,
                 sky_dict_t     globals,
                 sky_object_t   locals)
{
    ssize_t             i;
    sky_frame_t         back, frame;
    sky_object_t        builtins, iter, key, value, *values;
    sky_code_data_t     *code_data;
    sky_frame_data_t    *back_data, *frame_data;

    if (!sky_object_isa(code, sky_code_type) &&
        !sky_object_isa(code, sky_native_code_type))
    {
        sky_error_raise_format(sky_TypeError,
                               "expected %#@ or %#@; got %#@",
                               sky_type_name(sky_code_type),
                               sky_type_name(sky_native_code_type),
                               sky_type_name(sky_object_type(code)));
    }

    back = sky_tlsdata_get()->current_frame;
    back_data = (back ? sky_frame_data(back) : NULL);
    if (back && back_data->f_globals == globals) {
        builtins = back_data->f_builtins;
    }
    else {
        builtins = sky_dict_get(globals, sky_frame_builtins, NULL);
        if (sky_object_isa(builtins, sky_module_type)) {
            builtins = sky_object_dict(builtins);
        }
        if (!builtins) {
            if (sky_interpreter_module_builtins()) {
                builtins = sky_interpreter_builtins();
            }
            if (!builtins) {
                builtins = sky_dict_create();
                sky_dict_setitem(builtins, sky_object_repr(sky_None), sky_None);
            }
        }
        /* Yep, it's legal to change __builtins__ to something that does not
         * implement __getattr__(). It's a bad idea, but legal nonetheless.
         */
    }

    frame = sky_object_allocate(sky_frame_type);
    frame_data = sky_frame_data(frame);
    sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_back)), back, frame);
    sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_code)), code, frame);
    sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_globals)), globals, frame);
    sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_builtins)), builtins, frame);

    if (sky_object_isa(code, sky_code_type)) {
        code_data = sky_code_data(code);
        frame_data->f_lineno = code_data->co_firstlineno;
        if (!(code_data->co_flags & SKY_CODE_FLAG_NEWLOCALS)) {
            frame_data->nlocals = -1;
            sky_object_gc_set(SKY_AS_OBJECTP(&(frame_data->f_locals)),
                              (locals ? locals : globals),
                              frame);
        }
        else if (code_data->co_flags & SKY_CODE_FLAG_OPTIMIZED) {
            frame_data->nlocals = sky_object_len(code_data->co_varnames);
            frame_data->nvalues = frame_data->nlocals;
        }
        else {
            frame_data->nlocals = -1;
            sky_object_gc_set(&(frame_data->f_locals),
                              sky_dict_create(),
                              frame);
        }

        frame_data->ncellvars = sky_object_len(code_data->co_cellvars);
        frame_data->nfreevars = sky_object_len(code_data->co_freevars);
        frame_data->nvalues += code_data->co_stacksize +
                               frame_data->ncellvars +
                               frame_data->nfreevars;
        sky_error_validate_debug(frame_data->nvalues > 0);
        frame_data->values = sky_calloc(frame_data->nvalues,
                                        sizeof(sky_object_t));

        values = frame_data->values;
        if (frame_data->nlocals > 0) {
            frame_data->locals = values;
            values += frame_data->nlocals;
        }
        if (frame_data->ncellvars) {
            frame_data->cellvars = values;
            values += frame_data->ncellvars;
        }
        if (frame_data->nfreevars) {
            frame_data->freevars = values;
            values += frame_data->nfreevars;
        }
        frame_data->stack = values;
        frame_data->sp = frame_data->stack;

        /* Merge anything from f_locals into the fast locals */
        if (frame_data->f_locals && frame_data->nlocals >= 0) {
            if (sky_sequence_isiterable(frame_data->f_locals)) {
                SKY_SEQUENCE_FOREACH(frame_data->f_locals, key) {
                    i = sky_tuple_find(code_data->co_varnames, key);
                    if (i >= 0) {
                        value = sky_object_getitem(frame_data->f_locals, key);
                        sky_object_gc_set(&(frame_data->locals[i]),
                                          value,
                                          frame);
                    }
                } SKY_SEQUENCE_FOREACH_END;
            }
            else {
                iter = sky_object_iter(frame_data->f_locals);
                while ((key = sky_object_next(iter, NULL)) != NULL) {
                    i = sky_tuple_find(code_data->co_varnames, key);
                    if (i >= 0) {
                        value = sky_object_getitem(frame_data->f_locals, key);
                        sky_object_gc_set(&(frame_data->locals[i]),
                                          value,
                                          frame);
                    }
                }
            }
        }
    }

    return frame;
}


static sky_object_t
sky_frame_lineno_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_frame_data_t    *self_data = sky_frame_data(self);

    return sky_integer_create(sky_code_lineno(self_data->f_code,
                                              self_data->f_lasti));
}


void
sky_frame_lineno_setter(SKY_UNUSED sky_object_t self,       /* TODO */
                        SKY_UNUSED sky_object_t value,
                        SKY_UNUSED sky_type_t   type)
{
    sky_error_raise_string(sky_RuntimeError, "not implemented");
}


sky_object_t
sky_frame_locals(sky_frame_t self)
{
    sky_frame_data_t    *self_data = sky_frame_data(self);
    sky_code_data_t     *code_data;

    ssize_t         i;
    sky_dict_t      locals;
    sky_object_t    key, value;

    if (!sky_object_isa(self_data->f_code, sky_code_type)) {
        return sky_dict_create();
    }

    /* If there are fast locals, merge them into the locals mapping. */
    if (!(locals = self_data->f_locals)) {
        locals = sky_dict_create();
        sky_object_gc_set(&(self_data->f_locals), locals, self);
    }
    if (self_data->locals) {
        i = 0;
        code_data = sky_code_data(self_data->f_code);
        SKY_SEQUENCE_FOREACH(code_data->co_varnames, key) {
            if ((value = self_data->locals[i++]) != NULL) {
                sky_dict_setitem(locals, key, value);
            }
        } SKY_SEQUENCE_FOREACH_END;
    }

    return locals;
}


static sky_object_t
sky_frame_locals_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    return sky_frame_locals(self);
}


size_t
sky_frame_sizeof(sky_frame_t self)
{
    sky_frame_data_t    *self_data = sky_frame_data(self);

    size_t              nbytes;
    sky_frame_block_t   *block;

    if (sky_object_stack_push(self)) {
        return 0;
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        nbytes = sky_memsize(self);
        if (self_data->values) {
            nbytes += sky_memsize(self_data->values);
        }
        for (block = self_data->blocks; block; block = block->next) {
            nbytes += sky_memsize(block);
        }
    } SKY_ASSET_BLOCK_END;

    return nbytes;
}


static sky_object_t
sky_frame_trace_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_frame_data_t    *self_data = sky_frame_data(self);

    return (self_data->f_trace ? self_data->f_trace : sky_None);
}


static void
sky_frame_trace_setter(
                    sky_object_t    self,
                    sky_object_t    value,
        SKY_UNUSED  sky_type_t      type)
{
    sky_frame_data_t    *self_data = sky_frame_data(self);

    self_data->f_lineno = sky_code_lineno(self_data->f_code,
                                          self_data->f_lasti);
    sky_object_gc_set(&(self_data->f_trace), value, self);
}


SKY_TYPE_DEFINE_SIMPLE(frame,
                       "frame",
                       sizeof(sky_frame_data_t),
                       sky_frame_instance_initialize,
                       sky_frame_instance_finalize,
                       sky_frame_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

void
sky_frame_initialize_library(void)
{
    /* While SKY_STRING_LITERAL is pretty fast, given the rate at which frame
     * objects are created, it's worth saving the string object pointer and
     * skipping the cache lookup altogether.
     */
    sky_frame_builtins = SKY_STRING_LITERAL("__builtins__");

    sky_type_initialize_builtin(sky_frame_type, 0);

    sky_type_setattr_getset(
            sky_frame_type,
            "f_locals", 
            NULL,
            sky_frame_locals_getter,
            NULL);
    sky_type_setattr_getset(
            sky_frame_type,
            "f_lineno",
            NULL,
            sky_frame_lineno_getter,
            sky_frame_lineno_setter);
    sky_type_setattr_getset(
            sky_frame_type,
            "f_trace",
            NULL,
            sky_frame_trace_getter,
            sky_frame_trace_setter);

    sky_type_setmembers(sky_frame_type,
            "f_back",       NULL, offsetof(sky_frame_data_t, f_back),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "f_code",       NULL, offsetof(sky_frame_data_t, f_code),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "f_globals",    NULL, offsetof(sky_frame_data_t, f_globals),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "f_builtins",   NULL, offsetof(sky_frame_data_t, f_builtins),
                            SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "f_lasti",      NULL, offsetof(sky_frame_data_t, f_lasti),
                            SKY_DATA_TYPE_SSIZE_T, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(sky_frame_type, "__new__", sky_None);
    sky_type_setmethodslots(sky_frame_type,
            "__sizeof__", sky_frame_sizeof,
            NULL);
}
