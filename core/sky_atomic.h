/** @file
  * @brief Basic atomic operations.
  * @defgroup sky_atomic Atomic Operations
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ATOMIC_H__
#define __SKYTHON_CORE_SKY_ATOMIC_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** @defgroup sky_atomic_arithmetic Arithmetic
  * @{
  */

/** Atomically add an amount to a 32-bit value at the specified memory location,
  * returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_add32(int32_t amount, volatile int32_t *address);

/** Atomically add an amount to a 32-bit value at the specified memory location,
  * returning the value before the operation.
  */
SKY_EXTERN int32_t
sky_atomic_add32_old(int32_t amount, volatile int32_t *address);

/** Atomically decrement a 32-bit value at the specified memory location by one,
  * returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_decrement32(volatile int32_t *address);

/** Atomically decrement a 32-bit value at the specified memory location by one,
  * returning the value before the operation.
  */
SKY_EXTERN int32_t
sky_atomic_decrement32_old(volatile int32_t *address);

/** Atomically increment a 32-bit value at the specified memory location by one,
  * returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_increment32(volatile int32_t *address);

/** Atomically increment a 32-bit value at the specified memory location by one,
  * returning the value before the operation.
  */
SKY_EXTERN int32_t
sky_atomic_increment32_old(volatile int32_t *address);

/** Atomically add an amount to a 64-bit value at the specified memory location,
  * returning the resulting value.
  */
SKY_EXTERN int64_t
sky_atomic_add64(int64_t amount, volatile int64_t *address);

/** Atomically add an amount to a 32-bit value at the specified memory location,
  * returning the value before the operation.
  */
SKY_EXTERN int64_t
sky_atomic_add64_old(int64_t amount, volatile int64_t *address);

/** Atomically decrement a 64-bit value at the specified memory location by one,
  * returning the resulting value.
  */
SKY_EXTERN int64_t
sky_atomic_decrement64(volatile int64_t *address);

/** Atomically decrement a 64-bit value at the specified memory location by one,
  * returning the value before the operation.
  */
SKY_EXTERN int64_t
sky_atomic_decrement64_old(volatile int64_t *address);

/** Atomically increment a 64-bit value at the specified memory location by one,
  * returning the resulting value.
  */
SKY_EXTERN int64_t
sky_atomic_increment64(volatile int64_t *address);

/** Atomically increment a 64-bit value at the specified memory location by one,
  * returning the value before the operation.
  */
SKY_EXTERN int64_t
sky_atomic_increment64_old(volatile int64_t *address);

/** @} **/


/** @defgroup sky_atomic_cas Compare and Set
  * @{
  */

/** Atomically compare the pointer value at the specified memory location with
  * @a old_value, replacing it with @a new_value if they're equal, and return
  * @c SKY_TRUE or @c SKY_FALSE indicating whether the value was replaced.
  */
SKY_EXTERN sky_bool_t
sky_atomic_cas(void * restrict              old_value,
               void * restrict              new_value,
               void * restrict volatile *   address);

/** Atomically compare the 32-bit integer value at the specified memory location
  * with @a old_value, replacing it with @a new_value if they're equal, and
  * return @c SKY_TRUE or @c SKY_FALSE indicating whether the value was
  * replaced.
  */
SKY_EXTERN sky_bool_t
sky_atomic_cas32(int32_t            old_value,
                 int32_t            new_value,
                 volatile int32_t * address);

#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
/** Atomically compare the 64-bit integer value at the specified memory location
  * with @a old_value, replacing it with @a new_value if they're equal, and
  * return @c SKY_TRUE or @c SKY_FALSE indicating whether the value was
  * replaced.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN sky_bool_t
sky_atomic_cas64(int64_t            old_value,
                 int64_t            new_value,
                 volatile int64_t * address);
#endif

#if defined(__x86_64__) || defined(_M_X64)
/** Atomically compare the 128-bit integer value at the specified memory
  * location with the 128-bit value stored at the memory location specified by
  * @a old_value_address, replacing it with the 128-bit value stored at the
  * memory location specified by @a new_value address, and return @c SKY_TRUE
  * or @c SKY_FALSE indicating whether the value was replaced.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on amd64/x86_64 architectures.
  */
SKY_EXTERN sky_bool_t
sky_atomic_cas128(void * restrict           old_value_address,
                  void * restrict           new_value_address,
                  void * restrict volatile *address);
#endif

/** @} **/


/** @defgroup sky_atomic_xchg Exchange
  * @{
  */

/** Atomically set the pointer value at the specified memory location, and
  * return the value that was originally stored there.
  */
SKY_EXTERN void *
sky_atomic_exchange(void * restrict value, void * restrict volatile *address);

/** Atomically set the 32-bit integer value at the specified memory location,
  * and return the value that was originally stored there.
  */
SKY_EXTERN int32_t
sky_atomic_exchange32(int32_t value, int32_t *address);

/** Atomically set the 64-bit integer value at the specified memory location,
  * and return the value that was originally stored there.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_exchange64(int64_t value, int64_t *address);

/** @} **/


/** @defgroup sky_atomic_bitwise Bit-wise Logical Operators
  * @{
  */

/** Atomically AND the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_and32(uint32_t mask, volatile uint32_t *address);

/** Atomically AND the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the original value.
  */
SKY_EXTERN int32_t
sky_atomic_and32_old(uint32_t mask, volatile uint32_t *address);

/** Atomically OR the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_or32(uint32_t mask, volatile uint32_t *address);

/** Atomically OR the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the original value.
  */
SKY_EXTERN int32_t
sky_atomic_or32_old(uint32_t mask, volatile uint32_t *address);

/** Atomically XOR the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the resulting value.
  */
SKY_EXTERN int32_t
sky_atomic_xor32(uint32_t mask, volatile uint32_t *address);

/** Atomically XOR the specified mask with the 32-bit unsigned value at the
  * specified memory location, returning the original value.
  */
SKY_EXTERN int32_t
sky_atomic_xor32_old(uint32_t mask, volatile uint32_t *address);

#if defined(__i386__) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
/** Atomically AND the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the resulting value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_and64(uint64_t mask, volatile uint64_t *address);

/** Atomically AND the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the original value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_and64_old(uint64_t mask, volatile uint64_t *address);

/** Atomically OR the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the resulting value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_or64(uint64_t mask, volatile uint64_t *address);

/** Atomically OR the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the original value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_or64_old(uint64_t mask, volatile uint64_t *address);

/** Atomically XOR the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the resulting value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_xor64(uint64_t mask, volatile uint64_t *address);

/** Atomically XOR the specified mask with the 64-bit unsigned value at the
  * specified memory location, returning the original value.
  * @warning    Because this function depends on support from the hardware, it
  *             is not available on all architectures. It is currently only
  *             available on x86 and amd64/x86_64 architectures.
  */
SKY_EXTERN int64_t
sky_atomic_xor64_old(uint64_t mask, volatile uint64_t *address);

#endif

/** @} **/


/** @defgroup sky_atomic_bittest Bit Test and Set or Clear
  * @{
  */

/** Atomically clears the specified bit and returns its original value. **/
SKY_EXTERN sky_bool_t sky_atomic_testandclear(uint32_t bitn, volatile void *address);

/** Atomically sets the specified bit and returns its original value. **/
SKY_EXTERN sky_bool_t sky_atomic_testandset(uint32_t bitn, volatile void *address);

/** @} **/


/** @defgroup sky_atomic_processor_hints Processor Hints
  * @{
  */

/** Provide a hint to the processor that currently executing loop is a spin
  * loop.
  */
SKY_EXTERN void sky_atomic_pause(void);

/** @} **/


/** @defgroup sky_atomic_memory_barriers Memory Barriers
  * @{
  */

/** Wait for all memory load operations to complete before returning
  * (load barrier). 
  */
SKY_EXTERN void sky_atomic_lfence(void);

/** Wait for all memory load and store operations to complete before returning
  * (full barrier).
  */
SKY_EXTERN void sky_atomic_mfence(void);

/** Wait for all memory store operations to complete before returning
  * (store barrier). 
  */
SKY_EXTERN void sky_atomic_sfence(void);

/** @} **/


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ATOMIC_H__ */

/** @} **/
