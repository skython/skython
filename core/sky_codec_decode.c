#include "sky_private.h"
#include "sky_codec_private.h"
#include "sky_exceptions_private.h"
#include "sky_string_private.h"

#include <wchar.h>


/* CPython compatibility issues:
 *
 *  1.  CPython does not provide stateful implementations for all codecs.
 *      Notably: escape, raw-unicode-escape, unicode-escape, unicode-internal
 *      are not stateful. This list is not necessarily exhaustive.
 *      *** Skython provides stateful implementations for all codecs. ***
 *
 *  2.  PEP 293 does not allow for an error handler to change the object being
 *      decoded; however, CPython implements this functionality. The rule is
 *      that the reference implementation takes precedence over the PEP, so
 *      Skython implements this as well. I have no idea why this was done, but
 *      it can lead to subtle problems that may or may not later manifest as
 *      security issues ...
 *
 *  3.  For those codecs for which CPython provides stateful implementations,
 *      the returned value for how many input bytes were consumed is derived
 *      from the bytes array at return time, which means that if an error is
 *      raised and the error handler changes bytes (see #2, above), the result
 *      has no meaning to the original caller, because the caller does not know
 *      what bytes were decoded. This can lead to pointer arithmetic issues if
 *      the caller is assuming that the number of bytes consumed is relative to
 *      the input bytes initially passed in. Skython matches this behavior;
 *      however, I believe it should be addressed by both CPython and Skython
 *      and any other implementation that does the same.
 */


static void
sky_codec_decode_exception(sky_object_t *       exception,
                           const sky_codec_t *  codec,
                           sky_string_t         encoding,
                           const void *         bytes,
                           ssize_t              nbytes,
                           ssize_t              start,
                           ssize_t              end,
                           const char *         reason)
{
    if (!*exception) {
        sky_tuple_t args;

        if (sky_object_isnull(encoding)) {
            sky_error_validate_debug(NULL != codec);
            encoding = sky_string_createfrombytes(codec->name,
                                                  strlen(codec->name),
                                                  NULL,
                                                  NULL);
        }
        args = sky_object_build("(OOizizs)",
                                encoding,
                                sky_bytes_createfrombytes(bytes, nbytes),
                                start,
                                end,
                                reason);
        *exception = sky_object_call(sky_UnicodeDecodeError, args, NULL);
        if (!sky_object_isa(*exception, sky_UnicodeDecodeError)) {
            sky_error_raise_format(
                    sky_TypeError,
                    "%#@ expected; got %#@",
                    sky_type_name(sky_UnicodeDecodeError),
                    sky_type_name(sky_object_type(*exception)));
        }
    }
    else {
        sky_UnicodeError_data_t *exception_data;

        exception_data = sky_UnicodeError_data(*exception);
        exception_data->start = start;
        exception_data->end = end;
        sky_object_gc_set(
                &(exception_data->reason),
                sky_string_createfrombytes(reason, strlen(reason), NULL, NULL),
                *exception);
    }
}


static ssize_t
sky_codec_decode_error(sky_object_t *       exception,
                       const sky_codec_t *  codec,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       const void **        bytes,
                       ssize_t *            nbytes,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       ssize_t              start,
                       ssize_t              end,
                       const char *         reason)
{
    ssize_t                     new_offset;
    sky_tuple_t                 tuple;
    sky_buffer_t                buffer;
    sky_object_t                code, error_handler, input;
    sky_string_t                replacement;
    sky_string_data_t           *replacement_data, tagged_data;
    sky_UnicodeError_data_t     *exception_data;
    sky_codec_error_function_t  cfunction;

    sky_codec_decode_exception(exception,
                               codec,
                               encoding,
                               *bytes,
                               *nbytes,
                               start,
                               end,
                               reason);
    exception_data = sky_UnicodeError_data(*exception);
    input = exception_data->object;

    error_handler = sky_codec_lookup_error(errors);
    if (sky_object_isa(error_handler, sky_function_type) &&
        (code = sky_function_code(error_handler)) != NULL &&
        sky_object_isa(code, sky_native_code_type))
    {
        cfunction = (sky_codec_error_function_t)sky_native_code_function(code);
        tuple = cfunction(*exception);
    }
    else {
        tuple = sky_object_call(error_handler,
                                sky_tuple_pack(1, *exception),
                                NULL);
    }
    if (!sky_object_isa(tuple, sky_tuple_type) ||
        sky_tuple_len(tuple) != 2 ||
        !sky_object_isa(sky_tuple_get(tuple, 0), sky_string_type) ||
        !sky_object_isa(sky_tuple_get(tuple, 1), sky_integer_type))
    {
        sky_error_raise_string(
                sky_TypeError,
                "decoding error handler must return (str, int) tuple");
    }
    replacement = sky_string_createfromobject(sky_tuple_get(tuple, 0));
    replacement_data = sky_string_data_noctype(replacement, &tagged_data);
    new_offset = sky_integer_value(sky_number_index(sky_tuple_get(tuple, 1)),
                                   SSIZE_MIN,
                                   SSIZE_MAX,
                                   NULL);

    /* The callback may have changed the input object, in which case we must
     * update our internal input object. PEP 293 specifically disallows this,
     * but CPython implements it. Where the two differ, CPython wins.
     */
    if (exception_data->object != input) {
        if (!sky_object_isa(exception_data->object, sky_bytes_type)) {
            sky_error_raise_string(sky_TypeError,
                                   "exception attribute object must be bytes");
        }
        sky_buffer_acquire(&buffer,
                           exception_data->object,
                           SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        *bytes = buffer.buf;
        *nbytes = buffer.len;
    }

    if (new_offset < 0) {
        new_offset = *nbytes + new_offset;
    }
    if (new_offset < 0 || new_offset > *nbytes) {
        sky_error_raise_format(sky_IndexError,
                               "position %zd from error handler out of bounds",
                               new_offset);
    }

    if (replacement_data->len) {
        if (replacement_data->highest_codepoint < 0x100) {
            output(replacement_data->data.latin1,
                   replacement_data->len,
                   1,
                   output_arg);
        }
        else if (replacement_data->highest_codepoint < 0x10000) {
            output(replacement_data->data.ucs2,
                   replacement_data->len * 2,
                   2,
                   output_arg);
        }
        else {
            output(replacement_data->data.ucs4,
                   replacement_data->len * 4,
                   4,
                   output_arg);
        }
    }

    return new_offset;
}


#define SKY_CODEC_DECODE_ERROR(_c, _t, _s, _e, _r)                          \
    do {                                                                    \
        ssize_t new_offset;                                                 \
                                                                            \
        new_offset = sky_codec_decode_error(&exception,                     \
                                            sky_codec_##_c,                 \
                                            encoding,                       \
                                            errors,                         \
                                            &bytes,                         \
                                            &nbytes,                        \
                                            output,                         \
                                            output_arg,                     \
                                            (_s) - (const _t *)bytes,       \
                                            (_e) - (const _t *)bytes,       \
                                            _r);                            \
        c = (start = (const _t *)bytes + new_offset) - 1;                   \
        end = (const _t *)((const char *)bytes + (nbytes / sizeof(_t)));    \
    } while (0)


ssize_t
sky_codec_decode_ascii(SKY_UNUSED void *        state,
                       sky_string_t             encoding,
                       sky_string_t             errors,
                       sky_codec_output_t       output,
                       void *                   output_arg,
                       SKY_UNUSED uint32_t *    flags,
                       const void *             bytes,
                       ssize_t                  nbytes,
                       SKY_UNUSED sky_bool_t    final)
{
    sky_object_t    exception;
    const uint8_t   *c, *end, *start;

    exception = NULL;
    start = bytes;
    end = start + nbytes;

    for (c = start; c < end; ++c) {
        if (*c > 0x7F) {
            output(start, c - start, 1, output_arg);
            SKY_CODEC_DECODE_ERROR(ascii, uint8_t, c, c + 1,
                                   "ordinal not in range(128)");
        }
    }
    output(start, c - start, 1, output_arg);

    return nbytes;
}


ssize_t
sky_codec_decode_charmap(void *             state,
                         sky_string_t       encoding,
                         sky_string_t       errors,
                         sky_codec_output_t output,
                         void *             output_arg,
                         uint32_t *         flags,
                         const void *       bytes,
                         ssize_t            nbytes,
                         sky_bool_t         final)
{
    sky_object_t        exception, mapping;
    const uint8_t       *c, *end, *start;
    sky_string_data_t   *string_data, tagged_data;

    if (!(mapping = state)) {
        return sky_codec_decode_latin1(state,
                                       encoding,
                                       errors,
                                       output,
                                       output_arg,
                                       flags,
                                       bytes,
                                       nbytes,
                                       final);
    }

    exception = NULL;
    start = bytes;
    end = start + nbytes;

    if (sky_string_type == sky_object_type(mapping)) {
        string_data = sky_string_data(mapping, &tagged_data);
        if (string_data->highest_codepoint < 0x100) {
            uint8_t cp;

            for (c = start; c < end; ++c) {
                if (*c >= string_data->len) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                    continue;
                }
                cp = string_data->data.latin1[*c];
                if (cp == *c) {
                    continue;
                }
                output(start, c - start, 1, output_arg);
                output(&cp, 1, 1, output_arg);
                start = c + 1;
            }
        }
        else if (string_data->highest_codepoint < 0xFFFE) {
            uint16_t    cp;

            for (c = start; c < end; ++c) {
                if (*c >= string_data->len) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                    continue;
                }
                cp = string_data->data.ucs2[*c];
                if (cp == *c) {
                    continue;
                }
                output(start, c - start, 1, output_arg);
                output(&cp, 2, 2, output_arg);
                start = c + 1;
            }
        }
        else if (string_data->highest_codepoint < 0x10000) {
            uint16_t    cp;

            for (c = start; c < end; ++c) {
                if (*c >= string_data->len) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                    continue;
                }
                cp = string_data->data.ucs2[*c];
                if (cp == *c) {
                    continue;
                }
                output(start, c - start, 1, output_arg);
                if (0xFFFE == cp) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                }
                else {
                    output(&cp, 2, 2, output_arg);
                    start = c + 1;
                }
            }
        }
        else {
            sky_unicode_char_t  cp;

            for (c = start; c < end; ++c) {
                if (*c >= string_data->len) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                    continue;
                }
                cp = string_data->data.ucs4[*c];
                if (cp == *c) {
                    continue;
                }
                output(start, c - start, 1, output_arg);
                if (0xFFFE == cp) {
                    SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                           "character maps to <undefined>");
                }
                else {
                    output(&cp, 4, 4, output_arg);
                    start = c + 1;
                }
            }
        }
    }
    else {
        ssize_t             len;
        sky_object_t        o;
        sky_unicode_char_t  cp;

        for (c = start; c < end; ++c) {
            SKY_ERROR_TRY {
                o = sky_object_getitem(mapping, sky_integer_create(*c));
            } SKY_ERROR_EXCEPT(sky_LookupError) {
                o = NULL;
            } SKY_ERROR_TRY_END;
            if (sky_object_isa(o, sky_integer_type)) {
                cp = sky_integer_value(o, INT32_MIN, INT32_MAX, NULL);
                if (cp < SKY_UNICODE_CHAR_MIN || cp > SKY_UNICODE_CHAR_MAX) {
                    sky_error_raise_format(
                            sky_TypeError,
                            "character mapping must be in range(0x%x)",
                            SKY_UNICODE_CHAR_MAX);
                }
            }
            else if (sky_object_isnull(o)) {
                cp = 0xFFFE;
            }
            else if (sky_object_isa(o, sky_string_type)) {
                if (!(len = sky_object_len(o))) {
                    output(start, c - start, 1, output_arg);
                    start = c + 1;
                    continue;
                }
                o = sky_string_createfromobject(o);
                if (len > 1) {
                    output(start, c - start, 1, output_arg);
                    start = c + 1;

                    string_data = sky_string_data(o, &tagged_data);
                    if (string_data->highest_codepoint < 0x100) {
                        output(string_data->data.latin1,
                               string_data->len,
                               1,
                               output_arg);
                    }
                    else if (string_data->highest_codepoint < 0x10000) {
                        output(string_data->data.ucs2,
                               string_data->len * 2,
                               2,
                               output_arg);
                    }
                    else {
                        output(string_data->data.ucs4,
                               string_data->len * 4,
                               4,
                               output_arg);
                    }
                    continue;
                }
                cp = sky_string_charat(o, 0);
            }
            else {
                sky_error_raise_string(
                        sky_TypeError,
                        "character mapping must return integer, None or str");
            }
            if (cp == *c) {
                continue;
            }
            output(start, c - start, 1, output_arg);
            if (0xFFFE == cp) {
                SKY_CODEC_DECODE_ERROR(charmap, uint8_t, c, c + 1,
                                       "character maps to <undefined>");
            }
            else {
                output(&cp, 4, 4, output_arg);
                start = c + 1;
            }
        }
    }
    output(start, c - start, 1, output_arg);

    return nbytes;
}


ssize_t
sky_codec_decode_escape(SKY_UNUSED void *       state,
                        SKY_UNUSED sky_string_t encoding,
                        sky_string_t            errors,
                        sky_codec_output_t      output,
                        void *                  output_arg,
                        SKY_UNUSED uint32_t *   flags,
                        const void *            bytes,
                        ssize_t                 nbytes,
                        sky_bool_t              final)
{
    int             i;
    uint8_t         byte;
    const uint8_t   *c, *end, *start;

    /* As implemented by CPython, "escape" is not a true codec. It doesn't use
     * the normal exception machinery, and it produces bytes rather than a
     * string for output.
     */

    start = bytes;
    end = start + nbytes;

    for (c = start; c < end; ++c) {
        if (*c != '\\') {
            continue;
        }
        output(start, c - start, 1, output_arg);

        if (++c >= end) {
            --c;
            if (!final) {
                return c - (const uint8_t *)bytes;
            }
            /* CPython ignores the error handler here and always treats this as
             * a strict error.
             */
            sky_error_raise_string(sky_ValueError, "Trailing \\ in string");
        }
        i = 0;
        switch (*c++) {
            case '\n':
                goto skip_output;

            case '\\': case '\'': case '\"':
                byte = *(c - 1);
                break;

            case 'a': byte = 0x07; break;   /* BEL  */
            case 'b': byte = 0x08; break;   /* BS   */
            case 'f': byte = 0x0C; break;   /* FF   */
            case 'n': byte = 0x0A; break;   /* LF   */
            case 'r': byte = 0x0D; break;   /* CR   */
            case 't': byte = 0x09; break;   /* TAB  */
            case 'v': byte = 0x0B; break;   /* VT   */

            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7':
                byte = *(c - 1) - '0';
                for (i = 1; i < 3 && c < end; ++c, ++i) {
                    if (*c > 0x7F || !sky_ctype_isodigit(*c)) {
                        break;
                    }
                    byte = (byte << 3) | (*c - '0');
                }
                break;

            case 'x':
                byte = 0;
                for (i = 0; c < end && i < 2; ++c, ++i) {
                    if (*c >= 0x7F || !sky_ctype_isxdigit(*c)) {
                        break;
                    }
                    byte = (byte << 4) | sky_ctype_hexdigit(*c);
                }
                if (i != 2) {
                    if (!errors ||
                        sky_object_compare(errors,
                                           SKY_STRING_LITERAL("strict"),
                                           SKY_COMPARE_OP_EQUAL))
                    {
                        sky_error_raise_format(
                                sky_ValueError,
                                "invalid \\x escape at position %zd",
                                c - 2 - (const uint8_t *)bytes);
                    }
                    if (sky_object_compare(errors,
                                           SKY_STRING_LITERAL("replace"),
                                           SKY_COMPARE_OP_EQUAL))
                    {
                        output("?", 1, 1, output_arg);
                    }
                    else if (sky_object_compare(errors,
                                                SKY_STRING_LITERAL("ignore"),
                                                SKY_COMPARE_OP_NOT_EQUAL))
                    {
                        sky_error_raise_format(sky_ValueError,
                                               "decoding error; unknown "
                                               "error handling code: %#@",
                                               errors);
                    }
                    start = c--;
                    continue;
                }
                break;

            default:
                output("\\", 1, 1, output_arg);
                byte = *(c - 1);
                break;
        }

        output(&byte, 1, 1, output_arg);

skip_output:
        start = c--;
    }
    output(start, c - start, 1, output_arg);

    return c - (const uint8_t *)bytes;
}


ssize_t
sky_codec_decode_latin1(SKY_UNUSED void *       state,
                        SKY_UNUSED sky_string_t encoding,
                        SKY_UNUSED sky_string_t errors,
                        sky_codec_output_t      output,
                        void *                  output_arg,
                        SKY_UNUSED uint32_t *   flags,
                        const void *            bytes,
                        ssize_t                 nbytes,
                        SKY_UNUSED sky_bool_t   final)
{
    output(bytes, nbytes, 1, output_arg);
    return nbytes;
}


ssize_t
sky_codec_decode_locale(SKY_UNUSED void *       state,
                        sky_string_t            encoding,
                        sky_string_t            errors,
                        sky_codec_output_t      output,
                        void *                  output_arg,
                        SKY_UNUSED uint32_t *   flags,
                        const void *            bytes,
                        ssize_t                 nbytes,
                        sky_bool_t              final)
{
    static const size_t width = sizeof(wchar_t);

    size_t          rc, x;
    wchar_t         wcs[64], wc;
    mbstate_t       ps;
    const char      *c, *end, *start;
    sky_object_t    exception;

    x = 0;
    memset(&ps, 0, sizeof(ps));
    exception = NULL;
    start = c = bytes;
    end = start + nbytes;

    while (c < end) {
        rc = mbrtowc(&wc, c, end - c, &ps);
        if (!rc) {
            break;
        }
        if (rc > 0) {
            if (wc >= 0xD800 && wc <= 0xDFFF) {
                if (x > 0) {
                    output(wcs, x * width, width, output_arg);
                    x = 0;
                }
                SKY_CODEC_DECODE_ERROR(locale, char, c, c + rc,
                                       "illegal multibyte sequence");
                ++c;    
                continue;
            }
            if (x >= sizeof(wcs) / sizeof(wcs[0])) {
                output(wcs, x * width, width, output_arg);
                x = 0;
            }
            wcs[x++] = wc;
            c += rc;
            continue;
        }
        if ((size_t)-2 == rc) {
            if (x > 0) {
                output(wcs, x * width, width, output_arg);
                x = 0;
            }
            if (!final) {
                break;
            }
            SKY_CODEC_DECODE_ERROR(locale, char, c, c + 1,
                                   "unexpected end of data");
            ++c;
            continue;
        }
        if ((size_t)-1 == rc) {
            if (x > 0) {
                output(wcs, x * width, width, output_arg);
                x = 0;
            }
            SKY_CODEC_DECODE_ERROR(locale, char, c, c + 1,
                                   "illegal multibyte sequence");
            ++c;
            continue;
        }
        sky_error_fatal("internal error");
    }
    if (x > 0) {
        output(wcs, x * width, width, output_arg);
    }

    return c - (const char *)bytes;
}


ssize_t
sky_codec_decode_raw_unicode_escape(SKY_UNUSED void *       state,
                                    sky_string_t            encoding,
                                    sky_string_t            errors,
                                    sky_codec_output_t      output,
                                    void *                  output_arg,
                                    SKY_UNUSED uint32_t *   flags,
                                    const void *            bytes,
                                    ssize_t                 nbytes,
                                    sky_bool_t              final)
{
    int             i, n;
    uint32_t        cp;
    sky_object_t    exception;
    const uint8_t   *c, *end, *start;

    exception = NULL;
    start = bytes;
    end = start + nbytes;

    for (c = start; c < end; ++c) {
        if (*c != '\\') {
            continue;
        }

        output(start, c - start, 1, output_arg);
        if (++c >= end) {
            output(c - 1, 1, 1, output_arg);
            return c - (const uint8_t *)bytes;
        }
        if (*c != 'u' && *c != 'U') {
            start = c + 1;
            output(c - 1, 2, 1, output_arg);
            continue;
        }

        start = c - 1;
        n = (*c++ == 'u' ? 4 : 8);

        cp = 0;
        for (i = 0; i < n && c < end; ++c, ++i) {
            if (*c > 0x7F || !sky_ctype_isxdigit(*c)) {
                break;
            }
            cp = (cp << 4) | sky_ctype_hexdigit(*c);
        }
        if (i < n) {
            if (c >= end && !final) {
                return start - (const uint8_t *)bytes;
            }
            SKY_CODEC_DECODE_ERROR(raw_unicode_escape, uint8_t, start, c,
                                   ('u' == *c ? "truncated \\uXXXX escape"
                                              : "truncated \\Uxxxxxxxx escape"));
            continue;
        }
        if (cp > SKY_UNICODE_CHAR_MAX) {
            SKY_CODEC_DECODE_ERROR(raw_unicode_escape, uint8_t, start, c,
                                   "\\Uxxxxxxxx out of range");
            continue;
        }

        output(&cp, sizeof(cp), sizeof(cp), output_arg);
        start = c--;
    }
    output(start, c - start, 1, output_arg);

    return c - (const uint8_t *)bytes;
}


ssize_t
sky_codec_decode_unicode_escape(SKY_UNUSED void *       state,
                                sky_string_t            encoding,
                                sky_string_t            errors,
                                sky_codec_output_t      output,
                                void *                  output_arg,
                                SKY_UNUSED uint32_t *   flags,
                                const void *            bytes,
                                ssize_t                 nbytes,
                                sky_bool_t              final)
{
    int                 i, width;
    const char          *error;
    sky_object_t        exception;
    const uint8_t       *c, *end, *start;
    sky_unicode_char_t  cp;

    exception = NULL;
    start = bytes;
    end = start + nbytes;

    for (c = start; c < end; ++c) {
        if (*c != '\\') {
            continue;
        }
        output(start, c - start, 1, output_arg);

        start = c;  /* save this for \x, \u, and \U */
        if (++c >= end) {
            --c;
            if (!final) {
                return c - (const uint8_t *)bytes;
            }
            SKY_CODEC_DECODE_ERROR(unicode_escape, uint8_t, c, end,
                                   "\\ at end of string");
            continue;
        }
        i = 0;
        switch (*c++) {
            case '\n':
                goto skip_output;

            case '\\': case '\'': case '\"':
                cp = *(c - 1);
                break;

            case 'a': cp = 0x07; break;     /* BEL  */
            case 'b': cp = 0x08; break;     /* BS   */
            case 'f': cp = 0x0C; break;     /* FF   */
            case 'n': cp = 0x0A; break;     /* LF   */
            case 'r': cp = 0x0D; break;     /* CR   */
            case 't': cp = 0x09; break;     /* TAB  */
            case 'v': cp = 0x0B; break;     /* VT   */

            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7':
                cp = *(c - 1) - '0';
                for (i = 1; i < 3 && c < end; ++c, ++i) {
                    if (*c > 0x7F || !sky_ctype_isodigit(*c)) {
                        break;
                    }
                    cp = (cp << 3) | (*c - '0');
                }
                cp %= 256;
                break;

            case 'u':
                width = 2;
                error = "truncated \\uXXXX escape";
                goto hexescape;

            case 'U':
                width = 4;
                error = "truncated \\Uxxxxxxxx escape";
                goto hexescape;

            case 'x':
                width = 1;
                error = "truncated \\xXX escape";
hexescape:
                cp = 0;
                for (i = 0; c < end && i < width * 2; ++c, ++i) {
                    if (*c >= 0x7F || !sky_ctype_isxdigit(*c)) {
                        break;
                    }
                    cp = (cp << 4) | sky_ctype_hexdigit(*c);
                }
                if (i != width * 2) {
                    SKY_CODEC_DECODE_ERROR(unicode_escape, uint8_t, start, c,
                                           error);
                    continue;
                }
                if (cp < SKY_UNICODE_CHAR_MIN) {
                    SKY_CODEC_DECODE_ERROR(unicode_escape, uint8_t, start, c,
                                           "illegal Unicode character");
                    continue;
                }
                if (cp > SKY_UNICODE_CHAR_MAX) {
                    SKY_CODEC_DECODE_ERROR(unicode_escape, uint8_t, start, c,
                                           "\\Uxxxxxxxx out of range");
                    continue;
                }
                break;

            default:
                output("\\", 1, 1, output_arg);
                cp = *(c - 1);
                break;
        }

        output(&cp, sizeof(cp), sizeof(cp), output_arg);

skip_output:
        start = c--;
    }
    output(start, c - start, 1, output_arg);

    return c - (const uint8_t *)bytes;
}


ssize_t
sky_codec_decode_unicode_internal(SKY_UNUSED void *     state,
                                  sky_string_t          encoding,
                                  sky_string_t          errors,
                                  sky_codec_output_t    output,
                                  void *                output_arg,
                                  SKY_UNUSED uint32_t * flags,
                                  const void *          bytes,
                                  ssize_t               nbytes,
                                  sky_bool_t            final)
{
    sky_object_t                exception;
    sky_unicode_char_t          cp;
    const sky_unicode_char_t    *c, *end, *start;

    /* Built-in decoders require properly aligned input. */
    sky_error_validate_debug(!((uintptr_t)bytes & 0x3));

    sky_error_warn_string(sky_DeprecationWarning,
                          "unicode_internal codec has been deprecated");

    exception = NULL;
    start = c = bytes;
    end = start + (nbytes / sizeof(sky_unicode_char_t));

    for (;;) {
        if (++c > end) {
            --c;
            output(start, (c - start) * 4, 4, output_arg);
            if (final && (char *)end < (char *)bytes + nbytes) {
                SKY_CODEC_DECODE_ERROR(unicode_internal, sky_unicode_char_t,
                                       c, end,
                                       "truncated input");
                continue;
            }
            return (const char *)c - (const char *)bytes;
        }
        if (*c > SKY_UNICODE_CHAR_MAX) {
            output(start, (c - start) * 4, 4, output_arg);
            SKY_CODEC_DECODE_ERROR(unicode_internal, sky_unicode_char_t,
                                   c, c + 1,
                                   "illegal code point (> 0x10FFFF)");
            continue;
        }
        if (*c >= 0xD800 && *c <= 0xDFFF) {
            output(start, (c - start) * 4, 4, output_arg);
            if (c + 1 >= end) {
                if (!final) {
                    return (const char *)c - (const char *)bytes;
                }
                SKY_CODEC_DECODE_ERROR(unicode_internal, sky_unicode_char_t,
                                       c, end,
                                       "truncated input");
                continue;
            }
            if (*c > 0xDBFF || (c[1] < 0xDC00 || c[1] > 0xDFFF)) {
                SKY_CODEC_DECODE_ERROR(unicode_internal, sky_unicode_char_t,
                                       c, c + 1,
                                       "illegal encoding");
                continue;
            }
            cp = (((c[0] & 0x3FF) << 10) | (c[1] & 0x3FF)) + 0x10000;
            output(&cp, sizeof(cp), sizeof(cp), output_arg);
            start = ++c + 1;
            continue;
        }
    }

    sky_error_fatal("unreachable");
}


static sky_string_t
sky_codec_decode_unknown(sky_string_t       encoding,
                         sky_string_t       errors,
                         sky_codec_output_t output,
                         void *             output_arg,
                         uint32_t *         flags,
                         sky_bytes_t        input,
                         ssize_t *          consumed)
{
    sky_tuple_t         args, state, tuple;
    sky_object_t        codec, decoder, object;
    sky_string_data_t   *string_data, tagged_data;

    if (consumed) {
        if (flags && (*flags & SKY_CODEC_DECODE_FLAG_REQUIRE_TEXT)) {
            codec = sky_codec_lookup_text(encoding, "codecs.decode()");
        }
        else {
            codec = sky_codec_lookup(encoding);
        }
        decoder = sky_codec_incremental_decoder(codec, errors);
        object = sky_object_callmethod(decoder,
                                       SKY_STRING_LITERAL("decode"),
                                       sky_tuple_pack(1, input),
                                       NULL);
    }
    else {
        if (flags && (*flags & SKY_CODEC_DECODE_FLAG_REQUIRE_TEXT)) {
            decoder = sky_codec_text_decoder(encoding);
        }
        else {
            decoder = sky_codec_decoder(encoding);
        }
        args = sky_tuple_pack((sky_object_isnull(errors) ? 1 : 2),
                              input,
                              errors);
        tuple = sky_object_call(decoder, args, NULL);
        if (!sky_object_isa(tuple, sky_tuple_type)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "decoder must return a tuple (object, integer)");
        }
        object = sky_tuple_get(tuple, 0);
    }

    if (consumed) {
        state = sky_object_callmethod(decoder,
                                      SKY_STRING_LITERAL("getstate"),
                                      NULL,
                                      NULL);
        *consumed = sky_integer_value(sky_tuple_get(state, 1),
                                      SSIZE_MIN,
                                      SSIZE_MAX,
                                      NULL);
        if (*consumed < 0) {
            sky_error_raise_format(
                    sky_ValueError,
                    "decoder for encoding %@ returned consumed bytes < 0",
                    encoding);
        }
        if (*consumed > sky_object_len(input)) {
            sky_error_raise_format(
                    sky_ValueError,
                    "decoder for encoding %@ returned consumed bytes > %zd",
                    encoding,
                    sky_object_len(input));
        }
    }

    if (output) {
        if (sky_object_isa(object, sky_string_type)) {
            string_data = sky_string_data(sky_string_createfromobject(object),
                                          &tagged_data);
            if (string_data->len > 0) {
                if (string_data->highest_codepoint < 0x100) {
                    output(string_data->data.latin1,
                           string_data->len,
                           1,
                           output_arg);
                }
                else if (string_data->highest_codepoint < 0x10000) {
                    output(string_data->data.ucs2,
                           string_data->len * 2,
                           2,
                           output_arg);
                }
                else {
                    output(string_data->data.ucs4,
                           string_data->len * 4,
                           4,
                           output_arg);
                }
            }
        }
        else {
            SKY_ASSET_BLOCK_BEGIN {
                sky_buffer_t    buffer;

                sky_buffer_acquire(&buffer, object, SKY_BUFFER_FLAG_SIMPLE);
                sky_asset_save(&buffer,
                               (sky_free_t)sky_buffer_release,
                               SKY_ASSET_CLEANUP_ALWAYS);

                if (buffer.len > 0) {
                    output(buffer.buf, buffer.len, 1, output_arg);
                }
            } SKY_ASSET_BLOCK_END;
        }
    }

    return object;
}


ssize_t
sky_codec_decode_utf7(SKY_UNUSED void *     state,
                      sky_string_t          encoding,
                      sky_string_t          errors,
                      sky_codec_output_t    output,
                      void *                output_arg,
                      SKY_UNUSED uint32_t * flags,
                      const void *          bytes,
                      ssize_t               nbytes,
                      sky_bool_t            final)
{
    int                 nbits;
    uint32_t            bits;
    sky_object_t        exception;
    const uint8_t       *c, *end, *start;
    sky_unicode_char_t  cp, surrogate;

    exception = NULL;
    start = bytes;
    end = start + nbytes;
    surrogate = 0;

restart:
    for (c = start; c < end; ++c) {
        if ('+' == *c) {
            output(start, c - start, 1, output_arg);
            if (c + 1 >= end) {
                if (!final) {
                    return c - (const uint8_t *)bytes;
                }
                SKY_CODEC_DECODE_ERROR(utf7, uint8_t, c, end,
                                       "unexpected end of data");
                continue;
            }
            if ('-' == c[1]) {
                if (surrogate) {
                    SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                                           "illegal surrogate encoding");
                    surrogate = 0;
                    goto restart;
                }
                output(c++, 1, 1, output_arg);
                start = c + 1;
                continue;
            }
            if (!sky_ctype_isbase64(c[1])) {
                SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                                       "illegal encoding");
                continue;
            }

            /* Base64 encoding follows, which may encode an arbitrary number of
             * characters, including surrogates. The encoding terminates with
             * either end of input or a non-base64 character. For a stateful
             * decode, end of input cannot be considered termination, because
             * more data may follow; therefore, nothing can be output until the
             * true end of encoding is known. Scan forward to find it, or exit
             * if end of input is reached. This is not necessary for a
             * stateless decode, because end of input is also end of encoding.
             */
            start = c;
            if (!final) {
                while (++c < end && sky_ctype_isbase64(*c));
                if (c >= end) {
                    return start - (const uint8_t *)bytes;
                }
                c = start;
            }

            /* Decode the base64 encoding 16 bits at a time. */
            bits = 0;
            nbits = 0;
            while (++c < end && sky_ctype_isbase64(*c)) {
                bits = (bits << 6) | sky_ctype_base64digit(*c);
                nbits += 6;
                if (nbits >= 16) {
                    cp = (bits >> (nbits - 16));
                    nbits -= 16;
                    bits &= (1 << nbits) - 1;
                    sky_error_validate_debug(cp <= 0xFFFF);

                    if (surrogate) {
                        if (cp < 0xDC00 || cp > 0xDFFF) {
                            surrogate = 0;
                            SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                                                   "illegal surrogate encoding");
                            goto restart;
                        }
                        cp = (((surrogate & 0x3FF) << 10) |
                             (cp & 0x3FF)) + 0x10000;
                        output(&cp, sizeof(cp), sizeof(cp), output_arg);
                        surrogate = 0;
                    }
                    else if (cp >= 0xD800 && cp <= 0xDFFF) {
                        if (cp > 0xDBFF) {
                            SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                                                   "illegal surrogate encoding");
                            goto restart;
                        }
                        surrogate = cp;
                    }
                    else {
                        output(&cp, sizeof(cp), sizeof(cp), output_arg);
                    }
                }
            }

            /* A trailing '-' after a base64 sequence is absorbed, but anything
             * else must be preserved.
             */
            if (c < end && '-' == *c) {
                ++c;
            }
            if (nbits > 0) {
                if (nbits >= 6) {
                    SKY_CODEC_DECODE_ERROR(
                            utf7, uint8_t, start, c,
                            "partial character in shift sequence");
                    continue;
                }
                if (bits != 0) {
                    SKY_CODEC_DECODE_ERROR(
                            utf7, uint8_t, start, c,
                            "non-zero padding bits in shift sequence");
                    continue;
                }
            }

            start = c--;
            continue;
        }
        if (surrogate) {
            surrogate = 0;
            SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                                   "illegal surrogate encoding");
            goto restart;
        }
        if (*c > 0x7F) {
            output(start, c - start, 1, output_arg);
            SKY_CODEC_DECODE_ERROR(utf7, uint8_t, c, c + 1,
                                   "unexpected special character");
            continue;
        }
        /* anything else is a direct character */
    }
    if (surrogate) {
        surrogate = 0;
        SKY_CODEC_DECODE_ERROR(utf7, uint8_t, start, c,
                               "illegal surrogate encoding");
        goto restart;
    }
    output(start, c - start, 1, output_arg);

    return c - (const uint8_t *)bytes;
}


ssize_t
sky_codec_decode_utf8(SKY_UNUSED void *     state,
                      sky_string_t          encoding,
                      sky_string_t          errors,
                      sky_codec_output_t    output,
                      void *                output_arg,
                      SKY_UNUSED uint32_t * flags,
                      const void *          bytes,
                      ssize_t               nbytes,
                      sky_bool_t            final)
{
    ssize_t             n;
    const char          *reason;
    sky_object_t        exception;
    const uint8_t       *end, *c, *start;
    sky_unicode_char_t  cp;

    exception = NULL;
    c = bytes;
    end = c + nbytes;

    while (c < end) {
        if (*c < 0x80) {
            for (start = c; c < end && *c < 0x80; ++c);
            output(start, c - start, 1, output_arg);
            if (c >= end) {
                return c - (const uint8_t *)bytes;
            }
        }

        if (*c < 0xC2 || *c > 0xF4) {
            reason = "invalid start byte";
            goto error;
        }

#define IS_CONTINUATION_BYTE(ch) ((ch) >= 0x80 && (ch) < 0xC0)

        if (*c < 0xE0) {
            /* \xC2\x80..\xDF\xBF => U+0080..07FF */
            if (end - c < 2) {
                if (!final) {
                    return c - (const uint8_t *)bytes;
                }
                reason = "unexpected end of data";
                goto error;
            }
            if (!IS_CONTINUATION_BYTE(c[1])) {
                reason = "invalid continuation byte";
                goto error;
            }

            cp = ((c[0] & 0x1F) << 6) + (c[1] & 0x3F);
            sky_error_validate_debug(cp >= 0x0080 && cp <= 0x07FF);
            n = 2;
        }
        else if (*c < 0xF0) {
            /* \xE0\xA0\x80..\xEF\xBF\xBF => U+0800..FFFF */
            if (end - c < 3) {
                if (end - c < 2) {
                    if (!final) {
                        return c - (const uint8_t *)bytes;
                    }
                    reason = "unexpected end of data";
                    goto error;
                }
                if (!IS_CONTINUATION_BYTE(c[1]) ||
                    (c[1] < 0xA0 ? *c == 0xE0 : *c == 0xED))
                {
                    reason = "invalid continuation byte";
                    goto error;
                }
                if (!final) {
                    return c - (const uint8_t *)bytes;
                }
                reason = "unexpected end of data";
                goto error;
            }
            if (!IS_CONTINUATION_BYTE(c[1]) ||
                (0xE0 == *c && c[1] < 0xA0) ||
                (0xED == *c && c[1] >= 0xA0) ||
                !IS_CONTINUATION_BYTE(c[2]))
            {
                reason = "invalid continuation byte";
                goto error;
            }

            cp = ((c[0] & 0x0F) << 12) |
                 ((c[1] & 0x3F) <<  6) |
                  (c[2] & 0x3F);
            sky_error_validate_debug(cp >= 0x0800 && cp <= 0xFFFF);
            sky_error_validate_debug(cp < 0xD800 || cp > 0xDFFF);
            n = 3;
        }
        else if (*c < 0xF5) {
            /* \xF0\x90\x80\x80..\xF4\x8F\xBF\xBF => U+10000..10FFFF */
            if (end - c < 4) {
                if (end - c < 2) {
                    if (!final) {
                        return c - (const uint8_t *)bytes;
                    }
                    reason = "unexpected end of data";
                    goto error;
                }
                if (!IS_CONTINUATION_BYTE(c[1]) ||
                    (c[1] < 0x90 ? 0xF0 == *c : 0xF4 == *c))
                {
                    reason = "invalid continuation byte";
                    goto error;
                }
                if (end - c < 3) {
                    if (!final) {
                        return c - (const uint8_t *)bytes;
                    }
                    reason = "unexpected end of data";
                    goto error;
                }
                if (!IS_CONTINUATION_BYTE(c[2])) {
                    reason = "invalid continuation byte";
                    goto error;
                }
                if (!final) {
                    return c - (const uint8_t *)bytes;
                }
                reason = "unexpected end of data";
                goto error;
            }
            if (!IS_CONTINUATION_BYTE(c[1]) ||
                (0xF0 == *c && c[1] < 0x90) ||
                (0xF4 == *c && c[1] >= 0x90) ||
                !IS_CONTINUATION_BYTE(c[2]) ||
                !IS_CONTINUATION_BYTE(c[3]))
            {
                reason = "invalid continuation byte";
                goto error;
            }

            cp = ((c[0] & 0x07) << 18) |
                 ((c[1] & 0x3F) << 12) |
                 ((c[2] & 0x3F) <<  6) |
                  (c[3] & 0x3F);
            sky_error_validate_debug(cp >= 0x10000 && cp <= 0x10FFFF);
            n = 4;
        }

#undef IS_CONTINUATION_BYTE

        c += n;
        output(&cp, sizeof(cp), sizeof(cp), output_arg);
        continue;

error:
        SKY_CODEC_DECODE_ERROR(utf8, uint8_t, c, c + 1, reason);
        ++c;
    }

    return c - (const uint8_t *)bytes;
}


#define sky_codec_decode_utf16_error(_c, _e, _r)                            \
    do {                                                                    \
        ssize_t new_offset;                                                 \
                                                                            \
        new_offset = sky_codec_decode_error(                                \
                            &exception,                                     \
                            sky_codec_##_c,                                 \
                            encoding,                                       \
                            errors,                                         \
                            &bytes,                                         \
                            &nbytes,                                        \
                            output,                                         \
                            output_arg,                                     \
                            (const uint8_t *)c - (const uint8_t *)bytes,    \
                            (const uint8_t *)(_e) - (const uint8_t *)bytes, \
                            _r);                                            \
        c = (const uint16_t *)((const uint8_t *)bytes + new_offset);        \
        end = (const uint16_t *)bytes + (nbytes / sizeof(uint16_t));        \
    } while (0)


void *
sky_codec_decode_utf16_be_initialize(SKY_UNUSED sky_string_t        encoding,
                                     SKY_UNUSED sky_string_t        errors,
                                     SKY_UNUSED sky_codec_output_t  output,
                                     SKY_UNUSED void *              output_arg,
                                     uint32_t *                     flags)
{
    if (flags) {
        *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
        *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
    }
    return NULL;
}

ssize_t
sky_codec_decode_utf16_be(SKY_UNUSED void *     state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          SKY_UNUSED uint32_t * flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final)
{
#if SKY_ENDIAN != SKY_ENDIAN_BIG
    uint16_t            buffer[256], *outcp = buffer;
#else
    const uint16_t      *start;
#endif
    sky_object_t        exception;
    const uint16_t      *end, *c;
    sky_unicode_char_t  cp, cp2;

    exception = NULL;
    c = (const uint16_t *)bytes;
    end = c + (nbytes / sizeof(uint16_t));

    for (;;) {
        if (c >= end) {
            if ((nbytes & 1) && final) {
                sky_codec_decode_utf16_error(utf16_be,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
            }
            break;
        }
        cp = (sky_unicode_char_t)sky_endian_btoh16(*c);
        if (cp < 0xD800 || cp > 0xDFFF) {
#if SKY_ENDIAN != SKY_ENDIAN_BIG
            *outcp++ = cp;
            if (&(buffer[256]) == outcp) {
                output(buffer, (outcp - buffer) * 2, 2, output_arg);
                outcp = buffer;
            }
#else
            start = c;
#endif
            for (++c; c < end; ++c) {
                cp = (sky_unicode_char_t)sky_endian_btoh16(*c);
                if (cp >= 0xD800 && cp <= 0xDFFF) {
                    break;
                }
#if SKY_ENDIAN != SKY_ENDIAN_BIG
                *outcp++ = cp;
                if (&(buffer[256]) == outcp) {
                    output(buffer, (outcp - buffer) * 2, 2, output_arg);
                    outcp = buffer;
                }
#endif
            }
#if SKY_ENDIAN == SKY_ENDIAN_BIG
            output(start, (c - start) * 2, 2, output_arg);
#else
            if (outcp > buffer) {
                output(buffer, (outcp - buffer) * 2, 2, output_arg);
                outcp = buffer;
            }
#endif
        }
        if (cp >= 0xD800 && cp <= 0xDFFF) {
            if (cp > 0xDBFF) {
                sky_codec_decode_utf16_error(utf16_be, c + 1,
                                             "illegal encoding");
                continue;
            }
            if (++c >= end) {
                --c;
                if (!final) {
                    break;
                }
                sky_codec_decode_utf16_error(utf16_be,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
                if (c >= end) {
                    break;
                }
                continue;
            }

            cp2 = (sky_unicode_char_t)sky_endian_btoh16(*c);
            if (cp2 < 0xDC00 || cp2 > 0xDFFF) {
                --c;
                sky_codec_decode_utf16_error(utf16_be, c + 1,
                                             "illegal UTF-16 surrogate");
                continue;
            }
            cp = (((cp & 0x3FF) << 10) | (cp2 & 0x3FF)) + 0x10000;
            ++c;

            output(&cp, sizeof(cp), sizeof(cp), output_arg);
        }
    }

    if ((const char *)c > (const char *)bytes + nbytes) {
        return nbytes;
    }
    return (const char *)c - (const char *)bytes;
}


void *
sky_codec_decode_utf16_le_initialize(SKY_UNUSED sky_string_t        encoding,
                                     SKY_UNUSED sky_string_t        errors,
                                     SKY_UNUSED sky_codec_output_t  output,
                                     SKY_UNUSED void *              output_arg,
                                     uint32_t *                     flags)
{
    if (flags) {
        *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
        *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
    }
    return NULL;
}

ssize_t
sky_codec_decode_utf16_le(SKY_UNUSED void *     state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          SKY_UNUSED uint32_t * flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final)
{
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
    uint16_t            buffer[256], *outcp = buffer;
#endif
    sky_object_t        exception;
    const uint16_t      *end, *c, *start;
    sky_unicode_char_t  cp, cp2;

    exception = NULL;
    c = (const uint16_t *)bytes;
    end = c + (nbytes / sizeof(uint16_t));

    for (;;) {
        if (c >= end) {
            if ((nbytes & 1) && final) {
                sky_codec_decode_utf16_error(utf16_le,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
            }
            break;
        }
        cp = (sky_unicode_char_t)sky_endian_ltoh16(*c);
        if (cp < 0xD800 || cp > 0xDFFF) {
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
            *outcp++ = cp;
            if (&(buffer[256]) == outcp) {
                output(buffer, (outcp - buffer) * 2, 2, output_arg);
                outcp = buffer;
            }
#endif
            for (start = c++; c < end; ++c) {
                cp = (sky_unicode_char_t)sky_endian_ltoh16(*c);
                if (cp >= 0xD800 && cp <= 0xDFFF) {
                    break;
                }
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
                *outcp++ = cp;
                if (&(buffer[256]) == outcp) {
                    output(buffer, (outcp - buffer) * 2, 2, output_arg);
                    outcp = buffer;
                }
#endif
            }
#if SKY_ENDIAN == SKY_ENDIAN_LITTLE
            output(start, (c - start) * 2, 2, output_arg);
#else
            if (outcp > buffer) {
                output(buffer, (outcp - buffer) * 2, 2, output_arg);
                outcp = buffer;
            }
#endif
        }
        if (cp >= 0xD800 && cp <= 0xDFFF) {
            if (cp > 0xDBFF) {
                sky_codec_decode_utf16_error(utf16_le, c + 1,
                                             "illegal encoding");
                continue;
            }
            if (++c >= end) {
                --c;
                if (!final) {
                    break;
                }
                sky_codec_decode_utf16_error(utf16_le,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
                if (c >= end) {
                    break;
                }
            }

            cp2 = (sky_unicode_char_t)sky_endian_ltoh16(*c);
            if (cp2 < 0xDC00 || cp2 > 0xDFFF) {
                --c;
                sky_codec_decode_utf16_error(utf16_le, c + 1,
                                             "illegal UTF-16 surrogate");
                continue;
            }
            cp = (((cp & 0x3FF) << 10) | (cp2 & 0x3FF)) + 0x10000;
            ++c;

            output(&cp, sizeof(cp), sizeof(cp), output_arg);
        }
    }

    if ((const char *)c > (const char *)bytes + nbytes) {
        return nbytes;
    }
    return (const char *)c - (const char *)bytes;
}


ssize_t
sky_codec_decode_utf16(void *               state,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       uint32_t *           flags,
                       const void *         bytes,
                       ssize_t              nbytes,
                       sky_bool_t           final)
{
    ssize_t                     consumed, decoder_consumed;
    uint32_t                    dummy_flags;
    const void                  *original_bytes;
    sky_codec_decoder_decode_t  decoder;

    /* Built-in decoders require properly aligned input. */
    sky_error_validate_debug(!((uintptr_t)bytes & 0x1));

    if (!flags) {
        flags = &dummy_flags;
        dummy_flags = 0;
    }
    consumed = 0;

    if (nbytes >= 2) {
        const uint8_t   *b = (const uint8_t *)bytes;

        if (0xFF == b[0] && 0xFE == b[1]) {
            bytes += 2;
            nbytes -= 2;
            consumed += 2;
            *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
            *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
        }
        else if (0xFE == b[0] && 0xFF == b[1]) {
            bytes += 2;
            nbytes -= 2;
            consumed += 2;
            *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
            *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
        }
    }

    if (*flags & SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE) {
        decoder = sky_codec_decode_utf16_le;
    }
    else if (*flags & SKY_CODEC_DECODE_FLAG_ENDIAN_BIG) {
        decoder = sky_codec_decode_utf16_be;
    }
    else {
#if SKY_ENDIAN == SKY_ENDIAN_BIG
        decoder = sky_codec_decode_utf16_be;
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
        decoder = sky_codec_decode_utf16_le;
#else
#   error implementation missing
#endif
    }

    original_bytes = bytes;
    decoder_consumed = decoder(state,
                               encoding,
                               errors,
                               output,
                               output_arg,
                               flags,
                               bytes,
                               nbytes,
                               final);
    if (bytes != original_bytes) {
        return decoder_consumed;
    }
    return consumed + decoder_consumed;
}


#define sky_codec_decode_utf32_error(_c, _e, _r)                            \
    do {                                                                    \
        ssize_t new_offset;                                                 \
                                                                            \
        new_offset = sky_codec_decode_error(                                \
                            &exception,                                     \
                            sky_codec_##_c,                                 \
                            encoding,                                       \
                            errors,                                         \
                            &bytes,                                         \
                            &nbytes,                                        \
                            output,                                         \
                            output_arg,                                     \
                            (const uint8_t *)c - (const uint8_t *)bytes,    \
                            (const uint8_t *)(_e) - (const uint8_t *)bytes, \
                            _r);                                            \
        c = (const uint32_t *)(const uint8_t *)bytes + new_offset;          \
        end = (const uint32_t *)bytes + (nbytes / sizeof(uint32_t));        \
    } while (0)


void *
sky_codec_decode_utf32_be_initialize(SKY_UNUSED sky_string_t        encoding,
                                     SKY_UNUSED sky_string_t        errors,
                                     SKY_UNUSED sky_codec_output_t  output,
                                     SKY_UNUSED void *              output_arg,
                                     uint32_t *                     flags)
{
    if (flags) {
        *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
        *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
    }
    return NULL;
}

ssize_t
sky_codec_decode_utf32_be(SKY_UNUSED void *     state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          SKY_UNUSED uint32_t * flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final)
{
    static char not_in_range_error[36]; /* Room for 32-bit value MAX */

    sky_object_t        exception;
    const uint32_t      *end, *c;
#if SKY_ENDIAN == SKY_ENDIAN_BIG
    const uint32_t      *start;
#endif
    sky_unicode_char_t  cp, cp2;
#if SKY_ENDIAN != SKY_ENDIAN_BIG
    sky_unicode_char_t  buffer[256], *outcp = buffer;
#endif

    if (!not_in_range_error[0]) {
        sky_format_sprintf(not_in_range_error, sizeof(not_in_range_error),
                           "codepoint not in range(%#lx)",
                           SKY_UNICODE_CHAR_MAX);
    }

    exception = NULL;
    c = (const uint32_t *)bytes;
    end = c + (nbytes / sizeof(uint32_t));

    for (;;) {
restart:
        if (c >= end) {
            if ((nbytes & 3) && final) {
                sky_codec_decode_utf32_error(utf32_be,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
            }
            break;
        }
        cp = (sky_unicode_char_t)sky_endian_btoh32(*c);
        if (cp < SKY_UNICODE_CHAR_MIN || cp > SKY_UNICODE_CHAR_MAX) {
            sky_codec_decode_utf32_error(utf32_be, c + 1, not_in_range_error);
            continue;
        }
        if (cp < 0xD800 || cp > 0xDFFF) {
#if SKY_ENDIAN != SKY_ENDIAN_BIG
            *outcp++ = cp;
            if (&(buffer[256]) == outcp) {
                output(buffer, (outcp - buffer) * 4, 4, output_arg);
                outcp = buffer;
            }
#else
            start = c;
#endif
            for (++c; c < end; ++c) {
                cp = (sky_unicode_char_t)sky_endian_btoh32(*c);
                if (cp >= 0xD800 && cp <= 0xDFFF) {
                    break;
                }
                if (cp < SKY_UNICODE_CHAR_MIN || cp > SKY_UNICODE_CHAR_MAX) {
#if SKY_ENDIAN == SKY_ENDIAN_BIG
                    output(start, (c - start) * 4, 4, output_arg);
#else
                    output(buffer, (outcp - buffer) * 4, 4, output_arg);
                    outcp = buffer;
#endif
                    sky_codec_decode_utf32_error(utf32_be, c + 1,
                                                 not_in_range_error);
                    goto restart;
                }
#if SKY_ENDIAN != SKY_ENDIAN_BIG
                *outcp++ = cp;
                if (&(buffer[256]) == outcp) {
                    output(buffer, (outcp - buffer) * 4, 4, output_arg);
                    outcp = buffer;
                }
#endif
            }
#if SKY_ENDIAN == SKY_ENDIAN_BIG
            output(start, (c - start) * 4, 4, output_arg);
#else
            if (outcp > buffer) {
                output(buffer, (outcp - buffer) * 4, 4, output_arg);
                outcp = buffer;
            }
#endif
        }
        if (cp >= 0xD800 && cp <= 0xDFFF) {
            if (cp > 0xDBFF) {
                sky_codec_decode_utf32_error(utf32_be, c + 1,
                                             "illegal encoding");
                continue;
            }
            if (++c >= end) {
                --c;
                if (!final) {
                    break;
                }
                sky_codec_decode_utf32_error(utf32_be,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
                if (c >= end) {
                    break;
                }
                continue;
            }

            cp2 = (sky_unicode_char_t)sky_endian_btoh32(*c);
            if (cp2 < 0xDC00 || cp2 > 0xDFFF) {
                --c;
                sky_codec_decode_utf32_error(utf32_be, c + 1,
                                             "illegal UTF-16 surrogate");
                continue;
            }
            cp = (((cp & 0x3FF) << 10) | (cp2 & 0x3FF)) + 0x10000;
            ++c;

            output(&cp, sizeof(cp), sizeof(cp), output_arg);
        }
    }

    if ((const char *)c > (const char *)bytes + nbytes) {
        return nbytes;
    }
    return (const char *)c - (const char *)bytes;
}


void *
sky_codec_decode_utf32_le_initialize(SKY_UNUSED sky_string_t        encoding,
                                     SKY_UNUSED sky_string_t        errors,
                                     SKY_UNUSED sky_codec_output_t  output,
                                     SKY_UNUSED void *              output_arg,
                                     uint32_t *                     flags)
{
    if (flags) {
        *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
        *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
    }
    return NULL;
}

ssize_t
sky_codec_decode_utf32_le(SKY_UNUSED void *     state,
                          sky_string_t          encoding,
                          sky_string_t          errors,
                          sky_codec_output_t    output,
                          void *                output_arg,
                          SKY_UNUSED uint32_t * flags,
                          const void *          bytes,
                          ssize_t               nbytes,
                          sky_bool_t            final)
{
    static char not_in_range_error[36]; /* Room for 32-bit value MAX */

    sky_object_t        exception;
    const uint32_t      *end, *c, *start;
    sky_unicode_char_t  cp, cp2;
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
    sky_unicode_char_t  buffer[256], *outcp = buffer;
#endif

    if (!not_in_range_error[0]) {
        sky_format_sprintf(not_in_range_error, sizeof(not_in_range_error),
                           "codepoint not in range(%#lx)",
                           SKY_UNICODE_CHAR_MAX);
    }

    exception = NULL;
    c = (const uint32_t *)bytes;
    end = c + (nbytes / sizeof(uint32_t));

    for (;;) {
restart:
        if (c >= end) {
            if ((nbytes & 3) && final) {
                sky_codec_decode_utf32_error(utf32_le,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
            }
            break;
        }
        cp = (sky_unicode_char_t)sky_endian_ltoh32(*c);
        if (cp < SKY_UNICODE_CHAR_MIN || cp > SKY_UNICODE_CHAR_MAX) {
            sky_codec_decode_utf32_error(utf32_le, c + 1, not_in_range_error);
            continue;
        }
        if (cp < 0xD800 || cp > 0xDFFF) {
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
            *outcp++ = cp;
            if (&(buffer[256]) == outcp) {
                output(buffer, (outcp - buffer) * 4, 4, output_arg);
                outcp = buffer;
            }
#endif
            for (start = c++; c < end; ++c) {
                cp = (sky_unicode_char_t)sky_endian_ltoh32(*c);
                if (cp >= 0xD800 && cp <= 0xDFFF) {
                    break;
                }
                if (cp < SKY_UNICODE_CHAR_MIN || cp > SKY_UNICODE_CHAR_MAX) {
#if SKY_ENDIAN == SKY_ENDIAN_LITTLE
                    output(start, (c - start) * 4, 4, output_arg);
#else
                    output(buffer, (outcp - buffer) * 4, 4, output_arg);
                    outcp = buffer;
#endif
                    sky_codec_decode_utf32_error(utf32_le, c + 1,
                                                 not_in_range_error);
                    goto restart;
                }
#if SKY_ENDIAN != SKY_ENDIAN_LITTLE
                *outcp++ = cp;
                if (&(buffer[256]) == outcp) {
                    output(buffer, (outcp - buffer) * 4, 4, output_arg);
                    outcp = buffer;
                }
#endif
            }
#if SKY_ENDIAN == SKY_ENDIAN_LITTLE
            output(start, (c - start) * 4, 4, output_arg);
#else
            if (outcp > buffer) {
                output(buffer, (outcp - buffer) * 4, 4, output_arg);
                outcp = buffer;
            }
#endif
        }
        if (cp >= 0xD800 && cp <= 0xDFFF) {
            if (cp > 0xDBFF) {
                sky_codec_decode_utf32_error(utf32_le, c + 1,
                                             "illegal encoding");
                continue;
            }
            if (++c >= end) {
                --c;
                if (!final) {
                    break;
                }
                sky_codec_decode_utf32_error(utf32_le,
                                             (const uint8_t *)bytes + nbytes,
                                             "unexpected end of data");
                if (c >= end) {
                    break;
                }
                continue;
            }

            cp2 = (sky_unicode_char_t)sky_endian_ltoh32(*c);
            if (cp2 < 0xDC00 || cp2 > 0xDFFF) {
                --c;
                sky_codec_decode_utf32_error(utf32_le, c + 1,
                                             "illegal UTF-16 surrogate");
                continue;
            }
            cp = (((cp & 0x3FF) << 10) | (cp2 & 0x3FF)) + 0x10000;
            ++c;

            output(&cp, sizeof(cp), sizeof(cp), output_arg);
        }
    }

    if ((const char *)c > (const char *)bytes + nbytes) {
        return nbytes;
    }
    return (const char *)c - (const char *)bytes;
}


ssize_t
sky_codec_decode_utf32(void *               state,
                       sky_string_t         encoding,
                       sky_string_t         errors,
                       sky_codec_output_t   output,
                       void *               output_arg,
                       uint32_t *           flags,
                       const void *         bytes,
                       ssize_t              nbytes,
                       sky_bool_t           final)
{
    ssize_t                     consumed, decoder_consumed;
    uint32_t                    dummy_flags;
    const void                  *original_bytes;
    sky_codec_decoder_decode_t  decoder;

    /* Built-in decoders require properly aligned input. */
    sky_error_validate_debug(!((uintptr_t)bytes & 0x3));

    if (!flags) {
        flags = &dummy_flags;
        dummy_flags = 0;
    }
    consumed = 0;

    if (nbytes >= 4) {
        const uint8_t   *b = (const uint8_t *)bytes;

        if (0xFF == b[0] && 0xFE == b[1] && 0x00 == b[2] && 0x00 == b[3]) {
            bytes += 4;
            nbytes -= 4;
            consumed += 4;
            *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
            *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
        }
        if (0x00 == b[0] && 0x00 == b[1] && 0xFE == b[2] && 0xFF == b[3]) {
            bytes += 4;
            nbytes -= 4;
            consumed += 4;
            *flags &= ~SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE;
            *flags |= SKY_CODEC_DECODE_FLAG_ENDIAN_BIG;
        }
    }

    if (*flags & SKY_CODEC_DECODE_FLAG_ENDIAN_LITTLE) {
        decoder = sky_codec_decode_utf32_le;
    }
    else if (*flags & SKY_CODEC_DECODE_FLAG_ENDIAN_BIG) {
        decoder = sky_codec_decode_utf32_be;
    }
    else {
#if SKY_ENDIAN == SKY_ENDIAN_BIG
        decoder = sky_codec_decode_utf32_be;
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
        decoder = sky_codec_decode_utf32_le;
#else
#   error implementation missing
#endif
    }

    original_bytes = bytes;
    decoder_consumed = decoder(state,
                               encoding,
                               errors,
                               output,
                               output_arg,
                               flags,
                               bytes,
                               nbytes,
                               final);
    if (original_bytes != bytes) {
        return decoder_consumed;
    }
    return consumed + decoder_consumed;
}


sky_object_t
sky_codec_decode_prepareinput(const sky_codec_t *codec, sky_object_t input)
{
    if (!codec) {
        return input;
    }

    if (sky_object_isa(input, sky_string_type)) {
        if (codec->decoder_flags & SKY_CODEC_DECODER_FLAG_ENCODE_STRING_INPUT) {
            sky_error_validate_debug(!(codec->decoder_flags &
                                       SKY_CODEC_DECODER_FLAG_RETURN_STRING_INPUT));
            return sky_codec_encode(sky_codec_utf8, input, NULL, NULL, 0);
        }
    }

    return input;
}


static void
sky_codec_decode_bytes_callback(const void *bytes,
                                ssize_t     nbytes,
                     SKY_UNUSED size_t      width,
                                void *      arg)
{
    sky_bytes_builder_appendbytes(arg, bytes, nbytes);
}

static void
sky_codec_decode_string_callback(const void *   bytes,
                                 ssize_t        nbytes,
                                 size_t         width,
                                 void *         arg)
{
    sky_string_builder_appendcodepoints(arg, bytes, nbytes / width, width);
}

sky_object_t
sky_codec_decode(const sky_codec_t *codec,
                 sky_string_t       encoding,
                 sky_string_t       errors,
                 uint32_t *         flags,
                 sky_object_t       input)
{
    sky_object_t        builder, result;
    sky_codec_output_t  output;

    input = sky_codec_decode_prepareinput(codec, input);
    if (!codec) {
        codec = sky_codec_builtin(encoding);
        if (!codec) {
            return sky_codec_decode_unknown(encoding,
                                            errors,
                                            NULL,
                                            NULL,
                                            flags,
                                            input,
                                            NULL);
        }
    }

    if ((codec->decoder_flags & SKY_CODEC_DECODER_FLAG_RETURN_STRING_INPUT) &&
        sky_object_isa(input, sky_string_type))
    {
        return input;
    }

    SKY_ASSET_BLOCK_BEGIN {
        void            *state;
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, input, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        if (codec->decoder_flags & SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES) {
            builder = sky_bytes_builder_create();
            output = sky_codec_decode_bytes_callback;
        }
        else {
            builder = sky_string_builder_create();
            output = sky_codec_decode_string_callback;
        }

        if (codec->decoder_initialize) {
            state = codec->decoder_initialize(encoding,
                                              errors,
                                              output,
                                              builder,
                                              flags);
        }

        SKY_ERROR_TRY {
            codec->decoder_decode(state,
                                  encoding,
                                  errors,
                                  output,
                                  builder,
                                  flags,
                                  buffer.buf,
                                  buffer.len,
                                  SKY_TRUE);

            if (codec->decoder_finalize) {
                codec->decoder_finalize(state,
                                        encoding,
                                        errors,
                                        output,
                                        builder,
                                        flags);
            }
        } SKY_ERROR_EXCEPT_ANY {
            if (codec->decoder_finalize) {
                codec->decoder_finalize(state,
                                        encoding,
                                        errors,
                                        output,
                                        builder,
                                        flags);
            }
            sky_error_raise();
        } SKY_ERROR_TRY_END;

        if (codec->decoder_flags & SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES) {
            result = sky_bytes_builder_finalize(builder);
        }
        else {
            result = sky_string_builder_finalize(builder);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
sky_codec_decodewithcallback(const sky_codec_t *codec,
                             sky_string_t       encoding,
                             sky_string_t       errors,
                             sky_codec_output_t output,
                             void *             output_arg,
                             uint32_t *         flags,
                             const void *       bytes,
                             ssize_t            nbytes)
{
    void    *state;

    if (!codec) {
        codec = sky_codec_builtin(encoding);
        if (!codec) {
            sky_codec_decode_unknown(encoding,
                                     errors,
                                     output,
                                     output_arg,
                                     flags,
                                     sky_bytes_createfrombytes(bytes, nbytes),
                                     NULL);
            return;
        }
    }

    if (codec->decoder_initialize) {
        state = codec->decoder_initialize(encoding,
                                          errors,
                                          output,
                                          output_arg,
                                          flags);
    }
    SKY_ERROR_TRY {
        codec->decoder_decode(state,
                              encoding,
                              errors,
                              output,
                              output_arg,
                              flags,
                              bytes,
                              nbytes,
                              SKY_TRUE);

        if (codec->decoder_finalize) {
            codec->decoder_finalize(state,
                                    encoding,
                                    errors,
                                    output,
                                    output_arg,
                                    flags);
        }
    } SKY_ERROR_EXCEPT_ANY {
        if (codec->decoder_finalize) {
            codec->decoder_finalize(state,
                                    encoding,
                                    errors,
                                    output,
                                    output_arg,
                                    flags);
        }
        sky_error_raise();
    } SKY_ERROR_TRY_END;
}


sky_string_t
sky_codec_charmap_decode(sky_object_t   mapping,
                         sky_string_t   errors,
                         uint32_t *     flags,
                         sky_object_t   input)
{
    sky_string_builder_t    builder;

    SKY_ASSET_BLOCK_BEGIN {
        sky_buffer_t    buffer;

        sky_buffer_acquire(&buffer, input, SKY_BUFFER_FLAG_SIMPLE);
        sky_asset_save(&buffer,
                       (sky_free_t)sky_buffer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

        builder = sky_string_builder_create();
        sky_codec_decode_charmap(mapping,
                                 NULL,
                                 errors,
                                 sky_codec_decode_string_callback,
                                 builder,
                                 flags,
                                 buffer.buf,
                                 buffer.len,
                                 SKY_TRUE);
    } SKY_ASSET_BLOCK_END;

    return sky_string_builder_finalize(builder);
}


void
sky_codec_charmap_decodewithcallback(sky_object_t       mapping,
                                     sky_string_t       errors,
                                     sky_codec_output_t output,
                                     void *             output_arg,
                                     uint32_t *         flags,
                                     const void *       bytes,
                                     ssize_t            nbytes)
{
    if (!mapping) {
        sky_codec_decodewithcallback(sky_codec_latin1,
                                     NULL,
                                     errors,
                                     output,
                                     output_arg,
                                     flags,
                                     bytes,
                                     nbytes);
    }

    sky_codec_decode_charmap(mapping,
                             NULL,
                             errors,
                             output,
                             output_arg,
                             flags,
                             bytes,
                             nbytes,
                             SKY_TRUE);
}


sky_string_t
sky_codec_decodefilename(sky_object_t filename)
{
    return sky_codec_decode(sky_codec_filesystemencoding_codec,
                            sky_codec_filesystemencoding_string,
                            sky_codec_filesystemerrors_string,
                            NULL,
                            filename);
}
