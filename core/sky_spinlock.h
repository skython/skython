/** @file
  * @brief Spinlocks
  * @defgroup sky_atomic Atomic Operations
  * @{
  * @defgroup sky_spinlock Spinlocks
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SPINLOCK_H__
#define __SKYTHON_CORE_SKY_SPINLOCK_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** A spinlock. **/
typedef int32_t sky_spinlock_t;


/** Constant expression to be used to statically initialize a spinlock. **/
#define SKY_SPINLOCK_INITIALIZER        0


/** Initialize a spinlock.
  * Prepares a spinlock for use. Spinlocks must be initialized in one of two
  * ways before they can be used safely. Either statically initialize the
  * spinlock variable using @c SKY_SPINLOCK_INTIALIZER or call
  * sky_spinlock_init().
  *
  * @param[in]  spinlock    the spinlock to initialize.
  */
SKY_EXTERN void sky_spinlock_init(sky_spinlock_t *spinlock);


/** Wait indefinitely until a spinlock can be acquired.
  * This function will not return until the spinlock has been acquired. Upon
  * return, the caller is said to be the owner of the spinlock.
  *
  * @param[in]  spinlock    the spinlock to acquire.
  */
SKY_EXTERN void sky_spinlock_lock(volatile sky_spinlock_t *spinlock);


/** Try to acquire a spinlock without waiting.
  * This function will attempt to acquire the specified spinlock, but if it
  * cannot be acquired without waiting, it will return immediately.
  *
  * @param[in]  spinlock    the spinlock to acquire.
  * @return     @c SKY_TRUE if the spinlock was acquired; otherwise,
  *             @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t sky_spinlock_trylock(volatile sky_spinlock_t *spinlock);


/** Release a spinlock.
  * This function will release the spinlock. It is assumed that the caller
  * is the current owner of the spinlock.
  *
  * @param[in]  spinlock    the spinlock to release.
  */
SKY_EXTERN void sky_spinlock_unlock(volatile sky_spinlock_t *spinlock);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SPINLOCK_H__ */

/** @} **/
/** @} **/
