/** @file
  * @brief Thread-safe one-shot initialization
  * @defgroup threading Threading Support
  * @{
  * @defgroup sky_once One-shot initialization
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_ONCE_H__
#define __SKYTHON_CORE_SKY_ONCE_H__ 1


#include "sky_base.h"


SKY_CDECLS_BEGIN


/** A one-shot initializer. **/
typedef int32_t sky_once_t;


/** A function pointer that can be called by sky_once_run().
  * When sky_once_run() is called for a one-shot initializer that hasn't been
  * run yet, this function pointer will be called to perform the necessary
  * implementation.
  *
  * @param[in]  arg an arbitrary pointer passed through sky_once_run().
  */
typedef void (*sky_once_initialize_t)(void *arg);


/** Constant expression to be used to statically initialize a one-shot
  * initializer.
  */
#define SKY_ONCE_INITIALIZER            0


/** Initialize a one-shot initializer.
  * Prepare a one-shot initializer for use. One-shot initializers must be
  * initialized in one of two ways before they can be used safely. Either
  * statically initialize the one-shot initializer variable using
  * SKY_ONCE_INITIALIZER or call sky_once_init(). Due to the nature of
  * one-shot initializers, the safest way to initialize them is generally
  * going to be statically, using SKY_ONCE_INITIALIZER.
  *
  * @param[in]  once    the one-shot initializer to initialize.
  */
SKY_EXTERN void sky_once_init(sky_once_t *once);

/** Execute a one-shot initializer.
  * If this is the first time that sky_once_run() has been called for a one-shot
  * initializer, the specified function will be called. For successive calls,
  * nothing will be done. If a call to sky_once_run() is made for a one-shot
  * initializer while the initialization function is running on another thread,
  * sky_once_run() will not return until the initialization function finishes
  * running. Initialization functions should take as little time to run as
  * possible, because other threads waiting for them to finish will be spinning
  * in a CPU busy loop.
  *
  * @warning    Do not nest calls to sky_once_run() or sky_once_run_unsafe()
  *             for the same one-shot initializer; otherwise, a deadlock will
  *             occur.
  *
  * @param[in]  once        the one-shot initializer to run.
  * @param[in]  function    the function to call to do the initialization.
  * @param[in]  arg         an arbitrary pointer that will be passed through
  *                         to the initialization function.
  */
SKY_EXTERN void sky_once_run(volatile sky_once_t *  once,
                             sky_once_initialize_t  function,
                             void *                 arg);


/** Execute a one-shot initializer without sky_error handling.
  * sky_once_run_unsafe() behaves in exactly the same way as sky_once_run(),
  * except that it does not set up an error handler. The need for this function
  * is primarily during early library initialization, and should not generally
  * be used otherwise; however, potential legitimate uses may exist, and so it
  * has been exposed publicly.
  *
  * @warning    Do not nest calls to sky_once_run() or sky_once_run_unsafe()
  *             for the same one-shot initializer; otherwise, a deadlock will
  *             occur.
  *
  * @param[in]  once        the one-shot initializer to run.
  * @param[in]  function    the function to call to do the initialization.
  * @param[in]  arg         an arbitrary pointer that will be passed through
  *                         to the initialization function.
  */
SKY_EXTERN void sky_once_run_unsafe(volatile sky_once_t *   once,
                                    sky_once_initialize_t   function,
                                    void *                  arg);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_ONCE_H__ */

/** @} **/
/** @} **/
