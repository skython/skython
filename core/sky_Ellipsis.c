#include "sky_private.h"


SKY_TYPE_DEFINE_SIMPLE(Ellipsis,
                       "ellipsis",
                       0,
                       NULL,
                       NULL,
                       NULL,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

sky_Ellipsis_t const sky_Ellipsis = SKY_OBJECT_TAGGED_ELLIPSIS;


sky_object_t
sky_Ellipsis_new(SKY_UNUSED sky_type_t type, sky_tuple_t args, sky_dict_t kws)
{
    if ((args && sky_object_len(args)) || (kws && sky_object_len(kws))) {
        sky_error_raise_string(sky_TypeError,
                               "EllipsisType takes no arguments");
    }
    return sky_Ellipsis;
}


sky_string_t
sky_Ellipsis_repr(SKY_UNUSED sky_object_t self)
{
    return SKY_STRING_LITERAL("Ellipsis");
}


void
sky_Ellipsis_initialize_library(void)
{
    sky_type_setmethodslots(sky_Ellipsis_type,
            "__new__", sky_Ellipsis_new,
            "__repr__", sky_Ellipsis_repr,
            NULL);
}
