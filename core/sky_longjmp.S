#include "config.h"

#if !defined(__APPLE__) || \
    (!defined(__i386__) && !defined(__x86_64__)) || \
    !defined(HAVE_LIBUNWIND_H)
#   error this source file cannot be compiled in this configuration
#endif

#if defined(__i386__)
#   include "i386/sky_longjmp.S"
#elif defined(__x86_64__)
#   include "x86_64/sky_longjmp.S"
#endif

