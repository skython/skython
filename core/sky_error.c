#include "sky_private.h"

#include "sky_code.h"
#include "sky_frame_private.h"
#include "sky_python_compiler.h"
#include "sky_traceback.h"


sky_error_record_t *
sky_error_record_create(sky_type_t      type,
                        sky_object_t    value,
                        sky_traceback_t traceback,
                        const char *    function,
                        const char *    filename,
                        uint32_t        lineno)
{
    sky_error_record_t  *record;

    record = sky_calloc(1, sizeof(sky_error_record_t));
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->type)),
                          type,
                          SKY_FALSE);
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->value)),
                          (sky_object_isnull(value) ? sky_None : value),
                          SKY_FALSE);
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->traceback)),
                          (sky_object_isnull(traceback) ? NULL : traceback),
                          SKY_FALSE);
    record->function = function;
    record->filename = filename;
    record->lineno = lineno;
    record->refs = 1;

    return record;
}


void
sky_error_record_release(sky_error_record_t *record)
{
    if (record && !--record->refs) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->type)),
                              NULL,
                              SKY_FALSE);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->value)),
                              NULL,
                              SKY_FALSE);
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->traceback)),
                              NULL,
                              SKY_FALSE);
        sky_free(record);
    }
}


void
sky_error_record_retain(sky_error_record_t *record)
{
    ++record->refs;
}


void
sky_error_record_normalize(sky_error_record_t *record)
{
    sky_tuple_t args;

    if (!sky_type_issubtype(record->type, sky_BaseException)) {
        return;
    }

    if (!sky_object_isa(record->value, sky_BaseException) ||
        !sky_object_isa(record->value, record->type))
    {
        if (sky_object_isnull(record->value)) {
            args = NULL;
        }
        else if (sky_object_isa(record->value, sky_tuple_type)) {
            args = record->value;
        }
        else {
            args = sky_tuple_pack(1, record->value);
        }
        sky_object_gc_setroot(&(record->value),
                              sky_object_call(record->type, args, NULL),
                              SKY_FALSE);
    }
    else if (record->type != sky_object_type(record->value)) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->type)),
                              sky_object_type(record->value),
                              SKY_FALSE);
    }

    sky_BaseException_settraceback(record->value, record->traceback);
}


void
sky_error_block_pop(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_bool_t          error;
    sky_error_asset_t   *asset;
    sky_error_block_t   *block;

    block = tlsdata->error_blocks;
    tlsdata->error_blocks = block->next;

    /* Clear the block's error if it has been handled. If not, propagate it to
     * the next block. As far as asset clean up is concerned, it's still an
     * error even if it has been handled.
     */
    error = SKY_FALSE;
    if (block->error_record) {
        error = SKY_TRUE;
        if (SKY_ERROR_BLOCK_TYPE_NONE != block->block_type &&
            tlsdata->error_blocks)
        {
            sky_error_record_release(tlsdata->error_blocks->error_record);
            tlsdata->error_blocks->error_record = block->error_record;
            sky_error_record_retain(tlsdata->error_blocks->error_record);
        }
        sky_error_record_release(block->error_record);
        block->error_record = NULL;
    }

    /* Clean up the block's assets. */
    while ((asset = block->assets) != NULL) {
        block->assets = asset->next;
        if (asset->asset_cleanup == SKY_ASSET_CLEANUP_ALWAYS ||
            (asset->asset_cleanup == SKY_ASSET_CLEANUP_ON_ERROR &&
             error) ||
            (asset->asset_cleanup == SKY_ASSET_CLEANUP_ON_SUCCESS &&
             !error))
        {
            asset->asset_free(asset->asset_pointer);
        }
        asset->next = tlsdata->error_asset_freelist;
        tlsdata->error_asset_freelist = asset;
    }
}


void
sky_error_block_push(sky_error_block_t *    block,
                     sky_error_block_type_t block_type)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    block->next = tlsdata->error_blocks;
    block->assets = NULL;
    block->error_record = NULL;
    block->block_type = block_type;
    tlsdata->error_blocks = block;
}


sky_bool_t
sky_error_clear(void)
{
    sky_error_block_t   *block;

    for (block = sky_tlsdata_get()->error_blocks; block; block = block->next) {
        if (block->error_record) {
            sky_error_record_release(block->error_record);
            block->error_record = NULL;
            return SKY_TRUE;
        }
    }

    return SKY_FALSE;
}


sky_error_record_t *
sky_error_current(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_error_block_t   *block;

    for (block = tlsdata->error_blocks; block; block = block->next) {
        if (block->error_record) {
            sky_error_record_normalize(block->error_record);
            return block->error_record;
        }
    }

    return NULL;
}


static void
sky_error_display_text(sky_object_t write, ssize_t offset, sky_string_t text)
{
    sky_string_t            line;
    sky_string_builder_t    builder;

    builder = sky_string_builder_createwithcapacity(sky_string_len(text) + 5);
    sky_string_builder_appendcodepoint(builder, ' ', 4);

    if (offset >= 0) {
        SKY_ASSET_BLOCK_BEGIN {
            ssize_t nbytes;
            uint8_t *bytes, *nl;

            bytes = sky_codec_encodetocbytes(NULL, text, NULL, NULL, 0, &nbytes);
            sky_asset_save(bytes, sky_free, SKY_ASSET_CLEANUP_ALWAYS);

            if (offset > 0 && offset == nbytes && bytes[offset - 1] == '\n') {
                --offset;
            }
            for (;;) {
                nl = memchr(bytes, '\n', nbytes);
                if (!nl || nl - bytes >= offset) {
                    break;
                }
                offset -= (ssize_t)(nl + 1 - bytes);
                nbytes -= (nl + 1) - bytes;
                bytes = nl + 1;
            }

            while (nbytes && (' ' == *bytes || '\t' == *bytes)) {
                ++bytes;
                --nbytes;
                --offset;
            }

            sky_string_builder_appendbytes(builder, bytes, nbytes, NULL, NULL);
            if (nbytes && bytes[nbytes - 1] != '\n') {
                sky_string_builder_appendcodepoint(builder, '\n', 1);
            }
        } SKY_ASSET_BLOCK_END;
    }
    else {
        sky_string_builder_append(builder, text);
        if (sky_string_charat(text, -1) != '\n') {
            sky_string_builder_appendcodepoint(builder, '\n', 1);
        }
    }
    text = sky_string_builder_finalize(builder);
    sky_object_call(write, sky_tuple_pack(1, text), NULL);

    if (offset > 0) {
        builder = sky_string_builder_createwithcapacity(offset + 5);
        sky_string_builder_appendcodepoint(builder, ' ', offset + 3);
        sky_string_builder_appendcodepoint(builder, '^', 1);
        sky_string_builder_appendcodepoint(builder, '\n', 1);
        line = sky_string_builder_finalize(builder);
        sky_object_call(write, sky_tuple_pack(1, line), NULL);
    }
}


static void
sky_error_display_write(sky_object_t file, sky_object_t value)
{
    ssize_t         x;
    sky_type_t      type;
    sky_object_t    write;
    sky_string_t    class_name, line, module_name, type_name;
    sky_traceback_t traceback;

    if (!(write = sky_object_getattr(file,
                                     SKY_STRING_LITERAL("write"),
                                     NULL)))
    {
        return;
    }

    type = sky_object_type(value);
    traceback = sky_BaseException_traceback(value);
    if (sky_object_isa(traceback, sky_traceback_type)) {
        sky_traceback_print(traceback, file);
    }

    if (sky_object_hasattr(value, SKY_STRING_LITERAL("print_file_and_line"))) {
        SKY_ERROR_TRY {
            ssize_t         column;
            sky_object_t    filename, lineno, msg, offset, text;

            msg = sky_object_getattr(value,
                                     SKY_STRING_LITERAL("msg"),
                                     sky_NotSpecified);
            filename = sky_object_getattr(value,
                                          SKY_STRING_LITERAL("filename"),
                                          sky_NotSpecified);
            if (!sky_object_bool(filename)) {
                filename = SKY_STRING_LITERAL("<string>");
            }
            lineno = sky_object_getattr(value,
                                        SKY_STRING_LITERAL("lineno"),
                                        sky_NotSpecified);
            offset = sky_object_getattr(value,
                                        SKY_STRING_LITERAL("offset"),
                                        sky_NotSpecified);
            text = sky_object_getattr(value,
                                      SKY_STRING_LITERAL("text"),
                                      sky_NotSpecified);

            if (sky_object_isa(lineno, sky_integer_type)) {
                line = sky_string_createfromformat("  File \"%@\", line %@\n",
                                                   filename,
                                                   lineno);
                sky_object_call(write, sky_tuple_pack(1, line), NULL);
                value = msg;

                if (!sky_object_isnull(text) &&
                    sky_object_isa(text, sky_string_type))
                {
                    if (!sky_object_isa(offset, sky_integer_type)) {
                        column = -1;
                    }
                    else {
                        column = sky_integer_value(offset,
                                                   SSIZE_MIN, SSIZE_MAX,
                                                   NULL);
                    }
                    sky_error_display_text(write, column, text);
                }
            }
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;
    }

    class_name = sky_type_name(type);
    if ((x = sky_string_rfind(class_name,
                              SKY_STRING_LITERAL("."),
                              0,
                              SSIZE_MAX)) != -1)
    {
        class_name = sky_string_slice(class_name, x + 1, SSIZE_MAX, 1);
    }
    module_name = sky_type_module(type);
    if (module_name &&
        sky_object_compare(module_name,
                           SKY_STRING_LITERAL("builtins"),
                           SKY_COMPARE_OP_EQUAL))
    {
        module_name = NULL;
    }
    if (!module_name) {
        type_name = class_name;
    }
    else {
        type_name = sky_string_createfromformat("%@.%@",
                                                module_name,
                                                class_name);
    }

    if (!sky_object_isnull(value)) {
        value = sky_object_str(value);
    }
    if (sky_object_bool(value) && sky_object_isa(value, sky_string_type)) {
        line = sky_string_createfromformat("%@: %@\n", type_name, value);
    }
    else {
        line = sky_string_createfromformat("%@\n", type_name);
    }

    sky_object_call(write, sky_tuple_pack(1, line), NULL);
}


static sky_bool_t
sky_error_display_recursive(sky_object_t    file,
                            sky_object_t    value,
                            sky_set_t       displayed)
{
    static const char   *cause_message =
                        "\nThe above exception was the direct cause of the "
                        "following exception:\n\n";
    static const char   *context_message =
                        "\nDuring handling of the above exception, another "
                        "exception occurred:\n\n";

    sky_object_t    cause, context;
    sky_string_t    line;

    if (!sky_set_add(displayed, value)) {
        return SKY_FALSE;
    }

    cause = sky_BaseException_cause(value);
    context = sky_BaseException_context(value);

    if (!sky_object_isnull(cause)) {
        SKY_ERROR_TRY {
            if (sky_error_display_recursive(file, cause, displayed)) {
                line = sky_string_createfrombytes(cause_message,
                                                  strlen(cause_message),
                                                  NULL,
                                                  NULL);
                sky_object_callmethod(file,
                                      SKY_STRING_LITERAL("write"),
                                      sky_tuple_pack(1, line),
                                      NULL);
            }
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;
    }
    if (!sky_object_isnull(context) &&
        !sky_BaseException_suppress_context(value))
    {
        SKY_ERROR_TRY {
            if (sky_error_display_recursive(file, context, displayed)) {
                line = sky_string_createfrombytes(context_message,
                                                  strlen(context_message),
                                                  NULL,
                                                  NULL);
                sky_object_callmethod(file,
                                      SKY_STRING_LITERAL("write"),
                                      sky_tuple_pack(1, line),
                                      NULL);
            }
        } SKY_ERROR_EXCEPT_ANY {
        } SKY_ERROR_TRY_END;
    }

    SKY_ERROR_TRY {
        sky_error_display_write(file, value);
    } SKY_ERROR_EXCEPT_ANY {
    } SKY_ERROR_TRY_END;

    return SKY_TRUE;
}


void
sky_error_display(sky_error_record_t *record)
{
    sky_module_t    sys;
    sky_object_t    file;

    sky_error_record_normalize(record);

    if (!(sys = sky_interpreter_module_sys()) ||
        !(file = sky_object_getattr(sys, SKY_STRING_LITERAL("stderr"), NULL)))
    {
        sky_object_dump(record->value);
        sky_format_fprintf(stderr, "Exception Raised from %s line %u\n",
                           record->filename, record->lineno);
        sky_format_fprintf(stderr, "lost sys.stderr\n");
    }
    else {
        sky_error_display_recursive(file, record->value, sky_set_create(NULL));
    }
}


void
sky_error_raise_record(sky_error_record_t *record)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    char                *message;
    sky_object_t        context, value;
    sky_error_block_t   *block;

    if ((block = tlsdata->error_blocks) != NULL) {
        if (block->error_record != record) {
            /* Get the current exception if there is one. */
            while (block && !block->error_record) {
                block = block->next;
            }
            if (block) {
                sky_error_record_normalize(record);
                sky_error_record_normalize(block->error_record);

                value = block->error_record->value;
                context = sky_BaseException_context(value);
                while (!sky_object_isnull(context)) {
                    if (context == record->value) {
                        sky_BaseException_setcontext(value, NULL);
                        break;
                    }
                    value = context;
                    context = sky_BaseException_context(value);
                }
                sky_BaseException_setcontext(record->value,
                                             block->error_record->value);
            }

            block = tlsdata->error_blocks;
            if (block->error_record) {
                sky_error_record_release(block->error_record);
            }
            block->error_record = record;
            sky_error_record_retain(block->error_record);
        }

        /* Hold a reference to the error on the chance that it'll be needed to
         * deal with it being unhandled.
         */
        sky_error_record_retain(record);

        /* Keep popping off blocks until a TRY block is found. The act of
         * popping will propagate the exception forward.
         */
        while ((block = tlsdata->error_blocks) != NULL) {
            if (block->block_type == SKY_ERROR_BLOCK_TYPE_TRY) {
                sky_error_record_release(record);
                sky_longjmp(block->setjmp_context, SKY_ERROR_SETJMP_RAISE);
            }
            sky_error_block_pop();
        }
    }

    sky_error_display(record);
    sky_format_asprintf(&message,
                        "Unhandled exception raised from %s line %u.",
                        record->filename, record->lineno);
    sky_error_fatal(message);
}


void
sky_error_raise(void)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    sky_error_block_t   *block;

    /* Find an exception to raise. If there isn't one, one will be created!
     * Don't pop blocks as we go, because they'll change from success to error
     * if there's no exception to re-raise.
     */
    for (block = tlsdata->error_blocks; block; block = block->next) {
        if (SKY_ERROR_BLOCK_TYPE_NONE != block->block_type &&
            block->error_record)
        {
            sky_error_raise_record(block->error_record);
        }
    }

    sky_error_raise_string(sky_RuntimeError, "No active exception to reraise");
}


void
sky_error_raise_(sky_type_t     type,
                 sky_object_t   value,
                 const char *   function,
                 const char *   filename,
                 uint32_t       lineno)
{
    sky_error_record_t  *record;

    if (!sky_type_issubtype(type, sky_BaseException)) {
        sky_error_raise_string(sky_TypeError,
                               "exceptions must derive from BaseException");
    }

    record = sky_calloc(1, sizeof(sky_error_record_t));
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->type)),
                          type,
                          SKY_FALSE);
    sky_object_gc_setroot(SKY_AS_OBJECTP(&(record->value)),
                          value,
                          SKY_FALSE);
    record->function = function;
    record->filename = filename;
    record->lineno = lineno;
    record->refs = 0;

    sky_error_raise_record(record);
}


void
sky_error_fatal_(const char *   message,
                 const char *   function,
                 const char *   filename,
                 uint32_t       lineno)
{
#if defined(_WIN32)
    FatalAppExit(0, (message ? message : "fatal error"));
#else
    /* Do not use buffered I/O, because one of the most likely causes of this
     * function being called is an out of memory condition.
     */
    if (message) {
        char    buffer[16];
        size_t  buffer_length;

        buffer_length = sky_util_utoa(lineno, buffer, sizeof(buffer));

        /* FATAL: %(filename)s line %(lineno)u (%(function)s) */
        write(fileno(stderr), "FATAL: ", sizeof("FATAL: ") - 1);
        write(fileno(stderr), filename, strlen(filename));
        write(fileno(stderr), " line ", sizeof(" line ") - 1);
        write(fileno(stderr), buffer, buffer_length);
        write(fileno(stderr), " (", sizeof(" (") - 1);
        write(fileno(stderr), function, strlen(function));
        write(fileno(stderr), "):\n    ", sizeof("):\n    ") - 1);
        write(fileno(stderr), message, strlen(message));
        write(fileno(stderr), "\n", 1);
        sky_traceback_dump(2);
    }
    abort();
#endif
}


void
sky_error_raise_errno_(sky_type_t   type,
                       int          errno_code,
                       const char * function,
                       const char * filename,
                       uint32_t     lineno)
{
    const char      *message;
    sky_tuple_t     args;
    sky_object_t    value;

    if (EINTR == errno_code) {
        sky_signal_check_pending();
    }
    if (!errno_code || !(message = strerror(errno_code))) {
        message = "Error";
    }
    args = sky_object_build("(is)", errno_code, message);
    value = sky_object_call(type, args, NULL);

    sky_error_raise_(type, value, function, filename, lineno);
}


sky_dict_t      sky_error_warn__onceregistry = NULL;
sky_list_t      sky_error_warn_filters = NULL;
sky_string_t    sky_error_warn__defaultaction = NULL;


static sky_bool_t
sky_error_warn_match(sky_object_t x, sky_object_t y)
{
    if (sky_object_isnull(x)) {
        return SKY_TRUE;
    }

    return sky_object_bool(sky_object_callmethod(x,
                                                 SKY_STRING_LITERAL("match"),
                                                 sky_tuple_pack(1, y),
                                                 NULL));
}


static sky_object_t
sky_error_warn_filter(sky_type_t    category,
                      sky_object_t  message,
                      ssize_t       lineno,
                      sky_string_t  module)
{
    ssize_t         filter_lineno, i;
    sky_list_t      filters;
    sky_tuple_t     filter, item;
    sky_module_t    warnings;
    sky_object_t    action;

    if (!(warnings = sky_dict_get(sky_interpreter_modules(),
                                  SKY_STRING_LITERAL("warnings"),
                                  NULL)))
    {
        if (!(filters = sky_error_warn_filters)) {
            return NULL;
        }
    }
    else {
        filters = sky_object_getattr(warnings,
                                     SKY_STRING_LITERAL("filters"),
                                     sky_NotSpecified);
        if (!sky_object_isa(filters, sky_list_type)) {
            /* CPython raises ValueError -- shouldn't this be TypeError? */
            sky_error_raise_string(sky_ValueError,
                                   "warnings.filters must be a list");
        }
    }

    i = 0;
    item = NULL;
    SKY_SEQUENCE_FOREACH(filters, filter) {
        if (!sky_object_isa(filter, sky_tuple_type) ||
            sky_tuple_len(filter) != 5)
        {
            /* CPython raises ValueError -- shouldn't this be TypeError? */
            sky_error_raise_format(
                    sky_ValueError,
                    "warnings.filters item %zd isn't a 5-tuple",
                    i);
        }

        filter_lineno = sky_integer_value(sky_tuple_get(filter, 4),
                                          SSIZE_MIN,
                                          SSIZE_MAX,
                                          NULL);
        if (sky_error_warn_match(sky_tuple_get(filter, 1), message) &&
            sky_error_warn_match(sky_tuple_get(filter, 3), module) &&
            sky_type_issubtype(category, sky_tuple_get(filter, 2)) &&
            (!filter_lineno || filter_lineno == lineno))
        {
            item = filter;
            SKY_SEQUENCE_FOREACH_BREAK;
        }

        ++i;
    } SKY_SEQUENCE_FOREACH_END;

    if (!item) {
        if (!warnings) {
            if (sky_error_warn__defaultaction) {
                return sky_error_warn__defaultaction;
            }
            return SKY_STRING_LITERAL("default");
        }
        if (!(action = sky_object_getattr(warnings,
                                          SKY_STRING_LITERAL("defaultaction"),
                                          NULL)))
        {
            sky_error_raise_string(sky_ValueError,
                                   "warnings.defaultaction not found");
        }
        return action;
    }

    return item;
}


static sky_dict_t
sky_error_warn_onceregistry(void)
{
    sky_module_t    warnings;

    if (!(warnings = sky_dict_get(sky_interpreter_modules(),
                                  SKY_STRING_LITERAL("warnings"),
                                  NULL)))
    {
        return sky_error_warn__onceregistry;
    }

    return sky_object_getattr(warnings,
                              SKY_STRING_LITERAL("onceregistry"),
                              sky_error_warn__onceregistry);
}


static void
sky_error_warn_display(sky_object_t message,
                       sky_type_t   category,
                       sky_object_t instance,
                       sky_string_t filename,
                       ssize_t      lineno,
                       sky_object_t source_line)
{
    sky_module_t    warnings;
    sky_object_t    stderr_object, write_method;
    sky_string_t    category_name, line;

    if ((warnings = sky_dict_get(sky_interpreter_modules(),
                                 SKY_STRING_LITERAL("warnings"),
                                 NULL)) != NULL)
    {
        sky_object_t    showwarning;

        showwarning = sky_object_getattr(warnings,
                                         SKY_STRING_LITERAL("showwarning"),
                                         NULL);
        if (showwarning) {
            if (!sky_object_callable(showwarning)) {
                sky_error_raise_string(
                        sky_TypeError,
                        "warnings.showwarning() must be set to a callable");
            }
            sky_object_call(showwarning,
                            sky_object_build("(OOOiz)", instance,
                                                        category,
                                                        filename,
                                                        lineno),
                            NULL);
            return;
        }
    }

    stderr_object = sky_object_getattr(sky_interpreter_module_sys(),
                                       SKY_STRING_LITERAL("stderr"),
                                       sky_NotSpecified);
    write_method = sky_object_getattr(stderr_object,
                                      SKY_STRING_LITERAL("write"),
                                      sky_NotSpecified);

    /* Print "filename:lineno: category: message\n" to stderr */
    category_name = sky_object_getattr(category,
                                       SKY_STRING_LITERAL("__name__"),
                                       SKY_STRING_LITERAL("<unknown>"));
    line = sky_string_createfromformat("%@:%zd: %@: %@\n",
                                       filename,
                                       lineno,
                                       category_name,
                                       message);
    sky_object_call(write_method, sky_tuple_pack(1, line), NULL);

    /* Print "  source_line\n" */
    if (sky_object_bool(source_line)) {
        source_line = sky_string_createfromobject(source_line);
    }
    else {
        source_line = sky_python_compiler_line(filename, lineno);
    }
    line = sky_string_createfromformat("  %@\n",
                                       sky_string_strip(source_line, NULL));
    sky_object_call(write_method, sky_tuple_pack(1, line), NULL);
}


void
sky_error_warn(sky_object_t message, sky_type_t category, ssize_t stacklevel)
{
    ssize_t             lineno;
    sky_dict_t          globals, registry;
    sky_frame_t         frame;
    sky_string_t        filename, lower_filename, module;
    sky_frame_data_t    *frame_data;

    frame = sky_tlsdata_get()->current_frame;
    while (frame && --stacklevel > 0) {
        frame_data = sky_frame_data(frame);
        frame = frame_data->f_back;
    }

    if (!frame) {
        globals = sky_object_dict(sky_interpreter_module_sys());
        lineno = 1;
    }
    else {
        frame_data = sky_frame_data(frame);
        globals = frame_data->f_globals;
        lineno = sky_code_lineno(frame_data->f_code, frame_data->f_lasti);
    }
    if (!(registry = sky_dict_get(globals,
                                  SKY_STRING_LITERAL("__warningregistry__"),
                                  NULL)))
    {
        registry = sky_dict_create();
        sky_dict_setitem(globals,
                         SKY_STRING_LITERAL("__warningregistry__"),
                         registry);
    }

    module = sky_dict_get(globals,
                          SKY_STRING_LITERAL("__name__"),
                          SKY_STRING_LITERAL("<string>"));

    filename = sky_dict_get(globals, SKY_STRING_LITERAL("__file__"), NULL);
    if (sky_object_isa(filename, sky_string_type)) {
        filename = sky_string_copy(filename);
        lower_filename = sky_string_lower(filename);
        if (sky_string_endswith(lower_filename,
                                SKY_STRING_LITERAL(".pyc"),
                                0,
                                SSIZE_MAX) ||
            sky_string_endswith(lower_filename,
                                SKY_STRING_LITERAL(".pyo"),
                                0,
                                SSIZE_MAX))
        {
            filename = sky_string_slice(filename,
                                        0,
                                        sky_string_len(filename) - 4,
                                        1);
        }
    }
    else if (sky_object_compare(module,
                                SKY_STRING_LITERAL("__main__"),
                                SKY_COMPARE_OP_EQUAL))
    {
        sky_object_t    argv, argv0;

        filename = SKY_STRING_LITERAL("__main__");
        argv = sky_object_getattr(sky_module_import(SKY_STRING_LITERAL("sys")),
                                  SKY_STRING_LITERAL("argv"),
                                  NULL);
        if (sky_object_bool(argv) &&
            sky_object_bool((argv0 = sky_sequence_get(argv, 0))))
        {
            filename = argv0;
        }
    }
    if (!filename) {
        filename = module;
    }

    if (sky_object_isnull(category)) {
        category = sky_UserWarning;
    }

    sky_error_warn_explicit(message,
                            category,
                            filename,
                            lineno,
                            module,
                            registry,
                            NULL);
}


void
sky_error_warn_explicit(sky_object_t    message,
                        sky_type_t      category,
                        sky_string_t    filename,
                        ssize_t         lineno,
                        sky_string_t    module,
                        sky_dict_t      registry,
                        sky_dict_t      module_globals)
{
    sky_string_t    source_line = NULL;

    sky_tuple_t     key;
    sky_object_t    action, instance, item;

    if (sky_object_isnull(category)) {
        category = sky_RuntimeWarning;
    }
    else if (sky_object_isinstance(category, sky_Warning)) {
        category = sky_object_type(category);
    }
    else if (!sky_object_issubclass(category, sky_Warning)) {
        sky_error_raise_string(
                sky_ValueError,
                "category is not a subclass of Warning");
    }
    if (sky_object_isnull(module)) {
        if (!sky_object_bool(filename)) {
            module = SKY_STRING_LITERAL("<unknown>");
        }
        else {
            module = sky_string_createfromobject(filename);
            if (sky_string_endswith(sky_string_lower(module),
                                    SKY_STRING_LITERAL(".py"),
                                    0,
                                    SSIZE_MAX))
            {
                module = sky_string_slice(module,
                                          0,
                                          sky_string_len(module) - 3,
                                          1);
            }
        }
    }
    else {
        module = sky_string_createfromobject(module);
    }

    if (!sky_object_isnull(module_globals)) {
        sky_object_t    lines, loader, source;
        sky_string_t    module_name;

        loader = sky_dict_get(module_globals,
                              SKY_STRING_LITERAL("__loader__"),
                              sky_NotSpecified);
        module_name = sky_dict_get(module_globals,
                                   SKY_STRING_LITERAL("__name__"),
                                   sky_NotSpecified);
        if (sky_object_hasattr(loader, SKY_STRING_LITERAL("get_source"))) {
            source = sky_object_callmethod(loader,
                                           SKY_STRING_LITERAL("get_source"),
                                           sky_tuple_pack(1, module_name),
                                           NULL);
            if (sky_object_bool(source)) {
                lines = sky_object_callmethod(source,
                                              SKY_STRING_LITERAL("splitlines"),
                                              NULL,
                                              NULL);
                source_line = sky_sequence_get(lines, lineno);
            }
        }
    }

    if (!sky_object_isa(message, sky_Warning)) {
        instance = sky_object_call(category, sky_tuple_pack(1, message), NULL);
    }
    else {
        category = sky_object_type(message);
        instance = message;
        message = sky_object_str(message);
    }

    if (!sky_object_isnull(registry)) {
        key = sky_tuple_pack(3, message, category, sky_integer_create(lineno));
        if (sky_object_bool(sky_dict_get(registry, key, sky_False))) {
            return;
        }
    }

    if (!(item = sky_error_warn_filter(category, message, lineno, module))) {
        return;
    }
    else if (sky_object_isa(item, sky_tuple_type)) {
        action = sky_tuple_get(item, 0);
    }
    else {
        action = item;
        item = NULL;
    }

    if (sky_object_compare(action,
                           SKY_STRING_LITERAL("error"),
                           SKY_COMPARE_OP_EQUAL))
    {
        sky_error_raise_object(category, instance);
    }

    if (sky_object_compare(action,
                           SKY_STRING_LITERAL("always"),
                           SKY_COMPARE_OP_NOT_EQUAL))
    {
        if (!sky_object_isnull(registry)) {
            sky_dict_setitem(registry, key, sky_True);
        }
        if (sky_object_compare(action, 
                               SKY_STRING_LITERAL("ignore"),
                               SKY_COMPARE_OP_EQUAL))
        {
            return;
        }
        if (sky_object_compare(action,
                               SKY_STRING_LITERAL("once"),
                               SKY_COMPARE_OP_EQUAL))
        {
            if (sky_object_isnull(registry)) {
                registry = sky_error_warn_onceregistry();
            }
            if (!sky_object_isnull(registry)) {
                key = sky_tuple_pack(2, message, category);
                if (sky_object_bool(sky_dict_get(registry, key, sky_False))) {
                    return;
                }
                sky_dict_setitem(registry, key, sky_True);
            }
        }
        else if (sky_object_compare(action,
                                    SKY_STRING_LITERAL("module"),
                                    SKY_COMPARE_OP_EQUAL))
        {
            if (!sky_object_isnull(registry)) {
                key = sky_tuple_pack(2, message, category);
                if (sky_object_bool(sky_dict_get(registry, key, sky_False))) {
                    return;
                }
                sky_dict_setitem(registry, key, sky_True);
            }
        }
        else if (sky_object_compare(action,
                                    SKY_STRING_LITERAL("default"),
                                    SKY_COMPARE_OP_NOT_EQUAL))
        {
            sky_error_raise_format(
                    sky_RuntimeError,
                    "Unrecognized action (%@) in warnings.filters:\n %@",
                    action,
                    item);
        }
    }

    sky_error_warn_display(message,
                           category,
                           instance,
                           filename,
                           lineno,
                           source_line);
}


void
sky_error_warn_initialize_library(unsigned int flags)
{
    sky_list_t      filters;
    sky_string_t    action;

    filters = sky_list_create(NULL);
    sky_list_append(filters,
                    sky_tuple_pack(5, SKY_STRING_LITERAL("ignore"),
                                      sky_None,
                                      sky_DeprecationWarning,
                                      sky_None,
                                      sky_integer_zero));
    sky_list_append(filters,
                    sky_tuple_pack(5, SKY_STRING_LITERAL("ignore"),
                                      sky_None,
                                      sky_PendingDeprecationWarning,
                                      sky_None,
                                      sky_integer_zero));
    sky_list_append(filters,
                    sky_tuple_pack(5, SKY_STRING_LITERAL("ignore"),
                                      sky_None,
                                      sky_ImportWarning,
                                      sky_None,
                                      sky_integer_zero));

    if (flags & SKY_LIBRARY_INITIALIZE_FLAG_BYTES_ERRORS) {
        action = SKY_STRING_LITERAL("error");
    }
    else if (flags & SKY_LIBRARY_INITIALIZE_FLAG_BYTES_WARNINGS) {
        action = SKY_STRING_LITERAL("default");
    }
    else {
        action = SKY_STRING_LITERAL("ignore");
    }
    sky_list_append(filters,
                    sky_tuple_pack(5, action,
                                      sky_None,
                                      sky_BytesWarning,
                                      sky_None,
                                      sky_integer_zero));

#if defined(NDEBUG)
    action = SKY_STRING_LITERAL("ignore");
#else
    action = SKY_STRING_LITERAL("always");
#endif
    sky_list_append(filters,
                    sky_tuple_pack(5, action,
                                      sky_None,
                                      sky_ResourceWarning,
                                      sky_None,
                                      sky_integer_zero));

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_error_warn_filters),
            filters,
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_error_warn__onceregistry),
            sky_dict_create(),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_error_warn__defaultaction),
            SKY_STRING_LITERAL("default"),
            SKY_TRUE);
}
