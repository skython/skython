/** @file
  * @brief Capsule objects
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_capsule_t Capsule Objects
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_CAPSULE_H__
#define __SKYTHON_CORE_SKY_CAPSULE_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The signature of a function called when a capsule object is visited during
  * garbage collection.
  *
  * @param[in]  capsule     the capsule object being visited.
  * @param[in]  data        the visitation data.
  */
typedef void (*sky_capsule_visit_t)(sky_capsule_t               capsule,
                                    sky_object_visit_data_t *   data);


/** Create a new instance of a capsule object.
  *
  * @param[in]  pointer     the raw pointer to wrap with the new capsule object
  *                         instance.
  * @param[in]  name        an optional name for the capsule object. May be
  *                         @c NULL.
  * @param[in]  destructor  a function to be executed when the capsule object
  *                         is garbage collected. It will be called with the
  *                         wrapped raw pointer. May be @c NULL.
  * @param[in]  visit       a function to be executed when the capsule object
  *                         is visited during garbage collection.
  *                         May be @c NULL.
  * @return     the new capsule object instance.
  */
SKY_EXTERN sky_capsule_t
sky_capsule_create(void *               pointer,
                   const char *         name,
                   sky_free_t           destructor,
                   sky_capsule_visit_t  visit);

/** Determine whether an object is a valid capsule.
  *
  * @param[in]  object      the object to test.
  * @param[in]  name        the name that the capsule should have.
  * @return     @c SKY_TRUE if @a object is a valid capsule having the name
  *             @a name; otherwise, @c SKY_FALSE.
  */
SKY_EXTERN sky_bool_t
sky_capsule_isvalid(sky_object_t object, const char *name);

/** Return the raw pointer wrapped by a capsule object.
  * The @a name parameter must compare exactly to the name stored in the
  * capsule. If the name stored in the capsule is @c NULL, the @a name passed
  * in must also be @c NULL. The C function strcmp() is used to compare names.
  * If the names do not match, a @c sky_ValueError exception will be raised.
  *
  * @param[in]  capsule     the capsule object to query.
  * @param[in]  name        the capsule name.
  * @return     the raw pointer stored in @a capsule if @a name matches the
  *             name stored in the capsule.
  */
SKY_EXTERN void *
sky_capsule_pointer(sky_capsule_t capsule, const char *name);

/** Return the name assigned to a capsule object. **/
SKY_EXTERN const char *
sky_capsule_name(sky_capsule_t capsule);


SKY_CDECLS_END


#endif  /* __SKYTHON_SKY_CAPSULE_H__ */


/** @} **/
/** @} **/
