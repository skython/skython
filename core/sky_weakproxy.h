/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_weakproxy Weak Reference Proxies
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_WEAKPROXY_H__
#define __SKYTHON_CORE_SKY_WEAKPROXY_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new weak reference proxy object instance.
  *
  * @param[in]  object      the object to be weakly referenced.
  * @param[in]  callback    an optional callable object to be called when
  *                         @a object becomes unreachable.
  * @return     a new weak reference proxy object instance.
  */
SKY_EXTERN sky_callableweakproxy_t
sky_callableweakproxy_create(sky_object_t object, sky_object_t callback);


/** Create a new weak reference proxy object instance.
  *
  * @param[in]  object      the object to be weakly referenced.
  * @param[in]  callback    an optional callable object to be called when
  *                         @a object becomes unreachable.
  * @return     a new weak reference proxy object instance.
  */
SKY_EXTERN sky_weakproxy_t
sky_weakproxy_create(sky_object_t object, sky_object_t callback);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_WEAKPROXY_H__ */

/** @} **/
/** @} **/
