/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_type Object Types
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_TYPE_H__
#define __SKYTHON_CORE_SKY_TYPE_H__ 1


#include "sky_base.h"
#include "sky_buffer.h"
#include "sky_data_type.h"
#include "sky_getset.h"
#include "sky_malloc.h"
#include "sky_member.h"
#include "sky_native_code.h"
#include "sky_object.h"
#include "sky_sequence.h"


/** The signature of a function called to initialize an object instance after
  * allocation.
  * The initialization of an object is kept separate from allocation because
  * the initializer for an object instance is called for each type in the MRO
  * for the object's type. The initializer takes no arguments other than the
  * instance of the object being initialized, and so is really responsible for
  * putting the object into a state that can be safely finalized later.
  *
  * @param[in]  self    the object instance to be initialized.
  * @param[in]  data    the object's instance data for the type in the MRO for
  *                     which the initializer is being called.
  */
typedef void
(*sky_type_initialize_t)(sky_object_t self, void *data);


/** The signature of a function called to finalize an object instance prior to
  * deallocation.
  * The finalization of an object is kept separate from deallocation because it
  * occurs in two separate passes. All objects being swept are first finalized,
  * and then they're all deallocated. The reason for this is that finalization
  * may need to refer to another object that's also being swept in the same
  * pass (think of a weakly referenced object needing to zero out the weak
  * references to it). This finalization function must not resurrect the
  * object; to do so is a programming error that will cause an assertion to be
  * raised. Like initialization, the finalize function for each type in an
  * object instance's MRO is called; however, finalizers are called in reverse
  * order of initialization.
  *
  * @warning    It is a fatal error to raise an exception in a finalizer.
  *             Doing so will terminate the application via sky_error_fatal().
  *
  * @param[in]  self    the object instance to be finalized.
  * @param[in]  data    the object's instance data for the type in the MRO for
  *                     which the finalizer is being called.
  */
typedef void
(*sky_type_finalize_t)(sky_object_t self, void *data);


/** Create a new type object instance.
  * Creates a new type object instance. This is basically the implementation of
  * the class keyword in Python. If no metaclass is supplied, the appropriate
  * default will be determined from the base classes, or @c sky_type_type will
  * be used if there are no bases. If the metaclass has a __prepare__() method,
  * it will be called to obtain the locals for the new type, to be used when
  * evaluating @a code. If there's no __prepare__() method, an empty dictionary
  * will be created. If @a code is not @c NULL, it will be executed. Finally,
  * the metaclass's __new__() method will be called to actually create the type
  * object instance. For more detailed information, see Python PEP 3115.
  *
  * @param[in]  qualname    the qualified name of the new type.
  * @param[in]  bases       a sequence containing the list of base types. If
  *                         the sequence is empty or @c NULL, the base object
  *                         will be used as the type's only base.
  * @param[in]  metaclass   the metaclass to use for the type. May be @c NULL.
  * @param[in]  code        code to execute to initialize the type. It will be
  *                         executed after calling __prepare__(), but before
  *                         actually creating the new type. The dictionary
  *                         returned by __prepare__() will be used as locals
  *                         for the code. May be @c NULL.
  * @param[in]  closure     the closure to pass to sky_code_execute() when
  *                         @a code is executed, if any. May be @c NULL.
  * @param[in]  globals     the globals to use when executing @a code.
  * @param[in]  kws         an optional dictionary to pass to the new type's
  *                         metaclass's __prepare__() method as keyword
  *                         arguments.
  */
SKY_EXTERN sky_type_t
sky_type_create(sky_string_t    qualname,
                sky_object_t    bases,
                sky_object_t    metaclass,
                sky_code_t      code,
                sky_tuple_t     closure,
                sky_dict_t      globals,
                sky_dict_t      kws);


/** Flags that may be used when creating a built-in type. **/
typedef enum sky_type_create_flag_e {
    /** Set if instances of the type should have a __dict__ attribute. **/
    SKY_TYPE_CREATE_FLAG_HAS_DICT       =   0x2,
    /** Set if the __dict__ attribute should be read-only. **/
    SKY_TYPE_CREATE_FLAG_DICT_READONLY  =   0x4,
    /** Set if the type may not be used as a base type. **/
    SKY_TYPE_CREATE_FLAG_FINAL          =   0x8,
} sky_type_create_flag_t;


/** A structure used for creating built-in types.
  * The structure contains optional information that may be used to customize
  * the behavior of a built-in type.
  */
typedef struct sky_type_template_s {
    /** Only fields in the structure up to this point will be considered valid.
      * This is to be used for adding fields in the future without breaking
      * binary compatibility.
      */
    size_t                              sizeof_struct;

    /** This is a function to be used to allocate memory for instances of the
      * type.
      */
    sky_memalign_t                      allocate;

    /** This is a function to be used to deallocate memory for instances of the
      * type.
      */
    sky_free_t                          deallocate;

    /** This is a function to be used to initialize the object. It should be
      * used to initialize private data prior to the object's __init__() method
      * being called.
      */
    sky_type_initialize_t               initialize;

    /** This is a function to be used to finalize the object. It will be called
      * during garbage collection to clean up private data. Do not free the
      * object instance itself. That is to be done by the deallocate function,
      * and the two are kept separate for very good reasons. Skython will crash
      * if the instance is freed in this function.
      */
    sky_type_finalize_t                 finalize;

    /** This is a function to be used to visit the object. It'll be called at
      * various times primarily for garbage collection purposes. Its
      * implementation should visit every strong object reference held in
      * private data.
      */
    sky_object_visit_t                  visit;

    /** This is a function to be used to implement fast iteration. **/
    sky_sequence_iterate_t              iterate;
    /** This is a function to be used to clean up any state used during fast
      * iteration.
      */
    sky_sequence_iterate_cleanup_t      iterate_cleanup;
    /** This is a function to be used to acquire a buffer for an instance. **/
    sky_buffer_acquire_t                buffer_acquire;
    /** This is a function to be used to release a buffer for an instance. **/
    sky_buffer_release_t                buffer_release;
} sky_type_template_t;


/** Create a new built-in type object instance.
  * This will create a new type object that is implemented in C. Built-in types
  * can have private data areas and special functions (defined in the template)
  * to control behavior that otherwise cannot be control through Python. In
  * addition, built-in types are sometimes treated specially. For example, it
  * is not possible to set/delete attributes of a built-in type from Python
  * code.
  *
  * @param[in]  qualname        the qualified name of the type.
  * @param[in]  doc             the doc string for the type.
  * @param[in]  flags           flags to be used for the type (see
  *                             @c sky_type_create_flag_t).
  * @param[in]  data_size       number of bytes needed for private data.
  * @param[in]  data_alignment  alignment to use for private data. Default
  *                             alignment will be used if specified as 0.
  * @param[in]  bases           the list of base types for this type.
  * @param[in]  template        the template to be used for setting optional
  *                             behavioral control functions.
  * @return     the new built-in type object instance.
  */
SKY_EXTERN sky_type_t
sky_type_createbuiltin(sky_string_t         qualname,
                       const char *         doc,
                       uint32_t             flags,
                       size_t               data_size,
                       size_t               data_alignment,
                       sky_tuple_t          bases,
                       sky_type_template_t *template);


/** Determine if one type is a sub-type of another type.
  *
  * @param[in]  type        the type that may be the super-type of @a subtype.
  * @param[in]  subtype     the type that may be the sub-type of @a type.
  * @return     @c SKY_TRUE if @a subtype is a sub-type of @a type, or
  *             @c SKY_FALSE if not.
  */
SKY_EXTERN sky_bool_t
sky_type_issubtype(sky_type_t type, sky_type_t subtype);


/** Lookup an attribute in a type's inheritance hierarchy.
  * Searches through each type in the primary type's MRO to find the requested
  * attribute. If the attribute is not found, @c NULL is returned; otherwise,
  * the attribute value is returned. The attribute name must be a string.
  *
  * @param[in]  type    the primary type to search.
  * @param[in]  name    the name of the attribute to find.
  * @return     the attribute value if found, or @c NULL.
  */
SKY_EXTERN sky_object_t
sky_type_lookup(sky_type_t type, sky_string_t name);


/** Return the name of the module to which a type belongs.
  * This function implements the __module__ attribute for the type. For builtin
  * types, the module name is typically "builtins" unless the type has a dotted
  * decimal qualifier (e.g., a "built-in" extension type).
  *
  * @param[in]  type    the type for which its module is to be returned.
  * @return     the name of the module to which @a type belongs.
  */
SKY_EXTERN sky_string_t
sky_type_module(sky_type_t type);


/** Return the name of a type.
  *
  * @param[in]  type    the type for which the name is to be returned.
  * @return     the name of @a type.
  */
SKY_EXTERN sky_string_t
sky_type_name(sky_type_t type);


/** Return the qualified name of a type.
  *
  * @param[in]  type    the type for which the qualified name is to be returned.
  * @return     the qualified name of @a type.
  */
SKY_EXTERN sky_string_t
sky_type_qualname(sky_type_t type);


/** Set an attribute on a built-in type.
  *
  * @param[in]  type    the type object instance for which the attribute is
  *                     to be set. The type must be a built-in type.
  * @param[in]  key     the name of the attribute, which is assumed to be a
  *                     UTF-8 encoded source-level literal string.
  * @param[in]  value   the value to set for the attribute.
  */
SKY_EXTERN void
sky_type_setattr_builtin(sky_type_t     type,
                         const char *   key,
                         sky_object_t   value);


/** Set an attribute on a built-in type with a getset descriptor.
  * This is a convenience function that creates a new getset descriptor and
  * sets it on a type attribute via sky_type_setattr_builtin().
  *
  * @param[in]  type    the type on which to set the attribute.
  * @param[in]  name    the attribute to set.
  * @param[in]  doc     the doc string for the attribute. May be @c NULL.
  * @param[in]  get     the C function to call when the attribute is
  *                     retrieved (implementation of __get__()).
  * @param[in]  set     the C function to call when the attribute is
  *                     set or deleted (implementations of __set__() and
  *                     __delete__()).
  */
SKY_EXTERN_INLINE void
sky_type_setattr_getset(sky_type_t          type,
                        const char *        name,
                        const char *        doc,
                        sky_getset_get_t    get,
                        sky_getset_set_t    set)
{
    sky_type_setattr_builtin(
            type,
            name,
            sky_getset_create(type, name, doc, get, set));
}


/** Set numerous type members with getset descriptors.
  * This is a convenience wrapper around sky_type_setattr_getset() that allows
  * a single invocation to set a number of members for a type. The first
  * argument is the type followed by a variable number of arguments to set the
  * members. The list of members must be terminated with a @c NULL.
  *
  * @param[in]  type    the type for which the members are to be set.
  * @param[in]  ...     a @c NULL terminated list of getset data (which shall
  *                     be name, doc, get, and set for each member).
  */
SKY_EXTERN void
sky_type_setgetsets(sky_type_t type, ...);


/** Set an attribute on a built-in type with a member descriptor.
  * This is a convenience function that creates a new member descriptor and
  * sets it on a type attribute via sky_type_setattr_builtin().
  *
  * @param[in]  type        the type on which to set the attribute.
  * @param[in]  name        the name of the attribute.
  * @param[in]  doc         the doc string for the attribute. May be @c NULL.
  * @param[in]  offset      offset to the data in the type's instance data.
  * @param[in]  data_type   the data type.
  * @param[in]  flags       flags for the member descriptor.
  * @param[in]  ...         extra arguments dependent upon @a data_type.
  */
SKY_EXTERN void
sky_type_setattr_member(sky_type_t      type,
                        const char *    name,
                        const char *    doc,
                        size_t          offset,
                        sky_data_type_t data_type,
                        unsigned int    flags,
                        ...);


/** Set numerous type members with member descriptors.
  * This is a convenience wrapper around sky_type_setattr_member() that allows
  * a single invocation to set a number of members for a type. The first
  * argument is the type followed by a variable number of arguments to set the
  * members. The list of members must be terminated with a @c NULL.
  *
  * @param[in]  type    the type for which the members are to be set.
  * @param[in]  ...     a @c NULL terminated list of member data (which shall
  *                     be name, doc, offset, data_type, and flags for each
  *                     member).
  */
SKY_EXTERN void
sky_type_setmembers(sky_type_t type, ...);


/** Set a type's method slot with a native C implementation.
  * This function is intended to be a convenience for the implementation of
  * special functions in C. Not only does it provide a convenience, it also
  * ensures that names, doc strings, etc. are all consistent for method slots,
  * removing possible errors in keywords, types, default values, etc.
  *
  * @param[in]  type        the type for which the method slot is to be set.
  * @param[in]  name        the name of the method slot to set.
  * @param[in]  funcptr     the function that implements the method.
  */
SKY_EXTERN void
sky_type_setmethodslot(sky_type_t                   type,
                       const char *                 name,
                       sky_native_code_function_t   funcptr);


/** Set a type's method slot with a native C implementation.
  * This function is intended to be a convenience for the implementation of
  * special functions in C. Not only does it provide a convenience, it also
  * ensures that names, doc strings, etc. are all consistent for method slots,
  * removing possible errors in keywords, types, default values, etc.
  *
  * @param[in]  type        the type for which the method slot is to be set.
  * @param[in]  name        the name of the method slot to set.
  * @param[in]  funcptr     the function that implements the method.
  * @param[in]  keyword     the first keyword for the first parameter for the
  *                         slot method. May be @c NULL if the default
  *                         parameters for the method are to be used. This is
  *                         most commonly used for @c __call__, @c __init__,
  *                         and @c __new__.
  * @param[in]  ...         additional values for parameters, terminated by a
  *                         @c NULL pointer. Ignored if @a keyword is @c NULL.
  *                         See sky_parameter_list_createwithparameters() for
  *                         more information.
  */
SKY_EXTERN void
sky_type_setmethodslotwithparameters(sky_type_t                 type,
                                     const char *               name,
                                     sky_native_code_function_t funcptr,
                                     const char *               keyword,
                                     ...);


/** Set a type's method slot with a native C implementation.
  * This function is intended to be a convenience for the implementation of
  * special functions in C. Not only does it provide a convenience, it also
  * ensures that names, doc strings, etc. are all consistent for method slots,
  * removing possible errors in keywords, types, default values, etc.
  *
  * @param[in]  type        the type for which the method slot is to be set.
  * @param[in]  name        the name of the method slot to set.
  * @param[in]  funcptr     the function that implements the method.
  * @param[in]  parameters  the parameters to use for the method slot, which
  *                         may be @c NULL to use the defaults.
  */
SKY_EXTERN void
sky_type_setmethodslotwithparameterlist(sky_type_t                  type,
                                        const char *                name,
                                        sky_native_code_function_t  funcptr,
                                        sky_parameter_list_t        parameters);


/** Set numerous type method slots with native C implementations.
  * This is a convenience wrapper around sky_type_methodslot() that allows a
  * single invocation to set numerous method slots. The first argument is the
  * type followed by a variable number of arguments to set the slots and
  * functions. The list of slots should be terminated with a @c NULL.
  *
  * @param[in]  type    the type for which the method slots are to be set.
  * @param[in]  ...     a @c NULL terminated list of slot names (which shall
  *                     be @c const @c char @c *) followed by function pointers
  *                     to the function that implements the slot function.
  */
SKY_EXTERN void
sky_type_setmethodslots(sky_type_t type, ...);


/** Set the name of a type object.
  *
  * @param[in]  type    the type to have its name set.
  * @param[in]  name    the new name for the type, which must not contain any
  *                     nul bytes when UTF-8 encoded.
  */
SKY_EXTERN void
sky_type_setname(sky_type_t type, sky_string_t name);


#endif  /** __SKYTHON_CORE_SKY_TYPE_H__ */

/** @} **/
/** @} **/
