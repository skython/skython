#ifndef __SKYTHON_CORE_SKY_BYTESTRING_H__
#define __SKYTHON_CORE_SKY_BYTESTRING_H__


SKY_CDECLS_BEGIN


typedef struct sky_bytestring_s {
    uint8_t *                           bytes;
    ssize_t                             used;
    ssize_t                             size;
    union {
        sky_free_t                      free;
        uint8_t                         tiny_bytes[sizeof(void *)];
    } u;
} sky_bytestring_t;

#define SKY_BYTESTRING_INITIALIZER      { NULL, 0, 0, { NULL } }


typedef sky_object_t
        (*sky_bytestring_createfrombytes_t)(const uint8_t *, ssize_t);


extern void
sky_bytestring_appendbytes(sky_bytestring_t *   self,
                           const void *         bytes,
                           size_t               nbytes);

extern sky_bool_t
sky_bytestring_capitalize(const sky_bytestring_t *  self,
                          sky_bytestring_t *        target);

extern sky_bool_t
sky_bytestring_center(const sky_bytestring_t *  self,
                      ssize_t                   width,
                      uint8_t                   fillchar,
                      sky_bytestring_t *        target);

extern void
sky_bytestring_cleanup(sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_compare(const sky_bytestring_t * self,
                       const sky_bytestring_t * other,
                       sky_compare_op_t         compare_op);

extern void
sky_bytestring_concat(sky_bytestring_t *        self,
                      const sky_bytestring_t *  other);

extern ssize_t
sky_bytestring_count(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   sub,
                     ssize_t                    start,
                     ssize_t                    end,
                     ssize_t                    maxcount);

extern sky_string_t
sky_bytestring_decode(const sky_bytestring_t *  self,
                      sky_string_t              encoding,
                      sky_string_t              errors);

extern void
sky_bytestring_delete(sky_bytestring_t *self,
                      ssize_t           start,
                      ssize_t           end,
                      ssize_t           step);

extern void
sky_bytestring_destroy(sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_endswith(const sky_bytestring_t *self,
                        const sky_bytestring_t *suffix,
                        ssize_t                 start,
                        ssize_t                 end);

extern sky_bool_t
sky_bytestring_expandtabs(const sky_bytestring_t *  self,
                          ssize_t                   tabsize,
                          sky_bytestring_t *        target);

extern ssize_t
sky_bytestring_find(const sky_bytestring_t *self,
                    const sky_bytestring_t *sub,
                    ssize_t                 start,
                    ssize_t                 end);

extern void
sky_bytestring_fromhex(sky_bytestring_t *   self,
                       const char *         hex,
                       size_t               hexlen);

extern void
sky_bytestring_initfrombytes(sky_bytestring_t * self,
                             const void *       bytes,
                             ssize_t            nbytes,
                             ssize_t            capacity);

static inline void
sky_bytestring_initwithbytes(sky_bytestring_t * self,
                             void *             bytes,
                             ssize_t            nbytes,
                             sky_free_t         free)
{
    self->bytes = bytes;
    self->used = nbytes;
    self->size = nbytes;
    self->u.free = free;
}

extern void
sky_bytestring_insert(sky_bytestring_t *self,
                      ssize_t           index,
                      const void *      bytes,
                      ssize_t           nbytes);

extern sky_bool_t
sky_bytestring_isalnum(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_isalpha(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_isdigit(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_islower(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_isspace(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_istitle(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_isupper(const sky_bytestring_t *self);

extern sky_bool_t
sky_bytestring_ljust(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     uint8_t                    fillchar,
                     sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_lower(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_lstrip(const sky_bytestring_t *  self,
                      const sky_bytestring_t *  bytes,
                      sky_bytestring_t *        target);

extern void
sky_bytestring_maketrans(const sky_bytestring_t *   from,
                         const sky_bytestring_t *   to,
                         sky_bytestring_t *         target);

extern uint8_t
sky_bytestring_pop(sky_bytestring_t *   self,
                   ssize_t              index);

extern void
sky_bytestring_repeat(sky_bytestring_t *self, ssize_t count);

extern sky_bool_t
sky_bytestring_replace(const sky_bytestring_t * self,
                       const sky_bytestring_t * old,
                       const sky_bytestring_t * new,
                       ssize_t                  count,
                       sky_bytestring_t *       target);

extern sky_string_t
sky_bytestring_repr(sky_bytestring_t *  self,
                    const char *        prefix,
                    const char *        suffix);

extern uint8_t *
sky_bytestring_resize(sky_bytestring_t *self,
                      ssize_t           size);

extern ssize_t
sky_bytestring_rfind(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   sub,
                     ssize_t                    start,
                     ssize_t                    end);

extern sky_bool_t
sky_bytestring_rjust(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     uint8_t                    fillchar,
                     sky_bytestring_t *         target);

extern sky_list_t
sky_bytestring_rsplit(const sky_bytestring_t *          self,
                      const sky_bytestring_t *          sep,
                      ssize_t                           maxsplit,
                      sky_bytestring_createfrombytes_t  createfrombytes);

extern sky_bool_t
sky_bytestring_rstrip(const sky_bytestring_t *  self,
                      const sky_bytestring_t *  bytes,
                      sky_bytestring_t *        target);

extern sky_list_t
sky_bytestring_split(const sky_bytestring_t *           self,
                     const sky_bytestring_t *           sep,
                     ssize_t                            maxsplit,
                     sky_bytestring_createfrombytes_t   createfrombytes);

extern sky_bool_t
sky_bytestring_startswith(const sky_bytestring_t *  self,
                          const sky_bytestring_t *  prefix,
                          ssize_t                   start,
                          ssize_t                   end);

extern sky_bool_t
sky_bytestring_strip(const sky_bytestring_t *   self,
                     const sky_bytestring_t *   bytes,
                     sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_swapcase(const sky_bytestring_t *self,
                        sky_bytestring_t *      target);

extern sky_bool_t
sky_bytestring_title(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_translate(const sky_bytestring_t *   self,
                         const sky_bytestring_t *   table,
                         const sky_bytestring_t *   deletechars,
                         sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_upper(const sky_bytestring_t *   self,
                     sky_bytestring_t *         target);

extern sky_bool_t
sky_bytestring_zfill(const sky_bytestring_t *   self,
                     ssize_t                    width,
                     sky_bytestring_t *         target);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_BYTESTRING_H__ */
