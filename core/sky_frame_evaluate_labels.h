    static void *opcode_handlers[256] = {
        &&HANDLE_OPCODE_NOP,                    /* 0x00 */

        &&HANDLE_OPCODE_PUSH_CONST,             /* 0x01 */
        &&HANDLE_OPCODE_PUSH_LOCAL,             /* 0x02 */
        &&HANDLE_OPCODE_PUSH_GLOBAL,            /* 0x03 */
        &&HANDLE_OPCODE_PUSH_NAME,              /* 0x04 */
        &&HANDLE_OPCODE_PUSH_FREE,              /* 0x05 */
        &&HANDLE_OPCODE_PUSH_CELL,              /* 0x06 */
        &&HANDLE_OPCODE_PUSH_ATTRIBUTE,         /* 0x07 */
        &&HANDLE_OPCODE_PUSH_ITEM,              /* 0x08 */
        &&HANDLE_OPCODE_PUSH_ITERATOR,          /* 0x09 */
        &&HANDLE_OPCODE_PUSH_ITERATOR_NEXT,     /* 0x0A */
        &&HANDLE_OPCODE_PUSH_TOP,               /* 0x0B */
        &&HANDLE_OPCODE_PUSH_CLOSURE_CELL,      /* 0x0C */
        &&HANDLE_OPCODE_PUSH_CLOSURE_FREE,      /* 0x0D */
        &&invalid_opcode,                       /* 0x0E */
        &&invalid_opcode,                       /* 0x0F */

        &&HANDLE_OPCODE_POP,                    /* 0x10 */
        &&HANDLE_OPCODE_POP_LOCAL,              /* 0x11 */
        &&HANDLE_OPCODE_POP_GLOBAL,             /* 0x12 */
        &&HANDLE_OPCODE_POP_NAME,               /* 0x13 */
        &&HANDLE_OPCODE_POP_FREE,               /* 0x14 */
        &&HANDLE_OPCODE_POP_CELL,               /* 0x15 */
        &&HANDLE_OPCODE_POP_ATTRIBUTE,          /* 0x16 */
        &&HANDLE_OPCODE_POP_ITEM,               /* 0x17 */

        &&invalid_opcode,                       /* 0x18 */
        &&HANDLE_OPCODE_DELETE_LOCAL,           /* 0x19 */
        &&HANDLE_OPCODE_DELETE_GLOBAL,          /* 0x1A */
        &&HANDLE_OPCODE_DELETE_NAME,            /* 0x1B */
        &&HANDLE_OPCODE_DELETE_FREE,            /* 0x1C */
        &&HANDLE_OPCODE_DELETE_CELL,            /* 0x1D */
        &&HANDLE_OPCODE_DELETE_ATTRIBUTE,       /* 0x1E */
        &&HANDLE_OPCODE_DELETE_ITEM,            /* 0x1F */

        &&HANDLE_OPCODE_BINARY_ADD,             /* 0x20 */
        &&HANDLE_OPCODE_BINARY_SUB,             /* 0x21 */
        &&HANDLE_OPCODE_BINARY_MUL,             /* 0x22 */
        &&HANDLE_OPCODE_BINARY_DIV,             /* 0x23 */
        &&HANDLE_OPCODE_BINARY_MOD,             /* 0x24 */
        &&HANDLE_OPCODE_BINARY_POW,             /* 0x25 */
        &&HANDLE_OPCODE_BINARY_LSHIFT,          /* 0x26 */
        &&HANDLE_OPCODE_BINARY_RSHIFT,          /* 0x27 */
        &&HANDLE_OPCODE_BINARY_OR,              /* 0x28 */
        &&HANDLE_OPCODE_BINARY_XOR,             /* 0x29 */
        &&HANDLE_OPCODE_BINARY_AND,             /* 0x2A */
        &&HANDLE_OPCODE_BINARY_FLOORDIV,        /* 0x2B */
        &&invalid_opcode,                       /* 0x2C */
        &&invalid_opcode,                       /* 0x2D */
        &&invalid_opcode,                       /* 0x2E */
        &&invalid_opcode,                       /* 0x2F */

        &&HANDLE_OPCODE_INPLACE_ADD,            /* 0x30 */
        &&HANDLE_OPCODE_INPLACE_SUB,            /* 0x31 */
        &&HANDLE_OPCODE_INPLACE_MUL,            /* 0x32 */
        &&HANDLE_OPCODE_INPLACE_DIV,            /* 0x33 */
        &&HANDLE_OPCODE_INPLACE_MOD,            /* 0x34 */
        &&HANDLE_OPCODE_INPLACE_POW,            /* 0x35 */
        &&HANDLE_OPCODE_INPLACE_LSHIFT,         /* 0x36 */
        &&HANDLE_OPCODE_INPLACE_RSHIFT,         /* 0x37 */
        &&HANDLE_OPCODE_INPLACE_OR,             /* 0x38 */
        &&HANDLE_OPCODE_INPLACE_XOR,            /* 0x39 */
        &&HANDLE_OPCODE_INPLACE_AND,            /* 0x3A */
        &&HANDLE_OPCODE_INPLACE_FLOORDIV,       /* 0x3B */
        &&invalid_opcode,                       /* 0x3C */
        &&invalid_opcode,                       /* 0x3D */
        &&invalid_opcode,                       /* 0x3E */
        &&invalid_opcode,                       /* 0x3F */

        &&HANDLE_OPCODE_UNARY_NEGATIVE,         /* 0x40 */
        &&HANDLE_OPCODE_UNARY_POSITIVE,         /* 0x41 */
        &&HANDLE_OPCODE_UNARY_INVERT,           /* 0x42 */
        &&HANDLE_OPCODE_UNARY_NOT,              /* 0x43 */
        &&invalid_opcode,                       /* 0x44 */
        &&invalid_opcode,                       /* 0x45 */
        &&HANDLE_OPCODE_DISPLAY,                /* 0x46 */
        &&HANDLE_OPCODE_COMPARE_EXCEPTION,      /* 0x47 */
        &&HANDLE_OPCODE_COMPARE,                /* 0x48 */
        &&HANDLE_OPCODE_UNPACK_SEQUENCE,        /* 0x49 */
        &&HANDLE_OPCODE_UNPACK_STARRED,         /* 0x4A */
        &&HANDLE_OPCODE_LIST_APPEND,            /* 0x4B */
        &&HANDLE_OPCODE_SET_ADD,                /* 0x4C */
        &&HANDLE_OPCODE_IMPORT_MODULE,          /* 0x4D */
        &&HANDLE_OPCODE_IMPORT_STAR,            /* 0x4E */
        &&HANDLE_OPCODE_IMPORT_NAME,            /* 0x4F */

        &&HANDLE_OPCODE_BUILD_CLASS,            /* 0x50 */
        &&HANDLE_OPCODE_BUILD_DICT,             /* 0x51 */
        &&HANDLE_OPCODE_BUILD_FUNCTION,         /* 0x52 */
        &&invalid_opcode,                       /* 0x53 */
        &&HANDLE_OPCODE_BUILD_LIST,             /* 0x54 */
        &&HANDLE_OPCODE_BUILD_SET,              /* 0x55 */
        &&HANDLE_OPCODE_BUILD_SLICE,            /* 0x56 */
        &&HANDLE_OPCODE_BUILD_TUPLE,            /* 0x57 */
        &&invalid_opcode,                       /* 0x58 */
        &&invalid_opcode,                       /* 0x59 */
        &&invalid_opcode,                       /* 0x5A */
        &&invalid_opcode,                       /* 0x5B */
        &&invalid_opcode,                       /* 0x5C */
        &&invalid_opcode,                       /* 0x5D */
        &&HANDLE_OPCODE_ROTATE_TWO,             /* 0x5E */
        &&HANDLE_OPCODE_ROTATE_THREE,           /* 0x5F */

        &&HANDLE_OPCODE_CALL,                   /* 0x60 */
        &&HANDLE_OPCODE_CALL_ARGS,              /* 0x61 */
        &&HANDLE_OPCODE_CALL_KWS,               /* 0x62 */
        &&HANDLE_OPCODE_CALL_ARGS_KWS,          /* 0x63 */
        &&HANDLE_OPCODE_RETURN,                 /* 0x64 */
        &&HANDLE_OPCODE_YIELD,                  /* 0x65 */
        &&HANDLE_OPCODE_YIELD_FROM,             /* 0x66 */
        &&HANDLE_OPCODE_RAISE,                  /* 0x67 */
        &&HANDLE_OPCODE_JUMP,                   /* 0x68 */
        &&HANDLE_OPCODE_JUMP_IF_FALSE,          /* 0x69 */
        &&HANDLE_OPCODE_JUMP_IF_TRUE,           /* 0x6A */
        &&invalid_opcode,                       /* 0x6B */
        &&invalid_opcode,                       /* 0x6C */
        &&invalid_opcode,                       /* 0x6D */
        &&invalid_opcode,                       /* 0x6E */
        &&invalid_opcode,                       /* 0x6F */

        &&HANDLE_OPCODE_WITH_ENTER,             /* 0x70 */
        &&HANDLE_OPCODE_WITH_EXIT,              /* 0x71 */
        &&HANDLE_OPCODE_TRY_EXCEPT,             /* 0x72 */
        &&HANDLE_OPCODE_TRY_FINALLY,            /* 0x73 */
        &&HANDLE_OPCODE_TRY_EXIT,               /* 0x74 */
        &&HANDLE_OPCODE_EXCEPT_ENTER,           /* 0x75 */
        &&HANDLE_OPCODE_EXCEPT_EXIT,            /* 0x76 */
        &&HANDLE_OPCODE_FINALLY_ENTER,          /* 0x77 */
        &&HANDLE_OPCODE_FINALLY_EXIT,           /* 0x78 */
        &&invalid_opcode,                       /* 0x79 */
        &&invalid_opcode,                       /* 0x7A */
        &&invalid_opcode,                       /* 0x7B */
        &&invalid_opcode,                       /* 0x7C */
        &&invalid_opcode,                       /* 0x7D */
        &&invalid_opcode,                       /* 0x7E */
        &&invalid_opcode,                       /* 0x7F */

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,

        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
        &&invalid_opcode, &&invalid_opcode, &&invalid_opcode, &&invalid_opcode,
    };
