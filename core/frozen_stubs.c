#include "../modules/builtin_modules.h"

sky_builtin_module_t sky_builtin_modules[] = {
    { "builtins",   skython_module_builtins_initialize, NULL, 0 },
    { "sys",        skython_module_sys_initialize, NULL, 0 },

    { NULL, NULL, NULL, 0 },
};
