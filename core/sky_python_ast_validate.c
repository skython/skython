#include "sky_private.h"
#include "sky_python_ast.h"


/* The validation done here is taken exactly from CPython, primarily so that
 * it passes the AST unit test, which is looking for very specific errors and
 * error messages.
 */


static void
sky_python_ast_validate_expr(sky_object_t                   node,
                             sky_python_ast_expr_context_t  ctx);

static void
sky_python_ast_validate_expr_sequence(sky_object_t                  sequence,
                                      sky_python_ast_expr_context_t ctx,
                                      sky_bool_t                    null_ok);

static void
sky_python_ast_validate_stmt(sky_object_t node);

static void
sky_python_ast_validate_stmt_sequence(sky_object_t sequence);


static void
sky_python_ast_validate_nonempty_sequence(sky_object_t  sequence,
                                          const char *  what,
                                          const char *  owner)
{
    if (!sky_object_len(sequence)) {
        sky_error_raise_format(sky_ValueError, "empty %s on %s", what, owner);
    }
}


static void
sky_python_ast_validate_args(sky_object_t sequence)
{
    sky_python_ast_arg_t    arg;

    SKY_SEQUENCE_FOREACH(sequence, arg) {
        if (!sky_object_isnull(arg->annotation)) {
            sky_python_ast_validate_expr(arg->annotation, sky_python_ast_Load);
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_ast_validate_keywords(sky_object_t sequence)
{
    sky_python_ast_keyword_t    keyword;

    SKY_SEQUENCE_FOREACH(sequence, keyword) {
        sky_python_ast_validate_expr(keyword->value, sky_python_ast_Load);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_ast_validate_arguments(sky_python_ast_arguments_t node)
{
    sky_python_ast_validate_args(node->args);
    if (!sky_object_isnull(node->varargannotation)) {
        if (sky_object_isnull(node->vararg)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "varargannotation but no vararg on arguments");
        }
        sky_python_ast_validate_expr(node->varargannotation,
                                     sky_python_ast_Load);
    }
    sky_python_ast_validate_args(node->kwonlyargs);
    if (!sky_object_isnull(node->kwargannotation)) {
        if (sky_object_isnull(node->kwarg)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "kwargannotation but no kwarg on arguments");
        }
        sky_python_ast_validate_expr(node->kwargannotation,
                                     sky_python_ast_Load);
    }
    if (sky_object_len(node->defaults) > sky_object_len(node->args)) {
        sky_error_raise_string(
                sky_ValueError,
                "more positional defaults than args on arguments");
    }
    if (sky_object_len(node->kw_defaults) > sky_object_len(node->kwonlyargs)) {
        sky_error_raise_string(
                sky_ValueError,
                "length of kwonlyargs is not the same as kw_defaults on arguments");
    }
    sky_python_ast_validate_expr_sequence(node->defaults,
                                          sky_python_ast_Load,
                                          SKY_FALSE);
    sky_python_ast_validate_expr_sequence(node->kw_defaults,
                                          sky_python_ast_Load,
                                          SKY_FALSE);
}


static void
sky_python_ast_validate_comprehension(sky_object_t generators)
{
    sky_python_ast_comprehension_t  comprehension;

    if (!sky_object_bool(generators)) {
        sky_error_raise_string(sky_ValueError,
                               "comprehension with no generators");
    }
    SKY_SEQUENCE_FOREACH(generators, comprehension) {
        sky_python_ast_validate_expr(comprehension->target,
                                     sky_python_ast_Store);
        sky_python_ast_validate_expr(comprehension->iter,
                                     sky_python_ast_Load);
        sky_python_ast_validate_expr_sequence(comprehension->ifs,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_ast_validate_slice(sky_python_ast_slice_t slice)
{
    if (sky_object_isa(slice, sky_python_ast_Slice_type)) {
        sky_python_ast_Slice_t      node = (sky_python_ast_Slice_t)slice;

        if (!sky_object_isnull(node->lower)) {
            sky_python_ast_validate_expr(node->lower, sky_python_ast_Load);
        }
        if (!sky_object_isnull(node->upper)) {
            sky_python_ast_validate_expr(node->upper, sky_python_ast_Load);
        }
        if (!sky_object_isnull(node->step)) {
            sky_python_ast_validate_expr(node->step, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(slice, sky_python_ast_ExtSlice_type)) {
        sky_python_ast_ExtSlice_t   node = (sky_python_ast_ExtSlice_t)slice;

        sky_python_ast_slice_t  dim;

        sky_python_ast_validate_nonempty_sequence(node->dims,
                                                  "dims",
                                                  "ExtSlice");
        SKY_SEQUENCE_FOREACH(node->dims, dim) {
            sky_python_ast_validate_slice(dim);
        } SKY_SEQUENCE_FOREACH_END;
    }
    else if (sky_object_isa(slice, sky_python_ast_Index_type)) {
        sky_python_ast_Index_t      node = (sky_python_ast_Index_t)slice;

        sky_python_ast_validate_expr(node->value, sky_python_ast_Load);
    }
    else {
        sky_error_raise_string(sky_SystemError, "unknown slice node");
    }
}


static void
sky_python_ast_validate_expr(sky_object_t                   node,
                             sky_python_ast_expr_context_t  ctx)
{
    sky_bool_t                      check_ctx;
    sky_python_ast_expr_context_t   actual_ctx;

    /* First check expression context. */
    check_ctx = SKY_TRUE;
    ctx = sky_python_ast_normalize_expr_context(ctx);
    if (sky_object_isa(node, sky_python_ast_Attribute_type)) {
        actual_ctx = ((sky_python_ast_Attribute_t)node)->ctx;
    }
    else if (sky_object_isa(node, sky_python_ast_Subscript_type)) {
        actual_ctx = ((sky_python_ast_Subscript_t)node)->ctx;
    }
    else if (sky_object_isa(node, sky_python_ast_Starred_type)) {
        actual_ctx = ((sky_python_ast_Starred_t)node)->ctx;
    }
    else if (sky_object_isa(node, sky_python_ast_Name_type)) {
        actual_ctx = ((sky_python_ast_Name_t)node)->ctx;
    }
    else if (sky_object_isa(node, sky_python_ast_List_type)) {
        actual_ctx = ((sky_python_ast_List_t)node)->ctx;
    }
    else if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        actual_ctx = ((sky_python_ast_Tuple_t)node)->ctx;
    }
    else if (ctx != sky_python_ast_Load) {
        sky_error_raise_format(
                sky_ValueError,
                "expression which can't be assigned to in %@ context",
                sky_type_name(sky_object_type(ctx)));
    }
    else {
        check_ctx = SKY_FALSE;
    }
    if (check_ctx) {
        actual_ctx = sky_python_ast_normalize_expr_context(actual_ctx);
        if (actual_ctx != ctx) {
            sky_error_raise_format(
                    sky_ValueError,
                    "expression must have %@ context but has %@ instead",
                    sky_type_name(sky_object_type(ctx)),
                    sky_type_name(sky_object_type(actual_ctx)));
        }
    }

    /* Now validate expression. */
    if (sky_object_isa(node, sky_python_ast_BoolOp_type)) {
        sky_python_ast_BoolOp_t         expr = node;

        if (sky_object_len(expr->values) < 2) {
            sky_error_raise_string(sky_ValueError,
                                   "BoolOp with less than 2 values");
        }
        sky_python_ast_validate_expr_sequence(expr->values,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
    }
    else if (sky_object_isa(node, sky_python_ast_BinOp_type)) {
        sky_python_ast_BinOp_t          expr = node;

        sky_python_ast_validate_expr(expr->left, sky_python_ast_Load);
        sky_python_ast_validate_expr(expr->right, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_UnaryOp_type)) {
        sky_python_ast_UnaryOp_t        expr = node;

        sky_python_ast_validate_expr(expr->operand, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Lambda_type)) {
        sky_python_ast_Lambda_t         expr = node;

        sky_python_ast_validate_arguments(expr->args);
        sky_python_ast_validate_expr(expr->body, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_IfExp_type)) {
        sky_python_ast_IfExp_t          expr = node;

        sky_python_ast_validate_expr(expr->test, sky_python_ast_Load);
        sky_python_ast_validate_expr(expr->body, sky_python_ast_Load);
        sky_python_ast_validate_expr(expr->orelse, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Dict_type)) {
        sky_python_ast_Dict_t           expr = node;

        if (sky_object_len(expr->keys) != sky_object_len(expr->values)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "Dict doesn't have the same number of keys as values");
        }
        sky_python_ast_validate_expr_sequence(expr->keys,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
        sky_python_ast_validate_expr_sequence(expr->values,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
    }
    else if (sky_object_isa(node, sky_python_ast_Set_type)) {
        sky_python_ast_Set_t            expr = node;

        sky_python_ast_validate_expr_sequence(expr->elts,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
    }
    else if (sky_object_isa(node, sky_python_ast_ListComp_type)) {
        sky_python_ast_ListComp_t       expr = node;

        sky_python_ast_validate_comprehension(expr->generators);
        sky_python_ast_validate_expr(expr->elt, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_SetComp_type)) {
        sky_python_ast_SetComp_t        expr = node;

        sky_python_ast_validate_comprehension(expr->generators);
        sky_python_ast_validate_expr(expr->elt, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_DictComp_type)) {
        sky_python_ast_DictComp_t       expr = node;

        sky_python_ast_validate_comprehension(expr->generators);
        sky_python_ast_validate_expr(expr->key, sky_python_ast_Load);
        sky_python_ast_validate_expr(expr->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_GeneratorExp_type)) {
        sky_python_ast_GeneratorExp_t   expr = node;

        sky_python_ast_validate_comprehension(expr->generators);
        sky_python_ast_validate_expr(expr->elt, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Yield_type)) {
        sky_python_ast_Yield_t          expr = node;

        if (!sky_object_isnull(expr->value)) {
            sky_python_ast_validate_expr(expr->value, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_YieldFrom_type)) {
        sky_python_ast_YieldFrom_t      expr = node;

        sky_python_ast_validate_expr(expr->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Compare_type)) {
        sky_python_ast_Compare_t        expr = node;

        if (!sky_object_bool(expr->comparators)) {
            sky_error_raise_string(sky_ValueError,
                                   "Compare with no comparators");
        }
        if (sky_object_len(expr->comparators) != sky_object_len(expr->ops)) {
            sky_error_raise_string(
                    sky_ValueError,
                    "Compare has a different number of comparators and operands");
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Call_type)) {
        sky_python_ast_Call_t           expr = node;

        sky_python_ast_validate_expr(expr->func, sky_python_ast_Load);
        sky_python_ast_validate_expr_sequence(expr->args,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
        sky_python_ast_validate_keywords(expr->keywords);
        if (!sky_object_isnull(expr->starargs)) {
            sky_python_ast_validate_expr(expr->starargs, sky_python_ast_Load);
        }
        if (!sky_object_isnull(expr->kwargs)) {
            sky_python_ast_validate_expr(expr->kwargs, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Num_type)) {
        sky_python_ast_Num_t            expr = node;

        if (sky_object_type(expr->n) != sky_integer_type &&
            sky_object_type(expr->n) != sky_float_type &&
            sky_object_type(expr->n) != sky_complex_type)
        {
            sky_error_raise_string(sky_ValueError,
                                   "non-numeric type in Num");
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Str_type)) {
        sky_python_ast_Str_t            expr = node;

        if (sky_object_type(expr->s) != sky_string_type) {
            sky_error_raise_string(sky_ValueError,
                                   "non-string type in Str");
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Bytes_type)) {
        sky_python_ast_Bytes_t          expr = node;

        if (sky_object_type(expr->s) != sky_bytes_type) {
            sky_error_raise_string(sky_ValueError,
                                   "non-bytes type in Bytes");
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Attribute_type)) {
        sky_python_ast_Attribute_t      expr = node;

        sky_python_ast_validate_expr(expr->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Subscript_type)) {
        sky_python_ast_Subscript_t      expr = node;

        sky_python_ast_validate_slice(expr->slice);
        sky_python_ast_validate_expr(expr->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Starred_type)) {
        sky_python_ast_Starred_t        expr = node;

        sky_python_ast_validate_expr(expr->value, ctx);
    }
    else if (sky_object_isa(node, sky_python_ast_List_type)) {
        sky_python_ast_List_t           expr = node;

        sky_python_ast_validate_expr_sequence(expr->elts, ctx, SKY_FALSE);
    }
    else if (sky_object_isa(node, sky_python_ast_Tuple_type)) {
        sky_python_ast_Tuple_t          expr = node;

        sky_python_ast_validate_expr_sequence(expr->elts, ctx, SKY_FALSE);
    }
    else if (sky_object_isa(node, sky_python_ast_Name_type) ||
             sky_object_isa(node, sky_python_ast_Ellipsis_type))
    {
        /* pass */
    }
    else {
        sky_error_raise_string(sky_SystemError, "unexpected expression");
    }
}


static void
sky_python_ast_validate_expr_sequence(sky_object_t                  sequence,
                                      sky_python_ast_expr_context_t ctx,
                                      sky_bool_t                    null_ok)
{
    sky_object_t    expr;

    SKY_SEQUENCE_FOREACH(sequence, expr) {
        if (!sky_object_isnull(expr)) {
            sky_python_ast_validate_expr(expr, ctx);
        }
        else if (!null_ok) {
            sky_error_raise_string(sky_ValueError,
                                   "None disallowed in expression list");
        }
    } SKY_SEQUENCE_FOREACH_END;
}


static void
sky_python_ast_validate_targets(sky_object_t                    targets,
                                sky_python_ast_expr_context_t   ctx)
{
    ctx = sky_python_ast_normalize_expr_context(ctx);
    sky_python_ast_validate_nonempty_sequence(
            targets,
            "targets",
            (ctx == sky_python_ast_Del ? "Delete" : "Assign"));
    sky_python_ast_validate_expr_sequence(targets, ctx, SKY_FALSE);
}


static void
sky_python_ast_validate_body(sky_object_t body, const char *owner)
{
    sky_python_ast_validate_nonempty_sequence(body, "body", owner);
    sky_python_ast_validate_stmt_sequence(body);
}


static void
sky_python_ast_validate_stmt(sky_object_t node)
{
    if (sky_object_isa(node, sky_python_ast_FunctionDef_type)) {
        sky_python_ast_FunctionDef_t    stmt = node;

        sky_python_ast_validate_body(stmt->body, "FunctionDef");
        sky_python_ast_validate_arguments(stmt->args);
        sky_python_ast_validate_expr_sequence(stmt->decorator_list,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
        if (!sky_object_isnull(stmt->returns)) {
            sky_python_ast_validate_expr(stmt->returns, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_ClassDef_type)) {
        sky_python_ast_ClassDef_t       stmt = node;

        sky_python_ast_validate_body(stmt->body, "ClassDef");
        sky_python_ast_validate_expr_sequence(stmt->bases,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
        sky_python_ast_validate_keywords(stmt->keywords);
        sky_python_ast_validate_expr_sequence(stmt->decorator_list,
                                              sky_python_ast_Load,
                                              SKY_FALSE);
        if (!sky_object_isnull(stmt->starargs)) {
            sky_python_ast_validate_expr(stmt->starargs, sky_python_ast_Load);
        }
        if (!sky_object_isnull(stmt->kwargs)) {
            sky_python_ast_validate_expr(stmt->kwargs, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Return_type)) {
        sky_python_ast_Return_t         stmt = node;

        if (!sky_object_isnull(stmt->value)) {
            sky_python_ast_validate_expr(stmt->value, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Delete_type)) {
        sky_python_ast_Delete_t         stmt = node;

        sky_python_ast_validate_targets(stmt->targets, sky_python_ast_Del);
    }
    else if (sky_object_isa(node, sky_python_ast_Assign_type)) {
        sky_python_ast_Assign_t         stmt = node;

        sky_python_ast_validate_targets(stmt->targets, sky_python_ast_Store);
        sky_python_ast_validate_expr(stmt->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_AugAssign_type)) {
        sky_python_ast_AugAssign_t      stmt = node;

        sky_python_ast_validate_expr(stmt->target, sky_python_ast_Store);
        sky_python_ast_validate_expr(stmt->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_For_type)) {
        sky_python_ast_For_t            stmt = node;

        sky_python_ast_validate_expr(stmt->target, sky_python_ast_Store);
        sky_python_ast_validate_expr(stmt->iter, sky_python_ast_Load);
        sky_python_ast_validate_body(stmt->body, "For");
        sky_python_ast_validate_stmt_sequence(stmt->orelse);
    }
    else if (sky_object_isa(node, sky_python_ast_While_type)) {
        sky_python_ast_While_t          stmt = node;

        sky_python_ast_validate_expr(stmt->test, sky_python_ast_Load);
        sky_python_ast_validate_body(stmt->body, "While");
        sky_python_ast_validate_stmt_sequence(stmt->orelse);
    }
    else if (sky_object_isa(node, sky_python_ast_If_type)) {
        sky_python_ast_If_t             stmt = node;

        sky_python_ast_validate_expr(stmt->test, sky_python_ast_Load);
        sky_python_ast_validate_body(stmt->body, "If");
        sky_python_ast_validate_stmt_sequence(stmt->orelse);
    }
    else if (sky_object_isa(node, sky_python_ast_With_type)) {
        sky_python_ast_With_t           stmt = node;

        sky_python_ast_withitem_t   item;

        sky_python_ast_validate_nonempty_sequence(stmt->items, "items", "With");
        SKY_SEQUENCE_FOREACH(stmt->items, item) {
            sky_python_ast_validate_expr(item->context_expr,
                                         sky_python_ast_Load);
            if (!sky_object_isnull(item->optional_vars)) {
                sky_python_ast_validate_expr(item->optional_vars,
                                             sky_python_ast_Store);
            }
        } SKY_SEQUENCE_FOREACH_END;
        sky_python_ast_validate_body(stmt->body, "With");
    }
    else if (sky_object_isa(node, sky_python_ast_Raise_type)) {
        sky_python_ast_Raise_t          stmt = node;

        if (!sky_object_isnull(stmt->exc)) {
            sky_python_ast_validate_expr(stmt->exc, sky_python_ast_Load);
            if (!sky_object_isnull(stmt->cause)) {
                sky_python_ast_validate_expr(stmt->cause, sky_python_ast_Load);
            }
        }
        if (!sky_object_isnull(stmt->cause)) {
            sky_error_raise_string(sky_ValueError,
                                   "Raise with cause but no exception");
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Try_type)) {
        sky_python_ast_Try_t            stmt = node;

        sky_python_ast_ExceptHandler_t  handler;

        sky_python_ast_validate_body(stmt->body, "Try");
        if (!sky_object_bool(stmt->handlers) &&
            !sky_object_bool(stmt->finalbody))
        {
            sky_error_raise_string(
                    sky_ValueError,
                    "Try has neither except handlers nor finalbody");
        }
        if (!sky_object_bool(stmt->handlers) &&
            sky_object_bool(stmt->orelse))
        {
            sky_error_raise_string(
                    sky_ValueError,
                    "Try has orelse but no except handlers");
        }
        SKY_SEQUENCE_FOREACH(stmt->handlers, handler) {
            if (!sky_object_isnull(handler->type)) {
                sky_python_ast_validate_expr(handler->type,
                                             sky_python_ast_Load);
            }
            sky_python_ast_validate_body(handler->body, "ExceptHandler");
        } SKY_SEQUENCE_FOREACH_END;
        if (sky_object_bool(stmt->finalbody)) {
            sky_python_ast_validate_stmt_sequence(stmt->finalbody);
        }
        if (sky_object_bool(stmt->orelse)) {
            sky_python_ast_validate_stmt_sequence(stmt->orelse);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Assert_type)) {
        sky_python_ast_Assert_t         stmt = node;

        sky_python_ast_validate_expr(stmt->test, sky_python_ast_Load);
        if (!sky_object_isnull(stmt->msg)) {
            sky_python_ast_validate_expr(stmt->msg, sky_python_ast_Load);
        }
    }
    else if (sky_object_isa(node, sky_python_ast_Import_type)) {
        sky_python_ast_Import_t         stmt = node;

        sky_python_ast_validate_nonempty_sequence(stmt->names,
                                                  "names",
                                                  "Import");
    }
    else if (sky_object_isa(node, sky_python_ast_ImportFrom_type)) {
        sky_python_ast_ImportFrom_t     stmt = node;

        if (stmt->level < -1) {
            sky_error_raise_string(sky_ValueError,
                                   "ImportFrom level less than -1");
        }
        sky_python_ast_validate_nonempty_sequence(stmt->names,
                                                  "names",
                                                  "ImportFrom");
    }
    else if (sky_object_isa(node, sky_python_ast_Global_type)) {
        sky_python_ast_Global_t         stmt = node;

        sky_python_ast_validate_nonempty_sequence(stmt->names,
                                                  "names",
                                                  "Global");
    }
    else if (sky_object_isa(node, sky_python_ast_Nonlocal_type)) {
        sky_python_ast_Nonlocal_t       stmt = node;

        sky_python_ast_validate_nonempty_sequence(stmt->names,
                                                  "names",
                                                  "Nonlocal");
    }
    else if (sky_object_isa(node, sky_python_ast_Expr_type)) {
        sky_python_ast_Expr_t           stmt = node;

        sky_python_ast_validate_expr(stmt->value, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Pass_type) ||
             sky_object_isa(node, sky_python_ast_Break_type) ||
             sky_object_isa(node, sky_python_ast_Continue_type))
    {
        /* pass */
    }
    else {
        sky_error_raise_string(sky_SystemError, "unexpected statement");
    }
}


static void
sky_python_ast_validate_stmt_sequence(sky_object_t sequence)
{
    sky_object_t    stmt;

    SKY_SEQUENCE_FOREACH(sequence, stmt) {
        if (!sky_object_isnull(stmt)) {
            sky_python_ast_validate_stmt(stmt);
        }
        else {
            sky_error_raise_string(sky_ValueError,
                                   "None disallowed in statement list");
        }
    } SKY_SEQUENCE_FOREACH_END;
}


void
sky_python_ast_validate(sky_object_t node)
{
    sky_object_t    body;

    if (sky_object_isa(node, sky_python_ast_Module_type)) {
        body = ((sky_python_ast_Module_t)node)->body;
        sky_python_ast_validate_stmt_sequence(body);
    }
    else if (sky_object_isa(node, sky_python_ast_Interactive_type)) {
        body = ((sky_python_ast_Interactive_t)node)->body;
        sky_python_ast_validate_stmt_sequence(body);
    }
    else if (sky_object_isa(node, sky_python_ast_Expression_type)) {
        body = ((sky_python_ast_Expression_t)node)->body;
        sky_python_ast_validate_expr(body, sky_python_ast_Load);
    }
    else if (sky_object_isa(node, sky_python_ast_Suite_type)) {
        sky_error_raise_string(sky_ValueError,
                               "Suite is not valid in the Skython compiler");
    }
    else {
        sky_error_raise_string(sky_SystemError,
                               "impossible module node");
    }
}
