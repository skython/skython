#include "sky_private.h"

/* This deque implementation is based on that found in CPython 3.3.6:
 *
 * collections module implementation of a deque() datatype
 * Written and maintained by Raymond D. Hettinger <python@rcn.com>
 * Copyright (c) 2004 Python Software Foundation.
 * All rights reserved.
 */


/* The block length may be set to any number over 1.  Larger numbers
 * reduce the number of calls to the memory allocator but take more
 * memory.  Ideally, BLOCKLEN should be set with an eye to the
 * length of a cache line.
 */
#define SKY_DEQUE_BLOCKLEN              62
#define SKY_DEQUE_CENTER                ((SKY_DEQUE_BLOCKLEN - 1) / 2)

/* A `dequeobject` is composed of a doubly-linked list of `block` nodes.
 * This list is not circular (the leftmost block has leftlink==NULL,
 * and the rightmost block has rightlink==NULL).  A deque d's first
 * element is at d.left_block[left_index] and its last element is at
 * d.right_block[right_index]; note that, unlike as for Python slice
 * indices, these indices are inclusive on both ends.  By being inclusive
 * on both ends, algorithms for left and right operations become
 * symmetrical which simplifies the design.
 *
 * The list of blocks is never empty, so d.left_block and d.right_block
 * are never equal to NULL.
 *
 * The indices, d.left_index and d.right_index are always in the range
 *     0 <= index < BLOCKLEN.
 * Their exact relationship is:
 *     (d.left_index + d.len - 1) % BLOCKLEN == d.right_index.
 *
 * Empty deques have d.len == 0; d.left_block==d.right_block;
 * d.left_index == CENTER+1; and d.right_index == CENTER.
 * Checking for d.len == 0 is the intended way to see whether d is empty.
 *
 * Whenever d.left_block == d.right_block,
 *     d.left_index + d.len - 1 == d.right_index.
 *
 * However, when d.left_block != d.right_block, d.left_index and d.right_index
 * become indices into distinct blocks and either may be larger than the
 * other.
 */

typedef struct sky_deque_block_s sky_deque_block_t;
struct sky_deque_block_s {
    sky_deque_block_t * volatile        left;
    sky_deque_block_t * volatile        right;
    sky_object_t                        objects[SKY_DEQUE_BLOCKLEN];
};

typedef struct sky_deque_table_s {
    sky_deque_block_t *                 left_block;
    sky_deque_block_t *                 right_block;
    ssize_t                             left_index;
    ssize_t                             right_index;
    ssize_t                             len;
    ssize_t                             maxlen;
    volatile uintptr_t                  revision;
} sky_deque_table_t;

typedef struct sky_deque_data_s {
    sky_deque_table_t * volatile        table;
    sky_mutex_t                         mutex;
} sky_deque_data_t;

SKY_EXTERN_INLINE sky_deque_data_t *
sky_deque_data(sky_object_t self)
{
    return sky_object_data(self, sky_deque_type);
}

SKY_STATIC_INLINE sky_bool_t
sky_deque_isshared(sky_deque_t self)
{
    return (sky_object_gc_isthreaded() && sky_object_isglobal(self));
}

SKY_STATIC_INLINE ssize_t
sky_deque_index_normalize(ssize_t index, ssize_t len)
{
    if (index < 0) {
        index += len;
        if (index < 0) {
            sky_error_raise_string(sky_IndexError, "deque index out of range");
        }
    }
    if (index >= len) {
        sky_error_raise_string(sky_IndexError, "deque index out of range");
    }
    return index;
}

SKY_STATIC_INLINE void
sky_deque_lock(sky_deque_t self, sky_bool_t fast)
{
    sky_mutex_t mutex;

    if (fast && !sky_deque_isshared(self)) {
        return;
    }

    mutex = sky_deque_data(self)->mutex;
    sky_mutex_lock(mutex, SKY_TIME_INFINITE);
    sky_asset_save(mutex,
                   (sky_free_t)sky_mutex_unlock,
                   SKY_ASSET_CLEANUP_ALWAYS);
}

static sky_deque_block_t *
sky_deque_block_create(ssize_t len)
{
    /* To prevent len from overflowing SSIZE_MAX on 64-bit machines, we
     * refuse to allocate new blocks if the current len is dangerously
     * close.  There is some extra margin to prevent spurious arithmetic
     * overflows at various places.  The following check ensures that
     * the blocks allocated to the deque, in the worst case, can only
     * have SSIZE_MAX-2 entries in total.
     */
    if (len >= SSIZE_MAX - 2 * SKY_DEQUE_BLOCKLEN) {
        sky_error_raise_string(sky_OverflowError,
                               "cannot add more blocks to the deque");
    }

    return sky_calloc(1, sizeof(sky_deque_block_t));
}

static sky_deque_table_t *
sky_deque_table(sky_deque_t self, sky_bool_t fast)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    sky_hazard_pointer_t    *hazard;

    if (fast && !sky_deque_isshared(self)) {
        return self_data->table;
    }

    hazard = sky_hazard_pointer_acquire(SKY_AS_VOIDP(&(self_data->table)));
    sky_error_validate_debug(NULL != hazard->pointer);

    sky_asset_save(hazard,
                   (sky_free_t)sky_hazard_pointer_release,
                   SKY_ASSET_CLEANUP_ALWAYS);
    return hazard->pointer;
}

static sky_deque_table_t *
sky_deque_table_create(ssize_t maxlen)
{
    sky_deque_table_t   *table;

    table = sky_calloc(1, sizeof(sky_deque_table_t));
    table->left_block = sky_deque_block_create(0);
    table->left_index = SKY_DEQUE_CENTER + 1;
    table->right_block = table->left_block;
    table->right_index = SKY_DEQUE_CENTER;
    table->maxlen = maxlen;

    return table;
}

static void
sky_deque_table_free(sky_deque_table_t *table)
{
    sky_deque_block_t   *block, *next_block;

    next_block = table->left_block;
    while ((block = next_block) != NULL) {
        next_block = block->right;
        sky_free(block);
    }
}

static sky_object_t
sky_deque_table_pop(sky_deque_table_t *table)
{
    sky_object_t    item;

    if (!table->len) {
        sky_error_raise_string(sky_IndexError, "pop from an empty deque");
    }
    item = table->right_block->objects[table->right_index];
    --table->right_index;
    --table->len;
    ++table->revision;

    if (-1 == table->right_index) {
        if (!table->len) {
            sky_error_validate_debug(table->left_block == table->right_block);
            sky_error_validate_debug(table->left_index == table->right_index + 1);
            /* re-center instead of freeing a block */
            table->left_index = SKY_DEQUE_CENTER + 1;
            table->right_index = SKY_DEQUE_CENTER;
        }
        else {
            sky_deque_block_t   *right_block;

            right_block = table->right_block;
            sky_error_validate_debug(table->left_block != table->right_block);
            table->right_block = right_block->left;
            table->right_index = SKY_DEQUE_BLOCKLEN - 1;

            /* The order in which things are done here is important. */
            /* TODO cpu memory barriers needed */
            right_block->left->right = NULL;
            right_block->left = (sky_deque_block_t *)
                                ((uintptr_t)right_block->left | 1);
            right_block->right = (sky_deque_block_t *)
                                 ((uintptr_t)right_block->right | 1);
            sky_hazard_pointer_retire(right_block, sky_free);
        }
    }

    return item;
}

static sky_object_t
sky_deque_table_popleft(sky_deque_table_t *table)
{
    sky_object_t    item;

    if (!table->len) {
        sky_error_raise_string(sky_IndexError, "pop from an empty deque");
    }
    item = table->left_block->objects[table->left_index];
    ++table->left_index;
    --table->len;
    ++table->revision;

    if (SKY_DEQUE_BLOCKLEN == table->left_index) {
        if (!table->len) {
            sky_error_validate_debug(table->left_block == table->right_block);
            sky_error_validate_debug(table->left_index == table->right_index + 1);
            /* re-center instead of freeing a block */
            table->left_index = SKY_DEQUE_CENTER + 1;
            table->right_index = SKY_DEQUE_CENTER;
        }
        else {
            sky_deque_block_t   *left_block;

            left_block = table->left_block;
            sky_error_validate_debug(table->left_block != table->right_block);
            table->left_block = left_block->right;
            table->left_index = 0;

            /* The order in which things are done here is important. */
            /* TODO cpu memory barriers needed */
            left_block->right->left = NULL;
            left_block->left = (sky_deque_block_t *)
                               ((uintptr_t)left_block->left | 1);
            left_block->right = (sky_deque_block_t *)
                                ((uintptr_t)left_block->right | 1);
            sky_hazard_pointer_retire(left_block, sky_free);
        }
    }

    return item;
}

static void
sky_deque_table_rotate(sky_deque_table_t *table, ssize_t n, sky_deque_t deque)
{
    ssize_t             halflen, len, m;
    sky_deque_block_t   *block;

    len = table->len;
    if (len <= 1) {
        return;
    }

    halflen = len >> 1;
    if (n > halflen || n < -halflen) {
        n %= len;
        if (n > halflen) {
            n -= len;
        }
        else if (n < -halflen) {
            n += len;
        }
    }
    sky_error_validate_debug(len > 1);
    sky_error_validate_debug(-halflen <= n && n <= halflen);

    ++table->revision;
    while (n > 0) {
        if (!table->left_index) {
            block = sky_deque_block_create(len);
            sky_error_validate_debug(!table->left_block->left);
            block->right = table->left_block;
            table->left_block->left = block;
            table->left_block = block;
            table->left_index = SKY_DEQUE_BLOCKLEN;
        }
        sky_error_validate_debug(table->left_index > 0);

        m = n;
        if (m > table->right_index + 1) {
            m = table->right_index + 1;
        }
        if (m > table->left_index) {
            m = table->left_index;
        }
        sky_error_validate_debug(m > 0 && m <= len);
        memcpy(&(table->left_block->objects[table->left_index - m]),
               &(table->right_block->objects[table->right_index + 1 - m]),
               m * sizeof(sky_object_t));
        sky_object_gc_setarray(
                m,
                &(table->left_block->objects[table->left_index -m]),
                deque);
        table->right_index -= m;
        table->left_index -= m;
        n -= m;

        if (-1 == table->right_index) {
            sky_deque_block_t   *right_block;

            right_block = table->right_block;
            sky_error_validate_debug(table->left_block != table->right_block);
            table->right_block = right_block->left;
            table->right_index = SKY_DEQUE_BLOCKLEN - 1;

            /* The order in which things are done here is important. */
            /* TODO cpu memory barriers needed */
            right_block->left->right = NULL;
            right_block->left = (sky_deque_block_t *)
                                ((uintptr_t)right_block->left | 1);
            right_block->right = (sky_deque_block_t *)
                                 ((uintptr_t)right_block->right | 1);
            sky_hazard_pointer_retire(right_block, sky_free);
        }
    }
    while (n < 0) {
        if (SKY_DEQUE_BLOCKLEN - 1 == table->right_index) {
            block = sky_deque_block_create(len);
            sky_error_validate_debug(!table->right_block->right);
            block->left = table->right_block;
            table->right_block->right = block;
            table->right_block = block;
            table->right_index = -1;
        }
        sky_error_validate_debug(table->right_index < SKY_DEQUE_BLOCKLEN - 1);

        m = -n;
        if (m > SKY_DEQUE_BLOCKLEN - table->left_index) {
            m = SKY_DEQUE_BLOCKLEN - table->left_index;
        }
        if (m > SKY_DEQUE_BLOCKLEN - 1 - table->right_index) {
            m = SKY_DEQUE_BLOCKLEN - 1 - table->right_index;
        }
        sky_error_validate_debug(m > 0 && m <= len);
        memcpy(&(table->right_block->objects[table->right_index + 1]),
               &(table->left_block->objects[table->left_index]),
               m * sizeof(sky_object_t));
        sky_object_gc_setarray(
                m,
                &(table->right_block->objects[table->right_index + 1]),
                deque);
        table->left_index += m;
        table->right_index += m;
        n += m;

        if (SKY_DEQUE_BLOCKLEN == table->left_index) {
            sky_deque_block_t   *left_block;

            left_block = table->left_block;
            sky_error_validate_debug(table->left_block != table->right_block);
            table->left_block = left_block->right;
            table->left_index = 0;

            /* The order in which things are done here is important. */
            /* TODO cpu memory barriers needed */
            left_block->right->left = NULL;
            left_block->left = (sky_deque_block_t *)
                               ((uintptr_t)left_block->left | 1);
            left_block->right = (sky_deque_block_t *)
                                ((uintptr_t)left_block->right | 1);
            sky_hazard_pointer_retire(left_block, sky_free);
        }
    }
}


static void
sky_deque_instance_initialize(sky_object_t self, void *data)
{
    sky_deque_data_t    *self_data = data;

    self_data->table = sky_deque_table_create(-1);
    sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->mutex)),
                      sky_mutex_create(SKY_FALSE, SKY_TRUE),
                      self);
}

static void
sky_deque_instance_finalize(SKY_UNUSED sky_object_t self, void *data)
{
    sky_deque_table_free(((sky_deque_data_t *)data)->table);
}

static void
sky_deque_instance_iterate(sky_object_t                     self,
                           void *                           data,
                           sky_sequence_iterate_state_t *   state,
                           sky_object_t *                   objects,
                           ssize_t                          nobjects)
{
    sky_deque_data_t    *self_data = data;

    ssize_t                 index;
    uintptr_t               revision;
    sky_deque_block_t       *block;
    sky_deque_table_t       *table;
    sky_hazard_pointer_t    *block_hazard, *table_hazard;

    /* state->
     *      state       next starting index to return
     *      extra[0]    hazard pointer for self_data->table at start
     *      extra[1]    table->revision at start
     *      extra[2]    hazard pointer for current block
     *      extra[3]    next index into current block
     */

    SKY_ASSET_BLOCK_BEGIN {
        if (!state->state) {
            table_hazard = sky_hazard_pointer_acquire(
                                    SKY_AS_VOIDP(&(self_data->table)));
            sky_asset_save(table_hazard,
                           (sky_free_t)sky_hazard_pointer_release,
                           SKY_ASSET_CLEANUP_ON_ERROR);
            table = table_hazard->pointer;
            revision = table->revision;

            block_hazard = sky_hazard_pointer_acquire(
                                    SKY_AS_VOIDP(&(table->left_block)));
            sky_asset_save(block_hazard,
                           (sky_free_t)sky_hazard_pointer_release,
                           SKY_ASSET_CLEANUP_ON_ERROR);
            block = block_hazard->pointer;

            state->extra[0] = (uintptr_t)table_hazard;
            state->extra[1] = revision;
            state->extra[2] = (uintptr_t)block_hazard;
            state->extra[3] = index = table->left_index;
        }
        else {
            table_hazard = (sky_hazard_pointer_t *)state->extra[0];
            table = table_hazard->pointer;
            revision = state->extra[1];
            block_hazard = (sky_hazard_pointer_t *)state->extra[2];
            block = block_hazard->pointer;
            index = state->extra[3];
        }

        if (table != self_data->table || revision != table->revision) {
            sky_error_raise_string(sky_RuntimeError,
                                   "deque modified during iteration");
        }

        if (state->state >= table->len) {
            state->objects = NULL;
            state->nobjects = 0;
        }
        else {
            if (!sky_deque_isshared(self)) {
                if (block == table->left_block) {
                    state->objects = &(block->objects[table->left_index]);
                    if (block == table->right_block) {
                        state->nobjects = table->len;
                    }
                    else {
                        state->nobjects = SKY_DEQUE_BLOCKLEN -
                                          table->left_index;
                    };
                }
                else if (block == table->right_block) {
                    state->objects = block->objects;
                    state->nobjects = table->right_index + 1;
                }
                else {
                    state->objects = block->objects;
                    state->nobjects = SKY_DEQUE_BLOCKLEN;
                }
                sky_hazard_pointer_update(block_hazard,
                                          SKY_AS_VOIDP(&(block->right)));
                block = block_hazard->pointer;
            }
            else {
                ssize_t i;

                state->objects = objects;
                state->nobjects = table->len - state->state;
                if (state->nobjects > nobjects) {
                    state->nobjects = nobjects;
                }

                for (i = 0; i < state->nobjects; ++i) {
                    objects[i] = block->objects[index];
                    if (++index >= SKY_DEQUE_BLOCKLEN) {
                        sky_hazard_pointer_update(
                                block_hazard,
                                SKY_AS_VOIDP(&(block->right)));
                        block = block_hazard->pointer;
                        index = 0;
                    }

                    if (table != self_data->table ||
                        revision != table->revision)
                    {
                        sky_error_raise_string(
                                sky_RuntimeError,
                                "deque modified during iteration");
                    }
                }
                state->extra[3] = index;
            }
            state->state += state->nobjects;
        }
    } SKY_ASSET_BLOCK_END;
}

static void
sky_deque_instance_iterate_cleanup(
        SKY_UNUSED  sky_object_t                    self,
        SKY_UNUSED  void *                          data,
                    sky_sequence_iterate_state_t *  state)
{
    sky_hazard_pointer_t    *hazard;

    if ((hazard = (sky_hazard_pointer_t *)state->extra[2]) != NULL) {
        sky_hazard_pointer_release(hazard);
    }
    if ((hazard = (sky_hazard_pointer_t *)state->extra[0]) != NULL) {
        sky_hazard_pointer_release(hazard);
    }
}

static void
sky_deque_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_deque_data_t    *self_data = data;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t                 index, last_index;
        uintptr_t               revision;
        sky_deque_block_t       *block;
        sky_deque_table_t       *table;
        sky_hazard_pointer_t    *block_hazard, *table_hazard;

        table_hazard = sky_hazard_pointer_acquire(
                                SKY_AS_VOIDP(&(self_data->table)));
        sky_asset_save(table_hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);
        table = table_hazard->pointer;

        block_hazard = sky_hazard_pointer_acquire(NULL);
        sky_asset_save(block_hazard,
                       (sky_free_t)sky_hazard_pointer_release,
                       SKY_ASSET_CLEANUP_ALWAYS);

restart:
        revision = table->revision;
        sky_hazard_pointer_update(block_hazard,
                                  SKY_AS_VOIDP(&(table->left_block)));
        block = block_hazard->pointer;
        index = table->left_index;
        if (block == table->right_block) {
            last_index = table->right_index;
        }
        else {
            last_index = SKY_DEQUE_BLOCKLEN;
        }

        while (block) {
            if (self_data->table != table) {
                break;
            }
            if (((uintptr_t)block & 1) || table->revision != revision) {
                goto restart;
            }
            if (index < last_index) {
                sky_object_visit(block->objects[index], visit_data);
                ++index;
            }
            else {
                index = 0;
                sky_hazard_pointer_update(block_hazard,
                                          SKY_AS_VOIDP(&(block->right)));
                block = block_hazard->pointer;
                if (block == table->right_block) {
                    last_index = table->right_index;
                }
                else {
                    last_index = SKY_DEQUE_BLOCKLEN;
                }
            }
        }

        sky_object_visit(self_data->mutex, visit_data);
    } SKY_ASSET_BLOCK_END;
}


typedef struct sky_deque_iterator_data_s {
    sky_deque_t                         deque;
    sky_deque_table_t *                 table;
    uintptr_t                           revision;
    sky_deque_block_t *                 block;
    ssize_t                             block_index;
    ssize_t                             counter;
} sky_deque_iterator_data_t;

SKY_EXTERN_INLINE sky_deque_iterator_data_t *
sky_deque_iterator_data(sky_object_t self)
{
    return sky_object_data(self, sky_deque_iterator_type);
}

static void
sky_deque_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_deque_iterator_data_t   *self_data = data;

    sky_object_visit(self_data->deque, visit_data);
}


SKY_EXTERN_INLINE sky_deque_iterator_data_t *
sky_deque_reverse_iterator_data(sky_object_t self)
{
    return sky_object_data(self, sky_deque_reverse_iterator_type);
}

static void
sky_deque_reverse_iterator_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_deque_iterator_data_t   *self_data = data;

    sky_object_visit(self_data->deque, visit_data);
}


void
sky_deque_append(sky_deque_t self, sky_object_t item)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = self_data->table;

        ++table->revision;
        if (SKY_DEQUE_BLOCKLEN - 1 == table->right_index) {
            block = sky_deque_block_create(table->len);
            block->left = table->right_block;
            table->right_block->right = block;
            table->right_block = block;
            table->right_index = 0;
        }
        else {
            block = table->right_block;
            ++table->right_index;
        }
        ++table->len;
        sky_object_gc_set(&(block->objects[table->right_index]), item, self);
        if (table->maxlen >= 0 && table->len > table->maxlen) {
            sky_deque_table_popleft(table);
        }
    } SKY_ASSET_BLOCK_END;
}

void
sky_deque_appendleft(sky_deque_t self, sky_object_t item)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = self_data->table;

        ++table->revision;
        if (0 == table->left_index) {
            block = sky_deque_block_create(table->len);
            block->right = table->left_block;
            table->left_block->left = block;
            table->left_block = block;
            table->left_index = SKY_DEQUE_BLOCKLEN - 1;
        }
        else {
            block = table->left_block;
            --table->left_index;
        }
        ++table->len;
        sky_object_gc_set(&(block->objects[table->left_index]), item, self);
        if (table->maxlen >= 0 && table->len > table->maxlen) {
            sky_deque_table_pop(table);
        }
    } SKY_ASSET_BLOCK_END;
}

void
sky_deque_clear(sky_deque_t self)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *old_table;

        sky_deque_lock(self, SKY_TRUE);

        old_table = self_data->table;
        self_data->table = sky_deque_table_create(old_table->maxlen);
        sky_hazard_pointer_retire(old_table,
                                  (sky_free_t)sky_deque_table_free);
    } SKY_ASSET_BLOCK_END;
}

static sky_object_t
sky_deque_compare(sky_deque_t       self,
                  sky_object_t      other,
                  sky_compare_op_t  compare_op)
{
    sky_bool_t      result;
    sky_object_t    other_iter, self_iter;

    /* Shortcuts */
    switch (compare_op) {
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_EQUAL:
            if (self == other) {
                return sky_True;
            }
            if (sky_object_isa(other, sky_deque_type) &&
                !sky_deque_isshared(self) && !sky_deque_isshared(other))
            {
                if (sky_deque_data(self)->table->len !=
                    sky_deque_data(other)->table->len)
                {
                    return sky_False;
                }
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            if (sky_object_isa(other, sky_deque_type) &&
                !sky_deque_isshared(self) && !sky_deque_isshared(other))
            {
                if (sky_deque_data(self)->table->len !=
                    sky_deque_data(other)->table->len)
                {
                    return sky_True;
                }
            }
            break;
        case SKY_COMPARE_OP_LESS:
        case SKY_COMPARE_OP_GREATER:
        case SKY_COMPARE_OP_IS:
        case SKY_COMPARE_OP_IS_NOT:
        case SKY_COMPARE_OP_IN:
        case SKY_COMPARE_OP_NOT_IN:
            break;
    }

    if (!sky_object_isa(other, sky_deque_type)) {
        return sky_NotImplemented;
    }

    /* Search for the first index where items are different */
    self_iter = sky_object_iter(self);
    other_iter = sky_object_iter(other);
    for (;;) {
        sky_object_t    self_item, other_item;

        self_item = sky_object_next(self_iter, NULL);
        other_item = sky_object_next(other_iter, NULL);
        if (!self_item || !other_item) {
            switch (compare_op) {
                case SKY_COMPARE_OP_LESS:
                    result = (other_item != NULL ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_LESS_EQUAL:
                    result = (self_item == NULL ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_EQUAL:
                    result = (self_item == other_item ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_NOT_EQUAL:
                    result = (self_item != other_item ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_GREATER:
                    result = (self_item != NULL ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_GREATER_EQUAL:
                    result = (other_item == NULL ? SKY_TRUE : SKY_FALSE);
                    break;
                case SKY_COMPARE_OP_IS:
                case SKY_COMPARE_OP_IS_NOT:
                case SKY_COMPARE_OP_IN:
                case SKY_COMPARE_OP_NOT_IN:
                    sky_error_fatal("internal error");
            }
            break;
        }
        if (!sky_object_compare(self_item, other_item, SKY_COMPARE_OP_EQUAL)) {
            result = sky_object_compare(self_item, other_item, compare_op);
            break;
        }
    }

    return (result ? sky_True : sky_False);
}

sky_deque_t
sky_deque_copy(sky_deque_t self)
{
    sky_tuple_t args;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *self_table = sky_deque_table(self, SKY_TRUE);

        if (self_table->maxlen > 0) {
            args = sky_object_build("(Oi)", self, self_table->maxlen);
        }
        else {
            args = sky_tuple_pack(1, self);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_object_call(sky_object_type(self), args, NULL);
}

ssize_t
sky_deque_count(sky_deque_t self, sky_object_t value)
{
    ssize_t         count;
    sky_object_t    item;

    count = 0;
    SKY_SEQUENCE_FOREACH(self, item) {
        if (sky_object_compare(item, value, SKY_COMPARE_OP_EQUAL)) {
            ++count;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return count;
}

/* delitem() implemented in terms of rotate for simplicity and reasonable
 * performance near the end points.  If for some reason this method becomes
 * popular, it is not hard to re-implement this using direct data movement
 * (similar to code in list slice assignment) and achieve a two or threefold
 * performance boost.
 */
static void
sky_deque_delitem(sky_deque_t self, sky_object_t item)
{
    ssize_t i;

    if (!SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "sequence index must be integer, not %#@",
                               sky_type_name(sky_object_type(item)));
    }

    SKY_ERROR_TRY {
        i = sky_integer_value(sky_number_index(item),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_clear();
        sky_error_raise_string(sky_IndexError,
                               "cannot fit 'int' into an index-sized integer");
    } SKY_ERROR_TRY_END;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;

        i = sky_deque_index_normalize(i, table->len);
        if (i < 0 || i >= table->len) {
            sky_error_raise_string(sky_IndexError, "deque index out of range");
        }

        sky_deque_table_rotate(table, -i, self);
        sky_deque_table_popleft(table);
        sky_deque_table_rotate(table, i, self);
    } SKY_ASSET_BLOCK_END;
}

static sky_object_t
sky_deque_eq(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_EQUAL);
}

static void
sky_deque_extend(sky_deque_t self, sky_object_t iterable)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t        item, iter;
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        if (self == iterable) {
            iterable = sky_list_create(iterable);
        }
        iter = sky_object_iter(iterable);

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;

        if (!table->maxlen) {
            /* Consume the iterator and do nothing else */
            while (sky_object_next(iter, NULL));
        }
        else {
            while ((item = sky_object_next(iter, NULL)) != NULL) {
                ++table->revision;
                if (SKY_DEQUE_BLOCKLEN - 1 == table->right_index) {
                    block = sky_deque_block_create(table->len);
                    block->left = table->right_block;
                    table->right_block->right = block;
                    table->right_block = block;
                    table->right_index = 0;
                }
                else {
                    block = table->right_block;
                    ++table->right_index;
                }
                ++table->len;
                sky_object_gc_set(&(block->objects[table->right_index]),
                                  item,
                                  self);
                if (table->maxlen >= 0 && table->len > table->maxlen) {
                    sky_deque_table_popleft(table);
                }
            }
        }
    } SKY_ASSET_BLOCK_END;
}

static void
sky_deque_extendleft(sky_deque_t self, sky_object_t iterable)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_object_t        item, iter;
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        if (self == iterable) {
            iterable = sky_list_create(iterable);
        }
        iter = sky_object_iter(iterable);

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;

        if (!table->maxlen) {
            /* Consume the iterator and do nothing else */
            while (sky_object_next(iter, NULL));
        }
        else {
            while ((item = sky_object_next(iter, NULL)) != NULL) {
                ++table->revision;
                if (0 == table->left_index) {
                    block = sky_deque_block_create(table->len);
                    block->right = table->left_block;
                    table->left_block->left = block;
                    table->left_block = block;
                    table->left_index = SKY_DEQUE_BLOCKLEN - 1;
                }
                else {
                    block = table->left_block;
                    --table->left_index;
                }
                ++table->len;
                sky_object_gc_set(&(block->objects[table->left_index]),
                                  item,
                                  self);
                if (table->maxlen >= 0 && table->len > table->maxlen) {
                    sky_deque_table_pop(table);
                }
            }
        }
    } SKY_ASSET_BLOCK_END;
}

static sky_object_t
sky_deque_ge(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}

static sky_object_t
sky_deque_getitem(sky_deque_t self, sky_object_t item)
{
    ssize_t         i;
    sky_object_t    value;

    if (!SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "sequence index must be integer, not %#@",
                               sky_type_name(sky_object_type(item)));
    }

    SKY_ERROR_TRY {
        i = sky_integer_value(sky_number_index(item),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_clear();
        sky_error_raise_string(sky_IndexError,
                               "cannot fit 'int' into an index-sized integer");
    } SKY_ERROR_TRY_END;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             index, n;
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;

        i = sky_deque_index_normalize(i, table->len);
        if (i < 0 || i >= table->len) {
            sky_format_fprintf(stderr,
                               "IndexError (get) : %@ -> %zd %zd\n",
                               item, i, table->len);
            sky_error_raise_string(sky_IndexError, "deque index out of range");
        }
        index = i;

        if (!i) {
            i = table->left_index;
            block = table->left_block;
        }
        else if (table->len - 1 == i) {
            i = table->right_index;
            block = table->right_block;
        }
        else {
            i += table->left_index;
            n = i / SKY_DEQUE_BLOCKLEN;
            i %= SKY_DEQUE_BLOCKLEN;
            if (index < (table->len >> 1)) {
                for (block = table->left_block; n--; block = block->right);
            }
            else {
                n = ((table->left_index + table->len - 1) /
                     SKY_DEQUE_BLOCKLEN) - n;
                for (block = table->right_block; n--; block = block->left);
            }
        }

        value = block->objects[i];
    } SKY_ASSET_BLOCK_END;

    return value;
}

static sky_object_t
sky_deque_gt(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_GREATER);
}

static sky_object_t
sky_deque_iadd(sky_deque_t self, sky_object_t other)
{
    sky_deque_extend(self, other);
    return self;
}

static void
sky_deque_init(sky_deque_t self, sky_object_t iterable, sky_object_t maxlen)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    ssize_t         maxlen_value;

    if (sky_object_isnull(maxlen)) {
        maxlen_value = -1;
    }
    else {
        maxlen_value = sky_integer_value(sky_number_integer(maxlen),
                                         SSIZE_MIN, SSIZE_MAX,
                                         NULL);
        if (maxlen_value < 0) {
            sky_error_raise_string(sky_ValueError,
                                   "maxlen must be non-negative");
        }
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_lock(self, SKY_TRUE);
        if (!self_data->table->len) {
            self_data->table->maxlen = maxlen_value;
        }
        else {
            sky_deque_table_t   *old_table = self_data->table;

            self_data->table = sky_deque_table_create(maxlen_value);
            sky_hazard_pointer_retire(old_table,
                                      (sky_free_t)sky_deque_table_free);
        }
    } SKY_ASSET_BLOCK_END;

    if (!sky_object_isnull(iterable)) {
        sky_deque_extend(self, iterable);
    }
}

static sky_deque_iterator_t
sky_deque_iter(sky_deque_t self)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    sky_deque_iterator_t        iterator;
    sky_deque_iterator_data_t   *iterator_data;

    iterator = sky_object_allocate(sky_deque_iterator_type);
    iterator_data = sky_deque_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->deque)), self, iterator);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = self_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->block = table->left_block;
        iterator_data->block_index = table->left_index;
        iterator_data->counter = table->len;
    } SKY_ASSET_BLOCK_END;

    return iterator;
}

static sky_object_t
sky_deque_le(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}

ssize_t
sky_deque_len(sky_deque_t self)
{
    ssize_t len;

    SKY_ASSET_BLOCK_BEGIN {
        len = sky_deque_table(self, SKY_TRUE)->len;
    } SKY_ASSET_BLOCK_END;

    return len;
}

static sky_object_t
sky_deque_lt(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_LESS);
}

static sky_object_t
sky_deque_maxlen_getter(sky_object_t self, SKY_UNUSED sky_type_t type)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table = sky_deque_table(self, SKY_TRUE);

        if (table->maxlen < 0) {
            result = sky_None;
        }
        else {
            result = sky_integer_create(table->maxlen);
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_deque_ne(sky_deque_t self, sky_object_t other)
{
    return sky_deque_compare(self, other, SKY_COMPARE_OP_NOT_EQUAL);
}

sky_object_t
sky_deque_pop(sky_deque_t self)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_data_t    *self_data = sky_deque_data(self);

        sky_deque_lock(self, SKY_TRUE);
        result = sky_deque_table_pop(self_data->table);
    } SKY_ASSET_BLOCK_END;

    return result;
}

sky_object_t
sky_deque_popleft(sky_deque_t self)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_data_t    *self_data = sky_deque_data(self);

        sky_deque_lock(self, SKY_TRUE);
        result = sky_deque_table_popleft(self_data->table);
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_deque_reduce(sky_deque_t self)
{
    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table = sky_deque_table(self, SKY_TRUE);

        sky_object_t    dict, list;

        dict = sky_object_getattr(self, SKY_STRING_LITERAL("__dict__"), NULL);
        list = sky_list_create(self);

        if (!dict) {
            if (table->maxlen < 0) {
                result = sky_object_build("(O(O))",
                                          sky_object_type(self),
                                          list);
            }
            else {
                result = sky_object_build("(O(Oiz))",
                                          sky_object_type(self),
                                          list,
                                          table->maxlen);
            }
        }
        else {
            if (table->maxlen < 0) {
                result = sky_object_build("(O(OO)O)",
                                          sky_object_type(self),
                                          list,
                                          sky_None,
                                          dict);
            }
            else {
                result = sky_object_build("(O(Oiz)O)",
                                          sky_object_type(self),
                                          list,
                                          table->maxlen,
                                          dict);
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


typedef struct sky_deque_remove_state_s {
    sky_mutex_t                         mutex;
    sky_bool_t                          locked;
} sky_deque_remove_state_t;

static void
sky_deque_remove_cleanup(void *arg)
{
    sky_deque_remove_state_t    *state = arg;

    if (state->locked) {
        sky_mutex_unlock(state->mutex);
    }
}

void
sky_deque_remove(sky_deque_t self, sky_object_t value)
{
    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_data_t            *self_data = sky_deque_data(self);
        sky_deque_remove_state_t    state = { self_data->mutex, SKY_TRUE };

        ssize_t             i;
        uintptr_t           revision;
        sky_bool_t          found;
        sky_deque_table_t   *table;

        sky_mutex_lock(state.mutex, SKY_TIME_INFINITE);
        sky_asset_save(&state,
                       sky_deque_remove_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        table = sky_deque_data(self)->table;
        revision = table->revision;
        found = SKY_FALSE;

        SKY_ERROR_TRY {
            for (i = 0; i < table->len; ++i) {
                sky_bool_t      cmp;
                sky_object_t    item;
                
                item = table->left_block->objects[table->left_index];

                sky_mutex_unlock(state.mutex);
                state.locked = SKY_FALSE;
                cmp = sky_object_compare(item, value, SKY_COMPARE_OP_EQUAL);
                sky_mutex_lock(state.mutex, SKY_TIME_INFINITE);
                state.locked = SKY_TRUE;

                if (table != self_data->table || revision != table->revision) {
                    sky_error_raise_string(sky_IndexError,
                                           "deque mutated during remove().");
                }

                if (cmp) {
                    sky_deque_table_popleft(table);
                    sky_deque_table_rotate(table, i, self);
                    found = SKY_TRUE;
                    break;
                }
                sky_deque_table_rotate(table, -1, self);
                revision = table->revision;
            }
        } SKY_ERROR_EXCEPT_ANY {
            sky_deque_table_rotate(table, i, self);
            sky_error_raise();
        } SKY_ERROR_TRY_END;

        if (!found) {
            sky_error_raise_string(sky_ValueError,
                                   "deque.remove(x): x not in deque");
        }
    } SKY_ASSET_BLOCK_END;
}

sky_string_t
sky_deque_repr(sky_deque_t self)
{
    sky_string_t    result;

    if (sky_object_stack_push(self)) {
        return SKY_STRING_LITERAL("[...]");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t       *table;

        sky_asset_save(self, sky_object_stack_pop, SKY_ASSET_CLEANUP_ALWAYS);

        table = sky_deque_table(self, SKY_TRUE);
        if (table->maxlen >= 0) {
            result = sky_string_createfromformat("deque(%#@, maxlen=%zd)",
                                                 sky_list_create(self),
                                                 table->maxlen);
        }
        else {
            result = sky_string_createfromformat("deque(%#@)",
                                                 sky_list_create(self));
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

void
sky_deque_reverse(sky_deque_t self)
{
    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             left_index, i, n, right_index;
        sky_object_t        object;
        sky_deque_block_t   *left_block, *right_block;
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;
        left_block = table->left_block;
        left_index = table->left_index;
        right_block = table->right_block;
        right_index = table->right_index;

        n = table->len / 2;
        for (i = 0; i < n; ++i) {
            /* Validate that pointers haven't met in the middle */
            sky_error_validate_debug(left_block != right_block ||
                                     left_index < right_index);

            /* Swap */
            object = left_block->objects[left_index];
            sky_object_gc_set(&(left_block->objects[left_index]),
                              right_block->objects[right_index],
                              self);
            sky_object_gc_set(&(right_block->objects[right_index]),
                              object,
                              self);

            /* Advance left block/index pair */
            ++left_index;
            if (SKY_DEQUE_BLOCKLEN == left_index) {
                if (!left_block->right) {
                    break;
                }
                left_block = left_block->right;
                left_index = 0;
            }

            /* Step backwards with the right block/index pair */
            --right_index;
            if (-1 == right_index) {
                if (!right_block->left) {
                    break;
                }
                right_block = right_block->left;
                right_index = SKY_DEQUE_BLOCKLEN - 1;
            }
        }
    } SKY_ASSET_BLOCK_END;
}

sky_deque_reverse_iterator_t
sky_deque_reversed(sky_deque_t self)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    sky_deque_iterator_data_t       *iterator_data;
    sky_deque_reverse_iterator_t    iterator;

    iterator = sky_object_allocate(sky_deque_reverse_iterator_type);
    iterator_data = sky_deque_reverse_iterator_data(iterator);
    sky_object_gc_set(SKY_AS_OBJECTP(&(iterator_data->deque)), self, iterator);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = self_data->table;

        iterator_data->table = table;
        iterator_data->revision = table->revision;
        iterator_data->block = table->right_block;
        iterator_data->block_index = table->right_index;
        iterator_data->counter = table->len;
    } SKY_ASSET_BLOCK_END;

    return iterator;
}

void
sky_deque_rotate(sky_deque_t self, ssize_t n)
{
    sky_deque_data_t    *self_data = sky_deque_data(self);

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_lock(self, SKY_TRUE);
        sky_deque_table_rotate(self_data->table, n, self);
    } SKY_ASSET_BLOCK_END;
}

static void
sky_deque_setitem(sky_deque_t self, sky_object_t item, sky_object_t value)
{
    ssize_t i;

    if (!SKY_OBJECT_METHOD_SLOT(item, INDEX)) {
        sky_error_raise_format(sky_TypeError,
                               "sequence index must be integer, not %#@",
                               sky_type_name(sky_object_type(item)));
    }

    SKY_ERROR_TRY {
        i = sky_integer_value(sky_number_index(item),
                              SSIZE_MIN, SSIZE_MAX,
                              NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_error_clear();
        sky_error_raise_string(sky_IndexError,
                               "cannot fit 'int' into an index-sized integer");
    } SKY_ERROR_TRY_END;

    SKY_ASSET_BLOCK_BEGIN {
        ssize_t             halflen, index, n;
        sky_deque_block_t   *block;
        sky_deque_table_t   *table;

        sky_deque_lock(self, SKY_TRUE);
        table = sky_deque_data(self)->table;

        if (i < 0 || i >= table->len) {
            sky_error_raise_string(sky_IndexError, "deque index out of range");
        }
        index = i;
        halflen = (table->len + 1) >> 1;

        i += table->left_index;
        n = i / SKY_DEQUE_BLOCKLEN;
        i %= SKY_DEQUE_BLOCKLEN;
        if (index <= halflen) {
            for (block = table->left_block; n--; block = block->right);
        }
        else {
            n = (table->left_index + table->len - 1) / SKY_DEQUE_BLOCKLEN - n;
            for (block = table->right_block; n--; block = block->left);
        }
        sky_object_gc_set(&(block->objects[i]), value, self);
    } SKY_ASSET_BLOCK_END;
}

size_t
sky_deque_sizeof(sky_deque_t self)
{
    size_t                  result;
    sky_deque_block_t       *block;
    sky_hazard_pointer_t    *hazard;

    result = sky_memsize(self);
    if (!sky_deque_isshared(self)) {
        for (block = sky_deque_data(self)->table->left_block;
             block;
             block = block->right)
        {
            result += sky_memsize(block);
        }
    }
    else {
        SKY_ASSET_BLOCK_BEGIN {
            sky_deque_table_t   *table = sky_deque_table(self, SKY_TRUE);

            hazard = sky_hazard_pointer_acquire(NULL);
            sky_asset_save(hazard,
                           (sky_free_t)sky_hazard_pointer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);

            sky_hazard_pointer_update(hazard,
                                      SKY_AS_VOIDP(&(table->left_block)));
            while ((block = hazard->pointer) != NULL) {
                if ((uintptr_t)block & 1) {
                    result = sky_memsize(self);
                    sky_hazard_pointer_update(hazard,
                                              SKY_AS_VOIDP(&(table->left_block)));
                }
                else {
                    result += sky_memsize(block);
                    sky_hazard_pointer_update(hazard,
                                              SKY_AS_VOIDP(&(block->right)));
                }
            }
        } SKY_ASSET_BLOCK_END;
    }

    return result;
}


static sky_deque_iterator_t
sky_deque_iterator_iter(sky_deque_iterator_t self)
{
    return self;
}

static sky_object_t
sky_deque_iterator_length_hint(sky_deque_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data =
                                sky_deque_iterator_data(self);

    return sky_integer_create(self_data->counter);
}

static sky_object_t
sky_deque_iterator_new(
        SKY_UNUSED  sky_type_t  cls,
                    sky_deque_t deque,
                    ssize_t     index)
{
    sky_object_t    self;

    self = sky_deque_iter(deque);
    while (index-- > 0 && sky_object_next(self, NULL));

    return self;
}

static sky_object_t
sky_deque_iterator_next(sky_deque_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data =
                                sky_deque_iterator_data(self);

    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_data_t    *deque_data = sky_deque_data(self_data->deque);

        sky_deque_table_t   *table;

        sky_deque_lock(self_data->deque, SKY_TRUE);
        table = deque_data->table;

        if (self_data->table != table ||
            self_data->revision != table->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "deque mutated during iteration");
        }

        if (!self_data->counter) {
            result = NULL;
        }
        else {
            result = self_data->block->objects[self_data->block_index];
            ++self_data->block_index;
            --self_data->counter;
            if (self_data->counter > 0 &&
                SKY_DEQUE_BLOCKLEN == self_data->block_index)
            {
                self_data->block = self_data->block->right;
                self_data->block_index = 0;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_deque_iterator_reduce(sky_deque_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data =
                                sky_deque_iterator_data(self);

    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table;

        sky_deque_lock(self_data->deque, SKY_TRUE);
        table = sky_deque_data(self_data->deque)->table;

        result = sky_object_build("(O(Oiz))",
                                  sky_object_type(self),
                                  self_data->deque,
                                  table->len - self_data->counter);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static sky_deque_reverse_iterator_t
sky_deque_reverse_iterator_iter(sky_deque_reverse_iterator_t self)
{
    return self;
}

static sky_object_t
sky_deque_reverse_iterator_length_hint(sky_deque_reverse_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data = 
                                sky_deque_reverse_iterator_data(self);

    return sky_integer_create(self_data->counter);
}

static sky_object_t
sky_deque_reverse_iterator_new(
        SKY_UNUSED  sky_type_t  cls,
                    sky_deque_t deque,
                    ssize_t     index)
{
    sky_object_t    self;

    self = sky_deque_reversed(deque);
    while (index-- > 0 && sky_object_next(self, NULL));

    return self;
}

static sky_object_t
sky_deque_reverse_iterator_next(sky_deque_reverse_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data =
                                sky_deque_reverse_iterator_data(self);

    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_data_t    *deque_data = sky_deque_data(self_data->deque);

        sky_deque_table_t   *table;

        sky_deque_lock(self_data->deque, SKY_TRUE);
        table = deque_data->table;

        if (self_data->table != table ||
            self_data->revision != table->revision)
        {
            sky_error_raise_string(sky_RuntimeError,
                                   "deque mutated during iteration");
        }

        if (!self_data->counter) {
            result = NULL;
        }
        else {
            result = self_data->block->objects[self_data->block_index];
            --self_data->block_index;
            --self_data->counter;
            if (self_data->counter > 0 &&
                -1 == self_data->block_index)
            {
                self_data->block = self_data->block->left;
                self_data->block_index = SKY_DEQUE_BLOCKLEN - 1;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}

static sky_object_t
sky_deque_reverse_iterator_reduce(sky_deque_reverse_iterator_t self)
{
    sky_deque_iterator_data_t   *self_data =
                                sky_deque_reverse_iterator_data(self);

    sky_object_t    result;

    SKY_ASSET_BLOCK_BEGIN {
        sky_deque_table_t   *table;

        sky_deque_lock(self_data->deque, SKY_TRUE);
        table = sky_deque_data(self_data->deque)->table;

        result = sky_object_build("(O(Oiz))",
                                  sky_object_type(self),
                                  self_data->deque,
                                  table->len - self_data->counter);
    } SKY_ASSET_BLOCK_END;

    return result;
}


static const char sky_deque_type_doc[] =
"deque([iterable[, maxlen]]) --> deque object\n\n"
"Build an ordered collection with optimized access from its endpoints.";

SKY_TYPE_DEFINE_ITERABLE(deque,
                         "deque",
                         sizeof(sky_deque_data_t),
                         sky_deque_instance_initialize,
                         sky_deque_instance_finalize,
                         sky_deque_instance_visit,
                         sky_deque_instance_iterate,
                         sky_deque_instance_iterate_cleanup,
                         0,
                         sky_deque_type_doc);

SKY_TYPE_DEFINE_SIMPLE(deque_iterator,
                       "_deque_iterator",
                       sizeof(sky_deque_iterator_data_t),
                       NULL,
                       NULL,
                       sky_deque_iterator_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);

SKY_TYPE_DEFINE_SIMPLE(deque_reverse_iterator,
                       "_deque_reverse_iterator",
                       sizeof(sky_deque_iterator_data_t),
                       NULL,
                       NULL,
                       sky_deque_reverse_iterator_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       NULL);


void
sky_deque_initialize_library(void)
{
    sky_type_initialize_builtin(sky_deque_type, 0);

    sky_type_setattr_getset(
            sky_deque_type,
            "maxlen",
            "maximum size of a deque or None if unbounded",
            sky_deque_maxlen_getter,
            NULL);

    sky_type_setattr_builtin(sky_deque_type,
            "append",
            sky_function_createbuiltin(
                    "deque.append",
                    "Add an element to the right side of the deque.",
                    (sky_native_code_function_t)sky_deque_append,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "item", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "appendleft",
            sky_function_createbuiltin(
                    "deque.appendleft",
                    "Add an element to the left side of the deque.",
                    (sky_native_code_function_t)sky_deque_appendleft,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "item", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "clear",
            sky_function_createbuiltin(
                    "deque.clear",
                    "Remove all elements from the deque.",
                    (sky_native_code_function_t)sky_deque_clear,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "__copy__",
            sky_function_createbuiltin(
                    "deque.__copy__",
                    "Return a shallow copy of a deque.",
                    (sky_native_code_function_t)sky_deque_copy,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "count",
            sky_function_createbuiltin(
                    "deque.count",
                    "D.count(value) -> integer -- return number of occurrences of value",
                    (sky_native_code_function_t)sky_deque_count,
                    SKY_DATA_TYPE_SSIZE_T,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "extend",
            sky_function_createbuiltin(
                    "deque.extend",
                    "Extend the right side of the deque with elements from the iterable",
                    (sky_native_code_function_t)sky_deque_extend,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "extendleft",
            sky_function_createbuiltin(
                    "deque.extendleft",
                    "Extend the left side of the deque with elements from the iterable",
                    (sky_native_code_function_t)sky_deque_extendleft,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "iterable", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "pop",
            sky_function_createbuiltin(
                    "deque.pop",
                    "Remove and return the rightmost element.",
                    (sky_native_code_function_t)sky_deque_pop,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "popleft",
            sky_function_createbuiltin(
                    "deque.popleft",
                    "Remove and return the leftmost element.",
                    (sky_native_code_function_t)sky_deque_popleft,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "remove",
            sky_function_createbuiltin(
                    "deque.remove",
                    "D.remove(value) -- remove first occurrence of value.",
                    (sky_native_code_function_t)sky_deque_remove,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "value", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "reverse",
            sky_function_createbuiltin(
                    "deque.reverse",
                    "D.reverse() -- reverse *IN PLACE*",
                    (sky_native_code_function_t)sky_deque_reverse,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    NULL));
    sky_type_setattr_builtin(sky_deque_type,
            "rotate",
            sky_function_createbuiltin(
                    "deque.rotate",
                    "Rotate the deque n steps to the right (default n=1).  "
                    "If n is negative, rotates left.",
                    (sky_native_code_function_t)sky_deque_rotate,
                    SKY_DATA_TYPE_VOID,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
                    "n", SKY_DATA_TYPE_SSIZE_T, sky_integer_one,
                    NULL));

    sky_type_setattr_builtin(sky_deque_type, "__hash__", sky_None);
    sky_type_setmethodslotwithparameters(
            sky_deque_type,
            "__init__",
            (sky_native_code_function_t)sky_deque_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            "maxlen", SKY_DATA_TYPE_OBJECT, sky_NotSpecified,
            NULL);
    sky_type_setmethodslots(sky_deque_type,
            "__repr__", sky_deque_repr,
            "__sizeof__", sky_deque_sizeof,
            "__lt__", sky_deque_lt,
            "__le__", sky_deque_le,
            "__eq__", sky_deque_eq,
            "__ne__", sky_deque_ne,
            "__gt__", sky_deque_gt,
            "__ge__", sky_deque_ge,
            "__len__", sky_deque_len,
            "__getitem__", sky_deque_getitem,
            "__setitem__", sky_deque_setitem,
            "__delitem__", sky_deque_delitem,
            "__iter__", sky_deque_iter,
            "__reversed__", sky_deque_reversed,
            "__iadd__", sky_deque_iadd,
            "__reduce__", sky_deque_reduce,
            NULL);
    sky_type_setattr_builtin(sky_deque_type,
                             "__module__",
                             SKY_STRING_LITERAL("collections"));


    sky_type_initialize_builtin(sky_deque_iterator_type, 0);
    sky_type_setattr_builtin(sky_deque_iterator_type, "__hash__", sky_None);
    sky_type_setmethodslotwithparameters(
            sky_deque_iterator_type,
            "__new__",
            (sky_native_code_function_t)sky_deque_iterator_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "deque", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
            "index", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
            NULL);
    sky_type_setmethodslots(sky_deque_iterator_type,
            "__length_hint__", sky_deque_iterator_length_hint,
            "__iter__", sky_deque_iterator_iter,
            "__next__", sky_deque_iterator_next,
            "__reduce__", sky_deque_iterator_reduce,
            NULL);
    sky_type_setattr_builtin(sky_deque_iterator_type,
                             "__module__",
                             SKY_STRING_LITERAL("_collections"));


    sky_type_initialize_builtin(sky_deque_reverse_iterator_type, 0);
    sky_type_setattr_builtin(sky_deque_reverse_iterator_type,
                             "__hash__",
                             sky_None);
    sky_type_setmethodslotwithparameters(
            sky_deque_reverse_iterator_type,
            "__new__",
            (sky_native_code_function_t)sky_deque_reverse_iterator_new,
            "cls", SKY_DATA_TYPE_OBJECT_TYPE, NULL,
            "deque", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_deque_type,
            "index", SKY_DATA_TYPE_SSIZE_T, sky_integer_zero,
            NULL);
    sky_type_setmethodslots(sky_deque_reverse_iterator_type,
            "__length_hint__", sky_deque_reverse_iterator_length_hint,
            "__iter__", sky_deque_reverse_iterator_iter,
            "__next__", sky_deque_reverse_iterator_next,
            "__reduce__", sky_deque_reverse_iterator_reduce,
            NULL);
    sky_type_setattr_builtin(sky_deque_reverse_iterator_type,
                             "__module__",
                             SKY_STRING_LITERAL("_collections"));
}
