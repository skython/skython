#include "sky_private.h"


typedef struct sky_slice_data_s {
    sky_object_t                        start;
    sky_object_t                        stop;
    sky_object_t                        step;
} sky_slice_data_t;

SKY_EXTERN_INLINE sky_slice_data_t *
sky_slice_data(sky_object_t object)
{
    if (sky_object_type(object) == sky_slice_type) {
        return (sky_slice_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_slice_type);
}


static void
sky_slice_instance_visit(
        SKY_UNUSED  sky_object_t                object,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_slice_data_t    *slice_data = data;

    sky_object_visit(slice_data->start, visit_data);
    sky_object_visit(slice_data->stop, visit_data);
    sky_object_visit(slice_data->step, visit_data);
}


static sky_object_t
sky_slice_compare(sky_slice_t self, sky_slice_t other, sky_compare_op_t op)
{
    int             i;
    sky_object_t    *o, *s;

    switch (op) {
        case SKY_COMPARE_OP_EQUAL:
        case SKY_COMPARE_OP_LESS_EQUAL:
        case SKY_COMPARE_OP_GREATER_EQUAL:
            if (self == other) {
                return sky_True;
            }
            break;
        case SKY_COMPARE_OP_NOT_EQUAL:
            if (self == other) {
                return sky_False;
            }
            break;
        default:
            break;
    }

    if (!sky_object_isa(self, sky_slice_type) ||
        !sky_object_isa(other, sky_slice_type))
    {
        return sky_NotImplemented;
    }

    s = &(sky_slice_data(self)->start);
    o = &(sky_slice_data(other)->start);
    for (i = 0; i < 3; ++i) {
        if (!sky_object_compare(s[i], o[i], SKY_COMPARE_OP_EQUAL)) {
            return (sky_object_compare(s[i], o[i], op) ? sky_True : sky_False);
        }
    }

    return (((int)op & SKY_COMPARE_OP_EQUAL) ? sky_True : sky_False);
}


static ssize_t
sky_slice_index(sky_object_t object)
{
    if (!SKY_OBJECT_METHOD_SLOT(object, INDEX)) {
        sky_error_raise_string(
                sky_TypeError,
                "slice indices must be integers or None or have an __index__() method");
    }

    /* Call sky_number_index() to get a sky_integer_t object. */
    object = sky_number_index(object);

    /* Ask the sky_integer_t object to return a C numeric value clamped to
     * [-SSIZE_MAX:SSIZE_MAX] without raising an OverflowError exception.
     */
    return (ssize_t)sky_integer_value_clamped(object, SSIZE_MIN, SSIZE_MAX);
}


sky_slice_t
sky_slice_create(sky_object_t start, sky_object_t stop, sky_object_t step)
{
    sky_slice_t         slice;
    sky_slice_data_t    *slice_data;

    slice = sky_object_allocate(sky_slice_type);
    slice_data = sky_slice_data(slice);
    sky_object_gc_set(&(slice_data->start), (start ? start : sky_None), slice);
    sky_object_gc_set(&(slice_data->stop), (stop ? stop : sky_None), slice);
    sky_object_gc_set(&(slice_data->step), (step ? step : sky_None), slice);

    return slice;
}


sky_object_t
sky_slice_eq(sky_slice_t self, sky_slice_t other)
{
    int             i;
    sky_object_t    *o, *s;

    if (self == other) {
        return sky_True;
    }
    if (!sky_object_isa(self, sky_slice_type) ||
        !sky_object_isa(other, sky_slice_type))
    {
        return sky_NotImplemented;
    }

    s = &(sky_slice_data(self)->start);
    o = &(sky_slice_data(other)->start);
    for (i = 0; i < 3; ++i) {
        if (!sky_object_compare(s[i], o[i], SKY_COMPARE_OP_EQUAL)) {
            return sky_False;
        }
    }

    return sky_True;
}


sky_object_t
sky_slice_ge(sky_slice_t self, sky_slice_t other)
{
    return sky_slice_compare(self, other, SKY_COMPARE_OP_GREATER_EQUAL);
}


sky_object_t
sky_slice_gt(sky_slice_t self, sky_slice_t other)
{
    return sky_slice_compare(self, other, SKY_COMPARE_OP_GREATER);
}


sky_object_t
sky_slice_indices(sky_object_t self, ssize_t len)
{
    ssize_t start, stop, step;

    sky_slice_range(self, len, &start, &stop, &step);
    return sky_object_build("(iziziz)", start, stop, step);
}


void
sky_slice_init(sky_object_t self,
               sky_object_t start,
               sky_object_t stop,
               sky_object_t step)
{
    sky_slice_data_t    *slice_data = sky_slice_data(self);

    /* This swapping of stop and start is to maintain similarily with range().
     */
    if (sky_object_isnull(stop)) {
        stop = start;
        start = sky_None;
    }
    sky_object_gc_set(&(slice_data->start), start, self);
    sky_object_gc_set(&(slice_data->stop), stop, self);
    sky_object_gc_set(&(slice_data->step), step, self);
}


sky_object_t
sky_slice_le(sky_slice_t self, sky_slice_t other)
{
    return sky_slice_compare(self, other, SKY_COMPARE_OP_LESS_EQUAL);
}


sky_object_t
sky_slice_lt(sky_slice_t self, sky_slice_t other)
{
    return sky_slice_compare(self, other, SKY_COMPARE_OP_LESS);
}


sky_object_t
sky_slice_ne(sky_slice_t self, sky_slice_t other)
{
    int             i;
    sky_object_t    *o, *s;

    if (self == other) {
        return sky_False;
    }
    if (!sky_object_isa(self, sky_slice_type) ||
        !sky_object_isa(other, sky_slice_type))
    {
        return sky_NotImplemented;
    }

    s = &(sky_slice_data(self)->start);
    o = &(sky_slice_data(other)->start);
    for (i = 0; i < 3; ++i) {
        if (!sky_object_compare(s[i], o[i], SKY_COMPARE_OP_EQUAL)) {
            return sky_True;
        }
    }

    return sky_False;
}


ssize_t
sky_slice_range(sky_slice_t slice,
                ssize_t     length,
                ssize_t *   start,
                ssize_t *   stop,
                ssize_t *   step)
{
    sky_slice_data_t    *slice_data = sky_slice_data(slice);

    if (sky_object_isnull(slice_data->step)) {
        *step = 1;
    }
    else if (!(*step = sky_slice_index(slice_data->step))) {
        sky_error_raise_string(sky_ValueError,
                               "slice step cannot be zero.");
    }
    else if (*step < -SSIZE_MAX) {
        *step = -SSIZE_MAX;
    }
    if (sky_object_isnull(slice_data->start)) {
        *start = (*step < 0 ? length - 1 : 0);
    }
    else {
        *start = sky_slice_index(slice_data->start);
        if (*start < 0) {
            *start += length;
        }
        if (*start < 0) {
            *start = (*step < 0 ? -1 : 0);
        }
        if (*start >= length) {
            *start = (*step < 0 ? length - 1 : length);
        }
    }
    if (sky_object_isnull(slice_data->stop)) {
        *stop = (*step < 0 ? -1 : length);
    }
    else {
        *stop = sky_slice_index(slice_data->stop);
        if (*stop < 0) {
            *stop += length;
        }
        if (*stop < 0) {
            *stop = (*step < 0 ? -1 : 0);
        }
        if (*stop >= length) {
            *stop = (*step < 0 ? length - 1 : length);
        }
    }

    if ((*step < 0 && *stop >= *start) || (*step > 0 && *start >= *stop)) {
        return 0;
    }
    if (*step < 0) {
        return (*stop - *start + 1) / *step + 1;
    }
    return (*stop - *start - 1) / *step + 1;
}


sky_object_t
sky_slice_reduce(sky_object_t self)
{
    sky_slice_data_t    *slice_data = sky_slice_data(self);

    return sky_object_build("(O(OOO))",
                            sky_object_type(self),
                            slice_data->start,
                            slice_data->stop,
                            slice_data->step);
}


sky_string_t
sky_slice_repr(sky_object_t self)
{
    sky_slice_data_t    *slice_data = sky_slice_data(self);

    return sky_string_createfromformat("slice(%#@, %#@, %#@)",
                                       slice_data->start,
                                       slice_data->stop,
                                       slice_data->step);
}


sky_object_t
sky_slice_start(sky_slice_t slice)
{
    return sky_slice_data(slice)->start;
}

sky_object_t
sky_slice_step(sky_slice_t slice)
{
    return sky_slice_data(slice)->step;
}

sky_object_t
sky_slice_stop(sky_slice_t slice)
{
    return sky_slice_data(slice)->stop;
}


static const char sky_slice_type_doc[] =
"slice(stop)\n"
"slice(start, stop[, step])\n"
"\n"
"Create a slice object. This is used for extended slicing (e.g. a[0:10:2]).";


SKY_TYPE_DEFINE_SIMPLE(slice,
                       "slice",
                       sizeof(sky_slice_data_t),
                       NULL,
                       NULL,
                       sky_slice_instance_visit,
                       SKY_TYPE_FLAG_FINAL,
                       sky_slice_type_doc);


void
sky_slice_initialize_library(void)
{
    sky_type_initialize_builtin(sky_slice_type, 0);

    sky_type_setmethodslotwithparameters(
            sky_slice_type,
            "__init__",
            (sky_native_code_function_t)sky_slice_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "start", SKY_DATA_TYPE_OBJECT, sky_None,
            "stop", SKY_DATA_TYPE_OBJECT, sky_None,
            "step", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);

    sky_type_setmethodslots(sky_slice_type,
            "__repr__", sky_slice_repr,
            "__lt__", sky_slice_lt,
            "__le__", sky_slice_le,
            "__eq__", sky_slice_eq,
            "__ne__", sky_slice_ne,
            "__gt__", sky_slice_gt,
            "__ge__", sky_slice_ge,
            "__reduce__", sky_slice_reduce,
            NULL);

    /* Slice objects are very much intentionally not hashable. */
    sky_type_setattr_builtin(sky_slice_type, "__hash__", sky_None);

    sky_type_setmembers(sky_slice_type,
            "start",    NULL, offsetof(sky_slice_data_t, start),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "stop",     NULL, offsetof(sky_slice_data_t, stop),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "step",     NULL, offsetof(sky_slice_data_t, step),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);

    sky_type_setattr_builtin(
            sky_slice_type,
            "indices",
            sky_function_createbuiltin(
                    "indices",
                    "S.indices(len) -> (start, stop, stride)\n\n"
                    "Assuming a sequence of length len, calculate the start and stop\n"
                    "indices, and the stride length of the extended slice described by\n"
                    "S. Out of bounds indices are clipped in a manner consistent with the\n"
                    "handling of normal slices.",
                    (sky_native_code_function_t)sky_slice_indices,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_slice_type,
                    "len", SKY_DATA_TYPE_SSIZE_T, NULL,
                    NULL));
}
