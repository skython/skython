#ifndef __SKYTHON_CORE_SKY_FRAME_PRIVATE_H__
#define __SKYTHON_CORE_SKY_FRAME_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_object_private.h"


SKY_CDECLS_BEGIN


typedef enum sky_frame_block_type_e {
    SKY_FRAME_BLOCK_TYPE_TRY_EXCEPT,
    SKY_FRAME_BLOCK_TYPE_TRY_FINALLY,
    SKY_FRAME_BLOCK_TYPE_WITH,

    SKY_FRAME_BLOCK_TYPE_EXCEPT,
    SKY_FRAME_BLOCK_TYPE_FINALLY,
    SKY_FRAME_BLOCK_TYPE_WITH_CLEANUP,
} sky_frame_block_type_t;

typedef struct sky_frame_block_s sky_frame_block_t;
struct sky_frame_block_s {
    sky_frame_block_t *                 next;
    sky_frame_block_type_t              type;
    ssize_t                             enter_offset;
    ssize_t                             exit_offset;
    ssize_t                             jump_offset;
    sky_object_t *                      sp;
    sky_error_record_t *                error;

    sky_object_t                        with_mgr;
    sky_object_t                        with_exit;
};


typedef struct sky_frame_data_s {
    sky_frame_t                         f_back;
    sky_object_t                        f_code;
    sky_dict_t                          f_globals;
    sky_object_t                        f_locals;
    sky_object_t                        f_builtins;

    ssize_t                             f_lasti;
    int                                 f_lineno;
    sky_object_t                        f_trace;

    ssize_t                             nvalues;
    sky_object_t *                      values;

    ssize_t                             nlocals;
    sky_object_t *                      locals;
    sky_object_t *                      stack;
    ssize_t                             ncellvars;
    sky_object_t *                      cellvars;
    ssize_t                             nfreevars;
    sky_object_t *                      freevars;
    sky_object_t *                      sp;

    ssize_t                             pc;
    sky_frame_block_t *                 blocks;
    const uint8_t *                     bytecode;
} sky_frame_data_t;

SKY_EXTERN_INLINE sky_frame_data_t *
sky_frame_data(sky_object_t object)
{
    if (sky_frame_type == sky_object_type(object)) {
        return (sky_frame_data_t *)
               ((char *)object + sizeof(sky_object_data_t));
    }
    return sky_object_data(object, sky_frame_type);
}


struct sky_frame_s {
    sky_object_data_t                   object_data;
    sky_frame_data_t                    frame_data;
};


extern void
sky_frame_block_free(sky_frame_block_t *block);

extern sky_frame_t
sky_frame_create(sky_object_t code, sky_dict_t globals, sky_object_t locals);

extern sky_object_t
sky_frame_evaluate(sky_frame_t          frame,
                   sky_object_t         arg,
                   sky_error_record_t * record);


typedef enum sky_frame_evalute_trace_e {
    SKY_FRAME_EVALUATE_TRACE_CALL,
    SKY_FRAME_EVALUATE_TRACE_LINE,
    SKY_FRAME_EVALUATE_TRACE_RETURN,
    SKY_FRAME_EVALUATE_TRACE_EXCEPTION,
} sky_frame_evaluate_trace_t;

extern sky_bool_t
sky_frame_evaluate_trace(sky_frame_t                frame,
                         sky_frame_evaluate_trace_t trace,
                         sky_object_t               arg);

extern sky_object_t
sky_frame_locals(sky_frame_t frame);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_FRAME_PRIVATE_H__ */
