#include "sky_private.h"
#include "sky_codec_private.h"
#include "sky_string_private.h"

#include "sky_mutex.h"


const sky_codec_t *sky_codec_filesystemencoding_codec;
sky_string_t sky_codec_filesystemencoding_string;
sky_string_t sky_codec_filesystemerrors_string;

static sky_codec_t sky_codec_ascii_def = {
    "ascii",
    0, NULL, NULL, NULL, sky_codec_decode_ascii,
    (SKY_CODEC_ENCODE_FLAG_WIDTH_1 |
     SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII),
    NULL, NULL, NULL, sky_codec_encode_ascii,
};
sky_codec_t const * const sky_codec_ascii = &sky_codec_ascii_def;

static sky_codec_t sky_codec_charmap_def = {
    "charmap",
    0, NULL, NULL, NULL, sky_codec_decode_charmap,
    0, NULL, NULL, NULL, sky_codec_encode_charmap,
};
sky_codec_t const * const sky_codec_charmap = &sky_codec_charmap_def;

static sky_codec_t sky_codec_escape_def = {
    "escape",
    (SKY_CODEC_DECODER_FLAG_ENCODE_STRING_INPUT |
     SKY_CODEC_DECODER_FLAG_OUTPUT_BYTES),
    NULL, NULL, NULL, sky_codec_decode_escape,
    SKY_CODEC_ENCODE_FLAG_BUFFER_INPUT_REQUIRED,
    NULL, NULL, NULL, sky_codec_encode_escape,
};
sky_codec_t const * const sky_codec_escape = &sky_codec_escape_def;

static sky_codec_t sky_codec_latin1_def = {
    "iso8859-1",
    0, NULL, NULL, NULL, sky_codec_decode_latin1,
    (SKY_CODEC_ENCODE_FLAG_WIDTH_1 |
     SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII),
    NULL, NULL, NULL, sky_codec_encode_latin1,
};
sky_codec_t const * const sky_codec_latin1 = &sky_codec_latin1_def;

static sky_codec_t sky_codec_locale_def = {
    "locale",
    0, NULL, NULL, NULL, sky_codec_decode_locale,
    0, NULL, NULL, NULL, sky_codec_encode_locale,
};
sky_codec_t const * const sky_codec_locale = &sky_codec_locale_def;

static sky_codec_t sky_codec_raw_unicode_escape_def = {
    "raw-unicode-escape",
    SKY_CODEC_DECODER_FLAG_ENCODE_STRING_INPUT,
    NULL, NULL, NULL, sky_codec_decode_raw_unicode_escape,
    0, NULL, NULL, NULL, sky_codec_encode_raw_unicode_escape,
};
sky_codec_t const * const sky_codec_raw_unicode_escape = &sky_codec_raw_unicode_escape_def;

static sky_codec_t sky_codec_unicode_escape_def = {
    "unicode-escape",
    SKY_CODEC_DECODER_FLAG_ENCODE_STRING_INPUT,
    NULL, NULL, NULL, sky_codec_decode_unicode_escape,
    0, NULL, NULL, NULL, sky_codec_encode_unicode_escape,
};
sky_codec_t const * const sky_codec_unicode_escape = &sky_codec_unicode_escape_def;

static sky_codec_t sky_codec_unicode_internal_def = {
    "unicode-internal",
    SKY_CODEC_DECODER_FLAG_RETURN_STRING_INPUT,
    NULL, NULL, NULL, sky_codec_decode_unicode_internal,
    SKY_CODEC_ENCODE_FLAG_DECODE_BUFFER_INPUT,
    NULL, NULL, NULL, sky_codec_encode_unicode_internal,
};
sky_codec_t const * const sky_codec_unicode_internal = &sky_codec_unicode_internal_def;

static sky_codec_t sky_codec_utf16_def = {
    "utf-16",
    0, NULL, NULL, NULL, sky_codec_decode_utf16,
    SKY_CODEC_ENCODE_FLAG_WIDTH_2,
    sky_codec_encode_utf16_initialize,
    NULL,
    NULL,
#if SKY_ENDIAN == SKY_ENDIAN_BIG
    sky_codec_encode_utf16_be,
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
    sky_codec_encode_utf16_le,
#else
#   error implementation missing
#endif
};
sky_codec_t const * const sky_codec_utf16 = &sky_codec_utf16_def;

static sky_codec_t sky_codec_utf16_be_def = {
    "utf-16-be",
    0,
    sky_codec_decode_utf16_be_initialize,
    NULL,
    NULL,
    sky_codec_decode_utf16_be,
    SKY_CODEC_ENCODE_FLAG_WIDTH_2,
    NULL, NULL, NULL, sky_codec_encode_utf16_be,
};
sky_codec_t const * const sky_codec_utf16_be = &sky_codec_utf16_be_def;

static sky_codec_t sky_codec_utf16_le_def = {
    "utf-16-le",
    0,
    sky_codec_decode_utf16_le_initialize,
    NULL,
    NULL,
    sky_codec_decode_utf16_le,
    SKY_CODEC_ENCODE_FLAG_WIDTH_2,
    NULL, NULL, NULL, sky_codec_encode_utf16_le,
};
sky_codec_t const * const sky_codec_utf16_le = &sky_codec_utf16_le_def;

static sky_codec_t sky_codec_utf32_def = {
    "utf-32",
    0, NULL, NULL, NULL, sky_codec_decode_utf32,
    SKY_CODEC_ENCODE_FLAG_WIDTH_4,
    sky_codec_encode_utf32_initialize,
    NULL,
    NULL,
#if SKY_ENDIAN == SKY_ENDIAN_BIG
    sky_codec_encode_utf32_be,
#elif SKY_ENDIAN == SKY_ENDIAN_LITTLE
    sky_codec_encode_utf32_le,
#else
#   error implementation missing
#endif
};
sky_codec_t const * const sky_codec_utf32 = &sky_codec_utf32_def;

static sky_codec_t sky_codec_utf32_be_def = {
    "utf-32-be",
    0,
    sky_codec_decode_utf32_be_initialize,
    NULL,
    NULL,
    sky_codec_decode_utf32_be,
    SKY_CODEC_ENCODE_FLAG_WIDTH_4,
    NULL, NULL, NULL, sky_codec_encode_utf32_be,
};
sky_codec_t const * const sky_codec_utf32_be = &sky_codec_utf32_be_def;

static sky_codec_t sky_codec_utf32_le_def = {
    "utf-32-le",
    0,
    sky_codec_decode_utf32_le_initialize,
    NULL,
    NULL,
    sky_codec_decode_utf32_le,
    SKY_CODEC_ENCODE_FLAG_WIDTH_4,
    NULL, NULL, NULL, sky_codec_encode_utf32_le,
};
sky_codec_t const * const sky_codec_utf32_le = &sky_codec_utf32_le_def;

static sky_codec_t sky_codec_utf7_def = {
    "utf-7",
    0, NULL, NULL, NULL, sky_codec_decode_utf7,
    0, NULL, NULL, NULL, sky_codec_encode_utf7,
};
sky_codec_t const * const sky_codec_utf7 = &sky_codec_utf7_def;

static sky_codec_t sky_codec_utf8_def = {
    "utf-8",
    0, NULL, NULL, NULL, sky_codec_decode_utf8,
    SKY_CODEC_ENCODE_FLAG_7BITS_ARE_ASCII,
    NULL, NULL, NULL, sky_codec_encode_utf8,
};
sky_codec_t const * const sky_codec_utf8 = &sky_codec_utf8_def;


static sky_dict_t   sky_codec_registry_cache = NULL;
static sky_mutex_t  sky_codec_registry_cache_mutex;

static sky_list_t   sky_codec_registry = NULL;
static sky_mutex_t  sky_codec_registry_mutex;

static sky_dict_t   sky_codec_error_registry = NULL;
static sky_mutex_t  sky_codec_error_registry_mutex;


const sky_codec_t *
sky_codec_builtin(sky_string_t encoding)
{
    char        lower[32], *p;
    const char  *c;

    if (!encoding) {
        return sky_codec_utf8;
    }

    /* Convert the input encoding to lowercase and map " " and "-" to "_" */
    if (!(c = sky_string_cstring(encoding))) {
        return NULL;
    }
    for (p = lower; *c; ++c) {
        if (p >= &(lower[sizeof(lower) - 2])) {
            return NULL;
        }
        *p++ = (*c == '-' || *c == ' ' ? '_' : sky_ctype_tolower(*c));
    }
    *p = '\0';

    /* All of these mappings are taken from Python's encodings.aliases */
    switch (lower[0]) {
        case '\0':
            return sky_codec_utf8;

        case '6':
            if (!strcmp(lower, "646")) {
                return sky_codec_ascii;
            }
            return NULL;

        case '8':
            if (!strcmp(lower, "8859")) {
                return sky_codec_latin1;
            }
            return NULL;

        case 'a':
            if (!strcmp(lower, "ascii") ||
                !strcmp(lower, "ansi_x3.4_1968") ||
                !strcmp(lower, "ansi_x3_4_1968") ||
                !strcmp(lower, "ansi_x3.4_1986"))
            {
                return sky_codec_ascii;
            }
            return NULL;

        case 'c':
            if (!strcmp(lower, "cp367") ||
                !strcmp(lower, "csascii"))
            {
                return sky_codec_ascii;
            }
            if (!strcmp(lower, "cp819") ||
                !strcmp(lower, "csisolatin1"))
            {
                return sky_codec_latin1;
            }
            return NULL;

        case 'e':
            if (!strcmp(lower, "escape")) {
                return sky_codec_escape;
            }
            return NULL;

        case 'i':
            if (!strcmp(lower, "iso_8859_1") ||
                !strcmp(lower, "iso8859_1") ||
                !strcmp(lower, "iso_8859_1") ||
                !strcmp(lower, "iso_8859_1_1987") ||
                !strcmp(lower, "iso_ir_100") ||
                !strcmp(lower, "ibm819"))
            {
                return sky_codec_latin1;
            }
            if (!strcmp(lower, "iso646_us") ||
                !strcmp(lower, "iso_646.irv_1991") ||
                !strcmp(lower, "iso_ir_6") ||
                !strcmp(lower, "ibm367"))
            {
                return sky_codec_ascii;
            }
            return NULL;

        case 'l':
            if (!strcmp(lower, "latin") ||
                !strcmp(lower, "latin1") ||
                !strcmp(lower, "latin_1") ||
                !strcmp(lower, "l1"))
            {
                return sky_codec_latin1;
            }
            if (!strcmp(lower, "locale")) {
                return sky_codec_locale;
            }
            return NULL;

        case 'r':
            if (!strcmp(lower, "rawunicodeescape") ||
                !strcmp(lower, "raw_unicode_escape"))
            {
                return sky_codec_raw_unicode_escape;
            }
            return NULL;

        case 'u':
            if ('t' == lower[1] && 'f' == lower[2]) {
                p = ('_' == lower[3] ? &(lower[4]) : &(lower[3]));
                if ('8' == p[0]) {
                    if (!p[1] ||
                        !strcmp(&(p[1]), "_ucs2") ||
                        !strcmp(&(p[1]), "_ucs4"))
                    {
                        return sky_codec_utf8;
                    }
                    return NULL;
                }
                if ('1' == p[0] && '6' == p[1]) {
                    if (!p[2]) {
                        return sky_codec_utf16;
                    }
                    p += ('_' == p[2] ? 3 : 2);
                    if (!strcmp(p, "be")) {
                        return sky_codec_utf16_be;
                    }
                    if (!strcmp(p, "le")) {
                        return sky_codec_utf16_le;
                    }
                    return NULL;
                }
                if ('3' == p[0] && '2' == p[1]) {
                    if (!p[2]) {
                        return sky_codec_utf32;
                    }
                    p += ('_' == p[2] ? 3 : 2);
                    if (!strcmp(p, "be")) {
                        return sky_codec_utf32_be;
                    }
                    if (!strcmp(p, "le")) {
                        return sky_codec_utf32_le;
                    }
                }
                if ('7' == p[0] && !p[1]) {
                    return sky_codec_utf7;
                }
                if (!p[0]) {
                    return sky_codec_utf8;
                }
                return NULL;
            }
            if (!strcmp(lower, "us") ||
                !strcmp(lower, "us_ascii"))
            {
                return sky_codec_ascii;
            }
            if (!strcmp(lower, "unicodeescape") ||
                !strcmp(lower, "unicode_escape"))
            {
                return sky_codec_unicode_escape;
            }
            if (!strcmp(lower, "unicodeinternal") ||
                !strcmp(lower, "unicode_internal"))
            {
                return sky_codec_unicode_internal;
            }
            if (!strcmp(lower, "u7") ||
                !strcmp(lower, "unicode_1_1_utf_7")) {
                return sky_codec_utf7;
            }
            if (!strcmp(lower, "u8")) {
                return sky_codec_utf8;
            }
            if (!strcmp(lower, "u16")) {
                return sky_codec_utf16;
            }
            if (!strcmp(lower, "u32")) {
                return sky_codec_utf32;
            }
            if (!strcmp(lower, "unicodebigunmarked")) {
                return sky_codec_utf16_be;
            }
            if (!strcmp(lower, "unicodelittleunmarked")) {
                return sky_codec_utf16_le;
            }
            return NULL;
    }

    return NULL;
}


static sky_string_t
sky_codec_normalize_encoding(sky_string_t encoding)
    /* Convert an encoding to all lower case and replace spaces and hyphens
     * with underscores.
     */
{
    uint8_t                 cp;
    const uint8_t           *c, *end, *start;
    sky_unicode_ctype_t     *ctype;
    sky_string_builder_t    builder;

    /* Encoding names must be latin-1 only. Leave as-is, and let error handling
     * for not found take care of the problem.
     */
    if (!(start = sky_string_cstring(encoding))) {
        return encoding;
    }
    end = start + sky_object_len(encoding);
    builder = sky_string_builder_createwithcapacity(end - start);

    for (c = start; c < end; ++c) {
        if (*c == ' ' || *c == '-') {
            cp = '_';
        }
        else {
            ctype = sky_unicode_ctype_lookup_latin1(*c);
            sky_error_validate_debug(!ctype->special_lower[0]);
            if (!ctype->simple_lower) {
                /* All latin-1 lowercase conversions are simple, so if there
                 * isn't a simple conversion for this codepoint, there is no
                 * conversion at all.
                 */
                continue;
            }
            cp = *c + ctype->simple_lower;
        }

        if (c > start) {
            sky_string_builder_appendcodepoints(builder, start, c - start, 1);
        }
        sky_string_builder_appendcodepoint(builder, cp, 1);
        start = c + 1;
    }
    if (start < end) {
        sky_string_builder_appendcodepoints(builder, start, end - start, 1);
    }

    return sky_string_builder_finalize(builder);
}


static SKY_NORETURN void
sky_codec_error_handler_raise_TypeError(sky_object_t exc)
{
    sky_type_t      type;
    sky_string_t    name;

    type = sky_object_getattr(exc,
                              SKY_STRING_LITERAL("__class__"),
                              sky_NotSpecified);
    name = sky_object_getattr(type,
                              SKY_STRING_LITERAL("__name__"),
                              sky_NotSpecified);
    sky_error_raise_format(sky_TypeError,
                           "don't know how to handle %@ in error callback",
                           name);
}


static sky_tuple_t
sky_codec_error_handler_backslashreplace(sky_object_t exc)
{
    char                *bytes, *out;
    size_t              incr, length;
    ssize_t             end, start;
    sky_string_t        replacement;
    sky_string_data_t   *string_data, tagged_data;
    
    if (sky_object_isa(exc, sky_UnicodeEncodeError)) {
        start = sky_UnicodeError_start(exc);
        end = sky_UnicodeError_end(exc);
        string_data = sky_string_data(sky_UnicodeError_object(exc),
                                      &tagged_data);
        if (start < 0 || start > string_data->len ||
            end < 0 || end > string_data->len)
        {
            sky_error_raise_string(sky_IndexError,
                                   "start/end positions out of bounds");
        }

        if (string_data->highest_codepoint < 0x100) {
            uint8_t *c, *endc;

            if (end - start > (SSIZE_MAX / 4)) {
                end = start + (SSIZE_MAX / 4);
            }
            length = (end - start) * 4;
            bytes = out = sky_malloc(length);
            endc = string_data->data.latin1 + end;
            for (c = string_data->data.latin1 + start; c < endc; ++c) {
                *out++ = '\\';
                *out++ = 'x';
                *out++ = sky_ctype_lower_hexdigits[((*c >> 4) & 0xF)];
                *out++ = sky_ctype_lower_hexdigits[((*c     ) & 0xF)];
            }
        }
        else if (string_data->highest_codepoint < 0x10000) {
            uint16_t    *c, *endc;

            length = 0;
            endc = string_data->data.ucs2 + end;
            for (c = string_data->data.ucs2 + start; c < endc; ++c) {
                incr = (*c < 0x100 ? 4 : 6);
                if (length > SSIZE_MAX - incr) {
                    endc = c;
                    break;
                }
                length += incr;
            }
            bytes = out = sky_malloc(length);
            for (c = string_data->data.ucs2 + start; c < endc; ++c) {
                *out++ = '\\';
                if (*c < 0x100) {
                    *out++ = 'x';
                }
                else {
                    *out++ = 'u';
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 12) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >>  8) & 0xF)];
                }
                *out++ = sky_ctype_lower_hexdigits[((*c >> 4) & 0xF)];
                *out++ = sky_ctype_lower_hexdigits[((*c     ) & 0xF)];
            }
        }
        else {
            sky_unicode_char_t  *c, *endc;

            length = 0;
            endc = string_data->data.ucs4 + end;
            for (c = string_data->data.ucs4 + start; c < endc; ++c) {
                if (*c < 0x100) {
                    incr = 4;
                }
                else if (*c < 0x10000) {
                    incr = 6;
                }
                else {
                    incr = 10;
                }
                if (length > SSIZE_MAX - incr) {
                    endc = c;
                    break;
                }
                length += incr;
            }
            bytes = out = sky_malloc(length);
            for (c = string_data->data.ucs4 + start; c < endc; ++c) {
                *out++ = '\\';
                if (*c < 0x100) {
                    *out++ = 'x';
                }
                else if (*c < 0x10000) {
                    *out++ = 'u';
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 12) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >>  8) & 0xF)];
                }
                else {
                    *out++ = 'U';
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 28) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 24) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 20) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 16) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >> 12) & 0xF)];
                    *out++ = sky_ctype_lower_hexdigits[((*c >>  8) & 0xF)];
                }
                *out++ = sky_ctype_lower_hexdigits[((*c >> 4) & 0xF)];
                *out++ = sky_ctype_lower_hexdigits[((*c     ) & 0xF)];
            }
        }
    }
    else {
        sky_codec_error_handler_raise_TypeError(exc);
    }

    replacement = sky_string_createwithcodepoints(bytes,
                                                  out - bytes,
                                                  1,
                                                  sky_free);
    return sky_tuple_pack(2, replacement, sky_integer_create(end));
}


static sky_tuple_t
sky_codec_error_handler_ignore(sky_object_t exc)
{
    if (sky_object_isa(exc, sky_UnicodeDecodeError) ||
        sky_object_isa(exc, sky_UnicodeEncodeError) ||
        sky_object_isa(exc, sky_UnicodeTranslateError))
    {
        return sky_tuple_pack(2,
                              sky_string_empty,
                              sky_integer_create(sky_UnicodeError_end(exc)));
    }

    sky_codec_error_handler_raise_TypeError(exc);
}


static sky_tuple_t
sky_codec_error_handler_replace(sky_object_t exc)
{
    ssize_t         end, start;
    sky_string_t    replacement;

    end = sky_UnicodeError_end(exc);
    if (sky_object_isa(exc, sky_UnicodeDecodeError)) {
        return sky_tuple_pack(2,
                              SKY_STRING_LITERAL("\xef\xbf\xbd"),
                              sky_integer_create(end));
    }

    if (sky_object_isa(exc, sky_UnicodeEncodeError)) {
        replacement = SKY_STRING_LITERAL("?");
    }
    else if (sky_object_isa(exc, sky_UnicodeTranslateError)) {
        replacement = SKY_STRING_LITERAL("\xef\xbf\xbd");
    }
    else {
        sky_codec_error_handler_raise_TypeError(exc);
    }

    start = sky_UnicodeError_start(exc);
    return sky_tuple_pack(2,
                          sky_string_repeat(replacement, end - start),
                          sky_integer_create(end));
}


static sky_tuple_t
sky_codec_error_handler_strict(sky_object_t exc)
{
    if (sky_object_isa(exc, sky_BaseException)) {
        sky_error_raise_object(sky_object_type(exc), exc);
    }
    else {
        sky_error_raise_string(sky_TypeError,
                               "codec must pass exception instance");
    }
}


static sky_tuple_t
sky_codec_error_handler_surrogateescape(sky_object_t exc)
{
    char                *bytes, *outb;
    size_t              length;
    ssize_t             end, i, start;
    uint8_t             *p;
    sky_buffer_t        buffer;
    sky_object_t        replacement;
    sky_string_data_t   *string_data, tagged_data;
    sky_unicode_char_t  cp, outcp[4];

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isa(exc, sky_UnicodeEncodeError)) {
            start = sky_UnicodeError_start(exc);
            end = sky_UnicodeError_end(exc);
            string_data = sky_string_data(sky_UnicodeError_object(exc),
                                          &tagged_data);
            if (start < 0 || start > string_data->len ||
                end < 0 || end > string_data->len ||
                end <= start)
            {
                sky_error_raise_string(sky_IndexError,
                                       "start/end positions out of bounds");
            }
            if (string_data->highest_codepoint < 0x100) {
                sky_error_raise_object(sky_object_type(exc), exc);
            }

            length = (end - start);
            bytes = outb = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);
            if (string_data->highest_codepoint < 0x10000) {
                for (i = start; i < end; ++i) {
                    cp = string_data->data.ucs2[i];
                    if (cp < 0xdc80 || cp > 0xdcff) {
                        sky_error_raise_object(sky_object_type(exc), exc);
                    }
                    *outb++ = cp - 0xdc00;
                }
            }
            else {
                for (i = start; i < end; ++i) {
                    cp = string_data->data.ucs4[i];
                    if (cp < 0xdc80 || cp > 0xdcff) {
                        sky_error_raise_object(sky_object_type(exc), exc);
                    }
                    *outb++ = cp - 0xdc00;
                }
            }
            replacement = sky_bytes_createwithbytes(bytes, length, sky_free);
        }
        else if (sky_object_isa(exc, sky_UnicodeDecodeError)) {
            start = sky_UnicodeError_start(exc);
            end = sky_UnicodeError_end(exc);

            sky_buffer_acquire(&buffer,
                               sky_UnicodeError_object(exc),
                               SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            if (start < 0 || start > buffer.len ||
                end < 0 || end > buffer.len ||
                end <= start)
            {
                sky_error_raise_string(sky_IndexError,
                                       "start/end positions out of bounds");
            }

            p = (uint8_t *)buffer.buf + start;
            for (i = 0; i < 4 && i < end - start; ++i) {
                if (p[i] < 128) {
                    break;
                }
                outcp[i] = 0xdc00 + p[i];
            }
            if (!i) {
                sky_error_raise_object(sky_object_type(exc), exc);
            }
            replacement = sky_string_createfromcodepoints(outcp, i * 4, 4);
            end = start + i;
        }
        else {
            sky_codec_error_handler_raise_TypeError(exc);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_tuple_pack(2, replacement, sky_integer_create(end));
}


static sky_tuple_t
sky_codec_error_handler_surrogatepass(sky_object_t exc)
{
    char                *bytes, *outb;
    size_t              length;
    ssize_t             end, i, start;
    uint8_t             *p;
    sky_buffer_t        buffer;
    sky_object_t        replacement;
    sky_string_data_t   *string_data, tagged_data;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        if (sky_object_isa(exc, sky_UnicodeEncodeError)) {
            start = sky_UnicodeError_start(exc);
            end = sky_UnicodeError_end(exc);
            string_data = sky_string_data(sky_UnicodeError_object(exc),
                                          &tagged_data);
            if (start < 0 || start > string_data->len ||
                end < 0 || end > string_data->len ||
                end <= start)
            {
                sky_error_raise_string(sky_IndexError,
                                       "start/end positions out of bounds");
            }
            if (string_data->highest_codepoint < 0x100) {
                sky_error_raise_object(sky_object_type(exc), exc);
            }

            if (end - start > (SSIZE_MAX / 3)) {
                end = start + (SSIZE_MAX / 3);
            }
            length = (end - start) * 3;
            bytes = outb = sky_asset_malloc(length, SKY_ASSET_CLEANUP_ON_ERROR);
            if (string_data->highest_codepoint < 0x10000) {
                for (i = start; i < end; ++i) {
                    cp = string_data->data.ucs2[i];
                    if (cp < 0xd800 || cp > 0xdfff) {
                        sky_error_raise_object(sky_object_type(exc), exc);
                    }
                    *outb++ = (char)(0xe0 | (cp >> 12));
                    *outb++ = (char)(0x80 | ((cp >> 6) & 0x3f));
                    *outb++ = (char)(0x80 | (cp & 0x3f));
                }
            }
            else {
                for (i = start; i < end; ++i) {
                    cp = string_data->data.ucs4[i];
                    if (cp < 0xd800 || cp > 0xdfff) {
                        sky_error_raise_object(sky_object_type(exc), exc);
                    }
                    *outb++ = (char)(0xe0 | (cp >> 12));
                    *outb++ = (char)(0x80 | ((cp >> 6) & 0x3f));
                    *outb++ = (char)(0x80 | (cp & 0x3f));
                }
            }
            replacement = sky_bytes_createwithbytes(bytes, length, sky_free);
        }
        else if (sky_object_isa(exc, sky_UnicodeDecodeError)) {
            start = sky_UnicodeError_start(exc);
            end = sky_UnicodeError_end(exc);

            sky_buffer_acquire(&buffer,
                               sky_UnicodeError_object(exc),
                               SKY_BUFFER_FLAG_SIMPLE);
            sky_asset_save(&buffer,
                           (sky_free_t)sky_buffer_release,
                           SKY_ASSET_CLEANUP_ALWAYS);
            if (start < 0 || start > buffer.len ||
                end < 0 || end > buffer.len ||
                end <= start)
            {
                sky_error_raise_string(sky_IndexError,
                                       "start/end positions out of bounds");
            }

            cp = 0;
            p = (uint8_t *)buffer.buf + start;
            if (buffer.len - start >= 3 &&
                (p[0] & 0xf0) == 0xe0 &&
                (p[1] & 0xc0) == 0x80 &&
                (p[2] & 0xc0) == 0x80)
            {
                cp = ((p[0] & 0x0f) << 12) +
                     ((p[1] & 0x3f) <<  6) +
                     ( p[2] & 0x3f);
                if (cp < 0xd800 || cp > 0xdfff) {
                    cp = 0;
                }
            }
            if (!cp) {
                sky_error_raise_object(sky_object_type(exc), exc);
            }
            replacement = sky_string_createfromcodepoint(cp);
            end = start + 3;
        }
        else {
            sky_codec_error_handler_raise_TypeError(exc);
        }
    } SKY_ASSET_BLOCK_END;

    return sky_tuple_pack(2, replacement, sky_integer_create(end));
}


static sky_tuple_t
sky_codec_error_handler_xmlcharrefreplace(sky_object_t exc)
{
    char                *bytes, *out;
    size_t              length;
    ssize_t             end, start;
    sky_string_t        replacement;
    sky_string_data_t   *string_data, tagged_data;
    
    if (sky_object_isa(exc, sky_UnicodeEncodeError)) {
        start = sky_UnicodeError_start(exc);
        end = sky_UnicodeError_end(exc);
        string_data = sky_string_data(sky_UnicodeError_object(exc),
                                      &tagged_data);
        if (start < 0 || start > string_data->len ||
            end < 0 || end > string_data->len)
        {
            sky_error_raise_string(sky_IndexError,
                                   "start/end positions out of bounds");
        }

        if (end - start > (SSIZE_MAX / 3)) {
            end = start + (SSIZE_MAX / 3);
        }
        length = (end - start) * 3;
        if (string_data->highest_codepoint < 0x100) {
            uint8_t *c, *endc;

            endc = string_data->data.latin1 + end;
            for (c = string_data->data.latin1 + start; c < endc; ++c) {
                length += sky_util_utoa(*c, NULL, 0);
            }
            bytes = out = sky_malloc(length + 1);
            for (c = string_data->data.latin1 + start; c < endc; ++c) {
                *out++ = '&';
                *out++ = '#';
                out += sky_util_utoa(*c, out, (bytes + length) - out);
                *out++ = ';';
            }
            *out = '\0';
        }
        else if (string_data->highest_codepoint < 0x10000) {
            uint16_t    *c, *endc;

            endc = string_data->data.ucs2 + end;
            for (c = string_data->data.ucs2 + start; c < endc; ++c) {
                length += sky_util_utoa(*c, NULL, 0);
            }
            bytes = out = sky_malloc(length + 1);
            for (c = string_data->data.ucs2 + start; c < endc; ++c) {
                *out++ = '&';
                *out++ = '#';
                out += sky_util_utoa(*c, out, (bytes + length) - out);
                *out++ = ';';
            }
            *out = '\0';
        }
        else {
            sky_unicode_char_t  *c, *endc;

            endc = string_data->data.ucs4 + end;
            for (c = string_data->data.ucs4 + start; c < endc; ++c) {
                length += sky_util_utoa(*c, NULL, 0);
            }
            bytes = out = sky_malloc(length + 1);
            for (c = string_data->data.ucs4 + start; c < endc; ++c) {
                *out++ = '&';
                *out++ = '#';
                out += sky_util_utoa(*c, out, (bytes + length) - out);
                *out++ = ';';
            }
            *out = '\0';
        }
    }
    else {
        sky_codec_error_handler_raise_TypeError(exc);
    }

    replacement = sky_string_createwithcodepoints(bytes,
                                                  out - bytes,
                                                  1,
                                                  sky_free);
    return sky_tuple_pack(2, replacement, sky_integer_create(end));
}


const sky_codec_t *
sky_codec_filesystemcodec(void)
{
    return sky_codec_filesystemencoding_codec;
}


sky_string_t
sky_codec_filesystemencoding(void)
{
    return sky_codec_filesystemencoding_string;
}


sky_object_t
sky_codec_lookup(sky_string_t encoding)
{
    sky_tuple_t     args;
    sky_object_t    codec, function;

    if (!encoding) {
        encoding = SKY_STRING_LITERAL("utf_8");
    }
    else if (!sky_object_isa(encoding, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "encoding must be a string; got %#@",
                               sky_type_name(sky_object_type(encoding)));
    }
    else {
        encoding = sky_codec_normalize_encoding(encoding);
    }

    if (!(codec = sky_dict_get(sky_codec_registry_cache, encoding, NULL))) {
        args = sky_tuple_pack(1, encoding);
        SKY_SEQUENCE_FOREACH(sky_codec_registry, function) {
            codec = sky_object_call(function, args, NULL);
            if (!sky_object_isnull(codec)) {
                SKY_SEQUENCE_FOREACH_BREAK;
            }
        } SKY_SEQUENCE_FOREACH_END;
        if (sky_object_isnull(codec)) {
            sky_error_raise_format(sky_LookupError,
                                   "unknown encoding: %@",
                                   encoding);
        }
        SKY_ASSET_BLOCK_BEGIN {
            sky_mutex_lock(sky_codec_registry_cache_mutex, SKY_TIME_INFINITE);
            sky_asset_save(sky_codec_registry_cache_mutex,
                           (sky_free_t)sky_mutex_unlock,
                           SKY_ASSET_CLEANUP_ALWAYS);

            sky_dict_setitem(sky_codec_registry_cache, encoding, codec);
        } SKY_ASSET_BLOCK_END;
    }

    return codec;
}


sky_object_t
sky_codec_lookup_text(sky_string_t encoding, const char *alternative)
{
    sky_object_t    attr, codec;

    codec = sky_codec_lookup(encoding);

    /* Backwards compatibility: assume any raw tuple describes a text encoding,
     * and the same for anything lacking the private attribute.
     */
    if (sky_object_type(codec) != sky_tuple_type) {
        attr = sky_object_getattr(codec,
                                  SKY_STRING_LITERAL("_is_text_encoding"),
                                  sky_True);
        if (!sky_object_bool(attr)) {
            sky_error_raise_format(sky_LookupError,
                                   "%#@ is not a text encoding; "
                                   "use %s to handle arbitrary codecs",
                                   encoding,
                                   alternative);
        }
    }
    return codec;
}


void
sky_codec_register(sky_object_t search_function)
{
    if (!sky_object_callable(search_function)) {
        sky_error_raise_string(sky_TypeError,
                               "search_function must be callable.");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_mutex_lock(sky_codec_registry_mutex, SKY_TIME_INFINITE);
        sky_asset_save(sky_codec_registry_mutex,
                       (sky_free_t)sky_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_list_append(sky_codec_registry, search_function);
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_codec_lookup_error(sky_string_t name)
{
    sky_object_t    error;

    if (!name) {
        name = SKY_STRING_LITERAL("strict");
    }
    else if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "name must be a string; got %#@",
                               sky_type_name(sky_object_type(name)));
    }
    if (!(error = sky_dict_get(sky_codec_error_registry, name, NULL))) {
        sky_error_raise_format(sky_LookupError,
                               "unknown error handler name %#@",
                               name);
    }

    return error;
}


void
sky_codec_register_error(sky_string_t name, sky_object_t error_handler)
{
    if (!sky_object_isa(name, sky_string_type)) {
        sky_error_raise_format(sky_TypeError,
                               "name must be a string; got %#@",
                               sky_type_name(sky_object_type(name)));
    }
    if (!sky_object_callable(error_handler)) {
        sky_error_raise_string(sky_TypeError,
                               "error_handler must be callable.");
    }

    SKY_ASSET_BLOCK_BEGIN {
        sky_mutex_lock(sky_codec_error_registry_mutex, SKY_TIME_INFINITE);
        sky_asset_save(sky_codec_error_registry_mutex,
                       (sky_free_t)sky_mutex_unlock,
                       SKY_ASSET_CLEANUP_ALWAYS);

        sky_dict_setitem(sky_codec_error_registry, name, error_handler);
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_codec_decoder(sky_string_t encoding)
{
    return sky_object_getattr(sky_codec_lookup(encoding),
                              SKY_STRING_LITERAL("decode"),
                              sky_NotSpecified);
}


sky_object_t
sky_codec_incremental_decoder(sky_object_t  codec,
                              sky_string_t  errors)
{
    sky_object_t    factory;

    factory = sky_object_getattr(codec,
                                 SKY_STRING_LITERAL("incrementaldecoder"),
                                 sky_NotSpecified);

    if (!errors) {
        errors = SKY_STRING_LITERAL("strict");
    }
    return sky_object_call(factory, sky_tuple_pack(1, errors), NULL);
}


sky_object_t
sky_codec_text_decoder(sky_string_t encoding)
{
    sky_object_t    codec;

    codec = sky_codec_lookup_text(encoding, "codecs.decode()");
    return sky_tuple_get(codec, 1);
}


sky_object_t
sky_codec_encoder(sky_string_t encoding)
{
    return sky_object_getattr(sky_codec_lookup(encoding),
                              SKY_STRING_LITERAL("encode"),
                              sky_NotSpecified);
}


sky_object_t
sky_codec_incremental_encoder(sky_object_t  codec,
                              sky_string_t  errors)
{
    sky_object_t    factory;

    factory = sky_object_getattr(codec,
                                 SKY_STRING_LITERAL("incrementalencoder"),
                                 sky_NotSpecified);

    if (!errors) {
        errors = SKY_STRING_LITERAL("strict");
    }
    return sky_object_call(factory, sky_tuple_pack(1, errors), NULL);
}


sky_object_t
sky_codec_text_encoder(sky_string_t encoding)
{
    sky_object_t    codec;

    codec = sky_codec_lookup_text(encoding, "codecs.encode()");
    return sky_tuple_get(codec, 0);
}


void
sky_codec_initialize_library(SKY_UNUSED unsigned int flags)
{
    extern void sky_codec_charmap_initialize_library(void);

    const char  *fsencoding;

    sky_codec_charmap_initialize_library();

    sky_error_validate(!sky_codec_registry_cache);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_registry_cache),
            sky_dict_create(),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_registry_cache_mutex),
            sky_mutex_create(SKY_FALSE, SKY_TRUE),
            SKY_TRUE);

    sky_error_validate(!sky_codec_registry);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_registry),
            sky_list_create(NULL),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_registry_mutex),
            sky_mutex_create(SKY_FALSE, SKY_TRUE),
            SKY_TRUE);

    sky_error_validate(!sky_codec_error_registry);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_error_registry),
            sky_dict_create(),
            SKY_TRUE);
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_error_registry_mutex),
            sky_mutex_create(SKY_FALSE, SKY_TRUE),
            SKY_TRUE);

    sky_codec_register_error(
            SKY_STRING_LITERAL("backslashreplace"),
            sky_function_createbuiltin(
                    "codecs.backslashreplace_errors",
                    "Implements the 'backslashreplace' error handling, which replaces an unencodable character with a backslashed escape sequence.",
                    (sky_native_code_function_t)sky_codec_error_handler_backslashreplace,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("ignore"),
            sky_function_createbuiltin(
                    "codecs.ignore_errors",
                    "Implements the 'ignore' error handling, which ignores malformed data and continues.",
                    (sky_native_code_function_t)sky_codec_error_handler_ignore,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("replace"),
            sky_function_createbuiltin(
                    "codecs.replace_errors",
                    "Implements the 'replace' error handling, which replaces malformed data with a replacement marker.",
                    (sky_native_code_function_t)sky_codec_error_handler_replace,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("strict"),
            sky_function_createbuiltin(
                    "codecs.strict_errors",
                    "Implements the 'strict' error handling, which raises a UnicodeError on coding errors.",
                    (sky_native_code_function_t)sky_codec_error_handler_strict,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("surrogateescape"),
            sky_function_createbuiltin(
                    "codecs.surrogateescape",
                    NULL,
                    (sky_native_code_function_t)sky_codec_error_handler_surrogateescape,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("surrogatepass"),
            sky_function_createbuiltin(
                    "codecs.surrogatepass",
                    NULL,
                    (sky_native_code_function_t)sky_codec_error_handler_surrogatepass,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_codec_register_error(
            SKY_STRING_LITERAL("xmlcharrefreplace"),
            sky_function_createbuiltin(
                    "codecs.xmlcharrefreplace",
                    "Implements the 'xmlcharrefreplace' error handling, which replaces an unencodable character with the appropriate XML character reference.",
                    (sky_native_code_function_t)sky_codec_error_handler_xmlcharrefreplace,
                    SKY_DATA_TYPE_OBJECT_TUPLE,
                    "exc", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    fsencoding = sky_system_filesystemencoding();
    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_filesystemencoding_string),
            sky_string_createfrombytes(fsencoding,
                                       strlen(fsencoding),
                                       NULL,
                                       NULL),
            SKY_TRUE);
    sky_codec_filesystemencoding_codec =
            sky_codec_builtin(sky_codec_filesystemencoding_string);

    sky_object_gc_setlibraryroot(
            SKY_AS_OBJECTP(&sky_codec_filesystemerrors_string),
            SKY_STRING_LITERAL("surrogateescape"),
            SKY_TRUE);
}
