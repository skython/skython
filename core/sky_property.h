/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_property Properties
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_PROPERTY_H__
#define __SKYTHON_CORE_SKY_PROPERTY_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new property object instance.
  *
  * @param[in]  fget    the callable to be used for getting an attribute value.
  * @param[in]  fset    the callable to be used for setting an attribute value.
  * @param[in]  fdel    the callable to be used for deleting an attribute value.
  * @param[in]  doc     the documentation string to use.
  * @return     a new property object instance.
  */
SKY_EXTERN sky_property_t
sky_property_create(sky_object_t    fget,
                    sky_object_t    fset,
                    sky_object_t    fdel,
                    const char *    doc);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_PROPERTY_H__ */

/** @} **/
/** @} **/
