#include "sky_private.h"


typedef struct sky_property_data_s {
    sky_object_t                        fget;
    sky_object_t                        fset;
    sky_object_t                        fdel;
    sky_object_t                        doc;
} sky_property_data_t;

SKY_EXTERN_INLINE sky_property_data_t *
sky_property_data(sky_object_t object)
{
    return sky_object_data(object, sky_property_type);
}

struct sky_property_s {
    sky_object_data_t                   object_data;
    sky_property_data_t                 property_data;
};

static void
sky_property_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_property_data_t *self_data = data;

    sky_object_visit(self_data->fget, visit_data);
    sky_object_visit(self_data->fset, visit_data);
    sky_object_visit(self_data->fdel, visit_data);
    sky_object_visit(self_data->doc, visit_data);
}


static sky_property_t
sky_property_copy(sky_property_t    self,
                  sky_object_t      get,
                  sky_object_t      set,
                  sky_object_t      del)
{
    sky_property_data_t *self_data = sky_property_data(self);

    sky_object_t    doc;

    if (sky_object_isnull(get)) {
        get = self_data->fget;
    }
    if (sky_object_isnull(set)) {
        set = self_data->fset;
    }
    if (sky_object_isnull(del)) {
        del = self_data->fdel;
    }
    if (!sky_object_isnull(self_data->doc) && !sky_object_isnull(get)) {
        doc = self_data->doc;
    }
    else {
        doc = sky_None;
    }

    return sky_object_call(sky_object_type(self),
                           sky_tuple_pack(4, get, set, del, doc),
                           NULL);
}

sky_property_t
sky_property_deleter(sky_property_t self, sky_object_t deleter)
{
    return sky_property_copy(self, NULL, NULL, deleter);
}

sky_property_t
sky_property_getter(sky_property_t self, sky_object_t getter)
{
    return sky_property_copy(self, getter, NULL, NULL);
}

sky_property_t
sky_property_setter(sky_property_t self, sky_object_t setter)
{
    return sky_property_copy(self, NULL, setter, NULL);
}


static void
sky_property_delete(sky_property_t self, sky_object_t instance)
{
    sky_property_data_t *self_data = sky_property_data(self);

    if (sky_object_isnull(self_data->fdel)) {
        sky_error_raise_string(sky_AttributeError, "can't delete attribute");
    }
    sky_object_call(self_data->fdel, sky_tuple_pack(1, instance), NULL);
}


sky_object_t
sky_property_get(           sky_property_t self,
                            sky_object_t   instance,
                 SKY_UNUSED sky_type_t     type)
{
    sky_property_data_t *self_data = sky_property_data(self);

    if (sky_object_isnull(instance)) {
        return self;
    }
    if (sky_object_isnull(self_data->fget)) {
        sky_error_raise_string(sky_AttributeError, "unreadable attribute");
    }
    return sky_object_call(self_data->fget, sky_tuple_pack(1, instance), NULL);
}


void
sky_property_init(sky_property_t    self,
                  sky_object_t      fget,
                  sky_object_t      fset,
                  sky_object_t      fdel,
                  sky_object_t      doc)
{
    sky_property_data_t *self_data = sky_property_data(self);

    if (sky_object_isnull(doc) && !sky_object_isnull(fget)) {
        doc = sky_object_getattr(fget, SKY_STRING_LITERAL("__doc__"), NULL);
    }

    sky_object_gc_set(&(self_data->fget), fget, self);
    sky_object_gc_set(&(self_data->fset), fset, self);
    sky_object_gc_set(&(self_data->fdel), fdel, self);
    sky_object_gc_set(&(self_data->doc), doc, self);
}


static sky_object_t
sky_property_isabstractmethod_getter(
                    sky_object_t    self,
        SKY_UNUSED  sky_type_t      type)
{
    sky_property_data_t *self_data = sky_property_data(self);

    sky_object_t    isabstract;
    sky_string_t    name;

    name = SKY_STRING_LITERAL("__isabstractmethod__");
    if (!sky_object_isnull(self_data->fget)) {
        isabstract = sky_object_getattr(self_data->fget, name, NULL);
        if (sky_object_bool(isabstract)) {
            return sky_True;
        }
    }
    if (!sky_object_isnull(self_data->fset)) {
        isabstract = sky_object_getattr(self_data->fset, name, NULL);
        if (sky_object_bool(isabstract)) {
            return sky_True;
        }
    }
    if (!sky_object_isnull(self_data->fdel)) {
        isabstract = sky_object_getattr(self_data->fdel, name, NULL);
        if (sky_object_bool(isabstract)) {
            return sky_True;
        }
    }

    return sky_False;
}


void
sky_property_set(sky_property_t self, sky_object_t instance, sky_object_t value)
{
    sky_property_data_t *self_data = sky_property_data(self);

    if (sky_object_isnull(self_data->fset)) {
        sky_error_raise_string(sky_AttributeError, "can't set attribute");
    }
    sky_object_call(self_data->fset, sky_tuple_pack(2, instance, value), NULL);
}


static const char sky_property_type_doc[] =
"property(fget=None, fset=None, fdel=None, doc=None) -> property attribute\n"
"\n"
"fget is a function to be used for getting an attribute value, and likewise\n"
"fset is a function for setting, and fdel a function for del'ing, an\n"
"attribute.  Typical use is to define a managed attribute x:\n\n"
"class C(object):\n"
"    def getx(self): return self._x\n"
"    def setx(self, value): self._x = value\n"
"    def delx(self): del self._x\n"
"    x = property(getx, setx, delx, \"I'm the 'x' property.\")\n"
"\n"
"Decorators make defining new properties or modifying existing ones easy:\n\n"
"class C(object):\n"
"    @property\n"
"    def x(self):\n"
"        \"I am the 'x' property.\"\n"
"        return self._x\n"
"    @x.setter\n"
"    def x(self, value):\n"
"        self._x = value\n"
"    @x.deleter\n"
"    def x(self):\n"
"        del self._x\n";


SKY_TYPE_DEFINE_SIMPLE(property,
                       "property",
                       sizeof(sky_property_data_t),
                       NULL,
                       NULL,
                       sky_property_instance_visit,
                       0,
                       sky_property_type_doc);


void
sky_property_initialize_library(void)
{
    sky_type_initialize_builtin(sky_property_type, 0);

    sky_type_setmembers(sky_property_type,
            "fget",     NULL, offsetof(sky_property_data_t, fget),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "fset",     NULL, offsetof(sky_property_data_t, fset),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "fdel",     NULL, offsetof(sky_property_data_t, fdel),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            "__doc__",  NULL, offsetof(sky_property_data_t, doc),
                        SKY_DATA_TYPE_OBJECT, SKY_MEMBER_FLAG_READONLY,
            NULL);
    sky_type_setattr_getset(
            sky_property_type,
            "__isabstractmethod__",
            NULL,
            sky_property_isabstractmethod_getter,
            NULL);

    sky_type_setattr_builtin(
            sky_property_type,
            "deleter",
            sky_function_createbuiltin(
                    "property.deleter",
                    "Descriptor to change the deleter on a property.",
                    (sky_native_code_function_t)sky_property_deleter,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_property_type,
                    "deleter", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_property_type,
            "getter",
            sky_function_createbuiltin(
                    "property.getter",
                    "Descriptor to change the getter on a property.",
                    (sky_native_code_function_t)sky_property_getter,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_property_type,
                    "deleter", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));
    sky_type_setattr_builtin(
            sky_property_type,
            "setter",
            sky_function_createbuiltin(
                    "property.setter",
                    "Descriptor to change the setter on a property.",
                    (sky_native_code_function_t)sky_property_setter,
                    SKY_DATA_TYPE_OBJECT,
                    "self", SKY_DATA_TYPE_OBJECT_INSTANCEOF, NULL, sky_property_type,
                    "deleter", SKY_DATA_TYPE_OBJECT, NULL,
                    NULL));

    sky_type_setmethodslotwithparameters(
            sky_property_type,
            "__init__",
            (sky_native_code_function_t)sky_property_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "fget", SKY_DATA_TYPE_OBJECT, sky_None,
            "fset", SKY_DATA_TYPE_OBJECT, sky_None,
            "fdel", SKY_DATA_TYPE_OBJECT, sky_None,
            "doc", SKY_DATA_TYPE_OBJECT, sky_None,
            NULL);

    sky_type_setmethodslots(sky_property_type,
            "__get__", sky_property_get,
            "__set__", sky_property_set,
            "__delete__", sky_property_delete,
            NULL);
}
