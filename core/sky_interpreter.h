/** @file
  * @brief
  * @defgroup sky_interpreter Bytecode Interpreter State
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_INTERPRETER_H__
#define __SKYTHON_CORE_SKY_INTERPRETER_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Return the value of argc as passed to sky_library_initialize(). **/
SKY_EXTERN int
sky_interpreter_argc(void);


/** Return the value of argv as passed to sky_library_initialize(). **/
SKY_EXTERN char **
sky_interpreter_argv(void);


/** Return the value of argv0 as passed to sky_library_initialize(). **/
SKY_EXTERN const char *
sky_interpreter_argv0(void);


/** Return the path and filename of the running executable. **/
SKY_EXTERN sky_string_t
sky_interpreter_executable(void);


/** Return the path prefix for platform-specific executables. **/
SKY_EXTERN sky_string_t
sky_interpreter_exec_prefix(void);


/** Return the path prefix for platform-independent files. **/
SKY_EXTERN sky_string_t
sky_interpreter_prefix(void);


/** Return the list of paths in which to look for importing modules. **/
SKY_EXTERN sky_list_t
sky_interpreter_path(void);


/** Return the default optimization setting for the interpreter.
  * Valid optimization settings are: 0 for no optimization (@c __debug__ is
  * True), 1 to remove asserts (@c __debug__ is False), and 2 to remove asserts
  * and docstrings (@c __debug__ is False).
  */
SKY_EXTERN int
sky_interpreter_optlevel(void);

/** Set the default optimiztion setting for the interpreter.
  * Valid optimization settings are: 0 for no optimization (@c __debug__ is
  * True), 1 to remove asserts (@c __debug__ is False), and 2 to remove asserts
  * and docstrings (@c __debug__ is False).
  */
SKY_EXTERN void
sky_interpreter_setoptlevel(int value);


/** Add a warning option.
  * Warning options should be added before calling sky_library_initialize();
  * otherwise, they will have no effect. Warning options are exposed to code
  * via the sys module's warnoptions list, and they are processed during
  * initialization by the warning module.
  *
  * @param[in]  option      the warning option to add.
  */
SKY_EXTERN void
sky_interpreter_addwarnoption(const char *option);

/** Return the list of warning options that have been set. **/
SKY_EXTERN const char * const *
sky_interpreter_warnoptions(void);


/** Add a implementation-specific 'X' option.
  * 'X' options should be added before calling sky_library_initialize();
  * otherwise, they will have no effect. 'X' options are exposed to code
  * via the sys module's _xoptions dictionary.
  *
  * @param[in]  option      the 'X' option to add.
  */
SKY_EXTERN void
sky_interpreter_addxoption(const char *option);

/** Return the list of warning options that have been set. **/
SKY_EXTERN const char * const *
sky_interpreter_xoptions(void);


/** Return the dlopen flags in effect for dynamically loading modules.
  */
SKY_EXTERN int
sky_interpreter_dlopenflags(void);

/** Set the dlopen flags to use for dynamically loading modules.
  */
SKY_EXTERN void
sky_interpreter_setdlopenflags(int value);


/** Return the recursion limit in effect for the bytecode interpreter.
  */
SKY_EXTERN ssize_t
sky_interpreter_recursionlimit(void);

/** Set the recursion limit for the bytecode interpreter.
  */
SKY_EXTERN void
sky_interpreter_setrecursionlimit(ssize_t value);


/** Return the dictionary of builtins.
  * The dictionary will returned will be taken from the calling frame if there
  * is one; otherwise, it will be the dictionary for the builtins module as
  * returned by sky_interpreter_builtins().
  */
SKY_EXTERN sky_object_t
sky_interpreter_builtins(void);


/** Return the flags that were used to initialize the library.
  */
SKY_EXTERN unsigned int
sky_interpreter_flags(void);


/** Return the dictionary containing the current scope's global variables.
  * The dictionary may be modified to modify the scope's global variables.
  */
SKY_EXTERN sky_dict_t
sky_interpreter_globals(void);


/** Return the mapping containing the current scope's local variables.
  * Modifying the dictionary that is returned may not necessarily modify the
  * scope's local variables. In most cases, it will not.
  */
SKY_EXTERN sky_object_t
sky_interpreter_locals(void);


/** Return the builtins module.
  * The reference returned is the builtins module that was created when the
  * interpreter was initialize. This function is not affected by changes that
  * may be made to globals.
  */
SKY_EXTERN sky_module_t
sky_interpreter_module_builtins(void);

/** Return the __main__ module.
  * The reference returned is the __main__ module that was created when the
  * interpreter was initialize. This function is not affected by changes that
  * may be made to globals.
  */
SKY_EXTERN sky_module_t
sky_interpreter_module_main(void);

/** Return the sys module.
  * The reference returned is the sys module that was created when the
  * interpreter was initialize. This function is not affected by changes that
  * may be made to globals.
  */
SKY_EXTERN sky_module_t
sky_interpreter_module_sys(void);


/** Return a dictionary containing all of the modules that have been loaded.
  * The dictionary is keyed by absolute module names, and each value is the
  * module object that corresponds to the name.
  */
SKY_EXTERN sky_dict_t
sky_interpreter_modules(void);


/** Return the object that implements the module import machinery.
  * Skython normally contains a basic bootstrap loader for importing modules,
  * but that is quickly replaced with a proper module import implementation
  * that is actually rather complicated, and so it is written in Python.
  */
SKY_EXTERN sky_object_t
sky_interpreter_importlib(void);


/** Add a module to the interpreter list of known modules.
  *
  * @param[in]  module      the module to be registered.
  */
SKY_EXTERN void
sky_interpreter_addmodule(sky_module_t module);


/** Return the interpreter trace function set for the calling thread.
  * Each thread may set a trace function that will be called during interpreter
  * execution for various events. See sky_interpreter_settrace() for more
  * information.
  *
  * @return     the trace function that is to be called, or @c NULL if none is
  *             set.
  */
SKY_EXTERN sky_object_t
sky_interpreter_gettrace(void);


/** Set the interpreter trace function for the calling thread.
  * Each thread may set a trace function that will be called during interpreter
  * execution for various events. Trace functions should have three arguments:
  * @c frame, @c event, and @c arg. When the trace function is called, @c frame
  * is current stack frame, @c event is a string that describes the event that
  * is occurring, and @c arg is dependent upon @c event. Events may be one of
  * the following:
  *
  * @li @c 'call'           a function is called (or some other code block is
  *                         entered). The global trace function is called with
  *                         @c arg as @c None. The return value specifies the
  *                         local trace function to use for the frame, or
  *                         @c sky_None if the frame should not be traced.
  * @li @c 'line'           the interpreter is about to execute a new line of
  *                         code. The local trace function is called with
  *                         @c arg as @c None. The return value specifies the
  *                         new local trace function.
  * @li @c 'return'         a function (or other code block) is about to return.
  *                         The local trace function is called with @c arg as
  *                         the value to be returned. The return value is
  *                         ignored.
  * @li @c 'exception'      an exception has occurred. The local trace function
  *                         is called with @c arg as the tuple (@c exception,
  *                         @c value, @c traceback). The return value specifies
  *                         the new local trace function.
  *
  * @param[in]  function    the function to set, which may be @c NULL or
  *                         @c sky_None to clear it.
  */
SKY_EXTERN void
sky_interpreter_settrace(sky_object_t function);


/** Run the interpreter to evaluate a bytecode expression.
  *
  * @param[in]  source      the source to evaluate, which may be a code or
  *                         string object. It may also be any object that
  *                         supports the buffer protocol, such as a bytes
  *                         or bytearray object.
  * @param[in]  globals     the global variables to use. If @c NULL, the
  *                         interpreter's notion of global variables will be
  *                         used (see sky_interpreter_globals()).
  * @param[in]  locals      the local variables to use. If @c NULL, the
  *                         interpreter's notion of local variables will be
  *                         used, which will be sky_interpreter_locals() if
  *                         @a globals is also @c NULL; otherwise, @a globals
  *                         will be used. The object type should be a mapping.
  * @return     the result of the evaluation, which shall never be @c NULL.
  */
SKY_EXTERN sky_object_t
sky_interpreter_eval(sky_object_t   source,
                     sky_dict_t     globals,
                     sky_object_t   locals);


/** Run the interpreter to execute bytecode statements.
  *
  * @param[in]  source      the source to evaluate, which may be a code or
  *                         string object. It may also be any object that
  *                         supports the buffer protocol, such as a bytes
  *                         or bytearray object.
  * @param[in]  globals     the global variables to use. If @c NULL, the
  *                         interpreter's notion of global variables will be
  *                         used (see sky_interpreter_globals()).
  * @param[in]  locals      the local variables to use. If @c NULL, the
  *                         interpreter's notion of local variables will be
  *                         used, which will be sky_interpreter_locals() if
  *                         @a globals is also @c NULL; otherwise, @a globals
  *                         will be used. The object type should be a mapping.
  */
SKY_EXTERN void
sky_interpreter_exec(sky_object_t   source,
                     sky_dict_t     globals,
                     sky_object_t   locals);


/** Run the interpreter in interactive mode for a single input.
  *
  * @param[in]  flags       flags to pass to the lexer/parser/compiler.
  * @return     @c SKY_TRUE if EOF has been reached.
  */
SKY_EXTERN sky_bool_t
sky_interpreter_interactive(unsigned int flags);


/** Run the interpreter in interactive mode until EOF is reached.
  *
  * @param[in]  flags       flags to pass to the lexer/parser/compiler.
  */
SKY_EXTERN void
sky_interpreter_interactive_loop(unsigned int flags);



/** The signature of a function called to obtain a line of input.
  *
  * @param[in]  prompt      the prompt to display before accepting input.
  * @return     the input, which shall contain a trailing newline character. If
  *             EOF is reached, the empty string shall be returned.
  */
typedef sky_string_t (*sky_interpreter_input_t)(sky_string_t prompt);

/** Return the current input hook function. **/
SKY_EXTERN sky_interpreter_input_t
sky_interpreter_inputhook(void);

/** Set the input hook function. **/
SKY_EXTERN sky_interpreter_input_t
sky_interpreter_setinputhook(sky_interpreter_input_t hook);

/** Obtain a line of input by calling the input hook function.
  *
  * @param[in]  prompt      the prompt to display before accepting input.
  * @return     the input, which will contain a trailing newline character. If
  *             EOF is reached, the empty string will be returned.
  */
SKY_EXTERN sky_string_t
sky_interpreter_input(sky_string_t prompt);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_INTERPRETER_H__ */

/** @} **/
