#include "sky_private.h"


typedef struct sky_object_gc_library_root_ref_s sky_object_gc_library_root_ref_t;
struct sky_object_gc_library_root_ref_s {
    sky_object_gc_library_root_ref_t *  next;
    void **                             pointer;
};

typedef struct sky_object_gc_library_root_s {
    sky_hashtable_item_t                base;
    sky_object_t                        object;         /* weakref */
    sky_object_gc_library_root_ref_t *  refs;
} sky_object_gc_library_root_t;


static volatile uint32_t                sky_object_gc_threshold  = 1000000;
static volatile uint32_t                sky_object_gc_epoch      = 2;
static volatile uintmax_t               sky_object_gc_completed  = 0;
static volatile sky_bool_t              sky_object_gc_enabled    = SKY_TRUE;
static volatile sky_bool_t              sky_object_gc_finalizing = SKY_FALSE;
static volatile sky_bool_t              sky_object_gc_threaded   = SKY_FALSE;
static volatile sky_object_gc_mode_t    sky_object_gc_mode       = SKY_OBJECT_GC_MODE_IDLE;

static volatile int32_t     sky_object_gc_active_count;
static volatile int32_t     sky_object_gc_orphan_count;
static volatile int32_t     sky_object_gc_waiting_count;
static volatile sky_bool_t  sky_object_gc_wokenup;
static sky_atomic_lifo_t    sky_object_gc_root_list
                = SKY_ATOMIC_LIFO_INITIALIZER(offsetof(sky_object_map_t, next));
static sky_object_map_t *   sky_object_gc_roots;
static sky_pointer_set_t *  sky_object_gc_store_list;
static sky_pointer_set_t *  sky_object_gc_weakref_set;

/* Normally library roots are set during library initialization so there's no
 * inter-thread nonsense going on, but it's legitimate for there to be a root
 * set later, so some protection is needed. The use of a spinlock is simplest
 * since there should generally be near zero contention.
 */
static sky_spinlock_t       sky_object_gc_library_roots_lock
                = SKY_SPINLOCK_INITIALIZER;
static sky_hashtable_t      sky_object_gc_library_roots;

static pthread_t            sky_object_gc_thread;
static volatile int32_t     sky_object_gc_thread_running;
static volatile sky_bool_t  sky_object_gc_thread_shutdown;
static pthread_cond_t       sky_object_gc_thread_cond
                = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t      sky_object_gc_thread_mutex
                = PTHREAD_MUTEX_INITIALIZER;


uintmax_t
sky_object_gc_count(void)
{
    return sky_object_gc_completed;
}


void
sky_object_gc_disable(void)
{
    sky_object_gc_enabled = SKY_FALSE;
}


void
sky_object_gc_enable(void)
{
    sky_object_gc_enabled = SKY_TRUE;
}


sky_bool_t
sky_object_gc_isenabled(void)
{
    return sky_object_gc_enabled;
}


sky_bool_t
sky_object_gc_isthreaded(void)
{
    return sky_object_gc_threaded;
}


#if !defined(NDEBUG)
static inline sky_bool_t
sky_object_gc_isroot(sky_object_t object)
{
    return sky_object_map_contains(sky_object_gc_roots, object);
}
#endif


static void
sky_object_gc_mark_roots(void)
{
    sky_object_map_t    *map;

    /* Apply all of the per-thread root delta maps to the global root map. */
    while ((map = sky_atomic_lifo_pop(&sky_object_gc_root_list)) != NULL) {
        sky_object_map_combine(sky_object_gc_roots, map);
        sky_object_map_destroy(map);
    }

    /* Iterate over the global root list, marking each object. */
    sky_object_map_mark(sky_object_gc_roots, sky_object_gc_epoch);
}


static void
sky_object_gc_mark_stores(void)
{
    void                *pointer;
    sky_pointer_set_t   *store_set;

    while ((store_set = sky_object_gc_store_list) != NULL) {
        sky_object_gc_store_list = store_set->next;
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);

        while ((pointer = sky_pointer_set_pop_pointer(store_set)) != NULL) {
            sky_object_gc_mark(pointer, sky_object_gc_epoch);
        }
        sky_pointer_set_destroy(store_set);

        pthread_mutex_lock(&sky_object_gc_thread_mutex);
    }
}


static sky_bool_t
sky_object_gc_library_root_compare(sky_hashtable_item_t *item, void *key)
{
    return (((sky_object_gc_library_root_t *)item)->object == key);
}

SKY_EXTERN_INLINE uintptr_t
sky_object_gc_library_root_hash(void *key)
{
    return (uintptr_t)key >> 4;
}


static void
sky_object_gc_library_root_free(void *pointer)
{
    sky_object_gc_library_root_t    *item = pointer;

    sky_object_gc_library_root_ref_t    *ref;

    if (pointer) {
        while ((ref = item->refs) != NULL) {
            item->refs = ref->next;
            sky_free(ref);
        }
        sky_free(pointer);
    }
}


static void
sky_object_gc_library_root_insert(sky_object_t object, void **pointer)
{
    sky_object_gc_library_root_t        *new_item, *old_item;
    sky_object_gc_library_root_ref_t    *ref;

    ref = sky_malloc(sizeof(sky_object_gc_library_root_ref_t));
    ref->next = NULL;
    ref->pointer = pointer;

    new_item = sky_calloc(1, sizeof(sky_object_gc_library_root_t));
    new_item->object = object;
    new_item->refs = ref;

    old_item = (sky_object_gc_library_root_t *)
               sky_hashtable_insert(&sky_object_gc_library_roots,
                                    &(new_item->base),
                                    sky_object_gc_library_root_hash(object),
                                    object);
    if (old_item == new_item) {
        SKY_OBJECT_DATA(object)->libraryroot = 1;
    }
    else {
        sky_free(new_item);
        if (!old_item) {
            sky_error_fatal("library root table full");
        }
        ref->next = old_item->refs;
        old_item->refs = ref;
    }
}

static intmax_t
sky_object_gc_library_root_lookup(sky_object_t object)
{
    intmax_t                            count;
    sky_object_gc_library_root_t        *item;
    sky_object_gc_library_root_ref_t    *ref;

    if ((item = (sky_object_gc_library_root_t *)
                sky_hashtable_lookup(&sky_object_gc_library_roots,
                                     sky_object_gc_library_root_hash(object),
                                     object)) != NULL)
    {
        for (count = 0, ref = item->refs; ref; ++count, ref = ref->next);
        return count;
    }
    return 0;
}

static void
sky_object_gc_library_root_remove(sky_object_t object, void **pointer)
{
    uintptr_t                           hash;
    sky_object_gc_library_root_t        *item;
    sky_object_gc_library_root_ref_t    **prev_ref, *ref;

    hash = sky_object_gc_library_root_hash(object);
    item = (sky_object_gc_library_root_t *)
           sky_hashtable_lookup(&sky_object_gc_library_roots, hash, object);
    if (item) {
        prev_ref = &(item->refs);
        while ((ref = *prev_ref) != NULL) {
            if (ref->pointer != pointer) {
                prev_ref = &(ref->next);
            }
            else {
                *prev_ref = ref->next;
                sky_free(ref);
            }
        }
        if (!item->refs) {
            sky_hashtable_delete(&sky_object_gc_library_roots, hash, object);
            sky_free(item);
        }
    }
}

static void
sky_object_gc_library_root_wiperefs(sky_object_t object)
{
    sky_object_gc_library_root_t        *item;
    sky_object_gc_library_root_ref_t    *ref;

    if (!(item = (sky_object_gc_library_root_t *)
                 sky_hashtable_delete(&sky_object_gc_library_roots,
                                      sky_object_gc_library_root_hash(object),
                                      object)))
    {
        return;
    }
    while ((ref = item->refs) != NULL) {
        item->refs = ref->next;
        *ref->pointer = NULL;
        sky_free(ref);
    }
    sky_free(item);
}


static sky_bool_t
sky_object_gc_weakref_mark(sky_object_t             object,
                           sky_object_visit_data_t *visit_data)
{
    return !sky_pointer_set_add(visit_data->arg, object);
}


static sky_bool_t
sky_object_gc_sweep_1(sky_object_tlsdata_t *tlsdata,
                      sky_bool_t            run_destructors,
                      sky_time_t            limit)
{
    size_t                  count;
    uint32_t                sweep_epoch;
    sky_bool_t              more;
    sky_object_t            object;
    sky_object_data_t       *object_data;
    sky_object_reference_t  *ref;

    count = 0;
    sweep_epoch = tlsdata->gc_epoch - 2;
    more = SKY_FALSE;
    while ((object = tlsdata->gc_concrete_sweep.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        if (!(tlsdata->gc_concrete_sweep.head = object_data->gc_link)) {
            tlsdata->gc_concrete_sweep.tail = NULL;
        }

        sky_error_validate_debug(object_data->gc_epoch <= tlsdata->gc_epoch);
        if (object_data->gc_epoch >= sweep_epoch) {
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else if (object_data->refs && sky_object_hasrefs(object)) {
            sky_object_walk(object,
                            SKY_OBJECT_VISIT_REASON_WEAKREF_MARK,
                            sky_object_gc_weakref_mark,
                            tlsdata->gc_concrete_weakref_set);
            for (ref = object_data->refs; ref; ref = ref->next) {
                if (ref->ref) {
                    if (!run_destructors || !ref->callback) {
                        *ref->ref = NULL;
                    }
                    else {
                        SKY_ERROR_TRY {
                            ref->callback(ref->arg);
                        } SKY_ERROR_EXCEPT_ANY {
                            sky_error_display(sky_error_current());
                        } SKY_ERROR_TRY_END;
                    }
                    ref->ref = NULL;
                    ref->callback = NULL;
                    ref->arg = NULL;
                }
            }
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            sky_object_list_prepend(&(tlsdata->gc_concrete_dying), object);
        }
        if (!(++count % 100) && sky_time_current() >= limit) {
            more = tlsdata->gc_concrete_sweep.head != NULL;
            break;
        }
    }

    return more;
}


static sky_bool_t
sky_object_gc_sweep_2(sky_object_tlsdata_t *tlsdata,
                      sky_bool_t            run_destructors,
                      sky_time_t            limit,
                      sky_pointer_set_t *   weakref_set)
{
    size_t              count;
    sky_bool_t          more, swept;
    sky_object_t        object;
    sky_object_data_t   *object_data;

    count = 0;
    more = swept = SKY_FALSE;
    while ((object = tlsdata->gc_concrete_dying.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        if (!(tlsdata->gc_concrete_dying.head = object_data->gc_link)) {
            tlsdata->gc_concrete_dying.tail = NULL;
        }

        if (sky_pointer_set_contains(weakref_set, object)) {
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            swept = SKY_TRUE;
            object_data->global = object_data->concrete = 0;
            object_data->dead = 1;
            if (run_destructors &&
                SKY_OBJECT_METHOD_SLOT(object, DEL) &&
                !object_data->deleted)
            {
                sky_object_list_prepend(
                        &(tlsdata->gc_concrete_del),
                        object);
            }
            else {
                sky_object_list_prepend(
                        &(tlsdata->gc_concrete_finalize),
                        object);
            }
        }
        if (!(++count % 100) && sky_time_current() >= limit) {
            more = tlsdata->gc_concrete_dying.head != NULL;
            break;
        }
    }

    return (SKY_TIME_INFINITE == limit ? swept : more);
}


static sky_bool_t
sky_object_gc_sweep_3(sky_object_tlsdata_t *tlsdata,
                      sky_bool_t            run_destructors,
                      sky_time_t            limit)
{
    size_t              count;
    sky_bool_t          more;
    sky_object_t        object;
    sky_object_data_t   *object_data;

    /* Call __del__() methods for those objects that have them. Note that this
     * can cause resurrection of objects, so each object's dead state must be
     * checked before calling its __del__() method.
     */
    count = 0;
    more = SKY_FALSE;
    while ((object = tlsdata->gc_concrete_del.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        if (!(tlsdata->gc_concrete_del.head = object_data->gc_link)) {
            tlsdata->gc_concrete_del.tail = NULL;
        }

        if (!object_data->dead) {
            /* If the object is not dead, it's been resurrected by another
             * object's __del__() method. Move it to the list of live objects.
             */
            sky_error_validate_debug(!object_data->finalized);
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            sky_object_list_prepend(&(tlsdata->gc_concrete_finalize), object);
            if (run_destructors) {
                sky_object_del(object);
            }
        }
        if (!(++count % 100) && sky_time_current() >= limit) {
            more = tlsdata->gc_concrete_del.head != NULL;
            break;
        }
    }

#if !defined(NDEBUG)
    if (!more) {
        for (object = tlsdata->gc_concrete_finalize.head;
             object;
             object = object_data->gc_link)
        {
            object_data = SKY_OBJECT_DATA(object);
            if (object_data->dead) {
                object_data->finalizing = 1;
            }
        }
    }
#endif

    return more;
}


static sky_bool_t
sky_object_gc_sweep_4(sky_object_tlsdata_t *tlsdata,
                      sky_time_t            limit)
{
    size_t              count;
    sky_bool_t          more;
    sky_object_t        object;
    sky_object_data_t   *object_data;

    /* Run finalizers for those objects that have them. Note that objects may
     * have been previously resurrected from __del__() methods, so check for
     * that before calling a finalizer. Finalizers may not resurrect objects.
     * In general, they should never reference other objects at all. They're
     * intended to free up internal resources like dynamically allocated
     * memory.
     */
    count = 0;
    more = SKY_FALSE;
    while ((object = tlsdata->gc_concrete_finalize.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        if (!(tlsdata->gc_concrete_finalize.head = object_data->gc_link)) {
            tlsdata->gc_concrete_finalize.tail = NULL;
        }

        if (!object_data->dead) {
            /* If the object is not dead, it's been resurrected by another
             * object's __del__() method. Move it to the list of live objects.
             */
            sky_error_validate_debug(!object_data->finalized);
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            sky_object_list_prepend(&(tlsdata->gc_concrete_deallocate), object);
            if (object_data->libraryroot) {
                sky_spinlock_lock(&sky_object_gc_library_roots_lock);
                sky_object_gc_library_root_wiperefs(object);
                sky_spinlock_unlock(&sky_object_gc_library_roots_lock);
            }
            sky_object_finalize(object);
        }
        if (!(++count % 100) && sky_time_current() >= limit) {
            more = tlsdata->gc_concrete_finalize.head != NULL;
            break;
        }
    }

    return more;
}


static void
sky_object_gc_sweep_5(sky_object_tlsdata_t *tlsdata)
{
    sky_object_t        object;
    sky_object_data_t   *object_data;

    /* Make a pass over all objects marked for deallocation to ensure that
     * their 'deallocate' field has been set. The switch cannot be made above
     * because the finalizer run does not guarantee ordering, so it would be
     * possible for an object and its type object to be finalized type object
     * first. Note also that sky_type_data() cannot be used here.
     */
    for (object = tlsdata->gc_concrete_deallocate.head;
         object;
         object = object_data->gc_link)
    {
        object_data = SKY_OBJECT_DATA(object);
        sky_error_validate_debug(object_data->dead != 0);

        if (!object_data->finalized) {
            object_data->finalized = 1;
            object_data->u.deallocate =
                    object_data->u.type->type_data.deallocate;
        }
    }
}


static sky_bool_t
sky_object_gc_sweep_6(sky_object_tlsdata_t *tlsdata,
                      size_t                limit)
{
    size_t              count;
    sky_bool_t          more;
    sky_object_t        object;
    sky_object_data_t   *object_data;

    /* Deallocate any objects that need deallocating. By this time it's not
     * possible for there to be a resurrected object, because that should have
     * been handled by finalizer.
     */
    count = 0;
    more = SKY_FALSE;
    while ((object = tlsdata->gc_concrete_deallocate.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        if (!(tlsdata->gc_concrete_deallocate.head = object_data->gc_link)) {
            tlsdata->gc_concrete_deallocate.tail = NULL;
        }

        sky_error_validate_debug(object_data->dead != 0);
        sky_error_validate_debug(object_data->finalized != 0);
        object_data->u.deallocate(object);

        if (!(++count % 100) && sky_time_current() >= limit) {
            more = tlsdata->gc_concrete_deallocate.head != NULL;
            break;
        }
    }

    return more;
}


#if defined(SKY_EPHEMERAL_OBJECTS)
static void
sky_object_gc_sweep_ephemerals(sky_object_tlsdata_t *tlsdata)
{
    sky_object_t            object;
    sky_object_data_t       *object_data;
    sky_object_list_t       deallocate, del, finalize, *list, sweep,
                            weakref_callback, weakref_clear;
    sky_object_reference_t  *ref;

    if (!(sweep.head = tlsdata->gc_ephemeral_objects.head)) {
        return;
    }
    sweep.tail = tlsdata->gc_ephemeral_objects.tail;
    sky_object_list_empty(&(tlsdata->gc_ephemeral_objects));

    sky_object_list_empty(&deallocate);
    sky_object_list_empty(&weakref_clear);
    sky_object_list_empty(&weakref_callback);
    sky_object_list_empty(&del);
    sky_object_list_empty(&finalize);

    while ((object = sweep.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        sweep.head = object_data->gc_link;

        sky_error_validate_debug(!object_data->dead);
        if (object_data->concrete) {
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            object_data->dead = 1;
            if (object_data->refs && sky_object_hasrefs(object)) {
                sky_object_list_prepend(&weakref_clear, object);
            }
            else if (SKY_OBJECT_METHOD_SLOT(object, DEL)) {
                sky_object_list_prepend(&del, object);
            }
            else {
                sky_object_list_prepend(&finalize, object);
            }
        }
    }

    /* Clear all weak references first. Since ephemeral objects are local, we
     * can be sure that new weak references won't be added as they're being
     * cleared. The same goes for the object being resurrected. If there are
     * any callbacks, add the object to the weakref_callback list; otherwise,
     * add the object to the appropriate del or finalize list.
     */
    while ((object = weakref_clear.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        weakref_clear.head = object_data->gc_link;

        list = (SKY_OBJECT_METHOD_SLOT(object, DEL) ? &del : &finalize);
        for (ref = object_data->refs; ref; ref = ref->next) {
            if (ref->ref) {
                if (ref->callback) {
                    list = &weakref_callback;
                }
                *ref->ref = NULL;
                ref->ref = NULL;
            }
        }
        sky_object_list_prepend(list, object);
    }

    /* Run any weakref callbacks that there may be. There are no weak or strong
     * references to these objects any longer. Any references that may exist
     * are illegal.
     */
    while ((object = weakref_callback.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        weakref_callback.head = object_data->gc_link;

        list = (SKY_OBJECT_METHOD_SLOT(object, DEL) ? &del : &finalize);
        for (ref = object_data->refs; ref; ref = ref->next) {
            if (ref->callback) {
                sky_error_validate_debug(ref->callback != object);
                sky_error_validate_debug(ref->arg != object);
                SKY_ERROR_TRY {
                    ref->callback(ref->arg);
                } SKY_ERROR_EXCEPT_ANY {
                    sky_error_display(sky_error_current());
                } SKY_ERROR_TRY_END;
            }
        }
        sky_object_list_prepend(list, object);
    }

    while ((object = del.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        del.head = object_data->gc_link;

        if (!object_data->dead) {
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            sky_object_list_prepend(&finalize, object);
            sky_object_del(object);
        }
    }

#if !defined(NDEBUG)
    for (object = finalize.head; object; object = object_data->gc_link) {
        object_data = SKY_OBJECT_DATA(object);
        if (object_data->dead) {
            object_data->finalizing = 1;
        }
    }
#endif

    while ((object = finalize.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        finalize.head = object_data->gc_link;

        if (!object_data->dead) {
            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        }
        else {
            sky_object_list_prepend(&deallocate, object);
            sky_object_finalize(object);
        }
    }

    for (object = deallocate.head; object; object = object_data->gc_link) {
        object_data = SKY_OBJECT_DATA(object);
        sky_error_validate_debug(object_data->dead);

        if (!object_data->finalized) {
            object_data->finalized = 1;
            object_data->u.deallocate =
                    object_data->u.type->type_data.deallocate;
        }
    }

    while ((object = deallocate.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        deallocate.head = object_data->gc_link;

        object_data->u.deallocate(object);
    }
}
#endif


void
sky_object_gc_finalize_thread(sky_object_tlsdata_t *        tlsdata,
                              sky_object_tlsdata_orphan_t * orphan_tlsdata,
                              sky_bool_t                    final)
{
#if defined(SKY_EPHEMERAL_OBJECTS)
    sky_object_t        object;
    sky_object_data_t   *object_data;
#endif

#if defined(SKY_EPHEMERAL_OBJECTS)
    /* Convert any ephemeral objects that remain into concrete objects. */
    while ((object = tlsdata->gc_ephemeral_objects.head) != NULL) {
        object_data = SKY_OBJECT_DATA(object);
        tlsdata->gc_ephemeral_objects.head = object_data->gc_link;
        object_data->concrete = 1;
        sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
    }
    tlsdata->gc_ephemeral_objects.tail = NULL;
#endif

    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_objects),
                           &(tlsdata->gc_concrete_objects));
    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_sweep),
                           &(tlsdata->gc_concrete_sweep));
    if (!orphan_tlsdata->gc_concrete_weakref_set) {
        orphan_tlsdata->gc_concrete_weakref_set =
                tlsdata->gc_concrete_weakref_set;
    }
    else if (tlsdata->gc_concrete_weakref_set) {
        sky_pointer_set_merge(orphan_tlsdata->gc_concrete_weakref_set,
                              tlsdata->gc_concrete_weakref_set);
        sky_pointer_set_destroy(tlsdata->gc_concrete_weakref_set);
    }
    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_dying),
                           &(tlsdata->gc_concrete_dying));
    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_del),
                           &(tlsdata->gc_concrete_del));
    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_finalize),
                           &(tlsdata->gc_concrete_finalize));
    sky_object_list_extend(&(orphan_tlsdata->gc_concrete_deallocate),
                           &(tlsdata->gc_concrete_deallocate));
    orphan_tlsdata->gc_mode = tlsdata->gc_mode;

    if (tlsdata->gc_roots) {
        if (!sky_object_map_length(tlsdata->gc_roots)) {
            sky_object_map_destroy(tlsdata->gc_roots);
        }
        else {
            sky_atomic_lifo_push(&sky_object_gc_root_list,
                                 tlsdata->gc_roots);
        }
        tlsdata->gc_roots = NULL;
    }

    pthread_mutex_lock(&sky_object_gc_thread_mutex);
    if (tlsdata->gc_stores) {
        if (!sky_pointer_set_length(tlsdata->gc_stores)) {
            sky_pointer_set_destroy(tlsdata->gc_stores);
        }
        else {
            tlsdata->gc_stores->next = sky_object_gc_store_list;
            sky_object_gc_store_list = tlsdata->gc_stores;
        }
        tlsdata->gc_stores = NULL;
    }
    if (final && tlsdata->gc_registered) {
        tlsdata->gc_registered = 0;

        --sky_object_gc_active_count;
        ++sky_object_gc_orphan_count;

        /* Wake up the gc thread so that it can decide whether threads need
         * to be woken to adopt this thread's orphaned state.
         */
        pthread_cond_signal(&sky_object_gc_thread_cond);
    }

    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    if (final && tlsdata->gc_marker) {
        sky_object_gc_thread_running = 0;
    }
}


void
sky_object_gc_initialize_thread(sky_object_tlsdata_t *tlsdata)
{
    /* The marker thread should never be initialized as a mutator thread! */
    sky_error_validate(!tlsdata->gc_marker);

    if (!tlsdata->gc_registered) {
        tlsdata->gc_registered = 1;

        pthread_mutex_lock(&sky_object_gc_thread_mutex);
        tlsdata->gc_epoch = sky_object_gc_epoch;
        tlsdata->gc_mode = sky_object_gc_mode;
        if (++sky_object_gc_active_count > 1) {
            sky_object_gc_threaded = SKY_TRUE;
        }
        switch (sky_object_gc_mode) {
            case SKY_OBJECT_GC_MODE_IDLE:
            case SKY_OBJECT_GC_MODE_REQUESTED:
                break;
            case SKY_OBJECT_GC_MODE_STARTING:
            case SKY_OBJECT_GC_MODE_MARKING:
                tlsdata->gc_running = 1;
                break;
            case SKY_OBJECT_GC_MODE_MARK_ENDING:
            case SKY_OBJECT_GC_MODE_SWEEP_1:
            case SKY_OBJECT_GC_MODE_SWEEP_2:
            case SKY_OBJECT_GC_MODE_SWEEP_3:
            case SKY_OBJECT_GC_MODE_SWEEP_4:
            case SKY_OBJECT_GC_MODE_SWEEP_5:
            case SKY_OBJECT_GC_MODE_SWEEP_6:
                break;
        }
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    }
}


static void
sky_object_gc_thread_wakeup(SKY_UNUSED void *unused)
{
    sky_object_gc_wakeup();
}


static void
sky_object_gc_wakeup_threads(void)
{
    uint32_t                id;
    sky_tlsdata_t           *alien_tlsdata;
    sky_hazard_pointer_t    *hazard;

    for (id = 1;  id <= sky_tlsdata_highestid();  ++id) {
        if ((hazard = sky_tlsdata_getid(id)) != NULL) {
            if ((alien_tlsdata = hazard->pointer) != NULL &&
                (alien_tlsdata->flags & SKY_TLSDATA_FLAG_ALIVE) &&
                alien_tlsdata->object_tlsdata.gc_wakeup_function)
            {
                alien_tlsdata->object_tlsdata.gc_wakeup_function(
                        alien_tlsdata->object_tlsdata.gc_wakeup_arg);
            }
            sky_hazard_pointer_release(hazard);
        }
    }
}


/* Called only from sky_object_gc_thread_main() with
 * sky_object_gc_thread_mutex locked (it remains locked on return).
 */
static void
sky_object_gc_mode_set(sky_object_gc_mode_t mode)
{
    /* Both sky_object_gc_waiting_count and sky_object_gc_mode are declared
     * volatile, so the compiler will not re-order the assignments here.
     * However, the CPU could still re-order them, and it's vital that they're
     * executed in the proper order. Use store memory barriers to ensure that
     * they're ordered properly.
     */

    sky_object_gc_waiting_count = sky_object_gc_active_count;
    sky_atomic_sfence();
    sky_object_gc_mode = mode;
    sky_atomic_sfence();

    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    sky_object_gc_wakeup_threads();
    pthread_mutex_lock(&sky_object_gc_thread_mutex);
}


static inline sky_bool_t
sky_object_gc_thread_isstopping(void)
{
    return (!sky_object_gc_active_count || sky_object_gc_thread_shutdown);
}


/* Called only from sky_object_gc_thread_main() with
 * sky_object_gc_thread_mutex locked (it remains locked on return).
 */
static sky_bool_t
sky_object_gc_synchronize(void (*activity)(void))
{
    while (sky_object_gc_waiting_count > 0) {
        /* If there are orphans, tickle the gc mode so that mutator threads
         * will be woken up to adopt them. This may cause a small stampede, but
         * there isn't really a good, reliable way to limit this any better.
         */
        if (sky_object_gc_orphan_count) {
            sky_object_gc_orphan_count = 0;
            sky_object_gc_mode_set(sky_object_gc_mode);
        }
        if (!sky_object_gc_wokenup) {
            pthread_cond_wait(&sky_object_gc_thread_cond,
                              &sky_object_gc_thread_mutex);
        }
        sky_object_gc_wokenup = SKY_FALSE;
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);
        sky_atfork_check();
        pthread_mutex_lock(&sky_object_gc_thread_mutex);
        if (activity) {
            activity();
        }
        if (sky_object_gc_thread_isstopping()) {
            return SKY_FALSE;
        }
    }
    return (sky_object_gc_thread_isstopping() ? SKY_FALSE : SKY_TRUE);
}


static void *
sky_object_gc_thread_main(SKY_UNUSED void *unused)
{
    sky_tlsdata_t   *tlsdata = sky_tlsdata_get();

    int                 old_state;
    sky_pointer_set_t   *set, *weakref_set;

#if defined(HAVE_PTHREAD_SETNAME_NP)
    pthread_setname_np("sky_object_gc_thread");
#endif

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_state);

    tlsdata->object_tlsdata.gc_marker = 1;
    if (!sky_object_gc_roots) {
        sky_object_gc_roots = sky_object_map_create();
    }
    sky_atfork_register(sky_object_gc_thread_wakeup, NULL);

    pthread_mutex_lock(&sky_object_gc_thread_mutex);
    while (!sky_object_gc_thread_isstopping()) {
        switch (sky_object_gc_mode) {
            case SKY_OBJECT_GC_MODE_IDLE:
                sky_error_validate_debug(NULL == sky_object_gc_store_list);
                if (!sky_object_gc_wokenup) {
                    pthread_cond_wait(&sky_object_gc_thread_cond,
                                      &sky_object_gc_thread_mutex);
                }
                sky_object_gc_wokenup = SKY_FALSE;
                pthread_mutex_unlock(&sky_object_gc_thread_mutex);
                sky_atfork_check();
                pthread_mutex_lock(&sky_object_gc_thread_mutex);
                break;

            case SKY_OBJECT_GC_MODE_REQUESTED:
                ++sky_object_gc_epoch;
                sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_STARTING);
                break;

            case SKY_OBJECT_GC_MODE_STARTING:
                if (sky_object_gc_synchronize(NULL)) {
                    sky_object_gc_mode = SKY_OBJECT_GC_MODE_MARKING;
                }
                break;

            case SKY_OBJECT_GC_MODE_MARKING:
                pthread_mutex_unlock(&sky_object_gc_thread_mutex);
                sky_object_gc_mark_roots();
                pthread_mutex_lock(&sky_object_gc_thread_mutex);
                sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_MARK_ENDING);
                break;

            case SKY_OBJECT_GC_MODE_MARK_ENDING:
                if (sky_object_gc_synchronize(sky_object_gc_mark_stores)) {
                    sky_object_gc_mark_stores();
                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_1);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_1:
                if (sky_object_gc_synchronize(NULL)) {
                    if (!(weakref_set = sky_object_gc_weakref_set)) {
                        weakref_set = sky_pointer_set_create();
                    }
                    sky_object_gc_weakref_set = NULL;

                    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
                    while ((set = weakref_set->next) != NULL) {
                        weakref_set->next = set->next;
                        sky_pointer_set_merge(weakref_set, set);
                        sky_pointer_set_destroy(set);
                    }
                    pthread_mutex_lock(&sky_object_gc_thread_mutex);

                    sky_error_validate(!sky_object_gc_weakref_set);
                    sky_object_gc_weakref_set = weakref_set;

                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_2);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_2:
                if (sky_object_gc_synchronize(NULL)) {
                    set = sky_object_gc_weakref_set;
                    sky_object_gc_weakref_set = NULL;

                    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
                    sky_pointer_set_destroy(set);
                    pthread_mutex_lock(&sky_object_gc_thread_mutex);

                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_3);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_3:
                if (sky_object_gc_synchronize(NULL)) {
                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_4);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_4:
                if (sky_object_gc_synchronize(NULL)) {
                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_5);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_5:
                if (sky_object_gc_synchronize(NULL)) {
                    sky_object_gc_mode_set(SKY_OBJECT_GC_MODE_SWEEP_6);
                }
                break;

            case SKY_OBJECT_GC_MODE_SWEEP_6:
                if (sky_object_gc_synchronize(NULL)) {
                    ++sky_object_gc_completed;
                    sky_object_gc_mode = SKY_OBJECT_GC_MODE_IDLE;
                }
                break;
        }
    }
    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    return NULL;
}


static void
sky_object_gc_thread_check(void)
{
    /* Launch the gc thread if it's not running. */
    if (!sky_object_gc_finalizing &&
        !sky_object_gc_thread_running &&
        sky_atomic_cas32(0, 1, &sky_object_gc_thread_running))
    {
        int         pthread_rc;
#if !defined(_WIN32)
        sigset_t    oset, set;

        sigfillset(&set);
        sigdelset(&set, SIGILL);
        sigdelset(&set, SIGABRT);
        sigdelset(&set, SIGFPE);
#   if defined(SIGBUS)
        sigdelset(&set, SIGBUS);
#   endif
        sigdelset(&set, SIGSEGV);
#   if defined(SIGSYS)
        sigdelset(&set, SIGSYS);
#   endif
        pthread_sigmask(SIG_BLOCK, &set, &oset);
#endif
        pthread_rc = pthread_create(&sky_object_gc_thread, NULL,
                                    sky_object_gc_thread_main, NULL);
#if !defined(_WIN32)
        pthread_sigmask(SIG_SETMASK, &oset, NULL);
#endif
        if (pthread_rc) {
            sky_object_gc_thread_running = 0;
        }
    }
}


static void
sky_object_gc_sweep_objects(sky_object_tlsdata_t *tlsdata)
{
    /* Normally all of the sweep related lists should be empty, but if this
     * thread has adopted another thread's state, it's possible that they won't
     * be. Specifically, if this thread had already checked in STARTING, but
     * the adopted thread had not yet.
     */
    sky_object_list_extend(&(tlsdata->gc_concrete_sweep),
                           &(tlsdata->gc_concrete_objects));
    sky_object_list_empty(&(tlsdata->gc_concrete_objects));
    if (!tlsdata->gc_concrete_weakref_set) {
        tlsdata->gc_concrete_weakref_set = sky_pointer_set_create();
    }
}


static sky_bool_t
sky_object_gc_checkin(sky_object_tlsdata_t *object_tlsdata,
                      sky_object_gc_mode_t  mode,
                      void (*activity)(sky_object_tlsdata_t *))
{
    if (pthread_mutex_trylock(&sky_object_gc_thread_mutex)) {
        return SKY_TRUE;
    }
    if (activity) {
        activity(object_tlsdata);
    }
    object_tlsdata->gc_mode = mode;
    if (!--sky_object_gc_waiting_count) {
        pthread_cond_signal(&sky_object_gc_thread_cond);
    }
    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    return SKY_FALSE;
}

static void
sky_object_gc_checkin_starting(sky_object_tlsdata_t *object_tlsdata)
{
    object_tlsdata->gc_alloc_count = 0;
    object_tlsdata->gc_epoch = sky_object_gc_epoch;
    object_tlsdata->gc_running = 1;
    object_tlsdata->gc_sweep_5_done = 0;
    if (sky_object_map_length(object_tlsdata->gc_roots)) {
        sky_atomic_lifo_push(&sky_object_gc_root_list,
                             object_tlsdata->gc_roots);
        object_tlsdata->gc_roots = NULL;
    }
    sky_object_gc_sweep_objects(object_tlsdata);
}

static void
sky_object_gc_checkin_mark_ending(sky_object_tlsdata_t *object_tlsdata)
{
    object_tlsdata->gc_running = 0;
    if (sky_pointer_set_length(object_tlsdata->gc_stores)) {
        object_tlsdata->gc_stores->next = sky_object_gc_store_list;
        sky_object_gc_store_list = object_tlsdata->gc_stores;
        object_tlsdata->gc_stores = NULL;
    }
}

static void
sky_object_gc_checkin_sweep_1(sky_object_tlsdata_t *object_tlsdata)
{
    if (object_tlsdata->gc_concrete_weakref_set) {
        if (!sky_pointer_set_length(object_tlsdata->gc_concrete_weakref_set)) {
            sky_pointer_set_destroy(object_tlsdata->gc_concrete_weakref_set);
        }
        else {
            object_tlsdata->gc_concrete_weakref_set->next =
                    sky_object_gc_weakref_set;
            sky_object_gc_weakref_set =
                    object_tlsdata->gc_concrete_weakref_set;
        }
        object_tlsdata->gc_concrete_weakref_set = NULL;
    }
}


static void
sky_object_gc_cleanup(sky_object_tlsdata_t *object_tlsdata)
{
    object_tlsdata->gc_active = 0;
}

sky_bool_t
sky_object_gc(void)
{
    sky_tlsdata_t           *tlsdata        = sky_tlsdata_get();
    sky_object_tlsdata_t    *object_tlsdata = &(tlsdata->object_tlsdata);

    sky_bool_t              result, skip_checkin;
    sky_time_t              limit;
    sky_object_gc_mode_t    mode;

    result = SKY_FALSE;
    if (object_tlsdata->gc_active) {
        return SKY_FALSE;
    }

    if (sky_object_gc_finalizing && !sky_thread_ismain()) {
        sky_error_raise_object(sky_SystemExit, sky_None);
    }

    SKY_ASSET_BLOCK_BEGIN {
        object_tlsdata->gc_active = 1;
        sky_asset_save(object_tlsdata,
                       (sky_free_t)sky_object_gc_cleanup,
                       SKY_ASSET_CLEANUP_ALWAYS);

        /* Make sure that the GC thread is running. Launch it if it's not. */
        sky_object_gc_thread_check();

        /* If this thread is not currently a registered GC thread, register it
         * now. By calling sky_object_gc() at all, the thread has indicated
         * that it intends to participate in garbage collection.
         */
        if (!object_tlsdata->gc_registered) {
            sky_object_gc_initialize_thread(object_tlsdata);
        }

        /* Try to adopt an orphan. */
        while (sky_tlsdata_adopt());

#if defined(SKY_EPHEMERAL_OBJECTS)
        /* Always sweep ephemeral objects. */
        sky_object_gc_sweep_ephemerals(object_tlsdata);
#endif

        /* sky_object_gc_mode is the mode that we're trying to get to.
         * object_tlsdata->gc_mode is the mode that we've completed.
         */
        mode = sky_object_gc_mode;
        if (object_tlsdata->gc_mode != mode) {
            void (*activity)(sky_object_tlsdata_t *) = NULL;

            skip_checkin = SKY_FALSE;
            limit = sky_time_current() + (100 * 1000000);  /* 100ms */
            switch (mode) {
                case SKY_OBJECT_GC_MODE_IDLE:
                case SKY_OBJECT_GC_MODE_REQUESTED:
                case SKY_OBJECT_GC_MODE_MARKING:
                    object_tlsdata->gc_mode = mode;
                    skip_checkin = SKY_TRUE;
                    break;

                case SKY_OBJECT_GC_MODE_STARTING:
                    activity = sky_object_gc_checkin_starting;
                    break;
                case SKY_OBJECT_GC_MODE_MARK_ENDING:
                    activity = sky_object_gc_checkin_mark_ending;
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_1:
                    if (sky_object_gc_sweep_1(object_tlsdata, SKY_TRUE, limit)) {
                        skip_checkin = SKY_TRUE;
                        result = SKY_TRUE;
                    }
                    activity = sky_object_gc_checkin_sweep_1;
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_2:
                    if (sky_object_gc_sweep_2(object_tlsdata,
                                              SKY_TRUE,
                                              limit,
                                              sky_object_gc_weakref_set))
                    {
                        skip_checkin = SKY_TRUE;
                        result = SKY_TRUE;
                    }
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_3:
                    if (sky_object_gc_sweep_3(object_tlsdata, SKY_TRUE, limit)) {
                        skip_checkin = SKY_TRUE;
                        result = SKY_TRUE;
                    }
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_4:
                    if (sky_object_gc_sweep_4(object_tlsdata, limit)) {
                        skip_checkin = SKY_TRUE;
                        result = SKY_TRUE;
                    }
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_5:
                    if (!object_tlsdata->gc_sweep_5_done) {
                        sky_object_gc_sweep_5(object_tlsdata);
                        object_tlsdata->gc_sweep_5_done = 1;
                    }
                    break;
                case SKY_OBJECT_GC_MODE_SWEEP_6:
                    if (sky_object_gc_sweep_6(object_tlsdata, limit)) {
                        skip_checkin = SKY_TRUE;
                        result = SKY_TRUE;
                    }
                    break;
            }
            if (!skip_checkin &&
                sky_object_gc_checkin(object_tlsdata,
                                      mode,
                                      activity))
            {
                result = SKY_TRUE;
            }
        }
    } SKY_ASSET_BLOCK_END;

    return result;
}


void
sky_object_gc_collect(void)
{
    /* Make sure that the GC thread is running. Launch it if it's not. */
    sky_object_gc_thread_check();

    /* Only update the GC mode to REQUESTED if the GC mode is currently IDLE.
     * For any other mode, a GC cycle is already in progress.
     */
    if (sky_object_gc_mode == SKY_OBJECT_GC_MODE_IDLE) {
        pthread_mutex_lock(&sky_object_gc_thread_mutex);
        if (sky_object_gc_mode == SKY_OBJECT_GC_MODE_IDLE) {
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_REQUESTED;
            pthread_cond_signal(&sky_object_gc_thread_cond);
        }
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);
    }
}


static sky_bool_t
sky_object_gc_mark_visit(sky_object_t               object,
                         sky_object_visit_data_t *  visit_data)
{
    sky_object_data_t           *object_data = SKY_OBJECT_DATA(object);
    uint32_t                    mark = *(uint32_t *)visit_data->arg;

    if (object_data->gc_epoch < mark) {
        object_data->gc_epoch = mark;
        return SKY_TRUE;
    }

    return SKY_FALSE;
}

void
sky_object_gc_mark(sky_object_t object, uint32_t mark)
{
    sky_object_walk(object,
                    SKY_OBJECT_VISIT_REASON_GC_MARK,
                    sky_object_gc_mark_visit,
                    &mark);
}


static sky_bool_t
sky_object_gc_concrete_visit(sky_object_t               object,
                  SKY_UNUSED sky_object_visit_data_t *  visit_data)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    if (!object_data->concrete) {
        object_data->concrete = 1;
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static sky_bool_t
sky_object_gc_global_visit(sky_object_t             object,
                SKY_UNUSED sky_object_visit_data_t *visit_data)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    if (!object_data->global) {
        object_data->global = 1;
        object_data->concrete = 1;
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static sky_bool_t
sky_object_gc_resurrect_visit(sky_object_t              object,
                   SKY_UNUSED sky_object_visit_data_t * visit_data)
{
    sky_object_data_t   *object_data = SKY_OBJECT_DATA(object);

    sky_error_validate_debug(!object_data->finalizing);
    sky_error_validate_debug(!object_data->finalized);
    if (object_data->dead) {
        object_data->concrete = 1;
        object_data->dead = 0;
        return SKY_TRUE;
    }

    return SKY_FALSE;
}


void
sky_object_gc_notenew(sky_object_tlsdata_t *tlsdata,
           SKY_UNUSED sky_object_t          object)
{
#if defined(SKY_EPHEMERAL_OBJECTS)
    /* Make all objects concrete if finalization is in progress. */
    if (sky_object_gc_finalizing && !SKY_OBJECT_DATA(object)->concrete) {
        sky_error_validate_debug(tlsdata->gc_ephemeral_objects.head == object);
        sky_error_validate_debug(tlsdata->gc_ephemeral_objects.tail == object);
        sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
        tlsdata->gc_ephemeral_objects.head = NULL;
        tlsdata->gc_ephemeral_objects.tail = NULL;
        sky_object_walk(object,
                        SKY_OBJECT_VISIT_REASON_CONCRETE,
                        sky_object_gc_concrete_visit,
                        NULL);
    }
#endif

    if (++tlsdata->gc_alloc_count >= sky_object_gc_threshold &&
        sky_object_gc_threshold > 0 &&
        sky_object_gc_enabled)
    {
        tlsdata->gc_alloc_count = 0;
        sky_object_gc_collect();
    }
}


static void
sky_object_gc_notestore(sky_object_t    object,
                        sky_bool_t      global,
                        sky_bool_t      concrete)
{
    sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

    sky_object_data_t   *object_data;

    if (!tlsdata->gc_registered) {
        sky_object_gc_initialize_thread(tlsdata);
    }

    if (sky_object_istagged(object)) {
        if (!sky_object_istaggedtuple(object)) {
            return;
        }
        object = sky_object_taggedtupleobject(object);
        sky_error_validate_debug(NULL != object);
        if (sky_object_istagged(object)) {
            return;
        }
    }

    object_data = SKY_OBJECT_DATA(object);
    if (object_data->dead) {
        if (concrete) {
            sky_object_walk(object,
                            SKY_OBJECT_VISIT_REASON_RESURRECT,
                            sky_object_gc_resurrect_visit,
                            NULL);
        }
    }
    else if (global && !object_data->global) {
        sky_object_walk(object,
                        SKY_OBJECT_VISIT_REASON_GLOBAL,
                        sky_object_gc_global_visit,
                        NULL);
    }
    else if (concrete && !object_data->concrete) {
        sky_object_walk(object,
                        SKY_OBJECT_VISIT_REASON_CONCRETE,
                        sky_object_gc_concrete_visit,
                        NULL);
    }

    if (tlsdata->gc_running && object_data->gc_epoch < tlsdata->gc_epoch) {
        if (!tlsdata->gc_stores) {
            tlsdata->gc_stores = sky_pointer_set_create();
        }
        sky_pointer_set_add(tlsdata->gc_stores, object);
    }
}


sky_bool_t
sky_object_gc_cas(sky_object_t *pointer_location,
                  sky_object_t  expected_object_reference,
                  sky_object_t  new_object_reference,
                  sky_object_t  referencing_object)
{
    if (new_object_reference) {
        if (!referencing_object) {
            sky_object_gc_notestore(new_object_reference, SKY_FALSE, SKY_FALSE);
        }
        else {
            sky_object_data_t   *object_data =
                                SKY_OBJECT_DATA(referencing_object);

            sky_object_gc_notestore(new_object_reference,
                                    object_data->global,
                                    object_data->concrete);
        }
    }
    return sky_atomic_cas(expected_object_reference,
                          new_object_reference,
                          pointer_location);
}


void
sky_object_gc_set(sky_object_t *pointer_location,
                  sky_object_t  new_object_reference,
                  sky_object_t  referencing_object)
{
    if (new_object_reference) {
        if (!referencing_object) {
            sky_object_gc_notestore(new_object_reference, SKY_FALSE, SKY_FALSE);
        }
        else {
            sky_object_data_t   *object_data =
                                SKY_OBJECT_DATA(referencing_object);

            sky_object_gc_notestore(new_object_reference,
                                    object_data->global,
                                    object_data->concrete);
        }
    }
    *pointer_location = new_object_reference;
}


void
sky_object_gc_setarray(ssize_t          nobjects,
                       sky_object_t *   objects,
                       sky_object_t     referencing_object)
{
    ssize_t             i;
    sky_bool_t          concrete, global;
    sky_object_data_t   *object_data;

    if (!referencing_object) {
        concrete = global = SKY_FALSE;
    }
    else {
        object_data = SKY_OBJECT_DATA(referencing_object);
        concrete = object_data->concrete;
        global = object_data->global;
    }

    for (i = 0; i < nobjects; ++i) {
        sky_object_gc_notestore(objects[i], global, concrete);
    }
}


void
sky_object_gc_setlibraryroot(sky_object_t * root_pointer_location,
                             sky_object_t   new_root_reference,
                             sky_bool_t     global)
{
    sky_object_t    old_root_reference;

    old_root_reference = (root_pointer_location ? *root_pointer_location
                                                : NULL);
    if (new_root_reference != old_root_reference) {
        sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

        if (new_root_reference && !sky_object_istagged(new_root_reference)) {
            sky_object_gc_notestore(new_root_reference, global, SKY_TRUE);
        }

        if (!tlsdata->gc_roots) {
            tlsdata->gc_roots = sky_object_map_create();
        }

        sky_spinlock_lock(&sky_object_gc_library_roots_lock);
        if (new_root_reference && !sky_object_istagged(new_root_reference)) {
            sky_object_map_insert(tlsdata->gc_roots, new_root_reference);
            sky_object_gc_library_root_insert(new_root_reference,
                                              root_pointer_location);
        }
        if (old_root_reference && !sky_object_istagged(old_root_reference)) {
            sky_object_map_remove(tlsdata->gc_roots, old_root_reference);
            sky_object_gc_library_root_remove(old_root_reference,
                                              root_pointer_location);
        }
        *root_pointer_location = new_root_reference;
        sky_spinlock_unlock(&sky_object_gc_library_roots_lock);
    }
}


void
sky_object_gc_setroot(sky_object_t *root_pointer_location,
                      sky_object_t  new_root_reference,
                      sky_bool_t    global)
{
    sky_object_t    old_root_reference;

    old_root_reference = (root_pointer_location ? *root_pointer_location
                                                : NULL);
    if (new_root_reference != old_root_reference) {
        sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

        /* Either new_root_reference or old_root_reference is non-NULL, and so
         * gc_roots is going to be needed, so create it if it doesn't yet exist.
         */
        if (!tlsdata->gc_roots) {
            tlsdata->gc_roots = sky_object_map_create();
        }
        if (new_root_reference && !sky_object_istagged(new_root_reference)) {
            sky_object_map_insert(tlsdata->gc_roots, new_root_reference);
            sky_object_gc_notestore(new_root_reference, global, SKY_TRUE);
        }
        if (old_root_reference && !sky_object_istagged(old_root_reference)) {
            sky_object_map_remove(tlsdata->gc_roots, old_root_reference);
        }
        if (root_pointer_location) {
            *root_pointer_location = new_root_reference;
        }
    }
}


void
sky_object_gc_setweakref(sky_object_t *                 object_pointer,
                         sky_object_t                   new_object_reference,
                         sky_object_t                   referencing_object,
                         sky_object_weakref_callback_t  callback,
                         sky_object_t                   arg)
{
    sky_object_data_t       *object_data;
    sky_object_reference_t  *ref;

    /* If there is an existing reference, it shall first be cleared. It is not
     * an error if *object_pointer is non-NULL and a reference does not exist
     * for it already. This can happen if a strong reference is being converted
     * to a weak one. For this same reason, there is intentionally no fast-path
     * if *object_pointer == new_object_reference. In the case where a ref is
     * cleared, its data structure will be re-used for the new reference
     * instead of wasting a malloc/free combo.
     */

    if (*object_pointer && !sky_object_istagged(*object_pointer)) {
        object_data = SKY_OBJECT_DATA(*object_pointer);
        for (ref = object_data->refs; ref; ref = ref->next) {
            if (ref->ref == object_pointer) {
                ref->ref = NULL;
                ref->callback = NULL;
                ref->arg = NULL;
            }
        }
    }

    if (new_object_reference) {
        if (!referencing_object) {
            sky_object_gc_notestore(new_object_reference, SKY_FALSE, SKY_FALSE);
        }
        else {
            object_data = SKY_OBJECT_DATA(referencing_object);
            sky_object_gc_notestore(new_object_reference,
                                    object_data->global,
                                    object_data->concrete);
        }
    }

    if ((*object_pointer = new_object_reference) != NULL &&
        !sky_object_istagged(new_object_reference))
    {
        object_data = SKY_OBJECT_DATA(new_object_reference);
        for (ref = object_data->refs; ref; ref = ref->next) {
            if (!ref->ref &&
                sky_atomic_cas(NULL,
                               object_pointer,
                               SKY_AS_VOIDP(&(ref->ref))))
            {
                break;
            }
        }
        if (!ref) {
            ref = sky_malloc(sizeof(sky_object_reference_t));
            ref->ref = object_pointer;
            do {
                ref->next = object_data->refs;
            } while (!sky_atomic_cas(ref->next,
                                     ref,
                                     SKY_AS_VOIDP(&(object_data->refs))));
        }
        ref->arg = NULL;
        if ((ref->callback = callback) != NULL) {
            sky_object_gc_set(&(ref->arg), arg, new_object_reference);
        }
    }
}


void
sky_object_gc_register(sky_object_gc_wakeup_t function, void *arg)
{
    sky_object_tlsdata_t    *tlsdata = &(sky_tlsdata_get()->object_tlsdata);

    /* If this thread is not currently a registered GC thread, register it now.
     * By calling sky_object_gc() at all, the thread has indicated that it
     * intends to participate in garbage collection.
     */
    if (!tlsdata->gc_registered) {
        sky_object_gc_initialize_thread(tlsdata);
    }

    tlsdata->gc_wakeup_function = NULL;
    sky_atomic_sfence();

    tlsdata->gc_wakeup_arg = arg;
    sky_atomic_sfence();

    tlsdata->gc_wakeup_function = function;
}


uint32_t
sky_object_gc_getthreshold(void)
{
    return sky_object_gc_threshold;
}


void
sky_object_gc_setthreshold(uint32_t threshold)
{
    sky_object_gc_threshold = threshold;
}


void
sky_object_gc_wakeup(void)
{
    pthread_mutex_lock(&sky_object_gc_thread_mutex);
    sky_object_gc_wokenup = SKY_TRUE;
    pthread_cond_signal(&sky_object_gc_thread_cond);
    pthread_mutex_unlock(&sky_object_gc_thread_mutex);
}


void
sky_object_gc_atfork_child(SKY_UNUSED sky_object_tlsdata_t *object_tlsdata)
{
    /* Re-initialize the gc thread mutex. See the comments in sky_atfork.c for
     * re-initialization of sky_atfork_mutex for details.
     */
    memset(&sky_object_gc_thread_mutex, 0, sizeof(sky_object_gc_thread_mutex));
    pthread_mutex_init(&sky_object_gc_thread_mutex, NULL);
    memset(&sky_object_gc_thread_cond, 0, sizeof(sky_object_gc_thread_cond));
    pthread_cond_init(&sky_object_gc_thread_cond, NULL);
}


static sky_bool_t
sky_object_gc_finalize_collect(sky_tlsdata_t *  tlsdata,
                               sky_bool_t       run_destructors)
{
    sky_object_tlsdata_t    *object_tlsdata = &(tlsdata->object_tlsdata);

    sky_bool_t  swept;

    /* Force a hazard pointer scan so that anything hanging around in a retired
     * state will get freed.
     */
    sky_hazard_pointer_scan();

    if (object_tlsdata->object_stack) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(object_tlsdata->object_stack)),
                              NULL,
                              SKY_FALSE);
    }
    object_tlsdata->gc_epoch = ++sky_object_gc_epoch;
    object_tlsdata->gc_running = 1;

    if (sky_object_map_length(object_tlsdata->gc_roots)) {
        sky_atomic_lifo_push(&sky_object_gc_root_list, object_tlsdata->gc_roots);
        object_tlsdata->gc_roots = NULL;
    }
    sky_object_gc_sweep_objects(object_tlsdata);

    sky_object_gc_mark_roots();

    /* There should be nothing to mark here! */
    sky_error_validate(!sky_pointer_set_length(object_tlsdata->gc_stores));
    object_tlsdata->gc_running = 0;

    /* Sweep and return SKY_TRUE if anything was swept up. */
    sky_object_gc_sweep_1(object_tlsdata, run_destructors, SKY_TIME_INFINITE);

    swept = sky_object_gc_sweep_2(object_tlsdata,
                                  run_destructors,
                                  SKY_TIME_INFINITE,
                                  object_tlsdata->gc_concrete_weakref_set);
    sky_pointer_set_destroy(object_tlsdata->gc_concrete_weakref_set);
    object_tlsdata->gc_concrete_weakref_set = NULL;

    sky_object_gc_sweep_3(object_tlsdata, run_destructors, SKY_TIME_INFINITE);
    sky_object_gc_sweep_4(object_tlsdata, SKY_TIME_INFINITE);
    sky_object_gc_sweep_5(object_tlsdata);
    sky_object_gc_sweep_6(object_tlsdata, SKY_TIME_INFINITE);

    if (swept) {
#if defined(SKY_EPHEMERAL_OBJECTS)
        sky_error_validate_debug(!object_tlsdata->gc_ephemeral_objects.head);
        sky_error_validate_debug(!object_tlsdata->gc_ephemeral_objects.tail);
#endif
        return SKY_TRUE;
    }
    return SKY_FALSE;
}


static sky_bool_t
sky_object_gc_finalize_roots(sky_object_tlsdata_t *tlsdata)
{
    sky_hashtable_t *table = &(sky_object_gc_roots->table);

    intmax_t                    count;
    sky_bool_t                  collect;
    sky_object_t                object;
    sky_object_data_t           *object_data;
    sky_object_map_item_t       *item;
    sky_hashtable_iterator_t    iterator;

    /* Force a hazard pointer scan so that anything hanging around in a retired
     * state will get freed.
     */
    sky_hazard_pointer_scan();

    collect = SKY_FALSE;
    sky_hashtable_iterator_init(&iterator, table);
    while ((item = (sky_object_map_item_t *)
                   sky_hashtable_iterator_next(&iterator)) != NULL)
    {
        object = item->object;
        object_data = SKY_OBJECT_DATA(object);

        /* Clear library root references. */
        if ((count = sky_object_gc_library_root_lookup(object)) > 0) {
            sky_object_map_update(sky_object_gc_roots, object, -count);
            collect = SKY_TRUE;
        }

        /* Handle static types by removing them from the list of object roots
         * and adding them to this thread's list of objects so that they'll be
         * collected normally. There's no good reason to have them in the list
         * of objects until now, so do it here instead of when the type object
         * is completed at initialization time. In fact, having the objects in
         * the list will only serve to slow down sweep passes, if only by a
         * tiny amount, because the objects will never be swept.
         */
        if (object_data->u.type == sky_type_type &&
            (sky_type_data(object)->flags & SKY_TYPE_FLAG_STATIC))
        {
            sky_error_validate(1 == ((sky_object_map_item_t *)item)->counter);

            sky_object_list_prepend(&(tlsdata->gc_concrete_objects), object);
            sky_object_map_update(sky_object_gc_roots, object, -1);
            collect = SKY_TRUE;
        }
    }

    return collect;
}

void
sky_object_gc_finalize_library(void)
{
    sky_tlsdata_t           *tlsdata        = sky_tlsdata_get();
    sky_object_tlsdata_t    *object_tlsdata = &(tlsdata->object_tlsdata);

    void    *exit_status;

    sky_error_validate(!sky_object_gc_finalizing);
    sky_object_gc_finalizing = SKY_TRUE;

    sky_error_validate(!sky_object_gc_thread_shutdown);
    sky_object_gc_thread_shutdown = SKY_TRUE;

    while (sky_object_gc_thread_running) {
        pthread_mutex_lock(&sky_object_gc_thread_mutex);
        pthread_cond_signal(&sky_object_gc_thread_cond);
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);
        pthread_join(sky_object_gc_thread, &exit_status);
    }

    sky_object_gc_thread_shutdown = SKY_FALSE;
    sky_error_validate(!sky_object_gc_thread_running);

    if (sky_object_gc_active_count > 1) {
        sky_object_gc_wakeup_threads();

        pthread_mutex_lock(&sky_object_gc_thread_mutex);
        while (sky_object_gc_active_count > 1) {
            pthread_cond_wait(&sky_object_gc_thread_cond,
                              &sky_object_gc_thread_mutex);
        }
        pthread_mutex_unlock(&sky_object_gc_thread_mutex);
        sky_error_validate(sky_object_gc_active_count <= 1);
    }

    while (sky_tlsdata_adopt());

    /* If there was a GC cycle in progress, finish it. */
    switch (sky_object_gc_mode) {
        case SKY_OBJECT_GC_MODE_IDLE:
        case SKY_OBJECT_GC_MODE_REQUESTED:
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_IDLE;
            break;
        case SKY_OBJECT_GC_MODE_STARTING:
            if (!object_tlsdata->gc_running) {
                sky_object_gc_mode = SKY_OBJECT_GC_MODE_IDLE;
                break;
            }
            /* The thread has already checked-in, so the gc cycle must be
             * finished. Notably, object_tlsdata->gc_stores is not NULL and
             * likely not empty, which will cause problems later if the cycle
             * is skipped here.
             */
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_MARKING;
        case SKY_OBJECT_GC_MODE_MARKING:
            sky_object_gc_mark_roots();
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_MARK_ENDING;
        case SKY_OBJECT_GC_MODE_MARK_ENDING:
            object_tlsdata->gc_running = 0;
            if (object_tlsdata->gc_stores) {
                if (!sky_pointer_set_length(object_tlsdata->gc_stores)) {
                    sky_pointer_set_destroy(object_tlsdata->gc_stores);
                }
                else {
                    object_tlsdata->gc_stores->next = sky_object_gc_store_list;
                    sky_object_gc_store_list = object_tlsdata->gc_stores;
                }
                object_tlsdata->gc_stores = NULL;
            }
            if (sky_object_gc_store_list) {
                pthread_mutex_lock(&sky_object_gc_thread_mutex);
                sky_object_gc_mark_stores();
                pthread_mutex_unlock(&sky_object_gc_thread_mutex);
            }
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_1;
        case SKY_OBJECT_GC_MODE_SWEEP_1:
            sky_object_gc_sweep_1(object_tlsdata, SKY_TRUE, SKY_TIME_INFINITE);
            if (!sky_object_gc_weakref_set) {
                sky_object_gc_weakref_set =
                        object_tlsdata->gc_concrete_weakref_set;
            }
            else {
                sky_pointer_set_merge(sky_object_gc_weakref_set,
                                      object_tlsdata->gc_concrete_weakref_set);
                sky_pointer_set_destroy(object_tlsdata->gc_concrete_weakref_set);
            }
            object_tlsdata->gc_concrete_weakref_set = NULL;
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_2;
        case SKY_OBJECT_GC_MODE_SWEEP_2:
            sky_object_gc_sweep_2(object_tlsdata,
                                  SKY_TRUE,
                                  SKY_TIME_INFINITE,
                                  sky_object_gc_weakref_set);
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_3;
        case SKY_OBJECT_GC_MODE_SWEEP_3:
            sky_object_gc_sweep_3(object_tlsdata, SKY_TRUE, SKY_TIME_INFINITE);
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_4;
        case SKY_OBJECT_GC_MODE_SWEEP_4:
            sky_object_gc_sweep_4(object_tlsdata, SKY_TIME_INFINITE);
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_5;
        case SKY_OBJECT_GC_MODE_SWEEP_5:
            sky_object_gc_sweep_5(object_tlsdata);
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_SWEEP_6;
        case SKY_OBJECT_GC_MODE_SWEEP_6:
            sky_object_gc_sweep_6(object_tlsdata, SKY_TIME_INFINITE);
            sky_object_gc_mode = SKY_OBJECT_GC_MODE_IDLE;
    }
    sky_error_validate_debug(!object_tlsdata->gc_stores);

    /* These are normally cleaned up per thread by sky_tlsdata_cleanup(), but
     * the main thread is still running and so sky_tlsdata_cleanup() hasn't
     * been run for it, but it's important that these get cleared now so that
     * there are no GC leaks.
     */
    if (tlsdata->trace_function) {
        sky_object_gc_setroot(&(tlsdata->trace_function), NULL, SKY_FALSE);
    }
    if (tlsdata->current_frame) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                              NULL,
                              SKY_FALSE);
    }

    /* If sky_object_gc_roots is NULL, there never was a gc thread, but that
     * does not mean that there isn't anything to be collected. However, it
     * does mean that there was never a gc cycle run before, which means that
     * we need to artificially crank up the current epoch.
     */
    if (!sky_object_gc_roots) {
        sky_object_gc_roots = sky_object_map_create();
    }
    sky_object_gc_epoch = SKY_MAX(sky_object_gc_epoch, 4);
#if defined(SKY_EPHEMERAL_OBJECTS)
    sky_object_gc_sweep_ephemerals(object_tlsdata);
#endif

    /* Because the delta between the sweep epoch and the current epoch is
     * always two, do two additional collections to bridge the gap. There
     * should not be any new objects created by this time.
     */
    sky_object_gc_finalize_collect(tlsdata, SKY_TRUE);
    sky_object_gc_finalize_collect(tlsdata, SKY_TRUE);

    /* All that is left at this point should be roots and static types
     * including built-in exceptions) and the objects that they directly
     * reference. Scan the list of roots looking for these and clear them out.
     */
    if (sky_object_gc_finalize_roots(object_tlsdata)) {
        /* Bump the epoch by two to ensure a complete sweep. However, there are
         * probably weak references left from type objects, so a second run is
         * needed to clean those up after the first one.
         */
        sky_object_gc_epoch += 2;
        sky_object_gc_finalize_collect(tlsdata, SKY_FALSE);
        sky_object_gc_finalize_collect(tlsdata, SKY_FALSE);
    }

#if !defined(NDEBUG)
    do {
        intmax_t            total;
        sky_object_t        object;
        sky_object_data_t   *object_data;

#   if defined(SKY_EPHEMERAL_OBJECTS)
        if (object_tlsdata->gc_ephemeral_objects.head) {
            total = 0;
            sky_format_dprintf("Ephemeral objects remain:\n");
            for (object = object_tlsdata->gc_ephemeral_objects.head;
                 object;
                 object = object_data->gc_link)
            {
                object_data = SKY_OBJECT_DATA(object);
                sky_format_dprintf(
                        "    %3jd: %p %d %s%s\n",
                        total, object, object_data->gc_epoch,
                        sky_type_data(sky_object_type(object))->name,
                        (sky_object_gc_isroot(object) ? " [ROOT]" : ""));
                ++total;
            }
        }
#   endif

        if (object_tlsdata->gc_concrete_objects.head) {
            total = 0;
            sky_format_dprintf("Remaining uncollected objects (epoch %u):\n",
                               sky_object_gc_epoch);
            for (object = object_tlsdata->gc_concrete_objects.head;
                 object;
                 object = object_data->gc_link)
            {
                object_data = SKY_OBJECT_DATA(object);
                sky_format_dprintf(
                        "    %3jd: %p %d %s%s\n",
                        total, object, object_data->gc_epoch,
                        sky_type_data(sky_object_type(object))->name,
                        (sky_object_gc_isroot(object) ? " [ROOT]" : ""));
                ++total;
            }
        }
    } while (0);
#endif

    object_tlsdata->gc_epoch = 2;
#if defined(SKY_EPHEMERAL_OBJECTS)
    sky_object_list_empty(&(object_tlsdata->gc_ephemeral_objects));
#endif
    sky_object_list_empty(&(object_tlsdata->gc_concrete_objects));
    sky_object_list_empty(&(object_tlsdata->gc_concrete_sweep));
    sky_object_list_empty(&(object_tlsdata->gc_concrete_dying));
    sky_object_list_empty(&(object_tlsdata->gc_concrete_del));
    sky_object_list_empty(&(object_tlsdata->gc_concrete_finalize));
    sky_object_list_empty(&(object_tlsdata->gc_concrete_deallocate));

    if (object_tlsdata->gc_concrete_weakref_set) {
        sky_pointer_set_destroy(object_tlsdata->gc_concrete_weakref_set);
        object_tlsdata->gc_concrete_weakref_set = NULL;
    }

    if (object_tlsdata->gc_roots) {
        sky_object_map_destroy(object_tlsdata->gc_roots);
        object_tlsdata->gc_roots = NULL;
    }
    if (object_tlsdata->gc_stores) {
        sky_error_validate(!sky_pointer_set_length(object_tlsdata->gc_stores));
        sky_pointer_set_destroy(object_tlsdata->gc_stores);
        object_tlsdata->gc_stores = NULL;
    }

    sky_hashtable_cleanup(&sky_object_gc_library_roots,
                          sky_object_gc_library_root_free);
    sky_object_map_destroy(sky_object_gc_roots);
    sky_object_gc_roots = NULL;

    sky_object_gc_epoch = 2;
    sky_object_gc_completed = 0;
    sky_object_gc_threshold = 1000000;
    sky_object_gc_enabled = SKY_TRUE;
    sky_object_gc_finalizing = SKY_FALSE;
    sky_object_gc_threaded = SKY_FALSE;
}


void
sky_object_gc_initialize_library(void)
{
    sky_tlsdata_t           *tlsdata        = sky_tlsdata_get();
    sky_object_tlsdata_t    *object_tlsdata = &(tlsdata->object_tlsdata);

#if defined(SKY_EPHEMERAL_OBJECTS)
    sky_error_validate(NULL == object_tlsdata->gc_ephemeral_objects.head);
#endif
    sky_error_validate(NULL == object_tlsdata->gc_concrete_objects.head);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_sweep.head);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_weakref_set);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_dying.head);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_del.head);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_finalize.head);
    sky_error_validate(NULL == object_tlsdata->gc_concrete_deallocate.head);
    sky_error_validate(NULL == object_tlsdata->gc_roots);
    sky_error_validate(NULL == object_tlsdata->gc_stores);

    sky_error_validate(2 == sky_object_gc_epoch);
    sky_error_validate(SKY_FALSE == sky_object_gc_finalizing);
    sky_error_validate(SKY_OBJECT_GC_MODE_IDLE == sky_object_gc_mode);

    sky_error_validate(SKY_FALSE == sky_object_gc_wokenup);
    sky_error_validate(1 >= sky_object_gc_active_count);
    sky_error_validate(0 == sky_object_gc_waiting_count);
    sky_error_validate(NULL == sky_object_gc_roots);
    sky_error_validate(NULL == sky_object_gc_store_list);

    sky_error_validate(0 == sky_object_gc_thread_running);
    sky_error_validate(SKY_FALSE == sky_object_gc_thread_shutdown);

    if (!object_tlsdata->gc_registered) {
        sky_object_gc_initialize_thread(object_tlsdata);
    }
    sky_hashtable_init(&sky_object_gc_library_roots,
                       sky_object_gc_library_root_compare);
}
