/** @file
  * @brief
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_socket Socket Objects
  * @{
  */

#ifndef __SKYTHON_SKY_SOCKET_H__
#define __SKYTHON_SKY_SOCKET_H__ 1


#include "sky_base.h"
#include "sky_object.h"
#include "sky_exceptions.h"


SKY_CDECLS_BEGIN


SKY_EXCEPTION_DECLARE(socket_gaierror, socket_gaierror);
SKY_EXCEPTION_DECLARE(socket_timeout, socket_timeout);


/** Accept a new connection from a listening socket.
  *
  * @param[in]  self            the listening socket.
  * @param[out] remote_address  the address of the remote endpoint. May be
  *                             @c NULL if the information is not needed.
  * @return     a new socket object instance that is the accepted socket.
  */
SKY_EXTERN sky_socket_t
sky_socket_accept(sky_socket_t self, sky_object_t *remote_address);

/** Bind a socket object to an address.
  *
  * @param[in]  self    the socket to bind.
  * @param[in]  address the address to bind. The type of object for the address
  *                     varies based on the socket's address family.
  */
SKY_EXTERN void
sky_socket_bind(sky_socket_t self, sky_object_t address);

/** Close an open socket object.
  * The underlying socket descriptor will be closed and the socket object will
  * no longer be useable.
  *
  * @param[in]  self    the socket to close.
  */
SKY_EXTERN void
sky_socket_close(sky_socket_t self);

/** Return the length of an ancillary data item without trailing padding.
  * This is the same as CMSG_LEN(), but it does bounds checking, raising
  * @c sky_OverflowError if @a length is out of range.
  *
  * @param[in]  length  the length of the data item for which the length of
  *                     ancillary data is to be calculated.
  * @return     the total length, without trailing padding, of an ancillary
  *             data item with associated data of length @a length.
  */
SKY_EXTERN size_t
sky_socket_CMSG_LEN(ssize_t length);

/** Return the buffer size needed to receive ancillary data along with any
  * padding.
  * This is the same as CMSG_SPACE(), but it does bounds checking, raising
  * @c sky_OverflowError if @a length is out of range.
  *
  * @param[in]  length  the length of the data item for which the buffer size
  *             is to be calculated.
  * @return     the buffer size needed to receive an ancillary data item with
  *             associated data of length @a length, along with any trailing
  *             padding.
  */
SKY_EXTERN size_t
sky_socket_CMSG_SPACE(ssize_t length);

/** Connect a socket to a remote address.
  *
  * @param[in]  self        the socket to connect.
  * @param[in]  address     the address to connect to.
  */
SKY_EXTERN void
sky_socket_connect(sky_socket_t self, sky_object_t address);

/** Create a new socket object.
  *
  * @param[in]  family      the address family for the socket (e.g., @c AF_INET,
  *                         @c AF_LOCAL, etc.)
  * @param[in]  type        the type of socket (e.g., @c SOCK_STREAM,
  *                         @c SOCK_DGRAM, etc.)
  * @param[in]  protocol    the protocol (usually 0).
  * @return     a new socket object instance.
  */
SKY_EXTERN sky_socket_t
sky_socket_create(int family, int type, int protocol);

/** Create a new socket object with an existing socket desriptor.
  *
  * @param[in]  fd          the socket descriptor to use.
  * @param[in]  family      the address family for the socket (e.g., @c AF_INET,
  *                         @c AF_LOCAL, etc.)
  * @param[in]  type        the type of socket (e.g., @c SOCK_STREAM,
  *                         @c SOCK_DGRAM, etc.)
  * @param[in]  protocol    the protocol (usually 0).
  * @return     a new socket object instance using @a fd as its underlying
  *             socket descriptor.
  */
SKY_EXTERN sky_socket_t
sky_socket_createwithdescriptor(int fd, int family, int type, int protocol);

/** Detach a socket object from its underlying socket descriptor.
  * Once detached, the socket object will no longer be useable, because it will
  * not have an associated socket descriptor. The socket descriptor is returned
  * in an unmodified state.
  *
  * @param[in]  self    the socket object to detach.
  * @return     the socket descriptor that was attached to the socket, or -1 if
  *             there wasn't one.
  */
SKY_EXTERN int
sky_socket_detach(sky_socket_t self);

/** Return the underlying socket descriptor for a socket object.
  *
  * @param[in]  self    the socket object for which the underlying socket
  *                     descriptor is to be returned.
  * @return     the socket descriptor that is attached to the socket object, or
  *             -1 if there isn't one.
  */
SKY_EXTERN int
sky_socket_fileno(sky_socket_t self);

/** Return the default timeout for new socket objects.
  *
  * @return     the default timeout used for new socket objects.
  */
SKY_EXTERN double
sky_socket_getdefaulttimeout(void);

/** Return the remote endpoint address of a socket object.
  *
  * @param[in]  self    the socket object for which the remote endpoint address
  *                     is to be returned.
  * @return     the remote endpoint address of @a self. The type of object for
  *             the address varies based on the socket's address family.
  */
SKY_EXTERN sky_object_t
sky_socket_getpeername(sky_socket_t self);

/** Return the local endpoint address of a socket object.
  *
  * @param[in]  self    the socket object for which the local endpoint address
  *                     is to be returned.
  * @return     the local endpoint address of @a self. The type of object for
  *             the address varies based on the socket's address family.
  */
SKY_EXTERN sky_object_t
sky_socket_getsockname(sky_socket_t self);

/** Return the value of a socket option for a socket object.
  * See the Unix manual pages for getsockopt() for details.
  * 
  * @param[in]  self        the socket object for which a socket option is to
  *                         be returned.
  * @param[in]  level       the socket level.
  * @param[in]  option      the socket option.
  * @param[in]  buffersize  the buffer size to use for a string option, or -1
  *                         for an integer option.
  * @return     the value of the requested socket option, which may be either
  *             an integer (if @a buffersize is -1) or a bytes object.
  */
SKY_EXTERN sky_object_t
sky_socket_getsockopt(sky_socket_t  self,
                      int           level,
                      int           option,
                      ssize_t       buffersize);

/** Return the timeout set for a socket object.
  *
  * @param[in]  self    the socket object for which the timeout is to be
  *                     returned.
  * @return     the timeout for @a self. If the socket object is in blocking
  *             mode with no timeout, the return will be -1.0.
  */
SKY_EXTERN double
sky_socket_gettimeout(sky_socket_t self);

/** Listen for connections on a socket object.
  *
  * @param[in]  self    the socket object to be put into listening mode.
  * @param[in]  backlog the size of the connection queue to be used.
  */
SKY_EXTERN void
sky_socket_listen(sky_socket_t self, int backlog);

/** Raise a @c sky_socket_gaierror exception.
  *
  * @param[in]  error   the error code returned by getaddrinfo().
  */
SKY_EXTERN SKY_NORETURN void
sky_socket_raise_gaierror(int error);

/** Receive data from a socket.
  *
  * @param[in]  self    the socket to receive data from.
  * @param[in]  bytes   the buffer to receive data into.
  * @param[in]  nbytes  the maximum number of bytes to receive.
  * @param[in]  flags   flags (see the Unix manual page for recv).
  * @return     the number of bytes received.
  */
SKY_EXTERN ssize_t
sky_socket_recv_native(sky_socket_t self,
                       void *       bytes,
                       ssize_t      nbytes,
                       int          flags);


/** Receive data from a socket.
  *
  * @param[in]  self    the socket to receive data from.
  * @param[in]  nbytes  the maximum number of bytes to receive.
  * @param[in]  flags   flags (see the Unix manual page for recv).
  * @return     a bytes object instance containing the data received.
  */
SKY_EXTERN sky_bytes_t
sky_socket_recv(sky_socket_t self, ssize_t nbytes, int flags);


/** Receive data from a socket.
  *
  * @param[in]  self    the socket to receive data from.
  * @param[in]  buffer  the object to store data into, which may be any object
  *                     that supports the buffer interface.
  * @param[in]  nbytes  the maximum number of bytes to receive.
  * @param[in]  flags   flags (see the Unix manual page for recv).
  * @return     the number of bytes received.
  */
SKY_EXTERN ssize_t
sky_socket_recv_into(sky_socket_t   self,
                     sky_object_t   buffer,
                     ssize_t        nbytes,
                     int            flags);

/** Receive data from a socket.
  *
  * @param[in]  self            the socket to receive data from.
  * @param[in]  bytes           the buffer to receive data into.
  * @param[in]  nbytes          the maximum number of bytes to receive.
  * @param[in]  flags           flags (see the Unix manual page for recvfrom).
  * @param[out] remote_address  the remote address from which the data was
  *                             received, if not @c NULL.
  * @return     the number of bytes received.
  */
SKY_EXTERN ssize_t
sky_socket_recvfrom_native(sky_socket_t     self,
                           void *           bytes,
                           ssize_t          nbytes,
                           int              flags,
                           sky_object_t *   remote_address);

/** Receive data from a socket.
  *
  * @param[in]  self        the socket to receive data from.
  * @param[in]  buffersize  the maximum number of bytes to receive.
  * @param[in]  flags       flags (see the Unix manual page for recvfrom).
  * @return     a 2-tuple containing a bytes object that contains the data
  *             received and the remote address from which the data was
  *             received.
  */
SKY_EXTERN sky_tuple_t
sky_socket_recvfrom(sky_socket_t self, ssize_t buffersize, int flags);

/** Receive data from a socket.
  *
  * @param[in]  self    the socket to receive data from.
  * @param[in]  buffer  the object to store data into, which may be any object
  *                     that supports the buffer interface.
  * @param[in]  nbytes  the maximum number of bytes to receive.
  * @param[in]  flags   flags (see the Unix manual page for recvfrom).
  * @return     a 2-tuple containing a bytes object that contains the data
  *             received and the remote address from which the data was
  *             received.
  */
SKY_EXTERN sky_tuple_t
sky_socket_recvfrom_into(sky_socket_t   self,
                         sky_object_t   buffer,
                         ssize_t        nbytes,
                         int            flags);

/** Receive data from a socket.
  *
  * @param[in]  self    the socket to receive data from.
  * @param[in]  msg     the msghdr structure to be passed to recvmsg().
  * @param[in]  flags   flags (see the Unix manual page for recvmsg).
  * @return     the number of bytes received.
  */
SKY_EXTERN ssize_t
sky_socket_recvmsg_native(sky_socket_t self, struct msghdr *msg, int flags);

/** Receive data from a socket.
  *
  * @param[in]  self        the socket to receive data from.
  * @param[in]  bufsize     the maximum number of bytes to receive.
  * @param[in]  ancbufsize  the maximum number of bytes to receive for
  *                         ancillary data.
  * @param[in]  flags       flags (see the Unix manual page for recvmsg).
  * @return     a 4-tuple containing a bytes object that contains the data
  *             received, a list that contains any ancillary data received,
  *             flags describing the message received, and the remote address
  *             from which the data was received.
  */
SKY_EXTERN sky_tuple_t
sky_socket_recvmsg(sky_socket_t self, ssize_t bufsize, ssize_t ancbufsize, int flags);

/** Receive data from a socket.
  *
  * @param[in]  self        the socket to receive data from.
  * @param[in]  buffers     a sequence of objects supporting the buffer
  *                         interface, into which data received will be stored.
  * @param[in]  ancbufsize  the maximum number of bytes to receive for
  *                         ancillary data.
  * @param[in]  flags       flags (see the Unix manual page for recvmsg).
  * @return     a 4-tuple containing a bytes object that contains the data
  *             received, a list that contains any ancillary data received,
  *             flags describing the message received, and the remote address
  *             from which the data was received.
  */
SKY_EXTERN sky_tuple_t
sky_socket_recvmsg_into(sky_socket_t    self,
                        sky_object_t    buffers,
                        ssize_t         ancbufsize,
                        int             flags);

/** Send data on a socket.
  *
  * @param[in]  self    the socket to send data on.
  * @param[in]  data    the data to send, which may be any object that supports
  *                     the buffer interface.
  * @param[in]  flags   flags (see the Unix manual page for send).
  * @return     the number of bytes sent.
  */
SKY_EXTERN ssize_t
sky_socket_send(sky_socket_t self, sky_object_t data, int flags);

/** Send data on a socket.
  *
  * @param[in]  self    the socket to send data on.
  * @param[in]  bytes   the bytes to send.
  * @param[in]  nbytes  the number of bytes in @a bytes to send.
  * @param[in]  flags   flags (see the Unix manual page for send).
  * @return     the number of bytes sent.
  */
SKY_EXTERN ssize_t
sky_socket_send_native(sky_socket_t self,
                       const void * bytes,
                       ssize_t      nbytes,
                       int          flags);

/** Send data on a socket.
  * Unlike sky_socket_send(), this function will not return until all of the
  * data is sent or an error occurs. If an error occurs, it is impossible to
  * know how much data was sent.
  *
  * @param[in]  self    the socket to send data on.
  * @param[in]  data    the data to send, which may be any object that supports
  *                     the buffer interface.
  * @param[in]  flags   flags (see the Unix manual page for send).
  */
SKY_EXTERN void
sky_socket_sendall(sky_socket_t self, sky_object_t data, int flags);

/** Send data on a socket.
  * Unlike sky_socket_send_native(), this function will not return until all of
  * the data is sent or an error occurs. If an error occurs, it is impossible
  * to know how much data was sent.
  *
  * @param[in]  self    the socket to send data on.
  * @param[in]  bytes   the bytes to send.
  * @param[in]  nbytes  the number of bytes in @a bytes to send.
  * @param[in]  flags   flags (see the Unix manual page for send).
  */
SKY_EXTERN void
sky_socket_sendall_native(sky_socket_t  self,
                          const void *  bytes,
                          ssize_t       nbytes,
                          int           flags);

/** Send data on a socket.
  *
  * @param[in]  self        the socket to send data on.
  * @param[in]  buffers     an iterable yielding objects supporting the buffer
  *                         interface. The data from the objects will be sent
  *                         as a single message.
  * @param[in]  ancdata     an iterable yielding 3-tuples containing ancillary
  *                         data to be sent (level, type, data).
  * @param[in]  flags       flags (see the Unix manual page for sendmsg).
  * @param[in]  address     the remote address to send the data to.
  * @return     the number of bytes sent.
  */
SKY_EXTERN ssize_t
sky_socket_sendmsg(sky_socket_t self,
                   sky_object_t buffers,
                   sky_object_t ancdata,
                   int          flags,
                   sky_object_t address);

/** Send data on a socket.
  *
  * @param[in]  self        the socket to send data on.
  * @param[in]  data        the data to send, which may be any object that
  *                         supports the buffer interface.
  * @param[in]  flags       flags (see the Unix manual page for sendto).
  * @param[in]  address     the remote address to send the data to.
  * @return     the number of bytes sent.
  */
SKY_EXTERN ssize_t
sky_socket_sendto(sky_socket_t  self,
                  sky_object_t  data,
                  int           flags,
                  sky_object_t  address);

/** Send data on a socket.
  *
  * @param[in]  self        the socket to send data on.
  * @param[in]  bytes       the bytes to send.
  * @param[in]  nbytes      the number of bytes in @a bytes to send.
  * @param[in]  flags       flags (see the Unix manual page for sendto).
  * @param[in]  address     the remote address to send the data to.
  * @return     the number of bytes sent.
  */
SKY_EXTERN ssize_t
sky_socket_sendto_native(sky_socket_t   self,
                         const void *   bytes,
                         ssize_t        nbytes,
                         int            flags,
                         sky_object_t   address);

/** Set the blocking mode of a socket object.
  * This is equivalent to sky_socket_settimeout() with a timeout of -1.0 to
  * enable block mode or 0.0 to disable blocking mode.
  *
  * @param[in]  self    the socket object to be modified.
  * @param[in]  flag    @c SKY_TRUE to enable blocking mode, or @c SKY_FALSE to
  *                     disable blocking mode.
  */
SKY_EXTERN void
sky_socket_setblocking(sky_socket_t self, sky_bool_t flag);

/** Set the default timeout to use for new socket objects.
  *
  * @param[in] timeout  the timeout value to use.
  */
SKY_EXTERN void
sky_socket_setdefaulttimeout(double timeout);

/** Set a socket option for a socket object.
  * See the Unix manual pages for setsockopt() for details.
  * 
  * @param[in]  self    the socket object for which a socket option is to be
  *                     set.
  * @param[in]  level   the socket level.
  * @param[in]  option  the socket option.
  * @param[in]  value   the value to set, which may be either an integer object
  *                     or an object supporting the buffer protocol (typically
  *                     a bytes object).
  */
SKY_EXTERN void
sky_socket_setsockopt(sky_socket_t self, int level, int option, sky_object_t value);

/** Set the timeout for blocking operations on a socket object.
  *
  * @param[in]  self    the socket object for which the timeout is to be set.
  * @param[in]  timeout the timeout value to use, which should be -1.0 to put
  *                     the socket into blocking mode, or >= 0.0 to put the
  *                     socket into non-blocking mode.
  */
SKY_EXTERN void
sky_socket_settimeout(sky_socket_t self, double timeout);

/** Shutdown read and/or write sides of a socket object.
  *
  * @param[in]  self    the socket object to be shutdown.
  * @param[in]  flag    @c SHUT_RD to shutdown only the read side of the
  *                     socket, @c SHUT_WR to shutdown only the write side of
  *                     the socket, or @c SHUT_RDWR to shutdown both sides of
  *                     the socket.
  */
SKY_EXTERN void
sky_socket_shutdown(sky_socket_t self, int flag);


SKY_CDECLS_END


#endif  /* __SKYTHON_SKY_SOCKET_H__ */

/** @} **/
/** @} **/
