#include "sky_private.h"


typedef struct sky_enumerate_data_s {
    sky_object_t                        iterator;
    sky_integer_t                       index;
    uintmax_t                           iindex;
} sky_enumerate_data_t;

SKY_EXTERN_INLINE sky_enumerate_data_t *
sky_enumerate_data(sky_object_t object)
{
    return sky_object_data(object, sky_enumerate_type);
}

struct sky_enumerate_s {
    sky_object_data_t                   object_data;
    sky_enumerate_data_t                enumerate_data;
};


static void
sky_enumerate_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_enumerate_data_t    *self_data = data;

    sky_object_visit(self_data->iterator, visit_data);
    sky_object_visit(self_data->index, visit_data);
}


sky_enumerate_t
sky_enumerate_create(sky_object_t iterable, sky_integer_t start)
{
    sky_object_t                iterator;
    sky_enumerate_data_t        *enumerate_data;
    sky_enumerate_t volatile    enumerate;

    iterator = sky_object_iter(iterable);
    start = sky_number_index(start);

    enumerate = sky_object_allocate(sky_enumerate_type);
    enumerate_data = sky_enumerate_data(enumerate);
    sky_object_gc_set(&(enumerate_data->iterator), iterator, enumerate);

    SKY_ERROR_TRY {
        enumerate_data->iindex = sky_integer_value(start,
                                                   0,
                                                   UINTMAX_MAX,
                                                   NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(enumerate_data->index)),
                          start,
                          enumerate);
    } SKY_ERROR_TRY_END;

    return enumerate;
}


void
sky_enumerate_init(sky_object_t self, sky_object_t iterable, sky_object_t start)
{
    sky_enumerate_data_t    *self_data = sky_enumerate_data(self);

    sky_object_t    iterator;

    iterator = sky_object_iter(iterable);
    start = sky_number_index(start);

    SKY_ERROR_TRY {
        self_data->iindex = sky_integer_value(start, 0, UINTMAX_MAX, NULL);
    } SKY_ERROR_EXCEPT(sky_OverflowError) {
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)), start, self);
    } SKY_ERROR_TRY_END;
    sky_object_gc_set(&(self_data->iterator), iterator, self);
}


sky_object_t
sky_enumerate_iter(sky_enumerate_t self)
{
    return self;
}


sky_object_t
sky_enumerate_next(sky_enumerate_t self)
{
    sky_enumerate_data_t    *self_data = sky_enumerate_data(self);

    sky_tuple_t     result;
    sky_object_t    object;

    if (!(object = sky_object_next(self_data->iterator, NULL))) {
        return NULL;
    }

    if (self_data->index) {
        result = sky_tuple_pack(2, self_data->index, object);
        sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                          sky_number_add(self_data->index, sky_integer_one),
                          self);
    }
    else {
        result = sky_object_build("(ujO)", self_data->iindex, object);
        if (self_data->iindex++ == UINTMAX_MAX) {
            sky_object_gc_set(SKY_AS_OBJECTP(&(self_data->index)),
                              sky_number_add(sky_tuple_get(result, 0),
                                             sky_integer_one),
                              self);
        }
    }

    return result;
}


sky_object_t
sky_enumerate_reduce(sky_enumerate_t self)
{
    sky_enumerate_data_t    *self_data = sky_enumerate_data(self);

    if (self_data->index) {
        return sky_object_build("(O(OO))", sky_object_type(self),
                                           self_data->iterator,
                                           self_data->index);
    }
    return sky_object_build("(O(Ouj))", sky_object_type(self),
                                        self_data->iterator,
                                        self_data->iindex);
}


static const char sky_enumerate_type_doc[] =
"enumerate(iterable[, start]) -> iterator for index, value of iterable\n\n"
"Return an enumerate object.  iterable must be another object that supports\n"
"iteration.  The enumerate object yields pairs containing a count (from\n"
"start, which defaults to zero) and a value yielded by the iterable argument.\n"
"enumerate is useful for obtaining an indexed list:\n"
"    (0, seq[0]), (1, seq[1]), (2, seq[2]), ...";


SKY_TYPE_DEFINE_SIMPLE(enumerate,
                       "enumerate",
                       sizeof(sky_enumerate_data_t),
                       NULL,
                       NULL,
                       sky_enumerate_instance_visit,
                       0,
                       sky_enumerate_type_doc);


void
sky_enumerate_initialize_library(void)
{
    sky_type_initialize_builtin(sky_enumerate_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_enumerate_type,
            "__init__",
            (sky_native_code_function_t)sky_enumerate_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
            "start", SKY_DATA_TYPE_OBJECT, sky_integer_zero,
            NULL);
    sky_type_setmethodslots(sky_enumerate_type,
            "__iter__", sky_enumerate_iter,
            "__next__", sky_enumerate_next,
            "__reduce__", sky_enumerate_reduce,
            NULL);
}
