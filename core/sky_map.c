#include "sky_private.h"


typedef struct sky_map_data_s {
    sky_object_t                        function;
    sky_object_t                        iterators;
} sky_map_data_t;

SKY_EXTERN_INLINE sky_map_data_t *
sky_map_data(sky_object_t object)
{
    return sky_object_data(object, sky_map_type);
}

struct sky_map_s {
    sky_object_data_t                   object_data;
    sky_map_data_t                      map_data;
};


static void
sky_map_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_map_data_t  *self_data = data;

    sky_object_visit(self_data->function, visit_data);
    sky_object_visit(self_data->iterators, visit_data);
}


void
sky_map_init(sky_object_t self, sky_tuple_t args, sky_dict_t kws)
{
    ssize_t         i, n;
    sky_object_t    *iterators;
    sky_map_data_t  *self_data = sky_map_data(self);

    if (sky_object_bool(kws)) {
        sky_error_raise_string(sky_TypeError,
                               "map() does not take keyword arguments");
    }

    if (sky_tuple_len(args) < 2) {
        sky_error_raise_string(sky_TypeError,
                               "map() must have at least two arguments.");
    }

    SKY_ASSET_BLOCK_BEGIN {
        n = sky_tuple_len(args) - 1;
        iterators = sky_asset_malloc(n * sizeof(sky_object_t),
                                     SKY_ASSET_CLEANUP_ON_ERROR);

        for (i = 1; i <= n; ++i) {
            iterators[i - 1] = sky_object_iter(sky_tuple_get(args, i));
        }

        sky_object_gc_set(&(self_data->function),
                          sky_tuple_get(args, 0),
                          self);
        sky_object_gc_set(&(self_data->iterators),
                          sky_tuple_createwitharray(n, iterators, sky_free),
                          self);
    } SKY_ASSET_BLOCK_END;
}


sky_object_t
sky_map_iter(sky_map_t self)
{
    return self;
}


sky_object_t
sky_map_next(sky_map_t self)
{
    sky_map_data_t  *self_data = sky_map_data(self);

    ssize_t         i, n;
    sky_tuple_t     args;
    sky_object_t    iterator, *objects;

    SKY_ASSET_BLOCK_BEGIN {
        n = sky_tuple_len(self_data->iterators);
        objects = sky_asset_malloc(n * sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);
        for (i = 0; i < n; ++i) {
            iterator = sky_tuple_get(self_data->iterators, i);
            if (!(objects[i] = sky_object_next(iterator, NULL))) {
                sky_error_raise_object(sky_StopIteration, sky_None);
            }
        }
        args = sky_tuple_createwitharray(n, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return sky_object_call(self_data->function, args, NULL);
}


sky_object_t
sky_map_reduce(sky_map_t self)
{
    sky_map_data_t  *self_data = sky_map_data(self);

    sky_tuple_t     args;

    args = sky_tuple_insert(self_data->iterators, 0, self_data->function);
    return sky_object_build("(OO)", sky_object_type(self), args);
}


sky_map_t
sky_map_create(sky_object_t function, sky_object_t iterable, ...)
{
    va_list     ap;
    sky_map_t   map;

    va_start(ap, iterable);
    map = sky_map_createv(function, iterable, ap);
    va_end(ap);

    return map;
}


sky_map_t
sky_map_createv(sky_object_t function, sky_object_t iterable, va_list ap)
{
    sky_map_t       map;
    sky_list_t      list;
    sky_tuple_t     tuple;
    sky_object_t    object;
    sky_map_data_t  *map_data;

    list = sky_list_create(NULL);
    sky_list_append(list, iterable);
    while ((object = va_arg(ap, sky_object_t)) != NULL) {
        sky_list_append(list, sky_object_iter(object));
    }
    tuple = sky_tuple_create(list);

    map = sky_object_allocate(sky_map_type);
    map_data = sky_map_data(map);
    sky_object_gc_set(&(map_data->function), function, map);
    sky_object_gc_set(&(map_data->iterators), tuple, map);

    return map;
}


static const char sky_map_type_doc[] =
"map(func, *iterables) --> map object\n\n"
"Make an iterator that computes the function using arguments from\n"
"each of the iterables.  Stops when the shortest iterable is exhausted.";


SKY_TYPE_DEFINE_SIMPLE(map,
                       "map",
                       sizeof(sky_map_data_t),
                       NULL,
                       NULL,
                       sky_map_instance_visit,
                       0,
                       sky_map_type_doc);


void
sky_map_initialize_library(void)
{
    sky_type_initialize_builtin(sky_map_type, 0);
    sky_type_setmethodslots(sky_map_type,
            "__init__", sky_map_init,
            "__iter__", sky_map_iter,
            "__next__", sky_map_next,
            "__reduce__", sky_map_reduce,
            NULL);
}
