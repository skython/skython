#include "sky_private.h"


static sky_object_t sky_object_build_one(const char **format, va_list ap);


static size_t
sky_object_build_count(const char *format)
{
    char        terminator;
    size_t      count, depth;
    const char  *c;

    switch (*format) {
        case '[':
            ++format;
            terminator = ']';
            break;
        case '(':
            ++format;
            terminator = ')';
            break;
        case '{':
            ++format;
            if (*format != '[') {
                terminator = '}';
            }
            else {
                ++format;
                terminator = ']';
            }
            break;
        case '<':
            ++format;
            if (*format != '[') {
                terminator = '>';
            }
            else {
                ++format;
                terminator = ']';
            }
            break;
        default:
            sky_error_fatal("internal error");
    }

    count = depth = 0;
    for (c = format; *c; ++c) {
        switch (*c) {
            case '[':
            case '(':
            case '{':
            case '<':
                if (!depth) {
                    ++count;
                }
                ++depth;
                break;
            case ']':
            case ')':
            case '}':
            case '>':
                if (!depth) {
                    sky_error_validate_debug(*c == terminator);
                    return count;
                }
                --depth;
                break;
            case 'e':
                ++c;
            case 'O':
            case 's':
            case 'y':
            case 'Y':
            case 'B':
            case 'd':
            case 'p':
            case 'P':
                if (!depth) {
                    ++count;
                }
                break;
            case 'i':
            case 'u':
                if (c[1] == 'j' || c[1] == 'z') {
                    ++c;
                }
                if (!depth) {
                    ++count;
                }
                break;
        }
    }
    sky_error_fatal("unexpected end of format string");
}


static inline const char *
sky_object_build_skip(const char *format)
{
    while (*format &&
           (sky_ctype_isspace(*format) || *format == ':' || *format == ','))
    {
        ++format;
    }
    return format;
}


static sky_dict_t
sky_object_build_dict(const char **format, va_list ap)
{
    sky_dict_t      dict;
    sky_object_t    key, value;

    if (!(*format = sky_object_build_skip(&((*format)[1])))) {
        sky_error_raise_string(sky_ValueError, "invalid format string");
    }
    dict = sky_dict_create();
    while ((*format)[0] != '}') {
        key = sky_object_build_one(format, ap);
        if (!(*format = sky_object_build_skip(*format))) {
            sky_error_raise_string(sky_ValueError, "invalid format string");
        }
        value = sky_object_build_one(format, ap);
        if (!(*format = sky_object_build_skip(*format))) {
            sky_error_raise_string(sky_ValueError, "invalid format string");
        }
        sky_dict_setitem(dict, key, value);
    }
    *format = sky_object_build_skip(&((*format)[1]));

    return dict;
}


static sky_list_t
sky_object_build_list(const char **format, va_list ap)
{
    sky_list_t  list;

    if (!(*format = sky_object_build_skip(&((*format)[1])))) {
        sky_error_raise_string(sky_ValueError, "invalid format string");
    }
    list = sky_list_create(NULL);
    while ((*format)[0] != ']') {
        sky_list_append(list, sky_object_build_one(format, ap));
        if (!(*format = sky_object_build_skip(*format))) {
            sky_error_raise_string(sky_ValueError, "invalid format string");
        }
    }
    *format = sky_object_build_skip(&((*format)[1]));

    return list;
}


static sky_set_t
sky_object_build_set(const char **format, va_list ap)
{
    sky_set_t   set;

    if (!(*format = sky_object_build_skip(&((*format)[1])))) {
        sky_error_raise_string(sky_ValueError, "invalid format string");
    }
    set = sky_set_create(NULL);
    while ((*format)[0] != '>') {
        sky_set_add(set, sky_object_build_one(format, ap));
        if (!(*format = sky_object_build_skip(*format))) {
            sky_error_raise_string(sky_ValueError, "invalid format string");
        }
    }
    *format = sky_object_build_skip(&((*format)[1]));

    return set;
}


static sky_tuple_t
sky_object_build_tuple(const char **format, va_list ap)
{
    size_t          i, count;
    sky_tuple_t     tuple;
    sky_object_t    *objects;

    if (!(count = sky_object_build_count(*format))) {
        /* Skip over "(" and trailing whitespace */
        *format = sky_object_build_skip(&((*format)[1]));
        /* Skip over ")" and trailing whitespace */
        *format = sky_object_build_skip(&((*format)[1]));
        return sky_tuple_empty;
    }
    SKY_ASSET_BLOCK_BEGIN {
        objects = sky_asset_calloc(count,
                                   sizeof(sky_object_t),
                                   SKY_ASSET_CLEANUP_ON_ERROR);

        if (!(*format = sky_object_build_skip(&((*format)[1])))) {
            sky_error_raise_string(sky_ValueError, "invalid format string");
        }

        for (i = 0; (*format)[0] != ')'; ++i) {
            objects[i] = sky_object_build_one(format, ap);
            if (!(*format = sky_object_build_skip(*format))) {
                sky_error_raise_string(sky_ValueError, "invalid format string");
            }
        }
        *format = sky_object_build_skip(&((*format)[1]));

        tuple = sky_tuple_createwitharray(count, objects, sky_free);
    } SKY_ASSET_BLOCK_END;

    return tuple;
}


static sky_integer_t
sky_object_build_signed(const char **format, va_list ap)
{
    switch ((*format)[1]) {
        case 'j':
            ++(*format);
            return sky_integer_create(va_arg(ap, intmax_t));
        case 'z':
            ++(*format);
            return sky_integer_create(va_arg(ap, ssize_t));

        case '1':
            if ((*format)[2] == '6') {
                (*format) += 2;
                return sky_integer_create((int16_t)va_arg(ap, int));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '3':
            if ((*format)[2] == '2') {
                (*format) += 2;
                return sky_integer_create(va_arg(ap, int32_t));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '6':
            if ((*format)[2] == '4') {
                (*format) += 2;
                return sky_integer_create(va_arg(ap, int64_t));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '8':
            ++(*format);
            return sky_integer_create((int8_t)va_arg(ap, int));
    }

    return sky_integer_create(va_arg(ap, int));
}


static sky_integer_t
sky_object_build_unsigned(const char **format, va_list ap)
{
    switch ((*format)[1]) {
        case 'j':
            ++(*format);
            return sky_integer_createfromunsigned(va_arg(ap, uintmax_t));
        case 'z':
            ++(*format);
            return sky_integer_createfromunsigned(va_arg(ap, size_t));

        case '1':
            if ((*format)[2] == '6') {
                (*format) += 2;
                return sky_integer_createfromunsigned((uint16_t)va_arg(ap, unsigned int));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '3':
            if ((*format)[2] == '2') {
                (*format) += 2;
                return sky_integer_createfromunsigned(va_arg(ap, uint32_t));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '6':
            if ((*format)[2] == '4') {
                (*format) += 2;
                return sky_integer_createfromunsigned(va_arg(ap, uint64_t));
            }
            sky_error_raise_string(sky_ValueError, "invalid format string");
        case '8':
            ++(*format);
            return sky_integer_createfromunsigned((uint8_t)va_arg(ap, unsigned int));
    }

    return sky_integer_createfromunsigned(va_arg(ap, unsigned int));
}


static sky_object_t
sky_object_build_one(const char **format, va_list ap)
{
    void            *pointer;
    size_t          nbytes;
    const char      *name;
    const void      *bytes;
    sky_object_t    object;
    sky_string_t    encoding;

    switch ((*format)[0]) {
        case '[':
            return sky_object_build_list(format, ap);
        case '(':
            return sky_object_build_tuple(format, ap);
        case '{':
            return sky_object_build_dict(format, ap);
        case '<':
            return sky_object_build_set(format, ap);

        case 'O':
            object = va_arg(ap, sky_object_t);
            break;

        case 's':
            bytes = va_arg(ap, const void *);
            if ((*format)[1] != '#') {
                nbytes = strlen(bytes);
            }
            else {
                nbytes = va_arg(ap, size_t);
                ++(*format);
            }
            if (!bytes) {
                object = sky_None;
            }
            else {
                object = sky_string_createfrombytes(bytes, nbytes, NULL, NULL);
            }
            break;

        case 'e':
            ++(*format);
            encoding = va_arg(ap, sky_string_t);
            bytes = va_arg(ap, const void *);
            if ((*format)[2] != '#') {
                nbytes = strlen(bytes);
            }
            else {
                nbytes = va_arg(ap, size_t);
                ++(*format);
            }
            if (!bytes) {
                object = sky_None;
            }
            else {
                object = sky_string_createfrombytes(bytes,
                                                    nbytes,
                                                    encoding,
                                                    NULL);
            }
            break;

        case 'y':
            if ((*format)[1] != '#') {
                sky_error_raise_string(sky_ValueError, "invalid format string");
            }
            ++(*format);
            bytes  = va_arg(ap, const void *);
            nbytes = va_arg(ap, size_t);
            if (!bytes) {
                object = sky_None;
            }
            else {
                object = sky_bytes_createfrombytes(bytes, nbytes);
            }
            break;

        case 'Y':
            if ((*format)[1] != '#') {
                sky_error_raise_string(sky_ValueError, "invalid format string");
            }
            ++(*format);
            bytes  = va_arg(ap, const void *);
            nbytes = va_arg(ap, size_t);
            if (!bytes) {
                object = sky_None;
            }
            else {
                object = sky_bytearray_createfrombytes(bytes, nbytes);
            }
            break;

        case 'B':
            object = (va_arg(ap, int) ? sky_True : sky_False);
            break;
        case 'd':
            object = sky_float_create(va_arg(ap, double));
            break;
        case 'p':
            if (!(pointer = va_arg(ap, void *))) {
                object = sky_None;
            }
            else {
                object = sky_capsule_create(pointer, NULL, NULL, NULL);
            }
            break;
        case 'P':
            if (!(pointer = va_arg(ap, void *))) {
                object = sky_None;
                name = va_arg(ap, const char *);
            }
            else {
                name = va_arg(ap, const char *);
                object = sky_capsule_create(pointer, name, NULL, NULL);
            }
            break;

        case 'i':
            object = sky_object_build_signed(format, ap);
            break;
        case 'u':
            object = sky_object_build_unsigned(format, ap);
            break;

        default:
            sky_error_raise_string(sky_ValueError, "invalid format string");
    }
    ++(*format);
    return (object ? object : sky_None);
}


sky_object_t
sky_object_build(const char *format, ...)
{
    va_list         ap;
    sky_object_t    object;

    va_start(ap, format);
    object = sky_object_buildv(format, ap);
    va_end(ap);

    return object;
}


sky_object_t
sky_object_buildv(const char *format, va_list ap)
{
    sky_list_t      list;
    sky_object_t    object;

    if (!format) {
        return sky_None;
    }
    format = sky_object_build_skip(format);
    if (!*format) {
        return sky_None;
    }

    object = sky_object_build_one(&format, ap);
    format = sky_object_build_skip(format);
    if (!*format) {
        return object;
    }

    list = sky_list_create(NULL);
    sky_list_append(list, object);
    while (*format) {
        sky_list_append(list, sky_object_build_one(&format, ap));
        format = sky_object_build_skip(format);
    }

    return list;
}
