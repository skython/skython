/** @file
  * @brief
  * @defgroup sky_containers Containers
  * @{
  * @defgroup sky_mappingproxy Read-only Mapping Proxy
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MAPPINGPROXY_H__
#define __SKYTHON_CORE_SKY_MAPPINGPROXY_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** Create a new mappingproxy object instance.
  *
  * @param[in]  mapping     the mapping object to be proxied.
  * @return     a new mappingproxy object instance proxying @a mapping.
  */
sky_mappingproxy_t
sky_mappingproxy_create(sky_object_t mapping);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MAPPINGPROXY_H__ */

/** @} **/
/** @} **/
