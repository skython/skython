#include "sky_private.h"


typedef struct sky_filter_data_s {
    sky_object_t                        function;
    sky_object_t                        iterator;
} sky_filter_data_t;

SKY_EXTERN_INLINE sky_filter_data_t *
sky_filter_data(sky_object_t object)
{
    return sky_object_data(object, sky_filter_type);
}

struct sky_filter_s {
    sky_object_data_t                   object_data;
    sky_filter_data_t                   filter_data;
};


static void
sky_filter_instance_visit(
        SKY_UNUSED  sky_object_t                self,
                    void *                      data,
                    sky_object_visit_data_t *   visit_data)
{
    sky_filter_data_t   *self_data = data;

    sky_object_visit(self_data->function, visit_data);
    sky_object_visit(self_data->iterator, visit_data);
}


sky_filter_t
sky_filter_create(sky_object_t function, sky_object_t iterable)
{
    sky_filter_t        filter;
    sky_object_t        iterator;
    sky_filter_data_t   *filter_data;

    function = (sky_object_isnull(function) ? NULL : function);
    iterator = sky_object_iter(iterable);

    filter = sky_object_allocate(sky_filter_type);
    filter_data = sky_filter_data(filter);
    sky_object_gc_set(&(filter_data->function), function, filter);
    sky_object_gc_set(&(filter_data->iterator), iterator, filter);

    return filter;
}


void
sky_filter_init(sky_object_t self, sky_object_t function, sky_object_t iterable)
{
    sky_filter_data_t   *self_data = sky_filter_data(self);

    sky_object_t    iterator;

    function = (sky_object_isnull(function) ? NULL : function);
    iterator = sky_object_iter(iterable);

    sky_object_gc_set(&(self_data->function), function, self);
    sky_object_gc_set(&(self_data->iterator), iterator, self);
}


sky_object_t
sky_filter_iter(sky_filter_t self)
{
    return self;
}


sky_object_t
sky_filter_next(sky_filter_t self)
{
    sky_filter_data_t   *self_data = sky_filter_data(self);

    sky_object_t    object;

    if (!self_data->function) {
        while ((object = sky_object_next(self_data->iterator, NULL)) != NULL &&
               !sky_object_bool(object));
    }
    else {
        while ((object = sky_object_next(self_data->iterator, NULL)) != NULL &&
               !sky_object_bool(sky_object_call(self_data->function,
                                                sky_tuple_pack(1, object),
                                                NULL)));
    }

    return object;
}


sky_object_t
sky_filter_reduce(sky_filter_t self)
{
    sky_filter_data_t   *self_data = sky_filter_data(self);

    return sky_object_build("(O(OO))", sky_object_type(self),
                                       self_data->function,
                                       self_data->iterator);
}


static const char sky_filter_type_doc[] =
"filter(function or None, iterable) --> filter object\n\n"
"Return an iterator yielding those items of iterable for which function(item)\n"
"is true. If function is None, return the items that are true.";


SKY_TYPE_DEFINE_SIMPLE(filter,
                       "filter",
                       sizeof(sky_filter_data_t),
                       NULL,
                       NULL,
                       sky_filter_instance_visit,
                       0,
                       sky_filter_type_doc);


void
sky_filter_initialize_library(void)
{
    sky_type_initialize_builtin(sky_filter_type, 0);
    sky_type_setmethodslotwithparameters(
            sky_filter_type,
            "__init__",
            (sky_native_code_function_t)sky_filter_init,
            "self", SKY_DATA_TYPE_OBJECT, NULL,
            "function", SKY_DATA_TYPE_OBJECT | SKY_DATA_TYPE_OBJECT_OR_NONE, NULL,
            "iterable", SKY_DATA_TYPE_OBJECT, NULL,
            NULL);
    sky_type_setmethodslots(sky_filter_type,
            "__iter__", sky_filter_iter,
            "__next__", sky_filter_next,
            "__reduce__", sky_filter_reduce,
            NULL);
}
