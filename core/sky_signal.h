/** @file
  * @brief
  * @defgroup sky_signal Signals
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_SIGNAL_H__
#define __SKYTHON_CORE_SKY_SIGNAL_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The signature of a function to be called to handle a signal.
  *
  * @param[in]  signal      the signal that was raised.
  * @param[in]  object      the object registered with the signal handler.
  */
typedef void (*sky_signal_handler_t)(int signal, sky_object_t object);


/** Check for pending signals that need processing.
  * If there are any signals that have been delivered to the process, their
  * handlers will be run. This function does nothing on threads other than the
  * main thread.
  */
SKY_EXTERN void
sky_signal_check_pending(void);


/** Restore all signal handlers to the defaults that were set before Skython
  * set any of them.
  * Skython does not remember that signals were restored by calling this
  * function. If signals are changed after calling this, but before
  * sky_library_finalize() is called, sky_library_finalize() will "restore"
  * them again. This is primarily to be used in the child process after calling
  * fork(), but before calling exec().
  */
SKY_EXTERN void
sky_signal_restore_defaults(void);


/** Return the handler registered for a signal.
  *
  * @param[in]  signal      the signal number for which the handler is to be
  *                         returned.
  * @param[out] object      the object that was registered with the signal
  *                         handler.
  * @return     the signal handler registered for @a signal, which will be one
  *             of @c SIG_DFL, @c SIG_IGN, or a function pointer set via
  *             sky_signal_sethandler().
  */
SKY_EXTERN sky_signal_handler_t
sky_signal_handler(int signal, sky_object_t *object);


/** Set a handler for a signal.
  * Signal handlers are only ever called on the main thread, and they are never
  * called asynchronously. The signal machinery rememebers when a signal is
  * delivered asynchronously, but it waits to run the signal handler until a
  * safe time when sky_signal_check_pending() is called.
  *
  * @param[in]  signal      the signal number for which the handler is to be
  *                         set.
  * @param[in]  handler     the function to call to handle the signal,
  *                         @c SIG_IGN to ignore the signal, or @c SIG_DFL to
  *                         perform the default action (per the operating
  *                         system).
  * @param[in]  object      an object to pass to @a handler when it is called.
  */
SKY_EXTERN void
sky_signal_sethandler(int                   signal,
                      sky_signal_handler_t  handler,
                      sky_object_t          object);


/** Set the file descriptor to be written to when a signal is delivered.
  * When a signal is delivered to a process, the file descriptor specified will
  * have a single byte written to it that is the number of the signal that was
  * delivered. This can only be done for signals that are registered via
  * sky_signal_sethandler() and are neither @c SIG_DFL nor @c SIG_IGN.
  *
  * @param[in]  fd          the file descriptor to write to, or -1 to disable
  *                         the write.
  */
SKY_EXTERN void
sky_signal_setwakeupfd(int fd);


/** Trigger the receipt of a signal in the main thread.
  * This performs the same action as the actual receipt of the specified
  * signal. That is, it records the receipt of the signal and writes to the
  * signal file descriptor if set (see sky_signal_wakeupfd() for details).
  *
  * @param[in]  signal      the signal to trigger.
  */
SKY_EXTERN void
sky_signal_trigger(int signal);


/** Return the file descriptor that is written to when a signal is delivered.
  *
  * @return     the file descriptor to be written to, or -1 if there is no
  *             file descriptor set.
  */
SKY_EXTERN int
sky_signal_wakeupfd(void);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_SIGNAL_H__ */

/** @} **/
