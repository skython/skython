#include "sky_private.h"


#define sky_tlsdata_block_size          65536

/* Do not use sizeof(sky_tlsdata_t). Use sky_tlsdata_size instead. */
#define sky_tlsdata_size                \
        ((sizeof(sky_tlsdata_t) + SKY_CACHE_LINE_MASK) & ~SKY_CACHE_LINE_MASK)


pthread_key_t   sky_tlsdata_key;


static sky_atomic_lifo_t    sky_tlsdata_orphans =
       SKY_ATOMIC_LIFO_INITIALIZER(offsetof(sky_tlsdata_orphan_t, next));


static sky_tlsdata_block_t  *sky_tlsdata_blocks         = NULL;
static sky_tlsdata_block_t  *sky_tlsdata_blocks_tail    = NULL;

static sky_tlsdata_orphan_block_t   *sky_tlsdata_orphan_blocks      = NULL;
static sky_tlsdata_orphan_block_t   *sky_tlsdata_orphan_blocks_tail = NULL;


static sky_tlsdata_block_t *
sky_tlsdata_block_create(void)
{
    sky_tlsdata_block_t *block;

    block = sky_system_alloc(sky_tlsdata_block_size);
    sky_atomic_lifo_init(&(block->freelist), 0);
    block->total = (sky_tlsdata_block_size - sizeof(sky_tlsdata_block_t)) /
                   (sky_tlsdata_size + sizeof(sky_tlsdata_t *));
    block->used  = 0;

    if (!(block->next = sky_tlsdata_blocks)) {
        block->first_thread_id = 1;
    }
    else {
        block->first_thread_id = sky_tlsdata_blocks->first_thread_id +
                                 sky_tlsdata_blocks->total;
    }
    if (!sky_atomic_cas(block->next, block,
                        SKY_AS_VOIDP(&sky_tlsdata_blocks)))
    {
        sky_system_free(block, sky_tlsdata_block_size);
        return NULL;
    }
    *(block->next ? &(block->next->prev) : &(sky_tlsdata_blocks_tail)) = block;

    return block;
}


static sky_tlsdata_t *
sky_tlsdata_alloc(void)
{
    int32_t             used;
    size_t              header_size;
    sky_tlsdata_t       *tlsdata;
    sky_tlsdata_block_t *block;

    for (;;) {
        for (block = sky_tlsdata_blocks;  block;  block = block->next) {
            if ((tlsdata = sky_atomic_lifo_pop(&(block->freelist))) != NULL) {
                tlsdata->tlsdata_block = block;
                return tlsdata;
            }
            if ((used = block->used) < block->total) {
                if (sky_atomic_cas32(used, used + 1, &(block->used))) {
                    header_size = sizeof(sky_tlsdata_block_t) +
                                  (block->total * sizeof(sky_tlsdata_t *));
                    tlsdata = &(((sky_tlsdata_t *)((char *)block +
                                                   header_size))[used]);
                    tlsdata->tlsdata_block = block;
                    return tlsdata;
                }
            }
        }
        sky_tlsdata_block_create();
    }
}


static sky_tlsdata_orphan_block_t *
sky_tlsdata_orphan_block_create(void)
{
    size_t                      block_size = sky_system_pagesize();
    sky_tlsdata_orphan_block_t  *block;

    block = sky_system_alloc(block_size);
    sky_atomic_lifo_init(&(block->freelist), 0);
    block->total = (block_size - sizeof(sky_tlsdata_orphan_block_t)) /
                   sizeof(sky_tlsdata_orphan_t);
    block->used  = 0;
    block->next  = sky_tlsdata_orphan_blocks;

    if (!sky_atomic_cas(block->next, block,
                        SKY_AS_VOIDP(&sky_tlsdata_orphan_blocks)))
    {
        sky_system_free(block, block_size);
        return NULL;
    }
    *(block->next ? &(block->next->prev)
                  : &(sky_tlsdata_orphan_blocks_tail)) = block;

    return block;
}


static sky_tlsdata_orphan_t *
sky_tlsdata_orphan_alloc(void)
{
    int32_t                     used;
    sky_tlsdata_orphan_t        *orphan;
    sky_tlsdata_orphan_block_t  *block;

    for (;;) {
        for (block = sky_tlsdata_orphan_blocks; block; block = block->next) {
            if ((orphan = sky_atomic_lifo_pop(&(block->freelist))) != NULL) {
                memset(orphan, 0, sizeof(sky_tlsdata_orphan_t));
                orphan->orphan_block = block;
                return orphan;
            }
            if ((used = block->used) < block->total) {
                if (sky_atomic_cas32(used, used + 1, &(block->used))) {
                    orphan = &(block->slots[used]);
                    memset(orphan, 0, sizeof(sky_tlsdata_orphan_t));
                    orphan->orphan_block = block;
                    return orphan;
                }
            }
        }
        sky_tlsdata_orphan_block_create();
    }
}


static void
sky_tlsdata_orphan_free(sky_tlsdata_orphan_t *orphan)
{
    sky_atomic_lifo_push(&(orphan->orphan_block->freelist), orphan);
}


sky_bool_t
sky_tlsdata_adopt(void)
{
    sky_tlsdata_t           *tlsdata;
    sky_tlsdata_orphan_t    *orphan;

    if ((orphan = sky_atomic_lifo_pop(&sky_tlsdata_orphans)) != NULL) {
        tlsdata = sky_tlsdata_get();

        do {
            if (orphan->malloc_tlsdata) {
                if (!tlsdata->malloc_tlsdata) {
                    tlsdata->malloc_tlsdata = orphan->malloc_tlsdata;
                    orphan->malloc_tlsdata = NULL;
                }
                else {
                    sky_core_malloc_tlsdata_adopt(tlsdata->malloc_tlsdata,
                                                  orphan->malloc_tlsdata);
                }
            }
            sky_object_tlsdata_adopt(&(tlsdata->object_tlsdata),
                                     &(orphan->object_tlsdata));
            sky_hazard_pointer_tlsdata_adopt(&(tlsdata->hazard_pointer_tlsdata),
                                             &(orphan->hazard_pointer_tlsdata));
            sky_tlsdata_orphan_free(orphan);
        } while ((orphan = sky_atomic_lifo_pop(&sky_tlsdata_orphans)) != NULL);

        return SKY_TRUE;
    }

    return SKY_FALSE;
}


static void
sky_tlsdata_cleanup(void *data)
{
    sky_tlsdata_t   *tlsdata = data;

    void                    *original_data;
    size_t                  slot_index;
    sky_error_asset_t       *asset;
    sky_tlsdata_orphan_t    *orphan;

    tlsdata->flags &= ~SKY_TLSDATA_FLAG_ALIVE;
    original_data = pthread_getspecific(sky_tlsdata_key);
    pthread_setspecific(sky_tlsdata_key, data);

    /* Clear out user tls data first. It can cause callbacks into external
     * code, which could do any manner of things.
     */
    sky_tls_cleanup_tlsdata(&(tlsdata->tls_tlsdata));

    /* Clear out the tlsdata slot for the thread immediately so that no other
     * thread can get a pointer to it.
     */
    slot_index = tlsdata->thread_id - tlsdata->tlsdata_block->first_thread_id;
    tlsdata->tlsdata_block->slots[slot_index] = NULL;

    /* Force a hazard pointer scan so that everything that we can cleanup now
     * will be. Later, sky_hazard_pointer_tlsdata_cleanup() will do a scan too,
     * but ideally it should have nothing to retire, because that could affect
     * things like object roots.
     */
    sky_hazard_pointer_scan();

    /* Clean up everything else. Order is important here. Object data should
     * be done first, followed by hazard pointer data. The hazard pointer
     * clean up will always leave behind a retiree for the tlsdata itself to
     * be orphaned. Finally, the malloc data can be cleaned up and possibly
     * orphaned. Since we know that the hazard pointer is always going to
     * leave behind an orphan, there's no complicated deciding whether to
     * create an orphan structure or not. While all of this isn't particularly
     * pretty, it's much less fragile than past iterations of this code.
     */
    orphan = sky_tlsdata_orphan_alloc();

    if (tlsdata->thread_self) {
        sky_object_gc_setlibraryroot(SKY_AS_OBJECTP(&(tlsdata->thread_self)),
                                     NULL,
                                     SKY_FALSE);
    }
    if (tlsdata->trace_function) {
        sky_object_gc_setroot(&(tlsdata->trace_function), NULL, SKY_FALSE);
    }
    if (tlsdata->current_frame) {
        sky_object_gc_setroot(SKY_AS_OBJECTP(&(tlsdata->current_frame)),
                              NULL,
                              SKY_FALSE);
    }
    sky_object_tlsdata_cleanup(&(tlsdata->object_tlsdata),
                               &(orphan->object_tlsdata),
                               SKY_FALSE);

    sky_hazard_pointer_tlsdata_cleanup(&(tlsdata->hazard_pointer_tlsdata),
                                       &(orphan->hazard_pointer_tlsdata),
                                       tlsdata,
                                       sky_tlsdata_retire);

    /* The second pass here shouldn't run any external code. It should only
     * update the orphan structure with anything that got added due to hazard
     * pointers getting cleaned up. Therefore, no new hazard pointer retires
     * should transpire. It is possible for new hazard pointers to get created
     * and released, leaving unused_hazard_pointers != NULL. This is due to
     * the sky_object atfork table, which is built on sky_hashtable.
     */
    sky_object_tlsdata_cleanup(&(tlsdata->object_tlsdata),
                               &(orphan->object_tlsdata),
                               SKY_TRUE);
    sky_error_validate_debug(!tlsdata->hazard_pointer_tlsdata.active_hazard_count);
    sky_error_validate_debug(!tlsdata->hazard_pointer_tlsdata.retired_pointer_count);
    sky_error_validate_debug(!tlsdata->hazard_pointer_tlsdata.retired_pointers);
    if (tlsdata->hazard_pointer_tlsdata.unused_hazard_pointers) {
        sky_hazard_pointer_tlsdata_cleanup(&(tlsdata->hazard_pointer_tlsdata),
                                           &(orphan->hazard_pointer_tlsdata),
                                           NULL,
                                           NULL);
    }
    sky_error_validate_debug(!tlsdata->hazard_pointer_tlsdata.unused_hazard_pointers);

    while ((asset = tlsdata->error_asset_freelist) != NULL) {
        tlsdata->error_asset_freelist = asset->next;
        sky_free(asset);
    }

    sky_thread_wait_cleanup(&(tlsdata->wait));

    if (tlsdata->malloc_tlsdata) {
        sky_core_malloc_tlsdata_cleanup(tlsdata->malloc_tlsdata,
                                        &(orphan->malloc_tlsdata));
    }

    if (tlsdata->flags & SKY_TLSDATA_FLAG_ATFORK) {
        sky_atfork_tlsdata_cleanup(&(tlsdata->atfork_tlsdata));
        tlsdata->flags &= ~SKY_TLSDATA_FLAG_ATFORK;
    }

    sky_atomic_lifo_push(&sky_tlsdata_orphans, orphan);
    pthread_setspecific(sky_tlsdata_key, original_data);
}


void
sky_tlsdata_atfork_child(void)
{
    int32_t             i;
    sky_tlsdata_t       *tlsdata;
    sky_tlsdata_block_t *block;

    /* Update this thread's pthread_self pointer. It's probably the same as
     * it was in the parent, but it's entirely implementation dependent, so it
     * could very well be different.
     */
    tlsdata = sky_tlsdata_get();
    tlsdata->pthread_self = pthread_self();
    tlsdata->atfork_tlsdata.state = SKY_ATFORK_STATE_IDLE;

    /* Re-initialize the thread's wait mutex. See the comments in sky_atfork.c
     * for re-initialization of sky_atfork_mutex for details.
     */
    memset(&(tlsdata->wait.mutex), 0, sizeof(tlsdata->wait.mutex));
    pthread_mutex_init(&(tlsdata->wait.mutex), NULL);
    memset(&(tlsdata->wait.cond), 0, sizeof(tlsdata->wait.cond));
    pthread_cond_init(&(tlsdata->wait.cond), NULL);

    /* Run the object atfork handlers. */
    for (block = sky_tlsdata_blocks_tail;  block;  block = block->prev) {
        for (i = block->used;  i >= 0;  --i) {
            if (block->slots[i - 1]) {
                sky_object_atfork_child(&(block->slots[i - 1]->object_tlsdata));
            }
        }
    }

    /* Scan through all of the tlsdata that's active, and clean it up for each
     * and every thread that is not this one. Any threads that were running at
     * the time of the fork are no longer present in the child.
     */
    for (block = sky_tlsdata_blocks;  block;  block = block->next) {
        for (i = 0;  i < block->used;  ++i) {
            if (block->slots[i] && block->slots[i] != tlsdata) {
                block->slots[i]->atfork_tlsdata.state = SKY_ATFORK_STATE_IDLE;
                sky_tlsdata_cleanup(block->slots[i]);
            }
        }
    }
}


void
sky_tlsdata_atfork_parent(void)
{
    int32_t             i;
    sky_tlsdata_block_t *block;

    for (block = sky_tlsdata_blocks_tail;  block;  block = block->prev) {
        for (i = block->used;  i >= 0;   --i) {
            if (block->slots[i - 1]) {
                sky_object_atfork_parent(&(block->slots[i - 1]->object_tlsdata));
            }
        }
    }
}


void
sky_tlsdata_atfork_prepare(void)
{
    int32_t             i;
    sky_tlsdata_block_t *block;

    for (block = sky_tlsdata_blocks;  block;  block = block->next) {
        for (i = 0;  i < block->used;  ++i) {
            if (block->slots[i]) {
                sky_object_atfork_prepare(&(block->slots[i]->object_tlsdata));
            }
        }
    }
}


sky_tlsdata_t *
sky_tlsdata_create(void)
{
    size_t              header_size, slot_index;
    sky_tlsdata_t       *tlsdata;
    sky_tlsdata_block_t *block;

    if (unlikely(!(tlsdata = sky_tlsdata_alloc()))) {
        sky_error_fatal("out of memory");
    }

    block = tlsdata->tlsdata_block;
    memset(tlsdata, 0x00, sky_tlsdata_size);

    header_size = sizeof(sky_tlsdata_block_t) +
                  (block->total * sizeof(sky_tlsdata_t *));
    slot_index  = ((char *)tlsdata - ((char *)block + header_size)) /
                  sky_tlsdata_size;
#if defined(SKY_ARCH_64BIT)
    sky_error_validate_debug(slot_index <= UINT32_MAX);
#endif

    tlsdata->flags         = SKY_TLSDATA_FLAG_ALIVE | SKY_TLSDATA_FLAG_TRACING;
    tlsdata->tlsdata_block = block;
    tlsdata->thread_id     = block->first_thread_id + (uint32_t)slot_index;
    tlsdata->pthread_self  = pthread_self();
    sky_thread_wait_init(&(tlsdata->wait));

    return block->slots[slot_index] = tlsdata;
}


sky_hazard_pointer_t *
sky_tlsdata_getid(uint32_t id)
{
    sky_tlsdata_block_t     *block;
    sky_hazard_pointer_t    *hazard;

    sky_error_validate_debug(id > 0);

    for (block = sky_tlsdata_blocks;  block;  block = block->next) {
        if (id >= block->first_thread_id) {
            id -= block->first_thread_id;
            hazard = sky_hazard_pointer_acquire(
                            SKY_AS_TYPE(&(block->slots[id]), void **));
            if (hazard && !hazard->pointer) {
                sky_hazard_pointer_release(hazard);
                return NULL;
            }
            return hazard;
        }
    }

    return NULL;
}


uint32_t
sky_tlsdata_highestid(void)
{
    sky_tlsdata_block_t *block;

    if (unlikely(!(block = sky_tlsdata_blocks))) {
        return 0;
    }
    return block->first_thread_id + block->used - 1;
}


void
sky_tlsdata_retire(void *arg)
{
    sky_tlsdata_t       *tlsdata = arg;
    sky_tlsdata_block_t *block   = tlsdata->tlsdata_block;

    memset(tlsdata, 0x00, sky_tlsdata_size);
    sky_atomic_lifo_push(&(block->freelist), arg);
}


void
sky_tlsdata_finalize(void)
{
    sky_tlsdata_t           *tlsdata;
    sky_tlsdata_orphan_t    *orphan;

    if ((tlsdata = pthread_getspecific(sky_tlsdata_key)) != NULL) {
        sky_tlsdata_adopt();
        sky_tlsdata_cleanup(tlsdata);

        if ((orphan = sky_atomic_lifo_pop(&(sky_tlsdata_orphans))) != NULL) {
            /* There is almost certainly a bunch of leaked memory here. The
             * object tlsdata portion should be empty at this point, having
             * all been properly garbage collected in the object finalization
             * code. There will, of course, be tons of hazard pointer related
             * stuff here, because that stuff never gets freed by design. As
             * for core malloc data, there's likely some of that hanging around
             * too, and at some point should be checked out for memory leaks.
             */
        }
    }
}


void
sky_tlsdata_initialize(void)
    /* This is called by library initialization as a shared object constructor.
     * Therefore, it MUST NOT call into sky_malloc() AT ALL.
     */
{
    if (unlikely(pthread_key_create(&sky_tlsdata_key, sky_tlsdata_cleanup))) {
        sky_error_fatal("pthread_key_create() failure");
    }
}
