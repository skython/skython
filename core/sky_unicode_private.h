#ifndef __SKYTHON_CORE_SKY_UNICODE_PRIVATE_H__
#define __SKYTHON_CORE_SKY_UNICODE_PRIVATE_H__ 1


#include "sky_base.h"
#include "sky_error.h"
#include "sky_unicode.h"


SKY_CDECLS_BEGIN


typedef enum sky_unicode_ctype_flag_e {
    SKY_UNICODE_CTYPE_FLAG_ALPHA            =   0x000001,
    SKY_UNICODE_CTYPE_FLAG_LOWER            =   0x000002,
    SKY_UNICODE_CTYPE_FLAG_TITLE            =   0x000004,
    SKY_UNICODE_CTYPE_FLAG_UPPER            =   0x000008,

    SKY_UNICODE_CTYPE_FLAG_DECIMAL          =   0x000010,
    SKY_UNICODE_CTYPE_FLAG_DIGIT            =   0x000020,
    SKY_UNICODE_CTYPE_FLAG_INTEGRAL         =   0x000040,
    SKY_UNICODE_CTYPE_FLAG_FRACTIONAL       =   0x000080,
    SKY_UNICODE_CTYPE_FLAG_NUMERIC          =   0x0000F0,

    SKY_UNICODE_CTYPE_FLAG_SPACE            =   0x000100,
    SKY_UNICODE_CTYPE_FLAG_PRINTABLE        =   0x000200,
    SKY_UNICODE_CTYPE_FLAG_LINEBREAK        =   0x000400,

    SKY_UNICODE_CTYPE_FLAG_XID_START        =   0x001000,
    SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE     =   0x002000,

    SKY_UNICODE_CTYPE_FLAG_CASEFOLD_LOWER   =   0x010000,
    SKY_UNICODE_CTYPE_FLAG_CASEFOLD_TITLE   =   0x020000,
    SKY_UNICODE_CTYPE_FLAG_CASEFOLD_UPPER   =   0x040000,
    SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL =   0x080000,
    SKY_UNICODE_CTYPE_FLAG_CASEFOLD_MASK    =   0x0F0000,

    SKY_UNICODE_CTYPE_FLAG_MIRRORED         =   0x100000,
} sky_unicode_ctype_flag_t;


typedef struct sky_unicode_ctype_s {
    unsigned int                        flags;
    sky_unicode_char_t                  simple_lower;
    sky_unicode_char_t                  simple_title;
    sky_unicode_char_t                  simple_upper;

    sky_unicode_char_t                  special_lower[3];
    sky_unicode_char_t                  special_title[3];
    sky_unicode_char_t                  special_upper[3];
    sky_unicode_char_t                  casefold[3];

    /* numeric value must be 64 bits due to U+5146 (1,000,000,000,000) */
    union {
        struct {
            int32_t                     numerator;
            int32_t                     denominator;
        } fraction;
        int64_t                         integer;
    } numeric;

    unsigned char                       category;
    unsigned char                       bidirectional;
    unsigned char                       east_asian_width;
} sky_unicode_ctype_t;


typedef struct sky_unicode_ctype_data_s {
    sky_unicode_ctype_t *               data;
    int                                 shift;
    int16_t *                           index_1;
    int16_t *                           index_2;
    sky_unicode_ctype_t *               latin1_data;
} sky_unicode_ctype_data_t;

SKY_EXTERN const sky_unicode_ctype_data_t * const sky_unicode_ctype_tables;


static inline sky_unicode_ctype_t *
sky_unicode_ctype_lookup(sky_unicode_char_t c)
{
    const sky_unicode_ctype_data_t  *table = sky_unicode_ctype_tables;

    int index, shift = table->shift;

    sky_error_validate_debug(c >= SKY_UNICODE_CHAR_MIN);
    sky_error_validate_debug(c <= SKY_UNICODE_CHAR_MAX);
    index = table->index_1[c >> shift] << shift;
    index = table->index_2[index + (c & ((1 << shift) - 1))];
    return &(table->data[index]);
}

static inline sky_unicode_ctype_t *
sky_unicode_ctype_lookup_latin1(sky_unicode_char_t c)
{
    sky_error_validate_debug(c >= 0);
    sky_error_validate_debug(c <= 255);
    return &(sky_unicode_ctype_tables->latin1_data[c]);
}


enum sky_unicode_decomp_type_e {
    SKY_UNICODE_DECOMP_TYPE_CANONICAL   =   0x00,

    SKY_UNICODE_DECOMP_TYPE_FONT        =   0x01,
    SKY_UNICODE_DECOMP_TYPE_NOBREAK     =   0x02,
    SKY_UNICODE_DECOMP_TYPE_INITIAL     =   0x03,
    SKY_UNICODE_DECOMP_TYPE_MEDIAL      =   0x04,
    SKY_UNICODE_DECOMP_TYPE_FINAL       =   0x05,
    SKY_UNICODE_DECOMP_TYPE_ISOLATED    =   0x06,
    SKY_UNICODE_DECOMP_TYPE_CIRCLE      =   0x07,
    SKY_UNICODE_DECOMP_TYPE_SUPER       =   0x08,
    SKY_UNICODE_DECOMP_TYPE_SUB         =   0x09,
    SKY_UNICODE_DECOMP_TYPE_VERTICAL    =   0x0A,
    SKY_UNICODE_DECOMP_TYPE_WIDE        =   0x0B,
    SKY_UNICODE_DECOMP_TYPE_NARROW      =   0x0C,
    SKY_UNICODE_DECOMP_TYPE_SMALL       =   0x0D,
    SKY_UNICODE_DECOMP_TYPE_SQUARE      =   0x0E,
    SKY_UNICODE_DECOMP_TYPE_FRACTION    =   0x0F,
    SKY_UNICODE_DECOMP_TYPE_COMPAT      =   0x10,
    SKY_UNICODE_DECOMP_TYPE_COMPAT_MASK =   0x1F,

    SKY_UNICODE_DECOMP_TYPE_NFC_QC_MASK  =  0x0060,
    SKY_UNICODE_DECOMP_TYPE_NFD_QC_MASK  =  0x0180,
    SKY_UNICODE_DECOMP_TYPE_NFKC_QC_MASK =  0x0600,
    SKY_UNICODE_DECOMP_TYPE_NFKD_QC_MASK =  0x1800,
} sky_unicode_decomp_type_t;


typedef struct sky_unicode_decomp_s {
    uint32_t                            offset;
    uint8_t                             length;
    uint8_t                             ccc;
    uint16_t                            flags;
} sky_unicode_decomp_t;


typedef struct sky_unicode_decomp_data_s {
    sky_unicode_char_t *                data;
    sky_unicode_decomp_t *              table;
    int                                 shift;
    int16_t *                           index_1;
    int16_t *                           index_2;
    int64_t *                           compose_table;
    int                                 compose_shift;
    int16_t *                           compose_index_1;
    int16_t *                           compose_index_2;
} sky_unicode_decomp_data_t;

SKY_EXTERN const sky_unicode_decomp_data_t * const sky_unicode_decomp_tables;


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_UNICODE_PRIVATE_H__ */
