/** @file
  * @brief Serialization for internal objects.
  * This is not for general persistence. For general persistence and transfer
  * of objects through RPC calls, see the modules @c pickle and @c shelve. This
  * module exists mainly to support reading and writing code objects. Not all
  * object types can be serialized with this API. The following types are
  * supported: booleans, integers, floating point numbers, complex numbers,
  * strings, bytes, bytearrays, tuples, lists, sets, frozensets, dictionaries,
  * slices, and code objects. The singletons @c sky_None, @c sky_Ellipsis, and
  * @c sky_NotImplemented are also supported. Recursive objects are also not
  * supported (i.e., objects that contain references to themselves).
  *
  * @note   The format is intentionally undocumented due to the very limited
  *         purpose for which this module is intended. The format is
  *         compatible with other implementations of Python.
  *
  * @warning    This API is not intended to be secure against erroneous or
  *             maliciously constructed data. Never unmarshal data received
  *             from an untrusted or unauthenticated source.
  *
  * @defgroup sky_object Objects
  * @{
  * @defgroup sky_marshal Object Marshalling
  * @{
  */

#ifndef __SKYTHON_CORE_SKY_MARSHAL_H__
#define __SKYTHON_CORE_SKY_MARSHAL_H__ 1


#include "sky_base.h"
#include "sky_object.h"


SKY_CDECLS_BEGIN


/** The highest format version supported. **/
#define SKY_MARSHAL_FORMAT_VERSION      2


/** The signature of a function to be called to output serialized data.
  *
  * @param[in]  arg     the arbitrary pointer argument passed to
  *                     sky_marshal_dumpwithcallback().
  * @param[in]  bytes   the bytes to be output.
  * @param[in]  nbytes  the number of bytes contained in @a bytes.
  */
typedef void
        (*sky_marshal_dump_t)(void *        arg,
                              const void *  bytes,
                              ssize_t       nbytes);

/** Serialize an object.
  * Serialization is deep. That is, all objects to which an object refers are
  * serialized along with the object. Recursive objects are not supported.
  *
  * @param[in]  object      the object to serialize.
  * @param[in]  version     the serialization format version to use, which may
  *                         be 0 to use the most current version.
  * @param[in]  callback    the function to call to output serialized data.
  * @param[in]  arg         an arbitrary pointer to pass through to @a callback
  *                         when it is called.
  */
SKY_EXTERN void
sky_marshal_dumpwithcallback(sky_object_t       object,
                             uint32_t           version,
                             sky_marshal_dump_t callback,
                             void *             arg);


/** Serialize an object to a file or file-like object.
  *
  * @param[in]  object      the object to serialize.
  * @param[in]  file        the file to write output to.
  * @param[in]  version     the serialization format version to use, which may
  *                         be 0 to use the most current version.
  */
SKY_EXTERN void
sky_marshal_dump(sky_object_t   object,
                 sky_object_t   file,
                 uint32_t       version);


/** Serialize an object to a bytes object.
  *
  * @param[in]  object      the object to serialize.
  * @param[in]  version     the serialization format version to use, which may
  *                         be 0 to use the most current version.
  * @return     a bytes object instance that contains the serialized data.
  */
SKY_EXTERN sky_bytes_t
sky_marshal_dumps(sky_object_t  object,
                  uint32_t      version);


/** The signature of a function to be called to obtain data for loading.
  * Calls will often be made for small amounts of data at a time; therefore,
  * implementations should take care not to do too much work in each call.
  * Each call will always request the amount of data that is actually needed,
  * so it is an error (premature end of input) if the number of bytes returned
  * is less than the number of bytes requested. It is also an error to return
  * more data than requested. Implementations are expected to maintain memory
  * for the bytes returned.
  *
  * @param[in]  arg     the arbitrary pointer argument passed to
  *                     sky_marshal_loadwithcallback().
  * @param[in]  bytes   an array of bytes into which the requested data is to
  *                     be copied.
  * @param[in]  nbytes  the number of bytes requested.
  * @return     the number of bytes actually read. If less than @a nbytes, end
  *             of file is assumed, and an error will likely result.
  */
typedef ssize_t
        (*sky_marshal_load_t)(void *    arg,
                              void *    bytes,
                              ssize_t   nbytes);


/** Deserialize an object.
  * The @a source may be any object supporting the buffer protocol, or it may
  * be a readable file object opened in binary mode. A single object is read
  * from the source. Anything extra is ignored.
  *
  * @param[in]  callback    the function to call to obtain data as needed.
  * @param[in]  arg         an arbitrary pointer to pass through to @a callback
  *                         when it is called.
  * @return     the deserialized object, or @c NULL if no serialized object
  *             was found.
  */
SKY_EXTERN sky_object_t
sky_marshal_loadwithcallback(sky_marshal_load_t callback, void *arg);


/** Deserialize an object from a file or file-like object.
  *
  * @param[in]  file        the file from which data is to be read.
  * @return     the deserialized object, or @c NULL if no serialized object
  *             was found.
  */
SKY_EXTERN sky_object_t
sky_marshal_load(sky_object_t file);


/** Deserialize an object from a bytes or bytes-like object.
  * 
  * @param[in]  string      the object to provide data to be deserialized,
  *                         which may be any type supporting the buffer
  *                         protocol.
  * @return     the deserialized object, or @c NULL if no serialized object
  *             was found.
  */
SKY_EXTERN sky_object_t
sky_marshal_loads(sky_object_t string);


SKY_CDECLS_END


#endif  /* __SKYTHON_CORE_SKY_MARSHAL_H__ */

/** @} **/
/** @} **/
