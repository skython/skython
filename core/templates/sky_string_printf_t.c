#if SKY_STRING_WIDTH == 1
#   define F    latin1
#   define T    uint8_t
#elif SKY_STRING_WIDTH == 2
#   define F    ucs2
#   define T    uint16_t
#elif SKY_STRING_WIDTH == 4
#   define F    ucs4
#   define T    sky_unicode_char_t
#else
#   error implementation missing
#endif


#if !defined(__SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__)
#define __SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__ 1

static sky_object_t
sky_string_printf_argument(sky_tuple_t values, ssize_t index)
{
    sky_object_t    value;

    SKY_ERROR_TRY {
        value = sky_tuple_get(values, index);
    }
    SKY_ERROR_EXCEPT(sky_IndexError) {
        value = NULL;
    } SKY_ERROR_TRY_END;

    if (!value) {
        sky_error_raise_string(sky_TypeError,
                               "not enough arguments for format string");
    }
    return value;
}

static sky_object_t
sky_string_printf_keyword(sky_object_t mapping, sky_string_t key)
{
    sky_object_t    value;

    SKY_ERROR_TRY {
        value = sky_object_getitem(mapping, key);
    } SKY_ERROR_EXCEPT(sky_TypeError) {
        value = NULL;
    } SKY_ERROR_TRY_END;

    if (!value) {
        sky_error_raise_string(sky_TypeError, "format requires a mapping");
    }
    return value;
}

#endif  /* __SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__ */


sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_printf_, F)(
        sky_string_t                format,
        const sky_string_data_t *   format_data,
        sky_object_t                values)
{
    ssize_t                     next_value;
    const T                     *c, *end, *start;
    sky_bool_t                  tuplify;
    sky_object_t                object, value;
    sky_string_t                key, new_string;
    sky_string_data_t           *new_string_data;
    sky_unicode_char_t          codepoint;
    sky_format_specification_t  spec;

    tuplify = SKY_TRUE;
    start = format_data->data.F;
    end = start + format_data->len;

    /* Find the first '%' character. */
    for (c = start; c < end && '%' != *c; ++c);
    if (c >= end) {
        return sky_string_copy(format);
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);

    next_value = 0;

    do {
        if (c > start) {
            sky_string_data_append(new_string_data,
                                   start,
                                   (c - start) * SKY_STRING_WIDTH,
                                   SKY_STRING_WIDTH);
        }

        /* 1. The '%' character, which marks the start of the specifier. */
        ++c;
        value = NULL;

        /* 2. Mapping key (optional), consisting of a parenthesised sequence of
         *    characters (for example, "(somename)").
         */
        if ('(' == *c) {
            for (start = ++c; c < end && ')' != *c; ++c);
            if (c >= end) {
                sky_error_raise_string(sky_ValueError,
                                       "incomplete format key");
            }
            key = sky_string_createfromcodepoints(
                            start,
                            (c - start) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
            tuplify = SKY_FALSE;
            value = sky_string_printf_keyword(values, key);
            ++c;
        }

        /* 3. Conversion flags (optional), which affect the result of some
         *    conversion types.
         */
        spec.fill = ' ';
        spec.flags = SKY_FORMAT_FLAG_ALIGN_RIGHT |
                     SKY_FORMAT_FLAG_SIGN_MINUS;
        while (c < end) {
            if ('#' == *c) {
                spec.flags |= SKY_FORMAT_FLAG_ALTERNATE_FORM;
            }
            else if ('0' == *c) {
                spec.flags |= SKY_FORMAT_FLAG_ZERO_PAD;
            }
            else if ('-' == *c) {
                spec.flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_ALIGN_LEFT |
                              SKY_FORMAT_FLAG_SPECIFIED_ALIGN;
            }
            else if (' ' == *c) {
                spec.flags &= ~SKY_FORMAT_FLAG_SIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_SIGN_SPACE;
            }
            else if ('+' == *c) {
                spec.flags &= ~SKY_FORMAT_FLAG_SIGN_MASK;
                spec.flags |= SKY_FORMAT_FLAG_SIGN_PLUS;
            }
            else {
                break;
            }
            ++c;
        }

        /* 4. Minimum field width (optional). If specified as an '*' (asterisk),
         *    the actual width is read from the next element of the tuple in
         *    values, and the object to convert comes after the minimum field
         *    width and optional precision.
         */
        spec.precision = 0;
        if (c < end) {
            if ('*' == *c) {
                spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_WIDTH;
                if (tuplify && !sky_object_isa(values, sky_tuple_type)) {
                    values = sky_tuple_pack(1, values);
                    tuplify = SKY_FALSE;
                }
                object = sky_string_printf_argument(values, next_value++);
                spec.width = sky_integer_value(sky_number_index(object),
                                               SSIZE_MIN,
                                               SSIZE_MAX,
                                               NULL);
                ++c;
            }
            else if (sky_unicode_isdigit(*c)) {
                spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_WIDTH;
                spec.width = 0;
                for (; c < end && sky_unicode_isintegral(*c); ++c) {
                    spec.width = (spec.width * 10) + sky_unicode_integer(*c);
                }
            }
        }

        /* 5. Precision (optional), given as a '.' (dot) followed by the
         * precision. If specified as '*' (an asterisk), the actual precision
         * is read from the next element of the tuple in values, and the value
         * to convert comes after the precision.
         */
        if (c + 1 < end && '.' == *c) {
            ++c;
            if ('*' == *c) {
                spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_PRECISION;
                if (tuplify && !sky_object_isa(values, sky_tuple_type)) {
                    values = sky_tuple_pack(1, values);
                    tuplify = SKY_FALSE;
                }
                object = sky_string_printf_argument(values, next_value++);
                spec.precision = sky_integer_value(sky_number_index(object),
                                                   SSIZE_MIN,
                                                   SSIZE_MAX,
                                                   NULL);
                ++c;
            }
            else {
                spec.flags |= SKY_FORMAT_FLAG_SPECIFIED_PRECISION;
                spec.precision = 0;
                for (; c < end && sky_unicode_isintegral(*c); ++c) {
                    spec.precision = (spec.precision * 10) +
                                     sky_unicode_integer(*c);
                }
            }
        }

        /* 6. Length modifier (optional). */
        if (c < end && ('h' == *c || 'l' == *c || 'L' == *c)) {
            ++c;
        }

        /* 7. Conversion type. */
        if (c >= end) {
            sky_error_raise_string(sky_ValueError, "incomplete format");
        }
        if (!value && *c != '%') {
            if (tuplify && !sky_object_isa(values, sky_tuple_type)) {
                values = sky_tuple_pack(1, values);
                tuplify = SKY_FALSE;
            }
            value = sky_string_printf_argument(values, next_value++);
        }
        switch (*c) {
            case 'd': case 'i': case 'u':
                spec.type = 'd';
                sky_string_printf_format_integer(new_string_data,
                                                 sky_number_integer(value),
                                                 &spec);
                break;
            case 'o':
            case 'x': case 'X':
                spec.type = *c;
                sky_string_printf_format_integer(new_string_data,
                                                 sky_number_integer(value),
                                                 &spec);
                break;
            case 'e': case 'E':
            case 'f': case 'F':
            case 'g': case 'G':
                spec.type = *c;
                sky_string_printf_format_float(new_string_data,
                                               sky_number_float(value),
                                               &spec);
                break;

            case 'c':
                spec.type = *c;
                if (sky_object_isa(value, sky_string_type)) {
                    if (sky_object_len(value) != 1) {
                        sky_error_raise_string(sky_TypeError,
                                               "%c requires int or char");
                    }
                }
                else {
                    SKY_ERROR_TRY {
                        value = sky_number_integer(value);
                        codepoint = sky_integer_value(value,
                                                      SKY_UNICODE_CHAR_MIN,
                                                      SKY_UNICODE_CHAR_MAX,
                                                      NULL);
                        value = sky_string_createfromcodepoint(codepoint);
                    } SKY_ERROR_EXCEPT(sky_OverflowError) {
                        sky_error_raise_string(sky_TypeError,
                                               "%c requires int or char");
                    } SKY_ERROR_TRY_END;
                }
                sky_string_printf_format_string(new_string_data,
                                                value,
                                                &spec);
                break;

            case 'r':
                spec.type = 's';
                sky_string_printf_format_string(new_string_data,
                                                sky_object_repr(value),
                                                &spec);
                break;

            case 's':
                spec.type = 's';
                sky_string_printf_format_string(new_string_data,
                                                sky_object_str(value),
                                                &spec);
                break;

            case 'a':
                spec.type = 's';
                sky_string_printf_format_string(new_string_data,
                                                sky_object_ascii(value),
                                                &spec);
                break;

            case '%':
                sky_string_data_append_codepoint(new_string_data, '%', 1);
                break;

            default:
                sky_error_raise_format(
                        sky_ValueError,
                        "unsupported format character '%c' (%#02x) "
                        "at index %zd",
                        (*c > 0x1F && *c < 0x7F ? *c : '?'),
                        (int)*c,
                        (c - start));
        }

        /* Find the next '%' character. */
        for (start = ++c; c < end && '%' != *c; ++c);
    } while (c < end);

    if (start < end) {
        sky_string_data_append(new_string_data,
                               start,
                               (end - start) * SKY_STRING_WIDTH,
                               SKY_STRING_WIDTH);
    }

    if (tuplify && !sky_object_isa(values, sky_tuple_type)) {
        values = sky_tuple_pack(1, values);
        tuplify = SKY_FALSE;
    }
    if (sky_object_isa(values, sky_tuple_type) &&
        next_value < sky_object_len(values))
    {
        sky_error_raise_string(
                sky_TypeError,
                "not all arguments converted during string formatting");
    }

    return sky_string_cache_string(sky_string_cache(), new_string, SKY_FALSE);
}


#undef T
#undef F
