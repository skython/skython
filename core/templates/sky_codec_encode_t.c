#if SKY_CODEC_ENCODE_WIDTH == 1
#   define F    latin1
#   define T    uint8_t
#   if SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_BIG
#       define N    be_latin1
#   elif SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_LITTLE
#       define N    le_latin1
#   endif
#elif SKY_CODEC_ENCODE_WIDTH == 2
#   define F    ucs2
#   define T    uint16_t
#   if SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_BIG
#       define N    be_ucs2
#   elif SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_LITTLE
#       define N    le_ucs2
#   endif
#elif SKY_CODEC_ENCODE_WIDTH == 4
#   define F    ucs4
#   define T    sky_unicode_char_t
#   if SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_BIG
#       define N    be_ucs4
#   elif SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_LITTLE
#       define N    le_ucs4
#   endif
#else
#   error implementation missing
#endif


#if !defined(SKY_CODEC_ENCODE_ONCE)
#if SKY_CODEC_ENCODE_WIDTH == 1

static void
sky_codec_encode_ascii_latin1(sky_codec_context_t *context)
{
    const uint8_t   *c, *d, *end, *start;

    start = context->bytes;
    end = start + context->nbytes;
    for (c = start; c < end; ++c) {
        if (*c < 0x80) {
            continue;
        }
        if (c - start > 0) {
            context->callback(start, c - start, 1, context->arg);
        }
        for (d = c + 1; d < end && *d >= 0x80; ++d);

        sky_codec_encode_error(context,
                               (const void **)SKY_AS_VOIDP(&c),
                               d,
                               "ordinal not in range(128)");
        start = c--;
    }
    if (c - start > 0) {
        context->callback(start, c - start, 1, context->arg);
    }
}

#elif !defined(SKY_CODEC_ENCODE_ONCE)

static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_ascii_, F)(
        sky_codec_context_t *   context)
{
    char    *buffer, *outc;
    const T *c, *d, *end;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = outc = sky_asset_malloc(context->nbytes / context->width,
                                         SKY_ASSET_CLEANUP_ALWAYS);

        end = (const T *)context->bytes + (context->nbytes / context->width);
        for (c = (const T *)context->bytes; c < end; ++c) {
            if (*c < 0x80) {
                *outc++ = *c;
                continue;
            }
            if (outc > buffer) {
                context->callback(buffer, outc - buffer, 1, context->arg);
                outc = buffer;
            }
            for (d = c + 1; d < end && *d >= 0x80; ++d);

            sky_codec_encode_error(context,
                                   (const void **)SKY_AS_VOIDP(&c),
                                   d,
                                   "ordinal not in range(128)");
            --c;
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 1, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_latin1_, F)(
        sky_codec_context_t *   context)
{
    const T *c, *d, *end;
    uint8_t *buffer, *outc;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = outc = sky_asset_malloc(context->nbytes / context->width,
                                         SKY_ASSET_CLEANUP_ALWAYS);

        end = (const T *)context->bytes + (context->nbytes / context->width);
        for (c = (const T *)context->bytes; c < end; ++c) {
            if (*c < 0x100) {
                *outc++ = *c;
                continue;
            }
            if (outc > buffer) {
                context->callback(buffer, outc - buffer, 1, context->arg);
                outc = buffer;
            }
            for (d = c + 1; d < end && *d >= 0x100; ++d);

            sky_codec_encode_error(context,
                                   (const void **)SKY_AS_VOIDP(&c),
                                   d,
                                   "ordinal not in range(256)");
            --c;
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 1, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}

#endif


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_locale_, F)(
        sky_codec_context_t *   context)
{
    char        buffer[MB_CUR_MAX * 64], *outc;
    size_t      len;
    const T     *c, *end;
    mbstate_t   ps;

    outc = buffer;
    memset(&ps, 0, sizeof(ps));
    end = (const T *)context->bytes + (context->nbytes / context->width);
    for (c = (const T *)context->bytes; c < end; ++c) {
        if (sizeof(buffer) - (outc - buffer) < (size_t)MB_CUR_MAX) {
            context->callback(buffer, outc - buffer, 1, context->arg);
            outc = buffer;
        }
        len = wcrtomb(outc, (wchar_t)*c, &ps);
        if ((size_t)-1 == len) {
            if (outc > buffer) {
                context->callback(buffer, outc - buffer, 1, context->arg);
                outc = buffer;
            }
            sky_codec_encode_error(context,
                                   (const void **)SKY_AS_VOIDP(&c),
                                   c + 1,
                                   "invalid wide character code");
            --c;
        }
        else if (len > 0) {
            outc += len;
        }
        else {
            sky_error_fatal("internal error");
        }
    }
    if (outc > buffer) {
        context->callback(buffer, outc - buffer, 1, context->arg);
    }
}


#ifndef SKY_CODEC_ENCODE_CHARMAP_LOOKUP
#define SKY_CODEC_ENCODE_CHARMAP_LOOKUP

static sky_unicode_char_t
sky_codec_encode_charmap_lookup_charmap(sky_object_t map, sky_unicode_char_t cp)
{
    sky_codec_charmap_data_t    *charmap_data = sky_codec_charmap_data(map);

    int i, l;

    if (!cp) {
        return 0;
    }

    l = (cp >> 11);
    if ((i = charmap_data->level1[l]) == 0xFF) {
        return 0xFFFE;
    }

    l = (cp >> 7) & 0xF;
    if ((i = charmap_data->level23[(i * 16) * l]) == 0xFF) {
        return 0xFFFE;
    }

    l = cp & 0x7F;
    i = charmap_data->level23[(charmap_data->count2 * 16) + (i * 128) + l];
    return (i ? i : 0xFFFE);
}


static sky_object_t
sky_codec_encode_charmap_lookup_dict(sky_object_t map, sky_unicode_char_t cp)
{
    sky_object_t    o;

    if (sky_dict_type == sky_object_type(map)) {
        return sky_dict_get(map, sky_integer_create(cp), sky_None);
    }

    SKY_ERROR_TRY {
        o = sky_object_getitem(map, sky_integer_create(cp));
    }
    SKY_ERROR_EXCEPT(sky_LookupError) {
        o = sky_None;
    } SKY_ERROR_TRY_END;

    return o;
}

#endif


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_charmap_, F)(
        sky_codec_context_t *   context)
{
    const T             *c, *end;
    uint8_t             byte;
    sky_buffer_t        buffer;
    sky_object_t        o;
    sky_unicode_char_t  cp;

    end = (const T *)context->bytes + (context->nbytes / context->width);
    if (!sky_object_isa(context->mapping, sky_codec_charmap_type)) {
        for (c = (const T *)context->bytes; c < end; ++c) {
            o = sky_codec_encode_charmap_lookup_dict(context->mapping, *c);
            if (sky_object_isa(o, sky_integer_type)) {
                cp = sky_integer_value(o, INT32_MIN, INT32_MAX, NULL);
                if (cp < 0 || cp > 255) {
                    sky_error_raise_string(
                            sky_TypeError,
                            "character mapping must be in range(256)");
                }
                byte = (uint8_t)cp;
                context->callback(&byte, sizeof(byte), 1, context->arg);
            }
            else if (sky_object_isnull(o)) {
                sky_codec_encode_error(context,
                                       (const void **)SKY_AS_VOIDP(&c),
                                       c + 1,
                                       "character maps to <undefined>");
                --c;
            }
            else if (sky_buffer_check(o)) {
                SKY_ASSET_BLOCK_BEGIN {
                    sky_buffer_acquire(&buffer, o, SKY_BUFFER_FLAG_SIMPLE);
                    sky_asset_save(&buffer,
                                   (sky_free_t)sky_buffer_release,
                                   SKY_ASSET_CLEANUP_ALWAYS);

                    context->callback(buffer.buf, buffer.len, 1, context->arg);
                } SKY_ASSET_BLOCK_END;
            }
            else {
                sky_error_raise_string(
                        sky_TypeError,
                        "character mapping must return integer, None or bytes");
            }
        }
    }
    else {
        for (c = (const T *)context->bytes; c < end; ++c) {
            cp = sky_codec_encode_charmap_lookup_charmap(context->mapping, *c);
            if (0xFFFE == cp) { /* <undefined> */
                sky_codec_encode_error(context,
                                       (const void **)SKY_AS_VOIDP(&c),
                                       c + 1,
                                       "character maps to <undefined>");
                --c;
            }
            else if (cp < 0 || cp > 255) {
                sky_error_raise_string(
                        sky_TypeError,
                        "character mapping must be in range(256)");
            }
            else {
                byte = (uint8_t)cp;
                context->callback(&byte, sizeof(byte), 1, context->arg);
            }
        }
    }
}


#if SKY_CODEC_ENCODE_WIDTH != 1

static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_raw_unicode_escape_, F)(
        sky_codec_context_t *   context)
{
    const char * const  hexdigits = sky_ctype_lower_hexdigits;

    char    buffer[64], escape_buffer[16], *outc, *outend;
    const T *c, *end, *start;

    start = context->bytes;
    end = start + (context->nbytes / context->width);

    outc = buffer;
    outend = buffer + sizeof(buffer);
    escape_buffer[0] = '\\';
#if SKY_CODEC_ENCODE_WIDTH == 2
    escape_buffer[1] = 'u';
#endif

    for (c = start; c < end; ++c) {
        if (*c < 0x100) {
            *outc++ = *c;
            if (outc >= outend) {
                context->callback(buffer, outc - buffer, 1, context->arg);
                outc = buffer;
            }
            continue;
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 1, context->arg);
            outc = buffer;
        }
#if SKY_CODEC_ENCODE_WIDTH > 2
        if (*c >= 0x10000) {
            escape_buffer[1] = 'U';
            escape_buffer[2] = hexdigits[(*c >> 28) & 0xF];
            escape_buffer[3] = hexdigits[(*c >> 24) & 0xF];
            escape_buffer[4] = hexdigits[(*c >> 20) & 0xF];
            escape_buffer[5] = hexdigits[(*c >> 16) & 0xF];
            escape_buffer[6] = hexdigits[(*c >> 12) & 0xF];
            escape_buffer[7] = hexdigits[(*c >>  8) & 0xF];
            escape_buffer[8] = hexdigits[(*c >>  4) & 0xF];
            escape_buffer[9] = hexdigits[(*c      ) & 0xF];
            context->callback(escape_buffer, 10, 1, context->arg);
            continue;
        }
        escape_buffer[1] = 'u';
#endif
        escape_buffer[2] = hexdigits[(*c >> 12) & 0xF];
        escape_buffer[3] = hexdigits[(*c >>  8) & 0xF];
        escape_buffer[4] = hexdigits[(*c >>  4) & 0xF];
        escape_buffer[5] = hexdigits[(*c      ) & 0xF];
        context->callback(escape_buffer, 6, 1, context->arg);
    }
    if (outc > buffer) {
        context->callback(buffer, outc - buffer, 1, context->arg);
    }
}

#endif


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_unicode_escape_, F)(
        sky_codec_context_t *   context)
{
    const char * const  hexdigits = sky_ctype_lower_hexdigits;

    char    buffer[64], escape_buffer[16], *outc, *outend;
    const T *c, *end, *start;

    start = context->bytes;
    end = start + (context->nbytes / context->width);

    outc = buffer;
    outend = buffer + sizeof(buffer);
    escape_buffer[0] = '\\';

    for (c = start; c < end; ++c) {
        if (*c != '\\' && *c >= ' ' && *c < 0x7F) {
            *outc++ = *c;
            if (outc >= outend) {
                context->callback(buffer, outc - buffer, 1, context->arg);
                outc = buffer;
            }
            continue;
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 1, context->arg);
            outc = buffer;
        }
#if SKY_CODEC_ENCODE_WIDTH != 1
        if (*c < 0x100) {
#endif
            if ('\t' == *c) {
                escape_buffer[1] = 't';
                context->callback(escape_buffer, 2, 1, context->arg);
                continue;
            }
            if ('\n' == *c) {
                escape_buffer[1] = 'n';
                context->callback(escape_buffer, 2, 1, context->arg);
                continue;
            }
            if ('\r' == *c) {
                escape_buffer[1] = 'r';
                context->callback(escape_buffer, 2, 1, context->arg);
                continue;
            }
            if ('\\' == *c) {
                escape_buffer[1] = '\\';
                context->callback(escape_buffer, 2, 1, context->arg);
                continue;
            }
            escape_buffer[1] = 'x';
            escape_buffer[2] = hexdigits[(*c >> 4) & 0xF];
            escape_buffer[3] = hexdigits[(*c     ) & 0xF];
            context->callback(escape_buffer, 4, 1, context->arg);
            continue;
#if SKY_CODEC_ENCODE_WIDTH != 1
        }
#endif
#if SKY_CODEC_ENCODE_WIDTH > 2
        if (*c >= 0x10000) {
            escape_buffer[1] = 'U';
            escape_buffer[2] = hexdigits[(*c >> 28) & 0xF];
            escape_buffer[3] = hexdigits[(*c >> 24) & 0xF];
            escape_buffer[4] = hexdigits[(*c >> 20) & 0xF];
            escape_buffer[5] = hexdigits[(*c >> 16) & 0xF];
            escape_buffer[6] = hexdigits[(*c >> 12) & 0xF];
            escape_buffer[7] = hexdigits[(*c >>  8) & 0xF];
            escape_buffer[8] = hexdigits[(*c >>  4) & 0xF];
            escape_buffer[9] = hexdigits[(*c      ) & 0xF];
            context->callback(escape_buffer, 10, 1, context->arg);
            continue;
        }
#endif
        escape_buffer[1] = 'u';
        escape_buffer[2] = hexdigits[(*c >> 12) & 0xF];
        escape_buffer[3] = hexdigits[(*c >>  8) & 0xF];
        escape_buffer[4] = hexdigits[(*c >>  4) & 0xF];
        escape_buffer[5] = hexdigits[(*c      ) & 0xF];
        context->callback(escape_buffer, 6, 1, context->arg);
    }
    if (outc > buffer) {
        context->callback(buffer, outc - buffer, 1, context->arg);
    }
}


#if SKY_CODEC_ENCODE_WIDTH == 4

static void
sky_codec_encode_unicode_internal_ucs4(
        sky_codec_context_t *   context)
{
    context->callback(context->bytes, context->nbytes, 4, context->arg);
}

#else

static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_unicode_internal_, F)(
        sky_codec_context_t *   context)
{
    const T             *c, *end;
    sky_unicode_char_t  buffer[64], *outc, *outend;

    end = (const T *)context->bytes + (context->nbytes / context->width);
    outc = buffer;
    outend = buffer + (sizeof(buffer) / sizeof(buffer[0]));

    for (c = (const T *)context->bytes; c < end; ++c) {
        *outc++ = (sky_unicode_char_t)*c;
        if (outc >= outend) {
            context->callback(buffer, sizeof(buffer), 4, context->arg);
            outc = buffer;
        }
    }
    if (outc > buffer) {
        context->callback(buffer,
                          (outc - buffer) * sizeof(buffer[0]),
                          4,
                          context->arg);
    }
}

#endif


#endif


#if SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_BIG
#   define E(x) do { *outc++ = ((x) >> 8) & 0xFF; \
                     *outc++ = ((x) & 0xFF); } while (0)
#elif SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_LITTLE
#   define E(x) do { *outc++ = ((x) & 0xFF); \
                     *outc++ = ((x) >> 8) & 0xFF; } while (0)
#else
#   error implementation missing
#endif


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_utf16_, N)(
        sky_codec_context_t *   context)
{
    const T             *c, *end;
    uint8_t             *buffer, *outc, *outend;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(SKY_MAX(256, ((context->nbytes /
                                                 context->width) + 1) * 2),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        outc = buffer;
        outend = buffer + sky_memsize(buffer);

        if (context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM) {
            E(0xFEFF);
        }

        end = (const T *)context->bytes + (context->nbytes / context->width);
        for (c = (const T *)context->bytes; c < end; ++c) {
            cp = (sky_unicode_char_t)*c;
            if (cp >= 0x10000) {
                if (outc + 4 > outend) {
                    context->callback(buffer, outc - buffer, 2, context->arg);
                    outc = buffer;
                }
                E((uint16_t)(0xD800 | ((cp - 0x10000) >> 10)));
                E((uint16_t)(0xDC00 | ((cp - 0x10000) & 0x3FF)));
            }
            else {
                if (outc + 2 > outend) {
                    context->callback(buffer, outc - buffer, 2, context->arg);
                    outc = buffer;
                }
                E((uint16_t)cp);
            }
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 2, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}


#undef E


#if SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_BIG
#   define E(x) do { *outc++ = ((x) >> 24) & 0xFF; \
                     *outc++ = ((x) >> 16) & 0xFF; \
                     *outc++ = ((x) >> 8) & 0xFF; \
                     *outc++ = ((x) & 0xFF); } while (0)
#elif SKY_CODEC_ENCODE_ENDIAN == SKY_ENDIAN_LITTLE
#   define E(x) do { *outc++ = ((x) & 0xFF); \
                     *outc++ = ((x) >> 8) & 0xFF; \
                     *outc++ = ((x) >> 16) & 0xFF; \
                     *outc++ = ((x) >> 24) & 0xFF; } while (0)
#else
#   error implementation missing
#endif


static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_utf32_, N)(
        sky_codec_context_t *   context)
{
    const T             *c, *end;
    uint8_t             *buffer, *outc, *outend;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(SKY_MAX(256, ((context->nbytes /
                                                 context->width) + 1) * 4),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        outc = buffer;
        outend = buffer + sky_memsize(buffer);

        if (context->flags & SKY_CODEC_ENCODER_FLAG_INCLUDE_BOM) {
            E(0xFEFF);
        }

        end = (const T *)context->bytes + (context->nbytes / context->width);
        for (c = (const T *)context->bytes; c < end; ++c) {
            cp = (sky_unicode_char_t)*c;
            if (outc + 4 > outend) {
                context->callback(buffer, outc - buffer, 4, context->arg);
                outc = buffer;
            }
            E((uint32_t)cp);
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 4, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}


#undef E


#if !defined(SKY_CODEC_ENCODE_ONCE)

static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_utf7_, F)(
        sky_codec_context_t *   context)
    /* Conservative encoding per RFC 2152 */
{
    /* Set D (0):      A-Z, a-z, 0-9, '(),-./:?
     * Set O (2):      !"#$%&*;<=>@[]^_`{|}
     * Set B (base64): A-Z, a-z, 0-9, +, /
     * Whitespace (4): space, tab, cr, lf
     * Everything else (1)
     */
    static uint8_t charset[128] = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 1, 1, 4, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        4, 2, 2, 2, 2, 2, 2, 0, 0, 0, 2, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0,
        2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 2, 2,
        2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 1, 1,
    };

    const char * const  base64 = sky_ctype_base64_digits;

    int         nbits;
    char        buffer[64], *outc, *outend;
    const T     *c, *end;
    uint32_t    charset_mask;
    uint64_t    bits;

    end = (const T *)context->bytes + (context->nbytes / context->width);
    outc = buffer;
    outend = buffer + sizeof(buffer);
    charset_mask = 0x1 | (context->flags & 0x6);
    bits = 0;
    nbits = 0;

#define flush_buffer()                                              \
    do {                                                            \
        context->callback(buffer, outc - buffer, 1, context->arg);  \
        outc = buffer;                                              \
    } while (0)

#define maybe_flush_buffer(n)                                       \
    do {                                                            \
        if (outc + (n) >= outend) {                                 \
            flush_buffer();                                         \
        }                                                           \
    } while (0)

    for (c = (const T *)context->bytes; c < end; ++c) {
        if (*c <= 0x7F) {
            if ('+' == *c) {
                maybe_flush_buffer(2);
                *outc++ = '+';
                *outc++ = '-';
                continue;
            }
            if (!(charset[*c] & charset_mask)) {
                maybe_flush_buffer(1);
                *outc++ = *c;
                continue;
            }
        }
        /* We have a character that must be encoded. Drop a '+' into the
         * output buffer, and keep encoding characters until we find another
         * directly encodable character or the end of input.
         */
        maybe_flush_buffer(1);
        *outc++ = '+';
        do {
#if SKY_CODEC_ENCODE_WIDTH == 4
            if (*c >= 0x10000) {
                nbits += 32;
                bits = (bits << 16) | 0xD800 | ((*c - 0x10000) >> 10);
                bits = (bits << 16) | 0xDC00 | ((*c - 0x10000) & 0x3FF);
            }
            else {
#endif
                nbits += 16;
                bits = (bits << 16) | *c;
#if SKY_CODEC_ENCODE_WIDTH == 4
            }
#endif
            maybe_flush_buffer(nbits / 6);
            while (nbits >= 6) {
                *outc++ = base64[(bits >> (nbits - 6)) & 0x3F];
                nbits -= 6;
            }
        } while (++c < end && (*c > 0x7F || (charset[*c] & charset_mask)));
        if (nbits) {
            maybe_flush_buffer(1);
            *outc++ = base64[(bits << (6 - nbits)) & 0x3F];
            nbits = 0;
            bits = 0;
        }
        if (c >= end || (sky_ctype_isbase64(*c) || '-' == *c)) {
            maybe_flush_buffer(1);
            *outc++ = '-';
        }
        --c;
    }
    if (outc > buffer) {
        flush_buffer();
    }
}


#if SKY_CODEC_ENCODE_WIDTH == 1

static void
sky_codec_encode_utf8_latin1(
        sky_codec_context_t *   context)
{
    uint8_t         *buffer, *outc, *outend;
    const uint8_t   *c, *end, *start;

    /* Simple runs of ASCII will be common, so handle them by just passing them
     * off to the callback; however, encoding U+0080..U+00FF will require two
     * bytes per character, so handle runs of them in a temporary buffer. The
     * buffer can be pre-allocated since we know for sure we'll have at least
     * one character in that range.
     */

    SKY_ASSET_BLOCK_BEGIN {
        buffer = sky_asset_malloc(SKY_MIN(256, (context->nbytes /
                                                context->width) * 2),
                                  SKY_ASSET_CLEANUP_ALWAYS);
        outend = buffer + sky_memsize(buffer);

        start = context->bytes;
        end = start + (context->nbytes / context->width);
        for (c = start; c < end; ++c) {
            if (*c < 0x80) {
                continue;
            }
            if (c > start) {
                context->callback(start, c - start, 1, context->arg);
            }
            for (outc = buffer; c < end && *c >= 0x80 && outc < outend; ++c) {
                outc[0] = (uint8_t)(0xC0 | (*c >> 6));
                outc[1] = (uint8_t)(0x80 | (*c & 0x3F));
                outc += 2;
            }
            context->callback(buffer, outc - buffer, 1, context->arg);
            start = c--;
        }
        if (c > start) {
            context->callback(start, c - start, 1, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}

#else

static void
SKY_TOKEN_PASTE_EXPANDED(sky_codec_encode_utf8_, F)(
        sky_codec_context_t *   context)
{
    const T             *c, *end;
    uint8_t             *buffer, *outc, *outend;
    sky_unicode_char_t  cp;

    SKY_ASSET_BLOCK_BEGIN {
        buffer = outc = sky_asset_malloc(SKY_MAX(256, (context->nbytes /
                                                       context->width) * 4),
                                         SKY_ASSET_CLEANUP_ALWAYS);
        outend = buffer + sky_memsize(buffer);

        end = (const T *)context->bytes + (context->nbytes / context->width);
        for (c = (const T *)context->bytes; c < end; ++c) {
            if (*c < 0x80) {
                if (outc + 1 > outend) {
                    context->callback(buffer, outc - buffer, 1, context->arg);
                    outc = buffer;
                }
                *outc++ = *c;
            }
            else {
                cp = (sky_unicode_char_t)*c;
                if (cp < 0x800) {
                    if (outc + 2 > outend) {
                        context->callback(buffer,
                                          outc - buffer,
                                          1,
                                          context->arg);
                        outc = buffer;
                    }
                    *outc++ = (uint8_t)(0xC0 | (cp >> 6));
                    *outc++ = (uint8_t)(0x80 | (cp & 0x3F));
                }
                else if (cp < 0x10000) {
                    if (cp >= 0xD800 && cp <= 0xDFFF) {
                        if (outc > buffer) {
                            context->callback(buffer,
                                              outc - buffer,
                                              1,
                                              context->arg);
                            outc = buffer;
                        }
                        sky_codec_encode_error(
                                context,
                                (const void **)SKY_AS_VOIDP(&c),
                                c + 1,
                                "surrogates not allowed");
                        --c;
                        continue;
                    }
                    if (outc + 3 > outend) {
                        context->callback(buffer,
                                          outc - buffer,
                                          1,
                                          context->arg);
                        outc = buffer;
                    }
                    *outc++ = (uint8_t)(0xE0 | (cp >> 12));
                    *outc++ = (uint8_t)(0x80 | ((cp >> 6) & 0x3F));
                    *outc++ = (uint8_t)(0x80 | (cp & 0x3F));
                }
                else {
                    sky_error_validate_debug(cp <= SKY_UNICODE_CHAR_MAX);
                    if (outc + 4 > outend) {
                        context->callback(buffer,
                                          outc - buffer,
                                          1,
                                          context->arg);
                        outc = buffer;
                    }
                    *outc++ = (uint8_t)(0xF0 | (cp >> 18));
                    *outc++ = (uint8_t)(0x80 | ((cp >> 12) & 0x3F));
                    *outc++ = (uint8_t)(0x80 | ((cp >> 6) & 0x3F));
                    *outc++ = (uint8_t)(0x80 | (cp & 0x3F));
                }
            }
        }
        if (outc > buffer) {
            context->callback(buffer, outc - buffer, 1, context->arg);
        }
    } SKY_ASSET_BLOCK_END;
}

#endif
#endif


#undef N
#undef T
#undef F
