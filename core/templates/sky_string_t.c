/* Notes:
 *  -   search (count, find, rfind) are based on code from CPython. See
 *      http://effbot.org/zone/stringlib.htm for details
 */

#ifndef __SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__
#define __SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__ 1


SKY_EXTERN_INLINE unsigned long
sky_string_mask(sky_unicode_char_t cp)
{
    return 1UL << (cp & ((sizeof(unsigned long) * 8) - 1));
}


static ssize_t
sky_string_data_range(const sky_string_data_t * string_data,
                      ssize_t *                 start,
                      ssize_t *                 end)
{
    if (*start < 0) {
        *start += string_data->len;
        if (*start < 0) {
            *start = 0;
        }
    }
    if (*end < 0) {
        *end += string_data->len;
        if (*end < 0) {
            *end = 0;
        }
    }
    else if (*end > string_data->len) {
        *end = string_data->len;
    }
    return (*start >= *end ? 0 : *end - *start);
}


#if !defined(HAVE_MEMRCHR)
static void *
memrchr(const void *s, int c, size_t n)
{
    const uint8_t   *b, *startb = s;

    sky_error_validate_debug(c >= 0 && c <= 255);
    for (b = startb + n - 1; b >= startb; --b) {
        if (*b == (uint8_t)c) {
            return (void *)b;
        }
    }

    return NULL;
}
#endif


#endif  /* __SKYTHON_CORE_TEMPLATES_SKY_STRING_T_C__ */


#if SKY_STRING_WIDTH == 1
#   define F    latin1
#   define T    uint8_t
#elif SKY_STRING_WIDTH == 2
#   define F    ucs2
#   define T    uint16_t
#elif SKY_STRING_WIDTH == 4
#   define F    ucs4
#   define T    sky_unicode_char_t
#else
#   error implementation missing
#endif


#define sky_string_data_append_run(bytes, nbytes)                   \
        do {                                                        \
            if (nbytes > 0) {                                       \
                sky_string_data_append(new_string_data,             \
                                       bytes,                       \
                                       (nbytes) * SKY_STRING_WIDTH, \
                                       SKY_STRING_WIDTH);           \
            }                                                       \
        } while (0)


static const T *
SKY_TOKEN_PASTE_EXPANDED(sky_string_normalize_start_, F)(
        const sky_unicode_decomp_data_t *   data,
        const T *                           start,
        const T *                           end,
        uint16_t                            mask);


/* start, end are in character units.
 * mode: 0 = count, 1 = forward, -1 = reverse
 * maxcount is ignored if mode != 0
 */
static ssize_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
        const sky_string_data_t *   string,
        const sky_string_data_t *   sub,
        ssize_t                     start,
        ssize_t                     end,
        const int                   mode,
        ssize_t                     maxcount);


/* Used by lstrip, rstrip, strip - find a single codepoint in a run of
 * possible codepoints. Returns SKY_TRUE if found.
 */
static sky_bool_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_findchar_, F)(
        const sky_string_data_t *   string_data,
        T                           codepoint)
{
    ssize_t i;

    if (string_data->highest_codepoint < 0x100) {
        if (1 == string_data->len) {
            return (string_data->data.latin1[0] == codepoint);
        }
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.latin1[i] == codepoint) {
                return SKY_TRUE;
            }
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        if (1 == string_data->len) {
            return (string_data->data.ucs2[0] == codepoint);
        }
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.ucs2[i] == codepoint) {
                return SKY_TRUE;
            }
        }
    }
    else {
        if (1 == string_data->len) {
            return (string_data->data.ucs4[0] == codepoint);
        }
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.ucs4[i] == codepoint) {
                return SKY_TRUE;
            }
        }
    }
    return SKY_FALSE;
}


/* Used by count, find, rfind - match a run of any width (sub) against a
 * segment of type T (&(string_data->data.F[offset])) where the segment of
 * type T at least as long (in code point units) as string_data. Returns the
 * number of code points successfully matched.
 */
static ssize_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_match_, F)(
        const sky_string_data_t *   string_data,
        const T *                   segment)
{
    ssize_t i;

    if (string_data->highest_codepoint < 0x100) {
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.latin1[i] != segment[i]) {
                break;
            }
        }
    }
    else if (string_data->highest_codepoint < 0x10000) {
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.ucs2[i] != segment[i]) {
                break;
            }
        }
    }
    else {
        for (i = 0; i < string_data->len; ++i) {
            if (string_data->data.ucs4[i] != segment[i]) {
                break;
            }
        }
    }

    return i;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_capitalize_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    c = start = string_data->data.F;
    end = start + string_data->len;

#if SKY_STRING_WIDTH == 1
    ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
    ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
    if (ctype->special_upper[0]) {
        ++start;
        sky_string_data_append(
                new_string_data,
                &(ctype->special_upper[0]),
                (ctype->special_upper[1] ? (ctype->special_upper[2] ? 3
                                                                    : 2)
                                         : 1) * sizeof(sky_unicode_char_t),
                sizeof(sky_unicode_char_t));
    }
    else if (ctype->simple_upper) {
        ++start;
        cp = *c + ctype->simple_upper;
        sky_string_data_append(new_string_data,
                               &cp,
                               sizeof(sky_unicode_char_t),
                               sizeof(sky_unicode_char_t));
    }

    while (++c < end) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (ctype->special_lower[0]) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            sky_string_data_append(
                    new_string_data,
                    &(ctype->special_lower[0]),
                    (ctype->special_lower[1] ? (ctype->special_lower[2] ? 3
                                                                        : 2)
                                             : 1) * sizeof(sky_unicode_char_t),
                    sizeof(sky_unicode_char_t));
        }
        else if (ctype->simple_lower) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            cp = *c + ctype->simple_lower;
            sky_string_data_append(new_string_data,
                                   &cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_casefold_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_LOWER) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            cp = *c + ctype->simple_lower;
            sky_string_data_append(new_string_data,
                                   &cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
        else if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_TITLE) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            cp = *c + ctype->simple_title;
            sky_string_data_append(new_string_data,
                                   &cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
        else if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_UPPER) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            cp = *c + ctype->simple_upper;
            sky_string_data_append(new_string_data,
                                   &cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
        else if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_CASEFOLD_SPECIAL) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            sky_string_data_append(
                    new_string_data,
                    ctype->casefold,
                    (ctype->casefold[1] ? (ctype->casefold[2] ? 3 : 2) : 1) *
                    sizeof(sky_unicode_char_t),
                    sizeof(sky_unicode_char_t));
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


static inline const T *
SKY_TOKEN_PASTE_EXPANDED(sky_string_compose_starter_, F)(
        const sky_unicode_decomp_data_t *   data,
        const T *                           cps,
        const T *                           start,
        const T *                           end)
{
    int     ccc, prev_ccc;
    const T *L;

    if (start != cps) {
        /* Move backwards to find the first starter, L */
        prev_ccc = sky_string_decomp_lookup(data, *start)->ccc;
        for (L = start - 1; L >= cps; --L) {
            ccc = sky_string_decomp_lookup(data, *L)->ccc;
            if (!ccc || prev_ccc < ccc) {
                break;
            }
        }
        if (L >= cps) {
            return L;
        }
    }

    /* Move forwards to find the first starter, L */
    for (L = cps; L < end && sky_string_decomp_lookup(data, *L)->ccc; ++L);
    return (L < end ? L : NULL);
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_compose_, F)(
        sky_string_t                        string,
        const sky_string_data_t *           string_data,
        sky_bool_t                          compat,
        const sky_unicode_decomp_data_t *   data)
{
    const T *   (*compose_starter)(const sky_unicode_decomp_data_t *data,
                                   const T *, const T *, const T *) =
                SKY_TOKEN_PASTE_EXPANDED(sky_string_compose_starter_, F);
    const T *   (*normalize_start)(const sky_unicode_decomp_data_t *data,
                                   const T *, const T *, uint16_t) =
                SKY_TOKEN_PASTE_EXPANDED(sky_string_normalize_start_, F);

    int                 ccc, prev_ccc;
    size_t              new_width, old_width;
    const T             *C, *end, *start, *starter;
    ssize_t             out;
    uint16_t            mask;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  L, P, LIndex, VIndex;

    /* Find the first starter that may have non-starters following it that may
     * be able to be combined. This is basically looking for C starting from
     * the first character in the string for a pair <L, C> that may have a
     * composite. This is done specially at the start, because the original
     * string may be returned without doing any real work if no such pair
     * exists.
     */
    mask = (compat ? SKY_UNICODE_DECOMP_TYPE_NFKC_QC_MASK
                   : SKY_UNICODE_DECOMP_TYPE_NFC_QC_MASK);
    start = string_data->data.F;
    end = string_data->data.F + string_data->len;
    if (!(C = normalize_start(data, string_data->data.F, end, mask))) {
        return string;
    }
    if (!(starter = compose_starter(data, string_data->data.F, C, end))) {
        return string;
    }

    /* Fix for strings starting with a combining mark */
    if ((prev_ccc = sky_string_decomp_lookup(data, *starter)->ccc) != 0) {
        prev_ccc = 256;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);

    while ((C = starter + 1) < end) {
        sky_string_data_append_run(start, (C - 1) - start);

        /* Hangul Composition */
        LIndex = C[-1] - LBase;
        VIndex = C[ 0] - VBase;
        if (0 <= LIndex && LIndex < LCount && 0 <= VIndex && VIndex <= VCount) {
            P = SBase + ((LIndex * VCount) + VIndex) * TCount;
            if (C[1] >= TBase && C[1] < TBase + TCount) {
                P += (C[1] - TBase);
                ++C;
            }
            sky_string_data_append_codepoint(new_string_data, P, 1);
            if (++C >= end) {
                start = C + 1;
                break;
            }
        }
        else {
            /* R2. If there is such an L, and C is not blocked from L, and
             *     there exists a Primary Composite P which is canonically
             *     equivalent to the sequence <L, C>, then replace L by P
             *     in the sequence and delete C from the sequence.
             */

            /* Save space for the final composed character, but we have to
             * cheat here so that both all_ctype_flags any any_ctype_flags
             * end up being correct.
             */
            out = new_string_data->len++;
            old_width = sky_string_data_width(new_string_data);
            sky_string_data_resize(new_string_data, out + 1, old_width);

            L = *starter;
            for (; C < end; ++C) {
                ccc = sky_string_decomp_lookup(data, *C)->ccc;
                P = sky_string_composite(data, L, *C);
                if (P != -1 && (!prev_ccc || prev_ccc < ccc)) {
                    L = P;
                }
                else if (!ccc) {
                    if (prev_ccc) {
                        sky_string_data_append_codepoint(new_string_data, *C, 1);
                        old_width = sky_string_data_width(new_string_data);
                        ++C;
                    }
                    break;
                }
                else {
                    prev_ccc = ccc;
                    sky_string_data_append_codepoint(new_string_data, *C, 1);
                    old_width = sky_string_data_width(new_string_data);
                }
            }

            if (L > new_string_data->highest_codepoint) {
                if (L < 0x100) {
                    new_width = 1;
                }
                else if (L < 0x10000) {
                    new_width = 2;
                }
                else {
                    new_width = 4;
                }
                if (new_width > old_width) {
                    sky_string_data_widen(new_string_data,
                                          new_string_data->len,
                                          old_width,
                                          new_width);
                }
            }
            if (new_string_data->highest_codepoint < 0x100) {
                new_string_data->data.latin1[out] = L;
                sky_string_data_update_1(new_string_data, L);
                if (out + 1 == new_string_data->len) {
                    new_string_data->data.latin1[new_string_data->len] = 0;
                }
            }
            else {
                if (new_string_data->highest_codepoint < 0x10000) {
                    new_string_data->data.ucs2[out] = L;
                    if (out + 1 == new_string_data->len) {
                        new_string_data->data.ucs2[new_string_data->len] = 0;
                    }
                }
                else {
                    new_string_data->data.ucs4[out] = L;
                    if (out + 1 == new_string_data->len) {
                        new_string_data->data.ucs4[new_string_data->len] = 0;
                    }
                }
                sky_string_data_update(new_string_data, L);
            }
        }

        start = C;
        if (!(C = normalize_start(data, C, end, mask))) {
            break;
        }
        if (!(starter = compose_starter(data, string_data->data.F, C, end))) {
            break;
        }
        prev_ccc = sky_string_decomp_lookup(data, *starter)->ccc;
    }
    sky_string_data_append_run(start, end - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_decompose_, F)(
        sky_string_t                        string,
        const sky_string_data_t *           string_data,
        sky_bool_t                          compat,
        const sky_unicode_decomp_data_t *   data)
{
    const T *   (*normalize_start)(const sky_unicode_decomp_data_t *data,
                                   const T *, const T *, uint16_t) =
                SKY_TOKEN_PASTE_EXPANDED(sky_string_normalize_start_, F);

    int                         sp;
    size_t                      new_string_data_width, new_width;
    const T                     *c, *end;
    uint8_t                     ccc, count, prev_ccc;
    uint16_t                    mask;
    sky_string_t                new_string;
    sky_string_data_t           *new_string_data;
    sky_unicode_char_t          cp, out[3], SIndex;
    sky_unicode_char_t          stack[SKY_UNICODE_DECOMP_MAXLEN];
    const sky_unicode_decomp_t  *decomp;

    /* Find the start. If there isn't one, the string is already decomposed. */
    mask = (compat ? SKY_UNICODE_DECOMP_TYPE_NFKD_QC_MASK
                   : SKY_UNICODE_DECOMP_TYPE_NFD_QC_MASK);
    end = string_data->data.F + string_data->len;
    if (!(c = normalize_start(data, string_data->data.F, end, mask))) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);

    sp = 0;
    if (string_data->data.F == c) {
        prev_ccc = 0;
        new_string_data_width = 1;
    }
    else {
        sky_string_data_append(new_string_data,
                               string_data->data.F,
                               (c - string_data->data.F) * SKY_STRING_WIDTH,
                               SKY_STRING_WIDTH);
        prev_ccc = sky_string_decomp_lookup(data, *(c - 1))->ccc;
        new_string_data_width = sky_string_data_width(new_string_data);
        new_width = new_string_data_width;
    }

    while (c < end) {
        stack[sp++] = *c++;
        while (sp) {
            cp = stack[--sp];

            /* Hangul Decomposition */
            SIndex = cp - SBase;
            if (SIndex >= 0 && SIndex < SCount) {
                out[0] = LBase + (SIndex / NCount);             /* L */
                out[1] = VBase + ((SIndex % NCount) / TCount);  /* V */
                out[2] = TBase + (SIndex % TCount);             /* T */

                count = (out[2] != TBase ? 3 : 2);
                sky_string_data_append(new_string_data,
                                       out,
                                       (count * sizeof(sky_unicode_char_t)),
                                       sizeof(sky_unicode_char_t));
                prev_ccc = sky_string_decomp_lookup(data, out[count - 1])->ccc;
                new_string_data_width = SKY_MAX(new_string_data_width, 2);
                new_width = new_string_data_width;
                continue;
            }

            decomp = sky_string_decomp_lookup(data, cp);
            if (decomp->length &&
                (!(decomp->flags & SKY_UNICODE_DECOMP_TYPE_COMPAT_MASK) ||
                 compat))
            {
                for (count = 0; count < decomp->length; ++count) {
                    stack[sp++] = data->data[decomp->offset +
                                             (decomp->length - count) - 1];
                }
                continue;
            }

            if (cp > new_string_data->highest_codepoint) {
                if (cp < 0x100) {
                    new_width = 1;
                }
                else if (cp < 0x10000) {
                    new_width = 2;
                }
                else {
                    new_width = 4;
                }
            }

            /* Canonical Combining Class sort */
            if (!(ccc = decomp->ccc) || prev_ccc <= ccc) {
                sky_string_data_append_codepoint(new_string_data, cp, 1);
                new_string_data_width = new_width;
                prev_ccc = ccc;
                continue;
            }

            /* Figure out the proper insertion point for the character. */
            if (new_width > new_string_data_width) {
                sky_string_data_widen(new_string_data,
                                      new_string_data->len + 1,
                                      new_string_data_width,
                                      new_width);
                new_string_data_width = new_width;
            }
            else {
                sky_string_data_resize(new_string_data,
                                       new_string_data->len + 1,
                                       new_string_data_width);
            }
            if (1 == new_string_data_width) {
                uint8_t *new_end, *x;

                new_end = new_string_data->data.latin1 + new_string_data->len;
                for (x = new_end - 1; x >= new_string_data->data.latin1; --x) {
                    if (sky_string_decomp_lookup(data, *x)->ccc <= ccc) {
                        break;
                    }
                }
                ++x;
                memmove(x + 1, x, sizeof(*x) * (new_end - x));
                *x = cp;
                sky_string_data_update_1(new_string_data, cp);
            }
            else if (2 == new_string_data_width) {
                uint16_t    *new_end, *x;

                new_end = new_string_data->data.ucs2 + new_string_data->len;
                for (x = new_end - 1; x >= new_string_data->data.ucs2; --x) {
                    if (sky_string_decomp_lookup(data, *x)->ccc <= ccc) {
                        break;
                    }
                }
                ++x;
                memmove(x + 1, x, sizeof(*x) * (new_end - x));
                *x = cp;
                sky_string_data_update(new_string_data, cp);
            }
            else {
                sky_unicode_char_t  *new_end, *x;

                new_end = new_string_data->data.ucs4 + new_string_data->len;
                for (x = new_end - 1; x >= new_string_data->data.ucs4; --x) {
                    if (sky_string_decomp_lookup(data, *x)->ccc <= ccc) {
                        break;
                    }
                }
                ++x;
                memmove(x + 1, x, sizeof(*x) * (new_end - x));
                *x = cp;
                sky_string_data_update(new_string_data, cp);
            }
            ++new_string_data->len;
        }
    }

    return new_string;
}


static sky_bool_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_endswith_, F)(
        const sky_string_data_t *   string_data,
        sky_object_t                suffix,
        ssize_t                     start,
        ssize_t                     end)
{
    static ssize_t (*match)(const sky_string_data_t *, const T *) =
                   SKY_TOKEN_PASTE_EXPANDED(sky_string_match_, F);

    ssize_t             len, suffix_len;
    sky_bool_t          endswith;
    sky_tuple_t         suffixes;
    sky_string_data_t   *suffix_data, suffix_tagged_data;

    len = sky_string_data_range(string_data, &start, &end);
    if (start > string_data->len) {
        return SKY_FALSE;
    }
    if (sky_object_isa(suffix, sky_string_type)) {
        if (!(suffix_len = sky_object_len(suffix))) {
            return SKY_TRUE;
        }
        suffix_data = sky_string_data_noctype(suffix, &suffix_tagged_data);
        if (suffix_len <= len &&
            match(suffix_data,
                  &(string_data->data.F[end - suffix_len])) == suffix_len)
        {
            return SKY_TRUE;
        }
        return SKY_FALSE;
    }

    if (!sky_object_isa(suffix, sky_tuple_type)) {
        goto bad_suffix;
    }

    endswith = SKY_FALSE;
    suffixes = suffix;
    SKY_SEQUENCE_FOREACH(suffixes, suffix) {
        if (!(suffix_len = sky_object_len(suffix))) {
            endswith = SKY_TRUE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
        suffix_data = sky_string_data_noctype(suffix, &suffix_tagged_data);
        if (suffix_len <= len &&
            match(suffix_data, &(string_data->data.F[end - suffix_len])))
        {
            endswith = SKY_TRUE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return endswith;

bad_suffix:
    sky_error_raise_format(
            sky_TypeError,
            "endswith first arg must be str or a tuple of str, not %@",
            sky_type_name(sky_object_type(suffix)));
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_expandtabs_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        ssize_t                     tabsize)
{
    const T             *c, *end, *start;
    ssize_t             i, size, x;
    sky_bool_t          has_tabs;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    i = size = 0;
    has_tabs = SKY_FALSE;
    end = string_data->data.F + string_data->len;
    for (c = string_data->data.F; c < end; ++c) {
        if ('\t' == *c) {
            has_tabs = SKY_TRUE;
            if (tabsize > 0) {
                x = (tabsize - (i % tabsize));
                if (size > SSIZE_MAX - x) {
                    sky_error_raise_string(sky_OverflowError, "result too long");
                }
                size += x;
            }
        }
        else {
            if (size > SSIZE_MAX - 1) {
                sky_error_raise_string(sky_OverflowError, "result too long");
            }
            ++i;
            if ('\n' == *c || '\r' == *c) {
                if (size > SSIZE_MAX - i) {
                    sky_error_raise_string(sky_OverflowError, "result too long");
                }
                size += i;
                i = 0;
            }
        }
    }
    if (!has_tabs) {
        return string;
    }
    if (size > SSIZE_MAX - i) {
        sky_error_raise_string(sky_OverflowError, "result too long");
    }
    size += i;
    i = 0;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, size, SKY_STRING_WIDTH);

    start = string_data->data.F;
    for (c = start; c < end; ++c) {
        if ('\t' == *c) {
            i += (c - start);
            sky_string_data_append_run(start, c - start);
            if (tabsize > 0) {
                x = tabsize - (i % tabsize);
                sky_string_data_append_codepoint(new_string_data, ' ', x);
                i += x;
            }
            start = c + 1;
        }
        else if ('\n' == *c || '\r' == *c) {
            i = 0;
        }
    }
    sky_string_data_append_run(start, end - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_lower_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  lower_cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (ctype->special_lower[0]) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            sky_string_data_append(
                    new_string_data,
                    &(ctype->special_lower[0]),
                    (ctype->special_lower[1] ? (ctype->special_lower[2] ? 3
                                                                        : 2)
                                             : 1) * sizeof(sky_unicode_char_t),
                    sizeof(sky_unicode_char_t));
        }
        else if (ctype->simple_lower) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            lower_cp = *c + ctype->simple_lower;
            sky_string_data_append(new_string_data,
                                   &lower_cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_lstrip_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   chars_data)
{
    static sky_bool_t (*findchar)(const sky_string_data_t *, T) =
                      SKY_TOKEN_PASTE_EXPANDED(sky_string_findchar_, F);

    ssize_t             length, start_index, step, stop_index;
    const T             *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    if (!string_data->len) {
        return sky_string_empty;
    }
    start = string_data->data.F;
    end = string_data->data.F + string_data->len;
    if (!chars_data) {
        if (!(string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_SPACE)) {
            return string;
        }
        while (start < end && sky_unicode_isspace(*start)) {
            ++start;
        }
    }
    else {
        if (!(string_data->any_ctype_flags & chars_data->any_ctype_flags)) {
            return string;
        }
        while (start < end && findchar(chars_data, *start)) {
            ++start;
        }
    }

    start_index = start - string_data->data.F;
    stop_index = string_data->len;
    step = 1;
    length = sky_sequence_indices(string_data->len,
                                  &start_index,
                                  &stop_index,
                                  &step);
    if (length == string_data->len) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_append_slice(new_string_data,
                                 string_data,
                                 length,
                                 start_index,
                                 step);

    return new_string;
}


static const T *
SKY_TOKEN_PASTE_EXPANDED(sky_string_normalize_start_, F)(
        const sky_unicode_decomp_data_t *   data,
        const T *                           start,
        const T *                           end,
        uint16_t                            mask)
{
    int     last_ccc = 0;

    const T                 *c;
    sky_unicode_decomp_t    *decomp;

    for (c = start; c < end; ++c) {
        decomp = sky_string_decomp_lookup(data, *c);
        if ((decomp->flags & mask) || (last_ccc > decomp->ccc && decomp->ccc)) {
            return c;
        }
        last_ccc = decomp->ccc;
    }

    return NULL;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_replace_delete_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   substring_data,
        ssize_t                     count)
{
    const T             *end, *p, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    start = string_data->data.F;
    end = start + string_data->len;
    p = string_data->data.F +
        SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                string_data,
                substring_data,
                start - string_data->data.F,
                end - string_data->data.F,
                1,
                -1);
    if (p < start || p >= end) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);

    while (count-- > 0 && start < end) {
        if (p > start) {
            sky_string_data_append_run(start, p - start);
        }
        start = p + substring_data->len;

        p = string_data->data.F +
            SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                    string_data,
                    substring_data,
                    start - string_data->data.F,
                    end - string_data->data.F,
                    1,
                    -1);
        if (p < string_data->data.F) {
            break;
        }
    }
    if (start < end) {
        sky_string_data_append_run(start, end - start);
    }

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_replace_insert_, F)(
        SKY_UNUSED sky_string_t     string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   substring_data,
        ssize_t                     count)
{
    size_t              new_string_width, string_width, substring_width;
    ssize_t             i;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    if (count > string_data->len + 1) {
        count = string_data->len + 1;
    }

    string_width = sky_string_data_width(string_data);
    substring_width = sky_string_data_width(substring_data);
    new_string_width = SKY_MAX(string_width, substring_width);

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data,
                           string_data->len + (substring_data->len * count),
                           new_string_width);

    sky_string_data_combine(new_string_data, substring_data);
    for (i = 0, --count; i < count; ++i) {
        sky_string_data_append(new_string_data,
                               &(string_data->data.F[i]),
                               SKY_STRING_WIDTH,
                               SKY_STRING_WIDTH);
        sky_string_data_combine(new_string_data, substring_data);
    }
    if (i < string_data->len) {
        sky_string_data_append(new_string_data,
                               &(string_data->data.F[i]),
                               (string_data->len - i) * SKY_STRING_WIDTH,
                               SKY_STRING_WIDTH);
    }

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_replace_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   old_pattern_data,
        const sky_string_data_t *   new_pattern_data,
        ssize_t                     count)
{
    const T             *end, *p, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    if (!count) {
        return string;
    }
    if (!old_pattern_data->len) {
        /* 'string'.replace('', ' ') -> ' s t r i n g ' */
        return SKY_TOKEN_PASTE_EXPANDED(sky_string_replace_insert_, F)(
                        string,
                        string_data,
                        new_pattern_data,
                        count);
    }
    if (!new_pattern_data->len) {
        /* 's t r i n g '.replace(' ', '') -> 'string' */
        return SKY_TOKEN_PASTE_EXPANDED(sky_string_replace_delete_, F)(
                        string,
                        string_data,
                        old_pattern_data,
                        count);
    }

    start = string_data->data.F;
    end = string_data->data.F + string_data->len;
    if (start >= end) {
        return string;
    }
    p = string_data->data.F +
        SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                string_data,
                old_pattern_data,
                start - string_data->data.F,
                end - string_data->data.F,
                1,
                -1);
    if (p < start || p >= end) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);

    while (count-- > 0 && start < end) {
        if (p > start) {
            sky_string_data_append_run(start, p - start);
        }
        sky_string_data_combine(new_string_data, new_pattern_data);
        start = p + old_pattern_data->len;

        p = string_data->data.F +
            SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                    string_data,
                    old_pattern_data,
                    start - string_data->data.F,
                    end - string_data->data.F,
                    1,
                    -1);
        if (p < string_data->data.F) {
            break;
        }
    }
    if (start < end) {
        sky_string_data_append_run(start, end - start);
    }

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_repr_, F)(
        const sky_string_data_t *   string_data)
{
    char                buffer[10];
    ssize_t             dquote, incr, len, squote;
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  cp, highest_codepoint, quote;

    /* Compute the space needed for the resulting string. */
    len = 2;
    dquote = squote = 0;
    highest_codepoint = 0x7F;
    end = string_data->data.F + string_data->len;
    for (c = string_data->data.F; c < end; ++c) {
        cp = (sky_unicode_char_t)*c;
        switch (cp) {
            case '\'':
                ++squote;
                incr = 1;
                break;
            case '\"':
                ++dquote;
                incr = 1;
                break;
            case '\\': case '\t': case '\r': case '\n':
                incr = 2;
                break;
            default:
                if (cp < ' ' || cp == 0x7F) {
                    incr = 4;
                }
                else if (cp < 0x7F) {
                    incr = 1;
                }
                else if (sky_unicode_isprint(cp)) {
                    incr = 1;
                    highest_codepoint = SKY_MAX(highest_codepoint, cp);
                }
                else if (cp < 0x100) {
                    incr = 4;
                }
                else if (cp < 0x10000) {
                    incr = 6;
                }
                else {
                    incr = 10;
                }
                break;
        }
        if (len > SSIZE_MAX - incr) {
            sky_error_raise_string(sky_OverflowError,
                                   "string is too long to generate repr");
        }
        len += incr;
    }

    /* Decide which quote to use, prefering a single quote over a double quote.
     * If both are present, use the single quotes, but make room to escape the
     * ones embedded in the middle.
     */
    quote = '\'';
    if (squote) {
        if (dquote) {
            if (len > SSIZE_MAX - squote) {
                sky_error_raise_string(sky_OverflowError,
                                       "string is too long to generate repr");
            }
            len += squote;
        }
        else {
            quote = '\"';
        }
    }

    /* Allocate the new string with enough space to hold the result so that
     * multiple allocations are not necessary.
     */
    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    if (highest_codepoint < 0x100) {
        sky_string_data_resize(new_string_data, len, 1);
    }
    else if (highest_codepoint < 0x10000) {
        sky_string_data_resize(new_string_data, len, 2);
    }
    else {
        sky_string_data_resize(new_string_data, len, 4);
    }

    buffer[0] = (char)quote;
    sky_string_data_append(new_string_data, buffer, 1, 1);

    buffer[0] = '\\';
    for (c = start = string_data->data.F; c < end; ++c) {
        cp = (sky_unicode_char_t)*c;

        if (cp == quote || cp == '\\') {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = (char)cp;
            sky_string_data_append(new_string_data, buffer, 2, 1);
        }
        else if (cp == '\t') {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 't';
            sky_string_data_append(new_string_data, buffer, 2, 1);
        }
        else if (cp == '\n') {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'n';
            sky_string_data_append(new_string_data, buffer, 2, 1);
        }
        else if (cp == '\r') {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'r';
            sky_string_data_append(new_string_data, buffer, 2, 1);
        }
        else if (cp < ' ' || cp == 0x7F) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'x';
            buffer[2] = sky_ctype_lower_hexdigits[(cp >> 4) & 0xF];
            buffer[3] = sky_ctype_lower_hexdigits[ cp       & 0xF];
            sky_string_data_append(new_string_data, buffer, 4, 1);
        }
        else if (cp < 0x7F || sky_unicode_isprint(cp)) {
            /* pass */
        }
        else if (cp < 0x100) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'x';
            buffer[2] = sky_ctype_lower_hexdigits[(cp >> 4) & 0xF];
            buffer[3] = sky_ctype_lower_hexdigits[ cp       & 0xF];
            sky_string_data_append(new_string_data, buffer, 4, 1);
        }
        else if (cp < 0x10000) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'u';
            buffer[2] = sky_ctype_lower_hexdigits[(cp >> 12) & 0xF];
            buffer[3] = sky_ctype_lower_hexdigits[(cp >>  8) & 0xF];
            buffer[4] = sky_ctype_lower_hexdigits[(cp >>  4) & 0xF];
            buffer[5] = sky_ctype_lower_hexdigits[ cp        & 0xF];
            sky_string_data_append(new_string_data, buffer, 6, 1);
        }
        else {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            buffer[1] = 'U';
            buffer[2] = sky_ctype_lower_hexdigits[(cp >> 28) & 0xF];
            buffer[3] = sky_ctype_lower_hexdigits[(cp >> 24) & 0xF];
            buffer[4] = sky_ctype_lower_hexdigits[(cp >> 20) & 0xF];
            buffer[5] = sky_ctype_lower_hexdigits[(cp >> 16) & 0xF];
            buffer[6] = sky_ctype_lower_hexdigits[(cp >> 12) & 0xF];
            buffer[7] = sky_ctype_lower_hexdigits[(cp >>  8) & 0xF];
            buffer[8] = sky_ctype_lower_hexdigits[(cp >>  4) & 0xF];
            buffer[9] = sky_ctype_lower_hexdigits[ cp        & 0xF];
            sky_string_data_append(new_string_data, buffer, 10, 1);
        }
    }
    sky_string_data_append_run(start, c - start);

    buffer[0] = (char)quote;
    sky_string_data_append(new_string_data, buffer, 1, 1);

    return new_string;
}


static sky_list_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_rsplit_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        sky_string_t                sep,
        ssize_t                     maxsplit)
{
    const T             *c, *end, *start;
    ssize_t             index;
    sky_list_t          list;
    sky_string_t        piece;
    sky_string_data_t   *sep_data, sep_tagged_data;

    if (maxsplit >= 0) {
        list = sky_list_createwithcapacity(SKY_MIN(maxsplit, 12));
    }
    else {
        maxsplit = SSIZE_MAX;
        list = sky_list_createwithcapacity(12);
    }

    start = string_data->data.F;
    end = start + string_data->len;

    if (sky_object_isnull(sep)) {
        /* split by whitespace runs */
        while (maxsplit-- > 0 && end > start) {
            while (end > start && sky_unicode_isspace(*(end - 1))) {
                --end;
            }
            if (end <= start) {
                break;
            }

            for (c = end - 1;
                 c > start && !sky_unicode_isspace(*(c - 1));
                 --c);

            piece = sky_string_createfromcodepoints(
                            c,
                            (end - c) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
            sky_list_append(list, piece);
            end = c - 1;
        }
        if (end > start) {
            while (end > start && sky_unicode_isspace(*(end - 1))) {
                --end;
            }
            if (end > start) {
                piece = sky_string_createfromcodepoints(
                                start,
                                (end - start) * SKY_STRING_WIDTH,
                                SKY_STRING_WIDTH);
                sky_list_append(list, piece);
            }
        }
    }
    else {
        sep_data = sky_string_data(sep, &sep_tagged_data);
        if (!sep_data->len) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        /* split by a separator */
        while (maxsplit-- > 0 && end > start) {
            index = SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                            string_data,
                            sep_data,
                            start - string_data->data.F,
                            end - string_data->data.F,
                            -1,
                            0);
            if (index < 0) {
                break;
            }
            c = string_data->data.F + index + sep_data->len;
            piece = sky_string_createfromcodepoints(
                            c,
                            (end - c) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
            sky_list_append(list, piece);
            end = c - sep_data->len;
        }

        if (start <= end) {
            if (end - start == string_data->len) {
                piece = sky_string_copy(string);
            }
            else {
                piece = sky_string_createfromcodepoints(
                                start,
                                (end - start) * SKY_STRING_WIDTH,
                                SKY_STRING_WIDTH);
            }
            sky_list_append(list, piece);
        }
        else if (!sky_list_len(list)) {
            sky_list_append(list, sky_string_empty);
        }
    }

    sky_list_reverse(list);
    return list;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_rstrip_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   chars_data)
{
    static sky_bool_t (*findchar)(const sky_string_data_t *, T) =
                      SKY_TOKEN_PASTE_EXPANDED(sky_string_findchar_, F);

    const T             *end, *start;
    ssize_t             length, start_index, step, stop_index;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    if (!string_data->len) {
        return sky_string_empty;
    }
    start = string_data->data.F;
    end = string_data->data.F + string_data->len;
    if (!chars_data) {
        if (!(string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_SPACE)) {
            return string;
        }
        while (end > start && sky_unicode_isspace(*(end - 1))) {
            --end;
        }
    }
    else {
        if (!(string_data->any_ctype_flags & chars_data->any_ctype_flags)) {
            return string;
        }
        while (end > start && findchar(chars_data, *(end - 1))) {
            --end;
        }
    }


    start_index = 0;
    stop_index = end - string_data->data.F;
    step = 1;
    length = sky_sequence_indices(string_data->len,
                                  &start_index,
                                  &stop_index,
                                  &step);
    if (length == string_data->len) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_append_slice(new_string_data,
                                 string_data,
                                 length,
                                 start_index,
                                 step);

    return new_string;
}


static ssize_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   sub_data,
        ssize_t                     start,
        ssize_t                     end,
        const int                   mode,
        ssize_t                     maxcount)
    /* modes: 0 = count, -1 = reverse, 1 = forward */
    /* maxcount is ignored if mode != 0 */
{
    const T             *startcp;
    ssize_t             count, i, j, length, skip, sub_last;
    unsigned long       mask;
    sky_unicode_char_t  sub_firstcp, sub_lastcp;

    if (!mode) {
        if (!maxcount) {
            return 0;
        }
        if (maxcount <= 0) {
            maxcount = SSIZE_MAX;
        }
    }
    else {
        /* Code later makes assumptions about -1/1/0 */
        sky_error_validate_debug(-1 == mode || 1 == mode);
    }

    length = sky_string_data_range(string_data, &start, &end);
    if (start > string_data->len) {
        return (mode ? -1 : 0);
    }
    if (!sub_data->len) {
        if (!mode) {
            return (length < maxcount ? length + 1 : maxcount);
        }
        return (mode < 0 ? end : start);
    }
    if (!length || length < sub_data->len) {
        return (mode ? -1 : 0);
    }
    if (sub_data->highest_codepoint > string_data->highest_codepoint) {
        return (mode ? -1 : 0);
    }
    if ((string_data->any_ctype_flags & sub_data->any_ctype_flags) !=
        sub_data->any_ctype_flags)
    {
        return (mode ? -1 : 0);
    }
    count = 0;

    startcp = &(string_data->data.F[start]);
#if SKY_STRING_WIDTH == 1
    if (1 == sub_data->len) {
        if (1 == mode) {
            const T *cp = memchr(startcp, sub_data->data.latin1[0], length);
            return (cp ? cp - string_data->data.F : -1);
        }
        else if (-1 == mode) {
            const T *cp = memrchr(startcp, sub_data->data.latin1[0], length);
            return (cp ? cp - string_data->data.F : -1);
        }
        else {
            uint8_t subcp;
            const T *cp, *endcp;

            subcp = sub_data->data.latin1[0];
            endcp = startcp + length;
            while (startcp < endcp) {
                if (!(cp = memchr(startcp, subcp, endcp - startcp))) {
                    break;
                }
                if (++count >= maxcount) {
                    break;
                }
                startcp = cp + 1;
            }
            return count;
        }
    }
#endif

    sub_last = sub_data->len - 1;
    skip = sub_last - 1;

    mask = 0;
    if (sub_data->highest_codepoint < 0x100) {
        sub_firstcp = sub_data->data.latin1[0];
        sub_lastcp = sub_data->data.latin1[sub_last];
        if (mode >= 0) {
            for (i = 0; i < sub_last; ++i) {
                mask |= sky_string_mask(sub_data->data.latin1[i]);
                if (sub_data->data.latin1[i] == sub_lastcp) {
                    skip = sub_last - i - 1;
                }
            }
        } else {
            for (i = sub_last; i > 0; --i) {
                mask |= sky_string_mask(sub_data->data.latin1[i]);
                if (sub_data->data.latin1[i] == sub_firstcp) {
                    skip = i - 1;
                }
            }
        }
    }
    else if (sub_data->highest_codepoint < 0x10000) {
        sub_firstcp = sub_data->data.ucs2[0];
        sub_lastcp = sub_data->data.ucs2[sub_last];
        if (mode >= 0) {
            for (i = 0; i < sub_last; ++i) {
                mask |= sky_string_mask(sub_data->data.ucs2[i]);
                if (sub_data->data.ucs2[i] == sub_lastcp) {
                    skip = sub_last - i - 1;
                }
            }
        }
        else {
            for (i = sub_last; i > 0; --i) {
                mask |= sky_string_mask(sub_data->data.ucs2[i]);
                if (sub_data->data.ucs2[i] == sub_firstcp) {
                    skip = i - 1;
                }
            }
        }
    }
    else {
        sub_firstcp = sub_data->data.ucs4[0];
        sub_lastcp = sub_data->data.ucs4[sub_last];
        if (mode >= 0) {
            for (i = 0; i < sub_last; ++i) {
                mask |= sky_string_mask(sub_data->data.ucs4[i]);
                if (sub_data->data.ucs4[i] == sub_lastcp) {
                    skip = sub_last - i - 1;
                }
            }
        }
        else {
            for (i = sub_last; i > 0; --i) {
                mask |= sky_string_mask(sub_data->data.ucs4[i]);
                if (sub_data->data.ucs4[i] == sub_firstcp) {
                    skip = i - 1;
                }
            }
        }
    }
    mask |= sky_string_mask((mode < 0 ? sub_firstcp : sub_lastcp));

    length -= sub_data->len;
    if (mode >= 0) {
        for (i = 0; i <= length; ++i) {
            if (startcp[i + sub_data->len - 1] == sub_lastcp) {
                j = SKY_TOKEN_PASTE_EXPANDED(sky_string_match_, F)
                            (sub_data, &(startcp[i]));
                if (j == sub_data->len) {
                    if (mode) {
                        return i + start;
                    }
                    if (++count >= maxcount) {
                        return count;
                    }
                    i += sub_last;
                }
                else if (!(mask & sky_string_mask(startcp[i + sub_data->len]))) {
                    i += sub_data->len;
                }
                else {
                    i += skip;
                }
            }
            else if (!(mask & sky_string_mask(startcp[i + sub_data->len]))) {
                i += sub_data->len;
            }
        }
    }
    else {
        for (i = length; i >= 0; --i) {
            if (startcp[i] == sub_firstcp) {
                j = SKY_TOKEN_PASTE_EXPANDED(sky_string_match_, F)
                            (sub_data, &(startcp[i]));
                if (j == sub_data->len) {
                    return start + i;
                }
                if (i > 0 && !(mask & sky_string_mask(startcp[i - 1]))) {
                    i -= sub_data->len;
                }
                else {
                    i -= skip;
                }
            }
            else if (i > 0 && !(mask & sky_string_mask(startcp[i - 1]))) {
                i -= sub_data->len;
            }
        }
    }

    return (mode ? -1 : count);
}


static sky_list_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_split_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        sky_object_t                sep,
        ssize_t                     maxsplit)
{
    const T             *c, *end, *start;
    ssize_t             index;
    sky_list_t          list;
    sky_string_t        piece;
    sky_string_data_t   *sep_data, sep_tagged_data;

    if (maxsplit >= 0) {
        list = sky_list_createwithcapacity(SKY_MIN(maxsplit, 12));
    }
    else {
        maxsplit = SSIZE_MAX;
        list = sky_list_createwithcapacity(12);
    }

    start = string_data->data.F;
    end = start + string_data->len;

    if (sky_object_isnull(sep)) {
        /* split by whitespace runs */
        while (maxsplit-- > 0 && start < end) {
            while (start < end && sky_unicode_isspace(*start)) {
                ++start;
            }
            if (start >= end) {
                break;
            }

            for (c = start + 1; c < end && !sky_unicode_isspace(*c); ++c);
            piece = sky_string_createfromcodepoints(
                            start,
                            (c - start) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
            sky_list_append(list, piece);
            start = c + 1;
        }
        if (start < end) {
            while (start < end && sky_unicode_isspace(*start)) {
                ++start;
            }
            if (start < end) {
                piece = sky_string_createfromcodepoints(
                                start,
                                (end - start) * SKY_STRING_WIDTH,
                                SKY_STRING_WIDTH);
                sky_list_append(list, piece);
            }
        }
    }
    else {
        sep_data = sky_string_data(sep, &sep_tagged_data);
        if (!sep_data->len) {
            sky_error_raise_string(sky_ValueError, "empty separator");
        }

        /* split by a separator */
        while (maxsplit-- > 0 && start < end) {
            index = SKY_TOKEN_PASTE_EXPANDED(sky_string_search_, F)(
                            string_data,
                            sep_data,
                            start - string_data->data.F,
                            end - string_data->data.F,
                            1,
                            0);
            if (index < 0) {
                break;
            }
            c = string_data->data.F + index;
            piece = sky_string_createfromcodepoints(
                            start,
                            (c - start) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
            sky_list_append(list, piece);
            start = c + sep_data->len;
        }
        if (end - start == string_data->len) {
            piece = sky_string_copy(string);
        }
        else {
            piece = sky_string_createfromcodepoints(
                            start,
                            (end - start) * SKY_STRING_WIDTH,
                            SKY_STRING_WIDTH);
        }
        sky_list_append(list, piece);
    }

    return list;
}


static sky_list_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_splitlines_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        sky_bool_t                  keepends)
{
    const T         *c, *end, *ending, *start;
    sky_list_t      lines;
    sky_string_t    line;

    lines = sky_list_create(NULL);
    if (!string_data->len) {
        return lines;
    }

    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
        for (ending = end; c < end; ++c) {
            if ('\r' == *c) {
                ending = c++;
                if (c < end && '\n' == *c) {
                    ++c;
                }
                break;
            }
            if (sky_unicode_islinebreak(*c)) {
                ending = c++;
                break;
            }
        }
        if (keepends) {
            if (start == string_data->data.F && c == end) {
                line = sky_string_copy(string);
            }
            else {
                line = sky_string_createfromcodepoints(
                                start,
                                (c - start) * SKY_STRING_WIDTH,
                                SKY_STRING_WIDTH);
            }
        }
        else {
            if (start == string_data->data.F && ending == end) {
                line = sky_string_copy(string);
            }
            else {
                line = sky_string_createfromcodepoints(
                                start,
                                (ending - start) * SKY_STRING_WIDTH,
                                SKY_STRING_WIDTH);
            }
        }
        sky_list_append(lines, line);
        start = c--;
    }

    return lines;
}


static sky_bool_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_startswith_, F)(
        const sky_string_data_t *   string_data,
        sky_object_t                prefix,
        ssize_t                     start,
        ssize_t                     end)
{
    static ssize_t (*match)(const sky_string_data_t *, const T *) =
                   SKY_TOKEN_PASTE_EXPANDED(sky_string_match_, F);

    ssize_t             len, prefix_len;
    sky_bool_t          startswith;
    sky_tuple_t         prefixes;
    sky_string_data_t   *prefix_data, prefix_tagged_data;

    len = sky_string_data_range(string_data, &start, &end);
    if (start > string_data->len) {
        return SKY_FALSE;
    }
    if (sky_object_isa(prefix, sky_string_type)) {
        if (!(prefix_len = sky_object_len(prefix))) {
            return SKY_TRUE;
        }
        prefix_data = sky_string_data_noctype(prefix, &prefix_tagged_data);
        if (prefix_len <= len &&
            match(prefix_data,
                  &(string_data->data.F[start])) == prefix_len)
        {
            return SKY_TRUE;
        }
        return SKY_FALSE;
    }

    if (!sky_object_isa(prefix, sky_tuple_type)) {
        goto bad_prefix;
    }

    startswith = SKY_FALSE;
    prefixes = prefix;
    SKY_SEQUENCE_FOREACH(prefixes, prefix) {
        if (!(prefix_len = sky_object_len(prefix))) {
            startswith = SKY_TRUE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
        prefix_data = sky_string_data_noctype(prefix, &prefix_tagged_data);
        if (prefix_len <= len &&
            match(prefix_data, &(string_data->data.F[start])))
        {
            startswith = SKY_TRUE;
            SKY_SEQUENCE_FOREACH_BREAK;
        }
    } SKY_SEQUENCE_FOREACH_END;

    return startswith;

bad_prefix:
    sky_error_raise_format(
            sky_TypeError,
            "startswith first arg must be str or a tuple of str, not %@",
            sky_type_name(sky_object_type(prefix)));
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_strip_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        const sky_string_data_t *   chars_data)
{
    static sky_bool_t (*findchar)(const sky_string_data_t *, T) =
                      SKY_TOKEN_PASTE_EXPANDED(sky_string_findchar_, F);

    const T             *end, *start;
    ssize_t             length, start_index, stop_index, step;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;

    if (!string_data->len) {
        return sky_string_empty;
    }
    start = string_data->data.F;
    end = string_data->data.F + string_data->len;
    if (!chars_data) {
        if (!(string_data->any_ctype_flags & SKY_UNICODE_CTYPE_FLAG_SPACE)) {
            return string;
        }
        while (start < end && sky_unicode_isspace(*start)) {
            ++start;
        }
        while (end > start && sky_unicode_isspace(*(end - 1))) {
            --end;
        }
    }
    else {
        if (!(string_data->any_ctype_flags & chars_data->any_ctype_flags)) {
            return string;
        }
        while (start < end && findchar(chars_data, *start)) {
            ++start;
        }
        while (end > start && findchar(chars_data, *(end - 1))) {
            --end;
        }
    }

    start_index = start - string_data->data.F;
    stop_index = end - string_data->data.F;
    step = 1;
    length = sky_sequence_indices(string_data->len,
                                  &start_index,
                                  &stop_index,
                                  &step);
    if (length == string_data->len) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_append_slice(new_string_data,
                                 string_data,
                                 length,
                                 start_index,
                                 step);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_swapcase_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  simple, *special, swapped_cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_LOWER) {
            special = ctype->special_upper;
            simple = ctype->simple_upper;
        }
        else if (ctype->flags & SKY_UNICODE_CTYPE_FLAG_UPPER) {
            special = ctype->special_lower;
            simple = ctype->simple_lower;
        }
        else {
            continue;
        }

        sky_string_data_append_run(start, c - start);
        start = c + 1;
        if (special[0]) {
            sky_string_data_append(
                    new_string_data,
                    &(special[0]),
                    (special[1] ? (special[2] ? 3 : 2)
                                : 1) * sizeof(sky_unicode_char_t),
                    sizeof(sky_unicode_char_t));
        }
        else {
            swapped_cp = *c + simple;
            sky_string_data_append(new_string_data,
                                   &swapped_cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_title_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_bool_t          prev_cased;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    prev_cased = SKY_FALSE;
    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (!(ctype->flags & (SKY_UNICODE_CTYPE_FLAG_LOWER |
                              SKY_UNICODE_CTYPE_FLAG_TITLE |
                              SKY_UNICODE_CTYPE_FLAG_UPPER)))
        {
            prev_cased = SKY_FALSE;
            sky_string_data_append_run(start, c - start);
            start = c;
            continue;
        }
        if (prev_cased) {
            /* Make it lower */
            if (!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_LOWER)) {
                sky_string_data_append_run(start, c - start);
                start = c + 1;

                if (ctype->special_lower[0]) {
                    sky_string_data_append(
                            new_string_data,
                            &(ctype->special_lower[0]),
                            (ctype->special_lower[1] ?
                                    (ctype->special_lower[2] ? 3 : 2)
                                    : 1) * sizeof(sky_unicode_char_t),
                            sizeof(sky_unicode_char_t));
                }
                else {
                    cp = *c + ctype->simple_lower;
                    sky_string_data_append(new_string_data,
                                           &cp,
                                           sizeof(sky_unicode_char_t),
                                           sizeof(sky_unicode_char_t));
                }
            }
        }
        else {
            /* Make it title */
            sky_string_data_append_run(start, c - start);
            start = c + 1;

            if (ctype->special_title[0]) {
                sky_string_data_append(
                        new_string_data,
                        &(ctype->special_title[0]),
                        (ctype->special_title[1] ? (ctype->special_title[2] ? 3
                                                                            : 2)
                                                 : 1) * sizeof(sky_unicode_char_t),
                        sizeof(sky_unicode_char_t));
            }
            else {
                cp = *c + ctype->simple_title;
                sky_string_data_append(new_string_data,
                                       &cp,
                                       sizeof(sky_unicode_char_t),
                                       sizeof(sky_unicode_char_t));
            }
            prev_cased = SKY_TRUE;
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_translate_, F)(
        sky_string_t                string,
        const sky_string_data_t *   string_data,
        sky_dict_t                  table)
{
    const T             *c, *end, *start;
    sky_object_t        key, value;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data, tagged_data;
    sky_unicode_char_t  codepoint;

    start = string_data->data.F;
    end = string_data->data.F + string_data->len;

    /* Find the first character that needs translation. */
    if (sky_object_type(table) == sky_dict_type) {
        for (c = start; c < end; ++c) {
            key = SKY_OBJECT_MAKETAG(0x1, *c);
            if ((value = sky_dict_get(table, key, NULL)) != NULL) {
                break;
            }
        }
    }
    else {
        for (c = start; c < end; ++c) {
            key = SKY_OBJECT_MAKETAG(0x1, *c);
            SKY_ERROR_TRY {
                value = sky_object_getitem(table, key);
            } SKY_ERROR_EXCEPT(sky_LookupError) {
                value = NULL;
            } SKY_ERROR_TRY_END;
            if (value) {
                break;
            }
        }
    }
    if (c >= end) {
        return string;
    }

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_append_run(start, c - start);

    do {
        if (sky_object_isa(value, sky_string_type)) {
            sky_string_data_combine(new_string_data,
                                    sky_string_data(value, &tagged_data));
        }
        else if (sky_object_isa(value, sky_integer_type)) {
            SKY_ERROR_TRY {
                codepoint = sky_integer_value(value,
                                              SKY_UNICODE_CHAR_MIN,
                                              SKY_UNICODE_CHAR_MAX,
                                              NULL);
            } SKY_ERROR_EXCEPT(sky_OverflowError) {
                sky_error_raise_format(
                        sky_TypeError,
                        "character mapping must be in range(0x%X)",
                        (SKY_UNICODE_CHAR_MAX + 1));
            } SKY_ERROR_TRY_END;
            sky_string_data_append_codepoint(new_string_data, codepoint, 1);
        }
        else if (!sky_object_isnull(value)) {
            sky_error_raise_string(
                    sky_TypeError,
                    "character mapping must return integer, None or str");
        }

        /* Find the next character that needs translation. */
        if (sky_object_type(table) == sky_dict_type) {
            for (start = ++c; c < end; ++c) {
                key = SKY_OBJECT_MAKETAG(0x1, *c);
                if ((value = sky_dict_get(table, key, NULL)) != NULL) {
                    break;
                }
            }
        }
        else {
            for (start = ++c; c < end; ++c) {
                key = SKY_OBJECT_MAKETAG(0x1, *c);
                SKY_ERROR_TRY {
                    value = sky_object_getitem(table, key);
                } SKY_ERROR_EXCEPT(sky_LookupError) {
                    value = NULL;
                } SKY_ERROR_TRY_END;
                if (value) {
                    break;
                }
            }
        }
        sky_string_data_append_run(start, c - start);
    } while (c < end);

    return new_string;
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_upper_, F)(
        const sky_string_data_t *   string_data)
{
    const T             *c, *end, *start;
    sky_string_t        new_string;
    sky_string_data_t   *new_string_data;
    sky_unicode_char_t  upper_cp;
    sky_unicode_ctype_t *ctype;

    new_string = sky_object_allocate(sky_string_type);
    new_string_data = sky_string_data(new_string, NULL);
    sky_string_data_resize(new_string_data, string_data->len, SKY_STRING_WIDTH);

    start = string_data->data.F;
    end = start + string_data->len;
    for (c = start; c < end; ++c) {
#if SKY_STRING_WIDTH == 1
        ctype = sky_unicode_ctype_lookup_latin1(*c);
#else
        ctype = sky_unicode_ctype_lookup((sky_unicode_char_t)*c);
#endif
        if (ctype->special_upper[0]) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            sky_string_data_append(
                    new_string_data,
                    &(ctype->special_upper[0]),
                    (ctype->special_upper[1] ? (ctype->special_upper[2] ? 3
                                                                        : 2)
                                             : 1) * sizeof(sky_unicode_char_t),
                    sizeof(sky_unicode_char_t));
        }
        else if (ctype->simple_upper) {
            sky_string_data_append_run(start, c - start);
            start = c + 1;
            upper_cp = *c + ctype->simple_upper;
            sky_string_data_append(new_string_data,
                                   &upper_cp,
                                   sizeof(sky_unicode_char_t),
                                   sizeof(sky_unicode_char_t));
        }
    }
    sky_string_data_append_run(start, c - start);

    return new_string;
}


#undef sky_string_data_append_run


#undef T
#undef F
