#if SKY_STRING_WIDTH == 1
#   define F    latin1
#   define T    uint8_t
#elif SKY_STRING_WIDTH == 2
#   define F    ucs2
#   define T    uint16_t
#elif SKY_STRING_WIDTH == 4
#   define F    ucs4
#   define T    sky_unicode_char_t
#else
#   error implementation missing
#endif


static sky_object_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_format_parse_, F)(
        const T **          start,
        const T *           end,
        ssize_t *           number,
        sky_tuple_t         args,
        sky_object_t        kws,
        int                 depth)
{
    const T                 *c, *field, *format;
    ssize_t                 index, nbytes;
    sky_object_t            object;
    sky_string_t            format_spec, name;
    sky_string_builder_t    builder;

    if (depth > 2) {
        sky_error_raise_string(sky_ValueError,
                               "Max string recursion exceeded");
    }

    for (field = c = *start; c < end; ++c) {
        if ('}' == *c || '.' == *c || '[' == *c || '!' == *c || ':' == *c) {
            break;
        }
    }
    nbytes = (c - field) * SKY_STRING_WIDTH;
    name = sky_string_createfromcodepoints(*start, nbytes, SKY_STRING_WIDTH);
    if (sky_string_empty == name) {
        if (-1 == *number) {
            sky_error_raise_string(sky_ValueError,
                                   "cannot switch from manual field "
                                   "specification to automatic field "
                                   "numbering");
        }
        object = sky_tuple_get(args, *number);
        *number = *number + 1;
    }
    else {
        if (sky_unicode_isdigit(*field)) {
            *number = -1;
            index = sky_integer_value(sky_integer_createfromstring(name, 10),
                                      SSIZE_MIN,
                                      SSIZE_MAX,
                                      NULL);
            object = sky_tuple_get(args, index);
        }
        else {
            if (!kws) {
                sky_error_raise_format(sky_KeyError, "%#@", name);
            }
            object = sky_object_getitem(kws, name);
        }
    }

    while (c < end) {
        if ('.' == *c) {
            for (field = ++c; c < end; ++c) {
                if ('}' == *c || '.' == *c || '[' == *c ||
                    '!' == *c || ':' == *c)
                {
                    break;
                }
            }
            nbytes = (c - field) * SKY_STRING_WIDTH;
            name = sky_string_createfromcodepoints(field,
                                                   nbytes,
                                                   SKY_STRING_WIDTH);
            object = sky_object_getattr(object, name, sky_NotSpecified);
            continue;
        }
        if ('[' == *c) {
            for (field = ++c; c < end && ']' != *c; ++c);
            if (c >= end) {
                sky_error_raise_string(sky_ValueError,
                                       "Missing ']' in format string");
            }
            nbytes = (c - field) * SKY_STRING_WIDTH;
            name = sky_string_createfromcodepoints(field,
                                                   nbytes,
                                                   SKY_STRING_WIDTH);
            object = sky_object_getitem(object,
                                        sky_integer_createfromstring(name, 10));
            ++c;
            continue;
        }
        break;
    }

    if ('!' == *c) {
        switch (*++c) {
            case 'a':
                object = sky_object_ascii(object);
                break;
            case 'r':
                object = sky_object_repr(object);
                break;
            case 's':
                object = sky_object_str(object);
                break;
            default:
                if (*c >= 0x20 && *c < 0x7F) {
                    sky_error_raise_format(
                            sky_ValueError,
                            "Unknown conversion specifier %c",
                            *c);
                }
                else {
                    sky_error_raise_format(
                            sky_ValueError,
                            "Unknown conversion specifier \\x%x",
                            (unsigned int)*c);
                }
                break;
        }
        if (++c >= end) {
            sky_error_raise_string(sky_ValueError,
                                   "Single '{' encountered in format string");
        }
    }

    if (':' != *c) {
        format_spec = sky_string_empty;
    }
    else {
        builder = sky_string_builder_create();
        for (format = ++c; c < end && '}' != *c; ++c) {
            if (*c == '{') {
                if ((nbytes = (c - format) * SKY_STRING_WIDTH) > 0) {
                    sky_string_builder_appendcodepoints(builder,
                                                        format,
                                                        nbytes / SKY_STRING_WIDTH,
                                                        SKY_STRING_WIDTH);
                }
                ++c;
                sky_string_builder_append(
                        builder,
                        SKY_TOKEN_PASTE_EXPANDED(
                                sky_string_format_parse_, F)(&c,
                                                             end,
                                                             number,
                                                             args,
                                                             kws,
                                                             depth + 1));
                format = c + 1;
            }
        }
        if (c >= end) {
            sky_error_raise_string(sky_ValueError,
                                   "Single '{' encountered in format string");
        }
        if ((nbytes = (c - format) * SKY_STRING_WIDTH) > 0) {
            sky_string_builder_appendcodepoints(builder,
                                                format,
                                                nbytes / SKY_STRING_WIDTH,
                                                SKY_STRING_WIDTH);
        }
        format_spec = sky_string_builder_finalize(builder);
    }

    if ('}' != *c) {
        sky_error_raise_string(sky_ValueError,
                               "Single '{' encountered in format string");
    }

    *start = c;
    return sky_object_format(object, format_spec);
}


static sky_string_t
SKY_TOKEN_PASTE_EXPANDED(sky_string_format_, F)(
        sky_string_data_t * self_data,
        sky_tuple_t         args,
        sky_object_t        kws)
{
    const T                 *c, *end, *start;
    ssize_t                 number;
    sky_object_t            object;
    sky_string_builder_t    builder;

    builder = sky_string_builder_createwithcapacity(self_data->len);
    number = 0;

    start = self_data->data.F;
    end = start + self_data->len;
    for (c = self_data->data.F; c < end; ++c) {
        if ('}' == *c && '}' == c[1]) {
            sky_string_builder_appendcodepoints(builder,
                                                start,
                                                (++c - start) / SKY_STRING_WIDTH,
                                                SKY_STRING_WIDTH);
            start = c + 1;
            continue;
        }
        if ('{' != *c) {
            continue;
        }
        if (c > start) {
            sky_string_builder_appendcodepoints(builder,
                                                start,
                                                (c - start) / SKY_STRING_WIDTH,
                                                SKY_STRING_WIDTH);
        }
        if ('{' == *++c) {
            sky_string_builder_appendcodepoint(builder, '{', 1);
            start = c + 1;
            continue;
        }

        object = SKY_TOKEN_PASTE_EXPANDED(sky_string_format_parse_, F)(&c,
                                                                       end,
                                                                       &number,
                                                                       args,
                                                                       kws,
                                                                       1);
        sky_string_builder_append(builder, object);

        start = c + 1;
    }

    if (start < end) {
        sky_string_builder_appendcodepoints(builder,
                                            start,
                                            (end - start) / SKY_STRING_WIDTH,
                                            SKY_STRING_WIDTH);
    }
    return sky_string_builder_finalize(builder);
}


static void
SKY_TOKEN_PASTE_EXPANDED(sky_string_parseformatspecification_, F)(
        sky_string_data_t *         self_data,
        sky_format_specification_t *spec)
{
    const T     *c, *end;
    uint32_t    flag;

    c = self_data->data.F;
    end = c + self_data->len;

    /* [[fill]align] */
    if (c + 1 < end &&
        (flag = sky_string_format_align_flag(c[1])) != 0)
    {
        spec->fill = *c;
        spec->flags = (spec->flags & ~SKY_FORMAT_FLAG_ALIGN_MASK) |
                      SKY_FORMAT_FLAG_SPECIFIED_FILL |
                      SKY_FORMAT_FLAG_SPECIFIED_ALIGN |
                      flag;
        c += 2;
    }
    else if ((flag = sky_string_format_align_flag(*c)) != 0) {
        spec->flags = (spec->flags & ~SKY_FORMAT_FLAG_ALIGN_MASK) |
                      SKY_FORMAT_FLAG_SPECIFIED_ALIGN |
                      flag;
        ++c;
    }
            
    /* [sign] */
    if (c < end && (flag = sky_string_format_sign_flag(*c)) != 0) {
        spec->flags = (spec->flags & ~SKY_FORMAT_FLAG_SIGN_MASK) |
                      SKY_FORMAT_FLAG_SPECIFIED_SIGN |
                      flag;
        ++c;
    }

    /* [#] */
    if (c < end && *c == '#') {
        spec->flags |= SKY_FORMAT_FLAG_ALTERNATE_FORM;
        ++c;
    }

    /* [0] */
    if (c < end && *c == '0' &&
        !(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_FILL))
    {
        spec->flags |= SKY_FORMAT_FLAG_ZERO_PAD;
        spec->fill = '0';
        if (!(spec->flags & SKY_FORMAT_FLAG_SPECIFIED_ALIGN)) {
            spec->flags &= ~SKY_FORMAT_FLAG_ALIGN_MASK;
            spec->flags |= SKY_FORMAT_FLAG_SPECIFIED_ALIGN |
                           SKY_FORMAT_FLAG_ALIGN_PADDED;
        }
        ++c;
    }

    /* [minimumwidth] */
    if (c < end && sky_unicode_isdigit(*c)) {
        spec->flags |= SKY_FORMAT_FLAG_SPECIFIED_WIDTH;
        while (c < end && sky_unicode_isdigit(*c)) {
            spec->width = (spec->width * 10) + sky_unicode_integer(*c);
            ++c;
        }
    }

    /* Not in PEP 3101, but in CPython: [,] */
    if (c < end && *c == ',') {
        spec->flags |= SKY_FORMAT_FLAG_THOUSANDS_SEPARATORS;
        ++c;
    }

    /* [.precision] */
    if (c + 1 < end && *c == '.' && sky_unicode_isdigit(c[1] )) {
        spec->flags |= SKY_FORMAT_FLAG_SPECIFIED_PRECISION;
        for (++c; c < end && sky_unicode_isdigit(*c); ++c) {
            spec->precision = (spec->precision * 10) + sky_unicode_integer(*c);
        }
    }

    /* [type] */
    if (c < end) {
        spec->type = *c++;
    }

    if (c < end ) {
        sky_error_raise_string(sky_ValueError, "Invalid format specifier");
    }
}


#undef T
#undef F
