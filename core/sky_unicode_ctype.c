#include "sky_private.h"
#include "sky_unicode_private.h"
#include "sky_unicode_ctype.h"


static sky_unicode_ctype_data_t sky_unicode_ctype_data_struct = {
    sky_unicode_ctypes,
    sky_unicode_ctype_shift,
    sky_unicode_ctype_index_1,
    sky_unicode_ctype_index_2,
    sky_unicode_ctypes_latin1
};
const sky_unicode_ctype_data_t * const sky_unicode_ctype_tables =
    &sky_unicode_ctype_data_struct;


sky_bool_t
sky_unicode_isalnum(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & (SKY_UNICODE_CTYPE_FLAG_ALPHA |
                              SKY_UNICODE_CTYPE_FLAG_DECIMAL));
}


sky_bool_t
sky_unicode_isalpha(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_ALPHA);
}


sky_bool_t
sky_unicode_isdecimal(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_DECIMAL);
}


sky_bool_t
sky_unicode_isdigit(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_DIGIT);
}


sky_bool_t
sky_unicode_isfractional(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_FRACTIONAL);
}


sky_bool_t
sky_unicode_isintegral(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_INTEGRAL);
}


sky_bool_t
sky_unicode_islinebreak(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_LINEBREAK);
}


sky_bool_t
sky_unicode_islower(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_LOWER);
}


sky_bool_t
sky_unicode_isnumeric(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_NUMERIC);
}


sky_bool_t
sky_unicode_isprint(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_PRINTABLE);
}


sky_bool_t
sky_unicode_isspace(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_SPACE);
}


sky_bool_t
sky_unicode_istitle(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_TITLE);
}


sky_bool_t
sky_unicode_isupper(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_UPPER);
}


sky_bool_t
sky_unicode_isxid_continue(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_XID_CONTINUE);
}


sky_bool_t
sky_unicode_isxid_start(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return !!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_XID_START);
}


double
sky_unicode_fraction(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    if (!(ctype->flags & SKY_UNICODE_CTYPE_FLAG_FRACTIONAL)) {
        return 0.0;
    }
    return (double)ctype->numeric.fraction.numerator /
           (double)ctype->numeric.fraction.denominator;
}


int64_t
sky_unicode_integer(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return ctype->numeric.integer;
}


sky_unicode_char_t
sky_unicode_tolower(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return c + ctype->simple_lower;
}


sky_unicode_char_t
sky_unicode_totitle(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return c + ctype->simple_title;
}


sky_unicode_char_t
sky_unicode_toupper(sky_unicode_char_t c)
{
    sky_unicode_ctype_t *ctype = sky_unicode_ctype_lookup(c);

    return c + ctype->simple_upper;
}
